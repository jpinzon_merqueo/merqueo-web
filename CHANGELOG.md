# changelog

# [2.42.4] Hotfix 2020-12-01 17:20

[Fix] Ajusta la consulta para obtener el país a partir del storeId al enviar el email de "Hemos recibido tu pedido".

# [2.42.3] Hotfix 2020-11-12 12:52

[Fix] Ajustes en método que retorna datos de turno del usuario para recibir la orden.

# [2.42.2] Hotfix 2020-11-12 12:52

[Fix] Varias ajustes en servicio de comunicación con SNS.

# [2.42.1] Hotfix 2020-11-11 20:00

[Fix] Error enviando notificación SNS en post-checkout.

# [2.42.0] Release 2020-11-11 15:48

## Merged in feature/PRD-3932-envio-notificaciones-sns-postcheckout (pull request #381)(jpinzon@merqueo.com)

Feature/PRD-3932 envio notificaciones sns postcheckout

- Merge branch 'hotfix/2.41.1'

- Se agrega notificacion SNS despues de creada la orden.
  https://merqueo.atlassian.net/browse/PRD-3932

- Se adicionan pruebas unitarias y se actualiza el contrato para la SNS

- Actualizados los comentarios de funciones y reducción de condicionales anidados.

Approved-by: Camilo Andres Rojas Chaparro
Approved-by: Marlon Oviedo Rueda
Approved-by: Johan Álvarez Colmenares

# [2.41.1] Hotfix 2020-10-15 17:26

[Fix] Se elimina el llamado al endpoint de la información de la tarjeta a través de tpaga

# [2.41.0] Release 2020-10-13 09:38

## [feature/PRD-3439] descartar tarjeta CODENSA en el listar métodos de pago del help-desk(mcastaneda@merqueo.com)

### Author: Miguel Castañeda <mcastaneda@merqueo.com>

### Date: Thu Oct 8 15:57:33 -05 2020

Quita las tarjetas de crédito CODENSA en los métodos de pago que se listan en el help-desk

Approved-by: Ariel Patiño Arcos
Approved-by: Ricardo Martinez
Approved-by: Jonathan Guerrero Olivera
Approved-by: Ericka Almeida

# [2.40.3] Hotfix 2020-10-06 17:08

[Fix] Se ajusta consulta que obtiene los datos de una orden, agregando el atributo status, que se valida cuando se va a actualizar el estado de una
orden, mediante el pago por PSE.

# [2.40.2] Hotfix 2020-10-06 10:20

[Fix] Se previene cambiar estado de orden en callback de PayU cuando la orden NO tiene el estado de Validación, que es con el que inician los pedidos pagados mediante PSE.

# [2.40.1] Hotfix 2020-10-01 11:32

[fix] se corrige error en banners

# [2.40.0] Release 2020-10-01 11:32

## Merged in feature/BDC-3260-change-status-tracker-texts (pull request #379)(cvarango@merqueo.com)

Feature/BDC-3260 change status tracker texts

- [add] validate delivery date

Approved-by: Marlon Oviedo Rueda
Approved-by: David Felipe Villegas Ramirez

## Merged in feature/BDC-1263-Rotación-banners-del-home (pull request #378)(rrivera@merqueo.com)

Implementacion de ordenamiento de banner en la api v1.3

- Implementacion de ordenamiento de banner en la api v1.3

Approved-by: Marlon Oviedo Rueda
Approved-by: Ricardo Martinez

## Quita caracteres sobrantes en CHANGELOG.md(arielp@merqueo.com)

## Corrige conflictos en CHANGELOG.md(arielp@merqueo.com)

## [Feat] Agregar el banco en la tabla order_payments cuando el pago es a través de PSE(jonathang@merqueo.com)

### Author: Jonathan Guerrero <jonathang@merqueo.com>

### Date: Wed Sep 16 17:01:41 -05 2020

Guarda el banco que retorna PSE al momento de la confirmación.

Approved-by: Ariel Patiño Arcos
Approved-by: David Felipe Villegas Ramirez
Approved-by: Guillermo Elias Romo Noriega

# [2.39.2] Hotfix 2020-09-16 10:53

Se añadió la validación del campo cc_payment_description para ejecutar la modificación en el pago con tarjeta.

# [2.39.1] Hotfix 2020-08-04 16:05

fix: Solved eloquent error in user credits.

# [2.39.0] Release 2020-09-02 08:48

## Bugfix/BDC-1566 delete credit assign

### Author: Jeremy Reyes

Return coupon only when attendant is different to "Políticas Merqueo".
Return credits only when it is different to coupons.

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Yesika Vanegas
Approved-by: Leyniker Raul Rivera

## feature/BDC-739-backend-banner-video-banner-mult(gromo@merqueo.com)

### Author: Guillermo Romo <gromo@merqueo.com>

### Date: Tue Aug 18 11:18:50 -05 2020

Actualiza endpoint para que retorne los banners tipo video.

Approved-by: Ariel Patiño Arcos
Approved-by: Jean Frank Olaya Albino
Approved-by: Jonathan Guerrero Olivera

# [2.38.4] Hotfix 2020-08-04 16:05

Se da retro compatibilidad a las aplicaciones que usan los campos is_checked y is_on_the_way

# [2.38.3] Hotfix 2020-07-31 17:30

Se realia corrección en asignación de estado para una orden cuando se confirma estado de cobro desde PayU

# [2.38.2] Hotfix 2020-07-25 13:30

Se quitan estilos de Marketplace a plantilla de mail de pedido recibido

# [2.38.1] Hotfix 2020-07-22 18:44

Corrige EloquentCountryRepository->findByStoreId() para que use city_store.store_id
en lugar de cities.coverage_store_id porque los marketplace no existen en cities.

# [2.38] Release 2020-07-09 11:32

## Merged in feature/BDO-525-modify-subjects-emails (pull request #372)(eheredia@merqueo.com)

Actualización a texto de asunto y títulos del correo de orden recibida de "Tu mercado" por "Tu Merqueo"

Approved-by: Julio Alberto De Hoyos Martinez
Approved-by: Jonathan Sanchez
Approved-by: Johan Álvarez Colmenares
Approved-by: Jhon Sánchez

# [2.37.3] Hotfix 2020-06-19 17:35

- fix: Upgrade country query "findByStoreId".

# [2.37.2] Hotfix 2020-06-17 08:58

- Se ajustó servicio order status, no existe driver para las ordenes hasta no hacer la planeación

# [2.37.1] Hotfix 2020-06-10 11:55

- Se ajustó servicio order status, para devolver llave que identifica si el pedido es transportado por merqueo o por mensajeros urbanos

# [2.37.0] release 2020-06-08 10:45

## Merged in feature/BDC-656-best-price-vs-competitive-price (pull request #373)

### Feature/BDC-656 best price vs competitive price

#### Author: José Jorge Armenta Martínez <jarmenta@merqueo.com>

#### Date: Mon Jun 8 14:14:58 2020 +0000

- [Feat] Se agrega condición en el servicio de departamento para qué el mejor precio o producto destacado, tenga prioridad sobre el precio competitivo

- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/BDC-656-best-price-vs-competitive-price

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Norbert Steven Forero Galan

## Merged in feature/feature/BDC-632-delivery-discount (pull request #369)

### feat: The minimum delivery amount was changed.

#### Author: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>

#### Date: Thu May 28 20:02:12 2020 +0000

- feat: The minimum delivery amount was changed.

Before, this information was obtained from "discounts" table but now the "stores" table have the value with the minimum_order_amount_free_delivery name.

Approved-by: Sebastian Bladimir Iguavita Roa
Approved-by: Camilo Andres Rojas Chaparro
Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Guillermo Elias Romo Noriega

# [2.36.0] Hotfix 2020-07-28 10:30

- Ejecutar encendido de apagado de producto aun si lo la orden es creada en estado de Validación en el postCheckout

# [2.36] release 2020-05-27 17:26

## Merged in feature/BDO-573-Migrar-funcion-de-encendido-y-apagado-de-productos (pull request #370)(gluna@merqueo.com)

Feature/BDO-573 Migrar funcion de encendido y apagado de productos

Approved-by: Yeison Adrian Trejos Balaguera
Approved-by: Ricardo Martinez
Approved-by: Luis Paloma
Approved-by: Marlon Duván Oviedo Rueda

## Merged in feature/BDC-787-update-reasons (pull request #371)(jreyes@merqueo.com)

fix: Reasons was upgrade.

Approved-by: Cristian camilo Cantor Sua
Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Sebastian Bladimir Iguavita Roa

## Merged in bugfix/move-google-key-env (pull request #366)(dsalazar@merqueo.com)

Se mueve google key de BD a env

Approved-by: Johan Fabian Rodriguez Pinzon
Approved-by: Yeison Adrian Trejos Balaguera
Approved-by: Luis Carlos Rosas Orellano
Approved-by: Johan Álvarez Colmenares

# [2.35.2] Hotfix 2020-05-05 23:34

- El control del deployment ahora se hace con los eventos de codedeploy

# [2.35.1] Hotfix 2020-05-04 12:31

- Cast a int de \$this->sponsored

# [2.35.0] Release 2020-04-05 11:18

## Merged in bugfix/BDC-563-backendchore-readjust-rules-for-sponsored-products (pull request #368)

### Dentro de Dash actualmente se pueden patrocinar productos, este patrocinado tiene varias reglas que son: No se puede patrocinar la primera posición , no se puede patrocinar en un pasillo que tenga menos de 10 productos y no se puede patrocinar más de un producto por pasillo. Se espera cambiar estás 3 reglas. Para est PR son solo dos reglas las que se implementan.

#### Author: Ariel Rene Patiño Arcos <arielp@merqueo.com>

#### Date: Mon May 4 15:14:42 2020 +0000

- Se puede patrocinar la primera posición
- No se puede patrocinar pasillos que tenga menos de 4 productos

Approved-by: Guillermo Elias Romo Noriega <gromo@merqueo.com>
Approved-by: Miguel Angel Castañeda Vasquez <mcastaneda@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>

## Merged in feature/BDC-458-devolver-autorizacion-cuando-cancela-helpcenter (pull request #364)

### Author: Miguel Angel Castañeda Vasquez <mcastaneda@merqueo.com>

### Date: Mon May 4 14:55:31 2020 +0000

Al momento que se genere una cancelación de orden por parte del cliente debe garantizarse la devolución de la autorización siempre y cuando cumpla con:
Orden creada en el aplicativo con la configuración de México
Orden creada con método de pago tarjeta de crédito
La transacción de autorización del cobro que se genero una vez se crea la orden tenga el estado Aprobada

Approved-by: Guillermo Elias Romo Noriega <gromo@merqueo.com>
Approved-by: Sergio Rodríguez <serodriguez@merqueo.com>
Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Cristian Julian Rincon Perez <crinconp@merqueo.com>

## Merged in feature/BDC-590-modify-department-service-in-1.3 (pull request #367)

### feat/BDC 590 Validate express catalog against stock picking

#### Author: Cristian camilo Cantor Sua <ccantor@merqueo.com>

#### Date: Wed Apr 29 21:18:22 2020 +0000

- [feat] modify scope to StoreProductWarehouse to validate picking stock when zone is nodo
- [fix] correction to validate departments with picking stock

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Yesika Vanegas

# [2.34.4] Hotfix 2020-04-29 16:31

- se corrige conflicto entre asg y codedeploy en el despliegue

# [2.34.3] Hotfix 2020-04-20 18:54

- se remueve la seguridad en el endpoint: `api/1.3/store/*/banners/*`

# [2.34.1] Release 2020-04-16 15:14

## Merged in bugfix/fix-public-price (pull request #365)

### Bugfix/fix public price

#### Date: Thu Apr 16 20:05:03 2020 +0000

#### Author: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>

- Ajuste de validacion de publi price
- fix: Added get order by user to ApiStoreController - search method.
- Merge remote-tracking branch 'origin/bugfix/fix-public-price' into bugfix/fix-public-price
- fix: Added get order by user to ApiStoreController - get_product method.
- fix: Added verification to ValidateRealPublicPrice.
- Ajuste de variable en el checkout
- fix: Added to single banner user has order.
- Merge remote-tracking branch 'origin/bugfix/fix-public-price' into bugfix/fix-public-price
- fix: Added orders to methods.
- revert: Revert verification.

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Cristian Julian Rincon Perez

# [2.34.1] Hotfix 2020-04-14 16:54

- se corrige ValidateRealPublicPriceUseCase

# [2.34] Release 2020-04-14 09:20

## Merged in feature/BDO-336-deshabilitar-menú-de-planeación- (pull request #361)(mbarrera@merqueo.com)

Menú de planeación de rutas fue eliminado

Approved-by: Johan Fabian Rodriguez Pinzon
Approved-by: Luis Carlos Rosas Orellano
Approved-by: Jeremy Jose Reyes Barrios

## Merged in bugfix/disable-free-delivery-firts-order (pull request #360)(rrivera@merqueo.com)

Deshabilitar domicilios gratis para primera compra

- Deshabilitar domicilios gratis para primera compra

- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into bugfix/disable-free-delivery-firts-order

- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into bugfix/disable-free-delivery-firts-order

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Norbert Steven Forero Galan

# [2.33] Release 2020-04-14 09:20

## Merged in BDC-401-price-vs-other-prices (pull request #363)(jreyes@merqueo.com)

**Mejor precio**
Permite poder ver cuáles de los productos de Merqueo tienen un mejor precio en comparación al precio promedio en supermercados similares de la ciudad, con el fin de convencer de que Merqueo tiene los precios más económicos y es la mejor opción a la hora de comprar el mercado.

Approved-by: Yesika Vanegas
Approved-by: Kennit David Ruz Romero
Approved-by: Marlon Duván Oviedo Rueda

## Merged in bugfix/BDO-251-error-discount-unit-product (pull request #359)(rrivera@merqueo.com)

**Productos con stock de promoción**
Cuando un producto tiene límite de unidades en una promoción activa, se espera que a medida que los clientes compren dichos productos, el número de productos disponibles de la promoción vaya disminuyendo, y una vez el total de disponibles de la promoción llegue a 0, la promoción debería terminar.

Approved-by: Johan Álvarez Colmenares
Approved-by: Sebastian Bladimir Iguavita Roa

# [2.32.1] Release 2020-03-30 15:54

## Merged in bugfix/BDC-399-quitar-autorización-de-los-endpoints-banners (pull request #362)

### Quita la validación de autorización a los endpoints api v1.3 /banners y /departments-menu

#### Author: Jonathan Guerrero Olivera <jonathang@merqueo.com>

#### Date: Mon Mar 30 20:52:33 2020 +0000

- [Progress] Quita la validación de autorización a los endpoints api/1.3/store/{storeId}/banners y api/1.3/store/{storeId}/departments-menu
- [progress] Quita la validación de autorización en el endpoint api/1.3/store/_/department/_

Approved-by: Juan Enrique Escobar Robles
Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Ariel Rene Patiño Arcos

# [2.32] Release 2020-03-24 14:40

## Merged in bugfix/BDC-291-quita-actualización-de-campo-en-order-gropus (pull request #358)(gromo@merqueo.com)

Eliminar la actualización de delivery_window y delivery_amount en order_groups cuando el usuario por el helpcenter desea modificar su pedido

Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Jonathan Guerrero Olivera <jonathang@merqueo.com>
Approved-by: Yesika Vanegas <yvanegas@merqueo.com>

# [2.31.2] Hotfix 2020-03-23 13:20

- Fija configuración para validar si un pedido tiene mas del valor de crédito definido par descuentos

# [2.31.1] Hotfix 2020-03-23 11:17

- Fija configuración para que pedidos entren en validación por montos iguales o superiores al millón de pesos

# [2.31] Release 2020-03-19 08:27

## Merged in bugfix/BDC-198-implementar-cobro-configurado-help-center (pull request #357)(gromo@merqueo.com)

Bugfix/BDC-198 implementar cobro configurado help center

Cuando se use las opciones del helpcenter se desea obtener el domicilio real para la franja que el usurario escogió
en tiempo de ejecución.

Approved-by: Yesika Vanegas <yvanegas@merqueo.com>
Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Jonathan Guerrero Olivera <jonathang@merqueo.com>

# [2.30] Release 2020-03-16 08:44

## Merged in feature/BDC-157-remove-er-help-center (pull request #355)(jarmenta@merqueo.com)

Feature/BDC-157 remove er help center

Approved-by: Camilo Andres Rojas Chaparro
Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Michelle Vengoechea Moncada

## Merged in feature/BDC-93-i18n-colocar-imagen-en-correo-hemos-recibido (pull request #356)(arielp@merqueo.com)

Feature/BDC-93 Adiciona imagen de los mil ganadores al email de "Hemos Recibido tu pedido"

Adiciona imagen de los mil primeros nuevos usuarios que pidan en MX

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>
Approved-by: Jonathan Guerrero Olivera <jonathang@merqueo.com>
Approved-by: Yesika Vanegas <yvanegas@merqueo.com>

## Merge branch 'release/2.29.2' into develop(moviedo@merqueo.com)

# [2.29.2] Bugfix

## Merged in feature/BDC-168-complete-tag-services (pull request #354)

### feature/BDC-168-complete-tag-services

- feat: Added tag to user products.
- feat: Added tag to checkout service.

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Cristian Julian Rincon Perez
Approved-by: Kennit David Ruz Romero

# [2.29.1] Hotfix 2020-03-02 09:43

- [Fix] Previene que se envíe el email de bienvenida cuando se ejecuta OrderEventHandler::onOrderCreated

# [2.29] Release 2020-02-25 12:40

## Merged in feature/SC-54-support-orders-from-nodos-on-ferias (pull request #353)(eheredia@merqueo.com)

Soportar los pedidos de los nodos en la bodega de Ferias.

Approved-by: Ariel Rene Patiño Arcos
Approved-by: Jonathan Sanchez

# [2.28.0] Release 2020-02-05 11:56

## Merged in ETRD-565-tag-trade (pull request #352)

### ETRD-565 tag trade

#### Author: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>

#### Date: Tue Feb 25 15:28:01 2020 +0000

- fix: Changed conditions in with tags.
- feat: Added format last updated.
- feat: Added banner format.
- feat: Added search format.
- fix: If tags is empty set null.
- feat: Added tag to product detail.
- feat: Changed tags to tag in search.
- feat: Changed tags array to object in store product.
- Merge branch 'develop' into ETRD-565-tag-trade
- feat: Added tag to banners.

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Kennit David Ruz Romero
Approved-by: Sebastian Bladimir Iguavita Roa

# [2.28.1] Hotfix 2020-02-13

[fix] Obtener los datos del país del acuerdo a la tienda donde se realizó el pedido

# [2.28.0] Release 2020-02-05 11:56

## Merged in feature/I18NG-change-in-payment-method-copy-datafono-by-terminal (pull request #347)

### Feature/I18NG change in payment method copy datafono by terminal

#### Author: Jonathan Guerrero Olivera <jonathang@merqueo.com>

#### Date: Tue Feb 11 00:05:52 2020 +0000

- Ajustar validaciones del método de pago de datafono
- Agregar formato PSR
- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/I18NG-change-in-payment-method-copy-datafono-by-terminal
- Cambiar el copy de Datáfono por terminal al visualizar las ordenes pendientes
- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/I18NG-change-in-payment-method-copy-datafono-by-terminal
- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/I18NG-change-in-payment-method-copy-datafono-by-terminal
- Solucionar conflictos
- Agregar constantes al validar método de pago
- Formato PSR
- Solucionar conflictos
  Merged in feature/I18NG-change-in-payment-method-copy-datafono-by-terminal (pull request #347)
  Feature/I18NG change in payment method copy datafono by terminal
- Ajustar validaciones del método de pago de datafono
- Agregar formato PSR
- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/I18NG-change-in-payment-method-copy-datafono-by-terminal
- Cambiar el copy de Datáfono por terminal al visualizar las ordenes pendientes
- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/I18NG-change-in-payment-method-copy-datafono-by-terminal
- Merge branch 'develop' of bitbucket.org:merqueo/merqueo-web into feature/I18NG-change-in-payment-method-copy-datafono-by-terminal
- Solucionar conflictos
- Agregar constantes al validar método de pago
- Formato PSR
- Solucionar conflictos

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Ariel Rene Patiño Arcos
Approved-by: Yesika Vanegas

## Merged in bugfix/ECD-904-error-productos-sampling (pull request #351)

### Bugfix/ECD-904 error productos sampling

#### Author: Claudia Viviana Arango Grisales <cvarango@merqueo.com>

#### Date: Mon Feb 10 15:40:26 2020 +0000

- merge
- merge
- exclude products sampling

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Kennit David Ruz Romero
Approved-by: Luz Maria Muñoz Espinosa

## Merged in feature/I18n-First-thousad-orders-free-delivey-mx (pull request #349)

### Primeros 100 pedidos en mexico domicilio gratis por un año y modificación de texto en mexico

#### Author: guillermo elias romo noriega <gromo@merqueo.com>

#### Date: Fri Feb 7 20:54:20 2020 +0000

- send email first thousand orders promotional
- modify email orders
- modify email hemos recibido tu pedido
- sugerencias pr
- Merged develop into feature/I18n-First-thousad-orders-free-delivey-mx
- Merge branch 'develop' into feature/I18n-First-thousad-orders-free-delivey-mx
- Merge branch 'feature/I18n-First-thousad-orders-free-delivey-mx' of bitbucket.org:merqueo/merqueo-web into feature/I18n-First-thousad-orders-free-delivey-mx
- modify template email order received
- modify text domain by country
- modify email href countri

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Yesika Vanegas
Approved-by: Jeremy Jose Reyes Barrios

# [2.27.2] Hotfix 2020-02-10 11:48

fix: Added verification from discount query.

# [2.27.1] Hotfix 2020-02-07 01:45

#Se comento funcion que valida campaña en checkout

# [2.27.0] Hotfix 2020-02-05 11:56

## Merged in feature/I18N-266-change-copy-email-recovery-password-for-mexico (pull request #350)

### Feature/I18N-266 change copy email recovery password for mexico

#### Author: Jonathan Guerrero Olivera <jonathang@merqueo.com>

##### Date: Tue Feb 4 18:27:48 2020 +0000

- Adjust the subject of the email
- change copy footer in email to recovery password
- Change text recovery password

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Yesika Vanegas
Approved-by: guillermo elias romo noriega

# [2.26.4] Hotfix 2020-02-05 11:56

[fix]: Se comentó validación campaign gift

# [2.26.3] Hotfix

[fix]: adjustment in the order status ER

# [2.26.2] Hotfix

[fix]: Removed verification of type normal.

# [2.26.1] Bugfix

## Merged in bugfix/ETRD-562-brand-room (pull request #348)

### Bugfix/ETRD-562 brand room

#### Author: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>

##### Date: Thu Jan 23 21:22:12 2020 +0000

- [feat]: Comment normal type.
- [feat]: Removed comments.
- Merge branch 'develop' into bugfix/ETRD-562-brand-room

Approved-by: Julio Alberto de Hoyos Martínez
Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Sebastian Bladimir Iguavita Roa

# [2.26.1] Hotfix

- se retorna 0 en el peso y el volumen si no se encuentran los atributos

# [2.26.0] Release 2019-12-30 09:23

## Merged in feature/ETRD-534-brand-room (pull request #346)

### Feature/ETRD-534 brand room

#### Author: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>

##### Date: Fri Dec 27 16:19:44 2019 +0000

- [feat]: Added to get store method find by type deparment normal.
- [feat]: Added to get department method find by type deparment normal.
- [feat]: Added to get product method find by type deparment normal.
- [feat]: Added to search method find by type deparment normal.
- [feat]: Added to get departments menu method find by type deparment normal.
- Merge branch 'develop' into feature/ETRD-534-brand-room
- Merge branch 'develop' into feature/ETRD-534-brand-room
- Merge branch 'develop' into feature/ETRD-534-brand-room

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Jonathan Guerrero Olivera
Approved-by: Sebastian Bladimir Iguavita Roa

# [2.25.0] Release 2019-12-18 10:31

## Merged in feature/ER-411-type-the-order (pull request #345)

### finsh]create the type order in service of the detail

#### Author: Camilo Rojas <crojas@merqueo.com>

- [finsh]create the type order in service of the detail

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: FABIAN DANILO CAICEDO MOLINA
Approved-by: Sebastian Bladimir Iguavita Roa

# [2.24.3] Hotfix 2019-12-13 14:34

- [fix]: Se remueve el sitemap de la web vieja y se desactivan los robots

# [2.24.2] Hotfix 2019-12-12 10:18

- [fix]: Problem solved when the findActiveByStoreIds method receives an empty array.

# [2.24.1] Hotfix 2019-12-11 23:35

Fija sleep de 5 segundos en 'TriggerCampaignTrackEventJob' esperando que el dato se encuentre ya replicado en la DB, solucionando error reportado 18 veces en en lo que va del mes de diciembre en curso

# [2.24] Release 2019-12-11 23:10

## Merged in ETRD-528-suggested-product (pull request #337)(jreyes@merqueo.com)

Se requiere en los productos sugeridos tener un nivel más de busqueda que será por productos. Al ser configurado debe ser mostrado en el carrito discriminado por producto.

Approved-by: Marlon Duván Oviedo Rueda
Approved-by: Jonathan Guerrero Olivera
Approved-by: Michelle Vengoechea Moncada

# [2.23.1] Hotfix 2019-12-10 16:24

- Se habilita la opción del help center (Cancelar el pedido)

# [2.23] Release 2019-12-09 11:00

- fix: corrige error de referencia a índice de array en plantilla de email welcome.blade.php(jalvarez@merqueo.com)
- fix: corrige error de referencia a índice de array en class SendWelcomeEmailToUserAction(jalvarez@merqueo.com)

## Merged in feature/I18NG-669-ajuste-url-legales (pull request #341)(jonathang@merqueo.com)

Ajustar las urls para acceder a las políticas de privacidad y términos y condiciones.

Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Yesika Alejandra Vanegas Chacon <yvanegas@merqueo.com>
Approved-by: guillermo elias romo noriega <gromo@merqueo.com>

## Merged in feature/I18NG-fix-send-email-Hemos-recibido-tu-pedido (pull request #342)(ealmeida@merqueo.com)

Se realizan ajustes del código en merqueo-web para el envío del correo: Hemos recibido tu pedido.

Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Luz Maria Muñoz Espinosa <lmunoz@merqueo.com>

## Merged in feature/I18n-add-field-additional-iva-order-product (pull request #340)(gromo@merqueo.com)

Cada vez que se haga un pedido apuntando a la api 1.3 se debe guardar en la tabla order_product un campo adicional llamado additional_iva este se llenara con la misma información existente del campo addtional_iva en la tabla store_products.

Approved-by: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>
Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Luz Maria Muñoz Espinosa <lmunoz@merqueo.com>

## Merged in feature/I18NG-649-filtro-por-pais-en-getcreditavailable (pull request #338)(arielp@merqueo.com)

Se adiciona filtro por country_id al método getCreditAvailable del modelo UserCredit, en caso de no llegar el parámetro city_id se usa por defecto Colombia (1).

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>
Approved-by: Diego Fernando Salazar Martinez <dsalazar@merqueo.com>
Approved-by: Cristian Julian Rincon Perez <crinconp@merqueo.com>

# [2.22] Release 2019-12-04 10:27

## Merged in feature/I18NG-647-change-int-to-decimal-in-checklist-order (pull request #339)(ealmeida@merqueo.com)

Se modifica la vista checklist que se utiliza cuando el usuario comparte un pedido desde las apps, para que otra persona
lo vea. Esta vista no mostraba los decimales para México.

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>
Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Luz Maria Muñoz Espinosa <lmunoz@merqueo.com>

## Merged in feature/I18NG-638-send-email-welcome-in-checkout (pull request #334)(jonathang@merqueo.com)

Al realizar el flujo de una compra en la web o en las aplicaciones, al llegar al checkout sin que el usuario se haya
registrado previamente en Merqueo. al Registrarse y finalizar la compra al usuario le debe llegar al correo electrónico,
el correo de bienvenida.

Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Yesika Alejandra Vanegas Chacon <yvanegas@merqueo.com>
Approved-by: guillermo elias romo noriega <gromo@merqueo.com>

# [2.21] Release 2019-12-02 12:00

## Merged in I18NG-644-agregar-url-terminos-y-politicas-mexico (pull request #336)(jonathang@merqueo.com)

Agregar ruta de políticas y términos para méxico

Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>
Approved-by: Yesika Alejandra Vanegas Chacon <yvanegas@merqueo.com>
Approved-by: guillermo elias romo noriega <gromo@merqueo.com>

# [2.20.0] Release 2019-11-29 09:31

## Merged in bugfix/ECM-699-send-email-er-ex-orders (pull request #335)

### Bugfix/ECM-699 send email er ex orders

#### José Jorge Armenta Martínez <jarmenta@merqueo.com>

- [Feat] se ajusta para qué cuando la orden sea EX o ER se envíe el email al usuario
- [Fix] se elimina validación incorrecta
- [Fix] se ajusta el estado de la orden cuando se valida la lista negra

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Maira Yissela Ruge Cuellar <mruge@merqueo.com>
Approved-by: Cristian camilo Cantor Sua <ccantor@merqueo.com>

# [2.20.0] Release 2019-11-25 16:00

## Merged in feature/ECM-684-minimum-order-amount-express (pull request #332)

### [Feat] Ajuste en endpoint city para devolver el valor minimo de un pedido express

#### Leyniker Raul Rivera <rrivera@merqueo.com>

- [Feat] Ajuste en endpoint city para devolver el valor minimo de un pedido express
- [Feat] Ajuste de retornar el valor minimo del pedido

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Maira Yissela Ruge Cuellar <mruge@merqueo.com>

# [2.19.0] Release 2019-11-20 12:07

## Merged in bugfix/ECM-685-adjust-routing-for-er-orders (pull request #333)

### Bugfix/ECM-685 adjust routing for er orders

Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>
Approved-by: Cristian camilo Cantor Sua <ccantor@merqueo.com>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>

# [2.18.0] Release 2019-11-15 17:14

## Merged in feature/ECM-673-volume---weight-in-banners (pull request #331)

### Feature/ECM-673 volume weight in banners and search

- Se agrega volumen y peso a busqueda de elastic search
- Se elimina dd
- Se corrige validacion

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com>

# [2.17.0] Release 2019-11-15 11:58

## Merged in feature/ELG-Delivery-Time-Minutes (pull request #328)

### Feature/ELG Delivery Time Minutes

- Feature, cuando se realice una compra en la franja horaria Express y Entrega Rápida, se agregue el número de minutos `delivery_time_minutes` preconfigurado en la tabla `delivery_windows` a la columna `first_delivery_date` y `real_delivery_date`.
- Correcciones de PR
- Corrección de PR
- Ajuste de `$order->delivery_date` cuando es una compra tipo express o entrega rápida

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>

# [2.16.1] hotfix 2019-11-08 10:30

- [Fix] Se valida si tiene userDevice en fraudValidation

# [2.16] Release 2019-10-29 19:45

## Merged in feature/CORE-173-create-user-secretos-del-tropico (pull request #327)(eheredia@merqueo.com)

### CORE-173-create-user-secretos-del-tropico

- Se cambia el usuario que se usa para generar las rutas de planeación para dark

Approved-by: Johan Alvarez <jalvarez@merqueo.com>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>

# [2.15] Release 2019-10-29 12:00

## Merged in feature/CORE-134-fraud-limit-the-number-of-referrals (pull request #324)(rrivera@merqueo.com)

- [Feat] Validar el limite de referidos
- [Feat] Validar el limite de referidos
- [Feat] Ajuste de validación de número de referidos
- [Feat] Variable de entorno para validación

Approved-by: Luis Paloma <lpaloma@merqueo.com>
Approved-by: Jose Jaimes <jjaimes@merqueo.com>
Approved-by: Cristian Julian Rincon Perez <crinconp@merqueo.com>
Approved-by: Sergio Rodríguez <serodriguez@merqueo.com>

# [2.14] Release 2019-10-17 16:30

## Merged in feature/ECD-735-update-store-service-on-1.3 (pull request #309)(arielp@merqueo.com)

### Feature/ECD-735 update store service on 1.3

- [Progress] Crea migración para actualizar la columna image_list_app_url_temp con image_list_app_url en la tabla d
  epartments
- [Progress] Actualiza servicio para /api/1.3/store para que retorne image_list_app_url_temp en lugar de image_list_app_url
- [Progress] Actualliza testGetStoreMethod para que pruebe lo del image_list_app_url_temp en api/v13/ApiStoreController@get_store
- [Progress] Mejora la identación y adiciona las tablas a los campos en el query para que pasen los tests
- [Progress] Mejora al testShowAllProductsWithStock
- [Progress] PSR2
- [Progress] Ajuste para prevenir url vacías cuando adicionen un departamento nuevo.
- [Progress] Mejoras en el query que devuelve la imagen del departamento
- [Fix] Cambia las comillas sencillas por dobles para corregir bug

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com>
Approved-by: Ricardo Martinez <rmartinez@merqueo.com>

# [2.13.2] hotfix 2019-10-17 14:52

- [Fix] Se define el menu permitido para los usuarios de planeación.

# [2.13.1] hotfix 2019-10-17 14:50

- [Fix] Se agrega el peso al objeto del producto.

# [2.13] Release 2019-10-17 12:05

## Merged in feature/osrm-integration (pull request #326)(fcaicedo@merqueo.com)

Feature/osrm integration

- Adding all OSRM integration
- Adding OSRM service
- ADding class and Method
- method return change
- srting restriction removed
- now() changed
- adding model for logs
- adding routing service
- defining service
- clase configuration
- class Config
- changing get config in osrm resource
- changes solving comments in PR
- more comments resolved
- Last comment

Approved-by: Sergio Rodríguez <serodriguez@merqueo.com>
Approved-by: Luis Carlos Rosas Orellano <lrosas@merqueo.com>
Approved-by: Johan Fabian Rodriguez Pinzon <jrodriguez@merqueo.com>

# [2.12] Release 2019-10-16 14:00

## Merged in feature/ECM-599-quick-delivery-time-slot-api-1.3 (pull request #323)(jarmenta@merqueo.com)

### Feature/ECM-599 quick delivery time slot api 1.3

- [Feat] se configura los servicios de horarios para qué reciban la franja entrega rápida(ER) y se adapta el checkout para enrutar la orden cuando el usuario elija dicha franja
- [Fix] Se corrige PSR2
- [Fix] PSR2 de acuerdo a los comentarios

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Cristian camilo Cantor Sua <ccantor@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>
Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com>

## Merged in Feature/I18NG-441-service-orders-pending-old-current (pull request #320)(gromo@merqueo.com)

### Internacionalizando ordenes de usuario de acuerdo al pais

- internacionalizando ordenes de usuario de acuerdo al pais
- elimando DB ya que no se esta usnado
- añadiendo psr-2
- capturando la excepcion si gerOrders falla

Approved-by: Jeremy Jose Reyes Barrios <jreyes@merqueo.com>
Approved-by: Jonathan Guerrero Olivera <jonathang@merqueo.com>
Approved-by: Ericka Almeida <ealmeida@merqueo.com>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com>

# [2.11] Release 2019-10-15 14:00

## Merged in feature/CORE-166-add-user-to-ifood (pull request #325)(eheredia@merqueo.com)

Añadir tipo ifood a modelo de usuarios

Approved-by: Johan Alvarez <jalvarez@merqueo.com>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>

# [2.10.1] hotfix 2019-10-15 10:55

- [Fix] Se corrige error de versión de la app dónde no se envía latitud ni longitud,
  con una condición qué valida el objeto address en el servicio de horario.

# [2.10.1] hotfix 2019-10-15 10:25

- [Fix] Se corrige error de versión de la app dónde no se envía latitud ni longitud,
  con una condición qué valida el objeto address

# [2.10.0] Release 2019-10-10 16:28

## Merged in feature/ECM-555-adjust-express-for-darksupermark (pull request #315)

### Feature/ECM-555 adjust express for darksupermark

- [Progress] find a darksupermarket schedule
- [Feat] send order city to service

Approved-by: Jose Cabarcas <jcabarcas@merqueo.com>
Approved-by: Cristian camilo Cantor Sua <ccantor@merqueo.com>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>

# [2.9.2] hotfix 2019-10-09 16:00

- Se extiende a 16 (4:00 pm) la condición de recibir órdenes para el mismo día.

# [2.9.1] hotfix 2019-10-08 13:04

- Se corrige ruta de RejectReasonRepository

# [2.9] Release 2019-10-07 11:15

## Merged in feature/I18NG-merge-squad-003-mexico (pull request #321)(jonathang@merqueo.com)

### Ajustes en la API 1.3 y la web(antigua) para el nodo de mexico

- Ajustar endpoints api1.3 en los parámetros que muestran valores tipo string
- Formatear valores a float del endpoint de department/{department_id}
- Editar método getOrders para permitir decimiales en mis pedidos
- Se módifica el método getDetail para mostrar decimales
- se cambia de entero a float el dato que trae el amount de user_credit
- Formato float a los valores del endpoint de Cŕeditos
- Agregar helper para castear los valores de un producto a float
- Validar el valor de special_price cuando es 0
- Validar el valor de special_price en el endpoint de departamentos
- Cambiar el formato de los valores de store_products en el endpoint de /search
- Agregar index para direccionar a la tienda de mexico
- Capturar respuesta search
- Se modifica modelo user para agregar usuario que sólo ve la tienda de méxico y en store que sólo aparezca la bodega de méxico
- Se modifica modelo user para agregar usuario que sólo ve la tienda de méxico y en store que sólo aparezca la bodega de méxico
- Se modifica modelo user para agregar usuario que sólo ve la tienda de méxico y en store que sólo aparezca la bodega de méxico
- [Feat] Ajuste a redirección de tiendas para usuarios de
- se modifican valores en los endpoint de login-corre-moneda checkout y checkoutcart
- Castear valores a float del endpoint /search
- Cast a float de los valores del endpoint de search
- Se agrega el id de la bodega a warehouse_darksupermarket_id para que no valide esa zona
- Comentar validación del token
- fix: remueve rounds innecesarios y corrige error accediendo a propiedades de objetos
- Agregar formato al endpoint de /discounts
- se devuelven cambios de la rama modify-user-méxico
- cambio en modelo warehouse para usuario de méxico
- se modifica end point delivery time para que acepte decimales
- Se agrega validación en cobertura para usuario méxico
- Se agrega en modelo warehouse el id de la bodeba de méxico como darksupermarket para permitir comprar desde méxico en la web antigua
- Se agrega datos de usuario y bodega para méxico
- Agregar casteo de valores del endpoint de seacrh en el método clearProductResponse
- Corregir casteo del valor de los productos en search}
- Se agrega el floatval a los precios del autocomplete del buscador
- Se modifica la vista autocomplete para que en la web antigua se vean los precios con decimal
- Se modifica la vista autocomplete para que en la web antigua se vean los precios con decimal
- Se agrega 2 decimales a la función number_format para carrito en la antigua web
- Se agrega 2 decimales en number_format para el total del pedido
- descomentar lineas para la validación del token
- Agregar formato float a los valores del método add_order_cart
- Validar el special_price del endpoint de checkout/delivery_time
- Redondear valor del parámetro discount_percentege en el elastic search
- Formatear valor special_price en el método cart
- Validar el special_price en la vista de products-list
- Formatear valores de price, special_price y delivery_discount al obtener productos sugeridos

Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com>
Approved-by: Catalina Cortes Gutierrez <ccortes@merqueo.com>
Approved-by: Jonathan Sanchez <jsanchez@merqueo.com>
Approved-by: Johan Alvarez <jalvarez@merqueo.com>

# [2.8.4] Bugfix 2019-10-07 16:30

## Merged in bugfix/update-reject-reason-help-center (pull request #318)(amartineze@merqueo.com)

### Bugfix/update reject reason help center

- Add function to get reject reason from RejectReasonRepositoryInterface and RjectReasonRepository
- Refactor to CancelOrder and add function from RejectReasonRepository
- Add namespace and false timestamps in RejectReason model
- Indentation of "if"
- Remove type hinting in Repository and Contract
- Add self to call a constant in CancelOrder class
- Creates Migration and seeder to add new values for Help Center in Reject Reason table
- Add a TestSuite for Repositories in phpunit.xml
- Add testing for RejectReason and Factory in Traits
- Remove namespace and type hinting
- remove import with namespace for RejectReason model
- Add test for CancelOrder
- Add PSR2
- Reduce identation in migration
- Remove use of RejectReason in seeder
- Fix spaces and tabulations

Approved-by: Camilo Rojas <crojas@merqueo.com>
Approved-by: Luis Carlos Rosas Orellano <lrosas@merqueo.com>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com>
Approved-by: Jonathan Sanchez <jsanchez@merqueo.com>

# [2.8.3] hotfix 2019-10-07 08:00

- Se modifica función que actualiza los precios de productos de los pedidos del Dark para que elimine siempre los precios especiales

# [2.8.2] hotfix 2019-10-04 16:47

- modifica servicio de mensajeros urbanos

# [2.7.2] hotfix 2019-10-04 09:24

- [revert]: Remove free delivery discount from checkout old web.

# [2.7.1] hotfix 2019-10-03 15:20

- [fix]: city id.
- [docs]: Updated changelog.md
- [fix]: Change city_id by address->city_id.

# [2.7.0] Release 2019-10-03 14:20

## Merged in feature/EA-433-module-global-discount-in-delivery (pull request #313)

### Feature/EA-433 module global discount in delivery

- [feat]: Added use cases and repository discount.
- [feat]: Added repository findByCity and findByStore.
- [feat]: Added use case to view checkout.
- [fix]: Solved problem delivery.
- [feat]: Added global discount delivery to checkout web.
- [feat]: Added global discount delivery to checkout API v13.
- [fix]: Change query getCheckoutDiscounts.
- [feat]: Added verification in select hours.
- [fix]: Change discount query.
- [fix]: Change query find discount by date city subtotal and stores_ids is null.
- [fix]: Change name store_ids.
- [test]: Added unit test.
- [fix]: Added inject discount model.
- [test]: Added unit and integration test.
- [test]: Added Test when subtotal is less return delivery amount.
- [test]: Added when contain city and store.
- [test]: Added unit test.
- [docs]: Added logs info in checkout.
- [feat]: Added get and set on DeliveryDiscountUseCase and rename interface.
- [feat]: Added global delivery discount use case to delivery discount use case.
- [feat]: Added global delivery discount search by city.
- [feat]: Added global delivery discount search by city and store.
- [feat]: Added global delivery discount CheckoutController - Index.
- [feat]: Added global delivery discount CheckoutController - Checkout.
- [feat]: Added global delivery discount UserController - Cart.
- [feat]: Added global delivery discount ApiCheckoutController - get_delivery_dates.
- [feat]: Added global delivery discount ApiCheckoutController - delivery_times.
- [feat]: Added global delivery discount ApiCheckoutController - checkout.
- [revert]: Remove return from ApiController.
- [fix]: Updated integration test.
- [fix]: Updated integration test.
- [fix]: Remove dead code.
- [fix]: Added verify start and expirate discount.
- [feat]: Update parameters in handle.
- [feat]: Update parameters in handle.
- [feat]: Update parameters in handle and remove logs.
- [revert]: Remove setter and getter.
- [revert]: Remove setter and getter and added parameters to handle.
- [fix]: Solved problem from use case service provider.

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Julio Alberto de Hoyos Martínez <jhoyos@merqueo.com>
Approved-by: guillermo elias romo noriega <gromo@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>

# [2.7.0] Release 2019-10-03 11:30

## Merged in feature/CORE-118-adjust-validation-500-pesos (pull request #319)

### [Feat] Ajustar validación de 500 pesos solo para tarjetas de créditos nuevas

- [Feat] Ajustar validación de 500 pesos solo para tarjetas de créditos nuevas
- [Feat]Ajuste validación de numero de pedidos con una tarjeta de crédito

Approved-by: Cristian Julian Rincon Perez <crinconp@merqueo.com> <br />
Approved-by: Miguel Andres Morcillo Mosquera <mmorcillo@merqueo.com> <br />
Approved-by: Johan Alvarez <jalvarez@merqueo.com> <br />

# [2.6.13] hotfix 2019-09-24 15:00

- Cambio del recalculo del inventario en validateStockStatus

# [2.6.12] hotfix 2019-09-19 09:51

- apagar opcion de cancelar pedido en el helpcenter para gestionar las cancelaciones por medio de correo electronico

# [2.6.11] hotfix 2019-09-18 16:40

- [docs]: Updated changelog.
- [revert]: Hide funcionality.

# [2.6.10] hotfix 2019-09-18 11:51

- [revert]: Remove comments and duplicate message.
- [docs]: Updated changelog.
- [feat]: Change validation of the coupon percentage.
- [revert]: Removed method checkByMinAndMaxOrderAmountByType.
- [fix]: Solved problem when compare between min and max.
- [revert]: Remove logs in ApiCheckoutController.
- [feat]: Added verify when type is Credit Amount.
- [docs]: Uncomment date delivery.
- [feat]: Remove when minimum and maximum order amount is enable and subtotal
- [feat]: Added logic to use case.
- [feat]: Created interfaces and use cases.

# [2.6.9] hotfix 2019-09-17 15:25

- Se corrige los pum duplicados en agrupación de productos

# [2.6.8] hotfix 2019-09-18 11:32

- Se modifica el helper para corregir el error de ortográfica

# [2.6.7] hotfix 2019-09-16 14:35

- Se agrega volume al objeto de producto en la api1.3 para validaciones de express.

# [2.6.5] hotfix 2019-09-12 16:25

- Se habilita versión de android para validar los items del menu

# [2.6.4] hotfix 2019-09-12 13:50

- Se corrige endpoint faltante post-cancelacion de pedido PSE, nueva web.

# [2.6.3] hotfix 2019-09-12 13:31

- Se quita la barra de visita nueva web

# [2.6.2] hotfix 2019-09-12 12:05

- Se evita que al crear devoliución del precobro se marque el pedido como no pagado

# [2.6.1] hotfix 2019-09-10 12:43

- Log de creacion de planeación, para seguimiento de altos consumo google api

# [2.6] Release 2019-09-09 10:33

## Merged in bugfix/ECM-551-productos-grouping-api-1.3 (pull request #310)

### Bugfix/ECM-551 products grouping api 1.3

- [Fix] fix menu departments with produtcs grouping
- [Fix] fix api 1.3 menu departments with products grouping
- [Fix] fix menu top departments
- [Fix] fix menu top departments

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>

# [2.5] Release 2019-09-09 10:50

## Merged in feature/SOP-51-bincode-integration (pull request #286)(eheredia@merqueo.com)

### Feature/SOP-51 bincode integration

- [Progress] Se añade clase para validar los bines con bincodes.com

- [Feat] Ajustes finales para la validación de los bines con el servicio de bincodes

- [Fix] Se ajusta segun comentarios en PR

- [Fix] Se activa envío de mensajes de texto

- [Fix] Se quitan los bines autorizados para validar si les permite el ingreso

- [Fix] Se abilita la tarjeta de crédito de Codensa.

  - se descomenta validación en mi cuenta y en el API de mi cuenta

- [Fix] Se corrige respuesta de la función de validación para que retorne el mensaje de error correspondiente cuando la tarjeta no sea valida

- [Fix] Se ajusta respuesta del servicio para mostrar errores

- [Fix] Se ajusta función para retornar false cuando la

- [Fix] Mostrar respuesta del servicio

- [Fix] Validación de respuesta del servicio

- [Fix] Se añade mensaje de error del precobro

Approved-by: Leyniker Raul Rivera <rrivera@merqueo.com>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

# [2.4.3] Hotfix 2019-09-05 15:30

- [Fix] Se ajusta Footer con enlaces a politicas y términos y condiciones
- [Fix] Se corrigen las rutas para terminos y condiciones y politicas

# [2.4.2] Hotfix 2019-09-05 15:07

- Se habilitan modulos del help center

# [2.4.1] Hotfix 2019-09-05 14:22

- Deshabilitando redirección tráfico de mobile

# [2.4] Release 2019-09-02 11:00

## Merged in feature/ECM-484-express-default-schedule (pull request #311)(jarmenta@merqueo.com)

### Feature/ECM-484 express default schedule

- [Progress] validate default express schedule
- [Fix] fix conflict
- [Fix] Fix express warehouse
- [Fix] fix array merge zones express

Approved-by: Jose Cabarcas <jcabarcas@merqueo.com>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Cristian camilo Cantor Sua <ccantor@merqueo.com>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>

## Merged in bugfix/QA-338-alpha-sponsored-product-error (pull request #304)(jreyes@merqueo.com)

bugfix/QA-338-alpha-sponsored-product-error

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Luis Eduardo Gómez Correa <legomez@merqueo.com>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com>
Approved-by: Elvis Heredia <eheredia@merqueo.com>
Approved-by: Ricardo Martinez <rmartinez@merqueo.com>

# [2.3.2] Hotfix 2019-08-30 18:46

- Deshabilitar horario de entrega help center

# [2.3.1] Hotfix 2019-08-30 14:00

- Corrige error `Trying to get property of non-object` en endpoint callback de PayU por no encontrar la orden, ahora se controla el error y se devuelve un 404.

# [2.3] Release 2019-08-29 05:10

## Merged in feature/SOP-85-static-page-editor (pull request #301)(jcaceres@merqueo.com)

### Feature/SOP-85 static page editor

- se integra la carga estática de contenido editado en el dashboard, se crean los modelos CmsDocument, CmsHistory, el repositorio CmsDocumentRepository y su i
  nterface de métodos, el caso de uso StaticUseCase y su interface de métodos y la vista statics blade para renderizar el contenido html guardado en la base de
  datos.
- se corrige el margen de los textos en la etiqueta donde se renderiza el contenido html
- se cambia el endpoint de contenido estático de /static/_ a /content/_
- Se actualiza la página principal para agregar el enlace a la página estática de contenido legal
- se agregan correcciones
- se cambia el título a aviso legal en el footer de página principal

Approved-by: Elvis Heredia <eheredia@merqueo.com>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com>

## Merged in feature/sop-103-update-terms-and-conditions-group-r5-alliance (pull request #306)(lrosas@merqueo.com)

### [#SOP-103] Updated terms and conditions for group r5 alliance

Approved-by: Elvis Heredia <eheredia@merqueo.com>
Approved-by: Cristian Julian Rincon Perez <crinconp@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

# [2.2.277] hotfix 2019-08-29 10:18

- Actualizar hora máxima para pedidos de mismo día

# [2.2.276] hotfix 2019-08-29 08:38

- Actualizar hora máxima para pedidos de mismo día

# [2.2.275] hotfix 2019-08-28 10:48

- corrección exception canAddProducts

# [2.2.273] hotfix 2019-08-26 17:01

- [feat]: Added check first user in web.
- [docs]: CHANGELOG updated.

# [2.2.273] hotfix 2019-08-26 16:47

- deshabilitar agregar productos help center

# [2.2.272] hotfix 2019-08-26 11:45

- [feat]: Added discount by store.

# [2.2.271] hotfix 2019-08-26 10:13

- [docs]: Change false and true.
- [docs]: CHANGELOG updated.

#[2.2.270] hotfix 2019-08-23 16:45

- [Feat] Se añade función a testController para ejecutar la activación de promociones

# [2.2.269] hotfix 2019-08-23 16:40

- [docs]: Logs added when making a purchase.
- [docs]: Log added in step by step.

#[2.2.268] hotfix 2019-08-23 10:30

- [Fix] Ajuste en consulta que envia SMS de entrega de pedido de mas de 2 dias, excluyendo payment method PSE

#[2.2.267] hotfix 2019-08-22 16:048

- [Fix] Se actualiza command store-discounts para validar estado y visualizacion de productos
- [Fix] Se corrigen declaraciones de clausulas where in en consulta SQL >> command store-discounts

#[2.2.266] Release 2019-08-22 10:36
###Merged in feature/ER-242-who-receives-new (pull request #307)
Feature/ER-242 who receives new

- Creamos la migracion, de la tabla order_grups
- Creamos los servicios who receives, y en el checkout se seta la informacion
- arreglos de respuestas
- mejoras menores
- modificamos los test

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com><br>
Approved-by: Jonathan Sanchez <jsanchez@merqueo.com><br>

#[2.2.265] hotfix 2019-08-22 09:32

- [feat] Se añaden ids de nuevas bodegas DarkSupermarket

#[2.2.264] hotfix 2019-08-21 18:10

- [Fix] Actualización de términos y condiciones

# [2.2.263] hotfix 2019-08-21 08:35

- [docs]: Logs added when making a purchase.

#[2.2.262] hotfix 2019-08-20 09:02

- [fix]: Added subtotal cart validation.

# [2.2.261] hotfix 2019-08-16 11:51

- [feat]: Added global discount validation.

# [2.2.260] hotfix 2019-08-15 18:09

- warehouse_id checkout_log

# [2.2.259] hotfix 2019-08-15 17:01

- [feat]: Added delivery discount to checkout and delivery_times methods.
- [feat]: Change verify when user is logged.
- [feat]: Change use case in the api checkout controller.
- [feat]: Added delivery discount use case.
- [feat]: Added delivery discount of the first order.
- [feat]: Added use case and interface when first order is free.

# [2.2.258] hotfix 2019-08-13 19:24

- Ajustes a politicas de privacidad y preguntas frecuentes

# [2.2.257] hotfix 2019-08-08 14:19

- cambios en el autodeploy

# [2.2.256] BugFix 2019-08-03 11:30

- se fuerza actualización a los usuarios con versiones inferiores a la: 2143

# [2.2.255] BugFix 2019-08-01 17:40

## Merged in bugfix/show_items_helpcenter_acdording_status (pull request #303)

Bugfix/ER-245-show_items_helpcenter_acording_status

- se generan las versiones correspondintes de validaicon para cada egent, y se modifica las exepccion para mostrar el menu
- variable disablemenu, habilitada para item sin exepcption
- cambio de validacion del mentodo stristr de true a false, y cambio de ejecucciones segun respuesta
- codigo optimizado, se descarta el else
- se debuguea la validacion, del estado de la orden
- se cambia la sintaxis de validacion del order status
- PSR1

Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com><br>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>

# [2.2.254] HotFix 2019-07-31 18:26

- mejoramos el deployment para soportar staging y production

# [2.2.254] HotFix 2019-07-31 17:26

- apagamos deployment for staging, falta la DB

# [2.2.253] HotFix 2019-07-31 16:48

- auto deployment for staging

# [2.2.252] HotFix 2019-07-31 16:32

- auto deployment for staging

# [2.2.251] HotFix 2019-07-31 16:23

- auto deployment for staging

# [2.2.250] HotFix 2019-07-30 15:12

- Se desactiva cobro de 500 pesos a tarjetas registradas en TPaga y asi evitar que se creen pedidos sin productos

# [2.2.249] Releaase 2019-07-29 17:28

#Merged in feature/ER-223-enable_email (pull request #299)
enable_menu and labels menu -> Help_center

- add chat en vivo
- update copy
- cambiamos el el metodo de validacion, para las opciones del menú que estan desabilitadas por la version de la app
- Modificamos el mentodo de validacion de la respuesta para el campo menu_disable
- Se corrigió la identación del código
- Modificamos los métodos de validación del menú disabled, para poder gestionar las respuestas del servicio según versión de las app
- creamos la configuración de los mentodos de validacion é items desabilitados del menu help_center, y agregamos la validacion del egent
- Cambiamos los labels del menu Help_center
- se agregan las versiones estables de las apps, y se corrigue funcionamiento

Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>

# [2.2.248] Release 2019-07-25 17:48

#Merged in feature/ECM-475-update-products-queries-stage-2 (pull request #300)
Feature/ECM-475 update products queries stage 2

- [Progress] Se adaptan las consultas de la tienda.
- Inicialmente en la tienda web y la tienda de la api 1.3, las consultas relacionadas con productos y pasillos, se incluye la intermedia para la relación n:n
- Merge develop into feature/ECM-475-update-products-queries-stage-2
- Merge develop into feature/ECM-475-update-products-queries-stage-2
- Merge develop into feature/ECM-475-update-products-queries-stage-2
- Merge develop into feature/ECM-475-update-products-queries-stage-2
- [Fix] se relaciona pasillo y departamento en detalle del producto
- [Fix] se corrige realción pasillos y departamentos solo en tienda web
- [Fix] se ajustan las relaciones pasillos, productos y departamentos en la tienda de la api 1.3
- [Fix] error de sintaxis
- [Fix] se devuelven algunas relaciones, porque afectan el tiempo de respuesta en la api.
- [Fix] se soluciona agrupación de productos en home de la web

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Ana Belisa Martinez Escudero <amartineze@merqueo.com><br>
Approved-by: camilo rojas <crojas@merqueo.com><br>
Approved-by: David Sánchez <dsanchez@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>

# [2.2.247] Hotfix 2019-07-17 18:25

- Validar estado de pedido postOrder

# [2.2.246] Hotfix 2019-07-17 17:18

El monto mínimo del pedido para utilizar créditos 30000

# [2.2.245] Hotfix 2019-07-17 13:11

Se deshabilita la opcion del chat en el help center

# [2.2.244] Release 2019-07-17 11:37

#Merged in feature/live-chat (pull request #292)
chat menu help center

- add chat en vivo
- update copy

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Ariel Rene Patiño Arcos <arielp@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com><br>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com><br>

# [2.2.243] Hotfix 2019-07-16 18:44

Se corrige error al guardar el log de solicitud de reembolsos

# [2.2.242] Release 2019-07-16 18:00

precharge-to-all-credit-cards

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>

# [2.2.241] Release 2019-07-16 17:19

##Merged in feature/SOP-91-precharge-to-all-credit-cards (pull request #296)

- [Feat] Se elimina validación para que el cobro de los 500 pesos se cobre a todas las tarjetas de crédito
- [Feat] Se elimina validación para que el cobro de los 500 pesos se cobre a todas las tarjetas de crédito
- [Fix] Se activa envio de mensajes de texto
- [Fix] Se añade envio de SMS cuando el pedido es web y se ha hecho un precobro

Approved-by: Leyniker Raul Rivera <rrivera@merqueo.com><br>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>

# [2.2.240] Release 2019-07-16 09:31

- Se activa colas para el envio de correo electronico en el checkout

# [2.2.239] Hotfix 2019-07-15 16:20

- Se activa agregar productos en el help center

# [2.2.238] Release 2019-07-12 10:40

## Merged in feature/ER-145-Compensacion-automatica (pull request #280)(akurten@merqueo.com)

### Feature/ER-145 Compensacion automatica

- Add column category credit and new category
- base usecase order late delivery
- add verifications
- remove unnecessary data
- refactor code for add scalability
- Change environment variables for keys in database
- refactor usecase
- complete feature
- add test and refactor
- refactor for better test
- add mail
- change order notification
- fix var user
- fix data send notification
- fix text message
- add seeder for new configuration
- fix seed
- fix message
- [Progress] Add tests and usescase
- [Progress] refactor test
- final feature
- Fix type credits
- final feature
- fix name command
- update migration
- fix name
- remove migrations
- fix validations
- fix feature
- Fix, corrección al crear créditos de un usuario en el checkout
  Refactor, Se utilizaron constantes para nombrar los tipos de créditos definidos en DB
- Fix, corrección de consulta de ID en los tipos de créditos
- Fix, corrección al ingresar un campo inexistente en la BD

Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>

# [2.2.237] Hotfix 2019-07-11 17:00

- [Fix] Se elimina del current_stock el reception_stock
- [Feat] Se mueve a un Transaction el metodo postOrder de orderController

# [2.2.236] Release 2019-07-03 08:42

## Merged in bugfix/BUG-329-validation-coupon-percent (pull request #293)(amartineze@merqueo.com)

[bugfix] Bug redeeming a coupon in checkout web

Approved-by: Henry Giovanny Gonzalez Waltero <hgonzalez@merqueo.com>a<br>
Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com>a<br>

## Merged in feature/ECD-543-get-cities (pull request #248)(moviedo@merqueo.com)

[Feat] servicio para obtener ciudades principales y secundarias

Approved-by: Alejandro Rivera <ariveray@merqueo.com>a<br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com>a<br>
Approved-by: Alvaro Javier Martinez Duran <amartinezd@merqueo.com>a<br>
Approved-by: Kennit David Ruz Romero <kruz@merqueo.com>a<br>
Approved-by: Miguel Andres Morcillo Mosquera <mmorcillo@merqueo.com>a<br>

## Merged in feature/ECM-487-sponsored-product-api13 (pull request #291)(jarmenta@merqueo.com)

### Feature/ECM-487 sponsored product api13

- [Feat] validate sponsored product
- [Fix] validate sponsored product
- [Fix] add sponsored product on get_product
- [Fix] fix return value sponsored product

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>
Approved-by: David Sánchez <dsanchez@merqueo.com><br>

# [2.2.235] Hotfix 2019-07-03 08:42

- Se ajusta escritura de log al realizar el reembolso de los 500 pesos.

# [2.2.233] Hotfix 2019-07-01 15:13

- Se quita evento que envia los emails al finalizar la compra en el checkout

# [2.2.232] Hotfix 2019-06-28 17:05

- añade a lista de correos de reporte de últimos errores a:
  - moviedo@merqueo.com
  - arielp@merqueo.com
  - acorrea@merqueo.com
  - mmorcillo@merqueo.com
  - amartinezd@merqueo.com

# [2.2.231] Hotfix 2019-06-28 17:05

- Se quita evento que guarda log de solicitud de reembolso

# [2.2.230] Hotfix 2019-06-28 11:24

- Activación cobro 500 pesos para tarjetas nuevas

# [2.2.229] Hotfix 2019-06-27 15:24

- Actualización datos que se envian a la cola: ExecuteEventsInCreatedOrderJob. Se agrega delay.

# [2.2.228] Feature 2019-06-27 10:00

- Job que ejecuta el evento order created en el checkout

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Miguel Andres Morcillo Mosquera <mmorcillo@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>

# [2.2.227] Hotfix 2019-06-26 10:35

- Desactivación pre cobro de 500 pesos

# [2.2.226] Hotfix 2019-06-26 07:26

- Activación pre cobro de 500 pesos

# [2.2.225] Hotfix 2019-06-25 17:11

- Ajuste a OrderEventHandler en validacion de estado de pedido al crearlo

# [2.2.223] Release 2019-06-21 12:05

## Bugfix/redirect to web v2

- Redirección del tráfico responsive mobile a la web v2
- Ajustede Hotfix a Bugfix

Approved-by: Wbeimar Logatto <wlogatto@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>

## Validaciones de los metodos del controlador para calificaciones

# [2.2.222] Bugfix 2019-06-21 12:05

- Redirección del tráfico responsive mobile a la web v2

# [2.2.221] Hotfix 2019-06-21 12:05

- Ajuste a clase RejectReason y desactivar precobro PayU para TC

# [2.2.221] Release 2019-06-21 11:38

## Bugfix/BUG-478-error-calificaciones

- [Fix] Validaciones de los metodos del controlador para calificaciones

# [2.2.220] Release 2019-06-14 18:45

## Feature/SOP-69 order log on returns

- Se creo un log de solicitud de devoluciones y se deshabilito el envio de sms informando del reembolso
- Se creo un log de solicitud de devoluciones y se deshabilito el envio de sms informando del reembolso
- [Feat] Se activa el cobro de los 500 pesos
- [Fix] Se desactiva cobro de 500 pesos al crear tarjeta

Approved-by: Elvis Heredia <eheredia@merqueo.com><br>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com><br>

## Feature/SOP-48 charge credit card from my account

- [Feat] Implementación de precobro en registro de tarjeta de crédito nueva desde mi cuenta.
- [Feat]Ajuste de precobro desde mi cuenta
- [Feat] Ajuste de funciones de precobro desde mi cuenta
- [Feat]Ajuste de funcion para precobro desde registro tarjeta nueva desde mi cuenta
- [Feat]Implentación de precobro desde mi cuenta
- [Feat] Ajuste de mensaje de creación de la tarjeta
- [Feat] Cambio de status code en método de registrar tarjeta
- [Feat]Ajuste de formulario para deshabilitar botón despúes de submit

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Elvis Heredia <eheredia@merqueo.com><br>
Approved-by: Luis Trujillo <ltrujillo@merqueo.com><br>

## Bugfix/SOP-2 review order with possible fraud

- [Feat] Correción de validación de posible fraude
- [Fix] Ajuste de validación de posible fraude
- [Fix] Se ajusto validación de posible fraude con tarjetas de créditos reportadas y tarjetas nuevas

Approved-by: Elvis Heredia <eheredia@merqueo.com><br>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com><br>

## Feature/ECR-147 new flow help center email

- [fix] Capturar el error en caso tal que sea un token errado
- [fix] Capturar el Exception global
- [Progress] Cambio de logo y estilos en el navbar
- [Progress] Agregar libreria basica de lottie
- [Progress] Estructura semantica inicial de la vista de home de correo
- [Progress] Se agregaron las animaciones para lottie
- [Progress] Estilos de la vista de home de la calificación por correo
- [Progress] Modificaciones en estilos del home de calificaciones
- [Fix] Activar eventos de click de los botones
- [Progress] Footer compartido
- [Progress] Estilos generales para las vistas de Gracias por calificar y Home
- [Progress] Cambiar etiqueta button por a
- [Progress] Vista de error cuando el usuario ya calificó
- [Progress] Vista de error cuando el usuario califica negativamente
- [Progress] Limpiando comentarios
- [Progress] Cambio de tipificaciones en la selección de problemas
- [Progress] Redirección del link de ir a la tienda
- [Progress] Remover logica de zendesk
- [Progress] Estilos para Loading
- [Progress] Cambiar animación de loading
- [Fix] Animación de loading con fondo
- [Fix] Corrección de ortografia
- [Progress] Cambio de copys en email de confirmación de pedido entregado
- [Progress] Flag para validar calificación del pedido en SI
- [Progress] Cambio de estilos en email de entregado
- [Fix] Corrección de estilos de email
- [Fix] Corrección de estilo de imagen de pago
- [Fix] Agregar flag de done para calificación exitosa
- [Progress] Refactor de la animación de lottie
- [Progress] Validar si el usuario ya calificó el pedido
- [Fix] Link del boton NO en email de pedido entregado
- [Fix] Sobreescribir Color de los textos en el mail
- [Fix] Responsive
- [Fix] Reset email
- [Fix] Reset email error
- [Fix] Estilos con nuevo formato
- [Fix] Estilos de botones en iphone
- [Fix] Link de boton SI en email de entregado
- [Fix] Copy de error cuando el usuario intenta enviar el help center de error sin datos
- [Fix] Estilos del titulo
- [Fix] Redirección cuando el usuario ya calificó en la opción negativa
- [Fix] Copys de problema a inconvenientes
- [Fix] Copys de flujo
- [Fix] Copys de inconvenientes con tu pedido
- [Fix] Typo en funcion de angularjs
- [Progress] Cambio en el copy de la notificación push de orden entregada
- [Progress] Registrar eventos de analytics

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>

## Feature/change logic reject reason

- Add reject_reason_id in CheckoutController when a transaction is declined by payu
- Add reject_reason by id in ApiCheckoutController
- Change reject_reason string to reject_reason_id in orders from Help Center
- Add \ to import model rejectreason
- Change implementation model RejectReason
- Fix query to reject_reason_id

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com><br>

## share list Forever

- remove validation
- fix hash_code

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: José Jorge Armenta Martínez <jarmenta@merqueo.com><br>
Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>

# [2.2.219] Hotfix 2019-06-20 17:05

- Validacion de estados en eventos de pedidos

# [2.2.218] Hotfix 2019-06-18 18:05

- Parche a error googleApiDirections, si falla, intentar sin optimize

# [2.2.217] Hotfix 2019-06-17 16:15

- Se lanza la barra al 100% para la nueva web

# [2.2.216] Hotfix 2019-06-17 11:30

- Adiciona ordenamiento por banner_departments.position cuando llega el parámetro \$department en el servicio get_banners

# [2.2.215] Hotfix 2019-06-12 14:37

- Se actualizan los términos y condiciones así como las políticas de privacidad, para añadir las comunicaciones via watsapp

# [2.2.214] Release 2019-06-11 17:50

## Feature/ECM-474 express fixes help center

- [Fix] Se ajusta el servicio de Help Center
  - Para una orden express, no se permite agregar productos, cambiar el método de pago o redimir cupón desde el help center.
- [Fix] Se aplica formato al texto de salida cuando es una orden express

Approved-by: Jose Cabarcas <jcabarcas@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Angel Kurten Gutierrez <akurten@merqueo.com><br>
Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com><br>

# [2.2.213] Hotfix 2019-06-11 16:55

- Se actualizan texto en los términos y condiciones

# [2.2.212] Hotfix 2019-06-11 16:15

- Se actualizan los términos y condiciones

# [2.2.211] Hotfix 2019-06-11 14:31

- Ya hay despliegue automatico de los workers

# [2.2.210] Release 2019-06-10 11:35

- Me faltaron los cambios en CHANGELOG de la version 2.2.209

# [2.2.209] Release 2019-06-10 11:30

- Se instala new relic en los servidores

# [2.2.208] Release 2019-06-07 15:00

- Se desactiva exception al enviar correo desde SendOrderReceivedEmailAction

# [2.2.207] Release 2019-06-06 14:26

## [Fix] solo se valida la direccion para IOS, ciertas versiones aun utilizan el mapa

# [2.2.206] Hotfix 2019-06-06 17:30

- Agrega monto a cobrado en pedido transaccion con PSE

# [2.2.205] Hotfix 2019-06-05 06:30

- deshabilita opción de añadir productos desde Help Center

# [2.2.204] Hotfix 2019-06-04 19:44

## auto deploy

- se hacen cambios para permitir el depliegue automático de los workers en un futuro próximo

# [2.2.203] Release 2019-06-04 18:15

## Bugfix/SOP-4 validate minimum discount with credit

- [Fix] Ajustes al modificar el pedido desde el help center.
  - Se actualiza el estado del producto según el stock despues de actualizar las cantidades.
  - Se valida que un pedido con descuento por referido cumpla con el monto mínimo.
- [Fix] Agregar verificación de los totales del cupon al modificar orden desde el help center
  - Se modifica el modelo de Coupon separando la validación del cumplimiento de los montos de un cupon
  - Se modifica el modelo de Order separando la consulta del cupon asociado a una orden
  - Se modifica el modelo de OrderValidator agregando la consulta de cupones y la validación de los montos del cupon
- [Fix] Agregar formato a strings devueltos por funcion validateTotalAmounts()
  - Correccion a partir de revision de pull request
  - Se modifica el modelo de Coupon agregando formato a los textos devueltos por la funcion validateTotalAmounts()
- [Fix] Correccion de errores en tipado de funciones
  - se corrige el tipado en los parametros de la funcion validateTotalAmounts() en el modelo de Coupon
  - se modifica el llamado del modelo de Coupon en la funcion orderHasCoupons() en el modelo de Order
- [Fix] Se corrigen errores en validaciones y precio en 0 del help center
  - Se agrega condicion en una validacion acerca de la cantidad de cupones en el modelo app/models/orders/OrderValidator.php
  - Se corrige el valor del parametro quantity_special_price para evitar el error del precio en 0 en el archivo app/classes/AWSElasticsearch.php

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Elvis Heredia <eheredia@merqueo.com><br>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com><br>

# [2.2.202] Hotfix 2019-05-31 10:59

## Deshabilitación de enlaces de antiguo dash

# [2.2.201] Hotfix 2019-05-31 10:59

## intento de correccion de la tuberia de despliegue automatico

# [2.2.200] Release - 2019-05-30 21:21

## [Feat] Validacion de zona por segunda vez en el checkout

- [Progress] Validacion de zona por segunda vez
- [Fix] Se valida si no se encuntra la zona
- [Fix] validando zona

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Alvaro Javier Martinez Duran <amartinezd@merqueo.com><br>
Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>

# [2.2.199] Hotfix - 2019-05-30 20:51

## Adicionar bin a listado de excepciones

- Se añade el bin 513689 al listado de excepciones de tarjetas

# [2.2.198] Hotfix - 2019-05-30 16:55

## Adición de comentarios en terminos y condiciones

- se agregó comentarios de terminos y condiciones

# [2.2.197] Hotfix - 2019-05-29 15:18

## correccion en deploy automatico

- se actualiza la version del boto3 para corregir un problema al invocar codedeploy

# [2.2.196] Hotfix - 2019-05-29 15:18

## cambios en robots.txt

# [2.2.195] Release - 2019-05-28 18:20

## SOP-62 add new user type to domicilios

- [Progress] Se añade migración y valor a la constante de tipos de usuario
- [Feat] Se añade nuevo tipo de usuario para el convenio con domicilios
- [Fix] Se corrige el nombre de las tablas en las migraciones

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Jonathan Sanchez <jsanchez@merqueo.com><br>
Approved-by: Isabel Palmera Ortega <ipalmera@merqueo.com><br>

# [2.2.194] Hotfix - 2019-05-28 18:03

## fix deploy automático

# [2.2.193] Hotfix - 2019-05-28 17:47

## fix deploy automático

# [2.2.191] Release - 2019-05-28 17:00

## deployment automático terminado

Approved-by: Juan Enrique Escobar Robles <juanescobar@merqueo.com><br>
Approved-by: Alejandro Rivera <ariveray@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>

# [2.2.191] Hotfix - 2019-05-28 14:27

- [Fix] Se ajusta código de referencia enviado a payu para que la transacción de cobro del pedido se pueda realizar al momento de alistar el pedido

# [2.2.192] Hotfix - 2019-05-28 15:00

- Deshabilitando el cobro de \$500 pesos a la tarjeta de crédito.

# [2.2.190] Release - 2019-05-27 23:00

## [Feature] Se agrego validación para redirección en la web actual a la web nueva,

Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>

- [fix] soluciona error con conflicto en dependencias de composer

# [2.2.189] Release - 2019-05-27 23:00

- Deploy with aws CodeDeploy

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Marlon Duván Oviedo Rueda <moviedo@merqueo.com><br>

# [2.2.188] Release - 2019-05-24 22:00

- restablece los cambios de feature share-check-list

# [2.2.188] Hotfix - 2019-05-23 18:53

- Se valida en la función de confirmación de payu que no cambie la fecha de pago cuando es precobro

# [2.2.187] Hotfix - 2019-05-23 18:29

- Corrigiendo error de namespace en OrderPaymentLogEntity Queue

# [2.2.186] Hotfix - 2019-05-23 18:15

- Corrigiendo error de namespace en OrderPaymentLogEntity

# [2.2.185] Release - 2019-05-23 15:00

## Feature/modify composer dependencies build

- añade la librería anouar/fpdf a packages para instalación local y evitar 404 al instalarla de github
- configura dependencias de composer y añade composer.lock a tracking de git
- [Progress] Implementa InternalStatusController
- [Progress] Adiciona el estado de redis y corrige un typ en database.php REDIS_POST por REDIS_PORT

Approved-by: Jonathan Sanchez <jsanchez@merqueo.com><br>
Approved-by: Juan Enrique Escobar Robles <juanescobar@merqueo.com><br>

- [Fix] Ajustes a logs de precobro a tarjeta nueva
- [feat] fijando número de versiones mínimas para Android y iOS a 2.1.38 y 2.1.30 respectivamente

## Feature/ELG-1275 script random usuarios nueva web

- [Feature] Base script random
- [Feature] Implementación de random para mostrar la barra al 50% de usuarios que entren a la web
  - Se creo una clase para el manejo de la función de asignar usuarios a mostrar la barra
  - Se hicieron unit test con mochaJS
  - Se agregó el package.json

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Alejandro Rivera <ariveray@merqueo.com><br>

## [Fix] Validación de código de referido desde el HelpCenter

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Alejandro Rivera <ariveray@merqueo.com><br>

## SOP-52 Añadir campos a la tabla de pagos

- [Feat]. Se añaden campos a la tabla order_payments
  - Se ajusta función para cobro de tarjeta para que guarde el monto de la compra y si el precobro
  - Se ajusta para que el pedido no quede pagado al hacer un precobro
- [Fix] Se añade validación para que no tome los pagos de precobro
- [Fix] Se actualiza la fecha de pago a null cuando es precobro
  - Se cambia mensaje log precobro para que se guarde solo cuando se cobran los 500 pesos
- [Fix] Se ajusta migración y guardado de orden cuando hace devolución en precobro
- [Fix] Se añade función de validación de estado del pago de un pedido
  - Se modifica el servicio de cambio de estado del app de picking
  - Se añade validación cuando la tarjeta no existe

Approved-by: Luis Trujillo <ltrujillo@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

## Merged in bugfix/fix-shift-planning-select (pull request #265)

- [Fix] Se corrige select

Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>

## [Fix] Se corrige select

- Se corrige select con información errónea

# [2.2.184] HotFix - 2019-05-17 19:04

- [Fix] Se ajusta guardado del transacción ID en los LOG.

# [2.2.183] hotfix - 2019-05-17 16:00

## Added

- Ajuste para guardar en orderLog la transacción de precobro

# [2.2.182] Release - 2019-05-17 16:00

## Added

- habilita el cobro/reembolso de los en checkout

### Feature/SOP-38 save all logs payu

- [Feat] implementacion de la logica de almacenar el log en la tabla order_payment_log_entity
- [Feat] se implementa la logica para almecenar todos los logs en la tabla order_payment_log aplicando una cola para evitar el beginTransaction
- [Fix] Se añade log laravel
- [Fix] Se añaden log adicionales
- [Fix] Se añaden logs laravel
- [Fix] Mejora de log
- [Fix] Se añaden mas log
- [Fix] Se ajusta llamado al Job
- [Fix] Ajuste a guardado del JOB
- [Fix] Se añade mas datos al mensaje de error
- [Fix] se ajusta variable que llega al Job para extraer los datos de forma correcta
- [Fix] Se añade OrderId al guardado del log del cobro a la tarjeta
- [Fix] Se quitan mensajes en laravel LOG

Approved-by: Sebastian Alvarez Mejia <salvarez@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Elvis Heredia <eheredia@merqueo.com><br>

# [2.2.181] Release - 2019-05-17 14:40

- Escribe log de usuarios administradores que siguen usando este proyecto.

# [2.2.180] Hotfix - 2019-05-13 14:36

- Se deshabilitó la redirección al nuevo sitio web desde mobile

# [2.2.179] Hotfix - 2019-05-15 20:32

- [Fix] Se ajusta validacion por fraude para que evite los pedidos de darksupermarket

# [2.2.178] Release - 2019-05-15 17:15

## Feature/SOP-3 charge new credit card

- [Feat] Implementación de precobro para validar tarjeta de crédito
- [Feat] Ajuste de Calse payu y archivo conf para realizar precobro
- [Feat] Se ajusto la clase Payu con camelcase y en el model order se ajusto el metodo de cobro
- [Feat]Ajustes de variables a camelcase en jobs y en el modelo para el precobro
- [Feat] Se cambio usuario de ejecución de jobs para reembolso de precobro
- [Feat]Fijando variable de configuracion para habilitar o deshabilitar precobro de tarjeta de crédito
- [Feat]Ajuste de validación para tarjeta nueva en precobro

Approved-by: Luis Trujillo <ltrujillo@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Elvis Heredia <eheredia@merqueo.com><br>

# [2.2.177] Release - 2019-05-15 15:40

## Feature/SOP-34 add uber eats source

- Añadir UberEats o origen de compra
- Añadidos los orígenes en admin/orders-storage - sección Filtros
- [Feat] Se valida que solo actualice los precios de los productos cuando el usuario sea tipo Domicilios.com
- [Fix] Se corrige la actualización del precio originar cuando los pedidos son del domicilios.com

Approved-by: Luis Trujillo <ltrujillo@merqueo.com><br>
Approved-by: Elvis Heredia <eheredia@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>

- [Fix] se agregan pasillos faltantes

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Alejandro Rivera <ariveray@merqueo.com><br>

# [2.2.176] Hotfix - 2019-05-15 14:02

- Ajuste en validacion de zona por defecto y con status active en warehouse en la web actual que afecta la migración a la nueva web.

# [2.2.175] Hotfix - 2019-05-15 13:25

- Validación de dirección en session para migracion de datos entre la web actual y la nueva.

# [2.2.174] Hotfix - 2019-05-13 15:40

- Se ajustò la validacion del current zone, en caso de que la zona por defecto esté inactiva Ajuste para la migracion de la nueva web.

# [2.2.173] Hotfix - 2019-05-13 14:36

- Se habilitó la redirección al nuevo sitio web desde mobile

# [2.2.172] Release - 2019-05-09 17:22

## Validaciones modulo antifraude

- [Feat] Se añaden nuevas validaciones al modulo antifraude
- [Fix] Se añade validación para evitar error cuando el pedido es web y busca el device id

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Sebastian Bladimir Iguavita Roa <siguavita@merqueo.com>

# [2.2.171] Hotfix - 2019-05-08 21:36

- Se validan dígitos de mes de tarjeta de crédito nueva en checkout

# [2.2.170] Release - 2019-05-06 09:00

## Added

### Feature/ECM-392 validate coverage dark supermarket

- [Progress] Se valida las 4 condiciones express
  1. Zona de cobertura express
  2. Productos en bodega express
  3. Volumetría o capacidad máxima (50kg o 50x50x50 cms)
  4. Franja horaria express
- [Fix] Se resuelve conflicto al hacer pull de develop
- [Testing] probando Api 1.3 delivery_dates
- [Testing] devolviendo cambios de prueba
- [Progress] Se agrega transportador, conductor y vehículo genérico para MU
  - A cada orden express se le asigna una ruta y un transporte genérico de Mensajeros Urbanos.
- [Progress] Se agrega envío de email a bodega express
- [Fix] Se ajusta el texto de entrega en el email para ordenes express
- [Fix] Se remueve envío de email a bodega express
- [Feat] Se create servicio en api de producto que crea una orden en mensajeros urbanos.
- [Fix] se ajusta variables de entorno
- [Fix] configurando variables de entorno
- [Fix] Se fija el parámetro planogram en "Piso 1"
- [Fix] Se ajusta la hora de cierre de la franja horaria
  - La hora final de la franja horaria express, es la máxima en el qué el usuario puede realizar una compra.
  - Para volumetría se adiciona la cantidad del precio especial qué no se había tenido en cuenta.
- [Fix] se fija la tienda 2 para mensajeros urbanos
- [Fix] se valida qué los productos estén activos en darksupermarket
- [Fix] Control de excepciones
  - Sí alguno de los parámetros de la función de validación express es null retorna fals0e
- [Fix] Se asigna transportador mensajero urbano y se apunta a producción de MU

Approved-by: Alejandro Rivera <ariveray@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: John Guerrero <jguerrero@merqueo.com><br>
Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com>

### Feature/share checklist

- Initial view checklist
- Change active color card
- Remove action for change color button
- short url
- add dependency and fix view
- Update Changelog
- add hash code order status
- fix encode order-id
- fix dependency
- add hash code service api v1.3
- fix property
- Filter for not show screen "Seguir en sitio web"
- Add composer.lock
- Fix error css
- Revert "Fix error css"
  This reverts commit e15822509b02c58e350433a18b182d86b001fa87.
- fix css iphone

Approved-by: Michelle Vengoechea Moncada <mvengoechea@merqueo.com><br>
Approved-by: Alejandro Rivera <ariveray@merqueo.com><br>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

# [2.2.169] Release - 2019-05-03 00:50

## Fixed

### Merged in bugfix/order-stratum-id (pull request #243)

Bugfix/order stratum id

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: John Guerrero <jguerrero@merqueo.com>

- [Fix] Deshacer cambio de URL referido con Firebase
- [Fix] Comentar código que genera URL short dinámica con Firebase para referido de usuario nuevo

### Bugfix/move validate committed stock function to queue

- [Fix] Se crea job para generar cola he inactivar productos comprometidos en checkout
- [Fix] Se agrega use en apicheckoutcontroller

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com><br>
Approved-by: Luis Trujillo <ltrujillo@merqueo.com>

- [Fix] Ajuste a id de estrato económico en checkout de pedidos en web

# [2.2.168] Hotfix - 2019-05-02 16:46

- Se ajusta la transferencia del zoneId entre la web nueva y la actual
- Se soluciona problema de 404 cuando la zoneId no existe al dar logout

# [2.2.167] HotFix - 2019-05-02 12:00

- Se ajusta fecha utilizada en servicio de pagos recibidos por payment_date

# [2.2.166] Hotfix - 2019-04-29 19:36

- Comentar codigo de estrato economico en checkout

# [2.2.162] Hotfix - 2019-04-29 19:36

- Quitar funciones para actualizar el stock picked de los productos

# [2.2.162] Release - 2019-04-25 10:20

- Corrección banners

Approved-by: Alvaro Javier Martinez Duran <amartinezd@merqueo.com>
Approved-by: Alejandro Rivera <ariveray@merqueo.com>
Approved-by: Ricardo Martinez <rmartinez@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

# [2.2.161] Release - 2019-04-24 16:30

- Los usuarios en el horario de las 11 pm, pueden ordenar en las franjas "MD" (mismo día), del día siguiente.

Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com>
Approved-by: Alejandro Rivera <ariveray@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

# [2.2.160] Release - 2019-04-24 14:30

[Fix] Se ajusta pdf de factura segun terminos legales referidos por contabilidad

Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>
Approved-by: Luis Trujillo <ltrujillo@merqueo.com>

# [2.2.159] HotFix - 2019-04-22 23:00

## Fixed

- sube número de versión de aplicación de transportes a 103

# [2.2.158] HotFix - 2019-04-17 17:46

## Fixed

- Se modifican montos de sistema de referidos

# [2.2.157] HotFix - 2019-04-17 10:35

## Fixed

- agrega dos bodegas más a las que están quemadas en la clase QuerysForServicesAndTemplatesSAPAction

# [2.2.156] Hotfix - 2019-04-16 16:02

- Se corrige sólo el logo de sodexo en footer.

# [2.2.155] Release - 2019-04-16 10:00

## Added

### Feature/ECM-416 add logo sodexo footer

- Se agrega logo de sodexo en métodos de pagos footer
- Ajuste del logo sodexo en el footer
  Se cambia la imagen de sodexo con dimensiones ajustadas.
- Ajuste de estilos
  Se ajusta el width del logo de métodos de pago en el footer.

Approved-by: Jhon Sánchez <jhonsanchez@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

### Feature/migration economic stratum

- Se añade estrato socioeconomico en tabla order_groups
  - Se crea modelo para administrar tabla de estrato socioeconomico
  - Se crea tabla para estrato socioeconomico
  - Se crea campo stratum_economic_id en tabla order_groups
  - Se crea seeder para insertar datos en la tabla economic_stratum
  - Se modifica texto en la vista de términos y condiciones
- Se asigno estrato económico a la api V13

Approved-by: Leyniker Raul Rivera <rrivera@merqueo.com>
Approved-by: Elvis Heredia <eheredia@merqueo.com>
Approved-by: Sebastian Alvarez Mejia <salvarez@merqueo.com>
Approved-by: Johan Álvarez Colmenares <jalvarez@merqueo.com>

# [2.2.154] Hotfix - 2019-04-13 13:17

- Se colocó condicional en la barra que lleva al nuevo sitio, se estaba mostrando a todo publico.

# [2.2.153] Hotfix - 2019-04-12 14:43

- Se deshabilitó la redirección a la nueva web desde dispositivo móvil

# [2.2.152] Hotfix - 2019-04-11 18:18

- Se remueve la opción de agregar productos desde el help center.

# [2.2.151] Hotfix - 2019-04-11 17:18

- Nuevo flujo de direcciones
- Endpoints para la migración de sesión en la nueva web.

# [2.2.150] Hotfix - 2019-04-11 14:10

- Se ajustan servicios de nota crédito segun requerimiento de contabilidad.

### Feature/Feature/share checklist - 2019-05-03 11:00

- Se crea la funcionalidad de compartir pedido

# [2.2.149] Hotfix - 2019-04-06 14:10

- Añade columna `discount_amount` a query de orden para callback de PSE.

# [2.2.148] Hotfix - 2019-04-06 10:27

- Ajuste a callback de PSE para validar firma incluyendo descuento en pedido

# [2.2.147] Hotfix - 2019-04-05 18:10

- actualiza el valor total del pedido a cobrar por PSE con los descuentos

# [2.2.146] Hotfix - 2019-04-03 15:20

- Se ajusta query de grila de listado de pickers activos en el mòdulo del dash.

# [2.2.145] Hotfix - 2019-04-03 11:49

- Se agregan correos para el enví´o de reportes de conteos.
- Se agrega nuevo rol que permite cerrar conteos.

# [2.2.144] Hotfix - 2019-04-02 17:25

- Se agregan bodegas calle 13 y 106 a querys de SAP y se activan servicios de entrada y salida de inventarios para SAP
- Se agrega limite de tiempo para consulta de facturas de venta

# [2.2.143] Hotfix - 2019-04-02 14:57

- Se ajustan plantillas y servicios de sap de facturas de compra

# [2.2.142] Hotfix - 2019-04-01 14:32

- Al obtener el listado de bines se tienen en cuenta los bines debito de bancolombia.

# [2.2.141] Hotfix - 2019-04-01 12:06

- En la campaña de Bancolombia al otorgar el beneficio se envia un mensaje de texto.
- En IOs no se muestra en el checkout la campaña de Bancolombia.
- Al subir cualquier ajuste se reinician los "deamons" de las colas

# [2.2.140] Release - 2019-04-01 10:11

- Nueva Campaña de Bancolombia

# [2.2.139] Hotfix - 2019-03-29 11:21

- Ajuste en los valores que se almacenan en la tabla order_product_groups al crear un pedido.

# [2.2.138] Hotfix - 2019-03-27 9:49

- Se corrige atributo en entidad warehouse shift por shift_planning

# [2.2.138] Hotfix - 2019-03-28 10:41

- Ajustes en la configuración de supervisor
- Se ejecutan los workers con el flag --daemon

# [2.2.137] Hotfix - 2019-03-26 19:46

- Se corrige validación en el hotfix 2.2.135 Solo se bloquea el cambio de estado de producto si es para un estado igual a pendiente o faltante

# [2.2.136] Release - 2019-03-26 19:15

## Fixed

- Ajuste al reprogramar pedido con producto de muestra desde el dashboard
- Ajuste al reprogramar pedido con producto de muestra
- Agregar listado de productos disponibles y no disponibles al cuerpo del correo para poder reenviar el correo después de que haya sido entregado sin que genere error
- En Leanplum, la actualización del usuario se realiza después de 1 minutos.
- Ajustes en query 'bi_retention' del comando 'scripts'
- El valor de los créditos que se actualiza en Leanplum excluye los item vencidos

### Se ajustan términos y condiciones de campaña VISA

- Se ajustan términos y condiciones de campaña VISA
- Se agregan políticas a los T&C según requerimiento

### [Fix] Ajustes en query 'bi_retention' del comando 'scripts'

- Se refactorizó la forma en que se ejecutaban las queries
- Se modificó la tarea 'bi_retention'
- Se cambió el email al que le llegan los reportes de ejecución de queries

### bugfix/EQ-29-fix-store-product-observer

- Ajustes a StoreProductObserver cuando se cambia el precio de un producto simple
  - Cuando se actualiza el precio de un producto simple, se ajusta también el precio de los combos a los que pertenece. Si un producto cambia de precio de 1000 a 5000, los precios de los combos a los que pertenece también subirán 4000
  - Se valida que cuando se cambia el precio de un producto simple, los descuentos en combos no sean mayores al nuevo precio

### Bugfix/BUG-82 wrong coupon application

- Corregidos bugs en el manejo de cupones
  - Corregido bug en cupón de primera compra, si el usuario realiza su primera orden sin utilizar el cupón, este no podrá ser utilizado después sin importar el estado de la orden
  - Corregido bug en cupón normal, si el usuario redimía el cupón en una orden y luego esta se cancelaba, el cupón no podia ser utilizado otra vez, ahora al cancelar la orden se activa de nuevo el cupón redimido
- Ajustado manejo de cupones cuando se cancela orden desde el dashboard
- Ajustes en manejo de cupón redimido cuando se cancela orden
  - Cuando se cancela orden desde el dashboard, los cupones redimidos pueden volver a ser utilizados siempre que la razón para cancelar sea 'Cancelar' y no 'Pausar', 'Reprogramar', 'Rescatar' entre otras
- Ajustado manejo de cupón al cancelar orden
  - Si la orden tiene una orden padre, se vuelven a activar los cupones utilizados en la orden padre al cancelar la orden hija
- Corregido bug estético en distribución de descuento en combo
- Corregido bug estético en distribución de descuento en combo

### Bugfix/BUG-90-number-products-in-complaint-order

- Validar que no se pueda agregar una cantidad de un prodcuto a un pedido reclamo que supere la cantidad de producto del pedido original
- corregir label de precio reclamo en la tabla de agregar productos
- cargar solo los productos del pedido original en la busqueda del pedido reclamo

## Added

- Agregar nueva consulta para insertar en la tabla producto_detalle en el script de BI

### Feature/ELG-925-generate-sap-templates-in-queue

- Se agrega funcionalidad de generar procesos en cola
  - Se agrega funcionalidad para generar procesos en cola con redis y enviar el resultado por correo.
  - Se ajusta pdf de factura manual agregándole textos de normas de ley.
  - Se agrega query de traslados de bodega en servicio de entrada y salida de inventario para SAP
  - Se desacoplan los query de los servicios de SAP, con se generan las plantillas y los servicios desde el mismo lugar.
- Se desactiva el envío de correo en pruebas
- Se ajustan parámetros para generar plantillas SAP
- Se ajustan queries de SAP tal cual están en producción
- Se configuran 3 procesos en cola para generar plantillas de SAP alto, medio y bajo
- Se activan servicios de entrada y salida de inventarios para pruebas
- Se ajustan nombres para las colas al generar plantillas para SAP
- Añade funcionalidad faltante en nota crédito de venta para eliminar producto
- Se agrega validación en servicio de pagos recibidos total > 0
- Se desactivan servicios entrada y salida de inventarios para SAP

### Feature/EPK-266-servicios-picking-sin-planeación

- Se crea campo en tabla warehouse
  - Se crea campo en tabla warehouse para saber en que franja se hará picking sin planeación
- Se añade validación de franjas
  - Se añade validación de franjas para picking sin planeación.
- Se cambia nombre de campo en migración
- Se cambia nombre de variable
  - Se cambia nombre de variable por modificación de migración

### feature/Add-new-query-to-BI-script

- Agrega nueva consulta para insertar en la tabla producto_detalle en el script de BI

### feature/REC-148-add-provider-delivery-window

- Se añade modulo de franjas horarias para proveedores
- Se añade modelo para tabla de ventanas de tiempo de proveedores
  - Se ajustan las vistas
- Ajustes varios
  - Se añaden campos para la franja de entrega en la orden de compra
  - Se ajusta formulario para guardar franja de entrega
  - Se completa modulo de franjas de entrega
- Ajustes varios
  - Se ajusta que la hora de inicio no pueda ser mayor a la hora final
  - Se añade la palabra seleccione al campo Bodega cuando se escoge la ciudad
  - Se actualiza mensaje de error al eliminar franja horaria
  - Se añade evento que limpia formulario de creación y edición de franjas
  - Se añaden validaciones cuando se elimina o desactiva una franja horaria con ordenes activas
  - Se ajusta validación para que no permita crear franjas horarias iguales en las bodegas
  - Se actualiza listado de franjas añadiendo columna ciudad y bodega
- Se mejora limpieza del formulario para que el selector de horas no almacene la hora seleccionada anteriormente

# [2.2.135] Hotfix - 2019-03-26 18:10

- Si se desea cambiar el estado de un producto el pedido debe estar in progress, de lo contrario no podrá

# [2.2.133] Hotfix - 2019-03-22 16:25

- Se ajusta consulta de pagos recibidos para SAP para no enviar totales en 0

# [2.2.132] Hotfix - 2019-03-19 18:25

- Guardar el error en actualizacion de stock por archivo

# [2.2.131] Hotfix - 2019-03-19 18:25

- Manejo de lockforupdate en actualizacion de stock por archivo

# [2.2.130] Release - 2019-03-19 16:50

## Fixed

- Ajuste al cambiar método de pago de pedido desde el dashboard con transacción fallida
- Ajuste de markers en mapa de seguimiento de transportadores

# [2.2.129] Hotfix - 2019-03-19 13:13

## Fixed

Ajustes en visualización de reporte de Devoluciones, entregas tarde y calificación negativa con respecto a las franjas horarias

# [2.2.128] Hotfix - 2019-03-18 01:00

## Fixed

Ajustes a markers en planeacion de rutas en mapa

# [2.2.127] Hotfix - 2019-03-18 18:19

## Fixed

Actualizar consulta para agregar datos a la tabla firstemailorder en la base de datos de BI

# [2.2.126] Hotfix - 2019-03-18 18:30

Se almacena la ip privada de la instancia donde suceden los errores.

# [2.2.125] Hotfix - 2019-03-18 17:47

Se almacena el estado de la ultima orden creada por el usuario en Leanplum
Se obtiene el estado de la ultima orden a partir de la variable "\$lastOrderCreated"

# [2.2.124] Release - 2019-03-18 17:10

## Added/Changed

- Se agrega un "job" para actualizar los usuario y sus atributos.
  - Se agrega la carpeta "/jobs" al composer.jso
  - Se agrega el job responsable de actualizar la información.
- Nuevas variables de configuración para las "colas"
- El helper de errores también almacena excepciones lanzadas desde la consola.
- Se actualizan las variables del usuario en Leanplum y se agregan algunos campos.
  - Nuevos campos del usuario en Leanplum .
  - Se crea una nueva clase para obtener los datos a modificar en Leanplum.
  - Se encola una tarea cada vez que el usuario se actualiza, se agrega crédito o se gestiona un pedido.
- Se envía el identificador del usuario para poder realizar la consulta desde el job.
- Al obtener los datos de la cola se espera un arreglo asociativo con el identificador del usuario.
- Se eliminan las tareas con datos incorrectos.
- Todas las tareas se ejecutan en la cola "quick"
- Se agrega la configuración de supervisor
- Se actualizan los workers cada vez que se realiza un despliegue y existen un cambio en la configuración de supervisor.
- Se obtiene la configuración de redis de las variables de entorno.

# Fixed

- Add permissions validation to actions on coupons

# [2.2.123] Hotfix - 2019-03-16 10:45

## Fixed

- Se ajustó query en api de surtidor

# [2.2.122] Hotfix - 2019-03-16 10:45

## Fixed

- Se mueve log de posición en la que estaba generando error

# [2.2.121] Hotfix - 2019-03-15 18:05

## Fixed

- Todo dentro de un beginTransaction en api ordercontroller en actualizar estado del pedido
- Guarda log de laravel al momento de modificar el estado de un producto del pedido

# [2.2.120] Hotfix - 2019-03-14 18:11

## Changed

- Ajuste en términos y condiciones.

# [2.2.119] Hotfix - 2019-03-14 17:18

## Changed

- Ajuste en términos y condiciones.

# [2.2.118] Release - 2019-03-14 14:27

## Changed

- En el API el driver de la sesión debe ser "array"

# [2.2.117] Hotfix - 2019-03-14 12:02

## Fixed

- Ajuste en los vínculos mostrados en el detalle de un banner.
- Se agrega el slug de la cuidad para crear correctamente el vínculo.

# [2.2.116] Release - 2019-03-13 19:20

## Fixed

- Ordenar horarios de entrega en la respuesta del javascript al momento de hacer un pedido.
- Se agregaron validaciones antes de actualizar estados.

# [2.2.115] Hotfix - 2019-03-13 17:22

## Fixed

- Se ajusta error con permisos para cambiar cantidades recibidas en módulo
  de recibos de bodega.
- Se ajusta la forma de guardar los datos dependiendo de la información enviada al controlador.

# [2.2.113] Hotfix - 2019-03-13 13:42

## Fixed

- Se ajusta error con permisos para cambiar cantidades recibidas en módulo
  de recibos de bodega.

# [2.2.112] Hotfix - 2019-03-13 12:00

## Fixed

- Se corrigen rutas para el módulo de almacenamiento, se pasan a cammel case.

# [2.2.111] Hotfix - 2019-03-13 12:00

## Fixed

- Corrección a error en modificación en recibos de productos agrupados.

# [2.2.110] Hotfix - 2019-03-13 10:30

## Fixed

- Guarda el log de producto en traslado entre bodegas de picking

# [2.2.109] Hotfix - 2019-03-12 17:34

## Fixed

- Se ajusta valor de pedido minímo de dark supermarket a cero (0)

# [2.2.108] Hotfix - 2019-03-12 11:39

## Fixed

- Se ajusta función subTotal en ProviderOrderReception ya que no estaba sumando los productos tipo proveedor en los totales de la factura de compra

# [2.2.107] Hotfix - 2019-03-12 10:09

## Fixed

- Se cambia llamado a commit por rollback en caso de ocurrir una excepción
- Se añade guadado de errores cuando la excepción por query no es código 4001

# [2.2.106] Hotfix - 2019-03-11 18:05

## Fixed

- Comando para asignacion de creditos a usuarios a traves de archivo

# [2.2.105] Hotfix - 2019-03-11 15:12

## Fixed

- Se ajustan rutas del módulo de almacenamiento, se cambian a cammel case.

# [2.2.104] Release - 2019-03-08 19:20

## Changed

- Se ajusta el cálculo del pum en combos de productos diferentes
  - El pum de cada producto se multiplica por el porcentaje de descuento del combo en caso de tenerlo.
- Se ajusta el cálculo del pum en combos de productos iguales
  - El pum de cada producto se multiplica por el porcentaje de descuento del combo en caso de tenerlo.

# [2.2.103] Hotfix - 2019-03-08 18:15

## Fixed

- Se ajusta impresión de facturas de venta en modulo de orden y factura manual
- Se añade total de nota crédito en pdf del documento p en factura de compra

# [2.2.102] Release - 2019-03-08 14:40

## Changed

Log en transacción de actualización de productos en inventario desde las Api picking - Se agrego mensaje y log para proceso de actualización de productos en inventario desde las Api

# [2.2.101] Hotfix - 2019-03-08 15:25

## Fixed

- Corregido manejo de excepción en el importador

# [2.2.100] Hotfix - 2019-03-08 14:40

## Fixed

- Declara rutas del dashboard que fueron eliminadas en cambios previos

# [2.2.99] Release - 2019-03-08 12:20

## Changed

- Implementado console command para ejecutar merqueo-tasks
  - Las tareas que estaban en AdminCronController están ahora en el comando de consola 'command:merqueo-tasks' que es ejecutable con artisan, recibe como parámetro una <task> que es la tarea a ejecutar
  - Para ayuda, ejecutar 'php artisan command:merqueo-tasks -h'
- Las tareas que estaban en AdminCronController fueron migradas
  - Se eliminaron las rutas que ejecutaban las tareas en AdminCronController, se eliminó AdminCronController y AdminCronTest
  - Las tareas están ahora en el comando de consola 'command:merqueo-tasks <task>'

## Fixed

- Ajustes para mitigar problema de descuentos en store_product_group
- Ajustes para mitigar problema de descuentos en store_product_group
  - Se restringe uso del importador del dashboard para productos combo
  - Ajustado exportador para no incluir combos en los reportes
  - Nuevo console command para ajustar descuentos en store_product_group
- Se ajusta el importador para evitar valores negativos en precio, precio_base y precio_especial
- Ajustes al importador para evitar error de librería EncryptCookies
  - Cuando la importación para actualizar productos es exitosa, se retorna un mensaje de éxito, antes se retornaban n mensajes, donde n era el número de filas del archivo de excel que se adjuntaba
- Se agregan validaciones al importador
  - Ahora se valida que los campos 'precio_descuento_merqueo', 'precio_descuento_proveedor' y 'precio_descuento_vendedor' no sean negativos
- Se agregan más validaciones al importador
  - El base_price y base_cost se calculan automáticamente dependiendo del price y el iva, cuando se usa el importador
  - Se valida que el special_price no supere el price
  - Se valida que que las suma de las distribuciones de descuento entre merqueo, vendedor y proveedor sea 100

# [2.2.98] Release - 2019-03-08 11:17

## Fixed

- Se corrige error con la obtención del código de error.

# [2.2.97] Release - 2019-03-08 10:35

## Fixed

- Se ajusta mesaje de error cuando ocurre un bloqueo por inventarios.

# [2.2.96] Release - 2019-03-07 19:30

## Changed

- Se retira el return_stock del calculo del current stock para los productos.

# [2.2.95] Bugfix - 2019-03-07 17:30

- Se ajustan vistas de factura de compra para calcular retenciones
- Se ajustan vistas de factura de compra para calcular retenciones
  - Se ajustan vistas y flujos en recibos de bodega para calcular las retenciones con el subtotal de la factura menos el total de la nota crédito.
  - Se ajustan vistas y flujos en recibos de bodega para ver valores sin decimales
  - El total de la factura se calcula descontando el valor de la nota crédito
- Se ajusta módulo factura de compra para sumar todos los productos
  - Se ajusta módulo de factura de compra para que se puedan sumar los productos en estado no recibido que tengan unidades en factura
  - Se ajustan servicios para retornar la factura con los nuevos ajustes
  - Se ajusta modelo para filtrar los valores correctamente.
- Se ajusta campo Base de servicio factura de compra enviado a SAP
- Se ajusta calculo de retenciones en factura de compra

# [2.2.94] Hotfix - 2019-03-06 19:10

- Se ajustó módulo de seguimiento al transportador, con vistadas y controladores
- Se realizaron pruebas con el usuario final

# [2.2.93] Hotfix - 2019-03-06 17:52

- Se añade commit para finalizar la transacción al mometo de reprogramar un pedido

# [2.2.92] Hotfix - 2019-03-06 15:10

- Se ajusta módulo de novedades de transportador para generar reporte
- Se ajustan fechas y relaciones.

# [2.2.91] Release - 2019-03-06 13:00

## Added

- Añadidos últimos cambios de rama feature/EPK-300-implementar-lock-en-modulo-de-calidad (pull request #162)

# [2.2.90] Release - 2019-03-06 14:10

## Added

- Ajuste de método de investigación en modulo de calidad
  - Se ajustó método implementando transacción.
  - Se ajustó query que obtienen datos de inventarios
- Se agregó mensaje en exception de errores en api de picking.
- Se eliminó impresión de mensaje en excepción
  - Se eliminó mensaje de impresión en excepción en los métodos de investigación
- Ajuste método que actualiza inventario de productos
- Se implementa lockForUpdate
  - Se implementa lockForUpdate en método getStoreProductWarehouse utilizado stock-management/product-stock para actualizar el stock de picking
- Agregar lock en almacenamiento
  - Se agrega lockForUpdate al método save en admin/inventory/storage/storageController
  - Se crea seed para cambiar a camel case los nombres de los métodos del controlador en el menu_item
- Guardar la url al modificar un producto
- Agregar mensaje para error de concurrencia
- Agregar mensaje para error de concurrencia
  - se agrega al catch del método save en storageController el manejo de la excepción 40001 para mostrar al usuario que debe volver a intentar la transacción
  - Se agrega mensaje de error cuando ocurre error 40001

## Fixed

- Lock en pedidos, conteos, traslados de bodegas y picking
- Ajuste bloqueo de registros en reception stock
  - Se bloquean los registros al actualizar el reception stock en módulos de recibo de bodega, conteo por posiciones, almacenamiento de recibos.
- Se corrige implementación
  - Se cambia implementación de método getStoreProductWarehouse por getStoreProductWarehouseWithLocking y se elimino dd en un catch
- Cambio de bodega en tablero de pedidos
- Se ajusta funcionalidad de fechas en traslado de productos entre bodegas
  - Se guarda y se muestra correctamente la fecha de creación de traslado
  - Se guarda y se muestra correctamente la fecha de recibo del traslado
  - Se guarda y se muestra correctamente la fecha de almacenamiento de recibo
- Ajuste en surtidor de picking
  - Se cachean las excepciones cuando una posición de almacenamiento deja de existir
  - Se cachea la excepción cuando un surtidor de picking no existe.
- Actualizar el inventario de picking antes de cobrar en pedidos alistados
- Se ajusta código de surtidor de picking, se agrega manejo de excepciones
  - Código de surtidor de alistamiento, se eliminan consultas repetidas
  - módulo de traslado masivo, se agrega manejo de excepción de error deadlock
- Se ajusta el catch del las transacciones en movimiento entre posiciones
- Se agrego el queryexception en pedidos de dashboard, despachos, conteos y traslados de picking
- Se agrega excepción a módulos donde se usa el lockforupdate
- Se ajusta el módulo de almacenamiento de recibo
- Traer los pedidos con delivery_date en tablero de pedidos
- Se arregla confirmación al cerrar el conteo

# [2.2.89] Hotfix - 2019-03-06 11:40

## Fix

- Se agregan los terminos y condiciones
  - Se agregan los terminos y condiciones a la web
  - Cambio en el copy de la campaña de Visa

# [2.2.88] Release - 2019-03-05 22:40

## Added

- Se agregan campañas de forma dinámica al checkout.
  - Creación de una nueva carpeta en app/ con la lógica de las campañas.
  - Se implementan validaciones especificas que pueden ser reutilizadas en otra campaña.
  - Se agrega una nueva carpeta para los providers a use a en el proyecto.
  - Se agrega el fragmento de código a ApiCheckoutController y CheckoutController donde se realizan las validaciones referentes.

# [2.2.87] Hotfix - 2019-03-05 10:58

## Fixed

- Se agrega una notificacion por email al cron de envio de SMS diarios para ver ejecucion del cron

# [2.2.86] Hotfix - 2019-03-05 10:03

## Fixed

- Se ajusta formato de fecha a Y-m-d en servicio de pagos recibidos campo CardValidUntil

# [2.2.85] Hotfix - 2019-03-04 20:22

## Fixed

- Se ajusta ciclo para actualizar stock de productos en devoluciones y actualizar una sola vez el producto con el total de las cantidades
- Se añade función que trae una colección de productos bloqueada

# [2.2.83] Bugfix - 2019-03-04 10:52

## Fixed

- Se genera referencia al pedido hijo cuando se reprograma el padre
- Se remueve generar referencia de la clase Payu
  - Se ajusta la transacción a Payu con la referencia de la orden.
  - Antes se generaba una nueva referencia antes de la transacción que no era la de la orden.
- se resetea los valores de nota crédito en pedidos hijos.
- Se ajusta modulo de reprogramación de pedido
  - Se bloquean las ordenes lado servidor al momento de estar entregados y facturados los pedidos
  - al crear re programación se resetean los campos de factura y nota crédito de la nueva orden.
- Se agrega validación de orden entragada facturada al cambiar status

# [2.2.82] Hotfix - 2019-03-04 10:40

## Fixed

- Se ajusta formato en campo CardValidUntil del servicio pagos recibidos enviado a SAP, debido a error en la integración.

# [2.2.81] Hotfix - 2019-03-01 21:55

## Fixed

Se ajusta función para retornar las franjas horarias para las bodegas de Dark Supermarket

# [2.2.80] Hotfix - 2019-03-01 17:28

## Fixed

Ajuste en el proceso para apagar un banner.

- Se omiten los campos en la tabla banner_store_product a la hora de activar u ocultar un banner.
- Al activar los banner se toma el total de productos visibles.

# [2.2.79] Hotfix - 2019-02-28 17:08

## Fixed

- Se comentan servicio de entrada y salida de inventarios hasta nueva orden
- Se ajusta servicio de pagos recibidos para enviar las cuentas bien y la fecha de caducidad de tarjetas de crédito hasta el 2030

# [2.2.78] Hotfix - 2019-02-28 15:26

## Fixed

- Se ajusta order by de los productos a alistar

# [2.2.77] Hotfix - 2019-02-27 15:01

## Fixed

- Se añaden los estados Alistado e In Progress para quitar route_id al momento de eliminar una planeación

# [2.2.76] Hotfix - 2019-02-26 01:51

## Fixed

- Cache de 10 min a metodo que obtiene franjas de entrega

# [2.2.75] Hotfix - 2019-02-26 01:44

## Fixed

- Modificación y optimización a query de franjas de entrega utilizando vista de slots

# [2.2.74] Release - 2019-02-25 10:18

## Added

- Cache de Laravel por base de datos solo para servicio de horarios y configurable con variable de entorno

# [2.2.73] Hotfix - 2019-02-24 18:26

## Fixed

- Se ajusto método que retorna grupo de alistamiento

# [2.2.72] Hotfix - 2019-02-24 14:49

## Fixed

- Actualizacion de cache de horarios cada 10 min

# [2.2.72] Release - 2019-02-24 13:41

## Added

- Cronjob con comando para actualizar cache de slots por porductos por zona cada cierto tiempo
- Cache de Laravel por base de datos

## Fixed

- Ajuste a transacción de pedido en camino desde app de transportadores

# [2.2.71] Hotfix - 2019-02-24 10:30

## Fixed

- Cache de 5 min en servicio de horarios temporalmente
- Reemplazo de salto de linea en JSON de pedidos en mapa de seguimiento

# [2.2.70] Release - 2019-02-23 15:43

## Added

- Validación de estado anterior de pedido al actualizarlo desde el dashboard
- Validación de precio unitario de combo vs precio de productos individuales

#[2.2.69] Hotfix - 2019-02-23

## Fixed

- Se actualiza el pedido minimo para dark supermarket a 4000

#[2.2.68] Hotfix - 2019-02-23

## Fixed

- Ajuste de orderby en autoasignaciòn de picking sin planeación

#[2.2.67] Hotfix - 2019-02-23

## Fixed

- Ajuste de orden de visualización de auto asignación pedido

#[2.2.66] Hotfix - 2019-02-23

## Fixed

- Ajuste de orderby en auto asignación de picking

# [2.2.63] Release - 2019-02-21 18:59

## Added

- Nuevo tablero de picking
- Se agrega bloqueo de registros para actualizacion de stock en app y surtidor de picking
- Se agrega log a modificacion de samplings

## Fixed

- Se cambio la validación del estado de la transacción de una tarjeta de crédito para poder realizar el cambio de método de pago.
- Se ajustó filtro de productos tipo agrupado al devolver productos de un pedido entregado

Se cambio la validación del estado de la transacción de una tarjeta de crédito para poder realizar el cambio de método de pago.

# [2.2.62] Hotfix - 2019-02-21 14:30

## Fixed

- Se ajusta módulo nota crédito de venta para no generar devoluciones a bodega

# [2.2.61] Hotfix - 2019-02-21 14:30

## Fixed

- Se activa correo usuarios de bodega para recepcion de correos de pedidos

# [2.2.60] Release - 2019-02-21 13:49

## Added

- Nueva funcionalidad para pedidos de Darksupermarket con Domicilios.com desde web

# [2.2.59] Hotfix - 2019-02-21 12:21

## Fixed

- En página de detalles de pedido, alerta de reembolso de faltantes sólo aparece cuando el método de pago es PSE.

# [2.2.58] Hotfix - 2019-02-214 10:10

## Fixed

- Se ajusta servicio de factura de compras filtrando productos en estado diferente a no recibido y pendiente
- Se ajusta servicio de pagos recibidos, se quita relacion con modelo CreditCard y se toma el valor del campo cc_last_four en caso de ser pago con tarjeta de crédito.

# [2.2.57] Hotfix - 2019-02-20 19:28

## Fixed

- No asignar consecutivo de factura a pedidos en cronjob

# [2.2.56] Hotfix - 2019-02-20 19:28

## Fixed

- Agregar transacción en API de transporte al entregar pedido
- Ajuste a al actualizar costo de producto en recibo

# [2.2.55] Release - 2019-02-20 18:53

## Added

- Integración con sistema SAP (servicios, plantillas, modulos)

# [2.2.54] Release - 2019-02-20 15:27

## Added

- Datos de usuario para facturación de pedido en apps y web en el checkout

## Fixed

- Ajuste a valor por referir a nuevo usuario en pantalla de pedido exitoso en apps
- Ajuste en cantidad original de producto al crear pedido de reclamo
- Setear en cero productos indivicuales de comboi en pedido de reclamo cuando van en cero

# [2.2.53] Release - 2019-02-19 20:00

## Added

- Manejo de concurrencia en el módulo de traslado de productos entre bodegas.
  - Se agrega la función de concurrencia de los registros de storeProductWarehouses al momento de recibir el traslado.
  - Se agrega la función de concurrencia de los registros de WarehouseStorage al momento de almacenar el traslado.
  - Se refactoriza el modelo ProductTransfer.
  - Se ajusta controlador de traslado de productos, se quitan variables que no se usan.
  - Se capturan las excepciones correctas.

El changelog del proyecto se lleva ahora en el repositorio y NO en Google Drive

- En las consultas de recibo de bodega se utiliza la función para bloquear los registros mientras se hace la actualización del stock de recibo.

# [2.2.52] Release - 2019-02-18 16:00

## Added

- El changelog del proyecto se lleva ahora en el repositorio y no en Google Drive
- Reportes de inventario
  - Se crea el controlador StockReportController para generar los reportes de productos próximos a vencer, productos apagados con stock y productos activos sin stock
  - Se agrega en la carpeta seeds el archivo ReportStockSeeder para actualizar el menú de inventario con el enlace a los reportes
  - Se agrega en views/inventory la carpeta reports con los 3 reportes y los js para los mismos
- Reportes para inventario
  - Se agrega a ReportStockSeeder el nombre del método para el menú de los productos apagados con stock
- Ajuste en reporte apagados
  - Se modifica la consulta para productos apagados con stock porque faltaba restringir el almacenamiento por el id de bodega
  - Se modifica la vista para que el botón de buscar aparezca luego de los dos campos de búsqueda

# [2.2.51] Hotfix - 2019-02-15 13:17

## Fixed

Se ajustó de validación de pedidos cancelados en autoasignación
Se ajustó orden de pedidos por zonas
Corrige error accediendo a variable no definida en SendWelcomeEmailToUserAction (2019-02-18)

# [2.2.50] Release - 2019-02-14 18:28

## Added

- Guardar log de todos los movimientos de store_product_warehouse
- Modificación a cronjob con query de BI retention con fecha actual
- Se implementó envío de correos para tareas programadas de BI

# [2.2.49] Hotfix - 2019-02-14 16:15

## Fixed

- Se ajusta query de asignación de pedidos para que sea más ligera
- Se agrega uso de error log a la asignación de pedidos

# [2.2.46] Release - 2019-02-14 14:38

## Added

- Callback de PSE para actualización de estado de transacción.
- Envío de SMS con nuevo proveedor Infobit.

# [2.2.45] Hotfix - 2019-02-14 12:50

## Fixed

Posible fix para corregir error al enviar correo de pedido recibido

# [2.2.44] Hotfix - 2019-02-14 12:30

## Fixed

Se corrige error de asignación de pedidos a picker, ahora solo se asigna un pedido a un picker si este tiene productos de su tipo (frío o seco)

# [2.2.43] Hotfix - 2019-02-13 21:50

## Fixed

Se corrige envio de mail al crear pedido, se carga objeto order para obtener los productos del pedido y poder pintarlos en el mail

# [2.2.42] Hotfix - 2019-02-13 19:50

## Fixed

- Se corrige grupos duplicados en app
- Se agrega llave a arreglo con la orden asignada al picker
- se elimina código basura
- Se eliminó llamado cíclico para asignar pedidos a picker

# [2.2.41] Hotfix - 2019-02-13 14:45

## Fixed

Se ajusta validación de fechas en las ordenes en que se asignan los pedidos sin planeación a los picker

# [2.2.40] Hotfix - 2019-02-13 14:30

## Fixed

Se ajusta orden en que se asignan los pedidos sin planeación a los picker

# [2.2.39] Hotfix - 2019-02-13 11:30

## Fixed

Se ajusta asignación de órdenes a picking cuando se presenta alta concurrencia

# [2.2.38] Hotfix - 2019-02-12 14:42

## Fixed

Se agrega use de OrderLog para pagos con tarjeta de crédito

# [2.2.37] Release - 2019-02-11 19:23

## Added

- Nuevo método de pago PSE para web
- Campo para saber si una franja de entrega es nueva o no para las apps
- Nuevos filtros en grilla de órdenes de compra
- Cronjob para cerrar ordenes de compra automáticamente días después
- Modificación de términos y condiciones en costo de domicilio por horarios
- Nuevos servicios para picking sin planeación
- Se quitan queries en home de dashboard

# [2.2.36] Hotfix - 2019-02-07 17:16

## Fixed

Se filtran los productos de los banners tipo conjunto de productos.

# [2.2.35] Hotfix - 2019-02-07 22:43

## Fixed

Se ajusta la versión del app de transportador

# [2.2.34] Hotfix - 2019-02-07 16:51

## Fixed

- Se arregla error en traslado de productos entre bodegas
- Se ajusta el error de no enviar el id de la bodega de destino del traslado

# [2.2.33] Release - 2019-02-06 17:37

## Added

- Asociar el pedido de frío a seco en app de picking
- Se eliminaron métodos para la vista de alistar con carrito.
- Se ajustó la lógica para alistar solo pedidos con planeación.
- Se implementó método add de modelo ErrorLog al finalizar el alistamiento.
- Se eliminaron archivos que no pertenecían a la api actual.
- Módulo de traslado de productos de picking, permite el traslado de los productos de una posición a otra de picking, actualizando la altura y posición
- Modulo de investigación de inventario, El sistema registra en stock de investigación los conteos por posición donde hubo diferencias con ajustes positivos o negativos. Dichos registros se mostrarán en el módulo de inventario en investigación permitiendo al usuario verificar lo ocurrido y dar una solución a cada producto que tuvo ajustes.
- Se reestructura el log de faltantes por solicitud de BI ya que era algo engorrosa la lectura de datos.
- Ajustes a filtros de búsqueda en grilla de órdenes de compra.
- Cronjob para cerrar automáticamente órdenes de compra con fecha de entrega de más de dos días.

## Fixed

Se corrige longitud de texto en factura de pedido que estaba sobrepasando el límite de la celda.
Ajuste a detalle de pedidos en la misma dirección desde el dashboard.

# [2.2.32] Hotfix - 2019-02-04 18:21

## Fixed

- En el buscador muestra el 5% de descuento en los productos no tiene promoción o los que tienen menos del 5%.
- Se usa camelCase en la variable \$promo_condition
- Al eliminar un producto desde el help center se almacena en el log de la orden el registro y se eliminan los productos de la tabla order_product_groups.
- Al cambiar las cantidades de un combo se actualiza el campo quantity_original en la tabla order_product_groups.

# [2.2.30][2.2.31] Hotfix - 2019-01-31 15:09

## Fixed

- Se agrega el 5% de descuento a todos los productos en caso de que se cumplan ciertas condiciones, este porcentaje se ve reflejado en el API, banners y buscador.

# [2.2.29] Release - 2019-01-31 14:14

## Added

- Se agregan pruebas unitarias para pedidos en web y servicio de horarios de entrega en API

## Fixed

- Se ajusta servicio que muestra pedidos en misma dirección en el detalle del pedido en dashboard
- Se ajusta monto mínimo en comprar para utilizar crédito
- Ajuste a módulo de fraude cuando el descuento es mayor al que se entrega por referido
- Ajuste a enlace de términos y condiciones en modal de apps, enviar a sección de referidos
- Fijar cantidad original de productos de un pedido al crear reclamo

# [2.2.28] Hotfix - 2019-01-30 15:09

## Fixed

El monto mínimo del pedido para utilizar créditos aumenta a 80000

# [2.2.27][2.2.26] Hotfix - 2019-01-28 17:09

## Fixed

- Ajuste a enlaces y estilos en nuevo landing de referidos

# [2.2.25] Release - 2019-01-28 16:55

## Added

- Ajustes a sistema de referidos:
  - Nuevos montos de créditos
  - Nueva landing page que explica referidos por la app
  - Restricción al referir por web
  - Puntuación a pedido con posible fraude
- Query de BI en cronjob
- Unit test para pedido atómico en web

# [2.2.24] - 2019-01-25 12:17

## Fixed

- No activar productos con stock igual al comprometido

# [2.2.21][2.2.22] [2.2.23] Hotfix - 2019-01-24 16:20

## Fixed

- Pruebas con repositorio

# [2.2.20] Hotfix - 2019-01-24 20:39

## Fixed

- Eliminación de columna cantidad de productos en grilla de pedidos en pre planeación
- Cambio a límite de hora para pedir el mismo dia

# [2.2.19] Hotfix - 2019-01-24 16:20

## Fixed

- Ajustes generales
  - Habilitar domicilio gratis para adquisición
  - Ajuste a devolución de crédito a cliente al cancelar pedido
  - Ajuste en cantidad de productos en pantalla previa a generar planeación de rutas
  - Ajuste a ruta de grilla de alistadores
  - Ajuste a coordenadas que vienen en 0.0 en API de apps
  - Ajuste a validación de stock de productos agotados al crearse nuevo pedido
  - Avances en unit tests de pedido en web

# [2.2.18] Hotfix - 2019-01-24 15:38

## Fixed

- Se ajusta planeación para corregir error al generar rutas del mismo día.
  - Se corrige cuando el arreglo no trae la posición 0 o la posición 0 está vacía

# [2.2.17] Hotfix - 2019-01-23 16:57

## Fixed

- Se ajusta la función que envía por correo el reporte de conteos.
  - Se envía correos solo a las personas asociadas a la bodega del conteo.
  - Para las fechas se usa la librería Carbon.

# [2.2.16] Hotfix - 2019-01-22 17:05

## Fixed

Se cambia el valor de la variable minimum_discount_amount de 80.000 a 50.000

# [2.2.15] Hotfix - 2019-01-18 18:15

## Fixed

Ajuste a bloqueo de redención de código referido desde web.

# [2.2.14] Hotfix - 2019-01-18 11:37

## Fixed

Cambio en el listado de horarios de entrega y descuento en domicilio.

- Los horarios de entrega para el administrador se muestran en su totalidad cuando aplica el mismo día.
- Se realiza el descuento al costo del domicilio para todos los productos que aplica.

# [2.2.13] Hotfix - 2019-01-18 11:37

## Fixed

Se revierte código de validación de referido en checkout en web

# [2.2.12] Release - 2019-01-17 18:01

## Added

- Bloquear referido desde web

## Fixed

- Ajuste a ID de franja de entrega al generar reclamo
- Ajuste a cantidad original de producto al editar estado desde el dashboard

# [2.2.11] Hotfix - 2019-01-11 11:30

## Fixed

Bloquear el botón de alistado en el dashboard.

- Al momento de alistar el pedido desde el dashboard en bodega, bloquear el botón de alistado mientras realiza la petición y recarga la página.
- Permite un solo click al botón de alistar faltantes en el módulo de faltantes

# [2.2.10] Hotfix - 2019-01-16 14:18

## Fixed

Validación del monto del domicilio en el listado de horarios de entrega.

- A la hora de calcular los horarios de entrega se tiene en cuenta el precio especial del producto teniendo en cuenta los rangos de fechas configurados.
- A la hora de asignar el costo de la hora de entrega de cada franja se asignan los valores al modelo DeliveryWindow.

# [2.2.9] Hotfix - 2019-01-16 02:31

## Fixed

- Se quita validación de ruta en assignOrder en PickingGroup

# [2.2.9] Hotfix - 2019-01-16 01:58

## Added

- Se agregan dos roles nuevos a la creación de alistadores Empacador y Empacador Extra.

# [2.2.8] Hotfix - 2019-01-16 01:23

## Fixed

- Se modifica método recursivo de asignación de pedidos a pickers desde el app, Se sube para ajustar error 404.

# [2.2.8] Release - 2019-01-15 19:25

## Added

- Se modifica método de asignación de pedidos a pickers desde el app, ahora se asigna automáticamente. Se sube para realizar piloto en bodega.

# [2.2.7] Hotfix - 2019-01-15 19:53

## Fixed

La zona suministrada para obtener los horarios de entrega se obtiene del input, en caso de que no sea enviada se obtiene por la bodega actual.

# [2.2.6] Hotfix - 2019-01-15 19:03

## Fixed

Ajustes en la validación de los horarios de entrega en el dashboard.

- Se remueve la validación de la fecha de entrega a la hora de programar un pedido.
- Se muestran todos los horarios disponibles para la entrega sin incluir las franjas del mismo día.
- Se retornan los horarios de entrega dependiendo de la zona actual.
- Se modifica la hora límite de selección de horarios del mismo día a las 2pm.

# [2.2.5] Hotfix - 2019-01-15 12:47

## Fixed

Ajustes generales:

- Ajustes al revertir estado de pedido y cambiar franja de entrega
- Ajuste al redimir cupón en API checkout
- Habilitar token en API
- Validar eliminación de tarjeta de crédito en API

# [2.2.4] Hotfix - 2019-01-15 10:37

## Fixed

- Se ajusta javascript que carga la animación del loader en el módulo de novedades de transportador

# [2.2.3.1] Hotfix - 2019-01-15 10:14

## Fixed

- Al reprogramar un pedido se tiene en cuenta la nueva fecha de entrega obtenida del input.

# [2.2.3] Hotfix - 2019-01-15 10:14

## Fixed

- Ajuste a edición de fecha de entrega de pedido desde dashboard, al reprogramar un pedido se tiene en cuenta la nueva fecha de entrega obtenida del input.
- Ajustes a franjas horarias en módulo de validación de pedidos

# [2.2.2] Hotfix - 2019-01-15 09:39

## Fixed

- Ajuste en migración de menú de Zonas y en variable de barrio en API checkout al validar dirección
- Ajustes a franja horaria en módulo de generación de reclamos
- Eliminación de clases no utilizadas en API checkout

# [2.2.2] Hotfix - 2019-01-15 23:39

## Fixed

- Ajuste en migración de menú de Zonas y en variable de barrio en API al validar dirección.

# [2.2.1] Hotfix - 2019-01-14 23:39

## Fixed

- Se sube script para actualización de ids de franja horaria de pedidos

# [2.2.0] Release - 2019-01-14 22:44

## Added

- Nuevo módulo de franjas horarias
- Ajuste a módulo de slots de franjas horarias (pasó de pedidos a productos)
- La cobertura ahora se define por la zonas
- Eliminación de tablas y campos de tiendas del modelo de negocio anterior
- Modal de pare y compare de PUM en página web

# [2.1.1] Hotfix - 2019-01-11 17:15

## Fixed

- Se añade seteo de estado de cumplimiento para los padres de los productos agrupados que no se había definido en servicio de picking
- Se ajusta validación de login desde app picking para evitar error de sesión cuando cambian de dispositivo en version 11

# [2.1.1] Hotfix - 2019-01-11 16:58

## Fixed

- Se deshabilita el botón de creación de conteos para evitar que al oprimir varias veces se creen varios conteos iguales a la vez.
- Se agregó columna en detalle de pedido de cantidad alistada de producto tipo sampling.
- Se eliminan correos para informe de conteos por posición.

# [2.1.1] Hotfix - 2019-01-10 16:28

## Fixed

- Se cambia nombre de variable que no existía en API de app de picking para evitar error al alistar pedido específico.

# [2.1.1] Hotfix - 2019-01-10 15:29

## Fixed

- Se cambia nombre de variable que no existía en API de app de picking para evitar error al alistar pedido específico.

# [2.1.0] Release - 2019-01-09 18:00

## Added

- Nuevo módulo de faltantes
- PUM en banners conjunto de productos, combos en buscador y modal en web de mejor precio garantizado
- Se agregó columna en detalle de pedido grilla de productos con cantidad alistada

## Fixed

- Fijar campos a null al revertir estado de pedido gestionado a despachado desde el dashboard

# [2.1.1] Hotfix - 2019-01-10 15:29

## Fixed

- Manejo de excepciones en el importador al seleccionar 'Actualizar productos'
