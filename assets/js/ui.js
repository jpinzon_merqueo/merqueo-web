// Here we will handle everything related to the UI
var geocoder = "";
var address = null;
$.stringaddress = "";
var is_minimum_reached = [];
var pendingAddedProduct = null;

$(document).ready(function () {
  // se resetea la variable para migrar en la nueva web
  var storageNewWeb = JSON.parse(localStorage.getItem("data"));
  if (storageNewWeb && "user" in storageNewWeb) {
    var path = window.location.pathname

    if ("isMigrate" in storageNewWeb.user && storageNewWeb.user.isMigrate && path != "/") {
      trackEvent('new_to_old_transition');

      storageNewWeb.user.isMigrate = false;
      localStorage.setItem("data", JSON.stringify(storageNewWeb));
    }
  }

  $("body").on("click", ".logout", function (e) {
    e.preventDefault();
    
    var arrayNotClear = ['nav-bar', 'number', 'version_asigned'];
    for ( var i = 0, len = localStorage.length; i < len; ++i ) {
      if(!arrayNotClear.includes(localStorage.key(i))){
        localStorage.removeItem(localStorage.key(i));
      }
    }

    var href = $(this).attr("href");
    window.location.href = href;
  });

  get_cart();
  setUpEvents();

  if ($(".categories").length > 0) {
    var urlact = window.location.href;
    var arr = urlact.split("/");
    for (var i = 0; i <= 5; i++) {
      if (i == 0) urlact = arr[i];
      else urlact = urlact + "/" + arr[i];
    }

    $(".categories .nav-pills li").each(function () {
      var url_menu = $(this)
        .children()
        .attr("href");
      if (urlact == url_menu) {
        $(this)
          .children("a")
          .addClass("open dropup");
        $(this)
          .children("ul")
          .addClass("on");

        if (arr[6]) {
          console.log("submenu");
          $(this)
            .children(".sub-category")
            .children("a")
            .each(function () {
              var url_submenu = $(this).attr("href");
              var urlsub = urlact + "/" + arr[6];
              if (url_submenu == urlsub) {
                $(this).addClass("curent");
              }
            });
        }
      }
    });
  }

  var ancho = $(window).width();
  var anchocont = $(".navbar .col-md-12 .contentmenu_home").width();
  var anchodrop = $(".navbar .col-md-12 .dropdown-menuhome").width();
  var valuecal = anchocont - anchodrop;
  $(".navbar .col-md-12 .dropdown-menuhome").css("margin-left", valuecal);

  $(window).resize(function () {
    var ancho = $(window).width();
    var anchocont = $(".navbar .col-md-12 .contentmenu_home").width();
    var anchodrop = $(".navbar .col-md-12 .dropdown-menuhome").width();
    var valuecal = anchocont - anchodrop;
    $(".navbar .col-md-12 .dropdown-menuhome").css("margin-left", valuecal);
    if ($(".reloadcart").length > 0) {
      if (ancho < 1367) {
        $(".container-fluid .sub-menu .col-md-8").css("width", "61%");
        $(".container-fluid .sub-menu .col-md-4").css("width", "39%");
        $(".pagecontentscroll .container-fluid").css("width", "39%");
        $(".pagecontentscroll .container-fluid").css("top", "202px");
        $(".store-credit-message").css("margin-left", "2px");
        $(".nav-stacked").css("top", "202px");
      }
    }
  });

  var urlsend = "";
  $(window).resize(function () {
    if ($(window).width() > 767) {
      if ($(window).width() > 1366) {
        if ($(window).width() > 1904) {
          var distance = $("#cart-desc").hasClass("pinned")
            ? $(".top-head-fix").height()
            : $(".top-head-fix").height() -
            $("#navbar").height() -
            $(".banner-top").height() -
            ($(".top-head-fix").height() -
              $("#navbar").height() -
              $(".banner-top").height()) /
            1.9;
        } else {
          var distance = $("#cart-desc").hasClass("pinned")
            ? $(".top-head-fix").height()
            : $(".top-head-fix").height() -
            $("#navbar").height() -
            $(".banner-top").height() -
            ($(".top-head-fix").height() -
              $("#navbar").height() -
              $(".banner-top").height()) /
            2.1;
        }

        var height = $(window).innerHeight() - distance - 150;
        var distancenew =
          $(window).innerHeight() - $(".top-head-fix").height() - 80;
        if (!$("#cart-desc .contenscroll .contentstoredata").length) {
          height = height + 135;
        }

        $("#cart-desc .contenscroll").css({
          height: height - 95,
          top: distance
        });
      } else {
        var distance = $("#cart-desc").hasClass("pinned")
          ? $(".top-head-fix").height()
          : $(".top-head-fix").height() -
          $("#navbar").height() -
          $(".banner-top").height() -
          50;
        var height = $(window).innerHeight() - distance - 150;
        var distancenew =
          $(window).innerHeight() - $(".top-head-fix").height() - 80;

        if (!$("#cart-desc .contenscroll .contentstoredata").length) {
          height = height + 135;
        }

        $("#cart-desc .contenscroll").css({
          height: height - 95,
          top: distance
        });
      }

      $(".categories .nav").css({ height: distancenew, top: distance });
      $(".page-content").css("margin-bottom", "120px");
    }

    if ($(window).width() < 768) {
      $("#cart-desc").insertBefore(".pro-bg");
    }
  });

  $(".top-category").each(function (e) {
    if (
      $(this)
        .next()
        .find("a").length == 0
    ) {
      $(this)
        .find(".caret")
        .remove();
    } else {
      $(this).removeClass("link");
      $(this)
        .find(".click-link")
        .click(function () {
          $(this)
            .parent()
            .off()
            .trigger("click");
        });
    }
  });

  $(".top-category").click(function (e) {
    if (
      $(this)
        .next()
        .find("a").length > 0
    ) {
      e.preventDefault();
      e.stopPropagation();
      $(this)
        .toggleClass("open dropup")
        .next()
        .toggleClass("on");
      return false;
    }
  });

  $(".reloadcart").fancybox({
    maxWidth: 1200,
    maxHeight: 800,
    fitToView: false,
    width: "80%",
    height: "80%",
    autoSize: false,
    closeClick: false,
    openEffect: "none",
    closeEffect: "none",
    beforeClose: function () {
      var $iframe = $(".fancybox-iframe");
      if ($(".added-to-cart", $iframe.contents()).length)
        window.location.reload();
    }
  });

  $(".product-list-img img").lazyload();
  $("#cart-desc").hide();

  $("#cart-btn").click(function () {
    var button = $(this);
    urlsend = button.hasClass("reload");
    //console.log(urlsend);
    if (!urlsend) {
      if (button.hasClass("cart-close")) {
        button.removeClass("cart-close").addClass("cart-open");
        $("#arrow-icon")
          .removeClass("icon-chevron-right")
          .addClass("icon-chevron-down");
        $("#cart-desc").show();

        // Track Cart Viewed event
        trackEvent("cart_viewed", {
          size: $(".contentproductcart .sd-cart-pro-li").length,
          items: $(".cart-total-quantity").text(),
          ticket: parseInt(
            $(".number_pedi span")
              .text()
              .replace(/\D+/g, "")
          )
        });
        window.addEventListener("click", enableEventListener);
      } else {
        hide();
        window.removeEventListener("click", enableEventListener);
      }
    }

    function enableEventListener(event) {
      var element = $(event.target);

      if (element.prop("id") === "cart-btn") {
        return true;
      }

      while (element.parent().length) {
        if (
          element.parent().prop("id") === "cart-btn" ||
          element.parent().prop("id") === "cart-desc"
        ) {
          return true;
        }

        element = element.parent();
      }

      hide();
    }

    function hide() {
      $("#cart-btn")
        .removeClass("cart-open")
        .addClass("cart-close");
      $("#arrow-icon")
        .removeClass("icon-chevron-down")
        .addClass("icon-chevron-right");
      $("#cart-desc").hide();
    }
  });

  $("body").on("click", ".cart-less, .cart-add, .cart-add-initial", function (
    e
  ) {
    e.preventDefault();
    e.stopPropagation();
    $(".reloadcart").hide();
    $(".confirm-mapsaddress").addClass("hide");
    $(".addresses").addClass("hide");
    $(".select_address").show();
    modify_cart(
      $(this).attr("data-id"),
      $(this).attr("data-warning"),
      $(this).hasClass("cart-less") ? -1 : 1,
      0,
      $(this).data("referrer"),
      e
    );

    return false;
  });

  /**** Auto complete **/
  $(".navbar-form input")
    .keyup(debounce(buscar, 300))
    .focusout(function () {
      $(".navbar-form .input-group").removeClass("autocompleting");
    })
    .focus(function () {
      if ($(this).val().length > 3 && $(".autocomplete").html() != "") {
        $(".navbar-form .input-group").addClass("autocompleting");
      }
    });

  //validar cobertura en dirreccion
  $("#address-city .modal-dialog .modal-content").on(
    "click",
    ".btn-validate-address",
    function (event) {
      if (!$(".btn-validate-address").hasClass("disabled")) {
        if (
          $("#dir1").val() == "" ||
          $("#dir2").val() == "" ||
          $("#dir3").val() == "" ||
          $("#dir4").val() == ""
        ) {
          $(".form-has-errors")
            .html("Debes ingresar la dirección completa.")
            .show();
        } else {
          address = {};
          address.dir1 = $("#dir1").val();
          address.dir2 = $("#dir2").val();
          address.dir3 = $("#dir3").val();
          address.dir4 = $("#dir4").val();
          trackEvent("address_entered", {
            address: addressToStr(address),
            screen: pageName()
          });
          send_data_address(address);
        }
      }
    }
  );

  $("#address-city .modal-dialog .modal-content").on(
    "click",
    ".addressitemsave",
    function (event) {
      var address_id = $(this).data("id");
      $("#address-city .select_address .loading_user").css(
        "display",
        "inline-block"
      );
      send_data_address(address_id);
    }
  );

  $("body").on("click", ".confirm-address", function () {
    var latitude = $(this).data("lat"),
      longitude = $(this).data("lng");
    var point = { lat: latitude, lng: longitude };
    var address = $(this).data("address");
    $("#not-match-address, #match-address").attr({
      "data-address": address,
      "data-lat": latitude,
      "data-lng": longitude
    });

    $(".text-not-match").addClass("hide");
    $(".confirm-text").removeClass("hide");
    $(".confirm-address-text").html($(this).data("address"));
    $(".addresses").addClass("hide");
    $(".confirm-mapsaddress").removeClass("hide");
    show_map("map-point", point, false);
  });

  $("body").on("click", "#not-match-address", function () {
    var latitude = $(this).data("lat"),
      longitude = $(this).data("lng");
    var point = { lat: latitude, lng: longitude };
    show_map("map-point", point, true);
    $(".confirm-text").addClass("hide");
    $(".text-not-match").removeClass("hide");
    var attributes = {
      address: addressToStr(address || {}),
      latitude: point.lat,
      longitude: point.lng,
      screen: pageName(),
      action: "notMatch"
    };
    trackEvent("address_confirmed", attributes);
  });

  $("body").on("click", "#match-address", function () {
    $(this).addClass("disabled");
    var data = {
      address: $(this).attr("data-address"),
      lat: $(this).attr("data-lat"),
      lng: $(this).attr("data-lng")
    };
    send_data_address(data);
    showDefaultLocalizationModal();
    var attributes = {
      address: addressToStr(address || {}),
      latitude: data.lat,
      longitude: data.lng,
      screen: pageName(),
      action: "match"
    };
    trackEvent("address_confirmed", attributes);
  });

  $("body").on("click", "#go-back", function () {
    $(".confirm-mapsaddress").addClass("hide");
    $(".addresses").removeClass("hide");
    $(".addresses .user_addresses").show();
  });

  $("#address-city .modal-dialog .modal-content").on(
    "click",
    ".mdl-dialog__actions .close",
    function (event) {
      $(".mapsaddress").hide();
      $(".btn-validate-address").removeClass("disabled");
      $(".select_address").show();
      $(".directaddress").show();
    }
  );

  // Compartir código
  $(".contlink .btn-shared-merqueo").on("click", function (e) {
    if ($(this).hasClass("view")) {
      e.preventDefault();
      $(".inputlink").css("display", "none");
      $(".contred").css("display", "inline-block");
      $(this).css("opacity", "0.8");
      $(this).removeClass("view");
    } else {
      e.preventDefault();
      $(".contred").css("display", "none");
      $(".inputlink").css("display", "block");
      $(this).css("opacity", "0.3");
      $(this).addClass("view");
    }
  });

  $("body").on("click", "#accept-changes", function () {
    $("#modal-product-change").modal("hide");
    var data = getConfirmData();
    data.action = "continue";

    trackEvent("unavailable_products", data, confirmChange);
  });

  $("#modal-product-change").on("hidden.bs.modal", function () {
    var data = getConfirmData();
    data.action = "cancel";

    trackEvent("unavailable_products", data);
  });

  $("body").on("click", "#address-update-button", function () {
    showAddressesForm();
  });

  $("body").on("click", "#checkout-button", function (event) {
    var hasCoverage = $("#address-city").data("coverage");
    if (!hasCoverage) {
      event.preventDefault();
      showAddressesForm();
    }
  });
});

// Buscador
function buscar(e) {
  var $this = $(e.currentTarget);
  var str = $this.val();

  if (str.length > 3) {
    url = web_url + "/buscador/" + store_id;

    var data = {
      source: "web",
      store: window.store_name,
      query: str
    };

    if (window.department_name) {
      data.department = window.department_name;
    }

    if (window.shelf_name) {
      data.shelf = window.shelf_name;
    }

    $.ajax({
      url: url,
      data: { q: str },
      type: "GET",
      dataType: "html",
      success: function (response) {
        if (response.length) {
          var $html = $(response);
          var dataarr = [];
          $html.find("h1").css("color", "red");
          var arraid = $(".cart-data")
            .children(".contenscroll")
            .children(".contentstoredata")
            .children(".contentproductcart")
            .children("[data-id]");
          $.each(arraid, function (key, value) {
            var datahtml = $(value);
            var itemid = datahtml
              .find(".sd-ser-no")
              .children(".items_number_ta")
              .children("input")
              .attr("id");
            itemid = itemid.replace("product-", "");
            dataarr.push(itemid);
          });
          $.each(dataarr, function (key, value) {
            htmlsed = $html
              .children(".col-xs-12")
              .children(".col-xs-4")
              .find("div[data-id='" + value + "']")
              .html();
            if (htmlsed != undefined || htmlsed != "undefined") {
              var varact = $("#product-" + value).val();

              if ($.warning == 1 && localStorage.warning == 1) {
                $html
                  .children(".col-xs-12")
                  .children(".col-xs-4")
                  .find("div[data-id='" + value + "']")
                  .css("display", "none");
                $html
                  .children(".col-xs-12")
                  .children(".col-xs-4")
                  .find("div[data-id='" + value + "']")
                  .parent()
                  .children(".cont_search_cart")
                  .css("display", "block");
                $html
                  .children(".col-xs-12")
                  .children(".col-xs-4")
                  .find("div[data-id='" + value + "']")
                  .parent()
                  .children(".cont_search_cart")
                  .children(".dataactitem")
                  .html(varact);
                $html
                  .children(".col-xs-12")
                  .children(".col-xs-4")
                  .find("div[data-id='" + value + "']")
                  .parent()
                  .children(".cont_search_cart")
                  .children("div")
                  .css("display", "inline-block");
              } else {
                if ($.warning == 0) {
                  $html
                    .children(".col-xs-12")
                    .children(".col-xs-4")
                    .find("div[data-id='" + value + "']")
                    .css("display", "none");
                  $html
                    .children(".col-xs-12")
                    .children(".col-xs-4")
                    .find("div[data-id='" + value + "']")
                    .parent()
                    .children(".cont_search_cart")
                    .css("display", "block");
                  $html
                    .children(".col-xs-12")
                    .children(".col-xs-4")
                    .find("div[data-id='" + value + "']")
                    .parent()
                    .children(".cont_search_cart")
                    .children(".dataactitem")
                    .html(varact);
                  $html
                    .children(".col-xs-12")
                    .children(".col-xs-4")
                    .find("div[data-id='" + value + "']")
                    .parent()
                    .children(".cont_search_cart")
                    .children("div")
                    .css("display", "inline-block");
                }
              }
            }
          });

          $(".autocomplete").html($html);
          $(".navbar-form .input-group").addClass("autocompleting");
          $(".search-product-image").elevateZoom({
            borderSize: 2,
            zoomWindowHeight: 300,
            zoomWindowWidth: 300
          });
        } else {
          $(".autocomplete").html("");
          $(".navbar-form .input-group").removeClass("autocompleting");
        }
      }
    });
  }
}

//cargar tiendas en select para agregar producto personalizado
function load_stores_cart() {
  stores = {};
  $("#product-store")
    .find("option")
    .remove();
  $(".side-cart-top")
    .find(".store-cart")
    .each(function () {
      if (!$(this).data("is-storage"))
        stores[$(this).data("id")] = $(this).html();
    });
  if (stores[store_id] == undefined) {
    stores[store_id] = store_name;
  }
  $.each(stores, function (key, val) {
    if (store_id == key)
      $("#product-store").append(
        '<option value="' + key + '" selected="selected">' + val + "</option>"
      );
    else
      $("#product-store").append(
        '<option value="' + key + '">' + val + "</option>"
      );
  });
}

function get_list(list_id, store_id) {
  $(document).ready(function () {
    $.ajax({
      url: web_url + "/list/" + list_id + "?store_id=" + store_id,
      type: "get",
      async: true,
      success: function (response) {
        if (response) {
          $("#recipe-detail-modal-content").html(response);
          $("#recipe-detail-modal").modal("show");
        } else {
          console.log("error response");
        }
      }
    });
  });
}

function get_cart() {
  $(document).ready(function () {
    var cart_id = cart_id || 0;
    url = web_url + "/user/cart?cart_id=" + cart_id;
    if ($(".delivery_time").length && $(".delivery_time").val() != "")
      url +=
        "&delivery_day=" +
        $(".delivery_day").val() +
        "&delivery_window_id=" +
        $(".delivery_time").val();

    var anonymousId = 0; //analytics.user().anonymousId();

    $.ajax({
      url: url + "&anonymousId=" + anonymousId,
      type: "get",
      async: true,
      success: function (response) {
        if (response.status) {
          $(".cart-data").html(response.html);
          var total_quantity = 0;
          $(".sd-ser-no input").each(function (val) {
            total_quantity += parseInt($(this).val());
          });

          $(".in-cart").removeClass("in-cart"); // Cleans any in case of removing from cart
          $(".sd-cart-pro-li").each(function () {
            var id = $(this).attr("data-id");

            $(".product-" + id).addClass("in-cart");
            $(".product-" + id + " .count").text(
              $(this)
                .find("input")
                .val()
            );
          });

          if ($(".product-view").length) {
            showProductAdded();
          }

          $(".cart-total-quantity").text(total_quantity);
          $(".cart-total-amount").text(
            response.cart.total_amount.formatMoney()
          );
          $(window).trigger("resize");

          if (response.credit_available == 0) {
            $(".user-credit").html("");
          }
          if (
            response.free_delivery_days == 0 &&
            response.free_delivery_next_order == 0
          ) {
            $(".free-delivery-days").html("");
          }

          is_minimum_reached = [];
          for (var i in response.cart.stores) {
            var minimum_reached =
              response.cart.stores[i].is_minimum_reached === 1;

            is_minimum_reached.push({
              is_minimum_reached: minimum_reached,
              sub_total: response.cart.stores[i].sub_total,
              name: response.cart.stores[i].name
            });
          }

          if (window.location.pathname == "/checkout") {
            // Mostrar productos sugeridos
            if ($(".suggested-product").length) {
              $(".suggested-product").show();
              var timeStampInMs = Date.now();

              var checked = false;
              $(window).on("scroll", function () {
                var $suggested_product = $(".suggested-product");
                if (isElementIntoView($suggested_product) && !checked) {
                  checked = true;
                  trackEvent("suggested_product_viewed", {
                    name: $suggested_product.data("name"),
                    storeProductId: $suggested_product.data("id")
                  });
                }
              });

              $(window).trigger("scroll");
            }
          }
        } else {
          //alert(response.error);
        }
      }
    });
  });
}

function addItem(callback, screen) {
  var tmp_minimum_reached = is_minimum_reached.length
    ? is_minimum_reached[0].is_minimum_reached
    : false;
  $(document).ready(function () {
    var deliveryAmount = parseInt($(".delivery-amount").data("amount"));
    var referer = pageName();
    quantity = $.cantidad - $.olq;
    url =
      web_url +
      "/user/cart?cart_id=" +
      cart_id +
      "&store_product_id=" +
      $.idstoreproduct +
      "&quantity=" +
      quantity +
      "&add_as_full_price=" +
      $.add_as_full_price +
      "&referer=" +
      referer;
    quantity = $.cantidad - $.olq;
    if ($(".delivery-day").length)
      url += "&delivery_day=" + $(".delivery-day").val();
    $.ajax({
      url: url,
      type: "get",
      async: true,
      success: function (response) {
        if (callback && callback instanceof Function) {
          callback();
        }
        if (response.status) {
          var scroll_t = $("#cart-desc .cart-data .contenscroll").scrollTop();
          $(".cart-data").html(response.html);

          var total_quantity = 0;
          $(".sd-ser-no input").each(function (val) {
            total_quantity += parseInt($(this).val());
          });

          $(".in-cart").removeClass("in-cart"); // Cleans any in case of removing from cart

          $(".sd-cart-pro-li").each(function () {
            var id = $(this).attr("data-id");

            $(".product-" + id).addClass("in-cart");
            $(".product-" + id + " .count").text(
              $(this)
                .find("input")
                .val()
            );
          });

          if ($(".product-view").length) {
            showProductAdded();
          }
          $(".cart-total-quantity").text(total_quantity);
          $(".cart-total-amount").text(
            response.cart.total_amount.formatMoney()
          );
          //console.log(total_amount);
          $(window).trigger("resize");

          if (response.credit_available == 0) {
            $(".alert-info").remove();
            $(".user-credit").html("");
          }

          $("#cart-desc .contenscroll").scrollTop(scroll_t);

          is_minimum_reached = [];
          for (var i in response.cart.stores) {
            var minimum_reached =
              response.cart.stores[i].is_minimum_reached === 1;

            is_minimum_reached.push({
              is_minimum_reached: minimum_reached,
              sub_total: response.cart.stores[i].sub_total,
              name: response.cart.stores[i].name
            });

            if (minimum_reached && tmp_minimum_reached) {
              continue;
            }

            if (minimum_reached || tmp_minimum_reached) {
              // Track Minimum Order Reached event
              trackEvent("minimum_order", {
                reached: minimum_reached ? 1 : 0,
                ticket: response.cart.total_amount
              });
            }
          }

          // Evento
          if (response.event) {
            // Track Product Added/Removed event
            var props = response.event.props;
            props.screen = screen || pageName();
            trackEvent(response.event.name, props);
          }

          // si viene error aunque el status sea true
          if (typeof response.error !== "undefined") {
            $("#item-act-" + $.idstoreproduct)
              .parent()
              .css("display", "none");
            $("#item-act-" + $.idstoreproduct)
              .parent()
              .parent()
              .children(".cont_search_item")
              .css("display", "block");

            alert(response.error);
          }
        } else {
          if (typeof response.qty !== "undefined") {
            $("#product-" + $.idstoreproduct).val(response.qty);
          }
          if (typeof response.add_as_full_price !== "undefined") {
            //agregar producto con precio full
            if (confirm(response.error)) {
              $.add_as_full_price = 1;
              addItem(null, screen);
            }
          } else alert(response.error);
        }

        var product_val = $("#product-" + $.idstoreproduct).val();

        $("#item-act-" + $.idstoreproduct).html(product_val);
        $.idstoreproduct = null;
        var deliveryAmountTmp = parseInt($(".delivery-amount").data("amount"));
        if (deliveryAmount !== deliveryAmountTmp) {
          trackEvent("free_delivery", {
            reached: deliveryAmountTmp === 0,
            ticket: total_amount
          });
        }
      }
    });
  });
}

function modify_cart(
  store_product_id,
  warning,
  quantity,
  old_quantity,
  referrer,
  event,
  callback
) {
  $.cantidad = quantity;
  $.idstoreproduct = store_product_id;
  $.warning = warning;
  $.olq = old_quantity;
  $.add_as_full_price = 0;
  $.referrer = referrer;

  if (!localStorage.warning) localStorage.warning = "";

  var _event = event || window.event;
  $.this = null;

  if (_event) {
    $.this = $(_event.currentTarget);
  }

  if ($("#address-city").data("coverage") == 0) {
    showAddressesForm();
  } else {
    if ($.warning == 1 && !localStorage.warning)
      $("#age-validation").modal("show");
    else {
      if ($.warning == 0) addItem(callback, referrer);
      else {
        if (localStorage.warning && $.warning == 1) addItem(callback, referrer);
      }
    }
  }
}

function showAddressesForm() {
  trackEvent("address_input_viewed", { screen: pageName() });
  $(".errorcover").hide();
  $(".mapsaddress").hide();
  $(".directaddress").show();
  $(".user_addresses").show();
  $(".address").removeClass("btn-primary");
  $(".btn-validate-address").val("Validar");

  $(".addresses").addClass("hide");
  $(".confirm-mapsaddress").addClass("hide");
  $(".form-has-errors").hide();
  $(".select_address").show();

  if ($(".addressnew").length) $(".directaddress").hide();

  $("#address-city").modal("toggle");

  $(".addressnew").on("click", function () {
    $(".user_addresses").hide();
    $(".addressnewform").css("display", "inline-block");
    $(".directaddress").css("display", "inline-block");
  });

  $(".addressnewform").on("click", function () {
    $(".user_addresses").show();
    $(".addressnewform").hide();
    $(".directaddress").hide();
    $(".mapsaddress").hide();
  });
}

function addressToStr(data) {
  return data.dir1 + " " + data.dir2 + " " + data.dir3 + " " + data.dir4;
}

function validateCartChange() {
  $("#address-city").modal("hide");
  $.ajax({ url: "/user/cart/changed-products" })
    .done(function (response) {
      if (!response.result) {
        reloadCurrentPage();
        return;
      }

      if (response.result.address_text) {
        $("#address-description b").text(response.result.address_text);
      }

      $("#address-city").data("coverage", 1);
      pendingAddedProduct = pendingAddedProduct || function () { };

      if (response.result.removed_products.length) {
        showConfirmUpdateCartModal(
          response.message,
          response.result.html_content
        );
        pendingAddedProduct = null;
      } else if (response.result.warehouse_changed) {
        enableLoader();
        validateProductAdd(reloadCurrentPage);
        pendingAddedProduct = null;
      } else {
        pendingAddedProduct();
        pendingAddedProduct = null;
      }
    })
    .fail(console.error);
}

/**
 * @param {String} message
 * @param {String} htmlContent
 */
function showConfirmUpdateCartModal(message, htmlContent) {
  var modal = $("#modal-product-change");
  modal.find(".modal-title").html(message);
  modal.find(".modal-body").html(htmlContent);
  modal.modal("show");
}

function confirmChange() {
  enableLoader();
  $.ajax({ url: "/user/cart/alter-products" })
    .done(function () {
      showDefaultLocalizationModal();
      if (pendingAddedProduct) {
        reloadCurrentPage();
      } else {
        validateProductAdd(reloadCurrentPage);
      }
    })
    .fail(console.error);
}

function validateProductAdd(callback) {
  if (!$.idstoreproduct) {
    callback && callback instanceof Function ? callback() : null;
    return;
  }

  $.ajax({ url: "/store/product/product-exists/" + $.idstoreproduct })
    .done(function (response) {
      if (response.status) {
        addItem(callback);
      } else if (callback instanceof Function) {
        callback();
      }
    })
    .fail(console.error);
}

function showDefaultLocalizationModal() {
  $(".btn-validate-address").removeClass("disabled");
  $(".confirm-mapsaddress").addClass("hide");
  $(".directaddress").hide();
  $(".select_address").show();
  $(".user_addresses").show();
}

function reloadCurrentPage() {
  // TODO recargar la página de forma dinamica.
  var currentReference = window.location.href;
  location.reload();
}

function enableCommonSections(htmlContent) {
  var content = $(htmlContent);
  var body = $("body");

  var sideBarSelector = ".nav.nav-pills.nav-stacked";
  var sideBar = content.find(sideBarSelector);
  body.find(sideBarSelector).html(sideBar.html());

  var cartSelector = ".nav.nav-pills.nav-stacked";
  var cart = content.find(cartSelector);
  body.find(cartSelector).html(cart.html());

  var productsSelector = ".nav.nav-pills.nav-stacked";
  var products = content.find(productsSelector);
  body.find(productsSelector).html(products.html() || "<div></div>");
}

function enableLoader() {
  var loader = $(".main-loader");
  loader.show();
}

function hideLoader() {
  $(".main-loader").hide();
}

/**
 * VALIDAR COBERTURA CON DIRECCION
 */
function send_data_address(data, response) {
  var address = (data ? data.address || data : null) || address || {};
  city_id = $("#city_id").val();
  surrounded_city_id = $("#surrounded_city_id").length
    ? $("#surrounded_city_id").val()
    : 0;

  $(".btn-validate-address")
    .addClass("disabled")
    .html("Validando dirección...");

  $.ajax({
    url: web_url + "/user/geoaddress",
    data: {
      dir: JSON.stringify(data),
      store_id: store_id,
      city_id: city_id,
      surrounded_city_id: surrounded_city_id
    },
    type: "post",
    async: true,
    success: function (response) {
      var data = data || {};
      var attributes = {
        address: typeof address === "string" ? address : addressToStr(address)
      };

      $(".btn-validate-address")
        .removeClass("disabled")
        .html("Validar");
      if (response.status) {
        var address_quantity = response.result.addresses
          ? response.result.addresses.length
          : 1;
        if (address_quantity > 1) {
          var html = "";
          $(".addresses .addresslist").html("");
          $.each(response.result.addresses, function (index, val) {
            html +=
              '<div class="bg-info"><a class="btn btn-default btn-sm pull-right confirm-address" data-address="' +
              val.address +
              '" data-lat="' +
              val.latitude +
              '" data-lng="' +
              val.longitude +
              '" href="javascript:;"> > </a>';
            html +=
              '<span class="glyphicon glyphicon-map-marker"></span>&nbsp;' +
              val.address_html +
              "</div><hr>";
          });
          $(".addresses .addresslist").append(html);
          $(".select_address").hide();
          $(".addresses").removeClass("hide");
        } else {
          attributes.coverage = 1;
          attributes.latitude = data.lat;
          attributes.longitude = data.lng;
          trackEvent("address_validated", attributes);
          pendingAddedProduct = function (callback) {
            if ($.idstoreproduct) {
              modify_cart(
                $.idstoreproduct,
                $.warning,
                $.cantidad,
                $.olq,
                null,
                null,
                callback
              );
            } else if (callback instanceof Function) {
              callback();
            }
          };
          validateCartChange();
        }
      } else {
        if (response.error == "invalid_address") {
          $(".form-has-errors")
            .html("Dirección invalida.")
            .show();
          return;
        }

        $(".user_addresses").hide();
        $(".directaddress").hide();

        // Track Address Invalidated event
        var eventProperties = {
          source: "web"
        };

        if (data.hasOwnProperty("dir1")) {
          eventProperties.address = addressToStr(data);
        } else if (data.hasOwnProperty("lat")) {
          eventProperties.location = data.lat + "," + data.lng;
        }

        attributes.coverage = 0;
        attributes.latitude = data.lat;
        attributes.longitude = data.lng;
        trackEvent("address_validated", attributes);

        if (response.result == "no_coverage") {
          $(".confirm-mapsaddress").addClass("hide");
          $(".select_address").show();
          $(".mapsaddress").hide();
          $(".errorcover").show();
          $(".errorcover .red").html(response.message);
        } else {
          $(".mapsaddress").show();
          if (response.data) {
            point = {
              lat: parseFloat(response.data.lat),
              lng: parseFloat(response.data.lng)
            };
            show_map("map", point, true);
          } else show_map();
        }
      }

      $(".match-address").removeClass("disabled");
      $(".btn-validate-address").removeClass("disabled");
      $("#address-city .select_address .loading_user").css("display", "none");
    }
  });
}

function show_map(mapa = false, point = false, draggable = true) {
  document.getElementById("map").html = "";

  if (point === false) point = { lat: city_lat, lng: city_lng };

  if (mapa === false) mapa = "map";

  trackEvent("address_map_viewed", {
    address: addressToStr(address || {}),
    latitude: point.lat,
    longitude: point.lng
  });

  var map = new google.maps.Map(document.getElementById(mapa), {
    zoom: 14,
    center: point
  });
  //var image = web_url + '/assets/img/markermerqueo.png';
  marker = new google.maps.Marker({
    map: map,
    draggable: draggable,
    //icon: image,
    animation: google.maps.Animation.DROP,
    position: point
  });
  geocoder = new google.maps.Geocoder();
  google.maps.event.addListener(marker, "dragend", function () {
    maker_animate(geocoder, map, marker);
    var data = {
      address: $("#not-match-address").attr("data-address"),
      lat: marker.getPosition().lat(),
      lng: marker.getPosition().lng()
    };
    send_data_address(data);
  });

  $(".user_addresses").hide();
  $(".directaddress").hide();
  $(".select_address").hide();

  if (mapa === false) $(".mapsaddress").show();
}

function maker_animate(geocoder, map, marker) {
  if (marker.getAnimation() !== null) marker.setAnimation(null);
  else marker.setAnimation(google.maps.Animation.BOUNCE);
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById("address").value;
  geocoder.geocode({ address: address }, function (results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      //alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

// Set up event tracking
function setUpEvents() {
  $(document).on("click", "[data-banner]", function (e) {
    var $banner = $(e.currentTarget);
    // Track banner_clicked event
    trackEvent("banner_clicked", {
      title: $banner.data("title") || null,
      type: $banner.data("type") || null,
      position: $banner.data("position") || null,
      name: $banner.data("name") || null,
      shelfId: $banner.data("shelf-id") || null,
      storeProductId: $banner.data("product-id") || null,
      bannerId: $banner.data("banner-id") || null
    });
  });
}

// Util
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function isElementIntoView(element) {
  if ($(element).is(":hidden")) {
    return false;
  }

  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(element).offset().top;
  var elemBottom = elemTop + $(element).height();

  return elemBottom <= docViewBottom && elemTop >= docViewTop;
}

function getReferrer() {
  var pageMapping = [
    {
      pattern: /\/.+\/domicilios-[^\/]+\/[^\/]+\/[^\/]+\/[^\/]+$/,
      name: "product"
    },
    {
      pattern: /\/.+\/domicilios-[^\/]+\/[^\/]+\/[^\/]+$/,
      name: "shelf"
    },
    {
      pattern: /\/.+\/domicilios-[^\/]+\/[^\/]+$/,
      name: "department"
    },
    {
      pattern: /\/.+\/domicilios-[^\/]+$/,
      name: "store"
    }
  ];

  var path = window.location.pathname;

  for (var key in pageMapping) {
    var page = pageMapping[key];

    if (path.match(page.pattern)) {
      return page.name;
    }
  }
}

function getConfirmData() {
  var totalProducts = $("#removed-products-list .product-name");
  var totalItems = 0;
  $("#removed-products-list .product-quantity").each(function (index, element) {
    totalItems += parseInt($(element).text());
  });

  return {
    size: totalProducts.length,
    items: totalItems
  };
}

function show_modal(action, reference) {
  $("#modal-" + action).modal("show");
}

String.prototype.replaceArray = function (find, replace) {
  var replaceString = this;
  for (var i = 0; i < find.length; i++) {
    // global replacement
    var pos = replaceString.indexOf(find[i]);
    while (pos > -1) {
      replaceString = replaceString.replace(find[i], replace[i]);
      pos = replaceString.indexOf(find[i]);
    }
  }
  return replaceString;
};

// Useful functions:
Number.prototype.formatMoney = function (c, d, t) {
  var n = this,
    c = isNaN((c = Math.abs(c))) ? 0 : c,
    d = d === undefined ? "." : d,
    t = t === undefined ? "." : t,
    s = n < 0 ? "-" : "",
    i = parseInt((n = Math.abs(+n || 0).toFixed(c))) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
  return (
    s +
    (j ? i.substr(0, j) + t : "") +
    i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
    (c
      ? d +
      Math.abs(n - i)
        .toFixed(c)
        .slice(2)
      : "")
  );
};
