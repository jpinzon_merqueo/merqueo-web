const assert = require('assert');

$ = require('../jquery.min')
const RandomBar = require('../bar-new-web');

global.window = {}
require('mock-local-storage');
window.localStorage = global.localStorage


it('Is equal random number 0.2 ', function(done){
    window.localStorage.clear();
    let randomNumber = 0.2;
    let rand = new RandomBar(randomNumber);
    assert.strictEqual(rand.randomizer(), randomNumber);
    done();
});
    

context('Save and get localStorage', function(){

    it('nav bar ', function(done){
        window.localStorage.clear();
        let rand = new RandomBar();
        let navBarValue = true;
        rand.setNavBar(navBarValue);
        assert.strictEqual(rand.getNavBar(), navBarValue);
        done();
    });

    it('Version assigned ', function(done){
        window.localStorage.clear();
        let rand = new RandomBar();
        let currentVersion = 2;
        rand.setVersionAssigned(currentVersion);
        assert.strictEqual(rand.getVersionAssigned(), currentVersion);
        done();
    });

});

context("Release ", function(){
    it('Should release user ', function(done){
        window.localStorage.clear();
        let percentToRelease = 20;
        let randomNumber = 0.1;
        let currentVersion = 1;
    
        let rand = new RandomBar(randomNumber, percentToRelease, currentVersion, true);
        
        rand.initStorage();
        
        assert.strictEqual(rand.validateAndRelease(), true);
        done();
    });

    it('Not Should release user ', function(done){
        window.localStorage.clear();
        let percentToRelease = 20;
        let randomNumber = 0.5;
        let currentVersion = 1;
        
        let rand = new RandomBar(randomNumber, percentToRelease, currentVersion, true);
        
        rand.initStorage();
        
        assert.strictEqual(rand.validateAndRelease(), false);
        done();
    });

    it('Should release user with new version', function(done){
        window.localStorage.clear();

        let percentToReleaseV1 = 20;
        let randomNumberV1 = 0.5;
        let currentVersionV1 = 1;
        
        let randV1 = new RandomBar(randomNumberV1, percentToReleaseV1, currentVersionV1, true);
        randV1.initStorage();
        assert.strictEqual(randV1.validateAndRelease(), false);


        let percentToReleaseV2 = 20;
        let randomNumberV2 = 0.15;
        let currentVersionV2 = 2;

        let randV2 = new RandomBar(randomNumberV2, percentToReleaseV2, currentVersionV2, true);
        randV2.initStorage();
        assert.strictEqual(randV2.validateAndRelease(), true);

        done();
    });

    it('Not Should release user with new version', function(done){
        window.localStorage.clear();

        let percentToReleaseV1 = 20;
        let randomNumberV1 = 0.5;
        let currentVersionV1 = 1;
        
        let randV1 = new RandomBar(randomNumberV1, percentToReleaseV1, currentVersionV1, true);
        randV1.initStorage();
        assert.strictEqual(randV1.validateAndRelease(), false);


        let percentToReleaseV2 = 20;
        let randomNumberV2 = 0.5;
        let currentVersionV2 = 2;

        let randV2 = new RandomBar(randomNumberV2, percentToReleaseV2, currentVersionV2, true);
        randV2.initStorage();
        assert.strictEqual(randV2.validateAndRelease(), false);

        done();
    });

    it('Not Should release user with same version', function(done){
        window.localStorage.clear();

        let percentToReleaseV1 = 20;
        let randomNumberV1 = 0.13;
        let currentVersionV1 = 1;
        
        let randV1 = new RandomBar(randomNumberV1, percentToReleaseV1, currentVersionV1, true);
        randV1.initStorage();
        assert.strictEqual(randV1.validateAndRelease(), true);


        let percentToReleaseV2 = 20;
        let randomNumberV2 = 0.1;
        let currentVersionV2 = 1;

        let randV2 = new RandomBar(randomNumberV2, percentToReleaseV2, currentVersionV2, true);
        randV2.initStorage();
        assert.notStrictEqual(randV2.validateAndRelease(), true);

        done();
    });
});




