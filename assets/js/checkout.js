
$(document).ready(function() {

	//ingresar nueva direccion
	$('body').on('click', '.address-toggle', function() {
		$('.user-address, .new-address').toggle();
		if($('.new-address').is(':visible')) {
			$('.address').removeClass('btn-primary').addClass('btn-default');
			$('input[name="address_id"]').val('');
		}
	});

	//utilizar una direccion existente
	$('body').on('click', '.address', function(e) {
		$('.address').addClass('btn-default').removeClass('btn-primary');
		if ($(this).hasClass('btn-default')){
			$(this).addClass('btn-primary');
			$('input[name="address_id"]').val($(this).attr('data-id'));
		}else{
			$('input[name="address_id"]').val('');
		}
	});

	//ingresar nueva tarjeta
	$('body').on('click', '.credit-card-toggle', function() {
		$('.add-document-number-cc').hide();
		$('.user-credit-card, .new-credit-card').toggle();
		if($('.new-credit-card').is(':visible')) {
			$('.credit-card').removeClass('btn-primary').addClass('btn-default');
			$('input[name="credit_card_id"]').val('');
		}
	});

	//utilizar una tarjeta existente
	$('body').on('click', '.credit-card', function(e) {
		$('.add-document-number-cc').hide();
		if ($(this).hasClass('btn-default')){
			$('.credit-card').removeClass('btn-primary').addClass('btn-default');
			$(this).addClass('btn-primary');
			$('input[name="credit_card_id"]').val($(this).attr('data-id'));
			if ($(this).parent().find('.add-document-number-cc').length)
                $(this).parent().find('.add-document-number-cc').show();
		}else{
			$(this).removeClass('btn-primary').addClass('btn-default');
			$('input[name="credit_card_id"]').val('');
		}
	});

	//redimir cupon
	$('body').on('click', '.btn-show-coupon button', function(){
		$(this).parent().hide();
		$('.coupon').show();
	});

	$('body').on('click', '.btn-hide-coupon', function(){
		$('.btn-show-coupon').show();
		$('.coupon').hide();
	});

	$('body').on('click', '.btn-coupon', function() {
		var error = false;
		var couponCode = $('input[name="coupon_code"]').val();

		$('.coupon-success').hide();
		if (couponCode != '') {
			if($('.btn-coupon').hasClass('disabled'))
				return false;
			$('.btn-coupon').addClass('disabled');
			$('.loading-coupon').show();
			//validar cupon
			$.ajax({
				url: web_url + '/checkout/coupon',
				type: 'post',
				data: { coupon_code: couponCode, device_id: Leanplum._deviceId, os: getOs()},
				dataType: 'json',
				success: function(response) {
					$('.loading-coupon').hide();
					$('.btn-coupon').removeClass('disabled');
					if (response.status){
						$('.coupon-success').html(response.message).show();
						$('.checkout-coupon').remove();
						$('.btn-show-coupon').before(response.html);
						$('input[name="coupon_code"]').val('');
						get_cart();
					}else{
						$('.coupon-error').html(response.message).show();
						$('input[name="coupon_code"]').val('');
					}
				}, error: function() {
					$('.loading-coupon').hide();
					$('.btn-coupon').removeClass('disabled');
					$('.coupon-error').html('Ocurrió un error al validar el cupón').show();
					$('input[name="coupon_code"]').val('');
				}
			});
		}else error = 'Digita el código del cupón';
		if (error){
			$('.coupon-error').html(error).show();
			$(this).parent().addClass('has-error');
		}else{
			$('.coupon-error').hide();
			$(this).parent().removeClass('has-error');
		}
	});

	$('body').on('click', '.btn-remove-coupon', function() {
		var error = false;
		$('.coupon-success').hide();
		$('.btn-coupon').addClass('disabled');
		$('.loading-coupon').show();
		//eliminar cupon
		$.ajax({
			url: web_url + '/checkout/remove-coupon',
			type: 'get',
			dataType: 'json',
			success: function(response) {
				$('.loading-coupon').hide();
				$('.btn-coupon').removeClass('disabled');
				if (response.status){
					$('.checkout-coupon').remove();
					get_cart();
				}else{
					$('.coupon-error').html(response.message).show();
				}
			}, error: function() {
				$('.loading-coupon').hide();
				$('.btn-coupon').removeClass('disabled');
				$('.coupon-error').html('Ocurrió un error al eliminar el cupón').show();
			}
		});

		if (error){
			$('.coupon-error').html(error).show();
			$(this).parent().addClass('has-error');
		}else{
			$('.coupon-error').hide();
			$(this).parent().removeClass('has-error');
		}
	});

	//dia de entrega
	if ($(".blo_promo").length){
		$('body').on('change', '.delivery_day', function() {
			get_cart();
		});
	}

	//cargar costo de domicilio por franja horaria
	$('body').on('change', '.delivery_time', function() {
		get_cart();
	});

	//metodo de pago
	$('*[name="payment_method"]').on('change', function() {
		if($(this).val() == 'Tarjeta de crédito') {
			$('.cc').addClass('required').val('');
			$('#address_further_cc').removeClass('required');
			$('.form_cc').show();
		} else{
			$('.form_cc').hide();
			$('.cc').removeClass('required').val('');
			$('#expiration_cc').val('');
			$('#installments_cc').val('');
		}
		/*if($(this).val() == 'Débito - PSE') {
			$('.pse').addClass('required').val('');
			$('.form_pse').show();
			if($('.btn-payu-go-transaction').length > 0){
				$('.btn-payu-go-transaction').html('Ir al banco.');
			}
		} else{º
			$('.form_pse').hide();
			$('.pse').removeClass('required').val('');
			if($('.btn-payu-go-transaction').length > 0){
				$('.btn-payu-go-transaction').html('Guardar.');
			}
		}*/
		if($(this).val() == 'Débito - PSE') {
			$('.pse_field').addClass('required').val('');
			$('.form_pse').show();
			if($('.btn-payu-go-transaction').length > 0){
				$('.btn-payu-go-transaction').html('Ir al banco.');
			}
		} else{
			$('.form_pse').hide();
			$('.pse_field').removeClass('required').val('');
			if($('.btn-payu-go-transaction').length > 0){
				$('.btn-payu-go-transaction').html('Guardar.');
			}
		}
	});

	$('*[name="address_name"]').on('change', function() {
		if($(this).val() != 'Otro') {
			$('*[name="other_name"]').parent().hide();
		} else $('*[name="other_name"]').parent().show();
	});

	$('#number_cc').on('blur', function() {
		card_type = get_credit_card_type($(this).val());
		$('.help-block').remove();
		/*if (card_type == 'Mastercard'){
			bin = $(this).val();
			bin = bin.substr(0, 6);
			if (bin != '528209')
				$(this).after('<p class="help-block" style="color: green">Pagando con tarjeta de crédito Mastercard el costo del domicilio es gratis para pedidos mayores a $110.000.</p>');
			else $(this).after('<p class="help-block" style="color: green">Pagando con tarjeta de crédito CMR Falabella y utilizando el cupón cfallabela30 recibe $30.000 pesos de descuento para pedidos mayores a $60.000 y domicilio gratis para pedidos mayores a $110.000.</p>');
		}*/
		$('#installments_cc option').remove();
		if (card_type == 'Codensa'){
			installments = [1, 12, 18, 24, 36, 48];
			$.each(installments, function(index, value) {
				$('#installments_cc').append($("<option></option>").attr('value', value).text(value));
			});
		}else{
			for (i = 1; i <= 36; i++){
				$('#installments_cc').append($("<option></option>").attr('value', i).text(i));
			}
		}
	});

    function compare(a,b) {
        if(a.text.indexOf("5") == 0){
            return -1;
        }

       if(a.text.indexOf("am") == 5 && a.text.indexOf("5") != 0 && b.text.indexOf("5") != 0){
           return -1;
       }

       if(b.text.indexOf("am") == 6){
            return 1;
       }

       return 0;
    };

	//dia y hora de entrega
	$('.delivery_day').on('change', function() {
		store_id = $(this).data('store-id');
		$('#delivery_time_' + store_id + ' option').remove();
		$('#delivery_time_' + store_id).append($('<option value="">Selecciona la hora</option>'));
		if ($(this).val() != '') {
		    timesAux = Object.values(delivery_times[store_id]['time'][$(this).val()]);
			times = timesAux.sort(compare);
			$.each(times, function(index, value) {
				$('#delivery_time_' + store_id).append($("<option></option>").attr('value', value.value).text(value.text));
			});
		}
	});

	//validar formulario
	$('form.order').on('submit', function(e) {
        if($(this).attr('allowsubmit')) {
            $(this).off().submit();
            return true;
        }

		if($('button[type="submit"]', form).hasClass('disabled')) {
			return false;
		}
		$('button[type="submit"]', this).addClass('disabled');

		e.preventDefault();
		var errors = [];
		var form = this;

		//validar campos
		$('input[type!="hidden"], select', this).each(function() {
			if($(this).val().length == 0 && $(this).is(':visible') && $(this).hasClass('required')) {
				$(this).parent().addClass('has-error');
				if(errors.indexOf("Debes ingresar todos los campos") < 0) {
					errors.push("Debes ingresar todos los campos");
				}
			}else $(this).parent().removeClass('has-error');
		});

		if (!$('.new-address').is(':visible')){
			if ($('input[name="address_id"]', this).val() == ''){
				errors.push("Debes seleccionar la dirección");
			}
		}

		if($('input[name="password"]', this).length
		&& $('input[name="password"]', this).val() != '' && $('input[name="password"]', this).val().length < 6) {
			errors.push("La contraseña debe ser minimo de 6 caracteres");
			$('input[name="password"]').parent().addClass('has-error');
		}

		if ($('.new-credit-card').is(':visible')){
			if (!get_credit_card_type($('input[name="number_cc"]').val())){
				errors.push("El número de la tarjeta de crédito no es valido.");
				$('input[name="number_cc"]').parent().addClass('has-error');
			}
		}

		if (!$('.new-credit-card').is(':visible')){
			if ($('*[name="payment_method"]').val() == 'Tarjeta de crédito' && $('input[name="credit_card_id"]', this).val() == ''){
				errors.push("Debes seleccionar la tarjeta de crédito");
			}
		}

		if(!$('input[name="terms"]', this).is(':checked')) {
			errors.push("Debes aceptar los términos y condiciones");
		}

		if($('input[name="user_phone"]', this).length
        && $('input[name="user_phone"]', this).val() != '' && $('input[name="user_phone"]', this).val().length < 10) {
            errors.push("El número celular debe ser mínimo de 10 digitos");
            $('input[name="user_phone"]').parent().addClass('has-error');
        }

		if($('input[name="user_email"]', this).length && $('input[name="user_email"]').val() != '' && !validateEmail($('input[name="user_email"]').val())) {
			errors.push("El formato del email ingresado no es valido");
			$('input[name="user_email"]').parent().addClass('has-error');
		}

		if( $('input[name="invoice"]', this).is(':checked') ){
			if($('#type-invoice').val() == 'natural') {
                if ($('input[name="invoice_user_name"]').val() != '' && !/^\w+(\s\w+)*$/.test($('input[name="invoice_user_name"]').val())) {
                    errors.push("Nombre completo no es válido");
                    $('input[name="invoice_user_name"]').parent().addClass('has-error');
                }
                if ($('input[name="invoice_user_identity_number"]').val() != '' && !/^[1-9]\d*$/.test($('input[name="invoice_user_identity_number"]').val())) {
                    errors.push("Número de documento no es válido");
                    $('input[name="invoice_user_identity_number"]').parent().addClass('has-error');
                }
            }else{//company
				if(  $('input[name="invoice_company_name"]').val() != '' && !/^\w+(\s\w+)*$/.test($('input[name="invoice_company_name"]' ).val()) ){
					errors.push("Nombre de la empresa no es válido");
					$('input[name="invoice_company_name"]').parent().addClass('has-error');
				}
				if( $('input[name="invoice_company_nit"]').val() != '' && !/^[1-9]\d{8}$/.test($('input[name="invoice_user_identity_number"]').val())
					&& !($('input[name="invoice_company_nit"]').val() >= 800000000 && $('input[name="invoice_company_nit"]').val() <= 999999999)){
					errors.push("Formato de NIT debe ser de 9 digitos comprendidos entre 800000000 y 999999999");
					$('input[name="invoice_company_nit"]').parent().addClass('has-error');
				}
            }
		}

		if(errors.length) {
			var errors_list = '';
			for(var i in errors) {
				errors_list += '<li>' + errors[i] + '</li>';
			}
			$('.form-has-errors .error-list', this).html(errors_list);
			$('.form-has-errors', this).fadeIn();
			$('button[type="submit"]', this).removeClass('disabled');
			return false;
		}

		//deja pedir dado que es una dirección guardada
		$('.loading').show();
		$(this).attr('allowsubmit', '1').trigger('submit');


		return false;
	});

	/*var first_day_option = $('.delivery_day option').eq(1).val();
	$('.delivery_day').val(first_day_option).trigger('change');
	var first_time_option = $('.delivery_time option').eq(1).val();
	if ( $('input[name=credit_card_id]').length > 0 ) {
		var credit_card_option = $('select[name=payment_method] option').eq(1).val();
		$('select[name=payment_method]').val(credit_card_option).trigger('change');
		$('.credit-card.btn').trigger('click');
		$('select[name=installments_cc]').val(1);
	}else{
		var credit_card_option = $('select[name=payment_method] option').eq(2).val();
		$('select[name=payment_method]').val(credit_card_option).trigger('change');
	}*/

	if($('body .user-credit-card').is(':visible')){
		$('body .new-credit-card').hide();
		$('body .user-credit-card').show();
		var id =  $('body .user-credit-card').find('.credit-card:first-child').data('id');
		$('body .user-credit-card [name="credit_card_id"]').val(id);
		$('body .user-credit-card .credit-card').removeClass('btn-primary');
		$('body .user-credit-card .credit-card:eq(0)').removeClass('btn-default');
		$('body .user-credit-card .credit-card:eq(0)').addClass('btn-primary');
		$('body #installments_cc option:selected').removeAttr('selected');
		$('body #installments_cc').val(1);
		if ($('body .user-credit-card .credit-card:eq(0)').parent().find('.add-document-number-cc').length)
            $('body .user-credit-card .credit-card:eq(0)').parent().find('.add-document-number-cc').show();
	}

    //Check necesito factura
	$('body').on('change', '#invoice', function() {
		if(this.checked){
			var type_invoice = $('#type-invoice').val();
            if(type_invoice === 'natural'){
                $('#content-type-natural .form-control').addClass('required');
                $('#content-type-company .form-control').removeClass('required');
			}else{
                $('#content-type-company .form-control').addClass('required');
                $('#content-type-natural .form-control').removeClass('required');
			}
		}
        $(".content-invoice").toggle(this.checked);
    });

    //Tipo Persona/Empresa para datos de facturación
    $('body').on('change', '#type-invoice', function() {
        var type = $(this).val();
        if(type === 'natural'){
            $('#content-type-natural .form-control').addClass('required');
            $('#content-type-company .form-control').removeClass('required');
			$('#content-type-natural').show();
            $('#content-type-company').hide();
		}else{
            $('#content-type-company .form-control').addClass('required');
            $('#content-type-natural .form-control').removeClass('required');
            $('#content-type-company').show();
            $('#content-type-natural').hide();
		}
    });

});
