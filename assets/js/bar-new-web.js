
'use strict'
class RandomBar {
    constructor(randomizer, percentToRelease, currentVersion, testing = false) {
        this.randomizerNumber = randomizer;
        this.percentToRelease = percentToRelease;
        this.currentVersion = currentVersion;
        this.testing = testing;
    }

    initStorage() {
        if (this.getNavBar() == null) {
            this.setNavBar(null);
        }
    }

    isUserReleased() {
        return this.getNavBar();
    }

    randomizer() {
        let randomizerNumber = this.randomizerNumber;
        window.localStorage.setItem("number", randomizerNumber);
        return randomizerNumber
    }

    shouldUserRelease() {
        var maxPercent = 100;
        var valueByRelease = this.percentToRelease / maxPercent;
        return this.randomizer() <= valueByRelease;
    }

    setNavBar(active){
        window.localStorage.setItem("nav-bar", active);
    }

    getNavBar(){
        let randBarStorage = window.localStorage.getItem("nav-bar");
        return JSON.parse(randBarStorage);
    }

    setVersionAssigned(version){
        window.localStorage.setItem("version_asigned", version);
    }

    getVersionAssigned(){
        let versionAssignedStorage = window.localStorage.getItem("version_asigned");
        return JSON.parse(versionAssignedStorage);
    }

    validateAndRelease() {
        let inRelease = null;
        if(this.isUserReleased() == null || this.isOldVersionAssignedAndNotReleased() ){
            if (this.shouldUserRelease()) {
                inRelease = true;
            }else{
                inRelease = false;
            }
            this.setReleaseAndTrackEvent(inRelease);
        }
        return inRelease;
    }

    isOldVersionAssignedAndNotReleased(){
        if(
            this.currentVersion > this.getVersionAssigned() && 
            this.isUserReleased() == false)
        {
            return true;
        }else{
            return false;
        }
    }

    setReleaseAndTrackEvent(inRelease){
        this.setNavBar(inRelease);
        this.setVersionAssigned(this.currentVersion);
        if(!this.testing){
            trackEvent('show_bar_new', {
                release: inRelease,
                version: this.currentVersion,
                randNumber: this.randomizerNumber
            });
        } 
    }
    
    run(){
        this.initStorage();
        this.validateAndRelease();
    }
}


try {
    module.exports = RandomBar;
} catch (error) {
    
}

try {
    let percentToRelease = 100;
    let currentVersion = 2;

    var randBar = new RandomBar(Math.random(), percentToRelease, currentVersion);
    $().ready(function () {
        randBar.run();
    });
} catch (error) {
    
}