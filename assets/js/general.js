
jQuery(document).ready(function($){

	$('body').on('keyup', '.uppercase', function(){
		$(this).val($(this).val().toUpperCase());
	});
	$('body').on('change', '.uppercase', function(){
		$(this).val($(this).val().toUpperCase());
	});
	$('body').on('blur', '.uppercase', function(){
		$(this).val($(this).val().toUpperCase());
	});

	$('body').on('keyup', '.lowercase', function(){
		$(this).val($(this).val().toLowerCase());
	});
	$('body').on('change', '.lowercase', function(){
		$(this).val($(this).val().toLowerCase());
	});
	$('body').on('blur', '.lowercase', function(){
		$(this).val($(this).val().toLowerCase());
	});

	$('body').on('keypress', '.onlyNumbers', function(evt){
		var key = evt.keyCode ? evt.keyCode : evt.which;
		//console.log(key);
		return (key <= 27 || (key >= 48 && key <= 57) || key == 35 || key == 36 || (key >= 37 && key <= 40) || key == 46);
	});

	$('body').on('keypress', '.onlyLetters', function(evt){
		var key = evt.keyCode ? evt.keyCode : evt.which;
		return (key <= 32 || (key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key >= 128 || key == 35 || key == 36 || key == 37 || key == 39 || key == 46);
	});

	$('#newsletter-btn').click(function(){
		$('.form-nl .alert-success').hide();
		$('.form-nl .alert-danger').hide();
		if ($('#email-newsletter').val().length == 0){
			$('.form-nl .alert-danger').html('Por favor digita tu correo electrónico.').show();
	    }else{
	    	if (!validateEmail($('#email-newsletter').val())) {
	    		$('.form-nl .alert-danger').html('Por favor digita un formato de correo electrónico valido.').show();
	    		return false;
	    	}
	    	$.ajax({
				  method: 'POST',
				  url: web_url + '/newsletter',
				  data: { email: $('#email-newsletter').val() },
				  success: function(response){
				  	 if (response.status){
				  	 	$('.form-nl .alert-success').html('<p>' +response.message + '</p>').show();
				  	 	$('#email-newsletter').val('');
				  	 }else{
				  	 	$('.form-nl .alert-danger').html('<p>' + response.message + '</p>').show();
				  	 }
				  },
				  error: function(){
				  	 $('.form-nl .alert-danger').html('<p>' +response.message + '</p>').show();
				  }
			});
    	}
	});



	$(window).resize(function() {
		var carInitialButtonContainer = $('.cart-add-initial');
		carInitialButtonContainer.each(function(index, element) {
			var element = $(element);
			var columnContainer = element.closest('.product-column');
			if (!columnContainer.length) {
				return null;
			}

            var containerWidth = columnContainer.width();
            var elementWidth = element.width();
            var newleft = Math.round((((containerWidth / 2) * 100) / containerWidth)) - Math.round((((elementWidth / 2.1) * 100) / containerWidth));

            newleft = newleft + '%';
            element.css('left', newleft);
		});

      if(this.resizeTO) clearTimeout(this.resizeTO);
       this.resizeTO = setTimeout(function() {
          $(this).trigger('resizeEnd');
       }, 500);

      if( $('.contnt_block_store').is(":visible") ){
        $('.contnt_block_store').children('.dropdown-menu_list').children('.contnstore_list_change').children('.repeat').css("height", "auto");
      }
	});

	  $(window).bind("resizeEnd", function() {
	    if( $('.contnt_block_store').is(":visible") ){
	      var maxHeight = 0;
	      $('.contnt_block_store').children('.dropdown-menu_list').children('.contnstore_list_change').children('.repeat').each(function(){
	         if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
	      });
	      $('.contnt_block_store').children('.dropdown-menu_list').children('.contnstore_list_change').children('.repeat').height(maxHeight);
	    }
	  });

    $('.pro-row').each(function(){
      if($(this).find('.product-best').length){
        var apunt = $(this).find('.product-best').parent().parent();
        apunt.find('.product-list-img').each(function(){
          $(this).find('img').css('padding', '0px 0px 20px 0px');
        });
      }
    });



	//alto de contenedores de productos
  	$('.pro-row').each(function(){
  		//alto de nombre de producto
  		max_height = 0;
  		$(this).find('.product-name').each(function(){
	        if (max_height < $(this).height()){
	           max_height = $(this).height();
	        }
        });
    	$(this).find('.product-name').css('height', max_height);

    	max_height = 0;
    	$(this).find('.product-list').each(function(){
	        if (max_height < $(this).height()){
	           max_height = $(this).height();
	        }
        });
        $(this).find('.product-list').css('height', max_height);
  	});

    $('.relatedproduct').each(function(){
      //alto de nombre de producto
  		max_height = 0;
  		$(this).find('.product-name').each(function(){
	        if (max_height < $(this).height()){
	           max_height = $(this).height();
	        }
        });
    	$(this).find('.product-name').css('height', max_height);

    	max_height = 0;
    	$(this).find('.product-list').each(function(){
	        if (max_height < $(this).height()){
	           max_height = $(this).height();
	        }
        });
        $(this).find('.product-list').css('height', max_height);
    });

  	$('.autocomplete').on('click', '.cont_search_item',function(event){
  		$(this).css('display', 'none');
  		var valid = $(this).data('id');
  		var numberval = $('.cart-data #product-'+valid).val();
  		if(numberval == undefined || numberval == 'undefined'){
  			numberval = 1;
  		}
  		$(this).parent().children('.cont_search_cart').children('.dataactitem').text(numberval);
  		$(this).parent().children('.cont_search_cart').css('display', 'block');
  	});

  	$('.autocomplete').on('click', '.cont_search_cart .btndown',function(event){
  		var valid = $(this).data('id');
  		var numberval = $(this).parent().children('.dataactitem').text();
  		numberval = parseInt(numberval) - 1;
  		if(numberval == 0){
  			$(this).parent().css('display', 'none');
        $(this).parent().parent().children('.cont_search_item').css('display', 'block');
  		}else{
	  		$(this).parent().children('.dataactitem').text(numberval);
	  	}
  	});

  	$('.autocomplete').on('click', '.cont_search_cart .btnup',function(event){
  		var valid = $(this).data('id');
  		var numberval = $(this).parent().children('.dataactitem').text();
  		numberval = parseInt(numberval) + parseInt(1);
  		$(this).parent().children('.dataactitem').text(numberval);
  	});

  	//$('.scrollbar-inner').scrollbar();

	$('#bar_up').on('click', function(){
    if ($(this).hasClass('bar_up_select')){
  		$('.contnt_block_store').css('display', 'none');
      $('#bar_up').removeClass('bar_up_select');
    }else{
      $(this).addClass('bar_up_select');
      $('.contnt_block_store').css('display', 'block');
      var maxHeight = 0;
      $('.contnt_block_store').children('.dropdown-menu_list').children('.contnstore_list_change').children('.repeat').each(function(){
         if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
      });
      $('.contnt_block_store').children('.dropdown-menu_list').children('.contnstore_list_change').children('.repeat').height(maxHeight);

    }
	});

	$('.contnt_block_store .close').on('click', function(e){
		e.preventDefault();
		$(this).parent().parent().parent().parent().parent().css('display', 'none');
		$('#bar_up').removeClass('bar_up_select');
	});

  $('.cart-data').on('click', '.discount_block_close', function(e){
    e.preventDefault();
    $(this).parent().parent().remove();
  });

  $('.cart-data').on("mouseover", '.sd-cart-pro-li', function (e) {
    e.preventDefault();
    $(this).find('.btn_n_cart').css('opacity', 1);
    $(this).find('.btn_m_cart').css('opacity', 1);
    $(this).find('.price_tag span').css('display', 'none');
    $(this).find('.table_ico_cerrar').css('opacity', 1);
  });

  $('.cart-data').on("mouseout", '.sd-cart-pro-li', function (e) {
    e.preventDefault();
    $(this).find('.table_ico_cerrar').css('opacity', 0);
    $(this).find('.btn_n_cart').css('opacity', 0);
    $(this).find('.btn_m_cart').css('opacity', 0);
    $(this).find('.price_tag span').css('display', 'block');
  });

});

jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});

//takes the form field value and returns true on valid number
function validate_credit_card(number)
{
  // accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(number)) return false;

	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	number = number.replace(/\D/g, "");

	for (var n = number.length - 1; n >= 0; n--) {
		var cDigit = number.charAt(n),
			  nDigit = parseInt(cDigit, 10);

		if (bEven) {
			if ((nDigit *= 2) > 9) nDigit -= 9;
		}

		nCheck += nDigit;
		bEven = !bEven;
	}

	return (nCheck % 10) == 0;
}

function get_credit_card_type(number)
{
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "Visa";

    re = new RegExp("^5[1-5]");
    if (number.match(re) != null)
        return "Mastercard";

    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "AMEX";

    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "Discover";

    re = new RegExp("^36");
    if (number.match(re) != null)
        return "Diners";

    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
        return "Diners - Carte Blanche";

    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "JCB";

    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
        return "Visa Electron";

    number = number.substring(0, 6);
    if (number == 590712)
        return "Codensa";

    return false;
}

function validateEmail($email)
{
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

function window_open(url)
{
	window.open(url, 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
}