var fb_access = false;

$(document).ready(function () {
    $('form.login').on('submit', function (event) {
        var error = false;
        $('.form-control', this).each(function () {
            if (!$(this).val()) {
                $(this).parent().addClass('has-error');
                error = "Debes ingresar todos los campos";
            } else $(this).parent().removeClass('has-error');
        });

        if (error) {
            $('.form-has-errors').html(error).fadeIn();
            event.preventDefault();
        }
    });

    $('.fb-login').click(function () {
        fb_access = 'fb-login';
        toggleButtons(true);
        checkLoginState();
    });

    $('.fb-signin').click(function () {
        if ($('.crearCuentaBTN').length > 0) {
            $('.crearCuentaBTN').addClass('disabled');
        }
        if ($('form.register').length) {
            if ($('#phone').val().length == 0) {
                $('#phone').parent().addClass('has-error');
                $('.fb-signin-error').html("Por favor ingresa tu número celular").fadeIn();
                $('.crearCuentaBTN').removeClass('disabled');
                return false;
            } else {
                $('#phone').parent().removeClass('has-error');
                $('.fb-signin-error').fadeOut();
            }
        }
        fb_access = 'fb-signin';
        toggleButtons(true);
        checkLoginState();
    });

});

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    //console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        getUserData(response.authResponse);
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        loginFB();
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        loginFB();
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

//sign in fb
function loginFB() {
    FB.login(function (response) {
        if (response.authResponse) {
            getUserData(response.authResponse);
        } else {
            $('.fb-login').prop('disabled', false);
        }
    }, {
        scope: 'public_profile,email'
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: fb_app_id,
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8'
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    /*FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });*/
};

// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/es_ES/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
function getUserData(authResponse) {
    FB.api('/me?fields=id,first_name,last_name,email', function (response) {
        if (!response.id) {
            showError(
                'Ocurrió un error al obtener tus datos de Facebook. ' +
                'Por favor recarga la página e intenta de nuevo.'
            );

            return false;
        }

        needPhoneValidation(response.email, response.id, function (error, needValidation) {
            if (error) {
                errorCallback(error);
                return;
            }

            if (needValidation) {
                askValidation(null, function (error) {
                    if (error) {
                        errorCallback(error);
                    } else {
                        validCellphone();
                    }
                })
            } else {
                validCellphone();
            }

            function validCellphone() {
                toggleButtons(false);
                registerUser(response, authResponse)
            }

            function errorCallback(error) {
                toggleButtons(false);
                showError(
                    error instanceof FormValidationError
                        ? error.getErrorDetails()
                        : error.message
                )
            }
        });

        /**
         * @param {string} message
         */
        function showError(message) {
            if (isModalShown()) {
                $('.login-fb').append('<p style="color: #b94a48; margin-top: 5px;">' + message + '</p>');
            } else {
                $('.fb-error')
                    .html(message)
                    .fadeIn();
            }
        }
    });
}

/**
 * @param {string} email
 * @param {string} facebookId
 * @param {Function} callback
 */
function needPhoneValidation(email, facebookId, callback) {
    $.ajax({
        url: web_url + '/user/need-phone-validation',
        type: 'POST',
        data: {
            email: email,
            facebook_id: facebookId
        },
    })
        .done(function (response) {
            if (response.status) {
                callback(null, response.result);
            } else {
                callback(new Error(response.message));
            }
        })
        .fail(function (error) {
            callback(new Error(error.message));
        });
}

/**
 * @param {object} response
 * @param {object} authResponse
 */
function registerUser(response, authResponse) {
    var data = {
        first_name: response.first_name,
        last_name: response.last_name,
        email: response.email,
        fb_id: response.id,
        access_token: authResponse.accessToken
    };
    if ($('#promo_code').length) {
        data.register_fb_form = 1;
        data.phone = $('#phone').val();
        data.promo_code = $('#promo_code').val();
    }
    $('.' + fb_access).hide();
    $('.' + fb_access + '-loading').show();
    $.ajax({
        url: web_url + '/social',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            if (response.status) {
                //si es desde checkout
                if ($('form.order').length) {
                    if (isModalShown()) {
                        $('#login').modal('toggle');
                    }

                    if (response.result && response.result.is_new_user) {
                        var classesToUpdate = '.fb-signin, .login-button, .validate-button, input[name="phone"], #send-button';
                        $('input[name="first_name"]').val(response.result.facebook.first_name);
                        $('input[name="last_name"]').val(response.result.facebook.last_name);
                        $('input[name="email"]').val(response.result.facebook.email);
                        $('input[name="phone"]').val(response.result.phone.number);
                        $('.validate-button').addClass('disabled');

                        $(classesToUpdate + (response.result.facebook.email ? ',input[name="email"]' : ''))
                            .prop('disabled', true);

                        return;
                    }

                    $('.signin, .account').remove();
                    if (response.user_phone == '')
                        $('.user-phone').show();
                    else $('.user-phone').remove();
                    if (response.user_email == '')
                        $('.user-email').show();
                    else $('.user-email').remove();
                    $('.contentmenu_home').html(response.html.header);
                    $('.navbar .dropdown-menu').css('margin-top', '0px');
                    $('.new-address').show();
                    if (typeof response.html.credit_cards !== 'undefined') {
                        $('.user-credit-card').html(response.html.credit_cards);
                        $('.new-credit-card').prepend(response.html.credit_card_button);
                        //***********************Desplegar Tarjeta de credito registrada del usuario **********************/
                        $('*[name="payment_method"]').val("Tarjeta de crédito");
                        $('.cc').addClass('required').val('');
                        $('.form_cc').show();
                        $('body .new-credit-card').hide();
                        $('body .user-credit-card').show();
                        var id = $('body .user-credit-card').find('.credit-card:first-child').data('id');
                        $('body .user-credit-card [name="credit_card_id"]').val(id);
                        $('body .user-credit-card').find('hr').css({'margin': '30px 0px'});
                        $('body .user-credit-card .credit-card').removeClass('btn-primary');
                        $('body .user-credit-card .credit-card:eq(0)').removeClass('btn-default');
                        $('body .user-credit-card .credit-card:eq(0)').addClass('btn-primary');
                        $('body #installments_cc option:selected').removeAttr('selected');
                        $('body #installments_cc').val(1);

                    }
                    if (typeof response.html.credits !== 'undefined') {
                        $('.user-credit').html(response.html.credits);
                    }
                    if (typeof response.html.free_delivery_days !== 'undefined') {
                        $('.free-delivery-days').remove();
                        $('.store-delivery-slot').after(response.html.free_delivery_days);
                    }
                    cart_id = response.cart_id;
                    get_cart();
                } else if (response.result && response.result.redirect) {
                    window.location.replace(response.result.redirect);
                } else {
                    //si es desde registro
                    if ($('form.register').length) {
                        window.location = web_url + '/registro';
                    } else {
                        location.reload();
                    }
                }
            } else {
                $('.' + fb_access).show();
                $('.' + fb_access + '-loading').hide();
                $('.' + fb_access + '-error').html(response.message).fadeIn();
            }
        }, error: function () {
            $('.fb-login').show();
            $('.fb-loading').hide();
            $('.fb-error').html(
                'Ocurrió un error al procesar tus datos de Facebook. ' +
                'Por favor recarga la página e intenta de nuevo.'
            ).fadeIn();
        }
    });
}

/**
 * @returns {null|boolean|*}
 */
function isModalShown() {
    return ($('#login').data('bs.modal') || {}).isShown;
}

/**
 * @param disable
 */
function toggleButtons(disable) {
    $('.fb-login, .btn-phone-register').prop('disabled', disable);
}