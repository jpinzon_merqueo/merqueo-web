function geocode(address, city, callback) {
	var city_geo = {
		bogota: 'Bogotá, Colombia'
	};

	address += ',' + city_geo[city];

	var url = 'https://maps.googleapis.com/maps/api/geocode/json?address={address}&key=' + google_api_key;
		url = url.replace('{address}', encodeURIComponent(address));

	console.log(url);

	$.getJSON(url, function(result) {
		var lat = false;
		var lng = false;

		if(result.status = 'OK') {
			if(result.results.length > 0) {
				lat = result.results[0].geometry.location.lat;
				lng = result.results[0].geometry.location.lng;
			}
		}

		if(typeof callback == 'function') {
			try  {
				callback(lat, lng);
			} catch(e) {}
		}
	});
}