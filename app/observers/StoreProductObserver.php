<?php

/**
 * Class StoreProductObserver
 */
class StoreProductObserver
{
    /**
     * @param StoreProduct $store_product
     */
    public function updated(StoreProduct $store_product)
    {
        if ($store_product->isDirty()) {
            $dirty_data = $store_product->getDirty();
            $dirty_fields = array_keys($dirty_data);
            $fields_elastic_search_update = [
                'price',
                'special_price',
                'quantity_special_price',
                'delivery_discount_amount',
                'delivery_discount_start_date',
                'first_order_special_price',
                'is_best_price',
                'special_price_starting_date',
                'special_price_expiration_date',
            ];
            $has_dirty_fields = count(array_intersect($dirty_fields, $fields_elastic_search_update)) > 0;

            if (Config::get('app.aws.elasticsearch.is_enable') && $has_dirty_fields) {
                $aws_elasticsearch = new AWSElasticsearch();
                $aws_elasticsearch->update_products(['store_product_id' => $store_product->id]);
            }
        }

        if (isset($dirty_data) && Request::server('REQUEST_URI') === admin_url() . '/products/save/store-product' ||
            Request::server('REQUEST_URI') === admin_url() . '/products/import') {
            $this->log($store_product);
        }
    }

    /**
     * Actualiza el precio de los productos
     *
     * @param StoreProduct $store_product
     * @throws Exception
     */
    public function saving(StoreProduct $store_product)
    {
        $need_product_update = $store_product->isDirty('price') || $store_product->isDirty('cost');

        // actualiza el precio de los productos agrupados
        if ($need_product_update && $store_product->product->type !== 'Simple') {
            $store_product->calculatePriceOnGroup();
        }

        // recalcula base_price y base_cost para productos simples
        if($need_product_update && $store_product->product->type == "Simple"){
            $store_product->recalculateBasePriceAndCost();
        }
    }

    /**
     * Actualiza el precio de los productos simples
     *
     * @param StoreProduct $store_product
     * @throws Exception
     */
    public function saved(StoreProduct $store_product)
    {
        $need_product_update = $store_product->isDirty('price') || $store_product->isDirty('cost');

        if ($need_product_update && $store_product->product->type === 'Simple') {
            $store_product->updateAncestorsPrice();
        }
    }

    /**
     * @param StoreProduct $store_product
     */
    private function log(StoreProduct $store_product)
    {
        $dirty_data = $store_product->getDirty();
        $original_data = $store_product->getOriginal();

        $original = $dirty = [];
        foreach ($dirty_data as $att => $value) {
            $dirty[$att] = $value;
            $original[$att] = $original_data[$att];
        }
        $changes = ['original' => $original, 'changes' => $dirty];
        $store_product_log = new StoreProductLog;
        if (Session::has('admin_id')) {
            $store_product_log->admin_id = Session::get('admin_id');
        }
        $store_product_log->store_product_id = $store_product->id;
        $store_product_log->reason = json_encode($changes);
        $store_product_log->save();
    }
}
