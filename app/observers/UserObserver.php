<?php

use \Merqueo\Leanplum\SetUserAttributes;

/**
 * Class UserObserver
 */
class UserObserver
{
    /**
     * @param User $user
     * @throws Exception
     */
    public function updating(User $user)
    {
        $fields_to_validate = ['send_advertising', 'status'];
        $need_leanplum_update = array_intersect($fields_to_validate, array_keys($user->getDirty()));
        if (!empty($need_leanplum_update)) {
            try {
                retry(3, function () use ($user) {
                    Leanplum::setUserAttributes(new SetUserAttributes($user));
                });
            } catch (Exception $exception) {
                ErrorLog::add($exception);
            }
        }
    }
}
