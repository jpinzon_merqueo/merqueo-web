<?php

/**
 * Class StoreProductGroupObserver
 */
class StoreProductGroupObserver
{
    /**
     * @param StoreProductGroup $store_product_group
     * @throws Exception
     */
    public function created(StoreProductGroup $store_product_group)
    {
        $this->updateParent($store_product_group);
    }

    /**
     * @param StoreProductGroup $store_product_group
     * @throws Exception
     */
    public function deleted(StoreProductGroup $store_product_group)
    {
        $this->updateParent($store_product_group);
    }

    /**
     * @param StoreProductGroup $store_product_group
     * @throws Exception
     */
    private function updateParent(StoreProductGroup $store_product_group)
    {
        $store_product = $store_product_group->storeProduct;
        if ($store_product) {
            $store_product->calculatePriceOnGroup();
            $store_product->save();
        }
    }
}
