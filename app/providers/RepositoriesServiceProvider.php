<?php

namespace app\providers;

use Illuminate\Support\ServiceProvider;
use repositories\CityRepository;
use repositories\ConfigurationRepository;
use repositories\contracts\CityRepositoryInterface;
use repositories\contracts\ConfigurationRepositoryInterface;
use repositories\contracts\CountryRepositoryInterface;
use repositories\contracts\CreditRepositoryInterface;
use repositories\contracts\DiscountRepositoryInterface;
use repositories\contracts\EloquentDeliveryWindowInterface;
use repositories\contracts\EloquentStoreRepositoryInterface;
use repositories\Eloquent\EloquentDeliveryWindow;
use repositories\Eloquent\EloquentStoreRepository;
use repositories\Eloquent\EloquentTagRepository;
use repositories\contracts\TagRepositoryInterface;
use repositories\contracts\UserCreditRepositoryInterface;
use repositories\contracts\UserRepositoryInterface;
use repositories\CreditRepository;
use repositories\Eloquent\EloquentDeliveryExpressRepository;
use repositories\Eloquent\EloquentRoutesRepository;
use repositories\Eloquent\EloquentShelveSuggestedProductRepository;
use repositories\Eloquent\EloquentZoneRepository;
use repositories\Interfaces\DeliveryExpressRepositoryInterface;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\Interfaces\RoutesRepositoryInterface;
use repositories\Interfaces\EloquentShelvesSuggestedProductRepositoryInterface;
use repositories\Interfaces\ZoneRepositoryInterface;
use repositories\OrderRepository;
use repositories\EloquentDiscountRepository;
use repositories\UserCreditRepository;
use repositories\UserRepository;
use repositories\contracts\OrderGroupRepositoryInterface;
use repositories\OrderGroupRepository;
use repositories\contracts\RejectReasonRepositoryInterface;
use repositories\Eloquent\EloquentCountryRepository;
use repositories\Eloquent\RejectReasonRepository;
use repositories\contracts\OrderPaymentRepositoryInterface;
use repositories\Eloquent\EloquentOrderPaymentRepository;

/**
 * Class RepositoriesServiceProvider
 * @package app\providers
 */
class RepositoriesServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $classes = [
        CityRepositoryInterface::class => CityRepository::class,
        ConfigurationRepositoryInterface::class => ConfigurationRepository::class,
        EloquentShelvesSuggestedProductRepositoryInterface::class => EloquentShelveSuggestedProductRepository::class,
        UserCreditRepositoryInterface::class => UserCreditRepository::class,
        UserRepositoryInterface::class => UserRepository::class,
        CreditRepositoryInterface::class => CreditRepository::class,
        OrderRepositoryInterface::class => OrderRepository::class,
        OrderGroupRepositoryInterface::class => OrderGroupRepository::class,
        DiscountRepositoryInterface::class => EloquentDiscountRepository::class,
        ZoneRepositoryInterface::class => EloquentZoneRepository::class,
        DeliveryExpressRepositoryInterface::class => EloquentDeliveryExpressRepository::class,
        RoutesRepositoryInterface::class => EloquentRoutesRepository::class,
        RejectReasonRepositoryInterface::class => RejectReasonRepository::class,
        CountryRepositoryInterface::class => EloquentCountryRepository::class,
        TagRepositoryInterface::class => EloquentTagRepository::class,
        EloquentStoreRepositoryInterface::class => EloquentStoreRepository::class,
        EloquentDeliveryWindowInterface::class => EloquentDeliveryWindow::class,
        OrderPaymentRepositoryInterface::class => EloquentOrderPaymentRepository::class
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->classes as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }
}
