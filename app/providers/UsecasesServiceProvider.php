<?php


namespace app\providers;

use usecases\Checkout\Coupon\DiscountByValuesUseCase;
use usecases\Checkout\CouponUseCase;
use Tests\GlobalDiscountDeliveryUsecaseTest;
use usecases\Checkout\DeliveryDiscount\FirstOrderUseCase;
use usecases\Checkout\DeliveryDiscount\GlobalUseCase as GlobalDeliveryDiscountUseCase;
use usecases\Checkout\DeliveryDiscountUseCase;
use usecases\Checkout\DeliveryQuick\RouteOrderUseCase;
use usecases\cities\GetUsecase as CitiesGetUsecases;
use usecases\contracts\cities\GetUsecaseInterface as CitiesGetUsecasesInterface;
use Illuminate\Support\ServiceProvider;
use usecases\contracts\Countries\CountrySettingsUseCaseInterface;
use usecases\contracts\credits\AddCreditToUserUsecaseInterface;
use usecases\contracts\orders\AddCreditWhenDeliveryLateInterface;
use usecases\credits\AddCreditToUserUsecase;
use usecases\Interfaces\Checkout\Coupon\DiscountByValuesUseCaseInterface;
use usecases\Interfaces\Checkout\CouponUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryDiscount\FirstOrderUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryQuick\RouteOrderUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryDiscount\GlobalUseCaseInterface as GlobalDeliveryDiscountUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryDiscountUseCaseInterface;
use usecases\Interfaces\orders\CalculateDeliveryWindowAmountInterface;
use usecases\orders\AddCreditWhenDeliveryLate;
use usecases\orders\CalculateDeliveryWindowAmount;
use usecases\orders\OrderWhoReceives;
use usecases\contracts\orders\OrderWhoReceivesInterface;
use usecases\Products\Interfaces\GetIdsSuggestedProductByProductUseCaseInterface;
use usecases\Products\Suggested\GetIdsSuggestedProductByProductUseCase;
use usecases\Countries\CountrySettingsUseCase;
use usecases\Products\ValidatePrices\Interfaces\ValidateRealPublicPriceUseCaseInterface;
use usecases\Products\ValidatePrices\ValidateRealPublicPriceUseCase;
use usecases\orders\OrderCreatedNotification;
use usecases\contracts\orders\OrderCreatedNotificationInterface;
use usecases\orders\OrderCreatedTransformer;
use usecases\contracts\orders\OrderCreatedTransformerInterface;

/**
 * Class UsecasesServiceProvider
 * @package app\providers
 */
class UsecasesServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $classes = [
        CitiesGetUsecasesInterface::class => CitiesGetUsecases::class,
        GetIdsSuggestedProductByProductUseCaseInterface::class => GetIdsSuggestedProductByProductUseCase::class,
        AddCreditWhenDeliveryLateInterface::class => AddCreditWhenDeliveryLate::class,
        AddCreditToUserUsecaseInterface::class => AddCreditToUserUsecase::class,
        OrderWhoReceivesInterface::class => OrderWhoReceives::class,
        FirstOrderUseCaseInterface::class => FirstOrderUseCase::class,
        DeliveryDiscountUseCaseInterface::class => DeliveryDiscountUseCase::class,
        GlobalDeliveryDiscountUseCaseInterface::class => GlobalDeliveryDiscountUseCase::class,
        CouponUseCaseInterface::class => CouponUseCase::class,
        DiscountByValuesUseCaseInterface::class => DiscountByValuesUseCase::class,
        RouteOrderUseCaseInterface::class => RouteOrderUseCase::class,
        CountrySettingsUseCaseInterface::class => CountrySettingsUseCase::class,
        CalculateDeliveryWindowAmountInterface::class => CalculateDeliveryWindowAmount::class,
        ValidateRealPublicPriceUseCaseInterface::class => ValidateRealPublicPriceUseCase::class,
        OrderCreatedNotificationInterface::class => OrderCreatedNotification::class,
        OrderCreatedTransformerInterface::class => OrderCreatedTransformer::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->classes as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }
}