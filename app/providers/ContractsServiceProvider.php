<?php


use contracts\SendNotificationInterface;
use contracts\SendSNSNotificationInterface;
use Illuminate\Support\ServiceProvider;


class ContractsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $classes = [
        SendNotificationInterface::class => SendNotification::class,
        SendSNSNotificationInterface::class => SendSNSNotification::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->classes as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }
}