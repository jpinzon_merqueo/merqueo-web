<?php

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

/**
 * Class NewSiteServiceProvider
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class NewSiteServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NewSiteApi::class, function () {
            return new NewSiteApi(app(Client::class), Config::get('app.new_site_api'));
        });
    }
}
