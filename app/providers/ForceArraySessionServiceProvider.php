<?php

use Illuminate\Session\SessionServiceProvider;

/**
 * Class ForceArraySessionServiceProvider
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class ForceArraySessionServiceProvider extends SessionServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

        $this->forceArrayDriverOnApi();
    }

    /**
     * @return void
     */
    private function forceArrayDriverOnApi()
    {
        $request = $this->app['request'];

        if ($request->is('api/1.3/*')) {
            Config::set('session.driver', 'array');
        }
    }
}
