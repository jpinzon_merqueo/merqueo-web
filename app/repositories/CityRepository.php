<?php

namespace repositories;

use repositories\contracts\CityRepositoryInterface;

/**
 * Class CityRepository
 */
class CityRepository implements CityRepositoryInterface
{
    /**
     * @var \City
     */
    private $city;

    /**
     * CityRepository constructor.
     * @param \City $city
     */
    public function __construct(\City $city)
    {
        $this->city = $city;
    }


    /**
     * Busca la ciudades activas y principales, las anida con su respectivo store y sus "ciudades vecinas"
     * @return mixed
     */
    public function findAllMainActives()
    {
        return $this->city
            ->select('id', 'city', 'slug', 'latitude', 'longitude', 'coverage_store_id')
            ->where('status', \City::STATUS_ACTIVE)
            ->where('is_main', 1)
            ->with([
                'storeCovered' => function ($query) {
                    $query->select('id', 'city_id', 'name', 'slug', 'latitude', 'longitude');
                }
            ])
            ->with([
                'neighborhoods' => function ($query) {
                    $query
                        ->select('id', 'city', 'slug', 'latitude', 'longitude', 'coverage_store_id', 'parent_city_id')
                        ->where('status', \City::STATUS_ACTIVE);
                },
            ])->get();
    }

    /**
     * @return mixed
     */
    public function findAllActives()
    {
        return $this->city
            ->select('id', 'city', 'slug', 'latitude', 'longitude', 'coverage_store_id')
            ->where('status', \City::STATUS_ACTIVE)
            ->with([
                'storeCovered' => function ($query) {
                    $query->select('id', 'city_id', 'name', 'slug', 'latitude', 'longitude');
                }
            ])
            ->with([
                'neighborhoods' => function ($query) {
                    $query
                        ->select('id', 'city', 'slug', 'latitude', 'longitude', 'coverage_store_id', 'parent_city_id')
                        ->where('status', \City::STATUS_ACTIVE);
                },
            ])
            ->orderBy('city', 'asc')
            ->get();
    }
}