<?php


namespace repositories\contracts;

use app\objects\CreditDataObject;
use Credit;

interface UserRepositoryInterface
{
    public function getModel();

    public function findById($id);

    public function addCredit($id, Credit $credit, CreditDataObject $data);

    /**
     * @param $user
     * @return mixed
     */
    public function countOrders($user);
}
