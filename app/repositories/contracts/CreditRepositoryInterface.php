<?php


namespace repositories\contracts;


interface CreditRepositoryInterface
{

    public function getModel();

    public function findById($id);

}