<?php


namespace repositories\contracts;


interface OrderGroupRepositoryInterface
{
    public function updateWhoReceives($orderGroup, $nameReceives, $phoneReceives);
}