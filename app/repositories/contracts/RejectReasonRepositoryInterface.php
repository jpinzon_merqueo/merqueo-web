<?php

namespace repositories\contracts;

use RejectReason;

interface RejectReasonRepositoryInterface
{
    /**
     * Devuelve un objecto con el motivo de cancelación según los parámetros ingresados
     * @param string status
     * @param string | null attendant
     * @param string reason
     * @return RejectReason | null rejectReason
     */
    public function getRejectReasonIdUser($status, $reason, $attendant = null);
}
