<?php

namespace repositories\contracts;

/**
 * Interface TagRepositoryInterface
 * @package repositories\contracts
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
interface TagRepositoryInterface
{
    /**
     * @param $storeProductId
     * @return mixed
     */
    public function getTagByStoreProductId($storeProductId);
}