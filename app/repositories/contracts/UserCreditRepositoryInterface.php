<?php


namespace repositories\contracts;


use app\objects\CreditDataObject;

interface UserCreditRepositoryInterface
{
    public function getModel();


    public function add(CreditDataObject $creditDataObject);
}