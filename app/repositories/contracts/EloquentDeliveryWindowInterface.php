<?php
namespace repositories\contracts;


interface EloquentDeliveryWindowInterface
{
    /**
     * @param $deliveryWindowId
     * @return mixed
     */
    public function findDeliveryWindow($deliveryWindowId);
}