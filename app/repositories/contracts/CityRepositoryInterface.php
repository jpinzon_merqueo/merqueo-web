<?php

namespace repositories\contracts;

use Illuminate\Support\Collection;

/**
 * Interface CityRepositoryInterface
 */
interface CityRepositoryInterface
{

    /**
     * @return Collection
     */
    public function findAllMainActives();

    /**
     * @return Collection
     */
    public function findAllActives();

}