<?php

namespace repositories\contracts;

/**
 * Interface DiscountRepositoryInterface
 */
interface DiscountRepositoryInterface
{
    /**
     * @param $params object: currentDateTime, cityId, storeId, cartSubtotal
     * @return mixed|null
     */
    public function findByDateCitySubtotal($params);

    /**
     * @param $params object: currentDateTime, cityId, storeId, cartSubtotal
     * @return mixed|null
     */
    public function findByDateCityStoreSubtotal($params);

    /**
     * @param $cityId
     */
    public function findDiscountByCity($cityId);
}