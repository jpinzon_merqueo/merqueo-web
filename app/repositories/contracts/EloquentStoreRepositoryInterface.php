<?php
namespace repositories\contracts;

interface EloquentStoreRepositoryInterface
{
    /**
     * @param $storeId
     * @return mixed
     */
    public function findStore($storeId);
}