<?php
namespace repositories\contracts;

/**
 * Class OrderPaymentRepositoryInterface.
 */
interface OrderPaymentRepositoryInterface
{
    /*
     * @param int $orderId
     * @return OrderPayment
     */
    public function findAuthorizationByOrderId($orderId);

    /*
     * @param int $orderId
     * @return OrderPayment
     */
    public function findCaptureByOrderId($orderId);
}
