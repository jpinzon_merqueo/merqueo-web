<?php


namespace repositories\contracts;


interface ConfigurationRepositoryInterface
{
    public function getModel();

    /**
     * @param $key
     * @param int $city_id
     * @return mixed
     */
    public function findByKey($key, $city_id = 0);

}