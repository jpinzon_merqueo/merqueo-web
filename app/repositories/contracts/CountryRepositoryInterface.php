<?php
namespace repositories\contracts;

/**
 * Class CountryRepositoryInterface.
 */
interface CountryRepositoryInterface
{
    /*
     * @param int $storeId
     * @return Country
     */
    public function findByStoreId($storeId);

    /**
     * @param string $phoneCode
     * return Country
     */
    public function findCountryByPhoneCode($phoneCode);
}
