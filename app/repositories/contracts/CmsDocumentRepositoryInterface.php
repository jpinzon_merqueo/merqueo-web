<?php

namespace repositories\contracts;

interface CmsDocumentRepositoryInterface
{
    function findOne($slug);
}