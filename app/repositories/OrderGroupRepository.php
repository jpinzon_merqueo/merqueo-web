<?php


namespace repositories;

use repositories\contracts\OrderGroupRepositoryInterface;


/**
 * Class OrderGroupRepository
 * @package repositories
 */
class OrderGroupRepository implements OrderGroupRepositoryInterface
{
    /**
     * @param $orderGroup
     * @param $nameReceives
     * @param $phoneReceives
     * @return bool
     */
    public function updateWhoReceives($orderGroup, $nameReceives, $phoneReceives)
    {
        $orderGroup->reception_user_name = $nameReceives;
        $orderGroup->reception_user_phone = $phoneReceives;
        return $orderGroup->save() ? true : false;
    }
}