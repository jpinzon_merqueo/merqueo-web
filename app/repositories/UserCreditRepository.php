<?php


namespace repositories;


use app\objects\CreditDataObject;
use repositories\contracts\UserCreditRepositoryInterface;
use UserCredit;
use Carbon\Carbon;

class UserCreditRepository extends BaseRepository implements UserCreditRepositoryInterface
{

    /**
     * @return UserCredit|string
     */
    public function getModel()
    {
        return UserCredit::class;
    }

    /**
     * @param CreditDataObject $creditDataObject
     * @return UserCredit
     */
    public function add(CreditDataObject $creditDataObject)
    {
        $creditDataObject->current_amount = $creditDataObject->amount;
        $credit =  $this->create((array) $creditDataObject);
        $this->setModel($credit->load('user', 'credit'));
        return $this->obj;
    }

}