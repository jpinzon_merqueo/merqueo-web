<?php

namespace repositories;

use Carbon\Carbon;
use repositories\contracts\DiscountRepositoryInterface;
use Discount;
use Exception;
use repositories\Exceptions\EloquentDiscountRepositoryException;

/**
 * Class EloquentDiscountRepository
 * @package repositories
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class EloquentDiscountRepository implements DiscountRepositoryInterface
{
    /**
     * @var Discount
     */
    protected $discount;

    /**
     * @var EloquentDiscountRepositoryException
     */
    private $exception;

    /**
     * @var Carbon
     */
    private $dateTime;

    /**
     * EloquentDiscountRepository constructor.
     * @param Discount $discount
     */
    public function __construct(Discount $discount)
    {
        $this->discount  = $discount;
        $this->dateTime = Carbon::now()->format('Y-m-d H:i:s');
        $this->exception = new EloquentDiscountRepositoryException(__CLASS__);
    }

    /**
     * @param $params object: currentDateTime, cityId, storeId, cartSubtotal
     * @return mixed|null
     */
    public function findByDateCitySubtotal($params)
    {
        try {
            return $this->discount
                ->whereRaw("'{$params->currentDateTime}' BETWEEN start_date AND expiration_date")
                ->whereRaw("CONCAT(',',city_id,',') LIKE '%,{$params->cityId},%'")
                ->whereNull('store_ids')
                ->whereRaw("{$params->cartSubtotal} >= minimum_order_amount")
                ->where('type', 'free_delivery')
                ->get();
        }
        catch (Exception $e) {
            $this->exception->report($e, (object) ['params' => $params, 'method' => __FUNCTION__]);
            return null;
        }
    }

    /**
     * @param $params object: currentDateTime, cityId, storeId, cartSubtotal
     * @return mixed|null
     */
    public function findByDateCityStoreSubtotal($params)
    {
        try {
            return $this->discount
                ->whereRaw("'{$params->currentDateTime}' BETWEEN start_date AND expiration_date")
                ->whereRaw("CONCAT(',',city_id,',') LIKE '%,{$params->cityId},%'")
                ->whereRaw("CONCAT(',',store_ids,',') LIKE '%,{$params->storeId},%'")
                ->whereRaw("{$params->cartSubtotal} >= minimum_order_amount")
                ->where('type', 'free_delivery')
                ->get();
        }
        catch (Exception $e) {
            $this->exception->report($e, (object) ['params' => $params, 'method' => __FUNCTION__]);
            return null;
        }
    }

    /**
     * @param $cityId
     * @return mixed
     */
    public function findDiscountByCity($cityId)
    {

        return $this->discount->where('type', 'free_delivery')
            ->whereRaw("'{$this->dateTime}' BETWEEN start_date AND expiration_date")
            ->whereRaw("CONCAT(',',city_id,',') LIKE '%,{$cityId},%'")
            ->first();
    }
}