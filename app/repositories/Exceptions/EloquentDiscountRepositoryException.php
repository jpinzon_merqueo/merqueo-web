<?php

namespace repositories\Exceptions;

use Exception;
use Log;
use Throwable;

/**
 * Class EloquentDiscountRepositoryException
 * @package repositories\Exceptions
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class EloquentDiscountRepositoryException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Report or log an exception.
     *
     * @param $exception
     * @param $info
     */
    public function report(Exception $exception, $info)
    {
        Log::error("{$this->message} - {$info->method}: {$exception->getMessage()}");
        Log::debug((array) $info->params);
    }
}