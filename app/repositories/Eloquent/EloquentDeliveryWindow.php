<?php


namespace repositories\Eloquent;


use repositories\contracts\EloquentDeliveryWindowInterface;

class EloquentDeliveryWindow implements EloquentDeliveryWindowInterface
{
    /**
     * @var \DeliveryWindow
     */
    private $deliveryWindow;

    /**
     * EloquentDeliveryWindow constructor.
     * @param \DeliveryWindow $deliveryWindow
     */
    public function __construct(\DeliveryWindow $deliveryWindow)
    {
        $this->deliveryWindow = $deliveryWindow;
    }

    /**
     * @param $deliveryWindowId
     * @return mixed
     */
    public function findDeliveryWindow($deliveryWindowId)
    {
        return $this->deliveryWindow->findOrFail($deliveryWindowId);
    }
}