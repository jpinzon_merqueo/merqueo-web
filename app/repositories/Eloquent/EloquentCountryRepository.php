<?php
namespace repositories\Eloquent;

use Country;
use repositories\contracts\CountryRepositoryInterface;

class EloquentCountryRepository implements CountryRepositoryInterface
{
    /**
     * @param int $storeId
     * @return Country
     */
    public function findByStoreId($storeId)
    {
        return Country::join(
            'cities',
            'countries.id',
            '=',
            'cities.country_id'
        )->join(
            'city_store',
            'cities.id',
            '=',
            'city_store.city_id'
        )->where('city_store.store_id', '=', $storeId)->first();
    }

    /**
     * @param string $phoneCode
     * return Country
     */
    public function findCountryByPhoneCode($phoneCode)
    {
        return Country::where('phone_code', $phoneCode)->first();
    }
}
