<?php
namespace repositories\Eloquent;

use OrderPayment;
use repositories\contracts\OrderPaymentRepositoryInterface;

class EloquentOrderPaymentRepository implements OrderPaymentRepositoryInterface
{
    /*
     * Retorna la transacción de la autorización aprobada de una orden
     *
     * @param int $orderId
     * @return OrderPayment
     */
    public function findAuthorizationByOrderId($orderId)
    {
        return OrderPayment::where('order_payments.order_id', $orderId)
            ->where('order_payments.transaction_type', OrderPayment::TRANSACTION_TYPE_AUTHORIZE)
            ->where('order_payments.cc_payment_status', OrderPayment::TRANSACTION_STATUS_APPROVED)
            ->get();
    }

    /*
     * Retorna la transacción del capture de una orden
     *
     * @param int $orderId
     * @return OrderPayment
     */
    public function findCaptureByOrderId($orderId)
    {
        return OrderPayment::where('order_payments.order_id', $orderId)
            ->where('order_payments.transaction_type', OrderPayment::TRANSACTION_TYPE_CAPTURE)
            ->where('order_payments.cc_payment_status', OrderPayment::TRANSACTION_STATUS_APPROVED)
            ->get();
    }
}
