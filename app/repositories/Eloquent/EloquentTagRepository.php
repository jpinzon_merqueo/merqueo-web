<?php

namespace repositories\Eloquent;

use Carbon\Carbon;
use repositories\contracts\TagRepositoryInterface;
use Trade\Tags;

/**
 * Class EloquentTagRepository
 * @package repositories\contracts
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class EloquentTagRepository implements TagRepositoryInterface
{
    /**
     * @var Tags
     */
    private $tags;

    /**
     * @var string
     */
    private $currentDatetime;

    /**
     * EloquentTagRepository constructor.
     * @param Tags $tags
     */
    public function __construct(Tags $tags)
    {
        $this->tags = $tags;
        $this->currentDatetime = Carbon::now()->toDateTimeString();
    }

    /**
     * @param $storeProductId
     * @return mixed
     */
    public function getTagByStoreProductId($storeProductId)
    {
        return $this->tags
            ->select(
                'tags.title',
                'tags.description',
                'tags.button_text',
                'tags.tag_text',
                'tags.url_styles',
                'tags.updated_at AS last_updated'
            )
            ->join(
                'store_product_tags',
                'store_product_tags.tag_id',
                '=',
                'tags.id'
            )
            ->where('store_product_tags.store_product_id', $storeProductId)
            ->where('tags.status', Tags::ENABLED_STATUS)
            ->whereRaw("'{$this->currentDatetime}' BETWEEN tags.start_date AND tags.expiration_date")
            ->orderBy('tags.expiration_date')
            ->first();
    }
}