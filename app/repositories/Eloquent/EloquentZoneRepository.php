<?php


namespace repositories\Eloquent;


use Zone;
use Warehouse;
use repositories\Interfaces\ZoneRepositoryInterface;

class EloquentZoneRepository implements ZoneRepositoryInterface
{

    /**
     * find zona express
     *
     * @return Zone
     */
    public function findZoneExpress()
    {
        return Zone::with('warehouse')
            ->where('warehouse_id', Warehouse::WAREHOUSE_DARKSUPERMARKET_ID[0])
            ->first();
    }

    /**
     * find zone by latitude and longitude
     *
     * @param $latitude
     * @param $longitude
     *
     * @return Zone|null
     */
    public function getZoneExpressByCoordinates($latitude, $longitude)
    {
        $zones = Zone::with(['warehouse' => function ($query) {
                $query->where('warehouses.status', Warehouse::STATUS_ACTIVE);
            }])
            ->where('zones.status', Zone::STATUS_ACTIVE)
            ->whereIn('warehouse_id', Warehouse::WAREHOUSE_DARKSUPERMARKET_ID)
            ->get();

        foreach ($zones as $zone) {
            if (point_in_polygon($zone->delivery_zone, $latitude, $longitude)) {
                return $zone;
            }
        }

        return null;
    }
}