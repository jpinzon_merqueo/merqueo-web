<?php
namespace repositories\Eloquent;

use repositories\contracts\EloquentStoreRepositoryInterface;

class EloquentStoreRepository implements EloquentStoreRepositoryInterface
{
    /**
     * @var \Store
     */
    private $store;

    /**
     * EloquentStoreRepository constructor.
     * @param \Store $store
     */
    public function __construct(\Store $store)
    {
        $this->store = $store;
    }

    /**
     * @param $storeId
     * @return mixed
     */
    public function findStore($storeId)
    {
        return $this->store->findOrFail($storeId);
    }
}