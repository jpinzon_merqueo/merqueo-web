<?php

namespace repositories\Eloquent;

use RejectReason;
use repositories\contracts\RejectReasonRepositoryInterface;

class RejectReasonRepository implements RejectReasonRepositoryInterface
{
    const TYPE_ADMIN = 'admin';

    /**
     * Devuelve un objeto con el motivo de cancelación según los parámetros ingresados
     * @param string status
     * @param string | null attendant
     * @param string reason
     * @return RejectReason | null rejectReason
     */
    public function getRejectReasonIdUser($status, $reason, $attendant = null)
    {
        return RejectReason::where('status', $status)
            ->where('attendant', $attendant)
            ->where('reason', $reason)
            ->where('type', self::TYPE_ADMIN)
            ->first();
    }

    /**
     * Returns an object with the reason for cancellation according to the parameters entered.
     *
     * @param string status
     * @param array | null attendant
     * @param string reason
     * @return RejectReason | null rejectReason
     */
    public function getRejectReasonIdUserByReasons($status, $reason, $attendant = null)
    {
        return RejectReason::where('status', $status)
            ->where('attendant', $attendant)
            ->whereIn('reason', $reason)
            ->where('type', self::TYPE_ADMIN)
            ->first();
    }
}
