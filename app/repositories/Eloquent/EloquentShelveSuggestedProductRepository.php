<?php
namespace repositories\Eloquent;

use Carbon\Carbon;
use repositories\Interfaces\EloquentShelvesSuggestedProductRepositoryInterface;
use ShelvesSuggestedProducts;

/**
 * Class EloquentShelvesSuggestedProductsRepository
 * @package repositories
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class EloquentShelveSuggestedProductRepository implements EloquentShelvesSuggestedProductRepositoryInterface
{
    /**
     * @var int
     */
    const ACTIVE_STATUS = 1;

    /**
     * @var ShelvesSuggestedProducts
     */
    private $shelvesSuggestedProducts;

    /**
     * @var string
     */
    private $datetimeNow;

    /**
     * EloquentShelveSuggestedProductRepository constructor.
     * @param ShelvesSuggestedProducts $shelvesSuggestedProducts
     */
    public function __construct(ShelvesSuggestedProducts $shelvesSuggestedProducts)
    {
        $this->shelvesSuggestedProducts = $shelvesSuggestedProducts;
        $this->datetimeNow = Carbon::now()->toDateTimeString();
    }

    /**
     * @param array $storeProductIds
     * @return mixed
     */
    public function findActiveByStoreIds($storeProductIds = [])
    {
        if (!count($storeProductIds)) {
            return null;
        }

        $orWhere = '';

        $shelveActive = $this->shelvesSuggestedProducts
            ->where('status', self::ACTIVE_STATUS)
            ->whereRaw("'{$this->datetimeNow}' BETWEEN start_date AND end_date");

        foreach ($storeProductIds as $key => $storeProductId){
            $orWhere .= (!$key ? '' : ' OR ') . "CONCAT(',',store_product_ids,',') LIKE '%,{$storeProductId},%'";
        }

        return $shelveActive->whereRaw($orWhere)
            ->orderBy('priority')
            ->first();
    }
}