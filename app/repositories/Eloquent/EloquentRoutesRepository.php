<?php


namespace repositories\Eloquent;


use repositories\Interfaces\RoutesRepositoryInterface;
use Warehouse;
use Zone;
use Routes;

/**
 * Class EloquentRouteRepository
 * @package repositories\Eloquent
 */
class EloquentRoutesRepository implements RoutesRepositoryInterface
{

    /**
     * @param Warehouse $warehouse
     * @param Zone $zone
     * @return mixed
     */
    public function getCurrentDarksupermarketRoute(Warehouse $warehouse, Zone $zone)
    {
        return Routes::getCurrentDarksupermarketRoute($warehouse, $zone);
    }

    /**
     * @param Warehouse $warehouse
     * @param Zone $zone
     * @param string $shift
     * @return Routes
     */
    public function getCurrentDeliveryQuickRoute(Warehouse $warehouse, Zone $zone, $shift)
    {
        return Routes::getCurrentDarksupermarketRoute($warehouse, $zone, $shift);
    }
}