<?php


namespace repositories\Eloquent;

use DeliveryExpress;
use UserAddress;
use Zone;
use repositories\Interfaces\DeliveryExpressRepositoryInterface;

class EloquentDeliveryExpressRepository implements DeliveryExpressRepositoryInterface
{
    /**
     * validate products and coverage express
     *
     * @param UserAddress $address
     * @param Object $productsCart
     * @param Zone $zoneExpress
     *
     * @return mixed
     */
    public function validateDeliveryExpress($address, $productsCart, $zoneExpress)
    {
        return DeliveryExpress::validateCoverageAndProductsDarkSupermarket(
            $address,
            $productsCart,
            $productsCart,
            $zoneExpress
        );
    }
}