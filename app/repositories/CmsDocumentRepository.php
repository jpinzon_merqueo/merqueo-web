<?php

namespace repositories;

use repositories\contracts\CmsDocumentRepositoryInterface;
use models\CmsDocument;

class CmsDocumentRepository implements CmsDocumentRepositoryInterface
{
    public function findOne($slug)
    {
        return CmsDocument::where('slug', $slug)->first();
    }
}