<?php


namespace repositories;


use Credit;
use repositories\contracts\CreditRepositoryInterface;

class CreditRepository extends BaseRepository implements CreditRepositoryInterface
{

    /**
     * @return Credit|string
     */
    public function getModel()
    {
        return Credit::class;
    }

    /**
     * @param $id
     * @return Credit
     */
    public function findById($id)
    {
        return $this->findOrFail($id);
    }
}