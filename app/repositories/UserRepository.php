<?php


namespace repositories;


use app\objects\CreditDataObject;
use Credit;
use repositories\contracts\UserRepositoryInterface;
use User;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * @return string|User
     */
    public function getModel()
    {
        return User::class;
    }

    /**
     * @param $id
     * @return User
     */
    public function findById($id)
    {
        return $this->findOrFail($id);
    }

    public function addCredit($id, Credit $credit, CreditDataObject $data)
    {
        //dd($data);
        $user = $this->findById($id);
        return $user->credits()->save($credit, (array) $data);
    }

    /**
     * @param $user
     * @return mixed
     */
    public function countOrders($user)
    {
        return $user->orders()->count();
    }
}
