<?php


namespace repositories\Interfaces;

use app\objects\Object;
use Zone;
use UserAddress;


interface DeliveryExpressRepositoryInterface
{

    /**
     * validate products and coverage express
     *
     * @param UserAddress $address
     * @param Object $productsCart
     * @param Zone $zoneExpress
     *
     * @return mixed
     */
    public function validateDeliveryExpress($address, $productsCart, $zoneExpress);

}