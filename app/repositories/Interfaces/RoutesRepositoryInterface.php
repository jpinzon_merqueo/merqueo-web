<?php


namespace repositories\Interfaces;

use Routes;
use Warehouse;
use Zone;

/**
 * Interface RouteRepositoryInterface
 * @package repositories\Interfaces
 */
interface RoutesRepositoryInterface
{
    /**
     * @param Warehouse $warehouse
     * @param Zone $zone
     * @return mixed
     */
    public function getCurrentDarksupermarketRoute(Warehouse $warehouse, Zone $zone);

    /**
     * @param Warehouse $warehouse
     * @param Zone $zone
     * @param string $shift
     * @return Routes
     */
    public function getCurrentDeliveryQuickRoute(Warehouse $warehouse, Zone $zone, $shift);
}