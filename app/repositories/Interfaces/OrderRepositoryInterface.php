<?php

namespace repositories\Interfaces;

use Order;

/**
 * Interface OrderRepository
 */
interface OrderRepositoryInterface
{
    /**
     * OrderRepository constructor.
     * @param Order $order
     */
    public function __construct(Order $order);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $userId
     * @return mixed
     */
    public function getQuantityOrderByUser($userId);

    /**
     * @param $orderId
     * @param $userId
     * @return mixed
     */
    public function findByIdUserAndOrder($orderId, $userId);

    /**
     * @param $orderId
     * @return mixed
     */
    public function findOrderbyId($orderId);

    /**
     * @param int $orderId
     * @param int $routeId
     * @return mixed
     */
    public function updateStatusOrderExpressToRouted($orderId, $routeId);

    /**
     * @param $userId
     * @return mixed
     */
    public function findByUserId($userId);
}