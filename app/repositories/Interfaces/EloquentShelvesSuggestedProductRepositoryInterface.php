<?php
namespace repositories\Interfaces;

/**
 * Interface ShelvesSuggestedProductRepositoryInterface
 * @package repositories\Interfaces
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
interface EloquentShelvesSuggestedProductRepositoryInterface
{
    /**
     * @param array $storeProductIds
     * @return mixed
     */
    public function findActiveByStoreIds($storeProductIds = []);
}