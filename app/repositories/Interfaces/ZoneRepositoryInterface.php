<?php


namespace repositories\Interfaces;

use Zone;

interface ZoneRepositoryInterface
{
    /**
     * find zona express
     *
     * @return Zone
     */
    public function findZoneExpress();

    /**
     * find zone by latitude and longitude
     *
     * @param $latitude
     * @param $longitude
     *
     * @return Zone|null
     */
    public function getZoneExpressByCoordinates($latitude, $longitude);

}