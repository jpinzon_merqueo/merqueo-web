<?php


namespace repositories;

use DB;
use Log;
use Order;
use orders\OrderStatus;
use repositories\Interfaces\OrderRepositoryInterface;

/**
 * Class OrderRepository
 * @package repositories
 */
class OrderRepository implements OrderRepositoryInterface
{
    CONST SOURCE_TYPE = 'Reclamo';

    /**
     * @var Order
     */
    private $order;

    /**
     * OrderRepository constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection|mixed|Order|null
     */
    public function findById($id)
    {
        return $this->order->find($id);
    }

    /**
     * @param $userId
     * @return mixed|null
     */
    public function getQuantityOrderByUser($userId)
    {
        try {
            return $this->order
                ->select(
                    DB::raw('COUNT(orders.id) AS quantity')
                )
                ->leftJoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->where('orders.user_id', $userId)
                ->where('order_groups.source', '<>', self::SOURCE_TYPE)
                ->groupBy('orders.user_id')
                ->first();
        }
        catch (\Exception $e) {
            Log::error("OrderRepository - getQuantityOrderByUser: {$e->getMessage()}");
            return null;
        }
    }

    /**
     * @param $orderId
     * @param $userId
     * @return mixed
     */

    public function findByIdUserAndOrder($orderId, $userId)
    {
        return Order::where('id',$orderId)
            ->where('user_id', $userId)
            ->first();
    }

    /**
     * @param $orderId
     * @return \Illuminate\Support\Collection|Order
     */
    public function findOrderbyId($orderId)
    {
        return Order::findOrFail($orderId);
    }

    /**
     * @param int $orderId
     * @param int $routeId
     * @return mixed
     */
    public function updateStatusOrderExpressToRouted($orderId, $routeId)
    {
        return Order::where('id', $orderId)
                ->update(
                    [
                        'route_id' => $routeId,
                        'status' => OrderStatus::ROUTED,
                        'planning_date' => \Carbon\Carbon::now()->format('Y-m-d'),
                    ]
                );
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function findByUserId($userId)
    {
        return $this->order
            ->where('user_id', $userId)
            ->get();
    }
}