<?php


namespace repositories;

use Configuration;
use repositories\contracts\ConfigurationRepositoryInterface;

class ConfigurationRepository implements ConfigurationRepositoryInterface
{

    /**
     * @return Configuration
     */
    public function getModel()
    {
        return new Configuration();
    }

    /**
     * @param string $key
     * @param int $city_id
     * @return Configuration
     */
    public function findByKey($key, $city_id = 0)
    {
        return $this->getModel()->where('key', $key)->where('warehouse_id', $city_id)->pluck('value');
    }
}