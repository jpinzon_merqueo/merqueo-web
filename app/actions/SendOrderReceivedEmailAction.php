<?php

use App\Models\PaymentMethod;
use repositories\contracts\UserRepositoryInterface;

/**
 * Class SendOrderReceivedEmailAction.
 */
class SendOrderReceivedEmailAction
{
    /**
     * @var int
     */
    const MAXIMUN_ORDER_FOR_FREE_DELIVERY_EXPIRATION_DATE = 1;

    /**
     * @var string
     */
    const COUNTRY_CODE_MX = 'mx';

    /**
     * @var string
     */
    const SUBJECT_EMAIL_COL = 'Tu Merqueo';

    /**
     * @var string
     */
    const SUBJECT_EMAIL_MX = 'Tu pedido en Súper Merqueo';

    /**
     * @var Order
     */
    private $order;

    /**
     * @var array
     */
    private $orderProducts;

    /**
     * @var bool
     */
    private $orderExpress;

    /**
     * @var array
     */
    private $dataCountry;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var string
     */
    private $applyPromotionalFirstTHousandOrdersMexico;

    /**
     * @var User
     */
    private $user;

    /**
     * @param $order
     * @param null $orderProducts
     * @param bool $orderExpress
     */
    public function __construct($order, $orderProducts = null, $orderExpress = false, $dataCountry = [])
    {
        $this->order = $order;
        $this->user = $this->order->user;
        $this->orderProducts = $orderProducts;
        $this->orderExpress = $orderExpress;
        $this->dataCountry = $dataCountry;
        $this->applyPromotionalFirstTHousandOrdersMexico = false;
        $this->userRepository = app(UserRepositoryInterface::class);
    }

    public function run()
    {
        if (!empty($this->dataCountry['country_code'])
            && $this->dataCountry['country_code'] === self::COUNTRY_CODE_MX
            && $this->user->free_delivery_expiration_date
            && $this->userRepository->countOrders($this->user) === self::MAXIMUN_ORDER_FOR_FREE_DELIVERY_EXPIRATION_DATE
        ) {
            $this->applyPromotionalFirstTHousandOrdersMexico = true;
        }

        $orders = Order::where('group_id', $this->order->group_id)
            ->groupBy('type')
            ->get();

        if ($this->dataCountry['country_code'] === Country::COUNTRY_CODE_MEXICO) {
            if ($this->order->payment_method === PaymentMethod::DATAFONO) {
                $this->order->payment_method = PaymentMethod::TERMINAL;
            }

            if (count($orders) > 0) {
                foreach ($orders as $order) {
                    if ($order->payment_method === PaymentMethod::DATAFONO) {
                        $order->payment_method = PaymentMethod::TERMINAL;
                    }
                }
            }
        }

        $mail = [
            'template_name' => 'emails.order_received',
            'subject'       => $this->getSubjectByCountry(),
            'to'            => [
                [
                    'email' => $this->order->user->email,
                    'name'  => $this->order->user->first_name.' '.$this->order->user->last_name,
                ],
            ],
            'vars'          => [
                'orders'      => $orders && count($orders) ? $orders : [$this->order],
                'order_group' => $this->order->orderGroup,
                'products'    => $this->orderProducts ? $this->orderProducts : $this->order->orderProducts->map(function ($product) {
                    return [
                        'order_type' => $this->order->type,
                        'img'        => $product->product_image_url,
                        'name'       => $product->product_name,
                        'quantity'   => $product->product_quantity,
                        'unit'       => $product->product_unit,
                        'qty'        => $product->quantity,
                        'price'      => $product->price,
                        'total'      => $product->quantity * $product->price,
                        'type'       => $product->type,
                    ];
                }),
                'delivery_express' => $this->orderExpress,
                'country_data' => $this->dataCountry,
                'applyPromotionaFirstTHousandOrdersMexico' => $this->applyPromotionalFirstTHousandOrdersMexico
            ],
        ];

        send_mail($mail, false, false, $this->dataCountry['admin_email']);
    }

    /**
     * @return string
     */
    private function getSubjectByCountry()
    {
        if (!empty($this->dataCountry) && $this->dataCountry['country_code'] === self::COUNTRY_CODE_MX) {
            return self::SUBJECT_EMAIL_MX;
        }

        return self::SUBJECT_EMAIL_COL;
    }
}
