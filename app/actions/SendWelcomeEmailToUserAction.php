<?php

/**
 * Class SendWelcomeEmailToUserAction.
 */
class SendWelcomeEmailToUserAction
{
    /**
     * @var Order
     */
    private $user;

    /**
     * @var array
     */
    private $dataCountry;

    /**
     *
     * @param User $user
     */
    public function __construct($user, $dataCountry = [])
    {
        $this->user = $user;
        $this->dataCountry = $dataCountry;
    }

    public function run()
    {
        $mail = [
            'template_name' => 'emails.welcome',
            'subject'       => 'Bienvenido a Merqueo',
            'to'            => [
                [
                    'email' => $this->user->email,
                    'name'  => $this->user->first_name.' '.$this->user->last_name,
                ],
            ],
            'vars'          => [
                'referral_code' => $this->user->referral_code,
                'referred'      => User::validateReferred($this->user, null, true),
                'name'          => $this->user->first_name,
                'country_data' => $this->dataCountry,
            ],
        ];

        send_mail($mail, false, true, $this->dataCountry['admin_email']);
    }
}
