<?php

use Carbon\Carbon;
use Merqueo\Leanplum\LeanplumClient;
use Merqueo\Leanplum\SetUserAttributes;
use orders\OrderStatus;

/**
 * Class UpdateLeanplumUser
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class UpdateLeanplumUser
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var LeanplumClient
     */
    private $client;

    /**
     * UpdateLeanplumUser constructor.
     * @param User $user
     * @param LeanplumClient $client
     */
    public function __construct(User $user, LeanplumClient $client)
    {
        $this->user = $user;
        $this->client = $client;
    }

    /**
     * @param array $onlySomeAttributes
     * @return User|void
     */
    public function updateUser($onlySomeAttributes = [])
    {
        $this->refreshAttributesToUpdate($onlySomeAttributes);
        $this->makeRequestWithNewValues($onlySomeAttributes);
    }

    /**
     * @param $attributes
     * @return void
     */
    protected function makeRequestWithNewValues($attributes)
    {
        $this->client->setUserAttributes(new SetUserAttributes($this->user, $attributes));
    }

    /**
     * Asigna los valores del usuario con el fin de
     * que se actualicen en leanplum.
     *
     * @param array $attributeList
     */
    protected function updateAttributesList(array $attributeList)
    {
        foreach ($attributeList as $item) {
            // Al utilizar los métodos magicos el atributo
            // se asigna y se agrega al listado de campos
            // modificador que posteriormente van a ser
            // enviados a Leanplum.
            $this->user->$item = $this->user->$item;
        }
    }

    /**
     * @param array $onlySomeAttributes
     */
    protected function refreshAttributesToUpdate(array $onlySomeAttributes = [])
    {
        if (!empty($onlySomeAttributes)) {
            $this->updateAttributesList($onlySomeAttributes);
            return;
        }

        $attributes = $this->user->fullTraits;
        $lastOrderDelivered = $this->user->orders()
            ->where('status', OrderStatus::DELIVERED)
            ->whereHas('orderGroup', function ($query) {
                $query->where('source', '<>', 'Reclamo');
            })
            ->latest()
            ->first();

        $lastOrderCreated = $this->user->orders()
            ->latest()
            ->with('orderGroup.zone')
            ->first();

        $activeOrder = $this->user->orders()
            ->latest()
            ->whereNull('management_date')
            ->whereNotIn('status', ['Delivered', 'Cancelled'])
            ->first();

        $lastOrderZone = empty($lastOrderCreated->orderGroup->zone)
            ? null : $lastOrderCreated->orderGroup->zone;
        $totalOrders = $this->user->orderCount;
        $lastOrderCreatedAtDate = $lastOrderCreated ? new Carbon($lastOrderCreated->created_at) : null;
        $lastOrderDeliveredAtDate = $lastOrderDelivered ? new Carbon($lastOrderDelivered->delivery_date) : null;

        $this->user->daysSinceLastDeliveredOrder = $lastOrderDeliveredAtDate
            ? $lastOrderDeliveredAtDate->diffInDays(Carbon::now())
            : null;
        $this->user->daysSinceLastOrder = $lastOrderCreatedAtDate
            ? $lastOrderCreatedAtDate->diffInDays(Carbon::now())
            : null;
        $this->user->lastOrderZone = empty($lastOrderZone)
            ? null
            : ['id' => $lastOrderZone->id, 'name' => $lastOrderZone->name];
        $this->user->averageDaysBetweenOrder = $this->user->averageDaysBetweenOrders();
        $this->user->currentOrderStatus = $lastOrderCreated ? $lastOrderCreated->status : null;

        $this->user->lastOrderTicket = $lastOrderCreated ? $lastOrderCreated->total_amount : null;
        $this->user->credit = $this->user->getCreditAvailable() ?: null;
        $this->user->hasOrdered = $totalOrders > 0 ? 1 : 0;
        $this->user->lastOrder = $lastOrderCreatedAtDate ? $lastOrderCreatedAtDate->format('Ymd') : null;
        $this->user->userValidated = !empty($this->user->phone_validated_date) ? 1 : 0;
        $this->user->warehouseId = $lastOrderCreated ? $lastOrderCreated->orderGroup->warehouse_id : null;
        $this->user->lastOrderDelivered = $lastOrderDeliveredAtDate ? $lastOrderDeliveredAtDate->format('Ymd') : null;
        $this->user->inProgressOrder = $this->user->orders()
            ->whereNotIn('status', [OrderStatus::DELIVERED, OrderStatus::CANCELED])
            ->count() > 0 ? 1 : 0;

        foreach ($attributes as $name => $value) {
            $this->user->$name = is_bool($value)
                // En Leanplum se almacenan los valores booleanos con 1 o 0.
                ? ($value ? 1 : 0)
                : $value;
        }
    }

    /**
     * @param $userId
     * @return void
     */
    public static function upload($userId)
    {
        Queue::later(
            self::queueDelay(),
            UpdateLeanplumUserJob::class,
            ['user_id' => $userId],
            'quick'
        );
    }

    /**
     * @return Carbon
     */
    private static function queueDelay()
    {
        $delay = Config::get('app.leanplum.update_delay');

        return Carbon::now()->addMinutes($delay);
    }
}
