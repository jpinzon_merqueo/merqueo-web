<?php

namespace actions;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class QuerysForServicesAndTemplatesSAPAction
{

    /**
     * Obtiene el nombre de la bodega asignado en SAP
     *
     * @param int $warehouseId
     * @return mixed
     */
    public function getWarehouseNameInSAP($warehouseId)
    {
        $warehouses = [
            1 => 'FERIAS',
            2 => 'MONTEVID',
            3 => 'MEDELLIN',
            4 => 'PV106',
            5 => 'CALLE13',
            6 => 'FONTIBON_CENTRAL',
            7 => 'MARKET1',
        ];

        return $warehouses[$warehouseId];
    }

    /**
     * Obtiene productos
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getProducts($start_date, $end_date)
    {
        $products = \Product::join('store_products', 'products.id', '=', 'store_products.product_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->select(DB::raw("
                        products.id AS ItemCode,
                        UPPER(products.name) AS ItemName,
                        departments.sap_item_group_code AS ItemsGroupCode,
                        products.reference AS BarCode,
                        'Y' AS PurchaseItem,
                        'Y' AS SalesItem,
                        'Y' AS InventoryItem,
                        IF(store_products.iva > 0, 'Y', 'N') AS VatLiable,
                        IF(store_products.iva > 0, 'Y', 'N') AS IndirectTax,
                        IF(store_products.iva = 0, 'IVAD0',
                            IF(store_products.iva = 5,
                              'IVADC5',
                              'IVADC19')
                            ) AS TaxCodeAP,
                        IF(store_products.iva = 0, 'IVAG0',
                            IF(store_products.iva = 5,
                              'IVAG5',
                              'IVAG19')
                            ) AS TaxCodeAR,
                        'C' AS GLMethod,
                        unit AS InventoryUOM,
                        IF(store_products.store_id = 63, 
                            'MONTEVID',  
                            'MEDELLIN') AS WhsCode")
            )
            ->where('products.type', 'Simple')
            ->groupBy('store_products.product_id')
            ->whereBetween('products.created_at', [$start_date, $end_date]);

        $productsManualInvoices = \ProductInvoice::select(
            DB::raw("
                        CONCAT('A', id) AS ItemCode,
                        UPPER(name) AS ItemName,
                        '' AS ItemsGroupCode,
                        '' AS BarCode,
                        'N' AS PurchaseItem,
                        'Y' AS SalesItem,
                        'N' AS InventoryItem,
                        'N' AS VatLiable,
                        'Y' AS IndirectTax,
                        'IVADS19' AS TaxCodeAP,
                        'IVAG19' AS TaxCodeAR,
                        'C' AS GLMethod,
                        '' AS InventoryUOM,
                        'MERQUEO' AS WhsCode"
            )
        )
            ->whereBetween('created_at', [$start_date, $end_date]);

        $returnServices = [];
        $products->chunk(10000, function ($rows) use (&$returnServices) {
            foreach ($rows as $row) {
                $returnServices[] = $row->toArray();
            }
        });

        $productsManualInvoices->chunk(10000, function ($rows) use (&$returnServices) {
            foreach ($rows as $row) {
                $returnServices[] = $row->toArray();
            }
        });

        return $returnServices;
    }

    /**
     * Obtiene los clientes que piden facturas
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getClients($start_date, $end_date)
    {
        $clients = \Order::with('user')
            ->whereNotNull('user_identity_number')
            ->whereBetween('management_date', [$start_date, $end_date])
            ->groupBy('user_identity_number')
            ->orderBy('management_date', 'desc')
            ->get(['id', 'user_id', 'user_identity_type', 'user_identity_number', 'user_business_name', 'management_date'])
            ->map(function ($client) {
                $identity_type = ($client->user_identity_type == 'Cédula Extranjería') ? ['CE', 102] : ['CN', 100];
                return [
                    'CardCode' => $identity_type[0] . preg_replace(["/[-|—|_| ]\d?/", "/\./"], '', $client->user_identity_number),
                    'CardName' => $client->user_business_name,
                    'CardType' => 'C',
                    'GroupCode' => $identity_type[1],
                    'DebitorAccount' => '13050505',
                    'Cellular' => $client->user->phone,
                    'CreditLimit' => '',
                    'Currency' => '$',
                    'EmailAddress' => $client->user->email,
                    'FederalTaxID' => $client->user_identity_number,
                    'PayTermsGrpCode' => '',
                    'Phone1' => '',
                    'Phone2' => '',
                    'Properties1' => '',
                    'Properties2' => '',
                    'Properties3' => '',
                    'WTLiable' => 'N',
                    'U_BPCO_City' => '',
                    'U_BPCO_CS' => '',
                    'U_BPCO_Nombre' => '',
                    'U_BPCO_1Apellido' => '',
                    'U_BPCO_2Apellido' => '',
                    'U_BPCO_BPExt' => '',
                    'U_BPCO_TP' => '',
                    'U_BPCO_RTC' => '',
                    'U_BPCO_TDC' => '',
                    'U_BPCO_Address' => '',
                    'Attachment' => '',
                    'CodPersonContact' => '',
                    'FirstName' => '',
                    'LastName' => '',
                    'Cellolar' => ''
                ];
            });

        $clientsTerceros = \CustomerInvoice::select(
            DB::raw("CONCAT('CN', document_number) AS CardCode,
                fullname AS CardName,
                'C' AS CardType,
                '100' AS GroupCode,
                '13050505' AS DebitorAccount,
                phone AS Cellular,
                '' AS CreditLimit,
                '$' AS Currency,
                email AS EmailAddress,
                document_number AS FederalTaxID,
                '' AS PayTermsGrpCode,
                '' AS Phone1,
                '' AS Phone2,
                '' AS Properties1,
                '' AS Properties2,
                '' AS Properties3,
                'N' AS WTLiable,
                '' AS U_BPCO_City,
                '' AS U_BPCO_CS,
                '' AS U_BPCO_Nombre,
                '' AS U_BPCO_1Apellido,
                '' AS U_BPCO_2Apellido,
                '' AS U_BPCO_BPExt,
                '' AS U_BPCO_TP,
                '' AS U_BPCO_RTC,
                '' AS U_BPCO_TDC,
                '' AS U_BPCO_Address,
                '' AS Attachment,
                '' AS CodPersonContact,
                '' AS FirstName,
                '' AS LastName,
                '' AS Cellolar"))
            ->whereBetween('created_at', [$start_date, $end_date])
            ->get();

        $serviceReturn = array_merge($clients->toArray(), $clientsTerceros->toArray());

        return $serviceReturn;
    }

    /**
     * Obtiene proveedores
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getProviders($start_date, $end_date)
    {
        $providers = \Provider::leftJoin('provider_contacts', 'providers.id', '=', 'provider_id')
            ->select(
                DB::raw("
                    CONCAT('PN', IF(providers.nit LIKE '%-%', REPLACE(SUBSTRING(providers.nit, 1, LENGTH(providers.nit)-2), '.', ''), providers.nit)) AS CardCode,
                    name AS CardName,
                    'S' AS CardType,
                    '101' AS GroupCode,
                    '22050105' AS DebitorAccount,
                    phone AS Cellular,
                    credit_limit AS CreditLimit,
                    '$' AS Currency,
                    email AS EmailAddress,
                    providers.nit AS FederalTaxID,
                    financial_condition AS PayTermsGrpCode,
                    '' AS Phone1,
                    '' AS Phone2,
                    '' AS Properties1,
                    '' AS Properties2,
                    '' AS Properties3,
                    'Y' AS WTLiable,
                    '' AS U_BPCO_City,
                    '' AS U_BPCO_CS,
                    '' AS U_BPCO_Nombre,
                    '' AS U_BPCO_1Apellido,
                    '' AS U_BPCO_2Apellido,
                    '' AS U_BPCO_BPExt,
                    '' AS U_BPCO_TP,
                    '' AS U_BPCO_RTC,
                    '' AS U_BPCO_TDC,
                    '' AS U_BPCO_Address,
                    '' AS Attachment,
                    '' AS CodPersonContact,
                    '' AS FirstName,
                    '' AS LastName,
                    '' AS Cellolar"))
            ->whereBetween('providers.created_at', [$start_date, $end_date])->get();
        return $providers;
    }

    /**
     * Obtiene las retenciones aplicadas a los proveedores
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getRetentionProviders($start_date, $end_date)
    {
        $providers = \ProviderOrderReception::join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
            ->join('providers', 'providers.id', '=', 'provider_orders.provider_id')
            ->where(function($where){
                $where->whereNotNull('provider_order_receptions.wtcode_ica')
                    ->orWhereNotNull('provider_order_receptions.wtcode_fuente');
            })
            ->where('provider_order_receptions.status', 'Contabilizado')
            ->whereBetween('provider_order_receptions.date', [$start_date, $end_date])
            ->select(
                DB::raw("
                CONCAT('PN', IF(providers.nit LIKE '%-%', 
                                REPLACE(SUBSTRING(providers.nit, 1, LENGTH(providers.nit)-2), '.', ''), 
                                providers.nit)
                                ) AS CardCode,
                '' AS retentionsList,
                provider_order_receptions.wtcode_ica,
                provider_order_receptions.wtcode_fuente
                ")
            );
        $returnService = [];

        $providers->chunk(10000, function ($rows) use (&$returnService) {
            $retentionsList = [];
            foreach ($rows as $row) {
                $retentionsList = [];
                if ($row->wtcode_ica) {
                    $retentionsList[]['WTCode'] = $row->wtcode_ica;
                }
                if ($row->wtcode_fuente) {
                    $retentionsList[]['WTCode'] = $row->wtcode_fuente;
                }
                $row->retentionsList = $retentionsList;
                unset($row->wtcode_ica, $row->wtcode_fuente);
                $returnService[] = $row->toArray();
            }
        });

        return $returnService;
    }

    /**
     * Obtiene facturas de compra
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getPurchaseInvoices($start_date, $end_date)
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(0);

        $purchase_invoices = \ProviderOrderReception::with(['providerOrder' => function($providerOrder) {
            $providerOrder->with('provider')->get(['id', 'provider_id', 'warehouse_id']);
        }])
            ->with(['providerOrderReceptionDetail' => function($detail) {
                $detail->with(['providerOrderReceptionDetailProductGroup' => function($group) {
                    $group->with('storeProduct')
                        ->select('id', 'status', 'reception_detail_id', 'store_product_id',
                            'quantity_received', 'quantity_expected', 'base_cost', 'iva');
                }])
                    ->with('storeProduct')
                    ->select('id', 'status', 'reception_id', 'store_product_id', 'quantity_received',
                        'quantity_expected', 'base_cost',  'iva', 'type');
            }])
            ->with('provider')
            ->where('status', 'Contabilizado')
            ->whereBetween('date', [$start_date, $end_date])
            ->get(['id', 'provider_order_id', 'real_provider_id', 'invoice_number', 'received_date', 'storage_date',
                'date', 'expiration_date', 'wtcode_ica', 'rete_ica_percentage', 'rete_ica_amount', 'status',
                'wtcode_fuente', 'rete_fuente_percentage', 'rete_fuente_amount'])
            ->map(function ($reception) use ($end_date) {
                $dateCalculateAmount = '2019-02-20';
                $identity = 'PN' . preg_replace(["/[-|—|_| ]\d?/", "/\./"],'',
                        ((!$reception->real_provider_id)
                            ? $reception->providerOrder->provider->nit
                            : $reception->provider->nit)
                    );
                $products = $reception->providerOrderReceptionDetail
                    ->filter(function ($product) {
                        return $product->status == 'Recibido' || $product->status == 'Parcialmente recibido';
                    })
                    ->all();
                $counter = 0;
                $returnMap = [];
                $base = $reception->getSubTotal() - $reception->getSubTotal(false);
                $retentionsList = [];

                if ($reception->rete_ica_amount) {
                    //Rete ICA
                    $icaTax = [];
                    $icaTax['WtCode'] = $reception->wtcode_ica;
                    $icaTax['Tarifa'] = $reception->rete_ica_percentage;
                    $icaTax['Base'] = $base;
                    $icaTax['Valor'] = round($reception->rete_ica_amount);
                    $retentionsList[] = $icaTax;
                }
                if ($reception->rete_fuente_amount) {
                    //Rete Fuente
                    $fuenteTax = [];
                    $fuenteTax['WtCode'] = $reception->wtcode_fuente;
                    $fuenteTax['Tarifa'] = $reception->rete_fuente_percentage;
                    $fuenteTax['Base'] = $base;
                    $fuenteTax['Valor'] = round($reception->rete_fuente_amount);
                    $retentionsList[] = $fuenteTax;
                }

                foreach ($products as $product) {
                    $counter++;
                    if ($product->type === 'Proveedor') {
                        $productsGroup = $product->providerOrderReceptionDetailProductGroup
                            ->filter(function ($productGroup) {
                                return $productGroup->status == 'Recibido' || $productGroup->status == 'Parcialmente recibido';
                            })
                        ->all();
                        foreach ($productsGroup as $productGroup) {
                            $returnMap[] = [
                                'DocNum' => $reception->invoice_number,
                                'CardCode' => $identity,
                                'DocDate' => $reception->received_date,
                                'TaxDate' => $reception->storage_date,
                                'DocDueDate' => $reception->expiration_date,
                                'NumAtCard' => '',
                                'DocCur' => '$',
                                'LineNum' => $counter,
                                'ItemCode' => $productGroup->storeProduct->product_id,
                                'Quantity' => (explode(" ", $reception->date)[0] > $dateCalculateAmount)
                                    ? $productGroup->quantity_expected
                                    : $productGroup->quantity_received,
                                'WarehouseCode' => $this->getWarehouseNameInSAP($reception->providerOrder->warehouse_id),
                                'Price' => round($productGroup->base_cost),
                                'TaxCode' => (round($productGroup->base_cost) == 0 || $productGroup->iva == 0)
                                    ? 'IVAD0'
                                    : (($productGroup->iva == 5) ? 'IVADC5' : 'IVADC19'),
                                'WTLiable' => ($productGroup->iva == 0) ? 'N' : 'Y',
                                'CostingCode' => ($reception->providerOrder->warehouse_id == 3) ? 64 : 63,
                                'retentionsList' => $retentionsList
                            ];
                            $counter++;
                        }
                    } else {
                        $returnMap[] = [
                            'DocNum' => $reception->invoice_number,
                            'CardCode' => $identity,
                            'DocDate' => $reception->received_date,
                            'TaxDate' => $reception->storage_date,
                            'DocDueDate' => $reception->expiration_date,
                            'NumAtCard' => '',
                            'DocCur' => '$',
                            'LineNum' => $counter,
                            'ItemCode' => $product->storeProduct->product_id,
                            'Quantity' => (explode(" ", $reception->date)[0] > $dateCalculateAmount)
                                ? $product->quantity_expected
                                : $product->quantity_received,
                            'WarehouseCode' => $this->getWarehouseNameInSAP($reception->providerOrder->warehouse_id),
                            'Price' => round($product->base_cost),
                            'TaxCode' => (round($product->base_cost) == 0 || $product->iva == 0)
                                ? 'IVAD0'
                                : (($product->iva == 5) ? 'IVADC5' : 'IVADC19'),
                            'WTLiable' => ($product->iva == 0) ? 'N' : 'Y',
                            'CostingCode' => ($reception->providerOrder->warehouse_id == 3) ? 64 : 63,
                            'retentionsList' => $retentionsList
                        ];
                    }
                }

                return $returnMap;
            })->collapse();

        return $purchase_invoices;
    }

    /**
     * Obtiene facturas de venta
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getSaleInvoices($start_date, $end_date)
    {
        ini_set('memory_limit', '3072M');
        set_time_limit(0);

        $saleOrderInvoices = \Order::with('orderGroup')
            ->with(['orderProducts' => function ($orderProduct) {
                $orderProduct->with('storeProduct')
                    ->where('type', 'Product')
                    ->where('fulfilment_status', 'Fullfilled')
                    ->select('id', 'order_id', 'store_product_id', 'type', 'quantity', 'base_price', 'iva', 'fulfilment_status');
            }])->with(['orderProductGroup' => function ($orderProductGroup) {
                $orderProductGroup->with('storeProduct')
                    ->where('fulfilment_status', 'Fullfilled')
                    ->select('id', 'order_id', 'store_product_id', 'quantity', 'base_price', 'iva', 'fulfilment_status');
            }])->where('status', 'Delivered')
            ->whereNotNull('invoice_number')
            ->whereBetween('management_date', [$start_date, $end_date])
            ->orderBy('invoice_number')
            ->get(['id', 'group_id', 'invoice_number', 'management_date', 'status', 'discount_amount',
                'delivery_amount', 'user_identity_type', 'user_identity_number', 'user_business_name'])
            ->map(function ($order) {
                $identity = 'CN222222222';
                if ($order->user_identity_number) {
                    $identity = preg_replace(["/[-|—|_| ]\d?/", "/\./"], '', $order->user_identity_number);
                    $identity = ($order->user_identity_type == 'Cédula Extranjería') ?
                        'CE' . $identity :
                        'CN' . $identity;
                }
                $allProducts = array_merge($order->orderProducts->all(), $order->orderProductGroup->all());
                $returnMap = [];
                $counter = 0;
                foreach ($allProducts as $product) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => $order->invoice_number,
                        'CardCode' => $identity,
                        'DocDate' => $order->management_date,
                        'TaxDate' => $order->management_date,
                        'DocDueDate' => $order->management_date,
                        'NumAtCard' => '',
                        'DocCur' => '$',
                        'ExpensesCode' => '',
                        'DistributionRule' => '',
                        "CodeDescuento" => ($order->discount_amount == 0) ? "" : "2",
                        "TotalDescuento" => $order->discount_amount,
                        "CodeDomicilio" => ($order->delivery_amount == 0) ? "" : "1",
                        "TotalDomicilio" => $order->delivery_amount,
                        'LineNum' => $counter,
                        'ItemCode' => $product->storeProduct->product_id,
                        'Quantity' => $product->quantity,
                        'WarehouseCode' => $this->getWarehouseNameInSAP($order->orderGroup->warehouse_id),
                        'Price' => round($product->base_price),
                        'TaxCode' => (round($product->base_price) == 0 || $product->iva == 0)
                            ? 'IVAG0'
                            : (($product->iva == 5) ? 'IVAG5' : 'IVAG19'),
                        'WTLiable' => ($product->iva == 0) ? 'N' : 'Y',
                        'CostingCode' => $product->storeProduct->store_id,
                        'WtCode' => '',
                        'Tarifa' => '',
                        'Base' => ''
                    ];
                }

                return $returnMap;
            })->collapse();

        $saleManualInvoices = \Invoice::with('invoiceDetails')->with('customerInvoice')
            ->whereBetween('date', [$start_date, $end_date])
            ->get()
            ->map(function ($invoice) {
                $date = Carbon::parse($invoice->date)->toDateString();
                $expirationDate = Carbon::parse($invoice->expiration_date)->toDateString();
                $invoiceDetails = $invoice->invoiceDetails->all();
                $returnMap = [];
                $counter = 0;
                foreach ($invoiceDetails as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => $invoice->number,
                        'CardCode' => $invoice->customerInvoice->document_number,
                        'DocDate' => $date,
                        'TaxDate' => $date,
                        'DocDueDate' => $expirationDate,
                        'NumAtCard' => '',
                        'DocCur' => '$',
                        'ExpensesCode' => '',
                        'DistributionRule' => '',
                        "CodeDescuento" => "",
                        "TotalDescuento" => 0,
                        "CodeDomicilio" => "",
                        "TotalDomicilio" => 0,
                        'LineNum' => $counter,
                        'ItemCode' => 'A' . $detail->product_invoice_id,
                        'Quantity' => $detail->quantity,
                        'WarehouseCode' => 'FERIAS',
                        'Price' => round($detail->price),
                        'TaxCode' => (round($detail->price) == 0 || $detail->iva == 0)
                            ? 'IVAG0'
                            : (($detail->iva == 5) ? 'IVAG5' : 'IVAG19'),
                        'WTLiable' => ($detail->iva == 0) ? 'N' : 'Y',
                        'CostingCode' => 63,
                        'WtCode' => '',
                        'Tarifa' => '',
                        'Base' => ''
                    ];
                }

                return $returnMap;
            })->collapse();

        $sale_invoices = array_merge($saleOrderInvoices->toArray(), $saleManualInvoices->toArray());

        return $sale_invoices;
    }

    /**
     * Obtiene notas crédito de ventas
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getCreditNoteSale($start_date, $end_date)
    {
        $credit_note_sale = \Order::with(['orderProducts' => function ($query) {
            $query->with('storeProduct')
                ->where('type', 'Product')
                ->where('fulfilment_status', 'Fullfilled')
                ->select('id', 'order_id', 'store_product_id', 'type', 'quantity_credit_note',
                    'base_price', 'iva', 'fulfilment_status');
        }])->with(['orderProductGroup' => function ($query) {
            $query->with('storeProduct')
                ->where('fulfilment_status', 'Fullfilled')
                ->select('id', 'order_id', 'store_product_id', 'quantity_credit_note',
                    'base_price', 'iva', 'fulfilment_status');
        }])->with('orderGroup')
            ->where('status', 'Delivered')
            ->whereNotNull('credit_note_number')
            ->whereBetween('credit_note_date', [$start_date, $end_date])
            ->get(['id', 'group_id', 'credit_note_number', 'invoice_number',
                'credit_note_date', 'credit_note_date', 'credit_note_date'])
            ->map(function ($order) {
                $allProducts = array_merge($order->orderProducts->all(), $order->orderProductGroup->all());
                $returnMap = [];
                $counter = 0;
                foreach ($allProducts as $product) {
                    $counter++;
                    if ($product->quantity_credit_note) {
                        $returnMap[] = [
                            'DocNum' => $order->credit_note_number,
                            'NumFact' => $order->invoice_number,
                            'DocDate' => $order->credit_note_date,
                            'TaxDate' => $order->credit_note_date,
                            'DocDueDate' => $order->credit_note_date,
                            'DocCur' => '$',
                            'ExpensesCode' => '',
                            'DistributionRule' => '',
                            'TipoNota' => 'D',
                            'LineNum' => $counter,
                            'ItemCode' => $product->storeProduct->product_id,
                            'Quantity' => $product->quantity_credit_note,
                            'WarehouseCode' => $this->getWarehouseNameInSAP($order->orderGroup->warehouse_id),
                            'price' => round($product->base_price),
                            'TaxCode' => (round($product->base_price) == 0 || $product->iva == 0)
                                ? 'IVAG0'
                                : (($product->iva == 5) ? 'IVAG5' : 'IVAG19'),
                            'WTLiable' => ($product->iva == 0) ? 'N' : 'Y',
                            'CostingCode' => $product->storeProduct->store_id,
                            'WtCode' => '',
                            'Tarifa' => '',
                            'Base' => ''
                        ];
                    }
                }
                return $returnMap;
            })->collapse();

        return $credit_note_sale;
    }

    /**
     * Obtiene notas crédito de compras
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getCreditNotePurchase($start_date, $end_date)
    {
        $credit_note_purchase = \ProviderOrderReception::with(['providerOrderReceptionDetail' => function ($products) {
            $products->with('providerOrderReceptionDetailProductGroup');
        }])->with('providerOrder')
            ->where('status', 'Contabilizado')
            ->whereNotNull('credit_note_number')
            ->whereBetween('credit_note_date', [$start_date, $end_date])
            ->get(['id', 'provider_order_id', 'received_date', 'date', 'storage_date', 'invoice_number',
                'expiration_date', 'credit_note_date', 'credit_note_number', 'status'])
            ->map(function ($reception) {
                $products = $reception->providerOrderReceptionDetail
                    ->filter(function ($product) {
                        return $product->status == 'Recibido' || $product->status == 'Parcialmente recibido';
                    })
                    ->all();
                $returnMap = [];
                $counter = 0;
                foreach ($products as $product) {
                    $counter++;
                    if ($product->type == 'Proveedor') {
                        $productsGroup = $product->providerOrderReceptionDetailProductGroup
                            ->filter(function ($productGroup) {
                                return $productGroup->status == 'Recibido' || $productGroup->status == 'Parcialmente recibido';
                            })
                            ->all();
                        foreach ($productsGroup as $productGroup) {
                            if ($productGroup->quantity_credit_note) {
                                $returnMap[] = [
                                    'DocNum' => $reception->credit_note_number,
                                    'NumFact' => $reception->invoice_number,
                                    'DocDate' => $reception->received_date,
                                    'TaxDate' => $reception->storage_date,
                                    'DocDueDate' => $reception->expiration_date,
                                    'DocCur' => '$',
                                    'TipoNota' => 'D',
                                    'LineNum' => $counter,
                                    'ItemCode' => $productGroup->storeProduct->product_id,
                                    'Quantity' => $productGroup->quantity_credit_note,
                                    'WarehouseCode' => $this->getWarehouseNameInSAP($reception->providerOrder->warehouse_id),
                                    'Price' => round($productGroup->base_cost),
                                    'TaxCode' => (round($productGroup->base_cost) == 0 || $productGroup->iva == 0)
                                        ? 'IVAD0'
                                        : (($productGroup->iva == 5) ? 'IVADC5' : 'IVADC19'),
                                    'WTLiable' => ($productGroup->iva == 0) ? 'N' : 'Y',
                                    'CostingCode' => ($reception->providerOrder->warehouse_id == 3) ? 64 : 63,
                                    'WtCode' => '',
                                    'Tarifa' => '',
                                    'Base' => ''
                                ];
                            }
                            $counter++;
                        }
                    } else {
                        if ($product->quantity_credit_note) {
                            $returnMap[] = [
                                'DocNum' => $reception->credit_note_number,
                                'NumFact' => $reception->invoice_number,
                                'DocDate' => $reception->received_date,
                                'TaxDate' => $reception->storage_date,
                                'DocDueDate' => $reception->expiration_date,
                                'DocCur' => '$',
                                'TipoNota' => 'D',
                                'LineNum' => $counter,
                                'ItemCode' => $product->storeProduct->product_id,
                                'Quantity' => $product->quantity_credit_note,
                                'WarehouseCode' => $this->getWarehouseNameInSAP($reception->providerOrder->warehouse_id),
                                'Price' => round($product->base_cost),
                                'TaxCode' => (round($product->base_cost) == 0 || $product->iva == 0)
                                    ? 'IVAD0'
                                    : (($product->iva == 5) ? 'IVADC5' : 'IVADC19'),
                                'WTLiable' => ($product->iva == 0) ? 'N' : 'Y',
                                'CostingCode' => ($reception->providerOrder->warehouse_id == 3) ? 64 : 63,
                                'WtCode' => '',
                                'Tarifa' => '',
                                'Base' => ''
                            ];
                        }
                    }
                }
                return $returnMap;
            })->collapse();

        return $credit_note_purchase;
    }

    /**
     * Obtiene las entradas de inventario
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getInventoryEntry($start_date, $end_date)
    {
        /********************** Traslados *************************/

        $transfers = \ProductTransfer::with(['productTransferDetails' => function ($details) {
            $details->with(['storeProduct' => function ($storeProduct) {
                $storeProduct->get(['id', 'product_id', 'base_cost']);
            }])->get();
        }])->where('status','Storaged')
            ->whereBetween('created_at', [$start_date, $end_date])
            ->get()
            ->map(function ($transfer) {
                $details = $transfer->productTransferDetails
                    ->filter(function ($detail) {
                        return ($detail->status == 'Received' || $detail->status == 'Partially received')
                            && $detail->quantity_storaged > 0;
                    })->all();

                $counter = 0;
                $returnMap = [];

                foreach ($details as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => 'T' . $transfer->id,
                        'DocDate' => $transfer->created_at->toDateTimeString(),
                        'TaxDate' => $transfer->created_at->toDateTimeString(),
                        'LineNum' => $counter,
                        'ItemCode' => $detail->storeProduct->product_id,
                        'Quantity' => $detail->quantity_storaged,
                        'WarehouseCode' => $this->getWarehouseNameInSAP($transfer->destiny_warehouse_id),
                        'Price' => round($detail->storeProduct->base_cost)
                    ];
                }

                return $returnMap;
            })
            ->collapse();

        /********************* Conteos ***********************/

        $counts = \InventoryCountingPosition::with(['inventoryCountingPositionDetails' => function ($details) {
            $details->with(['storeProduct' => function ($storeProduct) {
                $storeProduct->get(['id', 'product_id', 'base_cost']);
            }])->get(['id', 'inventory_counting_position_id', 'store_product_id', 'quantity_original',
                'quantity_counted', 'picking_original', 'picking_counted']);
        }])
            ->where('status', 'Cerrado')
            ->whereBetween('end_count_date', [$start_date, $end_date])
            ->get(['id', 'count_date', 'end_count_date', 'action', 'warehouse_id'])
            ->map(function ($counting) {
                $details = $counting->inventoryCountingPositionDetails
                    ->map(function ($detail) use ($counting) {
                        $detail->quantity = ($counting->action === 'Almacenado')
                            ? ($detail->quantity_original - $detail->quantity_counted)
                            : ($detail->picking_original - $detail->picking_counted);

                        return $detail;
                    })
                    ->filter(function ($detail) {
                        return $detail->quantity > 0;
                    })
                    ->all();

                $counter = 0;
                $returnMap = [];

                foreach ($details as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => 'C' . $counting->id,
                        'DocDate' => $counting->count_date,
                        'TaxDate' => $counting->end_count_date,
                        'LineNum' => $counter,
                        'ItemCode' => $detail->storeProduct->product_id,
                        'Quantity' => $detail->quantity,
                        'WarehouseCode' => $this->getWarehouseNameInSAP($counting->warehouse_id),
                        'Price' => round($detail->storeProduct->base_cost)
                    ];
                }
                return $returnMap;
            })
            ->collapse();

        /****************** Devoluciones**************************/

        $orderReturn = \OrderReturn::with(['orderReturnDetail' =>  function($details) {
            $details->with(['storeProduct' => function ($storeProduct) {
                $storeProduct->get(['id', 'product_id', 'base_cost']);
            }])
                ->where('status', 'Devuelto')
                ->groupBy('order_return_id', 'store_product_id')
                ->get(['id', 'order_return_id', 'store_product_id',
                    DB::raw('COUNT(store_product_id) AS quantity')]);
        }])->with(['order.orderGroup' => function($orderGroup) {
            $orderGroup->get(['id', 'warehouse_id']);
        }])
            ->whereHas('order', function ($order) {
                $order->whereNull('credit_note_number');
            })
            ->where('status', 'Almacenado')
            ->whereBetween('storage_date', [$start_date, $end_date])
            ->orderBy('id')
            ->get(['id', 'order_id', 'status', 'reject_reason', 'storage_date'])
            ->map(function ($orderReturn) {
                $details = $orderReturn->orderReturnDetail->all();

                $returnMap = [];
                $counter = 0;

                foreach ($details as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => 'D' . $orderReturn->id,
                        'DocDate' => $orderReturn->storage_date,
                        'TaxDate' => $orderReturn->storage_date,
                        'LineNum' => $counter,
                        'ItemCode' => $detail->storeProduct->product_id,
                        'Quantity' => $detail->quantity,
                        'WarehouseCode' => $this->getWarehouseNameInSAP($orderReturn->order->orderGroup->warehouse_id),
                        'Price' => round($detail->storeProduct->base_cost)
                    ];
                }
                return $returnMap;
            })
            ->collapse();

        $inventory_entry = array_merge($transfers->toArray(), $counts->toArray(), $orderReturn->toArray());

        return $inventory_entry;
    }

    /**
     * Obtiene las salida de inventario
     *
     * @param $start_date
     * @param $end_date
     * @return string
     */
    public function getInventoryOutput($start_date, $end_date)
    {
        /********************** Traslados *************************/

        $transfers = \ProductTransfer::with(['productTransferDetails' => function ($details) {
            $details->with(['storeProduct' => function ($storeProduct) {
                $storeProduct->get(['id', 'product_id', 'base_cost']);
            }])->get();
        }])->where('status','Storaged')
            ->whereBetween('created_at', [$start_date, $end_date])
            ->get()
            ->map(function ($transfer) {
                $details = $transfer->productTransferDetails
                    ->filter(function ($detail) {
                        return ($detail->status == 'Received' || $detail->status == 'Partially received')
                            && $detail->quantity_storaged > 0;
                    })->all();

                $counter = 0;
                $returnMap = [];

                foreach ($details as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => 'T' . $transfer->id,
                        'DocDate' => $transfer->created_at->toDateTimeString(),
                        'TaxDate' => $transfer->created_at->toDateTimeString(),
                        'LineNum' => $counter,
                        'ItemCode' => $detail->storeProduct->product_id,
                        'Quantity' => $detail->transferred_quantity,
                        'WarehouseCode' => $this->getWarehouseNameInSAP($transfer->origin_warehouse_id),
                        'Price' => round($detail->storeProduct->base_cost)
                    ];
                }

                return $returnMap;
            })
            ->collapse();

        /***************************** Conteo ****************************/

        $counts = \InventoryCountingPosition::with(['inventoryCountingPositionDetails' => function ($details) {
            $details->with(['storeProduct' => function ($storeProduct) {
                $storeProduct->get(['id', 'product_id']);
            }])
                ->get(['id', 'inventory_counting_position_id', 'store_product_id', 'quantity_original',
                    'quantity_counted', 'picking_original', 'picking_counted']);
        }])
            ->where('status', 'Cerrado')
            ->whereBetween('end_count_date', [$start_date, $end_date])
            ->get(['id', 'count_date', 'end_count_date', 'action', 'warehouse_id'])
            ->map(function ($counting) {
                $details = $counting->inventoryCountingPositionDetails
                    ->map(function ($detail) use ($counting) {
                        $detail->quantity = ($counting->action === 'Almacenado')
                            ? ($detail->quantity_original - $detail->quantity_counted)
                            : ($detail->picking_original - $detail->picking_counted);

                        return $detail;
                    })
                    ->filter(function ($detail) {
                        return $detail->quantity < 0;
                    })
                    ->all();

                $counter = 0;
                $returnMap = [];

                foreach ($details as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => 'C' . $counting->id,
                        'DocDate' => $counting->count_date,
                        'TaxDate' => $counting->end_count_date,
                        'LineNum' => $counter,
                        'ItemCode' => $detail->storeProduct->product_id,
                        'Quantity' => ($detail->quantity * (-1)),
                        'WarehouseCode' => $this->getWarehouseNameInSAP($counting->warehouse_id),
                        'AccountCode' => '14651005'
                    ];
                }
                return $returnMap;
            })
            ->collapse();

        /*************************** Merma *********************************/

        $shrinkage = \Shrinkage::with(['shrinkageDetail' => function($details) {
            $details->with(['storeProduct' => function($storeProduct) {
                $storeProduct->get(['id', 'product_id']);
            }]);
        }])
            ->where('status', 'Cerrada')
            ->whereBetween('close_date', [$start_date, $end_date])
            ->get()
            ->map(function ($shrinkage) {
                $details = $shrinkage->shrinkageDetail->all();
                $returnMap = [];
                $counter = 0;

                foreach ($details as $detail) {
                    $counter++;
                    $returnMap[] = [
                        'DocNum' => 'M' . $shrinkage->id,
                        'DocDate' => $shrinkage->close_date,
                        'TaxDate' => $shrinkage->close_date,
                        'LineNum' => $counter,
                        'ItemCode' => $detail->storeProduct->product_id,
                        'Quantity' => $detail->quantity_reported,
                        'WarehouseCode' => $this->getWarehouseNameInSAP($shrinkage->warehouse_id),
                        'AccountCode' => '14651005'
                    ];
                }

                return $returnMap;
            })
            ->collapse();

        $inventory_output = array_merge($transfers->toArray(), $counts->toArray(), $shrinkage->toArray());

        return $inventory_output;
    }

    /**
     * Obtiene los pagos recibidos
     *
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    public function getPaymentsReceived($start_date, $end_date)
    {
        ini_set('memory_limit', '3072M');
        set_time_limit(0);

        $payments_received = \Order::where('status', 'Delivered')
            ->whereNotNull('invoice_number')
            ->whereNotNull('payment_date')
            ->where('is_checked', 1)
            ->whereBetween('payment_date', [$start_date, $end_date])
            ->get(['id', 'group_id', 'vehicle_id', 'driver_id', 'credit_card_id', 'payment_date', 'invoice_number',
                'total_amount', 'status', 'user_identity_type', 'user_identity_number', 'delivery_amount',
                'cc_last_four', 'discount_amount', 'payment_method', 'is_checked'])
            ->map(function ($order) {
                $identity = 'CN222222222';
                if ($order->user_identity_number) {
                    $identity = preg_replace(["/[-|—|_| ]\d?/", "/\./"], '', $order->user_identity_number);
                    $identity = ($order->user_identity_type == 'Cédula Extranjería') ?
                        'CE' . $identity :
                        'CN' . $identity;
                }
                $total = round(($order->total_amount + $order->delivery_amount - $order->discount_amount));
                if ($total > 0) {
                    $paymentsList = [];
                    if ($order->payment_method == 'Datáfono') {
                        //Pago Datáfono
                        $paymentsList[] = [
                            'SumApplied' => $total,
                            'MediodePago' => 'Transferencia',
                            'Account' => '11050504',
                            'TransferDate' => '',
                            'Remarks' => '',
                            'Reference1' => '',
                            'Reference2' => '',
                            'CreditCardNumber' => '',
                            'CardValidUntil' => ''
                        ];
                    } elseif ($order->payment_method == 'Tarjeta de crédito') {
                        //Pago Tarjeta de crédito
                        $paymentsList[] = [
                            'SumApplied' => $total,
                            'MediodePago' => 'Tarjeta',
                            'Account' => '11050510',
                            'TransferDate' => '',
                            'Remarks' => '',
                            'Reference1' => '',
                            'Reference2' => '',
                            'CreditCardNumber' => $order->cc_last_four,
                            'CardValidUntil' => '2030-09-30'
                        ];
                    } elseif ($order->payment_method == 'Efectivo y datáfono') {
                        $movement = \VehicleMovement::where('order_id', $order->id)
                            ->where('vehicle_id', $order->vehicle_id)
                            ->where('driver_id', $order->driver_id)
                            ->whereStatus(1)->first();

                        //Pago Datáfono
                        $paymentsList[] = [
                            'SumApplied' => $movement->user_card_paid,
                            'MediodePago' => 'Transferencia',
                            'Account' => '11050504',
                            'TransferDate' => '',
                            'Remarks' => '',
                            'Reference1' => '',
                            'Reference2' => '',
                            'CreditCardNumber' => '',
                            'CardValidUntil' => ''
                        ];
                        //Pago Efectivo
                        $paymentsList[] = [
                            'SumApplied' => $movement->user_cash_paid,
                            'MediodePago' => 'Efectivo',
                            'Account' => '11050503',
                            'TransferDate' => '',
                            'Remarks' => '',
                            'Reference1' => '',
                            'Reference2' => '',
                            'CreditCardNumber' => '',
                            'CardValidUntil' => ''
                        ];
                    } elseif ($order->payment_method == 'Débito - PSE') {
                        //Pago por PSE
                        $paymentsList[] = [
                            'SumApplied' => $total,
                            'MediodePago' => 'Transferencia',
                            'Account' => '11050509',
                            'TransferDate' => '',
                            'Remarks' => '',
                            'Reference1' => '',
                            'Reference2' => '',
                            'CreditCardNumber' => '',
                            'CardValidUntil' => ''
                        ];
                    } else {
                        //Pago en Efectivo
                        $paymentsList[] = [
                            'SumApplied' => $total,
                            'MediodePago' => $order->payment_method,
                            'Account' => '11050503',
                            'TransferDate' => '',
                            'Remarks' => '',
                            'Reference1' => '',
                            'Reference2' => '',
                            'CreditCardNumber' => '',
                            'CardValidUntil' => ''
                        ];
                    }

                    return [
                        'DocNum' => $order->invoice_number,
                        'CardCode' => $identity,
                        'DocDate' => $order->payment_date,
                        'DocDueDate' => '',
                        'TaxDate' => $order->payment_date,
                        'CounterRef' => '',
                        'PaymentsList' => $paymentsList
                    ];
                }
            });

        return $payments_received;
    }
}