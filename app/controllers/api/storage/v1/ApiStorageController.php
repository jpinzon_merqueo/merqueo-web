<?php

namespace api\storage\v1;

use Input, Config, DB, Carbon\Carbon, Picker, PickerStorage, Response, Request;

class ApiStorageController extends \ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->beforeFilter(function() {
            $headers = getallheaders();
            if (isset($headers['Authorization']) || isset($headers['authorization']))
            {
                $token = isset($headers['Authorization']) ? $headers['Authorization'] : $headers['authorization'];
                try{
                    $token = base64_decode($token);
                }catch(Exception $e){
                    return $this->jsonResponse('Acceso denegado.', 401);
                }
                $token = explode('|', $token);
                if (count($token) == 4) {
                    $datetime_device = round($token[0]/1000);
                    $datetime_server = strtotime(date('Y-m-d H:i:s'));
                    $interval = abs($datetime_device - $datetime_server);
                    $minutes = date('i', round($interval/60));
                    if ($minutes > 30)
                        return $this->jsonResponse('Acceso denegado.', 401);
                    if ($token[1] != Config::get('app.api.key'))
                        return $this->jsonResponse('Acceso denegado.', 401);
                    if ($token[2] != Config::get('app.api.public_key'))
                        return $this->jsonResponse('Acceso denegado.', 401);
                    if ($token[3] != Config::get('app.api.iv'))
                        return $this->jsonResponse('Acceso denegado.', 401);

                }else return $this->jsonResponse('Acceso denegado.', 401);

            }else return $this->jsonResponse('Acceso denegado.', 401);
        });
    }

    /**
     * ApiStorageController constructor.
     */

    /**
     * Convierte un string base64 en archivo de imagen.
     * @param $base64_string
     * @param $output_file
     * @return bool
     */
    function base64_to_jpeg($base64_string, $output_file)
    {
        list($type, $base64_string) = explode(';', $base64_string);
        list(, $base64_string)      = explode(',', $base64_string);
        $base64_string = str_replace(' ', '+', $base64_string);
        $base64_string = base64_decode($base64_string);
        file_put_contents($output_file, $base64_string);

        if (file_exists($output_file)) return true;
        else return false;
    }

    /**
     * @param $identity_number
     * @return string
     */
    public function get_picker_storage($identity_number)
    {
        if (!$picker = Picker::where('identity_number', $identity_number)->first())
            return Response::json(array('status'=> 400, 'message'=>'Picker no existe'));
        else return Response::json(array('status'=>200, 'message'=>'Informacion del picker', 'result'=>array('picker_id'=>$picker->id)));
    }

    /**
     * @param $identity_number
     * @return string
     */
    public function upload_storage_image($identity_number)
    {
        if (!Picker::where('identity_number', $identity_number)->first())
            return Response::json(array('status'=> 400, 'message'=>'Picker no existe'));

        $file_name = $identity_number.'-'.rand().'.jpg';
        $dir = 'storage/'.date('Y-m-d');

        if (!Input::has('image'))
            return $this->jsonResponse('No trae informacion de la imagen', 400);

        $image = Input::get('image');
        $path = public_path()."/uploads/storage/$file_name";
        $generate_image = $this->base64_to_jpeg($image, $path);

        if (!$generate_image)
            return Response::json(array('status'=> 400, 'message'=>'No genero la imagen'));

        $extension = pathinfo($file_name);
        $file_data['real_path'] = realpath($path);
        $file_data['client_original_name'] = basename($file_name);
        $file_data['client_original_extension'] = $extension['extension'];
        $url = upload_image($file_data, false, $dir);

        if($url)
            return Response::json(array('status'=>200, 'message'=>'Creacion de imagen correcta', 'result'=>array('url'=>$url)));
    }

    /**
     * @description salvar
     * @return string
     */
    public function save_picker_storage()
    {
        try {
            $picker_storage = new PickerStorage();
            if (!Input::has('type'))
                return Response::json(array('message'=>'No indica el tipo de accion Entrada/Salida', 'status' => 400));
            if (!Input::has('picker_id'))
                return Response::json(array('message'=>'No indica el picker asociado a la actividad', 'status' => 400));
            if (!Input::has('image'))
                return Response::json(array('message'=>'No envia URL de imagen', 'status' => 400));
            if (PickerStorage::select('id')->where('picker_storage.picker_id', Input::get('picker_id'))->where('picker_storage.created_at', date('Y-m-d H:i:s'))->first())
                return Response::json(array('message'=>'El usuario ya registro acceso', 'status' => 400));
            if (DB::select('SELECT id FROM picker_storage WHERE created_at BETWEEN "'.Carbon::today().'" AND "'.Carbon::now().'" AND picker_id ='.Input::get('picker_id').'  AND type = "'.Input::get('type').'" ORDER BY created_at DESC LIMIT 1;'))
                if (Input::get('type')=='Entrada')
                    return Response::json(array('message'=>'Ya registra ingreso el día de hoy', 'status' => 400));
                else
                    return Response::json(array('message'=>'Ya registra salida el día de hoy', 'status' => 400));

            $picker_storage->type = Input::get('type');
            $picker_storage->module = Input::get('module');
            $picker_storage->picker_id = Input::get('picker_id');
            $picker_storage->url_photo = Input::get('image');
            $picker_storage->save();
            if (Input::get('type')=='Entrada')
                return Response::json(array('message'=>'Registro exitoso de ingreso a turno el día de hoy', 'status' => 200));
            else
                return Response::json(array('message'=>'Registro exitoso de salida de turno el día de hoy', 'status' => 200));
        }
        catch(Exception $e) {
            DB::rollback();
            return Response::json(array('status'=> 500, 'message'=>'Ocurrio un problema a registrar la informacion'));
        }
    }

    /**
     * @description actualizar estado de pausa
     * @return mixed
     */
    public function halt_picker_storage()
    {
        try {
            DB::beginTransaction();
            if (!Picker::where('identity_number', (int)Input::get('identity_number'))->first())
                return Response::json(array('status' => 400, 'message' => 'Picker no existe'));
            $picker = Picker::where('identity_number', (int)Input::get('identity_number'))->first();

            if (!Input::has('code'))
                return Response::json(array('status' => 400, 'message' => 'No indica código'));

            $code = '0'.((strlen($picker->identity_number)>4)?substr($picker->identity_number, -4):$picker->identity_number);

            if (base64_decode(Input::get('code')) != $code)
                return Response::json(array('status' => 400, 'message' => 'Código incorrecto'));

            if (!DB::select('SELECT id FROM picker_storage WHERE created_at BETWEEN "' . Carbon::today() . '" AND "' . Carbon::now() . '" AND picker_id = ' . $picker->id . ' ORDER BY created_at DESC LIMIT 1;'))
                return Response::json(array('status' => 200, 'message' => 'Picker no registra actividad el dia de hoy'));

            if (DB::select('SELECT id FROM picker_storage WHERE created_at BETWEEN "' . Carbon::today() . '" AND "' . Carbon::now() . '" AND picker_id =' . $picker->id . '  AND halt = 1 ORDER BY created_at DESC LIMIT 1;'))
                return Response::json(array('status' => 200, 'message' => 'Picker ya fue pausada su actividad el dia de hoy'));

            $picker_storage = PickerStorage::where('picker_id', $picker->id)->whereBetween('created_at', array(Carbon::today(), Carbon::now()))->first();;
            $picker_storage->halt = 1;
            $picker_storage->save();
            DB::commit();
            return Response::json(array('message' => 'Registro exitoso de pausa en el turno el día de hoy', 'status' => 200));
        } catch (\Exception $exception)
        {
            return Response::json(array('status' => 400, 'message' => 'Error al crear registro '.$exception));
        }
    }
}