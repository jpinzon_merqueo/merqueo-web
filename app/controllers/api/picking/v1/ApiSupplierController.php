<?php

namespace api\picking\v1;

use View, City, Store, Request, Order, DB, Input, Response, Session, OrderProduct, Redirect, OrderProductGroup,
    Carbon\Carbon, OrderLog, Product, Exception, Routes, StoreProductGroup, OrderPickingQualityDetail, OrderPickingQuality, Warehouse, StoreProductWarehouse, OrderGroup;

class ApiSupplierController extends \ApiController
{

    public function __construct()
    {
        parent::__construct();

        $this->beforeFilter(function () {
            if (!Session::has('admin_id') && !Input::has('is_webservice')) {
                return Redirect::route('admin.login');
            } else {

                $request = get_current_request();
                $request['controller'] = str_replace('admin\\', '', $request['controller']);
                $admin_permissions = json_decode(Session::get('admin_permissions'), true);

                if (!Input::has('is_webservice')) {
                    //validar acceso - controlador
                    if (!isset($admin_permissions[$request['controller']]))
                        die('Acceso denegado');

                    //validar acceso - action
                    if (is_array($admin_permissions[$request['controller']]['actions'])) {
                        if (!in_array($request['action'], $admin_permissions[$request['controller']]['actions']))
                            die('Acceso denegado');
                    } else die('Acceso denegado');
                }

                $permissions = $admin_permissions[$request['controller']]['permissions'];

                View::share('admin_permissions', $permissions);
                $this->admin_permissions = $permissions;

                View::composer('admin.layout', function ($view) {
                    $view->with('menu', json_decode(Session::get('admin_menu'), true));
                });
            }
        }, array('except' => array('login', 'post_login', 'update_locations_ajax')));
    }

    /**
     * Función para cargar la vista incial
     */
    public function index()
    {
        $data = [
            'title' => 'Surtidor de Pedidos de calidad en Alistamiento'
        ];

        return View::make('admin.supplier_picking.index', $data);
    }

    /**
     * Obtiene las ordenes de la ruta selecionada
     */
    public function get_orders_ajax()
    {
        $city = City::find(Session::get('admin_city_id'));
        $warehouse = Warehouse::find(Session::get('admin_warehouse_id'));
        $orders = [
            'orders_w_missings' => [],
            'orders_w_colds' => []
        ];

        $orders_w_missings = Order::where('orders.status', '=', 'In Progress')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('order_products', 'orders.id', '=', 'order_products.order_id')
            ->leftJoin('order_product_group AS opg', 'orders.id', '=', 'opg.order_id')
            ->join('store_products', function ($join) {
                $join->on('order_products.store_product_id', '=', 'store_products.id');
                $join->orOn('opg.store_product_id', '=', 'store_products.id');
            })
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('routes', 'orders.route_id', '=', 'routes.id')
            ->join('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
            ->join('stores', 'orders.store_id', '=', 'stores.id')
            ->join('cities', 'cities.id', '=', 'stores.city_id')
            ->where(function ($query) {
                $query->where('order_products.fulfilment_status', '=', 'Missing')
                    ->orWhere('opg.fulfilment_status', '=', 'Missing');
            })
            ->where('cities.id', $city->id)
            ->where('order_picking_quality.status_dry', 'Validado')
            ->where('store_products.storage', 'Seco')
            ->select('orders.id', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'store_products.storage');

        if (Session::get('admin_designation') != 'Super Admin' && $warehouse)
            $orders_w_missings->where('order_groups.warehouse_id', $warehouse->id);

        $orders_w_missings = $orders_w_missings->groupBy('orders.id')->orderBy('orders.id', 'ASC')->get();


        $orders_w_colds = Order::where('orders.status', '=', 'In Progress')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('order_products', 'orders.id', '=', 'order_products.order_id')
            ->leftJoin('order_product_group AS opg', 'orders.id', '=', 'opg.order_id')
            ->join('store_products', function ($join) {
                $join->on('order_products.store_product_id', '=', 'store_products.id');
                $join->orOn('opg.store_product_id', '=', 'store_products.id');
            })
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('routes', 'orders.route_id', '=', 'routes.id')
            ->join('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
            ->join('stores', 'orders.store_id', '=', 'stores.id')
            ->join('cities', 'cities.id', '=', 'stores.city_id')
            ->where(function ($query) {
                $query->where('order_products.fulfilment_status', '=', 'Missing')
                    ->orWhere('opg.fulfilment_status', '=', 'Missing');
            })
            ->select('orders.id', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'store_products.storage')
            ->where('order_picking_quality.status_cold', 'Validado')
            ->where(function ($query) {
                $query->where('store_products.storage', 'Congelado');
                $query->orWhere('store_products.storage', 'Refrigerado');
            })
            ->where('cities.id', $city->id);

        if (Session::get('admin_designation') != 'Super Admin' && $warehouse)
            $orders_w_colds->where('order_groups.warehouse_id', $warehouse->id);

        $orders_w_colds = $orders_w_colds->groupBy('orders.id')->orderBy('orders.id', 'ASC')->get();

        $orders['orders_w_missings'] = View::make('admin.supplier_picking.index', ['orders_w_missings' => $orders_w_missings])->renderSections()['orders_w_missings'];
        $orders['orders_w_colds'] = View::make('admin.supplier_picking.index', ['orders_w_colds' => $orders_w_colds])->renderSections()['orders_w_colds'];
        return Response::json($orders);
    }

    /**
     * Obtiene los detalles de una orden, cargando los productos de seco o frio
     */
    public function get_order_details_ajax()
    {
        $order['order_id'] = Input::get('order_id', null);
        $order['type'] = Input::get('type', null);
        $order_group = OrderGroup::join('orders', 'group_id', '=', 'order_groups.id')->where('orders.id', $order['order_id'])->first();

        $products = OrderProduct::where('order_products.order_id', $order['order_id'])
            ->where('store_product_warehouses.warehouse_id', $order_group->warehouse_id)
            ->where('fulfilment_status', 'Missing')
            ->where('order_picking_quality_details.type', 'Product')
            ->where(function ($query) {
                $query->where('order_picking_quality_details.status', 'Faltante');
                $query->Orwhere('order_picking_quality_details.status_shrinkage', '=', 'Mal estado');
                $query->Orwhere('order_picking_quality_details.status_shrinkage', '=', 'Vencido');
            })
            ->join('orders', 'orders.id', '=', 'order_products.order_id')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
            ->join('order_picking_quality_details', function ($join) {
                $join->on('order_picking_quality.id', '=', 'order_picking_quality_id');
                $join->on('order_picking_quality_details.store_product_id', '=', 'store_products.id');
            })
            ->select('order_products.*',
                'store_products.storage',
                'store_product_warehouses.storage_position AS position',
                'store_product_warehouses.storage_height_position AS position_height',
                'picking_stock AS current_stock',
                DB::raw("COUNT(order_picking_quality_details.store_product_id) AS quantity_missing"));

        if ($order['type'] == 'seco') {
            $products->where('store_products.storage', 'Seco');
        } else {
            $products->where(function ($query) {
                $query->where('store_products.storage', 'Congelado');
                $query->orWhere('store_products.storage', 'Refrigerado');
            });
        }
        $products = $products->groupBy('order_products.store_product_id')->get()->toArray();

        $products_group = OrderProductGroup::where('order_product_group.order_id', $order['order_id'])
            ->where('store_product_warehouses.warehouse_id', $order_group->warehouse_id)
            ->where('order_picking_quality_details.type', 'Agrupado')
            ->where(function ($query) {
                $query->where('order_picking_quality_details.status', 'Faltante');
                $query->Orwhere('order_picking_quality_details.status_shrinkage', '=', 'Mal estado');
                $query->Orwhere('order_picking_quality_details.status_shrinkage', '=', 'Vencido');
            })
            ->join('orders', 'orders.id', '=', 'order_product_group.order_id')
            ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
            ->join('order_picking_quality_details', function ($join) {
                $join->on('order_picking_quality.id', '=', 'order_picking_quality_id');
                $join->on('order_picking_quality_details.store_product_id', '=', 'store_products.id');
            })
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->where('order_product_group.fulfilment_status', 'Missing');

        if ($order['type'] == 'seco') {
            $products_group->where('store_products.storage', 'Seco');
        } else {
            $products_group->where(function ($query) {
                $query->where('store_products.storage', 'Congelado');
                $query->orWhere('store_products.storage', 'Refrigerado');
            });
        }
        $products_group = $products_group->select('order_product_group.*',
            'store_products.storage',
            'store_product_warehouses.storage_position AS position',
            'store_product_warehouses.storage_height_position AS position_height',
            'picking_stock AS current_stock',
            DB::raw("COUNT(order_picking_quality_details.store_product_id) AS quantity_missing"))
            ->groupBy('order_product_group.store_product_id', 'order_product_id')
            ->get()
            ->toArray();

        foreach ($products as $key => $product) {
            if ($product['type'] == 'Agrupado') {
                unset($products[$key]);
            }
        }

        $products = array_merge($products, $products_group);
        if (Input::has('products')) {
            $localstorage_products = Input::get('products');
            foreach ($localstorage_products as $key => $prod) {
                foreach ($products as $key2 => &$product) {
                    if ($prod['id'] == $product['id']) {
                        $product['fulfilment_status'] = $prod['fulfilment_status'];
                    }
                }
            }
        }

        $response = [];
        $response['status'] = true;
        $response['message'] = 'Se encontraron los productos de este pedido.';
        $response['products'] = $products;
        $response['html'] = View::make('admin.supplier_picking.index', ['products' => $products, 'order' => $order])->renderSections()['order_detail'];

        return Response::json($response);
    }

    /**
     * Actualizado los estados de los productos.
     */
    public function update_order_status_ajax()
    {
        try {
            $order_id = Input::get('order_id');
            $products = Input::get('products');
            $order = Order::find($order_id);
            $order_group = OrderGroup::find($order->group_id);
            $products_fullfilled = Session::get('products_fullfilled') ? Session::get('products_fullfilled') : [];

            DB::beginTransaction();

            // Actualizar el estado de productos
            foreach ($products as $key => $product) {
                if ($product['fulfilment_status'] != 'Pending') {
                    if (isset($product['type']) && $product['type'] == 'Product') {
                        $order_product = OrderProduct::find($product['id']);

                        if ($product['fulfilment_status'] == 'Not Available' && $product['quantity'] > $product['quantity_missing']) {
                            $order_product->quantity = $product['quantity'] - $product['quantity_missing'];
                            $product['fulfilment_status'] = 'Fullfilled';
                        }
                        $order_product->fulfilment_status = $product['fulfilment_status'];
                        $order_product->save();

                        if ($order_product->fulfilment_status == 'Fullfilled') {
                            $products_fullfilled[] = $product;
                        }
                    } else {
                        $order_product_group = OrderProductGroup::where('id', $product['id'])->first();
                        /*if($product['fulfilment_status'] == 'Not Available' && $product['quantity'] > $product['quantity_missing'])
                        {
                            $order_product_group->quantity = $product['quantity'] - $product['quantity_missing'];
                            $product['fulfilment_status'] = 'Fullfilled';
                        }*/
                        $order_product_group->fulfilment_status = $product['fulfilment_status'];
                        $order_product_group->save();
                        $order_product_group_list[] = $order_product_group->toArray();

                        if ($order_product_group->fulfilment_status == 'Fullfilled') {
                            $products_fullfilled[] = $product;
                        }
                    }
                }
            }
            Session::put('products_fullfilled', $products_fullfilled);

            if (isset($order_product_group_list)) {
                foreach ($order_product_group_list as $order_product_group_item)
                    OrderProductGroup::updateComboStatus($order_product_group_item['order_product_id']);
            }

            //actualizar total de pedido
            $has_pending_products = false;
            $total_amount = 0;
            $order_products = OrderProduct::where('order_id', $order->id)->get();
            foreach ($order_products as $order_product) {
                if ($order_product->fulfilment_status == 'Fullfilled')
                    $total_amount += $order_product->price * $order_product->quantity;
                if ($order_product->fulfilment_status == 'Pending' || $order_product->fulfilment_status == 'Missing')
                    $has_pending_products = true;
            }
            $order->total_amount = $total_amount;
            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'Total de pedido actualizado: ' . $order->total_amount;
            $log->shopper_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            //actualizar pedido a Alistado
            if (!$has_pending_products) {
                if (($order->status == 'Enrutado' || $order->status == 'In Progress') && $order->payment_method == 'Tarjeta de crédito') {
                    $result = $this->charge_order($order);
                    if (!$result['status'])
                        return $this->jsonResponse($result['message']);
                }

                $order->picking_date = Carbon::now();
                $order->status = 'Alistado';

                if (count($products_fullfilled)) {
                    foreach ($products_fullfilled as $key => $product) {
                        if (isset($product['type']) && $product['type'] == 'Product' && $product['fulfilment_status'] == 'Fullfilled') {

                            $order_product = OrderProduct::find($product['id']);
                            if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($order_group->warehouse_id, $order_product->store_product_id)) {
                                $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock - $order_product->quantity;
                                $store_product_warehouse->picked_stock = $store_product_warehouse->picked_stock + $order_product->quantity;
                                $store_product_warehouse->save();
                            }
                        } elseif (isset($product['type']) && $product['fulfilment_status'] == 'Fullfilled') {
                            $order_product_group = OrderProductGroup::where('id', $product['id'])->first();
                            if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($order_group->warehouse_id, $order_product_group->store_product_id)) {
                                $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock - $order_product_group->quantity;
                                $store_product_warehouse->picked_stock = $store_product_warehouse->picked_stock + $order_product_group->quantity;
                                $store_product_warehouse->save();
                            }
                        }
                    }
                }
                Session::forget('products_fullfilled');
            } else $order->status = 'In Progress';

            \Log::info('Modificando estado de pedido: ' . $order_id . ' a ' . $order->status . ' desde api surtidor');
            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'Estado actualizado a: ' . $order->status;
            $log->shopper_id = $order->shopper_id;
            $log->order_id = $order->id;
            $log->save();

            DB::commit();

            $response['status'] = true;
            if ($order->status == 'In Progress') {
                $response['message'] = 'Se actualizaron los faltantes, pero el pedido tiene productos pendientes.';
            } else {
                $response['message'] = 'Se ha actualizado el pedido.';
            }
            return Response::json($response);
        } catch (\Exception $e) {

            DB::rollback();
            \Log::info('Rollback de pedido: ' . $order->id . ' al actualizar el estado desde api surtidor');
            $response['status'] = false;
            $response['message'] = 'Ha ocurrido un error al actualizar la orden.' . $e->getMessage();
            return Response::json($response);
        }
    }

    public function confirm_product_quantity_ajax()
    {
        $id = Input::get('order_product_id');
        $quantity = Input::get('quantity');
        $type = Input::get('type');

        if ($type == 'Product')
            $product = OrderProduct::find($id);
        else
            $product = OrderProductGroup::find($id);

        $quantity_missing = OrderPickingQualityDetail::join('order_picking_quality', 'order_picking_quality.id', '=', 'order_picking_quality_id')
            ->where('type', $type)
            ->where('store_product_id', $product->store_product_id)
            ->where('order_id', $product->order_id)
            ->where('order_picking_quality_details.status', 'Faltante')
            ->count();

        $quantity_validated = OrderPickingQualityDetail::join('order_picking_quality', 'order_picking_quality.id', '=', 'order_picking_quality_id')
            ->where('type', $type)
            ->where('store_product_id', $product->store_product_id)
            ->where('order_id', $product->order_id)
            ->where('order_picking_quality_details.status', 'Validado')
            ->count();

        if ($product) {
            if ($quantity_missing > $quantity) {
                $product->quantity = $quantity_validated + $quantity;
                $product->save();
                $response['status'] = true;
                $response['message'] = 'Se ha actualizado la cantidad del producto.';
                $response['quantity'] = $quantity;
            } else {
                if ($product->quantity < $quantity) {
                    $response['status'] = false;
                    $response['message'] = 'Ha ocurrido un error al actualizar la cantidad del producto a confirmar, no coincide con la registrada en el sistema.';
                } elseif ($product->quantity == $quantity) {
                    $response['status'] = true;
                    $response['message'] = 'Cantidad del producto confimada.';
                } elseif ($quantity_missing == $quantity) {
                    $response['status'] = true;
                    $response['message'] = 'Cantidad del producto confimada.';
                } elseif ($product->quantity > $quantity) {
                    $response['status'] = false;
                    $response['message'] = 'Error al confirmar la cantidad.';
                }
            }
        }

        return Response::json($response);
    }
}