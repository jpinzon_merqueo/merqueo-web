<?php

namespace api\picking\v1;

use View, City, Store, Request, Order, DB, Input, Response, Session, OrderProduct, Redirect, OrderProductGroup, Carbon\Carbon, OrderLog, Product, Exception, Routes, Warehouse;

class ApiValidationController extends \ApiController {

	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter(function()
		{
			if (!Session::has('admin_id') && !Input::has('is_webservice')){
				return Redirect::route('admin.login');
			} else {

				$request = get_current_request();
				$request['controller'] = str_replace('admin\\', '', $request['controller']);
				$admin_permissions = json_decode(Session::get('admin_permissions'), true);

				if (!Input::has('is_webservice'))
				{
					//validar acceso - controlador
					if (!isset($admin_permissions[$request['controller']]))
						die('Acceso denegado');

					//validar acceso - action
					if (is_array($admin_permissions[$request['controller']]['actions'])){
						if (!in_array($request['action'], $admin_permissions[$request['controller']]['actions']))
							die('Acceso denegado');
					}else die('Acceso denegado');
				}

				$permissions = $admin_permissions[$request['controller']]['permissions'];

				View::share('admin_permissions', $permissions);
				$this->admin_permissions = $permissions;

				View::composer('admin.layout', function($view){
					$view->with('menu', json_decode(Session::get('admin_menu'), true));
				});
			}
		}, array('except' => array('login', 'post_login', 'update_locations_ajax')));
	}

	/**
	 * Función para cargar la vista incial
	 */
	public function index()
	{
		if (Session::get('admin_designation') == 'Super Admin')
			$cities = City::join('stores', 'cities.id', '=', 'city_id')->where('stores.status', 1)->select('cities.id', 'cities.city')->groupBy('cities.id')->get();
		else
			$cities = City::join('stores', 'cities.id', '=', 'city_id')->where('city_id', Session::get('admin_city_id'))
							->where('stores.status', 1)->select('cities.id', 'cities.city')->groupBy('cities.id')->get();

		if (Session::get('admin_designation') == 'Super Admin'){
			$warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->select('warehouses.id', 'warehouses.warehouse')->get();
		}else
			$warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->where('id', Session::get('admin_warehouse_id'))->select('warehouses.id', 'warehouses.warehouse')->get();

		$data = [
			'title' => 'Validación de pedidos',
			'cities' => $cities,
			'warehouses' => $warehouses,
		];

		return View::make('admin.validation_picking.index', $data);
	}

	/**
	 * Funcion para obtener las rutas
	 */
	public function get_planning_route_ajax()
	{
		$html = Input::get('html', false);
		$admin_designation = Session::get('admin_designation');
		$routes = Routes::select('routes.id', 'routes.route')
							->join('orders', 'orders.route_id', '=', 'routes.id')
							->where(function ($query)
							{
								$query->where('orders.status', '=', 'In Progress');
								$query->orWhere('orders.status', '=', 'Alistado');
								$query->orWhere('orders.status', '=', 'Enrutado');
							})
							->where('warehouse_id', Input::get('warehouse_id'))
							->whereNotNull('orders.planning_sequence')
							->groupBy('routes.id')
							->orderBy('routes.id')
							->get();

		if ( $html ) {
			return View::make('admin.validation_picking.index', ['routes' => $routes])->renderSections()['routes'];
		}
		return Response::json(['routes' => $routes]);
	}

	/**
	 * Obtiene las ordenes de la ruta selecionada
	 */
	public function get_orders_ajax()
	{
		$route_id = Input::get('route_id', null);
		$orders = [
			'orders_w_missings' => [],
			'orders_w_colds' => [],
			'orders_to_check' => []
		];
		if ( empty($route_id) ) {
			echo '<pre>';
			print_r('string');
			echo '</pre>';
			die;
		}
		$orders_w_missings = Order::where('orders.status', '=', 'In Progress')
					->join('order_products', 'orders.id', '=', 'order_products.order_id')
					->leftJoin('order_product_group AS opg', 'order_products.id', '=', 'opg.order_product_id')
					->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
					->join('products', 'store_products.product_id', '=', 'products.id')
					->join('routes', 'orders.route_id', '=', 'routes.id')
					->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
					->where(function ($query)
					{
						$query->where('order_products.fulfilment_status', '=', 'Missing')
								->orWhere('opg.fulfilment_status', '=', 'Missing');
					})
					->whereNull('order_picking_quality.id')
					->where('orders.type', 'Merqueo')
					->where('orders.route_id', $route_id)
					->where('store_products.storage', 'Seco')
					->groupBy('orders.id')
					->orderBy('orders.id', 'ASC')
					->select('orders.id', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'store_products.storage')
					->get();


		$orders_w_colds = Order::where('orders.status', '=', 'In Progress')
							->join('order_products', 'orders.id', '=', 'order_products.order_id')
							->leftJoin('order_product_group AS opg', 'order_products.id', '=', 'opg.order_product_id')
							->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
							->join('products', 'store_products.product_id', '=', 'products.id')
							->join('routes', 'orders.route_id', '=', 'routes.id')
							->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
							->where(function ($query)
							{
								$query->where('order_products.fulfilment_status', '=', 'Missing')
										->orWhere('opg.fulfilment_status', '=', 'Missing');
							})
							->select('orders.id', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'store_products.storage')
							->where('orders.type', 'Merqueo')
							->where('orders.route_id', $route_id)
							->where(function ($query)
							{
								$query->where('store_products.storage', 'Congelado');
								$query->orWhere('store_products.storage', 'Refrigerado');
							})
							->whereNull('order_picking_quality.id')
							->groupBy('orders.id')
							->orderBy('orders.id', 'ASC')
							->get();

		$orders_to_check = Order::where('orders.status', '=', 'Alistado')
							->join('order_products', 'orders.id', '=', 'order_products.order_id')
							->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
							->join('products', 'store_products.product_id', '=', 'products.id')
							->join('routes', 'orders.route_id', '=', 'routes.id')
							->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
							->select('orders.id', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'store_products.storage', 'orders.picking_revision_date')
							->where('orders.route_id', $route_id)
							->whereNull('order_picking_quality.id')
							->groupBy('orders.id')
							->orderBy('orders.planning_sequence', 'ASC')
							->get();

		foreach ($orders_to_check as $key => &$order) {
			$cold_products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
									->join('order_products', 'store_products.id', '=', 'order_products.store_product_id')
									->where('order_products.order_id', $order->id)
									->where('order_products.type', '<>', 'Agrupado')
									->where(function ($query)
									{
										$query->where('store_products.storage', 'Congelado');
										$query->orWhere('store_products.storage', 'Refrigerado');
									})->count();

			$cold_products_group = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
											->join('order_product_group AS opg', 'store_products.id', '=', 'opg.store_product_id')
											->where('opg.order_id', $order->id)
											->where(function ($query)
											{
												$query->where('store_products.storage', 'Congelado');
												$query->orWhere('store_products.storage', 'Refrigerado');
											})
											->count();

			$dry_products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
									->join('order_products', 'store_products.id', '=', 'order_products.store_product_id')
									->where('order_products.order_id', $order->id)
									->where('store_products.storage', 'Seco')
									->where('order_products.type', '<>', 'Agrupado')
									->count();

			$dry_products_group = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
											->join('order_product_group AS opg', 'store_products.id', '=', 'opg.store_product_id')
											->where('opg.order_id', $order->id)
											->where('store_products.storage', 'Seco')
											->count();

			$order->cold_products = $cold_products + $cold_products_group;
			$order->dry_products = $dry_products + $dry_products_group;
		}

		$orders['orders_w_missings'] = View::make('admin.validation_picking.ajax.orders_w_missings', ['orders_w_missings' => $orders_w_missings])->render();
		$orders['orders_w_colds'] = View::make('admin.validation_picking.ajax.orders_w_colds', ['orders_w_colds' => $orders_w_colds])->render();
		$orders['orders_to_check'] = View::make('admin.validation_picking.ajax.orders_to_check', ['orders_to_check' => $orders_to_check])->render();

		return Response::json($orders);
	}

	/**
	 * Obtiene los detalles de una orden, cargando los productos de seco o frio
	 */
	public function get_order_details_ajax()
	{
		// $order = Order::select('routes.route')->join('routes', 'routes.id', '=', 'route_id')->where('orders.id', Input::get('order_id', null))->first();
		$order_obj = Order::with('route', 'orderGroup')->find(Input::get('order_id', null));
		$order['route'] = $order_obj->route->route;

		$order['order_id'] = Input::get('order_id', null);
		$order['sequence'] = Input::get('sequence', null);
		$order['type'] = Input::get('type', null);

		$products = OrderProduct::with('storeProduct')
							->where('order_id', $order['order_id'])
							->where('fulfilment_status', 'Missing')
							->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
							->join('products', 'store_products.product_id', '=', 'products.id')
							->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
							->where('store_product_warehouses.warehouse_id', $order_obj->orderGroup->warehouse_id)
							->select('order_products.*', 'store_products.storage', 'store_product_warehouses.storage_position', 'store_product_warehouses.storage_height_position');

		if ( $order['type'] == 'seco' ) {
			$products->where('store_products.storage', 'Seco');
		}else{
			$products->where(function ($query)
			{
				$query->where('store_products.storage', 'Congelado');
				$query->orWhere('store_products.storage', 'Refrigerado');
			});
		}

		$products = $products->get();
		$products->each(function ($product) use ($order_obj)
		{
			$product->current_stock = $product->storeProduct->getCurrentStock($order_obj->orderGroup->warehouse_id);
		});

		$products = $products->toArray();

		$products_group = OrderProductGroup::with('storeProduct')
											->where('order_id', $order['order_id'])
											->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
											->join('products', 'store_products.product_id', '=', 'products.id')
											->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
											->where('store_product_warehouses.warehouse_id', $order_obj->orderGroup->warehouse_id)
											->where('order_product_group.fulfilment_status', 'Missing');
		if ( $order['type'] == 'seco' ) {
			$products_group->where('store_products.storage', 'Seco');
		}else{
			$products_group->where(function ($query)
			{
				$query->where('store_products.storage', 'Congelado');
				$query->orWhere('store_products.storage', 'Refrigerado');
			});
		}
		$products_group = $products_group->select('order_product_group.*', 'store_products.storage', 'store_product_warehouses.storage_position', 'store_product_warehouses.storage_height_position')
										->get();

		$products_group->each(function ($product) use ($order_obj)
		{
			$product->current_stock = $product->storeProduct->getCurrentStock($order_obj->orderGroup->warehouse_id);
		});

		$products_group = $products_group->toArray();
		foreach ($products as $key => $product) {
			if ( $product['type'] == 'Agrupado' ) {
				unset($products[$key]);
			}
		}

		$products = array_merge($products, $products_group);

		if ( Input::has('products') ) {
			$localstorage_products = Input::get('products');
			foreach ($localstorage_products as $key => $prod) {
				foreach ($products as $key2 => &$product) {
					if ( $prod['id'] == $product['id'] ) {
						$product['fulfilment_status'] = $prod['fulfilment_status'];
					}
				}
			}
		}

		$response = [];
		$response['status'] = true;
		$response['message'] = 'Se encontraron los productos de este pedido.';
		$response['products'] = $products;
		$response['html'] = View::make('admin.validation_picking.ajax.order_detail', ['order' => $order, 'products' => $products])->render();

		return Response::json($response);
	}

	/**
	 * Actualizado los estados de los productos.
	 */
	public function update_order_status_ajax()
	{
		try {
			$order_id = Input::get('order_id');
			$products = Input::get('products');
			$order = Order::find($order_id);

			DB::beginTransaction();

			// Actualizar el estado de productos
			foreach ($products as $key => $product) {
				if ($product['fulfilment_status'] != 'Pending')
				{
					if ( isset($product['type']) && $product['type'] == 'Product' ) {
						$order_product = OrderProduct::find($product['id']);
						$order_product->fulfilment_status = $product['fulfilment_status'];
						$order_product->save();
					}else{
						$order_product_group = OrderProductGroup::where('id', $product['id'])->first();
						$order_product_group->fulfilment_status = $product['fulfilment_status'];
						$order_product_group->save();
						$order_product_group_list[] = $order_product_group->toArray();
					}
				}
			}

			if (isset($order_product_group_list)){
				foreach($order_product_group_list as $order_product_group_item)
					OrderProductGroup::updateProductStatus($order_product_group_item['order_product_id']);
			}

			//actualizar total de pedido
			$has_pending_products = false;
			$total_amount = 0;
			$order_products = OrderProduct::where('order_id', $order->id)->get();
			foreach ($order_products as $order_product) {
				if ($order_product->fulfilment_status == 'Fullfilled')
					$total_amount += $order_product->price * $order_product->quantity;
				if ($order_product->fulfilment_status == 'Pending' || $order_product->fulfilment_status == 'Missing')
					$has_pending_products = true;
			}
			$order->total_amount = $total_amount;
			$order->save();

			//log de pedido
			$log = new OrderLog();
			$log->type = 'Total de pedido actualizado: '.$order->total_amount;
			$log->shopper_id = $order->shopper_id;
			$log->order_id = $order->id;
			$log->save();

			//actualizar pedido a Alistado
			if (!$has_pending_products)
			{
				if (($order->status == 'Enrutado' || $order->status == 'In Progress') && $order->payment_method == 'Tarjeta de crédito'){
					$result = $this->charge_order($order);
					if (!$result['status'])
					   return $this->jsonResponse($result['message']);
				}

				$order->picking_date = Carbon::now();
				$order->status = 'Alistado';
			}else $order->status = 'In Progress';
            
            $order->save();

			//log de pedido
			$log = new OrderLog();
			$log->type = 'Estado actualizado a: ' . $order->status;
			$log->shopper_id = $order->shopper_id;
			$log->order_id = $order->id;
			$log->save();

			DB::commit();

			$response['status'] = true;
			if ( $order->status == 'In Progress' ) {
				$response['message'] = 'Se actualizaron los faltantes, pero el pedido tiene productos pendientes.';
			}else{
				$response['message'] = 'Se ha actualizado el pedido.';
			}
			return Response::json($response);
		} catch (\Exception $e) {

			DB::rollback();

			$response['status'] = false;
			$response['message'] = 'Ha ocurrido un error al actualizar la orden.'.$e->getMessage();
			return Response::json($response);
		}
	}

	/**
	 * Actualiza la fecha de revision de los pedidos
	 */
	public function update_order_revision_date_ajax()
	{
		$order_id = Input::get('order_id');

		$order = Order::find($order_id);
		$order->picking_revision_date = Carbon::now();
		$order->save();

		return Response::json(['status' => true, 'message' => 'Pedido actualizado.']);
	}

	/**
	 * Funcion para crear cargo a tarjeta
	 * @param  [type] $order [description]
	 */
	private function charge_order($order)
	{
		$response = array(
			'status' => false
		);

		if ($order && ($order->status == 'In Progress' || $order->status == 'Alistado'))
		{
			try {
				//DB::beginTransaction();

				$total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
				if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && (empty($order->payment_date) || $order->payment_date = '0000-00-00 00:00:00' ))
				{
					//REALIZAR COBRO DE TARJETA
					if (empty($order->cc_charge_id))
					{
						$log = new OrderLog();
						$result = Order::chargeCreditCard($order->id, $order->user_id);
						if (!$result['status'])
							throw new Exception($result['message'], 1);
						else{
							$log->type = 'Cobro a tarjeta exitoso.';
							$log->shopper_id = $order->shopper_id;
							$log->order_id = $order->id;
							$log->save();

							$response = array(
								'status' => true,
								'message' => $result['message'],
							);

							//DB::commit();

							return $response;
						}
					}else $response['message'] = 'El cobro a la tarjeta ya fue realizado con anterioridad.';
				}else $response['message'] = 'No es necesario realizar cobro a la tarjeta.';

			}catch(Exception $e){

				//DB::rollback();

				if (!$response['status']){
					$log->type = $result['message'];
					$log->shopper_id = $order->shopper_id;
					$log->order_id = $order->id;
					$log->save();
				}

				$response['message'] = $e->getMessage();
			}
		}else $response['message'] = 'No se pudo realizar el cobro por que el estado del pedido debe estar En proceso o Alistado.';

		return $response;
	}
}
