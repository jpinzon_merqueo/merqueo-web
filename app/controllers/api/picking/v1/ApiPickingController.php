<?php

namespace api\picking\v1;


use Input, Hash, Config, DB, Route, Picker, Response, View, Session, Auth, StoreProduct, Routes, PickingGroup, Pusherer, Warehouse;


class ApiPickingController extends \ApiController
{
    public function __construct()
    {
        $this->beforeFilter(function() {
            if(!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
            else $this->picker_id = Session::get('picker_id');
        }, array('except' => array('login')));
        parent::__construct();
    }

    /**
     * Realiza login de alistador
     *
     * @return array $response Respuesta
     */
    public function login()
    {
        //validar version
        if (Input::has('app_version'))
        {
            $update = 'not_required'; //not_required, required, suggested
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if ($app_version < 207)
                $update = 'required';
            if ($update == 'required'){
                $message = 'Hemos realizado cambios en la app, por favor actualiza a la última versión para continuar.';
                $url = 'https://drive.google.com/a/merqueo.com/file/d/0BwyNIBwqW1vGaXhnUDN4OTJRR00/view?usp=sharing';
                return Response::json(['status' => true, 'update_app' => ['message' => $message, 'url' => $url]] , 200);
            }
        }

        $email =  Input::get('email', null);
        $password =  Input::get('password', null);

        if (empty($email) || empty($password))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if ($picker = Picker::where('email', $email)->where('status', 1)->first())
        {
            if (Hash::check($password, $picker->password))
            {
                $picker->last_logged_in = date('Y-m-d H:i:s');
                $picker->save();

                Session::put('picker_id', $picker->id);
                Session::put('picker_email', $picker->email);
                Session::put('picker_name', $picker->first_name);
                Session::put('picker_city_id', $picker->city_id);
                Session::put('picker_profile', $picker->profile);
                Session::put('user_logged_in', true);

                $warehouses = Warehouse::where('city_id', $picker->city_id)->select('warehouses.id', 'warehouses.warehouse')->get();
                $picker = array(
                    'id' => $picker->id,
                    'first_name' => $picker->first_name,
                    'last_name' => $picker->last_name,
                    'email' => $picker->email,
                    'phone' => $picker->phone,
                    'photo_url' => $picker->photo_url,
                    'city_id' => $picker->city_id,
                    'picking_group_id' => '',
                    'profile'=> $picker->profile
                );

                $this->response = array(
                    'status' => true,
                    'message' => 'Sesión iniciada',
                    'result' => array('picker' => $picker, 'warehouses' => $warehouses)
                );
            }else $this->response['message'] = 'Contraseña incorrecta.';
        }else $this->response['message'] = 'Email incorrecto.';

        return $this->jsonResponse();
    }

    /**
     * Get grupo de alistamiento a cargar
     *
     * @return array $response Respuesta
     */
    public function get_picking_group()
    {
        if (Input::has('warehouse_id'))
        {
            Session::put('warehouse_id', Input::get('warehouse_id'));
            $picking_groups = Routes::select('picking_group.id', 'picking_group')->where(function($query){
                                $query->where('orders.status', '=', 'Enrutado')
                                    ->orWhere('orders.status', '=', 'In Progress')
                                    ->orWhere('orders.status', '=', 'Alistado');
                               })
                               ->join('orders', 'route_id', '=', 'routes.id')
                               ->join('order_groups', 'group_id', '=', 'order_groups.id')
                               ->join('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
                               ->join('zones', 'zones.id', '=', 'routes.zone_id')
                               ->whereNotNull('orders.route_id')
                               ->where('orders.type', 'Merqueo')
                               ->where('zones.warehouse_id', Input::get('warehouse_id'))
                               ->groupBy('picking_group.id')
                               ->orderBy('picking_group.id')
                               ->get();

            $this->response = array(
                'status' => true,
                'message' => 'picking group',
                'result' => array('picking_groups' => $picking_groups)
            );
        }else $this->response['message'] = 'Bodega es requerido.';

        return $this->jsonResponse();
    }

    /**
     * Setea grupo de alistamiento a cargar
     *
     * @return array $response Respuesta
     */
    public function set_picking_group()
    {
        if (!Input::has('warehouse_id'))
           $this->response['message'] = 'Bodega es requerido.';
        else{
            if (Input::has('picking_group_id'))
            {
                Session::put('picking_group', Input::get('picking_group_id'));
                $this->response = array(
                    'status' => true,
                    'message' => 'Grupo de alistamiento guardado'
                );
            }else $this->response['message'] = 'Grupo de alistamiento es requerido.';
        }

        return $this->jsonResponse();
    }

    /**
     * Cierra sesion
     */
    public function logout()
    {
        if (!Input::has('picker_id')){
            $this->response = array(
                'status' => true,
                'message' => 'ID picker es requerido'
            );
            return Response::json( $this->response, 400 );
        }
        Session::flush();

        $this->response = array(
            'status' => true,
            'message' => 'Sesión cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos para alistar
     *
     * @return array $response Respuesta
     */
    public function picking_orders($id)
    {
        if (!is_numeric($id))
            return $this->jsonResponse('ID picker debe ser numérico.', 400);

        if (!$picker = Picker::find($id))
            return $this->jsonResponse('ID picker no existe.', 400);

        $orders = PickingGroup::getNextOrder($picker, Session::get('picking_group'), true, Session::get('warehouse_id'));

        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'orders' => $orders
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Autenticacion de pusher
     */
    public function pusher_auth()
    {
        if (Auth::check() || Session::get('user_logged_in')){
            $response = Response::make(  Pusherer::auth_push( Input::get('channel_name'), Input::get('socket_id'), 'picker' ), 200);
            return $response->header('Content-Type', 'application/json');
        }

        return $this->jsonResponse();
    }

    /**
     * Obtener posicion de producto para almacenar
     *
     * @return array $response Respuesta
     */
    public function product_position_storage()
    {
        if(!Input::has('reference'))
            return $this->jsonResponse('Por favor ingresar referencia.', 400);

        $product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                               ->join('stores', 'stores.id', '=', 'store_products.store_id')
                               ->join('cities', 'cities.id', '=', 'stores.city_id')
                               ->join('store_product_warehouses', 'store_product_id', '=', 'store_products.id')
                               ->where('reference', Input::get('reference'))
                               ->where('warehouse_id', Input::get('warehouse_id'))
                               ->where('cities.id', Input::get('city_id'))
                               ->select('products.name AS product_name',
                                        'quantity AS product_quantity',
                                        'unit AS product_unit',
                                        DB::raw("CONCAT(storage_position, '-', storage_height_position) AS storage_position_product"),
                                        'image_medium_url AS image')
                               ->first();

        if($product){
            $this->response = array(
                'status' => true,
                'message' => 'Información de producto obtenida.',
                'result' => array(
                    'product' => $product
                )
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'No existe información del producto.'
            );
        }
        return $this->jsonResponse();
    }

}
