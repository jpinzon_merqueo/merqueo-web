<?php

namespace api\picking\v1;

use Input, Hash, Route, DB, Config, Picker, Order, OrderGroup, OrderProduct, OrderLog, Response, Exception, Session, Carbon\Carbon, OrderProductGroup, Event, PickingGroup, StoreProductRelated, StoreProduct, StoreProductWarehouse, DateTime, DateInterval;

class ApiOrderController extends \ApiController
{
    public function __construct()
    {
        $this->beforeFilter(function () {
            if (!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
            else $this->picker_id = Session::get('picker_id');
        });
        parent::__construct();
    }

    /**
     * Tomar pedido para alistamiento
     *
     * @return array $response Respuesta
     */
    public function update_order_picking_date($id)
    {

        if (!$order = Order::find($id))
            return $this->jsonResponse('ID pedido no existe.', 400);

        if (!$picker = Picker::find(Session::get('picker_id')))
            return $this->jsonResponse('Sesión no valida.', 400);

        if ($order->status == 'Alistado')
            return $this->jsonResponse('El pedido ya esta alistado.');

        if (($picker->profile == 'Alistador Seco' && !empty($order->picker_dry_id) && $order->picker_dry_id != $picker->id) ||
            ($picker->profile == 'Alistador Frío' && !empty($order->picker_cold_id) && $order->picker_cold_id != $picker->id)) {
            //obtener siguiente pedido a alistar
            $orders = PickingGroup::getNextOrder($picker, Session::get('picking_group'), false, Session::get('warehouse_id'));

            $this->response = array(
                'status' => false,
                'message' => 'El pedido ya esta asignado a otro alistador por favor toma el siguiente.',
                'result' => [
                    'orders' => $orders
                ]
            );

            return $this->jsonResponse();
        }

        //$order->allocated_date = date('Y-m-d H:i:s');
        $order->allocated_date = Carbon::now()->format('Y-m-d H:i:s');

        //$picking_start_date_mobile = date('Y-m-d H:i:s', strtotime(Input::get('picking_start_date')));
        //Session::put('picking_start_date_mobile', $picking_start_date_mobile);
        $picking_start_date = Carbon::now()->format('Y-m-d H:i:s');

        if ($picker->profile == 'Alistador Seco') {
            $order->picker_dry_id = $picker->id;
            $order->picking_dry_start_date = $picking_start_date;
        }

        if ($picker->profile == 'Alistador Frío') {
            $order->picker_cold_id = $picker->id;
            $order->picking_cold_start_date = $picking_start_date;
        }

        $order->save();

        $this->response = array(
            'status' => true,
            'message' => 'Pedido actualizado.'
        );

        return $this->jsonResponse();
    }

    /**
     * Actualizar estado de pedido
     *
     * @return array $response Respuesta
     */
    public function update_order_status($id)
    {

        if (!Input::has('picker_id') || !Input::has('products'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if (!$order = Order::find($id))
            return $this->jsonResponse('ID pedido no existe.', 400);

        if ($order->status == 'Alistado')
            return $this->jsonResponse('El pedido ya esta alistado.');

        $products = Input::get('products');
        if (!count($products))
            return $this->jsonResponse('La información de los productos esta vacia.', 400);

        //$this->picker_id = Input::get('picker_id');
        $picker = Picker::find($this->picker_id);

        if ($picker->profile == 'Alistador Seco' && !Input::has('picking_baskets'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if ($order->status != 'Enrutado' && $order->status != 'In Progress')
            return $this->jsonResponse('El estado del pedido no es valido. El pedido esta en estado: ' . $order->status);

        if ($picker->profile == 'Alistador Seco' && $order->picker_dry_id != 0 && $order->picker_dry_id != $this->picker_id)
            return $this->jsonResponse('El pedido ya esta asignado a otro alistador.');
        if ($picker->profile == 'Alistador Frío' && $order->picker_cold_id != 0 && $order->picker_cold_id != $this->picker_id)
            return $this->jsonResponse('El pedido ya esta asignado a otro alistador.');

        if (empty($order->received_date))
            $order->received_date = date('Y-m-d H:i:s');

        if ($picker->profile == 'Alistador Seco') {
            $valid_storage = ['Seco'];
            $order->picker_dry_id = Input::get('picker_id');
            $order->picking_dry_end_date = Carbon::now()->format('Y-m-d H:i:s');
            /*if($order->picking_dry_end_date <  $order->picking_dry_start_date){
                $order->picking_dry_end_date = $this->get_picking_end_date($order->picking_dry_start_date);
            }*/
            $order->picking_baskets = Input::get('picking_baskets');
        }

        if ($picker->profile == 'Alistador Frío') {
            $valid_storage = ['Refrigerado', 'Congelado'];
            $order->picker_cold_id = Input::get('picker_id');
            $order->picking_cold_end_date = Carbon::now()->format('Y-m-d H:i:s');
            /*if($order->picking_cold_end_date <  $order->picking_cold_start_date){
                $order->picking_dry_end_date = $this->get_picking_end_date($order->picking_cold_start_date);
            }*/
            $order->picking_bags = Input::get('picking_bags');
        }

        $order_group = OrderGroup::find($order->group_id);
        try {
            DB::beginTransaction();
            //actualizar estado de productos
            
            foreach ($products as $product) {
                if ($product['quantity'] <= 0 && $product['fulfilment_status'] == 'Fullfilled') {
                    DB::rollback();
                    return $this->jsonResponse("Las cantidades de los productos no pueden ser negativas o cero");
                }


                if ($product['fulfilment_status'] != 'Pending') {
                    $product['storage'] = !isset($product['storage']) ? ['Seco', 'Refrigerado', 'Congelado'] : $product['storage'];
                    if ($product['type'] == 'Agrupado') {
                        $order_product_group = OrderProductGroup::find($product['id']);
                        if ($order_product_group->fulfilment_status == 'Pending' && in_array($product['storage'], $valid_storage)) {
                            $order_product_group->fulfilment_status = $product['fulfilment_status'];
                            $order_product_group->picking_date = date('Y-m-d H:i:s', strtotime($product['picking_date']));
                            $order_product_group->save();
                            $order_product_group_list[] = $order_product_group;
                        }
                    } else {

                        //if ($product['is_gift'] == 0) {
                            $order_product = OrderProduct::find($product['id']);
                            if ($order_product->fulfilment_status == 'Pending' && in_array($product['storage'], $valid_storage)) {
                                //validar si el producto esta con special y full price
                                $order_products_per_price = OrderProduct::where('store_product_id', $order_product->store_product_id)
                                    ->where('order_id', $order_product->order_id)
                                    ->where('type', 'Product')
                                    ->where('fulfilment_status', 'Pending')
                                    ->orderBy('price')
                                    ->get();

                                if (count($order_products_per_price) > 1) {
                                    foreach ($order_products_per_price as $key => $order_product_per_price) {
                                        //producto con precio especial
                                        if ($key == 0) {
                                            $order_product_per_price->quantity_original = $order_product_per_price->quantity;
                                            if ($product['quantity'] < $order_product_per_price->quantity) {
                                                $order_product_per_price->quantity = $product['quantity'];
                                                $product['quantity'] = 0;
                                            }
                                            $order_product_per_price->fulfilment_status = $product['fulfilment_status'];
                                            $product['quantity'] -= $order_product_per_price->quantity;
                                        } else {
                                            //producto con precio full
                                            if ($product['quantity'] <= 0)
                                                $order_product_per_price->fulfilment_status = 'Missing';
                                            //$order_product_per_price->fulfilment_status = 'Not Available';
                                            else {
                                                $order_product_per_price->quantity_original = $order_product_per_price->quantity;
                                                $order_product_per_price->quantity = $product['quantity'];
                                                $order_product_per_price->fulfilment_status = $product['fulfilment_status'];
                                            }
                                        }
                                        $order_product_per_price->save();
                                    }
                                } else {
                                    $order_product->quantity_original = $order_product->quantity;
                                    $order_product->fulfilment_status = $product['fulfilment_status'];
                                    $order_product->quantity = $product['quantity'];
                                    $order_product->picking_date = date('Y-m-d H:i:s', strtotime($product['picking_date']));
                                    $order_product->save();


                                }
                            }
                        /*} else {
                            if ($product['fulfilment_status'] == 'Fullfilled' && $product['is_gift'] == 1) {
                                $store_product = StoreProduct::select('products.*', 'store_products.*')
                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                    ->where('store_products.id', $product['store_product_id'])
                                    ->first();

                                $store_product->is_gift = $product['is_gift'];
                                if (isset($product['parent_store_product_id'])) {
                                    $store_product->parent_id = $product['parent_store_product_id'];
                                }
                                $store_product->fulfilment_status = $product['fulfilment_status'];
                                $store_product->picking_date = date('Y-m-d H:i:s', strtotime($product['picking_date']));
                                $order_product = OrderProduct::saveProduct($product['price'], $product['quantity'], $store_product, $order);

                                if (isset($product['parent_store_product_id'])) {
                                    $product_parent = OrderProduct::where('store_product_id', $product['parent_store_product_id'])->where('order_id', $order->id)->first();
                                    $product_parent->fulfilment_status = 'Not Available';
                                    $product_parent->save();
                                }
                            }
                        }*/
                    }
                }

                /*if(isset($product['shrinkage_stock']) && $product['shrinkage_stock'] > 0 && $product['shrinkage_stock'] < 10 && is_numeric($product['shrinkage_stock'])){
                    $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($order_group->warehouse_id, $product['store_product_id']);
                    $store_product_warehouse->shrinkage_stock += intval($product['shrinkage_stock']);
                    //$store_product_warehouse->picking_stock -= intval($product['shrinkage_stock']);
                    $store_product_warehouse->save();
                    
                    $shrinkage_origin_log = new ShrinkageOriginLog;
                    $shrinkage_origin_log->admin_id = $this->picker_id;
                    $shrinkage_origin_log->warehouse_id = $order_group->warehouse_id;
                    $shrinkage_origin_log->store_product_id = $product['store_product_id'];
                    $shrinkage_origin_log->quantity = $product['shrinkage_stock'];
                    $shrinkage_origin_log->position = $store_product_warehouse->position;
                    $shrinkage_origin_log->position_height = $store_product_warehouse->position_height;
                    $shrinkage_origin_log->origin = 'Alistamiento';
                    $shrinkage_origin_log->save();
                }*/
            }

            if (isset($order_product_group_list)) {
                foreach ($order_product_group_list as $order_product_group_item)
                    OrderProductGroup::updateProductStatus($order_product_group_item->order_product_id);
            }

            //actualizar total de pedido
            $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status', ['Pending', 'Missing'])->where('type', '<>', 'Recogida')->count();
            $has_pending_products = $order_products ? true : false;
            Event::fire('order.total_log', [[$order, $this->picker_id, 'picker']]);


            //actualizar pedido a Alistado
            if (!$has_pending_products) {
                if (($order->status == 'Enrutado' || $order->status == 'In Progress') && $order->payment_method == 'Tarjeta de crédito') {
                    $result = $this->charge_order($order);
                    if (!$result['status'])
                        return $this->jsonResponse($result['message']);
                }

                $order->updatePickingStock();

                $order->picking_date = Carbon::now(-5)->format('Y-m-d H:i:s');
                $order->status = 'Alistado';
            } else $order->status = 'In Progress';

            $order->updateTotals();
            $order->save();

            DB::commit();

            Event::fire('order.status_log', [[$order, $this->picker_id, 'picker']]);

            //obtener siguiente pedido a alistar
            $orders = PickingGroup::getNextOrder($picker, Session::get('picking_group'), false, Session::get('warehouse_id'));
        } catch (Exception $e) {
            DB::rollback();
            return $this->jsonResponse('Ocurrió un error al actualizar los datos.' . $e->getLine());
        }

        $this->response = array(
            'status' => true,
            'message' => 'Estado de pedido actualizado.',
            'result' => [
                'orders' => $orders
            ]
        );

        return $this->jsonResponse();
    }

    /*
     * Cobrar total de pedido a tarjeta de cliente
     */
    private function charge_order($order)
    {
        $response = array(
            'status' => false
        );

        if ($order && ($order->status == 'In Progress' || $order->status == 'Enrutado')) {
            try {
                //DB::beginTransaction();

                $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                if ($order->payment_method == 'Tarjeta de crédito' && $order->pending_payment_response == 1) {
                    $response = array(
                        'status' => false,
                        'message' => 'El cobro a la tarjeta fue bloqueado por favor contacta al administrador',
                    );
                    return $response;
                }
                if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && (empty($order->payment_date) || $order->payment_date = '0000-00-00 00:00:00')) {
                    $payment = $order->getPayments(true);
                    //REALIZAR COBRO DE TARJETA
                    if (empty($payment->cc_charge_id) || (!empty($payment->cc_charge_id) && !empty($payment->cc_refund_date))) {
                        $log = new OrderLog();
                        $result = Order::chargeCreditCard($order->id, $order->user_id, $this->picker_id);
                        if (!$result['status'])
                            throw new Exception($result['message'], 1);
                        else {
                            $log->type = 'Cobro a tarjeta exitoso.';
                            $log->picker_id = $this->picker_id;
                            $log->order_id = $order->id;
                            $log->save();

                            $response = array(
                                'status' => true,
                                'message' => $result['message'],
                            );

                            //DB::commit();

                            return $response;
                        }
                    } else $response['message'] = 'El cobro a la tarjeta ya fue realizado con anterioridad.';
                } else $response['message'] = 'No es necesario realizar cobro a la tarjeta.';

            } catch (Exception $e) {

                //DB::rollback();

                if (!$response['status']) {
                    $log->type = $result['message'];
                    $log->picker_id = $this->picker_id;
                    $log->order_id = $order->id;
                    $log->save();

                    DB::commit();
                }

                $response['message'] = $e->getMessage();
            }
        } else $response['message'] = 'No se pudo realizar el cobro por que el estado del pedido debe estar En proceso o Alistado.';

        return $response;
    }

    /*
     * Obtener producto relacionado para obsequio
     */
    public function get_product_related($id)
    {
        $product_gift = "";
        $status = false;
        $product_id = Input::get('product_id');
        $priority = Input::has('priority') ? Input::get('priority') + 1 : 1;
        $order_product = OrderProduct::where('store_product_id', $product_id)->where('order_id', $id)->first();
        $warehouse_id = OrderGroup::join('orders', 'group_id', '=', 'order_groups.id')->where('orders.id', $id)->pluck('warehouse_id');
        $picker = Picker::find(Session::get('picker_id'));

        $product_related = StoreProductRelated::where('store_product_id', $product_id)
            ->where('priority', $priority)
            ->where('warehouse_id', $warehouse_id)
            ->where('status', 1)
            ->first();
        if ($product_related) {
            $validate_product_gift = OrderProduct::where('store_product_id', $product_related->store_product_related_id)->where('order_id', $id)->first();
            if (!$validate_product_gift) {
                $product_gift = StoreProduct::join('products', 'products.id', '=', 'product_id')
                    ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                    ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                    ->where('store_product_warehouses.status', 1)
                    ->where('store_products.id', $product_related->store_product_related_id)
                    ->select('base_cost',
                        'base_price',
                        'cost',
                        'picking_stock',
                        'description',
                        'iva',
                        'merqueo_discount',
                        'price AS original_price',
                        'store_products.store_id',
                        'store_products.id AS store_product_id',
                        'products.name AS product_name',
                        'quantity AS product_quantity',
                        'unit AS product_unit',
                        'provider_discount',
                        'reference',
                        'image_large_url',
                        'reception_stock',
                        'return_stock',
                        'storage',
                        DB::raw("CONCAT(storage_position, '-', storage_height_position) AS storage_position_product"))
                    ->first();

                if ($product_gift) {
                    $product_gift->id = $order_product->id;
                    $product_gift->created_at = $order_product->created_at;
                    $product_gift->fulfilment_status = 'Pending';
                    $product_gift->profile = $picker->profile;
                    $product_gift->parent_id = 0;
                    $product_gift->picking_date = $order_product->picking_date;
                    $product_gift->price = 0;
                    $product_gift->product_comment = NULL;
                    $product_gift->product_id = 0;
                    $product_gift->product_image_url = $product_gift->image_large_url;
                    $product_gift->order_id = (int)$id;
                    $product_gift->parent_store_product_id = (int)$product_id;
                    $product_gift->is_gift = 1;
                    $product_gift->priority = $product_related->priority;
                    $product_gift->quantity = $order_product->quantity;
                    $product_gift->type = "Product";
                    $status = true;
                }
            }
        }

        $this->response = array(
            'status' => $status,
            'message' => 'Producto obtenido',
            'result' => array(
                'product_gift' => $product_gift
            )
        );

        return $this->jsonResponse();
    }


    /**
     * Funcion que calcula la fecha final de picking a partir de las fechas de inicio y fin del teléfono, previamente capturadas
     * @param datetime $start_date fecha de inicio registrada en el pedido
     */
    /*private function get_picking_end_date($start_date)
    {
        $picking_start_date = Session::get('picking_start_date_mobile');
        $picking_end_date = date('Y-m-d H:i:s',strtotime(Input::get('picking_end_date')));
        $picking_start_date = new DateTime($picking_start_date);
        $picking_end_date = new DateTime($picking_end_date);
        $diff = $picking_end_date->diff($picking_start_date, true);
        $diff->h = $diff->h*3600;
        $diff->i = $diff->i*60;
        $diff->s = $diff->s+$diff->i+$diff->h;
        $start_date = new DateTime($start_date);
        $start_date->add(new DateInterval('PT'.$diff->s.'S'));
        return $start_date->format('Y-m-d H:i:s');
    }*/
}
