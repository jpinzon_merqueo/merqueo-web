<?php

namespace api\picking\v11;

use exceptions\PayUException;
use Illuminate\Database\QueryException;
use Input, Hash, Route, DB, Config, Picker, Order, OrderGroup, OrderProduct, OrderLog, Response, Exception, Session, Carbon\Carbon, OrderProductGroup, Event, PickingGroup, StoreProductRelated, StoreProduct, StoreProductWarehouse, DateTime, DateInterval;

class ApiOrderController extends \ApiController
{
    public function __construct()
    {
        //Session::put('picker_id', 793);
        $this->beforeFilter(function () {
            if (!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
            else
                $this->picker_id = Session::get('picker_id');
        });
        parent::__construct();
    }

    /**
     * Tomar pedido para alistamiento
     *
     * @return array $response Respuesta
     */
    public function update_order_picking_date($id)
    {

        if (!$order = Order::find($id))
            return $this->jsonResponse('ID pedido no existe.', 400);

        if (!$picker = Picker::find(Session::get('picker_id')))
            return $this->jsonResponse('Sesión no valida.', 400);

        if ($order->status == 'Alistado')
            return $this->jsonResponse('El pedido ya esta alistado.');

        if (($picker->profile == 'Alistador Seco' && !empty($order->picker_dry_id) && $order->picker_dry_id != $picker->id) ||
            ($picker->profile == 'Alistador Frío' && !empty($order->picker_cold_id) && $order->picker_cold_id != $picker->id)) {
            //obtener siguiente pedido a alistar
            $orders = PickingGroup::getNextOrder($picker, Session::get('picking_group'), false, Session::get('warehouse_id'));

            $this->response = array(
                'status' => false,
                'message' => 'El pedido ya esta asignado a otro alistador por favor toma el siguiente.',
                'result' => [
                    'orders' => $orders
                ]
            );

            return $this->jsonResponse();
        }

        //$order->allocated_date = date('Y-m-d H:i:s');
        $order->allocated_date = Carbon::now()->format('Y-m-d H:i:s');

        //$picking_start_date_mobile = date('Y-m-d H:i:s', strtotime(Input::get('picking_start_date')));
        //Session::put('picking_start_date_mobile', $picking_start_date_mobile);
        $picking_start_date = Carbon::now()->format('Y-m-d H:i:s');

        if ($picker->profile == 'Alistador Seco') {
            $order->picker_dry_id = $picker->id;
            $order->picking_dry_start_date = $picking_start_date;
        }

        if ($picker->profile == 'Alistador Frío') {
            $order->picker_cold_id = $picker->id;
            $order->picking_cold_start_date = $picking_start_date;
        }

        $order->save();

        $this->response = array(
            'status' => true,
            'message' => 'Pedido actualizado.'
        );

        return $this->jsonResponse();
    }

    /*
     * Cobrar total de pedido a tarjeta de cliente
     */
    private function charge_order($order)
    {
        $response = array(
            'status' => false
        );

        if ($order && ($order->status == 'In Progress' || $order->status == 'Enrutado')) {
            try {
                //DB::beginTransaction();
                $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && !$order->getPaymentStatus())
                {
                        $log = new OrderLog();
                        $result = Order::chargeCreditCard($order->id, $order->user_id, $this->picker_id);
                        if (!$result['status'])
                            throw new Exception($result['message'], 1);
                        else {
                            $log->type = 'Cobro a tarjeta exitoso.';
                            $log->picker_id = $this->picker_id;
                            $log->order_id = $order->id;
                            $log->save();

                            $response = array(
                                'status' => true,
                                'message' => $result['message'],
                            );

                            //DB::commit();

                            return $response;
                        }
                }  else {
                    $response = array(
                        'status' => true,
                        'message' => 'No es necesario realizar cobro a la tarjeta.',
                    );
                }

            } catch (Exception $e) {

                //DB::rollback();

                $response['message'] = $e->getMessage();
            }
        } else $response['message'] = 'No se pudo realizar el cobro por que el estado del pedido debe estar En proceso o Alistado.';

        return $response;
    }

    /*
     * Obtener producto relacionado para obsequio
     */
    public function get_product_related($id)
    {
        $product_gift = "";
        $status = false;
        $product_id = Input::get('product_id');
        $priority = Input::has('priority') ? Input::get('priority') + 1 : 1;
        $order_product = OrderProduct::where('store_product_id', $product_id)->where('order_id', $id)->first();
        $warehouse_id = OrderGroup::join('orders', 'group_id', '=', 'order_groups.id')->where('orders.id', $id)->pluck('warehouse_id');
        $picker = Picker::find(Session::get('picker_id'));

        $product_related = StoreProductRelated::where('store_product_id', $product_id)
            ->where('priority', $priority)
            ->where('warehouse_id', $warehouse_id)
            ->where('status', 1)
            ->first();
        if ($product_related) {
            $validate_product_gift = OrderProduct::where('store_product_id', $product_related->store_product_related_id)->where('order_id', $id)->first();
            if (!$validate_product_gift) {
                $product_gift = StoreProduct::join('products', 'products.id', '=', 'product_id')
                    ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                    ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                    ->where('store_product_warehouses.status', 1)
                    ->where('store_products.id', $product_related->store_product_related_id)
                    ->select('base_cost',
                        'base_price',
                        'cost',
                        'picking_stock',
                        'description',
                        'iva',
                        'merqueo_discount',
                        'price AS original_price',
                        'store_products.store_id',
                        'store_products.id AS store_product_id',
                        'products.name AS product_name',
                        'quantity AS product_quantity',
                        'unit AS product_unit',
                        'provider_discount',
                        'reference',
                        'image_large_url',
                        'reception_stock',
                        'return_stock',
                        'storage',
                        DB::raw("CONCAT(storage_position, '-', storage_height_position) AS storage_position_product"))
                    ->first();

                if ($product_gift) {
                    $product_gift->id = $order_product->id;
                    $product_gift->created_at = $order_product->created_at;
                    $product_gift->fulfilment_status = 'Pending';
                    $product_gift->profile = $picker->profile;
                    $product_gift->parent_id = 0;
                    $product_gift->picking_date = $order_product->picking_date;
                    $product_gift->price = 0;
                    $product_gift->product_comment = NULL;
                    $product_gift->product_id = 0;
                    $product_gift->product_image_url = $product_gift->image_large_url;
                    $product_gift->order_id = (int)$id;
                    $product_gift->parent_store_product_id = (int)$product_id;
                    $product_gift->is_gift = 1;
                    $product_gift->priority = $product_related->priority;
                    $product_gift->quantity = $order_product->quantity;
                    $product_gift->type = "Product";
                    $status = true;
                }
            }
        }

        $this->response = array(
            'status' => $status,
            'message' => 'Producto obtenido',
            'result' => array(
                'product_gift' => $product_gift
            )
        );

        return $this->jsonResponse();
    }

    /********************************************************** New App*******************************************************************/
    /**
     * Obtener Productos de un pedido para alistar
     *
     * @return array $response Respuesta
     */
    public function picking_products($id, $order_id)
    {
        if (!is_numeric($id))
            return $this->jsonResponse('ID picker debe ser numérico.', 400);

        if (!is_numeric($order_id))
            return $this->jsonResponse('ID de pedido debe ser numérico.', 400);

        if (!$picker = Picker::find($id))
            return $this->jsonResponse('ID picker no existe.', 400);

        if (!$picker = Picker::find(Session::get('picker_id')))
            return $this->jsonResponse('Sesión no valida.', 400);

        if (!$order = Order::find($order_id))
            return $this->jsonResponse('ID pedido no existe.', 400);

        if ($order->status == 'Alistado')
            return $this->jsonResponse('El pedido ya esta alistado.');

        if (($picker->profile == 'Alistador Seco' && !empty($order->picker_dry_id) && $order->picker_dry_id != $picker->id) ||
            ($picker->profile == 'Alistador Frío' && !empty($order->picker_cold_id) && $order->picker_cold_id != $picker->id)) {
            //obtener siguiente pedido a alistar
            $order = PickingGroup::getNextOrderTemp($picker, Session::get('picking_group'), false, Session::get('warehouse_id'));

            $this->response = array(
                'status' => false,
                'message' => 'El pedido ya esta asignado a otro alistador por favor toma el siguiente.',
                'result' => [
                    'order' => $order
                ]
            );

            return $this->jsonResponse();
        }

        $order->allocated_date = Carbon::now()->format('Y-m-d H:i:s');
        $picking_start_date = Carbon::now()->format('Y-m-d H:i:s');

        if ($picker->profile == 'Alistador Seco' && is_null($order->picker_dry_id)) {
            $order->picker_dry_id = $picker->id;
            $order->picking_dry_start_date = $picking_start_date;
        }

        if ($picker->profile == 'Alistador Frío' && is_null($order->picker_cold_id)) {
            $order->picker_cold_id = $picker->id;
            $order->picking_cold_start_date = $picking_start_date;
        }

        $order->save();
        $products = PickingGroup::getProductsList($picker, $order->id, Session::get('picker_warehouse_id'));

        $this->response = array(
            'status' => true,
            'message' => 'Pedido actualizado.',
            'result' => [
                'products' => json_encode($products),
                'secuence' => $order->planning_sequence,
                'route' => \Routes::where('id', $order->route_id)->pluck('route')
            ]
        );

        return $this->jsonResponse();
    }

    /**
     * Actualizar estado de pedido
     *
     * @return array $response Respuesta
     */
    public function update_order_status($id)
    {
        ini_set('post_max_size', '128M');

        try {
            DB::beginTransaction();

            if (!Input::has('picker_id') || !Input::has('products')) {
                DB::rollback();
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
            }

            if (!$order = Order::find($id)) {
                DB::rollback();
                return $this->jsonResponse('ID pedido no existe.', 400);
            }

            if ($order->status == 'Alistado') {
                DB::rollback();
                return $this->jsonResponse('El pedido ya esta alistado.');
            }

            $products = json_decode(Input::get('products'));
            if (!count($products)) {
                DB::rollback();
                return $this->jsonResponse('La información de los productos esta vacia.', 400);
            }

            $picker = Picker::find($this->picker_id);

            if ($picker->profile == 'Alistador Seco' && !Input::has('picking_baskets')) {
                DB::rollback();
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
            }

            if ($order->status != 'Enrutado' && $order->status != 'In Progress') {
                DB::rollback();
                return $this->jsonResponse('El estado del pedido no es valido. El pedido esta en estado: ' . $order->status);
            }

            if ($picker->profile == 'Alistador Seco' && $order->picker_dry_id != 0 && $order->picker_dry_id != $this->picker_id) {
                DB::rollback();
                return $this->jsonResponse('El pedido ya esta asignado a otro alistador.');
            }
            if ($picker->profile == 'Alistador Frío' && $order->picker_cold_id != 0 && $order->picker_cold_id != $this->picker_id) {
                DB::rollback();
                return $this->jsonResponse('El pedido ya esta asignado a otro alistador.');
            }

            if (empty($order->received_date)) {
                $order->received_date = date('Y-m-d H:i:s');
            }

            if ($picker->profile == 'Alistador Seco') {
                $validStorage = ['Seco'];
                $order->picker_dry_id = Input::get('picker_id');
                $order->picking_dry_end_date = Carbon::now()->format('Y-m-d H:i:s');
                $order->picking_baskets = Input::get('picking_baskets');
            }

            if ($picker->profile == 'Alistador Frío') {
                $validStorage = ['Refrigerado', 'Congelado'];
                $order->picker_cold_id = Input::get('picker_id');
                $order->picking_cold_end_date = Carbon::now()->format('Y-m-d H:i:s');
                $order->picking_bags = Input::get('picking_bags');
            }
            //actualizar estado de productos
            $orderProductGroupList = [];
            foreach ($products as $product) {
                if ($product->quantity <= 0 && $product->fulfilment_status == 'Fullfilled') {
                    DB::rollback();
                    return $this->jsonResponse("Las cantidades de los productos no pueden ser negativas o cero");
                }

                if ($product->fulfilment_status != 'Pending') {
                    $product->storage = !isset($product->storage) ? ['Seco', 'Refrigerado', 'Congelado'] : $product->storage;
                    if ($product->type == 'Agrupado') {

                        $order_product_group_ids = [];
                        foreach ($product->order_product_group_ids AS $key => $val) {
                            $order_product_group_ids[] = $key;
                        }

                        $orderProductGroups = OrderProductGroup::whereIn('order_product_group.id', $order_product_group_ids)->get();
                        if (count($orderProductGroups) > 1) {
                            $quantity = $product->quantity;
                            foreach ($orderProductGroups AS $orderProductGroup) {
                                $orderProduct = OrderProduct::find($orderProductGroup->order_product_id);
                                if ($orderProductGroup->fulfilment_status == 'Pending' && in_array($product->storage, $validStorage)) {
                                    $orderProductGroup->fulfilment_status = $product->fulfilment_status;
                                    //$orderProductGroup->quantity_original = $orderProductGroup->quantity;

                                    if ($quantity >= $orderProductGroup->quantity) {
                                        $orderProductGroup->fulfilment_status = 'Fullfilled';
                                        $orderProduct->fulfilment_status = 'Fullfilled';
                                        $quantity -= $orderProductGroup->quantity;
                                    } else {
                                        $orderProductGroup->fulfilment_status = 'Missing';
                                        $orderProduct->fulfilment_status = 'Missing';
                                        $orderProductGroup->quantity = $quantity;
                                    }
                                    $orderProduct->save();
                                    $orderProductGroup->picking_date = Carbon::now()->format('Y-m-d H:i:s');
                                    $orderProductGroup->save();
                                    $orderProductGroupList[] = $orderProductGroup;

                                    \Log::info('Modificado de la orden:'.$order->id.' el estado del producto :'.$orderProductGroup->id. ' a '.$orderProductGroup->fulfilment_status. ' en la app picking V11');
                                }
                            }
                        } else {
                            $orderProductGroupObject = OrderProductGroup::find($product->id);
                            $orderProduct = OrderProduct::find($orderProductGroupObject->order_product_id);
                            if ($orderProductGroupObject->fulfilment_status == 'Pending' && in_array($product->storage, $validStorage)) {
                                $orderProductGroupObject->fulfilment_status = $product->fulfilment_status;
//                                $orderProductGroupObject->picking_date = date('Y-m-d H:i:s', strtotime($product->picking_date));
                                //$orderProductGroupObject->quantity_original = $orderProductGroupObject->quantity;
                                $orderProductGroupObject->quantity = $product->quantity;
                                $orderProductGroupObject->picking_date = Carbon::now()->format('Y-m-d H:i:s');
                                $orderProduct->fulfilment_status= $product->fulfilment_status;
                                $orderProduct->save();
                                $orderProductGroupObject->save();
                                $orderProductGroupList[] = $orderProductGroupObject;

                                \Log::info('Modificado de la orden:'.$order->id.' el estado del producto grupo :'.$orderProductGroupObject->id. ' a '.$orderProductGroupObject->fulfilment_status. ' en la app picking V11');
                            }
                        }
                    } else {
                        $order_product = OrderProduct::find($product->id);

                        if (empty($order_product)) {
                            DB::rollback();
                            return $this->jsonResponse("El siguiente producto " . $product->name . " fue eliminado desde el dashboard por favor cierre e ingrese de nuevo al app.");
                        }

                        if ($order_product->fulfilment_status == 'Pending' && in_array($product->storage, $validStorage)) {
                            //validar si el producto esta con special y full price
                            $order_products_per_price = OrderProduct::where('store_product_id', $order_product->store_product_id)
                                ->where('order_id', $order_product->order_id)
                                ->where('type', 'Product')
                                ->where('fulfilment_status', 'Pending')
                                ->orderBy('price')
                                ->get();

                            if (count($order_products_per_price) > 1) {
                                foreach ($order_products_per_price as $key => $order_product_per_price) {
                                    //producto con precio especial
                                    if ($key == 0) {
                                        if ($product->quantity < $order_product_per_price->quantity) {
                                            $order_product_per_price->quantity = $product->quantity;
                                            $product->quantity = 0;
                                        }
                                        $order_product_per_price->fulfilment_status = $product->fulfilment_status;
                                        $product->quantity -= $order_product_per_price->quantity;
                                    } else {
                                        //producto con precio full
                                        if ($product->quantity <= 0)
                                            $order_product_per_price->fulfilment_status = 'Missing';
                                        else {
                                            $order_product_per_price->quantity = $product->quantity;
                                            $order_product_per_price->fulfilment_status = $product->fulfilment_status;
                                        }
                                    }
                                    $order_product_per_price->save();
                                    \Log::info('Modificado de la orden:'.$order->id.' el estado del producto :'.$order_product_per_price->id. ' a '.$order_product_per_price->fulfilment_status. ' en la app picking V11');

                                }
                            } else {
                                $order_product->fulfilment_status = $product->fulfilment_status;
                                $order_product->quantity = $product->quantity;
                                $order_product->picking_date = date('Y-m-d H:i:s', strtotime($product->picking_date));
                                $order_product->save();
                                \Log::info('Modificado de la orden:'.$order->id.' el estado del producto :'.$order_product->id. ' a '.$order_product->fulfilment_status. ' en la app picking V11');

                            }
                        }

                    }
                }
            }

            if (isset($orderProductGroupList)) {
                foreach ($orderProductGroupList as $orderProductGroupItem)
                    OrderProductGroup::updateProductStatus($orderProductGroupItem->order_product_id);
            }

            //actualizar total de pedido
            $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status', ['Pending', 'Missing'])->count();
            $has_pending_products = $order_products ? true : false;
            Event::fire('order.total_log', [[$order, $this->picker_id, 'picker']]);

            //actualizar pedido a Alistado
            $orderValidation = Order::find($order->id);
            if ($orderValidation->status == 'Alistado') {
                \Log::info('Rollback de pedido:'.$order->id. ' se encontraba en estado '.$order->status.' en la app picking V11');
                return $this->jsonResponse('El pedido fue alistado en otro proceso.');
            }else{
            if (!$has_pending_products) {
                $order->updatePickingStock();
                if (($order->status == 'Enrutado' || $order->status == 'In Progress') && $order->payment_method == 'Tarjeta de crédito') {
                    $result = $this->charge_order($order);
                    if (!$result['status'])
                        throw new PayUException($result['message']);
                }

                $order->picking_date = Carbon::now()->format('Y-m-d H:i:s');
                $order->status = 'Alistado';

            } else $order->status = 'In Progress';

                $order->updateTotals();
                $order->save();
                Event::fire('order.total_log', [[$order, $this->picker_id, 'picker']]);
                Event::fire('order.status_log', [[$order, $this->picker_id, 'picker']]);
                \Log::info('Modificando estado de pedido: '.$order->id. ' a '.$order->status.' desde app picking V11');
            }
            DB::commit();

            //obtener siguiente pedido a alistar
            $orders = PickingGroup::getNextOrder($picker, Session::get('picking_group'), false, Session::get('warehouse_id'));
        } catch (PayUException $e) {
            DB::rollback();

            $log = new OrderLog();
            $log->type = $e->getMessage();
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return $this->jsonResponse($e->getMessage());

        }  catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                \ErrorLog::add($e, 513);
                \Log::info('Rollback en actualización de inventario: '.$e->getMessage().' - desde app picking V11');
                return $this->jsonResponse('Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.\'');
            }
            \ErrorLog::add($e);
            \Log::info('Rollback en actualización de estado de pedido: '.$e->getMessage().' - desde app picking V11');
            return $this->jsonResponse('Ocurrió un error al actualizar los datos.');
        }  catch (Exception $e) {
            DB::rollback();
            \ErrorLog::add($e, 500);
            return $this->jsonResponse('Ocurrió un error al actualizar los datos.');
        }

        $picker = Picker::find(Input::get('picker_id'));
        $productivity = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('order_product_group', 'order_product_group.order_id', '=', 'orders.id');

        if ($picker->profile == 'Alistador Seco') {
            $productivity->whereRaw('DATE_FORMAT(picking_dry_start_date, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->where('picker_dry_id', $picker->id);
        } else {
            $productivity->whereRaw('DATE_FORMAT(picking_cold_start_date, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->where('picker_cold_id', $picker->id);
        }
        $productivity = $productivity->select(DB::raw('COUNT(DISTINCT(orders.id)) AS total_orders'),
            DB::raw('if(order_products.type = "Product" OR order_products.type = "Muestra", sum(order_products.quantity), sum(order_product_group.quantity)) total_products'),
            DB::raw("if(order_products.type = 'Product' OR order_products.type = 'Muestra', COUNT(DISTINCT(order_products.store_product_id)), COUNT(DISTINCT(order_product_group.store_product_id))) total_references"))->first();
        $this->response = array(
            'status' => true,
            'message' => 'Estado de pedido actualizado.',
            'result' => [
                'orders' => $orders,
                'productivity' => $productivity
            ]
        );

        return $this->jsonResponse();
    }

    /*
    * Obtener la productividad diaria de un picker
    *
    * @return object $response Respuesta
    *
    */
    public function picker_productivity()
    {
        $orders = $products = $references = 0;
        $picker = Picker::find($this->picker_id);
        $order = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('order_product_group', 'order_product_group.order_id', '=', 'orders.id');

        if ($picker->profile == 'Alistador Seco') {
            $order->whereRaw('DATE_FORMAT(picking_dry_start_date, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->where('picker_dry_id', $this->picker_id);
        } else {
            $order->whereRaw('DATE_FORMAT(picking_cold_start_date, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->where('picker_cold_id', $this->picker_id);
        }
        $order->select(DB::raw('COUNT(DISTINCT(orders.id)) AS total_orders'),
            DB::raw('if(order_products.type = "Product" OR order_products.type = "Muestra", sum(order_products.quantity), sum(order_product_group.quantity)) total_products'),
            DB::raw("if(order_products.type = 'Product' OR order_products.type = 'Muestra', COUNT(DISTINCT(order_products.store_product_id)), COUNT(DISTINCT(order_product_group.store_product_id))) total_references"))->first();
        IF (count($order) == 1) {
            $orders = $order->total_orders;
            $products = $order->total_products;
            $references = $order->total_references;
        }

        $this->response = array(
            'status' => true,
            'result' => array(
                'orders' => $orders,
                'products' => $products,
                'references' => $references
            )
        );
    }
}
