<?php

namespace api\picking\v11;

use Input, Picker, Response, Session, Routes, PickingGroup;

class ApiPickingController extends \ApiController
{
    public function __construct()
    {
        $this->beforeFilter(function () {
            if (!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
            else $this->picker_id = Session::get('picker_id');
        }, array('except' => array('login')));
        parent::__construct();
    }

    /**
     * Realiza login de alistador
     *
     * @return array $response Respuesta
     */
    public function login()
    {
        //validar version
        if (Input::has('app_version')) {
            $update = 'not_required'; //not_required, required, suggested
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if ($app_version < 207)
                $update = 'required';
            if ($update == 'required') {
                $message = 'Hemos realizado cambios en la app, por favor actualiza a la última versión para continuar.';
                $url = 'https://drive.google.com/file/d/16uj2vQWx4vLflrGIQLaVdD10YXX4Dgb4/view?usp=sharing';
                return Response::json(['status' => true, 'update_app' => ['message' => $message, 'url' => $url]], 200);
            }
        }
        $user_code = Input::get('user_code', null);

        if (empty($user_code))
            return $this->jsonResponse('Còdigo de usuario vacio.', 400);

        if ($picker = Picker::where('identity_number', $user_code)->where('status', 1)->first()) {
            $picker_log = \PickerSession::where('picker_id', $picker->id)
                ->whereRaw('DATE_FORMAT(picking_date_start, "%Y-%m-%d")= "' . date('Y-m-d').'"')
                ->orderby('created_at', 'desc')->first();
            if ($picker_log) {
                $date_start = new \DateTime($picker_log->picking_date_start);
                $current_date = date('Y-m-d');
                if ($date_start->format('Y-m-d') === $current_date && $picker_log->device_serial != Input::get('device_serial') && $picker_log->session_actived === 1) {
                    $this->response['message'] = 'El usuario se encuentra activo en otro dispositivo.';
                    return $this->jsonResponse();
                } else {
                    if ($date_start->format('Y-m-d') === $current_date && $picker_log->session_actived == 0) {
                        $pickerSession = \PickerSession::where('picker_id', $picker->id)
                            ->whereRaw('DATE_FORMAT(picking_date_start, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                            ->where('device_serial', '=', Input::get('device_serial'))
                            ->orderby('created_at', 'desc')->first();
                        if ($pickerSession) {
                            $pickerSession->session_actived = 1;
                            $pickerSession->save();
                        } else {
                            $this->register_picker_session($picker, Input::get('device_serial'));
                        }
                    }
                }
            } else {
                $this->register_picker_session($picker, Input::get('device_serial'));
            }

            $picker->last_logged_in = date('Y-m-d H:i:s');
            $picker->save();

            Session::put('picker_id', $picker->id);
            Session::put('picker_email', $picker->email);
            Session::put('picker_name', $picker->first_name);
            Session::put('picker_city_id', $picker->city_id);
            Session::put('picker_profile', $picker->profile);
            Session::put('picker_warehouse_id', $picker->warehouse_id);
            Session::put('picker_rol', $picker->rol);
            Session::put('user_logged_in', true);

            $response = array(
                'id' => $picker->id,
                'first_name' => $picker->first_name,
                'last_name' => $picker->last_name,
                'email' => $picker->email,
                'phone' => $picker->phone,
                'photo_url' => $picker->photo_url,
                'city_id' => $picker->city_id,
                'picking_group_id' => '',
                'profile' => $picker->profile,
                'warehouse_id' => $picker->warehouse_id,
                'user_code' => $user_code
            );

            $this->response = array(
                'status' => true,
                'message' => 'Sesión iniciada',
                'result' => array('picker' => $response)
            );
        } else $this->response['message'] = 'Código de usuario incorrecto.';

        return $this->jsonResponse();
    }


    private function register_picker_session($picker, $device_serial)
    {
        $log = new \PickerSession();
        $log->picker_id = $picker->id;
        $log->picking_date_start = date('Y-m-d H:i:s');
        $log->rol = $picker->rol;
        $log->device_serial = $device_serial;
        $log->save();
    }


    /**
     * Get grupo de alistamiento a cargar
     *
     * @return array $response Respuesta
     */
    public function get_picking_group()
    {
        if (Input::has('warehouse_id')) {
            Session::put('warehouse_id', Input::get('warehouse_id'));
            $picking_groups = Routes::select('picking_group.id', 'picking_group')->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress')
                    ->orWhere('orders.status', '=', 'Alistado');
            })
                ->join('orders', 'route_id', '=', 'routes.id')
                ->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->join('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
                ->join('zones', 'zones.id', '=', 'routes.zone_id')
                ->whereNotNull('orders.route_id')
                ->where('orders.type', 'Merqueo')
                ->where('zones.warehouse_id', Input::get('warehouse_id'))
                ->groupBy('picking_group.id')
                ->orderBy('picking_group.id')
                ->get();

            $this->response = array(
                'status' => true,
                'message' => 'picking group',
                'result' => array('picking_groups' => $picking_groups)
            );
        } else $this->response['message'] = 'Bodega es requerido.';

        return $this->jsonResponse();
    }

    /**
     * Setea grupo de alistamiento a cargar
     *
     * @return array $response Respuesta
     */
    public function set_picking_group()
    {
        if (!Input::has('warehouse_id'))
            $this->response['message'] = 'Bodega es requerido.';
        else {
            if (Input::has('picking_group_id')) {
                Session::put('picking_group', Input::get('picking_group_id'));
                $this->response = array(
                    'status' => true,
                    'message' => 'Grupo de alistamiento guardado'
                );
            } else $this->response['message'] = 'Grupo de alistamiento es requerido.';
        }

        return $this->jsonResponse();
    }

    /**
     * Cierra sesion
     */
    public function logout()
    {
        if (!Input::has('picker_id')) {
            $this->response = array(
                'status' => true,
                'message' => 'ID picker es requerido'
            );
            return Response::json($this->response, 400);
        }

        $picker_log = \PickerSession::where('picker_id', Input::get('picker_id'))->orderby('created_at', 'desc')->first();
        if ($picker_log) {
            if ($picker_log->session_actived === 1) {
                $picker_log->picking_date_end = date('Y-m-d H:i:s');
                $picker_log->session_actived = 0;
                $picker_log->save();
            }
        }

        Session::flush();
        $this->response = array(
            'status' => true,
            'message' => 'Sesión cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Obtener pedido para alistar
     *
     * @return object $response Respuesta
     */
    public function picking_order($id)
    {
        if (!is_numeric($id))
            return $this->jsonResponse('ID picker debe ser numérico.', 400);

        if (!$picker = Picker::find($id))
            return $this->jsonResponse('ID picker no existe.', 400);

        $order = PickingGroup::getNextOrderTemp($picker, Input::get('picking_group_id'), true, Input::get('warehouse_id'));
        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'order' => $order
            )
        );

        return $this->jsonResponse();
    }

}
