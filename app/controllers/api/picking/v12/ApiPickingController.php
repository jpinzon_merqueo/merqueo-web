<?php

namespace api\picking\v12;

use Input, Picker, Response, Session, Routes, PickingGroup, Order, DB;
use Carbon\Carbon;

class ApiPickingController extends \ApiController
{
    public function __construct()
    {
        $this->beforeFilter(function () {
            if (!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
            else $this->picker_id = Session::get('picker_id');
        }, array('except' => array('login')));
        parent::__construct();
    }

    /**
     * Realiza login de alistador
     *
     * @return array $response Respuesta
     */
    public function login()
    {
        //validar version
        if (Input::has('app_version')) {
            $update = 'not_required'; //not_required, required, suggested
            $appVersion = intval(str_replace('.', '', Input::get('app_version')));
            if ($appVersion < 207)
                $update = 'required';
            if ($update == 'required') {
                $message = 'Hemos realizado cambios en la app, por favor actualiza a la última versión para continuar.';
                $url = 'https://drive.google.com/file/d/16uj2vQWx4vLflrGIQLaVdD10YXX4Dgb4/view?usp=sharing';
                return Response::json(['status' => true, 'update_app' => ['message' => $message, 'url' => $url]], 200);
            }
        }
        $userCode = Input::get('user_code', null);

        if (empty($userCode))
            return $this->jsonResponse('Código de usuario vacio.', 200);

        if ($picker = Picker::where('identity_number', $userCode)->where('status', 1)->first()) {
            $pickerSession = \PickerSession::where('picker_id', $picker->id)
                ->whereRaw('DATE_FORMAT(picking_date_start, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->orderby('created_at', 'desc')->first();
            if ($pickerSession) {
                $date_start = new \DateTime($pickerSession->picking_date_start);
                $current_date = date('Y-m-d');
                if ($date_start->format('Y-m-d') === $current_date && $pickerSession->device_serial != Input::get('device_serial') && $pickerSession->session_actived === 1) {
                    $this->response['message'] = 'El usuario se encuentra activo en otro dispositivo.';
                    return $this->jsonResponse();
                } else {
                    if ($date_start->format('Y-m-d') === $current_date && $pickerSession->session_actived == 0) {
                        $pickerSession = \PickerSession::where('picker_id', $picker->id)
                            ->whereRaw('DATE_FORMAT(picking_date_start, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                            ->where('device_serial', '=', Input::get('device_serial'))
                            ->orderby('created_at', 'desc')->first();
                        if ($pickerSession) {
                            $pickerSession->session_actived = 1;
                            $pickerSession->save();
                        } else {
                            $this->registerPickerSession($picker, Input::get('device_serial'), Input::get('battery_level'));
                        }
                    }
                }
            } else {
                $this->registerPickerSession($picker, Input::get('device_serial'), Input::get('battery_level'));
            }

            $picker->last_logged_in = date('Y-m-d H:i:s');
            $picker->save();

            Session::put('picker_id', $picker->id);
            Session::put('picker_email', $picker->email);
            Session::put('picker_name', $picker->first_name);
            Session::put('picker_city_id', $picker->city_id);
            Session::put('picker_profile', $picker->profile);
            Session::put('picker_warehouse_id', $picker->warehouse_id);
            Session::put('picker_rol', $picker->rol);
            Session::put('user_logged_in', true);

            $date = Carbon::now();
            $productivity = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('order_product_group', 'order_product_group.order_id', '=', 'orders.id');

            if ($picker->profile == 'Alistador Seco') {
                $productivity->where('orders.picking_dry_end_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                    ->where('orders.picking_dry_end_date', '<', date('Y-m-d 23:59:59', strtotime($date)))
                    ->where('picker_dry_id', $picker->id);
            } else {
                $productivity->where('orders.picking_cold_end_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                    ->where('orders.picking_cold_end_date', '<', date('Y-m-d 23:59:59', strtotime($date)))
                    ->where('picker_cold_id', $picker->id);
            }
            $productivity = $productivity->select(DB::raw('COUNT(DISTINCT(orders.id)) AS total_orders'),
                DB::raw('if(order_products.type = "Product" OR order_products.type = "Muestra", sum(order_products.quantity), sum(order_product_group.quantity)) total_products'),
                DB::raw("if(order_products.type = 'Product' OR order_products.type = 'Muestra', COUNT(DISTINCT(order_products.store_product_id)), COUNT(DISTINCT(order_product_group.store_product_id))) total_references"))->first();


            $response = array(
                'id' => $picker->id,
                'first_name' => $picker->first_name,
                'last_name' => $picker->last_name,
                'email' => $picker->email,
                'phone' => $picker->phone,
                'photo_url' => $picker->photo_url,
                'city_id' => $picker->city_id,
                'picking_group_id' => '',
                'profile' => $picker->profile,
                'warehouse_id' => $picker->warehouse_id,
                'packing' => \Warehouse::where('id', $picker->warehouse_id)->pluck('use_packing'),
                'rol' => $picker->rol,
                'user_code' => $userCode
            );

            $this->response = array(
                'status' => true,
                'message' => 'Sesión iniciada',
                'result' => array('picker' => $response,
                    'productivity' => $productivity,
                    'isPlanning' => false)
            );
        } else $this->response['message'] = 'El código es inválido, verifícalo.';

        return $this->jsonResponse();
    }

    /***
     * Registra asignacion de empacador en el pedido
     */
    public function packingOrderAssignment()
    {
        if (!Input::has('picker_id')) {
            $this->response = array(
                'status' => true,
                'message' => 'ID picker es requerido'
            );
            return Response::json($this->response, 200);
        }

        $id = Input::get('picker_id');
        if (!$picker = Picker::find($id)) {
            $this->response = array(
                'status' => true,
                'message' => 'ID picker no existe.'
            );
            return Response::json($this->response, 200);
        }

        if (!$order = Order::find(Input::get('order_id')))
            return $this->jsonResponse('ID pedido no existe.', 200);

        if ($order->status != 'Alistado' && $order->status != 'In Progress')
            return $this->jsonResponse('Pedido se encuentra en estado ' . $order->status, 200);

        if (!is_null($order->packing_admin_id) && $picker->id != $order->packing_admin_id)
            return $this->jsonResponse('El pedido se encuentra asignado a otro empacador.', 200);

        $order->packing_admin_id = $picker->id;
        $order->save();

        $route = \Routes::find($order->route_id);
        $this->response = array(
            'status' => true,
            'message' => 'Pedido Asignado',
            'result' => array(
                'order_id' => $order->id,
                'route' => $route ? $route->route : 'Sin planeación'
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Valicadion de canastillas entregadas por el picker
     */
    public function packingUpdateBaskets()
    {

        ini_set('post_max_size', '128M');
        if (!$order = Order::find(Input::get('order_id')))
            return $this->jsonResponse('ID pedido no existe.', 200);

        if (!Input::has('picking_baskets'))
            return $this->jsonResponse('No se han registrado canastillas.', 200);

        if ($order->status != 'Alistado' && $order->status != 'In Progress')
            return $this->jsonResponse('El estado del pedido no es valido. El pedido esta en estado: ' . $order->status);


        if (Input::has('picking_baskets')) {
            $baskets = json_decode(Input::get('picking_baskets'));
            if (!empty($baskets)) {
                $pickingBaskets = \OrderPickingBasket::validateAvailability($baskets);
                if (!$pickingBaskets) {
                    $cont = 0;
                    foreach ($baskets as $basket) {
                        $orderPickingBasket = new \OrderPickingBasket();
                        $orderPickingBasket->order_id = $order->id;
                        $orderPickingBasket->type = 'Seco';
                        $orderPickingBasket->code_basket = $basket;
                        $orderPickingBasket->save();
                        $cont++;
                    }
                    $order->picking_baskets = $cont;
                    $order->save();
                } else {
                    if (count($pickingBaskets) > 1)
                        return $this->jsonResponse("Las canastillas se encuentran en uso en otro pedido.");
                    else
                        return $this->jsonResponse("La canastilla se encuentra en uso en otro pedido.");
                }
            }
        }

        $this->response = array(
            'status' => true,
            'message' => 'Canastillas registradas con éxito.',
            'result' => array(
                'order' => $order->id
            )
        );
        return $this->jsonResponse();
    }

    /**
     * Registra la sesión del picker
     *
     */
    private function registerPickerSession($picker, $device_serial, $device_battery)
    {
        $session = new \PickerSession();
        $session->picker_id = $picker->id;
        $session->picking_date_start = date('Y-m-d H:i:s');
        $session->rol = $picker->rol;
        $session->device_serial = $device_serial;
        $session->device_battery_level = $device_battery;
        $session->save();
    }


    /**
     * Get grupo de alistamiento a cargar
     *
     * @return array $response Respuesta
     */
    public function getPickingGroup()
    {
        if (Input::has('warehouse_id')) {
            Session::put('warehouse_id', Input::get('warehouse_id'));
            $picking_groups = \PickingGroup::select('picking_group.id', 'picking_group')
                ->where('picking_group.warehouse_id', Input::get('warehouse_id'))
                ->where('picking_group.status', 1)
                ->get();

            $this->response = array(
                'status' => true,
                'message' => 'picking group',
                'result' => array('picking_groups' => $picking_groups)
            );
        } else $this->response['message'] = 'Bodega es requerido.';
        return $this->jsonResponse();
    }

    /**
     * Setea grupo de alistamiento a cargar
     *
     * @return array $response Respuesta
     */
    public function setPickingGroup()
    {
        if (!Input::has('warehouse_id'))
            $this->response['message'] = 'Bodega es requerido.';
        else {
            if (Input::has('picking_group_id')) {
                Session::put('picking_group', Input::get('picking_group_id'));
                $this->response = array(
                    'status' => true,
                    'message' => 'Grupo de alistamiento guardado'
                );
            } else $this->response['message'] = 'Grupo de alistamiento es requerido.';
        }

        return $this->jsonResponse();
    }

    /**
     * Cierra sesion
     */
    public function logout()
    {
        if (!Input::has('picker_id')) {
            $this->response = array(
                'status' => true,
                'message' => 'ID picker es requerido'
            );
            return Response::json($this->response, 200);
        }

        $picker_session = \PickerSession::where('picker_id', Input::get('picker_id'))->orderby('created_at', 'desc')->first();
        if ($picker_session) {
            if ($picker_session->session_actived === 1) {
                $picker_session->picking_date_end = date('Y-m-d H:i:s');
                $picker_session->session_actived = 0;
                $picker_session->save();
            }

            $date = Carbon::now()->format('Y-m-d H:i:s');
            $pickingOrders = array();

            $picker = Picker::find(Input::get('picker_id'));
            //verificar si tiene un pedido pendiente asignado
            $order = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
                'picking_group', 'picking_group_id', 'order_groups.user_comments AS user_comments', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
                DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"),
                DB::raw('expected_containers->>"$.total" AS recommended_baskets'))
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->leftjoin('routes', 'route_id', '=', 'routes.id')
                ->leftjoin('zones', 'routes.zone_id', '=', 'zones.id')
                ->leftjoin('picking_group', 'picking_group_id', '=', 'picking_group.id')
                ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
                ->whereNull('order_picking_quality.id');

            $order->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress')
                    ->orWhere('orders.status', '=', 'Initiated');
            });

            if ($picker->profile == 'Alistador Seco') {
                $order->where('orders.picker_dry_id', $picker->id);
                $order->whereNotNull('orders.picking_dry_start_date');
                $order->whereNull('orders.picking_dry_end_date');
            } else {
                $order->where('orders.picker_cold_id', $picker->id);
                $order->whereNotNull('orders.picking_cold_start_date');
                $order->whereNull('orders.picking_cold_end_date');
            }

            $order = $order->first();
            if ($order) {
                if ($picker->profile == 'Alistador Seco') {
                    $order->picker_dry_id = NULL;
                    $order->picking_dry_start_date = NULL;
                    $order->picking_dry_end_date = NULL;
                } else {
                    $order->picker_cold_id = NULL;
                    $order->picking_cold_start_date = NULL;
                    $order->picking_cold_end_date = NULL;
                }
                $order->save();
            }
        }

        Session::flush();
        $this->response = array(
            'status' => true,
            'message' => 'Sesión cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Asignar pedido a picker
     *
     * @return object $response Respuesta
     */
    public function pickingOrderAssign($id)
    {
        $orderId = Input::get('orderId');
        $route = NULL;
        if (!is_numeric($id))
            return $this->jsonResponse('ID picker debe ser numérico.', 200);

        if (!$picker = Picker::find($id))
            return $this->jsonResponse('ID picker no existe.', 200);

        if (!empty($orderId) && $orderId != "") {
            if (!$order = Order::find($orderId))
                return $this->jsonResponse('ID pedido no existe.', 200);

            $route = Routes::where('id', '=', $order->route_id)->where('status', '=', 'Terminada')->first();
            if ($order->status == 'Cancelled' || $order->status == 'Delivered' || $order->status == 'Dispatched')
                return $this->jsonResponse('Pedido se encuentra en el estado ' . $order->status, 200);

            $date = date('Y-m-d');
            if (($order->delivery_date < date('Y-m-d 00:00:00', strtotime($date))) || ($order->delivery_date > date('Y-m-d 23:59:59', strtotime($date . ' +1 days'))))
                return $this->jsonResponse('Pedido se encuentra fuera del tiempo de entrega', 200);

            if ($picker->profile == 'Alistador Seco' && !is_null($order->picker_dry_id) && !is_null($order->picking_dry_end_date))
                return $this->jsonResponse('Pedido se encuentra alistado.', 200);
            elseif ($picker->profile == 'Alistador Frío' && !is_null($order->picker_cold_id) && !is_null($order->picking_cold_end_date))
                return $this->jsonResponse('Pedido se encuentra alistado.', 200);

            $orderGroup = \OrderGroup::find($order->group_id);
            if ($orderGroup->warehouse_id != Input::get('warehouse_id'))
                return $this->jsonResponse('El pedido no pertenece a esta bodega.', 200);

            if ($picker->profile == 'Alistador Seco' && !is_null($order->picker_dry_id) && $picker->id != $order->picker_dry_id)
                return $this->jsonResponse('el pedido  se encuentra asignado a otro picker.', 200);
            elseif ($picker->profile == 'Alistador Frío' && !is_null($order->picker_cold_id) && $picker->id != $order->picker_cold_id)
                return $this->jsonResponse('el pedido  se encuentra asignado a otro picker.', 200);
        }

        $orderAssigned = $this->assignOrder($picker, Input::get('picking_group_id'), Input::get('warehouse_id'), $orderId, $route);
        if (isset($orderAssigned['product_dry']) && $orderAssigned['product_dry'] == 0 && $picker->profile == 'Alistador Seco')
            return $this->jsonResponse('Pedido no contiene producto de seco.', 200);
        elseif (isset($orderAssigned['product_quantity_cold']) && $orderAssigned['product_quantity_cold'] == 0 && $picker->profile == 'Alistador Frío')
            return $this->jsonResponse('Pedido no contiene producto de fríos.', 200);
        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'order' => $orderAssigned
            )
        );
        return $this->jsonResponse();
    }

    /**
     * Función recursiva para buscar pedido sin picker asignado
     *
     * @param $picker
     * @param $pickingGroupId
     * @param $warehouseId
     * @param $orderId
     * @param $route
     * @return response
     */
    private function assignOrder($picker, $pickingGroupId, $warehouseId, $orderId, $route)
    {
        $warehouse = \Warehouse::find($warehouseId);
        $assignedOrder = PickingGroup::assignOrder($picker, $pickingGroupId, $warehouse, $orderId, $route);
        if (empty($assignedOrder)) {
            if($warehouse->without_planning){
                $assignedOrder = $this->assignOrderWithoutPlanning($picker, $orderId, $warehouse);
            }
            return $assignedOrder;
        } else {
            if (!empty($assignedOrder) && $picker->profile == 'Alistador Seco' && $assignedOrder['picker_dry_id'] != "" && $picker->id == $assignedOrder['picker_dry_id']) {
                return $assignedOrder;
            } else {
                if (!empty($assignedOrder) && $picker->profile != 'Alistador Seco' && $assignedOrder['picker_cold_id'] != "" && $picker->id == $assignedOrder['picker_cold_id']) {
                    return $assignedOrder;
                }
            }
        }
    }

    /**
     * Función recursiva para buscar pedido sin picker asignado y sin planeación
     * @param $picker
     * @param $orderId
     * @param $warehouseId
     * @return response
     */
    private function assignOrderWithoutPlanning($picker, $orderId, $warehouse)
    {
        $assignedOrder = PickingGroup::assignOrder($picker, null,  $warehouse, $orderId, null, true);

        if (empty($assignedOrder)) {
            return $assignedOrder;
        } else {
            if ($picker->profile == 'Alistador Seco' && $assignedOrder['picker_dry_id'] != "" && $picker->id == $assignedOrder['picker_dry_id']) {
                return $assignedOrder;
            } else {
                if ($picker->profile != 'Alistador Seco' && $assignedOrder['picker_cold_id'] != "" && $picker->id == $assignedOrder['picker_cold_id']) {
                    return $assignedOrder;
                }
            }
        }
    }

    /**
     * Autenticacion de pusher
     */
    public function pusher_auth()
    {
        if (\Auth::check() || Session::get('user_logged_in')){
            $response = Response::make(  \Pusherer::auth_push( Input::get('channel_name'), Input::get('socket_id'), 'picker' ), 200);
            return $response->header('Content-Type', 'application/json');
        }

        return $this->jsonResponse();
    }
}