<?php

namespace api\picking\v12;

use exceptions\PayUException;
use Illuminate\Database\QueryException;
use Input, ErrorLog, OrderLog, Routes, DB, Picker, Order, OrderGroup, OrderProduct, Response, Exception, Session, Carbon\Carbon, OrderProductGroup, Event, PickingGroup;

class ApiOrderController extends \ApiController
{
    public function __construct()
    {
        //Session::put('picker_id', 793);
        $this->beforeFilter(function () {
            if (!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
            else
                $this->picker_id = Session::get('picker_id');
        });
        parent::__construct();
    }

    /**
     * Obtener productos de un pedido para alistar
     *
     * @return array $response Respuesta
     */
    public function pickingProducts($id, $order_id)
    {
        if (!is_numeric($id))
            return $this->jsonResponse('ID picker debe ser numérico.', 200);

        if (!is_numeric($order_id))
            return $this->jsonResponse('ID de pedido debe ser numérico.', 200);

        if (!$picker = Picker::find($id))
            return $this->jsonResponse('ID picker no existe.', 200);

        if (!$picker = Picker::find(Session::get('picker_id')))
            return $this->jsonResponse('Sesión no valida.', 200);

        if (!$order = Order::find($order_id))
            return $this->jsonResponse('ID pedido no existe.', 200);

        if ($order->status == 'Alistado')
            return $this->jsonResponse('El pedido ya esta alistado.');

        if (($picker->profile == 'Alistador Seco' && !empty($order->picker_dry_id) && $order->picker_dry_id != $picker->id) ||
            ($picker->profile == 'Alistador Frío' && !empty($order->picker_cold_id) && $order->picker_cold_id != $picker->id)) {
            //obtener siguiente pedido a alistar
            $order = PickingGroup::getNextOrderTemp($picker, Session::get('picking_group'), false, Session::get('warehouse_id'));

            $this->response = array(
                'status' => false,
                'message' => 'El pedido ya esta asignado a otro alistador por favor toma el siguiente.',
                'result' => [
                    'order' => $order
                ]
            );

            return $this->jsonResponse();
        }

        $order->allocated_date = Carbon::now()->format('Y-m-d H:i:s');
        $picking_start_date = Carbon::now()->format('Y-m-d H:i:s');

        if ($picker->profile == 'Alistador Seco' && is_null($order->picker_dry_id)) {
            $order->picker_dry_id = $picker->id;
            $order->picking_dry_start_date = $picking_start_date;
        }

        if ($picker->profile == 'Alistador Frío' && is_null($order->picker_cold_id)) {
            $order->picker_cold_id = $picker->id;
            $order->picking_cold_start_date = $picking_start_date;
        }

        $order->save();
        $products = PickingGroup::getProductsListV12($picker, $order->id, Session::get('picker_warehouse_id'));

        $this->response = array(
            'status' => true,
            'message' => 'Pedido actualizado.',
            'result' => [
                'products' => json_encode($products),
                'secuence' => $order->planning_sequence,
                'route' => Routes::where('id', $order->route_id)->pluck('route')
            ]
        );

        return $this->jsonResponse();
    }

    /*
     * Cobrar total de pedido a tarjeta de cliente
     */
    private function charge_order($order)
    {
        $response = array(
            'status' => false
        );

        if ($order && ($order->status == 'In Progress' || $order->status == 'Enrutado')) {
            try {
                $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && !$order->getPaymentStatus()) {
                    $log = new OrderLog();
                    $result = Order::chargeCreditCard($order->id, $order->user_id, $this->picker_id);
                    if (!$result['status'])
                        throw new Exception($result['message'], 1);
                    else {
                        $log->type = 'Cobro a tarjeta exitoso.';
                        $log->picker_id = $this->picker_id;
                        $log->order_id = $order->id;
                        $log->save();

                            $response = array(
                                'status' => true,
                                'message' => $result['message'],
                            );
                            return $response;
                        }
                } else {
                    $response = array(
                        'status' => true,
                        'message' => 'No es necesario realizar cobro a la tarjeta.',
                    );
                }

            } catch (Exception $e) {
                $response['message'] = $e->getMessage();
            }
        } else $response['message'] = 'No se pudo realizar el cobro por que el estado del pedido debe estar En proceso o Alistado.';

        return $response;
    }

    /**
     * Actualizar estado de pedido
     *
     * @return array $response Respuesta
     */
    public function updateOrderStatus($id)
    {
        ini_set('post_max_size', '128M');

        try {
            DB::beginTransaction();

            if (!Input::has('picker_id') || !Input::has('products')) {
                DB::rollback();
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 200);
            }

            if (!$order = Order::find($id)) {
                DB::rollback();
                return $this->jsonResponse('ID pedido no existe.', 200);
            }

            if ($order->status == 'Alistado') {
                DB::rollback();
                return $this->jsonResponse('El pedido ya esta alistado.');
            }

            $products = json_decode(Input::get('products'));
            if (!count($products)) {
                DB::rollback();
                return $this->jsonResponse('La información de los productos esta vacia.', 200);
            }

            $picker = Picker::find($this->picker_id);
            if ($picker->profile == 'Alistador Seco' && !Input::has('picking_baskets')) {
                DB::rollback();
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 200);
            }

            if ($order->status != 'Enrutado' && $order->status != 'In Progress' && $order->status != 'Initiated') {
                DB::rollback();
                return $this->jsonResponse('El estado del pedido no es valido. El pedido esta en estado: ' . $order->status);
            }

            if ($picker->profile == 'Alistador Seco' && !is_null($order->picker_dry_id) && $order->picker_dry_id != $this->picker_id) {
                DB::rollback();
                return $this->jsonResponse('El pedido ya esta asignado a otro alistador.');
            }

            if ($picker->profile == 'Alistador Frío' && !is_null($order->picker_cold_id) && $order->picker_cold_id != $this->picker_id) {
                DB::rollback();
                return $this->jsonResponse('El pedido ya esta asignado a otro alistador.');
            }


            $order->picking_date = Carbon::now()->format('Y-m-d H:i:s');
            if (empty($order->received_date))
                $order->received_date = Carbon::now()->format('Y-m-d H:i:s');

            $orderGroup = OrderGroup::find($order->group_id);
            if ($picker->profile == 'Alistador Seco') {
                $validStorage = ['Seco'];
                $order->picking_dry_end_date = Carbon::now()->format('Y-m-d H:i:s');
                $orderGroup->app_version_picking_dry = Input::get('app_version');
            }

            if ($picker->profile == 'Alistador Frío') {
                $validStorage = ['Refrigerado', 'Congelado'];
                $order->picking_cold_end_date = Carbon::now()->format('Y-m-d H:i:s');
                $orderGroup->app_version_picking_cold = Input::get('app_version');
            }
            $orderGroup->save();
            //actualizar estado de productos
            foreach ($products as $product) {
                if ($product->quantity <= 0 && $product->fulfilment_status == 'Fullfilled') {
                    DB::rollback();
                    return $this->jsonResponse("Las cantidades de los productos no pueden ser negativas o cero");
                }

                if ($product->fulfilment_status != 'Pending') {
                    $product->storage = !isset($product->storage) ? ['Seco', 'Refrigerado', 'Congelado'] : $product->storage;
                    if ($product->type == 'Agrupado') {

                        $order_product_group_ids = [];
                        foreach ($product->order_product_group_ids AS $key => $val) {
                            $order_product_group_ids[] = $key;
                        }

                        $orderProductGroups = OrderProductGroup::whereIn('order_product_group.id', $order_product_group_ids)->get();
                        if (count($orderProductGroups) > 1) {
                            $quantity = $product->quantity;
                            foreach ($orderProductGroups AS $orderProductGroup) {
                                if ($orderProductGroup->fulfilment_status == 'Pending' && in_array($product->storage, $validStorage)) {
                                    $orderProductGroup->fulfilment_status = $product->fulfilment_status;

                                    if ($quantity >= $orderProductGroup->quantity) {
                                        $orderProductGroup->fulfilment_status = 'Fullfilled';
                                        $quantity -= $orderProductGroup->quantity;
                                    } else {
                                        $orderProductGroup->fulfilment_status = 'Missing';
                                        $orderProductGroup->quantity = $quantity;
                                    }
                                    $orderProductGroup->picking_date = date('Y-m-d H:i:s', strtotime($product->picking_date));
                                    $orderProductGroup->save();
                                    $orderProductGroupList[] = $orderProductGroup;

                                    \Log::info('Modificado estado del producto grupo:'.$orderProductGroup->id. ' a '.$orderProductGroup->fulfilment_status. ' en la app picking V12');
                                    Event::fire('order.product_log', [[$orderProductGroup, $this->picker_id, 'picker']]);
                                }
                            }
                        } else {
                            $orderProductGroupObject = OrderProductGroup::find($product->id);
                            if ($orderProductGroupObject->fulfilment_status == 'Pending' && in_array($product->storage, $validStorage)) {
                                $orderProductGroupObject->fulfilment_status = $product->fulfilment_status;
                                $orderProductGroupObject->picking_date = date('Y-m-d H:i:s', strtotime($product->picking_date));
                                $orderProductGroupObject->quantity = $product->quantity;
                                $orderProductGroupObject->save();
                                $orderProductGroupList[] = $orderProductGroupObject;
                                \Log::info('Modificado estado del producto grupo:'.$orderProductGroup->id. ' a '.$orderProductGroup->fulfilment_status. ' en la app picking V12');
                                Event::fire('order.product_log', [[$orderProductGroupObject, $this->picker_id, 'picker']]);
                            }
                        }
                    } else {
                        $orderProduct = OrderProduct::find($product->id);

                        if (empty($orderProduct)) {
                            DB::rollback();
                            return $this->jsonResponse("El siguiente producto " . $product->name . " fue eliminado desde el dashboard por favor cierre e ingrese de nuevo al app.");
                        }

                        if ($orderProduct->fulfilment_status == 'Pending' && in_array($product->storage, $validStorage)) {
                            //validar si el producto esta con special y full price
                            $orderProductsPerPrice = OrderProduct::where('store_product_id', $orderProduct->store_product_id)
                                ->where('order_id', $orderProduct->order_id)
                                ->where('type', 'Product')
                                ->where('fulfilment_status', 'Pending')
                                ->orderBy('price')
                                ->get();

                            if (count($orderProductsPerPrice) > 1) {
                                foreach ($orderProductsPerPrice as $key => $orderProductPerPrice) {
                                    //producto con precio especial
                                    if ($key == 0) {
                                        if ($product->quantity < $orderProductPerPrice->quantity) {
                                            $orderProductPerPrice->quantity = $product->quantity;
                                            $product->quantity = 0;
                                        }
                                        $orderProductPerPrice->fulfilment_status = $product->fulfilment_status;
                                        $product->quantity -= $orderProductPerPrice->quantity;
                                    } else {
                                        //producto con precio full
                                        if ($product->quantity <= 0)
                                            $orderProductPerPrice->fulfilment_status = 'Missing';
                                        else {
                                            $orderProductPerPrice->quantity = $product->quantity;
                                            $orderProductPerPrice->fulfilment_status = $product->fulfilment_status;
                                        }
                                    }
                                    $orderProductPerPrice->picking_date = date('Y-m-d H:i:s', strtotime($product->picking_date));
                                    $orderProductPerPrice->save();
                                    \Log::info('Modificado estado del producto:'.$orderProductPerPrice->id. ' a '.$orderProductPerPrice->fulfilment_status. ' en la app picking V12');
                                    Event::fire('order.product_log', [[$orderProductPerPrice, $this->picker_id, 'picker']]);
                                }
                            } else {
                                if ($orderProduct->quantity == $product->quantity)
                                    $orderProduct->fulfilment_status = $product->fulfilment_status;
                                else
                                    $orderProduct->fulfilment_status = 'Missing';

                                $orderProduct->quantity = $product->quantity;
                                $orderProduct->picking_date = date('Y-m-d H:i:s', strtotime($product->picking_date));
                                $orderProduct->save();

                                \Log::info('Modificado estado del producto:'.$orderProduct->id. ' a '.$orderProduct->fulfilment_status. ' en la app picking V12');
                                Event::fire('order.product_log', [[$orderProduct, $this->picker_id, 'picker']]);
                            }
                        }
                    }
                }
            }

            if (isset($orderProductGroupList)) {
                foreach ($orderProductGroupList as $order_product_group_item)
                    OrderProductGroup::updateProductStatus($order_product_group_item->order_product_id);
            }

            //actualizar total de pedido
            $orderProducts = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status', ['Pending', 'Missing'])->count();
            $hasPendingProducts = $orderProducts ? true : false;
            Event::fire('order.total_log', [[$order, $this->picker_id, 'picker']]);


            //actualizar pedido a Alistado

            $orderValidation = Order::find($order->id);
            if ($orderValidation->status == 'Alistado') {
                \Log::info('Rollback de pedido:' . $order->id . ' se encontraba en estado ' . $order->status . ' en la app picking V11');
                return $this->jsonResponse('El pedido fue alistado en otro proceso.');
            } else {
                if (!$hasPendingProducts) {
                    $order->updatePickingStock();
                    if (($order->status == 'Enrutado' || $order->status == 'In Progress') && $order->payment_method == 'Tarjeta de crédito') {
                        $result = $this->charge_order($order);
                        if (!$result['status'])
                            throw new PayUException($result['message']);
                    }

                    $order->picking_date = Carbon::now()->format('Y-m-d H:i:s');
                    $order->status = 'Alistado';
                } else $order->status = 'In Progress';

                if (Input::has('picking_baskets') && $picker->profile == 'Alistador Seco') {
                    $baskets = json_decode(Input::get('picking_baskets'));
                    if (!empty($baskets)) {
                        $pickingBaskets = \OrderPickingBasket::validateAvailability($baskets);
                        if (!$pickingBaskets) {
                            $cont = 0;
                            foreach ($baskets as $basket) {
                                $orderPickingBasket = new \OrderPickingBasket();
                                $orderPickingBasket->order_id = $order->id;
                                $orderPickingBasket->type = 'Seco';
                                $orderPickingBasket->code_basket = $basket;
                                $orderPickingBasket->save();
                                $cont++;
                            }
                            $order->picking_baskets = $cont;
                        } else {
                            $cold = "";
                            if ($picker->profile == 'Alistador Seco') {
                                $basketText = 'canastilla';
                            } else {
                                $basketText = 'bolsa';
                                $cold = 'de frío ';
                            }

                            if (count($pickingBaskets) > 1)
                                return $this->jsonResponse("la " . $basketText . " " . $cold . $pickingBaskets . " se encuentra en uso en otro pedido.");
                            else
                                return $this->jsonResponse("las " . $basketText . "s " . $cold . $pickingBaskets . " se encuentran en uso en otro pedido.");
                        }
                    }
                }

                if (Input::has('picking_bags')) {
                    $bags = json_decode(Input::get('picking_bags'));
                    if (!empty($bags)) {
                        if ($picker->profile == 'Alistador Frío') {
                            $pickingBags = \OrderPickingBasket::validateAvailability($bags);
                            if ($pickingBags) {
                                if (count($pickingBags) > 1)
                                    return $this->jsonResponse("La bolsa de frío " . $pickingBags . " se encuentra en uso en otro pedido.");
                                else
                                    return $this->jsonResponse("Las bolsas de frío " . $pickingBags . " se encuentran en uso en otro pedido.");
                            }
                        }

                        $cont = 0;
                        foreach ($bags as $bag) {
                            $orderPickingBag = \OrderPickingBasket::where('code_basket', $bag)
                                ->where('order_id', $order->id)
                                ->first();

                            if (!empty($orderPickingBag)) {
                                $orderPickingBag->validated = 1;
                            } else {
                                $orderPickingBag = new \OrderPickingBasket();
                                $orderPickingBag->order_id = $order->id;
                                $orderPickingBag->type = 'Frío';
                                $orderPickingBag->code_basket = $bag;

                            }
                            $orderPickingBag->save();
                            $cont++;
                        }
                        $order->picking_bags = $cont;
                    }
                }

                $order->updateTotals();
                $order->save();

                DB::commit();
                Event::fire('order.status_log', [[$order, $this->picker_id, 'picker']]);
                \Log::info('Modificando estado de pedido: ' . $order->id . ' a ' . $order->status . ' desde app picking V12');
            }

            //obtener siguiente pedido a alistar
        } catch (PayUException $e) {
            DB::rollback();

            $log = new OrderLog();
            $log->type = $e->getMessage();
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return $this->jsonResponse($e->getMessage());

        }  catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                \Log::info('Rollback en actualización de inventario: ' . $e->getMessage() . ' - desde app picking V12');
                return $this->jsonResponse('Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.\'');
            }
            return $this->jsonResponse('Ocurrió un error al actualizar los datos.');
        } catch (Exception $e) {
            DB::rollback();
            ErrorLog::add($e, 513);
            \Log::info('Rollback en actualización de estado de pedido: ' . $e->getMessage() . ' - desde app picking V12');
            return $this->jsonResponse('Ocurrió un error al actualizar los datos.');
        }
        $picker = Picker::find(Input::get('picker_id'));
        $date = Carbon::now();
        $productivity = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('order_product_group', 'order_product_group.order_id', '=', 'orders.id');
        if ($picker->profile == 'Alistador Seco') {
            $productivity->where('orders.picking_dry_end_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.picking_dry_end_date', '<', date('Y-m-d 23:59:59', strtotime($date)))
                ->where('picker_dry_id', $picker->id);
        } else {
            $productivity->where('orders.picking_cold_end_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.picking_cold_end_date', '<', date('Y-m-d 23:59:59', strtotime($date)))
                ->where('picker_cold_id', $picker->id);
        }
        $productivity = $productivity->select(DB::raw('COUNT(DISTINCT(orders.id)) AS total_orders'),
            DB::raw('if(order_products.type = "Product" OR order_products.type = "Muestra", sum(order_products.quantity), sum(order_product_group.quantity)) total_products'),
            DB::raw("if(order_products.type = 'Product' OR order_products.type = 'Muestra', COUNT(DISTINCT(order_products.store_product_id)), COUNT(DISTINCT(order_product_group.store_product_id))) total_references"))->first();


        $routeFinish = Routes::where('id', '=', $order->route_id)->where('status', '=', 'Terminada')->first();
        $this->response = array(
            'status' => true,
            'message' => 'Estado de pedido actualizado.',
            'result' => [
                'productivity' => $productivity,
                'isPlanning' => false,
                'route' => $routeFinish == null ? null : $routeFinish->route,
                'sequence' => $routeFinish == null ? $order->picking_sequence_letter : $order->sequence,
            ]
        );

        return $this->jsonResponse();
    }

    /**
     * Obtener la productividad diaria de un picker
     *
     * @return object $response Respuesta
     *
     */
    public function getPickerProductivity()
    {
        $orders = $products = $references = 0;
        $picker = Picker::find($this->picker_id);
        $order = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('order_product_group', 'order_product_group.order_id', '=', 'orders.id');

        if ($picker->profile == 'Alistador Seco') {
            $order->whereRaw('DATE_FORMAT(picking_dry_start_date, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->where('picker_dry_id', $this->picker_id);
        } else {
            $order->whereRaw('DATE_FORMAT(picking_cold_start_date, "%Y-%m-%d")= "' . date('Y-m-d') . '"')
                ->where('picker_cold_id', $this->picker_id);
        }
        $order->select(DB::raw('COUNT(DISTINCT(orders.id)) AS total_orders'),
            DB::raw('if(order_products.type = "Product" OR order_products.type = "Muestra", sum(order_products.quantity - order_products.quantity_missing), sum(order_product_group.quantity - order_product_group.quantity_missing)) total_products'),
            DB::raw("if(order_products.type = 'Product' OR order_products.type = 'Muestra', COUNT(DISTINCT(order_products.store_product_id)), COUNT(DISTINCT(order_product_group.store_product_id))) total_references"))->first();
        IF (count($order) == 1) {
            $orders = $order->total_orders;
            $products = $order->total_products;
            $references = $order->total_references;
        }

        $this->response = array(
            'status' => true,
            'result' => array(
                'orders' => $orders,
                'products' => $products,
                'references' => $references
            )
        );
    }


    /**
     * Devuelve los datos de una orden para el picker de frio
     *
     * @return Response
     *
     */
    public function getOrderProductsColdStatus()
    {
        $picker_id = Input::get('picker_id');
        $order_id = Input::get('id');

        if (!is_numeric($picker_id))
            return $this->jsonResponse('ID picker debe ser numérico.', 200);

        if (!is_numeric($order_id))
            return $this->jsonResponse('ID de pedido debe ser numérico.', 200);

        if (!$picker = Picker::find($picker_id))
            return $this->jsonResponse('ID picker no existe.', 200);

        if (!$order = Order::find($order_id))
            return $this->jsonResponse('ID pedido no existe.', 200);

        if ($order->status == 'Alistado')
            return $this->jsonResponse('El pedido ya esta alistado.');

        $this->response = array(
            'status' => true,
            'message' => 'Datos de orden',
            'result' => [
                'order' => $order
            ]
        );

        return $this->jsonResponse();

    }
}
