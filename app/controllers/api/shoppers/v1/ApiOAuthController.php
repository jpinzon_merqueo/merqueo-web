<?php

namespace api\shoppers\v1;

use Input, OAuthRequest, OAuthUtil, ApiRequestToken, ApiConsumer, Shopper;

class ApiOAuthController extends \ApiController {

	/**
	 * Devuelve un Request Token no autorizado
	 */
	public function request_token()
	{
		try {
		    $request = OAuthRequest::from_request();
		    $token = $this->oauth_server->fetch_request_token($request);

			$this->response = array(
				'status' => true,
				'message' => 'oauth_token='.OAuthUtil::urlencode_rfc3986($token->key).'&oauth_token_secret='.OAuthUtil::urlencode_rfc3986($token->secret)
			);
		}catch(OAuthException $e){
			return $this->jsonResponse($e->getMessage(), 401);
		}

		$this->setResponseFormat('plaintext');
		return $this->jsonResponse();
	}

	/**
	 * Autoriza un Request Token
	 */
	public function authorize_request_token()
	{
		//LoginSystem::ensure_user_is_logged_in();
		$this->auth_user_id = Input::has('shopper_id') ? Input::get('shopper_id') : 0;

		$oauth_token = $oauth_verifier = false;
		$headers = getallheaders();
		if (isset($headers['Authorization'])){
			$header_parameters = OAuthUtil::split_header($headers['Authorization']);
			if (isset($header_parameters['oauth_token']) && !empty($header_parameters['oauth_token']))
				$oauth_token = $header_parameters['oauth_token'];
			if (isset($header_parameters['oauth_verifier']) && !empty($header_parameters['oauth_verifier']))
				$oauth_verifier = $header_parameters['oauth_verifier'];
		}

		if (!$oauth_token)
			return $this->jsonResponse('Token is required', 401);
		if ($this->auth_user_id && !$oauth_verifier)
			return $this->jsonResponse('Token verifier is required', 401);

		$request = OAuthRequest::from_request();
		$token = ApiRequestToken::where('key', $oauth_token)->first();
		$consumer = ApiConsumer::find($token->consumer_id);

		if ($token && $consumer) {

			if ($token->expiry < date('Y-m-d H:i:s')){
				$token->delete();
				return $this->jsonResponse('Request token expired', 401);
			}

			if ($this->auth_user_id){
				$shopper = Shopper::find($this->auth_user_id);
				if ($shopper->oauth_verifier != $oauth_verifier)
					return $this->jsonResponse('Invalid token verifier', 401);
			}

		    $token->authorized = 1;
			$token->user_id = $this->auth_user_id;
			$token->save();

			$this->response = array(
				'status' => true,
				'message' => 'oauth_token='.OAuthUtil::urlencode_rfc3986($token->key)
			);

		}else{
			if ($token)
				$token->delete();
			return $this->jsonResponse('Unauthorized request token', 401);
		}

		$this->setResponseFormat('plaintext');
		return $this->jsonResponse();
	}

	/**
	 * Devuelve un Request Token autorizado (Access Token)
	 */
	public function request_access_token()
	{
		try {
			$request = OAuthRequest::from_request();
			$token = $this->oauth_server->fetch_access_token($request);

			$this->response = array(
				'status' => true,
				'message' => 'oauth_token='.OAuthUtil::urlencode_rfc3986($token->key).'&oauth_token_secret='.OAuthUtil::urlencode_rfc3986($token->secret)
			);
		}catch(OAuthException $e){
		    return $this->jsonResponse($e->getMessage(), 401);
		}
		$this->setResponseFormat('plaintext');
		return $this->jsonResponse();
	}
}