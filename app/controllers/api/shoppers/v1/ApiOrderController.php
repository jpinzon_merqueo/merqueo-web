<?php

namespace api\shoppers\v1;

use Input, Hash, Route, DB, Config, Shopper, Order, OrderGroup, OrderProduct, OrderLog, ShopperZone, Admin, StoreBranch,
Supermarket, User, UserCredit, Store, ShopperMovement, Response, Exception, Shopper;

class ApiOrderController extends \ApiController {

    private $shopper_id;

    /**
     * Actualizar estado de pedido
     *
     * @return array $response Respuesta
     */
    public function update_order_status()
    {
        if (!$order_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID pedido es requerido.', 400);

        if (!Input::has('shopper_id') || !Input::has('status'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if (!$order = Order::find($order_id))
            return $this->jsonResponse('ID pedido no existe.', 400);

        if ($order->status == Input::get('status')) {
            return $this->jsonResponse('El pedido ya fue actualizado al estado "' . $this->config['order_statuses'][Input::get('status')] . '", por favor cierra el App y vuelve a ingresar para actualizar la información.');

        }else{
            $this->shopper_id = Input::get('shopper_id');

            if ($order->status == 'Cancelled')
              return $this->jsonResponse('No puedes tomar este pedido por que fue cancelado por favor cierra el App y vuelve a ingresar para actualizar la información.');

            $new_status = Input::get('status');

            if ($new_status != 'In Progress' && $order->shopper_id != $this->shopper_id)
                return $this->jsonResponse('Pedido no asignado al shopper.');

            //pedido a In Progress
            if ($new_status == 'In Progress')
            {
                if ($order->shopper_id != 0 && $order->shopper_id != $this->shopper_id)
                    return $this->jsonResponse('El pedido ya fue asignado a otro shopper.');

                $orders_pending = Order::where('shopper_id', $this->shopper_id)->where('status', 'In Progress')->count();
                if ($orders_pending > 2)
                    return $this->jsonResponse('Debes entregar los pedidos pendientes para aceptar nuevos pedidos.');

                //validar que tome solo tome los dos primeros pedidos
                if (false && !$order->shopper_id)
                {
                    $shopper_zones = ShopperZone::select('zone_id')->join('zones', 'zones.id', '=', 'zone_id')->where('shopper_id', $this->shopper_id)->where('status', 1)->get();
                    $shopper = Shopper::find($this->shopper_id);
                    $days = $this->shopper_id == 10 ? 5 : 1;
                    $orders = array();
                    $orders_no_allocated = Order::join('users','orders.user_id','=','users.id')->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                                    ->join('stores','orders.store_id','=','stores.id')
                                    ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                                    ->where('orders.status', DB::raw("'Initiated'"))
                                    ->where('order_groups.user_city_id', $shopper->city_id)
                                    ->where('orders.shopper_id', 0)
                                    ->where(DB::raw("DATE_SUB(orders.delivery_date, INTERVAL IF(orders.delivery_time = 'Immediately', 110, 230) MINUTE)"), '<=', date('Y-m-d H:i:s'))
                                    ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'));
                    if ($shopper && !empty($shopper->stores)){
                        $shopper_stores = explode(',', $shopper->stores);
                        $orders_no_allocated->whereIn('orders.store_id', $shopper_stores);
                    }
                    $orders_no_allocated = $orders_no_allocated->select('orders.id', 'order_groups.user_address_latitude', 'order_groups.user_address_longitude')
                                           ->orderBy('scheduled_delivery', 'desc')->orderBy('orders.delivery_date')->get();

                    foreach ($orders_no_allocated as $order_pending)
                    {
                        if (empty($order_pending->zone_id)){
                            $orders[] = '#'.$order_pending->id;
                        }else{
                            //validar si shopper cubre la zona del pedido
                            if ($shopper_zones){
                                foreach ($shopper_zones as $zone){
                                    if ($order_pending->zone_id == $zone->zone_id){
                                        $orders[] = '#'.$order_pending->id;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (count($orders)){
                        $orders = array_slice($orders, 0, 2);
                        if (!in_array('#'.$order->id, $orders))
                            return $this->jsonResponse('Debes tomar solo los pedidos: '.trim(implode(', ', $orders), ', ').' que aparecen de primero en la lista.');
                    }
                }
            }

            $order_group = OrderGroup::find($order->group_id);

            //pedido a Delivered
            if ($new_status == 'Delivered')
            {
                if (!Input::has('image_invoice'))
                    return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

                if ((Input::get('customer_payment_method') == 'Datáfono' || Input::get('customer_payment_method') == 'Efectivo y datáfono') && !Input::has('image_voucher'))
                    return $this->jsonResponse('La foto del voucher del pedido es requerida.');

                if (!$this->upload_order_image(Input::get('image_invoice'), 'invoice'))
                    return $this->jsonResponse('Ocurrió un error al subir la imagen.');

                if ((Input::get('customer_payment_method') == 'Datáfono' || Input::get('customer_payment_method') == 'Efectivo y datáfono') && Input::has('image_voucher')){
                    if (!$this->upload_order_image(Input::get('image_voucher'), 'voucher'))
                        return $this->jsonResponse('Ocurrió un error al subir la imagen.');
                }

            }

            $user = User::find($order->user_id);

            //pedido a Dispatched
            if ($new_status == 'Dispatched')
            {
                //validar si ya se realizo el pago
                if ($order->payment_method == 'Tarjeta de crédito' && empty($order->cc_charge_id))
                    return $this->jsonResponse('Se debe realizar primero el cobro a la tarjeta de crédito.');

                if (!Input::has('products'))
                    return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

                if ($order->payment_method != 'Tarjeta de crédito')
                {
                    $products = json_decode(Input::get('products'));
                    if (!count($products))
                       return $this->jsonResponse('La información de los productos esta vacia.', 400);

                    //actualizar estado de productos
                    OrderProduct::where('order_id', $order->id)->where('type', 'Replacement')->delete();
                    foreach($products as $product)
                    {
                        if ($product->type == 'Product' || $product->type == 'Custom'){
                            $order_product = OrderProduct::find($product->id);
                            if ($product->type == 'Custom'){
                                $order_product->price = $product->price;
                                $order_product->original_price = $product->price;
                            }
                            $order_product->fulfilment_status = $product->status;
                            $order_product->save();
                        }else{
                            $order_product = new OrderProduct;
                            $order_product->price = $product->price;
                            $order_product->product_name = $product->name;
                            $order_product->fulfilment_status = $product->status;
                            $order_product->type = $product->type;
                            $order_product->store_id = $order->store_id;
                            $order_product->order_id = $order->id;
                            $order_product->product_image_url = image_url().'no_disponible.jpg';
                            $order_product->quantity = $product->quantity;
                            $order_product->save();
                        }
                    }
                    $total_amount = 0;
                    $order_products = OrderProduct::where('order_id', $order->id)->get();
                    foreach ($order_products as $order_product) {
                        if ($order_product->fulfilment_status == 'Fullfilled')
                            $total_amount += $order_product->price * $order_product->quantity;
                    }
                    $order->total_amount = $total_amount;

                    //recalcular descuento si es por porcentaje
                    if ($order->discount_percentage_amount)
                        $order->discount_amount = round($order->total_amount * ($order->discount_percentage_amount / 100), 0);

                    //validar total vs descuento
                    $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                    if ($total < 0 && $order->discount_amount)
                    {
                        $new_discount_amount = $order->total_amount + $order->delivery_amount;
                        if ($user_credit){
                            $user_credit = UserCredit::where('order_id', $order->id)->where('type', 0)->first();
                            $user_credit->amount = $new_discount_amount;
                            $user_credit->description = 'Deducción de $'.number_format($new_discount_amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
                            $user_credit->save();
                        }
                        $order->discount_amount = $new_discount_amount;
                    }

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'ORDER_TOTAL_UPDATE_APP: '.$order->total_amount;
                    $log->shopper_id = $order->shopper_id;
                    $log->order_id = $order->id;
                    $log->save();
                }
                $order->total_real_amount = Input::get('total_real_amount');
                if ($order_group->source == 'Callcenter' && $order->store_id == 26)
                    $order->total_amount = Input::get('total_real_amount');
                $order->save();

                //registrar movimiento de shopper
                if (ShopperMovement::where('shopper_id', $order->shopper_id)->where('status', 1)->first())
                {
                    $movement = new ShopperMovement;
                    $movement->order_id = $order->id;
                    $movement->shopper_id = $this->shopper_id;
                    $movement->movement = 'Gestión de pedido';
                    $movement->supermarket = Input::get('shopper_supermarket') == 'Otro' ? Input::get('shopper_supermarket_other') : Input::get('shopper_supermarket');
                    $movement->shopper_payment_method = Input::get('shopper_payment_method');
                    if (Input::get('shopper_payment_method') == 'Efectivo y tarjeta merqueo'){
                        $movement->shopper_cash_paid = Input::get('shopper_cash_paid');
                        $movement->shopper_card_paid = Input::get('shopper_card_paid');
                    }else{
                        $movement->shopper_cash_paid = Input::get('shopper_payment_method') == 'Efectivo' ? $order->total_real_amount : 0;
                        $movement->shopper_card_paid = Input::get('shopper_payment_method') == 'Tarjeta merqueo' ? $order->total_real_amount : 0;
                    }

                    if ($movement->shopper_payment_method == 'Efectivo y tarjeta merqueo' && $order->total_real_amount != ($movement->shopper_cash_paid + $movement->shopper_card_paid))
                        return $this->jsonResponse('El total ingresado en efectivo y con tarjeta merqueo no coincide con el total de la factura.');

                    ShopperMovement::saveMovement($movement, $order);
                }
            }

            if ($new_status == 'In Progress')
            {
                $this->deleteFirebaseData($order->id);

                $order->shopper_id = $this->shopper_id;
                $order->allocated_date = $order->received_date = date('Y-m-d H:i:s');

                $log = new OrderLog();
                $log->type = 'ORDER_ASSIGNED_TO_SHOPPER_APP: '.$this->shopper_id;
                $log->shopper_id = $this->shopper_id;
                $log->order_id = $order->id;
                $log->save();
            }
            if ($new_status == 'Dispatched')
                $order->dispatched_date = date('Y-m-d H:i:s');

            if ($new_status == 'Delivered'){
               if ($order->payment_method != 'Tarjeta de crédito')
                   $order->payment_date = date('Y-m-d H:i:s');
               $order->management_date = date('Y-m-d H:i:s');

               if ($order->payment_method != 'Tarjeta de crédito')
               {
                    $customer_payment_method = Input::get('customer_payment_method');
                    if (empty($customer_payment_method))
                        return $this->jsonResponse('El método de pago es requerido, por favor recarga el app.');

                    if ($order->payment_method != Input::get('customer_payment_method'))
                    {
                        $order->payment_method = $customer_payment_method;

                        $log = new OrderLog();
                        $log->type = 'ORDER_UPDATE_PAYMENT_METHOD_APP: '.$order->payment_method.' to '.$customer_payment_method;
                        $log->shopper_id = $order->shopper_id;
                        $log->order_id = $order->id;
                        $log->save();
                    }
               }

               if ($order->payment_method == 'Efectivo y datáfono'){
                    $total_order = $order_group->source == 'Callcenter' && $order->store_id == 26 ? $order->total_real_amount : $order->total_amount + $order->delivery_amount - $order->discount_amount;
                    if ($total_order != (Input::get('customer_cash_paid') + Input::get('customer_card_paid')))
                          return $this->jsonResponse('El total pagado en efectivo y con datáfono no coincide con el total del pedido/factura.');
               }

               if ($movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first())
                {
                    if (Shopper::where('id', $order->shopper_id)->where('profile', '<>', 'Shopper Cliente')->first())
                    {
                        if (Input::get('customer_payment_method') == 'Efectivo y datáfono'){
                            $movement->user_cash_paid = Input::get('customer_cash_paid');
                            $movement->user_card_paid = Input::get('customer_card_paid');
                        }else{
                            $movement->user_cash_paid = 0;
                            $movement->user_card_paid = 0;
                        }
                        ShopperMovement::saveMovement($movement, $order);
                    }
                }

            }
            $order->status = $new_status;
            $order->save();

            if (empty($order->management_date))
                $this->setFirebaseData(array($order->id));
            else $this->deleteFirebaseData($order->id);

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_STATUS_UPDATE_APP: ' . Input::get('status');
            $log->shopper_id = $order->shopper_id;
            $log->order_id = $order->id;
            $log->save();

            if ($order->status == 'Delivered')
            {
                //si es primera compra y fue referido
                $count_orders = Order::where('user_id', '=', $order->user_id)->where('status', '=', 'Delivered')->count();
                //validar que el usuario no tenga pedidos cancelados para no cargar dos veces el credito como referido
                $count_orders_cancelled = Order::where('user_id', '=', $order->user_id)->where('status', '=', 'Cancelled')->count();
                if ($count_orders == 1 && !$count_orders_cancelled)
                {
                    $user = User::find($order->user_id);
                    if ($user->referred_by && $referrer = User::find($user->referred_by))
                    {
                        if ($referrer->total_referrals < Config::get('app.referred.limit'))
                        {
                            //registrar crédito a usuario referido
                            /*$referred_by_amount = Config::get('app.referred.referred_by_amount');
                            $user_credit = new UserCredit;
                            $user_credit->user_id = $referrer->id;
                            $user_credit->referrer_id = $user->id;
                            $user_credit->amount = $referred_by_amount;
                            $user_credit->type = 1;
                            $user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por registro de amigo '.$user->first_name.' '.$user->last_name.' con ID usuario # '.$user->id.' con sistema de referidos en pedido con ID # '.$order->id;
                            $user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
                            $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                            $user_credit->save();*/

                            //referido con domicilio gratis
                            $referred_by_free_delivery_days = Config::get('app.referred.referred_by_free_delivery_days');
                            $user_free_delivery = new UserFreeDelivery;
                            $user_free_delivery->user_id = $referrer->id;
                            $user_free_delivery->referrer_id = $user->id;
                            $user_free_delivery->amount = $referred_by_free_delivery_days;
                            $user_free_delivery->expiration_date = empty($referrer->free_delivery_expiration_date) ? date('Y-m-d', strtotime('+'.$referred_by_free_delivery_days.' day')) : date('Y-m-d', strtotime($referrer->free_delivery_expiration_date.' +'.$referred_by_free_delivery_days.' day'));
                            $description = 'Adición de '.$referred_by_free_delivery_days.' días de domicilio gratis por registro de amigo '.$user->first_name.' '.$user->last_name.' con ID usuario # '.$user->id.'
                            con sistema de referidos en pedido con ID # '.$order->id.'. Nueva fecha de vencimiento: '.format_date('normal', $user_free_delivery->expiration_date);
                            $user_free_delivery->description = $description;
                            $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                            $user_free_delivery->save();

                            //aumentar cantidad de referidos y fecha de expiracion de domicilio gratis a usuario
                            $referrer->free_delivery_expiration_date = $user_free_delivery->expiration_date;
                            $referrer->total_referrals += 1;
                            $referrer->save();

                            $user_discounts = User::getDiscounts($referrer->id);

                            //enviar mail de cargo de credito a usuario referido
                            $mail = array(
                                'template_name' => 'emails.referred',
                                'subject' => 'Tienes '.$referred_by_free_delivery_days.' días de domicilio gratis en cualquier tienda',
                                'to' => array(array('email' => $referrer->email, 'name' => $referrer->first_name.' '.$referrer->last_name)),
                                'vars' => array(
                                    'username' => $referrer->first_name,
                                    'username_referrer' => $user->first_name,
                                    'free_delivery_days' => $user_discounts['free_delivery_days'],
                                    'referred_by_free_delivery_days' => $referred_by_free_delivery_days,
                                    'expiration_date' => format_date('normal', $user_free_delivery->expiration_date),
                                    'referal_code' => $referrer->referral_code
                                )
                            );
                            send_mail($mail);
                        }
                    }
                }
            }

            //enviar mail
            $templates = array(
                'In Progress' => 'emails.order_accepted',
                'Cancelled' => 'emails.order_rejected',
                'Delivered' => 'emails.order_delivered',
            );

            $subjects = array(
                'In Progress' => 'ha sido aceptado',
                'Cancelled' => 'ha sido cancelado',
                'Delivered' => 'ha sido entregado',
            );

            if (isset($templates[$order->status]))
            {
                $store = Store::find($order->store_id);

                $send_mail = true;
                $shopper = $delivery_time = '';
                if ($order->status == 'In Progress' || $order->status == 'Delivered'){
                    if ($shopper = Shopper::getShopper($order->shopper_id)){
                        $minutes = !empty($order->shopper_delivery_time_minutes) ? $order->shopper_delivery_time_minutes : 90;
                        $delivery_time = $minutes.' min.';
                        if ($order->status == 'In Progress' && strlen(trim($shopper->phone)) == 10){
                            $phone_number = trim($user->phone);
                            $msn = 'Merqueo.com: Acabamos de asignar a '.$shopper->first_name.', comprador experto para atender tu pedido en '.$store->name. '. Si tienes dudas puedes llamarlo al: '.$shopper->phone;
                            send_sms($phone_number, $msn);
                        }
                    }
                    else $send_mail = false;
                }

                $products_mail = array();
                $products = OrderProduct::where('order_id', $order->id)->get();
                foreach ($products as $product) {
                     $products_mail[$order->id][] = array(
                         'img' => $product->product_image_url,
                         'name' => $product->product_name,
                         'qty' => $product->quantity,
                         'quantity' => $product->product_quantity,
                         'unit' => $product->product_unit,
                         'price' => $product->price,
                         'total' => $product->quantity * $product->price,
                         'type' => $product->type,
                         'status' => $product->fulfilment_status != 'Not Available' ? 'available.png' : 'no_available.png'
                     );
                }

                $mail = array(
                    'template_name' => $templates[$order->status],
                    'subject' => 'Tu pedido en '.$store->name.' '.$subjects[$order->status],
                    'to' => array(array('email' => $order_group->user_email, 'name' => $order_group->user_firstname.' '.$order_group->user_lastname)),
                    'vars' => array(
                        'order' => $order,
                        'order_group' => $order_group,
                        'store' => $store,
                        'shopper' => $shopper,
                        'delivery_time' => $delivery_time,
                        'reject_reason' => $order->reject_reason,
                        'products' => $products_mail[$order->id]
                    )
                );

                if ($send_mail && $order_group->source != 'Web Service')
                    send_mail($mail);

                //enviar notificacion
                if ($order_group->source == 'Device'){
                    $notifications = array(
                        'In Progress' => 'Tu pedido en '.$store->name.' esta siendo preparado y será entregado en la dirección que indicaste.',
                        'Delivered' => '¡Cuéntanos cómo te fue con el pedido de '.$store->name.' que acabas de recibir!',
                        'Cancelled' => 'Tu pedido en '.$store->name.' fue cancelado.'
                    );
                    $data =  array(
                        'type' => $order->status,
                        'message' => $notifications[$order->status],
                        'order_id' => $order->id
                    );

                    //send_push_notification(array($order_group->user_device_id), $data);
                }
            }

            //actualizar datos en firebase
            if ($order->status != 'Delivered')
                $this->setFirebaseData(array($order->id));
            else $this->deleteFirebaseData($order->id);

            $this->response = array(
                'status' => true,
                'message' => 'Estado de pedido actualizado.',
                'result' => array(
                    'order' => $this->get_order_format($order->id),
                    'total_amount' => $order->total_amount,
                    'discount_amount' => $order->discount_amount,
                    'delivery_amount' => $order->delivery_amount,
                    'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount,
                    'balance' => Shopper::getBalance($this->shopper_id)
                )
            );

            return $this->jsonResponse();
        }
    }

    /**
     * Cambiar metodo de pago del pedido
     */
    public function update_order_payment_method()
    {
        if (!$order_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID pedido es requerido.', 400);

        if (!Input::has('shopper_id') || !Input::has('payment_method'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if (!$order = Order::where('id', $order_id)->where('shopper_id', Input::get('shopper_id'))->first())
            return $this->jsonResponse('ID pedido no asignado a shopper o no existe.', 400);

        $order->payment_method = Input::get('payment_method');
        $order->save();

        //log de pedido
        $log = new OrderLog();
        $log->type = 'ORDER_PAYMENT_METHOD_UPDATE_APP';
        $log->shopper_id = $order->shopper_id;
        $log->order_id = $order->id;
        $log->save();

        //actualizar datos en firebasse
        $this->setFirebaseData(array($order->id));

        $this->response = array(
            'status' => true,
            'message' => 'Método de pago actualizado.',
            'result' => array(
                'payment_method' => Input::get('payment_method'),
                'payment_method_text' => Input::get('payment_method') == 'Efectivo' ? 'con Datáfono' : 'en Efectivo'
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Subir imagen de factura y voucher
     */
     private function upload_order_image($image, $type)
     {
        $file_name = $type.'-'.rand().'.jpg';

        if ($type == 'invoice')
            $dir = 'orders/invoices/'.date('Y-m-d');
        if ($type == 'voucher')
            $dir = 'orders/vouchers/'.date('Y-m-d');

        $path = public_path()."/uploads/app/$file_name";
        $generate_image = $this->base64_to_jpeg($image, $path);

        if (!$generate_image)
           return false;

        $extension = pathinfo($file_name);
        $file_data['real_path'] = realpath($path);
        $file_data['client_original_name'] = basename($file_name);
        $file_data['client_original_extension'] = $extension['extension'];
        $url = upload_image($file_data, false, $dir);

        if ($url)
        {
            unlink($path);
            $order = Order::where('id', Input::get('order_id'))->where('shopper_id', $this->shopper_id)->where('status', 'Dispatched')->first();
            if ($order)
            {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                if ($type == 'invoice')
                {
                    if (!empty($order->invoice_image_url))
                    {
                        $path = str_replace($cloudfront_url, uploads_path(), $order->invoice_image_url);
                        remove_file($path);
                        remove_file_s3($order->invoice_image_url);
                    }
                    $order->invoice_image_url = $url;
                    $log_type = 'ORDER_INVOICE_UPLOAD_APP';
                }
                if ($type == 'voucher')
                {
                    if (!empty($order->voucher_image_url))
                    {
                        $path = str_replace($cloudfront_url, uploads_path(), $order->voucher_image_url);
                        remove_file($path);
                        remove_file_s3($order->voucher_image_url);
                    }
                    $order->voucher_image_url = $url;
                    $log_type = 'ORDER_VOUCHER_UPLOAD_APP';
                }
                $order->save();

                //log de pedido
                $log = new OrderLog();
                $log->type = $log_type;
                $log->shopper_id = $order->shopper_id;
                $log->order_id = $order->id;
                $log->save();

                //actualizar datos en firebasse
                $this->setFirebaseData(array($order->id));

                return true;
            }
        }else
            return false;
    }

    /*
     * Cobrar total de pedido a tarjeta de cliente
     */
    public function charge_order()
    {
        if (!$order_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID pedido es requerido.', 400);

        $order = Order::find($order_id);

        if (!Input::has('shopper_id') || !Input::has('status'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if (!Input::has('products'))
            return $this->jsonResponse('El estado de los productos es requerido.');

        if ($order && $order->status == 'In Progress')
        {
            $charge_failed = false;

            try {
                DB::beginTransaction();

                //actualizar estado de productos
                OrderProduct::where('order_id', $order->id)->where('type', 'Replacement')->delete();
                $products = json_decode(Input::get('products'));
                foreach($products as $product){
                    if ($product->type == 'Product' || $product->type == 'Custom'){
                        $order_product = OrderProduct::find($product->id);
                        if ($product->type == 'Custom'){
                            $order_product->price = $product->price;
                            $order_product->original_price = $product->price;
                        }
                        $order_product->fulfilment_status = $product->status;
                        $order_product->save();
                    }else{
                        $order_product = new OrderProduct;
                        $order_product->price = $product->price;
                        $order_product->original_price = $product->price;
                        $order_product->product_name = $product->name;
                        $order_product->fulfilment_status = $product->status;
                        $order_product->type = $product->type;
                        $order_product->store_id = $order->store_id;
                        $order_product->order_id = $order->id;
                        $order_product->product_image_url = image_url().'no_disponible.jpg';
                        $order_product->quantity = $product->quantity;
                        $order_product->save();
                    }
                }
                $total_amount = 0;
                $order_products = OrderProduct::where('order_id', $order->id)->get();
                foreach ($order_products as $order_product) {
                    if ($order_product->fulfilment_status == 'Fullfilled')
                        $total_amount += $order_product->price * $order_product->quantity;
                }
                $order->dispatched_date = date('Y-m-d H:i:s');
                $order->total_amount = $total_amount;
                $order->save();

                $this->setFirebaseData(array($order->id));

                //intenta pagar antes de actualizar el estado del pedido
                $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && $order->payment_date == '')
                {
                    //REALIZAR COBRO DE TARJETA
                    if (empty($order->cc_charge_id))
                    {
                        $log = new OrderLog();
                        $result = Order::chargeCreditCard($order->id, $order->user_id);

                        if ($result['status'] === false){
                            $charge_failed = true;
                            throw new Exception($result['message'], 1);
                        }else{
                            $log->type = 'ORDER_PAYMENT_SUCCESS_APP';
                            $log->shopper_id = $order->shopper_id;
                            $log->order_id = $order->id;
                            $log->save();

                            $this->setFirebaseData(array($order->id));

                            $this->response = array(
                                'status' => true,
                                'message' => $result['message'],
                            );

                            DB::commit();

                            return $this->jsonResponse();
                        }
                    }else return $this->jsonResponse('El cobro a la tarjeta ya fue realizado con anterioridad.');
                }else return $this->jsonResponse('No es necesario realizar cobro a la tarjeta.');

            }catch(Exception $e){

                DB::rollback();

                if ($charge_failed){
                    $log->type = 'ORDER_PAYMENT_FAILED_APP';
                    $log->shopper_id = $order->shopper_id;
                    $log->order_id = $order->id;
                    $log->save();
                }

                return $this->jsonResponse($e->getMessage());
            }
        }else return $this->jsonResponse('No se pudo realizar el cobro por que el estado del pedido debe estar En proceso.');
    }

    /**
     * Convierte un string base64 en archivo de imagen.
     */
    function base64_to_jpeg($base64_string, $output_file)
    {
        list($type, $base64_string) = explode(';', $base64_string);
        list(, $base64_string)      = explode(',', $base64_string);
        $base64_string = str_replace(' ', '+', $base64_string);
        $base64_string = base64_decode($base64_string);
        file_put_contents($output_file, $base64_string);

        if (file_exists($output_file))
            return true;
        else
            return false;
    }

    /**
     * Obtiene pedido con formato de Firebase
     */
    public function get_order_format($order_id)
    {
        $order = Order::select('orders.id','orders.status', 'orders.total_products', 'orders.delivery_amount', 'orders.discount_amount', 'orders.total_amount',
                    'orders.reject_reason', 'orders.shopper_id', 'orders.reference', 'orders.invoice_image_url', 'orders.voucher_image_url',
                    DB::raw("IF(orders.cc_charge_id IS NOT NULL, cc_charge_id, 'null') AS cc_charge_id"),
                    DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'), 'stores.name AS store_name', 'orders.management_date', 'orders.store_id', 'orders.shopper_delivery_time_minutes',
                    'order_groups.source', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_phone', 'order_groups.user_address',
                    'order_groups.user_address_latitude', 'order_groups.user_address_longitude', 'orders.delivery_time',
                    DB::raw("IF(orders.payment_method = 'Tarjeta de crédito','Online',orders.payment_method) AS payment_method"),
                    'order_groups.user_comments AS user_comments', 'stores.city_id','order_groups.zone_id', 'orders.delivery_date', 'order_groups.user_comments',
                    DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.user_id <> 5169 AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
                   ->join('stores','orders.store_id','=','stores.id')
                   ->join('users','orders.user_id','=','users.id')
                   ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                   ->where('orders.id', $order_id)
                   ->where('orders.status', '<>', 'Delivered')
                   ->where('orders.status', '<>', 'Cancelled')
                   ->where('orders.status', '<>', 'Validation')
                   ->first();
        if (!$order /*|| ($order && empty($order->zone_id))*/)
            return false;

        $order->products = OrderProduct::select('order_products.*',
                         DB::raw("IF(order_products.promo_type IS NOT NULL, order_products.promo_type, 'null') AS promo_type"),
                         DB::raw("IF(products.reference IS NOT NULL, products.reference, 'null') AS reference"),
                         DB::raw("IF(products.description IS NOT NULL, products.description, 'null') AS description"),
                         DB::raw("IF(products.image_large_url IS NOT NULL, products.image_large_url, 'null') AS image_large_url"))
                        ->where('order_id', '=', $order_id)
                        ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                        ->orderBy('department_id', 'desc')
                        ->orderBy('product_name', 'asc')
                        ->get()->toArray();

        $users_array = Admin::select('id', 'fullname AS name')->where('role_id', 3)->where('status', 1)->orderBy('fullname')->get()->toArray();
        $users = array();
        $user_select = array('id'=>0,'name'=>'Selecciona');
        if(!in_array($user_select,$users))
            $users[] = $user_select;

        foreach ($users_array as $key => $value){
            if(!in_array($value,$users))
               $users[] = $value;
        }
        $order->users = $users;

        if (empty($order->qty_orders))
           $order->qty_orders = 0;

        $store_branches = StoreBranch::where('store_id', $order->store_id)
                        ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                        ->where('store_branches.status', 1)
                        ->select('supermarket_id')
                        ->get()->toArray();
        $supermarket_ids = array_fetch($store_branches, 'supermarket_id');
        $supermarket_ids = array_unique($supermarket_ids);
        $supermarkets = Supermarket::select('name')->whereIn('id', $supermarket_ids)->where('status', 1)->get()->toArray();

        $supermarkets_array = array();
        $supermarket_select = array('name'=>'Selecciona');
        if(!in_array($supermarket_select,$supermarkets_array))
            $supermarkets_array[] = $supermarket_select;

        foreach ($supermarkets as $key => $value){
            if(!in_array($value,$supermarkets_array))
            $supermarkets_array[] = $value;
        }
        $supermarket_otro = array('name'=>'Otro');
        if(!in_array($supermarket_otro,$supermarkets_array))
            $supermarkets_array[] = $supermarket_otro;

        $order->supermarkets = $supermarkets_array;

        $supermarkets = Supermarket::select('supermarkets.name')
                                    ->join('store_branches', 'store_branches.supermarket_id', '=', 'supermarkets.id')
                                    ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                                    ->where('stores.city_id', $order->city_id)
                                    ->whereNotIn('supermarkets.id', $supermarket_ids)->where('supermarkets.status', 1)
                                    ->groupBy('supermarkets.id')->orderBy('supermarkets.name')->get()->toArray();

        $supermarkets_others = array();
        $supermarkets_others_select = array('name'=>'Selecciona');
        if(!in_array($supermarkets_others_select, $supermarkets_others))
            $supermarkets_others[] = $supermarkets_others_select;

        foreach ($supermarkets as $key => $value){
           if(!in_array($value,$supermarkets_others))
              $supermarkets_others[] = $value;
        }
        $order->supermarkets_others = $supermarkets_others;

        $order->source = $this->config['order_sources'][$order->source];

        return $order;

    }

}
