<?php

namespace api\shoppers\v1;

use Input, ShopperDevice, Config, ShopperDeviceToken;

class ApiIndexController extends \ApiController {

	/**
	 * Obtiene version del API para obligar actualizacion
	 *
	 * @return array $response Respuesta
	 */
	public function version()
	{
		$this->response = array(
			'status' => true,
			'message' => 'Datos obtenidos',
			'result' => array(
				'update' => 'no_required',
				'os' => 'all',
				'api' => '1.0',
				'url_android' => Config::get('app.android_url_shoppers'),
				'datetime' => date('Y-m-d H:i:s')
			)
		);

		return $this->jsonResponse();
	}

    /**
     * Metodo de inicializacion
     *
     * @return array $response Respuesta
     */
    public function init()
    {
         if (Input::has('os') && Input::has('regid') && Input::has('app_version')){
            extract(Input::all());
            if ($regid != 'null' && $regid)
            {
                //SERVICIO INIT
                //registrar id del dispositivo
                if (!$shopper_device = ShopperDevice::where('regid', $regid)->first()){
                    $shopper_device = new ShopperDevice;
                    $shopper_device->regid = $regid;
                    $shopper_device->os = $os;
                    if (isset($shopper_id) && !empty($shopper_id))
                        $shopper_device->shopper_id = $shopper_id;
                    $shopper_device->save();
                }else{
                    if (isset($shopper_id) && !empty($shopper_id)){
                        $shopper_device->shopper_id = $shopper_id;
                        $shopper_device->save();
                    }
                }

                //SERVICIO VERSION
                $update = 'not_required'; //not_required, required, suggested
                $update_os = 'android'; //ios, android, all, none
                $update_message = 'No se requiere actualización.';

                $app_version = intval(str_replace('.', '', $app_version));
                if ($os == 'android'){
                    //$app_current_version = intval(str_replace('.', '', Config::get('app.android_current_app_version')));
                    /*if ($app_version < 114){
                        $update_os = 'android';
                        $update = 'required';
                    }*/
                }
                if ($os == 'ios'){
                    //$app_current_version = intval(str_replace('.', '', Config::get('app.ios_current_app_version')));
                }

                if ($update == 'required' && ($os == $update_os || $update_os == 'all'))
                    $update_message = 'Hemos actualizado Merqueo Shopper para mejorar tu experiencia con los pedidos. Por favor actualiza a la última versión para continuar. Muchas gracias!';
                if ($update == 'suggested' && ($os == $update_os || $update_os == 'all'))
                    $update_message = 'Hemos actualizado Merqueo Shopper para mejorar tu experiencia con los pedidos. Te sugerimos actualizar a la última versión. Muchas gracias!';

                //SERVICIO TOKEN
                if (!$shopper_device = ShopperDevice::where('regid', $regid)->first())
                    return $this->jsonResponse('Acceso denegado ID de dispositivo no valido', 400);
                else{
                    $token = generate_token();
                    $shopper_device_token = new ShopperDeviceToken;
                    $shopper_device_token->token = $token;
                    $shopper_device_token->shopper_device_id = $shopper_device->id;
                    $shopper_device_token->expiry = date('Y-m-d H:i:s', strtotime('+4 hour'));
                    $shopper_device_token->save();
                }

                $this->response = array(
                    'status' => true,
                    'message' => 'Datos guardados y obtenidos.',
                    'result' => array(
                        'version' => array(
                            'update' => $update,
                            'message' => $update_message,
                            'os' => $update_os,
                            'api' => '1.0',
                            'url_android' => Config::get('app.android_url'),
                            'url_ios' => Config::get('app.ios_url'),
                            'datetime' => date('Y-m-d H:i:s')
                        ),
                        'config' => array(
                            'admin_email' => Config::get('app.admin_email'),
                            'admin_phone' => Config::get('app.admin_phone'),
                        ),
                        'token' => $token
                   )
                );
            }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        return $this->jsonResponse();
    }

	/**
	 * Devuelve token de acceso
	 *
	 * @return array $response Respuesta
	 */
	public function request_token()
	{
		if (Input::has('regid'))
		{
			$regid = Input::get('regid');
			if (!$shopper_device = ShopperDevice::where('regid', $regid)->first())
				return $this->jsonResponse('Acceso denegado ID de dispositivo no valido', 400);
			else{
				$token = generate_token();
				$shopper_device_token = new ShopperDeviceToken;
				$shopper_device_token->token = $token;
				$shopper_device_token->shopper_device_id = $shopper_device->id;
				$shopper_device_token->expiry = date('Y-m-d H:i:s', strtotime('+4 hour'));
				$shopper_device_token->save();

				$this->response = array(
					'status' => true,
					'message' => 'Datos guardados',
					'result' => array('token' => $token)
				);
			}
		}else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		return $this->jsonResponse();
	}

}
