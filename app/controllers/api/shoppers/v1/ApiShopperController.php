<?php

namespace api\shoppers\v1;

use Input, Hash, Config, DB, Route, Shopper, ShopperZone, ShopperDeviceToken, ShopperDevice, Order, OrderProduct, Zone, Admin, Crypt_RSA,
StoreBranch, Supermarket, ArrayObject, ShopperMovement, ShopperSchedule, Carbon\Carbon;

class ApiShopperController extends \ApiController {

    /**
     * Realiza login de shopper
     *
     * @return array $response Respuesta
     */
    public function login()
    {
        $regid =  Input::get('regid');
        $email =  Input::get('email');
        $password =  Input::get('password');

        if (empty($email) || empty($password) || empty($regid))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $shopper = Shopper::where('email', $email)->where('status', 1)->first();
        if ($shopper)
        {
            $password = base64_decode($password);
            $rsa = new Crypt_RSA();
            $rsa->loadKey(Config::get('app.private_key'));
            $password = $rsa->decrypt($password);
            $password = base64_decode($password);
            if (!$password){
                $this->response['message'] = 'Formato de password no valido.';
                return $this->jsonResponse();
            }
            if (Hash::check($password, $shopper->password)){
                if ($shopper->is_active){
                    //registrar id del dispositivo
                    if (!$shopper_device = ShopperDevice::where('regid', $regid)->first()){
                        $shopper_device = new ShopperDevice;
                        $shopper_device->regid = $regid;
                        $shopper_device->os = 'Android';
                    }
                    $shopper_device->shopper_id = $shopper->id;
                    $shopper_device->save();

                    $shopper->oauth_verifier = generate_token();
                    $shopper->save();

                    $this->setFirebaseShopperData($shopper->id);

                    $ids = array();
                    //pedidos pendientes
                    $orders = Order::select('orders.id')
                                   ->where('orders.shopper_id', $shopper->id)
                                   ->where('orders.status', '<>', 'Delivered')
                                   ->where('orders.status', '<>', 'Cancelled')
                                   ->where('orders.status', '<>', 'Validation')
                                   ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime('-5 days')))
                                   ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime('+2 hours')))
                                   ->orderBy('scheduled_delivery', 'desc')->orderBy('orders.delivery_date')
                                   ->get();

                    foreach($orders as $order)
                        $ids[] = $order->id;

                    //pedidos sin asignar
                    $minutes_rank = Config::get('app.shoppers.minutes_rank_before_delivery_date');
                    $minutes_immediately = Config::get('app.shoppers.minutes_immediately_before_delivery_date');
                    $orders_no_allocated = Order::select('orders.id')
                                                ->where('orders.status', '=' ,'Initiated')
                                                ->where('orders.shopper_id', 0)
                                                ->where(DB::raw("DATE_SUB(orders.delivery_date, INTERVAL IF(orders.delivery_time = 'Immediately', ".$minutes_immediately.", ".$minutes_rank.") MINUTE)"), '<=', date('Y-m-d H:i:s'))
                                                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
                                                ->orderBy('scheduled_delivery', 'desc')->orderBy('orders.delivery_date')
                                                ->get();

                    foreach($orders_no_allocated as $order)
                        $ids[] = $order->id;

                    //actualizar datos en firebase
                    if (count($ids))
                        $this->setFirebaseData($ids, $shopper->id);

                    $zones = Zone::select('zones.id')
                             ->join('shopper_zones','shopper_zones.zone_id', '=', 'zones.id')
                             ->join('shoppers', 'shoppers.id', '=', 'shopper_zones.shopper_id')
                             ->where('shoppers.id', '=', $shopper->id)
                             ->where('zones.status', 1)->get();

                    $count = count($zones);
                    $i = 1;
                    $zones_array = '';
                    foreach ($zones as $key => $value) {
                        if($i < $count) $zones_array .= $value->id.',';
                        else $zones_array .= $value->id;
                        $i++;
                    }

                    $shopper = array(
                        'id' => $shopper->id,
                        'first_name' => $shopper->first_name,
                        'last_name' => $shopper->last_name,
                        'email' => $shopper->email,
                        'phone' => $shopper->phone,
                        'photo_url' => $shopper->photo_url,
                        'zones'=> $zones_array,
                        'oauth_verifier' => $shopper->oauth_verifier,
                        'balance' => Shopper::getBalance($shopper->id)
                    );

                    $this->response = array(
                        'status' => true,
                        'message' => 'Sesión iniciada',
                        'result' => array('shopper' => $shopper)
                    );
                }else $this->response['message'] = 'Tu cuenta no esta activada. Por favor comunicate con la oficina para su activación.';
            }else $this->response['message'] = 'Contraseña incorrecta.';
        } else $this->response['message'] = 'Email incorrecto.';

        return $this->jsonResponse();
    }

    /**
     * Cierra sesion
     */
    public function logout()
    {
        if (!Input::has('shopper_id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        $shopper = Shopper::find(Input::get('shopper_id'));
        $shopper->is_available = 0;
        $shopper->save();

        $this->deleteFirebaseShopperData($shopper->id);

        $this->response = array(
            'status' => true,
            'message' => 'Sesión cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Actualiza estado de shopper en App
     */
    public function update_status()
    {
        if (!$shopper_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        if (!$shopper = Shopper::find($shopper_id))
            return $this->jsonResponse('ID shopper no existe.', 400);

        if (!Input::has('status'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $shopper->is_available = Input::get('status');
        $shopper->save();

        $this->response = array(
            'status' => true,
            'message' => $shopper->is_available ? 'Disponible' : 'No disponible'
        );

        return $this->jsonResponse();
    }

    /**
     * Actualiza posicion de shopper en App
     */
    public function location()
    {
        if (!$shopper_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        if (!$shopper = Shopper::find($shopper_id))
            return $this->jsonResponse('ID shopper no existe.', 400);

        if (!Input::has('location'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $shopper->location = Input::get('location');
        $shopper->last_date_location = date('Y-m-d H:i:s');
        $shopper->save();

        $this->response = array(
            'status' => true,
            'message' => 'Posicion actualizada'
        );

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos sin asignar y pendientes
     *
     * @return array $response Respuesta
     */
    public function pending_orders()
    {
        if (!$shopper_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        if (!$shopper = Shopper::find($shopper_id))
            return $this->jsonResponse('ID shopper no existe.', 400);

        $unallocated_orders = $pending_orders = array();

        //pedidos pendientes
        $orders = Order::select('orders.id', 'orders.status', 'orders.total_products', 'orders.delivery_amount', 'orders.discount_amount', 'orders.total_amount', 'orders.reject_reason',
                        DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'), 'stores.name AS store_name','orders.management_date', 'orders.store_id', 'orders.shopper_delivery_time_minutes',
                        'order_groups.source', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_phone', DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
                        'order_groups.user_address_latitude', 'order_groups.user_address_longitude', 'orders.delivery_time',
                        DB::raw("IF(orders.payment_method = 'Tarjeta de crédito','Online',orders.payment_method) AS payment_method"),
                        'orders.cc_charge_id','order_groups.user_comments AS user_comments', 'order_groups.zone_id', 'orders.delivery_date',
                        DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.user_id <> 5169 AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
                       ->join('stores','orders.store_id','=','stores.id')
                       ->join('users','orders.user_id','=','users.id')
                       ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                       ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                       ->where('orders.shopper_id', $shopper->id)
                       ->where('order_groups.user_city_id', $shopper->city_id)
                       ->where('orders.status', '<>', 'Delivered')
                       ->where('orders.status', '<>', 'Cancelled')
                       ->where('orders.status', '<>', 'Validation')
                       ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime('-5 days')))
                       ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime('+2 hours')));

        if ($shopper && !empty($shopper->stores)) {
            $shopper_stores = explode(',', $shopper->stores);
            $orders->whereIn('orders.store_id', $shopper_stores);
        }
        if (Input::get('order_id'))
            $orders->where('orders.id', Input::get('order_id'));
        if (Input::get('ids'))
            $orders->whereNotIn('orders.id', Input::get('ids'));

        $orders = $orders->orderBy('scheduled_delivery', 'desc')->orderBy('orders.delivery_date')->get();

        $ids = array();
        foreach($orders as $order){
            if ($order->status != 'Cancelled' && $order->status != 'Delivered')
                $ids[] = $order->id;
        }

        //obtener productos de los pedidos
        $products = OrderProduct::select('order_products.*','products.reference', 'products.description','products.image_large_url')
                            ->whereIn('order_id', $ids)
                            ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                            ->orderBy('department_id', 'desc')
                            ->orderBy('product_name', 'asc')
                            ->get();

        $pending_orders = $orders->toArray();
        $products = $products->toArray();

        //mezclar arrays
        foreach($pending_orders as $i => $order)
        {
            if ($order['management_date'] && date('Y-m-d 00:00:00') > $order['management_date']){
                unset($pending_orders[$i]);
                continue;
            }

            if (array_key_exists($pending_orders[$i]['source'], $this->config['order_sources']))
                $pending_orders[$i]['source'] = $this->config['order_sources'][$pending_orders[$i]['source']];
            if ($pending_orders[$i]['payment_method'] == 'Tarjeta de crédito')
                $pending_orders[$i]['payment_method'] = 'Online';
            if (!empty($order['campaign_order_id']))
               $pending_orders[$i]['user_comments'] .= ' ENTREGAR '.strtoupper($order['campaign_name']).'.';

            $order_products = array();
            foreach($products as $product) {
                if ($product['order_id'] == $order['id'])
                    $order_products[] = $product;
            }

            if ($order['status'] == 'In Progress')
                $pending_orders[$i]['shopper_delivery_time'] = $order['shopper_delivery_time_minutes'];

            if ($order['status'] == 'Cancelled')
                $pending_orders[$i]['reject_reason'] = $order['reject_reason'];

            if (empty($order['cc_charge_id']))
                $pending_orders[$i]['cc_charge_id'] = '';

            $pending_orders[$i]['products'] = $order_products;
            $store_branches = StoreBranch::where('store_id', $pending_orders[$i]['store_id'])
                            ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                            ->where('store_branches.status', 1)
                            ->select('supermarket_id')
                            ->get()->toArray();
            $supermarket_ids = array_fetch($store_branches, 'supermarket_id');
            $supermarket_ids = array_unique($supermarket_ids);
            $supermarkets = Supermarket::select('name')->whereIn('id', $supermarket_ids)->where('status', 1)->get()->toArray();

            $pending_orders[$i]['supermarkets'][] = array('name'=>'Selecciona');
            foreach ($supermarkets as $key => $value)
                $pending_orders[$i]['supermarkets'][] = $value;

            $pending_orders[$i]['supermarkets'][] = array('name'=>'Otro');
            $supermarkets = Supermarket::select('supermarkets.name')
                                        ->join('store_branches', 'store_branches.supermarket_id', '=', 'supermarkets.id')
                                        ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                                        ->where('stores.city_id', $shopper->city_id)
                                        ->whereNotIn('supermarkets.id', $supermarket_ids)->where('supermarkets.status', 1)
                                        ->groupBy('supermarkets.id')->orderBy('supermarkets.name')->get()->toArray();

            $pending_orders[$i]['supermarkets_others'][] = array('name'=>'Selecciona');
            foreach ($supermarkets as $key => $value)
                $pending_orders[$i]['supermarkets_others'][] = $value;

            $pending_orders[$i]['users'][] = array('id'=> 0 ,'name' => 'Selecciona');

            if (empty($pending_orders[$i]['qty_orders']))
                $pending_orders[$i]['qty_orders'] = 0;

            if (array_key_exists($pending_orders[$i]['source'], $this->config['order_sources']))
                $pending_orders[$i]['source'] = $this->config['order_sources'][$order['source']];

            //if (array_key_exists($pending_orders[$i]['status'], $this->config['order_statuses']))
                //$pending_orders[$i]['status'] = $this->config['order_statuses'][$order['status']];

        }

        //pedidos sin asignar
        $minutes_rank = Config::get('app.shoppers.minutes_rank_before_delivery_date');
        $minutes_immediately = Config::get('app.shoppers.minutes_immediately_before_delivery_date');
        $orders_no_allocated = Order::select('orders.id', 'orders.status', 'orders.total_products', 'orders.delivery_amount', 'orders.discount_amount', 'orders.total_amount', 'orders.reject_reason',
                                    DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'), 'stores.name AS store_name', 'orders.cc_charge_id',
                                    'order_groups.source', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_phone', DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
                                    'order_groups.user_address_latitude', 'order_groups.user_address_longitude', 'orders.delivery_time',
                                    DB::raw("IF(orders.payment_method = 'Tarjeta de crédito','Online',orders.payment_method) AS payment_method"),
                                    'order_groups.user_comments AS user_comments', 'order_groups.zone_id', 'orders.delivery_date',
                                    DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.user_id <> 5169 AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
                                    ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                                    ->join('users','orders.user_id','=','users.id')
                                    ->join('stores','orders.store_id','=','stores.id')
                                    ->where('orders.status', '=' ,'Initiated')
                                    ->where('order_groups.user_city_id', $shopper->city_id)
                                    ->where('orders.shopper_id', 0)
                                    ->where(DB::raw("DATE_SUB(orders.delivery_date, INTERVAL IF(orders.delivery_time = 'Immediately', ".$minutes_immediately.", ".$minutes_rank.") MINUTE)"), '<=', date('Y-m-d H:i:s'))
                                    ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
                                    ->orderBy('scheduled_delivery', 'desc')->orderBy('orders.delivery_date')
                                    ->get();

        $shopper_zones = ShopperZone::select('zone_id')->join('zones', 'zones.id', '=', 'zone_id')->where('shopper_id', $shopper->id)->where('is_active', 1)->get();
        $zones = Zone::select('delivery_zone')->where('status', 1)->get();

        foreach ($orders_no_allocated as $order)
        {
            if (array_key_exists($order->source, $this->config['order_sources']))
                $order->source = $this->config['order_sources'][$order->source];
            if ($order->payment_method == 'Tarjeta de crédito')
                $order->payment_method = 'Online';

            if (empty($order->qty_orders))
               $order->qty_orders = 0;

            if (empty($order->cc_charge_id))
                $order->cc_charge_id = '';

            if ($order->status != 'Cancelled' && $order->status != 'Delivered')
                $ids[] = $order->id;

            //if (array_key_exists($order->status, $this->config['order_statuses']))
                //$order->status = $this->config['order_statuses'][$order->status];

            $store_branches = StoreBranch::where('store_id', $order->store_id)
                            ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                            ->where('store_branches.status', 1)
                            ->select('supermarket_id')
                            ->get()->toArray();
            $supermarket_ids = array_fetch($store_branches, 'supermarket_id');
            $supermarket_ids = array_unique($supermarket_ids);
            $supermarkets = Supermarket::select('name')->whereIn('id', $supermarket_ids)->where('status', 1)->get();
            $order->supermarkets = $supermarkets;
            $supermarkets = Supermarket::select('supermarkets.name')
                                        ->join('store_branches', 'store_branches.supermarket_id', '=', 'supermarkets.id')
                                        ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                                        ->where('stores.city_id', $shopper->city_id)
                                        ->whereNotIn('supermarkets.id', $supermarket_ids)->where('supermarkets.status', 1)
                                        ->groupBy('supermarkets.id')->orderBy('supermarkets.name')->get();
            $order->supermarkets_others = $supermarkets;

            //obtener productos del pedido
            $products = OrderProduct::select('order_products.*','products.reference', 'products.description','products.image_large_url')
                        ->where('order_id', $order->id)
                        ->join('stores', 'order_products.store_id', '=', 'stores.id')
                        ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                        ->orderBy('department_id', 'DESC')
                        ->orderBy('product_name', 'ASC')
                        ->get();
            $order->products = $products;

            if (empty($order->zone_id)){
                $unallocated_orders[] = $order->toArray();
            }else{
                //validar si shopper cubre la zona del pedido
                if ($shopper_zones){
                    foreach ($shopper_zones as $zone){
                        if ($order->zone_id == $zone->zone_id){
                            $unallocated_orders[] = $order->toArray();
                            break;
                        }
                    }
                }
            }
        }

        //actualizar datos en firebasse
        $this->setFirebaseData($ids);

        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'orders' => array(
                    'unallocated' => $unallocated_orders,
                    'pending' => $pending_orders
                )
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos entregados
     *
     * @return array $response Respuesta
     */
    public function delivered_orders()
    {
        if (!$shopper_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        if (!$shopper = Shopper::find($shopper_id))
            return $this->jsonResponse('ID shopper no existe.', 400);

        $delivered_orders = array();

        //pedidos entregados
        $orders = Order::select('orders.id','orders.status', 'orders.total_products', 'orders.delivery_amount', 'orders.discount_amount', 'orders.total_amount', 'orders.reject_reason',
                        DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'), 'stores.name AS store_name',
                        'order_groups.source', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_phone', DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
                        'orders.delivery_time', 'orders.payment_method',
                        DB::raw("CONCAT('Si alguno de los productos no esta disponible', ' ', order_groups.user_comments) AS user_comments"),
                        DB::raw("DATE_FORMAT(orders.delivery_date, '%d/%m/%Y %l:%i %p') AS delivery_date"))
                       ->join('stores','orders.store_id','=','stores.id')
                       ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                       ->where('orders.shopper_id', $shopper->id)
                       ->where('order_groups.user_city_id', $shopper->city_id)
                       ->where('orders.status', 'Delivered')
                       ->orderBy('management_date', 'desc');

        $rows = 10;
        if (Input::has('p'))
            $page = intval(Input::get('p'));
        if (!isset($page)) {
            $begin = 0;
            $page = 1;
        }else $begin = ($page - 1) * $rows;

        $orders = $orders->limit($rows)->offset($begin)->get();

        $ids = array();
        foreach($orders as $order)
            $ids[] = $order->id;

        //obtener productos de los pedidos
        $products = OrderProduct::select('products.reference', 'order_products.product_name', 'order_id', 'order_products.price', 'order_products.quantity', 'order_products.fulfilment_status', 'products.image_app_url', 'products.image_large_url')
                            ->whereIn('order_id', $ids)
                            ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                            ->orderBy('department_id', 'desc')
                            ->orderBy('product_name', 'asc')
                            ->get();

        $delivered_orders = $orders->toArray();
        $products = $products->toArray();

        //mezclar arrays
        foreach($delivered_orders as $i => $order)
        {
            if (array_key_exists($delivered_orders[$i]['source'], $this->config['order_sources']))
                $delivered_orders[$i]['source'] = $this->config['order_sources'][$orders[$i]['source']];
            if ($delivered_orders[$i]['payment_method'] == 'Tarjeta de crédito')
                $delivered_orders[$i]['payment_method'] = 'Online';

            $order_products = array();
            foreach($products as $product) {
               if ($product['order_id'] == $order['id'])
                   $order_products[] = $product;
            }

            if ($order['status'] == 'In Progress')
                $delivered_orders[$i]['delivery_time'] = $order['shopper_delivery_time_minutes'];

            $delivered_orders[$i]['products'] = $order_products;
        }

        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'orders' => $delivered_orders
            )
        );

        return $this->jsonResponse();
    }

    /**
      * Obtener movimientos de shopper
      *
      * @return array $response Respuesta
      */
     public function get_movements()
     {
        if (!$shopper_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        if (!$shopper = Shopper::find($shopper_id))
            return $this->jsonResponse('ID shopper no existe.', 400);

         $movements_shoppers = array();
         $balance = Shopper::getBalance($shopper->id);
         $movements = ShopperMovement::select('shopper_movements.*', DB::raw("IF(admin_id IS NULL, CONCAT(first_name, ' ', last_name), fullname) AS user"),
                                    DB::raw("DATE_FORMAT(shopper_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"), 'orders.payment_method AS user_payment_method',
                                    'total_real_amount', DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"))
                                    ->leftJoin('admin', 'admin_id', '=', 'admin.id')
                                    ->leftJoin('orders', 'order_id', '=', 'orders.id')
                                    ->join('shoppers', 'shopper_movements.shopper_id', '=', 'shoppers.id')
                                    ->where('shopper_movements.shopper_id', $shopper->id)
                                    ->where('shopper_movements.status', 1)
                                    ->where('shopper_movements.created_at', '>', date('Y-m-d 00:00:00', strtotime('-2 days')))
                                    ->where('shopper_movements.created_at', '<', date('Y-m-d 23:59:59', strtotime('+3 hours')))
                                    ->orderBy('shopper_movements.created_at', 'desc');

        $rows = 10;
        if (Input::has('p'))
            $page = intval(Input::get('p'));
        if (!isset($page)) {
            $begin = 0;
            $page = 1;
        }else $begin = ($page - 1) * $rows;

        $movements_shoppers = array();
        $movements = $movements->orderBy('updated_at','desc')->limit($rows)->offset($begin)->get();

        if (count($movements))
        {
            foreach($movements as $i => $movement)
            {
                $movement_shopper = new ShopperMovement();
                $movement_shopper->shopper_balance = $movement->shopper_balance;
                $movement_shopper->card_balance = $movement->card_balance;
                $movement_shopper->date = $movement->date;
                $movement_shopper->movement = $movement->movement;
                if (!empty($movement->order_id))
                {
                       $movement_shopper->order_id = $movement->order_id;
                       $movement_shopper->supermarket = $movement->supermarket;
                       $movement_shopper->shopper_payment_method = $movement->shopper_payment_method;
                       $movement_shopper->order_total_amount = $movement->order_total_amount;
                       $movement_shopper->total_real_amount = $movement->total_real_amount;
                }
                $movements_shoppers[$i]['movement'] = $movement_shopper;
            }
        }

        $this->response = array(
            'status' => true,
            'message' => 'Movimientos obtenidos',
            'result' => array(
                'shopper_balance' => $balance['shopper'],
                'card_balance' => $balance['card'],
                'movements' => $movements_shoppers
            )
        );

        return $this->jsonResponse();
     }

    // Marca al usuario como no disponible/disponible
    public function update_agent_status()
    {

        if (!$shopper_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID shopper es requerido.', 400);

        if (!$shopper = Shopper::find($shopper_id))
            return $this->jsonResponse('ID shopper no existe.', 400);

        $status = false;
        $error = 'Undefined.';
        if ( $shopper ) {
            if ( $shopper->is_active == 1 ) {
                $shopper_schedule = ShopperSchedule::where('shopper_id', $shopper->id)->whereNull('deactivated_at')->orderBy('created_at', 'DESC')->first();
                if ( empty($shopper_schedule) ) {
                    $shopper_schedule = new ShopperSchedule();
                    $shopper_schedule->shopper_id   = $shopper->id;
                    if ( !is_null($shopper->activated_at) ) {
                        $shopper_schedule->activated_at = $shopper->activated_at;
                    }
                }
                $shopper_schedule->deactivated_at = Carbon::now()->toDateTimeString();
                $shopper->is_active = 0;
                $shopper->save();
                $shopper_schedule->save();

                $this->response = array(
                    'status' => true,
                    'message' => 'Usuario no disponible'
                );

                return $this->jsonResponse();
            }
            return $this->jsonResponse(200);
        } return $this->jsonResponse(404);
    }

}