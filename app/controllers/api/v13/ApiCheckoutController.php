<?php

namespace api\v13;

use Auth;
use Campaigns\Campaigns\BancolombiaCampaign;
use Campaigns\Campaigns\VisaCampaign;
use DeliveryWindow;
use exceptions\CoverageException;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Log;
use Input;
use jobs\ExecuteEventsInCreatedOrderJob;
use repositories\Eloquent\EloquentTagRepository;
use repositories\Interfaces\DeliveryExpressRepositoryInterface;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\Interfaces\ZoneRepositoryInterface;
use Route;
use Request;
use Config;
use usecases\Interfaces\Checkout\CouponUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryDiscountUserCaseInterface;
use usecases\Interfaces\Checkout\DeliveryQuick\RouteOrderUseCaseInterface;
use usecases\Interfaces\DeliveryDiscount\FirstOrderUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryDiscountUseCaseInterface;
use usecases\Products\Interfaces\GetIdsSuggestedProductByProductUseCaseInterface;
use usecases\Products\ValidatePrices\Interfaces\ValidateRealPublicPriceUseCaseInterface;
use Validator;
use Cart;
use CartProduct;
use User;
use UserAddress;
use UserCreditCard;
use UserCredit;
use Order;
use OrderGroup;
use OrderProduct;
use Event;
use OrderEventHandler;
use Coupon;
use UserCoupon;
use Store;
use OrderLog;
use DB;
use UserFreeDelivery;
use Survey;
use Zone;
use SuggestedProduct;
use City;
use Driver;
use StoreProduct;
use AlliedStore;
use Locality;
use OrderDiscount;
use StdClass;
use PayU;
use Sampling;
use Carbon\Carbon;
use carts\HelpCenterCartAdapter;
use delivery_times\DeliveryTimeConverter;
use exceptions\MerqueoException;
use carts\IosCartAdapter;
use orders\discounts\DeliveryDiscountByProduct;
use orders\discounts\FreeDeliveryDiscount;
use EconomicStratum;
use RejectReason;
use Warehouse;
use DeliveryExpress;

/**
 * Class ApiCheckoutController
 * @package api\v13
 */
class ApiCheckoutController extends \ApiController
{
    /**
     * @var int
     */
    const PRODUCT_SUGGESTED_LIMIT = 1;

    public $user_credit;

    /**
     * @var VisaCampaign
     */
    private $visaCampaign;

    /**
     * @var BancolombiaCampaign
     */
    private $bancolombiaCampaign;

    /**
     * @var array
     */
    private $user_discounts;

    /**
     * @var DeliveryDiscountUseCaseInterface
     */
    private $deliveryDiscountUseCase;

    /**
     * @var DeliveryExpressRepositoryInterface
     */
    private $deliveryExpressRepository;

    /**
     * @var ZoneRepositoryInterface
     */
    private $zoneRepository;

    /**
     * @var CouponUseCaseInterface
     */
    private $couponUseCase;

    /**
     * The parameters convert to float.
     *
     * @var array
     */
    private $parametersCast = ["price","special_price","delivery_discount_amount"];

    /**
     * @var RouteOrderUseCaseInterface
     */
    private $routeOrderUseCase;

    /**
     * @var GetIdsSuggestedProductByProductUseCaseInterface
     */
    private $getIdsSuggestedProductByProductUseCase;

    /**
     * @var EloquentTagRepository
     */
    private $eloquentTagRepository;

    /**
     * @var ValidateRealPublicPriceUseCaseInterface
     */
    private $validateRealPublicPriceUseCase;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * ApiCheckoutController constructor.
     * @param VisaCampaign $visaCampaign
     * @param BancolombiaCampaign $bancolombiaCampaign
     * @param DeliveryDiscountUseCaseInterface $deliveryDiscountUseCase
     * @param ZoneRepositoryInterface $zoneRepository
     * @param DeliveryExpressRepositoryInterface $deliveryExpressRepository
     * @param CouponUseCaseInterface $couponUseCase
     * @param RouteOrderUseCaseInterface $routeOrderUseCase
     * @param GetIdsSuggestedProductByProductUseCaseInterface $getIdsSuggestedProductByProductUseCase
     * @param EloquentTagRepository $eloquentTagRepository
     * @param ValidateRealPublicPriceUseCaseInterface $validateRealPublicPriceUseCase
     */
    public function __construct(
        VisaCampaign $visaCampaign,
        BancolombiaCampaign $bancolombiaCampaign,
        DeliveryDiscountUseCaseInterface $deliveryDiscountUseCase,
        ZoneRepositoryInterface $zoneRepository,
        DeliveryExpressRepositoryInterface $deliveryExpressRepository,
        CouponUseCaseInterface $couponUseCase,
        RouteOrderUseCaseInterface $routeOrderUseCase,
        GetIdsSuggestedProductByProductUseCaseInterface $getIdsSuggestedProductByProductUseCase,
        EloquentTagRepository $eloquentTagRepository,
        ValidateRealPublicPriceUseCaseInterface $validateRealPublicPriceUseCase,
        OrderRepositoryInterface $orderRepository
    ) {
        parent::__construct();
        $this->visaCampaign = $visaCampaign;
        $this->bancolombiaCampaign = $bancolombiaCampaign;
        $this->deliveryDiscountUseCase = $deliveryDiscountUseCase;
        $this->zoneRepository = $zoneRepository;
        $this->deliveryExpressRepository = $deliveryExpressRepository;
        $this->couponUseCase = $couponUseCase;
        $this->routeOrderUseCase = $routeOrderUseCase;
        $this->getIdsSuggestedProductByProductUseCase = $getIdsSuggestedProductByProductUseCase;
        $this->eloquentTagRepository = $eloquentTagRepository;
        $this->validateRealPublicPriceUseCase = $validateRealPublicPriceUseCase;
        $this->orderRepository = $orderRepository;

        $this->response = ['status' => false, 'message' => 'Error generico'];

        $this->beforeFilter(function () {
            $this->user_id = Input::get('user_id') ?: 0;
            $this->user_discounts = User::getDiscounts(
                $this->user_id,
                null,
                empty($this->device_id) ?: $this->isNewDevice()
            );
        });
    }

    /**
     * Obtiene horario de tiendas con base en carrito
     *
     * @return array $response Datos de horarios
     */
    public function delivery_time()
    {
        $cart = Input::get('cart');

        if (empty($cart)) {
            return $this->jsonResponse('Faltan datos requeridos.', 400);
        }

        $store_product_ids = [];
        $cart_product_info = [];
        $darkProducts = [];

        $products = json_decode($cart);
        if (count($products)) {
            foreach ($products as $id => $product) {
                $store_product_ids[] = $id;
                $cart_product_info[$id] = [
                    'quantity' => $product->qty,
                    'quantity_full_price' => isset($product->qty_full_price) ? $product->qty_full_price : 0,
                ];

                $cartProductExpress = new CartProduct();
                $cartProductExpress->store_product_id = $id;
                $cartProductExpress->quantity = $cart_product_info[$id]['quantity'];
                $cartProductExpress->quantity_full_price = $cart_product_info[$id]['quantity_full_price'];

                $darkProducts[] = $cartProductExpress;
            }
        }

        $store_id = 63;
        $stores = StoreProduct::whereIn('id', $store_product_ids)->groupBy('store_id')->get();
        if ($stores) {
            foreach ($stores as $store) {
                $store_id = $store->store_id;
                break;
            }
        }

        $delivery_store = $payment_methods = [];
        $config_payment_methods = $this->config['payment_methods'];
        unset($config_payment_methods[array_search('Débito - PSE', $config_payment_methods)]);
        $message_checkout = '';
        //mensaje para descuento global
        $checkout_discounts = Order::getCheckoutDiscounts();
        if ($checkout_discounts['discount_percentage']['status'] && $checkout_discounts['discount_percentage']['order_for_tomorrow']) {
            $discount_store_ids = explode(',', $checkout_discounts['discount_percentage']['store_id']);
            if (in_array($store_id, $discount_store_ids)) {
                $store = Store::find($store_id);
                $message_checkout = 'Gánate un ' . $checkout_discounts['discount_percentage']['amount'] . '% de descuento en tu pedido de ' . $store->name . ' programando tu entrega para mañana.';
            }
        }

        foreach ($config_payment_methods as $index => $value) {
            $payment_methods[] = ['value' => $value];
        }

        //validar si el usuario tiene domiclio gratis
        $has_free_delivery = false;
        $subtotal_cart = 0;
        $delivery_discount = 0;

        //verificar si el usuario tiene pedidos
        $info = $this->user_id ? \User::getOrdersInfo($this->user_id) : null;
        $last_order_date = !empty($info->last_order_date) ? new Carbon($info->last_order_date) : null;

        $store_products = StoreProduct::whereIn('store_products.id', $store_product_ids)->get();
        foreach ($store_products as $store_product2) {
            $store_product = $store_product2->toArray();
            $delivery_discount += $store_product['delivery_discount_amount'];
            $product_info = $cart_product_info[$store_product['id']];
            $special_quantity = $product_info['quantity'];
            $special_price = floatval(
                $store_product2->realSpecialPrice(
                    $info ? $info->qty : 0,
                    $last_order_date
                )
            );

            $price = empty($special_price) ? $store_product2->price : $special_price;
            $sub_total = $special_quantity * $price;

            if ($cart_product_info[$store_product['id']]['quantity_full_price']) {
                $sub_total += $cart_product_info[$store_product['id']]['quantity_full_price'] * $store_product['price'];
            }
            $subtotal_cart += $sub_total;
        }

        //domicilio gratis en proximo pedido
        if ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order']) {
            $has_free_delivery = true;
        }

        //validar domicilio gratis global
        if ($checkout_discounts['free_delivery']['status']) {
            if (!empty($checkout_discounts['free_delivery']['store_ids'])) {
                $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                $store_ids = count($store_ids) > 1 ? $store_ids : [$store_id];
                if (in_array($store_id,
                        $store_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                    $has_free_delivery = true;
                }
            } else {
                if (!empty($checkout_discounts['free_delivery']['city_ids'])) {
                    $store = Store::find($store_id);
                    $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                    $city_ids = count($city_ids) > 1 ? $city_ids : [$checkout_discounts['free_delivery']['city_ids']];
                    if (in_array($store->city_id,
                            $city_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                        $has_free_delivery = true;
                    }
                }
            }
        }

        $days = $time_week = [];
        $store = Store::find($store_id);
        $address = $this->getCurrentAddress();
        $zone = Input::get('zone_id') ? Zone::find(Input::get('zone_id')) : $this->getCurrentZone($store_id);
        $checkout_discounts = Order::getCheckoutDiscounts()['free_delivery'];
        $zoneDark = !empty($address)
        && isset($address->latitude)
        && isset($address->longitude)
            ? $this->zoneRepository->getZoneExpressByCoordinates($address->latitude, $address->longitude)
            : false;

        $has_free_delivery = $has_free_delivery || !empty($checkout_discounts['status']) &&
            $subtotal_cart >= $checkout_discounts['minimum_order_amount'];


        $freeDelivery = $this->deliveryDiscountUseCase->handle(
            $this->user_id,
            false,
            $this->isNewDevice(),
            $store->city_id,
            $store->id,
            $subtotal_cart
        );


        if ($address) {
            $validateExpress = $this->deliveryExpressRepository->validateDeliveryExpress(
                $address,
                $darkProducts,
                $zoneDark
            );
            $delivery = $store->getClearDeliverySlots(
                $zone,
                $has_free_delivery || $freeDelivery,
                $address->latitude,
                $address->longitude,
                $validateExpress
            );
        } else {
            $delivery = $store->getDeliverySlotCheckout(
                $zone,
                $has_free_delivery || $freeDelivery
            );
        }

        $slots = new DeliveryTimeConverter($delivery[$store->id], false, $this->getDefaultDeliveryDates());
        $selected_item = $slots->getSelectedItem();

        foreach ($delivery as $store_id => $delivery_store) {
            foreach ($delivery_store['days'] as $value => $text) {
                $days[] = ['value' => $value, 'text' => $text];
            }
            foreach ($delivery_store['time'] as $date => $times) {
                unset($time);
                foreach ($times as $value => $data) {
                    $time[] = [
                        'value' => $value,
                        'text' => $data['text'],
                        'delivery_amount' => $data['delivery_amount'],
                    ];
                }
                $day_times[$date] = $time;
            }
            if (isset($days) && isset($day_times)) {
                $delivery_store = ['days' => $days, 'time' => $day_times];
            }
        }

        //calcular total precio especial
        $special_price = 0;
        foreach (StoreProduct::whereIn('id', $store_product_ids)->get() as $product) {
            if ($product->special_price) {
                $special_price += ($product->price - $product->special_price) * $cart_product_info[$product->id]['quantity'];
            }
        }

        $showMessage = $this->visaCampaign->isEnabled([
            'user_id' => \Input::get('user_id'),
        ]);

        $payment_method_message = ['show' => 0];
        if ($showMessage) {
            $payment_method_message = [
                'show' => (int)$showMessage,
                'message' => $this->visaCampaign->getMessage(),
                'icon' => 'visa',
            ];
        } elseif ($this->bancolombiaCampaign->isEnabled(['user_id' => \Input::get('user_id'),]) && !$this->isIos()) {
            $payment_method_message = [
                'show' => 1,
                'message' => $this->bancolombiaCampaign->getMessage(),
                'icon' => '',
            ];
        }

        $delivery_time = [
            'message' => $message_checkout,
            'payment_methods' => $payment_methods,
            'payment_method_message' => $payment_method_message,
            'store' => empty($delivery_store) ? ['days' => [], 'time' => []] : $delivery_store,
            'special_price' => empty($special_price) ? 0 : floatval($special_price) ,
            'default_delivery_date' => $selected_item,
        ];

        $this->response = [
            'status' => true,
            'message' => 'Horario de tienda obtenido.',
            'result' => $delivery_time,
        ];

        return $this->jsonResponse();
    }

    /**
     * Obtiene los tiempos de entrega teniendo en cuenta .
     *
     * @param Store $store
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_delivery_dates(Store $store)
    {
        $cart_products = $darksupermarket_products = [];
        $delivery_discount = 0;
        $cart_total_amount = 0;
        $headers = getallheaders();
        $default_dates = $this->getDefaultDeliveryDates();
        $converter_class = isset($headers['User-Agent']) && strstr($headers['User-Agent'], 'iOS')
            ? IosCartAdapter::class
            : HelpCenterCartAdapter::class;

        $zone = Zone::with('warehouse')->findOrFail(\Input::get('zone_id'));
        $info = $this->user_id ? \User::getOrdersInfo($this->user_id) : null;
        $last_order_date = !empty($info->last_order_date) ? new Carbon($info->last_order_date) : null;

        try {
            $adapter = new $converter_class(\Input::get('cart'));
            $carts = $adapter->convert($zone->warehouse);

            $address = $this->getCurrentAddress();

            //Validar cobertura y productos en DarkSupermarket
            $zone_darksupermarket = !empty($address)
            && isset($address->latitude)
            && isset($address->longitude)
                ? $this->zoneRepository->getZoneExpressByCoordinates($address->latitude, $address->longitude)
                : false;
            if($zone_darksupermarket){
                $adapter_dark = $adapter->convert($zone_darksupermarket->warehouse);

                foreach ($adapter_dark as $dark) {
                    $darksupermarket_products = $dark->products;
                }
            }

            foreach ($carts as $cart) {
                $cart_products = $cart->products;
                foreach ($cart->products as $product) {
                    $full_items_price = $product->quantity_full_price * $product->price;
                    $special_items_price = ($product->quantity - $product->quantity_full_price)
                        * $product->storeProduct->realSpecialPrice($info ? $info->qty : 0, $last_order_date);
                    $cart_total_amount += $full_items_price + $special_items_price;
                    $delivery_discount += $product->storeProduct->delivery_discount_amount;
                }
            }

            $checkout_discounts = Order::getCheckoutDiscounts()['free_delivery'];
            $user_discounts = User::getDiscounts($this->user_id, null, $this->isNewDevice());
            $user_has_free_delivery = $user_discounts['free_delivery_next_order'] || $user_discounts['free_delivery_days'];
            $has_free_delivery_from_checkout = $user_has_free_delivery || !empty($checkout_discounts['status']) &&
                $cart_total_amount >= $checkout_discounts['minimum_order_amount'];


            $userHasfreeDelivery = $this->deliveryDiscountUseCase->handle(
                $this->user_id,
                false,
                $this->isNewDevice(),
                $store->city_id,
                $store->id,
                $cart_total_amount
            );

            if ($address) {

                $slot_warehouse_express = DeliveryExpress::validateCoverageAndProductsDarkSupermarket(
                    $address,
                    $cart_products,
                    $darksupermarket_products,
                    $zone_darksupermarket
                );

                $slots = $store->getClearDeliverySlots(
                    $zone,
                    $has_free_delivery_from_checkout || $user_has_free_delivery || $userHasfreeDelivery,
                    $address->latitude,
                    $address->longitude,
                    $slot_warehouse_express
                );

            } else {
                $slots = $store->getDeliverySlotCheckout(
                    $zone,
                    $has_free_delivery_from_checkout || $user_has_free_delivery || $userHasfreeDelivery
                );
            }
        } catch (MerqueoException $exception) {
            return $this->jsonResponse($exception->getMessage());
        }

        $slots = new DeliveryTimeConverter($slots[$store->id], false, $default_dates);
        $default_slot = $slots->hasFreeDelivery();

        $this->response = [
            'status' => true,
            'result' => [
                'free_delivery' => $default_slot,
                'delivery_dates' => $slots,
            ],
        ];
        return $this->jsonResponse('Ok');
    }

    /**
     * Procesa nuevo pedido
     * @return array $response Respuesta
     * @throws \Exception
     */
    public function checkout()
    {
        $user = null;
        $post_data = Input::all();
        try {
            DB::beginTransaction();

            //si no esta logueado el usuario se crea
            if (Input::has('create_user') && !Input::has('user_id')) {
                $email = Input::get('email');
                $password = Input::get('password');

                if (empty($email) || empty($password)) {
                    return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
                }

                //crear usuario
                $inputs = [
                    'first_name' => Input::get('first_name'),
                    'last_name' => Input::get('last_name'),
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                    'phone' => Input::get('phone'),
                    'do_not_send_mail' => 1,
                ];
                $request = Request::create('/api/1.3/user', 'POST', $inputs);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if (!$result['status']) {
                    $this->response['message'] = $result['message'];
                    return $this->jsonResponse();
                }
                $user = User::find($result['result']['user']['id']);
                $this->user_id = $user->id;
            } else {

                if (!$this->user_id) {
                    return $this->jsonResponse('ID usuario es requerido.', 400);
                }

                $user = User::find($this->user_id);
                $post_data['first_name'] = $user->first_name;
                $post_data['last_name'] = $user->last_name;
                $post_data['email'] = $user->email;

                if (empty($user->phone) && isset($post_data['user_phone'])) {
                    //verifica que el celular no exista
                    $validator = Validator::make(
                        ['phone' => $post_data['user_phone']],
                        ['phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com'],
                        [
                            'phone.required' => 'El número celular es requerido.',
                            'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                            'phone.numeric' => 'El número celular debe ser númerico.',
                            'phone.digits' => 'El número celular debe tener 10 digitos.',
                        ]
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        $this->response['message'] = $messages->first('phone');
                        return $this->jsonResponse();
                    }
                    $user->phone = $post_data['phone'] = $post_data['user_phone'];
                    $user->save();
                    unset($post_data['user_phone']);
                } else {
                    $post_data['phone'] = $user->phone;
                }
                if (empty($user->email) && isset($post_data['user_email'])) {
                    //verifica que el email no exista
                    $validator = Validator::make(
                        ['email' => $post_data['user_email'],],
                        ['email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com'],
                        [
                            'email.required' => 'El email es requerido.',
                            'email.unique' => 'El email ingresado ya se encuentra en uso.',
                            'email.email' => 'El formato del email ingresado no es valido.',
                        ]
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        $this->response['message'] = $messages->first('email');
                        return $this->jsonResponse();
                    }
                    $user->email = $post_data['email'] = $post_data['user_email'];
                    $user->save();
                    unset($post_data['user_email']);
                } else {
                    $post_data['email'] = $user->email;
                }
            }

            if (empty($user->email)) {
                $this->response['message'] = 'Para realizar el pedido debes ingresar un email.';
                return $this->jsonResponse();
            }

            if (!$user->status) {
                $this->response['message'] = 'Tu usuario ha sido bloqueado temporalmente, escribe a ' . \Config::get('app.admin_email');
                return $this->jsonResponse();
            }

            //APARTIR DE ESTE PUNTO YA EL USUARIO ESTA AUTENTICADO
            $post_data['user_id'] = $user->id;

            //validar cupon o codigo de referido
            $post_data['coupon_code'] = isset($post_data['coupon_code']) ? $post_data['coupon_code'] : null;
            if (!empty($post_data['coupon_code'])) {
                $data = ['coupon_code' => $post_data['coupon_code'], 'cart' => $post_data['cart'], 'validate' => 1];
                if (isset($post_data['create_user'])) {
                    $data['is_new_user'] = true;
                } else {
                    if (isset($post_data['user_id'])) {
                        $data['user_id'] = $post_data['user_id'];
                    }
                }
                $request = Request::create('/api/1.3/coupon', 'POST', $data);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if (!$result['status']) {
                    $this->response['message'] = $result['message'];
                    return $this->jsonResponse();
                }
            }

            //cargar codigo de referido
            $referred = User::validateReferred($user, $post_data['coupon_code']);
            if ($referred['success']) {
                $this->user_discounts = User::getDiscounts($user->id, $post_data['coupon_code'],
                    empty($this->device_id) ?: $this->isNewDevice(), true);
            }

            //verificar si el usuario tiene pedidos entregados
            $user_orders_info = User::getOrdersInfo($user->id);
            $user_has_orders = $user_orders_info->qty > 0;

            if (!$user_has_orders) {
                $this->validatePhoneNumber($user);
            }

            //si hay información de dirección se valida para crearla
            if (empty($post_data['address_id'])) {
                $address = new UserAddress;
                $address->user_id = $this->user_id;
                $address->label = $post_data['address_name'] == 'Otro' && isset($post_data['other_name']) ? $post_data['other_name'] : $post_data['address_name'];
                $address->address = trim($post_data['address']);
                $address->address_further = isset($post_data['address_further']) ? trim($post_data['address_further']) : 'Llamar para confirmar';
                $address->neighborhood = isset($post_data['address_neighborhood']) ? trim($post_data['address_neighborhood']) : '';
                $address->is_south_location = isset($post_data['address_is_south_location']) ? $post_data['address_is_south_location'] : 0;
                $dir = $post_data['dir'];
                if (is_string($dir)) {
                    $dir = json_decode($dir, true) ?: $dir;
                }
                $address->address_1 = trim($dir[0]);
                $address->address_2 = trim($dir[2]);
                $address->address_3 = trim($dir[6]);
                $address->address_4 = trim($dir[8]);

                //validar direccion
                if (empty($post_data['lat']) && empty($post_data['lng']) ||
                    (!empty($post_data['lat']) && !empty($post_data['lng'])
                        && $post_data['lat'] == '0.0' && $post_data['lng'] == '0.0')) {
                    $inputs = [
                        'address' => $address->address,
                        'city' => $post_data['city'],
                    ];
                    $request = Request::create('/api/location', 'GET', $inputs);
                    Request::replace($request->input());
                    $result = json_decode(Route::dispatch($request)->getContent(), true);
                    if ($result['status']) {
                        //validar direccion en el sur de la ciudad
                        $this->response = UserAddress::validateAddress($result, $post_data);
                        if ($this->response['status'] && $this->isIos()) {
                            return $this->jsonResponse();
                        }
                    } else {
                        $this->response = [
                            'status' => false,
                            'message' => 'No pudimos ubicar tu dirección',
                            'found' => false,
                            'detail' => 'Mueve el mapa hasta la ubicación donde quieres recibir tu pedido.',
                        ];
                        return $this->jsonResponse();
                    }

                    $address->latitude = $result['result']['latitude'];
                    $address->longitude = $result['result']['longitude'];
                    $address->city_id = $result['result']['city_id'];
                    $city = City::find($address->city_id);
                } else {
                    $address->latitude = $post_data['lat'];
                    $address->longitude = $post_data['lng'];
                    $post_data['city'] = str_replace(['á', 'é', 'í', 'ó', 'ú'], ['a', 'e', 'i', 'o', 'u'],
                        strtolower($post_data['city']));
                    if ($post_data['city'] == 'santafe de bogota') {
                        $post_data['city'] = 'bogota';
                    }
                    $post_data['city'] = str_replace(' ', '-', $post_data['city']);
                    $city = City::where('slug', $post_data['city'])->first();
                    $address->city_id = $city->id;
                }
                $address->save();
                $address_id = $address->id;
            } else {
                $address_id = $post_data['address_id'];
                if (!$address = UserAddress::find($address_id)) {
                    $this->response['message'] = 'La dirección que seleccionaste no existe en tu cuenta, por favor cambiala.';
                    return $this->jsonResponse();
                }
                $post_data['address'] = $address->address;
                $post_data['address_further'] = $address->address_further;
                $city = City::find($address->city_id);
            }

            //validar franja horaria express
            $shift_express = \DeliveryWindow::where(['id' => $post_data['delivery_time'],'shifts' => 'EX'])->count();

            try {
                //validar cobertura y obtener zona
                $zone = Zone::getByLatLng($this->getCoverageStoreCityId($address->city_id), $address->latitude, $address->longitude, $shift_express);

                if (!$zone){
                    $this->response['message'] = 'No hay cobertura en tu dirección en este momento.';
                    return $this->jsonResponse();
                }
            }catch (CoverageException $exception){
                Log::info("Zona sin cobertura",[
                    'exception' => $exception->getMessage(),
                    'address_city' => $address->city_id,
                    'latitude' => $address->latitude,
                    'longitude' => $address->longitude,
                    'user_id' => isset($post_data['user_id']) ? $post_data['user_id'] : 'Usuario no definido',
                ]);

                if (!isset($post_data['zone_id'])) {
                    $this->response['message'] = 'No hay cobertura en tu dirección en este momento.';
                    return $this->jsonResponse();
                }

                $zone = Zone::getActiveById($post_data['zone_id']);

                if (!$zone){
                    $this->response['message'] = 'No hay cobertura en tu dirección en este momento.';
                    return $this->jsonResponse();
                }

                $address->city_id = $zone->warehouse->city_id;
                $zone = Zone::getByLatLng($this->getCoverageStoreCityId($address->city_id), $address->latitude, $address->longitude, $shift_express);
            }

            //obtener longitud y latitud
            $stratum = EconomicStratum::getStratum($address->latitude, $address->longitude);

            //obtener carrito
            $cart = Cart::where('user_id', $this->user_id)->orderBy('updated_at', 'desc')->first();
            //si no existe entonces creo un carrito
            if (!$cart) {
                $cart = new Cart;
                $cart->user_id = $this->user_id;
                $cart->save();
            }

            //crear carrito en bd
            if (isset($post_data['cart'])) {
                $products = json_decode($post_data['cart']);
                if (count($products)) {
                    //obtener vesrsion del app
                    $os = strtolower($post_data['source_os']);
                    $app_version = intval(str_replace('.', '', $this->app_version));

                    CartProduct::where('cart_id', $cart->id)->delete();
                    foreach ($products as $store_product_id => $product) {
                        if ($product->qty <= 0) {
                            continue;
                        }

                        //validar estado de producto
                        $store_product = StoreProduct::select('store_products.id', 'products.name', 'products.quantity',
                            'products.unit', 'products.image_app_url',
                            'store_products.store_id', 'store_product_warehouses.status')
                            ->join('products', 'products.id', '=', 'store_products.product_id')
                            ->leftJoin('store_product_warehouses', function ($leftJoin) use ($zone) {
                                $leftJoin->on('store_products.id', '=', 'store_product_warehouses.store_product_id');
                                $leftJoin->where('store_product_warehouses.warehouse_id', '=', $zone->warehouse_id);
                            })
                            ->where('store_products.id', $store_product_id)
                            ->first();

                        if (!$store_product || empty($store_product->status) || $store_product->store_id != $city->coverage_store_id) {
                            if (!$store_product) {
                                $store_product = new stdClass;
                                $store_product->id = intval($store_product_id);
                                $store_product->name = $product->name;
                                $store_product->quantity = '';
                                $store_product->unit = '';
                                $store_product->image_app_url = '';
                            } else {
                                unset($store_product->store_id);
                                unset($store_product->status);
                            }
                            $store_product->price = $product->price;
                            $unavailable_products[] = $store_product;
                            continue;
                        }

                        if (!isset($unavailable_products)) {
                            $cart_product = new CartProduct;
                            $cart_product->cart_id = $cart->id;
                            $cart_product->store_product_id = $store_product->id;
                            $cart_product->quantity = $product->qty;
                            $cart_product->quantity_full_price = isset($product->qty_full_price) ? $product->qty_full_price : 0;
                            $cart_product->price = $product->price;
                            $cart_product->added_by = $this->user_id;
                            $cart_product->save();
                        }
                    }

                    if (isset($unavailable_products)) {
                        $this->response = [
                            'status' => false,
                            'message' => 'Hay productos que estan agotados para tu ubicación.',
                            'result' => [
                                'unavailable_products' => $unavailable_products,
                                'title' => 'Los siguientes productos estan agotados.',
                                'message' => 'Si continuas tu pedido quedará sin estos productos. ¿Deseas continuar?',
                                'warehouse_id' => $zone->warehouse_id,
                            ],
                        ];
                        return $this->jsonResponse();
                    }
                }
            }

            //validar metodo de pago
            $post_data['payment_method'] = $post_data['payment_method'] == 'Tarjeta de Crédito' ? 'Tarjeta de crédito' : $post_data['payment_method'];
            $is_credit_card = $post_data['payment_method'] == 'Tarjeta de crédito' ? true : false;
            if ($is_credit_card) {
                $payu = new PayU;

                //si la tarjeta es nueva se guarda y asocia a cliente en api
                $credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
                if (empty($credit_card_id)) {
                    //validar pais de tarjeta de credito
                    $bin = substr($post_data['number_cc'], 0, 6);
                    $credit_card = $payu->getCreditCardInfo($bin, $post_data);
                    if (!$credit_card) {
                        $this->response['message'] = 'Ocurrió un problema al validar el origen de tu tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    if (!$credit_card->is_valid) {
                        $this->response['message'] = 'Solo se aceptan tarjetas de crédito nacionales.';
                        return $this->jsonResponse();
                    }

                    $post_data['card_type'] = get_card_type($post_data['number_cc']);
                    if ($post_data['card_type'] == 'CODENSA') {
                        $this->response['message'] = 'No se aceptan pagos con tarjeta Codensa.';
                        return $this->jsonResponse();

                        if (!isset($post_data['email_cc'])) {
                            $this->response['message'] = 'Por favor actualiza la aplicación para pagos' .
                                ' con tarjeta Codensa.';
                            return $this->jsonResponse();
                        }
                    }

                    $post_data['expiration_month_cc'] = strlen($post_data['expiration_month_cc']) == 1 ? '0'.$post_data['expiration_month_cc'] : $post_data['expiration_month_cc'];
                    $result = $payu->associateCreditCard($post_data);
                    if (!$result['status']) {
                        $this->response['message'] = $result['message'];
                        return $this->jsonResponse();
                    }

                    if (!in_array($result['response']->creditCardToken->paymentMethod,
                        $this->config['credit_cards_types'])) {
                        $this->response['message'] = 'Solo se aceptan tarjetas de crédito ' . $this->config['credit_cards_message'];
                        return $this->jsonResponse();
                    }

                    $post_data['card_token'] = $result['response']->creditCardToken->creditCardTokenId;

                    //guardar tarjeta en bd
                    $user_credit_card = new UserCreditCard;
                    $user_credit_card->user_id = $user->id;
                    $user_credit_card->card_token = $post_data['card_token'];

                    if (isset($post_data['document_type_cc'])) {
                        $user_credit_card->holder_document_type = $post_data['document_type_cc'];
                        $user_credit_card->holder_document_number = $post_data['document_number_cc'];
                        $user_credit_card->holder_email = isset($post_data['email_cc']) ? $post_data['email_cc'] : $user->email;
                        $user_credit_card->holder_phone = $post_data['phone_cc'];
                        $user_credit_card->holder_address = $post_data['address_cc'];
                        $user_credit_card->holder_address_further = isset($post_data['address_further_cc']) ? $post_data['address_further_cc'] : '';
                        $user_credit_card->holder_city = isset($post_data['city_cc']) ? $post_data['city_cc'] : $city->city;
                    } else {
                        $user_credit_card->holder_document_number = isset($post_data['document_number_cc']) ? $post_data['document_number_cc'] : '';
                    }

                    $user_credit_card->bin = substr($post_data['number_cc'], 0, 6);
                    $user_credit_card->last_four = substr($post_data['number_cc'], 12, 4);
                    $user_credit_card->expiration_year = empty($post_data['expiration_year_cc']) ? '' : $post_data['expiration_year_cc'];
                    $user_credit_card->expiration_month = empty($post_data['expiration_month_cc']) ? '' : $post_data['expiration_month_cc'];
                    $user_credit_card->type = $post_data['card_type'];
                    $user_credit_card->holder_name = $post_data['name_cc'];
                    $user_credit_card->country = $credit_card->country_name;
                    $user_credit_card->created_at = date('Y-m-d H:i:s');
                    $user_credit_card->save();

                } else {
                    $user_credit_card = UserCreditCard::find($credit_card_id);
                    if (!$user_credit_card) {
                        $this->response['message'] = 'Ocurrió un error al obtener información de la tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    $post_data['card_token'] = $user_credit_card->card_token;
                    if (!empty($post_data['add_document_number_cc'])) {
                        $user_credit_card->holder_document_type = isset($post_data['add_document_type_cc']) ? $post_data['add_document_type_cc'] : 'CC';
                        $user_credit_card->holder_document_number = $post_data['add_document_number_cc'];
                        $user_credit_card->save();
                    }
                }
                //hacer cobro para validacion de tarjeta
                /*$post_data_validation = $post_data;
                $post_data_validation['total_amount'] = 1000;
                $post_data_validation['tax_amount'] = 0;
                $post_data_validation['installments_cc'] = 1;
                $post_data_validation['order_id'] = 1;
                $post_data_validation['description'] = 'Validación de tarjeta de crédito';
                $post_data_validation['nit'] = 'No especificado';
                $result = $payment->createCreditCardCharge($post_data_validation);

                if (!$result['status']){
                    $this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
                    return $this->jsonResponse();
                }

                $result = $payment->refundCreditCardCharge($user->id, $result['response']->id);
                if (!$result['status']){
                    $this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
                    return $this->jsonResponse();
                }*/
            }

            $cart_products = DB::table('cart_products')
                ->where('cart_id', $cart->id)
                ->leftJoin('store_products', 'cart_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->select(
                    'products.*', 'store_products.*', 'cart_products.quantity AS cart_quantity',
                    'cart_products.quantity_full_price AS cart_quantity_full_price',
                    'cart_products.store_id AS cart_store_id', 'cart_products.store_product_id',
                    'cart_products.price AS cart_price',
                    StoreProduct::getRawSpecialPriceQuery($user_orders_info->qty, $user_orders_info->last_order_date),
                    StoreProduct::getRawDeliveryDiscountByDate()
                )
                ->orderBy('store_id')
                ->get();

            $product_discount = new DeliveryDiscountByProduct();
            $response['cart']['delivery_amount'] = 0;
            $response['cart']['total_amount'] = 0;
            $response['cart']['discount_amount'] = 0;
            $response['cart']['total_quantity'] = 0;
            $response['cart']['stores'] = [];

            if ($cart_products) {
                $products = json_decode($post_data['cart'], true);
                $total_on_specific_department = 0;
                $response['cart']['is_minimum_reached'] = 1;

                foreach ($cart_products as $cart_product) {
                    if (!$cart_product->id) {
                        if (isset($products[$cart_product->product_id]) && isset($products[$cart_product->product_id]['name']) && !empty($products[$cart_product->product_id]['name'])) {
                            $this->response['message'] = 'El producto ' . $products[$cart_product->product_id]['name'] . ' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        } else {
                            $this->response['message'] = 'El producto con cantidad ' . $cart_product->cart_quantity . ' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        }
                        return $this->jsonResponse();
                    }

                    //validar producto en promocion en pedidos anteriores
                    if ($cart_product->special_price > -1 && $cart_product->quantity_special_price && $cart_product->cart_quantity) {
                        $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                            ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                            ->where('store_product_id', $cart_product->id)
                            ->where('order_products.price', $cart_product->special_price)
                            ->where('user_id', $this->user_id)
                            ->where('user_id', '<>', 425200)// Usuario trade
                            ->where('orders.status', '<>', 'Cancelled')
                            ->whereNotNull('store_products.special_price')
                            ->whereRaw(DB::raw(StoreProduct::getRawConditionByDateRange()))
                            ->where(DB::raw("DATEDIFF('" . date('Y-m-d') . "', date)"), '<=',
                                Config::get('app.minimum_promo_days'))
                            ->groupBy('order_products.store_product_id')
                            ->count();

                        if ($count_products) {
                            $product = new stdClass;
                            $product->id = $cart_product->id;
                            $product->name = $cart_product->name;
                            $product->quantity = $cart_product->quantity;
                            $product->unit = $cart_product->unit;
                            $product->price = $cart_product->cart_price;
                            $product->image_app_url = $cart_product->image_app_url;
                            $unavailable_products[] = $product;
                            continue;
                        }
                    }

                    //validar precio especial
                    /*$original_price = $cart_product->special_price ? $cart_product->special_price : $cart_product->price;
                    if ($original_price > $cart_product->cart_price){
                        $this->response['message'] = 'El precio del producto '.$cart_product->name.' ha cambiado, por favor eliminalo del carrito y vuelve a agregarlo.';
                        return $this->jsonResponse();
                    }*/

                    $store_product = json_decode(json_encode($cart_product), true);

                    //validar precio especial y cantidad del producto
                    if ($store_product['special_price'] > -1 && $store_product['quantity_special_price'] && $store_product['cart_quantity'] > $store_product['quantity_special_price'] && $store_product['special_price'] == $store_product['cart_price']) {
                        if (($store_product['first_order_special_price'] && !$user_has_orders) || ($store_product['cart_quantity'] > $store_product['quantity_special_price'])) {
                            $this->response['message'] = 'El producto ' . $store_product['name'] . ' por estar en promoción puedes agregar máximo ' . $store_product['quantity_special_price'] . ' unidad(es) en tu pedido, por favor disminuye la cantidad en el carrito para continuar.';
                            return $this->jsonResponse();
                        }
                    }

                    $store_id = $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id])) {
                        $tmp_store = Store::find($store_id);
                        $store = $tmp_store->toArray();
                        $store['count'] = 1;
                        $store['sub_total'] = 0;
                        $response['cart']['stores'][$store_id] = $store;
                        $response['cart']['stores'][$store_id]['products'] = [];
                        $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['sub_total'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_time_minutes'] = $store['delivery_time_minutes'];
                        $response['cart']['stores'][$store_id]['original_object'] = $tmp_store;
                    } else {
                        $response['cart']['stores'][$store_id]['count'] += 1;
                    }

                    if (!isset($store_product['cart_quantity'])) {
                        $store_product['cart_quantity'] = 1;
                    }

                    $store_product['sub_total'] = $store_product['cart_quantity'] * $store_product['cart_price'];
                    if ($store_product['cart_quantity_full_price']) {
                        $store_product['sub_total'] += $store_product['cart_quantity_full_price'] * $store_product['price'];
                    }
                    $response['cart']['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
                    $response['cart']['total_amount'] += $store_product['sub_total'];
                    $response['cart']['total_quantity']++;

                    if ($store_product['delivery_discount_amount']) {
                        $product_discount->add(StoreProduct::find($store_product['id']));
                    }

                    array_push($response['cart']['stores'][$store_id]['products'], $store_product);
                }

                if (isset($unavailable_products)) {
                    $this->response = [
                        'status' => false,
                        'message' => 'Hay productos en promoción que ya fueron pedidos.',
                        'result' => [
                            'unavailable_products' => $unavailable_products,
                            'title' => 'Ya realizaste un pedido con los siguientes productos en promoción:',
                            'message' => 'Si continuas el pedido quedará sin estos productos. ¿Deseas continuar?',
                        ],
                    ];
                    return $this->jsonResponse();
                }

                //bono para usuarios nuevos, pagos con mastercard y pedido mayor a 60000
                /*if (!$user_has_orders && $is_credit_card && $user_credit_card->type == 'MASTERCARD' && $response['cart']['total_amount'] > 60000){
                    $response['cart']['discount_amount'] = 20000;
                    foreach($response['cart']['stores'] as $store_id => $store){
                        $response['cart']['stores'][$store_id]['discount_amount'] = 20000;
                    }
                }*/
            } else {
                $this->response['message'] = 'Los productos en tu carrito cambiaron debido a una actualización, por favor eliminalos y vuelve a agregarlos al carrito de compras.';
                return $this->jsonResponse();
            }

            $subtotal_cart = $response['cart']['total_amount'];

            //aplicar descuento a la compra
            if (isset($this->user_discounts['coupon'])) {
                $coupon = $this->user_discounts['coupon'];

                //si el cupon cumple con totales requerido
                if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $subtotal_cart < $coupon->minimum_order_amount) {
                    $this->response['message'] = 'Para redimir el cupón de descuento ' . $coupon->code . ' el total del pedido debe ser mayor o igual a $' . number_format($coupon->minimum_order_amount,
                            0, ',', '.');
                    return $this->jsonResponse();
                } else {
                    if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $subtotal_cart > $coupon->maximum_order_amount) {
                        $this->response['message'] = 'Para redimir el cupón de descuento ' . $coupon->code . ' el total del pedido debe ser menor o igual a $' . number_format($coupon->maximum_order_amount,
                                0, ',', '.');
                        return $this->jsonResponse();
                    } else {
                        if ($coupon->minimum_order_amount && $coupon->maximum_order_amount && ($subtotal_cart < $coupon->minimum_order_amount || $subtotal_cart > $coupon->maximum_order_amount)) {
                            $this->response['message'] = 'Para redimir el cupón de descuento ' . $coupon->coupon . ' el total del pedido debe estar entre $' . number_format($coupon->minimum_order_amount,
                                    0, ',', '.') . ' y $' . number_format($coupon->maximum_order_amount, 0, ',', '.');
                            return $this->jsonResponse();
                        }
                    }
                }

                /* Verifica el cupon por rango mayor
                $couponUseCase = $this->couponUseCase;

                $couponUseCase->setCoupon($coupon);
                $couponUseCase->setCartSubtotal($subtotal_cart);

                if ($this->response['message'] = $couponUseCase->handle()) {
                    return $this->jsonResponse();
                }*/

                $discount_amount = 0;
                $coupon_discount = new OrderDiscount();
                $coupon_discount->coupon()->associate($coupon);
                //validar cupon para tienda / categoria / subcategoria / producto especifico
                if ($coupon->redeem_on == 'Specific Store') {
                    foreach ($cart_products as $store_product) {
                        $is_valid = false;
                        if ($store_product->store_id == $coupon->store_id) {
                            if ($coupon->department_id) {
                                if ($store_product->department_id == $coupon->department_id) {
                                    if ($coupon->shelf_id) {
                                        if ($store_product->shelf_id == $coupon->shelf_id) {
                                            if ($coupon->store_product_id) {
                                                if ($store_product->id == $coupon->store_product_id) {
                                                    $is_valid = true;
                                                }
                                            } else {
                                                $is_valid = true;
                                            }
                                        }
                                    } else {
                                        $is_valid = true;
                                    }
                                }
                            } else {
                                $is_valid = true;
                            }
                        }
                        if ($is_valid) {
                            if ($coupon->type == 'Discount Percentage') {
                                $discount_amount += round(($store_product->price * $store_product->cart_quantity) * ($coupon->amount / 100),
                                    0);
                            }
                            if ($coupon->type == 'Credit Amount') {
                                $discount_amount += round($store_product->price * $store_product->cart_quantity, 0);
                            }
                        }
                    }
                    $store = Store::select('stores.name AS store_name', 'departments.name AS department_name',
                        'shelves.name AS shelf_name',
                        DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"),
                        'cities.city')
                        ->leftJoin('departments', function ($join) use ($coupon) {
                            $join->on('departments.store_id', '=', 'stores.id');
                            $join->on('departments.id', '=',
                                DB::raw($coupon->department_id ? $coupon->department_id : 0));
                        })
                        ->leftJoin('shelves', function ($join) use ($coupon) {
                            $join->on('shelves.department_id', '=', 'departments.id');
                            $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                        })
                        ->leftJoin('store_products', function ($join) use ($coupon) {
                            $join->on('store_products.shelf_id', '=', 'shelves.id');
                            $join->on('store_products.id', '=',
                                DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                        })
                        ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                        ->join('cities', 'cities.id', '=', 'stores.city_id')
                        ->where('stores.id', $coupon->store_id)
                        ->first();
                    $discount_message = '';
                    if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name)) {
                        $discount_message = $store->department_name;
                    }
                    if (!empty($store->shelf_name) && empty($store->product_name)) {
                        $discount_message = $store->shelf_name;
                    }
                    if (!empty($store->product_name)) {
                        $discount_message = $store->product_name;
                    }
                    $discount_message .= ' en ' . $store->city;

                    if ($discount_amount) {
                        if ($coupon->type == 'Discount Percentage') {
                            $response['cart']['stores'][$coupon->store_id]['discount_percentage_amount'] = $coupon->amount;
                            $coupon_discount->percentage_amount = $coupon->amount;
                        } elseif ($discount_amount > $coupon->amount) {
                            $discount_amount = $coupon->amount;
                            $coupon_discount->amount = $coupon->amount;
                        }
                        $response['cart']['stores'][$coupon->store_id]['discount_amount'] = $discount_amount;
                        $response['cart']['discount_amount'] += $discount_amount;
                        $order_discount_data[] = $coupon_discount;
                    } else {
                        $this->response['message'] = 'El cupón de descuento ' . $coupon->code . ' aplica exclusivamente para ' . $discount_message;
                        return $this->jsonResponse();
                    }

                    $response['cart']['stores'][$coupon->store_id]['discount_message'] = $discount_message;
                }
                //validar metodo de pago y tipo de tarjeta
                if ($coupon->payment_method) {
                    if (($is_credit_card && $coupon->payment_method != 'Tarjeta de crédito' && $coupon->payment_method != $user_credit_card->type)
                        || (!$is_credit_card && $coupon->payment_method != $post_data['payment_method'])) {
                        $this->response['message'] = 'Para redimir el cupón de descuento ' . $coupon->code . ' el metodo de pago debe ser con ' . $this->config['payment_methods_names'][$coupon->payment_method] . trim(' ' . $coupon->cc_bank);
                        return $this->jsonResponse();
                    } else {
                        if ($is_credit_card && !empty($coupon->cc_bin)) {
                            if (empty($payment)) {
                                $payment = new \Payment();
                            }
                            $result = $payment->retriveCreditCard($user->customer_token, $user_credit_card->card_token,
                                $user->id);
                            if ($result['status']) {
                                $bin = $result['response']->bin;
                                $allowed_bines = explode(',', $coupon->cc_bin);
                                if (!in_array($bin, $allowed_bines)) {
                                    $this->response['message'] = 'Para redimir el cupón de descuento ' . $coupon->code . ' el metodo de pago debe ser con ' . $this->config['payment_methods_names'][$coupon->payment_method] . ' ' . $coupon->cc_bank;
                                    return $this->jsonResponse();
                                }
                            } else {
                                $this->response['message'] = 'No pudimos obtener información de tu tarjeta de crédito para la validación del cupón, por favor verificala e intenta de nuevo.';
                                return $this->jsonResponse();
                            }
                        }
                    }
                }
                //si el cupon no es de productos especificos se aplica el cupon
                if ($coupon->redeem_on != 'Specific Store') {
                    if ($coupon->type == 'Credit Amount') {
                        if ($coupon->amount > $response['cart']['total_amount']) {
                            $coupon->amount = $response['cart']['total_amount'];
                        }
                        $coupon_discount->amount = $coupon->amount;
                        $discount_credit_amount = $coupon->amount;
                    }
                    if ($coupon->type == 'Discount Percentage') {
                        $coupon_discount->percentage_amount = $coupon->amount;
                        $discount_percentage = $coupon->amount;
                    }

                    $order_discount_data[] = $coupon_discount;
                }
            }

            //si no tiene tiene cupon activo y si el total es mayor al requerido se aplica descuento con credito
            if (!isset($this->user_discounts['coupon']) && $this->user_discounts['amount'] && $subtotal_cart > $this->user_discounts['minimum_discount_amount']) {
                if ($this->user_discounts['amount'] > $response['cart']['total_amount']) {
                    $this->user_discounts['amount'] = $response['cart']['total_amount'];
                }
                $credit_discount = new OrderDiscount();
                $credit_discount->type = 'Crédito';
                $credit_discount->amount = $this->user_discounts['amount'];
                $order_discount_data[] = $credit_discount;
                $discount_credit_amount = $this->user_discounts['amount'];
            }

            //obtener descuentos activos en checkout
            $checkout_discounts = Order::getCheckoutDiscounts();
            $delivery_window = new StdClass();

            foreach ($response['cart']['stores'] as $store_id => $store) {

                if ($store['sub_total'] < $store['minimum_order_amount']) {
                    $response['cart']['is_minimum_reached'] = 0;
                }

                //valida que todas las tiendas tengan cobertura en la dirección y obtener deliveryWindow
                try {
                    $delivery_window = $store['original_object']->validateStoreDeliversToAddress(
                        $address->latitude,
                        $address->longitude,
                        new Carbon($post_data['delivery_day']),
                        $post_data['delivery_time']
                    );
                    $response['cart']['stores'][$store_id]['delivery_window'] = $delivery_window;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_window->delivery_amount;
                } catch (CoverageException $exception) {
                    $this->response = [
                        'status' => false,
                        'message' => $exception->getMessage(),
                        'found' => true,
                        'detail' => 'La dirección que ingresaste se encuentra fuera de nuestra cobertura.',
                    ];
                    return $this->jsonResponse();
                } catch (MerqueoException $exception) {
                    $this->response = [
                        'status' => false,
                        'message' => $exception->getMessage(),
                    ];
                    return $this->jsonResponse();
                }

                //validar fecha de entrega
                if (!self::hasValidDeliveryTime($zone, $post_data['delivery_day'], $delivery_window)) {
                    $this->response['message'] = 'Los horarios de entrega han cambiado, por favor vuelve a seleccionar el horario.';
                    return $this->jsonResponse();
                }

                //domicilio gratis en proximo pedido
                if ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order']) {
                    $free_delivery_discount = new \orders\discounts\FreeDeliveryDiscount(
                        $response['cart']['stores'][$store_id]['delivery_amount']
                    );
                    $free_delivery_discount->setUser($user);
                    $order_discount_data[] = $free_delivery_discount;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                }

                //validar domicilio gratis global
                if ($checkout_discounts['free_delivery']['status']) {
                    $free_delivery_discount = new \orders\discounts\FreeDeliveryDiscount(
                        $response['cart']['stores'][$store_id]['delivery_amount']
                    );
                    $free_delivery_discount->setUser($user);

                    if (!empty($checkout_discounts['free_delivery']['store_ids'])) {
                        $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                        $store_ids = count($store_ids) > 1 ? $store_ids : [$store_id];
                        if (in_array($store_id,
                                $store_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                            $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                            $free_delivery_discount->setReason(FreeDeliveryDiscount::BY_STORE);
                            $order_discount_data[] = $free_delivery_discount;
                        }
                    } else {
                        if (!empty($checkout_discounts['free_delivery']['city_ids'])) {
                            $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                            $city_ids = count($city_ids) > 1 ? $city_ids : [$checkout_discounts['free_delivery']['city_ids']];
                            if (in_array($response['cart']['stores'][$store_id]['city_id'],
                                    $city_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                                $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                                $free_delivery_discount->setReason(FreeDeliveryDiscount::BY_CITY);
                                $order_discount_data[] = $free_delivery_discount;
                            }
                        }
                    }
                }

                //validar descuento en domicilio por productos
                if ($response['cart']['stores'][$store_id]['delivery_amount'] && $product_discount->getTotalDiscount()) {
                    $discount = $product_discount->getTotalDiscount();
                    $delivery_amount = $response['cart']['stores'][$store_id]['delivery_amount'];
                    $delivery_amount = $delivery_amount >= $discount ? $delivery_amount - $discount : 0;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_amount;
                    if ($product_discount->count()) {
                        $order_discount_data[] = $product_discount;
                    }
                }

                $response['cart']['delivery_amount'] += $response['cart']['stores'][$store_id]['delivery_amount'];

                $userHasfreeDelivery = $this->deliveryDiscountUseCase->handle(
                    $this->user_id,
                    false,
                    $this->isNewDevice(),
                    $address->city_id,
                    $store_id,
                    $response['cart']['total_amount']
                );

                if ($userHasfreeDelivery) {
                    $response['cart']['delivery_amount'] = 0;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                }

                //sino hay cupon activo
                if (!isset($this->user_discounts['coupon'])) {
                    //validar descuento de credito global
                    if ($checkout_discounts['discount_credit']['status']) {
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_credit']['minimum_order_amount'] && !$checkout_discounts['discount_credit']['maximum_order_amount'] && $subtotal_cart < $checkout_discounts['discount_credit']['minimum_order_amount']) {
                            $is_valid = false;
                        } else {
                            if ($checkout_discounts['discount_credit']['maximum_order_amount'] && !$checkout_discounts['discount_credit']['minimum_order_amount'] && $subtotal_cart > $checkout_discounts['discount_credit']['maximum_order_amount']) {
                                $is_valid = false;
                            } else {
                                if ($checkout_discounts['discount_credit']['minimum_order_amount'] && $checkout_discounts['discount_credit']['maximum_order_amount'] &&
                                    ($subtotal_cart < $checkout_discounts['discount_credit']['minimum_order_amount'] || $subtotal_cart > $checkout_discounts['discount_credit']['maximum_order_amount'])) {
                                    $is_valid = false;
                                }
                            }
                        }

                        if ($is_valid) {
                            $discount_global_credit_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_credit']['store_id']) {
                                foreach ($store['products'] as $store_product) {
                                    $checkout_discount = new OrderDiscount();
                                    $store_product = (object)$store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_credit']['store_id']) {
                                        if ($checkout_discounts['discount_credit']['department_id']) {
                                            if ($store_product->department_id == $checkout_discounts['discount_credit']['department_id']) {
                                                if ($checkout_discounts['discount_credit']['shelve_id']) {
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_credit']['shelve_id']) {
                                                        if ($checkout_discounts['discount_credit']['product_id']) {
                                                            if ($store_product->id == $checkout_discounts['discount_credit']['store_product_id']) {
                                                                $checkout_discount->description = "Descuento en el producto #{$store_product->id}.";
                                                                $is_valid = true;
                                                            }
                                                        } else {
                                                            $checkout_discount->description = "Descuento en el pasillo #{$store_product->shelf_id}.";
                                                            $is_valid = true;
                                                        }
                                                    }
                                                } else {
                                                    $checkout_discount->description = "Descuento en el departamento #{$store_product->department_id}.";
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $checkout_discount->description = "Descuento en la tienda #{$store_product->store_id}.";
                                            $is_valid = true;
                                        }
                                    }
                                    if ($is_valid) {
                                        $discount_amount = round($product_price * $store_product->cart_quantity, 0);
                                        $discount_global_credit_amount += $discount_amount;
                                        $checkout_discount->type = 'Descuento';
                                        $checkout_discount->amount = $discount_amount;
                                        $order_discount_data[] = $checkout_discount;
                                    }
                                }
                            } else {
                                $checkout_discount = new OrderDiscount();
                                $checkout_discount->type = 'Descuento';
                                $checkout_discount->amount = $checkout_discounts['discount_credit']['amount'];
                                $checkout_discount->description = 'Descuento global';
                                $discount_global_credit_amount += $checkout_discounts['discount_credit']['amount'];
                            }

                            if ($discount_global_credit_amount) {
                                if ($discount_global_credit_amount > $checkout_discounts['discount_credit']['amount']) {
                                    $discount_global_credit_amount = $checkout_discounts['discount_credit']['amount'];
                                }
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_credit_amount;
                                $response['cart']['discount_amount'] += $discount_global_credit_amount;
                            }
                        }
                    }

                    //validar descuento por porcentaje global
                    if ($checkout_discounts['discount_percentage']['status']) {
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_percentage']['minimum_order_amount'] && !$checkout_discounts['discount_percentage']['maximum_order_amount'] && $subtotal_cart < $checkout_discounts['discount_percentage']['minimum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_percentage']['maximum_order_amount'] && !$checkout_discounts['discount_percentage']['minimum_order_amount'] && $subtotal_cart > $checkout_discounts['discount_percentage']['maximum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_percentage']['minimum_order_amount'] && $checkout_discounts['discount_percentage']['maximum_order_amount'] &&
                            ($subtotal_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'] || $subtotal_cart > $checkout_discounts['discount_percentage']['maximum_order_amount'])) {
                            $is_valid = false;
                        }

                        if ($is_valid) {
                            $discount_global_percentage_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_percentage']['store_id']) {
                                foreach ($store['products'] as $store_product) {
                                    $checkout_discount = new OrderDiscount();
                                    $store_product = (object)$store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_percentage']['store_id']) {
                                        if ($checkout_discounts['discount_percentage']['department_id']) {
                                            if ($store_product->department_id == $checkout_discounts['discount_percentage']['department_id']) {
                                                if ($checkout_discounts['discount_percentage']['shelve_id']) {
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_percentage']['shelve_id']) {
                                                        if ($checkout_discounts['discount_percentage']['product_id']) {
                                                            if ($store_product->id == $checkout_discounts['discount_percentage']['store_product_id']) {
                                                                $checkout_discount->description = "Descuento en el producto #{$store_product->id}.";
                                                                $is_valid = true;
                                                            }
                                                        } else {
                                                            $checkout_discount->description = "Descuento en el pasillo #{$store_product->shelf_id}.";
                                                            $is_valid = true;
                                                        }
                                                    }
                                                } else {
                                                    $checkout_discount->description = "Descuento en el departamento #{$store_product->department_id}.";
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $checkout_discount->description = "Descuento en la tienda #{$store_product->store_id}.";
                                            $is_valid = true;
                                        }
                                    }
                                    if ($is_valid) {
                                        $discount_global_percentage_amount += round(($product_price * $store_product->cart_quantity) * ($checkout_discounts['discount_percentage']['amount'] / 100),
                                            0);
                                        $checkout_discount->percentage_amount = $checkout_discounts['discount_percentage']['amount'];
                                        $checkout_discount->type = 'Descuento';
                                        $order_discount_data[] = $checkout_discount;
                                    }
                                }
                            } else {
                                $discount_global_percentage_amount += round($response['cart']['total_amount'] * ($checkout_discounts['discount_percentage']['amount'] / 100),
                                    0);
                                $checkout_discount = new OrderDiscount();
                                $checkout_discount->percentage_amount = $checkout_discounts['discount_percentage']['amount'];
                                $checkout_discount->type = 'Descuento';
                                $checkout_discount->description = 'Descuento global';
                                $order_discount_data[] = $checkout_discount;
                            }

                            if ($checkout_discounts['discount_percentage']['order_for_tomorrow']) {
                                $delivery_day = $post_data['delivery_day'];
                                if (!$delivery_day) {
                                    $discount_global_percentage_amount = 0;
                                } elseif ($delivery_day < date('Y-m-d', strtotime('+1 day'))) {
                                    $discount_global_percentage_amount = 0;
                                }
                            }

                            if ($discount_global_percentage_amount) {
                                $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $checkout_discounts['discount_percentage']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_percentage_amount;
                                $response['cart']['discount_amount'] += $discount_global_percentage_amount;
                            }
                        }
                    }
                }

                $campaignInput = [
                    'credit_card_id' => $is_credit_card ? $user_credit_card->id : null,
                    'user_id' => $user->id,
                    'total_amount' => $response['cart']['stores'][$store_id]['sub_total'],
                    'payment_method' => $post_data['payment_method'],
                ];

                //descuento pagos con tarjeta de credito
                if (!isset($this->user_discounts['coupon']) && $is_credit_card && !$response['cart']['stores'][$store_id]['discount_amount']) {
                    $applyDiscount = $this->visaCampaign->validate($campaignInput);

                    if ($applyDiscount) {
                        $discountAmount = $this->visaCampaign->getDiscountAmount(
                            $response['cart']['stores'][$store_id]['sub_total']
                        );

                        $response['cart']['discount_amount'] += $discountAmount;
                        $response['cart']['stores'][$store_id]['discount_percentage_amount'] = ($discountAmount / $response['cart']['stores'][$store_id]['sub_total']) * 100;
                        $response['cart']['stores'][$store_id]['discount_amount'] = $discountAmount;

                        $campaignDiscount = new OrderDiscount();
                        $campaignDiscount->percentage_amount = $this->visaCampaign->getPercentageDiscount();
                        $campaignDiscount->amount = $discountAmount;
                        $campaignDiscount->type = 'Descuento';
                        $campaignDiscount->description = $this->visaCampaign->getName();
                        $order_discount_data[] = $campaignDiscount;

                        $valid_credit_card_discount = true;
                    }
                }

                if ($this->bancolombiaCampaign->validate($campaignInput)) {
                    $this->bancolombiaCampaign->updateUser($user);
                    $user->save();

                    $campaignDiscount = new OrderDiscount();
                    $campaignDiscount->percentage_amount = $this->bancolombiaCampaign->getPercentageDiscount();
                    $campaignDiscount->amount = $response['cart']['delivery_amount'];
                    $campaignDiscount->type = 'Domicilio gratis';
                    $campaignDiscount->description = $this->bancolombiaCampaign->getName();
                    $order_discount_data[] = $campaignDiscount;

                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                    $response['cart']['delivery_amount'] = 0;
                }

                // Descuento por cupones de tipo credito
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                    && isset($discount_credit_amount) && $discount_credit_amount && !$response['cart']['stores'][$store_id]['discount_amount']) {
                    $total_order_store = $response['cart']['stores'][$store_id]['sub_total'] + $response['cart']['stores'][$store_id]['delivery_amount'];
                    $discount_amount = $discount_credit_amount > $total_order_store ? $total_order_store : $discount_credit_amount;
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['discount_amount'] += $discount_amount;
                    $discount_credit_amount -= $discount_amount;
                }
                // Descuento por cupones de tipo porcentaje
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                    && isset($discount_percentage) && $discount_percentage && !$response['cart']['stores'][$store_id]['discount_amount']) {

                    $discountValue = $response['cart']['stores'][$store_id]['sub_total'];
                    /*
                    if ($discountValue > $coupon->maximum_order_amount) {
                        $discountValue = $coupon->maximum_order_amount;
                    }*/

                    $discount_amount = round(($discountValue * $discount_percentage) / 100, 2);
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $discount_percentage;
                    $response['cart']['discount_amount'] += $discount_amount;
                }
            }

            if (isset($response['cart']['is_minimum_reached']) && !$response['cart']['is_minimum_reached']) {
                $this->response['message'] = 'Valor mínimo de pedido no alcanzado.';
                return $this->jsonResponse();
            }

            //cupon utilizado
            if (isset($coupon)) {
                //cargar cupon como utilizado a usuario
                $user_coupon = new UserCoupon;
                $user_coupon->coupon_id = $coupon->id;
                $user_coupon->user_id = $this->user_id;
                $user_coupon->save();
                //sumar credito de cupon a usuario
                $expiration_date = date('Y-m-d H:i:s', strtotime('+1 day'));
                UserCredit::addCredit($user, $response['cart']['discount_amount'], $expiration_date, 'coupon', $coupon);

                if (!$user->first_coupon_used && $coupon->type_use == 'Only The First Order') {
                    $user->first_coupon_used = 1;
                    $user->save();
                }
            }

            // obtener localidad
            $locality_id = null;
            $localities = Locality::where('city_id', $address->city_id)->where('status', 1)->get();
            if ($localities) {
                foreach ($localities as $locality) {
                    if (point_in_polygon($locality->polygon, $address->latitude, $address->longitude)) {
                        $locality_id = $locality->id;
                        break;
                    }
                }
            }

            //armar pedido de merqueo y pedidos de tiendas aliadas (MARKETPLACE)
            foreach ($cart_products as $id => $cart_product) {
                if (!empty($cart_product->allied_store_id)) {
                    if (!isset($orders['Marketplace'][$cart_product->allied_store_id])) {
                        $allied_store = AlliedStore::find($cart_product->allied_store_id);
                        $orders['Marketplace'][$cart_product->allied_store_id]['store_id'] = $cart_product->store_id;
                        $orders['Marketplace'][$cart_product->allied_store_id]['delivery_amount'] = 0;
                        $orders['Marketplace'][$cart_product->allied_store_id]['discount_amount'] = 0;
                        $orders['Marketplace'][$cart_product->allied_store_id]['total_amount'] = 0;
                        $orders['Marketplace'][$cart_product->allied_store_id]['allied_store_id'] = $cart_product->allied_store_id;
                        $orders['Marketplace'][$cart_product->allied_store_id]['delivery_date'] = get_delivery_date_marketplace(
                            Carbon::createFromFormat('Y-m-d H:i:s',
                                $post_data['delivery_day'] . ' ' . $delivery_window->hour_end),
                            $allied_store->delivery_time_hours
                        );
                    }
                    $type = 'Marketplace';
                    $store_id = $cart_product->allied_store_id;
                } else {
                    if (!isset($orders['Merqueo'][$cart_product->store_id])) {
                        $orders['Merqueo'][$cart_product->store_id]['store_id'] = $cart_product->store_id;
                        if (isset($response['cart']['stores'][$cart_product->store_id]['discount_percentage_amount'])) {
                            $orders['Merqueo'][$cart_product->store_id]['discount_percentage_amount'] = $response['cart']['stores'][$cart_product->store_id]['discount_percentage_amount'];
                        }
                        $orders['Merqueo'][$cart_product->store_id]['discount_amount'] = $response['cart']['stores'][$cart_product->store_id]['discount_amount'];
                        $orders['Merqueo'][$cart_product->store_id]['delivery_amount'] = $response['cart']['stores'][$cart_product->store_id]['delivery_amount'];
                        $orders['Merqueo'][$cart_product->store_id]['total_amount'] = 0;
                        $orders['Merqueo'][$cart_product->store_id]['allied_store_id'] = null;
                    }
                    $type = 'Merqueo';
                    $store_id = $cart_product->store_id;
                }

                //obtener precio de producto (full, especial / primera compra)
                if ($cart_product->special_price > -1) {
                    if ($cart_product->first_order_special_price) {
                        if (!$user_has_orders) {
                            $price = $cart_product->special_price;
                        } else {
                            $price = $cart_product->price;
                        }
                    } else {
                        $price = $cart_product->special_price;
                    }
                } else {
                    $price = $cart_product->price;
                }

                //calcular totales
                $orders[$type][$store_id]['total_amount'] += $cart_product->cart_quantity * $price;
                if ($cart_product->cart_quantity_full_price) {
                    $orders[$type][$store_id]['total_amount'] += $cart_product->cart_quantity_full_price * $cart_product->price;
                }
                $orders[$type][$store_id]['products'][] = $cart_product;
            }

            //recalcular descuento
            if (isset($orders['Merqueo'])) {
                reset($orders['Merqueo']);
                $store_id = key($orders['Merqueo']);
                if ($orders['Merqueo'][$store_id]['discount_amount'] > $orders['Merqueo'][$store_id]['total_amount']) {
                    $discount_amount = $orders['Merqueo'][$store_id]['discount_amount'];
                    $discount_amount -= $orders['Merqueo'][$store_id]['total_amount'];
                    $orders['Merqueo'][$store_id]['discount_amount'] = $orders['Merqueo'][$store_id]['total_amount'];
                    //asignar el resto del descuento a los marketplaces
                    foreach ($orders['Marketplace'] as $store_id => $store) {
                        if ($store['total_amount'] > $discount_amount) {
                            $orders['Marketplace'][$store_id]['discount_amount'] = $discount_amount;
                            break;
                        } else {
                            $discount_amount -= $orders['Marketplace'][$store_id]['total_amount'];
                            $orders['Marketplace'][$store_id]['discount_amount'] = $orders['Marketplace'][$store_id]['total_amount'];
                        }
                    }
                }
            } else {
                //si solo es pedido de marketplace se cobra domicilio y se aplica descuento si tiene
                reset($orders['Marketplace']);
                $store_id = key($orders['Marketplace']);
                $orders['Marketplace'][$store_id]['discount_amount'] = $response['cart']['discount_amount'];
                $orders['Marketplace'][$store_id]['delivery_amount'] = $response['cart']['delivery_amount'];
            }

            //crea un nuevo grupo para este pedido
            $order_group = new OrderGroup;
            $order_group->source = 'Device';
            if (isset($post_data['source_os'])) {
                $order_group->source_os = $post_data['source_os'];
            }

            if ($this->isNewDevice() && !$this->getCurrentDevice()) {
                $this->registerCurrentDevice($user);
            }

            if ($this->getCurrentDevice()) {
                $order_group->userDevice()->associate($this->getCurrentDevice());
            }

            $order_group->user_id = $this->user_id;
            $order_group->user_firstname = $user->first_name;
            $order_group->user_lastname = $user->last_name;
            $order_group->user_email = $user->email;
            $order_group->user_address = $address->address;
            $order_group->user_address_1 = $address->address_1;
            $order_group->user_address_2 = $address->address_2;
            $order_group->user_address_3 = $address->address_3;
            $order_group->user_address_4 = $address->address_4;
            $order_group->user_address_further = $address->address_further;
            $order_group->user_address_neighborhood = $address->neighborhood;
            $order_group->user_address_latitude = $address->latitude;
            $order_group->user_address_longitude = $address->longitude;
            $order_group->user_city_id = $address->city_id;
            $order_group->economic_stratum_id = !is_null($stratum) ? $stratum->id : null;
            $order_group->user_phone = $user->phone;
            $order_group->address_id = $address_id;
            $order_group->zone_id = isset($zone) ? $zone->id : null;
            $order_group->warehouse_id = isset($zone) ? $zone->warehouse_id : null;
            $order_group->locality_id = $locality_id;
            $order_group->discount_amount = $response['cart']['discount_amount'];
            $order_group->delivery_amount = $response['cart']['delivery_amount'];
            $order_group->total_amount = $response['cart']['total_amount'];
            $order_group->products_quantity = $response['cart']['total_quantity'];
            $order_group->user_comments = isset($post_data['comments']) ? trim($post_data['comments']) : '';
            $order_group->app_version = $this->app_version ? trim($this->app_version) : '';
            $order_group->device_player_id = isset($post_data['player_id']) ? trim($post_data['player_id']) : '';
            $order_group->ip = get_ip();
            $order_group->reception_user_name = sprintf('%s %s', $user->first_name, $user->last_name);
            $order_group->reception_user_phone = $user->phone;
            $order_group->save();

            $order_group_total_amount = 0;
            $sampling_orders = [];

            foreach ($orders as $type => $order_types) {
                foreach ($order_types as $store_id => $order_data) {
                    //inicializar variables para validar campañas de marca
                    //$is_valid_store = false;
                    //$allowed_stores = [36, 10];

                    $order = new Order;
                    $order->type = $type;
                    $order->reference = generate_reference();
                    $order->user()->associate($user);
                    $order->store_id = $order_data['store_id'];
                    $order->date = date("Y-m-d H:i:s");
                    $order->group_id = $order_group->id;
                    $order->total_products = count($order_data['products']);
                    $order->delivery_amount = $order_data['delivery_amount'];
                    $order->discount_amount = $order_data['discount_amount'];
                    $order->total_amount = $order_data['total_amount'];
                    $order->allied_store_id = $order_data['allied_store_id'];
                    $order->deliveryWindow()->associate($response['cart']['stores'][$store_id]['delivery_window']);
                    $order->delivery_time = $delivery_window->delivery_window;
                    if ($order->type === 'Merqueo') {
                        if ($delivery_window->shifts === DeliveryWindow::SHIFT_EXPRESS
                            || $delivery_window->shifts === DeliveryWindow::SHIFT_FAST_DELIVERY) {

                            $dateNow = Carbon::now();

                            $dateDelivery = $dateNow->addMinutes($delivery_window->delivery_time_minutes);

                            $order->first_delivery_date = sprintf('%s %s', $post_data['delivery_day'],
                                $dateDelivery->toTimeString());
                            $order->real_delivery_date = $order->delivery_date = $dateNow->toDateTimeString();
                        } else {
                            $order->first_delivery_date = sprintf('%s %s', $post_data['delivery_day'],
                                $delivery_window->hour_end);
                            $order->real_delivery_date = $order->delivery_date
                                = sprintf('%s %s', $post_data['delivery_day'], $delivery_window->hour_end);
                        }
                    } else {
                        $order->real_delivery_date = $order->delivery_date = $order->first_delivery_date = $order_data['delivery_date'];
                    }

                    $order->user_score_token = generate_token();
                    $order->payment_method = $post_data['payment_method'];

                    if (isset($order_data['discount_percentage_amount'])) {
                        $order->discount_percentage_amount = $order_data['discount_percentage_amount'];
                    }

                    if ($is_credit_card) {
                        $order->assignCreditCard(
                            $user_credit_card,
                            empty($post_data['installments_cc']) ? 1 : $post_data['installments_cc']
                        );

                        $checkCreditCardHistory = Order::where('credit_card_id', $user_credit_card->id)->where('status', 'Delivered')->count();
                        $order->save();
                        if( Config::get('app.payu.precharge_enable') && $checkCreditCardHistory == 0 ){
                            $preCharge = $order->preChargeCreditCard($user->id);
                            $order->payment_date = null;
                            $order->save();
                            if(isset($preCharge['status']) && !$preCharge['status']) {
                                $this->response['message'] = $preCharge['message'];
                                return $this->jsonResponse();
                            }
                        }
                    }

                    //validar datos de fraude para estado de pedido
                    $order->validateOrderBlackListed($post_data);

                    //datos de facturacion
                    if (isset($post_data['invoice_company_nit']) && !empty($post_data['invoice_company_nit'])) {
                        $order->user_identity_type = isset($post_data['invoice_identity_type']) ? $post_data['invoice_identity_type'] : 'NIT';
                        $order->user_business_name = $post_data['invoice_company_name'];

                        if (isset($post_data['invoice_identity_type']) && $post_data['invoice_identity_type'] == 'NIT') {
                            $nit = get_only_nit_number($post_data['invoice_company_nit']);
                            $digit = get_check_digit_nit($nit);
                            $order->user_identity_number = $nit . "-" . $digit;
                        } else {
                            $order->user_identity_number = $post_data['invoice_company_nit'];
                        }
                    }

                    $order->save();

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'Pedido creado.';
                    $log->user_id = $this->user_id;
                    $log->order_id = $order->id;

                    Log::info("Ordern guardada: {$order->id}");

                    $log->save();

                    $order_total_amount = 0;

                    foreach ($order_data['products'] as $cart_product) {
                        $store_product = StoreProduct::select(
                            'products.*', 'store_products.*', StoreProduct::getRawPublicPriceQuery(),
                            StoreProduct::getRawSpecialPriceQuery($user_orders_info->qty,
                                $user_orders_info->last_order_date),
                            StoreProduct::getRawDeliveryDiscountByDate()
                        )
                            ->join('products', 'store_products.product_id', '=', 'products.id')
                            ->where('store_products.id', $cart_product->id)
                            ->first();
                        //no deja que al pedido le aparezcan productos que no pertenecen a la tienda
                        if ($store_product->store_id != $order_data['store_id']) {
                            continue;
                        }

                        //INSERTAR PRODUCTOS
                        if ($cart_product->cart_quantity) {
                            //obtener precio de producto (full, especial / primera compra)
                            /*if ($store_product->special_price > -1) {
                                if ($store_product->first_order_special_price)
                                    if (!$user_has_orders)
                                        $price = $store_product->special_price;
                                    else $price = $store_product->price;
                                else $price = $store_product->special_price;
                            } else $price = $store_product->price;*/

                            $order_product = OrderProduct::saveProduct($cart_product->cart_price,
                                $cart_product->cart_quantity, $store_product, $order);
                            $order_total_amount += $order_product->price * $order_product->quantity;

                            $products_mail[] = [
                                'order_type' => $order->type,
                                'img' => $order_product->product_image_url,
                                'name' => $order_product->product_name,
                                'quantity' => $order_product->product_quantity,
                                'unit' => $order_product->product_unit,
                                'qty' => $order_product->quantity,
                                'price' => $order_product->price,
                                'total' => $order_product->quantity * $order_product->price,
                                'type' => $order_product->type,
                            ];
                        }

                        //INSERTAR PRODUCTOS ADICIONALES CON PRECIO FULL
                        if ($cart_product->cart_quantity_full_price) {
                            $order_product = OrderProduct::saveProduct($store_product->price,
                                $cart_product->cart_quantity_full_price, $store_product, $order);
                            $order_total_amount += $order_product->price * $order_product->quantity;

                            $products_mail[] = [
                                'order_type' => $order->type,
                                'img' => $order_product->product_image_url,
                                'name' => $order_product->product_name,
                                'quantity' => $order_product->product_quantity,
                                'unit' => $order_product->product_unit,
                                'qty' => $order_product->quantity,
                                'price' => $order_product->price,
                                'total' => $order_product->quantity * $order_product->price,
                                'type' => $order_product->type,
                            ];
                        }

                        //validar stock en promocion de producto
                        $store_product->validateStockDiscount($cart_product->cart_quantity);
                    }

                    if ($user->free_delivery_next_order) {
                        $user->free_delivery_next_order = 0;
                        $user->save();
                    }

                    if ($order->total_amount != $order_total_amount) {
                        $order->total_amount = $order_total_amount;
                        $order->save();
                    }

                    $order_group_total_amount += $order_total_amount;
                    $sampling_orders[] = ['order' => $order, 'products' => $order_data['products']];

                    //verificar condiciones de campaña de marca
                    /*if (!$is_valid_store){
                        if (in_array($store_id, $allowed_stores) && !isset($product_found)){
                            $is_valid_store = true;
                            if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'lavomatic')->first()) {
                                $user_brand_campaign = new UserBrandCampaign;
                                $user_brand_campaign->user_id = $this->user_id;
                                $user_brand_campaign->order_id = $order->id;
                                $user_brand_campaign->campaign = 'lavomatic';
                                $user_brand_campaign->save();
                            }
                        }
                    }
                    //verificar condiciones de campaña de marca
                    if ($address->city_id == 1 && isset($product_found) && $product_found) {
                        if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'mini chivas')->first()) {
                            $user_brand_campaign = new UserBrandCampaign;
                            $user_brand_campaign->user_id = $this->user_id;
                            $user_brand_campaign->order_id = $order->id;
                            $user_brand_campaign->campaign = 'mini chivas';
                            $user_brand_campaign->save();
                        }
                    }*/

                    //descontar credito utilizado a usuario
                    if (empty($valid_credit_card_discount) && $order_data['discount_amount'] && (isset($coupon) || $this->user_discounts['amount'])) {
                        if (isset($coupon)) {
                            UserCredit::removeCredit($user, $order_data['discount_amount'], 'order', $order,
                                $coupon->id);
                        } else {
                            UserCredit::removeCredit($user, $order_data['discount_amount'], 'order', $order);
                        }
                    }

                    //registrar log de domicilio gratis
                    if (!$order->delivery_amount && $user->free_delivery_expiration_date > date('Y-m-d')) {
                        $user_free_delivery = new UserFreeDelivery;
                        $user_free_delivery->user_id = $user->id;
                        $user_free_delivery->order_id = $order->id;
                        $user_free_delivery->amount = null;
                        $user_free_delivery->expiration_date = null;
                        $user_free_delivery->description = 'Domicilio gratis en pedido #' . $order->id . '. Fecha de vencimiento: ' . format_date('normal',
                                $user->free_delivery_expiration_date);
                        $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                        $user_free_delivery->save();
                    }

                    if ($order_group->total_amount != $order_group_total_amount) {
                        $order_group->total_amount = $order_group_total_amount;
                        $order_group->save();
                    }

                    //guardar descuento en pedido
                    if (isset($order_discount_data)) {
                        foreach ($order_discount_data as $discount) {
                            $discount->order()->associate($order);
                            $discount->save();
                        }
                    }
                }
            }

            //debug($order);

            //inactivar productos comprometidos
            $order->validateCommittedStock();

            if($shift_express){
                //pasar orden a estado enrutado directamente.
                $order->validateOrderExpress($zone);

                //crear servicio en mensajeros urbanos
                if(!$order->createServiceMensajerosUrbanos()){
                    $this->response['message'] = 'No hay cobertura express en este momento';
                    return $this->jsonResponse();
                }
            }

            //Enrutar la orden cuando sea entrega rápida
            $deliveryQuick = $delivery_window && $delivery_window->shifts == 'ER';
            if ($deliveryQuick) {
                $this->routeOrderUseCase->handler($order, $zone);
            }

            //insertar productos sampling
            foreach ($sampling_orders AS $sampling) {
                Sampling::getSamplingRelations($order_group, $sampling['products'], $sampling['order']);
            }

            // Campañas de regalo
            /*if ($this->user_agent == 'Android') {
                $gift = \CampaignGift::validateCampaignGift($zone->warehouse_id, $order);
            } else {*/
                $gift = [
                    'win_gift' => false,
                    'campaign' => [],
                ];
            //}

            $this->user_discounts = User::getDiscounts($user->id, null, true, true);
            $message = '';
            if (isset($orders['Merqueo']) && isset($orders['Marketplace'])) {
                $message = 'Algunos productos serán entregados en una fecha diferente a la seleccionada.';
            } else {
                if (!isset($orders['Merqueo']) && isset($orders['Marketplace'])) {
                    $message = 'Debido a los productos de tu pedido la fecha de entrega ha cambiado.';
                }
            }

            $this->response = [
                'status' => true,
                'message' => 'Pedido registrado',
                'result' => [
                    'user_id' => $user->id,
                    'order_id' => $order->id,
                    'order_status' => 'Recibido',
                    'order_reference' => $order->reference,
                    'order_date' => format_date('normal_short_with_time', $order_group->created_at),
                    'order_delivery_date' => format_date('normal_short',
                            substr($order->delivery_date, 0, 10)) . ' ' . $order->delivery_time,
                    'order_address' => $order_group->user_address . ' ' . $order_group->user_address_further,
                    'order_products' => $order_group->products_quantity,
                    'order_total' => $order_group->total_amount + $order_group->delivery_amount - $order_group->discount_amount,
                    'order_payment_method' => $order->payment_method,
                    'user_referral_code' => $user->referral_code,
                    'user_referral_url' => $user->referral_url,
                    'user_new' => isset($post_data['create_user']) ? 1 : 0,
                    'user_phone_validated' => empty($user->phone_validated_date) ? 0 : 1,
                    'special_price' => 0,
                    'message' => $message,
                    'referred_credit_amount' => currency_format(Config::get('app.referred.referred_by_amount')),
                    'gift' => $gift,
                ],
            ];

            if (isset($post_data['create_user'])) {
                $survey = Survey::find(1);
                $answers = explode(';', $survey->answers);
                shuffle($answers);
                $this->response['result']['survey'] = [
                    'id' => $survey->id,
                    'question' => $survey->question,
                    'answers' => $answers,
                ];
            }

            //elimina productos del carrito
            CartProduct::where('cart_id', $cart->id)->delete();

            DB::commit();

            //enviar mails
            $order_data = [
                'is_new_user' => isset($post_data['create_user']) ? 1 : 0,
                'products_mail' => $products_mail,
                'referred' => $referred,
                'total_products' => count($cart_products),
                'is_express' => $shift_express || $deliveryQuick,
            ];

            $delay = Carbon::now()->addSeconds(10);

            \Queue::later($delay, ExecuteEventsInCreatedOrderJob::class, [
                'order_id' => $order->id,
                'order_data' => $order_data,
            ], 'quick');

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene productos sugeridos y descuento de usuarios
     *
     * @return array $response Respuesta
     */
    public function get_cart()
    {
        $user_discounts_data = [];
        $suggestedProduct = null;
        $user_orders_info = $has_orders = $last_order_date = $special_price = 0;

        if (Input::has('user_id')) {
            $user = User::find(Input::get('user_id'));
            if ($user) {
                $user_discounts = User::getDiscounts($user->id);
                $user_discounts_data = [
                    'credit_available' => $user_discounts['amount'],
                    'free_delivery_days' => $user_discounts['free_delivery_days'],
                ];
                $user_orders_info = User::getOrdersInfo($user->id);
                $has_orders = $user_orders_info->qty;
                $last_order_date = $user_orders_info->last_order_date;
            }
        }

        if (Input::has('cart')) {
            $cart = Input::get('cart');
            $product_ids = [];
            $quantities = [];
            $shelf_ids = [];
            $products_to_update = [];
            $products = json_decode($cart, true);

            if (count($products)) {

                $orders = [];
                if (! empty($user->id)) {
                    $orders = $this->orderRepository
                        ->findByUserId($user->id)
                        ->toArray();
                }

                //colleccionar ids y cantidad
                foreach ($products as $id => $product) {
                    $product_ids[] = $id;
                    $quantities[intval($id)] = $product['qty'];

                    //validar productos de primera compra
                    /*if ($id > 0 && Input::has('user_id'))
                    {
                        if (!isset($product->first_order_special_price)){
                            $product_obj = Product::find($id);
                            $first_order_special_price = $product_obj->first_order_special_price;
                        }else $first_order_special_price = $product->first_order_special_price;
                        if ($first_order_special_price && $user_has_orders){
                            if (!isset($product_obj))
                                $product_obj = Product::find($id);
                            if ($product_obj->special_price == $product->price)
                                $products_to_update[] = ['id' => intval($id), 'price' => $product_obj->price];
                        }
                    }*/
                }

                //obtener productos del carrito
                $store_products = StoreProduct::select('store_products.*',
                    StoreProduct::getRawSpecialPriceQuery($has_orders, $last_order_date),
                    StoreProduct::getRawDeliveryDiscountByDate())
                    ->whereIn('id', $product_ids)->get();

                foreach ($store_products as $store_product) {
                    $storeProductIds[] = $store_product->id;
                    $shelf_ids[] = $store_product->shelf_id;
                    if ($store_product->special_price) {
                        $special_price += ($store_product->price - $store_product->special_price) * $quantities[$store_product->id];
                    }
                    if ($store_product->special_price && $products[$store_product->id]['price'] != $store_product->special_price && !$store_product->first_order_special_price) {
                        $products_to_update[] = [
                            'id' => $store_product->id,
                            'price' => floatval($store_product->price),
                            'special_price' => $store_product->special_price ? floatval($store_product->special_price) : null,
                            'quantity_special_price' => $store_product->quantity_special_price,
                        ];
                    } else {
                        if (!$store_product->special_price && $products[$store_product->id]['price'] != $store_product->price && !$store_product->first_order_special_price) {
                            $products_to_update[] = [
                                'id' => $store_product->id,
                                'price' => floatval($store_product->price),
                                'special_price' => $store_product->special_price ? floatval($store_product->special_price) : null,
                                'quantity_special_price' => $store_product->quantity_special_price,
                            ];
                        }
                    }
                }
                $warehouse = $this->getCurrentWarehouse();
                $excluded = SuggestedProduct::promosForProducts($product_ids);
                $result = SuggestedProduct::forShelves(
                    $shelf_ids,
                    $excluded,
                    self::PRODUCT_SUGGESTED_LIMIT,
                    $warehouse,
                    $user_orders_info,
                    $this->getIdsSuggestedProductByProductUseCase->handle($storeProductIds)
                );
                $result = castValueToFloat($result, $this->parametersCast);
                $result = setToNullIfZero($result, ['special_price']);

                if (! empty($result[0])) {
                    $suggestedProduct = $result[0];
                    $suggestedProduct->tag = $this->getTagFormat(
                        $this->eloquentTagRepository->getTagByStoreProductId($suggestedProduct->id)
                    );

                    if ($user->id) {
                        // Set real public price
                        $suggestedProduct->public_price = $this->validateRealPublicPriceUseCase->handler(
                            $orders,
                            $suggestedProduct->price,
                            $suggestedProduct->public_price,
                            $suggestedProduct->special_price,
                            $suggestedProduct->first_order_special_price
                        );
                    }
                }
            }
        }

        $this->response = [
            'status' => true,
            'message' => 'Información del carrito obtenida.',
            'result' => [
                'user_discounts' => $user_discounts_data,
                'suggested_products' => $suggestedProduct,
                'special_price' => empty($special_price) ? null : floatval($special_price) ,
                'products_to_update' => [
                    'status' => count($products_to_update) ? true : false,
                    'message' => 'Se ha actualizado el precio de algunos productos de tu carrito.',
                    'products' => $products_to_update,
                ],
            ],
        ];

        return $this->jsonResponse();
    }

    /**
     * Obtener datis calificacion de pedido
     *
     * @return array $response Respuesta
     */
    public function get_order_score()
    {
        if ($order_id = Route::current()->parameter('id')) {
            if (!Input::has('user_id')) {
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
            }

            $order = Order::where('id', $order_id)->where('status', 'Delivered')->where('user_id',
                Input::get('user_id'))->first();
            if (!$order) {
                $this->response['message'] = 'Ocurrió un problema al obtener información del pedido.';
                return $this->jsonResponse();
            }

            if (!empty($order->user_score)) {
                $this->response['message'] = 'Ya habias calificado este pedido.';
                return $this->jsonResponse();
            }

            $this->response = [
                'status' => true,
                'message' => 'Datos de calificación obtenidos.',
                'result' => Config::get('app.order_score'),
            ];

        } else {
            return $this->jsonResponse('El ID del pedido es requerido.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Registrar calificacion de pedido
     *
     * @return array $response Respuesta
     */
    public function save_order_score()
    {
        if ($order_id = Route::current()->parameter('id')) {
            if (!Input::has('score') || !Input::has('user_id') || !Input::get('token')) {
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
            }

            $order = Order::where('user_score_token', Input::get('token'))->where('orders.status', 'Delivered')
                ->whereNull('user_score')->orderBy('orders.id', 'desc')->first();
            if (!$order) {
                $this->response['message'] = 'Ya habias calificado este pedido.';
                return $this->jsonResponse();
            }

            $user_id = Input::get('user_id');
            if (!$order = Order::where('id', $order_id)->where('user_id', $user_id)->where('status',
                'Delivered')->first()) {
                $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual o el estado para calificarlo no es valido.';
                return $this->jsonResponse();
            }

            $order->user_score = Input::get('score') < 4 ? 0 : 1;
            $order->user_score_source = 'App';
            $order->user_score_comments = Input::has('comments') ? Input::get('comments') : null;
            $order->user_score_date = date('Y-m-d H:i:s');
            $order->save();

            if ($order->user_score < 4) {
                $order_group = OrderGroup::where('order_groups.id',
                    $order->group_id)->select('order_groups.user_firstname', 'order_groups.user_lastname')->first();
                $html = '<b>ID del pedido: </b> ' . $order->id . '<br>
                    <b>Email del usuario: </b> ' . $order_group->user_email . '<br>
                    <b>Nombre del usuario: </b> ' . $order_group->user_firstname . ' ' . $order_group->user_lastname . '<br>
                    <b>Valor del pedido: </b> $' . number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount),
                        0, ',', '.') . '<br>
                    <b>Fecha de ingreso del pedido: </b> ' . $order->created_at . '<br>
                    <b>Fecha de entrega del pedido: </b> ' . $order->management_date . '<br>
                    <b>Calificacion: </b> ' . $order->user_score . '<br>
                    <b>Comentarios del pedido: </b> ' . $order->user_score_comments . '<br>
                    <b>Dispositivo: </b>Móvil';
                $mail = [
                    'template_name' => 'emails.daily_report',
                    'subject' => 'Calificacion de pedido #' . $order->id,
                    'to' => [
                        ['email' => 'servicioalcliente@merqueo.com', 'name' => 'Servicio al cliente'],
                    ],
                    'vars' => [
                        'title' => 'Calificacion de pedido #' . $order->id,
                        'html' => $html,
                    ],
                ];
                send_mail($mail);
            }

            $log = new OrderLog();
            $log->type = 'Calificación a conductor: ' . Input::get('score');
            $log->user_id = $user_id;
            $log->order_id = $order->id;
            $log->save();

            $this->response = [
                'status' => true,
                'message' => 'Tu calificación fue guardada.',
            ];

        } else {
            return $this->jsonResponse('El ID del pedido es requerido.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtener datos de pedido para notificacion
     *
     * @return array $response Respuesta
     */
    public function notification_order()
    {
        if ($order_id = Route::current()->parameter('id')) {
            if (!Input::has('type') || !Input::has('user_id')) {
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
            }

            $user_id = Input::get('user_id');
            $order = Order::join('stores', 'stores.id', '=', 'orders.store_id')
                ->select('stores.name AS store_name', 'minimum_order_amount', 'app_logo_small_url', 'orders.*',
                    'is_storage')
                ->where('orders.id', $order_id)->where('user_id', $user_id)->first();
            if (!$order) {
                $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual.';
                return $this->jsonResponse();
            }

            $cloudfront_url = Config::get('app.aws.cloudfront_url');

            //pedido asignado
            if (Input::get('type') == 'In Progress' || Input::get('type') == 'Dispatched') {
                $driver = Driver::find($order->driver_id);
                $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('driver_id',
                    $order->driver_id)
                    ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('driver_id')->first();
                $driver_score_average = $result ? round($result->average, 1) : 0;
                $data = [
                    'shopper' => [
                        'image' => !empty($driver->photo_url) ? $driver->photo_url : $cloudfront_url . '/shoppers/photos/avatar.png',
                        'name' => $driver->first_name . ' ' . $driver->last_name,
                        'phone' => $driver->phone,
                        'shopper_score_average' => $driver_score_average,
                    ],
                    'order' => [
                        'id' => $order->id,
                        'total_amount' => $order->total_amount,
                        'total_delivery' => $order->delivery_amount,
                        'total_discount' => $order->discount_amount,
                    ],
                ];

                $order_products = OrderProduct::select('store_product_id AS id', 'price', 'quantity AS quantity_cart',
                    'product_name AS name', 'product_image_url AS image_url', 'product_quantity AS quantity',
                    'product_unit AS unit', 'fulfilment_status AS status')
                    ->where('order_id', $order->id)
                    ->where('is_gift', 0)
                    ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                    ->get();

                //obtener productos regalo
                if ($order->date > '2018-01-01 00:00:00') {
                    foreach ($order_products as $order_product) {
                        if ($order_product->status == 'Not Available') {
                            $order_product_gift = OrderProduct::select('store_product_id AS id', 'price',
                                'quantity AS quantity_cart', 'product_name AS name',
                                'product_image_url AS image_url', 'product_quantity AS quantity',
                                'product_unit AS unit', 'fulfilment_status AS status')
                                ->where('parent_id', $order_product->id)
                                ->where('order_id', $order->id)
                                ->where('is_gift', 1)
                                ->first();
                            if ($order_product_gift) {
                                $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                                $order_product->gift = $order_product_gift->toArray();
                            }
                        }
                    }
                }

                $data['products'] = $order_products->toArray();
            }
            //pedido entregado
            if (Input::get('type') == 'Delivered') {
                $driver = Driver::find($order->driver_id);

                $data = [
                    'shopper' => [
                        'image' => !empty($driver->photo_url) ? $driver->photo_url : $cloudfront_url . '/shoppers/photos/avatar.png',
                        'name' => $driver->first_name . ' ' . $driver->last_name,
                    ],
                ];
            }
            //pedido cancelado
            if (Input::get('type') == 'Cancelled') {
                $order_group = OrderGroup::find($order->group_id);
                $order_delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_short_with_time',
                    $order->delivery_date) : format_date('normal_short',
                        substr($order->delivery_date, 0, 10)) . ' ' . $order->delivery_time;
                $reject = $order->reject_reason;
                if(($order->reject_reason_id != 0)){
                    $reject = RejectReason::find($order->reject_reason_id);
                    $reject = $reject->reason;
                }

                $data = [
                    'reject_reason' => $reject,
                    'order' => [
                        'id' => $order->id,
                        'reference' => $order->reference,
                        'date' => format_date('normal_short_with_time', $order_group->created_at),
                        'delivery_date' => $order_delivery_date,
                        'address' => $order_group->user_address,
                        'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount,
                        'payment_method' => $order->payment_method,
                    ],
                ];
            }
            $data['type'] = Input::get('type');
            $data['store']['name'] = $order->store_name;
            $data['store']['minimum_order_amount'] = $order->minimum_order_amount;
            $data['store']['app_logo_small_url'] = $order->app_logo_small_url;

            $this->response = ['status' => true, 'message' => 'Notificación obtenida.', 'result' => $data];

        } else {
            return $this->jsonResponse('El ID del pedido es requerido.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos pendientes por usuario
     *
     * @return array $response Respuesta
     */
    public function pending_orders()
    {
        $countryId = Input::get('country_id');
        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('El ID usuario es requerido.', 400);
        }

        if (!$user = User::find($user_id)) {
            return $this->jsonResponse('Usuario no existe.', 400);
        }

        try {
            $orders_user = $user->getOrders('pending', null, $countryId);
        } catch (\Exception $exception) {
            return $this->response = [
                'status' => false,
                'message' => $exception->getMessage(),
            ];
        }

        if ($orders_user instanceof Paginator) {
            $orders_user = $orders_user->getCollection();
        }

        $this->response = [
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => $orders_user,
        ];

        return $this->jsonResponse();
    }

    /**
     * Validar cupon o codigo de referido en compra
     *
     * @return array $response Respuesta
     */
    public function coupon()
    {
        if (Input::get('user_id')) {
            $user = User::find(Input::get('user_id'));
        } else {
            $user = 0;
        }

        $coupon_code = Input::get('coupon_code');
        if (empty($coupon_code)) {
            return $this->jsonResponse('Codigo del cupón es requerido.', 400);
        }

        //validar codigo del cupon
        $coupon = Coupon::where('code', $coupon_code)->first();
        if (!$coupon) {
            $referrer = User::where('referral_code', Input::get('coupon_code'))->where('status', 1);
            if ($user) {
                $referrer->where('id', '<>', $user->id);
            }
            $referrer = $referrer->first();
            if (!$referrer) {
                $this->response['message'] = 'El código no es valido.';
                return $this->jsonResponse();
            }
            if ($referrer->referral_code_blocked) {
                $this->response['message'] = 'El código referido esta bloqueado.';
                return $this->jsonResponse();
            }
            if (!$referrer->orders()->where('status', 'Delivered')->count()) {
                $this->response['message'] = $referrer->first_name . ' debe recibir su primer pedido para que puedas utilizar el beneficio de créditos por referido.';
                return $this->jsonResponse();
            }
            $referrerCount = User::where('referred_by', $referrer->id)->count();
            if($referrerCount >= Config::get('app.merqueo.referred_limit')){
                $this->response['message'] = $referrer->first_name.' ha alcanzado el número máximo de referidos.';
                return $this->jsonResponse();
            }
        }

        //redimir cupon de descuento
        if ($coupon) {
            //validar estado
            if (!$coupon->status) {
                $this->response['message'] = 'El cupón ya no esta activo.';
                return $this->jsonResponse();
            }
            //validar fecha de expiracion
            if (date('Y-m-d') > $coupon->expiration_date) {
                $this->response['message'] = 'El cupón ya no esta vigente.';
                return $this->jsonResponse();
            }
            //validar numero de usos
            $uses = UserCoupon::where('coupon_id', $coupon->id)->count();
            if ($uses >= $coupon->number_uses) {
                $this->response['message'] = 'El cupón ya fue utilizado.';
                return $this->jsonResponse();
            }

            //validar si el usuario ya utilizo el cupon
            $user_coupon = $user ? UserCoupon::where('user_id', $user->id)->where('coupon_id',
                $coupon->id)->first() : false;
            if ($user_coupon) {
                $this->response['message'] = 'Ya redimiste este cupón.';
                return $this->jsonResponse();
            }
            //validar credito de primera compra
            /*if ($user && $user->first_coupon_used){
                $this->response['message'] = 'Este tipo de cupón ya fue redimido en tu cuenta.';
                return $this->jsonResponse();
            }*/
            //validar el tipo de uso del cupon
            if ($user && $coupon->type_use == 'Only The First Order') {
                $count_orders = Order::where('user_id', '=', $user->id)->where('status', '<>', 'Cancelled');
                //ignorar pedido especifico
                if (Input::has('order_id')) {
                    $count_orders->where('id', '<>', Input::get('order_id'));
                }
                $count_orders = $count_orders->count();
                if ($count_orders) {
                    $this->response['message'] = 'Este cupón solo podias utilizarlo en tu primera compra.';
                    return $this->jsonResponse();
                }
            }
            if ($user && $user->created_at < date('Y-m-d H:i:s') && $coupon->type_use == 'Only New Customers' && !Input::has('is_new_user')) {
                $this->response['message'] = 'Este cupón solo podias utilizarlo al crear tu cuenta de Merqueo.';
                return $this->jsonResponse();
            }
            //validar cupon en grupo de campaña
            if ($user && !empty($coupon->campaign_validation)) {
                $campaign = Coupon::select('coupons.id')
                    ->join('user_coupons', function ($join) use ($user) {
                        $join->on('user_coupons.coupon_id', '=', 'coupons.id');
                        $join->on('user_coupons.user_id', '=', DB::raw($user->id));
                    })
                    ->where('coupons.campaign_validation', $coupon->campaign_validation)
                    ->first();
                if ($campaign) {
                    $this->response['message'] = 'Ya usaste un cupón de esta campaña.';
                    return $this->jsonResponse();
                }
            }
            //validar cupon a tienda o categoria especifica
            if ($coupon->redeem_on == 'Specific Store') {
                if ($products = $this->currentCart('getProducts')) {
                    $is_valid = false;
                    foreach ($products as $product) {
                        if ($product->store_id == $coupon->store_id) {
                            if ($coupon->department_id) {
                                if ($product->department_id == $coupon->department_id) {
                                    if ($coupon->shelf_id) {
                                        if ($product->shelf_id == $coupon->shelf_id) {
                                            if ($coupon->store_product_id) {
                                                if ($product->id == $coupon->store_product_id) {
                                                    $is_valid = true;
                                                }
                                            } else {
                                                $is_valid = true;
                                            }
                                        }
                                    } else {
                                        $is_valid = true;
                                    }
                                }
                            } else {
                                $is_valid = true;
                            }
                        }
                    }
                    if (!$is_valid) {
                        $store = Store::select('stores.name AS store_name', 'departments.name AS department_name',
                            'shelves.name AS shelf_name', 'products.name AS product_name', 'cities.city')
                            ->leftJoin('departments', function ($join) use ($coupon) {
                                $join->on('departments.store_id', '=', 'stores.id');
                                $join->on('departments.id', '=',
                                    DB::raw($coupon->department_id ? $coupon->department_id : 0));
                            })
                            ->leftJoin('shelves', function ($join) use ($coupon) {
                                $join->on('shelves.department_id', '=', 'departments.id');
                                $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                            })
                            ->leftJoin('store_products', function ($join) use ($coupon) {
                                $join->on('store_products.shelf_id', '=', 'shelves.id');
                                $join->on('store_products.id', '=',
                                    DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                            })
                            ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                            ->join('cities', 'cities.id', '=', 'stores.city_id')
                            ->where('stores.id', $coupon->store_id)
                            ->first();
                        $message = '';
                        if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name)) {
                            $message = $store->department_name;
                        }
                        if (!empty($store->shelf_name) && empty($store->product_name)) {
                            $message = $store->shelf_name;
                        }
                        if (!empty($store->product_name)) {
                            $message = $store->product_name;
                        }
                        $message .= ' en ' . $store->city;

                        $this->response['message'] = 'Este cupón aplica exclusivamente para ' . $message . ', debe estar en el carrito.';
                        return $this->jsonResponse();
                    }
                }
            }

            //validar que el carrito no tenga productos en promocion
            /*if ($this->currentCart('hasSpecialprice')){
                $this->response['message'] = 'No puedes redimir el cupón por que ya tienes productos en promoción en el carrito.';
                return $this->jsonResponse();
            }*/

            //validar antes de procesar compra
            if (Input::has('validate')) {
                $this->response = [
                    'status' => true,
                    'message' => 'Cupón valido',
                    'result' => [
                        'coupon_id' => $coupon->id,
                        'coupon_amount' => $coupon->amount,
                    ],
                ];

                return $this->jsonResponse();
            }

            $message = 'El cupón ha sido agregado a tu pedido actual.';
            $total = $this->currentCart('getTotal');
            if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount) {
                $message .= ' Para usarlo el total del pedido debe ser mayor o igual a $' . number_format($coupon->minimum_order_amount,
                        0, ',', '.') . '.';
            } else {
                if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount) {
                    $message .= ' Para usarlo el total del pedido debe ser menor o igual a $' . number_format($coupon->maximum_order_amount,
                            0, ',', '.') . '.';
                } else {
                    if ($coupon->minimum_order_amount && $coupon->maximum_order_amount) {
                        $message .= ' Para usarlo el total del pedido debe estar entre $' . number_format($coupon->minimum_order_amount,
                                0, ',', '.') . ' y $' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '.';
                    }
                }
            }
            if ($coupon->payment_method) {
                $message .= ' El metodo de pago debe ser con ' . $this->config['payment_methods_names'][$coupon->payment_method] . trim(' ' . $coupon->cc_bank) . '.';
            }
            if ($coupon->code == 'MASTERCAMBIO') {
                $message .= ' Debes tener un pedido entregado y pagado con una tarjeta diferente a Mastercard.';
            }

            $credit = $user ? User::getDiscounts($user->id) : ['amount' => 0];

            $this->response = [
                'status' => true,
                'message' => $message,
                'result' => [
                    'type' => 'coupon',
                    'coupon_id' => $coupon->id,
                    'coupon_type' => $coupon->type,
                    'coupon_amount' => $coupon->amount,
                    'coupon_minimum_order_amount' => $coupon->minimum_order_amount,
                    'coupon_maximum_order_amount' => $coupon->maximum_order_amount,
                    'coupon_redeem_on' => $coupon->redeem_on,
                    'coupon_store_id' => $coupon->store_id,
                    'coupon_department_id' => $coupon->department_id,
                    'coupon_shelf_id' => $coupon->shelf_id,
                    'coupon_payment_method' => $coupon->payment_method,
                    'coupon_product_id' => $coupon->store_product_id,
                    'credit_amount' => $credit['amount'],
                ],
            ];
        } else {
            if ($user) {
                if ($order = Order::where('user_id', $user->id)->first()) {
                    $this->response['message'] = 'No puedes agregar un código referido por que ya hiciste tu primer pedido.';
                    return $this->jsonResponse();
                }
                if (!empty($user->referred_by)) {
                    $this->response['message'] = 'Ya redimiste un código de referido.';
                    return $this->jsonResponse();
                }
            }
            if ($referrer->total_referrals >= Config::get('app.referred.limit')) {
                $this->response['message'] = 'El código referido que ingresaste no es valido por que ha alcanzado el limite permitido de referidos por cuenta.';
                return $this->jsonResponse();
            }
            if (!$this->isNewDevice() && ($this->user_agent == 'Android' || ($this->user_agent == 'iOS' && version_compare($this->app_version,
                            '2.1.17', '>=')))) {
                $this->response['message'] = 'No se pudo validar el código de referido por incumplimiento de nuestros términos y condiciones.';
                return $this->jsonResponse();
            }
            //validar antes de procesar compra
            if (Input::has('validate')) {
                $this->response = [
                    'status' => true,
                    'message' => 'Código de referido valido',
                ];
                return $this->jsonResponse();
            }

            $message = 'Tienes ' . currency_format(Config::get('app.referred.amount')) . ' de crédito, para usarlos el subtotal de tu pedido debe ser mínimo de ' . currency_format(Config::get('app.minimum_discount_amount')) . '.';

            $this->response = [
                'status' => true,
                'message' => $message,
                'result' => [
                    'type' => 'referred',
                    'credit_amount' => Config::get('app.referred.amount'),
                    'minimum_for_discount' => Config::get('app.minimum_discount_amount'),
                ],
            ];
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene informacion del carrito actual
     *
     * @param int $action Accion a ejecutar
     * @param int $product_id ID product
     *
     * @return bool|mixed
     */
    protected function currentCart($action, $store_product_id = null)
    {
        if (Input::has('cart')) {
            $products = json_decode(Input::get('cart'));
            if (count($products)) {
                foreach ($products as $id => $data) {
                    if ($store_product = StoreProduct::find($id)) {
                        $store_product->cart_price = $data->price;
                        $store_product->cart_quantity = $data->qty;
                        $cart_products[$id] = $store_product;
                    }
                }
                if (!isset($cart_products)) {
                    return false;
                }

                //validar si el producto esta agregado en el carrito actual
                if ($action == 'productIsInCart') {
                    if (array_key_exists($store_product_id, $cart_products)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                //obtener productos en carrito
                if ($action == 'getProducts') {
                    return $cart_products;
                }

                $has_special_price = false;
                $store_id = 0;
                $cart = [
                    'total_amount' => 0,
                    'delivery_amount' => 0,
                    'total_quantity' => 0,
                ];

                foreach ($cart_products as $cart_product) {
                    $store_id = !$cart_product->id ? $cart_product->cart_store_id : $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id])) {
                        $store_id = $cart_product->store_id;
                        $store = Store::find($store_id)->toArray();
                        $store['sub_total'] = 0;
                        $cart['stores'][$store_id] = $store;
                        $cart['stores'][$store_id]['products'] = [];
                        $cart['stores'][$store_id]['delivery_amount'] = 0;
                        $cart['stores'][$store_id]['discount_amount'] = 0;
                    }

                    $store_product = json_decode(json_encode($cart_product), true);
                    if ($store_product['special_price'] > -1) {
                        if ($store_product['cart_price'] < $store_product['price']) {
                            $has_special_price = true;
                        }
                    }
                    $store_product['sub_total'] = $store_product['cart_quantity'] * $store_product['cart_price'];
                    $cart['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
                    $cart['total_amount'] += $store_product['sub_total'];
                    $cart['total_quantity']++;
                    array_push($cart['stores'][$store_id]['products'], $store_product);
                }

                //obtener total del carrito
                if ($action == 'getTotal') {
                    return $cart['total_amount'] + $cart['delivery_amount'];
                }
                //varificar si tiene precio especial
                if ($action == 'hasSpecialprice') {
                    return $has_special_price;
                }

            }
        }

        return false;
    }

    /**
     * Valida productos en carrito con base en ubicacion del cliente
     *
     * @return mixed
     */
    public function validate_coordinates()
    {
        $validator = \Validator::make(\Input::all(), [
            'warehouse_id' => 'sometimes|exists:warehouses,id',
            'lat' => 'required',
            'lng' => 'required',
            'cart' => 'string',
        ]);

        if ($validator->fails()) {
            return $this->jsonResponse('Error en campos requeridos.', 400);
        }

        $current_zone_id = intval(\Input::get('zone_id'));
        $latitude = \Input::get('lat');
        $longitude = \Input::get('lng');
        $cart_decoded = json_decode(\Input::get('cart'));

        if (!empty($cart_decoded) && !($cart_decoded instanceof \StdClass)) {
            $this->response = [
                'status' => false,
                'message' => \Config::get('debug')
                    ? 'El carrito debe ser un objeto.'
                    : 'Tenemos problemas con tu carrito, intenta reiniciar la sesión.',
            ];
            return $this->jsonResponse();
        }

        try {
            // Se debe obtener la bodega asociada a las coordenadas, para no validar en cada
            // zona se obtienen las zonas de la tienda que tiene cobertura en las coordenadas.
            $store = Store::getByLatlng($latitude, $longitude);
            $zone = Zone::getByLatLng($store->city_id, $latitude, $longitude);
        } catch (CoverageException $exception) {
            $this->response = [
                'status' => false,
                'message' => 'La dirección que ingresaste se encuentra fuera de nuestra cobertura.',
            ];
            return $this->jsonResponse();
        }

        $current_zone = Zone::find($current_zone_id) ?: new Zone();

        if ($current_zone->warehouse_id === $zone->warehouse_id || empty($cart_decoded)) {
            $this->response = [
                'status' => true,
                'message' => 'Carrito actualizado.',
                'result' => [
                    'removed_products' => [],
                    'zone_id' => $zone->id,
                ],
            ];
            return $this->jsonResponse();
        }

        $zone->load('warehouse');
        $warehouse = $zone->warehouse;
        $removed_products = Cart::sanitizeByWarehouse($warehouse, $cart_decoded);
        $this->response = [
            'status' => true,
            'message' => empty($removed_products)
                ? ''
                : 'Al cambiar de ubicación, tu carrito se ha modificado. Los siguientes productos no están disponibles por el momento.',
            'result' => [
                'removed_products' => $removed_products,
                'zone_id' => $zone->id,
            ],
        ];

        return $this->jsonResponse();
    }

    /**
     * @return array
     */
    protected function getDefaultDeliveryDates()
    {
        return [
            Carbon::create()->hour(20),
            Carbon::create()->addDays(1)->hour(18),
        ];
    }

    /**
     * @return null|UserAddress
     */
    private function getCurrentAddress()
    {
        $user_address = null;
        $address_id = \Input::get('address_id');
        $latitude = \Input::get('latitude');
        $longitude = \Input::get('longitude');

        if (!empty($address_id)) {
            $user_address = UserAddress::find($address_id);
        }

        if (empty($user_address) && !empty($latitude) && !empty($longitude)) {
            $user_address = new UserAddress();
            $user_address->latitude = $latitude;
            $user_address->longitude = $longitude;
        }

        return $user_address;
    }

    /**
     * @param User $user
     */
    public function validatePhoneNumber(User $user)
    {
        $is_ios = $this->user_agent === 'iOS';
        $valid_android = !$is_ios && version_compare($this->app_version, '2.1.21', '>=');
        $valid_ios = $is_ios && version_compare($this->app_version, '2.1.20', '>=');
        if ($valid_android || $valid_ios) {
            $user->phone_validated_date = Carbon::create();
            $user->save();
        }
    }

    /**
     * @param $tag
     * @return |null
     */
    private function getTagFormat($tag)
    {
        if (!isset($tag)) {
            return null;
        }

        $tag->last_updated = strtotime($tag->last_updated);

        return $tag;
    }
}
