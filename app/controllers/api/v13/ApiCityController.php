<?php


namespace api\v13;

use app\repositories\contracts\CityRepositoryInterface;
use usecases\contracts\cities\GetUsecaseInterface as CitiesGetUsecaseInterface;

/**
 * Class ApiCityController
 * @package api\v13
 */
class ApiCityController extends \ApiController
{
    /**
     * @var CitiesGetUsecaseInterface
     */
    private $getUsecase;


    /**
     * ApiCityController constructor.
     * @param CitiesGetUsecaseInterface $getUsecase
     */
    public function __construct(CitiesGetUsecaseInterface $getUsecase)
    {
        parent::__construct();

        $this->getUsecase = $getUsecase;
    }


    /**
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function getAllAction()
    {

        $result['cities'] = $this->getUsecase->allActives();

        $this->response = [
            'status' => true,
            'result' => $result,
            'message' => 'Ciudades Obtenidas'
        ];

        return $this->jsonResponse();
    }

    /**
     * La ciudad con identificador 1 y la ciudad con identificador 2, siempre seran las ciudades pricipales;
     * El modelo de datos actual no permite categorizar las ciudades. Este metodo se debe replantear. Se realiza
     * acorde al requerimiento.
     * @return mixed
     */
    public function getMainAndNeighborhoodsAction()
    {
        $result['cities'] = $this->getUsecase->mainAndNeighborhoodsActives();

        $this->response = [
            'status' => true,
            'result' => $result,
            'message' => 'Ciudades Obtenidas'
        ];

        return $this->jsonResponse();
    }

}