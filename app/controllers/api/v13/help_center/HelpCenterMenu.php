<?php

namespace api\v13\help_center;

/**
 * Class HelpCenterMenu
 * @package api\v13\help_center
 */
class HelpCenterMenu
{
    protected $validator;

    /**
     * HelpCenterMenu constructor.
     * @param $validator
     */
    public function __construct($validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $hide
     * @return array
     */
    public function createMenu(array $hide = [], $statusVersion = true)
    {
        $menu = [
            [
                'id' => 1,
                'text' => 'El pedido no ha llegado',
                'validation_method' => ['canShowDeliveryIsLate'],
            ],
            [
                'id' => 2,
                'text' => 'Cambiar el horario de entrega',
                'validation_method' => ['canChangeTime'],
            ],
            [
                'id' => 3,
                'text' => 'Cambiar el método de pago',
                'validation_method' => ['canChangePaymentMethod'],
            ],
            [
                'id' => 8,
                'text' => 'Agregar o quitar productos',
                'validation_method' => ['canAddProducts'],
            ],
            [
                'id' => 4,
                'text' => 'Ingresar un cupón',
                'validation_method' => ['canAddCoupon', 'canAddReferenceCode'],
            ],
            [
                'id' => 5,
                'text' => 'Cambiar datos de facturación',
                'validation_method' => ['canAssociateCompanyOnBill'],
            ],
            [
                'id' => 6,
                'text' => 'Cancelar el pedido',
                'validation_method' => ['canBeCancelled'],
            ],
            [
                'id' => 9,
                'text' => 'Chatear con un asesor',
                'validation_method' => [],
            ],
            [
                'id' => 7,
                'text' => 'Tengo una solicitud diferente',
                'validation_method' => [],
            ],
        ];

        return ($statusVersion) ?
            $this->validator->validateAndCreateMenuNewVersion($menu):
            $this->validator->validateAndCreateMenuVersionPrevious($menu, $hide);
    }
}
