<?php

namespace api\v13\help_center;

use Carbon\Carbon;
use delivery_times\DeliveryTimeConverter;
use exceptions\CouponException;
use exceptions\MerqueoException;
use exceptions\ReferenceCodeException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Order;
use orders\CancelOrder;
use orders\OrderBuilder;
use orders\OrderProductUpdating;
use orders\OrderProductValidator;
use orders\OrderStatus;
use orders\OrderValidator;
use orders\RescheduleOrder;
use carts\HelpCenterCartAdapter;
use usecases\Interfaces\orders\CalculateDeliveryWindowAmountInterface;
use repositories\contracts\CountryRepositoryInterface;
use GuzzleHttp\Client;
use UserCredit;
use repositories\contracts\OrderPaymentRepositoryInterface;

/**
 * Class ApiOrderController
 * @package api\help_center\v13
 */
class ApiOrderController extends \ApiController
{
    const ORDER_LATE = 'order_late';
    const RESCHEDULE = 'reschedule';
    const CANCEL_REASONS = 'cancel_reasons';
    const CANCEL_DELIVERY = 'cancel_delivery';
    const ADD_COUPON_CODE = 'add_code';
    const CHANGE_BILL = 'change_bill';
    const CHANGE_PAYMENT_METHOD = 'change_payment_method';
    const LIST_PAYMENT_METHOD = 'list_payment_methods';
    const LIST_ORDER_PRODUCTS = 'list_order_products';
    const ALTER_ORDER = 'alter_order';
    const PREVIEW_ORDER = 'preview_order';

    /**
     *  metodos de configuracion para los apps
     */
    protected $configValidateIos = [
        'version' => '2.2.8',
        'itemsHiden' => ['Chatear con un asesor']
    ];
    protected $configValidateAndroid = [
        'version' => '2.1.48',
        'itemsHiden' => ['Chatear con un asesor']
    ];

    /**
     * @var array
     */
    protected $additional_orders;

    /**
     * ApiOrderController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        \Event::subscribe(new \OrderEventHandler());
        $this->additional_orders = [];
        $this->has_new_products = false;
    }

    /**
     * Items principales del help center.
     *
     * @param $order_id
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_menu_values($order_id)
    {
        $order = \Order::findOrFail($order_id);
        $menu = new HelpCenterMenu(new OrderValidator($order));

        // validamos el agent del la orden
        if ($this->user_agent == 'iOS') {
            // validamos las versiones en Ios
            $responseMenu = (version_compare($this->app_version, $this->configValidateIos['version'], '<')) ?
                $menu->createMenu($this->configValidateIos['itemsHiden'], false) :
                $menu->createMenu();
        }else{
            // validamos las versiones en Android
            $responseMenu = (version_compare($this->app_version, $this->configValidateAndroid['version'], '<')) ?
                $menu->createMenu($this->configValidateAndroid['itemsHiden'], false) :
                $menu->createMenu();
        }

        $this->response = [
            'status' => true,
            'message' => 'Ok',
            'result' => [
                'menu' => $responseMenu,
                'reference' => $order->reference
            ],
        ];

        return $this->jsonResponse();
    }

    /**
     * @param $order_id
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function manage_order($order_id)
    {
        if ($result = $this->validateAction([self::CANCEL_REASONS])) {
            return $result;
        }

        $action = \Input::get('action');
        try {
            $order = \Order::findOrFail($order_id);
        } catch (ModelNotFoundException $exception) {
            return $this->jsonResponse($exception->getMessage(), 404);
        }

        if ($action === self::CANCEL_REASONS) {
            return $this->cancel_delivery_reasons($order);
        }
    }

    /**
     * @param $user_id
     * @param $order_id
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function manage_user_orders($user_id, $order_id)
    {
        if ($result = $this->validateAction([
            self::CANCEL_DELIVERY,
            self::ADD_COUPON_CODE,
            self::CHANGE_BILL,
            self::ORDER_LATE,
            self::RESCHEDULE,
            self::CHANGE_PAYMENT_METHOD,
            self::LIST_PAYMENT_METHOD,
            self::LIST_ORDER_PRODUCTS,
            self::PREVIEW_ORDER,
            self::ALTER_ORDER,
        ])) {
            return $result;
        }

        $order = \Order::where('user_id', $user_id)->findOrFail($order_id);
        $action = \Input::get('action');

        switch ($action) {
            case self::CANCEL_DELIVERY:
                return $this->cancel_delivery($order, \RejectReason::findOrFail(\Input::get('reason_id')));
            case self::ADD_COUPON_CODE:
                return $this->add_coupon($order, $order->user, \Input::get('code'));
            case self::CHANGE_BILL:
                return $this->assign_bill_to_company($order);
            case self::ORDER_LATE:
                return $this->get_late_order_messages($order);
            case self::LIST_PAYMENT_METHOD:
                return $this->get_payment_methods_data($order);
            case self::CHANGE_PAYMENT_METHOD:
                return $this->change_payment_method($order);
            case self::LIST_ORDER_PRODUCTS:
                return $this->list_order_products($order);
            case self::PREVIEW_ORDER:
            case self::ALTER_ORDER:
                return $this->updateOrder($order, \Input::get('cart'), $action === self::PREVIEW_ORDER);
            case self::RESCHEDULE:
                return $this->reschedule(
                    $order,
                    \Input::get('delivery_date'),
                    \Input::get('delivery_time'),
                    \Input::get('reason')
                );
        }
    }

    /**
     * @param \Order $order
     * @param array $cart
     * @param $is_mock
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    private function updateOrder(\Order $order, array $cart, $is_mock)
    {
        $calculateDeliveryWindowAmount = app()->make(CalculateDeliveryWindowAmountInterface::class);
        try {
            \DB::beginTransaction();
            $order_validator = new OrderValidator($order);
            $order_validator->canAddProducts();
            $order->load('orderGroup.warehouse');
            $converter = new HelpCenterCartAdapter($cart);
            $product_validator = new OrderProductValidator($order);
            $order_builder = new OrderBuilder($order);
            $updater = new OrderProductUpdating($order, $product_validator, $order_builder);

            $carts = $converter->convert($order->orderGroup->warehouse);

            foreach ($carts as $cart) {
                $updater->addFromCart($cart);
            }
            $calculateDeliveryWindowAmount = $calculateDeliveryWindowAmount->handle($order);
        
            $this->updateOrderAmount(
                $order,
                $calculateDeliveryWindowAmount['delivery_amount']
            );

            $this->response = [
                'status' => true,
                'result' => $calculateDeliveryWindowAmount
            ];

            if ($updater->hasNewProducts()) {
                $this->response['result']['title'] = 'Recuerda que algunos productos agregados podrán ' .
                    'añadirse a otro pedido sin ningún costo adicional.';
            }

            if ($is_mock) {
                \DB::rollback();
            } else {
                $order->validateCommittedStock();
                \DB::commit();
            }

        } catch (MerqueoException $exception) {
            \DB::rollback();

            return $this->jsonResponse($exception->getMessage(), 400);
        }

        return $this->jsonResponse(
            $is_mock
                ? ($order->orderGroup->isDirty('total_amount')
                ? '¡El valor de tu pedido cambiará!'
                : 'No hay cambios en tu pedido')
                : '¡Pedido modificado exitosamente!'
        );
    }

    /**
     * @param \Order $order
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    private function list_order_products(\Order $order)
    {
        $products = $order->orderProducts()
            ->select(
                \DB::raw('store_product_id as id'),
                'quantity',
                \DB::raw('product_name as name'),
                \DB::raw('CONCAT_WS(" ", product_quantity, product_unit) as unit'),
                \DB::raw('product_image_url as image_url'),
                'price',
                'original_price',
                \DB::raw('quantity_special_price_stock'),
                \DB::raw('IF(price < original_price, TRUE, FALSE) as has_special_price')
            )
            ->whereNull('sampling_id')
            ->where('is_gift', 0)
            ->get()
            ->each(function ($product) {
                $product->has_special_price = (bool)$product->has_special_price;
                $pum_special_price = ($product->has_special_price ? 'store_products.special_price AS pum_special_price' : 'store_products.price AS pum_special_price');
                //Obtener pum de cada producto de la orden.
                $store_product = \StoreProduct::select(
                    'products.*',
                    'store_products.*',
                    $pum_special_price
                )
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->pum()
                    ->where('store_products.id', $product->id)
                    ->first();
                $product->pum = (isset($store_product->pum) ? $store_product->pum : NULL);

                return $product;
            });

        $this->response = [
            'status' => true,
            'result' => $products->toArray(),
        ];

        return $this->jsonResponse('Ok');
    }

    /**
     * @param \Order $order
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function cancel_delivery_reasons(\Order $order)
    {
        $status = new OrderValidator($order);

        try {
            $status->canBeCancelled();
            $this->response['result'] = [
                'title' => 'Nos gustaria conocer un poco más sobre tu decisión.',
                'reasons' => \RejectReason::select('id', 'status', 'reason')
                    ->helpCenter()
                    ->get()
                    ->toArray(),
            ];

        } catch (MerqueoException  $exception) {
            return $this->jsonResponse($exception->getMessage(), 400);
        }

        return $this->jsonResponse('Ok');
    }

    /**
     * @param \Order $order
     * @param \RejectReason $reason
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function cancel_delivery(\Order $order, \RejectReason $reason)
    {
        $order_status = new OrderValidator($order);

        try {
            \DB::beginTransaction();
            $order_status->canBeCancelled();
            $cancellation = new CancelOrder($order);
            $cancellation->manage($reason);

            // Se borran los registros de los cupones redimidos en la orden que se cancela
            if ($reason->attendant !== 'Políticas Merqueo') {
                UserCredit::removeRedeemedCoupons($order);
            }

            $order->validateCommittedStock();
            // Se verifica si existe autorización de cobro para MX y se reversa
            $this->cancelAuthorize($order);

            \DB::commit();
            \Order::getStatusFlow($order->id, $order->user_id, true);
            \Event::fire('order.canceled', [$order, null, $order->user]);
        } catch (MerqueoException $exception) {
            \DB::rollback();
            return $this->jsonResponse($exception->getMessage(), 400);
        }

        return $this->jsonResponse('¡Cancelación exitosa!');
    }

    /**
     * @param \Order $order
     * @param \User $user
     * @param string $referral_code
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function add_coupon(\Order $order, \User $user, $referral_code)
    {
        $coupon = \Coupon::where('code', $referral_code)->first();

        try {
            $status = new OrderValidator($order);
            \DB::beginTransaction();
            $order->removeVisaDiscount();

            if (empty($coupon)) {
                $reason = 'referido';
                $status->canAddReferenceCode();
                $referrer = $user->validateReferrerCode($referral_code, $this->isNewDevice());
                $credit = \UserCredit::addFromReferral($user, $referrer);
                $amount = $credit->amount;

                if ($credit->amount > $order->finalAmount) {
                    $credit->amount = $order->finalAmount;
                    $credit->save();
                }

                $status->canAddDiscount($credit->amount);

                $order->discount_amount += $credit->amount;
                $order->save();

                \UserCredit::removeCredit($user, $credit->amount, 'order', $order);

                $user->referred_by = $referrer->id;
                $user->save();
            } else {
                $reason = 'cupón';
                $status->canAddCoupon($order->total_amount);
                $coupon->addToOrder($order);
                $amount = $coupon->amount;
            }

            \Event::fire('order.credit_added', [$order, $amount, $reason, null, $order->user]);
            \DB::commit();
        } catch (MerqueoException $exception) {
            // El rollback re realiza en el método "jsonResponse"
            return $this->jsonResponse($exception->getMessage(), 400);
        } catch (ReferenceCodeException $exception) {
            return $this->jsonResponse($exception->getMessage(), 200);
        } catch (CouponException $exception) {
            return $this->jsonResponse($exception->getMessage(), 200);
        }

        $this->response['status'] = true;
        return $this->jsonResponse(
            empty($coupon) ? '¡Ingreso de código exitoso!' : '¡Ingreso de cupón exitoso!',
            200
        );
    }

    /**
     * Asigna al pedido los datos de una compañia.
     *
     * @param \Order $order
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function assign_bill_to_company(\Order $order)
    {
        $state = new OrderValidator($order);
        try {
            $state->canAssociateCompanyOnBill();
            $order->assignBillToCompany(
                \Input::get('invoice_company_nit'),
                \Input::get('invoice_company_name')
            );
            \Event::fire('order.business_assigned', [$order, null, $order->user]);
            $order->save();
        } catch (MerqueoException $exception) {
            $this->response = ['status' => false];
            return $this->jsonResponse($exception->getMessage(), 400);
        }

        $this->response['status'] = true;
        return $this->jsonResponse('¡Cambio exitoso!');
    }

    /**
     * @param \Order $order
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_payment_methods_data(\Order $order)
    {
        try {
            $status = new OrderValidator($order);
            $status->canChangePaymentMethod();
        } catch (MerqueoException $exception) {
            return $this->jsonResponse($exception->getMessage(), 400);
        }

        $payment_methods = array_values($this->config['payment_methods']);
        unset($payment_methods[array_search('Débito - PSE', $payment_methods)]);

        $can_choose_credit_cards = in_array($order->status, [OrderStatus::VALIDATION, OrderStatus::INITIATED]);
        $current_payment_method = $order->payment_method;
        $credit_cards = $can_choose_credit_cards
            ? $order->user->creditCards()
                ->select('id', 'user_id', 'last_four', 'type')
                ->withoutCodensaCards($order)
                ->get()
            : [];

        if ($order->payment_method === 'Tarjeta de crédito') {
            foreach ($credit_cards as $credit_card) {
                $credit_card->was_selected = $credit_card->id == $order->credit_card_id;
                if ($credit_card->was_selected) {
                    $credit_card->installments = $order->cc_installments;
                }
            }
        }

        if (!$can_choose_credit_cards) {
            array_shift($payment_methods);
        }

        $this->response = [
            'result' => compact('payment_methods', 'credit_cards', 'current_payment_method'),
            'status' => true,
            'message' => 'Ok'
        ];

        return $this->jsonResponse();
    }

    /**
     * @param \Order $order
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    private function change_payment_method(\Order $order)
    {
        $status = new OrderValidator($order);
        $credit_card_id = \Input::get('credit_card_id');
        $credit_card_payment_method = 'Tarjeta de crédito';

        try {
            \DB::beginTransaction();
            $status->canChangePaymentMethod($credit_card_id ? $credit_card_payment_method : \Input::get('payment_method'));
            if (!empty($credit_card_id)) {
                $order->load('orderGroup');

                $result = $order->validateOrderFraud([
                    'payment_method' => $credit_card_payment_method,
                    'address_id' => $order->orderGroup->address_id,
                    'user_id' => $order->user_id,
                    'credit_card_id' => $credit_card_id,
                ]);
                $order->status = $result['validation'] || $order->type == 'Marketplace' ? 'Validation' : 'Initiated';
                $order->posible_fraud = empty($result['posible_fraud']) ? 0 : $result['posible_fraud'];
                $order->save();

                $user_credit_card = \UserCreditCard::findOrFail($credit_card_id);
                $installments = \Input::get('installments');
                $order->changePaymentMethod($credit_card_payment_method);
                $order->assignCreditCard($user_credit_card, $installments);
            } else {
                // Limpia los datos de la tajeta de credito.
                $order->status = $order->status === OrderStatus::VALIDATION
                    ? OrderStatus::INITIATED
                    : $order->status;
                $order->assignCreditCard(new \UserCreditCard(), null);
                $selected_method = \Input::get('payment_method');
                $order->changePaymentMethod($selected_method);
            }

            \Event::fire('order.payment_method_changed', [$order, null, $order->user]);
            $order->save();
            \DB::commit();
            $this->response = ['status' => true];
        } catch (MerqueoException $exception) {
            \DB::rollback();
            return $this->jsonResponse($exception->getMessage(), 400);
        }

        return $this->jsonResponse('¡Cambio de método de pago exitoso!');
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_delivery_time_change_reasons()
    {
        $this->response = [
            'status' => true,
            'message' => 'Selecciona el motivo de tu cambio y cuéntanos ' .
                'en qué horario te gustaría que te entregaramos el pedido.',
            'result' => [
                'Elegí un horario incorrecto',
                'No hay quien reciba el pedido',
                'Otro',
            ]
        ];

        return $this->jsonResponse();
    }

    /**
     * @param $user_id
     * @param $order_id
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_delivery_time_data($user_id, $order_id)
    {
        $order = \Order::with('user', 'orderGroup', 'store', 'alliedStore')
            ->where('user_id', $user_id)
            ->findOrFail($order_id);

        $reschedule = new RescheduleOrder($order);
        $slots = $reschedule->getDeliveryDates();
        $has_results = !empty($slots['days']);

        $morning = Carbon::now()->addDay()->format('Y-m-d');

        if (\Routes::where('planning_date', $morning)->count() > 0) {
            unset($slots['days'][$morning]);
            unset($slots['time'][$morning]);
        }

        $this->response = [
            'status' => $has_results,
            'result' => new DeliveryTimeConverter($slots),
        ];

        return $this->jsonResponse(
            $has_results ? 'Ok' : 'No hay horarios disponibles para reprogramar el pedido',
            $has_results ? 200 : 400
        );
    }

    /**
     * @param \Order $order
     * @param $delivery_date
     * @param $delivery_time
     * @param $reason
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function reschedule(\Order $order, $delivery_date, $delivery_time, $reason)
    {
        $status = new OrderValidator($order);
        try {
            $status->canChangeTime();
            $schedule = new RescheduleOrder($order);
            $schedule->make($delivery_date, $delivery_time, $reason);
        } catch (MerqueoException $exception) {
            $this->response['status'] = false;
            return $this->jsonResponse($exception->getMessage(), 400);
        }

        $this->response['status'] = true;
        return $this->jsonResponse('¡Cambio de hora de entrega exitoso!');
    }

    /**
     * @param \Order $order
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_late_order_messages(\Order $order)
    {
        $driver = $order->driver()
            ->select('first_name', 'last_name')
            ->first();

        $order_group = $order->orderGroup()
            ->select('user_address_latitude', 'user_address_longitude')
            ->first();

        $delivery_date = new Carbon($order->delivery_date);
        $delivery_date->addMinute(Carbon::MINUTES_PER_HOUR / 2);
        $is_delivery_late = $delivery_date < Carbon::create();
        $time = explode(' ', $order->delivery_date)[0];
        $title = $is_delivery_late
            ? 'Sabemos que tu pedido se encuentra retrasado, estamos haciendo todo lo posible para llegar pronto.'
            : '¡No te preocupes! Vamos en camino con tu pedido.';

        $this->response = [
            'status' => true,
            'message' => $title,
            'result' => [
                'order' => [
                    'id' => $order->id,
                    'status' => $order->status,
                    'status_name' => $this->config['order_statuses'][$order->status],
                    'delivery_date' => format_date('normal_long', $time),
                    'delivery_time' => $order->delivery_time,
                    'total_amount' => $order->total_amount,
                    'reference' => $order->reference,
                    'driver' => $driver,
                    'order_group' => $order_group,
                ],
                'reschedule' => false,
            ]
        ];

        return $this->jsonResponse();
    }

    /**
     * @param $order
     * @param $deliveryWindowAmount
     */
    private function updateOrderAmount($order, $deliveryWindowAmount)
    {
        $order->delivery_amount = $deliveryWindowAmount;
        $order->save();
    }

    private function cancelAuthorize($order)
    {
        $countryRepository = app()->make(CountryRepositoryInterface::class);
        $orderPayment = app()->make(OrderPaymentRepositoryInterface::class);
        $country = $countryRepository->findByStoreId($order->store->id);

        // Check if method payment is credit card
        $methodPaymentNotIsCreditCard = $order->payment_method != Order::PAYMENT_METHOD_CC;

        // Check if the action of authorization is inactive
        $authorizationIsDisabled = $country->is_precharge_enable == 1;

        // Check any approved authorization's transaction of an order
        $authorization = $orderPayment->findAuthorizationByOrderId($order->id);

        if ($methodPaymentNotIsCreditCard || $authorizationIsDisabled || $authorization->isEmpty()) {
            return;
        }

        try {
            $client = new Client(['base_uri' => Config::get('app.payment.url')]);
            $res = $client->post('payment/v1/credit-card/cancel-authorize', [
             'json' => ['orderId' => $order->id],
             'headers' => [ 'Content-Type' => 'application/json', 'Accept' => 'application/json' ]
            ]);
            $dataResponse = json_decode($res->getBody()->getContents());

            if (isset($dataResponse->status) && $dataResponse->status == 0) {
                throw new MerqueoException($dataResponse->message, 400);
            }
        } catch (RequestException $e) {
            $msg = 'Error al reversar la autorización de la compra';

            if ($e->hasResponse()) {
                $dataResponse = json_decode($e->getResponse()->getBody()->getContents());
                $msg = isset($dataResponse->message) ? $dataResponse->message : $msg;
            }

            throw new MerqueoException($msg, 400);
        }
        // otherwise do the refund
        // default return nothing
    }
}
