<?php

namespace api\v13;

use exceptions\CoverageException;
use Input;
use repositories\Eloquent\EloquentTagRepository;
use repositories\Interfaces\OrderRepositoryInterface;
use Route;
use Request;
use DB;
use Store;
use Department;
use Shelf;
use Product;
use Order;
use SearchLog;
use Banner;
use StoreProductWarehouse;
use usecases\Products\ValidatePrices\Interfaces\ValidateRealPublicPriceUseCaseInterface;
use UserAddress;
use Zone;
use StoreLocation;
use Config;
use Carbon\Carbon;
use Response;
use StoreProduct;
use BannerStoreProduct;
use BannerDepartment;
use User;
use AWSElasticsearch;


/**
 * Class ApiStoreController
 * @package api\v13
 * @property \Warehouse $warehouse
 */
class ApiStoreController extends \ApiController
{
    const MINIMUM_ACTIVE_PRODUCTS = 4;
    /**
     * @var Carbon|null
     */
    private $lastOrderDate = null;

    /**
     * @var int
     */
    private $hasOrders = 0;

    /**
     * @var mixed
     */
    private $specialPriceFields;

    private static $SORT_LAST = 1000;

    /**
     * The parameters convert to float.
     *
     * @var array
     */
    private $parametersCast = ["price","special_price","delivery_discount_amount"];

    /**
     * @var EloquentTagRepository
     */
    private $eloquentTagRepository;

    /**
     * @var ValidateRealPublicPriceUseCaseInterface
     */
    private $validateRealPublicPriceUseCase;

    /**
     * @var array
     */
    private $orders = [];

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * ApiStoreController constructor.
     * @param EloquentTagRepository $eloquentTagRepository
     * @param ValidateRealPublicPriceUseCaseInterface $validateRealPublicPriceUseCase
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        EloquentTagRepository $eloquentTagRepository,
        ValidateRealPublicPriceUseCaseInterface $validateRealPublicPriceUseCase,
        OrderRepositoryInterface $orderRepository
    ) {
        parent::__construct();

        $userId = Route::current()->parameter('user_id')
            ? Route::current()->parameter('user_id')
            : (Input::has('user_id') ? Input::get('user_id') : null);

        $storeId = Route::current()->parameter('store_id') ?: \Input::get('store_id');
        $this->warehouse = $this->getCurrentWarehouse($storeId);

        // Validacion precio para primera compra
        if ($userId) {
            $userOrdersInfo = User::getOrdersInfo($userId);
            $this->hasOrders = $userOrdersInfo->qty;
            $this->lastOrderDate = $userOrdersInfo->last_order_date;
        }

        $this->specialPriceFields = StoreProduct::getRawSpecialPriceQuery($this->hasOrders);
        $this->eloquentTagRepository = $eloquentTagRepository;
        $this->validateRealPublicPriceUseCase = $validateRealPublicPriceUseCase;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Obtiene tienda que atiende en una ubicación de latitud y longitud
     *
     * @return array $response Respuesta
     */
    public function get_store()
    {
        $is_new_device = empty($this->device_id) ?: $this->isNewDevice();
        $user_id = Input::get('user_id');
        $address = Input::get('address');
        $lat = null;
        $lng = null;
        $neighborhood = null;
        $locality = null;
        $store = null;
        $zone = null;

        if (Input::has('lat') && Input::has('lng') && empty($address)) {
            $lat = Input::get('lat');
            $lng = Input::get('lng');
        } else {
            $city = Input::get('city');
            $address = urldecode($address);

            if (!empty($address) && !empty($city)) {
                $get_data = Input::all();
                // Obtener latitud longitud
                $request = Request::create('/api/location', 'GET', ['address' => $address, 'city' => $city]);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if ($result['status']) {
                    $lat = $result['result']['latitude'];
                    $lng = $result['result']['longitude'];
                    $address = $result['result']['address'];
                    $neighborhood = $result['result']['neighborhood'];
                    $locality = $result['result']['locality'];

                    // Validar direccion en el sur de la ciudad
                    $this->response = UserAddress::validateAddress($result, $get_data);
                    if ($this->response['status']) {
                        return $this->jsonResponse();
                    }
                } else {
                    $this->response = [
                        'message' => 'En este momento no tenemos cobertura en tu dirección',
                        'detail' => 'Por favor ingresa otra dirección',
                        'status' => false,
                        'found' => false,
                        'result' => empty($result) ? new \StdClass() : $result,

                    ];

                    return $this->jsonResponse();
                }

            }
        }

        $result = [];
        $found = true;

        // Obtener direccion
        if (!isset($address)) {
            $address = null;
            $neighborhood = null;
            $locality = null;
        }

        $address_obj = [
            'address' => trim($address),
            'lat' => strval($lat),
            'lng' => strval($lng),
            'neighborhood' => isset($neighborhood) ? $neighborhood : null,
            'locality' => isset($locality) ? $locality : null,
        ];

        $result['address'] = $address_obj;

        if (!empty($lat) && !empty($lng)) {
            try {
                $store = Store::getByLatlng($lat, $lng);
                $zone = Zone::getByLatLng($store->city_id, $lat, $lng);
            } catch (CoverageException $exception) {
                $this->response = [
                    'message' => 'En este momento no tenemos cobertura en tu dirección',
                    'detail' => 'Por favor ingresa otra dirección',
                    'status' => false,
                    'found' => false,
                    'result' => empty($result) ? new \StdClass() : $result,
                ];
                return $this->jsonResponse();
            }
        } elseif (Input::get('store_id')) {
            $store = Store::find(\Input::get('store_id'));
            $zone = $this->getCurrentZone($store->id);
        } else {
            $this->response = ['status' => 'false'];

            return $this->jsonResponse('El campo "store_id" es obligatorio');
        }

        $status = true;
        $message = 'Se encontró una tienda dentro de la ubicación';
        $detail = '';
        $store->zone_id = $zone->id;
        $store->warehouse_id = $zone->warehouse_id;
        $warehouse_id = $zone->warehouse_id;


        // Se construye un arreglo asociativo que contenga los mismos
        // elementos de la tienda expuestos en el versiones anteriores del API.
        $fields = [
            'id',
            'name',
            'city_id',
            'warehouse_id',
            'minimum_order_amount',
            'zone_id',
        ];

        foreach ($fields as $field) {
            $result['store'][$field] = $store->$field;
        }

        $result['store']['delivery_order_amount'] = 0;
        $result['store']['minimum_order_amount'] = ($zone->type == 'Darksupermarket') ?  floatval($store->minimun_order_amount_express) : floatval($store->minimum_order_amount);

        $departments = Department::select(
            'departments.id',
            'departments.name',
            'departments.store_id',
            DB::raw('COUNT(store_products.id) AS products_qty'),
            DB::raw('IF(ISNULL(departments.image_list_app_url_temp) OR departments.image_list_app_url_temp = "", departments.image_list_app_url, departments.image_list_app_url_temp) AS image_list_app_url')
        )
            ->join('shelves', 'departments.id', '=', 'shelves.department_id')
            ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
            ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join(
                'store_product_warehouses',
                'store_products.id',
                '=',
                'store_product_warehouses.store_product_id'
            )
            ->where('departments.store_id', $store->id)
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('departments.status', 1)
            ->where('departments.type', Department::NORMAL)
            ->where('shelves.status', 1)
            ->where('products.type', '<>', 'Proveedor')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1,
            IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->groupBy('departments.id')
            ->orderBy('departments.sort_order')
            ->orderBy('departments.name');

        if ($this->user_agent == 'iOS') {
            $departments->where('departments.name', 'NOT LIKE', '%Ciga%');
        }

        $result['store']['departments'] = $departments->get()
            ->toArray();

        // Si el usuario esta logueado
        if ($user_id) {
            // Mostrar calificacion de pedido
            $order = Order::select('id')
                ->where('user_id', $user_id)
                ->where('status', 'Delivered')
                ->whereNull('user_score')
                ->whereRaw("TIMESTAMPDIFF(DAY, management_date, '" . date('Y-m-d H:i:s') . "') < 5")
                ->orderBy('id', 'desc')
                ->first();

            $result['pending_order_score'] = $order ? $order->id : 0;

            // Domicilio gratis para primera compra
            if (!$this->hasOrders && $is_new_device) {
                $result['store']['delivery_order_amount'] = 0;
            }
        } elseif ($is_new_device) {
            $result['store']['delivery_order_amount'] = 0;
        }

        $this->response = [
            'status' => $status,
            'message' => $message,
            'result' => empty($result) ? new \StdClass : $result,
            'found' => $found,
            'detail' => $detail,
        ];

        return $this->jsonResponse();
    }

    /**
     * Obtiene tiempo de entrega de tienda en texto
     *
     * @return array $response Respuesta
     */
    public function get_store_delivery_time()
    {
        if (!Input::has('lat') || !Input::has('lng')) {
            $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        try {
            $store = Store::getByLatlng(Input::get('lat'), Input::get('lng'));
            $this->response = [
                'status' => true,
                'message' => 'Horario de tienda obtenido',
                'result' => $store,
            ];
        } catch (CoverageException $e) {
            $this->response = [
                'status' => false,
                'message' => 'En este momento no tenemos cobertura en tu dirección',
            ];
        }

        return $this->jsonResponse();
    }

    private function getTypeValidationEx() {
        $stock = StoreProductWarehouse::CURRENT_STOCK;

        $zoneId = Request::query('zone_id');

        if (! empty($zoneId)) {
            $zone = Zone::getActiveById($zoneId);

            $stock = $zone->type === Zone::ZONE_TYPE_DARKSUPERMARKET
                ? StoreProductWarehouse::PICKING_STOCK
                : StoreProductWarehouse::CURRENT_STOCK;
        }

        return $stock;
    }

    /**
     * Obtiene departamento con pasillos y productos
     *
     * @return array $response Respuesta
     */
    public function get_department()
    {
        $stock = $this->getTypeValidationEx();

        if (! empty(Request::query('user_id'))) {
            $this->orders = $this->orderRepository
                ->findByUserId(Request::query('user_id'))
                ->toArray();
        }

        if (empty($this->warehouse)) {
            return $this->jsonResponse(
                Config::get('debug')
                    ? 'ID no existe o categoria sin productos'
                    : 'No se encontraron productos en la categoria.',
                400
            );
        }

        if ($id = Route::current()->parameter('department_id')) {
            $store_id = Route::current()->parameter('store_id');
            $department = Department::select('departments.id', 'departments.store_id', 'departments.name',
                DB::raw('COUNT(store_products.id) AS products_qty'), 'image_header_app_url')
                ->join('shelves', 'departments.id', '=', 'shelves.department_id')
                ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->join(
                    'store_product_warehouses',
                    'store_products.id',
                    '=',
                    'store_product_warehouses.store_product_id'
                )
                ->where('store_product_warehouses.warehouse_id', $this->warehouse->id)
                ->whereRaw(sprintf('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.%s > 0, TRUE, FALSE), TRUE), TRUE)', $stock))
                ->where('departments.store_id', $store_id)
                ->where('store_product_warehouses.status', 1)
                ->where('store_product_warehouses.is_visible', 1)
                ->where('departments.status', 1)
                ->where('products.type', '<>', 'Proveedor')
                ->where('departments.id', $id)
                ->groupBy('departments.id')
                ->first();
            if ($department) {
                $result['department'] = $department ? $department->toArray() : [];
                $shelves = Shelf::select('shelves.id', 'shelves.store_id', 'shelves.department_id', 'shelves.name',
                    DB::raw('COUNT(store_products.id) AS products_qty'), 'shelves.description', 'shelves.has_warning')
                    ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                    ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->join(
                        'store_product_warehouses',
                        'store_products.id',
                        '=',
                        'store_product_warehouses.store_product_id'
                    )
                    ->join('departments', 'shelves.department_id', '=', 'departments.id')
                    ->where('store_product_warehouses.warehouse_id', $this->warehouse->id)
                    ->where('shelves.department_id', $department->id)
                    ->where('store_product_warehouses.status', 1)
                    ->where('store_product_warehouses.is_visible', 1)
                    ->where('shelves.status', 1)
                    ->where('departments.status', 1)
                    ->whereRaw(sprintf('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.%s > 0, TRUE, FALSE), TRUE), TRUE)', $stock))
                    ->where('products.type', '<>', 'Proveedor');
                if ($this->hideCigarettes()) {
                    $shelves->where('shelves.name', 'NOT LIKE', '%Ciga%');
                }

                // Se evita realizar una consulta solo para
                // obtener el identificador de la tienda.
                $store = new \Store();
                $store->id = $store_id;
                $shelves = $shelves->groupBy('shelves.id')
                    ->orderBy('shelves.sort_order')
                    ->orderBy('shelves.name')
                    ->limit(999)
                    ->get();

                $result['department']['total_banners'] = Banner::publicQuery(
                    $this->warehouse,
                    $store,
                    $department,
                    $this->hasOrders,
                    $this->isIos() ? 'ios' : 'android'
                )->count();

                $result['department']['shelves'] = $shelves ? $shelves->toArray() : [];

                foreach ($shelves as $index => $shelf) {

                    $products = StoreProduct::select(
                        'store_products.public_price',
                        'store_products.special_price',
                        'store_products.first_order_special_price',
                        'store_products.price',
                        'store_products.id', 'products.name', $this->specialPriceFields,
                        'products.image_medium_url', 'products.image_app_url', 'store_products.is_best_price',
                        'products.quantity', 'products.unit', 'store_products.store_id', 'store_products.department_id',
                        'store_product_shelves.shelf_id', 'shelves.has_warning', StoreProduct::getRawDeliveryDiscountByDate(),
                        DB::raw('IF(store_products.allied_store_id IS NULL, 0, ' . Config::get('app.marketplace_delivery_days') . ') AS marketplace_delivery_days'),
                        DB::raw('IF(DATE_SUB(NOW(), INTERVAL 5 HOUR) BETWEEN store_products.sponsored_starting_date AND store_products.sponsored_expiration_date, 1, 0) AS sponsored')
                    )
                        ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
                        ->join('products', 'store_products.product_id', '=', 'products.id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->join('shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                        ->join('departments', 'departments.id', '=', 'shelves.department_id')
                        ->with('tag')
                        ->pum()
                        ->join(
                            'store_product_warehouses',
                            'store_products.id',
                            '=',
                            'store_product_warehouses.store_product_id'
                        )
                        ->where('store_product_warehouses.warehouse_id', $this->warehouse->id)
                        ->where('store_product_shelves.shelf_id', $shelf->id)
                        ->where('store_product_warehouses.status', 1)
                        ->where('store_product_warehouses.is_visible', 1)
                        ->where('products.type', '<>', 'Proveedor')
                        ->whereRaw(sprintf('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.%s > 0, TRUE, FALSE), TRUE), TRUE)', $stock))
                        ->orderBy('store_product_shelves.sort_order')
                        ->limit(999)
                        ->get();

                    // Set real public price
                    $products->map(function ($product) {
                        $product->public_price = $this->validateRealPublicPriceUseCase->handler(
                            $this->orders,
                            $product->price,
                            $product->public_price,
                            $product->special_price,
                            $product->first_order_special_price
                        );
                    });

                    $products = castValueToFloat($products, $this->parametersCast);
                    $products = setToNullIfZero($products, ['special_price']);

                    $numberProducts = $products->count();
                    $result['department']['shelves'][$index]['products'] = $products->map(function ($product) use ($numberProducts) {
                        $product->sponsored = (int)($numberProducts >= self::MINIMUM_ACTIVE_PRODUCTS && $product->sponsored);

                        //prioridad de mejor precio o destacado sobre el competitivo (public_price).
                        $product->is_best_price = empty($product->special_price) ? $product->is_best_price : 0;
                        if ($product->is_best_price || $product->sponsored) {
                            $product->public_price = 0;
                        }

                        $product = $product->toArray();

                        if (isset($product['tag'][0])) {
                            unset($product['tag'][0]['pivot']);
                            $product['tag'][0]['last_updated'] = strtotime($product['tag'][0]['last_updated']);
                            $product['tag'] = $product['tag'][0];
                        } else {
                            $product['tag'] = null;
                        }

                        return $product;
                    });
                }

                //obtener menu header
                $warehouse = $this->warehouse;

                if ($this->user_agent != 'Android') {
                    $departments = Department::select('departments.id', 'departments.name', 'departments.store_id',
                        DB::raw('COUNT(store_products.id) AS products_qty'), 'image_list_app_url')
                        ->join('shelves', 'departments.id', '=', 'shelves.department_id')
                        ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                        ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                        ->join('products', 'products.id', '=', 'store_products.product_id')
                        ->join(
                            'store_product_warehouses',
                            'store_products.id',
                            '=',
                            'store_product_warehouses.store_product_id'
                        )
                        ->with([
                            'shelves' => function ($query) use ($warehouse, $stock) {
                                $query->select('shelves.id', 'shelves.store_id', 'shelves.department_id',
                                    'shelves.name',
                                    'shelves.image_menu_app_url', DB::raw('COUNT(store_products.id) AS products_qty'))
                                    ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                                    ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                                    ->join('departments', 'shelves.department_id', '=', 'departments.id')
                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                    ->join(
                                        'store_product_warehouses',
                                        'store_products.id',
                                        '=',
                                        'store_product_warehouses.store_product_id'
                                    )
                                    ->where('store_product_warehouses.status', 1)
                                    ->where('store_product_warehouses.is_visible', 1)
                                    ->where('shelves.status', 1)
                                    ->where('departments.status', 1)
                                    ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                                    ->whereRaw(sprintf('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.%s > 0, TRUE, FALSE), TRUE), TRUE)', $stock))
                                    ->where('products.type', '<>', 'Proveedor')
                                    ->groupBy('shelves.id')
                                    ->orderBy('shelves.sort_order')
                                    ->orderBy('shelves.name');
                            },
                        ])
                        ->where('departments.store_id', $store_id)
                        ->where('store_product_warehouses.is_visible', 1)
                        ->where('store_product_warehouses.status', 1)
                        ->where('departments.status', 1)
                        ->where('shelves.status', 1)
                        ->where('products.type', '<>', 'Proveedor')
                        ->where('store_product_warehouses.warehouse_id', $this->warehouse->id)
                        ->whereRaw(sprintf('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.%s > 0, TRUE, FALSE), TRUE), TRUE)', $stock))
                        ->groupBy('departments.id')
                        ->orderBy('departments.sort_order')
                        ->orderBy('departments.name')
                        ->get();

                    $result['menu'] = $departments;
                }
                $this->response = [
                    'status' => true,
                    'message' => 'Departamento obtenido',
                    'result' => $result,
                ];
            } else {
                return $this->jsonResponse('ID no existe o categoria sin productos', 400);
            }
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene producto
     *
     * @param int $store_product_id
     * @return array $response Respuesta
     */
    public function get_product($store_product_id)
    {
        if (empty($store_product_id)) {
            return $this->jsonResponse();
        }

        if (! empty(Request::query('user_id'))) {
            $this->orders = $this->orderRepository
                ->findByUserId(Request::query('user_id'))
                ->toArray();
        }

        // En versiones anteriores no se envia el "zone_id", esto afecta Medellin
        // debido a que se consulta en la primer bodega de Bogotá.
        if (!Input::has('zone_id')) {
            $products_store = StoreProduct::select('store_id')
                ->find($store_product_id);
            if (!empty($products_store->store_id)) {
                $this->warehouse = $this->getCurrentWarehouse($products_store->store_id);
            }
        }

        $product = \StoreProduct::select(
            'store_products.public_price',
            'store_products.id', 'products.slug', 'products.name', 'store_products.price', $this->specialPriceFields,
            'shelves.description as shelve_description',
            'store_products.first_order_special_price', 'products.image_large_url', 'products.image_medium_url',
            'products.image_small_url', 'products.image_app_url', 'store_products.is_best_price',
            'products.quantity', 'products.unit', 'products.description', 'products.nutrition_facts', 'store_products.store_id',
            'stores.name AS store', 'shelves.department_id', 'departments.name AS department',
            'store_product_shelves.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning',
            StoreProduct::getRawDeliveryDiscountByDate(), 'store_products.product_id',
            DB::raw('IF(store_products.allied_store_id IS NULL, 0, ' . Config::get('app.marketplace_delivery_days') . ') AS marketplace_delivery_days'),
            DB::raw('IF(DATE_SUB(NOW(), INTERVAL 5 HOUR) BETWEEN store_products.sponsored_starting_date AND store_products.sponsored_expiration_date, 1, 0) AS sponsored'),
            'store_products.guarantee_policies'
        )
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->join('stores', 'stores.id', '=', 'store_products.store_id')
            ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
            ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
            ->join('departments', 'departments.id', '=', 'shelves.department_id')
            ->pum()
            ->where('store_products.id', $store_product_id)
            ->where('store_product_warehouses.warehouse_id', $this->warehouse->id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF(store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('products.type', '<>', 'Proveedor')
            ->has('product')
            ->with([
                'product' => function ($query) {
                    $query->with('details')
                        ->with([
                            'images' => function ($query) {
                                $query->select('id', 'product_id', 'image_large_url', 'image_small_url');
                            },
                        ]);
                }
            ])
            ->first();

        if (empty($product)) {
            return $this->jsonResponse('ID no existe o esta inactivo', 400);
        }

        $product->product->populatePrimaryImage();
        $product->images = $product->product->images;
        $product->details = $product->product->details;
        $product->price = floatval($product->price);
        $product->delivery_discount_amount = floatval($product->delivery_discount_amount);
        $product->special_price = empty($product->special_price) ? null : floatval($product->special_price);
        $product->tag = $this->getTagFormat(
            $this->eloquentTagRepository->getTagByStoreProductId($product->id)
        );

        // Set real public price
        $product->public_price = $this->validateRealPublicPriceUseCase->handler(
            $this->orders,
            $product->price,
            $product->public_price,
            $product->special_price,
            $product->first_order_special_price
        );

        unset($product->product);
        $this->response = [
            'status' => true,
            'message' => 'Producto obtenido',
            'result' => $product,
        ];

        return $this->jsonResponse();
    }

    /**
     * Obtiene ultimos productos de un usuario pedidos en una tienda
     *
     * @return array $response Respuesta
     */
    public function get_user_products()
    {
        $this->orders = [];
        $store_id = Route::current()->parameter('store_id');
        $user_id = Route::current()->parameter('user_id');
        if ($store_id && $user_id) {
            $user = User::find($user_id);
            $page = Input::has('page') ? Input::get('page') : 1;
            $user_products = $user->getProducts($store_id, $this->warehouse->id, $this->specialPriceFields, $page, 60);
            $user_products = $user_products ? $user_products->toArray() : [];

            if (! empty($user_id)) {
                $this->orders = $this->orderRepository
                    ->findByUserId($user_id)
                    ->toArray();
            }

            foreach ($user_products as $key => $product) {
                $user_products[$key]['tag'] = $this->getTagFormat(
                    $this->eloquentTagRepository->getTagByStoreProductId($product['id'])
                );

                if (empty($user_products[$key]['public_price'])) {
                    continue;
                }

                // Set real public price
                $user_products[$key]['public_price'] = $this->validateRealPublicPriceUseCase->handler(
                    $this->orders,
                    $user_products[$key]['price'],
                    $user_products[$key]['public_price'],
                    $user_products[$key]['special_price'],
                    $user_products[$key]['first_order_special_price']
                );
            }

            $this->response = [
                'status' => true,
                'message' => 'Productos de usuario en tienda obtenidos',
                'result' => $user_products,
            ];
        } else {
            return $this->jsonResponse('Datos obligatorios vacios.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Buscador de productos por tiendas
     *
     * @return array $response Respuesta
     */
    public function search()
    {
        if (! empty($this->user_id)) {
            $this->orders = $this->orderRepository
                ->findByUserId($this->user_id)
                ->toArray();
        }

        if ($id = Route::current()->parameter('store_id')) {
            if (!Input::has('q')) {
                return $this->response = [
                    'status' => true,
                    'message' => 'No se encontraron resultados.',
                    'result' => [],
                ];
            }

            $elasticSearch = new AWSElasticsearch();
            $warehouse_id = $this->warehouse->id;
            $order_id = \Input::get('order_id');
            $excluded_product_ids = [];

            if (!empty($order_id)) {
                $order = \Order::with([
                    'orderGroup.orders.orderProducts' => function ($query) {
                        $query->select('id', 'store_product_id', 'order_id');
                    },
                ])
                    ->select('id', 'group_id', 'store_id')
                    ->findOrFail($order_id);

                // Algunas versiones de la aplicación no envian el "store_id" real
                // del pedido, por ello es necesario forzar la este valor.
                $id = $order->store_id;
                $warehouse_id = $order->orderGroup->warehouse_id;

                foreach ($order->orderGroup->orders as $order) {
                    foreach ($order->orderProducts as $orderProduct) {
                        $excluded_product_ids[] = $orderProduct->store_product_id;
                    }
                }
            }

            $store = Store::find($id);
            $city_id = $store->city_id;
            $rows = 15;
            if (Input::has('page') && Input::get('page')) {
                $page = intval(Input::get('page'));
            } else {
                $rows = 60;
            }
            if (!isset($page)) {
                $begin = 0;
                $page = 1;
            } else {
                $begin = ($page - 1) * $rows;
            }

            if (Config::get('app.aws.elasticsearch.is_enable')) {
                //NUEVO BUSCADOR
                $exclude = $this->hideCigarettes() ? ['Cigar', 'cigar', 'CIGAR'] : [];
                $q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);
                $exclude['store_product_ids'] = $excluded_product_ids;
                $result = $elasticSearch->search($warehouse_id, $q, $page, $rows, $exclude);
                $products = $result['result'][$warehouse_id];
            } else {
                //ANTIGUO BUSCADOR
                $query = Product::where('stores.city_id', $city_id);

                $q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);
                $keys = explode(' ', $q);
                if (count($keys)) {
                    $keywords = '';
                    foreach ($keys as $key) {
                        if (($key != '') and ($key != ' ')) {
                            if ($keywords != '') {
                                $keywords .= '-space-';
                            }
                            $keywords .= $key;
                        }
                    }
                    $keywords = explode('-space-', $keywords);
                    foreach ($keywords as $keyword) {
                        $query->whereRaw('(products.name LIKE "%' . trim($keyword) . '%" COLLATE utf8_spanish_ci OR products.name LIKE "%' . get_plural_string(trim($keyword)) . '%" COLLATE utf8_spanish_ci)');
                    }
                }

                if (!empty($excluded_product_ids)) {
                    $query->whereNotIn('store_products.id', $excluded_product_ids);
                }

                $query->select(
                    'store_products.id', 'store_products.id AS store_product_id', 'store_products.store_id',
                    'store_products.department_id', 'store_products.shelf_id', 'products.name', $this->specialPriceFields,
                    'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url',
                    'is_best_price', 'quantity', 'unit', 'products.description', 'nutrition_facts', StoreProduct::getRawDeliveryDiscountByDate(),
                    'shelves.has_warning',
                    DB::raw('IF(store_products.allied_store_id IS NULL, 0, ' . Config::get('app.marketplace_delivery_days') . ') AS marketplace_delivery_days'),
                    StoreProduct::getRawPublicPriceQuery()
                )->distinct()
                    ->join('store_products', 'store_products.product_id', '=', 'products.id')
                    ->join('store_product_warehouses', 'store_products.id', '=',
                        'store_product_warehouses.store_product_id')
                    ->join('shelves', 'store_products.shelf_id', '=', 'shelves.id')
                    ->join('departments', 'store_products.department_id', '=', 'departments.id')
                    ->join('stores', 'store_products.store_id', '=', 'stores.id')
                    ->where('departments.status', 1)
                    ->where('departments.type', Department::NORMAL)
                    ->where('shelves.status', 1)
                    ->where('stores.status', 1)
                    ->where('store_product_warehouses.status', 1)
                    ->where('store_product_warehouses.is_visible', 1)
                    ->where('warehouse_id', $this->warehouse->id)
                    ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                    ->where('products.type', '<>', 'Proveedor')
                    ->orderBy(
                        DB::raw('CASE
                                WHEN store_products.store_id = ' . (int)$id . ' THEN 0
                                WHEN store_products.store_id != ' . (int)$id . ' THEN 1
                            END
                    ')
                    )
                    ->orderBy('stores.id');

                if (!empty($keywords)) {
                    foreach ($keywords as $keyword) {
                        $keywords[] = get_plural_string(trim($keyword));
                    }
                    foreach ($keywords as $keyword) {
                        $query->orderBy(DB::raw('   CASE
                                                        WHEN products.name LIKE \'' . trim($keyword) . ' %\' THEN 0
                                                        WHEN products.name LIKE \'% %' . trim($keyword) . '% %\' THEN 1
                                                        WHEN products.name LIKE \'%' . trim($keyword) . '\' THEN 2
                                                        ELSE 3
                                                    END
                                                '));
                    }
                }
                if ($this->hideCigarettes()) {
                    $query->where('products.name', 'NOT LIKE', '%Ciga%');
                }

                $products = $query->orderBy('products.name', 'asc')->get();
            }

            $result = [];
            if (count($products)) {
                $current_date = Carbon::create();
                $this->lastOrderDate = $this->lastOrderDate ? new Carbon($this->lastOrderDate) : null;
                $elasticSearch->setApplyFivePercentDiscount($this->applyFivePercentDiscount());
                foreach ($products as $product) {
                    if ($this->hideCigarettes() && !empty($product['shelf_slug']) && $product['shelf_slug'] === 'cigarrillos') {
                        continue;
                    }

                    $product = $elasticSearch->clearProductResponse(
                        $product,
                        $current_date,
                        $this->hasOrders,
                        $this->lastOrderDate
                    );

                    $product['price'] = floatval($product['price']);
                    $product['delivery_discount_amount'] = floatval($product['delivery_discount_amount']);
                    $product['special_price'] = empty($product['special_price']) ? null : floatval($product['special_price']);

                    $result[$product['store_id']]['logo_url'] = $store->app_logo_small_url;
                    $result[$product['store_id']]['name'] = $store->name;
                    $result[$product['store_id']]['store_id'] = $store->id;
                    $product['id'] = $product['store_product_id'];
                    unset($product['city_slug']);
                    unset($product['store_slug']);
                    unset($product['department']);
                    unset($product['department_slug']);
                    unset($product['department_status']);
                    unset($product['shelf']);
                    unset($product['shelf_slug']);
                    unset($product['shelf_status']);
                    unset($product['store_product_id']);
                    unset($product['slug']);
                    unset($product['all_product_slug']);
                    unset($product['brand']);
                    unset($product['is_visible']);
                    unset($product['current_stock']);
                    unset($product['is_visible_stock']);
                    unset($product['status']);
                    $product['tag'] = $this->getTagFormat(
                        $this->eloquentTagRepository->getTagByStoreProductId($product['id'])
                    );

                    // Set real public price
                    $product['public_price'] = $this->validateRealPublicPriceUseCase->handler(
                        $this->orders,
                        $product['price'],
                        $product['public_price'],
                        $product['special_price'],
                        $product['first_order_special_price']
                    );

                    $result[$product['store_id']]['products'][] = $product;
                }
                $result = array_values($result);
                $this->response = ['status' => true, 'message' => 'Resultado de busqueda', 'result' => $result];
            } else {
                $search_log = new SearchLog;
                $search = Input::get('q');
                if ($this->user_id) {
                    $search_log->user_id = $this->user_id;
                }
                $search_log->search = $search;
                $search_log->ip = Request::getClientIp();
                $search_log->store_id = $store->id;
                $search_log->save();
                $this->response = ['status' => true, 'message' => 'No se encontraron resultados.', 'result' => $result];
            }

        } else {
            return $this->jsonResponse('ID no existe', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene menu de departamentos con pasillos
     */
    public function get_departments_menu()
    {
        if (empty($this->warehouse)) {
            return $this->jsonResponse(Config::get('debug') ? 'ID de tienda no existe' : 'No se encontraron productos departamentos.', 400);
        }

        if (!$store_id = Route::current()->parameter('store_id')) {
            return $this->jsonResponse('ID tienda es requerido.', 400);
        }

        //obtener menu header
        $warehouse = $this->warehouse;
        $departments = Department::select('departments.id', 'departments.name', 'departments.store_id',
            DB::raw('COUNT(store_products.id) AS products_qty'), 'image_list_app_url')
            ->join('shelves', 'departments.id', '=', 'shelves.department_id')
            ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
            ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')->where('departments.store_id',
                $store_id)
            ->where('store_product_warehouses.warehouse_id', $this->warehouse->id)
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('departments.type', Department::NORMAL)
            ->where('departments.status', 1)->groupBy('departments.id')
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->where('products.type', '<>', 'Proveedor')
            ->with([
                'shelves' => function ($query) use ($warehouse) {
                    $query->select('shelves.id', 'shelves.store_id', 'shelves.department_id', 'shelves.name',
                        'shelves.image_menu_app_url', DB::raw('COUNT(store_products.id) AS products_qty'))
                        ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                        ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                        ->join('products', 'store_products.product_id', '=', 'products.id')
                        ->join('store_product_warehouses', 'store_products.id', '=',
                            'store_product_warehouses.store_product_id')
                        ->where('store_product_warehouses.status', 1)
                        ->where('store_product_warehouses.is_visible', 1)
                        ->where('shelves.status', 1)
                        ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                        ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                        ->where('products.type', '<>', 'Proveedor')
                        ->groupBy('shelves.id')
                        ->orderBy('shelves.sort_order')
                        ->orderBy('shelves.name')
                        ->limit(999);
                },
            ])
            ->orderBy('departments.sort_order')
            ->orderBy('departments.name')
            ->get();

        $this->response = [
            'status' => true,
            'message' => 'Menu obtenido',
            'result' => $departments,
        ];

        return $this->jsonResponse();
    }

    /**
     * Obtiene los banners que una ciudad tiene disponibles
     *
     * @param \City $city
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_banners_by_city(\City $city)
    {
        $available_store = $city->stores()
            ->where('status', 1)
            ->first();

        $available_warehouse = $city->warehouses()
            ->where('status', 1)
            ->whereHas('zones', function ($query) {
                $query->where('status', 1);
            })
            ->first();

        $first_zone = $available_warehouse->zones()
            ->where('status', 1)
            ->first();

        \Input::merge([
            'zone_id' => $first_zone->id,
            'limit' => 3,
        ]);

        return $this->get_banners($available_store->id);
    }

    /**
     * Obtiene banners de una tienda
     *
     * @param $store_id
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_banners($store_id)
    {
        if (!$store = Store::find($store_id)) {
            return $this->jsonResponse('ID tienda es requerido.', 400);
        }

        $zone = $this->getCurrentZone($store_id);
        StoreProduct::$applyFivePercentDiscount = $this->applyFivePercentDiscount();
        $full_banner_info = $this->isIos() || version_compare($this->app_version, '2.1.28', '<=');
        $department = Input::get('department_id') ? Department::find(Input::get('department_id')) : null;
        $warehouse = $zone->warehouse;
        $platform = $this->isIos() ? 'ios' : 'android';

        $query = Banner::publicQuery(
            $warehouse,
            Store::find($store_id),
            $department,
            $this->hasOrders,
            $platform
        )
            ->with(['storeProduct' => function ($query) use ($store_id) {
                $query->with('product', 'shelf', 'department', 'store.city')
                    ->inWarehouse($this->getCurrentWarehouse($store_id));
            }]);

        if ($full_banner_info) {
            $query->with(['bannerStoreProducts' => function ($query) use ($store_id) {
                $query->with('product', 'shelf', 'department', 'store.city')
                    ->orderBy('banner_store_product.id')
                    ->inWarehouse($this->getCurrentWarehouse($store_id))
                    ->select('store_products.*', $this->specialPriceFields)
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->pum();
            }]);
        }

        if (\Input::get('limit')) {
            $query->limit(intval(\Input::get('limit')));
        }

        $banners = $query->get();

        if (isset($department)) {
            $banners = $this->sortBannersByDepartment($banners, $department);
        }

		$this->response = [
            'status' => true,
            'result' => $this->sortBanner($this->getBannersFormat($banners, $full_banner_info)),
        ];

        return $this->jsonResponse('Banners obtenidos.');
    }

    /**
     * Ordena los banners por Departamento
     *
     * @param $banners Collection
     * @param $department Oject
     *
     * @return Collection
     */
    private function sortBannersByDepartment($banners, $department)
    {
        $sorted = BannerDepartment::where('department_id', $department->id)
            ->whereIn('banner_id', $banners->lists('id'))
            ->orderBy('position')
            ->get(['banner_id', 'position'])
            ->lists('position', 'banner_id');

        foreach ($banners as $i => $banner) {
            $banners[$i]->{'sort'} = self::$SORT_LAST;
            if (isset($sorted[$banner->id])) {
                $banners[$i]->{'sort'} = $sorted[$banner->id];
            }
        }

        $banners->sortBy('sort');
        return $banners;
    }

    /**
     * Obtiene interior de un banner tipo conjunto de productos
     *
     * @param $store_id
     * @param $bannerId
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function single_banner($store_id, $bannerId)
    {
        if (! empty(Request::query('user_id'))) {
            $this->orders = $this->orderRepository
                ->findByUserId(Request::query('user_id'))
                ->toArray();
        }

        StoreProduct::$applyFivePercentDiscount = $this->applyFivePercentDiscount();
        $warehouse = $this->getCurrentWarehouse($store_id);
        $platform = $this->isIos() ? 'ios' : 'android';
        $store = new Store();
        $store->id = $store_id;

        $query = Banner::publicQuery($warehouse, $store, null, $this->hasOrders, $platform, true)
            ->with(['bannerStoreProducts' => function ($query) use ($store_id) {
                $query->with('shelf', 'product')
                    ->orderBy('banner_store_product.id')
                    ->inWarehouse($this->getCurrentWarehouse($store_id))
                    ->select('store_products.*', $this->specialPriceFields)
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->pum();
            }])
            ->with(['storeProduct' => function ($query) use ($store_id) {
                $query->with('shelf', 'product', 'tag')
                    ->inWarehouse($this->getCurrentWarehouse($store_id));
            }]);

        $this->response = [
            'status' => true,
            'result' => $this->getSingleBannerFormat($query->findOrFail($bannerId), true, $this->orders),
        ];

        return $this->jsonResponse('Interior de banner obtenido.');
    }

    /**
     * Obtener formato de respuesta de banners
     *
     * @param $list
     * @param $full_info
     * @return array
     */
    private function getBannersFormat($list, $full_info)
    {
        $banners = [];

        foreach ($list as $banner) {
            $banners[] = $this->getSingleBannerFormat($banner, $full_info, []);
        }

        return $banners;
    }

    /**
     * Obtener formato de respuesta de conjuntos de productos
     *
     * @param $banner
     * @param null $orders
     * @return array
     */
    private function getBannerProductGroupFormat($banner, $orders = null)
    {
        $storeProducts = [];

        foreach ($banner->bannerStoreProducts as $storeProduct) {
            $storeProducts[] = $this->getProductFormat($storeProduct, $orders);
        }

        return $storeProducts;
    }

    /**
     * Obtener formato de respuesta de producto
     *
     * @param $banner
     * @param null $orders
     * @return array|null
     */
    private function getBannerProductFormat($banner, $orders = null)
    {
        if ($banner->category !== 'Producto') {
            return null;
        }

        return $this->getProductFormat($banner->storeProduct, $orders);
    }

    /**
     * Obtiene formato de respuesta de un solo banner
     *
     * @param $banner
     * @param $full_info
     * @param null $orders
     * @return array
     */
    private function getSingleBannerFormat($banner, $full_info, $orders = null)
    {
        $item = [
            'id' => $banner->id,
            'associated_store_product_id' => $banner->store_product_id,
            'content_title' => $banner->content_title,
            'content_subtitle' => $banner->content_subtitle,
            'content' => $banner->content,
            'department_id' => $banner->deeplink_department_id,
            'description' => $banner->description,
            'image_app_modal_url' => $banner->image_app_modal_url,
            'image_app_url' => $banner->image_app_url,
            'product_id' => $banner->deeplink_store_product_id,
            'product_type' => $banner->category,
            'video_url' => $this->formatVideoUrls($banner->video_url),
            'shelf_id' => $banner->deeplink_shelf_id,
            'store_id' => $banner->store_id,
            'store_product_id' => $banner->deeplink_store_product_id,
            'title' => $banner->title,
            'type' => $banner->deeplink_type,
			'position' => $banner->position,
        ];

        $product_group = $full_info
            ? $this->getBannerProductGroupFormat($banner, $orders)
            : [];

        $single_product = $this->getBannerProductFormat($banner);

        if (!$full_info) {
            unset($item['content']);
        }

        if (!empty($product_group)) {
            $item['products'] = $product_group;
        }

        if (!empty($single_product)) {
            $item['product'] = $single_product;
        }

        return array_filter($item, function ($value) {
            return $value !== null;
        });
    }

    /**
     * Obtiene formato de respuesta de producto
     *
     * @param StoreProduct $store_product
     * @param null $orders
     * @return array
     */
    private function getProductFormat(StoreProduct $store_product, $orders = null)
    {
        // Set real public price
        $publicPrice = $this->validateRealPublicPriceUseCase->handler(
            $orders,
            $store_product->price,
            $store_product->public_price,
            $store_product->special_price,
            $store_product->first_order_special_price
        );

        return [
            'id' => $store_product->id,
            'delivery_discount_amount' => floatval($store_product->delivery_discount_amount),
            'department' => '',
            'department_id' => $store_product->department_id,
            'description' => $store_product->product->description,
            'special_price' => empty($store_product->realSpecialPrice($this->hasOrders)) ? null : floatval($store_product->realSpecialPrice($this->hasOrders)),
            'discount_percentage' => $store_product->getPercentageDiscount($this->hasOrders),
            'first_order_special_price' => $store_product->first_order_special_price,
            'has_warning' => $store_product->shelf->has_warning,
            'image_app_url' => $store_product->product->image_app_url,
            'image_large_url' => $store_product->product->image_large_url,
            'image_medium_url' => $store_product->product->image_medium_url,
            'image_small_url' => $store_product->product->image_small_url,
            'is_best_price' => $store_product->is_best_price,
            'name' => $store_product->product->name,
            'nutrition_facts' => $store_product->product->nutrition_facts,
            'price' => floatval($store_product->getPublicPrice($this->hasOrders)),
            'product' => $store_product->product->name,
            'quantity' => $store_product->product->quantity,
            'quantity_special_price' => $store_product->getQuantitySpecialPrice($this->hasOrders),
            'shelf' => '',
            'shelf_id' => $store_product->shelf_id,
            'slug' => $store_product->product->slug,
            'store' => '',
            'store_id' => $store_product->store_id,
            'unit' => $store_product->product->unit,
            'pum' => (isset($store_product->pum) ? $store_product->pum: null),
            'volume' => (isset($store_product->volume) ? $store_product->volume: 0),
            'weight' => (isset($store_product->weight) ? $store_product->weight: 0),
            'tag' => null,
            'public_price' => $publicPrice
            //'tag' => $this->getTagFormat(
            //    $this->eloquentTagRepository->getTagByStoreProductId($store_product->id)
            //)
        ];
    }

    private function formatVideoUrls($urlsVideo)
    {
        $urls = json_decode($urlsVideo,true);

        if (is_array($urls)) {
            return $urls;
        }
        return null;
    }

    /**
     * @param $tag
     * @return |null
     */
    private function getTagFormat($tag)
    {
        if (!isset($tag)) {
            return null;
        }

        $tag->last_updated = strtotime($tag->last_updated);

        return $tag;
    }

	/**
	 * @param $banners
	 * @return array
	 */
    private function sortBanner($banners)
	{
		if(empty($banners)) {
			return $banners;
		}

		$positionsReserved = [];
		$positionsRandom = [];

		foreach ($banners as $banner) {
			if (isset($banner['position']) && $banner['position'] <= Banner::NUMBER_POSITION) {
				$positionsReserved[] = $banner;
				continue;
			}

			$positionsRandom[] = $banner;
		}

		if (!empty($positionsRandom)) {
			shuffle($positionsRandom);
		}

		return array_merge($positionsReserved, $positionsRandom);
	}
}
