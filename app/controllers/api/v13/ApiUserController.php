<?php

namespace api\v13;

use Country;
use exceptions\MerqueoException;
use Hashids\Hashids;
use Input, Route, Request, Hash, Config, DateTime, Validator, DB, User, StoreProduct,
    UserAddress, UserCreditCard, Password, Order, OrderProduct, Payment, UserAppComment, UserSurvey, Lang, OrderGroup, PayU, UserCredit;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use tickets\Ticket;
use usecases\contracts\Countries\CountrySettingsUseCaseInterface;
use usecases\contracts\orders\OrderWhoReceivesInterface;
use usecases\Exception\OrderNotFoundException;

class ApiUserController extends \ApiController
{
    const ACTION_CURRENT_ORDERS = 'current';
    const ACTION_OLD_ORDERS = 'old';
    /**
     * @var OrderWhoReceivesInterface
     */
    private $orderWhoReceives;

     /**
     * The parameters convert to float.
     *
     * @var array
     */
    private $parametersCast = ["price","special_price","delivery_discount_amount"];

    /**
     * @var CountrySettingsUseCaseInterface
     */
    private $countryUseCase;

    /**
     * ApiUserController constructor.
     * @param OrderWhoReceivesInterface $orderWhoReceives
     */
    public function __construct(
        OrderWhoReceivesInterface $orderWhoReceives,
        CountrySettingsUseCaseInterface $countryUseCase
    ) {
        parent::__construct();
        $this->orderWhoReceives = $orderWhoReceives;
        $this->countryUseCase = $countryUseCase;
    }

    /**
     * Realiza login de usuario
     *
     * @return array $response Respuesta
     */
    public function login()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        $warehouse = $this->getCurrentWarehouse();

        if (empty($email) || empty($password)) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $user = User::where('email', $email)->where('type', 'merqueo.com')->first();
        if ($user) {
            /*$rsa = new Crypt_RSA();
            $rsa->loadKey(Config::get('app.private_key'));
            $password = $rsa->decrypt(base64_decode($password));
            if (!$password){
                $this->response['message'] = 'Formato de password no valido.';
                return $this->jsonResponse();
            }*/

            //if (Auth::validate(array('email' => $email, 'password' => $password))){
            if (Hash::check($password, $user->password)) {
                if ($user->status) {
                    $user->oauth_verifier = generate_token();
                    $user->save();

                    $user_data = $this->getUserData($user->id);

                    $user_products = [];
                    if (Input::has('store_id')) {
                        $user_products = $user->getProducts(Input::get('store_id'), $warehouse->id);
                        $user_products = $user_products ? $user_products->toArray() : [];
                    }

                    $this->response = array(
                        'status' => true,
                        'message' => 'Sesion iniciada',
                        'result' => array(
                            'user' => $user_data,
                            'user_products' => $user_products,
                            'cart' => []
                        )
                    );
                } else {
                    $this->response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros a ' . Config::get('app.admin_email') . '.';
                }
            } else {
                $this->response['message'] = 'Contraseña incorrecta.';
            }
        } else {
            $this->response['message'] = 'Email incorrecto.';
        }

        return $this->jsonResponse();
    }

    /**
     * Cierra sesion
     */
    public function logout()
    {
        $this->response = array(
            'status' => true,
            'message' => 'Sesion cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Realiza registro de usuario
     *
     * @return array $response Respuesta
     */
    public function save_user()
    {
        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $email = Input::get('email');
        $password = Input::get('password');
        $phone = Input::get('phone');
        if (empty($first_name) || empty($last_name) || empty($email) || empty($password) || empty($phone)) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        //verifica que el usuario no exista
        $validator = Validator::make(
            array('email' => $email, 'phone' => $phone),
            array(
                'email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com',
                'phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com'
            ),
            array(
                'email.required' => 'El email es requerido.',
                'email.unique' => 'El email ingresado ya se encuentra en uso.',
                'email.email' => 'El formato del email ingresado no es valido.',
                'phone.required' => 'El número celular es requerido.',
                'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                'phone.numeric' => 'El número celular debe ser númerico.',
                'phone.digits' => 'El número celular debe tener 10 digitos.'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            if (!$error = $messages->first('email')) {
                $error = $messages->first('phone');
            }
            $this->response['message'] = $error;
            return $this->jsonResponse();
        }
        //crear el usuario, realizar login y enviar mail de bienvenida
        $user = User::add($first_name, $last_name, $phone, $email, $password);
        $user->origin = strtolower($this->user_agent);
        $user->save();
        //validar y cargar codigo de referido
        $referred = User::validateReferred($user);

        if (!Input::has('do_not_send_mail')) {
            $mail = array(
                'template_name' => 'emails.welcome',
                'subject' => 'Bienvenido a Merqueo',
                'to' => array(array('email' => $user->email, 'name' => $user->first_name . ' ' . $user->last_name)),
                'vars' => array(
                    'referral_code' => $user->referral_code,
                    'referred' => $referred,
                    'name' => $user->first_name
                )
            );
            send_mail($mail);
        }
        $user = $this->getUserData($user->id);

        $this->response = array(
            'status' => true,
            'message' => 'Usuario creado',
            'result' => array('user' => $user)
        );

        return $this->jsonResponse();
    }

    /**
     * Realiza registro y login de usuario con Facebook
     *
     * @return array $response Respuesta
     */
    public function social()
    {
        $fb_id = Input::get('fb_id');
        if (empty($fb_id)) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $this->response['message'] = 'Ocurrió un error al procesar tus datos de Facebook.';

        if (Input::has('user_id')) {
            if ($user = User::find(Input::get('user_id'))) {
                $user->fb_id = $fb_id;
                $user->save();

                $user_data = $this->getUserData($user->id);
                $this->response = array(
                    'status' => true,
                    'message' => 'Cuenta asociada',
                    'result' => array('user' => $user_data, 'fb_type' => 'associated_account')
                );
            } else {
                return $this->jsonResponse('ID de usuario no existe.', 400);
            }
        } else {
            //login con fb
            if ($user = User::where('fb_id', $fb_id)->first()) {
                if ($user->status) {
                    $user_data = $this->getUserData($user->id);

                    $this->response = array(
                        'status' => true,
                        'message' => 'Sesión iniciada',
                        'result' => array('user' => $user_data, 'fb_type' => 'login')
                    );
                } else {
                    $this->response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros a ' . Config::get('app.admin_email') . '.';
                }
            } else {
                $email = Input::get('email');
                if ($user = User::where('email', $email)->first()) {
                    if ($user->status) {
                        $user->fb_id = $fb_id;
                        $user->save();
                        $user_data = $this->getUserData($user->id);
                        $this->response = array(
                            'status' => true,
                            'message' => 'Cuenta asociada',
                            'result' => array('user' => $user_data, 'fb_type' => 'signed_up')
                        );
                    } else {
                        $this->response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros a ' . Config::get('app.admin_email') . '.';
                    }
                } else {
                    //registro y login
                    $first_name = Input::get('first_name');
                    $last_name = Input::get('last_name');
                    $phone = Input::has('phone') ? Input::get('phone') : '';

                    if (empty($first_name) || empty($last_name)) {
                        return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
                    }
                    if (empty($email)) {
                        $email = '';
                    }
                    /*$this->response['message'] = 'El email es requerido, por favor ingresalo en tu cuenta de Facebook.';
                    return $this->jsonResponse();*/

                    //verifica que el usuario no exista
                    $validator = Validator::make(
                        array('email' => $email, 'phone' => $phone),
                        array(
                            'email' => 'email|unique:users,email,NULL,id,type,merqueo.com',
                            'phone' => 'numeric|unique:users,phone,NULL,id,type,merqueo.com'
                        ),
                        array( //'email.required' => 'El email es requerido.',
                            'email.unique' => 'El email ingresado ya se encuentra en uso.',
                            'email.email' => 'El formato del email ingresado no es valido.',
                            'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                            'phone.numeric' => 'El número celular debe ser númerico.'
                        )
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        if (!$error = $messages->first('email')) {
                            $error = $messages->first('phone');
                        }
                        $this->response['message'] = $error;
                        return $this->jsonResponse();
                    } else {
                        $user = User::add($first_name, $last_name, $phone, $email, '');
                        $user->origin = strtolower($this->user_agent);
                        $user->fb_id = $fb_id;
                        $user->save();

                        //validar y cargar codigo de referido
                        $referred = User::validateReferred($user);

                        $mail = array(
                            'template_name' => 'emails.welcome',
                            'subject' => 'Bienvenido a Merqueo',
                            'to' => array(
                                array(
                                    'email' => $user->email,
                                    'name' => $user->first_name . ' ' . $user->last_name
                                )
                            ),
                            'vars' => array(
                                'referral_code' => $user->referral_code,
                                'referred' => $referred,
                                'name' => $user->first_name
                            )
                        );
                        send_mail($mail);
                        $user_data = $this->getUserData($user->id);

                        $this->response = array(
                            'status' => true,
                            'message' => 'Cuenta creada y asociada',
                            'result' => array('user' => $user_data, 'fb_type' => 'signed_up')
                        );
                    }
                }
            }
        }

        if ($this->response['status'] && Input::has('store_id')) {
            $warehouse = $this->getCurrentWarehouse(Input::get('store_id'));
            $user_products = $user->getProducts(Input::get('store_id'), $warehouse->id);
            $this->response['result']['user_products'] = $user_products ? $user_products->toArray() : [];
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos de usuario con direcciones, pedidos, tarjetas de credito
     *
     * @return array $response Respuesta
     */
    public function get_user()
    {
        if (!$id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if ($user = $this->getUserData($id)) {
            $this->response = array(
                'status' => true,
                'message' => 'Usuario obtenido',
                'result' => $user
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Actualiza datos de usuario
     *
     * @param int $user_id
     * @return array $response Respuesta
     */
    public function update_user($user_id)
    {
        if (empty($user_id)) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        $user = User::find($user_id);

        if (!$user) {
            return $this->jsonResponse('ID no existe.', 400);
        }

        if (Input::has('first_name')) {
            $user->first_name = Input::get('first_name');
        }

        if (Input::has('last_name')) {
            $user->last_name = Input::get('last_name');
        }

        if (Input::has('password')) {
            $user->password = Hash::make(Input::get('password'));
        }

        if (Input::get('email') && empty($user->email)) {
            $user->email = Input::get('email');
        }

        if (Input::get('phone') && empty($user->phone)) {
            $user->phone = Input::get('phone');
        }

        if ($user->isDirty('email') || $user->isDirty('phone')) {
            //verifica que el usuario no exista
            $rules = [];
            $messages = [
                'email.unique' => 'El email ingresado ya se encuentra en uso.',
                'email.email' => 'El formato del email ingresado no es valido.',
                'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                'phone.numeric' => 'El número celular debe ser númerico.'
            ];

            if ($user->isDirty('email')) {
                $rules['email'] = "required|email|unique:users,email,{$user->id},id,type,merqueo.com";
            }

            if ($user->isDirty('phone')) {
                $rules['phone'] = "required|numeric|unique:users,phone,{$user->id},id,type,merqueo.com";
            }

            $validator = Validator::make(['email' => $user->email, 'phone' => $user->phone], $rules, $messages);

            if ($validator->fails()) {
                $messages = $validator->messages();
                if (!$error = $messages->first('email')) {
                    $error = $messages->first('phone');
                }
                $this->response['message'] = $error;

                return $this->jsonResponse();
            }
        }

        $user->save();
        $user = $this->getUserData($user->id);

        $this->response = [
            'status' => true,
            'message' => 'Usuario actualizado',
            'result' => $user
        ];

        return $this->jsonResponse();
    }

    /**
     * Agregar codigo de referido
     */
    public function referred()
    {
        if (!$id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if (!Input::has('promo_code')) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $user = User::find($id);
        if (!$user) {
            return $this->jsonResponse('Usuario no existe.', 400);
        }

        if ($user->referred_by) {
            $this->response['message'] = 'Ya agregaste un código referido.';
            return $this->jsonResponse();
        }

        $order = Order::where('user_id', $user->id)->where('status', 'Delivered')->first();
        if ($order) {
            $this->response['message'] = 'No puedes agregar un código referido por que ya hiciste tu primer pedido.';
            return $this->jsonResponse();
        }

        $referred = User::validateReferred($user);
        if ($referred['success']) {
            $user_discounts = User::getDiscounts($user->id);
            $this->response = array(
                'status' => true,
                'message' => 'Tu código de referido fue procesado con éxito, tienes ' . currency_format($user_discounts['credits']['total']) . ' en créditos en tu cuenta.',
                'result' => array(
                    'credit_available' => $user_discounts['amount'],
                    'referral_code' => $user->referral_code,
                    'free_delivery_days' => $user_discounts['free_delivery_days'],
                    'free_delivery_expiration_date' => format_date('normal_long', $user->free_delivery_expiration_date),
                )
            );
        }
        if ($referred['limit']) {
            $this->response['message'] = 'El código referido que ingresaste no es valido por que ha alcanzado el limite permitido de referidos por cuenta.';
        } else {
            if ($referred['error']) {
                $this->response['message'] = 'El código de referido que ingresaste no es valido, por favor verificalo y vuelve a intentarlo.';
            }
        }

        return $this->jsonResponse();
    }

    /**
     * Recordar clave
     *
     * @return array $response Respuesta
     */
    public function password_remind()
    {
        if ($user = Password::getUser(array('email' => Input::get('email')))) {
            //generar token
            $country = $this->countryUseCase->getCountryByPhoneCode($user->phone_prefix);
            $token = str_shuffle(sha1($user->email . microtime(true)));
            $token = hash_hmac('sha1', $token, 'ofkshgokshogkis');
            $values = ['email' => $user->email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')];
            DB::table('password_reminders')->insert($values);

            $vars = array(
                'username' => $user->first_name,
                'website_name' => Config::get('app.website_name'),
                'user_url' => url('/mi-cuenta'),
                'contact_email' => $country['contact_email'],
                'contact_phone' => $country['admin_phone'],
                'android_url' => $country['android_url'],
                'ios_url' => $country['ios_url'],
                'facebook_url' => $country['facebook_url'],
                'twitter_url' => $country['twitter_url'],
                'token' => $token,
                'country_data' => $country
            );

            $subject = $country['country_code'] === Country::COLOMBIA_CODE ? 'Restaurar contraseña' : 'Restablece tu contraseña';

            $mail = array(
                'template_name' => 'emails.auth.reminder',
                'subject' => $subject,
                'to' => array(
                    array('email' => $user->email, 'name' => $user->first_name)
                ),
                'vars' => $vars
            );

            $result = send_mail($mail, false, false, $country['contact_email']);
            if ($result) {
                $this->response = array(
                    'status' => true,
                    'message' => Lang::get('reminders.sent')
                );
            } else {
                $this->response['message'] = Lang::get('reminders.error');
            }
        } else {
            $this->response['message'] = Lang::get('reminders.user');
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos de una direccion
     *
     * @return array $response Respuesta
     */
    public function get_address()
    {
        if (!$id = Route::current()->parameter('address_id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        $address = UserAddress::select('user_address.*',
            DB::raw('IF (parent_city_id IS NULL, city_id, parent_city_id) AS parent_city_id'))
            ->join('cities', 'city_id', '=', 'cities.id')->where('id', $id)->where('user_id', $user_id)->get();
        if (count($address)) {
            $this->response = array(
                'status' => true,
                'message' => 'Dirección obtenida',
                'result' => array('address' => $address->toArray())
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Guarda una nueva direccion
     *
     * @return array $response Respuesta
     */
    public function save_address()
    {
        if (!$user_id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        $label = Input::get('label');
        $address = Input::get('address');
        $city = Input::get('city');
        $dir = Input::get('dir');

        if (empty($label) || empty($address) || empty($city) || empty($dir)) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        if (!UserAddress::isCorrectAddress($address)) {
            $this->response['status'] = false;
            return $this->jsonResponse('Dirección invalida', 200);
        }

        $address = new UserAddress;
        $address->user_id = $user_id;
        $address->label = Input::get('label');
        $address->address = Input::get('address');
        $address->address_further = Input::get('address_further');
        $address->neighborhood = Input::has('neighborhood') ? Input::get('neighborhood') : '';
        $dir = Input::get('dir');
        $address->address_1 = $dir[0];
        $address->address_2 = $dir[2];
        $address->address_3 = $dir[6];
        $address->address_4 = $dir[8];
        $request = Request::create('/api/location', 'GET',
            array('address' => $address->address, 'city' => Input::get('city')));
        Request::replace($request->input());
        $result = json_decode(Route::dispatch($request)->getContent());
        if ($result->status) {
            $address->latitude = $result->result->latitude;
            $address->longitude = $result->result->longitude;
            $address->city_id = $result->result->city_id;
            $address->save();
            $this->response = array(
                'status' => true,
                'message' => 'Dirección guardada.',
                'result' => array('address' => $address->toArray())
            );
        } else {
            $this->response['message'] = 'Ocurrió un error al ubicar tu dirección.';
        }

        return $this->jsonResponse();
    }

    /**
     * Actualiza una direccion
     *
     * @return array $response Respuesta
     */
    public function update_address()
    {
        if (!$id = Route::current()->parameter('address_id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        $label = Input::get('label');
        $address = Input::get('address');
        $city = Input::get('city');
        $dir = Input::get('dir');

        if (empty($label) || empty($address) || empty($city) || empty($dir)) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $address = UserAddress::where('user_id', $user_id)->where('id', $id)->first();
        if ($address) {
            $address->label = Input::get('label');
            $address->address = Input::get('address');
            $address->address_further = Input::get('address_further');
            $dir = Input::get('dir');
            $address->address_1 = $dir[0];
            $address->address_2 = $dir[2];
            $address->address_3 = $dir[6];
            $address->address_4 = $dir[8];

            if (!UserAddress::isCorrectAddress($address->address)) {
                $this->response['status'] = false;
                return $this->jsonResponse('Dirección invalida', 200);
            }

            $request = Request::create('/api/location', 'GET',
                array('address' => $address->address, 'city' => Input::get('city')));
            Request::replace($request->input());
            $result = json_decode(Route::dispatch($request)->getContent());
            if ($result->status) {
                $address->latitude = $result->result->latitude;
                $address->longitude = $result->result->longitude;
                $address->city_id = $result->result->city_id;
                $address->save();
                $this->response = array(
                    'status' => true,
                    'message' => 'Dirección actualizada.',
                    'result' => array('address' => $address->toArray())
                );
            } else {
                $this->response['message'] = 'Ocurrió un error al ubicar tu dirección.';
            }
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Elimina una direccion
     *
     * @return array $response Respuesta
     */
    public function delete_address()
    {
        if (!$id = Route::current()->parameter('address_id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        $affectedRows = UserAddress::where('user_id', $user_id)->where('id', $id)->delete();
        if ($affectedRows) {
            $this->response = array(
                'status' => true,
                'message' => 'Dirección eliminada',
                'result' => ''
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos de una tarjeta de credito
     *
     * @return array $response Respuesta
     */
    public function get_credit_card()
    {
        if (!$id = Route::current()->parameter('credit_card_id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        $credit_card = UserCreditCard::select('id', 'last_four', 'type')->where('id', $id)->where('user_id',
            $user_id)->get();
        if (count($credit_card)) {
            $this->response = array(
                'status' => true,
                'message' => 'Tarjeta de crédito obtenida.',
                'result' => array('credit_card' => $credit_card->toArray())
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Guarda una nueva tarjeta de crédito
     *
     * @return array $response Respuesta
     */
    public function save_credit_card()
    {
        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID usuario es requerido.', 200);
        }

        if (!Input::has('number_cc') || !Input::has('expiration_month_cc') || !Input::has('expiration_year_cc') || !Input::has('code_cc') || !Input::has('name_cc')) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 200);
        }

        if (Input::has('user_id')) {
            //validar datos de tarjeta
            /*$rsa = new Crypt_RSA();
            $rsa->loadKey(Config::get('app.private_key'));

            $number_cc = $rsa->decrypt(base64_decode($number_cc));
            if (!$number_cc)
                return $this->jsonResponse('Formato de número de tarjeta no valido.', 400);
            $code_cc = $rsa->decrypt(base64_decode($code_cc));
            if (!$code_cc)
                return $this->jsonResponse('Formato de código de tarjeta no valido.', 400);
            $name_cc = $rsa->decrypt(base64_decode($name_cc));
            if (!$name_cc)
                 return $this->jsonResponse('Formato de nombre de tarjeta no valido.', 400);
            $expiration_month_cc = $rsa->decrypt(base64_decode($expiration_month_cc));
            if (!$expiration_month_cc)
                return $this->jsonResponse('Formato de mes de tarjeta no valido.', 400);
            $expiration_year_cc = $rsa->decrypt(base64_decode($expiration_year_cc));
            if (!$expiration_year_cc)
                 return $this->jsonResponse('Formato de año de tarjeta no valido.', 400);*/

            if (!$user = User::find(Input::get('user_id'))) {
                return $this->jsonResponse('Usuario no valido.', 200);
            }

            $data = array(
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'user_id' => $user->id
            );

            $address = UserAddress::select('user_address.*', 'city')->join('cities', 'cities.id', '=',
                'city_id')->where('user_id', $user->id)->first();
            if ($address) {
                $data['address'] = $address->address;
                $data['address_further'] = $address->address_further;
            } else {
                $data['address'] = $data['address_further'] = 'No especificada';
            }

            $data = array_merge($data, Input::all());

            $payu = new PayU;

            //validar pais de tarjeta de credito
            $bin = substr(Input::get('number_cc'), 0, 6);
            $credit_card = $payu->getCreditCardInfo($bin, $data);
            if (!$credit_card) {
                return $this->jsonResponse('Ocurrió un problema al validar el origen de tu tarjeta de crédito.', 200);
            }
            if (!$credit_card->is_valid)
                return $this->jsonResponse('Solo se aceptan tarjetas de crédito nacionales.', 200);

            $data['card_type'] = get_card_type($data['number_cc']);

            $result = $payu->associateCreditCard($data);
            if (!$result['status']) {
                return $this->jsonResponse($result['message'], 200);
            }

            if (!in_array($result['response']->creditCardToken->paymentMethod, $this->config['credit_cards_types'])) {
                return $this->jsonResponse('Solo se aceptan tarjetas de crédito ' . $this->config['credit_cards_message'], 200);
            }

            $user_credit_card = new UserCreditCard;
            $user_credit_card->card_token = $result['response']->creditCardToken->creditCardTokenId;
            $user_credit_card->user_id = $user->id;
            $user_credit_card->bin = substr($data['number_cc'], 0, 6);
            $user_credit_card->type = $data['card_type'];
            $user_credit_card->last_four = substr($data['number_cc'], 12, 4);
            $user_credit_card->country = $credit_card->country_name;
            $user_credit_card->holder_document_number = isset($data['document_number_cc']) ? $data['document_number_cc'] : '';
            $user_credit_card->holder_name = $data['name_cc'];
            $user_credit_card->expiration_month = $data['expiration_month_cc'];
            $user_credit_card->expiration_year = $data['expiration_year_cc'];

            if (isset($data['document_type_cc'])) {
                $user_credit_card->holder_document_type = $data['document_type_cc'];
                $user_credit_card->holder_email = isset($data['email_cc']) ? $data['email_cc'] : $user->email;
                $user_credit_card->holder_phone = $data['phone_cc'];
                $user_credit_card->holder_address = $data['address_cc'];
                $user_credit_card->holder_address_further = isset($data['address_further_cc']) ? $data['address_further_cc'] : '';
                $user_credit_card->holder_city = isset($data['city_cc']) ? $data['city_cc'] : isset($address) && isset($address->city) ? $address->city : '';
            }

            $user_credit_card->save();
            $message = "";
            if ($credit_card && Config::get('app.payu.precharge_enable')) {
                $preCharge = $user->preChargeCreditCard($user->id, $user_credit_card->id, 1);
                if (isset($preCharge['status']) && !$preCharge['status']) {
                    $userCreditCard = UserCreditCard::find($user_credit_card->id);
                    $userCreditCard->delete();
                    return $this->jsonResponse('Ocurrió un error al añadir la tarjeta de crédito.', 200);
                } elseif (isset($preCharge['status']) && $preCharge['status']) {
                    $message = ', ' . $preCharge['message'];
                }
            }

            $credit_card = [
                'id' => $user_credit_card->id,
                'last_four' => $user_credit_card->last_four,
                'type' => $user_credit_card->type
            ];

            $this->response = array(
                'status' => true,
                'message' => 'Tarjeta de crédito guardada. ' . $message,
                'result' => array('credit_card' => $credit_card)
            );

        } else {
            return $this->jsonResponse('Usuario no valido.', 200);
        }

        return $this->jsonResponse();
    }

    /**
     * Elimina una tarjeta de crédito
     *
     * @return array $response Respuesta
     */
    public function delete_credit_card()
    {
        if (!$id = Route::current()->parameter('credit_card_id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        //validar pedidos en proceso con la tarjeta a eliminar
        $orders = Order::join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->where('status', '<>', 'Delivered')
            ->where('status', '<>', 'Cancelled')
            ->where('credit_card_id', $id)
            ->count();
        if (!$orders) {
            $user = User::find($user_id);
            if (!$user_credit_card = UserCreditCard::find($id)) {
                $this->response['message'] = 'Ocurrió un error al intentar eliminar la tarjeta de crédito.';
                return $this->jsonResponse();
            }
            if (strlen($user_credit_card->card_token) == Config::get('app.payu.token_length')) {
                $payu = new PayU;
                $result = $payu->deleteCreditCard($user->id, $user_credit_card->card_token);
            } else {
                $payment = new Payment;
                $result = $payment->deleteCreditCard($user->id, $user->customer_token, $user_credit_card->card_token);
            }
            if (!$result['status']) {
                $this->response['message'] = 'Ocurrió un error al eliminar la tarjeta de crédito.';
                return $this->jsonResponse();
            }
        } else {
            $this->response['message'] = 'No se puede eliminar la tarjeta de crédito por que esta asociada a un pedido en proceso.';
            return $this->jsonResponse();
        }

        if (UserCreditCard::where('user_id', $user->id)->where('id', $id)->delete()) {
            $this->response = array(
                'status' => true,
                'message' => 'Tarjeta de crédito eliminada.',
                'result' => ''
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene credito actual del usuario
     *
     * @return array $response Respuesta
     */
    public function get_user_credit()
    {
        if (!$id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        $user = User::find($id);
        if ($user) {

            $user_discounts = User::getDiscounts($user->id);
            $this->response = array(
                'status' => true,
                'message' => 'Crédito disponible obtenido.',
                'result' => array(
                    'credit_available' => $user_discounts['amount']
                )
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene movimientos de credito del usuario
     *
     * @return array $response Respuesta
     */
    public function get_user_credit_movements()
    {
        if (!$id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        $user = User::find($id);
        if ($user) {

            $page = Input::has('page') ? Input::get('page') : 1;
            $credit_movements = UserCredit::getCreditMovements($user->id, 'user', $page);

            $this->response = array(
                'status' => true,
                'message' => 'Movimientos de crédito obtenidos.',
                'result' => $credit_movements
            );
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene descuentos de usuario actual (credito y domicilio gratis)
     *
     * @return array $response Respuesta
     */
    public function get_user_discounts()
    {
        if (!$id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID es requerido.', 400);
        }

        $user = User::find($id);
        if ($user) {

            $user_discounts = User::getDiscounts($user->id);
            $this->response = [
                'status' => true,
                'message' => 'Descuentos de usuario obtenidos.',
                'result' => [
                    //para versiones anteriores 2.1.14 Android y 2.1.13 iOS
                    'referral_code' => $user->referral_code,
                    'credit_available' => $user_discounts['amount'],
                    'free_delivery_days' => $user_discounts['free_delivery_days'],
                    'free_delivery_expiration_date' => format_date('normal_long', $user->free_delivery_expiration_date),

                    'free_delivery' =>
                        [
                            'days' => $user_discounts['free_delivery_days'],
                            'expiration_date' => format_date('normal_long', $user->free_delivery_expiration_date),
                            'on_next_order' => $user_discounts['free_delivery_next_order'],
                        ],
                    'credit' =>
                        [
                            'available' => currency_format($user_discounts['amount']),
                            'movements' => UserCredit::getCreditMovements($user->id, 'user')
                        ],
                    'referred' =>
                        [
                            'code' => $user->referral_code,
                            'by_credit' => currency_format(Config::get('app.referred.amount')),
                            'referrer_by_credit' => currency_format(Config::get('app.referred.referred_by_amount')),
                            'credit_expiration_days' => Config::get('app.referred.amount_expiration_days'),
                            'minimum_amount_order_for_credit' => currency_format(Config::get('app.minimum_discount_amount')),
                            'by_free_delivery_days' => Config::get('app.referred.free_delivery_days'),
                            'referrer_by_free_delivery_days' => Config::get('app.referred.referred_by_free_delivery_days'),
                            'share_image_url' => Config::get('app.referred.share_image_url'),
                            'share_title' => Config::get('app.referred.share_title'),
                            'share_description' => Config::get('app.referred.share_description'),
                            'referral_url' => $user->referral_url,
                            'terms_url' => 'https://merqueo.com/terminos#referidos',
                        ]
                ]
            ];
        } else {
            return $this->jsonResponse('ID no existe.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtener productos de pedido para agregar al carrito
     *
     * @return array $response Respuesta
     */
    public function add_order_cart()
    {
        if (!$order_id = Route::current()->parameter('order_id')) {
            return $this->jsonResponse('ID del pedido es requerido.', 400);
        }

        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID del usuario es requerido.', 400);
        }

        //validacion precio para primera compra
        $special_price = 'special_price';
        if ($has_orders = Order::select('id')->where('user_id', $user_id)->where('status', '<>', 'Cancelled')->orderBy('id', 'desc')->first()) {
            $special_price = DB::raw("IF(first_order_special_price = 1, NULL, special_price) AS special_price");
            $discount_percentage = DB::raw('IF(first_order_special_price = 1, NULL, CAST(ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS UNSIGNED)) AS discount_percentage');
        } else {
            $discount_percentage = DB::raw('CAST(ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS UNSIGNED) AS discount_percentage');
        }

        $order_group = OrderGroup::select('order_groups.warehouse_id')->join('orders', 'orders.group_id', '=', 'order_groups.id')->where('orders.id', $order_id)->first();
        $products = OrderProduct::select('store_products.id', 'products.slug', 'products.name', 'store_products.price',
            $special_price, $discount_percentage,
            'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url',
            'image_small_url', 'image_app_url', 'is_best_price', 'store_products.merqueo_discount',
            'products.quantity', 'products.unit', 'products.description', 'nutrition_facts', 'store_products.store_id',
            'stores.name AS store', 'store_products.department_id',
            'departments.name AS department', 'store_products.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning',
            StoreProduct::getRawDeliveryDiscountByDate(), 'order_products.quantity AS quantity_cart')
            ->join('orders', 'orders.id', '=', 'order_products.order_id')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->join('stores', 'stores.id', '=', 'departments.store_id')
            ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->where('orders.id', $order_id)
            ->where('order_products.type', '<>', 'Muestra')
            ->where('orders.user_id', $user_id)
            ->where('store_product_warehouses.warehouse_id', empty($order_group->warehouse_id) ? 0 : $order_group->warehouse_id)
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('stores.status', 1)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->groupBy('store_products.id')
            ->get();
        if (!count($products)) {
            $this->response['message'] = 'No se puede agregar porque la tienda o los productos donde se hizo el pedido no están habilitados.';
            return $this->jsonResponse();
        }

        $products = castValueToFloat($products, $this->parametersCast);                   
        $products = setToNullIfZero($products, ['special_price']);

        foreach ($products as $store_product) {
            if ($store_product->special_price > -1 && $store_product->quantity_special_price && $store_product->cart_quantity) {
                $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->where('store_product_id', $store_product->id)
                    ->where('price', $store_product->special_price)
                    ->where('user_id', $user_id)
                    ->where('status', '<>', 'Cancelled')
                    ->where(DB::raw("DATEDIFF('" . date('Y-m-d') . "', date)"), '<=',
                        Config::get('app.minimum_promo_days'))
                    ->groupBy('order_products.store_product_id')
                    ->count();
                if ($count_products) {
                    $this->response['message'] = 'Ya realizaste un pedido con el limite permitido para el producto en promoción: ' . $store_product->name;
                    return $this->jsonResponse();
                }

                if ($store_product->quantity_cart > $store_product->quantity_special_price) {
                    $this->response['message'] = 'El producto ' . $store_product->name . ' por estar en promoción puedes agregar máximo ' . $store_product->quantity_special_price . ' unidades al carrito.';
                    return $this->jsonResponse();
                }
            }
        }

        $this->response = array(
            'status' => true,
            'message' => 'Productos obtenidos',
            'store' => Order::find($order_id)->store->name,
            'result' => $products
        );

        return $this->jsonResponse();
    }

    /**
     * Guarda comentario negativo de usuario en el app
     *
     * @return array $response Respuesta
     */
    public function save_comment()
    {
        if (!$user_id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        if (!Input::has('comment') || !Input::has('os') || !Input::has('app_version')) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $user_app_comment = new UserAppComment();
        $user_app_comment->user_id = $user_id;
        if (Input::has('order_id')) {
            $user_app_comment->order_id = Input::get('order_id');
        }

        $user_app_comment->os = Input::get('os');
        $user_app_comment->app_version = Input::get('app_version');
        $user_app_comment->comment = Input::get('comment');
        $user_app_comment->save();

        if (Input::has('order_id') && Input::has('comment')) {
            $order = Order::where('orders.id', Input::get('order_id'))->first();
            $order->user_score_comments = $user_app_comment->comment;
            $order->save();

            $order_group = OrderGroup::where('order_groups.id', $order->group_id)->select('order_groups.user_firstname',
                'order_groups.user_lastname', 'order_groups.user_email', 'order_groups.source')->first();

            $html = '<b>ID del pedido: </b> ' . $order->id . '<br>
                    <b>Nombre del usuario: </b> ' . $order_group->user_firstname . ' ' . $order_group->user_lastname . '<br>
                    <b>Valor del pedido: </b> $' . number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount),
                    0, ',', '.') . '<br>
                    <b>Fecha de ingreso del pedido: </b> ' . $order->created_at . '<br>
                    <b>Comentarios de experiencia: </b> ' . $user_app_comment->comment . '<br>
                    <b>Dispositivo: </b>Móvil' . $order_group->source;

            $message = new Ticket();
            $message->subject = "asunto: Comentario experiencia app pedido #" . $order->id;
            $message->from = 'de: ' . $order_group->user_firstname . ' ' . $order_group->user_lastname . ' (' . $order_group->user_email . ')';
            $message->from_email = $order_group->user_email;
            $message->from_user = $order_group->user_firstname . ' ' . $order_group->user_lastname;
            $message->to = '';
            $message->date = 'fecha: ' . date('Y-m-d H:i:s');
            $message->body = "Mensaje:<br>" . $html;

            Ticket::createTicket($order, \CustomerService::MAIL_TICKET, $message);
        }

        $this->response = array(
            'status' => true,
            'message' => 'Tu comentario fue guardado, muchas gracias.',
        );

        return $this->jsonResponse();
    }

    /**
     * Guarda respuesta de encuesta para primer pedido
     *
     * @return array $response Respuesta
     */
    public function save_survey()
    {
        if (!$user_id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID usuario es requerido.', 400);
        }

        if (!Input::has('survey_id') || !Input::has('answer') || !Input::has('os') || !Input::has('order_id')) {
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $user_app_survey = new UserSurvey;
        $user_app_survey->survey_id = Input::get('survey_id');
        $user_app_survey->user_id = $user_id;
        $user_app_survey->order_id = Input::get('order_id');
        $user_app_survey->os = Input::get('os');
        $user_app_survey->answer = Input::get('answer');
        $user_app_survey->save();

        $this->response = array(
            'status' => true,
            'message' => 'Tu respuesta fue guardada, muchas gracias.'
        );

        return $this->jsonResponse();
    }

    /**
     * Actualiza numero de celular del usuario como validado
     *
     * @return array $response Respuesta
     */
    public function cellphone_validated()
    {
        if (!$user_id = Route::current()->parameter('id')) {
            return $this->jsonResponse('ID del usuario es requerido.', 400);
        }

        if (!Input::has('cellphone')) {
            return $this->jsonResponse('Celular es requerido.', 400);
        }

        if (!$user = User::find($user_id)) {
            return $this->jsonResponse('El usuario no existe.', 400);
        }

        //error si el celular validado es diferente
        if ($user->phone != Input::get('cellphone')) {
            $this->response['message'] = 'El número celular ingresado es diferente al registrado en tu cuenta.';
            return $this->jsonResponse();
        }

        $user->phone_validated_date = date('Y-m-d H:i:s');
        $user->save();

        User::where('phone', $user->phone)->where('id', '<>', $user->id)->update(['phone_validated_date' => null]);

        $this->response = array(
            'status' => true,
            'message' => 'Tu número celular fue validado con éxito.'
        );

        return $this->jsonResponse();
    }

    /**
     * Determina si el número de celular ya se encuentra en uso.
     *
     * @return mixed $response
     */
    public function cellphone_validation()
    {
        $code = \Input::get('code');

        if (empty($code)) {
            $this->response = [
                'status' => false,
                'message' => 'El parámetro "code" es obligatorio.',
                'result' => []
            ];

            return $this->jsonResponse();
        }

        $user_id = \Input::get('user_id');
        $user = $user_id ? \User::find($user_id) : null;

        try {
            $phone = User::validatePhoneNumber($code, $user);
        } catch (MerqueoException $exception) {
            $this->response = [
                'status' => false,
                'message' => $exception->getMessage(),
                'result' => [
                    'is_valid' => false,
                    'phone' => null,
                ],
            ];

            return $this->jsonResponse();
        }

        $this->response = [
            'status' => true,
            'message' => 'Tu número celular fue validado con éxito.',
            'result' => [
                'is_valid' => true,
                'phone' => $phone,
            ],
        ];

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos por usuario
     *
     * @return array $response Respuesta
     */
    public function get_orders()
    {
        if (!$user_id = Route::current()->parameter('id')) {
            return $this->jsonResponse('El ID usuario es requerido.', 400);
        }

        if (!$user = User::find($user_id)) {
            return $this->jsonResponse('Usuario no existe.', 400);
        }

        $action = Input::get('action');
        $only_pending = $action === self::ACTION_CURRENT_ORDERS;
        $skip_pending = $action === self::ACTION_OLD_ORDERS;
        $countryId = Input::get('country_id');
        try {
            $orders_user = $user->getOrders($only_pending, $skip_pending, $countryId);
        } catch (\Exception $exception) {
            return $this->response = [
                'status' => false,
                'message' => $exception->getMessage(),
            ];
        }

        if ($orders_user instanceof \Illuminate\Pagination\Paginator) {
            if ($action === self::ACTION_CURRENT_ORDERS) {
                $title = $orders_user->count() === 0 ? 'No tienes pedidos activos' : '';
            } else {
                $title = $orders_user->count() === 0 ? 'No tienes historial de pedidos' : '';
            }

            $this->response = [
                'status' => true,
                'message' => 'Pedidos obtenidos',
                'result' => [
                    'title' => $title,
                    'pager' => $orders_user->toArray()
                ]
            ];
        } else {
            $this->response = [
                'status' => true,
                'message' => 'Pedidos obtenidos',
                'result' => $orders_user
            ];
        }


        return $this->jsonResponse();
    }

    /**
     * Obtiene flujo de estado de pedido
     *
     * @return array $response Respuesta
     */
    public function get_order_status()
    {
        $user_id = Route::current()->parameter('user_id');
        $order_id = Route::current()->parameter('order_id');
        if ($user_id && $order_id) {
            $this->response = Order::getStatusFlow($order_id, $user_id, false);
        } else {
            return $this->jsonResponse('El ID usuario es requerido.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene detalle de pedido
     *
     * @return array $response Respuesta
     */
    public function get_order_detail()
    {
        if (!$order_id = Route::current()->parameter('order_id')) {
            return $this->jsonResponse('ID de pedido es requerido.', 400);
        }
        if (!$user_id = Route::current()->parameter('user_id')) {
            return $this->jsonResponse('ID de usuario es requerido.', 400);
        }

        if (!$order = Order::getDetail($user_id, $order_id)) {
            return $this->jsonResponse('El pedido no existe.', 400);
        }

        // El cliente formatea el campo "delivery_date", pero no el campo date.
        if ($this->user_agent === 'Android') {
            $order->date = format_date('normal_short_with_time', $order->date);
        }

        $this->response = array(
            'status' => true,
            'message' => 'Pedido obtenido',
            'result' => $order,
            'hash_code' => generate_hash_order($order->id),
        );

        return $this->jsonResponse();
    }

    /**
     * Actualizar tags de usuario
     *
     * @return array $response Respuesta
     */
    public function get_user_tags()
    {
        $user_id = Route::current()->parameter('id');
        if ($user_id) {
            if (!$user = User::find($user_id)) {
                return $this->jsonResponse('Usuario no existe.', 400);
            }

            $user_average = Order::select(DB::raw("COUNT(orders.id) AS orders_qty,
                        DATE_FORMAT(MIN(orders.delivery_date), '%Y%m%d') AS first_order_date,
                        DATE_FORMAT(MAX(orders.delivery_date), '%Y%m%d') AS last_order_date,
                        ROUND(SUM(user_score) / COUNT(orders.id), 1) AS orders_score_average,
                        ROUND(SUM(orders.total_amount) / COUNT(orders.id)) AS orders_total_average,
                        SUM(IF(payment_method = 'Efectivo', 1, 0)) AS orders_with_cash,
                        SUM(IF(payment_method = 'Datáfono', 1, 0)) AS orders_with_card_reader,
                        SUM(IF(cc_type = 'VISA', 1, 0)) AS orders_with_visa,
                        SUM(IF(cc_type = 'MASTERCARD', 1, 0)) AS orders_with_mastercard,
                        ROUND(SUM(IF(orders.date BETWEEN '" . date('Y-m-d',
                    strtotime('-12 month')) . "' AND '" . date('Y-m-d') . "', 1, 0)) / 12, 1) AS orders_frequency_average"))
                ->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->where('orders.user_id', $user->id)
                ->where('orders.is_hidden', 0)
                //->where('source', '<>', 'Reclamo')
                ->where('orders.status', 'Delivered')
                ->first();

            $tags = array(
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'is_phone_validated' => empty($user->phone_validated_date) ? 0 : 1,
                'first_order_date' => !empty($user_average->first_order_date) ? $user_average->first_order_date : 0,
                'last_order_date' => $user_average->last_order_date,
                'orders_qty' => $user_average->orders_qty,
                'free_delivery_expiration_date' => !empty($user_average->free_delivery_expiration_date) ? date('Ymd',
                    strtotime($user->free_delivery_expiration_date)) : 0,
                'orders_frequency_average' => !empty($user_average->orders_frequency_average) ? $user_average->orders_frequency_average : 0,
                'orders_total_average' => !empty($user_average->orders_total_average) ? $user_average->orders_total_average : 0,
                'orders_score_average' => !empty($user_average->orders_score_average) ? $user_average->orders_score_average : 0,
                'cash' => $user_average->orders_with_cash ? 1 : 0,
                'card_reader' => $user_average->orders_with_card_reader ? 1 : 0,
                'credit_card' => $user_average->orders_with_visa || $user_average->orders_with_mastercard ? 1 : 0,
                'franchise_visa' => $user_average->orders_with_visa ? 1 : 0,
                'franchise_mastercard' => $user_average->orders_with_mastercard ? 1 : 0,
                'send_advertising' => $user->send_advertising
            );

            $this->response = array(
                'status' => true,
                'message' => 'Datos obtenidos',
                'result' => $tags
            );

        } else {
            return $this->jsonResponse('El ID usuario es requerido.', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Retorna los textos que se van a mostrar para el usuario al momento de calificar un pedido entregado
     *
     * @param $id
     * @return string
     */
    public function get_score_order($id)
    {
        if (!Input::has('user_id')) {
            return $this->jsonResponse('El ID usuario es requerido.', 400);
        }

        $order = Order::select('id', 'management_date', 'user_id', 'created_at')
            ->where('user_id', Input::get('user_id'))
            ->with('user')
            ->findOrFail($id);

        $json_response = $order->getOrderScoreFormat();
        $this->response['status'] = true;

        return $this->jsonResponse($json_response, 200);
    }

    /**
     * Almacena la calificación realizada por el usuario.
     *
     * @param $order_id
     * @return string
     * @throws HttpException
     */
    public function save_score_order($order_id)
    {
        // TODO refactorizar.
        $input = Input::all();
        $input['order_id'] = $order_id;
        DB::beginTransaction();

        try {
            $order = Order::validateScoreData($input);
            $order->user_score_source = 'App';
            $order->saveOrderScore($input);
            DB::commit();

        } catch (HttpException $exception) {
            DB::rollback();
            $this->response['status'] = false;

            return $this->jsonResponse($exception->getMessage(), $exception->getStatusCode());
        }

        if (!$order->hasGoodRate()) {
            Ticket::createTicket($order, \CustomerService::TICKET);
        }

        try {
            $management_date = new DateTime($order->management_date);
            $current_date = new DateTime();
            $diff = $current_date->diff($management_date);
            $hours = $diff->h;
            $hours = $hours + ($diff->days * 24);
            if ($hours <= 12)
                Order::getStatusFlow($order->id, $order->user_id, true);
        } catch (Exception $exception) {
        }

        $this->response['status'] = true;
        $message = $order->hasGoodRate()
            ? 'Tu opinión es muy valiosa para nosotros.'
            : 'Recibimos tu mensaje. Pronto te daremos respuesta a tu correo electrónico.';

        return $this->jsonResponse($message, 200);
    }

    /**
     * @return \Illuminate\Http\Response
     */

    public function update_user_receives()
    {
        $request = Input::all();
        $validator = Validator::make($request, [
            'order_id' => 'required|string',
            'name_receives' => 'required|string',
            'phone_receives' => 'required|string'
        ]);
        if ($validator->fails()) {
            return Response::make([
                "errors" => [
                    [
                        "detail" => "Los datos enviados son inválidos, por favor revise el tipo de dato.",
                        "status" => "400",
                        "title" => "Datos Inválidos"
                    ]
                ]
            ], 400);
        }

        $orderId = $request['order_id'];
        $nameReceives = $request['name_receives'];
        $phoneReceives = $request['phone_receives'];

        try{
            $resp = $this->orderWhoReceives->updateWhoReceives($orderId, $nameReceives, $phoneReceives);
            if (!$resp) {
                return Response::make(
                    [
                        "errors" => [
                            [
                                "detail" => "No se pudo guardar, intente nuevamente.",
                                "status" => "400",
                                "title" => "Error al actualizar."
                            ]
                        ],
                    ], 400);
            }

            return Response::make('', 200);
        }catch (\Exception $exception){
            return [
                "errors" => [
                    [
                        "detail" => $exception->getMessage(),
                        "status" => "400",
                        "title" => "Orden $orderId no se encontro",
                    ]
                ]
            ];
        }
    }


    /**
     * @param $userId
     * @param $orderId
     * @return \Illuminate\Http\Response
     */

    public function get_user_receives($userId, $orderId)
    {
        try {
            $userId = (int)$userId;
            $orderId = (int)$orderId;

            $dataOrderReceives = $this->orderWhoReceives->getWhoReceives($orderId, $userId);

            return Response::make([
                "data" => $dataOrderReceives
            ], 200);
        } catch (OrderNotFoundException $exception) {
            $error = [
                "errors" => [
                    [
                        "detail" => $exception->getMessage(),
                        "status" => "400",
                        "title" => "Información de la Orden $orderId del usuario $userId no se encontro"
                    ]
                ]
            ];

            return Response::make($error, 400);
        } catch (\Exception $exception) {
            $error = [
                "errors" => [
                    [
                        "detail" => $exception->getMessage(),
                        "status" => "400",
                        "title" => "Error Inesperado"
                    ]
                ]
            ];

            return Response::make($error, 400);
        }
    }
}
