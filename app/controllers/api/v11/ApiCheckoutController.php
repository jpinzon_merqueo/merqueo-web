<?php

namespace api\v11;

use Carbon\Carbon;
use exceptions\MerqueoException;
use Input, Route, Request, Session, Config, View, Validator, Cart, CartProduct, User, UserAddress, UserCreditCard, UserBrandCampaign,
DateTime, UserCredit, Order, OrderGroup, OrderProduct, Payment, Coupon, UserCoupon, Store, Product, OrderLog, Shopper, DB, UserFreeDelivery,
UserDevice, Survey, Zone, Siftscience;

class ApiCheckoutController extends \ApiController
{
	public $user_credit;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->response = array('status' => false, 'message' => 'Error generico');

        $this->beforeFilter(function() {
        	if (Input::has('user_id'))
        		$this->user_id = Input::get('user_id');
			else $this->user_id = 0;
			$this->user_discounts = User::getDiscounts($this->user_id);
        });
    }

	/**
	 * Obtiene horario de tiendas con base en carrito
	 *
	 * @return array $response Datos de horarios
	 */
	 public function delivery_time()
     {
        $cart = Input::get('cart');
        if (empty($cart))
            return $this->jsonResponse('Datos del carrito es requerido.', 400);

        $product_ids = array();

        $products = json_decode($cart);
        if (count($products)){
            foreach ($products as $id => $product) {
                $product_ids[] = $id;
            }
        }

        $store_ids = array();
        $stores = Product::select('store_id')->whereIn('id', $product_ids)->groupBy('store_id')->get();
        if ($stores){
            foreach($stores as $store)
                $store_ids[] = $store->store_id;
        }
        if (!count($store_ids)) $store_ids[] = 36;

        $validate_slots = true;
        $payment_methods = array();
        $config_payment_methods = $this->config['payment_methods'];
        if (in_array(62, $store_ids)){
            unset($config_payment_methods['Tarjeta de crédito']);
            $validate_slots = count($store_ids) == 1 ? false : true;
        }

        $message_checkout = in_array(36, $store_ids) || in_array(37, $store_ids) ? 'Gánate un 15% de descuento en tu pedido de Merqueo Super programando tu entrega para mañana.' : '';

        foreach ($config_payment_methods as $index => $value)
            $payment_methods[] = array('value' => $value);

        $days = $day_times = $time_week = array();

        $delivery = Store::getDeliverySlotCheckout($store_ids, $validate_slots);
        foreach($delivery['days'] as $value => $text){
            $days[] = array('value' => $value, 'text' => $text);
        }
        foreach($delivery['time'] as $date => $times){
            unset($time);
            foreach($times as $value => $text){
                $time[] = array('value' => $value, 'text' => $text);
            }
            $day_times[$date] = $time;
        }

        $delivery_time = array(
            'message' => $message_checkout,
            'payment_methods' => $payment_methods,
            'days' => $days,
            'time' => $day_times
        );

        $this->response = array(
            'status' => true,
            'message' => 'Horarios de tienda obtenidos.',
            'result' => $delivery_time
        );

        return $this->jsonResponse();
     }

	/**
	 * Procesa nuevo pedido
	 *
	 * @return array $response Respuesta
	 */
	public function checkout()
	{
	    $post_data = Input::all();
    	$sift = new Siftscience;
		try {
			DB::beginTransaction();

			//si no esta logueado el usuario se crea
			if (Input::has('create_user') && !Input::has('user_id'))
			{
				$email =  Input::get('email');
				$password =  Input::get('password');

				if (empty($email) || empty($password))
					return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

				//crear usuario
				$inputs = array(
					'first_name' => Input::get('first_name'),
					'last_name' => Input::get('last_name'),
					'email' => Input::get('email'),
					'password' => Input::get('password'),
					'phone' => Input::get('phone'),
					'do_not_send_mail' => 1
				);
				$request = Request::create('/api/1.1/user', 'POST', $inputs);
				Request::replace($request->input());
				$result = json_decode(Route::dispatch($request)->getContent(), true);
				if (!$result['status']){
					$this->response['message'] = $result['message'];
					return $this->jsonResponse();
				}
				$user = User::find($result['result']['user']['id']);
				$this->user_id = $user->id;
			}else{

				if (!$this->user_id)
					return $this->jsonResponse('ID usuario es requerido.', 400);

				$user = User::find($this->user_id);
				$post_data['first_name'] = $user->first_name;
				$post_data['last_name'] = $user->last_name;
				$post_data['email'] = $user->email;
				if (empty($user->phone) && isset($post_data['user_phone'])){
					$user->phone = $post_data['phone'] = $post_data['user_phone'];
					$user->save();
					unset($post_data['user_phone']);
				}else $post_data['phone'] = $user->phone;
			}
			//$sift->login($user, true);

			$post_data['user_id'] = $user->id;

			//APARTIR DE ESTE PUNTO YA EL USUARIO ESTA AUTENTICADO

			//obtener pedidos del usuario
            $user_has_orders = Order::select('orders.id')->where('user_id', $this->user_id)->where('status', 'Delivered')->orderBy('id', 'desc')->first();

			//si hay información de dirección se valida para crearla
			if (empty($post_data['address_id']))
			{
			    $address = new UserAddress;
				$address->user_id = $this->user_id;
				$address->label = $post_data['address_name'] == 'Otro' && isset($post_data['other_name']) ? $post_data['other_name'] : $post_data['address_name'];
				$address->address = $post_data['address'];
				$address->address_further = isset($post_data['address_further']) ? $post_data['address_further'] : 'Llamar para confirmar';
				$dir = $post_data['dir'];
				$address->address_1 = $dir[0];
				$address->address_2 = $dir[2];
				$address->address_3 = $dir[6];
				$address->address_4 = $dir[8];
				//validar direccion
				$inputs = array(
					'address' => $address->address,
					'city' => $post_data['city']
				);
				$request = Request::create('/api/location', 'GET', $inputs);
				Request::replace($request->input());
				$result = json_decode( Route::dispatch($request)->getContent(), true);
				if (!$result['status']){
					$this->response['message'] = 'No pudimos ubicar tu dirección por favor verificala.';
					return $this->jsonResponse();
				}
                $address->latitude = $result['result']['latitude'];
                $address->longitude = $result['result']['longitude'];
				$address->city_id = $result['result']['city_id'];
                $address->save();
				$address_id = $address->id;
			}else{
				$address_id = $post_data['address_id'];
				$address = UserAddress::find($address_id);
                if (!$address = UserAddress::find($address_id)){
                    $this->response['message'] = 'Ocurrió un error al procesar los datos de tu dirección por favor verificala.';
                    return $this->jsonResponse();
                }
				$post_data['address'] = $address->address;
				$post_data['address_further'] = $address->address_further;
			}

			//obtener carrito
			$cart = Cart::where('user_id', $this->user_id)->orderBy('updated_at', 'desc')->first();
			//si no existe entonces creo un carrito
			if (!$cart) {
				$cart = new Cart;
				$cart->user_id = $this->user_id;
				$cart->save();
			}

	        //crear carrito en bd
	        if (isset($post_data['cart'])){
	            $products = json_decode($post_data['cart']);
                if (count($products)){
                    CartProduct::where('cart_id', $cart->id)->delete();
					foreach ($products as $id => $product) {
						if ($product->qty <= 0) continue;
						$cart_product = new CartProduct;
						$cart_product->cart_id = $cart->id;
						$cart_product->product_id = $id;
						$cart_product->quantity = $product->qty;
                        if ($id < 0){
                            $cart_product->product_name = $product->name;
                            $cart_product->store_id = $product->store_id;
                            $cart_product->comment = $product->comment;
                        }
                        $cart_product->price = $product->price;
						$cart_product->added_by = $this->user_id;
						$cart_product->save();
					}
				}
			}

            //validar metodo de pago
			$post_data['payment_method'] = $post_data['payment_method'] == 'Tarjeta de Crédito' ? 'Tarjeta de crédito' : $post_data['payment_method'];
			$is_credit_card = $post_data['payment_method'] == 'Tarjeta de crédito' ? true : false;
			if ($is_credit_card)
			{
				$payment = new Payment;

                //si el usuario no esta creado en la api se crea
				$create_user_api = false;
				if (empty($user->customer_token))
					$create_user_api = true;
				else $post_data['customer_token'] = $user->customer_token;

				if ($create_user_api){
					$result = $payment->createCustomer($post_data);
					if (!$result['status']){
						$this->response['message'] = 'Tus datos personales no pasaron la validación de la tarjeta de crédito.';
						return $this->jsonResponse();
					}
					$user->customer_token = $post_data['customer_token'] = $result['response']->id;
					$user->save();
				}

				//si la tarjeta es nueva se guarda y asocia a cliente en api
				$credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
				if (empty($credit_card_id))
				{
				    //validar pais de tarjeta de credito
                    $bin = substr($post_data['number_cc'], 0, 6);
                    $credit_card = $payment->getCreditCardInfo($bin, $post_data);
                    if (!$credit_card){
                        $this->response['message'] = 'Ocurrió un problema al validar el origen de tu tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    if (!$credit_card->is_valid){
                        $this->response['message'] = 'Solo se aceptan tarjetas de crédito nacionales.';
                        return $this->jsonResponse();
                    }

					$result = $payment->associateCreditCard($post_data);
					if (!$result['status']){
						$this->response['message'] = 'Tuvimos un problema con la validación de los datos de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
						return $this->jsonResponse();
					}
                    if (!in_array($result['response']->type, $this->config['credit_cards_types'])){
						$this->response['message'] = 'Solo se aceptan tarjetas de crédito '.$this->config['credit_cards_message'];
						return $this->jsonResponse();
	                }

					$post_data['card_token'] = $result['response']->id;
					//guardar tarjeta en bd
					$user_credit_card = new UserCreditCard;
					$user_credit_card->user_id = $user->id;
					$user_credit_card->card_token = $post_data['card_token'];
                    $user_credit_card->holder_name = $post_data['name_cc'];
					$user_credit_card->last_four = $result['response']->lastFour;
					$user_credit_card->type = $result['response']->type;
                    $user_credit_card->country = $credit_card->country_name;
					$user_credit_card->created_at = $user_credit_card->updated_at = date('Y-m-d H:i:s');
					$user_credit_card->save();
					//$sift->create_user($user, null, true, true);
				}else{
					$user_credit_card = UserCreditCard::find($credit_card_id);
					if (!$user_credit_card){
						$this->response['message'] = 'Ocurrió un error al obtener información de la tarjeta de crédito.';
						return $this->jsonResponse();
					}
				    $post_data['card_token'] = $user_credit_card->card_token;
				}
                $credit_card_type = $user_credit_card->type;
			}

			$cart_products = DB::table('cart_products')
								->where('cart_id', $cart->id)
								->leftJoin('products', 'cart_products.product_id', '=', 'products.id')
								->leftJoin('users', 'cart_products.added_by', '=', 'users.id')
								->select('products.*', 'cart_products.price AS cart_price', 'cart_products.quantity AS cart_quantity', 'cart_products.product_id',
                                'cart_products.product_name AS cart_product_name', 'cart_products.comment AS cart_comment', 'cart_products.store_id AS cart_store_id')
								->orderBy('store_id')
								->get();

            $response['cart']['delivery_amount'] = 0;
			$response['cart']['total_amount'] = 0;
            $response['cart']['discount_amount'] = 0;
			$response['cart']['total_quantity'] = 0;
			$response['cart']['stores'] = array();

            $store_id = 0;
			if ($cart_products)
			{
			    $products = json_decode($post_data['cart'], true);
			    $response['cart']['is_minimum_reached'] = 1;
				foreach ($cart_products as $cart_product)
				{
				    if (!$cart_product->id  && empty($cart_product->cart_product_name)){
				        if (isset($products[$cart_product->product_id]) && isset($products[$cart_product->product_id]['name']) && !empty($products[$cart_product->product_id]['name']))
                            $this->response['message'] = 'El producto '.$products[$cart_product->product_id]['name'].' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        else $this->response['message'] = 'El producto con cantidad '.$cart_product->cart_quantity.' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        return $this->jsonResponse();
				    }

                    $store_id = !$cart_product->id ? $cart_product->cart_store_id : $cart_product->store_id;
					//validar producto en promocion en pedidos anteriores en merqueo super
                    if ($store_id == 36 || $store_id == 37){
    					$count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
    									->where('product_id', '=', $cart_product->id)
    									->where('price', '=', $cart_product->special_price)
    									->where('user_id', '=', $this->user_id)
    									->where('status', '<>', 'Cancelled')
    									->where(DB::raw("DATEDIFF('".date('Y-m-d')."', date)"), '<=', 8)
    									->groupBy('order_products.store_product_id')->count();
    					if ($count_products){
    						$this->response['message'] = 'Ya realizaste un pedido con el producto '.$cart_product->name.' en promoción, por favor eliminalo del carrito de compras para continuar.';
    						return $this->jsonResponse();
    					}
					}

                    //validar precio especial
                    /*$original_price = $cart_product->special_price ? $cart_product->special_price : $cart_product->price;
                    if ($original_price > $cart_product->cart_price){
                        $this->response['message'] = 'El precio del producto '.$cart_product->name.' ha cambiado, por favor eliminalo del carrito y vuelve a agregarlo.';
                        return $this->jsonResponse();
                    }*/

                    //validar precio especial y cantidad del producto
                    if ($cart_product->cart_quantity > 0 && $cart_product->special_price > -1){
                        //validar primera compra
                        if ($user_has_orders && $cart_product->first_order_special_price){
                            $this->response['message'] = 'La promoción del producto '.$cart_product->name.' aplica solo para primera compra, por favor eliminalo del carrito para continuar.';
                            return $this->jsonResponse();
                        }
                        //validar cantidad del producto por pedido
                        if ($cart_product->quantity_special_price){
                            if ($cart_product->cart_quantity > $cart_product->quantity_special_price){
                                $this->response['message'] = 'El producto '.$cart_product->name.' por estar en promoción puedes agregar máximo '.$cart_product->quantity_special_price.' unidades en tu pedido, por favor disminuye la cantidad en el carrito para continuar.';
                                return $this->jsonResponse();
                            }
                        }
                    }

                    if (!isset($response['cart']['stores'][$store_id]))
                    {
						$tmp_store = Store::find($store_id);
						$store = $tmp_store->toArray();
						$store['count'] = 1;
						$store['sub_total'] = 0;
						$response['cart']['stores'][$store_id] = $store;
						$response['cart']['stores'][$store_id]['products'] = array();
						$response['cart']['stores'][$store_id]['delivery_amount'] = $store['delivery_order_amount'];
						$response['cart']['stores'][$store_id]['discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['sub_total'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_time_minutes'] = $store['delivery_time_minutes'];
                        $response['cart']['stores'][$store_id]['original_object'] = $tmp_store;
					}else $response['cart']['stores'][$store_id]['count'] += 1;

	                $sub_total = $cart_product->cart_quantity * $cart_product->cart_price;
					$response['cart']['stores'][$store_id]['sub_total'] += $sub_total;
					$response['cart']['total_amount'] += $sub_total;
					$response['cart']['total_quantity']++;
					$response['cart']['stores'][$store_id]['products'][] = (array) $cart_product;
				}

                //domicilio gratis para pagos con mastercard y pedido mayor a 110000
                /*if ($is_credit_card && $credit_card_type == 'MASTERCARD' && $response['cart']['total_amount'] > 110000){
                    $response['cart']['delivery_amount'] = 0;
                    foreach($response['cart']['stores'] as $store_id => $store){
                        $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                    }
                }*/
			}

            $total_cart = $response['cart']['total_amount'] + $response['cart']['delivery_amount'];

            if (isset($this->user_discounts['coupon']))
            {
                $coupon = $this->user_discounts['coupon'];
                $data = array('coupon_code' => $coupon->code, 'cart' => $post_data['cart'], 'validate' => 1);
                if (isset($post_data['create_user']))
                    $data['is_new_user'] = true;
                else if (isset($post_data['user_id']))
                    $data['user_id'] = $post_data['user_id'];
                $request = Request::create('/api/1.1/coupon', 'POST', $data);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if (!$result['status']){
                    $this->response['message'] = $result['message'];
                    return $this->jsonResponse();
                }
                //si el cupon cumple con totales requerido
                if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $total_cart < $coupon->minimum_order_amount){
                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el total del pedido debe ser mayor o igual a $'.number_format($coupon->minimum_order_amount, 0, ',', '.');
                    return $this->jsonResponse();
                }else if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $total_cart > $coupon->maximum_order_amount){
                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el total del pedido debe ser menor o igual a $'.number_format($coupon->maximum_order_amount, 0, ',', '.');
                    return $this->jsonResponse();
                }else if ($coupon->minimum_order_amount && $coupon->maximum_order_amount && ($total_cart < $coupon->minimum_order_amount || $total_cart > $coupon->maximum_order_amount)){
                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->coupon.' el total del pedido debe estar entre $'.number_format($coupon->minimum_order_amount, 0, ',', '.').' y $'.number_format($coupon->maximum_order_amount, 0, ',', '.');
                    return $this->jsonResponse();
                }

                $discount_amount = 0;
                //validar cupon para tienda / categoria / subcategoria / producto especifico
                if ($coupon->redeem_on == 'Specific Store')
                {
                    foreach($cart_products as $product){
                        $is_valid = false;
                        if ($product->store_id == $coupon->store_id){
                            if ($coupon->department_id){
                                if ($product->department_id == $coupon->department_id){
                                    if ($coupon->shelf_id){
                                        if ($product->shelf_id == $coupon->shelf_id){
                                            if ($coupon->product_id){
                                                if ($product->id == $coupon->product_id)
                                                    $is_valid = true;
                                            }else $is_valid = true;
                                        }
                                    }else $is_valid = true;
                                }
                            }else $is_valid = true;
                        }
                        if ($is_valid){
                            if ($coupon->type == 'Discount Percentage')
                                $discount_amount += round(($product->price * $product->cart_quantity) * ($coupon->amount / 100), 0);
                            if ($coupon->type == 'Credit Amount')
                                $discount_amount += round($product->price * $product->cart_quantity, 0);
                        }
                    }
                    $store = Store::select('stores.name AS store_name', 'departments.name AS department_name', 'shelves.name AS shelf_name', 'products.name AS product_name')
                                  ->leftJoin('departments', function($join) use ($coupon){
                                      $join->on('departments.store_id', '=', 'stores.id');
                                      $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                                  })
                                  ->leftJoin('shelves', function($join) use ($coupon){
                                      $join->on('shelves.department_id', '=', 'departments.id');
                                      $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                                  })
                                  ->leftJoin('products', function($join) use ($coupon){
                                      $join->on('products.shelf_id', '=', 'shelves.id');
                                      $join->on('products.id', '=', DB::raw($coupon->product_id ? $coupon->product_id : 0));
                                  })
                                  ->where('stores.id', $coupon->store_id)
                                  ->first();
                    $discount_message = $store->store_name;
                    if (!empty($store->department_name))
                        $discount_message .= ' / '.$store->department_name;
                    if (!empty($store->shelf_name))
                        $discount_message .= ' / '.$store->shelf_name;
                    if (!empty($store->product_name))
                        $discount_message .= ' / '.$store->product_name;

                    if ($discount_amount){
                        if ($coupon->type == 'Discount Percentage')
                            $response['cart']['stores'][$coupon->store_id]['discount_percentage_amount'] = $coupon->amount;
                        else if ($discount_amount > $coupon->amount)
                                $discount_amount = $coupon->amount;
                        $response['cart']['stores'][$coupon->store_id]['discount_amount'] = $discount_amount;
                        $response['cart']['discount_amount'] += $discount_amount;
                    }else{
                        $this->response['message'] = 'El cupón de descuento '.$coupon->code.' aplica exclusivamente para productos de la tienda '.$discount_message;
                        return $this->jsonResponse();
                    }

                    $response['cart']['stores'][$coupon->store_id]['discount_message'] = $discount_message;
                }
                //validar metodo de pago y tipo de tarjeta
                if ($coupon->payment_method){
                    if (($is_credit_card && $coupon->payment_method != 'Tarjeta de crédito' && $coupon->payment_method != $credit_card_type)
                    || (!$is_credit_card && $coupon->payment_method != $post_data['payment_method'])){
                        $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el metodo de pago debe ser con '.$this->config['payment_methods_names'][$coupon->payment_method].trim(' '.$coupon->cc_bank);
                        return $this->jsonResponse();
                    }else{
                        if ($is_credit_card && !empty($coupon->cc_bin)){
                            $result = $payment->retriveCreditCard($user->customer_token, $user_credit_card->card_token, $user->id);
                            if ($result['status']){
                                $bin = $result['response']->bin;
                                $allowed_bines = explode(',', $coupon->cc_bin);
                                if (!in_array($bin, $allowed_bines)){
                                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el metodo de pago debe ser con '.$this->config['payment_methods_names'][$coupon->payment_method].' '.$coupon->cc_bank;
                                    return $this->jsonResponse();
                                }
                            }else{
                                $this->response['message'] = 'No pudimos obtener información de tu tarjeta de crédito para la validación del cupón, por favor verificala e intenta de nuevo.';
                                return $this->jsonResponse();
                            }
                         }
                    }
                }
                //si el cupon no es de productos especificos se aplica el cupon
                if ($coupon->redeem_on != 'Specific Store'){
                    if ($coupon->type == 'Credit Amount'){
                        if ($coupon->amount > $response['cart']['total_amount'])
                            $coupon->amount = $response['cart']['total_amount'];
                        $discount_credit_amount = $coupon->amount;
                    }
                    if ($coupon->type == 'Discount Percentage')
                        $discount_percentage = $coupon->amount;
                }
            }
            //si no tiene tiene cupon activo y si el total es mayor al requerido se aplica descuento con credito
            if (!isset($this->user_discounts['coupon']) && $this->user_discounts['amount'] && $total_cart > $this->user_discounts['minimum_discount_amount'])
            {
                if ($this->user_discounts['amount'] > $response['cart']['total_amount'])
                    $this->user_discounts['amount'] = $response['cart']['total_amount'];
                $discount_credit_amount = $this->user_discounts['amount'];
            }

            //validar y cargar codigo de referido
            $post_data['coupon_code'] = isset($post_data['coupon_code']) ? $post_data['coupon_code'] : null;
            $referred = $this->validateReferred($user, $post_data['coupon_code']);

            $delivery_time_minutes = 0;

            //domicilio a mitad de precio si hay mas de una tienda
            ksort($response['cart']['stores']);
            if (count($response['cart']['stores']) > 1)
                $first_store_id = current(array_keys($response['cart']['stores']));

            //obtener descuentos activos en checkout
            $checkout_discounts = Order::getCheckoutDiscounts($address->city_id);
            $delivery_day = $post_data['delivery_day'];

	        foreach ($response['cart']['stores'] as $store_id => $store)
			{
                try {
                    $delivery_window = $store['original_object']->validateStoreDeliversToAddress(
                        $address->latitude,
                        $address->longitude,
                        new Carbon($delivery_day),
                        $post_data['delivery_time']
                    );
                } catch (MerqueoException $exception) {
                    $this->response['message'] = $exception->getMessage();
                    return $this->jsonResponse();
                }

                //validar domicilio gratis global
                if ($checkout_discounts['free_delivery']['status']){
                    if (!empty($checkout_discounts['free_delivery']['store_ids'])){
                        $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                        $store_ids = count($store_ids) > 1 ? $store_ids : array($store_id);
                        if (in_array($store_id, $store_ids) && $total_cart > $checkout_discounts['free_delivery']['minimum_order_amount'])
                            $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                    }else{
                        if (!empty($checkout_discounts['free_delivery']['city_ids'])){
                            $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                            $city_ids = count($city_ids) > 1 ? $city_ids : array($checkout_discounts['free_delivery']['city_ids']);
                            if (in_array($response['cart']['stores'][$store_id]['city_id'], $city_ids) && $total_cart > $checkout_discounts['free_delivery']['minimum_order_amount'])
                                $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                        }
                    }
                }

                //sino hay cupon activo
                if (!isset($this->user_discounts['coupon']))
                {
                    //validar descuento de credito global
                    if ($checkout_discounts['discount_credit']['status']){
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_credit']['minimum_order_amount'] && !$checkout_discounts['discount_credit']['maximum_order_amount'] && $total_cart < $checkout_discounts['discount_credit']['minimum_order_amount'])
                            $is_valid = false;
                        else if ($checkout_discounts['discount_credit']['maximum_order_amount'] && !$checkout_discounts['discount_credit']['minimum_order_amount'] && $total_cart > $checkout_discounts['discount_credit']['maximum_order_amount'])
                            $is_valid = false;
                        else if ($checkout_discounts['discount_credit']['minimum_order_amount'] && $checkout_discounts['discount_credit']['maximum_order_amount'] &&
                                ($total_cart < $checkout_discounts['discount_credit']['minimum_order_amount'] || $total_cart > $checkout_discounts['discount_credit']['maximum_order_amount']))
                            $is_valid = false;

                        if ($is_valid){
                            $discount_global_credit_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_credit']['store_id']){
                                foreach($store['products'] as $product)
                                {
                                    $product = (object)$product;
                                    $product_price = empty($product->special_price) ? $product->price : $product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_credit']['store_id']){
                                        if ($checkout_discounts['discount_credit']['department_id']){
                                            if ($product->department_id == $checkout_discounts['discount_credit']['department_id']){
                                                if ($checkout_discounts['discount_credit']['shelve_id']){
                                                    if ($product->shelf_id == $checkout_discounts['discount_credit']['shelve_id']){
                                                        if ($checkout_discounts['discount_credit']['product_id']){
                                                            if ($product->id == $checkout_discounts['discount_credit']['product_id'])
                                                                $is_valid = true;
                                                        }else $is_valid = true;
                                                    }
                                                }else $is_valid = true;
                                            }
                                        }else $is_valid = true;
                                    }
                                    if ($is_valid)
                                        $discount_global_credit_amount += round($product_price * $product->cart_quantity, 0);
                                }
                            }else $discount_global_credit_amount += $checkout_discounts['discount_credit']['amount'];

                            if ($discount_global_credit_amount){
                                if ($discount_global_credit_amount > $checkout_discounts['discount_credit']['amount'])
                                    $discount_global_credit_amount = $checkout_discounts['discount_credit']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_credit_amount;
                                $response['cart']['discount_amount'] += $discount_global_credit_amount;
                            }
                        }
                    }

                    //validar descuento por porcentaje global
                    if ($checkout_discounts['discount_percentage']['status']){
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_percentage']['minimum_order_amount'] && !$checkout_discounts['discount_percentage']['maximum_order_amount'] && $total_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'])
                            $is_valid = false;
                        else if ($checkout_discounts['discount_percentage']['maximum_order_amount'] && !$checkout_discounts['discount_percentage']['minimum_order_amount'] && $total_cart > $checkout_discounts['discount_percentage']['maximum_order_amount'])
                            $is_valid = false;
                        else if ($checkout_discounts['discount_percentage']['minimum_order_amount'] && $checkout_discounts['discount_percentage']['maximum_order_amount'] &&
                                ($total_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'] || $total_cart > $checkout_discounts['discount_percentage']['maximum_order_amount']))
                            $is_valid = false;

                        if ($is_valid){
                            $discount_global_percentage_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_percentage']['store_id']){
                                foreach($store['products'] as $product)
                                {
                                    $product = (object)$product;
                                    $product_price = empty($product->special_price) ? $product->price : $product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_percentage']['store_id']){
                                        if ($checkout_discounts['discount_percentage']['department_id']){
                                            if ($product->department_id == $checkout_discounts['discount_percentage']['department_id']){
                                                if ($checkout_discounts['discount_percentage']['shelve_id']){
                                                    if ($product->shelf_id == $checkout_discounts['discount_percentage']['shelve_id']){
                                                        if ($checkout_discounts['discount_percentage']['product_id']){
                                                            if ($product->id == $checkout_discounts['discount_percentage']['product_id'])
                                                                $is_valid = true;
                                                        }else $is_valid = true;
                                                    }
                                                }else $is_valid = true;
                                            }
                                        }else $is_valid = true;
                                    }
                                    if ($is_valid)
                                        $discount_global_percentage_amount += round(($product_price * $product->cart_quantity) * ($checkout_discounts['discount_percentage']['amount'] / 100), 0);
                                }
                            }else $discount_global_percentage_amount += round($response['cart']['total_amount'] * ($checkout_discounts['discount_percentage']['amount'] / 100), 0);

                            if ($checkout_discounts['discount_percentage']['order_for_tomorrow'])
                                if (!$delivery_day) $discount_global_percentage_amount = 0;
                                else if ($delivery_day < date('Y-m-d', strtotime('+1 day'))) $discount_global_percentage_amount = 0;

                            if ($discount_global_percentage_amount){
                                $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $checkout_discounts['discount_percentage']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_percentage_amount;
                                $response['cart']['discount_amount'] += $discount_global_percentage_amount;
                            }
                        }
                    }
                }

                //domicilio gratis en proximo pedido
			    if ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order'])
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                else if (isset($first_store_id) && $store_id != $first_store_id && $store['delivery_amount'])
                        $response['cart']['stores'][$store_id]['delivery_amount'] = round($store['delivery_amount'] / 2, 0);
                $response['cart']['delivery_amount'] += $response['cart']['stores'][$store_id]['delivery_amount'];

			    if ($store['sub_total'] < $store['minimum_order_amount'])
					$response['cart']['is_minimum_reached'] = 0;

				//credito
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                && isset($discount_credit_amount) && $discount_credit_amount){
                    $total_order_store = $response['cart']['stores'][$store_id]['sub_total'] + $response['cart']['stores'][$store_id]['delivery_amount'];
                    $discount_amount = $discount_credit_amount > $total_order_store ? $total_order_store : $discount_credit_amount;
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['discount_amount'] += $discount_amount;
                    $discount_credit_amount -= $discount_amount;
                }
                //porcentaje
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                && isset($discount_percentage) && $discount_percentage){
                    $discount_amount = round(($response['cart']['stores'][$store_id]['sub_total'] * $discount_percentage) / 100, 2);
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $discount_percentage;
                    $response['cart']['discount_amount'] += $discount_amount;
                }
                //tiempo de entrega inmediata
                if ($response['cart']['stores'][$store_id]['delivery_time_minutes'] > $delivery_time_minutes)
                    $delivery_time_minutes = $response['cart']['stores'][$store_id]['delivery_time_minutes'];
			}

	        if (isset($response['cart']['is_minimum_reached']) && $response['cart']['is_minimum_reached'] == 1)
			{
			    //cupon utilizado
                if (isset($coupon)){
                    //cargar cupon como utilizado a usuario
                    $user_coupon = new UserCoupon;
                    $user_coupon->coupon_id = $coupon->id;
                    $user_coupon->user_id = $this->user_id;
                    $user_coupon->save();
                    //sumar credito de cupon a usuario
                    $user_credit = new UserCredit;
                    $user_credit->user_id = $user->id;
                    $user_credit->coupon_id = $coupon->id;
                    $user_credit->amount = $response['cart']['discount_amount'];
                    $user_credit->type = 1;
                    $user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por redimir cupón código '.$coupon->code;
                    $user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
                    $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                    $user_credit->save();

                    if (!$user->first_coupon_used && $coupon->type_use == 'Only The First Order'){
                        $user->first_coupon_used = 1;
                        $user->save();
                    }
                }

                //obtener zona
                $zone_id = null;
                $zones = Zone::where('city_id', $address->city_id)->where('status', 1)->get();
                if ($zones){
                    foreach ($zones as $zone){
                        if (point_in_polygon($zone->delivery_zone, $address->latitude, $address->longitude)){
                            $zone_id = $zone->id;
                            break;
                        }
                    }
                }

				//crea un nuevo grupo para este pedido
				$order_group = new OrderGroup;

				//realizar cargo a tarjeta
				/*if ($is_credit_card)
				{
					$post_data_validation = $post_data;
					$post_data_validation['total_amount'] = 1000;
					$post_data_validation['tax_amount'] = 0;
					$post_data_validation['installments_cc'] = 1;
					$post_data_validation['order_id'] = 1;
					$post_data_validation['description'] = 'Validación de tarjeta de crédito';
					$post_data_validation['nit'] = 'No especificado';
					$result = $payment->createCreditCardCharge($post_data_validation);

					if (!$result['status']){
						$this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
						return $this->jsonResponse();
					}

					$result = $payment->refundCreditCardCharge($user->id, $result['response']->id);
					if (!$result['status']){
						$this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
						return $this->jsonResponse();
					}
				}*/

                //validar datos de fraude
                $posible_fraud = $this->validateOrderFraud($post_data);
                $posible_fraud = empty($posible_fraud) ? 0 : $posible_fraud;

                //asociar dispositivo a usuario
                if ($user_device = UserDevice::find($this->user_device_id)){
                    $user_device->user_id = $this->user_id;
                    $user_device->save();
                }

				$order_group->source = 'Device';
                if (isset($post_data['source_os']))
                    $order_group->source_os = $post_data['source_os'];
				$order_group->user_device_id = $this->user_device_id;
                $order_group->user_id = $this->user_id;
				$order_group->user_firstname = $user->first_name;
				$order_group->user_lastname = $user->last_name;
				$order_group->user_email = $user->email;
				$order_group->user_address = $address->address.' '.$address->address_further;
                $order_group->user_address_latitude = $address->latitude;
                $order_group->user_address_longitude = $address->longitude;
				$order_group->user_city_id = $address->city_id;
				$order_group->user_phone = $user->phone;
				$order_group->address_id = $address_id;
                $order_group->zone_id = $zone_id;
				$order_group->discount_amount = $response['cart']['discount_amount'];
				$order_group->delivery_amount = $response['cart']['delivery_amount'];
				$order_group->total_amount = $response['cart']['total_amount'];
				$order_group->products_quantity = $response['cart']['total_quantity'];
				$order_group->user_comments = trim($post_data['comments'].' '.$post_data['comments_further']);
                $order_group->ip = get_ip();
                $order_group->save();

				//dia y hora de entrega

				if ((isset($post_data['delivery_time_today']) && $post_data['delivery_time_today'] == 'immediately') ||
				    (isset($post_data['delivery_time']) && $post_data['delivery_time'] == 'immediately')){
					$hour_today = new DateTime();
                    $hour_today->modify('+'.$delivery_time_minutes.' minutes');
                    $delivery_time = $hour_today->format('H:i:s');
                    $delivery_time_text = 'Immediately';
                    $real_delivery_date = $post_data['delivery_day'].' '.$delivery_time;
				}else{
					$delivery_time = isset($post_data['delivery_time_today']) && !empty($post_data['delivery_time_today']) ? $post_data['delivery_time_today'] : $post_data['delivery_time'];
					$delivery_time = explode(' - ', $delivery_time);
					$delivery_time_begin = explode(':', $delivery_time[0]);
					$hour_begin = new DateTime();
					$hour_begin->setTime($delivery_time_begin[0], $delivery_time_begin[1]);

					$delivery_time_end = explode(':', $delivery_time[1]);
					$hour_end = new DateTime();
					$hour_end->setTime($delivery_time_end[0], $delivery_time_end[1]);

					$delivery_time_text = $hour_begin->format('g:i a').' - '.$hour_end->format('g:i a');
					$hour_begin = $post_data['delivery_day'].' '.$delivery_time[0];
					if ($hour_end->format('G') < 3)
					   $post_data['delivery_day'] = date('Y-m-d', strtotime($post_data['delivery_day'] . '+1 day'));

					$hour_end = $real_delivery_date = $post_data['delivery_day'].' '.$delivery_time[1];
					$delivery_time = get_delivery_time($hour_begin, $hour_end);
				}
				//inicializar variables para validar campañas de marca
                //$is_valid_store = false;
                //$allowed_stores = [36, 10];

                $order_group_total_amount = 0;

				//crea un pedido por cada tienda
				foreach ($response['cart']['stores'] as $store_id => $store)
				{
					$order = new Order;
					$order->reference = generate_reference();
					$order->user_id = $this->user_id;
					$order->store_id = $store_id;
					$order->date = date("Y-m-d H:i:s");
					$order->group_id = $order_group->id;
					$order->total_products = count($store['products']);
					$order->delivery_amount = $store['delivery_amount'];
					$order->discount_amount = $store['discount_amount'];
					$order->total_amount = $store['sub_total'];

					$status = $this->validate_history_order_payment($post_data);
					if($status)
						$order->status ='Initiated';
					else
						$order->status = $post_data['payment_method'] == 'Tarjeta de crédito' || $posible_fraud ? 'Validation' : 'Initiated';


					$order->user_score_token = generate_token();
                    $order->delivery_date = $post_data['delivery_day'].' '.$delivery_time;
                    $order->real_delivery_date = $real_delivery_date;
                    $order->delivery_time = $delivery_time_text;
                    $order->payment_method = $post_data['payment_method'];
                    $order->posible_fraud = $posible_fraud;

                    if (isset($store['discount_percentage_amount']))
                        $order->discount_percentage_amount = $store['discount_percentage_amount'];

                    if ($is_credit_card){
                        //guardar datos de tarjeta
                        $order->credit_card_id = $user_credit_card->id;
                        $order->cc_token = $user_credit_card->card_token;
                        $order->cc_holder_name = $user_credit_card->holder_name;
                        $order->cc_last_four = $user_credit_card->last_four;
                        $order->cc_type = $user_credit_card->type;
                        $order->cc_country = $user_credit_card->country;
                        $order->cc_installments = $post_data['installments_cc'] ? $post_data['installments_cc'] : 1;
                    }

                    //si es merqueo de la noche asignar shopper y cambiar estado
                    if ($order->store_id == 62)
                    {
                        $order->shopper_id = 313;
                        $order->status = 'In Progress';
                        $order->received_date = date('Y-m-d H:i:s');
                        $order->shopper_delivery_time_minutes = $store['delivery_time_minutes'];
                        //enviar mail
                        $delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_with_time', $order->delivery_date) : format_date('normal', substr($order->delivery_date, 0, 10)).' '.$order->delivery_time;
                        $html = 'Hay un nuevo pedido para entregar.<br><br>
                        <b>Referencia: </b> '.$order->reference.'<br>
                        <b>Cliente: </b> '.$order_group->user_firstname.' '.$order_group->user_lastname.'<br>
                        <b>Dirección: </b> '.$order_group->user_address.'<br>
                        <b>Fecha de entrega: </b> '.$delivery_date.'<br>
                        <b>Total a pagar: </b> $'.number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount), 0, ',', '.').'<br><br>
                        Por favor ingresa al <a href="https://merqueo.com/admin" target="_blank">administrador</a> para ver detalles.<br><br>';
                        $mail = array(
                            'template_name' => 'emails.daily_report',
                            'subject' => 'Nuevo pedido Merqueo de la noche',
                            'to' => array(
                                array('email' => 'gbahamon@teletrade.com.co', 'name' => 'Gilberto Bahhamon'),
                                array('email' => 'domicilios@teletrade.com.co', 'name' => 'Domicilios'),
                                array('email' => 'smontejo@merqueo.com', 'name' => 'Silvia Montejo'),
                            ),
                            'vars' => array(
                                'title' => 'Nuevo pedido en Merqueo de la noche',
                                'html' => $html
                            )
                        );
                        send_mail($mail);
                    }

					$order->save();

					$sift_order = $order->toArray();
                    $sift_order['user_obj'] = $user->toArray();
                    $sift_order['address_obj'] = $address->toArray();

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'ORDER_CREATED';
                    $log->user_id = $this->user_id;
                    $log->order_id = $order->id;
                    $log->save();

                    $order_total_amount = 0;

					foreach ($cart_products as $cart_product)
					{
						if ($cart_product->id)
                            $product = Product::find($cart_product->id);
                        else{
                            $product = (object) array(
                                'id' => 0,
                                'special_price' => null,
                                'price' => 0,
                                'name' => $cart_product->cart_product_name,
                                'image_medium_url' => image_url().'no_disponible.jpg',
                                'comment' => $cart_product->cart_comment,
                                'quantity' => 1,
                                'unit' => 'Unid',
                                'promo_type' => null,
                                'promo_type_special_price' => 0,
                                'store_id' => $cart_product->cart_store_id,
                                'total_sale' => 0,
                                'type' => 'Product'
                            );
                        }
						//no deja que al pedido le aparezcan productos que no pertenecen a la tienda
						if ($product->store_id != $store_id) continue;

						$order_product = new OrderProduct;
						$order_product->order_id = $order->id;
						$order_product->product_id = $product->id;
						$price = $product->special_price ? $product->special_price : $cart_product->cart_price;
						$order_product->price = $price;
						$order_product->original_price = $product->price;
						$order_product->quantity = $cart_product->cart_quantity;
                        $order_product->promo_type = $product->promo_type_special_price;
						$order_product->fulfilment_status = 'Pending';
						$order_product->type = $product->id ? 'Product' : 'Custom';
						$order_product->parent_id = 0;
						$order_product->product_name = $product->name;
						$order_product->product_image_url = $product->image_medium_url;
						$order_product->product_quantity = $product->quantity;
						$order_product->product_unit = $product->unit;
						$order_product->store_id = $product->store_id;
                        $order_product->product_comment = $cart_product->cart_comment;
						$order_product->save();

                        $order_total_amount += $order_product->price * $order_product->quantity;

						$sift_order['products'][] = $order_product->toArray();

						$products_mail[$order->id][] = array(
							'img' => $product->image_medium_url,
							'name' => $product->name,
							'qty' => $cart_product->cart_quantity,
							'price' => $price,
							'total' => $cart_product->cart_quantity * $price,
							'type' => $product->type
						);

                        if (!isset($product_found))
                            if (strpos(strtolower($product->name), 'chivas'))
                                $product_found = true;
					}

					$sift_order_info[] = $sift_order;

                    if ($order->total_amount != $order_total_amount){
                        $order->total_amount = $order_total_amount;
                        $order->save();
                    }
                    $order_group_total_amount += $order_total_amount;

                    //verificar condiciones de campaña de marca
                    /*if (!$is_valid_store){
                        if (in_array($store_id, $allowed_stores) && !isset($product_found)){
                            $is_valid_store = true;
                            if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'lavomatic')->first()) {
                                $user_brand_campaign = new UserBrandCampaign;
                                $user_brand_campaign->user_id = $this->user_id;
                                $user_brand_campaign->order_id = $order->id;
                                $user_brand_campaign->campaign = 'lavomatic';
                                $user_brand_campaign->save();
                            }
                        }
                    }*/
					//verificar condiciones de campaña de marca
                    if ($address->city_id == 1 && isset($product_found) && $product_found) {
                        if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'mini chivas')->first()) {
                            $user_brand_campaign = new UserBrandCampaign;
                            $user_brand_campaign->user_id = $this->user_id;
                            $user_brand_campaign->order_id = $order->id;
                            $user_brand_campaign->campaign = 'mini chivas';
                            $user_brand_campaign->save();
                        }
                    }

					//descontar credito utilizado a usuario
					if ($store['discount_amount']){
					    $user_credit = new UserCredit;
						$user_credit->user_id = $user->id;
						$user_credit->order_id = $order->id;
                        if (isset($coupon))
                            $user_credit->coupon_id = $coupon->id;
						$user_credit->amount = $store['discount_amount'];
						$user_credit->type = 0;
						$user_credit->description = 'Deducción de $'.number_format($user_credit->amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
						$user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
						$user_credit->save();
					}

                    //registrar log de domicilio gratis
                    if (!$order->delivery_amount && $user->free_delivery_expiration_date > date('Y-m-d')){
                        $user_free_delivery = new UserFreeDelivery;
                        $user_free_delivery->user_id = $user->id;
                        $user_free_delivery->order_id = $order->id;
                        $user_free_delivery->amount = null;
                        $user_free_delivery->expiration_date = null;
                        $user_free_delivery->description = 'Domicilio gratis en pedido #'.$order->id.'. Fecha de vencimiento: '.format_date('normal', $user->free_delivery_expiration_date);
                        $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                        $user_free_delivery->save();
                    }
				}

                if ($order_group->total_amount != $order_group_total_amount){
                    $order_group->total_amount = $order_group_total_amount;
                    $order_group->save();
                }

				//$sift->create_order($sift_order_info);

				$orders = Order::where('group_id', $order_group->id)
                                ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                                ->select('orders.*', 'user_brand_campaigns.campaign')
                                ->get();

				$user = User::find($this->user_id);

				foreach($orders as $order)
				{
					//enviar mail de pedido a usuario
                    $store = Store::find($order->store_id);
                    $mail = array(
                        'template_name' => 'emails.order_received',
                        'subject' => 'Tu pedido en '.$store->name,
                        'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
                        'vars' => array(
                            'order' => $order,
                            'order_group' => $order_group,
                            'store' => $store,
                            'products' => $products_mail[$order->id]
                        )
                    );
                    send_mail($mail);
				}

				//elimina productos del carrito
				CartProduct::where('cart_id', $cart->id)->delete();

				$count_orders = OrderGroup::where('user_id', '=', $user->id)->count();
				$this->user_discounts = User::getDiscounts($user->id);
                $order_delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_short_with_time', $order->delivery_date) : format_date('normal_short', substr($order->delivery_date, 0 ,10)).' '.$order->delivery_time;

				$this->response = array(
					'status' => true,
					'message' => 'Pedido registrado',
					'result' => array(
					    'user_id' => $user->id,
						'order_id' => $order->id,
						'order_reference' => $order->reference,
						'order_date' => format_date('normal_short_with_time', $order_group->created_at),
						'order_delivery_date' => $order_delivery_date,
						'order_address' => $order_group->user_address,
						'order_products' => $order_group->products_quantity,
						'order_total' => $order_group->total_amount + $order_group->delivery_amount - $order_group->discount_amount,
						'order_payment_method' => $order->payment_method,
						'user_referral_code' => $user->referral_code,
						'user_new' => $count_orders == 1 ? 1 : 0,
					)
				);

                if ($count_orders == 1)
                {
                    $survey = Survey::find(1);
                    $answers = explode(';', $survey->answers);
                    shuffle($answers);
                    $this->response['result']['survey'] = array(
                        'id' => $survey->id,
                        'question' => $survey->question,
                        'answers' => $answers
                    );
                }

				DB::commit();

			}else $this->response['message'] = 'Valor mínimo de pedido no alcanzado.';

		}catch(\Exception $e){
		    DB::rollback();
			throw $e;
		}

		return $this->jsonResponse();
	}

	/**
	 * Registrar calificacion de pedido
	 *
	 * @return array $response Respuesta
	 */
	public function score_order()
	{
		if ($order_id = Route::current()->parameter('id'))
		{
			if (!Input::has('score') || !Input::has('user_id'))
				return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

			$user_id = Input::get('user_id');
			if (!$order = Order::where('id', $order_id)->where('user_id', $user_id)->where('status', 'Delivered')->first()){
			    $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual o el estado para calificarlo no es valido.';
				return $this->jsonResponse();
            }

			$order->user_score = Input::get('score');
			$order->user_score_comments = Input::has('comments') ? Input::get('comments') : null;
			$order->user_score_date = date('Y-m-d H:i:s');
			$order->save();

			$log = new OrderLog();
			$log->type = 'ORDER_SCORE_UPDATE_APP: '.Input::get('score');
			$log->user_id = $user_id;
			$log->order_id = $order->id;
			$log->save();

			$this->response = array(
			     'status' => true,
			     'message' => 'Tu calificación fue guardada.'
             );

		}else return $this->jsonResponse('El ID del pedido es requerido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtener datos de pedido para notificacion
	 *
	 * @return array $response Respuesta
	 */
	public function notification_order()
	{
		if ($order_id = Route::current()->parameter('id'))
		{
			if (!Input::has('type') || !Input::has('user_id'))
				return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

			$user_id = Input::get('user_id');
			$order = Order::join('stores', 'stores.id', '=', 'orders.store_id')
			->select('stores.name AS store_name', 'minimum_order_amount', 'app_logo_small_url', 'orders.*')
			->where('orders.id', $order_id)->where('user_id', $user_id)->first();
			if (!$order){
			    $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual.';
                return $this->jsonResponse();
			}

            $cloudfront_url = Config::get('app.aws.cloudfront_url');

			//pedido asignado
			if (Input::get('type') == 'In Progress' && $shopper = Shopper::find($order->shopper_id))
			{
				$result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('shopper_id', $order->shopper_id)
				->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('shopper_id')->first();
				$shopper_score_average = $result ? round($result->average, 1) : 0;
				$minutes = !empty($order->shopper_delivery_time_minutes) ? $order->shopper_delivery_time_minutes : 90;

				$data =  array(
					'shopper' => array(
						'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
						'name' => $shopper->first_name.' '.$shopper->last_name,
						'phone' => $shopper->phone,
						'shopper_score_average' => $shopper_score_average,
						'delivery_time' => $minutes.' minutos',
					 ),
					'order' => array(
						'id' => $order->id,
						'total_amount' => $order->total_amount,
						'total_delivery' => $order->delivery_amount,
						'total_discount' => $order->discount_amount,
						'products' => OrderProduct::select('product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name', 'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit')
											->where('order_id', $order->id)->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))->get()->toArray()
					)
				);
			}
			//pedido entregado
			if (Input::get('type') == 'Delivered' && $shopper = Shopper::find($order->shopper_id)){
				$data =  array(
					'shopper' => array(
						'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
						'name' => $shopper->first_name.' '.$shopper->last_name
					 ),
				);
			}
            //pedido cancelado
			if (Input::get('type') == 'Cancelled'){
			    $order_group = OrderGroup::find($order->group_id);
			    $order_delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_short_with_time', $order->delivery_date) : format_date('normal_short', substr($order->delivery_date, 0 ,10)).' '.$order->delivery_time;
				$data = array(
				    'reject_reason' => $order->reject_reason,
                    'order' => array(
                        'id' => $order->id,
                        'reference' => $order->reference,
                        'date' => format_date('normal_short_with_time', $order_group->created_at),
                        'delivery_date' => $order_delivery_date,
                        'address' => $order_group->user_address,
                        'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount,
                        'payment_method' => $order->payment_method,
                    )
                );
			}
			$data['type'] = Input::get('type');
			$data['store']['name'] = $order->store_name;
			$data['store']['minimum_order_amount'] = $order->minimum_order_amount;
            $data['store']['app_logo_small_url'] = $order->app_logo_small_url;

			$this->response = array('status' => true, 'message' => 'Notificación obtenida.', 'result' => $data);

		}else return $this->jsonResponse('El ID del pedido es requerido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtener pedidos pendientes por usuario
	 *
	 * @return array $response Respuesta
	 */
	public function pending_orders()
	{
		if ($user_id = Route::current()->parameter('user_id'))
		{
			$orders = Order::select('orders.id', 'orders.reference', 'orders.status', 'store_id', 'date', 'total_products', 'orders.total_amount', 'orders.delivery_date', 'orders.delivery_time',
							'orders.total_amount AS subtotal', 'orders.delivery_amount', 'orders.discount_amount', 'orders.payment_method', 'order_groups.user_address', 'orders.shopper_id',
							'stores.name AS store_name', 'stores.app_logo_small_url AS store_logo_url')
							->join('order_groups', 'group_id', '=', 'order_groups.id')
							->join('stores', 'store_id', '=', 'stores.id')
							->where('orders.user_id', $user_id)->where('orders.status', '<>', 'Delivered')->where('orders.status', '<>', 'Cancelled')
							->orderBy('orders.id', 'DESC')->get();
			$status = array(
			    'Validation' => 'Iniciado',
				'Initiated' => 'Iniciado',
				'In Progress' => 'En Proceso',
				'Dispatched' => 'En Camino',
				'Delivered' => 'Entregado',
				'Cancelled' => 'Cancelado'
			);
			$orders_user = array();
            if ($orders)
            {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
    			foreach($orders as $order)
    			{
    			    //si el pedido tiene shopper
                    if ($shopper = Shopper::find($order->shopper_id)){
                        $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('shopper_id', $order->shopper_id)
                        ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('shopper_id')->first();
                        $shopper_score_average = $result ? round($result->average, 1) : 0;
                        $minutes = !empty($order->shopper_delivery_time_minutes) ? $order->shopper_delivery_time_minutes : 90;

                        $order->shopper = array(
                            'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
                            'name' => $shopper->first_name.' '.$shopper->last_name,
                            'phone' => $shopper->phone,
                            'shopper_score_average' => $shopper_score_average,
                            'delivery_time' => $minutes.' minutos',
                         );
                    }

    				$order->status = $status[$order->status];
    				$order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
    				$order->products = OrderProduct::select('product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name',
    									'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit', 'fulfilment_status AS status')
    									->where('order_id', $order->id)->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))->get()->toArray();

    				$orders_user[] = $order->toArray();
    			}
			}

			$this->response = array(
				'status' => true,
				'message' => 'Pedidos obtenidos',
				'result' => $orders_user
			);

		}else return $this->jsonResponse('El ID usuario es requerido.', 400);

		return $this->jsonResponse();
	}

    /**
     * Validar cupon o codigo de referido en compra
     *
     * @return array $response Respuesta
     */
    public function coupon()
    {
        if (Input::get('user_id'))
            $user = User::find(Input::get('user_id'));
        else $user = 0;

        $coupon_code = Input::get('coupon_code');
        if (empty($coupon_code))
            return $this->jsonResponse('Codigo del cupón es requerido.', 400);

        //validar codigo del cupon
        $coupon = Coupon::where('code', $coupon_code)->first();
        if (!$coupon){
            $referrer = User::where('referral_code', Input::get('coupon_code'))->where('status', 1);
            if ($user) $referrer->where('id', '<>', $user->id);
            $referrer = $referrer->first();
            if (!$referrer){
                $this->response['message'] = 'El código no es valido.';
                return $this->jsonResponse();
            }
        }

        //redimir cupon de descuento
        if ($coupon)
        {
            //validar estado
            if (!$coupon->status){
                $this->response['message'] = 'El cupón ya no esta activo.';
                return $this->jsonResponse();
            }
            //validar fecha de expiracion
            if (date('Y-m-d') > $coupon->expiration_date){
                $this->response['message'] = 'El cupón ya no esta vigente.';
                return $this->jsonResponse();
            }
            //validar numero de usos
            $uses = UserCoupon::where('coupon_id', $coupon->id)->count();
            if ($uses >= $coupon->number_uses){
                $this->response['message'] = 'El cupón ya fue utilizado.';
                return $this->jsonResponse();
            }

            //validar si el usuario ya utilizo el cupon
            $user_coupon = $user ? UserCoupon::where('user_id', $user->id)->where('coupon_id', $coupon->id)->first() : false;
            if ($user_coupon){
                $this->response['message'] = 'Ya redimiste este cupón.';
                return $this->jsonResponse();
            }
            //validar credito de primera compra
            /*if ($user && $user->first_coupon_used){
                $this->response['message'] = 'Este tipo de cupón ya fue redimido en tu cuenta.';
                return $this->jsonResponse();
            }*/
            //validar el tipo de uso del cupon
            if ($user && $coupon->type_use == 'Only The First Order'){
                $count_orders = Order::where('user_id', '=', $user->id)->where('status', '<>', 'Cancelled');
                //ignorar pedido especifico
                if (Input::has('order_id'))
                    $count_orders->where('id', '<>', Input::get('order_id'));
                $count_orders = $count_orders->count();
                if ($count_orders){
                    $this->response['message'] = 'Este cupón solo podias utilizarlo en tu primera compra.';
                    return $this->jsonResponse();
                }
            }
            if ($user && $user->created_at < date('Y-m-d H:i:s') && $coupon->type_use == 'Only New Customers' && !Input::has('is_new_user')){
                $this->response['message'] = 'Este cupón solo podias utilizarlo al crear tu cuenta de Merqueo.';
                return $this->jsonResponse();
            }
            //validar cupon en grupo de campaña
            if ($user && !empty($coupon->campaign_validation)){
				$campaign = Coupon::select('coupons.id')
										->join('user_coupons', function($join) use ($user){
											$join->on('user_coupons.coupon_id', '=', 'coupons.id');
											$join->on('user_coupons.user_id', '=', DB::raw($user->id));
										})
										->where('coupons.campaign_validation', $coupon->campaign_validation)
										->first();
				if($campaign){
					$this->response['message'] = 'Ya usaste un cupón de esta campaña.';
                	return $this->jsonResponse();
				}
            }
            //validar cupon a tienda o categoria especifica
            if ($coupon->redeem_on == 'Specific Store')
            {
                if ($products = $this->currentCart('getProducts'))
                {
                    $is_valid = false;
                    foreach($products as $product){
                        if ($product->store_id == $coupon->store_id){
                            if ($coupon->department_id){
                                if ($product->department_id == $coupon->department_id){
                                    if ($coupon->shelf_id){
                                        if ($product->shelf_id == $coupon->shelf_id){
                                            if ($coupon->product_id){
                                                if ($product->id == $coupon->product_id)
                                                    $is_valid = true;
                                            }else $is_valid = true;
                                        }
                                    }else $is_valid = true;
                                }
                            }else $is_valid = true;
                        }
                    }
                    if (!$is_valid){
                        $store = Store::select('stores.name AS store_name', 'departments.name AS department_name', 'shelves.name AS shelf_name', 'products.name AS product_name', 'cities.city')
                                      ->leftJoin('departments', function($join) use ($coupon){
                                          $join->on('departments.store_id', '=', 'stores.id');
                                          $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                                      })
                                      ->leftJoin('shelves', function($join) use ($coupon){
                                          $join->on('shelves.department_id', '=', 'departments.id');
                                          $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                                      })
                                      ->leftJoin('products', function($join) use ($coupon){
                                          $join->on('products.shelf_id', '=', 'shelves.id');
                                          $join->on('products.id', '=', DB::raw($coupon->product_id ? $coupon->product_id : 0));
                                      })
                                      ->join('cities', 'cities.id', '=', 'stores.city_id')
                                      ->where('stores.id', $coupon->store_id)
                                      ->first();
                        $message = $store->store_name;
                        if (!empty($store->department_name))
                            $message .= ' / '.$store->department_name;
                        if (!empty($store->shelf_name))
                            $message .= ' / '.$store->shelf_name;
                        if (!empty($store->product_name))
                            $message .= ' / '.$store->product_name;
                        $message .= ' en '.$store->city;

                        $this->response['message'] = 'Este cupón aplica exclusivamente para '.$message.', debe estar en el carrito.';
                        return $this->jsonResponse();
                    }
                }
            }

            //validar que el carrito no tenga productos en promocion
            /*if ($this->currentCart('hasSpecialprice')){
                $this->response['message'] = 'No puedes redimir el cupón por que ya tienes productos en promoción en el carrito.';
                return $this->jsonResponse();
            }*/

            //validar antes de procesar compra
            if (Input::has('validate')){
                $this->response = array(
                    'status' => true,
                    'message' => 'Cupón valido',
                    'result' => array(
                        'coupon_id' => $coupon->id,
                        'coupon_amount' => $coupon->amount
                    )
                );

                return $this->jsonResponse();
            }

            $message = 'El cupón ha sido agregado a tu pedido actual.';
            $total = $this->currentCart('getTotal');
            if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount)
                $message .= ' Para usarlo el total del pedido debe ser mayor o igual a $'.number_format($coupon->minimum_order_amount, 0, ',', '.').'.';
            else if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount)
                $message .= ' Para usarlo el total del pedido debe ser menor o igual a $'.number_format($coupon->maximum_order_amount, 0, ',', '.').'.';
            else if ($coupon->minimum_order_amount && $coupon->maximum_order_amount)
                $message .= ' Para usarlo el total del pedido debe estar entre $'.number_format($coupon->minimum_order_amount, 0, ',', '.').' y $'.number_format($coupon->maximum_order_amount, 0, ',', '.').'.';
            if ($coupon->payment_method)
                $message .= ' El metodo de pago debe ser con '.$this->config['payment_methods_names'][$coupon->payment_method].trim(' '.$coupon->cc_bank).'.';
            if ($coupon->code == 'MASTERCAMBIO')
                $message .= ' Debes tener un pedido entregado y pagado con una tarjeta diferente a Mastercard.';

            $credit = $user ? User::getDiscounts($user->id) : array('amount' => 0);

            $this->response = array(
                'status' => true,
                'message' => $message,
                'result' => array(
                    'type' => 'coupon',
                    'coupon_id' => $coupon->id,
                    'coupon_type' => $coupon->type,
                    'coupon_amount' => $coupon->amount,
                    'coupon_minimum_order_amount' => $coupon->minimum_order_amount,
                    'coupon_maximum_order_amount' => $coupon->maximum_order_amount,
                    'coupon_redeem_on' => $coupon->redeem_on,
                    'coupon_store_id' => $coupon->store_id,
                    'coupon_department_id' => $coupon->department_id,
                    'coupon_shelf_id' => $coupon->shelf_id,
                    'coupon_payment_method' => $coupon->payment_method,
                    'coupon_product_id' => $coupon->product_id,
                    'credit_amount' => $credit['amount'],
                )
            );
        }else{
            if ($user){
                if ($order = Order::where('user_id', $user->id)->where('status', 'Delivered')->first()){
                    $this->response['message'] = 'No puedes agregar un código referido por que ya hiciste tu primer pedido.';
                    return $this->jsonResponse();
                }
                if ($user->free_delivery_expiration_date){
                    $this->response['message'] = 'Ya redimiste un código de referido.';
                    return $this->jsonResponse();
                }
            }
            if ($referrer->total_referrals >= Config::get('app.referred.limit')){
                $this->response['message'] = 'El código referido que ingresaste no es valido por que ha alcanzado el limite permitido de referidos por cuenta.';
                return $this->jsonResponse();
            }
            //validar antes de procesar compra
            if (Input::has('validate')){
                $this->response = array(
                    'status' => true,
                    'message' => 'Código de referido valido'
                );
                return $this->jsonResponse();
            }

            $message = 'Código de referido cargado con éxito. Tienes '.Config::get('app.referred.free_delivery_days').' días de domicilio gratis en cualquier tienda. Por favor termina tu pedido.';

            $this->response = array(
                'status' => true,
                'message' => $message,
                'result' => array(
                    'type' => 'referred'
                )
            );
        }

        return $this->jsonResponse();
    }

	/**
     * Obtiene informacion del carrito actual
     *
     * @param int $action Accion a ejecutar
     * @param int $product_id ID product
     *
     */
    protected function currentCart($action, $product_id = null)
    {
        if (Input::has('cart'))
        {
            $products = json_decode(Input::get('cart'));
            if (count($products)){
                foreach($products as $id => $data){
                    if ($product = Product::find($id)){
                        $product->cart_price = $data->price;
                        $product->cart_quantity = $data->qty;
                        $cart_products[$id] = $product;
                    }
                }
                if (!isset($cart_products))
                    return false;

                //validar si el producto esta agregado en el carrito actual
                if ($action == 'productIsInCart'){
                    if (array_key_exists($product_id, $cart_products))
                        return true;
                    else return false;
                }

                //obtener productos en carrito
                if ($action == 'getProducts')
                    return $cart_products;

                $has_special_price = false;
                $store_id = 0;
                $cart = array(
                    'total_amount' => 0,
                    'delivery_amount' => 0,
                    'total_quantity' => 0
                );

                foreach ($cart_products as $cart_product)
                {
                    $store_id = !$cart_product->id ? $cart_product->cart_store_id : $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id]))
                    {
                        $store_id = $cart_product->store_id;
                        $store = Store::find($store_id)->toArray();
                        $store['sub_total'] = 0;
                        $cart['stores'][$store_id] = $store;
                        $cart['stores'][$store_id]['products'] = array();
                        $cart['stores'][$store_id]['delivery_amount'] = $store['delivery_order_amount'];
                        $cart['stores'][$store_id]['discount_amount'] = 0;
                        $cart['delivery_amount'] += $store['delivery_order_amount'];
                    }

                    $product = json_decode(json_encode($cart_product), true);
                    if ($product['special_price'] > -1){
                        if ($product['cart_price'] < $product['price'])
                            $has_special_price = true;
                    }
                    $product['sub_total'] = $product['cart_quantity'] * $product['cart_price'];
                    $cart['stores'][$store_id]['sub_total'] += $product['sub_total'];
                    $cart['total_amount'] += $product['sub_total'];
                    $cart['total_quantity']++;
                    array_push($cart['stores'][$store_id]['products'] , $product);
                }

                //obtener total del carrito
                if ($action == 'getTotal')
                    return $cart['total_amount'] + $cart['delivery_amount'];
                //varificar si tiene precio especial
                if ($action == 'hasSpecialprice')
                    return $has_special_price;

            }
        }

        return false;
    }

}
