<?php

namespace api\v11;

use Input, UserDevice, UserDeviceLog, Config, UserDeviceToken, City, AppErrorLog, User, UserAddress, UserCreditCard, Order, OrderProduct, Discount;

class ApiIndexController extends \ApiController {

    /**
     * Metodo de inicializacion
     *
     * @return array $response Respuesta
     */
    public function init()
    {
        if (Input::has('lat') && Input::has('lng') && Input::has('brand')
        && Input::has('model') && Input::has('os') && Input::has('os_api') && Input::has('regid')
        && Input::has('os_version') && Input::has('app_version') && Input::has('api_version')){
            extract(Input::all());
            if ($regid != 'null' && $regid)
            {
                //SERVICIO INIT
                $os == 'IOS' ? 'iOS' : $os;
                $user_device_log = new UserDeviceLog;
                $user_device_log->lat = $lat;
                $user_device_log->lng = $lng;
                $user_device_log->brand = $brand;
                $user_device_log->model = $model;
                $user_device_log->os = $os;
                $user_device_log->os_version = $os_version;
                $user_device_log->os_api = $os_api;
                $user_device_log->app_version = $app_version;
                $user_device_log->api_version = $api_version;
                $user_device_log->regid = $regid;
                if (isset($user_id) && !empty($user_id))
                    $user_device_log->user_id = $user_id;
                $user_device_log->save();

                //registrar id del dispositivo
                if (!$user_device = UserDevice::where('regid', $regid)->first()){
                    $user_device = new UserDevice;
                    $user_device->regid = $regid;
                    $user_device->os = $os;
                    if (isset($user_id) && !empty($user_id))
                        $user_device->user_id = $user_id;
                    $user_device->save();
                }else{
                    if (isset($user_id) && !empty($user_id)){
                        $user_device->user_id = $user_id;
                        $user_device->save();
                    }
                }

                //SERVICIO VERSION
                $update = 'not_required'; //not_required, required, suggested
                $update_os = 'none'; //ios, android, all, none
                $update_message = 'No se requiere actualización.';

                $os = strtolower($os);
                $app_version = intval(str_replace('.', '', $app_version));
                if ($os == 'android'){
                    //$app_current_version = intval(str_replace('.', '', Config::get('app.android_current_app_version')));
                    if ($app_version < 200){
                        $update_os = 'android';
                        $update = 'required';
                    }
                }
                if ($os == 'ios'){
                    //$app_current_version = intval(str_replace('.', '', Config::get('app.ios_current_app_version')));
                    if ($app_version < 200){
                        $update_os = 'ios';
                        $update = 'required';
                    }
                }

                if ($update == 'required' && ($os == $update_os || $update_os == 'all'))
                    $update_message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Por favor actualiza a la última versión para continuar. Muchas gracias!';
                if ($update == 'suggested' && ($os == $update_os || $update_os == 'all'))
                    $update_message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Te sugerimos actualizar a la última versión. Muchas gracias!';

                //SERVICIO TOKEN
                if (!$user_device = UserDevice::where('regid', $regid)->first()){
                    $result_token = array(
                        'status' => false,
                        'message' => 'Acceso denegado a dispositivo'
                    );
                }else{
                    if (isset($user_id)){
                        $user_device->user_id = $user_id;
                        $user_device->save();
                    }
                    $token = generate_token();
                    $user_device_token = new UserDeviceToken;
                    $user_device_token->token = $token;
                    $user_device_token->user_device_id = $user_device->id;
                    $user_device_token->expiry = date('Y-m-d H:i:s', strtotime('+4 hour'));
                    $user_device_token->save();

                    $result_token = array(
                        'status' => true,
                        'message' => 'Token obtenido',
                        'token' => $token
                    );
                }

                //SERVICIO CITIES
                $cities = City::select('id', 'city', 'slug')->get();
                $result_cities = array();
                if (count($cities)){
                    foreach ($cities as $city)
                        $result_cities[] = $city;
                }

                $init_message = array('show' => 0);

                $city_id = 1;

                //SERVICIO USUARIO
                $user = array();
                if (isset($user_id) && !empty($user_id)){
                    $user = $this->getUserData($user_id);
                    if ($user['user']['free_delivery_next_order'])
                        $init_message = array(
                            'show' => 1,
                            'title' => 'Domicilio Gratis',
                            'message' => 'Tienes domicilio gratis en cualquier tienda en tu próximo pedido.'
                        );
                    //TEMPORAL DESCUENTO MEDELLIN
                    if (count($user['addresses']))
                        $city_id = $user['addresses'][0]['city_id'];
                }else{
                    //TEMPORAL DESCUENTO MEDELLIN
                    $url = sprintf(
                        'https://maps.googleapis.com/maps/api/geocode/json?latlng=%s&key=%s',
                        $lat.','.$lng, Config::get('app.google_api_key')
                    );
                    if ($result = @file_get_contents($url, false)){
                        $result = json_decode($result, true);
                        if (isset($result['status']) && $result['status'] == 'OK' && isset($result['results'][0]['formatted_address'])){
                            $address = $result['results'][0]['formatted_address'];
                            $address = str_replace('á', 'a', $address);
                            $address = str_replace('í', 'i', $address);
                            $address = strtolower($address);
                            if (strstr($address, 'bogota')) $city_id = 1;
                            if (strstr($address, 'medellin')) $city_id = 2;
                        }
                    }
                }

                //SERVICIO DESCUENTOS EN CHECKOUT
                $checkout_discounts = Order::getCheckoutDiscounts($city_id);

                //version iOS no valida
                /*if ($os_version == '10.0')
                    $init_message = array(
                        'show' => 1,
                        'title' => 'Versión Beta Detectada',
                        'message' => 'Tienes una versión de iOS que actualmente esta en Beta, la aplicación puede presentar problemas.'
                    );*/

                $this->response = array(
                    'status' => true,
                    'message' => 'Datos guardados y obtenidos.',
                    'result' => array(
                        'init_message' => $init_message,
                        'version' => array(
                            'update' => $update,
                            'message' => $update_message,
                            'os' => $update_os,
                            'api' => '1.1',
                            'url_android' => Config::get('app.android_url'),
                            'url_ios' => Config::get('app.ios_url'),
                            'datetime' => date('Y-m-d H:i:s')
                        ),
                        'config' => array(
                            'admin_email' => Config::get('app.admin_email'),
                            'admin_phone' => '031'.Config::get('app.admin_phone'),
                            'facebook_url' => Config::get('app.facebook_url'),
                            'twitter_url' => Config::get('app.twitter_url'),
                            'referred_share_image_url' => Config::get('app.referred.share_image_url'),
                            'referred_share_title' => Config::get('app.referred.share_title'),
                            'referred_share_description' => Config::get('app.referred.share_description'),
                            'referred_by_amount' => Config::get('app.referred.amount'),
                            'referred_by_free_delivery_days' => Config::get('app.referred.referred_by_free_delivery_days'),
                            'amount_minimum_discount' => Config::get('app.minimum_discount_amount'),
                            'messages' => Config::get('app.app_messages')
                        ),
                        'cities' => $result_cities,
                        'token' => $result_token,
                        'user' => $user,
                        'checkout' => $checkout_discounts
                    )
                );
            }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtiene version del API para obligar actualizacion
     *
     * @return array $response Respuesta
     */
    public function version()
    {
        $update = 'not_required'; //not_required, required, suggested
        $os = 'none'; //ios, android, all, none
        $message = 'No se requiere actualización.';

        if (Input::has('app_version') && Input::has('os')){
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if (Input::get('os') == 'android'){
                //$app_current_version = intval(str_replace('.', '', Config::get('app.android_current_app_version')));
                if ($app_version < 118){
                    $os = 'android';
                    $update = 'required';
                }
            }
            if (Input::get('os') == 'ios'){
                //$app_current_version = intval(str_replace('.', '', Config::get('app.ios_current_app_version')));
            }
        }else{
            //nada mas pide actualizacion obligatoria en versiones de android debajo de 1.1.5 en ios no por que no estaba implementado
            $update = 'required';
            $os = 'all';
        }

        if ($update == 'required')
            $message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Por favor actualiza a la última versión para continuar. Muchas gracias!';
        if ($update == 'suggested')
            $message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Te sugerimos actualizar a la última versión. Muchas gracias!';

        $this->response = array(
            'status' => true,
            'message' => $message,
            'result' => array(
                'update' => $update,
                'os' => $os,
                'api' => '1.1',
                'url_android' => Config::get('app.android_url'),
                'url_ios' => Config::get('app.ios_url'),
                'datetime' => date('Y-m-d H:i:s')
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos generales de configuracion
     *
     * @return array $response Respuesta
     */
    public function config()
    {
        $this->response = array(
            'status' => true,
            'message' => 'Datos obtenidos',
            'result' => array(
                'admin_email' => Config::get('app.admin_email'),
                'admin_phone' => Config::get('app.admin_phone'),
                'facebook_url' => Config::get('app.facebook_url'),
                'twitter_url' => Config::get('app.twitter_url'),
                'referrals_limit' => Config::get('app.referrals_limit'),
                'referred_by_amount' => Config::get('app.referred_by_amount'),
                'referrer_amount' => Config::get('app.referrer_amount'),
                'amount_minimum_discount' => Config::get('app.minimum_discount_amount'),
                'messages' => Config::get('app.app_messages'),
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Obtiene ciudades con cobertura
     *
     * @return array $response Respuesta
     */
    public function get_cities()
    {
        $cities = City::select('id', 'city', 'slug')->get();

        $result = array();
        if (count($cities)){
            foreach ($cities as $city)
                $result[] = $city;
        }

        $this->response = array(
            'status' => true,
            'message' => 'Ciudades obtenidas',
            'result' => $result
        );

        return $this->jsonResponse();
    }


    /**
     * Devuelve token de acceso
     *
     * @return array $response Respuesta
     */
    public function request_token()
    {
        if (Input::has('regid'))
        {
            $regid = Input::get('regid');
            if (!$user_device = UserDevice::where('regid', $regid)->first())
                return $this->jsonResponse('Acceso denegado ID de dispositivo no valido', 400);
            else{
                if (Input::has('user_id')){
                    $user_device->user_id = Input::get('user_id');
                    $user_device->save();
                }
                $token = generate_token();
                $user_device_token = new UserDeviceToken;
                $user_device_token->token = $token;
                $user_device_token->user_device_id = $user_device->id;
                $user_device_token->expiry = date('Y-m-d H:i:s', strtotime('+4 hour'));
                $user_device_token->save();

                $this->response = array(
                    'status' => true,
                    'message' => 'Datos guardados',
                    'result' => array('token' => $token)
                );
            }
        }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        return $this->jsonResponse();
    }
}
