<?php

namespace api\v11;

use Input, Route, Request, DB, Store, Department, Shelf, Product, Order, SearchLog, Banner, BannerStore, GuzzleHttp;

class ApiStoreController extends \ApiController {

    /**
     * Obtiene tiendas que atienden una ubicación de latitud y longitud
     *
     * @return array $response Respuesta
     */
    public function get_stores()
    {
        $address = Input::get('address');
        if (Input::has('lat') && Input::has('lng') && empty($address)){
            $lat = Input::get('lat');
            $lng = Input::get('lng');
        }else{
            $city = Input::get('city');
            $address = urldecode($address);
            if (!empty($address) && !empty($city)){
                //obtener latitud longitud
                $request = Request::create('/api/location', 'GET', array('address' => $address, 'city' => $city));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if ($result['status']){
                    $lat = $result['result']['latitude'];
                    $lng = $result['result']['longitude'];
                    $address = $result['result']['address'];
                }else{
                    $lat = 1;
                    $lng = 1;
                }
            }else $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        if (isset($lat) && isset($lng)){
            $result = array();
            $rows = Store::getByLatlng($lat, $lng);
            if (count($rows)){
                foreach($rows as $store){
                    $slot = Store::getDeliverySlot($store->id);
                    $store->is_open = $slot['is_open'];
                    $store->slot = strtolower($slot['next_slot']);
                    $stores[] = $store->toArray();
                }
                //obtener direccion
                if (!isset($address)){
                    $request = Request::create('/api/location_reverse', 'GET', array('lat' => $lat, 'lng' => $lng));
                    Request::replace($request->input());
                    $result = json_decode(Route::dispatch($request)->getContent(), true);
                    $address = $result['status'] ? $result['result'] : false;
                }

                $this->response = array(
                    'status' => true,
                    'message' => 'Se encontraron tiendas dentro de la ubicación',
                    'result' => array(
                        'address' => array(
                            'address' => trim($address),
                            'lat' => strval($lat),
                            'lng' => strval($lng)
                        ),
                    'stores' => $stores)
                );
            }else $this->response = array(
                    'status' => false,
                    'message' => 'En este momento no tenemos cobertura en tu dirección'
                  );
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene tiempo de entrega de tiendas en texto
     *
     * @return array $response Respuesta
     */
    public function get_stores_delivery_time()
    {
        if (!Input::has('lat') || !Input::has('lng'))
            $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $stores = array();
        $rows = Store::getByLatlng(Input::get('lat'), Input::get('lng'));
        if (count($rows))
        {
            foreach($rows as $store){
                $slot = Store::getDeliverySlot($store->id);
                $stores[$store->id]['is_open'] = $slot['is_open'];
                $stores[$store->id]['slot'] = $slot['next_slot'];
            }

            $this->response = array(
                'status' => true,
                'message' => 'Horarios de tiendas obtenidos',
                'result' => $stores
            );
        }else{
              $this->response = array(
                'status' => false,
                'message' => 'En este momento no tenemos cobertura en tu dirección'
              );
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos de tienda, arbol de categorias y categorias con productos
     *
     * @return array $response Respuesta
     */
    public function get_store($id)
    {
        if ($id)
        {
            $store = Store::find($id);
            if ($store){
                $slot = Store::getDeliverySlot($store->id);
                $store->is_open = $slot['is_open'];
                $store->slot = $slot['next_slot'];
                $result['store'] = $store->toArray();
                //paginacion para departments
                $rows = 5;
                if (Input::has('p'))
                    $page = intval(Input::get('p'));
                if (!isset($page)) {
                    $begin = 0;
                    $page = 1;
                }else $begin = ($page - 1) * $rows;
                $departments = Department::select('departments.*')->join('products', 'departments.id', '=', 'department_id')->where('departments.store_id', $store->id)
                ->where('products.status', 1)->where('departments.status', 1);
                if (Input::has('user_id')){
                    if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                        $departments->where('first_order_special_price', 0);
                }
                if ($this->user_agent == 'iOS')
                    $departments->where('departments.name', 'NOT LIKE', '%Ciga%');
                $departments = $departments->groupBy('departments.id')->orderBy('departments.sort_order')->orderBy('departments.name')->limit($rows)->offset($begin)->get();
                $result['store']['departments'] = $departments ? $departments->toArray() : array();
                foreach($result['store']['departments'] as $index => $department){
                    $products = Product::where('department_id', $department['id'])->where('store_id', $store->id)->where('status', 1);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $products->where('first_order_special_price', 0);
                    if ($this->user_agent == 'iOS')
                        $products->where('products.name', 'NOT LIKE', '%Ciga%');
                    $products = $products->limit(3)->orderBy('sort_order')->get();
                    $result['store']['departments'][$index]['products'] = $products ? $products->toArray() : array();
                }

                //categorias y subcategorias para menu
                if ($page == 1){
                    $departments = Department::select('departments.*')->join('products', 'departments.id', '=', 'department_id')->where('departments.store_id', $store->id)->where('departments.status', 1)->where('products.status', 1);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $departments->where('first_order_special_price', 0);
                    if ($this->user_agent == 'iOS')
                        $departments->where('departments.name', 'NOT LIKE', '%Ciga%');
                    $departments = $departments->groupBy('departments.id')->orderBy('departments.sort_order')->orderBy('departments.name')->get();
                    $result['store']['menu']['departments'] = $departments ? $departments->toArray() : array();
                    foreach($departments as $index => $department){
                        $shelves = Shelf::select('shelves.*')->join('products', 'shelves.id', '=', 'shelf_id')->where('shelves.department_id', $department['id'])->where('products.status', 1)->where('shelves.status', 1);
                        if (Input::has('user_id') && isset($has_orders) && $has_orders)
                            $shelves->where('first_order_special_price', 0);
                        if ($this->user_agent == 'iOS')
                            $shelves->where('shelves.name', 'NOT LIKE', '%Ciga%');
                        $shelves = $shelves->groupBy('shelves.id')->orderBy('shelves.sort_order')->orderBy('shelves.name')->get();
                        $result['store']['menu']['departments'][$index]['shelves'] = $shelves->toArray();
                    }
                }

                //si el usuario esta logueado mostrar productos comprados
                if ($page == 1 && Input::has('user_id')){
                    $products = Product::join('order_products','products.id','=','order_products.product_id')
                                   ->join('orders','orders.id','=','order_products.order_id')
                                   ->where('user_id', Input::get('user_id'))
                                   ->where('order_products.store_id', $store->id)
                                   ->where('products.status', 1)
                                   ->select('products.*', DB::raw('COUNT(products.id) AS quantity_products'))
                                   ->groupBy('products.id')
                                   ->orderBy('quantity_products', 'desc')
                                   ->limit(3);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $products->where('first_order_special_price', 0);
                    $products = $products->get();

                   $result['user_products'] = $products;
                }

                $banners = Banner::where('status', 1)
                            ->whereNull('city_id')
                            ->select('banners.title', 'banners.image_app_url', 'banners.position', 'banners.deeplink_type AS type', 'banners.store_id AS store_id', 'banners.department_id', 'banners.shelf_id', 'banners.product_id')
                            ->get()
                            ->toArray();
                $banner_stores = BannerStore::where('banner_stores.store_id', $store->id)
                                                ->where('banners.status', 1)
                                                ->join('banners', 'banner_stores.banner_id', '=', 'banners.id')
                                                ->orderBy('banners.position', 'ASC')
                                                ->select('banners.title', 'banners.image_app_url', 'banners.position', 'banners.deeplink_type AS type', 'banners.store_id AS store_id', 'banners.department_id', 'banners.shelf_id', 'banners.product_id')
                                                ->get()->toArray();

                $banners_front = array_merge($banner_stores, $banners);

                $order = array();
                foreach ($banners_front as $key => $row)
                {
                    $order[$key] = $row['position'];
                }
                array_multisort($order, SORT_ASC, $banners_front);

                foreach ($banners_front as $key => &$banner_front) {
                    foreach ($banner_front as $key2 => &$value) {
                        if ( empty($value) ) {
                            unset($banner_front[$key2]);
                        }
                        if ( $key2 == 'position' ) {
                            unset($banner_front[$key2]);
                        }
                    }
                }

                $result['banners'] = $banners_front;

                $this->response = array(
                    'status' => true,
                    'message' => 'Tienda obtenida',
                    'result' => $result
                );
            }else return $this->jsonResponse('ID no existe', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene categoria con subcategorias y productos
     *
     * @return array $response Respuesta
     */
    public function get_department()
    {
        if ($id = Route::current()->parameter('department_id'))
        {
            $store_id = Route::current()->parameter('store_id');
            $department = Department::select('departments.*')->join('products', 'departments.id', '=', 'department_id')->where('departments.store_id', $store_id)
            ->where('products.status', 1)->where('departments.status', 1)->groupBy('departments.id')->where('departments.id', $id);
            if (Input::has('user_id')){
                if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                    $department->where('first_order_special_price', 0);
            }
            $department = $department->first();
            if ($department){
                $result['department'] = $department ? $department->toArray() : array();
                $promotions_departments_ids = array(2151, 2154, 1033, 2326, 2308, 2197, 2194);
                if (!in_array($department->id, $promotions_departments_ids)){
                    //paginacion para shelves
                    $rows = 5;
                    if (Input::has('p'))
                        $page = intval(Input::get('p'));
                    if (!isset($page)) {
                        $begin = 0;
                        $page = 1;
                    }else $begin = ($page - 1) * $rows;
                }else{
                    $rows = 5;
                    $begin = 0;
                    $page = 1;
                }
                $shelves = Shelf::select('shelves.*', DB::raw('IF(shelves.id IN (10803, 10806, 10168, 11452, 11314, 10889, 10993, 10522), 0, 1) AS show_more'))->join('products', 'shelves.id', '=', 'shelf_id')->where('shelves.department_id', $department->id)
                ->where('products.status', 1)->where('shelves.status', 1);
                if (Input::has('user_id') && isset($has_orders) && $has_orders)
                    $shelves->where('first_order_special_price', 0);
                if ($this->user_agent == 'iOS')
                    $shelves->where('shelves.name', 'NOT LIKE', '%Ciga%');
                $shelves = $shelves->groupBy('shelves.id')->orderBy('shelves.sort_order')->orderBy('shelves.name')->limit($rows)->offset($begin)->get();
                $result['department']['shelves'] = $shelves ? $shelves->toArray() : array();

                $promotions_shelves_ids = array(10803, 10806, 10168, 11452, 11314, 10889, 10993, 10522);
                foreach ($shelves as $index => $shelf)
                {
                    $shelf_data = array();
                    $products = Product::where('shelf_id', $shelf->id)->where('status', 1);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $products->where('first_order_special_price', 0);
                    if ($this->user_agent == 'iOS')
                        $products->where('products.name', 'NOT LIKE', '%Ciga%');
                    //paginacion para productos en promocion
                    if (in_array($shelf->id, $promotions_shelves_ids)){
                        $rows = 24;
                        if (Input::has('p'))
                            $page = intval(Input::get('p'));
                        if (!isset($page)) {
                            $begin = 0;
                            $page = 1;
                        }else $begin = ($page - 1) * $rows;
                        $products = $products->orderBy('sort_order')->limit($rows)->offset($begin)->get();
                    }else{
                        $limit = $shelf->show_more ? 3 : 220;
                        $products = $products->orderBy('sort_order')->limit($limit)->get();
                    }
                    foreach ($products as $product) {
                        array_push($shelf_data, $product->toArray());
                    }
                    $result['department']['shelves'][$index]['products'] = $shelf_data;
                }

                $this->response = array(
                    'status' => true,
                    'message' => 'Categoria obtenida',
                    'result' => $result
                );
            }else return $this->jsonResponse('ID no existe o categoria sin productos', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene subcategoria con productos
     *
     * @return array $response Respuesta
     */
    public function get_shelf()
    {
        if ($id = Route::current()->parameter('shelf_id'))
        {
            $store_id = Route::current()->parameter('store_id');
            $department_id = Route::current()->parameter('department_id');
            $shelf = Shelf::select('shelves.*')->join('products', 'shelves.id', '=', 'shelf_id')->where('shelves.store_id', $store_id)->where('shelves.department_id', $department_id)
            ->where('products.status', 1)->where('shelves.status', 1);
            if (Input::has('user_id')){
                if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                    $shelf->where('first_order_special_price', 0);
            }
            $shelf = $shelf->groupBy('shelves.id')->where('shelves.id', $id)->first();
            if ($shelf){
                $result['shelf'] = $shelf ? $shelf->toArray() : array();
                $shelf_data = array();
                //paginacion para productos
                $rows = 24;
                if (Input::has('p'))
                    $page = intval(Input::get('p'));
                if (!isset($page)) {
                    $begin = 0;
                    $page = 1;
                }else $begin = ($page - 1) * $rows;
                $products = Product::where('shelf_id', $shelf->id)->where('status', 1);
                if (Input::has('user_id') && isset($has_orders) && $has_orders)
                    $products->where('first_order_special_price', 0);
                if ($this->user_agent == 'iOS')
                    $products->where('products.name', 'NOT LIKE', '%Ciga%');
                $products = $products->orderBy('sort_order')->limit($rows)->offset($begin)->get();

                foreach ($products as $product) {
                    array_push($shelf_data, $product->toArray());
                }
                $result['shelf']['products'] = $shelf_data;

                $this->response = array(
                    'status' => true,
                    'message' => 'Subcategoria obtenida',
                    'result' => $result
                );
            }else return $this->jsonResponse('ID no existe o subcategoria sin productos', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene producto
     *
     * @return array $response Respuesta
     */
    public function get_product()
    {
        if ($id = Route::current()->parameter('id'))
        {
            $product = Product::where('id', $id)->where('status', 1)->first();

            if ($product){
                $this->response = array(
                    'status' => true,
                    'message' => 'Producto obtenido',
                    'result' => $product->toArray()
                );
            }else return $this->jsonResponse('ID no existe o esta inactivo', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene ultimos productos de un usuario pedidos en una tienda
     *
     * @return array $response Respuesta
     */
    public function get_user_products()
    {
        $store_id = Route::current()->parameter('store_id');
        $user_id = Route::current()->parameter('user_id');
        if ($store_id && $user_id)
        {
            $result = array();
            $products = Product::join('order_products', 'products.id', '=', 'order_products.product_id')
                           ->join('orders','orders.id', '=', 'order_products.order_id')
                           ->where('user_id', $user_id)
                           ->where('order_products.store_id', $store_id)
                           ->where('products.status', 1)
                           ->select('products.*', DB::raw('COUNT(products.id) AS quantity_products'))
                           ->groupBy('products.id')
                           ->orderBy('quantity_products', 'desc');

           if ($has_orders = Order::select('orders.reference')->where('user_id', $user_id)->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                $products->where('first_order_special_price', 0);

           $products = $products->get();
           if ($products)
               $result = $products;

            $this->response = array(
                'status' => true,
                'message' => 'Productos de usuario en tienda obtenidos',
                'result' => $result
            );
        }else return $this->jsonResponse('Datos obligatorios vacios.', 400);

        return $this->jsonResponse();
    }

    /**
     * Buscador de productos por tiendas
     *
     * @return array $response Respuesta
     */
    public function search()
    {
        if ($id = Route::current()->parameter('id'))
        {
            $store = Store::find($id);
            $city_id = $store->city_id;

            /*//NUEVO BUSCADOR
            if (Input::has('q'))
            {
                $q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);

                $rows = 15;
                $page = Input::has('p') ? intval(Input::get('p')) : 1;

                $client = new GuzzleHttp\Client([
                    'base_uri' => getenv('APP_AES.MSE_HOST')
                ]);
                $response = $client->request('GET', '/store/'.$store->id.'/search/'.$q, [
                    'query' => [
                        'page' => $page,
                        'size' => $rows
                    ],
                    'headers' => [
                        'Authorization' => 'Bearer '. getenv('APP_AES.MERQUEO_READ_TOKEN'),
                        'Content-Type:' => 'application/json'
                    ]
                ]);

                if ($response->getStatusCode() == 200) {
                    $response = $response->getBody()->getContents();
                    $response = json_decode($response, true);
                    if ($response['status']) {
                        $store_products = $response['result'];
                        if (!count($store_products[$store->id])) {
                            $search_log = new SearchLog;
                            $search = Input::get('q');
                            if ($this->user_id)
                                $search_log->user_id = $this->user_id;
                            $search_log->search = $search;
                            $search_log->ip = Request::getClientIp();
                            $search_log->store_id = $store->id;
                            $search_log->save();
                        }
                    }
                }
            }

            $stores = array();
            if (isset($store_products[$store->id]) && count($store_products[$store->id])){
                $data['logo_url'] = $store->app_logo_small_url;
                $data['name'] = $store->name;
                $data['store_id'] = $store->id;
                foreach ($store_products[$store->id] as $key => $product){
                    $data['products'][] = $product;
                }
                $stores[] = $data;
            }
            if (isset($store_products['other']) && count($store_products['other'])){
                foreach ($store_products['other'] as $key => $products) {
                    $store = Store::where('id', $key)->where('status', 1)->first();
                    if (!is_null($store)) {
                        $data['logo_url'] = $store->app_logo_small_url;
                        $data['name'] = $store->name;
                        $data['store_id'] = $store->id;
                        $data['products'] = $products;
                        $stores[] = $data;
                    }
                }
            }

            if (!count($stores))
                $this->response = array('status' => true, 'message' => 'No se encontraron resultados.', 'result' => $stores);
            else $this->response = array('status' => true, 'message' => 'Resultado de busqueda', 'result' => $stores);*/

            //ANTIGUO BUSCADOR
            $query = Product::where('products.status', 1);
            if (Input::has('lat') && Input::has('lng')){
                $store_ids = array();
                $lat = Input::get('lat');
                $lng = Input::get('lng');
                $result = array();
                $rows = Store::getByLatlng($lat, $lng);
                if (count($rows)){
                    foreach($rows as $store)
                        $store_ids[] = $store->id;
                }
                $query->whereIn('stores.id', $store_ids);
            }

            if (Input::has('q')){
                $q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);
                $keys = explode(' ', $q);
                if (count($keys)){
                    $keywords = '';
                    foreach($keys as $key){
                        if (($key != '') and ($key != ' ')){
                            if ($keywords != '') $keywords .= '-space-';
                            $keywords .= $key;
                        }
                    }
                    $keywords = explode('-space-', $keywords);
                    foreach($keywords as $keyword){
                        $query->whereRaw('(products.name LIKE "%'.trim($keyword).'%" COLLATE utf8_spanish_ci OR products.name LIKE "%'.get_plural_string(trim($keyword)).'%" COLLATE utf8_spanish_ci)');
                    }
                }

                $query->where('stores.city_id', $city_id)
                ->select(
                    'products.*',
                    'departments.id AS department_id',
                    'shelves.id AS shelf_id',
                    'departments.slug AS department_slug',
                    'shelves.slug AS shelf_slug',
                    'stores.name AS store_name',
                    'stores.slug AS store_slug',
                    'stores.app_logo_small_url AS store_logo',
                    'stores.id AS store_id'
                )->distinct()
                ->join('shelves', 'products.shelf_id', '=', 'shelves.id')
                ->join('departments', 'shelves.department_id', '=', 'departments.id')
                ->join('stores', 'departments.store_id', '=', 'stores.id')
                ->where('departments.status', 1)
                ->where('shelves.status', 1)
                ->where('stores.status', 1)
                ->orderBy(
                    DB::raw('CASE
                                WHEN products.store_id = '.(int)$id.' THEN 0
                                WHEN products.store_id != '.(int)$id.' THEN 1
                            END
                    ')
                )
                ->orderBy('stores.id');

                if ( !empty($keywords) ) {
                    foreach($keywords as $keyword){
                        $keywords[] = get_plural_string(trim($keyword));
                    }
                    foreach($keywords as $keyword){
                        $query->orderBy(DB::raw('   CASE
                                                        WHEN products.name LIKE \''.trim($keyword).' %\' THEN 0
                                                        WHEN products.name LIKE \'% %'.trim($keyword).'% %\' THEN 1
                                                        WHEN products.name LIKE \'%'.trim($keyword).'\' THEN 2
                                                        ELSE 3
                                                    END
                                                '));
                    }
                }
                if (Input::has('user_id')){
                    if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                        $query->where('first_order_special_price', 0);
                }
                if ($this->user_agent == 'iOS')
                    $query->where('products.name', 'NOT LIKE', '%Ciga%');
            }

            $rows = 15;
            if (Input::has('p'))
                $page = intval(Input::get('p'));
            if (!isset($page)) {
                $begin = 0;
                $page = 1;
            }else $begin = ($page - 1) * $rows;
            // $products = $query->orderBy('products.name', 'asc')->limit($rows)->offset($begin)->get();
            $products = $query->orderBy('products.name', 'asc')->get();

            $result = array();
            $store_id = 0;
            if (count($products)){
                foreach ($products as $product){
                    $result[$product->store_id]['logo_url'] = $product->store_logo;
                    $result[$product->store_id]['name'] = $product->store_name;
                    $result[$product->store_id]['store_id'] = $product->store_id;
                    $product_temp = $product->toArray();
                    unset($product_temp['department_slug']);
                    unset($product_temp['shelf_slug']);
                    unset($product_temp['store_name']);
                    unset($product_temp['store_slug']);
                    unset($product_temp['store_logo']);
                    $result[$product->store_id]['products'][] = $product_temp;
                }
                $result = array_values($result);
                $this->response = array('status' => true, 'message' => 'Resultado de busqueda', 'result' => $result);
            }else{
                $search_log = new SearchLog;
                $search = Input::get('q');
                if ($this->user_id)
                    $search_log->user_id = $this->user_id;
                $search_log->search = $search;
                $search_log->ip = Request::getClientIp();
                $search_log->store_id = $store->id;
                $search_log->save();
                $this->response = array('status' => true, 'message' => 'No se encontraron resultados.', 'result' => $result);
            }

        }else return $this->jsonResponse('ID no existe', 400);

        return $this->jsonResponse();
    }

}
