<?php

namespace api\external_services\services\v1;
use Input;
use Session;
use ExternalService;
class ApiServiceController extends \ApiController
{
    private $movilred = null;
    public function __construct()
    {
        $this->movilred = new \MovilRed;

    }

    /**
     * Obtener tipos de operadores moviles
     */
    public function get_operators_mobile()
    {

        try{

            $result = $this->movilred->getTopUpOperators();
            $status = true;

        }catch(\exceptions\ExternalServiceException $e){

            $result = $e->getMessage();
            $status = false;

        }

        $this->response = array(
            'status' => $status,
            'message' => 'Operadores',
            'result' => $result
        );
        
        return $this->jsonResponse();

    }

    /**
     * Realizar recarga a numero de telefono segun el operador
     */
    public function recharge()
    {

        if (!Input::has('productId') || !Input::has('amount') || !Input::has('destinationNumber')  || !Input::has('EANCode')){
            $this->response = [
                'status' => false
            ];
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        try{
            $productId = Input::get('productId');
            $amount = Input::get('amount');
            $destinationNumber = Input::get('destinationNumber');
            $EANCode = Input::get('EANCode');
            $operatorName = Input::get('operatorName');

            $data = [
                'recharge' => [
                    'productId' => $productId,
                    'amount'    => $amount,
                    'destinationNumber' => $destinationNumber,
                    'EANCode'   => $EANCode,
                    'operatorName' => $operatorName
                ],

                'buyer' => [
                    'name' => '', // Auth::user()->first_name. " " . Auth::user()->last_name 
                    'address' => '',
                    'email' => '', // Auth::user()->email
                    'phone' => '' // Auth::user()->phone
                ],

                'credit_card_id' => Input::get('credit_card_id'),
                'cc_installments' => Input::get('cc_installments'),
                'type' => Input::get('type'),
                'user_id' => Input::get('user_id'),
                'user_city_id' => Session::get('city_id') ? Session::get('city_id') : Input::get('city_id'),
                'amount' => $amount
            ];


            $payment = ExternalService::saveExternal($data);
            $result = '';

            if(isset($payment->id)){

                $charge_card = ExternalService::chargeCreditCard($payment->id, $payment->user_id);
                $status = $charge_card['status'];
                $result = $charge_card['message'];

                if($status){
                    $result = $this->movilred->processTopUpTransaction($productId, $EANCode, $amount, $destinationNumber);
                    
                    if(isset($result->transactionCode)){ #TODO verificar si es transactionCode o invoiceNumber

                        $payment->external_id = $result->transactionCode;
                        $payment->save();

                        // Send Email
                        /*$mail = [
                            'template_name' => '',
                            'subject' => 'Recarga realizada ',
                            'to' => [
                                ['email' => $data["buyer"]["email"], 'name' => $data["buyer"]["name"]  ]
                            ],
                            //'vars' => $vars
                        ];
                        
                        send_mail($mail);*/

                    }
                }

                
            }


        }catch(\exceptions\ExternalServiceException $e){

            $result = $e->getMessage();
            $status = false;

        }

        $this->response = array(
            'status' => $status,
            'message' => 'Recarga a celular',
            'result' => $result
        );
        
        return $this->jsonResponse();

    }

    /**
     * Obtener Categorias Para pagos de facturas.
     */
    public function get_billers_categories($parentCategoryId="")
    {

        try{

            $result = $this->movilred->getBillerCategories($parentCategoryId);
            $status = true;

        }catch(\exceptions\ExternalServiceException $e){

            $result = $e->getMessage();
            $status = false;

        }

        $this->response = array(
            'status' => $status,
            'message' => 'Categorias de Servicios para pagos',
            'result' => $result
        );
        
        return $this->jsonResponse();

    }

    /**
     * Obtener Servicios Para pagos de facturas por categoria.
     */
    public function get_billers_category($categoryId)
    {

        try{

            $result = $this->movilred->getBillersByCategory($categoryId);
            $status = true;

        }catch(\exceptions\ExternalServiceException $e){

            $result = $e->getMessage();
            $status = false;

        }

        $this->response = array(
            'status' => $status,
            'message' => 'Servicios para pagos',
            'result' => $result
        );
        
        return $this->jsonResponse();

    }

     /**
     * Verificar viabilidad de pago de servicio.
     */
    public function validate_bill_reference()
    {

        if (!Input::has('billerCode') || !Input::has('shortReferenceNumber') || !Input::has('valueToPay')){
            $this->response = [
                'status' => false
            ];
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        try{
            $billerCode = Input::get("billerCode");
            $shortReferenceNumber = explode(",", Input::get("shortReferenceNumber"));
            $valueToPay = Input::get("valueToPay");

            $result = $this->movilred->validateBillPaymentByReference($billerCode, $shortReferenceNumber, $valueToPay);
            $status = true;

        }catch(\exceptions\ExternalServiceException $e){

            $result = $e->getMessage();
            $status = false;

        }

        $this->response = array(
            'status' => $status,
            'message' => 'Validacion para pagos',
            'result' => $result
        );
        
        return $this->jsonResponse();

    }



}

