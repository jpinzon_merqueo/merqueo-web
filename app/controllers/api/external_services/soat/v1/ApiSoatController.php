<?php

namespace api\external_services\soat\v1;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use exceptions\ExternalServiceException;
use Hash, Config, DB, Route, Response, Auth, Session;
use Carbon\Carbon;
use Task;
use ExternalService;
use ExternalServiceLog;
use UserAddress;
use PayU;
use UserCreditCard;
use UserCredit;
use User;

use \Firebase\JWT\JWT;

class ApiSoatController extends \ApiController
{
    private $key = null;
    public function __construct()
    {
        $this->config['credit_cards_message'] = 'Visa, Mastercard y American Express.';

        //codigos de tipos de tarjetas aceptadas
        $this->config['credit_cards_types'] = array(
            'VISA',
            'MASTERCARD',
            'AMEX',
            'CREDENCIAL',
            /*'CODENSA'*/
        );

        $this->key = Config::get('app.jwt.key');

        $this->beforeFilter(function() {
            $headers = getallheaders();
            //validar token de acceso
            if (isset($headers['Authorization']) || isset($headers['authorization']))
            {
                $token = isset($headers['Authorization']) ? $headers['Authorization'] : $headers['authorization'];
                $token = str_replace('Bearer ','',$token);
            }
    
            try{
                $decoded = JWT::decode($token, $this->key, array('HS256'));
                
                if(Auth::check() && $decoded->sub == Auth::user()->id){
                    $status = true;
                }else{
                    $status = false;
                }

            }catch(\Firebase\JWT\ExpiredException $e){
                $status = false;
            }catch(Exception $e){
                $status = false;
            }
            
            if(!$status){

                $this->response = array(
                    'status' => false,
                    'message' => 'Error Token.'
                ); 

                return $this->jsonResponse(null, 401);
            }
        	
        });
    }

    /**
     * Obtener inforomacion de SOAT segun placa y documento
     */
    public function get_soat_data()
    {        
        if (!Input::has('plate') || !Input::has('document') || !Input::has('type'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $email = '';
        if(Auth::check())
            $email = Auth::user()->email;

        $data = [
            'plate' => Input::get('plate'),
            'document' => Input::get('document'),
            'type' => Input::get('type'),
            'email' => $email
        ];

        $soat = new \Soat;
        $result = $soat->check($data);

        if ($result) {
            $status = true;
            $code = 'OK';

            if (isset($result->error) && $result->error) {
                $status = false;
                $code = 'ERR';

                if (strpos((string)$result->error, 'tienes') !== false){
                    $code = 'SOAT_ACTIVE';
                    $result->expireDate = Carbon::createFromFormat('d/m/Y',$result->soatExpiryDate)->toDateString();
                }

                if (strpos((string)$result->error, 'dueño') !== false){
                    $code = 'ERR_OWNER';
                }

                if (strpos((string)$result->error,'posible')!== false){
                    $code = 'ERR_CALC';
                }

            }

            if($code == 'OK'){
                $result->dateStart = Carbon::now()->addDay()->toDateString();
                $result->dateEnd = Carbon::now()->addYear()->toDateString();
            }

            $this->response = array(
                'status' => $status,
                'message' => 'Datos obtenidos de soat.',
                'result' => [
                    'soat' => $result,
                    'code' => $code
                ]
            );
        } else {
            $this->response = array(
                'status' => false,
                'message' => 'No existe información.'
            );
        }
        return $this->jsonResponse();

    }

    /**
     * Obtener informacion de usuario logueado
     */
    public function get_info_user()
    {

        $user = Auth::user();

        $info_user = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'phone' => $user->phone,
            'email' => $user->email,
            'user_id' => $user->id,
            'city_id' => Session::get('city_id'),
            'address' => $user->addresses()->select('user_id','address')->first(),
            'credit_cards' => $user->creditcards()->select('id','user_id','type','last_four')->get()
        ];

        $this->response = [
            'status' => true,
            'message' => 'Información del usuario',
            'result' => $info_user
        ];


        return $this->jsonResponse();
    }

    /**
     * Agregar recordatorio de vencimiento  de SOAT
     */
    public function set_reminder()
    {

        if (!Input::has('plate') || !Input::has('document') 
            || !Input::has('documentType') || !Input::has('type') 
            || !Input::has('expire_date') || !Input::has('email')){
                $this->response['status'] = false;
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
            }

        try{
            DB::beginTransaction();
            $reminder = new Task();
            $reminder->user_id = Auth::user()->id;
            $reminder->type = Input::get('type');
            $reminder->expire_date = Input::get('expire_date');
            $reminder->email = Input::get('email');
            $reminder->data = json_encode([
                'plate'=>Input::get('plate'),
                'document'=>Input::get('document'),
                'documentType'=>Input::get('documentType')
            ]);

            $reminder->save();

            $status = true;
            $message = 'Recordatorio Programado';
            DB::commit();

        }catch(Exception $e){ 
            $status = false;
            $message = 'Error al programar recordatorio';
            DB::roolback();
        }
        
        $this->response = [
            'status' => $status,
            'message' => $message,
            'result' => ''
        ];

        return $this->jsonResponse();

    }

    /**
     * Obtener States y Cities de MiMotor
     */

    public function get_states($department_id=null)
    {

        $soat = new \Soat();
        $result = $soat->getStates($department_id);
        $status = true;

        $this->response = array(
            'status' => $status,
            'message' => 'Datos States',
            'result' => $result
        );

        return $this->jsonResponse();

    }

    /*
    Pagar SOAT y enviar a emitir
    */
    public function pay()
    {

        try{
            DB::beginTransaction();
            $buyer = json_decode(Input::get('buyer'), false);
            $taker = json_decode(Input::get('taker'), false);
            $runt = json_decode(Input::get('runt'), true);

            $data = [
                'documentType' => Input::get('documentType'),
                'document' => Input::get('document'),
                'plate' => Input::get('plate'),
                'email' => Input::get('email'),
                'ua' => Input::get('userAgent'),
                'test' => true,
                'runt' => $runt,
                'taker' => [
                    'address' => $taker->address,
                    'firstName' => $taker->firstName,
                    'lastName' => $taker->lastName,
                    'lastName2' => $taker->lastName2,
                    'state' => $taker->state,
                    'genre' => $taker->genre,
                    'municipality' => $taker->municipality,
                    'phone' => $taker->phone
                ],

                // extra internos
                'buyer' => [
                    'name' => $buyer->name,
                    'address' => $buyer->address,
                    'email' => $buyer->email,
                    'phone' => $buyer->phone
                ],
                
                'credit_card_id' => Input::get('credit_card_id'),
                'cc_installments' => Input::get('cc_installments'),
                'type' => Input::get('type'),
                'user_id' => Input::get('user_id'),
                'user_city_id' => Session::get('city_id') ? Session::get('city_id') : Input::get('city_id'),
                'amount' => $runt["soatCost"]


            ];

            $payment = ExternalService::saveExternal($data);
            $result = '';

            if(isset($payment->id)){
                $status = true;

                $soat = new \Soat;
                //verificamos si es posible emitir SOAT antes de continuar con el cobro
                $verifica_emitir = $soat->sendSoat($data, true); 
                
                if(!isset($verifica_emitir->mimotorId)){
                    $status = false;
                    $result = $verifica_emitir->error;

                    $message = 'Datos de Pago';
                    throw new ExternalServiceException($message,$result);

                }

                $credit_card = UserCreditCard::findOrFail($payment->credit_card_id);

                $payment->userCreditCard()->associate($credit_card);

                $charge_card = $payment->chargeCreditCard();
                //dd($charge_card);
                $status = $charge_card['status'];
                $result = $charge_card['result'];
                $message = $charge_card['message'];

                // Validamos que se pueda generar, que sea tipo SOAT y que la tarjeta no sea a Validar
                if($status && $data['type'] == 'soat' && $result != 'PENDING' ){

                    $result_soat = $soat->sendSoat($data, Config::get('app.soat.test'));
                    
                    if(isset($result_soat->mimotorId)){
                        $payment->external_id = $result_soat->mimotorId;
                        $payment->soat_form_number = $result_soat->soatFormNumber;
                        $payment->save();

                        $user = User::find($payment->user_id);

                        $expiration_date = date('Y-m-d', strtotime('+' . Config::get('app.referred.amount_expiration_days') . ' day'));
                        $amount_buy = Config::get('app.soat.amount_buy');
                        UserCredit::addCredit($user, $amount_buy, $expiration_date, 'external_service',$payment);

                        $soat->sendEmail($data);
                    }else{
                        $status = false;
                        $result = 'ERROR';
                        $message = 'Error emitir';
                    }
                    
                }

            }else{
                $status = false;
            }

            if(!$status){
                throw new ExternalServiceException($message,$result);
            }

            $this->response = [
                'status' => $status,
                'message' => $message,
                'result' => $result
            ];
            DB::commit();
            return $this->jsonResponse();

        }catch(ExternalServiceException $e){
            
            $this->response = [
                'status' => false,
                'message' => $e->message,
                'result' => $e->result
            ];
            DB::rollback();

            try{
                $log_external_service = new ExternalServiceLog();
                $log_external_service->user_id = Input::get('user_id');
                $log_external_service->type = $e->message.' '.$e->result;
                $log_external_service->save();
            }catch(Exception $ex){

            }

            return $this->jsonResponse();

        }

    }

    /**
     * Asociar nueva tarjeta de credito
     */
    public function set_credit_card()
    {
        try{
            DB::beginTransaction();
            $user = Auth::user();

            $data = array(
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'user_id' => $user->id
            );

            $address = UserAddress::where('user_id', $user->id)->first();
            if ($address) {
                $data['address'] = $address->address;
                $data['address_further'] = $address->address_further;
            } else {
                $data['address'] = $data['address_further'] = 'No especificada';
            }

            $data = array_merge($data, Input::all());

            $payu = new PayU;

            $bin = substr($data['number_cc'], 0, 6);
            $credit_card = $payu->getCreditCardInfo($bin, $data);

            if(!$credit_card){
                $this->response = [
                    'status' => false,
                    'message' => 'Ocurrió un problema al validar el origen de tu tarjeta de crédito.'
                ];
                return $this->jsonResponse();
            }

            if (!$credit_card->is_valid) {
                $this->response = [
                    'status' => false,
                    'message' => 'Solo se aceptan tarjetas de crédito nacionales.'
                ];
                return $this->jsonResponse();
            }

            $result = $payu->associateCreditCard($data);
            if (!$result['status']) {
                $this->response = ['status' => false, 'message' => $result['message']];
                return $this->jsonResponse();
            }

            if (!in_array($result['response']->creditCardToken->paymentMethod, $this->config['credit_cards_types'])) {

                $this->response = [
                    'status' => false,
                    'message' => 'Solo se aceptan tarjetas de crédito ' . $this->config['credit_cards_message']];

                return $this->jsonResponse();
            }

            $user_credit_card = new UserCreditCard;
            $user_credit_card->card_token = $result['response']->creditCardToken->creditCardTokenId;

            $user_credit_card->user_id = $user->id;
            $user_credit_card->bin = substr($data['number_cc'], 0, 6);
            $user_credit_card->type = $data['card_type'];
            $user_credit_card->last_four = substr($data['number_cc'], 12, 4);
            $user_credit_card->country = $credit_card->country_name;
            $user_credit_card->holder_document_type = $data['document_type_cc'];
            $user_credit_card->holder_document_number = $data['document_number_cc'];
            $user_credit_card->holder_name = $data['name_cc'];
            $user_credit_card->holder_email = $data['email_cc'];
            $user_credit_card->holder_phone = $data['phone_cc'];
            $user_credit_card->holder_address = $data['address_cc'];
            $user_credit_card->holder_address_further = $data['address_further_cc'];
            $user_credit_card->holder_city = $data['city_cc'];
            $user_credit_card->expiration_year = $data['expiration_year_cc'];
            $user_credit_card->expiration_month = $data['expiration_month_cc'];
            $user_credit_card->save();

            $this->response = ['status' => true, 'message' => 'Tarjeta de crédito guardada con éxito'];
            DB::commit();
        }catch(Exception $e){
            $this->response = ['status' => false, 'message' => 'Error al asociar tarjeta'];
            DB::rollback();
        }

        return $this->jsonResponse();

    }

}