<?php

namespace api\v1;

use Input, Route, Request, Session, Hash, Config, View, Validator, DB, Cart, User, Coupon, UserCoupon,
UserCredit, UserAddress, UserCreditCard, UserDeviceToken, Password, Order, OrderProduct, Payment, Product;

class ApiUserController extends \ApiController {

	/**
	 * Realiza login de usuario
	 *
	 * @return array $response Respuesta
	 */
	public function login()
	{
		$email =  Input::get('email');
		$password =  Input::get('password');

		if (empty($email) || empty($password))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$user = User::where('email', $email)->where('type', 'merqueo.com')->first();
		if ($user)
		{
			if (Hash::check($password, $user->password)){

				$user->oauth_verifier = generate_token();
				$user->save();

				$cart = Cart::where('user_id', $user->id)->orderBy('updated_at', 'desc')->first();
				if ($cart){
					$cart_products = Product::join('cart_products', 'cart_products.product_id', '=', 'products.id')
										   ->select('products.*', 'cart_products.quantity AS quantity_cart')
										   ->where('cart_id', $cart->id)->get();
					$cart = array();
					if (count($cart_products)){
						foreach ($cart_products as $product){
							$product->price = $product->special_price > 0 ? $product->special_price : $product->price;
							$cart[] = $product->toArray();
						}
					}
				}else $cart = array();

				$credit = User::getDiscounts($user->id);
				$user = array(
					'id' => $user->id,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'email' => $user->email,
					'phone' => $user->phone,
					'fb_id' => $user->fb_id,
					'referral_code' => $user->referral_code,
					'credit_available' => $credit['amount'],
					'oauth_verifier' => $user->oauth_verifier
				);

				$this->response = array(
					'status' => true,
					'message' => 'Sesion iniciada',
					'result' => array('user' => $user, 'cart' => $cart)
				);
			}else $this->response['message'] = 'Contraseña incorrecta.';
		} else $this->response['message'] = 'Email incorrecto.';

		return $this->jsonResponse();
	}

	/**
	 * Cierra sesion
	 */
	public function logout()
	{
		UserDeviceToken::where('token', $this->token)->delete();

		$this->response = array(
			'status' => true,
			'message' => 'Sesion cerrada'
		);

		return $this->jsonResponse();
	}

	/**
	 * Recordar clave
	 *
	 * @return array $response Respuesta
	 */
	public function password_remind()
	{
		if ($user = Password::getUser(array('email' => Input::get('email'))))
		{
			$vars = array(
                'username' => $user->first_name,
                'website_name' => Config::get('app.website_name'),
                'user_url' => url('/mi-cuenta'),
                'contact_email' => Config::get('app.admin_email'),
                'contact_phone' => Config::get('app.admin_phone'),
                'android_url' => Config::get('app.android_url'),
                'ios_url' => Config::get('app.ios_url'),
                'facebook_url' => Config::get('app.facebook_url'),
                'twitter_url' => Config::get('app.twitter_url')
            );

			View::composer('emails.auth.reminder', function($view) use ($user, $vars) {
	          	$view->with($vars);
	       	});
		}

		$response = Password::remind(Input::only('email'), function($message){
	        $message->subject('Restaurar contraseña');
	    });

		switch ($response)
        {
            case Password::INVALID_USER:
                $this->response['message'] = 'No existe una cuenta con el email.';
            break;

            case Password::REMINDER_SENT:
                $this->response = array(
                    'status' => true,
                    'message' => 'Hemos enviado un mail con los pasos a seguir para recuperar tu contraseña.'
                );
            break;
        }

		return $this->jsonResponse();
	}

	/**
	 * Realiza registro de usuario
	 *
	 * @return array $response Respuesta
	 */
	public function save_user()
	{
		$first_name = Input::get('first_name');
		$last_name  = Input::get('last_name');
		$email 		= Input::get('email');
		$password   = Input::get('password');
		$phone      = Input::get('phone');

        if (empty($first_name) || empty($last_name) || empty($email) || empty($password) || empty($phone))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		//verifica que el usuario no exista
		$validator = Validator::make(
                    array( 'email' => $email, 'phone' => $phone ),
                    array( 'email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com', 'phone' => 'required|numeric|unique:users,phone,NULL,id,type,merqueo.com' ),
                    array( 'email.required' => 'El email es requerido.',
                           'email.unique' => 'El email ingresado ya se encuentra en uso.',
                           'email.email' => 'El formato del email ingresado no es valido.',
                           'phone.required' => 'El número celular es requerido.',
                           'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                           'phone.numeric' => 'El número celular debe ser númerico.'
                    )
        );
		if ($validator->fails()){
			$messages = $validator->messages();
			if (!$error = $messages->first('email'))
				$error = $messages->first('phone');
			$this->response['message'] = $error;
			return $this->jsonResponse();
		}else {
			//crear el usuario, realizar login y enviar mail de bienvenida
			$user = User::add($first_name, $last_name, $phone, $email, $password);

             //validar y cargar codigo de referido
            $referred = $this->validateReferred($user);

			if (!Input::has('do_not_send_mail')){
				$mail = array(
	                'template_name' => 'emails.welcome',
	                'subject' => 'Bienvenido a Merqueo',
	                'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
	                'vars' => array(
	                    'referral_code' => $user->referral_code,
                        'referred' => $referred
	                )
	            );
				send_mail($mail);
			}

			$variables = array(
				'referrer' => $referred['referrer_name'],
				'referrer_amount' => $referred['referred_free_delivery_days'],
				'referred_by_amount' => $referred['referred_by_free_delivery_days'],
				'referrals_limit' => $referred['limit'],
				'credit_amount' => false,
				'error_promo_code' => $referred['error'],
			);

			$credit = User::getDiscounts($user->id);
			$user = array(
				'id' => $user->id,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'phone' => $user->phone,
				'fb_id' => $user->fb_id,
				'referral_code' => $user->referral_code,
				'credit_available' => $credit['amount']
			);

			$this->response = array(
				'status' => true,
				'message' => 'Usuario creado',
				'result' => array('user' => $user, 'variables' => $variables)
			);
		}

		return $this->jsonResponse();
	}

	/**
	 * Realiza registro y login de usuario con Facebook
	 *
	 * @return array $response Respuesta
	 */
	public function social()
	{
		$fb_id = Input::get('fb_id');

		if (empty($fb_id))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$this->response['message'] = 'Ocurrió un error al procesar tus datos de Facebook.';

		if (Input::has('user_id')){
			if ($user = User::find(Input::get('user_id'))){
				$user->fb_id = $fb_id;
				$user->save();

				$credit = User::getDiscounts($user->id);
				$user = array(
					'id' => $user->id,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'email' => $user->email,
					'phone' => $user->phone,
					'fb_id' => $user->fb_id,
					'referral_code' => $user->referral_code,
					'credit_available' => $credit['amount']
				);

				$this->response = array(
					'status' => true,
					'message' => 'Cuenta asociada',
					'result' => array('user' => $user)
				);
			}else return $this->jsonResponse('ID de usuario no existe.', 400);
		} else {
		    //login con fb
	        $user = User::where('fb_id', $fb_id)->first();
			if ($user){

				$credit = User::getDiscounts($user->id);
				$user = array(
					'id' => $user->id,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'email' => $user->email,
					'phone' => $user->phone,
					'fb_id' => $user->fb_id,
					'referral_code' => $user->referral_code,
					'credit_available' => $credit['amount']
				);

				$this->response = array(
					'status' => true,
					'message' => 'Sesión iniciada',
					'result' => array('user' => $user)
				);
			} else{

                if (!Input::has('tmp_var') || Input::has('tmp_var') && !Input::has('first_name')){
				    $this->response['message'] = 'No existe una cuenta de merqueo asociada a la cuenta de Facebook.';
				    return $this->jsonResponse();
                }

            	//registro y login
				$first_name = Input::get('first_name');
				$last_name = Input::get('last_name');
				$email = Input::get('email');
				$phone = Input::has('phone') ? Input::get('phone') : '';

				if (empty($first_name) || empty($last_name) || empty($email))
					return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

				//verifica que el usuario no exista
				$validator = Validator::make(
                            array( 'email' => $email, 'phone' => $phone ),
                            array( 'email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com', 'phone' => 'numeric|unique:users,phone,NULL,id,type,merqueo.com' ),
                            array( 'email.required' => 'El email es requerido.',
                                   'email.unique' => 'El email ingresado ya se encuentra en uso.',
                                   'email.email' => 'El formato del email ingresado no es valido.',
                                   'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                                   'phone.numeric' => 'El número celular debe ser númerico.'
                            )
                );
				if ($validator->fails()){
					$messages = $validator->messages();
					if (!$error = $messages->first('email'))
						$error = $messages->first('phone');
					$this->response['message'] = $error;
					return $this->jsonResponse();
				}else {
					$user = User::add($first_name, $last_name, $phone, $email, '');
					$user->fb_id = $fb_id;
					$user->save();

                    //validar y cargar codigo de referido
                    $referred = $this->validateReferred($user);

					$mail = array(
		                'template_name' => 'emails.welcome',
		                'subject' => 'Bienvenido a Merqueo',
		                'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
		                'vars' => array(
		                    'referral_code' => $user->referral_code,
                            'referred' => $referred
		                )
		            );
					send_mail($mail);

					$credit = User::getDiscounts($user->id);
					$user = array(
						'id' => $user->id,
						'first_name' => $user->first_name,
						'last_name' => $user->last_name,
						'email' => $user->email,
						'phone' => $user->phone,
						'fb_id' => $user->fb_id,
						'referral_code' => $user->referral_code,
						'credit_available' => $credit['amount']
					);

					$this->response = array(
						'status' => true,
						'message' => 'Cuenta creada y asociada',
						'result' => array('user' => $user)
					);
				}
			}
		}

		return $this->jsonResponse();
	}

	/**
	 * Obtiene datos de usuario con direcciones, pedidos, tarjetas de credito
	 *
	 * @return array $response Respuesta
	 */
	public function get_user()
	{
		if (!$id = Route::current()->parameter('id'))
			return $this->jsonResponse('ID es requerido.', 400);

		$user = User::find($id);
		if ($user){

			$addresses = UserAddress::select('id', 'label', 'address', 'address_further', 'city_id', 'latitude', 'longitude', 'address_1', 'address_2', 'address_3', 'address_4')
									 ->where('user_id', $user->id)->get()->toArray();
			$credit_cards = UserCreditCard::select('id', 'last_four', 'type')->where('user_id', $user->id)->get()->toArray();
			$orders = Order::select('orders.id', 'orders.reference', 'orders.status', 'store_id', 'date', 'total_products', 'orders.total_amount',
							'orders.delivery_amount', 'orders.discount_amount', 'orders.payment_method', 'order_groups.user_address',
							'stores.name AS store_name', 'stores.app_logo_small_url AS store_logo_url')
							->join('order_groups', 'group_id', '=', 'order_groups.id')
							->join('stores', 'store_id', '=', 'stores.id')
							->where('orders.user_id', $user->id)->orderBy('orders.id', 'DESC')->get();
			$status = array(
			    'Validation' => 'Iniciado',
				'Initiated' => 'Iniciado',
				'In Progress' => 'En Proceso',
				'Dispatched' => 'En Camino',
				'Delivered' => 'Entregado',
				'Cancelled' => 'Cancelado'
			);
			$orders_user = array();
			foreach($orders as $order){
				$order->status = $status[$order->status];
				$order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
				$order->products = OrderProduct::select('product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name',
									'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit',  'fulfilment_status AS status')
									->where('order_id', $order->id)->get()->toArray();

				$orders_user[] = $order->toArray();
			}

			$credit = User::getDiscounts($user->id);
			$user = array(
				'id' => $user->id,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'phone' => $user->phone,
				'referral_code' => $user->referral_code,
				'credit_available' => $credit['amount']
			);

			$this->response = array(
				'status' => true,
				'message' => 'Usuario obtenido',
				'result' => array(
					'user' => $user,
					'addresses' => $addresses,
					'credit_cards' => $credit_cards,
					'orders' => $orders_user
				)
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Actualiza datos de usuario
	 *
	 * @return array $response Respuesta
	 */
	public function update_user()
	{
		if (!$id = Route::current()->parameter('id'))
			return $this->jsonResponse('ID es requerido.', 400);

		$user = User::find($id);
		if ($user)
		{
			if (Input::has('first_name'))
				$user->first_name = Input::get('first_name');
			if (Input::has('last_name'))
				$user->last_name = Input::get('last_name');
			if (Input::has('phone'))
				$user->phone = Input::get('phone');
			if (Input::has('email'))
				$user->email = Input::get('email');
			if (Input::has('password'))
				$user->password = Hash::make(Input::get('password'));

			//verifica que el usuario no exista
			$validator = Validator::make(
					    array( 'email' => $user->email, 'phone' => $user->phone ),
					    array( 'email' => 'required|email|unique:users,email,'.$user->id.',id,type,merqueo.com', 'phone' => 'required|numeric|unique:users,phone,'.$user->id.',id,type,merqueo.com'),
					    array( 'email.unique' => 'El email ingresado ya se encuentra en uso.',
					    	   'email.email' => 'El formato del email ingresado no es valido.',
					    	   'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
					    	   'phone.numeric' => 'El número celular debe ser númerico.'
						)
			);

			if ($validator->fails()){
				$messages = $validator->messages();
				if (!$error = $messages->first('email'))
					$error = $messages->first('phone');
				$this->response['message'] = $error;
				return $this->jsonResponse();
			}

			$user->save();

			$credit = User::getDiscounts($user->id);
			$user = array(
				'id' => $user->id,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'phone' => $user->phone,
				'referral_code' => $user->referral_code,
				'credit_available' => $credit['amount']
			);

			$this->response = array(
				'status' => true,
				'message' => 'Usuario actualizado',
				'result' => array('user' => $user)
			);

		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();

	}

	/**
	 * Obtiene datos de una direccion
	 *
	 * @return array $response Respuesta
	 */
	public function get_address()
	{
		if (!$id = Route::current()->parameter('address_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$address = UserAddress::where('id', $id)->where('user_id', $user_id)->get();
		if (count($address)){
			$this->response = array(
				'status' => true,
				'message' => 'Dirección obtenida',
				'result' => array('address' => $address->toArray())
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Guarda una nueva direccion
	 *
	 * @return array $response Respuesta
	 */
	public function save_address()
	{
		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$label = Input::get('label');
		$address = Input::get('address');
		$city = Input::get('city');
		$dir = Input::get('dir');

		if (empty($label) || empty($address) || empty($city) || empty($dir))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$address = new UserAddress;
		$address->user_id = $user_id;
		$address->label = Input::get('label');
		$address->address = Input::get('address');
		$address->address_further = Input::get('address_further');
		$dir = Input::get('dir');
		$address->address_1 = $dir[0];
		$address->address_2 = $dir[2];
		$address->address_3 = $dir[6];
		$address->address_4 = $dir[8];
		$request = Request::create('/api/location', 'GET', array('address' => $address->address, 'city' => Input::get('city')));
		Request::replace($request->input());
		$result = json_decode(Route::dispatch($request)->getContent());
		if ($result->status){
			$address->latitude = $result->result->latitude;
			$address->longitude = $result->result->longitude;
			$address->city_id = $result->result->city_id;
			$address->save();
			$this->response = array(
				'status' => true,
				'message' => 'Dirección guardada',
				'result' => array('address' => $address->toArray())
			);
		}else $this->response['message'] = 'Ocurrió un error al ubicar tu dirección.';

		return $this->jsonResponse();
	}

	/**
	 * Actualiza una direccion
	 *
	 * @return array $response Respuesta
	 */
	public function update_address()
	{
		if (!$id = Route::current()->parameter('address_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$label = Input::get('label');
		$address = Input::get('address');
		$city = Input::get('city');
		$dir = Input::get('dir');

		if (empty($label) || empty($address) || empty($city) || empty($dir))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$address = UserAddress::where('user_id', $user_id)->where('id', $id)->first();
		if ($address){
			$address->label = Input::get('label');
			$address->address = Input::get('address');
			$address->address_further = Input::get('address_further');
			$dir = Input::get('dir');
			$address->address_1 = $dir[0];
			$address->address_2 = $dir[2];
			$address->address_3 = $dir[6];
			$address->address_4 = $dir[8];
			$request = Request::create('/api/location', 'GET', array('address' => $address->address, 'city' => Input::get('city')));
			Request::replace($request->input());
			$result = json_decode(Route::dispatch($request)->getContent());
			if ($result->status){
				$address->latitude = $result->result->latitude;
				$address->longitude = $result->result->longitude;
				$address->city_id = $result->result->city_id;
				$address->save();
				$this->response = array(
					'status' => true,
					'message' => 'Dirección actualizada',
					'result' => array('address' => $address->toArray())
				);
			}else $this->response['message'] = 'Ocurrió un error al ubicar tu dirección.';
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Elimina una direccion
	 *
	 * @return array $response Respuesta
	 */
	public function delete_address()
	{
		if (!$id = Route::current()->parameter('address_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$affectedRows = UserAddress::where('user_id', $user_id)->where('id', $id)->delete();
		if ($affectedRows){
			$this->response = array(
				'status' => true,
				'message' => 'Dirección eliminada',
				'result' => ''
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtiene datos de una tarjeta de credito
	 *
	 * @return array $response Respuesta
	 */
	public function get_credit_card()
	{
		if (!$id = Route::current()->parameter('credit_card_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$credit_card = UserCreditCard::select('id', 'last_four', 'type')->where('id', $id)->where('user_id', $user_id)->get();
		if (count($credit_card)){
			$this->response = array(
				'status' => true,
				'message' => 'Tarjeta de crédito obtenida',
				'result' => array('credit_card' => $credit_card->toArray())
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Guarda una nueva tarjeta de crédito
	 *
	 * @return array $response Respuesta
	 */
	public function save_credit_card()
	{
		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$number_cc = Input::get('number_cc');
		$expiration_month_cc = Input::get('expiration_month_cc');
		$expiration_year_cc = Input::get('expiration_year_cc');
		$code_cc = Input::get('code_cc');
		$name_cc = Input::get('name_cc');

		if (empty($number_cc) || empty($expiration_month_cc) || empty($expiration_year_cc) || empty($code_cc) || empty($name_cc))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		if (Input::has('user_id'))
		{
			//validar datos de tarjeta
			if (!$user = User::find(Input::get('user_id')))
				return $this->jsonResponse('Usuario no valido.', 400);

			$data = array(
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'phone' => $user->phone,
				'user_id' => $user->id
			);

			$address = UserAddress::where('user_id', $user->id)->first();
			if ($address){
				$data['address'] = $address->address;
				$data['address_further'] = $address->address_further;
			}else $data['address'] = $data['address_further'] = 'No especificada';

			$data = array_merge($data, Input::all());
			$data['number_cc'] = $number_cc;
			$data['code_cc'] = $code_cc;
			$data['name_cc'] = $name_cc;
			$data['expiration_month_cc'] = $expiration_month_cc;
			$data['expiration_year_cc'] = $expiration_year_cc;

			$payment = new Payment;

			//si el usuario no esta creado en la api se crea
			if (empty($user->customer_token)){
				$result = $payment->createCustomer($data);
				if (!$result['status']){
					$this->response['message'] = 'Ocurrió un error al procesar tus datos personales de la tarjeta de crédito.';
					return $this->jsonResponse();
			    }
				$user->customer_token = $data['customer_token'] = $result['response']->id;
				$user->save();
			}else $data['customer_token'] = $user->customer_token;

			//se guarda y asocia a cliente en api
			$result = $payment->associateCreditCard($data);
			if (!$result['status']){
				$this->response['message'] = 'Ocurrió un error al validar los datos de tu tarjeta de crédito.';
				return $this->jsonResponse();
		    }
            if (!in_array($result['response']->type, $this->config['credit_cards_types'])){
                $this->response['message'] = 'Solo se aceptan tarjetas de crédito '.$this->config['credit_cards_message'];
				return $this->jsonResponse();
		    }

			$data['card_token'] = $result['response']->id;
			//guardar tarjeta en bd
			$user_credit_card = new UserCreditCard;
			$user_credit_card->user_id = $user->id;
			$user_credit_card->card_token = $data['card_token'];
			$user_credit_card->last_four = $result['response']->lastFour;
			$user_credit_card->type = $result['response']->type;
			$user_credit_card->created_at = $user_credit_card->updated_at = date('Y-m-d H:i:s');
			$user_credit_card->save();

			$credit_card['id'] = $user_credit_card->id;
			$credit_card['last_four'] = $user_credit_card->last_four;
			$credit_card['type'] = $user_credit_card->type;

			$this->response = array(
				'status' => true,
				'message' => 'Tarjeta de crédito guardada',
				'result' => array('credit_card' => $credit_card)
			);

		}else return $this->jsonResponse('Usuario no valido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Elimina una tarjeta de crédito
	 *
	 * @return array $response Respuesta
	 */
	public function delete_credit_card()
	{
		if (!$id = Route::current()->parameter('credit_card_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		//validar pedidos en proceso con la tarjeta a eliminar
		$orders = Order::join('order_groups', 'order_groups.id', '=', 'orders.group_id')
						->where('status', '<>', 'Delivered')
						->where('status', '<>', 'Cancelled')
						->where('credit_card_id', $id)
						->count();
		if (!$orders){
			$user = User::find($user_id);
			$user_credit_card = UserCreditCard::find($id);
			$payment = new Payment;
			$result = $payment->deleteCreditCard($user->id, $user->customer_token, $user_credit_card->card_token);
			if (!$result['status']){
				$this->response['message'] = 'Ocurrió un error al eliminar la tarjeta de crédito.';
				return $this->jsonResponse();
		    }
		}else{
			$this->response['message'] = 'No se puede eliminar la tarjeta de crédito por que esta asociada a un pedido en proceso.';
			return $this->jsonResponse();
		}

		if (UserCreditCard::where('user_id', $user->id)->where('id', $id)->delete()){
			$this->response = array(
				'status' => true,
				'message' => 'Tarjeta de crédito eliminada',
				'result' => ''
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

    /**
     * Obtiene credito actual del usuario
     *
     * @return array $response Respuesta
     */
    public function get_user_credit()
    {
        if (!$id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID es requerido.', 400);

        $user = User::find($id);
        if ($user){

            $credit = User::getDiscounts($user->id);
            $this->response = array(
                'status' => true,
                'message' => 'Crédito disponible obtenido',
                'result' => array(
                    'credit_available' => $credit['amount']
                )
            );
        }else return $this->jsonResponse('ID no existe.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtener productos de pedido para agregar al carrito
     *
     * @return array $response Respuesta
     */
    public function add_order_cart()
    {
        if (!$order_id = Route::current()->parameter('order_id'))
            return $this->jsonResponse('ID del pedido es requerido.', 400);

        if (!$user_id = Route::current()->parameter('user_id'))
            return $this->jsonResponse('ID del usuario es requerido.', 400);

        $products = OrderProduct::select('products.*', 'order_products.quantity AS quantity_cart')
                                ->join('orders', 'orders.id', '=', 'order_products.order_id')
                                ->join('products', 'products.id', '=', 'order_products.store_product_id')
                                ->where('orders.id', $order_id)
                                ->where('orders.user_id', $user_id)
                                ->where('order_products.type', 'Product')
                                ->where('products.status', 1)
                                ->get();

        foreach($products as $product)
        {
            if ($product->quantity_cart > 0 && $product->special_price && $product->quantity_special_price){
                //validar cantidad de pedidos del producto por usuario
                $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                                        ->where('product_id', '=', $product->id)
                                        ->where('price', '=', $product->special_price)
                                        ->where('user_id', '=', $user_id)
                                        ->where('status', '<>', 'Cancelled')
                                        ->where(DB::raw('DATEDIFF(NOW(), date)'), '<=', 30)
                                        ->groupBy('order_products.store_product_id')->count();
                if ($count_products){
                    $this->response['message'] = 'Ya realizaste un pedido con el limite permitido para el producto en promoción: '.$product->name;
                    return $this->jsonResponse();
                }

                if ($product->quantity_cart > $product->quantity_special_price){
                    $this->response['message'] = 'El producto '.$product->name.' por estar en promoción puedes agregar máximo '.$product->quantity_special_price.' unidades al carrito.';
                    return $this->jsonResponse();
                }
            }
        }

        $this->response = array(
            'status' => true,
            'message' => 'Productos obtenidos',
            'result' => $products
        );

        return $this->jsonResponse();
    }

}
