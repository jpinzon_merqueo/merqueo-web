<?php

namespace api\v1;

use Input, UserDevice, UserDeviceLog, Config, UserDeviceToken, City;

class ApiIndexController extends \ApiController {

	/**
	 * Metodo de inicializacion
	 *
	 * @return array $response Respuesta
	 */
	public function init()
	{
		if (Input::has('lat') && Input::has('lng') && Input::has('brand')
		&& Input::has('model') && Input::has('os') && Input::has('os_api') && Input::has('regid')
		&& Input::has('app_version') && Input::has('api_version')){
			extract(Input::all());
			if ($regid != 'null' && $regid){
			    $os == 'IOS' ? 'iOS' : $os;
				$user_device_log = new UserDeviceLog;
				$user_device_log->lat = $lat;
				$user_device_log->lng = $lng;
				$user_device_log->brand = $brand;
				$user_device_log->model = $model;
				$user_device_log->os = $os;
                if (Input::has('os_version'))
                    $user_device_log->os_version = $os_version;
				$user_device_log->os_api = $os_api;
				$user_device_log->app_version = $app_version;
				$user_device_log->api_version = $api_version;
				$user_device_log->regid = $regid;
				if (isset($user_id) && !empty($user_id))
					$user_device_log->user_id = $user_id;
				$user_device_log->save();

				//registrar id del dispositivo
				if (!$user_device = UserDevice::where('regid', $regid)->first()){
					$user_device = new UserDevice;
					$user_device->regid = $regid;
                    if (in_array($os, array('Android', 'iOS')))
                        $user_device->os = $os;
					if (isset($user_id) && !empty($user_id))
						$user_device->user_id = $user_id;
					$user_device->save();
				}else{
					if (isset($user_id) && !empty($user_id)){
						$user_device->user_id = $user_id;
                        if (in_array($os, array('Android', 'iOS')))
                            $user_device->os = $os;
						$user_device->save();
					}
				}

				$this->response = array(
					'status' => true,
					'message' => 'Datos guardados'
				);
			}else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
		}else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtiene version del API para obligar actualizacion
	 *
	 * @return array $response Respuesta
	 */
	public function version()
    {
        $update = 'not_required'; //not_required, required, suggested
        $os = 'none'; //ios, android, all, none
        $message = 'No se requiere actualización.';

        if (Input::has('app_version') && Input::has('os')){
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if (Input::get('os') == 'android'){
                //$app_current_version = intval(str_replace('.', '', Config::get('app.android_current_app_version')));
                if ($app_version < 114){
                    $os = 'android';
                    $update = 'required';
                }
            }
            if (Input::get('os') == 'ios'){
                //$app_current_version = intval(str_replace('.', '', Config::get('app.ios_current_app_version')));
            }
        }

        if ($update == 'required')
            $message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Por favor actualiza a la última versión para continuar. Muchas gracias!';
        if ($update == 'suggested')
            $message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Te sugerimos actualizar a la última versión. Muchas gracias!';

        $this->response = array(
            'status' => true,
            'message' => $message,
            'result' => array(
                'update' => $update,
                'os' => $os,
                'api' => '1.1',
                'url_android' => Config::get('app.android_url'),
                'url_ios' => Config::get('app.ios_url'),
                'datetime' => date('Y-m-d H:i:s')
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos generales de configuracion
     *
     * @return array $response Respuesta
     */
    public function config()
    {
        $this->response = array(
            'status' => true,
            'message' => 'Datos obtenidos',
            'result' => array(
                'admin_email' => Config::get('app.admin_email'),
                'admin_phone' => Config::get('app.admin_phone'),
                'facebook_url' => Config::get('app.facebook_url'),
                'twitter_url' => Config::get('app.twitter_url'),
                'referrals_limit' => Config::get('app.referrals_limit'),
                'referred_by_amount' => Config::get('app.referred_by_amount'),
                'referrer_amount' => Config::get('app.referrer_amount'),
                'amount_minimum_discount' => Config::get('app.minimum_discount_amount'),
            )
        );

        return $this->jsonResponse();
    }

	/**
	 * Obtiene ciudades con cobertura
	 *
	 * @return array $response Respuesta
	 */
	public function get_cities()
	{
		$cities = City::select('id', 'city', 'slug')->get();

		$result = array();
		if (count($cities)){
			foreach ($cities as $city)
				$result[] = $city;
		}

		$this->response = array(
			'status' => true,
			'message' => 'Ciudades obtenidas',
			'result' => $result
		);

		return $this->jsonResponse();
	}


	/**
	 * Devuelve token de acceso
	 *
	 * @return array $response Respuesta
	 */
	public function request_token()
	{
		if (Input::has('regid'))
		{
			$regid = Input::get('regid');
			if (!$user_device = UserDevice::where('regid', $regid)->first())
				return $this->jsonResponse('Acceso denegado ID de dispositivo no valido', 400);
			else{
				if (Input::has('user_id')){
					$user_device->user_id = Input::get('user_id');
					$user_device->save();
				}
				$token = generate_token();
				$user_device_token = new UserDeviceToken;
				$user_device_token->token = $token;
				$user_device_token->user_device_id = $user_device->id;
				$user_device_token->expiry = date('Y-m-d H:i:s', strtotime('+4 hour'));
				$user_device_token->save();

				$this->response = array(
					'status' => true,
					'message' => 'Datos guardados',
					'result' => array('token' => $token)
				);
			}
		}else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		return $this->jsonResponse();
	}

}
