<?php

namespace api\v1;

use exceptions\MerqueoException;
use Input, Route, Request, Session, Config, View, Validator, Cart, CartProduct, User, UserAddress, UserCreditCard,
DateTime, UserCredit, Order, OrderGroup, OrderProduct, Payment, Coupon, UserCoupon, Store, Product, OrderLog,
Shopper, DB, Zone;

class ApiCheckoutController extends \ApiController
{
    const SUBJECT_EMAIL = 'Tu Merqueo';
	public $user_credit;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->response = array('status' => false, 'message' => 'Error generico');

        $this->beforeFilter(function() {
        	if (Input::has('user_id'))
        		$this->user_id = Input::get('user_id');
			else $this->user_id = 0;
			$this->user_discounts = User::getDiscounts($this->user_id);
        });
    }

	/**
	 * Obtiene horario de tiendas con base en carrito
	 *
	 * @return array $response Datos de horarios
	 */
	 public function delivery_time()
	 {
	    $cart = Input::get('cart');
		if (empty($cart))
			return $this->jsonResponse('Datos del carrito es requerido.', 400);

		$product_ids = array();

		$products = json_decode($cart);
		if (count($products)){
			foreach ($products as $id => $product) {
				$product_ids[] = $id;
			}
		}

		$store_ids = array();
		$stores = Product::select('store_id')->whereIn('id', $product_ids)->groupBy('store_id')->get();
		if ($stores){
			foreach($stores as $store)
				$store_ids[] = $store->store_id;
		}

        $days = $time_week = array();

		$delivery = Store::getDeliverySlotCheckout($store_ids);
		foreach($delivery['days'] as $index => $value){
			$days[] = array('value' => $index, 'text' => $value);
		}
		if (isset($delivery['time']['today'])){
			foreach($delivery['time']['today'] as $index => $value){
				$time_today[] = array('value' => $index, 'text' => $value);
			}
		}
		foreach($delivery['time']['week'] as $index => $value){
			$time_week[] = array('value' => $index, 'text' => $value);
		}

		$delivery_time = array(
			'days' => $days,
			'time' => array(
				'week' => $time_week
			)
		);
        if (isset($time_today))
            $delivery_time['time']['today'] = $time_today;

		$this->response = array(
			'status' => true,
			'message' => 'Horarios de tienda obtenidos.',
			'result' => $delivery_time
		);

		return $this->jsonResponse();
	 }

	/**
	 * Procesa nuevo pedido
	 *
	 * @return array $response Respuesta
	 */
	public function checkout()
	{
		$post_data = Input::all();

		try {
			DB::beginTransaction();

			//si no esta logueado el usuario se crea
			if (Input::has('create_user') && !Input::has('user_id'))
			{
				$email =  Input::get('email');
				$password =  Input::get('password');

				if (empty($email) || empty($password))
					return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

				//crear usuario
				$inputs = array(
					'first_name' => Input::get('first_name'),
					'last_name' => Input::get('last_name'),
					'email' => Input::get('email'),
					'password' => Input::get('password'),
					'phone' => Input::get('phone'),
					'do_not_send_mail' => 1
				);
				$request = Request::create('/api/1.0/user', 'POST', $inputs);
				Request::replace($request->input());
				$result = json_decode(Route::dispatch($request)->getContent(), true);
				if (!$result['status']){
					$this->response['message'] = $result['message'];
					return $this->jsonResponse();
				}
				$user = User::find($result['result']['user']['id']);
				$this->user_id = $user->id;
			}else{

				if (!$this->user_id)
					return $this->jsonResponse('ID usuario es requerido.', 400);

				$user = User::find($this->user_id);
				$post_data['first_name'] = $user->first_name;
				$post_data['last_name'] = $user->last_name;
				$post_data['email'] = $user->email;
				if (empty($user->phone) && isset($post_data['user_phone'])){
					$user->phone = $post_data['phone'] = $post_data['user_phone'];
					$user->save();
					unset($post_data['user_phone']);
				}else $post_data['phone'] = $user->phone;
			}

			$post_data['user_id'] = $user->id;

			//APARTIR DE ESTE PUNTO YA EL USUARIO ESTA AUTENTICADO

			//obtener pedidos del usuario
            $user_has_orders = Order::select('orders.reference')->where('user_id', $this->user_id)->where('status', 'Delivered')->orderBy('id', 'desc')->first();

			//si hay información de dirección se valida para crearla
			if (empty($post_data['address_id']))
			{
			    $address = new UserAddress;
				$address->user_id = $this->user_id;
                $post_data['address_name'] = !isset($post_data['address_name']) ? 'Casa' : $post_data['address_name'];
                $post_data['other_name'] = !isset($post_data['other_name']) ? 'Casa' : $post_data['other_name'];
				$address->label = $post_data['address_name'] == 'Otro' ? $post_data['other_name'] : $post_data['address_name'];
				$address->address = $post_data['address'];
                if (isset($post_data['address_further']))
				    $address->address_further = $post_data['address_further'];
                else $address->address_further = $post_data['address_further'] = 'Llamar para confirmar';
				$dir = $post_data['dir'];
				$address->address_1 = $dir[0];
				$address->address_2 = $dir[2];
				$address->address_3 = $dir[6];
				$address->address_4 = $dir[8];
				//validar direccion
				$inputs = array(
					'address' => $address->address,
					'city' => $post_data['city']
				);
				$request = Request::create('/api/location', 'GET', $inputs);
				Request::replace($request->input());
				$result = json_decode( Route::dispatch($request)->getContent(), true);
				if (!$result['status']){
					$this->response['message'] = 'No pudimos ubicar tu dirección por favor verificala.';
					return $this->jsonResponse();
				}
                $address->latitude = $result['result']['latitude'];
                $address->longitude = $result['result']['longitude'];
				$address->city_id = $result['result']['city_id'];
                $address->save();
				$address_id = $address->id;
			}else{
				$address_id = $post_data['address_id'];
				$address = UserAddress::find($address_id);
				$post_data['address'] = $address->address;
				$post_data['address_further'] = $address->address_further;
			}

			//obtener carrito
            $cart = Cart::where('user_id', $this->user_id)->orderBy('updated_at', 'desc')->first();
            //si no existe entonces creo un carrito
            if (!$cart) {
                $cart = new Cart;
                $cart->user_id = $this->user_id;
                $cart->save();
            }

            //crear carrito en bd
			if (isset($post_data['cart'])){
				$products = json_decode($post_data['cart']);
				if (count($products)){
					CartProduct::where('cart_id', $cart->id)->delete();
					foreach ($products as $id => $product) {
						if ($product->qty <= 0) continue;
						$cart_product = new CartProduct;
						$cart_product->cart_id = $cart->id;
						$cart_product->product_id = $id;
						$cart_product->quantity = $product->qty;
                        if ($id < 0){
                            $cart_product->product_name = $product->name;
                            $cart_product->store_id = $product->store_id;
                            $cart_product->comment = $product->comment;
                        }
                        $cart_product->price = $product->price;
						$cart_product->added_by = $this->user_id;
						$cart_product->save();
					}
				}
			}

            //validar metodo de pago
			$post_data['payment_method'] = $post_data['payment_method'] == 'Tarjeta de Crédito' ? 'Tarjeta de crédito' : $post_data['payment_method'];
			$is_credit_card = $post_data['payment_method'] == 'Tarjeta de crédito' ? true : false;
			if ($is_credit_card)
			{
				$payment = new Payment;

				//si el usuario no esta creado en la api se crea
				$create_user_api = false;
				if (empty($user->customer_token))
					$create_user_api = true;
				else $post_data['customer_token'] = $user->customer_token;

				if ($create_user_api){
					$result = $payment->createCustomer($post_data);
					if (!$result['status']){
						$this->response['message'] = 'Tus datos personales no pasaron la validación de la tarjeta de crédito.';
						return $this->jsonResponse();
					}
					$user->customer_token = $post_data['customer_token'] = $result['response']->id;
					$user->save();
				}

				//si la tarjeta es nueva se guarda y asocia a cliente en api
				$credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
				if (empty($credit_card_id))
				{
				    //validar pais de tarjeta de credito
                    $bin = substr($post_data['number_cc'], 0, 6);
                    $credit_card = $payment->getCreditCardInfo($bin, $post_data);
                    if (!$credit_card){
                        $this->response['message'] = 'Ocurrió un problema al validar el origen de tu tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    if (!$credit_card->is_valid){
                        $this->response['message'] = 'Solo se aceptan tarjetas de crédito nacionales.';
                        return $this->jsonResponse();
                    }

					$result = $payment->associateCreditCard($post_data);
					if (!$result['status']){
						$this->response['message'] = 'Tuvimos un problema con la validación de los datos de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
						return $this->jsonResponse();
					}
                    if (!in_array($result['response']->type, $this->config['credit_cards_types'])){
						$this->response['message'] = 'Solo se aceptan tarjetas de crédito Visa, Mastercard y American Express.';
						return $this->jsonResponse();
	                }

					$post_data['card_token'] = $result['response']->id;
					//guardar tarjeta en bd
					$user_credit_card = new UserCreditCard;
					$user_credit_card->user_id = $user->id;
					$user_credit_card->card_token = $post_data['card_token'];
                    $user_credit_card->holder_name = $post_data['name_cc'];
					$user_credit_card->last_four = $result['response']->lastFour;
					$user_credit_card->type = $result['response']->type;
                    $user_credit_card->country = $credit_card->country_name;
					$user_credit_card->created_at = $user_credit_card->updated_at = date('Y-m-d H:i:s');
					$user_credit_card->save();
				}else{
					$user_credit_card = UserCreditCard::find($credit_card_id);
					if (!$user_credit_card){
						$this->response['message'] = 'Ocurrió un error al obtener información de la tarjeta de crédito.';
						return $this->jsonResponse();
					}
				    $post_data['card_token'] = $user_credit_card->card_token;
				}
                $credit_card_type = $user_credit_card->type;
			}

			$cart_products = DB::table('cart_products')
								->where('cart_id', $cart->id)
								->leftJoin('products', 'cart_products.product_id', '=', 'products.id')
								->leftJoin('users', 'cart_products.added_by', '=', 'users.id')
								->select('products.*', 'cart_products.price AS cart_price', 'cart_products.quantity AS cart_quantity',
                                'cart_products.product_name AS cart_product_name', 'cart_products.comment AS cart_comment', 'cart_products.store_id AS cart_store_id')
								->orderBy('store_id')
								->get();

            $response['cart']['delivery_amount'] = 0;
			$response['cart']['total_amount'] = 0;
			$response['cart']['total_quantity'] = 0;
			$response['cart']['stores'] = [];

			$store_id = 0;
			if ($cart_products)
			{
			    $response['cart']['is_minimum_reached'] = 1;
				foreach ($cart_products as $cart_product)
				{
				    if (!$cart_product->id  && empty($cart_product->cart_product_name)){
                        $this->response['message'] = 'El producto con cantidad '.$cart_product->cart_quantity.' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        return $this->jsonResponse();
                    }

					//validar producto en promocion en pedidos anteriores en merqueo super
                    if ($store_id == 36 || $store_id == 37){
    				   $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
    									->where('product_id', '=', $cart_product->id)
    									->where('price', '=', $cart_product->special_price)
    									->where('user_id', '=', $this->user_id)
    									->where('status', '<>', 'Cancelled')
    									->where(DB::raw('DATEDIFF(NOW(), date)'), '<=', 30)
    									->groupBy('order_products.store_product_id')->count();
    					if ($count_products){
    						$this->response['message'] = 'Ya realizaste un pedido con el producto '.$cart_product->name.' en promoción, por favor eliminalo del carrito de compras para continuar.';
    						return $this->jsonResponse();
    					}
					}

                    //validar precio especial
                    /*$original_price = $cart_product->special_price ? $cart_product->special_price : $cart_product->price;
                    if ($original_price > $cart_product->cart_price){
                        $this->response['message'] = 'El precio del producto '.$cart_product->name.' ha cambiado, por favor eliminalo del carrito y vuelve a agregarlo.';
                        return $this->jsonResponse();
                    }*/

                    //validar precio especial y cantidad del producto
                    if ($cart_product->cart_quantity > 0 && $cart_product->special_price > -1){
                        //validar primera compra
                        /*if ($user_has_orders && $cart_product->first_order_special_price){
                            $this->response['message'] = 'La promoción del producto '.$cart_product->name.' aplica solo para primera compra, por favor eliminalo del carrito para continuar.';
                            return $this->jsonResponse();
                        }*/
                        //validar cantidad del producto por pedido
                        if ($cart_product->quantity_special_price){
                            if ($cart_product->cart_quantity > $cart_product->quantity_special_price){
                                $this->response['message'] = 'El producto '.$cart_product->name.' por estar en promoción puedes agregar máximo '.$cart_product->quantity_special_price.' unidades en tu pedido, por favor disminuye la cantidad en el carrito para continuar.';
                                return $this->jsonResponse();
                            }
                        }
                    }

					$store_id = !$cart_product->id ? $cart_product->cart_store_id : $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id]))
                    {
						$store_id = $cart_product->store_id;
						//obtener tienda
						$store = Store::find($store_id);
						$store->count = 1;
						$store->sub_total = 0;
                        $store->products = [];
                        $store->delivery_amount = $store->delivery_order_amount;
                        $store->discount_amount = 0;
                        $store->sub_total = 0;
                        $response['cart']['delivery_amount'] += $store->delivery_order_amount;
                        $response['cart']['stores'][$store_id] = $store;
                    } else {
						$response['cart']['stores'][$store_id]->count += 1;
					}

                    if ($cart_product->special_price > -1){
						//$has_special_price = true;
						$price = $cart_product->special_price;
					}else $price = $cart_product->cart_price;

					$sub_total = $cart_product->cart_quantity * $price;
                    $response['cart']['stores'][$store_id]->sub_total += $sub_total;
                    $response['cart']['total_amount'] += $sub_total;
                    $response['cart']['total_quantity']++;
                    $response['cart']['stores'][$store_id]->products[] = (array) $cart_product;
				}
			}
	        $delivery_time_minutes = $total_credit_amount = $total_discount_amount = 0;

			if (($response['cart']['total_amount'] + $response['cart']['delivery_amount'] > $this->user_discounts['minimum_discount_amount'] && isset($this->user_discounts['coupon']))
			|| (isset($this->user_discounts['coupon']) && !empty($this->user_discounts['coupon']->product_id))) //si es cupon de producto especifico
			{
			    //si es pedido atomico y tiene un cupon
				if (isset($this->user_discounts['coupon']) && isset($post_data['create_user']))
				{
				    $coupon = $this->user_discounts['coupon'];
					$coupon_code = $coupon->code;
				    $data = array('coupon_code' => $coupon_code, 'validate' => 1, 'is_new_user' => 1);
					$request = Request::create('/api/1.1/coupon', 'POST', $data);
					Request::replace($request->input());
					$result = json_decode(Route::dispatch($request)->getContent(), true);
					if ($result['status']){
						$coupon_id = $result['result']['coupon_id'];
                        $result = $result['result'];
					}else{
						$this->response['message'] = $result['message'];
						return $this->jsonResponse();
					}

                    if (!isset($has_special_price)){
    					if ($coupon->type == 'Credit Amount'){
    						//descuento de credito
    						if ($result['coupon_amount'] > $response['cart']['total_amount'])
    							$result['coupon_amount'] = $response['cart']['total_amount'];
    						$credit_amount = $result['coupon_amount'];
    						$total_credit_amount = $total_discount_amount = $credit_amount;
    						$stores_count = count($response['cart']['stores']);
    						$discount_credit_amount = intval($total_discount_amount / $stores_count);
    					}
    					if ($coupon->type == 'Discount Percentage'){
    						//no aplicar descuento de credito sino solo el descuento por porcentaje
    						unset($discount_credit_amount);
    						$total_discount_amount = 0;
    						$discount_percentage_amount = $result['coupon_amount'];
    					}
					}
				}else{
					//si tiene credito disponible
					if (!isset($has_special_price) && $this->user_discounts['amount']){
						if ($this->user_discounts['amount'] > $response['cart']['total_amount'])
							$this->user_discounts['amount'] = $response['cart']['total_amount'];
						$credit_amount = $this->user_discounts['amount'];
						$total_credit_amount = $total_discount_amount = $credit_amount;
						$stores_count = count($response['cart']['stores']);
						$discount_credit_amount = intval($total_discount_amount / $stores_count);
					}
				}

                /*if ($coupon->code === 'MASTERCAMBIO')
                {
                    //validar que el usuario tenga un pedido con tarjeta diferente a mastercard
                    $order = Order::leftJoin('user_credit_cards', 'credit_card_id', '=', 'user_credit_cards.id')
                                     ->where('orders.user_id', $user->id)
                                     ->where('orders.status', 'Delivered')
                                     ->where('type', '<>', 'MASTERCARD')
                                     ->where('payment_method', 'Tarjeta de crédito')
                                     ->first();
                    if (!$order){
                        $this->response['message'] = 'Para redimir el cupón '.$coupon->code.' debes tener un pedido entregado y pagado con una tarjeta diferente a Mastercard.';
                        return $this->jsonResponse();
                    }
                }*/
			}

            //domicilio a mitad de precio si hay mas de una tienda
            if (count($response['cart']['stores']) > 1) {
			    $first_store_id = current(array_keys($response['cart']['stores']));
            }

            foreach ($response['cart']['stores'] as $store_id => $store) {
			    if (isset($first_store_id) && $store_id != $first_store_id && $store->delivery_amount) {
                    $store->delivery_amount = round($store->delivery_amount / 2, 0);
                }

				if ($store->sub_total < $store->minimum_order_amount) {
                    $response['cart']['is_minimum_reached'] = 0;
                }

				//valida que todas las tiendas tengan cobertura en la dirección
                try {
                    $store->validateStoreDeliversToAddress($address->latitude, $address->longitude);
                } catch (MerqueoException $exception) {
                    $this->response['message'] = "En este momento la tienda {$store->name} no tiene cobertura en tu dirección.";
                    return $this->jsonResponse();
                }
				//credito
				if (isset($discount_credit_amount) && $discount_credit_amount > 0) {
                    $store->discount_amount = $discount_credit_amount;
                }

				//porcentaje de descuento
				if (isset($discount_percentage_amount) && $discount_percentage_amount > 0){
					$discount_amount = round(($store->sub_total * $discount_percentage_amount) / 100);
					$store->discount_amount = $discount_amount;
					$total_discount_amount += $discount_amount;
					$store->discount_percentage_amount = $discount_percentage_amount;
				}
                //tiempo de entrega inmediata
                if ($store->delivery_time_minutes > $delivery_time_minutes)
                    $delivery_time_minutes = $store->delivery_time_minutes;
			}

	        if (isset($response['cart']['is_minimum_reached']) && $response['cart']['is_minimum_reached'] == 1)
			{
			    //cupon utilizado
                if (isset($coupon_id)){
                    //cargar cupon como utilizado a usuario
                    $user_coupon = new UserCoupon;
                    $user_coupon->coupon_id = $coupon_id;
                    $user_coupon->user_id = $this->user_id;
                    $user_coupon->save();

                    $coupon = Coupon::find($coupon_id);
                    if ($coupon && $coupon->type == 'Credit Amount'){
                        //sumar credito de cupon a usuario
                        $user_credit = new UserCredit;
                        $user_credit->user_id = $user->id;
                        $user_credit->coupon_id = $coupon->id;
                        $user_credit->amount = $coupon->amount;
                        $user_credit->type = 1;
                        $user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por redimir cupón código '.$coupon->code;
                        $user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
                        $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                        $user_credit->save();

                        if (!$user->first_coupon_used && $coupon->type_use == 'Only The First Order'){
                            $user->first_coupon_used = 1;
                            $user->save();
                        }
                    }
                }

                //obtener zona
                $zone_id = null;
                $zones = Zone::where('city_id', $address->city_id)->where('status', 1)->get();
                if ($zones){
                    foreach ($zones as $zone){
                        if (point_in_polygon($zone->delivery_zone, $address->latitude, $address->longitude)){
                            $zone_id = $zone->id;
                            break;
                        }
                    }
                }

				//crea un nuevo grupo para este pedido
				$order_group = new OrderGroup;

				//realizar cargo a tarjeta
				if ($is_credit_card)
				{
					$post_data_validation = $post_data;
					$post_data_validation['total_amount'] = 1000;
					$post_data_validation['tax_amount'] = 0;
					$post_data_validation['installments_cc'] = 1;
					$post_data_validation['order_id'] = 1;
					$post_data_validation['description'] = 'Validación de tarjeta de crédito';
					$post_data_validation['nit'] = 'No especificado';
					$result = $payment->createCreditCardCharge($post_data_validation);

					if (!$result['status']){
						$this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
						return $this->jsonResponse();
					}

					$result = $payment->refundCreditCardCharge($user->id, $result['response']->id);
					if (!$result['status']){
						$this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
						return $this->jsonResponse();
					}
				}

                //validar datos de fraude
                $posible_fraud = $this->validateOrderFraud($post_data);
                $posible_fraud = empty($posible_fraud) ? 0 : $posible_fraud;

				$order_group->source = 'Device';
                if (isset($post_data['source_os']))
                    $order_group->source_os = $post_data['source_os'];
				$order_group->user_device_id = $this->user_device_id;
				$order_group->user_id = $this->user_id;
				$order_group->user_firstname = $user->first_name;
				$order_group->user_lastname = $user->last_name;
				$order_group->user_email = $user->email;
				$order_group->user_address = $address->address.' '.$address->address_further;
                $order_group->user_address_latitude = $address->latitude;
                $order_group->user_address_longitude = $address->longitude;
				$order_group->user_city_id = $address->city_id;
				$order_group->user_phone = $user->phone;
				$order_group->address_id = $address_id;
                $order_group->zone_id = $zone_id;
				$order_group->discount_amount = $total_discount_amount;
				$order_group->delivery_amount = $response['cart']['delivery_amount'];
				$order_group->total_amount = $response['cart']['total_amount'];
				$order_group->products_quantity = $response['cart']['total_quantity'];
				$order_group->user_comments = trim($post_data['comments'].' '.$post_data['comments_further']);
                $order_group->ip = get_ip();
                $order_group->save();

				//dia y hora de entrega
                if (isset($post_data['delivery_time_today']) && $post_data['delivery_time_today'] == 'immediately'){
                    /*$hour_begin = Date('Y-m-d H:i:s');
                    $hour_today = new DateTime($hour_begin);
                    $hour_end = $hour_today->modify('+'.$delivery_time_minutes.' minutes');
	                $hour_end = $hour_end->format('Y-m-d H:i:s');
					$delivery_time = get_delivery_time($hour_begin, $hour_end);
					$delivery_time_text = 'Immediately';*/
					$hour_today = new DateTime();
                    $hour_today->modify('+'.$delivery_time_minutes.' minutes');
                    $delivery_time = $hour_today->format('H:i:s');
                    $delivery_time_text = 'Immediately';
                }else{
                    $delivery_time = isset($post_data['delivery_time_today']) && !empty($post_data['delivery_time_today']) ? $post_data['delivery_time_today'] : $post_data['delivery_time'];
                    $delivery_time = explode(' - ', $delivery_time);
                    $delivery_time_begin = explode(':', $delivery_time[0]);
					$hour_begin = new DateTime();
					$hour_begin->setTime($delivery_time_begin[0], $delivery_time_begin[1]);

					$delivery_time_end = explode(':', $delivery_time[1]);
					$hour_end = new DateTime();
					$hour_end->setTime($delivery_time_end[0], $delivery_time_end[1]);

					$delivery_time_text = $hour_begin->format('g:i a').' - '.$hour_end->format('g:i a');
					$hour_begin = $post_data['delivery_day'].' '.$delivery_time[0];
					if ($hour_end->format('G') < 3)
					   $post_data['delivery_day'] = date('Y-m-d', strtotime($post_data['delivery_day'] . '+1 day'));

					$hour_end = $post_data['delivery_day'].' '.$delivery_time[1];
					$delivery_time = get_delivery_time($hour_begin, $hour_end);
                }
                //crea un pedido por cada tienda
				foreach ($response['cart']['stores'] as $store_id => $store)
				{
					$order = new Order;
					$order->reference = generate_reference();
					$order->user_id = $this->user_id;
					$order->store_id = $store_id;
					$order->date = date("Y-m-d H:i:s");
					$order->group_id = $order_group->id;
					$order->total_products = count($store->products);
					$order->delivery_amount = $store->delivery_amount;
					$order->discount_amount = $store->discount_amount;
					$order->total_amount = $store->sub_total;

					$status = $this->validate_history_order_payment($post_data);
					if ($status) {
                        $order->status = 'Initiated';
                    } else {
                        $order->status = $post_data['payment_method'] == 'Tarjeta de crédito' || $posible_fraud ? 'Validation' : 'Initiated';
                    }

					$order->user_score_token = generate_token();
                    $order->delivery_date = $post_data['delivery_day'].' '.$delivery_time;
                    $order->delivery_time = $delivery_time_text;
                    $order->payment_method = $post_data['payment_method'];
                    $order->posible_fraud = $posible_fraud;

                    if ($is_credit_card){
                        //guardar datos de tarjeta
                        $order->credit_card_id = $user_credit_card->id;
                        $order->cc_token = $user_credit_card->card_token;
                        $order->cc_holder_name = $user_credit_card->holder_name;
                        $order->cc_last_four = $user_credit_card->last_four;
                        $order->cc_type = $user_credit_card->type;
                        $order->cc_country = $user_credit_card->country;
                        $order->cc_installments = $post_data['installments_cc'];
                    }

					$order->save();

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'ORDER_CREATED';
                    $log->user_id = $this->user_id;
                    $log->order_id = $order->id;
                    $log->save();

					foreach ($cart_products as $cart_product)
					{
						if ($cart_product->id)
                            $product = Product::find($cart_product->id);
                        else{
                            $product = (object) array(
                                'id' => 0,
                                'special_price' => null,
                                'price' => 0,
                                'name' => $cart_product->cart_product_name,
                                'image_medium_url' => image_url().'no_disponible.jpg',
                                'comment' => $cart_product->cart_comment,
                                'quantity' => 1,
                                'unit' => 'Unid',
                                'promo_type' => null,
                                'promo_type_special_price' => 0,
                                'store_id' => $cart_product->cart_store_id,
                                'total_sale' => 0,
                                'type' => 'Product'
                            );
                        }
						//no deja que al pedido le aparezcan productos que no pertenecen a la tienda
						if ($product->store_id != $store_id) continue;

						$order_product = new OrderProduct;
						$order_product->order_id = $order->id;
						$order_product->product_id = $product->id;
						$price = $product->special_price ? $product->special_price : $cart_product->cart_price;
						$order_product->price = $price;
						$order_product->original_price = $product->price;
						$order_product->quantity = $cart_product->cart_quantity;
                        $order_product->promo_type = $product->promo_type_special_price;
						$order_product->fulfilment_status = 'Pending';
						$order_product->type = $product->id ? 'Product' : 'Custom';
						$order_product->parent_id = 0;
						$order_product->product_name = $product->name;
						$order_product->product_image_url = $product->image_medium_url;
						$order_product->product_quantity = $product->quantity;
						$order_product->product_unit = $product->unit;
						$order_product->store_id = $product->store_id;
                        $order_product->product_comment = $cart_product->cart_comment;
						$order_product->save();

						$products_mail[$order->id][] = array(
							'img' => $product->image_medium_url,
							'name' => $product->name,
							'qty' => $cart_product->cart_quantity,
							'price' => $price,
							'total' => $cart_product->cart_quantity * $price,
							'type' => $product->type
						);

					}

					//descontar credito utilizado a usuario
					if ($store->discount_amount){
					    $user_credit = new UserCredit;
						$user_credit->user_id = $user->id;
						$user_credit->order_id = $order->id;
						$user_credit->amount = $store->discount_amount;
						$user_credit->type = 0;
						$user_credit->description = 'Deducción de '.currency_format($user_credit->amount).' utilizados en pedido con ID # '.$order->id;
						$user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
						$user_credit->save();
					}

				}

                Session::forget('coupon_code');
                Session::save();

				$orders = Order::where('group_id', $order_group->id)->get();
				$user = User::find($this->user_id);

				foreach($orders as $order) {
					//enviar mail de pedido a usuario
                    $store = Store::find($order->store_id);
                    $mail = array(
                        'template_name' => 'emails.order_received',
                        'subject' => self::SUBJECT_EMAIL,
                        'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
                        'vars' => array(
                            'order' => $order,
                            'order_group' => $order_group,
                            'store' => $store,
                            'products' => $products_mail[$order->id]
                        )
                    );
                    send_mail($mail);
				}

				//elimina productos del carrito
				CartProduct::where('cart_id', $cart->id)->delete();

				$count_orders = Order::where('user_id', '=', $user->id)->count();
				$type = $count_orders == 1 ? 'new' : 'old';

				$this->user_discounts = User::getDiscounts($user->id);

				$this->response = array(
					'status' => true,
					'message' => 'Pedido registrado',
					'result' => array(
						'order_id' => $order->id,
						'order_reference' => $order->reference,
						'order_date' => format_date('normal_with_time', $order_group->created_at),
						'order_address' => $order_group->user_address,
						'order_products' => $order_group->products_quantity,
						'order_total' => $order_group->total_amount + $order_group->delivery_amount - $order_group->discount_amount,
						'order_payment_method' => $order->payment_method,
						'user_credit_amount' => $this->user_discounts['amount']
					)
				);

				DB::commit();

			}else $this->response['message'] = 'Valor mínimo de pedido no alcanzado.';

		}catch(\Exception $e){
		    DB::rollback();
			throw $e;
		}

		return $this->jsonResponse();
	}

	/**
	 * Validar cupon en compra
	 *
	 * @return array $response Respuesta
	 */
	public function coupon()
	{
		$coupon_code = Input::get('coupon_code');
		if (empty($coupon_code))
			return $this->jsonResponse('Codigo del cupón es requerido.', 400);

		if (Input::get('user_id'))
			$user = User::find(Input::get('user_id'));
		else $user = false;

		//validar codigo del cupon
		$coupon = Coupon::where('code', $coupon_code)->where('status', 1)->first();
		if (!$coupon){
			$this->response['message'] = 'El código del cupón no es valido';
			return $this->jsonResponse();
		}
		//validar fecha de expiracion
		if (date('Y-m-d') > $coupon->expiration_date){
			$this->response['message'] = 'El cupón ya no esta vigente.';
			return $this->jsonResponse();
		}
		//validar numero de usos
		$uses = UserCoupon::where('coupon_id', $coupon->id)->count();
		if ($uses >= $coupon->number_uses){
			$this->response['message'] = 'El cupón ya fue utilizado.';
			return $this->jsonResponse();
		}

		//validar si el usuario ya utilizo el cupon
		$user_coupon = $user ? UserCoupon::where('user_id', $user->id)->where('coupon_id', $coupon->id)->first() : false;
		if ($user_coupon){
			$this->response['message'] = 'Ya redimiste este cupón';
			return $this->jsonResponse();
		}
		//validar credito de primera compra
		/*if ($user && $user->first_coupon_used){
			$this->response['message'] = 'Este tipo de cupón ya fue redimido en tu cuenta.';
			return $this->jsonResponse();
		}*/
		//validar el tipo de uso del cupon
		if ($user && $coupon->type_use == 'Only The First Order'){
			$count_orders = Order::where('user_id', '=', $user->id)->where('status', '<>', 'Cancelled');
			//ignorar pedido especifico
			if (Input::has('order_id'))
				$count_orders->where('id', '<>', Input::get('order_id'));
			$count_orders = $count_orders->count();
			if ($count_orders){
				$this->response['message'] = 'Este cupón solo podias utilizarlo en tu primera compra.';
				return $this->jsonResponse();
			}
		}
		if ($user && $user->created_at < date('Y-m-d H:i:s') && $coupon->type_use == 'Only New Customers' && !Input::has('is_new_user')){
			$this->response['message'] = 'Este cupón solo podias utilizarlo al crear tu cuenta de Merqueo.';
			return $this->jsonResponse();
		}
		//validar cupon en grupo de campaña
		if ($user && !empty($coupon->campaign_validation)){
			$campaign = Coupon::select('coupons.id')
									->join('user_coupons', function($join) use ($user){
										$join->on('user_coupons.coupon_id', '=', 'coupons.id');
										$join->on('user_coupons.user_id', '=', DB::raw($user->id));
									})
									->where('coupons.campaign_validation', $coupon->campaign_validation)
									->first();
			if($campaign){
				$this->response['message'] = 'Ya usaste un cupón de esta campaña.';
		    	return $this->jsonResponse();
			}
		}
		//validar tipo de redencion
		if ($coupon->redeem_on == 'Specific Product'){
			if (!Input::has('mobile') && !$this->productIsInCart($coupon->product_id)){
				if ($product = Product::find($coupon->product_id))
					$this->response['message'] = 'Este cupón aplica exclusivamente para el producto "'.$product->name.'" debes agregarlo al carrito.';
				else $this->response['message'] = 'El producto asociado al cupón no es valido.';
				return $this->jsonResponse();
			}
		}

		//aplicar descuento con credito a compra
		if ($coupon->type == 'Credit Amount'){
			//validar antes de procesar compra
	 		if (Input::has('validate')){
	 			$this->response = array(
					'status' => true,
					'message' => 'Cupón valido',
					'result' => array('coupon_id' => $coupon->id, 'coupon_amount' => $coupon->amount)
				);

	 			return $this->jsonResponse();
			}

			if ($user){
				//cargar cupon como utilizado a usuario
				$user_coupon = new UserCoupon;
				$user_coupon->coupon_id = $coupon->id;
				$user_coupon->user_id = $user->id;
				$user_coupon->save();
				//registrar movimiento de crédito a usuario
				$user_credit = new UserCredit;
				$user_credit->user_id = $user->id;
                $user_credit->coupon_id = $coupon->id;
				$user_credit->amount = $coupon->amount;
				$user_credit->type = 1;
				$user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por redimir cupón código '.$coupon->code;
				$user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
				$user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
				$user_credit->save();

				$credit = User::getDiscounts($user->id);
				$this->response = array(
					'status' => true,
					'message' => 'Cupón redimido con éxito, se ha cargado $'.number_format($coupon->amount, 0, ',', '.').' de crédito a tu cuenta.',
					'result' => array(
						'credit_amount' => $credit['amount'],
						'amount_minimum_discount' => number_format(Config::get('app.minimum_discount_amount'), 0, ',', '.'),
						'coupon_product_id' => $coupon->product_id
					)
				);
			}else{
				Session::put('coupon_code', $coupon->code);
				Session::save();
				$this->response = array(
					'status' => true,
					'message' => 'Cupón redimido con éxito, se ha cargado $'.number_format($coupon->amount, 0, ',', '.').' de crédito a tu cuenta.',
					'result' => array(
						'credit_amount' => $coupon->amount,
						'amount_minimum_discount' => number_format(Config::get('app.minimum_discount_amount'), 0, ',', '.'),
						'coupon_product_id' => $coupon->product_id
					)
				);
			}
		}else{
			//aplicar descuento con porcentaje a compra
			if ($coupon->type == 'Discount Percentage'){
				//validar antes de procesar compra
		 		if (Input::has('validate')){
		 			$this->response = array(
		 				'status' => true,
		 				'message' => 'Cupón valido',
		 				'result' => array('coupon_id' => $coupon->id, 'coupon_amount' => $coupon->amount)
					);
					return $this->jsonResponse();
				}

				Session::put('coupon_code', $coupon->code);
				$credit = User::getDiscounts();
				$this->response = array(
					'status' => true,
					'message' => 'Se ha aplicado '.$coupon->amount.'% de descuento a tu pedido actual',
					'result' => array(
						'credit_amount' => $credit['amount'],
						'amount_minimum_discount' => number_format(Config::get('app.minimum_discount_amount'), 0, ',', '.'),
						'coupon_product_id' => $coupon->product_id
					)
				);
			}
		}

		return $this->jsonResponse();
	}

	/**
	 * Registrar calificacion de pedido
	 *
	 * @return array $response Respuesta
	 */
	public function score_order()
	{
		if ($order_id = Route::current()->parameter('id'))
		{
			if (!Input::has('score') || !Input::has('user_id'))
				return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

			$user_id = Input::get('user_id');
			if (!$order = Order::where('id', $order_id)->where('user_id', $user_id)->where('status', 'Delivered')->first()){
                $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual o el estado para calificarlo no es valido.';
                return $this->jsonResponse();
            }

			$order->user_score = Input::get('score');
			$order->user_score_comments = Input::has('comments') ? Input::get('comments') : null;
			$order->user_score_date = date('Y-m-d H:i:s');
			$order->save();

			$log = new OrderLog();
			$log->type = 'ORDER_SCORE_UPDATE_APP: '.Input::get('score');
			$log->user_id = $user_id;
			$log->order_id = $order->id;
			$log->save();

			$this->response = array('status' => true, 'message' => 'Calificación registrada.');

		}else return $this->jsonResponse('El ID del pedido es requerido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtener datos de pedido para notificacion
	 *
	 * @return array $response Respuesta
	 */
	public function notification_order()
	{
		if ($order_id = Route::current()->parameter('id'))
		{
			if (!Input::has('type') || !Input::has('user_id'))
				return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

			$user_id = Input::get('user_id');
			$order = Order::join('stores', 'stores.id', '=', 'orders.store_id')
			->select('stores.name AS store_name', 'minimum_order_amount', 'app_logo_small_url', 'orders.*')
			->where('orders.id', $order_id)->where('user_id', $user_id)->first();
			if (!$order){
                $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual.';
                return $this->jsonResponse();
            }

			//notificacion pedido confirmado
			if (Input::get('type') == 'In Progress' && $shopper = Shopper::find($order->shopper_id)){
				$result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('shopper_id', $order->shopper_id)
				->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('shopper_id')->first();
				$shopper_score_average = $result ? round($result->average, 2) : 0;
				$minutes = !empty($order->shopper_delivery_time_minutes) ? $order->shopper_delivery_time_minutes : 90;
                $cloudfront_url = Config::get('app.aws.cloudfront_url');

				$data =  array(
					'type' => Input::get('type'),
					'shopper' => array(
						'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
						'name' => $shopper->first_name.' '.$shopper->last_name,
						'phone' => $shopper->phone,
						'shopper_score_average' => $shopper_score_average,
						'delivery_time' => $minutes.' minutos',
					 ),
					'order' => array(
						'id' => $order->id,
						'total_amount' => $order->total_amount,
						'total_delivery' => $order->delivery_amount,
                        'total_discount' => $order->discount_amount,
						'products' => OrderProduct::select('product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name', 'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit')
											->where('order_id', $order->id)->get()->toArray()
					)
				);
			}
			//notificacion pedido entregado
			if (Input::get('type') == 'Delivered' && $shopper = Shopper::find($order->shopper_id)){
				$data =  array(
					'shopper' => array(
						'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
						'name' => $shopper->first_name.' '.$shopper->last_name
					 ),
				);
			}
			if (Input::get('type') == 'Cancelled'){
				$data['reject_reason'] = $order->reject_reason;
			}
			$data['type'] = Input::get('type');
			$data['store']['name'] = $order->store_name;
			$data['store']['minimum_order_amount'] = $order->minimum_order_amount;
            $data['store']['app_logo_small_url'] = $order->app_logo_small_url;

			$this->response = array('status' => true, 'message' => 'Notificación obtenida.', 'result' => $data);

		}else return $this->jsonResponse('El ID del pedido es requerido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtener pedidos pendientes por usuario
	 *
	 * @return array $response Respuesta
	 */
	public function pending_orders()
	{
		if ($user_id = Route::current()->parameter('user_id'))
		{
			$orders = Order::select('orders.id', 'orders.reference', 'orders.status', 'store_id', 'date', 'total_products', 'orders.total_amount',
							'orders.delivery_amount', 'orders.discount_amount', 'orders.payment_method', 'order_groups.user_address',
							'stores.name AS store_name', 'stores.app_logo_small_url AS store_logo_url')
							->join('order_groups', 'group_id', '=', 'order_groups.id')
							->join('stores', 'store_id', '=', 'stores.id')
							->where('orders.user_id', $user_id)->where('orders.status', '<>', 'Delivered')->where('orders.status', '<>', 'Cancelled')
							->orderBy('orders.id', 'DESC')->get();
			$status = array(
			    'Validation' => 'Iniciado',
				'Initiated' => 'Iniciado',
				'In Progress' => 'En Proceso',
				'Dispatched' => 'En Camino',
				'Delivered' => 'Entregado',
				'Cancelled' => 'Cancelado'
			);
			$orders_user = array();
            if ($orders){
    			foreach($orders as $order){
    				$order->status = $status[$order->status];
    				$order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
    				$order->products = OrderProduct::select('product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name',
    									'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit')
    									->where('order_id', $order->id)->get()->toArray();

    				$orders_user[] = $order->toArray();
    			}
			}

			$this->response = array(
				'status' => true,
				'message' => 'Pedidos obtenidos',
				'result' => $orders_user
			);

		}else return $this->jsonResponse('El ID usuario es requerido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Valida si un producto esta agregado en el carrito actual
	 *
	 * @param int $product_id ID product
	 */
	protected function productIsInCart($product_id)
	{
		$cart_id = Session::get('cart_id');
	    if(Session::has('tmp_cart'))
			$cart_user = json_decode(Session::get('tmp_cart'));
		else {
			//si no hay un carrito, crear un esqueleto del nuevo carrito para la sesión
			$cart_user = (object)array('products' => (object)array());
			Session::put('tmp_cart', json_encode($cart_user));
		}

		// Se simula obtener el carrito por DB para el usuario guest que sólo maneja sesión
		$products_ids = array();
		foreach($cart_user->products as $product) {
			if (property_exists($product, 'cart_quantity') && $product->cart_quantity <= 0) continue;
			$products_ids[] = $product->product_id;
		}
		$products = Product::whereIn('id', $products_ids)->get();

		// Armo la estructura de los productos:
		$cart_products = array();
		foreach ($products as $product)
			$cart_products[$product->id] = (object)array_merge($product->toArray(), (array)$cart_user->products->{$product->id});
		$cart_users = false;

		if (isset($cart_products) && array_key_exists($product_id, $cart_products))
			return true;
		else return false;
	}

}
