<?php

namespace api\v1;

use Input, Route, Request, DB, Store, Department, Shelf, Product, Order, SearchLog;

class ApiStoreController extends \ApiController {

	/**
	 * Obtiene tiendas que atienden una ubicación de latitud y longitud
	 *
	 * @return array $response Respuesta
	 */
	public function get_stores()
	{
	    $address = Input::get('address');
		if (Input::has('lat') && Input::has('lng') && empty($address)){
			$lat = Input::get('lat');
			$lng = Input::get('lng');
		}else{
			$city = Input::get('city');
			$address = urldecode($address);
			if (!empty($address) && !empty($city)){
				//obtener latitud longitud
				$request = Request::create('/api/location', 'GET', array('address' => $address, 'city' => $city));
				Request::replace($request->input());
				$result = json_decode(Route::dispatch($request)->getContent(), true);
				if ($result['status']){
					$lat = $result['result']['latitude'];
					$lng = $result['result']['longitude'];
					$address = $result['result']['address'];
				}else{
					$lat = 1;
					$lng = 1;
				}
			}else $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
		}

		if (isset($lat) && isset($lng)){
			$result = array();
			$rows = Store::getByLatlng($lat, $lng);
			if (count($rows)){
				foreach($rows as $store){
					$slot = Store::getDeliverySlot($store->id);
					$store->is_open = $slot['is_open'];
					$store->slot = $slot['next_slot'];
					$stores[] = $store->toArray();
				}
				//obtener direccion
				if (!isset($address)){
					$request = Request::create('/api/location_reverse', 'GET', array('lat' => $lat, 'lng' => $lng));
					Request::replace($request->input());
					$result = json_decode(Route::dispatch($request)->getContent(), true);
					$address = $result['status'] ? $result['result'] : false;
				}

				$this->response = array(
					'status' => true,
					'message' => 'Se encontraron tiendas dentro de la ubicación',
					'result' => array(
						'address' => array(
							'address' => trim($address),
							'lat' => $lat,
							'lng' => $lng
						),
					'stores' => $stores)
				);
			}else $this->response = array(
					'status' => false,
					'message' => 'En este momento no tenemos cobertura en tu dirección'
				  );
		}

		return $this->jsonResponse();
	}

	/**
	 * Obtiene datos de tienda, arbol de categorias y categorias con productos
	 *
	 * @return array $response Respuesta
	 */
	public function get_store()
	{
		if ($id = Route::current()->parameter('id'))
		{
			$store = Store::find($id);
			if ($store){
				$slot = Store::getDeliverySlot($store->id);
				$store->is_open = $slot['is_open'];
				$store->slot = $slot['next_slot'];
				$result['store'] = $store->toArray();
				//paginacion para departments
				$rows = 5;
				if (Input::has('p'))
					$page = intval(Input::get('p'));
				if (!isset($page)) {
				    $begin = 0;
				    $page = 1;
				}else $begin = ($page - 1) * $rows;
				$departments = Department::select('departments.*')->join('products', 'departments.id', '=', 'department_id')->where('departments.store_id', $store->id)
				->where('products.status', 1)->where('departments.status', 1);
                if (Input::has('user_id')){
                    if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                        $departments->where('first_order_special_price', 0);
                }
                $departments = $departments->groupBy('departments.id')->orderBy('departments.sort_order')->orderBy('departments.name')->limit($rows)->offset($begin)->get();
				$result['store']['departments'] = $departments ? $departments->toArray() : array();

				foreach($result['store']['departments'] as $index => $department){
					$products = Product::where('department_id', $department['id'])->where('store_id', $store->id)->where('status', 1);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $products->where('first_order_special_price', 0);
                    $products = $products->limit(6)->orderBy('total_sale', 'desc')->orderBy('sort_order')->get();
					$result['store']['departments'][$index]['products'] = $products ? $products->toArray() : array();
				}

				//categorias y subcategorias para menu
				if ($page == 1){
					$departments = Department::select('departments.*')->join('products', 'departments.id', '=', 'department_id')->where('departments.store_id', $store->id)->where('departments.status', 1)->where('products.status', 1);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $departments->where('first_order_special_price', 0);
                    $departments = $departments->groupBy('departments.id')->orderBy('departments.sort_order')->orderBy('departments.name')->get();
					$result['store']['menu']['departments'] = $departments ? $departments->toArray() : array();
					foreach($departments as $index => $department){
						$shelves = Shelf::select('shelves.*')->join('products', 'shelves.id', '=', 'shelf_id')->where('shelves.department_id', $department['id'])->where('products.status', 1)->where('shelves.status', 1);
                        if (Input::has('user_id') && isset($has_orders) && $has_orders)
                            $shelves->where('first_order_special_price', 0);
                        $shelves = $shelves->groupBy('shelves.id')->orderBy('shelves.sort_order')->orderBy('shelves.name')->get();
						$result['store']['menu']['departments'][$index]['shelves'] = $shelves->toArray();
					}
				}

				//si el usuario esta logueado mostrar productos comprados
				if ($page == 1 && Input::has('user_id')){
					$products = Product::join('order_products','products.id','=','order_products.product_id')
								   ->join('orders','orders.id','=','order_products.order_id')
								   ->where('user_id', Input::get('user_id'))
								   ->where('products.store_id', $store->id)
								   ->where('products.status', 1)
								   ->select(DB::raw('products.*, SUM(order_products.quantity) AS quantity'))
								   ->groupBy('products.id')
								   ->orderBy('quantity', 'desc')
								   ->get();

				   $result['user_products'] = $products;
				}

				$this->response = array(
					'status' => true,
					'message' => 'Tienda obtenida',
					'result' => $result
				);
			}else return $this->jsonResponse('ID no existe', 400);
		}

		return $this->jsonResponse();
	}

	/**
	 * Obtiene categoria con subcategorias y productos
	 *
	 * @return array $response Respuesta
	 */
	public function get_department()
	{
		if ($id = Route::current()->parameter('department_id'))
		{
			$store_id = Route::current()->parameter('store_id');
			$department = Department::select('departments.*')->join('products', 'departments.id', '=', 'department_id')->where('departments.store_id', $store_id)
            ->where('products.status', 1)->where('departments.status', 1)->groupBy('departments.id')->where('departments.id', $id);
            if (Input::has('user_id')){
                if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                    $department->where('first_order_special_price', 0);
            }
            $department = $department->first();
			if ($department){
				$result['department'] = $department ? $department->toArray() : array();
				//paginacion para shelves
				$rows = 5;
				if (Input::has('p'))
					$page = intval(Input::get('p'));
				if (!isset($page)) {
				    $begin = 0;
				    $page = 1;
				}else $begin = ($page - 1) * $rows;
				$shelves = Shelf::select('shelves.*')->join('products', 'shelves.id', '=', 'shelf_id')->where('shelves.department_id', $department->id)
                ->where('products.status', 1)->where('shelves.status', 1);
                if (Input::has('user_id') && isset($has_orders) && $has_orders)
                    $shelves->where('first_order_special_price', 0);
                $shelves = $shelves->groupBy('shelves.id')->orderBy('shelves.sort_order')->orderBy('shelves.name')->limit($rows)->offset($begin)->get();
				$result['department']['shelves'] = $shelves ? $shelves->toArray() : array();

				foreach ($shelves as $index => $shelf) {
					$shelf_data = array();
					$products = Product::where('shelf_id', $shelf->id)->where('status', 1);
                    if (Input::has('user_id') && isset($has_orders) && $has_orders)
                        $products->where('first_order_special_price', 0);
                    $products = $products->limit(6)->orderBy('total_sale', 'desc')->orderBy('sort_order')->get();
					foreach ($products as $product) {
						array_push($shelf_data, $product->toArray());
					}
					$result['department']['shelves'][$index]['products'] = $shelf_data;
				}

				$this->response = array(
					'status' => true,
					'message' => 'Categoria obtenida',
					'result' => $result
				);
			}else return $this->jsonResponse('ID no existe o categoria sin productos', 400);
		}

		return $this->jsonResponse();
	}

	/**
	 * Obtiene subcategoria con productos
	 *
	 * @return array $response Respuesta
	 */
	public function get_shelf()
	{
		if ($id = Route::current()->parameter('shelf_id'))
		{
			$store_id = Route::current()->parameter('store_id');
			$department_id = Route::current()->parameter('department_id');
			$shelf = Shelf::select('shelves.*')->join('products', 'shelves.id', '=', 'shelf_id')->where('shelves.store_id', $store_id)->where('shelves.department_id', $department_id)
            ->where('products.status', 1)->where('shelves.status', 1);
            if (Input::has('user_id')){
                if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                    $shelf->where('first_order_special_price', 0);
            }
            $shelf = $shelf->groupBy('shelves.id')->where('shelves.id', $id)->first();
			if ($shelf){
				$result['shelf'] = $shelf ? $shelf->toArray() : array();
				$shelf_data = array();
				//paginacion para productos
				$rows = 24;
				if (Input::has('p'))
					$page = intval(Input::get('p'));
				if (!isset($page)) {
				    $begin = 0;
				    $page = 1;
				}else $begin = ($page - 1) * $rows;
				$products = Product::where('shelf_id', $shelf->id)->where('status', 1);
                if (Input::has('user_id') && isset($has_orders) && $has_orders)
                    $products->where('first_order_special_price', 0);
                $products = $products->orderBy('total_sale', 'desc')->orderBy('sort_order')->limit($rows)->offset($begin)->get();

				foreach ($products as $product) {
					array_push($shelf_data, $product->toArray());
				}
				$result['shelf']['products'] = $shelf_data;

				$this->response = array(
					'status' => true,
					'message' => 'Subcategoria obtenida',
					'result' => $result
				);
			}else return $this->jsonResponse('ID no existe o subcategoria sin productos', 400);
		}

		return $this->jsonResponse();
	}

    /**
     * Obtiene producto
     *
     * @return array $response Respuesta
     */
    public function get_product()
    {
        if ($id = Route::current()->parameter('id'))
        {
            $product = Product::where('id', $id)->where('status', 1)->first();

            if ($product){
                $this->response = array(
                    'status' => true,
                    'message' => 'Producto obtenido',
                    'result' => $product->toArray()
                );
            }else return $this->jsonResponse('ID no existe o esta inactivo', 400);
        }

        return $this->jsonResponse();
    }

	/**
	 * Buscador de productos en tienda
	 *
	 * @return array $response Respuesta
	 */
	public function search()
    {
        if ($id = Route::current()->parameter('id'))
        {
            $store = Store::find($id);
            $query = Product::where('products.store_id', $id);

            if (Input::has('q')){
                $q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);
                $keys = explode(' ', $q);
                if (count($keys)){
                    $keywords = '';
                    foreach($keys as $key){
                        if (($key != '') and ($key != ' ')){
                            if ($keywords != '') $keywords .= '-space-';
                            $keywords .= $key;
                        }
                    }
                    $keywords = explode('-space-', $keywords);
                    foreach($keywords as $keyword){
                        $query->whereRaw('products.name LIKE "%'.trim($keyword).'%" COLLATE utf8_spanish_ci');
                    }
                }

                $query->select('products.*', 'departments.slug AS department_slug', 'shelves.slug AS shelf_slug')->distinct()
                    ->join('departments', 'department_id', '=', 'departments.id')
                    ->join('shelves', 'shelf_id', '=', 'shelves.id')
                    ->join('stores', 'departments.store_id', '=', 'stores.id')
                    ->where('departments.status', 1)->where('shelves.status', 1)
                    ->where('stores.status', 1)->where('products.status', 1);
                    if ( !empty($keywords) ) {
                        foreach($keywords as $keyword){
                            $query->orderBy(DB::raw('   CASE
                                                            WHEN products.name LIKE \''.trim($keyword).' %\' THEN 0
                                                            WHEN products.name LIKE \'% %'.trim($keyword).'% %\' THEN 1
                                                            WHEN products.name LIKE \'%'.trim($keyword).'\' THEN 2
                                                            ELSE 3
                                                        END
                                                    '));
                        }
                    }
                    if (Input::has('user_id')){
                        if ($has_orders = Order::select('orders.reference')->where('user_id', Input::get('user_id'))->where('status', 'Delivered')->orderBy('id', 'desc')->first())
                            $query->where('first_order_special_price', 0);
                    }
            }

            $rows = 15;
            if (Input::has('p'))
                $page = intval(Input::get('p'));
            if (!isset($page)) {
                $begin = 0;
                $page = 1;
            }else $begin = ($page - 1) * $rows;
            $products = $query->orderBy('products.name', 'asc')->limit($rows)->offset($begin)->get();

            $result = array();
            if (count($products)){
                foreach ($products as $product)
                    $result[] = $product;
                $this->response = array('status' => true, 'message' => 'Resultado de busqueda', 'result' => $result);
            }else $this->response = array('status' => true, 'message' => 'No se encontraron resultados.', 'result' => $result);

        }else return $this->jsonResponse('ID no existe', 400);

        return $this->jsonResponse();
    }

}
