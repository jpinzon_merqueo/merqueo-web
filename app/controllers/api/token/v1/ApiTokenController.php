<?php

namespace api\token\v1;

use \Firebase\JWT\JWT;
use Config;
//use Guzzle\Http\Message\Request;
//use Request;
//use Input;
use Auth;

use \Illuminate\Http\Request;

class ApiTokenController extends \ApiController
{
    private $key = null;
    private $expire = 0;
    public function __construct()
    {
        $this->key = Config::get('app.jwt.key');
        $this->expire = Config::get('app.jwt.expire');
    }

    public function generate_token()
    {
        try{
            $token = array(
                "sub" => Auth::user()->id,
                "exp" =>microtime(true)+$this->expire
            );

            $jwt = JWT::encode($token, $this->key);

            $this->response = array(
                'status' => true,
                'message' => 'Token generado',
                'result'=> $jwt
            );
        }catch(\Exception $e){
            $this->response = array(
                'status' => false,
                'message' => 'Token no generado',
                'result' => ''
            );
        }

        return $this->jsonResponse();
    }

}