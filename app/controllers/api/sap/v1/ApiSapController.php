<?php

namespace api\sap\v1;

use actions\QuerysForServicesAndTemplatesSAPAction;
use function foo\func;
use Input;
use Exception;

class ApiSapController extends \ApiController
{

    /**
     * @var QuerysForServicesAndTemplatesSAPAction
     */
    private $query;

    /**
     * Genera el token de autorización de consulta al api
     *
     * ApiSapController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->beforeFilter(function () {
            $headers = getallheaders();
            if (isset($headers['Authorization']) || isset($headers['authorization'])) {
                $authorization = isset($headers['Authorization']) ? $headers['Authorization'] : $headers['authorization'];
                try {
                    $authorization = base64_decode($authorization);
                } catch (Exception $e) {
                    return $this->jsonResponse('Acceso denegado.', 401);
                }

                $token = md5('M3rQu30' . (date('Y') * (intval(date('m')) - 1) * intval(date('d'))));
                if ($authorization != $token) {
                    return $this->jsonResponse('Acceso denegado.', 401);
                }
            } else return $this->jsonResponse('Acceso denegado.', 401);
        });

        $this->query = new QuerysForServicesAndTemplatesSAPAction();
    }

    /**
     * Redirecciona a las funciones especificas para obtener la información
     *
     * @param $template
     * @param $start_date
     * @param $end_date
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function templates($template, $start_date, $end_date)
    {
        $start_date = $start_date . ' 00:00:00';
        $end_date = $end_date . ' 23:59:59';
        switch ($template) {
            case 'products':
                $products = $this->query->getProducts($start_date, $end_date);
                if (count($products) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de productos obtenidos con exito.',
                        'result' => [
                            'products' => $products
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen productos en el rango de fechas ingresadas.',
                        'result' => [
                            'products' => $products
                        ]
                    ];
                }

                return $this->jsonResponse();
                break;

            case 'clients':
                $clients = $this->query->getClients($start_date, $end_date);
                if (count($clients) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de clientes obtenidos con exito.',
                        'result' => [
                            'clients' => $clients
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen clientes en el rango de fechas ingresadas.',
                        'result' => [
                            'clients' => $clients
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'providers':
                $providers = $this->query->getProviders($start_date, $end_date);
                if (count($providers) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de proveedores obtenidos con exito.',
                        'result' => [
                            'providers' => $providers
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen proveedores en el rango de fechas ingresadas.',
                        'result' => [
                            'providers' => $providers
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'retention-providers':
                $retentions = $this->query->getRetentionProviders($start_date, $end_date);
                if (count($retentions) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de retenciones de proveedores obtenidos con exito.',
                        'result' => [
                            'retention_providers' => $retentions
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen retenciones de proveedeores en el rango de fechas ingresadas.',
                        'result' => [
                            'retention_providers' => $retentions
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'purchase-invoices':
                $purchase_invoices = $this->query->getPurchaseInvoices($start_date, $end_date);
                if (count($purchase_invoices) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de facturas de compra obtenidos con exito.',
                        'result' => [
                            'purchase_invoices' => $purchase_invoices
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen facturas de compras en el rango de fechas ingresadas.',
                        'result' => [
                            'purchase_invoices' => $purchase_invoices
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'sale-invoices':
                $sale_invoices = $this->query->getSaleInvoices($start_date, $end_date);
                if (count($sale_invoices) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de facturas de venta obtenidos con exito.',
                        'result' => [
                            'sale_invoices' => $sale_invoices
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen facturas de ventas en el rango de fechas ingresadas.',
                        'result' => [
                            'sale_invoices' => $sale_invoices
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'credit-note-sale':
                $credit_note_sale = $this->query->getCreditNoteSale($start_date, $end_date);
                if (count($credit_note_sale) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de nota crédito de ventas obtenidos con exito.',
                        'result' => [
                            'credit_note_sale' => $credit_note_sale
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen datos de nota crédito de ventas en el rango de fechas ingresadas.',
                        'result' => [
                            'credit_note_sale' => $credit_note_sale
                        ]
                    ];
                }
                return $this->jsonResponse();

                break;

            case 'credit-note-purchase':
                $credit_note_purchase = $this->query->getCreditNotePurchase($start_date, $end_date);
                if (count($credit_note_purchase) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de nota crédito de compras obtenidos con exito.',
                        'result' => [
                            'credit_note_purchase' => $credit_note_purchase
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen datos de nota crédito de compras en el rango de fechas ingresadas.',
                        'result' => [
                            'credit_note_purchase' => $credit_note_purchase
                        ]
                    ];
                }
                return $this->jsonResponse();

                break;

            case 'inventory-entry':
                $inventory_entry = $this->query->getInventoryEntry($start_date, $end_date);
                if (count($inventory_entry) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de entrada de inventario obtenidos con exito.',
                        'result' => [
                            'inventory_entry' => $inventory_entry
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existen datos de entrada de inventario en el rango de fechas ingresadas.',
                        'result' => [
                            'inventory_entry' => $inventory_entry
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'inventory-output':
                $inventory_output = $this->query->getInventoryOutput($start_date, $end_date);
                if (count($inventory_output) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de salidas de inventario obtenidos con exito.',
                        'result' => [
                            'inventory_output' => $inventory_output
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existe salida de inventario en el rango de fechas ingresadas.',
                        'result' => [
                            'inventory_output' => $inventory_output
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            case 'payments-received':
                $payments_received = $this->query->getPaymentsReceived($start_date, $end_date);
                if (count($payments_received) > 0) {
                    $this->response = [
                        'status' => true,
                        'message' => 'Datos de pagos recibidos obtenidos con exito.',
                        'result' => [
                            'payments_received' => $payments_received
                        ]
                    ];
                } else {
                    $this->response = [
                        'status' => false,
                        'message' => 'No existe pagos recibidos en el rango de fechas ingresadas.',
                        'result' => [
                            'payments_received' => $payments_received
                        ]
                    ];
                }
                return $this->jsonResponse();
                break;

            default:
                echo "Opción no valida";
                break;
        }
    }
}
