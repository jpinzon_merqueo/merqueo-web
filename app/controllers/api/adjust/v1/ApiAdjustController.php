<?php

namespace api\adjust\v1;

use Adjust, Input, Route, DB, Config;

class ApiAdjustController extends \ApiController {

	/**
	* Registrar eventos de adjust
	*
	* @return array $response Respuesta
	*/
	public function adjust()
	{
	    $adjust = new Adjust;
        $adjust->event = Input::get('event');
        $adjust->user_id = intval(Input::get('user_id'));
        $adjust->store = Input::get('store');
        $adjust->campaign_name = Input::get('campaign_name');
        $adjust->creative_name = Input::get('creative_name');
        $adjust->adgroup_name = Input::get('adgroup_name');
        $adjust->app_version = Input::get('app_version');
        $adjust->tracker = Input::get('tracker');
        $adjust->tracker_name = Input::get('tracker_name');

        $created_at = intval(Input::get('created_at'));
        $date = new \DateTime();
		$date->setTimestamp($created_at);
		$adjust->date = $date->format('Y-m-d H:i:s');
		$adjust->city = Input::get('city');
        $adjust->device_name = Input::get('device_name');
        $adjust->device_type = Input::get('device_type');
        $adjust->os_name = Input::get('os_name');
        $adjust->os_version = Input::get('os_version');
        $adjust->ip_address = Input::get('ip_address');
        $adjust->environment = Input::get('environment');

		$adjust->save();

		return $this->jsonResponse('Datos almacenados satifactoriamente.', 200);
	}

}