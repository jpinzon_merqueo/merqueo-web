<?php

namespace api\v12;

use Input, UserDevice, UserDeviceLog, Config, City, AppErrorLog, User, UserAddress, UserCreditCard, Order, OrderProduct, Discount, StoreProduct;

class ApiIndexController extends \ApiController {

    /**
     * Metodo de inicializacion
     *
     * @return array $response Respuesta
     */
    public function init()
    {
        if (Input::has('lat') && Input::has('lng') && Input::has('brand')
        && Input::has('model') && Input::has('os') && Input::has('os_api') && Input::has('regid')
        && Input::has('os_version') && Input::has('app_version') && Input::has('api_version')){
            extract(Input::all());
            if ($regid != 'null' && $regid)
            {
                //SERVICIO INIT
                $os == 'IOS' ? 'iOS' : $os;
                $user_device_log = new UserDeviceLog;
                $user_device_log->lat = $lat;
                $user_device_log->lng = $lng;
                $user_device_log->brand = $brand;
                $user_device_log->model = $model;
                $user_device_log->os = $os;
                $user_device_log->os_version = $os_version;
                $user_device_log->os_api = $os_api;
                $user_device_log->app_version = $app_version;
                $user_device_log->api_version = $api_version;
                $user_device_log->regid = $regid;
                if (isset($user_id) && !empty($user_id))
                    $user_device_log->user_id = $user_id;
                $user_device_log->save();

                //registrar id del dispositivo
                if (!$user_device = UserDevice::where('regid', $regid)->first()){
                    $user_device = new UserDevice;
                    $user_device->regid = $regid;
                    $user_device->os = $os;
                    if (isset($user_id) && !empty($user_id))
                        $user_device->user_id = $user_id;
                    $user_device->save();
                }else{
                    if (isset($user_id) && !empty($user_id)){
                        $user_device->user_id = $user_id;
                        $user_device->save();
                    }
                }

                //SERVICIO VERSION
                $update = 'not_required'; //not_required, required, suggested
                $update_os = 'none'; //ios, android, all, none
                $update_message = 'No se requiere actualización.';

                $os = strtolower($os);
                $app_version = intval(str_replace('.', '', $app_version));
                if ($os == 'android'){
                    if ($app_version < 216){
                        $update_os = 'android';
                        $update = 'required';
                    }
                }
                if ($os == 'ios'){
                    if ($app_version < 215){
                        $update_os = 'ios';
                        $update = 'required';
                    }
                }

                if ($update == 'required' && ($os == $update_os || $update_os == 'all'))
                    $update_message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Por favor actualiza a la última versión para continuar. Muchas gracias!';
                if ($update == 'suggested' && ($os == $update_os || $update_os == 'all'))
                    $update_message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Te sugerimos actualizar a la última versión. Muchas gracias!';

                //SERVICIO CITIES
                if ($os == 'ios' && str_replace('.', '', $app_version) <= 215)
                    $cities = City::select('id', 'city', 'slug', 'latitude', 'longitude')->where('is_main', 1)->whereNotIn('id', [11, 12])->where('status', 1)->get();
                else $cities = City::select('id', 'city', 'slug', 'latitude', 'longitude')->where('is_main', 1)->where('status', 1)->get();
                $result_cities = [];
                if (count($cities)){
                    foreach ($cities as $city)
                    {
                        $city->latitude = $city->lat = floatval($city->latitude);
                        $city->longitude = $city->lng = floatval($city->longitude);
                        $surrounded_cities_result = [];
                        $surrounded_cities = City::select('id', 'city', 'slug', 'latitude', 'longitude')->where('parent_city_id', $city->id)->where('status', 1)->get();
                        foreach ($surrounded_cities as $surrounded_city)
                        {
                            $surrounded_city->latitude = floatval($surrounded_city->latitude);
                            $surrounded_city->longitude = floatval($surrounded_city->longitude);
                            $surrounded_cities_result[] =  $surrounded_city->toArray();
                        }
                        if (count($surrounded_cities_result))
                            array_unshift($surrounded_cities_result, $city->toArray());
                        $city->surrounded_cities = $surrounded_cities_result;
                        $result_cities[] = $city->toArray();
                    }
                }

                $init_message = array('show' => 0);

                //SERVICIO USUARIO
                $user = array();
                if (isset($user_id) && !empty($user_id)){
                    $user = $this->getUserData($user_id);
                    if ($user['user']['free_delivery_next_order'])
                        $init_message = array(
                            'show' => 1,
                            'title' => 'Domicilio Gratis',
                            'message' => 'Tienes domicilio gratis en tu próximo pedido.'
                        );
                }

                //SERVICIO DESCUENTOS EN CHECKOUT
                $checkout_discounts = Order::getCheckoutDiscounts();

                //ACTUALIZAR CARRITO
                $cart = [];
                if ($os == 'android')
                    if (Input::has('cart'))
                        $cart = $this->update_cart();

                if ($os == 'ios')
                    if (Input::has('cart'))
                       $cart = $this->update_cart();

                //version iOS no valida
                /*if ($os_version == '10.0')
                    $init_message = array(
                        'show' => 1,
                        'title' => 'Versión Beta Detectada',
                        'message' => 'Tienes una versión de iOS que actualmente esta en Beta, la aplicación puede presentar problemas.'
                    );*/

                $this->response = array(
                    'status' => true,
                    'message' => 'Datos guardados y obtenidos.',
                    'result' => array(
                        'init_message' => $init_message,
                        'version' => array(
                            'update' => $update,
                            'message' => $update_message,
                            'os' => $update_os,
                            'api' => '1.1',
                            'url_android' => Config::get('app.android_url'),
                            'url_ios' => Config::get('app.ios_url'),
                            'datetime' => date('Y-m-d H:i:s')
                        ),
                        'config' => array(
                            'admin_email' => Config::get('app.admin_email'),
                            'admin_phone' => '031'.Config::get('app.admin_phone'),
                            'facebook_url' => Config::get('app.facebook_url'),
                            'twitter_url' => Config::get('app.twitter_url'),
                            'referred_share_image_url' => Config::get('app.referred.share_image_url'),
                            'referred_share_title' => Config::get('app.referred.share_title'),
                            'referred_share_description' => Config::get('app.referred.share_description'),
                            'referred_by_amount' => Config::get('app.referred.amount'),
                            'referred_by_free_delivery_days' => Config::get('app.referred.referred_by_free_delivery_days'),
                            'amount_minimum_discount' => Config::get('app.minimum_discount_amount'),
                            'messages' => Config::get('app.app_messages')
                        ),
                        'cities' => $result_cities,
                        'user' => $user,
                        'checkout' => $checkout_discounts,
                        'cart' => $cart
                    )
                );
            }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }else return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtiene version del API para obligar actualizacion
     *
     * @return array $response Respuesta
     */
    public function version()
    {
        $update = 'not_required'; //not_required, required, suggested
        $os = 'none'; //ios, android, all, none
        $message = 'No se requiere actualización.';

        if (Input::has('app_version') && Input::has('os')){
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if (Input::get('os') == 'android'){
                //$app_current_version = intval(str_replace('.', '', Config::get('app.android_current_app_version')));
                if ($app_version < 118){
                    $os = 'android';
                    $update = 'required';
                }
            }
            if (Input::get('os') == 'ios'){
                //$app_current_version = intval(str_replace('.', '', Config::get('app.ios_current_app_version')));
            }
        }else{
            //nada mas pide actualizacion obligatoria en versiones de android debajo de 1.1.5 en ios no por que no estaba implementado
            $update = 'required';
            $os = 'all';
        }

        if ($update == 'required')
            $message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Por favor actualiza a la última versión para continuar. Muchas gracias!';
        if ($update == 'suggested')
            $message = 'Hemos actualizado Merqueo para mejorar tu experiencia de hacer mercado. Te sugerimos actualizar a la última versión. Muchas gracias!';

        $this->response = array(
            'status' => true,
            'message' => $message,
            'result' => array(
                'update' => $update,
                'os' => $os,
                'api' => '1.1',
                'url_android' => Config::get('app.android_url'),
                'url_ios' => Config::get('app.ios_url'),
                'datetime' => date('Y-m-d H:i:s')
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Obtiene datos generales de configuracion
     *
     * @return array $response Respuesta
     */
    public function config()
    {
        $this->response = array(
            'status' => true,
            'message' => 'Datos obtenidos',
            'result' => array(
                'admin_email' => Config::get('app.admin_email'),
                'admin_phone' => Config::get('app.admin_phone'),
                'facebook_url' => Config::get('app.facebook_url'),
                'twitter_url' => Config::get('app.twitter_url'),
                'referrals_limit' => Config::get('app.referrals_limit'),
                'referred_by_amount' => Config::get('app.referred_by_amount'),
                'referrer_amount' => Config::get('app.referrer_amount'),
                'amount_minimum_discount' => Config::get('app.minimum_discount_amount'),
                'messages' => Config::get('app.app_messages'),
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Obtiene ciudades con cobertura
     *
     * @return array $response Respuesta
     */
    public function get_cities()
    {
        $cities = City::select('id', 'city', 'slug')->get();

        $result = array();
        if (count($cities)){
            foreach ($cities as $city)
                $result[] = $city;
        }

        $this->response = array(
            'status' => true,
            'message' => 'Ciudades obtenidas',
            'result' => $result
        );

        return $this->jsonResponse();
    }

    /**
     * actualiza id's de los productos en el carrito
     *
     * @return array $response products
     */
    private function update_cart()
    {
        $new_cart = [];
        foreach (json_decode(Input::get('cart')) as $key => $value)
        {
            $store_product_id = StoreProduct::where('product_id', $key)->where('store_id', 63)->pluck('id');
            if($store_product_id){
                $value->id = $store_product_id;
                $value->old_id = $key;
                $new_cart[] = $value;
            }
        }

        return $new_cart;
    }
}
