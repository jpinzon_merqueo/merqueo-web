<?php

namespace api\v12;

use Input, Route, Request, Session, Config, View, Validator, Cart, CartProduct, User, UserAddress, UserCreditCard,
DateTime, UserCredit, Order, OrderGroup, OrderProduct, Payment, Coupon, UserCoupon, Store, OrderLog, Shopper, DB, UserFreeDelivery,
UserDevice, Survey, Zone, SuggestedProduct, City, Driver, StoreProduct;

class ApiCheckoutController extends \ApiController
{
    const SUBJECT_EMAIL = 'Tu Merqueo';

    public $user_credit;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->response = array('status' => false, 'message' => 'Error generico');

        $this->beforeFilter(function() {
            if (Input::has('user_id'))
                $this->user_id = Input::get('user_id');
            else $this->user_id = 0;
            $this->user_discounts = User::getDiscounts($this->user_id);
        });
    }

    /**
     * Obtiene horario de tiendas con base en carrito
     *
     * @return array $response Datos de horarios
     */
     public function delivery_time()
     {
        $cart = Input::get('cart');
        if (empty($cart))
            return $this->jsonResponse('Datos del carrito es requerido.', 400);

        $store_product_ids = array();
        $quantities = array();

        $products = json_decode($cart);
        if (count($products)){
            foreach ($products as $id => $product) {
                $store_product_ids[] = $id;
                $quantities[intval($id)] = $product->qty;
            }
        }

        $store_ids = array();
        $stores = StoreProduct::select('store_id')->whereIn('id', $store_product_ids)->groupBy('store_id')->get();
        if ($stores){
            foreach($stores as $store)
                $store_ids[] = $store->store_id;
        }
        if (!count($store_ids)) $store_ids[] = 63;

        $validate_slots = true;
        $delivery_stores = $payment_methods = [];
        $config_payment_methods = $this->config['payment_methods'];
        /*if (in_array(62, $store_ids)){
            unset($config_payment_methods['Tarjeta de crédito']);
            $validate_slots = count($store_ids) == 1 ? false : true;
        }*/
        $message_checkout= '';
        //mensaje para descuento global
        $checkout_discounts = Order::getCheckoutDiscounts();
        if ($checkout_discounts['discount_percentage']['status'] && $checkout_discounts['discount_percentage']['order_for_tomorrow']){
            $discount_store_ids = explode(',', $checkout_discounts['discount_percentage']['store_id']);
            reset($store_ids);
            if (in_array(current($store_ids), $discount_store_ids)){
                $store = Store::find(current($store_ids));
                $message_checkout = 'Gánate un '.$checkout_discounts['discount_percentage']['amount'].'% de descuento en tu pedido de '.$store->name.' programando tu entrega para mañana.';
            }
        }

        foreach ($config_payment_methods as $index => $value)
            $payment_methods[] = array('value' => $value);

        $days = $time_week = array();

        $delivery = Store::getDeliverySlotCheckout($store_ids[0], $validate_slots, false, $this->user_discounts);
        foreach($delivery as $store_id => $delivery_store)
        {
            foreach($delivery_store['days'] as $value => $text){
                $days[] = array('value' => $value, 'text' => $text);
            }
            foreach($delivery_store['time'] as $date => $times){
                unset($time);
                foreach($times as $value => $data){
                    $time[] = array('value' => $value, 'text' => $data['text'], 'delivery_amount' => $data['delivery_amount']);
                }
                $day_times[$date] = $time;
            }
            if (isset($days) && isset($day_times)){
                $store = Store::find($store_id);
                $delivery_stores[] = ['store_id' => $store_id, 'title' => count($store_ids) > 1 ? 'Pedido en tienda '.$store->name : '', 'days' => $days, 'time' => $day_times];
                unset($days);
                unset($day_times);
            }
        }

        //calcular total precio especial
        $special_price = 0;
        foreach (StoreProduct::whereIn('id', $store_product_ids)->get() as $product) {
            if ($product->special_price)
                $special_price += ($product->price - $product->special_price) * $quantities[$product->id];
        }

        $delivery_time = array(
            'message' => $message_checkout,
            'payment_methods' => $payment_methods,
            'stores' => $delivery_stores,
            'special_price' => $special_price
        );

        $this->response = array(
            'status' => true,
            'message' => 'Horarios de tienda obtenidos.',
            'result' => $delivery_time
        );

        return $this->jsonResponse();
     }

    /**
     * Procesa nuevo pedido
     * @return array $response Respuesta
     * @throws \Exception
     */
    public function checkout()
    {
        $post_data = Input::all();
        try {
            DB::beginTransaction();

            //si no esta logueado el usuario se crea
            if (Input::has('create_user') && !Input::has('user_id'))
            {
                $email =  Input::get('email');
                $password =  Input::get('password');

                if (empty($email) || empty($password))
                    return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

                //crear usuario
                $inputs = array(
                    'first_name' => Input::get('first_name'),
                    'last_name' => Input::get('last_name'),
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                    'phone' => Input::get('phone'),
                    'do_not_send_mail' => 1
                );
                $request = Request::create('/api/1.2/user', 'POST', $inputs);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if (!$result['status']){
                    $this->response['message'] = $result['message'];
                    return $this->jsonResponse();
                }
                $user = User::find($result['result']['user']['id']);
                $this->user_id = $user->id;
            }else{

                if (!$this->user_id)
                    return $this->jsonResponse('ID usuario es requerido.', 400);

                $user = User::find($this->user_id);
                $post_data['first_name'] = $user->first_name;
                $post_data['last_name'] = $user->last_name;
                $post_data['email'] = $user->email;

                if (empty($user->phone) && isset($post_data['user_phone'])){
                    //verifica que el celular no exista
                    $validator = Validator::make(
                                array( 'phone' => $post_data['user_phone'] ),
                                array( 'phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com'),
                                array( 'phone.required' => 'El número celular es requerido.',
                                       'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                                       'phone.numeric' => 'El número celular debe ser númerico.',
                                       'phone.digits' => 'El número celular debe tener 10 digitos.'
                                )
                    );
                    if ($validator->fails()){
                        $messages = $validator->messages();
                        $this->response['message'] = $messages->first('phone');
                        return $this->jsonResponse();
                    }
                    $user->phone = $post_data['phone'] = $post_data['user_phone'];
                    $user->save();
                    unset($post_data['user_phone']);
                }else $post_data['phone'] = $user->phone;
                if (empty($user->email) && isset($post_data['user_email'])) {
                    //verifica que el email no exista
                    $validator = Validator::make(
                                array( 'email' => $post_data['user_email'], ),
                                array( 'email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com'),
                                array( 'email.required' => 'El email es requerido.',
                                       'email.unique' => 'El email ingresado ya se encuentra en uso.',
                                       'email.email' => 'El formato del email ingresado no es valido.',
                                )
                    );
                    if ($validator->fails()){
                        $messages = $validator->messages();
                        $this->response['message'] = $messages->first('email');
                        return $this->jsonResponse();
                    }
                    $user->email = $post_data['email'] = $post_data['user_email'];
                    $user->save();
                    unset($post_data['user_email']);
                }else $post_data['email'] = $user->email;
            }

            if (empty($user->email)){
                $this->response['message'] = 'Para realizar el pedido debes ingresar un email.';
                return $this->jsonResponse();
            }

            $post_data['user_id'] = $user->id;

            //APARTIR DE ESTE PUNTO YA EL USUARIO ESTA AUTENTICADO

            //verificar si el usuario tiene pedidos entregados
            $user_has_orders = Order::select('orders.id')->where('user_id', $this->user_id)->where('status', '<>', 'Cancelled')->orderBy('id', 'desc')->first();

            //si hay información de dirección se valida para crearla
            if (empty($post_data['address_id']))
            {
                $address = new UserAddress;
                $address->user_id = $this->user_id;
                $address->label = $post_data['address_name'] == 'Otro' && isset($post_data['other_name']) ? $post_data['other_name'] : $post_data['address_name'];
                $address->address = trim($post_data['address']);
                $address->address_further = isset($post_data['address_further']) ? trim($post_data['address_further']) : 'Llamar para confirmar';
                $address->neighborhood = isset($post_data['address_neighborhood']) ? trim($post_data['address_neighborhood']) : '';
                $address->is_south_location = isset($post_data['address_is_south_location']) ? $post_data['address_is_south_location'] : 0;
                $dir = $post_data['dir'];
                $address->address_1 = trim($dir[0]);
                $address->address_2 = trim($dir[2]);
                $address->address_3 = trim($dir[6]);
                $address->address_4 = trim($dir[8]);

                //validar direccion
                if (empty($post_data['lat']) && empty($post_data['lng'])){
                    $inputs = array(
                        'address' => $address->address,
                        'city' => $post_data['city']
                    );
                    $request = Request::create('/api/location', 'GET', $inputs);
                    Request::replace($request->input());
                    $result = json_decode( Route::dispatch($request)->getContent(), true);
                    if ($result['status']){
                        //validar direccion en el sur de la ciudad
                        $this->response = UserAddress::validateAddress($result, $post_data);
                        if ($this->response['status'])
                            return $this->jsonResponse();
                    }else{
                        $this->response = array(
                            'status' => false,
                            'message' => 'No pudimos ubicar tu dirección',
                            'found' => false,
                            'detail' => 'Mueve el mapa hasta la ubicación donde quieres recibir tu pedido.'
                        );
                        return $this->jsonResponse();
                    }

                    $address->latitude = $result['result']['latitude'];
                    $address->longitude = $result['result']['longitude'];
                    $address->city_id = $result['result']['city_id'];
                    $city = City::find($address->city_id);
                }else{
                    $address->latitude = $post_data['lat'];
                    $address->longitude = $post_data['lng'];
                    $post_data['city'] = str_replace(array ('á','é','í','ó','ú'), array ('a','e','i','o','u'), strtolower($post_data['city']));
                    if ($post_data['city'] == 'santafe de bogota')
                        $post_data['city'] = 'bogota';
                    $post_data['city'] = str_replace(' ', '-', $post_data['city']);
                    $city = City::where('slug', $post_data['city'])->first();
                    $address->city_id = $city->id;
                }
                $address->save();
                $address_id = $address->id;
            }else{
                $address_id = $post_data['address_id'];
                if (!$address = UserAddress::find($address_id)){
                    $this->response['message'] = 'La dirección que seleccionaste no existe en tu cuenta, por favor cambiala.';
                    return $this->jsonResponse();
                }
                $post_data['address'] = $address->address;
                $post_data['address_further'] = $address->address_further;
                $city = City::find($address->city_id);
            }

            //obtener carrito
            $cart = Cart::where('user_id', $this->user_id)->orderBy('updated_at', 'desc')->first();
            //si no existe entonces creo un carrito
            if (!$cart) {
                $cart = new Cart;
                $cart->user_id = $this->user_id;
                $cart->save();
            }

            //crear carrito en bd
            if (isset($post_data['cart']))
            {
                $products = json_decode($post_data['cart']);
                if (count($products))
                {
                    CartProduct::where('cart_id', $cart->id)->delete();
                    foreach ($products as $id => $product)
                    {
                        if ($product->qty <= 0) continue;

                        //validar productos en el carrito con ids viejos
                        $store_product = StoreProduct::select('store_products.id')->join('products', 'products.id', '=', 'store_products.product_id')
                                                     ->where('store_products.id', $id)
                                                     ->where('store_id', $city->coverage_store_id)
                                                     ->where('products.name', $product->name)
                                                     ->first();
                        if (!$store_product){
                            $store_product = StoreProduct::select('store_products.id')->join('products', 'products.id', '=', 'store_products.product_id')
                                                     ->where('store_products.product_id', $id)
                                                     ->where('store_id', $city->coverage_store_id)
                                                     ->where('products.name', $product->name)
                                                     ->first();
                             if ($store_product)
                                $id = $store_product->id;
                             else continue;
                        }

                        $cart_product = new CartProduct;
                        $cart_product->cart_id = $cart->id;
                        $cart_product->store_product_id = $id;
                        $cart_product->quantity = $product->qty;
                        $cart_product->quantity_full_price = isset($product->qty_full_price) ? $product->qty_full_price : 0;
                        $cart_product->price = $product->price;
                        $cart_product->added_by = $this->user_id;
                        $cart_product->save();
                    }
                }
            }

            //validar metodo de pago
            $post_data['payment_method'] = $post_data['payment_method'] == 'Tarjeta de Crédito' ? 'Tarjeta de crédito' : $post_data['payment_method'];
            $is_credit_card = $post_data['payment_method'] == 'Tarjeta de crédito' ? true : false;
            if ($is_credit_card)
            {
                $payment = new Payment;

                //si el usuario no esta creado en la api se crea
                $create_user_api = false;
                if (empty($user->customer_token))
                    $create_user_api = true;
                else $post_data['customer_token'] = $user->customer_token;

                if ($create_user_api){
                    $result = $payment->createCustomer($post_data);
                    if (!$result['status']){
                        $this->response['message'] = 'Tus datos personales no pasaron la validación de la tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    $user->customer_token = $post_data['customer_token'] = $result['response']->id;
                    $user->save();
                }

                //si la tarjeta es nueva se guarda y asocia a cliente en api
                $credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
                if (empty($credit_card_id))
                {
                    //validar pais de tarjeta de credito
                    $bin = substr($post_data['number_cc'], 0, 6);
                    $credit_card = $payment->getCreditCardInfo($bin, $post_data);
                    if (!$credit_card){
                        $this->response['message'] = 'Ocurrió un problema al validar el origen de tu tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    if (!$credit_card->is_valid){
                        $this->response['message'] = 'Solo se aceptan tarjetas de crédito nacionales.';
                        return $this->jsonResponse();
                    }

                    $result = $payment->associateCreditCard($post_data);
                    if (!$result['status']){
                        $this->response['message'] = 'Tuvimos un problema con la validación de los datos de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
                        return $this->jsonResponse();
                    }
                    if (!in_array($result['response']->type, $this->config['credit_cards_types'])){
                        $this->response['message'] = 'Solo se aceptan tarjetas de crédito '.$this->config['credit_cards_message'];
                        return $this->jsonResponse();
                    }

                    $post_data['card_token'] = $result['response']->id;
                    //guardar tarjeta en bd
                    $user_credit_card = new UserCreditCard;
                    $user_credit_card->user_id = $user->id;
                    $user_credit_card->card_token = $post_data['card_token'];
                    $user_credit_card->holder_name = $post_data['name_cc'];
                    $user_credit_card->last_four = $result['response']->lastFour;
                    $user_credit_card->type = $result['response']->type;
                    $user_credit_card->document_number = isset($post_data['document_number_cc']) ? $post_data['document_number_cc'] : null;
                    $user_credit_card->country = $credit_card->country_name;
                    $user_credit_card->created_at = $user_credit_card->updated_at = date('Y-m-d H:i:s');
                    $user_credit_card->save();
                }else{
                    $user_credit_card = UserCreditCard::find($credit_card_id);
                    if (!$user_credit_card){
                        $this->response['message'] = 'Ocurrió un error al obtener información de la tarjeta de crédito.';
                        return $this->jsonResponse();
                    }
                    $post_data['card_token'] = $user_credit_card->card_token;
                    if (!empty($post_data['add_document_number_cc'])){
                        $user_credit_card->document_number = $post_data['add_document_number_cc'];
                        $user_credit_card->save();
                    }
                }
            }

            $cart_products = DB::table('cart_products')
                                ->where('cart_id', $cart->id)
                                ->leftJoin('store_products', 'cart_products.store_product_id', '=', 'store_products.id')
                                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                ->leftJoin('shelves', 'shelf_id', '=', 'shelves.id')
                                ->leftJoin('users', 'cart_products.added_by', '=', 'users.id')
                                ->select('products.*', 'store_products.*', 'cart_products.quantity AS cart_quantity',
                                'cart_products.quantity_full_price AS cart_quantity_full_price',
                                'cart_products.store_id AS cart_store_id', 'cart_products.store_product_id', 'users.first_name AS added_by', 'cart_products.price AS cart_price')
                                ->orderBy('store_id')
                                ->get();

            $response['cart']['delivery_amount'] = 0;
            $response['cart']['total_amount'] = 0;
            $response['cart']['discount_amount'] = 0;
            $response['cart']['total_quantity'] = 0;
            $response['cart']['stores'] = array();

            $store_id = 0;
            if ($cart_products)
            {
                $products = json_decode($post_data['cart'], true);
                $response['cart']['is_minimum_reached'] = 1;

                foreach ($cart_products as $cart_product)
                {
                    //validar productos en carrito de tienda vieja
                    if ($cart_product->cart_store_id == 54){
                        $this->response['message'] = 'Por favor elimina los productos de la tienda Euro Supermercado del carrito de compras para continuar.';
                        return $this->jsonResponse();
                    }

                    //validar estado
                    if (!$cart_product->status){
                        $this->response['message'] = 'El producto '.$cart_product->name.' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        return $this->jsonResponse();
                    }

                    if (!$cart_product->id){
                        if (isset($products[$cart_product->product_id]) && isset($products[$cart_product->product_id]['name']) && !empty($products[$cart_product->product_id]['name']))
                            $this->response['message'] = 'El producto '.$products[$cart_product->product_id]['name'].' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        else $this->response['message'] = 'El producto con cantidad '.$cart_product->cart_quantity.' ya no esta disponible, por favor eliminalo del carrito de compras para continuar.';
                        return $this->jsonResponse();
                    }

                    //validar producto en promocion en pedidos anteriores
                    if ($cart_product->promo_type_special_price == 'Merqueo' && $cart_product->special_price > -1 && $cart_product->quantity_special_price && $cart_product->cart_quantity) {
                        $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                                        ->where('store_product_id', $cart_product->id)
                                        ->where('price', $cart_product->special_price)
                                        ->where('user_id', $this->user_id)
                                        ->where('status', '<>', 'Cancelled')
                                        ->where(DB::raw("DATEDIFF('".date('Y-m-d')."', date)"), '<=', Config::get('app.minimum_promo_days'))
                                        ->groupBy('order_products.store_product_id')
                                        ->count();
                        if ($count_products){
                            $this->response['message'] = 'Ya realizaste un pedido con el producto '.$cart_product->name.' en promoción con precio $'.number_format($cart_product->special_price, 0, ',', '.').', por favor eliminalo del carrito de compras para continuar.';
                            return $this->jsonResponse();
                        }
                    }

                    //validar precio especial
                    /*$original_price = $cart_product->special_price ? $cart_product->special_price : $cart_product->price;
                    if ($original_price > $cart_product->cart_price){
                        $this->response['message'] = 'El precio del producto '.$cart_product->name.' ha cambiado, por favor eliminalo del carrito y vuelve a agregarlo.';
                        return $this->jsonResponse();
                    }*/

                    $store_product = json_decode(json_encode($cart_product), true);

                    //validar precio especial y cantidad del producto
                    if ($store_product['special_price'] > -1 && $store_product['quantity_special_price'] && $store_product['cart_quantity'] > $store_product['quantity_special_price'] && $store_product['special_price'] == $store_product['cart_price']) {
                        if (($store_product['first_order_special_price'] && !$user_has_orders) || ($store_product['cart_quantity'] > $store_product['quantity_special_price'])){
                            $this->response['message'] = 'El producto '.$store_product['name'].' por estar en promoción puedes agregar máximo '.$store_product['quantity_special_price'].' unidad(es) en tu pedido, por favor disminuye la cantidad en el carrito para continuar.';
                            return $this->jsonResponse();
                        }
                    }

                    $store_id = $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id]))
                    {
                        $store = Store::find($store_id)->toArray();
                        $store['count'] = 1;
                        $store['sub_total'] = 0;
                        $response['cart']['stores'][$store_id] = $store;
                        $response['cart']['stores'][$store_id]['products'] = array();
                        $response['cart']['stores'][$store_id]['delivery_amount'] = $store['delivery_order_amount'];
                        $response['cart']['stores'][$store_id]['delivery_discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['sub_total'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_time_minutes'] = $store['delivery_time_minutes'];
                    }else $response['cart']['stores'][$store_id]['count'] += 1;

                    if (!isset($store_product['cart_quantity']))
                        $store_product['cart_quantity'] = 1;

                    $store_product['sub_total'] = $store_product['cart_quantity'] * $store_product['cart_price'];
                    if ($store_product['cart_quantity_full_price'])
                        $store_product['sub_total'] += $store_product['cart_quantity_full_price'] * $store_product['price'];
                    $response['cart']['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
                    $response['cart']['total_amount'] += $store_product['sub_total'];
                    $response['cart']['total_quantity']++;

                    if ($store_product['delivery_discount_amount'])
                        $response['cart']['stores'][$store_id]['delivery_discount_amount'] += $store_product['delivery_discount_amount'];

                    array_push($response['cart']['stores'][$store_id]['products'], $store_product);
                }

                //bono para usuarios nuevos, pagos con mastercard y pedido mayor a 60000
                /*if (!$user_has_orders && $is_credit_card && $user_credit_card->type == 'MASTERCARD' && $response['cart']['total_amount'] > 60000){
                    $response['cart']['discount_amount'] = 20000;
                    foreach($response['cart']['stores'] as $store_id => $store){
                        $response['cart']['stores'][$store_id]['discount_amount'] = 20000;
                    }
                }*/
            }else{
                $this->response['message'] = 'Los productos en tu carrito cambiaron debido a una actualización, por favor eliminalos y vuelve a agregarlos al carrito de compras.';
                return $this->jsonResponse();
            }

            $subtotal_cart = $response['cart']['total_amount'];

            //aplicar descuento a la compra
            if (isset($this->user_discounts['coupon']))
            {
                $coupon = $this->user_discounts['coupon'];
                $data = array('coupon_code' => $coupon->code, 'cart' => $post_data['cart'], 'validate' => 1);
                if (isset($post_data['create_user']))
                    $data['is_new_user'] = true;
                else if (isset($post_data['user_id']))
                    $data['user_id'] = $post_data['user_id'];
                $request = Request::create('/api/1.2/coupon', 'POST', $data);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if (!$result['status']){
                    $this->response['message'] = $result['message'];
                    return $this->jsonResponse();
                }
                //si el cupon cumple con totales requerido
                if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $subtotal_cart < $coupon->minimum_order_amount){
                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el total del pedido debe ser mayor o igual a $'.number_format($coupon->minimum_order_amount, 0, ',', '.');
                    return $this->jsonResponse();
                }else if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $subtotal_cart > $coupon->maximum_order_amount){
                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el total del pedido debe ser menor o igual a $'.number_format($coupon->maximum_order_amount, 0, ',', '.');
                    return $this->jsonResponse();
                }else if ($coupon->minimum_order_amount && $coupon->maximum_order_amount && ($subtotal_cart < $coupon->minimum_order_amount || $subtotal_cart > $coupon->maximum_order_amount)){
                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->coupon.' el total del pedido debe estar entre $'.number_format($coupon->minimum_order_amount, 0, ',', '.').' y $'.number_format($coupon->maximum_order_amount, 0, ',', '.');
                    return $this->jsonResponse();
                }

                $discount_amount = 0;
                //validar cupon para tienda / categoria / subcategoria / producto especifico
                if ($coupon->redeem_on == 'Specific Store')
                {
                    foreach($cart_products as $store_product){
                        $is_valid = false;
                        if ($store_product->store_id == $coupon->store_id){
                            if ($coupon->department_id){
                                if ($store_product->department_id == $coupon->department_id){
                                    if ($coupon->shelf_id){
                                        if ($store_product->shelf_id == $coupon->shelf_id){
                                            if ($coupon->store_product_id){
                                                if ($store_product->id == $coupon->store_product_id)
                                                    $is_valid = true;
                                            }else $is_valid = true;
                                        }
                                    }else $is_valid = true;
                                }
                            }else $is_valid = true;
                        }
                        if ($is_valid){
                            if ($coupon->type == 'Discount Percentage')
                                $discount_amount += round(($store_product->price * $store_product->cart_quantity) * ($coupon->amount / 100), 0);
                            if ($coupon->type == 'Credit Amount')
                                $discount_amount += round($store_product->price * $store_product->cart_quantity, 0);
                        }
                    }
                    $store = Store::select('stores.name AS store_name', 'departments.name AS department_name', 'shelves.name AS shelf_name',
                                    DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"), 'cities.city')
                                  ->leftJoin('departments', function($join) use ($coupon){
                                      $join->on('departments.store_id', '=', 'stores.id');
                                      $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                                  })
                                  ->leftJoin('shelves', function($join) use ($coupon){
                                      $join->on('shelves.department_id', '=', 'departments.id');
                                      $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                                  })
                                  ->leftJoin('store_products', function ($join) use ($coupon) {
                                      $join->on('store_products.shelf_id', '=', 'shelves.id');
                                      $join->on('store_products.id', '=', DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                                  })
                                  ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                  ->join('cities', 'cities.id', '=', 'stores.city_id')
                                  ->where('stores.id', $coupon->store_id)
                                  ->first();
                    $discount_message = $store->store_name;
                    if (!empty($store->department_name))
                        $discount_message .= ' / '.$store->department_name;
                    if (!empty($store->shelf_name))
                        $discount_message .= ' / '.$store->shelf_name;
                    if (!empty($store->product_name))
                        $discount_message .= ' / '.$store->product_name;
                    $discount_message .= ' en '.$store->city;

                    if ($discount_amount){
                        if ($coupon->type == 'Discount Percentage')
                            $response['cart']['stores'][$coupon->store_id]['discount_percentage_amount'] = $coupon->amount;
                        else if ($discount_amount > $coupon->amount)
                                $discount_amount = $coupon->amount;
                        $response['cart']['stores'][$coupon->store_id]['discount_amount'] = $discount_amount;
                        $response['cart']['discount_amount'] += $discount_amount;
                    }else{
                        $this->response['message'] = 'El cupón de descuento '.$coupon->code.' aplica exclusivamente para productos de la tienda '.$discount_message;
                        return $this->jsonResponse();
                    }

                    $response['cart']['stores'][$coupon->store_id]['discount_message'] = $discount_message;
                }
                //validar metodo de pago y tipo de tarjeta
                if ($coupon->payment_method){
                    if (($is_credit_card && $coupon->payment_method != 'Tarjeta de crédito' && $coupon->payment_method != $user_credit_card->type)
                    || (!$is_credit_card && $coupon->payment_method != $post_data['payment_method'])){
                        $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el metodo de pago debe ser con '.$this->config['payment_methods_names'][$coupon->payment_method].trim(' '.$coupon->cc_bank);
                        return $this->jsonResponse();
                    }else{
                        if ($is_credit_card && !empty($coupon->cc_bin)){
                            $result = $payment->retriveCreditCard($user->customer_token, $user_credit_card->card_token, $user->id);
                            if ($result['status']){
                                $bin = $result['response']->bin;
                                $allowed_bines = explode(',', $coupon->cc_bin);
                                if (!in_array($bin, $allowed_bines)){
                                    $this->response['message'] = 'Para redimir el cupón de descuento '.$coupon->code.' el metodo de pago debe ser con '.$this->config['payment_methods_names'][$coupon->payment_method].' '.$coupon->cc_bank;
                                    return $this->jsonResponse();
                                }
                            }else{
                                $this->response['message'] = 'No pudimos obtener información de tu tarjeta de crédito para la validación del cupón, por favor verificala e intenta de nuevo.';
                                return $this->jsonResponse();
                            }
                         }
                    }
                }
                //si el cupon no es de productos especificos se aplica el cupon
                if ($coupon->redeem_on != 'Specific Store'){
                    if ($coupon->type == 'Credit Amount'){
                        if ($coupon->amount > $response['cart']['total_amount'])
                            $coupon->amount = $response['cart']['total_amount'];
                        $discount_credit_amount = $coupon->amount;
                    }
                    if ($coupon->type == 'Discount Percentage')
                        $discount_percentage = $coupon->amount;
                }
            }
            //si no tiene tiene cupon activo y si el total es mayor al requerido se aplica descuento con credito
            if (!isset($this->user_discounts['coupon']) && $this->user_discounts['amount'] && $subtotal_cart > $this->user_discounts['minimum_discount_amount'])
            {
                if ($this->user_discounts['amount'] > $response['cart']['total_amount'])
                    $this->user_discounts['amount'] = $response['cart']['total_amount'];
                $discount_credit_amount = $this->user_discounts['amount'];
            }

            //validar y cargar codigo de referido
            $post_data['coupon_code'] = isset($post_data['coupon_code']) ? $post_data['coupon_code'] : null;

            $referred = User::validateReferred($user, $post_data['coupon_code']);
            if ($referred['success'])
                $this->user_discounts = User::getDiscounts($user->id, $post_data['coupon_code']);

            $delivery_time_minutes = 0;

            //domicilio a mitad de precio si hay mas de una tienda
            ksort($response['cart']['stores']);
            if (count($response['cart']['stores']) > 1)
                $first_store_id = current(array_keys($response['cart']['stores']));

            //obtener descuentos activos en checkout
            $checkout_discounts = Order::getCheckoutDiscounts();

            foreach ($response['cart']['stores'] as $store_id => $store)
            {
                //valida que todas las tiendas tengan cobertura en la dirección
                $delivery_data = array('day' => $post_data['delivery_day_'.$store_id], 'time' => $post_data['delivery_time_'.$store_id]);
                $result = Store::validateStoreDeliversToAddress($store_id, $address_id, false, false, $delivery_data);
                if (!$result['coverage']) {
                    if ($result['type'] == 'no_coverage')
                        $this->response = array(
                            'status' => false,
                            'message' => $result['message'],
                            'found' => true,
                            'detail' => 'La dirección que ingresaste se encuentra fuera de nuestra cobertura.'
                        );
                    else $this->response = array(
                                'status' => false,
                                'message' => $result['message']
                         );
                    return $this->jsonResponse();
                }

                //domicilio gratis en proximo pedido
                if ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order'])
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;

                //validar domicilio gratis global
                if ($checkout_discounts['free_delivery']['status']){
                    if (!empty($checkout_discounts['free_delivery']['store_ids'])){
                        $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                        $store_ids = count($store_ids) > 1 ? $store_ids : array($store_id);
                        if (in_array($store_id, $store_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount'])
                            $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                    }else{
                        if (!empty($checkout_discounts['free_delivery']['city_ids'])){
                            $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                            $city_ids = count($city_ids) > 1 ? $city_ids : array($checkout_discounts['free_delivery']['city_ids']);
                            if (in_array($response['cart']['stores'][$store_id]['city_id'], $city_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount'])
                                $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                        }
                    }
                }

                //validar domicilio por horario de entrega
                if ($response['cart']['stores'][$store_id]['delivery_amount'] && $store['delivery_order_increase_amount']){
                    $delivery_time = explode(' - ', $post_data['delivery_time_'.$store_id]);
                    $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_time[1] == '22:00:00' ? $store['delivery_order_amount'] + $store['delivery_order_increase_amount'] : $store['delivery_order_amount'];
                }

                //validar descuento en domicilio por productos
                if ($response['cart']['stores'][$store_id]['delivery_amount'] && $response['cart']['stores'][$store_id]['delivery_discount_amount']){
                    $delivery_amount = $response['cart']['stores'][$store_id]['delivery_amount'] >= $response['cart']['stores'][$store_id]['delivery_discount_amount'] ? $response['cart']['stores'][$store_id]['delivery_amount'] - $response['cart']['stores'][$store_id]['delivery_discount_amount'] : 0;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_amount;
                }

                $response['cart']['delivery_amount'] += $response['cart']['stores'][$store_id]['delivery_amount'];

                //sino hay cupon activo
                if (!isset($this->user_discounts['coupon']))
                {
                    //validar descuento de credito global
                    if ($checkout_discounts['discount_credit']['status']){
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_credit']['minimum_order_amount'] && !$checkout_discounts['discount_credit']['maximum_order_amount'] && $subtotal_cart < $checkout_discounts['discount_credit']['minimum_order_amount'])
                            $is_valid = false;
                        else if ($checkout_discounts['discount_credit']['maximum_order_amount'] && !$checkout_discounts['discount_credit']['minimum_order_amount'] && $subtotal_cart > $checkout_discounts['discount_credit']['maximum_order_amount'])
                            $is_valid = false;
                        else if ($checkout_discounts['discount_credit']['minimum_order_amount'] && $checkout_discounts['discount_credit']['maximum_order_amount'] &&
                                ($subtotal_cart < $checkout_discounts['discount_credit']['minimum_order_amount'] || $subtotal_cart > $checkout_discounts['discount_credit']['maximum_order_amount']))
                            $is_valid = false;

                        if ($is_valid){
                            $discount_global_credit_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_credit']['store_id']){
                                foreach($store['products'] as $store_product)
                                {
                                    $store_product = (object)$store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_credit']['store_id']){
                                        if ($checkout_discounts['discount_credit']['department_id']){
                                            if ($store_product->department_id == $checkout_discounts['discount_credit']['department_id']){
                                                if ($checkout_discounts['discount_credit']['shelve_id']){
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_credit']['shelve_id']){
                                                        if ($checkout_discounts['discount_credit']['product_id']){
                                                            if ($store_product->id == $checkout_discounts['discount_credit']['store_product_id'])
                                                                $is_valid = true;
                                                        }else $is_valid = true;
                                                    }
                                                }else $is_valid = true;
                                            }
                                        }else $is_valid = true;
                                    }
                                    if ($is_valid)
                                        $discount_global_credit_amount += round($product_price * $store_product->cart_quantity, 0);
                                }
                            }else $discount_global_credit_amount += $checkout_discounts['discount_credit']['amount'];

                            if ($discount_global_credit_amount){
                                if ($discount_global_credit_amount > $checkout_discounts['discount_credit']['amount'])
                                    $discount_global_credit_amount = $checkout_discounts['discount_credit']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_credit_amount;
                                $response['cart']['discount_amount'] += $discount_global_credit_amount;
                            }
                        }
                    }

                    //validar descuento por porcentaje global
                    if ($checkout_discounts['discount_percentage']['status']){
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_percentage']['minimum_order_amount'] && !$checkout_discounts['discount_percentage']['maximum_order_amount'] && $subtotal_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'])
                            $is_valid = false;
                        elseif ($checkout_discounts['discount_percentage']['maximum_order_amount'] && !$checkout_discounts['discount_percentage']['minimum_order_amount'] && $subtotal_cart > $checkout_discounts['discount_percentage']['maximum_order_amount'])
                            $is_valid = false;
                        elseif ($checkout_discounts['discount_percentage']['minimum_order_amount'] && $checkout_discounts['discount_percentage']['maximum_order_amount'] &&
                                ($subtotal_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'] || $subtotal_cart > $checkout_discounts['discount_percentage']['maximum_order_amount']))
                            $is_valid = false;

                        if ($is_valid){
                            $discount_global_percentage_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_percentage']['store_id']){
                                foreach($store['products'] as $store_product)
                                {
                                    $store_product = (object)$store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_percentage']['store_id']){
                                        if ($checkout_discounts['discount_percentage']['department_id']){
                                            if ($store_product->department_id == $checkout_discounts['discount_percentage']['department_id']){
                                                if ($checkout_discounts['discount_percentage']['shelve_id']){
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_percentage']['shelve_id']){
                                                        if ($checkout_discounts['discount_percentage']['product_id']){
                                                            if ($store_product->id == $checkout_discounts['discount_percentage']['store_product_id'])
                                                                $is_valid = true;
                                                        }else $is_valid = true;
                                                    }
                                                }else $is_valid = true;
                                            }
                                        }else $is_valid = true;
                                    }
                                    if ($is_valid)
                                        $discount_global_percentage_amount += round(($product_price * $store_product->cart_quantity) * ($checkout_discounts['discount_percentage']['amount'] / 100), 0);
                                }
                            }else $discount_global_percentage_amount += round($response['cart']['total_amount'] * ($checkout_discounts['discount_percentage']['amount'] / 100), 0);

                            if ($checkout_discounts['discount_percentage']['order_for_tomorrow']){
                                $delivery_day = $post_data['delivery_day_'.$store_id];
                                if (!$delivery_day)
                                    $discount_global_percentage_amount = 0;
                                elseif ($delivery_day < date('Y-m-d', strtotime('+1 day')))
                                    $discount_global_percentage_amount = 0;
                            }

                            if ($discount_global_percentage_amount){
                                $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $checkout_discounts['discount_percentage']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_percentage_amount;
                                $response['cart']['discount_amount'] += $discount_global_percentage_amount;
                            }
                        }
                    }
                }

                if ($store['sub_total'] < $store['minimum_order_amount'])
                    $response['cart']['is_minimum_reached'] = 0;

                //credito
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                && isset($discount_credit_amount) && $discount_credit_amount){
                    $total_order_store = $response['cart']['stores'][$store_id]['sub_total'] + $response['cart']['stores'][$store_id]['delivery_amount'];
                    $discount_amount = $discount_credit_amount > $total_order_store ? $total_order_store : $discount_credit_amount;
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['discount_amount'] += $discount_amount;
                    $discount_credit_amount -= $discount_amount;
                }
                //porcentaje
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                && isset($discount_percentage) && $discount_percentage){
                    $discount_amount = round(($response['cart']['stores'][$store_id]['sub_total'] * $discount_percentage) / 100, 2);
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $discount_percentage;
                    $response['cart']['discount_amount'] += $discount_amount;
                }
                //tiempo de entrega inmediata
                if ($response['cart']['stores'][$store_id]['delivery_time_minutes'] > $delivery_time_minutes)
                    $delivery_time_minutes = $response['cart']['stores'][$store_id]['delivery_time_minutes'];

                //domicilio gratis y descuento del 10% para usuarios que pagan con visa por primera vez (maximo 50000 de descuento)
                if (!isset($this->user_discounts['coupon']) && $is_credit_card && $user_credit_card->type == 'VISA'){
                    //verificar si el usuario tiene pedidos pagados con visa
                    $valid_discount = false;
                    $has_order_visa = Order::select('id')->where('user_id', $this->user_id)->where('status', '<>', 'Cancelled')->where('cc_type', 'VISA')->first();
                    if ($has_order_visa){
                        //validar que no existan pedidos con la misma tarjeta
                        $has_order_visa = Order::select('orders.id')->where('cc_holder_document_number', $user_credit_card->holder_document_number)->where('cc_last_four', $user_credit_card->last_four)->where('cc_type', 'VISA')->where('status', '<>', 'Cancelled')->first();
                        if (!$has_order_visa)
                            $valid_discount = true;
                    }else $valid_discount = true;

                    if ($valid_discount){
                        $discount_percentage = 10;
                        $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $discount_percentage;
                        if ($response['cart']['stores'][$store_id]['sub_total'] > 333400)
                            $discount_amount = 50000;
                        else $discount_amount = round(($response['cart']['stores'][$store_id]['sub_total'] * $discount_percentage) / 100, 2);
                        $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                        $response['cart']['discount_amount'] = $discount_amount;

                        $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                        $response['cart']['delivery_amount'] = 0;
                    }
                }

                //bono para usuarios nuevos, pagos con mastercard y pedido mayor a 60000
                /*if (!$user_has_orders && $is_credit_card && $user_credit_card->type == 'MASTERCARD' && $response['cart']['total_amount'] > 60000){
                    $response['cart']['discount_amount'] = 20000;
                    foreach($response['cart']['stores'] as $store_id => $store){
                        $response['cart']['stores'][$store_id]['discount_amount'] = 20000;
                    }
                }*/
            }

            if (isset($response['cart']['is_minimum_reached']) && $response['cart']['is_minimum_reached'] == 1)
            {
                //cupon utilizado
                if (isset($coupon)){
                    //cargar cupon como utilizado a usuario
                    $user_coupon = new UserCoupon;
                    $user_coupon->coupon_id = $coupon->id;
                    $user_coupon->user_id = $this->user_id;
                    $user_coupon->save();
                    //sumar credito de cupon a usuario
                    $user_credit = new UserCredit;
                    $user_credit->user_id = $user->id;
                    $user_credit->coupon_id = $coupon->id;
                    $user_credit->amount = $response['cart']['discount_amount'];
                    $user_credit->type = 1;
                    $user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por redimir cupón código '.$coupon->code;
                    $user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
                    $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                    $user_credit->save();

                    if (!$user->first_coupon_used && $coupon->type_use == 'Only The First Order'){
                        $user->first_coupon_used = 1;
                        $user->save();
                    }
                }

                //obtener zona
                $zone_id = null;
                $zones = Zone::where('city_id', $this->getCoverageStoreCityId($address->city_id))->where('status', 1)->get();
                if ($zones) {
                    $cart_store_ids = array_keys($response['cart']['stores']);
                    foreach ($zones as $zone) {
                        if(!empty($zone->stores)){
                            $zone_store_ids = explode(',', $zone->stores);
                            foreach ($cart_store_ids as $cart_store_id) {
                                if(in_array($cart_store_id, $zone_store_ids) && point_in_polygon($zone->delivery_zone, $address->latitude, $address->longitude))
                                    $zone_id = $zone->id;
                                    break;
                            }
                        }
                    }
                }

                //crea un nuevo grupo para este pedido
                $order_group = new OrderGroup;

                //realizar cargo a tarjeta
                /*if ($is_credit_card)
                {
                    $post_data_validation = $post_data;
                    $post_data_validation['total_amount'] = 1000;
                    $post_data_validation['tax_amount'] = 0;
                    $post_data_validation['installments_cc'] = 1;
                    $post_data_validation['order_id'] = 1;
                    $post_data_validation['description'] = 'Validación de tarjeta de crédito';
                    $post_data_validation['nit'] = 'No especificado';
                    $result = $payment->createCreditCardCharge($post_data_validation);

                    if (!$result['status']){
                        $this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
                        return $this->jsonResponse();
                    }

                    $result = $payment->refundCreditCardCharge($user->id, $result['response']->id);
                    if (!$result['status']){
                        $this->response['message'] = 'Tuvimos un problema con la validación de tu tarjeta de crédito, por favor verificalos y vuelve a intentar.';
                        return $this->jsonResponse();
                    }
                }*/

                //asociar dispositivo a usuario
                if ($user_device = UserDevice::find($this->user_device_id)){
                    $user_device->user_id = $this->user_id;
                    $user_device->save();
                }

                $order_group->source = 'Device';
                if (isset($post_data['source_os']))
                    $order_group->source_os = $post_data['source_os'];
                $order_group->user_device_id = $this->user_device_id;
                $order_group->user_id = $this->user_id;
                $order_group->user_firstname = $user->first_name;
                $order_group->user_lastname = $user->last_name;
                $order_group->user_email = $user->email;
                $order_group->user_address = $address->address;
                $order_group->user_address_further = $address->address_further;
                $order_group->user_address_neighborhood = $address->neighborhood;
                $order_group->user_address_latitude = $address->latitude;
                $order_group->user_address_longitude = $address->longitude;
                $order_group->user_city_id = $address->city_id;
                $order_group->user_phone = $user->phone;
                $order_group->address_id = $address_id;
                $order_group->zone_id = $zone_id;
                $order_group->discount_amount = $response['cart']['discount_amount'];
                $order_group->delivery_amount = $response['cart']['delivery_amount'];
                $order_group->total_amount = $response['cart']['total_amount'];
                $order_group->products_quantity = $response['cart']['total_quantity'];
                $order_group->user_comments = isset($post_data['comments']) ? trim($post_data['comments']) : '';
                $order_group->app_version = isset($post_data['app_version']) ? trim($post_data['app_version']) : '';
                $order_group->ip = get_ip();
                $order_group->save();

                //inicializar variables para validar campañas de marca
                //$is_valid_store = false;
                //$allowed_stores = [36, 10];

                $order_group_total_amount = 0;

                //crea un pedido por cada tienda
                foreach ($response['cart']['stores'] as $store_id => $store)
                {
                    $order = new Order;
                    $order->type = 'Merqueo';
                    $order->reference = generate_reference();
                    $order->user_id = $this->user_id;
                    $order->store_id = $store_id;
                    $order->date = date("Y-m-d H:i:s");
                    $order->group_id = $order_group->id;
                    $order->total_products = count($store['products']);
                    $order->delivery_amount = $store['delivery_amount'];
                    $order->discount_amount = $store['discount_amount'];
                    $order->total_amount = $store['sub_total'];

                    $delivery_time = explode(' - ', $post_data['delivery_time_'.$store_id]);
                    $delivery_time_begin = explode(':', $delivery_time[0]);
                    $hour_begin = new DateTime();
                    $hour_begin->setTime($delivery_time_begin[0], $delivery_time_begin[1]);
                    $delivery_time_end = explode(':', $delivery_time[1]);
                    $hour_end = new DateTime();
                    $hour_end->setTime($delivery_time_end[0], $delivery_time_end[1]);
                    $delivery_time_text = $hour_begin->format('g:i a').' - '.$hour_end->format('g:i a');
                    $order->first_delivery_date = $post_data['delivery_day_'.$store_id].' '.$delivery_time[1];
                    $order->real_delivery_date = $order->delivery_date = $post_data['delivery_day_'.$store_id].' '.$delivery_time[1];
                    $order->delivery_time = $delivery_time_text;

                    $order->user_score_token = generate_token();
                    $order->payment_method = $post_data['payment_method'];

                    if (isset($store['discount_percentage_amount']))
                        $order->discount_percentage_amount = $store['discount_percentage_amount'];

                    if ($is_credit_card){
                        //guardar datos de tarjeta
                        $order->credit_card_id = $user_credit_card->id;
                        $order->cc_token = $user_credit_card->card_token;
                        $order->cc_holder_name = $user_credit_card->holder_name;
                        $order->cc_holder_document_number = $user_credit_card->document_number;
                        $order->cc_last_four = $user_credit_card->last_four;
                        $order->cc_type = $user_credit_card->type;
                        $order->cc_country = $user_credit_card->country;
                        $order->cc_installments = $post_data['installments_cc'] ? $post_data['installments_cc'] : 1;
                    }

                    //validar datos de fraude para estado de pedido
                    $result = $order->validateOrderFraud($post_data);
                    $order->status = $result['validation'] ? 'Validation' : 'Initiated';
                    $order->posible_fraud = empty($result['posible_fraud']) ? 0 : $result['posible_fraud'];

                    $order->save();

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'Pedido creado.';
                    $log->user_id = $this->user_id;
                    $log->order_id = $order->id;
                    $log->save();

                    $order_total_amount = 0;

                    foreach ($cart_products as $cart_product)
                    {
                        $store_product = StoreProduct::select('products.*', 'store_products.*')->join('products', 'store_products.product_id', '=', 'products.id')
                                                     ->where('store_products.id', $cart_product->id)->first();
                        //no deja que al pedido le aparezcan productos que no pertenecen a la tienda
                        if ($store_product->store_id != $store_id)
                            continue;

                        //INSERTAR PRODUCTOS
                        if ($cart_product->cart_quantity)
                        {
                            //obtener precio de producto (full, especial / primera compra)
                            if ($store_product->special_price > -1) {
                                if ($store_product->first_order_special_price)
                                    if (!$user_has_orders)
                                        $price = $store_product->special_price;
                                    else $price = $store_product->price;
                                else $price = $store_product->special_price;
                            } else $price = $store_product->price;

                            $order_product = OrderProduct::saveProduct($price, $cart_product->cart_quantity, $store_product, $order);
                            $order_total_amount += $order_product->price * $order_product->quantity;

                            $products_mail[$order->id][] = array(
                                'img' => $order_product->product_image_url,
                                'name' => $order_product->product_name,
                                'quantity' => $order_product->product_quantity,
                                'unit' => $order_product->product_unit,
                                'qty' => $order_product->quantity,
                                'price' => $order_product->price,
                                'total' => $order_product->quantity * $order_product->price,
                                'type' => $order_product->type
                            );
                        }

                        //INSERTAR PRODUCTOS ADICIONALES CON PRECIO FULL
                        if ($cart_product->cart_quantity_full_price)
                        {
                            $order_product = OrderProduct::saveProduct($store_product->price, $cart_product->cart_quantity_full_price, $store_product, $order);
                            $order_total_amount += $order_product->price * $order_product->quantity;

                            $products_mail[$order->id][] = array(
                                'img' => $order_product->product_image_url,
                                'name' => $order_product->product_name,
                                'quantity' => $order_product->product_quantity,
                                'unit' => $order_product->product_unit,
                                'qty' => $order_product->quantity,
                                'price' => $order_product->price,
                                'total' => $order_product->quantity * $order_product->price,
                                'type' => $order_product->type
                            );
                        }

                        /*if (!isset($product_found))
                            if (strpos(strtolower($product->name), 'chivas'))
                                $product_found = true;*/
                    }

                    if($user->free_delivery_next_order){
                        $user->free_delivery_next_order = 0;
                        $user->save();
                    }

                    if ($order->total_amount != $order_total_amount){
                        $order->total_amount = $order_total_amount;
                        $order->save();
                    }
                    $order_group_total_amount += $order_total_amount;

                    //verificar condiciones de campaña de marca
                    /*if (!$is_valid_store){
                        if (in_array($store_id, $allowed_stores) && !isset($product_found)){
                            $is_valid_store = true;
                            if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'lavomatic')->first()) {
                                $user_brand_campaign = new UserBrandCampaign;
                                $user_brand_campaign->user_id = $this->user_id;
                                $user_brand_campaign->order_id = $order->id;
                                $user_brand_campaign->campaign = 'lavomatic';
                                $user_brand_campaign->save();
                            }
                        }
                    }
                    //verificar condiciones de campaña de marca
                    if ($address->city_id == 1 && isset($product_found) && $product_found) {
                        if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'mini chivas')->first()) {
                            $user_brand_campaign = new UserBrandCampaign;
                            $user_brand_campaign->user_id = $this->user_id;
                            $user_brand_campaign->order_id = $order->id;
                            $user_brand_campaign->campaign = 'mini chivas';
                            $user_brand_campaign->save();
                        }
                    }*/

                    //descontar credito utilizado a usuario
                    if ($store['discount_amount'] && (isset($coupon) || $this->user_discounts['amount'])){
                        $user_credit = new UserCredit;
                        $user_credit->user_id = $user->id;
                        $user_credit->order_id = $order->id;
                        if (isset($coupon))
                            $user_credit->coupon_id = $coupon->id;
                        $user_credit->amount = $store['discount_amount'];
                        $user_credit->type = 0;
                        $user_credit->description = 'Deducción de $'.number_format($user_credit->amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
                        $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                        $user_credit->save();
                    }

                    //registrar log de domicilio gratis
                    if (!$order->delivery_amount && $user->free_delivery_expiration_date > date('Y-m-d')){
                        $user_free_delivery = new UserFreeDelivery;
                        $user_free_delivery->user_id = $user->id;
                        $user_free_delivery->order_id = $order->id;
                        $user_free_delivery->amount = null;
                        $user_free_delivery->expiration_date = null;
                        $user_free_delivery->description = 'Domicilio gratis en pedido #'.$order->id.'. Fecha de vencimiento: '.format_date('normal', $user->free_delivery_expiration_date);
                        $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                        $user_free_delivery->save();
                    }
                }

                if ($order_group->total_amount != $order_group_total_amount){
                    $order_group->total_amount = $order_group_total_amount;
                    $order_group->save();
                }

                $user = User::find($this->user_id);

                //elimina productos del carrito
                CartProduct::where('cart_id', $cart->id)->delete();

                $count_orders = OrderGroup::where('user_id', '=', $user->id)->count();
                $this->user_discounts = User::getDiscounts($user->id);
                $order_delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_short_with_time', $order->delivery_date) : format_date('normal_short', substr($order->delivery_date, 0 ,10)).' '.$order->delivery_time;

                $this->response = array(
                    'status' => true,
                    'message' => 'Pedido registrado',
                    'result' => array(
                        'user_id' => $user->id,
                            'order_id' => $order->id,
                            'order_status' => 'Recibido',
                            'order_reference' => $order->reference,
                            'order_date' => format_date('normal_short_with_time', $order_group->created_at),
                            'order_delivery_date' => $order_delivery_date,
                            'order_address' => $order_group->user_address.' '.$order_group->user_address_further,
                            'order_products' => $order_group->products_quantity,
                            'order_total' => $order_group->total_amount + $order_group->delivery_amount - $order_group->discount_amount,
                            'order_payment_method' => $order->payment_method,
                            'user_referral_code' => $user->referral_code,
                            'user_new' => $count_orders == 1 ? 1 : 0,
                            'user_phone_validated' => empty($user->phone_validated_date) ? 0 : 1,
                            'special_price' => 0
                        )
                );

                if ($count_orders == 1)
                {
                    $survey = Survey::find(1);
                    $answers = explode(';', $survey->answers);
                    shuffle($answers);
                    $this->response['result']['survey'] = array(
                        'id' => $survey->id,
                        'question' => $survey->question,
                        'answers' => $answers
                    );
                }

                DB::commit();

                //enviar mail de pedido a usuario
                $orders = Order::where('group_id', $order_group->id)
                                ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                                ->select('orders.*', 'user_brand_campaigns.campaign')
                                ->get();
                foreach($orders as $order)
                {
                    $store = Store::find($order->store_id);
                    $mail = array(
                        'template_name' => 'emails.order_received',
                        'subject' => self::SUBJECT_EMAIL,
                        'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
                        'vars' => array(
                            'order' => $order,
                            'order_group' => $order_group,
                            'store' => $store,
                            'products' => $products_mail[$order->id]
                        )
                    );
                    send_mail($mail);
                }

            }else $this->response['message'] = 'Valor mínimo de pedido no alcanzado.';

        }catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene productos sugeridos y descuento de usuarios
     *
     * @return array $response Respuesta
     */
    public function get_cart()
    {
        $user_discounts_data = array();
        $suggested_products = null;
        $special_price = 0;

        if (Input::has('user_id')){
            $user = User::find(Input::get('user_id'));
            if ($user){
                $user_discounts = User::getDiscounts($user->id);
                $user_discounts_data = array('credit_available' => $user_discounts['amount'], 'free_delivery_days' => $user_discounts['free_delivery_days']);
            }
            //verificar si el usuario tiene pedidos
            $user_has_orders = Order::select('orders.id')->where('user_id', $this->user_id)->where('status', '<>', 'Cancelled')->orderBy('id', 'desc')->first();
        }

        if(Input::has('cart'))
        {
            $cart = Input::get('cart');
            $product_ids = [];
            $quantities = [];
            $shelf_ids = [];
            $products_to_update = [];
            $products = json_decode($cart);

            if (count($products))
            {
                //colleccionar ids y cantidad
                foreach ($products as $id => $product)
                {
                    $product_ids[] = $id;
                    $quantities[intval($id)] = $product->qty;

                    //validar productos de primera compra
                    /*if ($id > 0 && Input::has('user_id'))
                    {
                        if (!isset($product->first_order_special_price)){
                            $product_obj = Product::find($id);
                            $first_order_special_price = $product_obj->first_order_special_price;
                        }else $first_order_special_price = $product->first_order_special_price;
                        if ($first_order_special_price && $user_has_orders){
                            if (!isset($product_obj))
                                $product_obj = Product::find($id);
                            if ($product_obj->special_price == $product->price)
                                $products_to_update[] = ['id' => intval($id), 'price' => $product_obj->price];
                        }
                    }*/
                }

                //coleccionar shelves
                foreach (StoreProduct::whereIn('id', $product_ids)->get() as $product){
                    $shelf_ids[] = $product->shelf_id;
                    if ($product->special_price)
                        $special_price += ($product->price - $product->special_price) * $quantities[$product->id];
                }
                $excluded = SuggestedProduct::promosForProducts($product_ids);
                $result = SuggestedProduct::forShelves($shelf_ids, $excluded, 1);
                if ($result)
                    $suggested_products = $result[0];
            }
        }

        $this->response = array(
            'status' => true,
            'message' => 'Información del carrito obtenida.',
            'result' => array(
                'user_discounts' => $user_discounts_data,
                'suggested_products' => $suggested_products,
                'special_price' => $special_price,
                'products_to_update' => [
                    'status' => count($products_to_update) ? true : false,
                    'message' => 'Se ha actualizado el precio de productos en promoción que era solo para primera compra.',
                    'products' => $products_to_update
                ]
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Registrar calificacion de pedido
     *
     * @return array $response Respuesta
     */
    public function score_order()
    {
        if ($order_id = Route::current()->parameter('id'))
        {
            if (!Input::has('score') || !Input::has('user_id'))
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

            $user_id = Input::get('user_id');
            if (!$order = Order::where('id', $order_id)->where('user_id', $user_id)->where('status', 'Delivered')->first()){
                $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual o el estado para calificarlo no es valido.';
                return $this->jsonResponse();
            }

            $order->user_score = Input::get('score');
            $order->user_score_comments = Input::has('comments') ? Input::get('comments') : null;
            $order->user_score_date = date('Y-m-d H:i:s');
            $order->save();

            $order_group = OrderGroup::where('order_groups.id', $order->group_id)->select('order_groups.user_firstname', 'order_groups.user_lastname')->first();
            $html = '<b>ID del pedido: </b> '.$order->id.'<br>
                    <b>Nombre del usuario: </b> '.$order_group->user_firstname.' '.$order_group->user_lastname.'<br>
                    <b>Email del usuario: </b> '.$order_group->user_email.'<br>
                    <b>Valor del pedido: </b> $'.number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount), 0, ',', '.').'<br>
                    <b>Fecha de ingreso del pedido: </b> '.$order->created_at.'<br>
                    <b>Fecha de entrega del pedido: </b> '.$order->management_date.'<br>
                    <b>Calificacion: </b> '.$order->user_score.'<br>
                    <b>Comentarios del pedido: </b> '.$order->user_score_comments.'<br>
                    <b>Dispositivo: </b>Móvil';
            $mail = array(
                'template_name' => 'emails.daily_report',
                'subject' => 'Calificacion de pedido #'.$order->id,
                'to' => array(
                    array('email' => 'servicioalcliente@merqueo.com', 'name' => 'Servicio al cliente')
                ),
                'vars' => array(
                    'title' => 'Calificacion de pedido #'.$order->id,
                    'html' => $html
                )
            );
            send_mail($mail);

            $log = new OrderLog();
            $log->type = 'Calificación a conductor: '.Input::get('score');
            $log->user_id = $user_id;
            $log->order_id = $order->id;
            $log->save();

            $this->response = array(
                 'status' => true,
                 'message' => 'Tu calificación fue guardada.'
             );

        }else return $this->jsonResponse('El ID del pedido es requerido.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtener datos de pedido para notificacion
     *
     * @return array $response Respuesta
     */
    public function notification_order()
    {
        if ($order_id = Route::current()->parameter('id'))
        {
            if (!Input::has('type') || !Input::has('user_id'))
                return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

            $user_id = Input::get('user_id');
            $order = Order::join('stores', 'stores.id', '=', 'orders.store_id')
            ->select('stores.name AS store_name', 'minimum_order_amount', 'app_logo_small_url', 'orders.*', 'is_storage')
            ->where('orders.id', $order_id)->where('user_id', $user_id)->first();
            if (!$order){
                $this->response['message'] = 'El pedido a calificar no pertenece al usuario actual.';
                return $this->jsonResponse();
            }

            $cloudfront_url = Config::get('app.aws.cloudfront_url');

            //pedido asignado
            if (Input::get('type') == 'In Progress' || Input::get('type') == 'Dispatched')
            {
                if ($order->is_storage){
                    $shopper = Driver::find($order->driver_id);
                    $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('driver_id', $order->driver_id)
                    ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('driver_id')->first();
                }else{
                    $shopper = Shopper::find($order->shopper_id);
                    $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('shopper_id', $order->shopper_id)
                    ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('shopper_id')->first();
                }
                $shopper_score_average = $result ? round($result->average, 1) : 0;
                $data =  array(
                    'shopper' => array(
                        'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
                        'name' => $shopper->first_name.' '.$shopper->last_name,
                        'phone' => $shopper->phone,
                        'shopper_score_average' => $shopper_score_average
                     ),
                    'order' => array(
                        'id' => $order->id,
                        'total_amount' => $order->total_amount,
                        'total_delivery' => $order->delivery_amount,
                        'total_discount' => $order->discount_amount,
                        'products' => OrderProduct::select('store_product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name', 'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit', 'fulfilment_status AS status')
                                            ->where('order_id', $order->id)->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))->get()->toArray()
                    )
                );
            }
            //pedido entregado
            if (Input::get('type') == 'Delivered'){
                if ($order->is_storage)
                    $shopper = Driver::find($order->driver_id);
                else $shopper = Shopper::find($order->shopper_id);

                $data =  array(
                    'shopper' => array(
                        'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
                        'name' => $shopper->first_name.' '.$shopper->last_name
                     ),
                );
            }
            //pedido cancelado
            if (Input::get('type') == 'Cancelled'){
                $order_group = OrderGroup::find($order->group_id);
                $order_delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_short_with_time', $order->delivery_date) : format_date('normal_short', substr($order->delivery_date, 0 ,10)).' '.$order->delivery_time;
                $data = array(
                    'reject_reason' => $order->reject_reason,
                    'order' => array(
                        'id' => $order->id,
                        'reference' => $order->reference,
                        'date' => format_date('normal_short_with_time', $order_group->created_at),
                        'delivery_date' => $order_delivery_date,
                        'address' => $order_group->user_address,
                        'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount,
                        'payment_method' => $order->payment_method,
                    )
                );
            }
            $data['type'] = Input::get('type');
            $data['store']['name'] = $order->store_name;
            $data['store']['minimum_order_amount'] = $order->minimum_order_amount;
            $data['store']['app_logo_small_url'] = $order->app_logo_small_url;

            $this->response = array('status' => true, 'message' => 'Notificación obtenida.', 'result' => $data);

        }else return $this->jsonResponse('El ID del pedido es requerido.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos pendientes por usuario
     *
     * @return array $response Respuesta
     */
    public function pending_orders()
    {
        if ($user_id = Route::current()->parameter('user_id'))
        {
            $orders = Order::select('orders.id', 'orders.reference', 'orders.status', 'store_id', 'date', 'total_products', 'orders.total_amount', 'orders.delivery_date', 'orders.delivery_time',
                            'orders.total_amount AS subtotal', 'orders.delivery_amount', 'orders.discount_amount', 'orders.payment_method', 'orders.shopper_id',
                            DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
                            'stores.name AS store_name', 'stores.app_logo_small_url AS store_logo_url', 'stores.is_storage')
                            ->join('order_groups', 'group_id', '=', 'order_groups.id')
                            ->join('stores', 'store_id', '=', 'stores.id')
                            ->where('orders.user_id', $user_id)
                            //->where('source', '<>', 'Reclamo')
                            ->where(function($query){
                                $query->whereNotIn('orders.status', ['Delivered', 'Cancelled'])
                                      ->orWhereRaw("TIMESTAMPDIFF(HOUR, management_date, '".date('Y-m-d H:i:s')."') < 12");
                            })
                            ->orderBy('orders.id', 'DESC')->get();
            $status = array(
                'Validation' => 'Iniciado',
                'Initiated' => 'Iniciado',
                'Enrutado' => 'En Proceso',
                'In Progress' => 'En Proceso',
                'Alistado' => 'En Proceso',
                'Dispatched' => 'En Camino',
                'Delivered' => 'Entregado',
                'Cancelled' => 'Cancelado'
            );
            $orders_user = array();
            if ($orders)
            {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                foreach($orders as $order)
                {
                    if ($order->is_storage){
                        if ($shopper = Driver::find($order->driver_id)){
                            $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('driver_id', $order->driver_id)
                            ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('driver_id')->first();
                        }
                    }else{
                        if ($shopper = Shopper::find($order->shopper_id)){
                            $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('shopper_id', $order->shopper_id)
                            ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('shopper_id')->first();
                        }
                    }
                    if ($shopper){
                        $shopper_score_average = $result ? round($result->average, 1) : 0;
                        $order->shopper = array(
                            'image' => !empty($shopper->photo_url) ? $shopper->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
                            'name' => $shopper->first_name.' '.$shopper->last_name,
                            'phone' => $shopper->phone,
                            'shopper_score_average' => $shopper_score_average
                        );
                    }

                    $order->status = $status[$order->status];
                    $order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                    $order->products = OrderProduct::select('store_product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name',
                                        'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit', 'fulfilment_status AS status')
                                        ->where('order_id', $order->id)->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))->get()->toArray();

                    $orders_user[] = $order->toArray();
                }
            }

            $this->response = array(
                'status' => true,
                'message' => 'Pedidos obtenidos',
                'result' => $orders_user
            );

        }else return $this->jsonResponse('El ID usuario es requerido.', 400);

        return $this->jsonResponse();
    }

    /**
     * Validar cupon o codigo de referido en compra
     *
     * @return array $response Respuesta
     */
    public function coupon()
    {
        if (Input::get('user_id'))
            $user = User::find(Input::get('user_id'));
        else $user = 0;

        $coupon_code = Input::get('coupon_code');
        if (empty($coupon_code))
            return $this->jsonResponse('Codigo del cupón es requerido.', 400);

        //validar codigo del cupon
        $coupon = Coupon::where('code', $coupon_code)->first();
        if (!$coupon){
            $referrer = User::where('referral_code', Input::get('coupon_code'))->where('status', 1);
            if ($user) $referrer->where('id', '<>', $user->id);
            $referrer = $referrer->first();
            if (!$referrer){
                $this->response['message'] = 'El código no es valido.';
                return $this->jsonResponse();
            }
        }

        //redimir cupon de descuento
        if ($coupon)
        {
            //validar estado
            if (!$coupon->status){
                $this->response['message'] = 'El cupón ya no esta activo.';
                return $this->jsonResponse();
            }
            //validar fecha de expiracion
            if (date('Y-m-d') > $coupon->expiration_date){
                $this->response['message'] = 'El cupón ya no esta vigente.';
                return $this->jsonResponse();
            }
            //validar numero de usos
            $uses = UserCoupon::where('coupon_id', $coupon->id)->count();
            if ($uses >= $coupon->number_uses){
                $this->response['message'] = 'El cupón ya fue utilizado.';
                return $this->jsonResponse();
            }

            //validar si el usuario ya utilizo el cupon
            $user_coupon = $user ? UserCoupon::where('user_id', $user->id)->where('coupon_id', $coupon->id)->first() : false;
            if ($user_coupon){
                $this->response['message'] = 'Ya redimiste este cupón.';
                return $this->jsonResponse();
            }
            //validar credito de primera compra
            /*if ($user && $user->first_coupon_used){
                $this->response['message'] = 'Este tipo de cupón ya fue redimido en tu cuenta.';
                return $this->jsonResponse();
            }*/
            //validar el tipo de uso del cupon
            if ($user && $coupon->type_use == 'Only The First Order'){
                $count_orders = Order::where('user_id', '=', $user->id)->where('status', '<>', 'Cancelled');
                //ignorar pedido especifico
                if (Input::has('order_id'))
                    $count_orders->where('id', '<>', Input::get('order_id'));
                $count_orders = $count_orders->count();
                if ($count_orders){
                    $this->response['message'] = 'Este cupón solo podias utilizarlo en tu primera compra.';
                    return $this->jsonResponse();
                }
            }
            if ($user && $user->created_at < date('Y-m-d H:i:s') && $coupon->type_use == 'Only New Customers' && !Input::has('is_new_user')){
                $this->response['message'] = 'Este cupón solo podias utilizarlo al crear tu cuenta de Merqueo.';
                return $this->jsonResponse();
            }
            //validar cupon en grupo de campaña
            if ($user && !empty($coupon->campaign_validation)){
                $campaign = Coupon::select('coupons.id')
                                        ->join('user_coupons', function($join) use ($user){
                                            $join->on('user_coupons.coupon_id', '=', 'coupons.id');
                                            $join->on('user_coupons.user_id', '=', DB::raw($user->id));
                                        })
                                        ->where('coupons.campaign_validation', $coupon->campaign_validation)
                                        ->first();
                if($campaign){
                    $this->response['message'] = 'Ya usaste un cupón de esta campaña.';
                    return $this->jsonResponse();
                }
            }
            //validar cupon a tienda o categoria especifica
            if ($coupon->redeem_on == 'Specific Store')
            {
                if ($products = $this->currentCart('getProducts'))
                {
                    $is_valid = false;
                    foreach($products as $product){
                        if ($product->store_id == $coupon->store_id){
                            if ($coupon->department_id){
                                if ($product->department_id == $coupon->department_id){
                                    if ($coupon->shelf_id){
                                        if ($product->shelf_id == $coupon->shelf_id){
                                            if ($coupon->store_product_id){
                                                if ($product->id == $coupon->store_product_id)
                                                    $is_valid = true;
                                            }else $is_valid = true;
                                        }
                                    }else $is_valid = true;
                                }
                            }else $is_valid = true;
                        }
                    }
                    if (!$is_valid){
                        $store = Store::select('stores.name AS store_name', 'departments.name AS department_name', 'shelves.name AS shelf_name', 'products.name AS product_name', 'cities.city')
                                      ->leftJoin('departments', function($join) use ($coupon){
                                          $join->on('departments.store_id', '=', 'stores.id');
                                          $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                                      })
                                      ->leftJoin('shelves', function($join) use ($coupon){
                                          $join->on('shelves.department_id', '=', 'departments.id');
                                          $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                                      })
                                      ->leftJoin('store_products', function ($join) use ($coupon) {
                                          $join->on('store_products.shelf_id', '=', 'shelves.id');
                                          $join->on('store_products.id', '=', DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                                      })
                                      ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                      ->join('cities', 'cities.id', '=', 'stores.city_id')
                                      ->where('stores.id', $coupon->store_id)
                                      ->first();
                        $message = $store->store_name;
                        if (!empty($store->department_name))
                            $message .= ' / '.$store->department_name;
                        if (!empty($store->shelf_name))
                            $message .= ' / '.$store->shelf_name;
                        if (!empty($store->product_name))
                            $message .= ' / '.$store->product_name;
                        $message .= ' en '.$store->city;

                        $this->response['message'] = 'Este cupón aplica exclusivamente para '.$message.', debe estar en el carrito.';
                        return $this->jsonResponse();
                    }
                }
            }

            //validar que el carrito no tenga productos en promocion
            /*if ($this->currentCart('hasSpecialprice')){
                $this->response['message'] = 'No puedes redimir el cupón por que ya tienes productos en promoción en el carrito.';
                return $this->jsonResponse();
            }*/

            //validar antes de procesar compra
            if (Input::has('validate')){
                $this->response = array(
                    'status' => true,
                    'message' => 'Cupón valido',
                    'result' => array(
                        'coupon_id' => $coupon->id,
                        'coupon_amount' => $coupon->amount
                    )
                );

                return $this->jsonResponse();
            }

            $message = 'El cupón ha sido agregado a tu pedido actual.';
            $total = $this->currentCart('getTotal');
            if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount)
                $message .= ' Para usarlo el total del pedido debe ser mayor o igual a $'.number_format($coupon->minimum_order_amount, 0, ',', '.').'.';
            else if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount)
                $message .= ' Para usarlo el total del pedido debe ser menor o igual a $'.number_format($coupon->maximum_order_amount, 0, ',', '.').'.';
            else if ($coupon->minimum_order_amount && $coupon->maximum_order_amount)
                $message .= ' Para usarlo el total del pedido debe estar entre $'.number_format($coupon->minimum_order_amount, 0, ',', '.').' y $'.number_format($coupon->maximum_order_amount, 0, ',', '.').'.';
            if ($coupon->payment_method)
                $message .= ' El metodo de pago debe ser con '.$this->config['payment_methods_names'][$coupon->payment_method].trim(' '.$coupon->cc_bank).'.';
            if ($coupon->code == 'MASTERCAMBIO')
                $message .= ' Debes tener un pedido entregado y pagado con una tarjeta diferente a Mastercard.';

            $credit = $user ? User::getDiscounts($user->id) : array('amount' => 0);

            $this->response = array(
                'status' => true,
                'message' => $message,
                'result' => array(
                    'type' => 'coupon',
                    'coupon_id' => $coupon->id,
                    'coupon_type' => $coupon->type,
                    'coupon_amount' => $coupon->amount,
                    'coupon_minimum_order_amount' => $coupon->minimum_order_amount,
                    'coupon_maximum_order_amount' => $coupon->maximum_order_amount,
                    'coupon_redeem_on' => $coupon->redeem_on,
                    'coupon_store_id' => $coupon->store_id,
                    'coupon_department_id' => $coupon->department_id,
                    'coupon_shelf_id' => $coupon->shelf_id,
                    'coupon_payment_method' => $coupon->payment_method,
                    'coupon_product_id' => $coupon->store_product_id,
                    'credit_amount' => $credit['amount'],
                )
            );
        }else{
            if ($user){
                if ($order = Order::where('user_id', $user->id)->where('status', 'Delivered')->first()){
                    $this->response['message'] = 'No puedes agregar un código referido por que ya hiciste tu primer pedido.';
                    return $this->jsonResponse();
                }
                if ($user->free_delivery_expiration_date){
                    $this->response['message'] = 'Ya redimiste un código de referido.';
                    return $this->jsonResponse();
                }
            }
            if ($referrer->total_referrals >= Config::get('app.referred.limit')){
                $this->response['message'] = 'El código referido que ingresaste no es valido por que ha alcanzado el limite permitido de referidos por cuenta.';
                return $this->jsonResponse();
            }
            //validar antes de procesar compra
            if (Input::has('validate')){
                $this->response = array(
                    'status' => true,
                    'message' => 'Código de referido valido'
                );
                return $this->jsonResponse();
            }

            $message = 'Código de referido cargado con éxito. Tienes '.Config::get('app.referred.free_delivery_days').' días de domicilio gratis. Por favor termina tu pedido.';

            $this->response = array(
                'status' => true,
                'message' => $message,
                'result' => array(
                    'type' => 'referred'
                )
            );
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene informacion del carrito actual
     *
     * @param int $action Accion a ejecutar
     * @param int $product_id ID product
     *
     * @return bool|mixed
     */
    protected function currentCart($action, $store_product_id = null)
    {
        if (Input::has('cart'))
        {
            $products = json_decode(Input::get('cart'));
            if (count($products)){
                foreach($products as $id => $data){
                    if ($store_product = StoreProduct::find($id)){
                        $store_product->cart_price = $data->price;
                        $store_product->cart_quantity = $data->qty;
                        $cart_products[$id] = $store_product;
                    }
                }
                if (!isset($cart_products))
                    return false;

                //validar si el producto esta agregado en el carrito actual
                if ($action == 'productIsInCart'){
                    if (array_key_exists($store_product_id, $cart_products))
                        return true;
                    else return false;
                }

                //obtener productos en carrito
                if ($action == 'getProducts')
                    return $cart_products;

                $has_special_price = false;
                $store_id = 0;
                $cart = array(
                    'total_amount' => 0,
                    'delivery_amount' => 0,
                    'total_quantity' => 0
                );

                foreach ($cart_products as $cart_product)
                {
                    $store_id = !$cart_product->id ? $cart_product->cart_store_id : $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id]))
                    {
                        $store_id = $cart_product->store_id;
                        $store = Store::find($store_id)->toArray();
                        $store['sub_total'] = 0;
                        $cart['stores'][$store_id] = $store;
                        $cart['stores'][$store_id]['products'] = array();
                        $cart['stores'][$store_id]['delivery_amount'] = $store['delivery_order_amount'];
                        $cart['stores'][$store_id]['discount_amount'] = 0;
                        $cart['delivery_amount'] += $store['delivery_order_amount'];
                    }

                    $store_product = json_decode(json_encode($cart_product), true);
                    if ($store_product['special_price'] > -1){
                        if ($store_product['cart_price'] < $store_product['price'])
                            $has_special_price = true;
                    }
                    $store_product['sub_total'] = $store_product['cart_quantity'] * $store_product['cart_price'];
                    $cart['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
                    $cart['total_amount'] += $store_product['sub_total'];
                    $cart['total_quantity']++;
                    array_push($cart['stores'][$store_id]['products'] , $store_product);
                }

                //obtener total del carrito
                if ($action == 'getTotal')
                    return $cart['total_amount'] + $cart['delivery_amount'];
                //varificar si tiene precio especial
                if ($action == 'hasSpecialprice')
                    return $has_special_price;

            }
        }

        return false;
    }

}
