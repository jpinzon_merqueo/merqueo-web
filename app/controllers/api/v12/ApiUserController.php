<?php

namespace api\v12;

use Input, Route, Request, Session, Hash, Config, View, DateTime, Validator, DB, Cart, User,
UserAddress, UserCreditCard, Password, Order, OrderProduct, Payment, UserAppComment, Product,
UserSurvey, Lang, OrderGroup;

class ApiUserController extends \ApiController {

	/**
	 * Realiza login de usuario
	 *
	 * @return array $response Respuesta
	 */
	public function login()
	{
		$email =  Input::get('email');
		$password =  Input::get('password');

		if (empty($email) || empty($password))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$user = User::where('email', $email)->where('type', 'merqueo.com')->first();
		if ($user)
		{
			/*$rsa = new Crypt_RSA();
			$rsa->loadKey(Config::get('app.private_key'));
			$password = $rsa->decrypt(base64_decode($password));
			if (!$password){
				$this->response['message'] = 'Formato de password no valido.';
				return $this->jsonResponse();
			}*/

			//if (Auth::validate(array('email' => $email, 'password' => $password))){
			if (Hash::check($password, $user->password))
			{
			    if ($user->status)
                {
    				$user->oauth_verifier = generate_token();
    				$user->save();

    				$cart = Cart::where('user_id', $user->id)->orderBy('updated_at', 'desc')->first();
    				if ($cart){
    					$cart_products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
    					                       ->join('cart_products', 'cart_products.store_product_id', '=', 'store_products.id')
    										   ->select('products.*', 'store_products.*', 'cart_products.quantity AS quantity_cart')
    										   ->where('cart_id', $cart->id)->get();
    					$cart = array();
    					if (count($cart_products)){
    						foreach ($cart_products as $store_product){
    							$store_product->price = $store_product->special_price > 0 ? $store_product->special_price : $store_product->price;
    							$cart[] = $store_product->toArray();
    						}
    					}
    				}else $cart = array();

    				$user = $this->getUserData($user->id);

    				$this->response = array(
    					'status' => true,
    					'message' => 'Sesion iniciada',
    					'result' => array(
        					   'user' => $user,
        					   'cart' => $cart
                         )
    				);
                }else {
                	$this->response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros.';
            	}
			}else {
				$this->response['message'] = 'Contraseña incorrecta.';
			}
		} else $this->response['message'] = 'Email incorrecto.';

		return $this->jsonResponse();
	}

	/**
	 * Cierra sesion
	 */
	public function logout()
	{
		/*$result = $user_device_token->leftJoin('user_devices AS ud', 'user_device_token.user_device_id', '=', 'ud.id')->pluck('ud.user_id');
		if (!is_null($result)) {
		}
		$user_device_token->delete();*/

		$this->response = array(
			'status' => true,
			'message' => 'Sesion cerrada'
		);

		return $this->jsonResponse();
	}

	/**
	 * Realiza registro de usuario
	 *
	 * @return array $response Respuesta
	 */
	public function save_user()
	{
		$first_name = Input::get('first_name');
		$last_name  = Input::get('last_name');
		$email      = Input::get('email');
		$password   = Input::get('password');
		$phone      = Input::get('phone');
        if (empty($first_name) || empty($last_name) || empty($email) || empty($password) || empty($phone))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		//verifica que el usuario no exista
		$validator = Validator::make(
                    array( 'email' => $email, 'phone' => $phone ),
                    array( 'email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com',
                           'phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com'
                    ),
                    array( 'email.required' => 'El email es requerido.',
                           'email.unique' => 'El email ingresado ya se encuentra en uso.',
                           'email.email' => 'El formato del email ingresado no es valido.',
                           'phone.required' => 'El número celular es requerido.',
                           'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                           'phone.numeric' => 'El número celular debe ser númerico.',
                           'phone.digits' => 'El número celular debe tener 10 digitos.'
                    )
        );
		if ($validator->fails()){
			$messages = $validator->messages();
			if (!$error = $messages->first('email'))
				$error = $messages->first('phone');
			$this->response['message'] = $error;
			return $this->jsonResponse();
		}else {
			//crear el usuario, realizar login y enviar mail de bienvenida
			$user = User::add($first_name, $last_name, $phone, $email, $password);
            //validar y cargar codigo de referido
            $referred = User::validateReferred($user);

			if (!Input::has('do_not_send_mail'))
			{
				$mail = array(
	                'template_name' => 'emails.welcome',
	                'subject' => 'Bienvenido a Merqueo',
	                'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
	                'vars' => array(
	                  'referral_code' => $user->referral_code,
                      'referred' => $referred,
                      'name' => $user->first_name
	                )
	            );
				send_mail($mail);
			}
			$user = $this->getUserData($user->id);

			$this->response = array(
				'status' => true,
				'message' => 'Usuario creado',
				'result' => array('user' => $user)
			);
		}

		return $this->jsonResponse();
	}

	/**
	 * Realiza registro y login de usuario con Facebook
	 *
	 * @return array $response Respuesta
	 */
	public function social()
	{
		$fb_id = Input::get('fb_id');
		if (empty($fb_id))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$this->response['message'] = 'Ocurrió un error al procesar tus datos de Facebook.';

		if (Input::has('user_id')){
			if ($user = User::find(Input::get('user_id'))){
				$user->fb_id = $fb_id;
				$user->save();

                $user = $this->getUserData($user->id);
				$this->response = array(
					'status' => true,
					'message' => 'Cuenta asociada',
					'result' => array('user' => $user, 'fb_type' => 'associated_account')
				);
			}else return $this->jsonResponse('ID de usuario no existe.', 400);
		} else {
		    //login con fb
			if ($user = User::where('fb_id', $fb_id)->first())
			{
			    if ($user->status){
		    		$user = $this->getUserData($user->id);

    				$this->response = array(
    					'status' => true,
    					'message' => 'Sesión iniciada',
    					'result' => array('user' => $user, 'fb_type' => 'login')
    				);
                }else $this->response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros.';
			} else{
                $email = Input::get('email');
                if ($user = User::where('email', $email)->first()){
                    if ($user->status){
                        $user->fb_id = $fb_id;
                        $user->save();
                        $user = $this->getUserData($user->id);
                        $this->response = array(
                            'status' => true,
                            'message' => 'Cuenta asociada',
                            'result' => array('user' => $user, 'fb_type' => 'signed_up')
                        );
                    }else $this->response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros.';
                }else{
                	//registro y login
    				$first_name = Input::get('first_name');
    				$last_name = Input::get('last_name');
    				$phone = Input::has('phone') ? Input::get('phone') : '';

    				if (empty($first_name) || empty($last_name))
    					return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
                    if (empty($email))
                        $email = '';
                        /*$this->response['message'] = 'El email es requerido, por favor ingresalo en tu cuenta de Facebook.';
                        return $this->jsonResponse();*/

    				//verifica que el usuario no exista
    				$validator = Validator::make(
                                array( 'email' => $email, 'phone' => $phone ),
                                array( 'email' => 'email|unique:users,email,NULL,id,type,merqueo.com', 'phone' => 'numeric|unique:users,phone,NULL,id,type,merqueo.com' ),
                                array( //'email.required' => 'El email es requerido.',
                                       'email.unique' => 'El email ingresado ya se encuentra en uso.',
                                       'email.email' => 'El formato del email ingresado no es valido.',
                                       'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                                       'phone.numeric' => 'El número celular debe ser númerico.'
                                )
                    );
    				if ($validator->fails()){
    					$messages = $validator->messages();
    					if (!$error = $messages->first('email'))
    						$error = $messages->first('phone');
    					$this->response['message'] = $error;
    					return $this->jsonResponse();
    				}else {
    					$user = User::add($first_name, $last_name, $phone, $email, '');
    					$user->fb_id = $fb_id;
    					$user->save();

                        //validar y cargar codigo de referido
                        $referred = User::validateReferred($user);

    					$mail = array(
    		                'template_name' => 'emails.welcome',
    		                'subject' => 'Bienvenido a Merqueo',
    		                'to' => array(array('email' => $user->email, 'name' => $user->first_name.' '.$user->last_name)),
    		                'vars' => array(
    		                    'referral_code' => $user->referral_code,
                                'referred' => $referred,
                                'name' => $user->first_name
    		                )
    		            );
    					send_mail($mail);
    					$user = $this->getUserData($user->id);

    					$this->response = array(
    						'status' => true,
    						'message' => 'Cuenta creada y asociada',
    						'result' => array('user' => $user, 'fb_type' => 'signed_up')
    					);
    				}
				}
			}
		}

		return $this->jsonResponse();
	}

    /**
	 * Obtiene datos de usuario con direcciones, pedidos, tarjetas de credito
	 *
	 * @return array $response Respuesta
	 */
	public function get_user()
	{
		if (!$id = Route::current()->parameter('id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if ($user = $this->getUserData($id))
		{
			$this->response = array(
				'status' => true,
				'message' => 'Usuario obtenido',
				'result' => $user
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Actualiza datos de usuario
	 *
	 * @return array $response Respuesta
	 */
	public function update_user()
	{
		if (!$id = Route::current()->parameter('id'))
			return $this->jsonResponse('ID es requerido.', 400);

		$user = User::find($id);
		if ($user)
		{
			if (Input::has('first_name'))
				$user->first_name = Input::get('first_name');
			if (Input::has('last_name'))
				$user->last_name = Input::get('last_name');
			/*if (Input::has('phone'))
				$user->phone = Input::get('phone');
			if (Input::has('email'))
				$user->email = Input::get('email');*/
			if (Input::has('password'))
				$user->password = Hash::make(Input::get('password'));

			//verifica que el usuario no exista
			$validator = Validator::make(
					    array( 'email' => $user->email, 'phone' => $user->phone ),
					    array( 'email' => 'required|email|unique:users,email,'.$user->id.',id,type,merqueo.com', 'phone' => 'required|numeric|unique:users,phone,'.$user->id.',id,type,merqueo.com'),
					    array( 'email.unique' => 'El email ingresado ya se encuentra en uso.',
					    	   'email.email' => 'El formato del email ingresado no es valido.',
					    	   'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
					    	   'phone.numeric' => 'El número celular debe ser númerico.'
						)
			);

			if ($validator->fails()){
				$messages = $validator->messages();
				if (!$error = $messages->first('email'))
					$error = $messages->first('phone');
				$this->response['message'] = $error;
				return $this->jsonResponse();
			}

			$user->save();
			$user = $this->getUserData($user->id);

			$this->response = array(
				'status' => true,
				'message' => 'Usuario actualizado',
				'result' => $user
			);

		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

    /**
     * Agregar codigo de referido
     */
    public function referred()
    {
        if (!$id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID es requerido.', 400);

        if (!Input::has('promo_code'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $user = User::find($id);
        if ($user)
        {
            $order = Order::where('user_id', $user->id)->where('status', 'Delivered')->first();
            if (!$order)
            {
                $referred = User::validateReferred($user);
                if ($referred['success'])
                {
                    $diff = new DateTime($user->free_delivery_expiration_date);
                    $free_delivery_days = intval($diff->diff(new DateTime())->format('%a'));
                    $this->response = array(
                        'status' => true,
                        'message' => 'Tu código de referido fue procesado con éxito, tienes '.$free_delivery_days.' días de domicilio gratis a partir de hoy.',
                        'result' => array(
                            'free_delivery_days' => $free_delivery_days,
                            'free_delivery_expiration_date' => format_date('normal_long', $referred['free_delivery_expiration_date']),
                        )
                    );
                }
                if ($referred['limit'])
                    $this->response['message'] = 'El código referido que ingresaste no es valido por que ha alcanzado el limite permitido de referidos por cuenta.';
                else if ($referred['error'])
                        $this->response['message'] = 'El código de referido que ingresaste no es valido, por favor verificalo y vuelve a intentarlo.';

            }else $this->response['message'] = 'No puedes agregar un código referido por que ya hiciste tu primer pedido.';

            return $this->jsonResponse();

        }else return $this->jsonResponse('Usuario no existe.', 400);
    }

    /**
     * Recordar clave
     *
     * @return array $response Respuesta
     */
    public function password_remind()
    {
        if ($user = Password::getUser(array('email' => Input::get('email'))))
        {
            //generar token
            $token = str_shuffle(sha1($user->email.microtime(true)));
            $token = hash_hmac('sha1', $token, 'ofkshgokshogkis');
            $values = ['email' => $user->email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')];
            DB::table('password_reminders')->insert($values);

            $vars = array(
				'username' => $user->first_name,
	            'website_name' => Config::get('app.website_name'),
	            'user_url' => url('/mi-cuenta'),
	            'contact_email' => Config::get('app.admin_email'),
				'contact_phone' => Config::get('app.admin_phone'),
				'android_url' => Config::get('app.android_url'),
				'ios_url' => Config::get('app.ios_url'),
				'facebook_url' => Config::get('app.facebook_url'),
				'twitter_url' => Config::get('app.twitter_url'),
				'token' => $token
	        );

            $mail = array(
                'template_name' => 'emails.auth.reminder',
                'subject' => 'Restaurar contraseña',
                'to' => array(
                    array('email' => $user->email, 'name' => $user->first_name)
                ),
                'vars' => $vars
            );
            $result = send_mail($mail);
            if ($result){
                $this->response = array(
                    'status' => true,
                    'message' => Lang::get('reminders.sent')
                );
            }else $this->response['message'] = Lang::get('reminders.error');
        }else $this->response['message'] = Lang::get('reminders.user');

        return $this->jsonResponse();
    }

	/**
	 * Obtiene datos de una direccion
	 *
	 * @return array $response Respuesta
	 */
	public function get_address()
	{
		if (!$id = Route::current()->parameter('address_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$address = UserAddress::select('user_address.*', DB::raw('IF (parent_city_id IS NULL, city_id, parent_city_id) AS parent_city_id'))
		                      ->join('cities', 'city_id', '=', 'cities.id')->where('id', $id)->where('user_id', $user_id)->get();
		if (count($address)){
			$this->response = array(
				'status' => true,
				'message' => 'Dirección obtenida',
				'result' => array('address' => $address->toArray())
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Guarda una nueva direccion
	 *
	 * @return array $response Respuesta
	 */
	public function save_address()
	{
		if (!$user_id = Route::current()->parameter('id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$label = Input::get('label');
		$address = Input::get('address');
		$city = Input::get('city');
		$dir = Input::get('dir');

		if (empty($label) || empty($address) || empty($city) || empty($dir))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$address = new UserAddress;
		$address->user_id = $user_id;
		$address->label = Input::get('label');
		$address->address = Input::get('address');
		$address->address_further = Input::get('address_further');
        $address->neighborhood = Input::has('neighborhood') ? Input::get('neighborhood') : '';
		$dir = Input::get('dir');
		$address->address_1 = $dir[0];
		$address->address_2 = $dir[2];
		$address->address_3 = $dir[6];
		$address->address_4 = $dir[8];
		$request = Request::create('/api/location', 'GET', array('address' => $address->address, 'city' => Input::get('city')));
		Request::replace($request->input());
		$result = json_decode(Route::dispatch($request)->getContent());
		if ($result->status){
			$address->latitude = $result->result->latitude;
			$address->longitude = $result->result->longitude;
			$address->city_id = $result->result->city_id;
			$address->save();
			$this->response = array(
				'status' => true,
				'message' => 'Dirección guardada.',
				'result' => array('address' => $address->toArray())
			);
		}else $this->response['message'] = 'Ocurrió un error al ubicar tu dirección.';

		return $this->jsonResponse();
	}

	/**
	 * Actualiza una direccion
	 *
	 * @return array $response Respuesta
	 */
	public function update_address()
	{
		if (!$id = Route::current()->parameter('address_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$label = Input::get('label');
		$address = Input::get('address');
		$city = Input::get('city');
		$dir = Input::get('dir');

		if (empty($label) || empty($address) || empty($city) || empty($dir))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		$address = UserAddress::where('user_id', $user_id)->where('id', $id)->first();
		if ($address){
			$address->label = Input::get('label');
			$address->address = Input::get('address');
			$address->address_further = Input::get('address_further');
			$dir = Input::get('dir');
			$address->address_1 = $dir[0];
			$address->address_2 = $dir[2];
			$address->address_3 = $dir[6];
			$address->address_4 = $dir[8];
			$request = Request::create('/api/location', 'GET', array('address' => $address->address, 'city' => Input::get('city')));
			Request::replace($request->input());
			$result = json_decode(Route::dispatch($request)->getContent());
			if ($result->status){
				$address->latitude = $result->result->latitude;
				$address->longitude = $result->result->longitude;
				$address->city_id = $result->result->city_id;
				$address->save();
				$this->response = array(
					'status' => true,
					'message' => 'Dirección actualizada.',
					'result' => array('address' => $address->toArray())
				);
			}else $this->response['message'] = 'Ocurrió un error al ubicar tu dirección.';
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Elimina una direccion
	 *
	 * @return array $response Respuesta
	 */
	public function delete_address()
	{
		if (!$id = Route::current()->parameter('address_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$affectedRows = UserAddress::where('user_id', $user_id)->where('id', $id)->delete();
		if ($affectedRows){
			$this->response = array(
				'status' => true,
				'message' => 'Dirección eliminada',
				'result' => ''
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Obtiene datos de una tarjeta de credito
	 *
	 * @return array $response Respuesta
	 */
	public function get_credit_card()
	{
		if (!$id = Route::current()->parameter('credit_card_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		$credit_card = UserCreditCard::select('id', 'last_four', 'type')->where('id', $id)->where('user_id', $user_id)->get();
		if (count($credit_card)){
			$this->response = array(
				'status' => true,
				'message' => 'Tarjeta de crédito obtenida.',
				'result' => array('credit_card' => $credit_card->toArray())
			);
		}else return $this->jsonResponse('ID no existe.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Guarda una nueva tarjeta de crédito
	 *
	 * @return array $response Respuesta
	 */
	public function save_credit_card()
	{
		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);
		$number_cc = Input::get('number_cc');
		$expiration_month_cc = Input::get('expiration_month_cc');
		$expiration_year_cc = Input::get('expiration_year_cc');
		$code_cc = Input::get('code_cc');
		$name_cc = Input::get('name_cc');

		if (empty($number_cc) || empty($expiration_month_cc) || empty($expiration_year_cc) || empty($code_cc) || empty($name_cc))
			return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

		if (Input::has('user_id'))
		{
			//validar datos de tarjeta
			/*$rsa = new Crypt_RSA();
			$rsa->loadKey(Config::get('app.private_key'));

			$number_cc = $rsa->decrypt(base64_decode($number_cc));
			if (!$number_cc)
				return $this->jsonResponse('Formato de número de tarjeta no valido.', 400);
			$code_cc = $rsa->decrypt(base64_decode($code_cc));
			if (!$code_cc)
			    return $this->jsonResponse('Formato de código de tarjeta no valido.', 400);
			$name_cc = $rsa->decrypt(base64_decode($name_cc));
			if (!$name_cc)
			 	return $this->jsonResponse('Formato de nombre de tarjeta no valido.', 400);
			$expiration_month_cc = $rsa->decrypt(base64_decode($expiration_month_cc));
			if (!$expiration_month_cc)
			    return $this->jsonResponse('Formato de mes de tarjeta no valido.', 400);
			$expiration_year_cc = $rsa->decrypt(base64_decode($expiration_year_cc));
			if (!$expiration_year_cc)
			 	return $this->jsonResponse('Formato de año de tarjeta no valido.', 400);*/

			if (!$user = User::find(Input::get('user_id')))
				return $this->jsonResponse('Usuario no valido.', 400);

			$data = array(
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'phone' => $user->phone,
				'user_id' => $user->id
			);

			$address = UserAddress::where('user_id', $user->id)->first();
			if ($address){
				$data['address'] = $address->address;
				$data['address_further'] = $address->address_further;
			}else $data['address'] = $data['address_further'] = 'No especificada';

			$data = array_merge($data, Input::all());
			$data['number_cc'] = $number_cc;
			$data['code_cc'] = $code_cc;
			$data['name_cc'] = $name_cc;
			$data['expiration_month_cc'] = $expiration_month_cc;
			$data['expiration_year_cc'] = $expiration_year_cc;

			$payment = new Payment;

			//si el usuario no esta creado en la api se crea
			if (empty($user->customer_token)){
				$result = $payment->createCustomer($data);
				if (!$result['status']){
					$this->response['message'] = 'Ocurrió un error al procesar tus datos personales de la tarjeta de crédito.';
					return $this->jsonResponse();
			    }
				$user->customer_token = $data['customer_token'] = $result['response']->id;
				$user->save();
			}else $data['customer_token'] = $user->customer_token;

			//se guarda y asocia a cliente en api
			$result = $payment->associateCreditCard($data);
			if (!$result['status']){
				$this->response['message'] = 'Ocurrió un error al validar los datos de tu tarjeta de crédito.';
				return $this->jsonResponse();
		    }
            if (!in_array($result['response']->type, $this->config['credit_cards_types'])){
				$this->response['message'] = 'Solo se aceptan tarjetas de crédito '.$this->config['credit_cards_message'];
				return $this->jsonResponse();
		    }

			$data['card_token'] = $result['response']->id;
			//guardar tarjeta en bd
			$user_credit_card = new UserCreditCard;
			$user_credit_card->user_id = $user->id;
			$user_credit_card->card_token = $data['card_token'];
			$user_credit_card->last_four = $result['response']->lastFour;
			$user_credit_card->type = $result['response']->type;
			$user_credit_card->created_at = $user_credit_card->updated_at = date('Y-m-d H:i:s');
			$user_credit_card->save();

			$credit_card['id'] = $user_credit_card->id;
			$credit_card['last_four'] = $user_credit_card->last_four;
			$credit_card['type'] = $user_credit_card->type;

			$this->response = array(
				'status' => true,
				'message' => 'Tarjeta de crédito guardada.',
				'result' => array('credit_card' => $credit_card)
			);

		}else return $this->jsonResponse('Usuario no valido.', 400);

		return $this->jsonResponse();
	}

	/**
	 * Elimina una tarjeta de crédito
	 *
	 * @return array $response Respuesta
	 */
	public function delete_credit_card()
	{
		if (!$id = Route::current()->parameter('credit_card_id'))
			return $this->jsonResponse('ID es requerido.', 400);

		if (!$user_id = Route::current()->parameter('user_id'))
			return $this->jsonResponse('ID usuario es requerido.', 400);

		//validar pedidos en proceso con la tarjeta a eliminar
		$orders = Order::join('order_groups', 'order_groups.id', '=', 'orders.group_id')
						->where('status', '<>', 'Delivered')
						->where('status', '<>', 'Cancelled')
						->where('credit_card_id', $id)
						->count();
		if (!$orders){
			$user = User::find($user_id);
			$user_credit_card = UserCreditCard::find($id);
			$payment = new Payment;
			$result = $payment->deleteCreditCard($user->id, $user->customer_token, $user_credit_card->card_token);
			if (!$result['status']){
				$this->response['message'] = 'Ocurrió un error al eliminar la tarjeta de crédito.';
				return $this->jsonResponse();
		    }
		}else{
			$this->response['message'] = 'No se puede eliminar la tarjeta de crédito por que esta asociada a un pedido en proceso.';
			return $this->jsonResponse();
		}

		if (UserCreditCard::where('user_id', $user->id)->where('id', $id)->delete()){
			$this->response = array(
				'status' => true,
				'message' => 'Tarjeta de crédito eliminada.',
				'result' => ''
			);
		}else return $this->jsonResponse('ID no existe.', 400);
		return $this->jsonResponse();
	}

    /**
     * Obtiene credito actual del usuario
     *
     * @return array $response Respuesta
     */
    public function get_user_credit()
    {
        if (!$id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID es requerido.', 400);

        $user = User::find($id);
        if ($user){

            $user_discounts = User::getDiscounts($user->id);
            $this->response = array(
                'status' => true,
                'message' => 'Crédito disponible obtenido.',
                'result' => array(
                    'credit_available' => $user_discounts['amount']
                )
            );
        }else return $this->jsonResponse('ID no existe.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtiene descuentos de usuario actual (credito y domicilio gratis)
     *
     * @return array $response Respuesta
     */
    public function get_user_discounts()
    {
        if (!$id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID es requerido.', 400);

        $user = User::find($id);
        if ($user){

            $user_discounts = User::getDiscounts($user->id);
            $this->response = array(
                'status' => true,
                'message' => 'Descuentos de usuario obtenidos.',
                'result' => array(
                    'credit_available' => $user_discounts['amount'],
                    'free_delivery_days' => $user_discounts['free_delivery_days'],
                    'free_delivery_expiration_date' => format_date('normal_long', $user->free_delivery_expiration_date)
                )
            );
        }else return $this->jsonResponse('ID no existe.', 400);

        return $this->jsonResponse();
    }

    /**
     * Obtener productos de pedido para agregar al carrito
     *
     * @return array $response Respuesta
     */
    public function add_order_cart()
    {
        if (!$order_id = Route::current()->parameter('order_id'))
            return $this->jsonResponse('ID del pedido es requerido.', 400);

        if (!$user_id = Route::current()->parameter('user_id'))
            return $this->jsonResponse('ID del usuario es requerido.', 400);

        //validacion precio para primera compra
        $special_price = 'special_price';
        if ($has_orders = Order::select('id')->where('user_id', $user_id)->where('status', '<>', 'Cancelled')->orderBy('id', 'desc')->first()){
            $special_price = DB::raw("IF(first_order_special_price = 1, NULL, special_price) AS special_price");
            $discount_percentage = DB::raw('IF(first_order_special_price = 1, NULL, ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0)) AS discount_percentage');
        }else $discount_percentage = DB::raw('ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS discount_percentage');

        $products = OrderProduct::select('store_products.id', 'products.slug', 'products.name', 'store_products.price', $special_price, $discount_percentage,
                                'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
                                'products.quantity', 'products.unit', 'products.description', 'nutrition_facts', 'store_products.store_id', 'stores.name AS store', 'store_products.department_id',
                                'departments.name AS department', 'store_products.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning', 'delivery_discount_amount', 'order_products.quantity AS quantity_cart')
                                ->join('orders', 'orders.id', '=', 'order_products.order_id')
                                ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                                ->join('products', 'products.id', '=', 'store_products.product_id')
                                ->join('departments', 'departments.id', '=', 'store_products.department_id')
                                ->join('stores', 'stores.id', '=', 'departments.store_id')
                                ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                                ->where('orders.id', $order_id)
                                ->where('orders.user_id', $user_id)
                                ->where('store_products.status', 1)
                                ->where('stores.status', 1)
                                ->groupBy('store_products.id')
                                ->get();
        if (!$products){
            $this->response['message'] = 'No se puede agregar por que la tienda o los productos donde se hizo el pedido no esta habilitados.';
            return $this->jsonResponse();
        }

        foreach($products as $store_product)
        {
            if ($store_product->promo_type_special_price == 'Merqueo' && $store_product->special_price > -1 && $store_product->quantity_special_price && $store_product->cart_quantity) {
                $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                                        ->where('store_product_id', $store_product->id)
                                        ->where('price', $store_product->special_price)
                                        ->where('user_id', $user_id)
                                        ->where('status', '<>', 'Cancelled')
                                        ->where(DB::raw("DATEDIFF('".date('Y-m-d')."', date)"), '<=', Config::get('app.minimum_promo_days'))
                                        ->groupBy('order_products.store_product_id')
                                        ->count();
                if ($count_products){
                    $this->response['message'] = 'Ya realizaste un pedido con el limite permitido para el producto en promoción: '.$store_product->name;
                    return $this->jsonResponse();
                }

                if ($store_product->quantity_cart > $store_product->quantity_special_price){
                    $this->response['message'] = 'El producto '.$store_product->name.' por estar en promoción puedes agregar máximo '.$store_product->quantity_special_price.' unidades al carrito.';
                    return $this->jsonResponse();
                }
            }
        }

        $this->response = array(
            'status' => true,
            'message' => 'Productos obtenidos',
			'store' => Order::find($order_id)->store->name,
            'result' => $products
        );

        return $this->jsonResponse();
    }

    /**
     * Guarda comentario negativo de usuario en el app
     *
     * @return array $response Respuesta
     */
    public function save_comment()
    {
        if (!$user_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID usuario es requerido.', 400);

        if (!Input::has('comment') || !Input::has('os') || !Input::has('app_version'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $user_app_comment = new UserAppComment;
        $user_app_comment->user_id = $user_id;
        if (Input::has('order_id'))
            $user_app_comment->order_id = Input::get('order_id');

        $user_app_comment->os = Input::get('os');
        $user_app_comment->app_version = Input::get('app_version');
        $user_app_comment->comment = Input::get('comment');
        $user_app_comment->save();

        if (Input::has('order_id')) {
            $order = Order::where('orders.id', Input::get('order_id'))->first();
            $order_group = OrderGroup::where('order_groups.id', $order->group_id)->select('order_groups.user_firstname', 'order_groups.user_lastname')->first();
            $html = '<b>ID del pedido: </b> '.$order->id.'<br>
                    <b>Nombre del usuario: </b> '.$order_group->user_firstname.' '.$order_group->user_lastname.'<br>
                    <b>Valor del pedido: </b> $'.number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount), 0, ',', '.').'<br>
                    <b>Fecha de ingreso del pedido: </b> '.$order->created_at.'<br>
                    <b>Comentarios de experiencia: </b> '.$user_app_comment->comment.'<br>
                    <b>Dispositivo: </b>Móvil';
            $mail = array(
                'template_name' => 'emails.daily_report',
                'subject' => 'Comentario experiencia app pedido #'.$order->id,
                'to' => array(
                    array('email' => 'servicioalcliente@merqueo.com', 'name' => 'Servicio al cliente')
                ),
                'vars' => array(
                    'title' => 'Comentario experiencia app pedido #'.$order->id,
                    'html' => $html
                )
            );
            send_mail($mail);
        }

        $this->response = array(
            'status' => true,
            'message' => 'Tu comentario fue guardado, muchas gracias.',
        );

        return $this->jsonResponse();
    }

    /**
     * Guarda respuesta de encuesta para primer pedido
     *
     * @return array $response Respuesta
     */
    public function save_survey()
    {
        if (!$user_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID usuario es requerido.', 400);

        if (!Input::has('survey_id') || !Input::has('answer') || !Input::has('os') || !Input::has('order_id'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $user_app_survey = new UserSurvey;
        $user_app_survey->survey_id = Input::get('survey_id');
        $user_app_survey->user_id = $user_id;
        $user_app_survey->order_id = Input::get('order_id');
        $user_app_survey->os = Input::get('os');
        $user_app_survey->answer = Input::get('answer');
        $user_app_survey->save();

        $this->response = array(
            'status' => true,
            'message' => 'Tu respuesta fue guardada, muchas gracias.'
        );

        return $this->jsonResponse();
    }

    /**
     * Actualiza numero de celular del usuario como validado
     *
     * @return array $response Respuesta
     */
    public function cellphone_validated()
    {
        if (!$user_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID del usuario es requerido.', 400);

        if (!Input::has('cellphone'))
            return $this->jsonResponse('Celular es requerido.', 400);

        if (!$user = User::find($user_id))
            return $this->jsonResponse('El usuario no existe.', 400);

        //actualizar celular
        if ($user->phone != Input::get('cellphone'))
            $user->phone = Input::get('cellphone');

        $user->phone_validated_date = date('Y-m-d H:i:s');
        $user->save();

        User::where('phone', $user->phone)->where('id', '<>', $user->id)->update(['phone_validated_date' => null]);

        $this->response = array(
            'status' => true,
            'message' => 'Tu número celular fue validado con éxito.'
        );

        return $this->jsonResponse();
    }

}
