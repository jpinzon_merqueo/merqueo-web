<?php

namespace api\v12;

use Input, Route, Request, DB, Store, Department, Shelf, Product, Order, SearchLog, Banner, UserAddress;

class ApiStoreController extends \ApiController
{
    public function __construct()
    {
        parent::__construct();

        //validacion precio para primera compra
        $this->has_orders = false;
        $this->special_price = 'special_price';
        if (Input::has('user_id')){
            if ($this->has_orders = Order::select('id')->where('user_id', Input::get('user_id'))->where('status', '<>', 'Cancelled')->orderBy('id', 'desc')->first())
                $this->special_price = DB::raw("IF(first_order_special_price = 1, NULL, special_price) AS special_price");
        }
        $this->discount_percentage = DB::raw('ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS discount_percentage');
        if ($this->has_orders)
            $this->discount_percentage = DB::raw('IF(first_order_special_price = 1, NULL, ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0)) AS discount_percentage');
    }

	/**
     * Obtiene tiendas que atienden una ubicación de latitud y longitud
     *
     * @return array $response Respuesta
     */
    public function get_stores()
    {
        $address = Input::get('address');
        if (Input::has('lat') && Input::has('lng') && empty($address)) {
            $lat = Input::get('lat');
            $lng = Input::get('lng');
        } else {
            $city = Input::get('city');
            $address = urldecode($address);

            if (!empty($address) && !empty($city))
            {
                $get_data = Input::all();
                //obtener latitud longitud
                $request = Request::create('/api/location', 'GET', array('address' => $address, 'city' => $city));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if ($result['status'])
                {
                    $lat = $result['result']['latitude'];
                    $lng = $result['result']['longitude'];
                    $address = $result['result']['address'];
                    $neighborhood = $result['result']['neighborhood'];
                    $locality = $result['result']['locality'];

                    //validar direccion en el sur de la ciudad
                    $this->response = UserAddress::validateAddress($result, $get_data);
                    if ($this->response['status'])
                        return $this->jsonResponse();

                } else {
                    $lat = null;
                    $lng = null;
                }
            } else $this->jsonResponse('Hay parametros obligatorios vacios.', 400);
        }

        $message = 'No encontramos tu dirección';
        $detail = 'Mueve el mapa hasta la ubicación donde quieres recibir tu pedido.';
        $status = false;
        $found = false;
        $result = array();

        if (isset($lat) && isset($lng))
        {
            $found = true;
            //obtener direccion
            if (!isset($address)) {
                $request = Request::create('/api/location_reverse', 'GET', array('lat' => $lat, 'lng' => $lng));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);

                $address = null;
                $neighborhood = null;
                $locality = null;

                if ($result['status']) {
                    $address = $result['result']['address'];
                    $neighborhood = $result['result']['neighborhood'];
                    $locality = $result['result']['locality'];
                }
            }

            $address_obj = array(
                'address' => trim($address),
                'lat' => strval($lat),
                'lng' => strval($lng),
                'neighborhood' => $neighborhood,
                'locality' => $locality
            );

            $stores = array();
            $rows = Store::getByLatlng($lat, $lng);

            if (count($rows)) {
                $status = true;
                $message = 'Se encontraron tiendas dentro de la ubicación';
                $detail = '';

                foreach($rows as $store) {
                    $slot = Store::getDeliverySlot($store->id);
                    $store->is_open = $slot['is_open'];
                    $store->slot = mb_strtolower($slot['next_slot']);
                    $stores[] = $store->toArray();
                }
            } else {
                $status = false;
                $message = 'En este momento no tenemos cobertura en tu dirección';
                $detail = 'Por favor ingresa otra dirección.';
            }

            $result = array('address' => $address_obj, 'stores' => $stores);
        }

        $this->response = array(
            'status' => $status,
            'message' => $message,
            'result' => $result,
            'found' => $found,
            'detail' => $detail
        );

        return $this->jsonResponse();
    }

    /**
     * Obtiene tiempo de entrega de tiendas en texto
     *
     * @return array $response Respuesta
     */
    public function get_stores_delivery_time()
    {
        if (!Input::has('lat') || !Input::has('lng'))
            $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $stores = array();
        $rows = Store::getByLatlng(Input::get('lat'), Input::get('lng'));
        if (count($rows))
        {
            foreach($rows as $store){
                $slot = Store::getDeliverySlot($store->id);
                $stores[$store->id]['is_open'] = $slot['is_open'];
                $stores[$store->id]['slot'] = $slot['next_slot'];
            }

            $this->response = array(
                'status' => true,
                'message' => 'Horarios de tiendas obtenidos',
                'result' => $stores
            );
        }else{
              $this->response = array(
                'status' => false,
                'message' => 'En este momento no tenemos cobertura en tu dirección'
              );
        }

        return $this->jsonResponse();
    }

	/**
	 * Obtiene datos de tienda, arbol de categorias y categorias con productos
	 *
	 * @return array $response Respuesta
	 */
	public function get_store($id)
	{
		if ($id)
		{
			$store = Store::select('id', 'name', 'is_storage', 'city_id', 'delivery_time', 'minimum_order_amount', 'delivery_order_amount',
		                 'description_app', 'app_logo_large_url', 'app_logo_small_url')
			              ->where('id', $id)->first();
			if ($store){
				$slot = Store::getDeliverySlot($store->id);
				$store->is_open = $slot['is_open'];
				$store->slot = $slot['next_slot'];
                $result['store'] = $store->toArray();

				$departments = Department::select('departments.id', 'departments.name', 'departments.store_id')
                ->join('store_products', 'store_products.department_id', '=', 'departments.id')
				->join('products', 'products.id', '=', 'store_products.product_id')->where('departments.store_id', $store->id)
				->where('store_products.status', 1)->where('departments.status', 1)->groupBy('departments.id')
                ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
                ->where('products.type', '<>', 'Proveedor')
				->orderBy('departments.sort_order')->orderBy('departments.name');

                if ($this->user_agent == 'iOS')
                    $departments->where('departments.name', 'NOT LIKE', '%Ciga%');

				$departments = $departments->get();

				$result['store']['departments'] = $departments ? $departments->toArray() : array();

                //si el usuario esta logueado mostrar ultimos pedidos
                if (Input::has('user_id')){
                    $products = Product::select('store_products.id', 'products.slug', 'products.name', 'store_products.price', $this->special_price, $this->discount_percentage,
                                    'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
                                    'products.quantity', 'products.unit', 'products.description', 'nutrition_facts', 'store_products.store_id', 'stores.name AS store', 'store_products.department_id',
                                    'departments.name AS department', 'store_products.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning', 'delivery_discount_amount', DB::raw('COUNT(products.id) AS quantity_products'))
                                   ->join('store_products', 'store_products.product_id', '=', 'products.id')
                                   ->join('order_products','store_products.id','=','order_products.store_product_id')
                                   ->join('orders','orders.id','=','order_products.order_id')
                                   ->join('departments', 'departments.id', '=', 'store_products.department_id')
                                   ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                                   ->join('stores', 'stores.id', '=', 'departments.store_id')
                                   ->where('user_id', Input::get('user_id'))
                                   ->where('order_products.store_id', $store->id)
                                   ->where('orders.status', '<>', 'Cancelled')
                                   ->where('store_products.status', 1)
                                   ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
                                   ->where('products.type', '<>', 'Proveedor')
                                   ->groupBy('products.id')
                                   ->orderBy('quantity_products', 'desc')
                                   ->get();

                   $result['user_products'] = count($products) ? 1 : 0;
                }

				$banners_front = Banner::where('banners.store_id', $store->id)
                            ->where('banners.status', 1)
                            ->select('banners.title', 'banners.image_app_url', 'banners.position', 'banners.deeplink_type AS type', 'banners.deeplink_store_id AS store_id',
                            'banners.deeplink_department_id AS department_id', 'banners.deeplink_shelf_id AS shelf_id', 'banners.deeplink_store_product_id AS store_product_id', 'banners.deeplink_store_product_id AS product_id', 'stores.name AS store', 'departments.name AS department', 'shelves.name AS shelf', 'products.name AS product', 'banners.is_for_first_order')
							->leftJoin('stores', 'banners.deeplink_store_id', '=', 'stores.id')
                            ->leftJoin('departments', 'banners.deeplink_department_id', '=', 'departments.id')
                            ->leftJoin('shelves', 'banners.deeplink_shelf_id', '=', 'shelves.id')
                            ->leftJoin(DB::raw('(store_products, products)'), function($join){
                                 $join->on('banners.deeplink_store_product_id', '=', 'store_products.id');
                                 $join->on('store_products.product_id', '=', 'products.id');
                            })
							->get()
                            ->toArray();

				$order = array();
				foreach ($banners_front as $key => $row)
				{
					if($this->has_orders && $row['is_for_first_order'])
		               unset($banners_front[$key]);
		            else
		               $order[$key] = $row['position'];
				}
				array_multisort($order, SORT_ASC, $banners_front);

				foreach ($banners_front as $key => &$banner_front) {
					foreach ($banner_front as $key2 => &$value) {
						if ( empty($value) ) {
							unset($banner_front[$key2]);
						}
						if ( $key2 == 'position' ) {
							unset($banner_front[$key2]);
						}
					}
				}

				$result['banners'] = $banners_front;

				$this->response = array(
					'status' => true,
					'message' => 'Tienda obtenida',
					'result' => $result
				);
			}else return $this->jsonResponse('ID no existe', 400);
		}

		return $this->jsonResponse();
	}

	/**
     * Obtiene departamento con pasillos y productos
     *
     * @return array $response Respuesta
     */
    public function get_department()
    {
        if ($id = Route::current()->parameter('department_id'))
        {
            $store_id = Route::current()->parameter('store_id');
            $department = Department::select('departments.id', 'departments.store_id', 'departments.name', 'departments.show_more_app')
                                    ->join('store_products', 'store_products.department_id', '=', 'departments.id')
                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                    ->where('departments.store_id', $store_id)
                                    ->where('store_products.status', 1)
                                    ->where('departments.status', 1)
                                    ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
                                    ->where('products.type', '<>', 'Proveedor')
                                    ->where('departments.id', $id)
                                    ->groupBy('departments.id')
                                    ->first();
            if ($department){
                $result['department'] = $department ? $department->toArray() : array();
                if ($department->show_more_app){
                    //paginacion para shelves
                    $rows = 5;
                    if (Input::has('p'))
                        $page = intval(Input::get('p'));
                    if (!isset($page)) {
                        $begin = 0;
                        $page = 1;
                    }else $begin = ($page - 1) * $rows;
                }else{
                    $rows = 5;
                    $begin = 0;
                    $page = 1;
                }
                $shelves = Shelf::select('shelves.id', 'shelves.store_id', 'shelves.department_id', 'shelves.name', 'show_more_app')
                                ->join('store_products', 'store_products.shelf_id', '=', 'shelves.id')
                                ->join('products', 'store_products.product_id', '=', 'products.id')
                                ->where('shelves.department_id', $department->id)
                                ->where('store_products.status', 1)
                                ->where('shelves.status', 1)
                                ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
                                ->where('products.type', '<>', 'Proveedor');
                if ($this->user_agent == 'iOS')
                    $shelves->where('shelves.name', 'NOT LIKE', '%Ciga%');
                $shelves = $shelves->groupBy('shelves.id')->orderBy('shelves.sort_order')->orderBy('shelves.name')->limit($rows)->offset($begin)->get();
                $result['department']['shelves'] = $shelves ? $shelves->toArray() : array();

                foreach ($shelves as $index => $shelf)
                {
                    $shelf_data = array();
                    $products = Product::select('store_products.id', 'products.slug', 'products.name', 'price', $this->special_price, $this->discount_percentage,
											'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
											'quantity', 'unit', 'products.description', 'nutrition_facts', 'store_products.store_id', 'stores.name AS store', 'store_products.department_id',
											'departments.name AS department', 'store_products.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning', 'delivery_discount_amount')
                                            ->join('store_products', 'store_products.product_id', '=', 'products.id')
											->join('stores', 'stores.id', '=', 'store_products.store_id')
											->join('departments', 'departments.id', '=', 'store_products.department_id')
											->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
	                                        ->where('store_products.shelf_id', $shelf->id)
										    ->where('store_products.status', 1)
	                                        ->whereRaw('IF(store_products.is_visible_stock = 0, IF(store_products.current_stock > 0, TRUE, FALSE), TRUE)');
                    if ($this->user_agent == 'iOS')
                        $products->where('products.name', 'NOT LIKE', '%Ciga%');
                    //paginacion para productos sin boton ver mas
                    if (!$shelf->show_more_app){
                        $rows = 24;
                        if (Input::has('p'))
                            $page = intval(Input::get('p'));
                        if (!isset($page)) {
                            $begin = 0;
                            $page = 1;
                        }else $begin = ($page - 1) * $rows;
                        $products = $products->orderBy('store_products.sort_order')->limit($rows)->offset($begin)->get();
                    }else{
                        $limit = $shelf->show_more_app ? 3 : 220;
                        $products = $products->orderBy('store_products.sort_order')->limit($limit)->get();
                    }
                    foreach ($products as $product) {
                        array_push($shelf_data, $product->toArray());
                    }
                    $result['department']['shelves'][$index]['products'] = $shelf_data;

                    $shelves = Shelf::select('shelves.id', 'shelves.name', 'shelves.department_id', 'shelves.store_id')
                                    ->join('store_products', 'shelves.id', '=', 'store_products.shelf_id')
                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                    ->where('shelves.department_id', $department->id)
                                    ->where('store_products.status', 1)
                                    ->where('shelves.status', 1)
                                    ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
                                    ->where('products.type', '<>', 'Proveedor')
                                    ->groupBy('shelves.id')
                                    ->orderBy('shelves.sort_order')
                                    ->orderBy('shelves.name')
                                    ->get();
                    $result['department']['menu'] = $shelves ? $shelves->toArray() : array();
                }

                $this->response = array(
                    'status' => true,
                    'message' => 'Categoria obtenida',
                    'result' => $result
                );
            }else return $this->jsonResponse('ID no existe o categoria sin productos', 400);
        }

        return $this->jsonResponse();
    }

	/**
	 * Obtiene pasillo con productos
	 *
	 * @return array $response Respuesta
	 */
	public function get_shelf()
	{
		if ($id = Route::current()->parameter('shelf_id'))
		{
			$store_id = Route::current()->parameter('store_id');
			$department_id = Route::current()->parameter('department_id');
			$shelf = Shelf::select('shelves.id', 'shelves.name')
                         ->join('store_products', 'shelves.id', '=', 'store_products.shelf_id')
                         ->where('shelves.store_id', $store_id)
			             ->where('shelves.department_id', $department_id)
			             ->where('store_products.status', 1)
			             ->where('shelves.status', 1)
			             ->groupBy('shelves.id')
			             ->where('shelves.id', $id)
			             ->first();

			if ($shelf){
				$result['shelf'] = $shelf ? $shelf->toArray() : array();
				$shelf_data = array();
				//paginacion para productos
				$rows = 24;
				if (Input::has('p'))
					$page = intval(Input::get('p'));
				if (!isset($page)) {
				    $begin = 0;
				    $page = 1;
				}else $begin = ($page - 1) * $rows;

                $products = Product::select('store_products.id', 'products.slug', 'products.name', 'price', $this->special_price, $this->discount_percentage,
									'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
									'quantity', 'unit', 'products.description', 'nutrition_facts', 'shelves.has_warning', 'delivery_discount_amount',
									'store_products.store_id', 'stores.name AS store', 'store_products.department_id', 'departments.name AS department', 'store_products.shelf_id', 'shelves.name AS shelf')
                                     ->join('store_products', 'products.id', '=', 'store_products.product_id')
									 ->join('stores', 'stores.id', '=', 'store_products.store_id')
									 ->join('departments', 'departments.id', '=', 'store_products.department_id')
									 ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
									 ->where('store_products.shelf_id', $shelf->id)
									 ->where('store_products.status', 1)
		                             ->whereRaw('IF(store_products.is_visible_stock = 0, IF(store_products.current_stock > 0, TRUE, FALSE), TRUE)');
                if ($this->user_agent == 'iOS')
                    $products->where('products.name', 'NOT LIKE', '%Ciga%');
				$products = $products->orderBy('store_products.sort_order')->limit($rows)->offset($begin)->get();

				foreach ($products as $product) {
					array_push($shelf_data, $product->toArray());
				}
				$result['shelf']['products'] = $shelf_data;

				$this->response = array(
					'status' => true,
					'message' => 'Subcategoria obtenida',
					'result' => $result
				);
			}else return $this->jsonResponse('ID no existe o subcategoria sin productos', 400);
		}

		return $this->jsonResponse();
	}

    /**
     * Obtiene producto
     *
     * @return array $response Respuesta
     */
    public function get_product()
    {
        if ($id = Route::current()->parameter('id'))
        {
            $product = Product::select('store_products.id', 'products.slug', 'products.name', 'price', $this->special_price, $this->discount_percentage,
									'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
									'quantity', 'unit', 'products.description', 'nutrition_facts',  'store_products.store_id', 'stores.name AS store', 'store_products.department_id', 'departments.name AS department',
									'store_products.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning', 'delivery_discount_amount')
                                    ->join('store_products', 'products.id', '=', 'store_products.product_id')
									->join('stores', 'stores.id', '=', 'store_products.store_id')
									->join('departments', 'departments.id', '=', 'store_products.department_id')
									->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
		                            ->where('store_products.id', $id)
									->where('store_products.status', 1)
									->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
									->where('products.type', '<>', 'Proveedor')
									->first();
            if ($product){
                $this->response = array(
                    'status' => true,
                    'message' => 'Producto obtenido',
                    'result' => $product->toArray()
                );
            }else return $this->jsonResponse('ID no existe o esta inactivo', 400);
        }

        return $this->jsonResponse();
    }

    /**
     * Obtiene ultimos productos de un usuario pedidos en una tienda
     *
     * @return array $response Respuesta
     */
    public function get_user_products()
    {
        $store_id = Route::current()->parameter('store_id');
        $user_id = Route::current()->parameter('user_id');
        if ($store_id && $user_id)
        {
            $result = array();
            $products = Product::select('store_products.id', 'products.slug', 'products.name', 'store_products.price', $this->special_price, $this->discount_percentage,
										'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
										'products.quantity', 'unit', 'products.description', 'nutrition_facts', 'delivery_discount_amount',
										'store_products.store_id', 'stores.name AS store', 'store_products.department_id', 'departments.name AS department', 'store_products.shelf_id', 'shelves.name AS shelf', 'shelves.has_warning',
										DB::raw('COUNT(products.id) AS quantity_products'))
                                       ->join('store_products', 'store_products.product_id', '=', 'products.id')
                                       ->join('order_products','store_products.id','=','order_products.store_product_id')
                                       ->join('orders','orders.id','=','order_products.order_id')
                                       ->join('departments', 'departments.id', '=', 'store_products.department_id')
                                       ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                                       ->join('stores', 'stores.id', '=', 'departments.store_id')
			                           ->where('user_id', $user_id)
			                           ->where('order_products.store_id', $store_id)
			                           ->where('store_products.status', 1)
			                           ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
			                           ->where('products.type', '<>', 'Proveedor')
			                           ->groupBy('products.id')
			                           ->orderBy('quantity_products', 'desc')
                                       ->get();
           if ($products)
               $result = $products;

            $this->response = array(
                'status' => true,
                'message' => 'Productos de usuario en tienda obtenidos',
                'result' => $result
            );
        }else return $this->jsonResponse('Datos obligatorios vacios.', 400);

        return $this->jsonResponse();
    }

	/**
	 * Buscador de productos por tiendas
	 *
	 * @return array $response Respuesta
	 */
	public function search()
    {
        if ($id = Route::current()->parameter('id'))
		{
		    $store = Store::find($id);
			$city_id = $store->city_id;

            /*//NUEVO BUSCADOR
            if (Input::has('q'))
            {
                $q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);

                $rows = 15;
                $page = Input::has('p') ? intval(Input::get('p')) : 1;

                $client = new GuzzleHttp\Client([
                    'base_uri' => getenv('APP_AES.MSE_HOST')
                ]);
                $response = $client->request('GET', '/store/'.$store->id.'/search/'.$q, [
                    'query' => [
                        'page' => $page,
                        'size' => $rows
                    ],
                    'headers' => [
                        'Authorization' => 'Bearer '. getenv('APP_AES.MERQUEO_READ_TOKEN'),
                        'Content-Type:' => 'application/json'
                    ]
                ]);

                if ($response->getStatusCode() == 200) {
                    $response = $response->getBody()->getContents();
                    $response = json_decode($response, true);
                    if ($response['status']) {
                        $store_products = $response['result'];
                        if (!count($store_products[$store->id])) {
                            $search_log = new SearchLog;
                            $search = Input::get('q');
                            if ($this->user_id)
                                $search_log->user_id = $this->user_id;
                            $search_log->search = $search;
                            $search_log->ip = Request::getClientIp();
                            $search_log->store_id = $store->id;
                            $search_log->save();
                        }
                    }
                }
            }

            $stores = array();
            if (isset($store_products[$store->id]) && count($store_products[$store->id])){
                $data['logo_url'] = $store->app_logo_small_url;
                $data['name'] = $store->name;
                $data['store_id'] = $store->id;
                foreach ($store_products[$store->id] as $key => $product){
                    $data['products'][] = $product;
                }
                $stores[] = $data;
            }
            if (isset($store_products['other']) && count($store_products['other'])){
                foreach ($store_products['other'] as $key => $products) {
                    $store = Store::where('id', $key)->where('status', 1)->first();
                    if (!is_null($store)) {
                        $data['logo_url'] = $store->app_logo_small_url;
                        $data['name'] = $store->name;
                        $data['store_id'] = $store->id;
                        $data['products'] = $products;
                        $stores[] = $data;
                    }
                }
            }

            if (!count($stores))
                $this->response = array('status' => true, 'message' => 'No se encontraron resultados.', 'result' => $stores);
            else $this->response = array('status' => true, 'message' => 'Resultado de busqueda', 'result' => $stores);*/

            //ANTIGUO BUSCADOR
            $query = Product::where('stores.city_id', $city_id);
            if (Input::has('lat') && Input::has('lng')){
                $store_ids = array();
                $lat = Input::get('lat');
                $lng = Input::get('lng');
                $result = array();
                $rows = Store::getByLatlng($lat, $lng);
                if (count($rows)){
                    foreach($rows as $store)
                        $store_ids[] = $store->id;
                }
                $query->whereIn('stores.id', $store_ids);
            }

			if (Input::has('q')){
				$q = DB::connection()->getPdo()->quote(strip_tags(Input::get('q')));
                $q = get_clean_string($q);
				$keys = explode(' ', $q);
				if (count($keys)){
					$keywords = '';
					foreach($keys as $key){
						if (($key != '') and ($key != ' ')){
							if ($keywords != '') $keywords .= '-space-';
							$keywords .= $key;
						}
					}
					$keywords = explode('-space-', $keywords);
					foreach($keywords as $keyword){
						$query->whereRaw('(products.name LIKE "%'.trim($keyword).'%" COLLATE utf8_spanish_ci OR products.name LIKE "%'.get_plural_string(trim($keyword)).'%" COLLATE utf8_spanish_ci)');
					}
				}

                $query->select(
                    'store_products.id', 'products.slug', 'store_products.store_id', 'store_products.department_id', 'store_products.shelf_id', 'products.name', 'price', $this->special_price, $this->discount_percentage,
                    'quantity_special_price', 'first_order_special_price', 'image_large_url', 'image_medium_url', 'image_small_url', 'image_app_url', 'is_best_price',
                    'quantity', 'unit', 'products.description', 'nutrition_facts', 'delivery_discount_amount',
                    'departments.slug AS department_slug',
					'departments.name AS department',
                    'shelves.slug AS shelf_slug',
					'shelves.name AS shelf',
					'shelves.has_warning',
                    'stores.name AS store_name',
					'stores.name AS store',
                    'stores.slug AS store_slug',
                    'stores.app_logo_small_url AS store_logo',
                    'stores.description_app AS store_description_app'
                )->distinct()
                ->join('store_products', 'store_products.product_id', '=', 'products.id')
                ->join('shelves', 'store_products.shelf_id', '=', 'shelves.id')
                ->join('departments', 'store_products.department_id', '=', 'departments.id')
                ->join('stores', 'store_products.store_id', '=', 'stores.id')
                ->where('departments.status', 1)
                ->where('shelves.status', 1)
                ->where('stores.status', 1)
                ->where('store_products.status', 1)
                ->whereRaw('IF( store_products.is_visible_stock = 0, IF( store_products.current_stock > 0, TRUE, FALSE), TRUE )')
                ->where('products.type', '<>', 'Proveedor')
                ->orderBy(
                    DB::raw('CASE
                                WHEN store_products.store_id = '.(int)$id.' THEN 0
                                WHEN store_products.store_id != '.(int)$id.' THEN 1
                            END
                    ')
                )
                ->orderBy('stores.id');

                if ( !empty($keywords) ) {
                	foreach($keywords as $keyword){
                	    $keywords[] = get_plural_string(trim($keyword));
                	}
                    foreach($keywords as $keyword){
                        $query->orderBy(DB::raw('   CASE
                                                        WHEN products.name LIKE \''.trim($keyword).' %\' THEN 0
                                                        WHEN products.name LIKE \'% %'.trim($keyword).'% %\' THEN 1
                                                        WHEN products.name LIKE \'%'.trim($keyword).'\' THEN 2
                                                        ELSE 3
                                                    END
                                                '));
                    }
                }
                if ($this->user_agent == 'iOS')
                    $query->where('products.name', 'NOT LIKE', '%Ciga%');
			}

			$rows = 15;
			if (Input::has('p'))
				$page = intval(Input::get('p'));
			if (!isset($page)) {
			    $begin = 0;
			    $page = 1;
			}else $begin = ($page - 1) * $rows;
            // $products = $query->orderBy('products.name', 'asc')->limit($rows)->offset($begin)->get();
            $products = $query->orderBy('products.name', 'asc')->get();

			$result = array();
        	$store_id = 0;
			if (count($products)){
				foreach ($products as $product){
					$result[$product->store_id]['logo_url'] = $product->store_logo;
	                $result[$product->store_id]['name'] = $product->store_name;
	                $result[$product->store_id]['store_id'] = $product->store_id;
                    $result[$product->store_id]['description_app'] = $product->store_description_app;
	                $product_temp = $product->toArray();
	                unset($product_temp['department_slug']);
	                unset($product_temp['shelf_slug']);
	                unset($product_temp['store_name']);
	                unset($product_temp['store_slug']);
	                unset($product_temp['store_logo']);
	                $result[$product->store_id]['products'][] = $product_temp;
				}
				$result = array_values($result);
				$this->response = array('status' => true, 'message' => 'Resultado de busqueda', 'result' => $result);
			}else{
                $search_log = new SearchLog;
                $search = Input::get('q');
                if ($this->user_id)
                    $search_log->user_id = $this->user_id;
                $search_log->search = $search;
                $search_log->ip = Request::getClientIp();
                $search_log->store_id = $store->id;
                $search_log->save();
				$this->response = array('status' => true, 'message' => 'No se encontraron resultados.', 'result' => $result);
			}

		}else return $this->jsonResponse('ID no existe', 400);

		return $this->jsonResponse();
    }

}
