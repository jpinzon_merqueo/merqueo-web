<?php

namespace api\transporters\v11;

use Aws\CloudFront\Exception\Exception;
use Input, Hash, Config, DB, Route, Vehicle, Order, OrderProduct, Admin, ArrayObject, Carbon\Carbon, Response, Driver, View, RejectReason,
 Auth, VehicleMovement, Product, OrderProductGroup, Routes, Zone, Pusherer;
use OrderPickingBasket;

class ApiTransporterController extends \ApiController
{

    /**
     * Realiza login de transportador
     *
     * @return array $response Respuesta
     */
    public function login()
    {
        $plate =  Input::get('plate');
        $password =  Input::get('password');
        if(!Input::has('device_id'))
            return $this->jsonResponse('El id del dispositivo es obligatorio', 400);

        if (empty($plate) || empty($password))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if ($vehicle = Vehicle::where('plate', $plate)->where('status', 1)->first())
        {
            if (Hash::check($password, $vehicle->password))
            {
                if ($vehicle->status)
                {
                    $vehicle_drivers = \VehicleDrivers::where('vehicle_id',$vehicle->id)->first();
                    $driver = Driver::find($vehicle_drivers->driver_id);

                    if (!$vehicle->terms){
                        $view = View::make('shopper.terms');
                        $vehicle->terms = $view->render();
                    }

                    $vehicle->device_id = Input::get('device_id');
                    $vehicle->save();

                    $vehicle = array(
                        'id' => $vehicle->id,
                        'plate' => $vehicle->plate,
                        'driver' => $driver->first_name.' '.$driver->last_name,
                        'terms' => $vehicle->terms
                    );

                    $this->response = array(
                        'status' => true,
                        'message' => 'Sesión iniciada',
                        'result' => array('vehicle' => $vehicle, 'balance' => Vehicle::getBalance($vehicle['id']))
                    );
                }else $this->response['message'] = 'Debes seleccionar un shopper con perfil de Alistador Frío de la lista de abajo.';
            }else $this->response['message'] = 'Contraseña incorrecta.';
        }else $this->response['message'] = 'Placa incorrecta.';

        return $this->jsonResponse();
    }

    /** Funcion para validar la versión de la app instalada
     * @return \Illuminate\Http\JsonResponse
     */
    public function validate_version(){
        if (Input::has('app_version'))
        {
            $update = 'not_required'; //not_required, required, suggested
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if ($app_version < Config::get('app.transporters.app_version')){
                $message = 'Hemos realizado cambios en el app, por favor actualiza a la última versión para continuar.';
                $url = 'https://drive.google.com/file/d/1Fm3f9Vphfar7ObU_94cO7tMVXyHOBkuh/view?usp=sharing';
                return Response::json(array('status' => true, 'update_app' => array('message' => $message, 'url' => $url)) , 200);
            }else{
                return Response::json(array('status' => false) , 200);
            }
        }
    }

    /**
     * Cierra sesion
     */
    public function logout()
    {
        if (!Input::has('vehicle_id')){
            $this->response = array(
                'status' => true,
                'message' => 'ID vehiculo es requerido'
            );
            return Response::json( $this->response, 400 );
        }

        $this->response = array(
            'status' => true,
            'message' => 'Sesión cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Acepta terminos y condiciones
     */
    public function terms($id)
    {
        if (!$id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID vehiculo es requerido.', 400);

        if (Input::has('terms') && $vehicle = Vehicle::where('id', $id)->first()){
            $vehicle->terms = 1;
            $vehicle->save();

            $this->response = array(
                'status' => true,
                'message' => 'Terminos actualizados.'
            );
        }else return $this->jsonResponse('Ocurrió un error al actualizar los datos.');

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos en proceso y alistados asignados al transportador
     *
     * @return array $response Respuesta
     */
    public function enlisted_orders($id)
    {
        $array_status = ['Dispatched' => 'Despachado','Delivered'=>'Entregado','Cancelled'=>'Cancelado', 'En Camino'=>'En Camino'];
        if (!is_numeric($id))
            return $this->jsonResponse('ID transportador debe ser numérico.', 400);

        if (!$vehicle = Vehicle::find($id))
            return $this->jsonResponse('ID vehiculo no existe.', 400);

        //pedidos pendientes
        $orders = Order::select(
            'orders.id',
            DB::raw("IF(orders.status = 'Dispatched' AND orders.driver_cancel_date IS NULL AND orders.onmyway_date IS NOT NULL, 'En Camino', IF(orders.status = 'Dispatched' AND orders.driver_cancel_date IS NOT NULL, 'Cancelled', orders.status)) AS status"),
            'orders.delivery_amount',
            'orders.discount_amount',
            'orders.total_amount',
            'orders.reject_reason',
            DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'),
            DB::raw("(SELECT sum(IFNULL(order_product_group.quantity, 0)) + SUM(IF(order_products.`type` = 'Agrupado', 0, order_products.quantity)) FROM order_products LEFT JOIN order_product_group ON order_products.id = order_product_group.order_product_id WHERE order_products.order_id = orders.id) AS total_products"),
            'orders.onmyway_date',
            'orders.arrived_date',
            'orders.management_date',
            DB::raw("IFNULL(orders.picking_baskets,0) AS picking_baskets"),
            DB::raw("IFNULL(orders.picking_bags,0) AS picking_bags"),
            'orders.planning_sequence',
            'orders.route_id',
            'routes.route AS planning_route',
            'order_groups.user_firstname',
            'order_groups.user_lastname',
            'order_groups.user_phone',
            'order_groups.user_address',
            'order_groups.user_address_further',
            'order_groups.user_address_neighborhood',
            'cities.city AS user_city',
            'order_groups.user_address_latitude',
            'order_groups.user_address_longitude',
            'orders.delivery_time',
            'payment_method',
            'orders.cc_charge_id',
            'order_groups.user_comments AS user_comments',
            'orders.delivery_date',
            DB::raw('DATE(delivery_date) AS delivery_date_day'),
            DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.user_id <> 5169 AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
            ->join('users','orders.user_id','=','users.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('cities', 'cities.id', '=', 'user_city_id')
            ->join('routes', 'routes.id', '=', 'route_id')
            //->where('orders.type', 'Merqueo')
            ->where('orders.vehicle_id', $vehicle->id)
            ->whereIn('orders.status', ['Dispatched', 'Delivered', 'Cancelled'])
            ->where(DB::raw('DATE(orders.delivery_date)'),Carbon::now()->toDateString())
            ->orderBy('orders.status', 'DESC')
            ->orderBy('routes.planning_date')
            ->orderBy('routes.id')
            ->orderBy('orders.planning_sequence');

        if (Input::get('order_id'))
            $orders->where('orders.id', Input::get('order_id'));
        if (Input::get('ids'))
            $orders->whereNotIn('orders.id', Input::get('ids'));

        $orders = $orders->get();

        $pending_orders = $orders->toArray();
        $reject_reasons = RejectReason::select('id', 'status','reason','type')->where('is_storage', 1)->where('status', 'Cancelado temporalmente')->orderBy('reason')->get()->toArray();
        $type_news = \NewReason::select(DB::raw('DISTINCT type'))->where('status', 1)->whereIn('type', ['Productos','Cancelación','Reprogramación','Error o Cambio de dirección'])->orderBy('type')->get()->toArray();
        $new_reasons = \NewReason::select('id','reason','type')->where('status', 1)->whereIn('type', ['Productos','Cancelación','Reprogramación','Error o Cambio de dirección'])->orderBy('reason')->get()->toArray();
        $return_reasons = \NewReason::select('id','reason','type')->where('status', 1)->whereIn('type', ['Recogida'])->orderBy('reason')->get()->toArray();

        //mezclar arrays
        foreach($pending_orders as $i => $order)
        {
            if ($order['management_date'] && date('Y-m-d 00:00:00') > $order['management_date']){
                unset($pending_orders[$i]);
                continue;
            }
            $news_transportes = \TransporterNew::where('order_id',$order['id'] )->whereIn('status', ['Sin Asignar', 'Pendiente'])->count();
            $pending_orders[$i]['has_news'] = ($news_transportes > 0 );
            $pending_orders[$i]['status_text'] = $array_status[$order['status']];

            //OBTENER PRODUCTOS DEL PEDIDO
            $products = OrderProduct::select(
                'order_products.*',
                DB::raw("IF(order_products.fulfilment_status = 'Not Available',0,order_products.quantity) AS quantity"),
                'products.reference',
                'products.description',
                'products.image_large_url',
                'store_products.storage',
                'orders.arrived_date'
            )
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->orderBy('department_id', 'desc')
                ->orderBy('product_name', 'asc')
                ->where('order_id', $order['id'])
                ->whereIn('order_products.type', ['Product','Muestra', 'Recogida'])
                ->get();

            $products_returned = OrderProduct::select('order_products.id')
                ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->orderBy('department_id', 'desc')
                ->orderBy('product_name', 'asc')
                ->where('order_id', $order['id'])
                ->whereIn('order_products.type', ['Recogida'])
                ->count();

            $order_products = $products->toArray();
            $product_group = OrderProductGroup::select(
                'order_products.*',
                DB::raw("IF(order_products.fulfilment_status = 'Not Available',0,order_products.quantity) AS quantity"),
                'order_product_group.*',
                'store_products.storage',
                'orders.arrived_date'
            )
                ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->where('order_product_group.order_id', $order['id'])
                ->get();

            if ($product_group){
                $order_products =array_merge($order_products, $product_group->toArray() );
            }
            $pending_orders[$i]['products_returned'] = $products_returned;
            $pending_orders[$i]['reject_reasons'] = $reject_reasons;
            $pending_orders[$i]['type_news'] = $type_news;
            $pending_orders[$i]['new_reasons'] = $new_reasons;
            if ($order['status'] == 'Cancelled')
                $pending_orders[$i]['reject_reason'] = $order['reject_reason'];

            if (empty($order['cc_charge_id']))
                $pending_orders[$i]['cc_charge_id'] = '';

            //$pending_orders[$i]['products'] = $order_products;

            if (empty($pending_orders[$i]['qty_orders']))
                $pending_orders[$i]['qty_orders'] = 0;

            //ocultar direccion teniendo en cuenta la franja de entrega
            if ($pending_orders[$i]['planning_sequence'] > 1){
                $delivery_time = explode(' - ', $pending_orders[$i]['delivery_time']);
                if (date('Y-m-d H:i', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes')) >= date('Y-m-d H:i')){
                    $pending_orders[$i]['user_address'] = 'Es visible a las '.date('g:i a', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes'));
                    $pending_orders[$i]['user_address_neighborhood'] = 'Es visible a las '.date('g:i a', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes'));
                    $pending_orders[$i]['user_address_latitude'] = 0;
                    $pending_orders[$i]['user_address_longitude'] = 0;
                }
            }
        }
        $pending_orders = array_values($pending_orders);

        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'orders' => $pending_orders,
                'return_reasons' => $return_reasons
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Ordenar pedidos por secuencia o distancia
     *
     * @return array $response Respuesta
     */
    public function orders_sort_by()
    {
        if (!Input::has('order_ids'))
            return $this->jsonResponse('No hay pedidos para ordenar', 200);

        if (!Input::has('lat') && !Input::has('lng'))
            return $this->jsonResponse('Para ordenar los pedidos por favor habilita tu GPS.', 200);

        $order_ids = explode(',', Input::get('order_ids'));
        $merqueo_location = [
            'lat' => Input::get('lat'),
            'lng' => Input::get('lng')
        ];
        $orders = Order::select('orders.id','planning_sequence',
                                DB::raw("(6371 * ACOS(
                                COS(RADIANS(".$merqueo_location['lat']."))
                                * COS(RADIANS(user_address_latitude))
                                * COS(RADIANS(user_address_longitude)
                                - RADIANS(".$merqueo_location['lng']."))
                                + SIN(RADIANS(".$merqueo_location['lat']."))
                                * SIN(RADIANS(user_address_latitude))
                                   )) AS distance"))
                                ->join('order_groups', 'group_id', '=', 'order_groups.id')
                                ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
                                ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
                                ->where('orders.type', 'Merqueo')
                                ->where('orders.status', 'Dispatched')
                                ->whereIn('orders.id', $order_ids)
                                ->orderBy('orders.delivery_date')
                                ->orderBy(Input::get('orderby'))
                                ->get()
                                ->toArray();

        if ($orders){
            $this->response = array(
                'status' => true,
                'message' => 'Pedidos ordenados.',
                'result' => array(
                    'orders' => $orders
                )
            );

            return $this->jsonResponse();

        }else return $this->jsonResponse('No se encontraron pedidos para ordenar.');
    }

    /**
      * Obtener movimientos de shopper
      *
      * @return array $response Respuesta
      */
    public function get_movements()
    {
        if (!Input::has('id'))
            return $this->jsonResponse('ID vehiculo es requerido.', 400);

        $vehicle_id = Input::get('id');
        if (!$vehicle = Vehicle::find($vehicle_id))
            return $this->jsonResponse('Vehiculo no existe.', 400);

        $balance = Vehicle::getBalance($vehicle->id);
        $movements = VehicleMovement::select('vehicle_movements.*',DB::raw("ROUND(IF(vehicle_movements.user_cash_paid IS NULL AND vehicle_movements.user_card_paid = 0 , vehicle_movements.driver_balance*-1, vehicle_movements.user_cash_paid )) AS cash_paid"),
            DB::raw("IF(vehicle_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
            DB::raw("DATE_FORMAT(vehicle_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"), 'payment_method',
            DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"))
            ->leftJoin('admin', 'vehicle_movements.admin_id', '=', 'admin.id')
            ->leftJoin('orders', 'vehicle_movements.order_id', '=', 'orders.id')
            ->join('drivers', 'vehicle_movements.driver_id', '=', 'drivers.id')
            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'))
            ->where('vehicle_movements.vehicle_id', $vehicle->id)
            /*->where(function ($query){
                $query->whereNull('orders.type');
            })*/
            ->groupBy('vehicle_movements.id')
            ->groupBy('vehicle_movements.order_id')
            ->groupBy('vehicle_movements.status')
            ->orderBy('vehicle_movements.created_at', 'desc')
            ->get();

        $color = $balance < 0 ? 'red' : 'green';

        $this->response = array(
            'status' => true,
            'message' => 'Movimientos obtenidos',
            'result' => array(
                'driver_balance' => round($balance),
                'movements' => $movements,
                'color_balance'=> $color,
                'date' => date('d/m/Y')
            )
        );

        return $this->jsonResponse();
     }

     /**
     * Autenticacion de pusher
     */
    public function pusher_auth()
    {
        if (Auth::check() || Session::get('user_logged_in')){
            $response = Response::make(  Pusherer::auth_push( Input::get('channel_name'), Input::get('socket_id'), 'transporter' ), 200);
            return $response->header('Content-Type', 'application/json');
        }

        return $this->jsonResponse();
    }

    /**
     * Función para generar alertas en el módulo de alertas de transportadores
     */
    public function add_transporter_alert()
    {
        if(!Input::has('id'))
            return $this->jsonResponse('El ID del vehiculo es obligatorio.', 400);
        $id = Input::get('id'); 

        if(!Input::has('alert_num'))
            return $this->jsonResponse('El número de la alerta es obligatorio.', 400);

        $alert_num = Input::get('alert_num');

        if ( ($vehicle = Vehicle::find($id)) && !empty($alert_num) ){
            $vehicle->addAlert((int)$alert_num, 'Pendiente');
            return $this->jsonResponse('Se ha agregado la alerta.', 200);
        }
        return $this->jsonResponse('ID vehiculo no existe.', 400);
    }

    /**
     * Función para validar que el vehículo esté dentro de la zona asignada.
     */
    public function validate_vehicle_zone()
    {
        if(!Input::has('id'))
            return $this->jsonResponse('El ID del vehiculo es obligatorio', 400);
        $id = Input::get('id');
        if ( ($vehicle = Vehicle::find($id)) && Input::has('lat') && Input::has('lng') ){
            $now = Carbon::now();
            $order = Order::where('orders.vehicle_id', $vehicle->id)
                        ->where('orders.type', 'Merqueo')
                        ->where(DB::raw('DATE(orders.management_date)'), '=', $now->toDateString())
                        ->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString())
                        ->where('orders.status', 'Delivered')
                        ->orderBy('orders.delivery_date', 'DESC')
                        ->get();
            if ( count($order) > 0 ) {
                $order = $order->first();
                $route = Routes::find($order->route_id);
                $zone = Zone::find($route->zone_id);

                $is_valid = self::inZone($zone->delivery_zone, Input::get('lat'), Input::get('lng'));
                if ( !$is_valid ) {
                    $vehicle->addAlert(4, 'Pendiente');
                    return $this->jsonResponse('Se ha agregado la alerta.', 200);
                }else{
                    return $this->jsonResponse('El vehículo se encuentra en la zona asignada.', 200);
                }
            }
            return $this->jsonResponse('El vehiculo aún no tiene pedidos asignados.', 200);
        }
        return $this->jsonResponse('ID vehiculo no existe.', 400);
    }

    /**
     * Retorna un token de firebase que otorga permisos
     * valido de escritura.
     *
     * @return string
     */
    public function create_custom_token()
    {
        $this->setResponseFormat('json');
        $token = \FirebaseClient::makeKey('general');

        return $this->jsonResponse(compact('token'), 200);
    }

    /**
     * Registra las novedades de los transportadores
     */
    public function register_news()
    {
        try{
            DB::beginTransaction();
            $transporter_new = new \TransporterNew;
            $transporter_new->type = Input::get('type');
            $transporter_new->reason = Input::get('new_reason');
            $transporter_new->reason_comments = Input::get('news_comments');
            $transporter_new->status = 'Sin Asignar';
            $transporter_new->order_id = Input::get('order_id');
            $transporter_new->vehicle_id = Input::get('vehicle_id');
            $transporter_new->url_file = Input::get('url');
            $transporter_new->save();

            // si la orden fue paga mediante PSE, el tipo de novedad es cancelación
            // y es por voluntad del cliente, entonces marcamos el pedido con tipo
            // de validación 7 (Pagados con PSE y cancelado por cliente)
            if (Input::get('type') === 'Cancelación' && starts_with(Input::get('new_reason'), 'Cliente -')) {
                \Order::whereId(Input::get('order_id'))
                    ->wherePaymentMethod('Débito - PSE')
                    ->update(['order_validation_reason_id' => 7]);
            }

            if(Input::get('type') == 'Productos'){
                foreach(Input::get('news_products') AS $news_product){
                    $type_product = $news_product['product']['type'];
                    $id_product = $news_product['product']['id'];
                    $transporter_new_detail = new \TransporterNewDetail;
                    $transporter_new_detail->product_type = $type_product;
                    $transporter_new_detail->order_product_id = $id_product;
                    $transporter_new_detail->transporter_new_id = $transporter_new->id;

                    if($type_product == 'Agrupado'){

                        $order_product = OrderProductGroup::find($transporter_new_detail->order_product_id);
                        $transporter_new_detail->reason = $news_product['reason'];
                        if(preg_match('/Devolución/i', $news_product['reason'] )){
                            if( $order_product->quantity == 0 ){
                                DB::rollback();
                                return $this->jsonResponse('No hay unidades para devolver de este producto.', 200);
                            }
                            if( $order_product->quantity < $news_product['number_returned'] ){
                                DB::rollback();
                                return $this->jsonResponse('La cantidad a devolver no puede ser mayor a la cantidad disponible.', 200);
                            }

                            $reason_returned = explode('-', $news_product['reason']);
                            $reason_returned = substr(trim($reason_returned[1]), 0, strlen(trim($reason_returned[1]))-1);
                            $reason_returned = ($reason_returned == 'Rechazado por el cliente')?'Rechazado cliente':$reason_returned;
                            $order_product->quantity_returned = ($order_product->quantity_returned>0)? $order_product->quantity_returned + $news_product['number_returned']: $news_product['number_returned'];
                            $order_product->reason_returned = $reason_returned;
                            if($order_product->quantity == $news_product['number_returned']){
                                $order_product->fulfilment_status = 'Returned';
                            }else if($order_product->quantity > $news_product['number_returned']){
                                $order_product->quantity = $order_product->quantity - $news_product['number_returned'];
                                //$order_product->quantity = ($order_product->quantity < 0)? 0 : $order_product->quantity;
                            }
                            $order_product->reference_returned = $order_product->reference;
                        }else{
                            $order_product->fulfilment_status = 'Not Available';
                        }
                        $order_product->save();
                        $transporter_new_detail->store_product_id = $order_product->store_product_id;
                        $transporter_new_detail->quantity_returned = ($transporter_new->quantity_returned>0)? $transporter_new->quantity_returned + $news_product['number_returned'] : $news_product['number_returned'];
                    }else{
                        $order_product = OrderProduct::find($transporter_new_detail->order_product_id);
                        $transporter_new_detail->reason = $news_product['reason'];
                        if(preg_match('/Devolución/i', $news_product['reason'])){
                            if( $order_product->quantity == 0 ){
                                DB::rollback();
                                return $this->jsonResponse('No hay unidades para devolver de este producto.', 200);
                            }
                            if( $order_product->quantity < $news_product['number_returned'] ){
                                DB::rollback();
                                return $this->jsonResponse('La cantidad a devolver no puede ser mayor a la cantidad disponible.', 200);
                            }
                            $reason_returned = explode('-', $news_product['reason']);
                            $reason_returned = substr(trim($reason_returned[1]), 0, strlen(trim($reason_returned[1]))-1);
                            $reason_returned = ($reason_returned == 'Rechazado por el cliente')?'Rechazado cliente':$reason_returned;
                            $order_product->quantity_returned = $news_product['number_returned'];
                            $order_product->reason_returned = $reason_returned;
                            if($order_product->quantity == $news_product['number_returned']){
                                $order_product->fulfilment_status = 'Returned';
                            }else if($order_product->quantity > $news_product['number_returned']){
                                $order_product->quantity = $order_product->quantity - $news_product['number_returned'];
                                //$order_product->quantity = ($order_product->quantity < 0)? 0 : $order_product->quantity;
                            }
                            $order_product->reference_returned = $order_product->reference;
                        }else{
                            $order_product->fulfilment_status = 'Not Available';
                        }
                        $transporter_new_detail->store_product_id = $order_product->store_product_id;
                        $transporter_new_detail->quantity_returned = ($transporter_new->quantity_returned > 0 )? $transporter_new->quantity_returned + $news_product['number_returned'] : $news_product['number_returned'];
                        $order_product->save();
                    }
                    $transporter_new_detail->save();
                }
            }

            $transporter_new->save();

            DB::commit();
            $this->response = array(
                'status' => true,
                'message' => 'La novedad se reporto con éxito.',
                'result' => []
            );

            return $this->jsonResponse();
        }catch(\Exception $e){
            DB::rollback();
            return $this->jsonResponse('Ocurrio un error al reportar la novedad error.'.$e->getMessage().' line::'.$e->getLine(), 200);

        }
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_response_news(){
        try{
            $transporter_news = \TransporterNew::select('transporter_news.*')
                ->join('orders', 'orders.id', '=', 'transporter_news.order_id')
                ->where('transporter_news.vehicle_id', Input::get('vehicle_id'))
                ->whereRaw("DATE(orders.delivery_date) >= '".date('Y-m-d')."'")
                ->where('transporter_news.status','Resuelto')
                ->get();
            $news = [];
            foreach($transporter_news as $key => $t_news){
                $prods = [];
                if(count($t_news->transporterNewDetail)){
                    foreach($t_news->transporterNewDetail as $detail){
                        $prods[] = [
                            'name' => $detail->orderProducts()->product_name.' '.$detail->orderProducts()->product_quantity.' '.$detail->orderProducts()->product_unit,
                            'reason' => $detail->reason,
                            'quantity' => $detail->quantity_returned
                        ];

                    }
                    $t_news->products = $prods;
                }
                $news[] = [
                    "id" => $t_news->id,
                    "admin_id" => $t_news->admin_id,
                    "vehicle_id" => $t_news->vehicle_id,
                    "order_id" => $t_news->order_id,
                    "order_product_id" => $t_news->order_product_id,
                    "store_product_id" => $t_news->store_product_id,
                    "quantity_returned" => $t_news->quantity_returned,
                    "reason" => $t_news->reason,
                    "reason_comments" => $t_news->reason_comments,
                    "resolution_message" => json_decode($t_news->resolution_message, true),
                    "type" => $t_news->type,
                    "product_type" => $t_news->product_type,
                    "status" => $t_news->status,
                    "resolve_date" => $t_news->resolve_date,
                    "products" => $prods,
                    "created_at" => $t_news->created_at,
                    "updated_at" => $t_news->updated_at
                ];
            }
            $transporter_news = $news;
            if(count($transporter_news)>0){
                $this->response = array(
                    'status' => true,
                    'message' => 'Notificaciones resueltas',
                    'result' => array(
                        'news' => $transporter_news
                    )
                );
            }else{
                $this->response = array(
                    'status' => false,
                    'message' => 'No hay notificaciones de novedades',
                    'result' =>[ 'news' => []]

                );
            }
            return $this->jsonResponse();

        }catch(\Exception $e){
            return $this->jsonResponse('Ocurrio un error al consultar la novedad.'.$e->getMessage(), 400);
        }
    }

    /**
     * Subir imagen de referencia de las novedades
     */
    function upload_reference_image()
    {
        $file = Input::file('image');
        $file_data['real_path'] = $file->getRealPath();
        $file_data['client_original_name'] = $file->getClientOriginalName();
        $file_data['client_original_extension'] = $file->getClientOriginalExtension();

        $dir = 'news/reference/'.date('Y-m-d');
        $url = upload_image($file, false, $dir);

        if ($url)
        {
            return Response::json(array('status' => true, 'message' => 'La imagen fue subida con éxito.', 'url' => $url) , 200);
        }
        return Response::json(array('status' => false, 'message' => 'Ocurrió un error al subir la imagen.'.$url) , 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function save_pickup_products()
    {
        try {
            DB::beginTransaction();
            //$order = \Order::find(Input::get('order_id'));
            $products = Input::get('products');
            foreach ($products as $product) {
                $order_product = \OrderProduct::find($product['id']);
                $order_product->quantity_returned = $product['number_returned'];
                $order_product->quantity_original = $order_product->quantity;
                if ($order_product->quantity < $product['number_returned']) {
                    $order_product->quantity = $order_product->quantity - $product['number_returned'];
                }
                $order_product->fulfilment_status = 'Returned';
                $order_product->reason_returned = $product['reason'];
                $order_product->save();
            }
            return Response::json(array(
                'status' => true,
                'message' => 'Se registró con éxito los productos a recoger.'
            ), 200);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return Response::json(array('status' => false, 'message' => 'Ocurrio un error al '), 200);
        }

    }

    /**
     * validar que los codigos de las canastillas pertenezcan a la ruta
     * @return \Illuminate\Http\JsonResponse
     */
    public function validate_baskets_transporter()
    {

        $baskets_transporter = Input::get('baskets');
        $vehicle_id = Input::get('vehicle_id');


        $orders = Order::whereNotNull('route_id')
            ->where('vehicle_id', $vehicle_id)
            ->where('status', 'Dispatched')
            ->where(DB::raw('DATE(delivery_date)'), \Carbon\Carbon::now()->toDateString())
            ->get();

        if (!count($orders)) {
            return Response::json(array(
                'status' => '',
                'message' => 'No contiene pedidos despachados para el día de hoy.',
                'result' => ''
            ), 200);
        }

        $route = Routes::find($orders[0]->route_id);
        $orders_ids = [];
        foreach ($orders as $data) {
            $orders_ids[] = $data->id;
        }

        $order_baskets = OrderPickingBasket::whereIn('order_id', $orders_ids)
            ->where('type', 'Seco')
            ->where('missing', '<>', 1)
            ->select('code_basket', 'order_id', (
            DB::raw(isset($route->route) ? "'" . $route->route . "' as route" : "'Ruta' as route")))
            ->get()->toArray();
        $validated_baskets = [];
        $success = true;

        foreach ($baskets_transporter as $data) {
            $value = isset($data->code_basket) ? $data->code_basket : $data["code_basket"];
            $index = OrderPickingBasket::searchBasketCode($order_baskets, $value);
            if (is_numeric($index)) {
                $validated_baskets[] = [
                    "code_basket" => $value,
                    "value" => true
                ];
                array_splice($order_baskets, $index, 1);
            } else {
                $success = false;
                $validated_baskets[] = OrderPickingBasket::getInfoBasket($value);
            }
        }

        if (count($order_baskets)) {
            $success = false;
        }

        if ($success) {
            $this->update_route_transporter($orders[0]->route_id, $orders_ids);
        }

        return Response::json(array(
            'status' => $success,
            'message' => 'Validaciones.',
            'result' => ['validated' => $validated_baskets, 'missing' => $order_baskets]
        ), 200);

    }

    /**
     * actualiza las ordenes y la ruta del check transporter
     * @param $route_id
     * @param $orders_ids
     */
    public function update_route_transporter($route_id, $orders_ids)
    {
        OrderPickingBasket::whereIn('order_id', $orders_ids)
            ->where('type', 'Seco')
            ->where('missing', '<>', 1)
            ->update(['check_transporter_date' =>  Carbon::now(),'validated' =>  1]);
    }


    /**
     * Trae la ruta del vehiculo
     * @param $vehicle_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function status_route_transporter($vehicle_id)
    {
        $orders = Order::join('order_picking_baskets', 'order_picking_baskets.order_id', '=', 'orders.id')
            ->where('orders.vehicle_id', $vehicle_id)
            ->whereNotNull('orders.route_id')
            ->whereNull('order_picking_baskets.check_transporter_date')
            ->where('order_picking_baskets.missing', '<>', 1)
            ->where('orders.status', 'Dispatched')
            ->where(DB::raw('DATE(orders.delivery_date)'), Carbon::now()->toDateString())
            ->count();

        return Response::json(array(
            'status' => true,
            'message' => 'Ruta.',
            'result' => $orders
        ), 200);
    }

    /**
     * Guarda las canastillas faltantes y actualiza las canastillas que no
     * se guardaron en picking
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeReportRouteDispatch()
    {
        $failBaskets = [];
        $addBaskets = [];
        if (Input::has('missing')) {
            foreach (Input::get('missing') as $data) {
                $failBaskets[][$data["code_basket"]] = OrderPickingBasket::where('code_basket', $data["code_basket"])
                    ->where('order_id', $data["order_id"])
                    ->update(['missing' => 1]);
            }
        }

        if (Input::has('order_baskets')) {
            foreach (Input::get('order_baskets') as $data) {
                $orderPickingDaskets = new OrderPickingBasket();
                $orderPickingDaskets->order_id = $data["order_id"];
                $orderPickingDaskets->type = 'Seco';
                $orderPickingDaskets->code_basket = $data["code_basket"];
                $orderPickingDaskets->validated = 1;
                $orderPickingDaskets->check_transporter_date = null;
                $orderPickingDaskets->added = 1;
                $orderPickingDaskets->save();
                $addBaskets[] = $orderPickingDaskets;
            }
        }

        return Response::json(array(
            'status' => true,
            'message' => 'Reporte.',
            'result' => ['missing' => $failBaskets, 'add' => $addBaskets]
        ), 200);
    }


    /**
     * Registra el estado del producto en la llegada del transportador
     */
    public function registerStatusProductTransporter()
    {
        try {
            DB::beginTransaction();
            foreach (Input::get('news_products') AS $news_product) {
                $type_product = $news_product['type'];
                $id_product = $news_product['id'];
                $status = $news_product['status'];

                if ($type_product == 'Agrupado') {
                    OrderProductGroup::where('id', $id_product)
                        ->update(['product_delivered' => $status]);
                } else {
                    OrderProduct::where('id', $id_product)
                        ->update(['product_delivered' => $status]);
                }
            }

            DB::commit();
            $this->response = array(
                'status' => true,
                'message' => 'La novedad se reporto con éxito.',
                'result' => []
            );

            return $this->jsonResponse();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->jsonResponse('Ocurrio un error al reportar la novedad error.' . $e->getMessage() . ' line::' . $e->getLine(),
                200);

        }
    }
}
