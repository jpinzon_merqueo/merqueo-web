<?php

namespace api\transporters\v1;

use Carbon\Carbon;
use Input, Route, Config, DB, Order, OrderGroup, OrderProduct, OrderLog, Product, Response, TransporterNew,
Vehicle, Session, VehicleMovement, OrderReturn, OrderProductGroup, Event;
use usecases\contracts\orders\AddCreditWhenDeliveryLateInterface;

class ApiOrderController extends \ApiController
{
    /**
     * @var AddCreditWhenDeliveryLateInterface
     */
    private $addCreditWhenDeliveryLate;

    public function __construct(AddCreditWhenDeliveryLateInterface $addCreditWhenDeliveryLate)
    {
        $this->beforeFilter(function() {
            if(!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
        }, array('except' => array('login')));
        parent::__construct();
        $this->addCreditWhenDeliveryLate = $addCreditWhenDeliveryLate;
    }

    /**
     * Actualizar estado de pedido
     *
     * @return array $response Respuesta
     */
    public function update_order_status()
    {
        if (!Input::has('vehicle_id') || !Input::has('status') || !Input::has('order_id'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        $id = Input::get('order_id');
        
        if (!$order = Order::find($id))
            return $this->jsonResponse('ID pedido no existe.', 400);

        if (!$vehicle = Vehicle::find(Input::get('vehicle_id')))
            return $this->jsonResponse('ID de vehículo no existe.', 400);

        if ($order->status == Input::get('status'))
            return $this->jsonResponse('El pedido ya fue actualizado al estado "' . $this->config['order_statuses'][Input::get('status')] . '", por favor cierra el App y vuelve a ingresar para actualizar la información.');

        if ($order->status == 'Cancelled')
          return $this->jsonResponse('No puedes actualizar este pedido por que fue cancelado por favor cierra el App y vuelve a ingresar para actualizar la información.');

        $old_order_status = $order->status;
        $new_status = Input::get('status');
        $order_group = OrderGroup::find($order->group_id);

        if ($new_status == 'En Camino')
        {
            $date = date('Y-m-d');
            //validar cantidad maxima de pedidos pendientes al tiempo
            $maximum_orders_pending = 3; //Config::get('app.transporters.maximum_orders_pending');
            $pending_orders = Order::where('vehicle_id', $vehicle->id)->whereRaw("DATE(delivery_date) = '$date'")->where('status', 'Dispatched')->whereNotNull('onmyway_date')->count();
            if ($pending_orders >= $maximum_orders_pending){
                return $this->jsonResponse('Puedes tener solo '.$maximum_orders_pending.' pedidos pendientes al tiempo.');
            }

            $maximum_orders_pending_with_news = 2; //Config::get('app.maximum_orders_pending_with_news');
            $orders_with_news = Order::where('orders.vehicle_id', $vehicle->id)
                ->whereRaw("DATE(delivery_date) = '$date'")
                ->where('orders.status', 'Dispatched')
                ->whereNotNull('orders.onmyway_date')
                ->join('transporter_news', 'transporter_news.order_id', '=', 'orders.id')
                ->whereIn('transporter_news.status', ['Sin Asignar', 'Pendiente'])
                ->groupBy('orders.id')
                ->count();

            if ($orders_with_news >= $maximum_orders_pending_with_news){
                return $this->jsonResponse('Puedes tener solo '.$maximum_orders_pending_with_news.' pedidos sin entregar y con novedades pendientes al tiempo.');
            }

            //validar secuencia de pedido
            if ($vehicle->validate_sequence){
                $next_sequence_order = Order::where('vehicle_id', $vehicle->id)->whereRaw("DATE(delivery_date) = '$date'")->where('status', 'Dispatched')->whereNull('onmyway_date')->min('planning_sequence');
                if ($next_sequence_order && $next_sequence_order != $order->planning_sequence){
                    $vehicle->addAlert(0, 'Pendiente');
                    return $this->jsonResponse('No puedes tomar este pedido por que el siguiente a entregar es el de la secuencia '.$next_sequence_order.'.');
                }
            }

            //validar hora de entrega de pedido
            $delivery_date = date('Y-m-d H:i:s', strtotime($order->delivery_date.' -330 minutes'));
            if ($delivery_date > date('Y-m-d H:i:s') && !$order->allow_early_delivery && !$vehicle->allow_early_delivery)
                return $this->jsonResponse('Para entregar este pedido debes esperar hasta 30 minutos antes del horario de entrega.');

            $order->onmyway_date = date('Y-m-d H:i:s');
            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'Conductor se dirige a entregar el pedido.';
            $log->driver_id = $order->driver_id;
            $log->order_id = $order->id;
            $log->save();

            $order = Order::select('orders.*', 'user_firstname', 'user_lastname', 'user_email', 'user_phone', 'delivery_time',
                                'user_address', 'user_address_further', 'source', 'device_player_id', 'user_device_id', 'source_os', 'app_version')
                                ->join('order_groups', 'order_groups.id', '=', 'group_id')
                                ->where('orders.id', $order->id)
                                ->first();
            Event::fire('order.onmyway', [[$order]]);
            $order_data = Order::getStatusFlow($order->id, $order->user_id, false, true);

            $this->response = array(
                'status' => true,
                'message' => 'Fecha pedido en camino actualizada.',
                'result' => [
                    'user_address' => "{$order->user_address} {$order->user_address_further}",
                    'order_data' => empty($order_data['result']) ? null : $order_data['result'],
                    'next_orders' => empty($order_data['next_orders']) ? null : $order_data['next_orders'],
                ]
            );
            return $this->jsonResponse();
        }

        if ($new_status == 'He llegado')
        {
            $order->arrived_date = date('Y-m-d H:i:s');
            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'Conductor llego a la ubicación del cliente.';
            $log->driver_id = $order->driver_id;
            $log->order_id = $order->id;
            $log->save();

            $qty_orders = Order::where('user_id', $order->user_id)->where('status', 'Delivered')->count();

            Event::fire('order.arrived', [[$order, $order_group]]);
            $order_data = Order::getStatusFlow($order->id, $order->user_id, false, true);

            $this->response = array(
                'status' => true,
                'message' => 'Fecha pedido ha llegado actualizada.',
                'result' => [
                    'payment_method' => $order->payment_method,
                    'total_products' => $order->total_products,
                    'total_amount' => number_format($order->total_amount, 0, ',', '.'),
                    'delivery_amount' => number_format($order->delivery_amount, 0, ',', '.'),
                    'discount_amount' => number_format($order->discount_amount, 0, ',', '.'),
                    'grand_total' => number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.'),
                    'user_firstname' => $order_group->user_firstname,
                    'user_lastname' => $order_group->user_lastname,
                    'user_address' => $order_group->user_address.' '.$order_group->user_address_further,
                    'user_phone' => $order_group->user_phone,
                    'qty_orders' => $qty_orders,
                    'order_data' => empty($order_data['result']) ? null : $order_data['result'],
                    'next_orders' => empty($order_data['next_orders']) ? null : $order_data['next_orders'],
                ]
            );
            return $this->jsonResponse();
        }

        if ($new_status == 'Delivered')
        {
            $order_products_pick_up = OrderProduct::where('type', 'Recogida')->where('order_id', $order->id)->get();
            
            if(!empty($order_products_pick_up)){
                $order_products_pick_up = OrderProduct::where('type', 'Recogida')->where('order_id', $order->id)->where('fulfilment_status', 'Pending')->count();
                if($order_products_pick_up > 0){
                    return $this->jsonResponse('Hay productos pendientes por ser recogidos, favor validar en la lista de productos.');
                }
            }

            if (in_array(Input::get('customer_payment_method'), ['Datáfono', 'Efectivo y datáfono']) && empty($order->voucher_image_url))
                return $this->jsonResponse('La foto del voucher del pedido es requerida.');

            if ($order->payment_method != 'Tarjeta de crédito'){
                $customer_payment_method = Input::get('customer_payment_method');
                if (empty($customer_payment_method))
                    return $this->jsonResponse('El método de pago es requerido, por favor recarga el app.');

                if ($order->payment_method != Input::get('customer_payment_method')){
                    $log = new OrderLog();
                    $log->type = 'Método de pago actualizado: '.$order->payment_method.' a '.$customer_payment_method;
                    $log->driver_id = $order->driver_id;
                    $log->order_id = $order->id;
                    $log->save();
                    $order->payment_method = $customer_payment_method;
                }
                $order->payment_date = date('Y-m-d H:i:s');
                $order->updateTotals();
            }else{
                $payment = $order->getPayments(true);
                if (!$payment || ($payment && !empty($payment->cc_refund_date)))
                    return $this->jsonResponse('El método de pago del cliente es con tarjeta de crédito y no se ha cobrado.');
            }
            $order->management_date = date('Y-m-d H:i:s');

            //registrar movimiento de conductor
            if (VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('status', 1)->first()){
                $movement = VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('order_id', $order->id)->where('status', 1)->first();

                if(empty($movement))
                    $movement = new VehicleMovement;

                $movement->order_id = $order->id;
                $movement->vehicle_id = $order->vehicle_id;
                $movement->driver_id = $order->driver_id;
                $movement->movement = 'Gestión de pedido';
                $movement->description = 'Movimiento registrado por el conductor con el id:'.$order->driver_id;
                $movement->payment_method = $order->payment_method;
                $total_order = $order->total_amount + $order->delivery_amount - $order->discount_amount;

                if ($order->payment_method == 'Efectivo y datáfono'){
                    $movement->user_card_paid = Input::get('customer_card_paid');
                    if ($total_order != (Input::get('customer_cash_paid') + Input::get('customer_card_paid')))
                          return $this->jsonResponse('El total pagado en efectivo y con datáfono no coincide con el total del pedido/factura.');
                }

                if ($order->payment_method == 'Datáfono'){
                    $movement->user_card_paid = $total_order;
                    $movement->user_cash_paid = 0;
                }

                if ($order->payment_method == 'Tarjeta de crédito'){
                    $movement->user_card_paid = $total_order;
                    $movement->user_cash_paid = 0;
                }

                VehicleMovement::saveMovement($movement, $order);
            }

            //eliminar devolucion a bodega si aplica
            OrderReturn::deleteOrderReturn($order->id, $new_status);
            //registrar devolucion a bodega si aplica
            OrderReturn::saveOrderReturn($order->id, $old_order_status);

            $order->updateInvestigationStockBack($order->status);

            // Genera consecutivo de factura
            $order->generateConsecutive();
            $this->addCreditWhenDeliveryLate->handle($order, Carbon::now()->addYear());
        }if ($new_status == 'Cancelled') {
            if (!Input::has('reject_reason'))
                return $this->jsonResponse('Debes seleccionar un motivo para la cancelación del pedido.', 400);

            $order->reject_reason = Input::get('reject_reason');
            $order->reject_comments = Input::get('reject_comments');
            $order->driver_cancel_date = date('Y-m-d H:i:s');

            $transporter_new = new TransporterNew;
            $transporter_new->reason = Input::get('reject_reason');
            $transporter_new->reason_comments = Input::get('reject_comments');
            $transporter_new->type = 'Cancelación';
            $transporter_new->status = 'Sin Asignar';
            $transporter_new->order_id = $order->id;
            $transporter_new->vehicle_id = $order->vehicle_id;
            $transporter_new->save();
            /*if ($order->reject_reason == 'Reprogramación de pedido de bodega')
                $order->scheduled_delivery = 1;
            //actualizar estado de productos
            OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status', ['Fullfilled', 'Returned'])->update(array('fulfilment_status' => 'Returned'));
            $order->product_return_storage = 1;

            //registrar devolucion a bodega
            $result = OrderReturn::saveOrderReturn($order->id, $old_order_status);
            if (!$result['status'])
                return $this->jsonResponse($result['message']);*/
        }else $order->status = $new_status;

        $order->save();

        Event::fire('order.status_log', [[$order, $order->driver_id, 'driver']]);
        \Log::info('Modificando estado de pedido: '.$order->id. ' a '.$order->status.' desde app transporte');

        if ($order->status == 'Delivered'){
            $order->validateReferred();
            $vehicle->onDeliveredOrder($order);
        }

        if ($order->status == 'Delivered' || $order->status == 'Cancelled'){
            $order_group = OrderGroup::find($order->group_id);
            Event::fire('order.managed', [[$order, $order_group]]);
        }

        $order_data = Order::getStatusFlow($order->id, $order->user_id, false, true);

        $this->response = array(
            'status' => true,
            'message' => 'Estado de pedido actualizado.',
            'result' => array(
                'driver_balance' => Vehicle::getBalance($vehicle->id),
                'total_amount' => $order->total_amount,
                'discount_amount' => $order->discount_amount,
                'delivery_amount' => $order->delivery_amount,
                'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount,
                'order_data' => empty($order_data['result']) ? null : $order_data['result'],
                'next_orders' => empty($order_data['next_orders']) ? null : $order_data['next_orders'],
            )
        );

        return $this->jsonResponse();
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    function get_products(){
        $order_id = Input::get('order_id');
        $products = OrderProduct::select(
            'order_products.id', 'order_products.store_id', 'order_products.order_id', 'order_products.store_product_id', 'order_products.sampling_id', 'order_products.price', 'order_products.original_price',
            'order_products.base_price', 'order_products.quantity', 'order_products.quantity_returned', 'order_products.quantity_special_price_stock', 'order_products.reason_returned', 'order_products.reference_returned',
            'order_products.fulfilment_status', 'order_products.type', 'order_products.is_gift', 'order_products.parent_id', 'order_products.reference', 'order_products.product_name', 'order_products.product_quantity',
            'order_products.product_unit', 'order_products.product_image_url', 'order_products.product_delivery_discount_amount',
            DB::raw("IF(order_products.fulfilment_status = 'Not Available',0,order_products.quantity) AS quantity"),
            'products.reference', 'products.description', 'products.image_large_url', 'store_products.storage', 'orders.arrived_date'
        )
            ->join('orders', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
            ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
            ->orderBy('department_id', 'desc')
            ->orderBy('product_name', 'asc')
            ->where('order_id', $order_id)
            ->whereIn('order_products.type', ['Product','Muestra', 'Recogida'])
            ->get();

        $order_products = $products->toArray();
        $product_group = OrderProductGroup::select(
        //'order_products.*',
            'order_products.store_id', 'order_products.order_id', 'order_products.store_product_id', 'order_products.sampling_id', 'order_products.price', 'order_products.original_price',
            'order_products.base_price', 'order_products.quantity', 'order_products.quantity_returned', 'order_products.quantity_special_price_stock', 'order_products.reason_returned', 'order_products.reference_returned',
            'order_products.fulfilment_status', 'order_products.type', 'order_products.is_gift', 'order_products.parent_id', 'order_products.reference', 'order_products.product_name', 'order_products.product_quantity',
            'order_products.product_unit', 'order_products.product_image_url', 'order_products.product_delivery_discount_amount',
            DB::raw("IF(order_products.fulfilment_status = 'Not Available',0,order_products.quantity) AS quantity"),
            'order_product_group.*',
            'store_products.storage',
            'orders.arrived_date'
        )
            ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
            ->join('orders', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
            ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
            ->where('order_product_group.order_id', $order_id)
            ->get();

        if ($product_group){
            $order_products =array_merge($order_products, $product_group->toArray() );
        }
        $this->response = array(
            'status' => true,
            'message' => 'Productos obtenidos',
            'result' => array(
                'products' => $order_products
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Subir imagen de factura y voucher
     */
    function upload_order_image()
    {
        if (!$order_id = Input::get('order_id'))
            return $this->jsonResponse('ID pedido es requerido.', 400);

        if (!Input::has('vehicle_id') || !Input::hasFile('image'))
            return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

        if (!$order = Order::where('id', $order_id)->where('vehicle_id', Input::get('vehicle_id'))->first())
            return $this->jsonResponse('ID pedido no asignado a un vehiculo o no existe.', 400);

        if ($order->status != 'Dispatched'){
            $this->response['message'] = 'No se puede subir la imagen por que el estado actual del pedido no es En Camino.';
            return $this->jsonResponse();
        }

        $vehicle = Vehicle::find(Input::get('vehicle_id'));
        $file = Input::file('image');
        $file_data['real_path'] = $file->getRealPath();
        $file_data['client_original_name'] = $file->getClientOriginalName();
        $file_data['client_original_extension'] = $file->getClientOriginalExtension();

        $dir = 'app/shoppers/vouchers/'.date('Y-m-d');
        $url = upload_image($file, false, $dir);

        if ($url)
        {
            $cloudfront_url = Config::get('app.aws.cloudfront_url');
            if ($order = Order::find($order_id))
            {
                if (!empty($order->voucher_image_url))
                {
                    $path = str_replace($cloudfront_url, uploads_path(), $order->voucher_image_url);
                    remove_file($path);
                    remove_file_s3($order->voucher_image_url);
                }
                $order->voucher_image_url = $url;
                $order->save();
            }

            //log de pedido
            $log = new OrderLog();
            $log->type = 'Imagen de voucher subida.';
            $log->driver_id = $order->driver_id;
            $log->order_id = $order_id;
            $log->save();

            return Response::json(array('status' => true, 'message' => 'La imagen fue subida con éxito.', 'url' => $url) , 200);
        }
        return Response::json(array('status' => false, 'message' => 'Ocurrió un error al subir la imagen.') , 200);
    }

    /**
     * Actualiza el estado de un producto
     */
    public function update_product_status()
    {
        if(!Input::has('order_id'))
            return $this->jsonResponse('El id del pedido es obligatorio.', 400);

        if (!$order = Order::find(Input::get('order_id')))
            return $this->jsonResponse('El pedido no existe.', 400);

        if (!Input::has('order_product_id') || !Input::has('fulfilment_status'))
            return $this->jsonResponse('La información del producto esta vacia.', 400);


        if(!$order_product = OrderProduct::find(Input::get('order_product_id')))
            return $this->jsonResponse('El producto no existe.', 400);

        if (!$order = Order::find($order_product->order_id))
            return $this->jsonResponse('El pedido no existe.', 400);

        if ($order->status != 'Dispatched')
           return $this->jsonResponse('El pedido no esta en un estado valido.');

        if (Input::get('fulfilment_status') == 'Not Available'){
            return $this->jsonResponse('Se genero la petición de eliminación de producto.');
        }

        //actualizar estado de producto
        if ($order_product){
            if ($order_product->fulfilment_status == 'Fullfilled' && Input::get('fulfilment_status') == 'Not Available'){
                $order_product->update_stock_back = 1;
            }elseif($order_product->type == 'Recogida'){
                $order_product->update_stock_back = 1;
            }else{
                $order_product->update_stock_back = 0;
            }
            $order_product->fulfilment_status = Input::get('fulfilment_status');
            $order_product->save();

            Event::fire('order.product_log', [[$order_product, $order->driver_id, 'driver']]);
        }

        $order->updateTotals();
        $order->save();

        Event::fire('order.total_log', [[$order, $order->driver_id, 'driver']]);

        $this->response = array(
            'status' => true,
            'message' => 'Estado de pedido actualizado.',
            'result' => array(
                'total_amount' => $order->total_amount,
                'discount_amount' => $order->discount_amount,
                'delivery_amount' => $order->delivery_amount,
                'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Actualiza producto con devolución total parcial
     */
    public function update_product_status_returns()
    {
        if(!Input::has('order_id'))
            return $this->jsonResponse('El id del pedido es obligatorio.', 400);

        if (!$order = Order::find(Input::get('order_id')))
            return $this->jsonResponse('El pedido no existe.', 400);

        if (!Input::has('order_product_id'))
           return $this->jsonResponse('La información del producto esta vacia.', 400);

        if(!$order_product = OrderProduct::find(Input::get('order_product_id')))
            return $this->jsonResponse('El producto no existe.', 400);

        if (!$order = Order::find($order_product->order_id))
            return $this->jsonResponse('El pedido no existe.', 400);

        if ($order->status != 'Dispatched')
           return $this->jsonResponse('El pedido no esta en un estado valido. OP_OID:'.$order->status.'-'.$order->id);

        if (Input::get('type') == 'Agrupado'){
            $order_product_group = OrderProductGroup::where('id', Input::get('order_product_id'))->where('store_product_id', Input::get('product_id'))->first();

            if(Input::has('quantity') && $order_product_group->quantity < Input::get('quantity'))
                return $this->jsonResponse('La cantidad de unidades del producto a devolver es mayor a la registrada en el pedido.');

            $order_product_group->update_stock_back = 0;
            $order_product_group->fulfilment_status = Input::has('quantity') && Input::get('quantity') == $order_product_group->quantity ? 'Returned' : 'Fullfilled';

            if(Input::has('quantity') && $order_product_group->fulfilment_status != 'Returned')
                $order_product_group->quantity = $order_product_group->quantity - Input::get('quantity');
            elseif($order_product_group->quantity_returned)
                $order_product_group->quantity = $order_product_group->quantity_returned + $order_product_group->quantity;

            if(Input::has('quantity') && $order_product_group->fulfilment_status != 'Returned'){
                $order_product_group->quantity_returned = $order_product_group->quantity_returned + Input::get('quantity');
            }else
                $order_product_group->quantity_returned = NULL;

            $order_product_group->reason_returned = Input::get('reason_returned');

            if(Input::has('reference_returned') && Input::get('reason_returned') == 'Errado'){
                $order_product_group->reference_returned = Input::get('reference_returned');
                $product = Product::join('store_products', 'products.id', '=', 'product_id')
                                            ->where('reference', Input::get('reference_returned'))
                                            ->where('store_id', $order->store_id)
                                            ->first();
                if (!$product)
                    return $this->jsonResponse('La referencia del producto ingresado no existe.');
            }

            $order_product_group->save();
            Event::fire('order.product_log', [[$order_product_group, $order->driver_id, 'driver']]);

            $fulfilment_status = $order_product_group->fulfilment_status;
            $product_quantity =  $order_product_group->quantity;
            OrderProductGroup::updateProductStatus($order_product_group->order_product_id);
        }else{
            $order_product = OrderProduct::where('id', Input::get('order_product_id'))->where('store_product_id', Input::get('product_id'))->first();

            if(Input::has('quantity') && $order_product->quantity < Input::get('quantity'))
                 return $this->jsonResponse('La cantidad de unidades del producto a devolver es mayor a la registrada en el pedido.');

            $order_product->update_stock_back = 0;
            $order_product->fulfilment_status = Input::has('quantity') && Input::get('quantity') == $order_product->quantity ? 'Returned' : 'Fullfilled';

            if(Input::has('quantity') && $order_product->fulfilment_status != 'Returned')
                $order_product->quantity = $order_product->quantity - Input::get('quantity');
            elseif($order_product->quantity_returned)
                $order_product->quantity = $order_product->quantity_returned + $order_product->quantity;

            if(Input::has('quantity') && $order_product->fulfilment_status != 'Returned'){
                $order_product->quantity_returned = $order_product->quantity_returned + Input::get('quantity');
            }else
                $order_product->quantity_returned = Null;

            $order_product->reason_returned = Input::get('reason_returned');
            if(Input::has('reference_returned') && Input::get('reason_returned') == 'Errado'){
                $order_product->reference_returned = Input::get('reference_returned');
                $product = Product::join('store_products', 'products.id', '=', 'product_id')
                                            ->where('reference', Input::get('reference_returned'))
                                            ->where('store_id', $order->store_id)
                                            ->first();
                if (!$product)
                    return $this->jsonResponse('La referencia del producto ingresado no existe.');
            }


            $order_product->save();
            Event::fire('order.product_log', [[$order_product, $order->driver_id, 'driver']]);

            $fulfilment_status = $order_product->fulfilment_status;
            $product_quantity =  $order_product->quantity;
        }

        $order->updateTotals();
        $order->save();


        Event::fire('order.total_log', [[$order, $order->driver_id, 'driver']]);
        $this->response = array(
            'status' => true,
            'message' => 'Estado de pedido actualizado.',
            'result' => array(
                'product_quantity' =>$product_quantity,
                'fulfilment_status' => $fulfilment_status,
                'total_amount' => $order->total_amount,
                'discount_amount' => $order->discount_amount,
                'delivery_amount' => $order->delivery_amount,
                'total' => $order->total_amount + $order->delivery_amount - $order->discount_amount
            )
        );

        return $this->jsonResponse();
    }
}
