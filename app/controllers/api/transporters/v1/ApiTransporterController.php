<?php

namespace api\transporters\v1;

use Input, Hash, Config, DB, Route, Vehicle, Order, OrderProduct, Admin, ArrayObject, Carbon\Carbon, Session, Response, Driver, View, RejectReason,
    Auth, VehicleMovement, Product, OrderProductGroup, Routes, Zone, Pusherer;

class ApiTransporterController extends \ApiController
{

    public function __construct()
    {
        $this->beforeFilter(function() {
            if(!Session::get('user_logged_in'))
                return $this->jsonResponse('Acceso denegado.', 401);
        }, array('except' => array('login', 'create_custom_token')));
        parent::__construct();
    }

    /**
     * Realiza login de transportador
     *
     * @return array $response Respuesta
     */ public function login()
{
    $plate =  Input::get('plate');
    $password =  Input::get('password');

    if (empty($plate) || empty($password))
        return $this->jsonResponse('Hay parametros obligatorios vacios.', 400);

    if ($vehicle = Vehicle::where('plate', $plate)->where('status', 1)->first())
    {
        //SERVICIO VERSION
        if (Input::has('app_version'))
        {
            $update = 'not_required'; //not_required, required, suggested
            $app_version = intval(str_replace('.', '', Input::get('app_version')));
            if ($app_version < 120  || !$vehicle->old_version)
                $update = 'required';
            if ($update == 'required'){
                $message = 'Hemos realizado cambios en el app, por favor actualiza a la última versión para continuar.';
                $url = 'https://drive.google.com/file/d/1Fm3f9Vphfar7ObU_94cO7tMVXyHOBkuh/view?usp=sharing';
                return Response::json(array('status' => true, 'update_app' => array('message' => $message, 'url' => $url)) , 200);
            }
        }
        if (Hash::check($password, $vehicle->password))
        {
            if ($vehicle->status)
            {
                Session::put('vehicle_id', $vehicle->id);
                Session::put('vehicle_plate', $vehicle->plate);
                Session::put('user_logged_in', true);

                $vehicle_drivers = \VehicleDrivers::where('vehicle_id',$vehicle->id)->first();
                $driver = Driver::find($vehicle_drivers->driver_id);

                if (!$vehicle->terms){
                    $view = View::make('shopper.terms');
                    $vehicle->terms = $view->render();
                }

                $vehicle = array(
                    'id' => $vehicle->id,
                    'plate' => $vehicle->plate,
                    'driver' => $driver->first_name.' '.$driver->last_name,
                    'terms' => $vehicle->terms
                );

                $this->response = array(
                    'status' => true,
                    'message' => 'Sesión iniciada',
                    'result' => array('vehicle' => $vehicle, 'balance' => Vehicle::getBalance($vehicle['id']))
                );
            }else $this->response['message'] = 'Debes seleccionar un shopper con perfil de Alistador Frío de la lista de abajo.';
        }else $this->response['message'] = 'Contraseña incorrecta.';
    }else $this->response['message'] = 'Placa incorrecta.';

    return $this->jsonResponse();
}

    /**
     * Cierra sesion
     */
    public function logout()
    {
        if (!Input::has('vehicle_id')){
            $this->response = array(
                'status' => true,
                'message' => 'ID vehiculo es requerido'
            );
            return Response::json( $this->response, 400 );
        }
        Session::flush();

        $this->response = array(
            'status' => true,
            'message' => 'Sesión cerrada'
        );

        return $this->jsonResponse();
    }

    /**
     * Acepta terminos y condiciones
     */
    public function terms($id)
    {
        if (!$id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID vehiculo es requerido.', 400);

        if (Input::has('terms') && $vehicle = Vehicle::where('id', $id)->first()){
            $vehicle->terms = 1;
            $vehicle->save();

            $this->response = array(
                'status' => true,
                'message' => 'Terminos actualizados.'
            );
        }else return $this->jsonResponse('Ocurrió un error al actualizar los datos.');

        return $this->jsonResponse();
    }

    /**
     * Obtener pedidos en proceso y alistados asignados al transportador
     *
     * @return array $response Respuesta
     */
    public function enlisted_orders($id)
    {
        if (!is_numeric($id))
            return $this->jsonResponse('ID transportador debe ser numérico.', 400);

        if (!$vehicle = Vehicle::find($id))
            return $this->jsonResponse('ID vehiculo no existe.', 400);

        $pending_orders = array();

        //pedidos pendientes
        $orders = Order::select(
            'orders.id',
            DB::raw("IF(orders.status = 'Dispatched' AND orders.driver_cancel_date IS NULL AND orders.onmyway_date IS NOT NULL, 'En Camino', IF(orders.status = 'Dispatched' AND orders.driver_cancel_date IS NOT NULL, 'Cancelled', orders.status)) AS status"),
            'orders.total_products',
            'orders.delivery_amount',
            'orders.discount_amount',
            'orders.total_amount',
            'orders.reject_reason',
            DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'),
            'orders.onmyway_date',
            'orders.arrived_date',
            'orders.management_date',
            'orders.picking_baskets',
            'orders.picking_bags',
            'orders.planning_sequence',
            'orders.route_id',
            'routes.route AS planning_route',
            'order_groups.user_firstname',
            'order_groups.user_lastname',
            'order_groups.user_phone',
            DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
            'order_groups.user_address_neighborhood',
            'cities.city AS user_city',
            'order_groups.user_address_latitude',
            'order_groups.user_address_longitude',
            'orders.delivery_time',
            'payment_method',
            'orders.cc_charge_id',
            'order_groups.user_comments AS user_comments',
            'orders.delivery_date',
            DB::raw('DATE(delivery_date) AS delivery_date_day'),
            DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.user_id <> 5169 AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
            ->join('users','orders.user_id','=','users.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('cities', 'cities.id', '=', 'user_city_id')
            ->join('routes', 'routes.id', '=', 'route_id')
            //->where('orders.type', 'Merqueo')
            ->where('orders.vehicle_id', $vehicle->id)
            ->whereIn('orders.status', ['Dispatched', 'Delivered', 'Cancelled'])
            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime('+1 days')))
            ->orderBy('orders.status', 'DESC')
            ->orderBy('orders.planning_sequence','ASC')
            ->orderBy('routes.planning_date')
            ->orderBy('routes.id');

        if (Input::get('order_id'))
            $orders->where('orders.id', Input::get('order_id'));
        if (Input::get('ids'))
            $orders->whereNotIn('orders.id', Input::get('ids'));

        $orders = $orders->get();

        $ids = array();
        foreach($orders as $order)
            $ids[] = $order->id;

        $pending_orders = $orders->toArray();

        $reject_reasons = []; //RejectReason::where('is_storage', 1)->where('status', 'Cancelado temporalmente')->orderBy('reason')->get()->toArray();
        $type_news = \NewReason::select(DB::raw('DISTINCT type'))->where('status', 1)->orderBy('type')->get()->toArray();
        $new_reasons =[]; //\NewReason::where('status', 1)->orderBy('reason')->get()->toArray();
        //$reject_reasons = RejectReason::where('is_storage', 1)->admin()->where('status', 'Cancelado temporalmente')->orderBy('reason')->get()->toArray();


        //mezclar arrays
        foreach($pending_orders as $i => $order)
        {
            if ($order['management_date'] && date('Y-m-d 00:00:00') > $order['management_date']){
                unset($pending_orders[$i]);
                continue;
            }

            //OBTENER PRODUCTOS DEL PEDIDO
            //$order_products = [];
            /*$products = OrderProduct::select(
                'order_products.id',
                'order_products.store_id',
                'order_products.order_id',
                'order_products.store_product_id',
                'order_products.sampling_id',
                'order_products.price',
                'order_products.original_price',
                'order_products.base_price',
                'order_products.quantity',
                'order_products.quantity_returned',
                'order_products.quantity_special_price_stock',
                'order_products.reason_returned',
                'order_products.reference_returned',
                'order_products.fulfilment_status',
                'order_products.type',
                'order_products.is_gift',
                'order_products.parent_id',
                'order_products.reference',
                'order_products.product_name',
                'order_products.product_quantity',
                'order_products.product_unit',
                'order_products.product_image_url',
                'order_products.product_delivery_discount_amount',
                //'products.description',
                'products.image_large_url',
                'store_products.storage',
                'orders.arrived_date')
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->orderBy('department_id', 'desc')
                ->orderBy('product_name', 'asc')
                ->where('order_id', $order['id'])
                ->whereIn('order_products.type', ['Product','Muestra', 'Recogida'])
                ->get();

            $products_info = [];
            foreach($products as $product)
            {
                $order_products[] = $product->toArray();
            }*/

            /*$product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*', 'store_products.storage', 'orders.arrived_date')
                ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->where('order_product_group.order_id', $order['id'])
                ->get();
            if ($product_group){
                foreach($product_group as $product_group_item){
                    if (!array_key_exists($product_group_item->order_store_product_id, $order_products))
                        $order_products[] = $product_group_item->toArray();
                    else{
                        $order_products[]['quantity_group'] = isset($products_info[$product_group_item->order_store_product_id]['quantity_group']) ? $products_info[$product_group_item->order_store_product_id]['quantity_group'] + $product_group_item->quantity : $product_group_item->quantity;
                        $order_products[]['order_product_group_id'] = $product_group_item->id;
                    }
                }
            }*/
            //debug($reject_reasons);

            $pending_orders[$i]['reject_reasons'] = $reject_reasons;
            $pending_orders[$i]['type_news'] = $type_news;
            $pending_orders[$i]['new_reasons'] = $new_reasons;
            if ($order['status'] == 'Cancelled')
                $pending_orders[$i]['reject_reason'] = $order['reject_reason'];

            if (empty($order['cc_charge_id']))
                $pending_orders[$i]['cc_charge_id'] = '';

            //$pending_orders[$i]['products'] = $order_products;

            if (empty($pending_orders[$i]['qty_orders']))
                $pending_orders[$i]['qty_orders'] = 0;

            //ocultar direccion teniendo en cuenta la franja de entrega
            if ($pending_orders[$i]['planning_sequence'] > 1){
                $delivery_time = explode(' - ', $pending_orders[$i]['delivery_time']);
                if (date('Y-m-d H:i', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes')) >= date('Y-m-d H:i')){
                    $pending_orders[$i]['user_address'] = 'Es visible a las '.date('g:i a', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes'));
                    $pending_orders[$i]['user_address_neighborhood'] = 'Es visible a las '.date('g:i a', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes'));
                    $pending_orders[$i]['user_address_latitude'] = 0;
                    $pending_orders[$i]['user_address_longitude'] = 0;
                }
            }
        }
        $pending_orders = array_values($pending_orders);

        $this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'orders' => $pending_orders
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Ordenar pedidos por secuencia o distancia
     *
     * @return array $response Respuesta
     */
    public function orders_sort_by()
    {
        if (!Input::has('order_ids'))
            return $this->jsonResponse('No hay pedidos para ordenar', 200);

        if (!Input::has('lat') && !Input::has('lng'))
            return $this->jsonResponse('Para ordenar los pedidos por favor habilita tu GPS.', 200);

        $order_ids = explode(',', Input::get('order_ids'));
        $merqueo_location = [
            'lat' => Input::get('lat'),
            'lng' => Input::get('lng')
        ];
        $orders = Order::select('orders.id','planning_sequence',
            DB::raw("(6371 * ACOS(
                                COS(RADIANS(".$merqueo_location['lat']."))
                                * COS(RADIANS(user_address_latitude))
                                * COS(RADIANS(user_address_longitude)
                                - RADIANS(".$merqueo_location['lng']."))
                                + SIN(RADIANS(".$merqueo_location['lat']."))
                                * SIN(RADIANS(user_address_latitude))
                                   )) AS distance"))
            ->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
            ->where('orders.type', 'Merqueo')
            ->where('orders.status', 'Dispatched')
            ->whereIn('orders.id', $order_ids)
            ->orderBy('orders.delivery_date')
            ->orderBy(Input::get('orderby'))
            ->get()
            ->toArray();

        if ($orders){
            $this->response = array(
                'status' => true,
                'message' => 'Pedidos ordenados.',
                'result' => array(
                    'orders' => $orders
                )
            );

            return $this->jsonResponse();

        }else return $this->jsonResponse('No se encontraron pedidos para ordenar.');
    }

    /**
     * Obtener movimientos de shopper
     *
     * @return array $response Respuesta
     */
    public function get_movements()
    {
        if (!$vehicle_id = Route::current()->parameter('id'))
            return $this->jsonResponse('ID vehiculo es requerido.', 400);

        if (!$vehicle = Vehicle::find($vehicle_id))
            return $this->jsonResponse('Vehiculo no existe.', 400);

        $balance = Vehicle::getBalance($vehicle->id);
        $movements = VehicleMovement::select('vehicle_movements.*',
            DB::raw("IF(vehicle_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
            DB::raw("DATE_FORMAT(vehicle_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"), 'payment_method',
            DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"))
            ->leftJoin('admin', 'vehicle_movements.admin_id', '=', 'admin.id')
            ->leftJoin('orders', 'vehicle_movements.order_id', '=', 'orders.id')
            ->join('drivers', 'vehicle_movements.driver_id', '=', 'drivers.id')
            ->where('orders.type', 'Merqueo')
            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime('-1 days')))
            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime('+3 hours')))
            ->where('vehicle_movements.vehicle_id', $vehicle->id)
            ->groupBy('vehicle_movements.id')
            ->groupBy('vehicle_movements.order_id')
            ->groupBy('vehicle_movements.status')
            ->orderBy('vehicle_movements.created_at', 'desc')->get();

        $color = $balance < 0 ? 'red' : 'green';
        $html = '
         <h4>Movimientos desde '.date('d/m/Y', strtotime('-1 days')).'</h4>
         <table width="100%">
            <tbody>
                <tr>
                    <td width="50%" align="center"><b>Saldo actual vehículo</b></td>
                    <td width="50%" align="center"><span class="badge balance bg-'.$color.'">$<span class="balance-shopper">'.number_format($balance, 0, ',', '.').'</span></span></td>
                </tr>
            </tbody>
         </table><br>
         <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>';
        if (count($movements))
        {
            foreach($movements as $movement)
            {
                $color = $movement->driver_balance < 0 ? 'red' : 'green';
                $html .= '<tr>
                    <th>Fecha</th>
                    <td>'.$movement->date.'</td>
                </tr>
                <tr>
                    <th>Tipo</th>
                    <td>'.$movement->movement.'</td>
                </tr>';
                if (!empty($movement->order_id)){
                    $html .= '<tr>
                        <th>Pedido</th>
                        <td>#'.$movement->order_id.'</td>
                    </tr>
                    <tr>
                        <th>Método de pago</th>
                        <td>Cliente: '.$movement->payment_method.'</td>
                    </tr>
                    <tr>
                        <th>Total pedido</th>
                        <td align="right">$'.number_format($movement->order_total_amount, 0, ',', '.').'</td>
                    </tr>';
                }
                $html .= '<tr>
                        <th>Saldo conductor</th>
                        <td align="right" style="color: '.$color.'"><b>$'.number_format($movement->driver_balance, 0, ',', '.').'</b></td>
                    </tr>
                <tr><td colspan="2" class="separador">&nbsp;</td></tr>';
            }
        }else $html .= '<tr><td colspan="8" class="separador" align="center">No hay movimientos.</td></tr>';
        $html .= '</tbody>
            </table>
         </div>';
        $this->response = array(
            'status' => true,
            'message' => 'Movimientos obtenidos',
            'result' => array(
                'driver_balance' => $balance,
                'html' => $html
            )
        );

        return $this->jsonResponse();
    }

    /**
     * Autenticacion de pusher
     */
    public function pusher_auth()
    {
        if (Auth::check() || Session::get('user_logged_in')){
            $response = Response::make(  Pusherer::auth_push( Input::get('channel_name'), Input::get('socket_id'), 'transporter' ), 200);
            return $response->header('Content-Type', 'application/json');
        }

        return $this->jsonResponse();
    }

    /**
     * Función para generar alertas en el módulo de alertas de transportadores
     */
    public function add_transporter_alert($id, $alert_num)
    {
        if ( ($vehicle = Vehicle::find($id)) && !empty($alert_num) ){
            $vehicle->addAlert((int)$alert_num, 'Pendiente');
            return $this->jsonResponse('Se ha agregado la alerta.', 200);
        }
        return $this->jsonResponse('ID vehiculo no existe.', 400);
    }

    /**
     * Función para validar que el vehículo esté dentro de la zona asignada.
     */
    public function validate_vehicle_zone($id)
    {
        if ( ($vehicle = Vehicle::find($id)) && Input::has('lat') && Input::has('lng') ){
            $now = Carbon::now();
            $order = Order::where('orders.vehicle_id', $vehicle->id)
                ->where('orders.type', 'Merqueo')
                ->where(DB::raw('DATE(orders.management_date)'), '=', $now->toDateString())
                ->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString())
                ->where('orders.status', 'Delivered')
                ->orderBy('orders.delivery_date', 'DESC')
                ->get();
            if ( count($order) > 0 ) {
                $order = $order->first();
                $route = Routes::find($order->route_id);
                $zone = Zone::find($route->zone_id);

                $is_valid = point_in_polygon($zone->delivery_zone, Input::get('lat'), Input::get('lng'));
                if ( !$is_valid ) {
                    $vehicle->addAlert(4, 'Pendiente');
                    return $this->jsonResponse('Se ha agregado la alerta.', 200);
                }else{
                    return $this->jsonResponse('El vehículo se encuentra en la zona asignada.', 200);
                }
            }
            return $this->jsonResponse('El vehiculo aún no tiene pedidos asignados.', 200);
        }
        return $this->jsonResponse('ID vehiculo no existe.', 400);
    }

    /**
     * Retorna un token de firebase que otorga permisos
     * valido de escritura.
     *
     * @return string
     */
    public function create_custom_token()
    {
        $this->setResponseFormat('json');
        $token = \FirebaseClient::makeKey('general');

        return $this->jsonResponse(compact('token'), 200);
    }

    /**
     * Registra las novedades de los transportadores
     *
     */
    public function register_news()
    {
        try{

            DB::beginTransaction();
            $transporter_new = new \TransporterNew;
            $transporter_new->type = Input::get('type_new');
            $transporter_new->reason = Input::get('new_reason');
            $transporter_new->reason_comments = Input::get('news_comments');
            $transporter_new->status = 'Sin Asignar';
            $transporter_new->order_id = Input::get('order_id');
            $transporter_new->vehicle_id = Input::get('vehicle_id');
            $transporter_new->url_file = Input::get('url-new-'.$transporter_new->order_id);
            $transporter_new->save();

            if(Input::get('type_new') == 'Productos'){
                foreach(Input::get('news_products') AS $news_product){
                    $type_product = Input::get('news_products_type_'.$news_product);
                    $id_product = $news_product;
                    $transporter_new_detail = new \TransporterNewDetail;
                    $transporter_new_detail->product_type = $type_product;
                    $transporter_new_detail->order_product_id = $id_product;
                    $transporter_new_detail->transporter_new_id = $transporter_new->id;

                    if($type_product == 'Agrupado'){

                        $order_product = OrderProductGroup::find($transporter_new_detail->order_product_id);
                        $transporter_new_detail->reason = Input::get('new_reason_'.$id_product);

                        if(preg_match('/Devolución/i', Input::get('new_reason_'.$id_product))){
                            if( $order_product->quantity == 0 ){
                                DB::rollback();
                                return $this->jsonResponse('No hay unidades para devolver de este producto.', 200);
                            }
                            if( $order_product->quantity < Input::get('news_product_qty_'.$id_product) ){
                                DB::rollback();
                                return $this->jsonResponse('La cantidad a devolver no puede ser mayor a la cantidad disponible.', 200);
                            }

                            $reason_returned = explode('-', Input::get('new_reason_'.$id_product));
                            $reason_returned = substr(trim($reason_returned[1]), 0, strlen(trim($reason_returned[1]))-1);
                            $reason_returned = ($reason_returned == 'Rechazado por el cliente')?'Rechazado cliente':$reason_returned;
                            $order_product->quantity_returned = ($order_product->quantity_returned>0)? $order_product->quantity_returned + Input::get('news_product_qty_'.$id_product): Input::get('news_product_qty_'.$id_product);
                            $order_product->reason_returned = $reason_returned;
                            if($order_product->quantity == Input::get('news_product_qty_'.$id_product)){
                                $order_product->fulfilment_status = 'Returned';
                            }else if($order_product->quantity > Input::get('news_product_qty_'.$id_product)){
                                $order_product->quantity = $order_product->quantity - Input::get('news_product_qty_'.$id_product);
                                $order_product->quantity = ($order_product->quantity < 0)? 0 : $order_product->quantity;
                            }
                            $order_product->reference_returned = $order_product->reference;
                        }else{
                            $order_product->quantity = $order_product->quantity - Input::get('news_product_qty_'.$id_product);
                            if($order_product->quantity == 0){
                                $order_product->fulfilment_status = 'Not Available';
                            }
                        }
                        $order_product->save();
                        $transporter_new_detail->store_product_id = $order_product->store_product_id;
                        $transporter_new_detail->quantity_returned = ($transporter_new->quantity_returned>0)? $transporter_new->quantity_returned + Input::get('news_product_qty_'.$id_product) : Input::get('news_product_qty_'.$id_product);
                    }else{
                        $order_product = OrderProduct::find($transporter_new_detail->order_product_id);
                        $transporter_new_detail->reason = Input::get('new_reason_'.$id_product);

                        if(preg_match('/Devolución/i', Input::get('new_reason_'.$id_product))){
                            if( $order_product->quantity == 0 ){
                                DB::rollback();
                                return $this->jsonResponse('No hay unidades para devolver de este producto.', 200);
                            }
                            if( $order_product->quantity < Input::get('news_product_qty_'.$id_product) ){
                                DB::rollback();
                                return $this->jsonResponse('La cantidad a devolver no puede ser mayor a la cantidad disponible.', 200);
                            }
                            $reason_returned = explode('-', Input::get('new_reason_'.$id_product));
                            $reason_returned = substr(trim($reason_returned[1]), 0, strlen(trim($reason_returned[1]))-1);
                            $reason_returned = ($reason_returned == 'Rechazado por el cliente')?'Rechazado cliente':$reason_returned;
                            $order_product->quantity_returned = ($order_product->quantity_returned>0)? $order_product->quantity_returned + Input::get('news_product_qty_'.$id_product): Input::get('news_product_qty_'.$id_product);
                            $order_product->reason_returned = $reason_returned;
                            if($order_product->quantity == Input::get('news_product_qty_'.$id_product)){
                                $order_product->fulfilment_status = 'Returned';
                            }else if($order_product->quantity > Input::get('news_product_qty_'.$id_product)){
                                $order_product->quantity = $order_product->quantity - Input::get('news_product_qty_'.$id_product);
                                $order_product->quantity = ($order_product->quantity < 0)? 0 : $order_product->quantity;
                            }
                            $order_product->reference_returned = $order_product->reference;
                        }else{
                            $order_product->quantity = $order_product->quantity - Input::get('news_product_qty_'.$id_product);
                            if($order_product->quantity == 0){
                                $order_product->fulfilment_status = 'Not Available';
                            }
                        }
                        $transporter_new_detail->store_product_id = $order_product->store_product_id;
                        $transporter_new_detail->quantity_returned = ($transporter_new->quantity_returned > 0 )? $transporter_new->quantity_returned + Input::get('news_product_qty_'.$id_product) : Input::get('news_product_qty_'.$id_product);
                        $order_product->save();
                    }
                    $transporter_new_detail->save();
                }
            }

            $transporter_new->save();

            DB::commit();
            $this->response = array(
                'status' => true,
                'message' => 'La novedad se reporto con éxito.',
                'result' => []
            );

            return $this->jsonResponse();
        }catch(\Exception $e){
            DB::rollback();
            return $this->jsonResponse('Ocurrio un error al reportar la novedad error.'.$e->getMessage().' line::'.$e->getLine(), 200);

        }
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function get_response_news()
    {
        /*$this->response = array(
            'status' => false,
            'message' => 'No hay notificaciones de novedades',
            'result' =>[ 'news' => '']
        );
        return $this->jsonResponse();*/

        try{
            $transporter_news = \TransporterNew::select('transporter_news.*')
                ->join('orders', 'orders.id', '=', 'transporter_news.order_id')
                ->where('transporter_news.vehicle_id', Input::get('vehicle_id'))
                ->whereRaw("DATE(orders.delivery_date) >= '".date('Y-m-d')."'")
                ->where('transporter_news.status','Resuelto')
                ->get();
            $news = [];
            foreach($transporter_news as $key => $t_news){
                $prods = [];
                if(count($t_news->transporterNewDetail)){
                    foreach($t_news->transporterNewDetail as $detail){
                        $prods[] = [
                            'name' => $detail->orderProducts()->product_name.' '.$detail->orderProducts()->product_quantity.' '.$detail->orderProducts()->product_unit,
                            'reason' => $detail->reason,
                            'quantity' => $detail->quantity_returned
                        ];

                    }
                    $t_news->products = $prods;
                }
                $news[] = [
                    "id" => $t_news->id,
                    "admin_id" => $t_news->admin_id,
                    "vehicle_id" => $t_news->vehicle_id,
                    "order_id" => $t_news->order_id,
                    "order_product_id" => $t_news->order_product_id,
                    "store_product_id" => $t_news->store_product_id,
                    "quantity_returned" => $t_news->quantity_returned,
                    "reason" => $t_news->reason,
                    "reason_comments" => $t_news->reason_comments,
                    "resolution_message" => json_decode($t_news->resolution_message, true),
                    "type" => $t_news->type,
                    "product_type" => $t_news->product_type,
                    "status" => $t_news->status,
                    "resolve_date" => $t_news->resolve_date,
                    "products" => $prods,
                    "created_at" => $t_news->created_at,
                    "updated_at" => $t_news->updated_at
                ];
            }
            $transporter_news = $news;
            if(count($transporter_news)>0){
                $this->response = array(
                    'status' => true,
                    'message' => 'Notificaciones resueltas',
                    'result' => array(
                        'news' => $transporter_news
                    )
                );
            }else{
                $this->response = array(
                    'status' => false,
                    'message' => 'No hay notificaciones de novedades',
                    'result' =>[ 'news' => '']
                );
            }
            return $this->jsonResponse();

        }catch(\Exception $e){
            return $this->jsonResponse('Ocurrio un error al consultar la novedad.'.$e->getMessage(), 400);
        }
    }


    /**
     * Subir imagen de referencia de las novedades
     */
    function upload_reference_image()
    {
        $file = Input::file('image');
        $file_data['real_path'] = $file->getRealPath();
        $file_data['client_original_name'] = $file->getClientOriginalName();
        $file_data['client_original_extension'] = $file->getClientOriginalExtension();

        $dir = 'news/reference/'.date('Y-m-d');
        $url = upload_image($file, false, $dir);

        if ($url)
        {

            return Response::json(array('status' => true, 'message' => 'La imagen fue subida con éxito.', 'url' => $url) , 200);
        }
        return Response::json(array('status' => false, 'message' => 'Ocurrió un error al subir la imagen.'.$url) , 200);
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function log_call_user(){
        try{
            $call_user_log = new \CallUserLog;
            $call_user_log->vehicle_id = Input::get('vehicle_id');
            $call_user_log->order_id = Input::get('order_id');
            $call_user_log->save();
            return $this->jsonResponse('Se almaceno correctamente el registro de la llamada.', 200);
        }catch(\Exception $e){
            return $this->jsonResponse('Ocurrio un error al guardar el registro de la llamada.', 400);
        }
    }
}
