<?php

use exceptions\MigrateException;
use Illuminate\Http\JsonResponse;

/**
 * Class NewSiteController
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class NewSiteController extends BaseController
{
    /**
     * @var NewSiteApi
     */
    private $api;

    /**
     * NewSiteController constructor.
     * @param NewSiteApi $api
     */
    public function __construct(NewSiteApi $api)
    {
        parent::__construct();
        $this->api = $api;
    }

    /**
     * Redirecciona a un usuario a la nueva página web.
     *
     * @return mixed
     */
    public function redirectToNewSite()
    {
        $referrer = Request::server('HTTP_REFERER');
        $referrer = str_replace(Config::get('app.url'), '', $referrer);
        $referrer = str_replace('/domicilios-super-ahorro', '', $referrer);
        $referrer = str_replace("/buscar?q=", "/buscar/", $referrer);

        $address = Session::get("address");

        if($address && $address->city_id){
            $cityCurrent = City::where(['status' => 1, 'id' => $address->city_id])->first();
            $myCity = City::where(['status' => 1, 'coverage_store_id' => $cityCurrent->coverage_store_id])->first();
            $referrer = str_replace('/'.$myCity->slug, '/'.$cityCurrent->slug, $referrer);
        }

        $migrateUrl = ( str_contains($referrer, '?') || str_contains($referrer, '&') ) ? '&' : '?';

        return Redirect::to(Config::get('app.new_site_url') . $referrer . $migrateUrl .'migrate=true');
    }

    /**
     * Inicializa la sesión del usuario por medio de los párametros
     * enviados en la petición.
     *
     * @return JsonResponse
     */
    public function import()
    {
        Session::invalidate();
        $user = null;
        $userToken = Input::get('token');
        $cart = Input::get('cart');
        $address = Input::get('address');
        $zoneId = Input::get('zoneId');
        $zone = null;
        $cityId = Input::get('cityId');
        $storeId = Input::get('storeId');

        Session::put('city_id', $cityId);
        Cookie::queue('city_id', $cityId);
        Session::put('store_id', $storeId);

        if($zoneId){
            $zone = Zone::findOrFail(Input::get('zoneId'));
        }

        if ($userToken) {
            $userId = $this->api->getUserIdFromToken($userToken);
            $user = User::findOrFail($userId);
            Auth::login($user, true);
        }else{
            Auth::logout();
        }

        if (!empty($zone)) {
            $this->setSessionCart($zone, $cart, $user);
        }

        $this->setSessionAddress($address);

        if($zone){
            Session::put('zone_id', $zone->getKey());
            Session::put('warehouse_id', $zone->warehouse->getKey());
            Session::put('city_id', $zone->warehouse->city->getKey());
        }else{
            Session::put('zone_id', $this->getCurrentZone($storeId)->id);
        }

        return Response::json(['message' => 'ok']);
    }

    /**
     * Asigna la dirección a la sesión actual.
     *
     * @param $address
     */
    private function setSessionAddress($address)
    {
        if (empty($address)) {
            Session::put('coverage', 0);
            return;
        }

        if (!empty($address['id'])) {
            $address = UserAddress::select("*", "address AS address_text")
                ->findOrFail($address['id']);
            Session::put('address', (object) $address->getAttributes());
            Session::put('coverage', 1);

            return;
        }

        Session::put('coverage', 1);
        Session::put('address', (object) $address);
    }

    /**
     * Asigna el carrito a la sesión actual.
     *
     * @param Zone $zone
     * @param $cartItems
     * @param User|null $user
     */
    private function setSessionCart(Zone $zone, $cartItems, User $user = null)
    {
        if (empty($user)) {
            $this->storeTemporalCart($zone->warehouse, $cartItems);
            return;
        }

        $this->persistCart($zone, $cartItems, $user);
    }

    /**
     * Retorna los datos de la sesión actual en el formato utilizado en al nueva web.
     *
     * @return JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function export()
    {
        DB::beginTransaction();
        $cartId = $this->getCartIdentifier();
        DB::commit();

        try {
            $userData = $this->api->setUser(Auth::user())
                ->setAddress($this->getAddressObject())
                ->setCartId($cartId)
                ->setZoneId($this->getZoneIdentifier())
                ->setCityId($this->getCityId())
                ->getUserSession();
        } catch (MigrateException $exception) {
            return Response::make($exception)
                ->header('Content-Type', 'application/json');
        }

        return Response::json($userData);
    }

    /**
     * Obtiene los datos de la sesión actual que serán enviados a la nueva web.
     *
     * @return StdClass|UserAddress
     */
    private function getAddressObject()
    {
        $sessionAddress = Session::get('address');

        if (empty($sessionAddress)) {
            return null;
        }
        $storeId = Session::get('store_id') ? Session::get('store_id') : 63;

        $address = new StdClass();
        if(isset($sessionAddress->address_text)){
            $address->address = $sessionAddress->address_text;
            $address->latitude = $sessionAddress->latitude;
            $address->longitude = $sessionAddress->longitude;
        }        
        $address->zoneId = $this->getCurrentZone($storeId)->id;
        $address->city_id = $sessionAddress->city_id;

        if (!empty($sessionAddress->id)) {
            $address->id = $sessionAddress->id;
            $address->addressFurther = $sessionAddress->address_further;
            $address->neighborhood = $sessionAddress->neighborhood;
        }

        return $address;
    }

    /**
     * Obtiene el identificador del carrito actual.
     *
     * @return string
     */
    private function getCartIdentifier()
    {
        $cartId = Session::get('cart_id');

        if ($cartId) {
            return $cartId;
        }

        $sessionCart = json_decode(Session::get('tmp_cart'));

        if (empty($sessionCart)) {
            return null;
        }

        $cart = new Cart();
        $cart->user_id = 173787;
        $cart->warehouse()->associate($this->getCurrentWarehouse());
        $cart->save();

        foreach ($sessionCart->products as $id => $product) {
            $cartProduct = new CartProduct();
            $cartProduct->cart_id = $cart->getKey();
            $cartProduct->store_product_id = $id;
            $cartProduct->quantity = $product->cart_quantity;
            $cartProduct->quantity_full_price = $product->cart_quantity_full_price;
            $cartProduct->added_by = $cart->user_id;
            $cartProduct->save();
        }

        return $cart->getKey();
    }

    /**
     * @param $cartItems
     * @return void
     */
    private function storeTemporalCart(Warehouse $warehouse, $cartItems)
    {
        $cart = new StdClass();
        $cartProducts = new StdClass();
        $cart->products = $cartProducts;
        $cart->warehouse_id = $warehouse->id;

        foreach ($cartItems as $cartItem) {
            $quantityFullPrice = $cartItem['quantityFullPrice'];
            $quantitySpecialPrice = $cartItem['quantitySpecialPrice'];

            $product = new StdClass();
            $product->added_by = 0;
            $product->cart_id = 0;
            $product->cart_quantity = $quantitySpecialPrice ?: $quantityFullPrice;
            $product->cart_quantity_full_price = $quantitySpecialPrice ? $quantityFullPrice : 0;
            $product->store_product_id = $cartItem['product']['id'];

            $cartProducts->{$cartItem['product']['id']} = $product;
        }

        Session::put('tmp_cart', json_encode($cart));
    }

    /**
     * @param Zone $zone
     * @param $cartItems
     * @param User $user
     * @return void
     */
    private function persistCart(Zone $zone, $cartItems, User $user)
    {
        $cart = new Cart();
        $cart->warehouse()->associate($zone->warehouse);
        $cart->user()->associate($user);
        $cart->save();

        foreach ($cartItems as $cartItem) {
            $quantityFullPrice = $cartItem['quantityFullPrice'];
            $quantitySpecialPrice = $cartItem['quantitySpecialPrice'];

            $product = new CartProduct();
            $product->store_product_id = $cartItem['product']['id'];
            $product->cart()->associate($cart);
            $product->quantity = $quantitySpecialPrice ?: $quantityFullPrice;
            $product->quantity_full_price = $quantitySpecialPrice ? $quantityFullPrice : 0;
            $product->store_id = $cartItem['product']['storeId'];
            $product->product_name = $cartItem['product']['name'];
            $product->price = $cartItem['product']['specialPrice'] ?: $cartItem['product']['price'];
            $product->save();
        }

        Session::put('cart_id', $cart->getKey());
    }

    /**
     * Obtiene el identificador de la zona actual.
     *
     * @return string
     */
    private function getZoneIdentifier()
    { 
        $zoneId = Session::get('zone_id');
        $zone = Zone::where(
            ['id' => $zoneId],
            ['status' => 1]
        )->first();

        if ($zoneId && $zone) {
            return $zoneId;
        }else{
            $storeId = Session::get('store_id');
            return $this->getCurrentZone($storeId)->id;
        }

    }

    /**
     * Obtiene el identificador de la tienda actual.
     *
     * @return string
     */
    private function getCityId()
    { 
        $cityId = Session::get('city_id');
        
        $addressObject = $this->getAddressObject();
        if($addressObject){
            return $addressObject->city_id;
        }

        if ($cityId) {
            return $cityId;
        }

    }
}
