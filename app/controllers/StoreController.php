<?php

use Carbon\Carbon;
use exceptions\CoverageException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Class StoreController
 * @property Warehouse $warehouse
 */
class StoreController extends BaseController
{
    /**
     * @var Collection|array
     */
    public $departments;

    /**
     * @var int
     */
    private $total_orders = 0;

    /**
     * @var null
     */
    private $last_order_date = null;

    /**
     * @var null|string
     */
    private $special_price_query_string = null;

    /**
     * @var NewSiteApi
     */
    private $api;

    /**
     * StoreController constructor.
     * @param NewSiteApi $api
     */
    public function __construct(NewSiteApi $api)
    {
        parent::__construct();

        $this->api = $api;
        $this->warehouse = $this->getCurrentWarehouse();
        $rules = ['except' => $this->getRoutesWhichIgnoreFilter()];

        if ($this->user_id) {
            $info = User::getOrdersInfo($this->user_id);
            $this->total_orders = $info->qty;
            $this->last_order_date = new Carbon($info->last_order_date);
        }

        $this->special_price_query_string = StoreProduct::getRawSpecialPriceQuery(
            $this->total_orders,
            $this->last_order_date
        );

        $this->beforeFilter(function () {
            $city_slug = Route::current()->parameter('city_slug');
            $store_slug = Route::current()->parameter('store_slug');

            if ($city_slug && $store_slug) {
                // Redireccionar seo antiguo
                $store_slugs = array('super-ahorro');
                if (!in_array($store_slug, $store_slugs)) {
                    header("HTTP/1.1 301 Moved Permanently");
                    if ($city_slug == 'bogota') {
                        header("Location: " . Config::get('app.url') . "/bogota/domicilios-super-ahorro");
                        return Redirect::to($city_slug . '/domicilios-super-ahorro');
                    } else if ($city_slug == 'medellin') {
                        header("Location: " . Config::get('app.url') . "/medellin/domicilios-super-ahorro");
                        return Redirect::to($city_slug . '/domicilios-super-ahorro');
                    } else if ($city_slug == 'mexico-df') {
                        header("Location: " . Config::get('app.url') . "/mexico-df/domicilios-super-ahorro");
                        return Redirect::to($city_slug . '/domicilios-super-ahorro');
                    }
                }

                $this->store = Store::select('stores.*', 'cities.city AS city_name', 'cities.slug AS city_slug', 'cities.latitude AS city_lat', 'cities.longitude AS city_lng')
                    ->join('cities', 'cities.id', '=', 'stores.city_id')
                    ->where('cities.slug', $city_slug)
                    ->where('stores.slug', $store_slug)
                    ->where('stores.is_hidden', 0)
                    ->where('stores.status', 1)
                    ->first();

                if ($this->store) {
                    Session::put('store_id', $this->store->id);
                    //cargar id de cookie o guardar nuevo
                    if (Cookie::get('city_id')) {
                        if (Cookie::get('city_id') != $this->store->city_id) {
                            //eliminar carrito
                            if (Auth::check()) {
                                if (Session::has('cart_id'))
                                    CartProduct::where('cart_id', Session::get('cart_id'))->delete();
                            } else {
                                Session::forget('tmp_cart');
                            }

                            Cookie::queue('city_id', $this->store->city_id, 2628000);
                        }
                    } else {
                        Cookie::queue('city_id', $this->store->city_id, 2628000);
                    }
                } else {
                    App::abort(404);
                }
            } else {
                return Redirect::to('/');
            }

            if (!Session::has('cart_id')) {
                Session::put('cart_id', 0);
            }

            if (!Session::has('coverage')) {
                Session::put('coverage', 0);
            }

            $this->departments = Department::join('shelves', 'departments.id', '=', 'shelves.department_id')
                ->join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->select('departments.*', DB::raw('COUNT(products.id) AS qty_products'))
                ->where('store_product_warehouses.status', 1)
                ->where('store_product_warehouses.is_visible', 1)
                ->where('departments.status', 1)
                ->where('shelves.status', 1)
                ->where('warehouse_id', $this->warehouse->id)
                ->where('departments.store_id', $this->store->id)
                ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                ->where('products.type', '<>', 'Proveedor');

            if ($this->user_id) {
                // Verificar si tiene pedidos entregados para primera compra

                if ($this->total_orders) {
                    Session::put('total_orders', $this->total_orders);
                    Session::put('has_orders', 1);
                    Session::put('last_order_date', $this->last_order_date->toTimeString());
                } else {
                    Session::put('has_orders', 0);
                }

                // Verificar si tiene pedidos pendientes para chat de Intercom
                $pending_orders = Order::select('orders.id')
                    ->where('user_id', $this->user_id)
                    ->whereNotIn('status', ['Delivered', 'Cancelled'])
                    ->count();

                Session::put('has_pending_orders', $pending_orders > 0);
            }

            $this->departments = $this->departments
                ->groupBy('departments.id')
                ->orderBy('departments.sort_order')
                ->orderBy('name')
                ->get();

            $slot = Store::getDeliverySlot($this->store->id);
            $this->store->is_open = $slot['is_open'];
            $this->store->slot = $slot['next_slot'];

            // Cargar datos en vista
            $views = ['store', 'department', 'shelf', 'product', 'search', 'my_products'];
            $warehouse = $this->warehouse;
            View::composer($views, function ($view) use ($warehouse) {
                $stores = Store::getByLatLng();
                $user_discounts = User::getDiscounts();

                $shelves = Shelf::join('store_product_shelves', 'shelves.id', '=', 'store_product_shelves.shelf_id')
                    ->join('store_products', 'store_product_shelves.store_product_id', '=', 'store_products.id')
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->select('shelves.*', DB::raw('COUNT(products.id) AS qty_products'))
                    ->where('shelves.status', 1)
                    ->where('store_product_warehouses.status', 1)
                    ->where('store_product_warehouses.is_visible', 1)
                    ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                    ->where('shelves.store_id', $this->store->id)
                    ->where('warehouse_id', $warehouse->id)
                    ->where('products.type', '<>', 'Proveedor')
                    ->groupBy('shelves.id')
                    ->orderBy('shelves.sort_order')
                    ->orderBy('name')
                    ->get();

                $user_addresses = array();
                $last_order = false;
                if ($this->user_id) {
                    // Direcciones del usuario
                    $user_addresses = UserAddress::select('user_address.*', 'cities.city')
                        ->join('cities', 'city_id', '=', 'cities.id')
                        ->where('user_id', $this->user_id)
                        ->where('coverage_store_id', Session::get('store_id'))
                        ->get();

                    if ($user_addresses) {
                        $user_addresses = $user_addresses->toArray();
                    }
                }

                $city = City::remember(10)->find(Session::get('city_id'));
                $metadata['city_name'] = $city->city;
                $metadata['city_slug'] = $city->slug;

                // Obtener ciudades aledañas
                $all_cities = City::where('coverage_store_id', $this->store->id)
                    ->where('is_main', 1)
                    ->where('status', 1)
                    ->get();

                $surrounded_cities['city'] = $city;
                $surrounded_cities['surrounded_cities'] = City::where('parent_city_id', $city->id)
                    ->where('status', 1)
                    ->get();

                $view->with('store', $this->store)
                    ->with('departments', $this->departments)
                    ->with('cities', $all_cities)
                    ->with('surrounded_cities', $surrounded_cities)
                    ->with('user_discounts', $user_discounts)
                    ->with('shelves', $shelves)
                    ->with('stores', $stores)
                    ->with('metadata', $metadata)
                    ->with('cart_id', Session::get('cart_id'))
                    ->with('footer', $this->getFooter())
                    ->with('user_addresses', $user_addresses)
                    ->with('warehouse', $this->getCurrentWarehouse())
                    ->with('last_order', $last_order);
            });
        }, $rules);
    }

    /**
     * Muestra home de tienda con categorias
     *
     * @param $city_slug
     * @param $store_slug
     * @return \Illuminate\Contracts\View\View
     */
    public function all_departments($city_slug, $store_slug)
    {
        $user = Auth::user();
        $data = empty($user) ? null : $user->FullTraits;
        self::trackEventOnBrowser(['store_opened' => new StdClass()]);
        $city = City::where('slug', $city_slug)->first();
        $data['store'] = $this->store->toArray();
        $slot = Store::getDeliverySlot($this->store->id);
        $data['store']['next_delivery_slot'] = $slot['next_slot'];

        Session::set('city_id', $city->id);
        //si el usuario esta logueado mostrar productos comprados
        if ($this->user_id) {
            $products = $user->getProducts($this->store->id, $this->warehouse->id, $this->special_price_query_string, 1, 7);
            if ($products) {
                $data['store']['user_products'] = $products;
            }
        }

        $departments_data = new Collection();
        foreach ($this->departments as $department) {
            // Validacion precio para primera compra
            $products = StoreProduct::select(
                'products.*',
                'store_products.*',
                'has_warning AS warning',
                'shelves.slug AS shelf_slug',
                DB::raw("'{$department->slug}' as department_slug"),
                $this->special_price_query_string,
                StoreProduct::getRawDeliveryDiscountByDate()
            )
                ->pum()
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
                ->join('departments', 'shelves.department_id', '=', 'departments.id')
                ->where('shelves.department_id', $department->id)
                ->where('store_products.store_id', $this->store->id)
                ->where('products.type', '<>', 'Proveedor')
                ->where('store_product_warehouses.status', 1)
                ->where('departments.status', 1)
                ->where('shelves.status', 1)
                ->where('store_product_warehouses.is_visible', 1)
                ->where('warehouse_id', $this->warehouse->id)
                ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                ->orderBy('store_product_shelves.sort_order')
                ->limit(7)
                ->get();

            $department->products = $products;
            $departments_data->push($department);
        }

        $data['store']['departments'] = $departments_data;
        $has_orders = Session::get('has_orders');

        $banners = Banner::publicQuery($this->warehouse, $this->store, null, $this->total_orders)
            ->with(['storeProduct' => function ($query) {
                $query->with('product', 'shelf', 'department', 'store.city')
                    ->select('store_products.*', $this->special_price_query_string);
            }])
            ->get();

        $referred = $this->getReferrer();
        $variation_key = $this->getExperimentVariant([]);

        return View::make('store', compact('banners', 'referred', 'has_orders', 'data', 'variation_key'));
    }

    /**
     * Muestra home de categoria de tienda
     */
    public function single_department($city_slug, $store_slug, $department_slug)
    {
        $department = Department::where('slug', $department_slug)
            ->where('store_id', $this->store->id)
            ->where('status', 1)
            ->first();

        if (!$department) {
            App::abort(404);
        }

        $has_orders = Session::get('has_orders');
        $banners = Banner::publicQuery($this->warehouse, $this->store, $department, $has_orders)
            ->with(['storeProduct' => function ($query) {
                $query->with('product', 'shelf', 'department', 'store.city')
                    ->inWarehouse($this->getCurrentWarehouse())
                    ->select('store_products.*', $this->special_price_query_string)
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->pum();
            }])
            ->get();

        $shelves = Shelf::where('department_id', $department->id)
            ->where('status', 1)
            ->orderBy('sort_order')
            ->orderBy('name')
            ->with('department')
            ->get();

        $data['store'] = $this->store->toArray();
        $data['department'] = $department;

        $data['department']['shelves'] = $shelves;

        foreach ($shelves as $index => $shelf) {
            $products = StoreProduct::select(
                'products.*',
                'store_products.*',
                'has_warning AS warning',
                // Remover cuando se utilicen objetos en blade y no un
                // objeto con todos los atributos necesarios.
                DB::raw("'{$department->slug}' AS department_slug"),
                DB::raw('shelves.slug AS shelf_slug'),
                $this->special_price_query_string,
                StoreProduct::getRawDeliveryDiscountByDate(),
                'store_product_shelves.shelf_id'
            )
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
                ->pum()
                ->where('store_products.store_id', $this->store->id)
                ->where('products.type', '<>', 'Proveedor')
                ->where('store_product_warehouses.status', 1)
                ->where('shelves.status', 1)
                ->where('store_product_warehouses.is_visible', 1)
                ->where('warehouse_id', $this->warehouse->id)
                ->where('store_product_shelves.shelf_id', $shelf->id)
                ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                ->groupBy('store_product_shelves.shelf_id')
                ->orderBy('store_product_shelves.sort_order')
                ->get();

            $data['department']['shelves'][$index]['products'] = $products->map(function ($product) {
                return $product;
            });
        }

        Session::put('store_id', $this->store->id);

        $properties['department'] = $department->name;
        $properties['departmentId'] = $department->id;
        $events = ['department_viewed' => $properties];
        if ($this->getReferrer() === 'department' || $this->getReferrer() === 'store') {
            $events['change_department'] = $properties;
            $events['change_department'] += [
                'shelf' => count($shelves) ? $shelves[0]->name : '',
                'shelfId' => count($shelves) ? $shelves[0]->id : ''
            ];
        }
        self::trackEventOnBrowser($events);
        $variation_key = $this->getExperimentVariant([]);
        $currentDepartment = $department;

        return View::make('department', compact('variation_key', 'banners', 'has_orders', 'data', 'currentDepartment'));
    }

    /**
     * Muestra home de subcategoria de tienda
     */
    public function single_shelf($city_slug, $store_slug, $department_slug, $shelf_slug)
    {
        $shelf = Shelf::select(
            'shelves.*',
            'departments.slug AS department_slug',
            'departments.name AS department_name'
        )
            ->join('departments', 'department_id', '=', 'departments.id')
            ->where('departments.slug', $department_slug)
            ->where('shelves.slug', $shelf_slug)
            ->where('shelves.store_id', $this->store->id)
            ->where('departments.status', 1)
            ->where('shelves.status', 1)
            ->first();

        if (!$shelf) {
            App::abort(404);
        }

        $shelf_data = $shelf;
        $products = StoreProduct::select(
            'products.*',
            'store_products.*',
            'shelves.has_warning AS warning',
            DB::raw("'{$shelf->department_slug}' AS department_slug"),
            DB::raw('shelves.slug AS shelf_slug'),
            $this->special_price_query_string,
            StoreProduct::getRawDeliveryDiscountByDate()
        )
            ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->pum()
            ->where('store_product_shelves.shelf_id', $shelf->id)
            ->where('store_products.store_id', $this->store->id)
            ->where('products.type', '<>', 'Proveedor')
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('warehouse_id', $this->warehouse->id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->orderBy('store_product_shelves.sort_order')
            ->get();

        $shelf_data->products = $products;
        $data['store'] = $this->store->toArray();
        $data['shelf'] = $shelf_data;
        $refered = $this->getReferrer();

        if ($refered === 'shelf' || $refered === 'department') {
            self::trackEventOnBrowser(['swipe_shelf' => [
                'department' => $shelf->department_name,
                'departmentId' => $shelf->department_id,
                'shelf' => $shelf->name,
                'shelfId' => $shelf->id,
            ]]);
        }

        $variation_key = $this->getExperimentVariant([]);

        return View::make('shelf', compact('variation_key', 'data'))
            ->with('currentDepartment', $shelf->department)
            ->with('currentShelf', $shelf);
    }

    /**
     * Muestra los productos disponibles que el usuario ha comprado.
     *
     * @param $city_slug
     * @param $store_slug
     * @return mixed
     */
    public function bought_products($city_slug, $store_slug)
    {
        $user = Auth::user();
        if (!$user) {
            App::abort(404);
        }

        $store = Store::with('city')
            ->where('slug', $store_slug)
            ->first();

        $products = $user->getProducts($store->id, $this->getCurrentWarehouse()->id)
            ?: new Collection();

        // Se agregan atributos que son necesarios para que se
        // renderice el listado de los productos.
        $products = $products->map(function ($product) {
            $product->warning = $product->has_warning;

            return $product;
        });

        self::trackEventOnBrowser(['last_products_bought_viewed' => null]);

        return View::make('my_products', compact('products', 'store'));
    }

    /**
     * Detalle de producto
     *
     * @param $city_slug
     * @param $store_slug
     * @param $department_slug
     * @param $shelf_slug
     * @param $product_slug
     * @return \Illuminate\Contracts\View\View
     */
    public function single_product($city_slug, $store_slug, $department_slug, $shelf_slug, $product_slug)
    {
        $product_base = StoreProduct::select(
            'products.*',
            'store_products.*',
            'has_warning AS warning',
            'departments.slug AS department_slug',
            'shelves.slug AS shelf_slug',
            'departments.id AS department_id',
            'shelves.id AS shelf_id',
            'products.id',
            'store_products.id as store_product_id',
            'departments.name AS department_name',
            'shelves.name AS shelf_name',
            StoreProduct::getRawDeliveryDiscountByDate(),
            $this->special_price_query_string
        )
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
            ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
            ->join('departments', 'shelves.department_id', '=', 'departments.id')
            ->join('stores', 'stores.id', '=', 'store_products.store_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->pum()
            ->where('departments.slug', $department_slug)
            ->where('shelves.slug', $shelf_slug)
            ->where('products.slug', $product_slug)
            ->where('stores.id', $this->store->id)
            ->where('stores.city_id', $this->store->city_id)
            ->where('products.type', '<>', 'Proveedor')
            ->where('warehouse_id', $this->warehouse->id)
            ->with([
                'product' => function ($query) {
                    $query->with('details')
                        ->with([
                            'images' => function ($query) {
                                $query->select('id', 'product_id', 'image_large_url', 'image_small_url');
                            },
                        ]);
                }
            ]);

        $verify_product = clone $product_base;

        $verify_product = $verify_product->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->first();

        $product_disable = false;

        if (!$verify_product) {
            $product = $product_base->first();
            $product_disable = true;
        } else {
            $product = $verify_product;
        }

        if (!$product) {
            App::abort(404);
        }

        $product->id = $product->store_product_id;
        $product->product->populatePrimaryImage();
        $related_products =  StoreProduct::select(
            'products.*',
            'store_products.*',
            'shelves.has_warning AS warning',
            'departments.slug AS department_slug',
            'shelves.slug AS shelf_slug',
            $this->special_price_query_string
        )
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
            ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
            ->join('departments', 'shelves.department_id', '=', 'departments.id')
            ->join('stores', 'stores.id', '=', 'store_products.store_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->pum()
            ->where('store_products.department_id', '=', $product['department_id'])
            ->where('store_products.store_id', '=', $product['store_id'])
            ->where('store_products.shelf_id', '=', $product->shelf_id)
            ->where('store_products.id', '<>', $product->store_product_id)
            ->where('products.type', '<>', 'Proveedor')
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('warehouse_id', $this->warehouse->id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->orderBy(DB::raw('RAND()'))
            ->limit(7)
            ->get();

        $variation_key = $this->getExperimentVariant([]);

        if (Auth::check() && !empty($variation_key['social-proof'])) {
            if ($variation_key['social-proof']->getVariant() === 'control') {
                $product->social_proofs_total = false;
            }
        }

        $department =  Department::find($product->department_id);
        $shelf = Shelf::find($product->shelf_id);
        // Track Product Viewed event
        self::trackEventOnBrowser(['product_viewed' => [
            'department' => $product->department_name,
            'departmentId' => $product->department_id,
            'shelf' => $product->shelf_name,
            'shelfId' => $product->shelf_id,
            'storeProductId' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'specialPrice' => $product->special_price ?: 0,
            'bestPriceGuaranted' => $product->is_best_price ? 1 : 0,
            'screen' => self::getReferrer()
        ]]);

        return View::make('product', compact('product', 'related_products', 'department', 'variation_key', 'shelf', 'product_disable'));
    }

    /**
     * Buscador de productos autocompletar en tienda
     *
     * @param int $store_id ID de la tienda
     * @return object View HTML de productos
     */
    public function autocomplete($store_id)
    {
        $products = [];
        $store = Store::select('stores.*', 'cities.slug AS city_slug')
            ->join('cities', 'cities.id', '=', 'stores.city_id')
            ->where('stores.id', $store_id)
            ->first();

        if (Input::has('q')) {
            $q = Input::get('q');
            $q = DB::connection()->getPdo()->quote(strip_tags($q));
            $q = get_clean_string($q);

            $rows = 8;
            $page = 1;

            if (Config::get('app.aws.elasticsearch.is_enable')) {
                // NUEVO BUSCADOR
                $currentDate = Carbon::create();
                $elasticSearch = new AWSElasticsearch;
                $elasticSearch->setApplyFivePercentDiscount($this->applyFivePercentDiscount());
                $result = $elasticSearch->search($this->warehouse->id, $q, $page, $rows);
                $products = $result['result'][$this->warehouse->id] ?: [];

                foreach ($products as $key => $product) {
                    $products[$key] = $elasticSearch->clearProductResponse(
                        $product,
                        $currentDate,
                        $this->total_orders,
                        $this->last_order_date
                    );
                }
            } else {
                //ANTIGUO BUSCADOR
                $query = Product::where('store_products.store_id', $store_id);
                $keys = explode(' ', $q);
                if (count($keys)) {
                    $keywords = '';
                    foreach ($keys as $key) {
                        if (($key != '') and ($key != ' ')) {
                            if ($keywords != '') {
                                $keywords .= '-space-';
                            }
                            $keywords .= $key;
                        }
                    }
                    $keywords = explode('-space-', $keywords);
                    foreach ($keywords as $keyword) {
                        $query->whereRaw('(products.name LIKE "%' . trim($keyword) . '%" COLLATE utf8_spanish_ci OR products.name LIKE "%' . get_plural_string(trim($keyword)) . '%" COLLATE utf8_spanish_ci)');
                    }
                }

                $query->select(
                    'products.*',
                    'store_products.*',
                    'has_warning',
                    'departments.slug AS department_slug',
                    'shelves.slug AS shelf_slug',
                    $this->special_price_query_string,
                    StoreProduct::getRawDeliveryDiscountByDate()
                )
                    ->distinct()
                    ->join('store_products', 'store_products.product_id', '=', 'products.id')
                    ->join('departments', 'department_id', '=', 'departments.id')
                    ->join('shelves', 'shelf_id', '=', 'shelves.id')
                    ->join('stores', 'departments.store_id', '=', 'stores.id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->where('departments.status', 1)
                    ->where('shelves.status', 1)
                    ->where('warehouse_id', $this->warehouse->id)
                    ->where('stores.status', 1)
                    ->where('store_product_warehouses.status', 1)
                    ->where('store_product_warehouses.is_visible', 1)
                    ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                    ->where('products.type', '<>', 'Proveedor');
                if (!empty($keywords)) {
                    foreach ($keywords as $keyword) {
                        $keywords[] = get_plural_string(trim($keyword));
                    }
                    foreach ($keywords as $keyword) {
                        $query->orderBy(DB::raw('CASE
                                WHEN products.name LIKE \'' . trim($keyword) . '%\' THEN 0
                                WHEN products.name LIKE \'% %' . trim($keyword) . '% %\' THEN 1
                                WHEN products.name LIKE \'%' . trim($keyword) . '\' THEN 2
                                ELSE 3
                            END
                        '));
                    }
                }
                $products = $query->orderBy('products.name', 'asc')->limit(8)->get();
            }
        }

        if (!count($products)) {
            $search_log = new SearchLog;
            $search = Input::get('q');
            if ($this->user_id) {
                $search_log->user_id = $this->user_id;
            }
            $search_log->search = $search;
            $search_log->ip = Request::getClientIp();
            $search_log->store_id = $store_id;
            $search_log->save();
        }

        return View::make('autocomplete')
            ->with('products', $products)
            ->with('search', $q)
            ->with('query', urlencode($q))
            ->with('store', $store);
    }

    /**
     * Buscar productos
     *
     * @param $citySlug
     * @param $storeSlug
     * @return \Illuminate\Contracts\View\View
     */
    public function search_products($citySlug, $storeSlug)
    {
        if (!Input::has('q')) {
            return View::make('search', ['products' => [], 'q' => '', 'city_slug' => $citySlug, 'content_ids' => '']);
        }

        $city_id = Session::get('city_id');
        $query = Input::get('q');
        $query = DB::connection()->getPdo()->quote(strip_tags($query));
        $query = get_clean_string($query);
        $elasticSearch = new AWSElasticsearch();
        $rows = 15;

        if (Input::has('page') && Input::get('page')) {
            $page = intval(Input::get('page'));
        } else {
            $rows = 60;
        }

        if (!isset($page)) {
            $begin = 0;
            $page = 1;
        } else {
            $begin = ($page - 1) * $rows;
        }

        if (Config::get('app.aws.elasticsearch.is_enable')) {
            //NUEVO BUSCADOR
            $query = get_clean_string($query);
            $result = $elasticSearch->search($this->warehouse->id, $query, $page, $rows);
            $products = $result['result'][$this->warehouse->id];
        } else {
            //ANTIGUO BUSCADOR
            $query = Product::where('stores.city_id', $city_id);

            $keys = explode(' ', $query);
            if (count($keys)) {
                $keywords = '';
                foreach ($keys as $key) {
                    if (($key != '') and ($key != ' ')) {
                        if ($keywords != '') {
                            $keywords .= '-space-';
                        }
                        $keywords .= $key;
                    }
                }
                $keywords = explode('-space-', $keywords);
                foreach ($keywords as $keyword) {
                    $query->whereRaw('(products.name LIKE "%' . trim($keyword) . '%" COLLATE utf8_spanish_ci OR products.name LIKE "%' . get_plural_string(trim($keyword)) . '%" COLLATE utf8_spanish_ci)');
                }
            }

            $query->select(
                'products.*',
                'store_products.*',
                'store_products.id AS store_product_id',
                'has_warning',
                'cities.slug AS city_slug',
                'stores.slug AS store_slug',
                'departments.slug AS department_slug',
                'shelves.slug AS shelf_slug',
                $this->special_price_query_string,
                StoreProduct::getRawDeliveryDiscountByDate()
            )->distinct()
                ->join('store_products', 'store_products.product_id', '=', 'products.id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->join('shelves', 'store_products.shelf_id', '=', 'shelves.id')
                ->join('departments', 'shelves.department_id', '=', 'departments.id')
                ->join('stores', 'departments.store_id', '=', 'stores.id')
                ->join('cities', 'stores.city_id', '=', 'cities.id')
                ->where('departments.status', 1)
                ->where('shelves.status', 1)
                ->where('warehouse_id', $this->warehouse->id)
                ->where('stores.status', 1)
                ->where('store_product_warehouses.status', 1)
                ->where('store_product_warehouses.is_visible', 1)
                ->where('products.type', '<>', 'Proveedor')
                ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                ->orderBy(
                    DB::raw('CASE
                            WHEN store_products.store_id = ' . (int) $this->store->id . ' THEN 0
                            WHEN store_products.store_id != ' . (int) $this->store->id . ' THEN 1
                        END
                ')
                )
                ->orderBy('stores.id');

            if (!empty($keywords)) {
                foreach ($keywords as $keyword) {
                    $keywords[] = get_plural_string(trim($keyword));
                }
                foreach ($keywords as $keyword) {
                    $query->orderBy(DB::raw('CASE
                                            WHEN products.name LIKE \'' . trim($keyword) . '%\' THEN 0
                                            WHEN products.name LIKE \'% %' . trim($keyword) . '% %\' THEN 1
                                            WHEN products.name LIKE \'%' . trim($keyword) . '\' THEN 2
                                            ELSE 3
                                        END
                                    '));
                }
            }

            $products = $query->orderBy('products.name', 'asc')->get();
        }

        self::trackEventOnBrowser(['products_searched' => [
            'screen' => self::getReferrer(),
            'textSearched' => Input::get('q') ?: '',
            'totalResult' => count($products),
        ]]);

        $content_ids = '';
        if (count($products)) {
            $current_date = Carbon::create();
            $product_ids = [];
            $elasticSearch->setApplyFivePercentDiscount($this->applyFivePercentDiscount());

            foreach ($products as $key => $product) {
                $product_ids[] = $product['store_product_id'];
                $products[$key] = $elasticSearch->clearProductResponse(
                    $product,
                    $current_date,
                    $this->total_orders,
                    $this->last_order_date
                );
            }
            $content_ids = implode("', '", $product_ids);
            $content_ids = "'" . $content_ids . "'";
        } else {
            $search_log = new SearchLog;
            $search = Input::get('q');
            if ($this->user_id) {
                $search_log->user_id = $this->user_id;
            }
            $search_log->search = $search;
            $search_log->ip = Request::getClientIp();
            $search_log->store_id = $this->store->id;
            $search_log->save();
        }
        return View::make('search', ['products' => $products, 'q' => Input::get('q'), 'city_slug' => $citySlug, 'content_ids' => $content_ids]);
    }

    /**
     * Validar cobertura de tienda en direccion
     */
    public function validate_address_coverage_ajax()
    {
        if (Request::ajax()) {
            $city_id = Input::get('city_id');
            $surrounded_city_id = Input::get('surrounded_city_id');
            $store_id = Input::get('store_id');
            $address = Input::get('dir');

            //guardar direccion en sesion
            $address_data = new stdClass;
            $address_data->city_id = $city_id;
            $address_data->surrounded_city_id = $surrounded_city_id;
            $address_data->address = $address;

            if (is_numeric($address)) {
                //direccion guardada
                if ($address = UserAddress::find($address)) {
                    $address_data->id = $address->id;
                    $address_data->address_text = $address_text = $address->address;
                    $address_data->address_further = $address->address_further;
                    $address_data->neighborhood = $address->neighborhood;
                    $address_data->latitude = $address->latitude;
                    $address_data->longitude = $address->longitude;
                    $latitude = $address->latitude;
                    $longitude = $address->longitude;
                    Session::put('possible_address', $address_data);
                }
            } else {
                $address = json_decode($address);
                //direccion escrita
                if (isset($address->dir1)) {
                    //obtener latitud longitud
                    if ($surrounded_city_id)
                        $city = City::find($surrounded_city_id);
                    else $city = City::find($city_id);

                    $address_text = '';
                    foreach ($address as $key => $value) {
                        $address_text .= $value . ' ';
                        if ($key == 'dir2')
                            $address_text .= '# ';
                        if ($key == 'dir3')
                            $address_text .= '- ';
                    }
                    $address_data->address_text = $address_text = trim($address_text);

                    if (!UserAddress::isCorrectAddress($address_text)) {
                        return Response::json(array('status' => false, 'error' => 'invalid_address', 'data' => ''), 200);
                    }

                    Session::put('possible_address', $address_data);
                    $inputs = array('address' => $address_text, 'city' => $city->city);
                    $request = Request::create('/api/location', 'GET', $inputs);
                    Request::replace($request->input());
                    $result = json_decode(Route::dispatch($request)->getContent(), true);

                    if ($result['status']) {
                        $latitude = $result['result']['latitude'];
                        $longitude = $result['result']['longitude'];
                        $address = $result['result']['address'];
                        $neighborhood = $result['result']['neighborhood'];
                        $locality = $result['result']['locality'];
                        $get_data = ['app_version' => 213, 'address' => $inputs['address'], 'city' => $inputs['city']];
                        $address_data->latitude = $latitude;
                        $address_data->longitude = $longitude;

                        //validar direccion en el sur de la ciudad
                        $result_address = UserAddress::validateAddress($result, $get_data);
                        if ($result_address['status']) {
                            if (count($result_address['result']['addresses']) > 1)
                                return Response::json($result_address, 200);
                        }

                        Session::put('possible_address', $address_data);
                    } else {
                        if ($surrounded_city_id)
                            $city = City::find($surrounded_city_id);
                        else $city = City::find($city_id);

                        $data = ['lat' => $city->latitude, 'lng' => $city->longitude];
                        return Response::json(array('status' => false, 'error' => 'wrong_address', 'data' => $data), 200);
                    }
                } else {
                    //coordenadas por mapa
                    if (isset($address->lat)) {
                        // Se debe sobreescribir el valor original de la
                        // dirección en caso de que el usuario haya
                        // seleccionado un valor distinto al que ingreso incialmente.
                        $possible_address = Session::get('possible_address') ?: new StdClass();

                        $possible_address->latitude = $latitude = $address->lat;
                        $possible_address->longitude = $longitude = $address->lng;
                        $has_string_address = !empty($address->address);
                        $address_parts = json_decode(empty($possible_address) ? null : $possible_address->address);
                        $is_south_address = preg_match('/sur/i', $has_string_address ? $address->address : '');
                        $temporal_has_south_address = preg_match('/sur/i', $address_parts ? $possible_address->address : '');

                        if (!empty($address_parts->dir2) && $is_south_address && !$temporal_has_south_address) {
                            $address_parts->dir2 = trim($address_parts->dir2);
                            $address_parts->dir2 .= ' SUR';
                        } elseif (!$is_south_address && $temporal_has_south_address) {
                            $address_parts->dir2 = str_replace('SUR', '', $address_parts->dir2);
                        }

                        if ($has_string_address) {
                            $possible_address->address_text = $address->address;
                        }

                        if (!empty($possible_address)) {
                            $possible_address->address = json_encode($address_parts);
                            Session::put('possible_address', $possible_address);
                        }
                    } else {
                        //si conincide manda direccion
                        if (!$this->is_JSON($address)) {
                            //obtener latitud longitud
                            $city = City::find(Session::get('city_id'));
                            $inputs = array('address' => $address, 'city' => $city->city);
                            $request = Request::create('/api/location', 'GET', $inputs);
                            Request::replace($request->input());
                            $result = json_decode(Route::dispatch($request)->getContent(), true);
                            if (!$result['status'])
                                return Response::json(array('status' => false, 'error' => 'wrong_address'), 200);
                            $latitude = $result['result']['latitude'];
                            $longitude = $result['result']['longitude'];
                        }
                    }
                }
            }

            //validar cobertura en tienda
            try {
                if ($city_id == '' && $store_id == 65) {
                    $city_id = 17;
                };
                $zone = Zone::getByLatLng($this->getCoverageStoreCityId($city_id), $latitude, $longitude);
            } catch (CoverageException $e) {
                return Response::json([
                    'status' => false,
                    'result' => 'no_coverage',
                    'message' => 'No hay cobertura en tu dirección en este momento.',
                ]);
            }

            Session::put('possible_zone_id', $zone->id);
            //obtener texto de direccion
            if (!isset($address_text)) {
                $inputs = array('lat' => $latitude, 'lng' => $longitude);
                $request = Request::create('/api/location_reverse', 'GET', $inputs);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if ($result['status']) {
                    $address_text = $result['result']['address'];
                    $address_data->address_text = $address_text;
                    Session::put('possible_address', $address_data);
                } else $address_text = '';
            }
            Session::put('possible_coverage', 1);
            return Response::json(array('status' => true, 'result' => array('address' => $address_text)), 200);
        } else {
            return Response::json(array('status' => false, 'message' => 'Ocurrió un error al validar cobertura.'), 200);
        }
    }

    public function is_JSON($args)
    {
        json_decode($args);
        return (json_last_error() === JSON_ERROR_NONE);
    }

    /**
     * Inscribir email al newsletter
     */
    public function newsletter()
    {
        if (Request::ajax() && Input::has('email')) {
            if (!$newsletter = Newsletter::where('email', Input::get('email'))->first()) {
                $newsletter = new Newsletter;
                $newsletter->email = Input::get('email');
                $newsletter->save();
            }
            return Response::json(
                [
                    'status' => true,
                    'message' => 'Tu correo electrónico fue inscrito con éxito.'
                ],
                200
            );
        }
        return Response::json(
            [
                'status' => false,
                'message' => 'Ocurrió un error al inscribir tu correo electrónico.'
            ],
            200
        );
    }

    /**
     * Feed de productos de tienda
     */
    public function feed()
    {
        $city_slug = Route::current()->parameter('city_slug');
        $store_slug = Route::current()->parameter('store_slug');

        $valid_access = false;
        $headers = getallheaders();
        if (isset($headers['Authorization']) || isset($headers['authorization'])) {
            $authorization = isset($headers['Authorization']) ? $headers['Authorization'] : $headers['authorization'];
            $authorization = str_replace('Basic ', '', $authorization);
            $authorization = explode(':', base64_decode($authorization));
            if (count($authorization) == 2) {
                if ($authorization[0] == 'merqueofeed' && $authorization[1] == 'jdG$4D3#') {
                    $valid_access = true;
                }
            }
        }

        if (!$valid_access) {
            return Response::make('401 Unauthorized', 401);
        }

        if ($store_slug) {
            // Feed por tienda
            $stores = Store::select('stores.*', 'cities.slug AS city_slug')->join('cities', 'cities.id', '=', 'city_id')
                ->where('cities.slug', $city_slug)
                ->where('stores.slug', $store_slug)
                ->get();
        } else {
            // Feed por ciudad
            $stores = Store::select('stores.*', 'cities.slug AS city_slug')->join('cities', 'cities.id', '=', 'city_id')
                ->where('cities.slug', $city_slug)
                ->get();
        }
        if ($stores) {
            $feed = array();
            foreach ($stores as $store) {
                if ($store_slug) {
                    //feed por tienda
                    $products = Product::select(
                        'products.*',
                        'store_products.*',
                        'has_warning AS warning',
                        'departments.slug AS department_slug',
                        'shelves.slug AS shelf_slug',
                        $this->special_price_query_string,
                        StoreProduct::getRawDeliveryDiscountByDate()
                    )
                        ->join('store_products', 'store_products.product_id', '=', 'products.id')
                        ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
                        ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
                        ->join('departments', 'shelves.department_id', '=', 'departments.id')
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->where('store_products.store_id', $store->id)
                        ->where('store_product_warehouses.status', 1)
                        ->where('store_product_warehouses.is_visible', 1)
                        ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                        ->where('products.type', '<>', 'Proveedor')
                        ->where('warehouse_id', $this->warehouse->id)
                        ->get();
                } else {
                    //feed por ciudad
                    $products = Product::select(
                        'products.*',
                        'store_products.*',
                        'has_warning AS warning',
                        'departments.slug AS department_slug',
                        'shelves.slug AS shelf_slug',
                        DB::raw('SUM(order_products.quantity) AS qty'),
                        $this->special_price_query_string,
                        StoreProduct::getRawDeliveryDiscountByDate()
                    )
                        ->join('store_products', 'store_products.product_id', '=', 'products.id')
                        ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
                        ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
                        ->join('departments', 'shelves.department_id', '=', 'departments.id')
                        ->join('order_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->join('orders', 'orders.id', '=', 'order_products.order_id')
                        ->join('stores', 'stores.id', '=', 'order_products.store_id')
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->where('orders.status', 'Delivered')
                        ->where('order_products.fulfilment_status', 'Fullfilled')
                        ->where('order_products.type', 'Product')
                        ->whereNotNull('order_products.store_product_id')
                        ->where('order_products.id', '<>', 0)
                        ->where('stores.id', $store->id)
                        ->where('store_product_warehouses.status', 1)
                        ->where('store_product_warehouses.is_visible', 1)
                        ->where('warehouse_id', $this->warehouse->id)
                        ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                        ->where('products.type', '<>', 'Proveedor')
                        ->groupBy('order_products.store_product_id')
                        ->having('qty', '>', '3')
                        ->orderBy('qty', 'desc')
                        ->limit(500)
                        ->get();
                }
                foreach ($products as $product) {
                    $feed[] = array(
                        'id' => strval($product->id),
                        'reference' => strval($product->reference),
                        'title' => strval($product->name),
                        'price' => empty($product->special_price) ? strval($product->price) : strval($product->special_price),
                        'image' => strval($product->image_medium_url),
                        'url' => web_url() . '/' . $store->city_slug . '/' . $store->slug . '/' . $product['department_slug'] . '/' . $product['shelf_slug'] . '/' . $product['slug']
                    );
                }
            }

            $products = new stdClass;
            $products->products = $feed;

            return Response::json($products, 200);
        } else {
            App::abort(404);
        }
    }

    public static function getReferrer()
    {
        $pageMapping = [
            [
                'pattern' => '/\/.+\/domicilios-[^\/]+\/[^\/]+\/[^\/]+\/[^\/]+$/',
                'name' => 'product'
            ],
            [
                'pattern' => '/\/.+\/domicilios-[^\/]+\/[^\/]+\/[^\/]+$/',
                'name' => 'shelf'
            ],
            [
                'pattern' => '/\/.+\/domicilios-[^\/]+\/[^\/]+$/',
                'name' => 'department'
            ],
            [
                'pattern' => '/\/.+\/domicilios-[^\/]+$/',
                'name' => 'store'
            ],
            [
                'pattern' => '/\/?checkout/',
                'name' => 'checkout'
            ],
            [
                'pattern' => '/\/?registro/',
                'name' => 'sign_up'
            ],
        ];

        $url = URL::previous();

        foreach ($pageMapping as $page) {
            if (preg_match($page['pattern'], $url)) {
                return $page['name'];
            }
        }
    }

    /**
     * @param array $experiment_names
     * @return array
     */
    private function getExperimentVariant($experiment_names)
    {
        $result = [];
        $is_logged_in = Auth::check();
        $user = Auth::user();

        foreach ($experiment_names as $experiment) {
            if ($is_logged_in) {
                $experiment = new $experiment($user);
                $result[$experiment->getExperimentName()] = $experiment->activate();
            }
        }

        return $result;
    }

    /**
     * Servicio para modal de mejor precio garantizado
     */
    public function best_price()
    {
        if (!Input::get('warehouse_id') || !Input::get('store_id')) {
            return Response::json([
                'status' => false,
                'message' => 'Faltan parametros',
            ], 200);
        }

        $warehouse_id = Input::get('warehouse_id');
        $store_id = Input::get('store_id');
        $products = StoreProduct::select(
            'products.*',
            'store_products.*',
            'has_warning AS warning',
            'shelves.slug AS shelf_slug',
            'departments.slug AS department_slug',
            $this->special_price_query_string,
            StoreProduct::getRawDeliveryDiscountByDate()
        )
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->join('store_product_shelves', 'store_products.id', '=', 'store_product_shelves.store_product_id')
            ->join('shelves', 'store_product_shelves.shelf_id', '=', 'shelves.id')
            ->join('departments', 'shelves.department_id', '=', 'departments.id')
            ->pum()
            ->where('store_products.store_id', $store_id)
            ->where('store_products.is_best_price', '=', 1)
            ->where('products.type', '<>', 'Proveedor')
            ->where('store_product_warehouses.status', 1)
            ->where('departments.status', 1)
            ->where('shelves.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('warehouse_id', $warehouse_id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->orderBy('departments.sort_order')
            ->orderBy('shelves.sort_order')
            ->paginate(40);

        if (!$products->count()) {
            return 'false';
        }

        return View::make('elements.products-list', [
            'products' => $products->all(),
            'store' => $this->store,
            'referrer' => 'best_price'
        ]);
    }

    /**
     * @return array
     */
    private function getRoutesWhichIgnoreFilter()
    {
        return [
            'newsletter',
            'feed',
            'autocomplete',
            'validate_address_coverage_ajax',
            'getBannerDetails'
        ];
    }

    /*
     * A partir del banner actual se obtiene el contenido con el listado de productos
     * y se retorna en formato html para ser renderizado.
     *
     * @param $bannerId
     * @return \Illuminate\Contracts\View\View
     */
    public function getBannerDetails($bannerId)
    {
        $totalOrders = 0;
        $store = Store::findOrFail(Session::get('store_id'));

        if (Auth::check()) {
            $info = User::getOrdersInfo($this->user_id);
            $totalOrders = $info->qty;
        }

        $banner = Banner::publicQuery($this->getCurrentWarehouse(), $store, null, $totalOrders, 'web', true)
            ->with('store.city')
            ->with(['bannerStoreProducts' => function ($query) {
                $query->with('product', 'shelf', 'department', 'store.city')
                    ->orderBy('banner_store_product.id')
                    ->select('store_products.*', $this->special_price_query_string)
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->inWarehouse($this->warehouse)
                    ->pum();
            }])
            ->findOrFail($bannerId);

        return View::make('elements.banner-content', compact('banner'));
    }
}
