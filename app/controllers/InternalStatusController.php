<?php

class InternalStatusController extends Controller
{
    /**
     * Verifica el estado de MySQL y Elasticsearch
     *
     * @return json
     */
    public function checkStatus()
    {
        $message = [
            'mysql' => $this->checkMysql(),
            'elastic' => $this->checkElasticsearch(),
            'redis' => $this->checkRedis()
        ];

        $serviceStatus = $this->serviceStatus($message);

        $response  = Response::make($message, $serviceStatus);
        return $response;
    }

    private function checkMysql()
    {
        $message = 'Hubo un problema con Mysql.';
        $status = false;
        try {
            $pdo = DB::connection()->getPdo();
            if ($pdo) {
                $status = true;
                $message = 'MySQL funcionando.';
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return [
            'status' => $status,
            'message' => $message
        ];
    }

    private function checkElasticsearch()
    {
        $message = 'Hubo un problema con elastic.';
        $status = false;
        try {
            if ($this->statusElastic()) {
                $status = true;
                $message = 'Elasticsearch funcionando.';
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return [
            'status' => $status,
            'message' => $message
        ];
    }

    private function checkRedis()
    {
        $message = 'Hubo un problema con redis.';
        $status = false;
        try {
            $ping = Redis::command('ping');
            if ($ping) {
                $status = true;
                $message = 'Redis funcionando.';
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return [
            'status' => $status,
            'message' => $message
        ];
    }

    private function statusElastic()
    {
        // curl -XGET https://localhost:9200 -u admin:admin --insecure
        $ping = file_get_contents(Config::get('app.aws.elasticsearch.host') . '/_cat/indices?format=json&pretty');
        if (!empty($ping)) {
            $json = json_decode($ping, true);
            if (isset($json[0]) && isset($json[0]['status']) && $json[0]['status'] == 'open') {
                return true;
            }
        }
        return false;
    }

    private function serviceStatus($message)
    {
        foreach ($message as $response) {
            if (!$response['status']) {
                return 500;
            }
        }

        return 200;
    }
}
