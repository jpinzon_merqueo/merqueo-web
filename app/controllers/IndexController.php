<?php

class IndexController extends BaseController
{
    /**
     * Home del portal para seleccionar tiendas en ciudad
     */
    public function index()
    {
        //redireccionar a bogota
        if (!Cookie::get('city_id')) {
            if (isset($_SERVER['HTTP_USER_AGENT']) && !strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'googlebot'))
                return Redirect::route('frontStore.all_departments', ['city_slug' => 'bogota', 'store_slug' => 'super-ahorro'], 301)->with('is_new', 1);
        }

        if ($city_slug = Route::current()->parameter('city_slug')) {
            if ($city = City::where('slug', $city_slug)->first()) {
                //eliminar carrito
                if (Auth::check()) {
                    if (Session::has('cart_id'))
                        CartProduct::where('cart_id', Session::get('cart_id'))->delete();
                } else Session::forget('tmp_cart');

                Session::put('coverage', 0);
                Cookie::queue('city_id', $city->id, 2628000);
                Session::put('city_id', $city->id);
                Session::forget('zone_id');
                Session::forget('address');
            } else App::abort(404);
        }


        $stores = Store::getByLatLng();
        //metadata
        $city = City::remember(10)->find(Session::get('city_id'));
        $metadata['city_name'] = $city->city;
        $metadata['city_slug'] = $city->slug;
        $metadata['stores'] = '';
        if ($n = count($stores)) {
            $cont = 1;
            foreach ($stores as $store) {
                if ($cont != $n)
                    $metadata['stores'] .= $store->name . ', ';
                else {
                    $metadata['stores'] = trim($metadata['stores'], ', ');
                    $metadata['stores'] .= ' y ' . $store->name;
                }
                $cont++;
            }
        } else App::abort(404);

        //redireccionar a tienda
        header("HTTP/1.1 301 Moved Permanently");
        if ($city->id == 2) {
            header("Location: " . Config::get('app.url') . "/medellin/domicilios-super-ahorro");
            //exit;
            //return Redirect::route('frontStore.all_departments', ['city_slug' => 'medellin', 'store_slug' => 'euro-supermercados']);
        } else if ($city->id == 17) {
            header("Location: " . Config::get('app.url') . "/mexico-df/domicilios-super-ahorro");
        } else {
            header("Location: " . Config::get('app.url') . "/bogota/domicilios-super-ahorro");
            //exit;
            //return Redirect::route('frontStore.all_departments', ['city_slug' => 'bogota', 'store_slug' => 'super-ahorro']);
        } 

        $user_discounts = User::getDiscounts();
        $surrounded_cities['city'] = $city;
        $surrounded_cities['surrounded_cities'] = City::where('parent_city_id', $city->id)->where('status', 1)->get();

        return View::make('index')
            ->with('metadata', $metadata)
            ->with('stores', $stores)
            ->with('departments', [])
            ->with('user_addresses', [])
            ->with('cities', City::where('coverage_store_id', $stores[0]->id)->where('is_main', 1)->where('status', 1)->get())
            ->with('surrounded_cities', $surrounded_cities)
            ->with('user_discounts', $user_discounts)
            ->with('warehouse', $this->getCurrentWarehouse())
            ->with('footer', $this->getFooter());
    }
}
