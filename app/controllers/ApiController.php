<?php

class ApiController extends BaseController
{
    const PAGE_LENGTH = 20;

    protected $token;
    protected $auth_user_id;
    protected $user_device_id;
    protected $driver_id;
    protected $oauth_server;
    protected $response;
    protected $status_code = 200;
    protected $response_format = 'json';
    protected $inputs;
    protected $http_method;
    protected $user_agent;
    protected $user_device;
    protected $device_id;
    protected $app_version;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        if (empty(self::$filterer)) {
            return null;
        }

        $this->beforeFilter(function () {
            //API Clientes
            if ($this->checkIfEndpointNeedsAuthorization()) {
                //validar request oauth
                /*try{
                    $request = OAuthRequest::from_request();
                    list($consumer, $token) = $this->oauth_server->verify_request($request);
                }catch(OAuthException $e){
                    return $this->jsonResponse($e->getMessage(), 401);
                }
                //aplicar urldecode a variables por oauth
                $params = Input::all();
                if (count($params)){
                    foreach($params as $key => $value)
                        $new_params[$key] = urldecode($value);
                    Input::replace($new_params);
                }*/
                //filtrar acceso a metodos privados
                $this->auth_user_id = Input::get('user_id') ?: 0;
                if (!$this->auth_user_id) {
                    if (Request::segment(3) === 'user') {
                        $this->auth_user_id = Route::current()->parameter('id');
                    }
                }

                //obtener sistema operativo
                $headers = getallheaders();
                if (isset($headers['X-Merqueo-Agent'])) {
                    $this->user_agent = strstr($headers['X-Merqueo-Agent'], 'iOS') ? 'iOS' : 'Android';
                }

                //validar token de acceso
                if (isset($headers['Authorization']) || isset($headers['authorization'])) {
                    $token = isset($headers['Authorization']) ? $headers['Authorization'] : $headers['authorization'];
                    $aes = new Crypt_AES(CRYPT_MODE_CBC);
                    $aes->setKey(Config::get('app.api.public_key'));
                    $aes->setIV(Config::get('app.api.iv'));
                    try {
                        $token = $aes->decrypt(base64_decode($token));
                    } catch (Exception $e) {
                        return $this->jsonResponse('Acceso denegado d.', 401);
                    }
                  $token = explode('|', $token);
                    if (count($token) >= 3) {
                        //validar hora
                        $datetime_device = $token[0];
                        $datetime_server = date('Y-m-d H:i:s');
                        $interval = abs($datetime_device - strtotime($datetime_server));
                        $minutes = round($interval / 60);
                        if ($minutes > 70)
                            return $this->jsonResponse('Acceso denegado t.', 401);
                        //validar url servicio
                        if ($token[1] != Request::getPathInfo())
                            return $this->jsonResponse('Acceso denegado p.', 401);
                        //validar api key
                        if ($token[2] != Config::get('app.api.key'))
                            return $this->jsonResponse('Acceso denegado k.', 401);
                      $this->app_version = empty($token[3]) ? Input::get('app_version') : $token[3];
                        $this->device_id = empty($token[4]) ? null : $token[4];
                      // Algunas funciones necesitan el item "app_version" para realizar
                        // las validaciones según la versión.
                        Input::merge(['app_version' => $this->app_version]);
                  }else return $this->jsonResponse('Acceso denegado s.', 401);
                }else return $this->jsonResponse('Acceso denegado h.', 401);
            } else {
                Event::subscribe(new OrderEventHandler);
            }
        }, array('except' => array('init',
                    'cities', 'request_token', 'authorize_request_token', 'request_access_token',
                    'delivery_zone', 'location', 'location_reverse', 'create_remote_order', 'adjust',
                    'get_picker_storage', 'upload_storage_image', 'save_picker_storage','halt_picker_storage',
                    'register_news','get_response_news','upload_image','save_pickup_products','upload_reference_image'
                )
            )
        );
    }

    /**
     * Retorna repuesta de request al API
     *
     * @param string $message Mensaje de respuesta
     * @param string $status_code Codigo HTTP de respuesta
     *
     * @return \Illuminate\Http\JsonResponse|array|string $response JSON con respuesta de servicio
     */
    public function jsonResponse($message = null, $status_code = null)
    {
        if ($message) $this->response['message'] = $message;
        if ($status_code) $this->setStatusCode($status_code);

        //registrar log
        try{
            $response_headers = '';
            $trace = debug_backtrace();
            $caller = $trace[1];
            $function = $caller['function'];
            if (isset($caller['class']))
                $function .= ' in '.$caller['class'];
            $request = $this->inputs;

            //encriptacion de tarjeta de credito
            if(isset($request['number_cc']) && !empty($request['number_cc'])){
                $request['number_cc'] = substr($request['number_cc'], 0, 6).'******'.substr($request['number_cc'], 12, 16);
                $request['code_cc'] = '***';
            }

            $request = json_encode($request);
            $request_headers = '';
            foreach (getallheaders() as $name => $value) {
               $request_headers .= "\r\n\t".$name.": ".$value;
            }

            $url = URL::current();
            $response_log = $this->getResponseFormat() == 'json' ? json_encode($this->response) : $this->response['message'];

            if (!$this->response['status'] && $this->getStatusCode() == 401){
                $response = Response::make($this->response['message'], $this->getStatusCode());
                $response->header('WWW-Authenticate', 'OAuth realm="https://merqueo.com/"');
            }else{
                 if ($this->getResponseFormat() == 'json')
                     $response = Response::json($this->response, $this->getStatusCode());
                 else $response = Response::make($this->response['message'], $this->getStatusCode());
            }

            foreach ($response->headers as $name => $value) {
                $response_headers .= "\r\n\t".$name.": ".$value[0];
            }

        }catch(Exception $e) {
            if (!isset($url))
               $url = 'Not defined';
            $request = is_string($request) ? $request : 'Error en json_encode: Invalid UTF-8 sequence in argument';
        }

        if (!$this->response['status'] && DB::transactionLevel() == 1)
            DB::rollback();

        if (Config::get('app.api_log')) {
            //guardar log
            $log = new ApiLog;
            $log->function = $function;
            $log->url = $url;
            $log->response_http_code = $this->getStatusCode();
            $request_string = "METHOD: " . $function;
            $request_string .= "\r\nURL: " . $url . " " . $this->http_method;
            $request_string .= "\r\nREQUEST HEADERS: " . $request_headers;
            $request_string .= "\r\nREQUEST: " . $request;
            $request_string .= "\r\nRESPONSE HEADERS: " . $response_headers;
            $request_string .= "\r\nRESPONSE: " . $response_log;
            $log->request = $request_string;
            $log->ip = get_ip();
            $log->save();
        }

        //guardar log de error en checkout
        if (!$this->response['status'] && $this->http_method == 'POST')
        {
            $urls_log = array(
                web_url().'/api/1.3/checkout',
                web_url().'/api/1.3/coupon',
            );
            if (in_array($url, $urls_log))
            {
                $log = new CheckoutLog;
                $log->url = $url;
                $log->request = "URL: ".$url." POST\r\nREQUEST HEADERS: ".$request_headers."\r\nREQUEST: ".$request;
                $log->message = $this->response['message'];
                $log->response = $response_log;
                $log->ip = get_ip();
                $log->save();
            }
        }

        return $response;
    }

    /**
     * Obtiene codigo HTTP de respuesta
     *
     * @return int $status_code codigo HTTP de respuesta
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * Actualiza codigo HTTP de respuesta
     *
     * @param int $status_code codigo HTTP de respuesta
     */
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
    }

    /**
     * Obtiene formato HTTP de respuesta
     *
     * @return string $response_format formato HTTP de respuesta
     */
    public function getResponseFormat()
    {
        return $this->response_format;
    }

    /**
     * Actualiza formato HTTP de respuesta
     *
     * @param string $response_format formato HTTP de respuesta
     */
    public function setResponseFormat($response_format)
    {
        $this->response_format = $response_format;
    }

    /**
     * Setea error en el servidor
     *
     * @param int $code codigo HTTP de respuesta
     * @param int $message Mensaje de error
     */
    public function internalError($code = 500, $message = 'Error interno')
    {
        $this->setStatusCode($code);
        if (!empty($message))
            $this->response['message'] = $message;

        return $this->jsonResponse();
    }

    /**
     * Obtener informacion del usuario
     *
     * @param int $id ID del usuario
     * @return array $user Informacion del usuario
     */
    protected function getUserData($id)
    {
        $user = User::find($id);
        if ($user)
        {
            $addresses = UserAddress::select('user_address.*', DB::raw('IF (parent_city_id IS NULL, city_id, parent_city_id) AS parent_city_id'))
                                     ->join('cities', 'city_id', '=', 'cities.id')
                                     ->where('user_id', $user->id)->get()->toArray();
            $credit_cards = UserCreditCard::select('id', 'last_four', 'type', 'holder_document_number AS document_number')->where('user_id', $user->id)->get()->toArray();
            $orders = Order::select('orders.id', 'orders.reference', 'orders.status', 'orders.store_id', 'orders.date', 'orders.total_products', 'orders.total_amount', 'orders.delivery_date',
                            'orders.delivery_time', 'orders.driver_id', 'orders.total_amount AS subtotal', 'orders.delivery_amount', 'orders.discount_amount', 'orders.payment_method', 'user_score',
                            'order_groups.user_city_id AS city_id', DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"), 'user_score_date',
                            'stores.name AS store_name', 'stores.app_logo_small_url AS store_logo_url', 'stores.is_storage', 'orders.driver_id', 'orders.management_date')
                            ->join('order_groups', 'group_id', '=', 'order_groups.id')
                            ->join('stores', 'orders.store_id', '=', 'stores.id')
                            ->where('orders.is_hidden', 0)
                            //->where('source', '<>', 'Reclamo')
                            ->where('orders.user_id', $user->id)
                            ->orderBy('orders.id', 'DESC')
                            ->limit(10)
                            ->get();

            $count_orders = $user->orderGroups()->count();
            $can_refer = !$count_orders && !$user->referred_by ? 1 : 0;

            $status = array(
                'Validation' => 'Iniciado',
                'Initiated' => 'Iniciado',
                'Enrutado' => 'En Proceso',
                'In Progress' => 'En Proceso',
                'Alistado' => 'En Proceso',
                'Dispatched' => 'En Camino',
                'Delivered' => 'Entregado',
                'Cancelled' => 'Cancelado'
            );
            $orders_user = array();
            $cloudfront_url = Config::get('app.aws.cloudfront_url');
            $user_city_id = 1;
            $order_id_to_score = $user_last_order_date = 0;
            foreach($orders as $key => $order)
            {
                if ($key == 0){
                    $user_last_order_date = date('Ymd', strtotime($order->date));
                    if ($user_last_order_date >= date('Ymd', strtotime('-5 days')) && empty($order->user_score_date) && $order->status == 'Delivered'){
                        $order_id_to_score = $order->id;
                    }
                }

                if ($driver = Driver::find($order->driver_id)){
                    $order->shopper = array(
                        'image' => !empty($driver->photo_url) ? $driver->photo_url : $cloudfront_url.'/shoppers/photos/avatar.png',
                        'name' => $driver->first_name.' '.$driver->last_name,
                        'phone' => $driver->phone,
                        'shopper_score_average' => 4
                    );
                }
                $order->status = $status[$order->status];
                $order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;

                $order->products = OrderProduct::select(
                    'store_product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name',
                    'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit',
                    'fulfilment_status AS status', 'product_comment AS comment'
                )
                    ->where('order_id', $order->id)
                    ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                    ->get()
                    ->toArray();

                //no enviar productos
                if (Input::has('app_version'))
                {
                    $headers = getallheaders();
                    $user_agent = isset($headers['User-Agent']) && strstr($headers['User-Agent'], 'iOS') ? 'iOS' : 'Android';
                    $app_version = Input::get('app_version');
                    $app_version = intval(str_replace('.', '', $app_version));
                    if ('Android' == $user_agent && $app_version >= 218) {
                        $order->products = [];
                    }
                }

                $orders_user[] = $order->toArray();

                if ($key == 9)
                    break;
            }

            $user_discounts = User::getDiscounts($user->id, null, $this->isNewDevice());
            $user_data = array(
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'fb_id' => $user->fb_id,
                'referral_code' => $user->referral_code,
                'number_orders' => $count_orders,
                'last_order_date' => $user_last_order_date,
                'credit_available' => $user_discounts['amount'],
                'free_delivery_days' => $user_discounts['free_delivery_days'],
                'free_delivery_expiration_date' => format_date('normal_long', $user->free_delivery_expiration_date),
                'free_delivery_next_order' => $user_discounts['free_delivery_next_order'],
                'can_refer' => $can_refer,
                'oauth_verifier' => $user->oauth_verifier,
                'phone_validated' => empty($user->phone_validated_date) ? 0 : 1,
                'phone_validated_message' => 'Valida que el '.$user->phone.' te pertenece. A través de este número nos contactaremos contigo en caso de ser necesario.',
                'has_visa_purchases' => false,//$user->ordersWithVisa()->count() > 0,
                'send_advertising' => $user->send_advertising,
                'status' => $user->status
            );

            $user_products = [];
            if (Input::has('store_id')){
                $warehouse = $this->getCurrentWarehouse();
                if ($user_products = $user->getProducts(Input::get('store_id'), $warehouse->id))
                    $user_products->toarray();
            }

            return array(
                'id' => $user->id, //temporal para versiones 1.0.8 iOS y 1.1.9 Android
                'first_name' => $user->first_name, //temporal para versiones 1.0.8 iOS y 1.1.9 Android
                'last_name' => $user->last_name,//temporal para versiones 1.0.8 iOS y 1.1.9 Android
                'user' => $user_data,
                'user_products' => $user_products,
                'addresses' => $addresses,
                'credit_cards' => $credit_cards,
                'orders' => $orders_user,
                'order_id_to_score' => $order_id_to_score
            );
        }

        return false;
    }

    /**
     * Obtener informacion del usuario
     *
     * @param int $id ID del usuario
     * @return array $user Informacion del usuario
     */
    protected function getUser($id)
    {
        $user = User::find($id);
        if ($user)
        {
            $user_orders_info = User::getOrdersInfo($user->id);

            $user_data =[
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'orders_qty' => $user_orders_info->qty,
                'last_order_date' => $user_orders_info->last_order_date ? $user_orders_info->last_order_date : 0,
                'send_advertising' => $user->send_advertising,
                'status' => $user->status
            ];

            return [
                'info' => $user_data,
                'order_id_pending_score' => $user_orders_info->order_id_pending_score
            ];
        }

        return false;
    }

    /**
     * Recibe una direccion y la transforma en latitud longitud
     *
     * @return object JSON array
     */
    public function location()
    {
        $response = array('status' => false);

        $address = Input::get('address');
        $city = Input::get('city');
        $use_google = Input::has('use_google') ? false : true;

        if(empty($address) || empty($city))
            return Response::json($response, 200);

        if (!$city = City::where('slug', $city)->first())
            return Response::json($response, 200);

        if ($city){
            $city_name = str_replace(array ('á','é','í','ó','ú', 'ü'), array ('a','e','i','o','u', 'u'), $city->city);
            $result = $this->SitiMapas($address, $city_name);
            if (!$result['status'] && isset($result['error']) && $use_google)
                $result = $this->GoogleMaps($address, $city_name);

            //$result = $this->GeoApps($address, $city);
            //$this->MapasLatinos($address, $city);
            //$latlng = $this->GoogleMaps($address, $city);

            if (!$result['status']){
                $user_address_log = new UserAddressLog;
                $user_address_log->address = $address;
                $user_address_log->city_id = $city->id;
                $user_address_log->type = 'wrong_address';
                if (Auth::check())
                    $user_address_log->user_id = Auth::user()->id;
                $user_address_log->save();
            }else{
                $result['city_id'] = $city->id;
                $response = array('status' => true, 'result' => $result);
            }
        }

        return Response::json($response, 200);
   }

    /**
     * Recibe una latitud longitud y la transforma en una direccion
     *
     * @return object JSON array
     */
    public function location_reverse()
    {
        $lat = Input::get('lat');
        $lng = Input::get('lng');

        if(empty($lat) || empty($lng))
            return Response::json(array('status' => false), 200);

        $address = $this->GoogleMapsReverse($lat, $lng);
        $response_array = array('status' => $address !== false, 'result' => $address);
            return Response::json($response_array, 200);
   }

   /**
    * Recibe una direccion y la transforma en latitud longitud con SitiMapas
    *
    * @param string $address Direccion
    * @param string $city Ciudad
    * @return object JSON array
    */
   private function SitiMapas($address, $city)
   {
        $data = array(
            'address' => $address,
            'city' => $city
        );

        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,'https://sitidata-stdr.appspot.com/api/geocoder');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            $headers = ['Authorization: '.Config::get('app.sitimapas_token'), 'Content-type: application/x-www-form-urlencoded'];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $response = curl_exec ($ch);
            $response_http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close ($ch);

            $result = json_decode($response);

        }catch(Exception $e){
            $response_http_code = 500;
            $response = $e->getMessage();
        }

        //guardar log
        $data = json_encode($data);
        $request_headers = '';
        foreach ($headers as $value) {
            $request_headers .= '
  '.$value;
        }
        $date = date('Y-m-d H:i:s');
        $ip = get_ip();
        $log = new AddressLog;
        $log->provider = 'sitimapas';
        $log->function = 'location in ApiController';
        $log->url = web_url().'/api/location POST';
        $log->response_http_code = $response_http_code;
$request_string = 'METHOD: '.$log->function;
        $request_string .= '
URL: '.$log->url;
        $request_string .= '
REQUEST HEADERS: '.$request_headers;
        $request_string .= '
REQUEST: '.$data;
        $request_string .= '
RESPONSE: '.$response;
        $log->request = $request_string;
        $log->ip = $ip;
        $log->created_at = $log->updated_at = $date;
        $log->save();

        if (isset($result) && isset($result->data->latitude) && $result->data->latitude != 0){
            return array(
                'status' => true,
                'latitude' => $result->data->latitude,
                'longitude' => $result->data->longitude,
                'address' => $result->data->dirtrad,
                'readable' => $address,
                'neighborhood' => $result->data->barrio,
                'locality' => $result->data->localidad
            );
        }

        if ($response_http_code == 500)
            return ['status' => false, 'error' => true];

        return ['status' => false, 'error' => true];
   }

   /**
     * Recibe una direccion y la transforma en latitud longitud con Google Maps
     *
     * @param string $address Direccion
     * @param string $city Ciudad
     * @return object JSON array
     */
    private function GoogleMaps($address, $city)
    {
        $search = $address.', '.ucfirst(str_replace('-', ' ', $city)).', Colombia';
        $city = strtolower(str_replace(' ', '-', $city));

        $url = sprintf(
            'https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s',
            urlencode($search),
            Config::get('app.planning_google_api_key')
        );

        try{
            $response = file_get_contents($url, false);
            $response_http_code = 200;
            $result = json_decode($response, true);
        }catch(Exception $e){
            $response_http_code = 500;
            $response = $e->getMessage();
        }

        //guardar log
        $data = json_encode(array('address' => $address, 'city' => $city));
        $request_headers = '';
        $date = date('Y-m-d H:i:s');
        $ip = get_ip();
        $log = new AddressLog;
        $log->provider = 'google';
        $log->function = 'location in ApiController';
        $log->url = web_url().'/api/location GET';
        $log->response_http_code = $response_http_code;
$request_string = 'METHOD: '.$log->function;
        $request_string .= '
URL: '.$log->url;
        $request_string .= '
REQUEST HEADERS: '.$request_headers;
        $request_string .= '
REQUEST: '.$data;
        $request_string .= '
RESPONSE: '.$response;
        $log->request = $request_string;
        $log->ip = $ip;
        $log->created_at = $log->updated_at = $date;
        $log->save();

        if ($city = City::where('slug', $city)->first())
            $city = $city->city;
        else $city = '';

        $neighborhood = '';
        $locality = '';

        if (isset($result['status']) && $result['status'] == 'OK' && isset($result['results'][0]['geometry']['location']['lat'])
        && !empty($result['results'][0]['formatted_address']) && strstr($result['results'][0]['formatted_address'], $city)) {
            foreach ($result['results'][0]['address_components'] as $address_component) {
                if ($address_component['types'][0] == 'neighborhood')
                    $neighborhood = $address_component['long_name'];

                if ($address_component['types'][0] == 'political')
                    $locality = $address_component['long_name'];
            }

            return array(
                'status' => true,
                'latitude' => $result['results'][0]['geometry']['location']['lat'],
                'longitude' => $result['results'][0]['geometry']['location']['lng'],
                'address' => explode(',', $result['results'][0]['formatted_address'])[0],
                'readable' => $address,
                'neighborhood' => $neighborhood,
                'locality' => $locality
            );
        }

        return ['status' => false];
    }

    /**
     * Recibe latitud longitud y retorna direccion con Google Maps
     *
     * @param string $latitude Latitud
     * @param string $longitude Longitud
     * @return string Direccion
     */
    private function GoogleMapsReverse($latitude, $longitude)
    {
        return false;

        $url = sprintf(
            'https://maps.googleapis.com/maps/api/geocode/json?latlng=%s&key=%s',
            $latitude.','.$longitude, Config::get('app.google_api_key')
        );

        if (!$result = @file_get_contents($url, false))
            return false;

        $result = json_decode($result, true);

        //debug($result);

        $neighborhood = '';
        $locality = '';

        if (isset($result['status']) && $result['status'] == 'OK' && isset($result['results'][0]['formatted_address'])){
            $address = explode(',', $result['results'][0]['formatted_address']);
            $address = !is_numeric($address[0]) ? $address[0] : $address[1];

            foreach ($result['results'][0]['address_components'] as $address_component) {
                if ($address_component['types'][0] == 'neighborhood')
                    $neighborhood = $address_component['long_name'];

                if ($address_component['types'][0] == 'political')
                    $locality = $address_component['long_name'];
            }

            return array(
                'address' => $address,
                'neighborhood' => $neighborhood,
                'locality' => $locality
            );
        }

        return false;
    }

    /**
    * Recibe una direccion y la transforma en latitud longitud con GeoApps
    *
    * @param string $search Direccion
    * @param string $city Ciudad
    * @return object JSON array
    */
   private function GeoApps($search, $city) {

       return array(
            'latitude' => 1,
            'longitude' => 1,
            'address' => '',
            'readable' => $search
        );

        $url = sprintf(
            'http://geocoder.geoapps.co/geocode/co/%s?address=%s',
            $city, urlencode($search)
        );

        $username = Config::get('app.geoapps_credentials.key');
        $password = Config::get('app.geoapps_credentials.secret');

        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));

        if (!$result = @file_get_contents($url, false, $context))
            return false;

        $geo_json = json_decode($result, true);

        return array(
            'latitude' => $geo_json['coordinates'][1],
            'longitude' => $geo_json['coordinates'][0],
            'address' => $geo_json['properties']['address'],
            'readable' => $search
        );
   }

   /**
    * Recibe una latitud longitud y la transforma en una direccion con GeoApps
    *
    * @param string $lat Latitud
    * @param string $lng Longitud
    * @return string Direccion
    */
   private function GeoAppsReverse($lat, $lng)
   {
        //return '';

        $url = sprintf(
            'http://geocoder.geoapps.co/v2/reverse?lon=%s&lat=%s',
            $lng, $lat
        );

        $username = Config::get('app.geoapps_credentials.key');
        $password = Config::get('app.geoapps_credentials.secret');

        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));

        if (!$result = @file_get_contents($url, false, $context))
            return false;

        $geo_json = json_decode($result, true);

        return $geo_json['response']['properties']['address'];
   }

    /**
    * Recibe una direccion y la transforma en latitud longitud con MapasLatinos
    *
    * @param string $search Direccion
    * @param string $city Ciudad
    * @return object JSON array
    */
    private function MapasLatinos($search, $city)
    {
        $url  = sprintf(
            'http://mapaslatinos.com/geocode_prompt.jsp?country=co&city=%s&sitio=&addint=&tipo=rbdir&address=%s',
            $city, urlencode($search)
        );

        $result = file_get_contents($url);
        $dom = new DOMDocument();

        if( @$dom->loadHTML($result) ) {
            $latitude =  null;
            $longitude = null;

            $selects = $dom->getElementsByTagName('select');
            foreach($selects as $select) {
                if($select->getAttribute('name') === 'addresses') {
                    $options = $select->getElementsByTagName('option');
                    foreach($options as $option) {
                        if(strpos('no encontrada', $option->nodeValue) === FALSE) {
                            $latLong = explode(', ', $option->getAttribute('value'));

                            if(isset($latLong[1])) {
                                $latitude = trim($latLong[1]);
                                $longitude = trim($latLong[0]);
                                break;
                            }
                        }
                    }
                }
            }

            if($latitude == null || $longitude == null) return false;
            else return array('latitude' => $latitude, 'longitude' => $longitude);
        }

        return false;
    }

    /**
    * @param $to_validate
    * @return array|\Illuminate\Http\JsonResponse|string
    */
    protected function validateAction($to_validate)
    {
        $validator = \Validator::make(\Input::all(), [
            'action' => 'required|in:' . implode(',', $to_validate),
        ]);

        if ($validator->fails()) {
            $this->response = [
                'status' => false,
                'result' => $validator->messages()
            ];

        return $this->jsonResponse('La acción utilizada no es valida.', 400);
        }
    }

    /**
     * Obtiene el UserDevice actual
     *
     * @return UserDevice|null
     */
    protected function getCurrentDevice()
    {
        if ($this->user_device) {
            return $this->user_device;
        }

        if ($this->device_id) {
            $this->user_device = UserDevice::where('regid', $this->device_id)->first();

            return $this->user_device;
        }

        return null;
    }

    /**
     * Registra el dispositivo actual
     *
     * @param User $user
     * @return UserDevice|null
     */
    protected function registerCurrentDevice(User $user)
    {
        if (empty($this->device_id)) {
            return null;
        }

        $this->user_device = new UserDevice();
        $this->user_device->user()->associate($user);
        $this->user_device->os = mb_strtolower($this->user_agent);
        $this->user_device->regid = $this->device_id;
        $this->user_device->save();

        return $this->user_device;
    }

    /**
     * Determina si en el dispositivo actual ya se hicieron pedidos
     *
     * @return bool
     */
    protected function isNewDevice()
    {
        if (empty($this->device_id)) {
            return false;
        }

        $current_device = $this->getCurrentDevice();
        return empty($current_device) ?: $current_device->totalOrders === 0;
    }

    /**
     * Determina si el dispoitivo desde donde se realiza la petición es ios.
     *
     * @return bool
     */
    public function isIos()
    {
		$headers = getallheaders();
		if (isset($headers['X-Merqueo-Agent'])) {
			$this->user_agent = strstr($headers['X-Merqueo-Agent'], 'iOS') ? 'iOS' : 'Android';
		}

        return $this->user_agent === 'iOS';
    }

    /**
     * @return mixed
     */
    public function hideCigarettes()
    {
        return !\Config::get('app.merqueo.show_cigarettes') && $this->isIos();
    }

    /**
     * Verifica si el endpoint que se está consultando necesita autorización
     *
     * @return boolean
     */
    public function checkIfEndpointNeedsAuthorization()
    {
        return !Request::is('api/transporter/*')
            && !Request::is('api/picking/*')
            && !Request::is('api/sap/*')
            && !Request::is('api/1.3/store/*/banners')
            && !Request::is('api/1.3/store/*/banners/*')
            && !Request::is('api/1.3/store/*/departments-menu')
            && !Request::is('api/1.3/store/*/department/*');
    }
}
