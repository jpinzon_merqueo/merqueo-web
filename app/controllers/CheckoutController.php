<?php

use Campaigns\Campaigns\BancolombiaCampaign;
use Campaigns\Campaigns\VisaCampaign;
use exceptions\MerqueoException;
use Illuminate\Database\QueryException;
use Leanplum\Message\Request\Track;
use orders\OrderStatus;
use \orders\discounts\DeliveryDiscountByProduct;
use \orders\discounts\FreeDeliveryDiscount;
use \Carbon\Carbon;
use usecases\Interfaces\Checkout\DeliveryDiscountUseCaseInterface;

/**
 * Class CheckoutController
 */
class CheckoutController extends BaseController
{
    const REMOVED_PRODUCTS_SESSION_KEY = 'removed_products';
    const MUST_TO_SET_ADDRESS_SESSION_KEY = 'need_some_address';

    /**
     * @var mixed
     */
    public $cart_id;

    /**
     * @var VisaCampaign
     */
    private $visaCampaign;

    /**
     * @var BancolombiaCampaign
     */
    private $bancolombiaCampaign;

    /**
     * @var DeliveryDiscountUseCaseInterface
     */
    private $deliveryDiscountUseCase;

    /**
     * CheckoutController constructor.
     * @param VisaCampaign $visaCampaign
     * @param BancolombiaCampaign $bancolombiaCampaign
     * @param DeliveryDiscountUseCaseInterface $deliveryDiscountUseCase
     */
    public function __construct(
        VisaCampaign $visaCampaign,
        BancolombiaCampaign $bancolombiaCampaign,
        DeliveryDiscountUseCaseInterface $deliveryDiscountUseCase
    ) {
        parent::__construct();
        $this->visaCampaign = $visaCampaign;
        $this->bancolombiaCampaign = $bancolombiaCampaign;
        $this->deliveryDiscountUseCase = $deliveryDiscountUseCase;
        $this->beforeFilter(function () {
            if (!Session::has('store_id')) {
                $this->store = Store::select('stores.*', 'cities.city AS city_name', 'cities.slug AS city_slug', 'cities.latitude AS city_lat', 'cities.longitude AS city_lng')
                    ->join('cities', 'cities.id', '=', 'stores.city_id')
                    ->where('cities.id', Session::get('city_id'))
                    ->where('stores.status', 1)
                    ->first();
            } else {
                $this->store = Store::select('stores.*', 'cities.city AS city_name', 'cities.slug AS city_slug', 'cities.latitude AS city_lat', 'cities.longitude AS city_lng')
                    ->join('cities', 'cities.id', '=', 'stores.city_id')
                    ->where('stores.id', Session::get('store_id'))
                    ->where('stores.status', 1)
                    ->first();
            }
            if (!Session::has('cart_id')) {
                Session::put('cart_id', 0);
            }

            $this->user_id = Auth::check() ? Auth::user()->id : 0;
            $lat = Session::get('latitude') ?: 1;
            $lng = Session::get('longitude') ?: 1;
            $this->stores = Store::getByLatLng($lat, $lng);
            $slot = Store::getDeliverySlot($this->store->id);
            $this->store->is_open = $slot['is_open'];
            $this->store->slot = $slot['next_slot'];
            $this->cart_id = Session::get('cart_id');
            $this->user_discounts = User::getDiscounts();
        }, ['except' => ['coupon']]);
    }

    /**
     * Resumen de pedido previo checkout
     */
    public function index()
    {
        $addresses = [];
        $credit_cards = [];
        $invoice_data = [];

        if (!Session::has('address')) {
            $city = City::find(Session::get('city_id'));
            Session::forget('coverage');
            Session::put(self::MUST_TO_SET_ADDRESS_SESSION_KEY, true);

            return Redirect::route(
                'frontStore.all_departments',
                [
                    'city_slug' => $city->slug,
                    'store_slug' => 'super-ahorro'
                ]
            );
        }

        if (Auth::check()) {
            $user = User::find($this->user_id);
            $addresses = UserAddress::select('user_address.*', 'cities.city')
                ->join('cities', 'city_id', '=', 'cities.id')
                ->where('user_id', $this->user_id)
                ->where(function ($query) {
                    $query->where('city_id', Session::get('city_id'));
                    $query->orWhere('parent_city_id', Session::get('city_id'));
                })
                ->get();

            $addresses = $addresses->toArray();
            $credit_cards = UserCreditCard::where('user_id', $this->user_id)->get()->toArray();
            $cart = Cart::where('user_id', $this->user_id)->orderBy('updated_at', 'desc')->first();
            if ($cart) {
                Session::put('cart_id', $cart->id);
            }

            $last_order = Order::select('orders.reference')
                ->where('user_id', $this->user_id)
                ->where('status', '<>', 'Cancelled')
                ->orderBy('id', 'desc')
                ->first();

            $invoice_data = $this->getInvoiceData($this->user_id);

            Session::put('has_orders', !empty($last_order));
        }

        $post = Session::has('post') ? Session::get('post') : array();

        //fecha vencimiento de tarjeta de credito
        for ($i = 0; $i <= 9; $i++) {
            $dates_cc['years'][] = date('Y') + $i;
        }
        for ($i = 1; $i <= 12; $i++) {
            if ($i < 10) {
                $i = '0' . $i;
            }
            $dates_cc['months'][] = $i;
        }

        //obtener ids de productos y id de tienda en el carrito
        $warehouse = $this->getCurrentWarehouse();
        $cart_product_info = $store_product_ids = array();
        $total_products = 0;
        $total_items = 0;
        if (Auth::check()) {
            $cart_id = Session::get('cart_id');
            $cart_products = CartProduct::select('store_products.id', 'store_products.store_id', 'cart_products.quantity', 'cart_products.quantity_full_price')
                ->join('carts', 'carts.id', '=', 'cart_products.cart_id')
                ->join('store_products', 'cart_products.store_product_id', '=', 'store_products.id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                ->where('cart_id', $cart_id)
                ->where('user_id', Auth::user()->id)
                ->orderBy('carts.updated_at', 'desc')
                ->get();
            $total_products = count($cart_products);
            if ($cart_products) {
                foreach ($cart_products as $store_product) {
                    $store_product_ids[] = $store_product->id;
                    $store_id = $store_product->store_id;
                    $total_items += $store_product->quantity;
                    $cart_product_info[$store_product->id] = [
                        'quantity' => $store_product->quantity,
                        'quantity_full_price' => $store_product->quantity_full_price
                    ];
                }
            } else {
                $cart = (object) array('products' => (object) array());
            }
        } else {
            if (Session::has('tmp_cart')) {
                $cart = json_decode(Session::get('tmp_cart'));
                $total_products = count((array) $cart);
                foreach ($cart->products as $cart_product) {
                    if (!in_array($cart_product->store_product_id, $store_product_ids)) {
                        $store_product_ids[] = $cart_product->store_product_id;
                        $total_items += $cart_product->cart_quantity;
                        $cart_product_info[$cart_product->store_product_id] = [
                            'quantity' => $cart_product->cart_quantity,
                            'quantity_full_price' => $cart_product->cart_quantity_full_price
                        ];
                    }
                }
                $store_id = StoreProduct::join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                    ->whereIn('store_products.id', $store_product_ids)
                    ->groupBy('store_id')
                    ->pluck('store_id');
            }
        }

        //IDs para Facebook Pixels
        $content_ids = implode("', '", $store_product_ids);
        $content_ids = "'" . $content_ids . "'";

        if (!isset($store_id) || !$store_id)
            $store_id = 63;

        $payment_methods = $this->config['payment_methods'];
        if (Auth::check() && in_array(Auth::user()->type, User::DARKSUPERMARKET_TYPE)) {
            $payment_methods = $this->config['payment_methods_darksupermaket'];
        }
        /*if (in_array(62, $store_ids))
            unset($payment_methods['Tarjeta de crédito']);*/

        $checkout_discounts = Order::getCheckoutDiscounts(Session::get('city_id'));

        //validar si el usuario tiene domiclio gratis
        $has_free_delivery = false;
        $subtotal_cart = 0;
        $delivery_discount = 0;

        if ($cart_product_info) {
            //verificar si el usuario tiene pedidos
            $user_has_orders = Order::select('orders.id')->where('user_id', $this->user_id)->orderBy('id', 'desc')->first();
            $store_products = StoreProduct::whereIn('store_products.id', $store_product_ids)->get()->toArray();
            foreach ($store_products as $store_product) {
                $delivery_discount += $store_product['delivery_discount_amount'];
                //obtener precio de producto (full, especial / primera compra)
                if ($store_product['special_price'] > -1) {
                    //$response['cart']['has_special_price'] = true;
                    if ($store_product['first_order_special_price'])
                        if (!$user_has_orders)
                            $price = $store_product['special_price'];
                        else $price = $store_product['price'];
                    else $price = $store_product['special_price'];
                } else $price = $store_product['price'];

                $sub_total = $cart_product_info[$store_product['id']]['quantity'] * $price;
                if ($cart_product_info[$store_product['id']]['quantity_full_price'])
                    $sub_total += $cart_product_info[$store_product['id']]['quantity_full_price'] * $store_product['price'];
                $subtotal_cart += $sub_total;
            }
        }

        //domicilio gratis en proximo pedido
        if ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order'])
            $has_free_delivery = true;

        //validar domicilio gratis global
        if ($checkout_discounts['free_delivery']['status']) {
            if (!empty($checkout_discounts['free_delivery']['store_ids'])) {
                $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                $store_ids = count($store_ids) > 1 ? $store_ids : array($store_id);
                if (in_array($store_id, $store_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                    $has_free_delivery = true;
                }
            } else {
                if (!empty($checkout_discounts['free_delivery']['city_ids'])) {
                    $store = Store::find($store_id);
                    $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                    $city_ids = count($city_ids) > 1 ? $city_ids : array($checkout_discounts['free_delivery']['city_ids']);
                    if (in_array($store->city_id, $city_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                        $has_free_delivery = true;
                    }
                }
            }
        }

        $store = Store::find($store_id);
        $zone = $this->getCurrentZone($store_id);
        $address = Session::get('address');

        $freeDelivery = $this->deliveryDiscountUseCase->handle(
            $this->user_id,
            true,
            false,
            $store->city_id,
            $store->id,
            $subtotal_cart
        );

        if (empty($address->latitude) || empty($address->longitude)) {
            $delivery = $store->getDeliverySlotCheckout($zone, $has_free_delivery | $freeDelivery);
        } else {
            $delivery = $store->getClearDeliverySlots(
                $zone,
                $has_free_delivery | $freeDelivery,
                $address->latitude,
                $address->longitude
            );
        }

        $stores = [$store];
        //metadata
        $city = City::remember(10)->find(Session::get('city_id'));
        $metadata['city_name'] = $city->city;
        $metadata['city_slug'] = $city->slug;
        $metadata['stores'] = '';
        if ($n = count($stores)) {
            $cont = 1;
            foreach ($stores as $store) {
                if ($cont != $n) {
                    $metadata['stores'] .= $store->name . ', ';
                } else {
                    $metadata['stores'] = trim($metadata['stores'], ', ');
                    $metadata['stores'] .= ' y ' . $store->name;
                }
                $cont++;
            }
        }

        //obtener ciudades aledañas
        $surrounded_cities['city'] = $city;
        $surrounded_cities['surrounded_cities'] = City::where('parent_city_id', $city->id)->where('status', 1)->get();

        $current_address = Session::has('address') ? Session::get('address') : false;
        $current_address_parts = Session::has('address') && isset(Session::get('address')->address) ? json_decode(Session::get('address')->address) : false;
        $banks = PseBank::where('pseCode', '!=', '0')->orderBy('description')->get();

        // Track Product Viewed event
        self::trackEventOnBrowser(['checkout_started' => [
            'login' => Auth::check() ? 1 : 0,
            'ticket' => $subtotal_cart,
            'size' => $total_products,
            'items' => $total_items,
        ]]);

        $campaigns = [
            'visa' => $this->visaCampaign,
            'bancolombia' => $this->bancolombiaCampaign,
        ];

        $phone_validated = Session::get('phoneValidation.validated');
        $facebook_validated = Session::get('facebookData');
        $post['first_name'] = Session::get('post.first_name') ?: Session::get('facebookData.first_name');
        $post['last_name'] = Session::get('post.last_name') ?: Session::get('facebookData.last_name');
        $post['email'] = Session::get('post.email') ?: Session::get('facebookData.email');
        $post['phone'] = Session::get('phoneValidation.validated')
            ? Session::get('phoneValidation.number')
            : Session::get('post.phone');

        return View::make('checkout')
            ->with('store', $this->store)
            ->with('metadata', $metadata)
            ->with('user_discounts', $this->user_discounts)
            ->with('stores', $this->stores)
            ->with('cities', City::where('coverage_store_id', $city->coverage_store_id)->where('is_main', 1)->where('status', 1)->get())
            ->with('surrounded_cities', $surrounded_cities)
            ->with('current_address', $current_address)
            ->with('current_address_parts', $current_address_parts)
            ->with('addresses', $addresses)
            ->with('credit_cards', $credit_cards)
            ->with('invoice_data', $invoice_data)
            ->with('delivery', $delivery)
            ->with('dates_cc', $dates_cc)
            ->with('post', $post)
            ->with('content_ids', $content_ids)
            ->with('checkout_discounts', $checkout_discounts)
            ->with('payment_methods', $payment_methods)
            ->with('campaigns', $campaigns)
            ->with('device_session_id', md5(session_id() . microtime()))
            ->with('phone_validated', $phone_validated)
            ->with('facebook_validated', $facebook_validated)
            ->with('banks', $banks)
            ->with('footer', $this->getFooter());
    }

    /**
     * Procesar pedido
     */
    public function checkout()
    {
        Event::subscribe(new OrderEventHandler());

        $post_data = Input::all();
        $current_session_address = Session::get('address');
        // Cargar carrito
        $cart_id = $this->cart_id;
        if (!$cart_id) {
            $cart_id = Input::get('cart_id');
        }

        try {
            DB::beginTransaction();

            //si no esta logueado el usuario se crea
            if (!Auth::check() && Input::has('create_user')) {
                if (!Session::get('phoneValidation.validated')) {
                    throw new MerqueoException('Debes validar tu número de celular.');
                }

                //si vienen los campos se valida la info y crea el usuario
                $first_name = Input::get('first_name');
                $last_name = Input::get('last_name');
                $email = Session::get('facebookData.email') ?: Input::get('email');
                $password = Input::get('password');
                $phone = trim(Session::get('phoneValidation.number'));

                if (empty($first_name) || empty($last_name) || empty($email) || empty($password)) {
                    throw new Exception('Debes especificar toda la información para crear tu usuario.');
                }

                ///verifica que el usuario no exista
                $validator = Validator::make(
                    compact('email', 'phone'),
                    [
                        'email' => "required|email|unique:users,email,NULL,id,type,merqueo.com",
                        'phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com',
                    ],
                    [
                        'email.unique' => 'El email ingresado ya se encuentra en uso.',
                        'email.email' => 'El formato del email ingresado no es valido.',
                        'phone.required' => 'Debes validar tu número de celular.',
                        'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                        'phone.numeric' => 'El número celular debe ser númerico.',
                        'phone.digits' => 'El número celular debe tener 10 digitos.',
                    ]
                );
                if ($validator->fails()) {
                    $messages = $validator->messages();
                    if (!$error = $messages->first('email')) {
                        $error = $messages->first('phone');
                    }
                    throw new Exception($error);
                } else {
                    //crear el usuario, realizar login y enviar mail de bienvenida
                    $user = User::add(trim($first_name), trim($last_name), trim($phone), trim($email), $password);
                    $user->phone_validated_date = Carbon::create();
                    $user->origin = 'web';
                    $user->fb_id = Session::get('facebookData.id');
                    $user->save();
                    // Track Product Viewed event
                    self::trackEventOnBrowser(['user_signed_up' => [
                        'screen' => StoreController::getReferrer(),
                    ]]);

                    if (Auth::attempt(array('email' => $email, 'password' => $password)))
                        $this->user_id = $user->id;
                    else throw new Exception('Falló al iniciar sesión. Intenta nuevamente.');
                }
            } else {
                if (!Auth::check())
                    throw new Exception('Por favor inicia sesión nuevamente para continuar.');
                $user = User::find($this->user_id);

                if (!$user->status) {
                    throw new Exception(
                        'Tu usuario ha sido bloqueado temporalmente, escribe a ' . Config::get('app.admin_email')
                    );
                }

                $post_data['first_name'] = $user->first_name;
                $post_data['last_name'] = $user->last_name;
                $post_data['email'] = $user->email;
                if (empty($user->phone) && isset($post_data['user_phone'])) {
                    //verifica que el celular no exista
                    $validator = Validator::make(
                        array('phone' => $post_data['user_phone']),
                        array('phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com'),
                        array(
                            'phone.required' => 'El número celular es requerido.',
                            'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                            'phone.numeric' => 'El número celular debe ser númerico.',
                            'phone.digits' => 'El número celular debe tener 10 digitos.'
                        )
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        throw new Exception($messages->first('phone'));
                    }
                    $user->phone = $post_data['phone'] = $post_data['user_phone'];
                    $user->save();
                    unset($post_data['user_phone']);
                } else $post_data['phone'] = $user->phone;
                if (empty($user->email) && isset($post_data['user_email'])) {
                    $validator = Validator::make(
                        array('email' => $post_data['user_email'],),
                        array('email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com'),
                        array(
                            'email.required' => 'El email es requerido.',
                            'email.unique' => 'El email ingresado ya se encuentra en uso.',
                            'email.email' => 'El formato del email ingresado no es valido.',
                        )
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        throw new Exception($messages->first('email'));
                    }
                    $user->email = $post_data['email'] = $post_data['user_email'];
                    $user->save();
                    unset($post_data['user_email']);
                } else $post_data['email'] = $user->email;
            }

            if (empty($user->email)) {
                throw new Exception('Para realizar el pedido debes ingresar un email.');
            }

            $post_data['user_id'] = $user->id;

            // APARTIR DE ESTE PUNTO YA EL USUARIO ESTA AUTENTICADO

            /*// Validar y cargar codigo de referido
            $referred = User::validateReferred($user, null, true);
            if ($referred['success'])
                $this->user_discounts = User::getDiscounts(null, null, true, true);*/

            // Bloquear referido desde web
            $referral_code = Input::has('promo_code') ? Input::get('coupon_code') : (Session::has('referred_code') ? Session::get('referred_code') : false);
            if ($referral_code) {
                $referrer = User::where('referral_code', trim($referral_code))->where('id', '<>', $user->id)->where('referral_code_blocked', 0)->first();
                if ($referrer) {
                    throw new Exception('Para referir a otro usuario debes hacerlo desde nuestra app.');
                }
            }

            // Verificar si el usuario tiene pedidos entregados
            $info = User::getOrdersInfo($user->id);
            $user_has_orders = $info->qty;

            $address_id = Input::get('address_id');
            if (empty($address_id)) {
                // Si hay información de dirección, se valida y se crea
                $dir = Input::get('dir');
                $address = new UserAddress;
                $address->user_id = $this->user_id;
                $address->label = Input::get('address_name') == 'Otro' ? Input::get('other_name') : Input::get('address_name');
                $address->address = implode('', $dir);
                $address->address_further = Input::get('address_further');
                $address->neighborhood = Input::get('address_neighborhood');
                $address->address_1 = trim($dir[0]);
                $address->address_2 = trim($dir[2]);
                $address->address_3 = trim($dir[6]);
                $address->address_4 = trim($dir[8]);
                $address->city_id = isset($post_data['surrounded_city_id']) ? $post_data['surrounded_city_id'] : Input::get('city_id');
                // Validar direccion
                $city = City::find($address->city_id);
                $inputs = [
                    'address' => $address->address,
                    'city' => $city->slug
                ];
                $request = Request::create('/api/location', 'GET', $inputs);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                $session_address = Session::get('address');
                $has_coordinates = !(empty($session_address->lat) || empty($session_address->lng));

                if (empty($result['status']) && !$has_coordinates) {
                    throw new Exception('No pudimos ubicar tu dirección por favor verificala.');
                }

                $address->latitude = $result['status'] ? $result['result']['latitude'] : $session_address->lat;
                $address->longitude = $result['status'] ? $result['result']['longitude'] : $session_address->lng;
                $address->save();
                $address_id = $address->id;
                // Es necesario asignar el identificador de la dirección
                // almacenada en la sesión con el fin de que no sea necesario
                // solicitar la dirección nuevamente.
                $address->address_text = $address->address;
                Session::put('address', $address);
            } else {
                if (!$address = UserAddress::find($address_id))
                    throw new Exception('Ocurrió un error al procesar los datos de tu dirección por favor verificala.');
                $post_data['address_further'] = $address->address_further;
                $city = City::find($address->city_id);
            }
            $post_data['address'] = $address->address;

            //validar cobertura y obtener zona
            $zone = Zone::getByLatLng($this->getCoverageStoreCityId($address->city_id), $address->latitude, $address->longitude);
            if (!$zone) {
                throw new Exception('No hay cobertura en tu dirección en este momento.');
            }

            $stratum = \EconomicStratum::getStratum($address->latitude, $address->longitude);

            //busco si el carrito existe
            $cart = Cart::where('id', $cart_id)->where('user_id', $user->id)->first();

            //si no existe entonces creo un carrito
            $was_created_recently = false;
            if (!$cart) {
                $was_created_recently = true;
                $cart = new Cart;
                $cart->user_id = $this->user_id;
                $cart->save();
                $cart_id = $cart->id;
                Session::put('cart_id');
            }
            // TODO En el registro del usuario debe eliminarse la variable de sesión "tmp_cart".
            //si el usuario tiene un carrito en sesión, lo pongo en el carrito actual borrando todo lo que tengo en db
            if (Session::has('tmp_cart') && $was_created_recently) {
                $cart = json_decode(Session::get('tmp_cart'));
                if (isset($cart->products)) {
                    $products = (array) $cart->products;
                    if (count($products)) {
                        CartProduct::where('cart_id', $cart_id)->delete();
                        foreach ($cart->products as $id => $product) {
                            if ($product->cart_quantity <= 0) {
                                continue;
                            };

                            //validar estado de producto
                            $store_product = StoreProduct::select('store_products.id', 'store_product_warehouses.status', 'products.name', 'products.image_medium_url')
                                ->join('products', 'products.id', '=', 'store_products.product_id')
                                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                                ->where('store_products.id', $id)
                                ->where('store_products.store_id', $city->coverage_store_id)
                                ->where('store_product_warehouses.warehouse_id', $zone->warehouse_id)
                                ->first();
                            if (!$store_product) {
                                continue;
                            }

                            $cart_product = new CartProduct();
                            $cart_product->cart_id = $cart_id;
                            $cart_product->store_product_id = $store_product->id;
                            $cart_product->quantity = $product->cart_quantity;
                            $cart_product->quantity_full_price = $product->cart_quantity_full_price;
                            $cart_product->added_by = Auth::user()->id;
                            $cart_product->save();
                        }
                    }
                }
            }

            //validar metodo de pago
            $is_credit_card = $post_data['payment_method'] == 'Tarjeta de crédito' ? true : false;
            if ($is_credit_card) {
                //validar si tarjeta es nueva
                $credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
                if (empty($credit_card_id)) {
                    $payu = new PayU;

                    //validar pais de la tarjeta de credito
                    $bin = substr($post_data['number_cc'], 0, 6);
                    $credit_card = $payu->getCreditCardInfo($bin, $post_data);

                    if (!$credit_card) {
                        throw new Exception('Ocurrió un problema al validar el origen de tu tarjeta de crédito.');
                    }

                    if (!$credit_card->is_valid) {
                        throw new Exception('Solo se aceptan tarjetas de crédito nacionales.');
                    }

                    $post_data['card_type'] = get_card_type($post_data['number_cc']);
                    $result = $payu->associateCreditCard($post_data);

                    if (!$result['status']) {
                        throw new Exception($result['message']);
                    }

                    if (!in_array($result['response']->creditCardToken->paymentMethod, $this->config['credit_cards_types'])) {
                        throw new Exception('Solo se aceptan tarjetas de crédito ' . $this->config['credit_cards_message']);
                    }

                    $post_data['card_token'] = $result['response']->creditCardToken->creditCardTokenId;

                    //guardar tarjeta en bd
                    $user_credit_card = new UserCreditCard;
                    $user_credit_card->user_id = $user->id;
                    $user_credit_card->card_token = $post_data['card_token'];
                    $user_credit_card->bin = substr($post_data['number_cc'], 0, 6);
                    $user_credit_card->last_four = substr($post_data['number_cc'], 12, 4);
                    $user_credit_card->expiration_year = empty($post_data['expiration_year_cc']) ? '' : $post_data['expiration_year_cc'];
                    $user_credit_card->expiration_month = empty($post_data['expiration_month_cc']) ? '' : $post_data['expiration_month_cc'];
                    $user_credit_card->type = $post_data['card_type'];
                    $user_credit_card->country = $credit_card->country_name;
                    $user_credit_card->holder_document_type = $post_data['document_type_cc'];
                    $user_credit_card->holder_document_number = $post_data['document_number_cc'];
                    $user_credit_card->holder_name = $post_data['name_cc'];
                    $user_credit_card->holder_email = $user->email;
                    $user_credit_card->holder_phone = $post_data['phone_cc'];
                    $user_credit_card->holder_address = $post_data['address_cc'];
                    $user_credit_card->holder_address_further = '';
                    $user_credit_card->holder_city = $city->city;
                    $user_credit_card->save();
                } else {
                    $user_credit_card = UserCreditCard::find($credit_card_id);
                    if (!$user_credit_card)
                        throw new Exception('Ocurrió un error al obtener información de la tarjeta de crédito.');
                    $post_data['card_token'] = $user_credit_card->card_token;
                    if (!empty($post_data['add_document_number_cc_' . $user_credit_card->id])) {
                        $user_credit_card->holder_document_type = $post_data['add_document_type_cc_' . $user_credit_card->id];
                        $user_credit_card->holder_document_number = $post_data['add_document_number_cc_' . $user_credit_card->id];
                        $user_credit_card->save();
                    }
                }
            }

            $cart_products = CartProduct::select(
                'products.*',
                'store_products.*',
                'cart_products.quantity AS cart_quantity',
                'cart_products.quantity_full_price AS cart_quantity_full_price',
                'store_products.allied_store_id',
                'cart_products.store_id AS cart_store_id',
                'cart_products.store_product_id',
                'store_product_warehouses.status',
                'cart_products.id AS cart_product_id',
                StoreProduct::getRawSpecialPriceQuery(
                    $info->qty,
                    $info->last_order_date
                ),
                StoreProduct::getRawDeliveryDiscountByDate()
            )
                ->join('store_products', 'cart_products.store_product_id', '=', 'store_products.id')
                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->where('cart_id', $cart_id)
                ->where('store_product_warehouses.warehouse_id', $zone->warehouse_id)
                ->orderBy('store_id')
                ->get();

            $product_discount = new DeliveryDiscountByProduct();
            $response['cart']['delivery_amount'] = 0;
            $response['cart']['total_amount'] = 0;
            $response['cart']['discount_amount'] = 0;
            $response['cart']['total_quantity'] = 0;
            $response['cart']['stores'] = [];

            $unavailable_products = [];
            if ($cart_products) {
                $response['cart']['is_minimum_reached'] = 1;

                foreach ($cart_products as $cart_product) {
                    if (empty($cart_product->status)) {
                        $unavailable_products[] = $cart_product;
                        continue;
                    }

                    //validar producto en promocion en pedidos anteriores
                    if ($cart_product->special_price > -1 && $cart_product->quantity_special_price && $cart_product->cart_quantity) {
                        $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                            ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                            ->where('store_product_id', $cart_product->id)
                            ->where('order_products.price', $cart_product->special_price)
                            ->where('user_id', Auth::user()->id)
                            ->where('user_id', '<>', 425200) //usuario trade
                            ->where('status', '<>', 'Cancelled')
                            ->whereNotNull('store_products.special_price')
                            ->whereRaw(DB::raw(StoreProduct::getRawConditionByDateRange()))
                            ->where(DB::raw("DATEDIFF('" . date('Y-m-d') . "', date)"), '<=', Config::get('app.minimum_promo_days'))
                            ->groupBy('order_products.store_product_id')
                            ->count();
                        if ($count_products) {
                            $error = 'Ya realizaste un pedido con el producto ' . $cart_product->name . ' en promoción con precio $' . number_format($cart_product->special_price, 0, ',', '.') . ', por favor eliminalo del carrito de compras para continuar.';
                            throw new Exception($error);
                        }
                    }

                    $store_product = json_decode(json_encode($cart_product), true);

                    //validar precio especial y cantidad del producto
                    if ($store_product['special_price'] > -1 && $store_product['quantity_special_price'] && $store_product['cart_quantity'] > $store_product['quantity_special_price']) {
                        if ($store_product['first_order_special_price'] && !$user_has_orders) {
                            $error = 'El producto ' . $store_product['name'] . ' por estar en promoción puedes agregar máximo ' . $store_product['quantity_special_price'] . ' unidad(es) en tu pedido, por favor disminuye la cantidad en el carrito para continuar.';
                            throw new Exception($error);
                        }
                    }

                    $store_id = $cart_product->store_id;
                    if (!isset($response['cart']['stores'][$store_id])) {
                        $tmp_store = Store::find($store_id);
                        $store = $tmp_store->toArray();
                        $store['count'] = 1;
                        $store['sub_total'] = 0;
                        $response['cart']['stores'][$store_id] = $store;
                        $response['cart']['stores'][$store_id]['products'] = [];
                        $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['sub_total'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_time_minutes'] = $store['delivery_time_minutes'];
                        $response['cart']['stores'][$store_id]['original_object'] = $tmp_store;
                    } else $response['cart']['stores'][$store_id]['count'] += 1;

                    if (!isset($store_product['cart_quantity']))
                        $store_product['cart_quantity'] = 1;

                    //obtener precio de producto (full, especial / primera compra)
                    if ($store_product['special_price'] > -1) {
                        //$response['cart']['has_special_price'] = true;
                        if ($store_product['first_order_special_price'])
                            if (!$user_has_orders)
                                $price = $store_product['special_price'];
                            else $price = $store_product['price'];
                        else $price = $store_product['special_price'];
                    } else $price = $store_product['price'];

                    $store_product['sub_total'] = $store_product['cart_quantity'] * $price;
                    if ($store_product['cart_quantity_full_price'])
                        $store_product['sub_total'] += $store_product['cart_quantity_full_price'] * $store_product['price'];
                    $response['cart']['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
                    $response['cart']['total_amount'] += $store_product['sub_total'];
                    $response['cart']['total_quantity']++;

                    if ($store_product['delivery_discount_amount']) {
                        $product_discount->add(StoreProduct::find($store_product['id']));
                    }

                    array_push($response['cart']['stores'][$store_id]['products'], $store_product);
                }
            }

            if (!empty($unavailable_products)) {
                Session::put('removed_from_checkout_post', true);
                Session::put('removed_products', $unavailable_products);
                throw new Exception('Algunos de los productos no están disponibles.');
            }

            $subtotal_cart = $response['cart']['total_amount'];

            //aplicar descuento a la compra
            if (isset($this->user_discounts['coupon'])) {
                $coupon = $this->user_discounts['coupon'];
                $data = array('coupon_code' => $coupon->code, 'validate' => 1);
                if (isset($post_data['create_user']))
                    $data['is_new_user'] = true;
                $request = Request::create('/checkout/coupon', 'POST', $data);
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if (!$result['status'])
                    throw new Exception($result['message']);

                //si el cupon cumple con totales requerido
                if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $subtotal_cart < $coupon->minimum_order_amount)
                    throw new Exception('Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el total del pedido sin incluir el domicilio debe ser mayor o igual a <b>$' . number_format($coupon->minimum_order_amount, 0, ',', '.') . '</b>');
                elseif ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $subtotal_cart > $coupon->maximum_order_amount)
                    throw new Exception('Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el total del pedido sin incluir el domicilio debe ser menor o igual a <b>$' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '</b>');
                elseif ($coupon->minimum_order_amount && $coupon->maximum_order_amount && ($subtotal_cart < $coupon->minimum_order_amount || $subtotal_cart > $coupon->maximum_order_amount))
                    throw new Exception('Para redimir el cupón de descuento <b>' . $coupon->coupon . '</b> el total del pedido sin incluir el domicilio debe estar entre <b>$' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '</b> y <b>$' . number_format($coupon->minimum_order_amount, 0, ',', '.') . '</b>');

                $discount_amount = 0;
                $coupon_discount = new OrderDiscount();
                $coupon_discount->coupon()->associate($coupon);
                //validar cupon para tienda / categoria / subcategoria / producto especifico
                if ($coupon->redeem_on == 'Specific Store') {
                    foreach ($cart_products as $store_product) {
                        $is_valid = false;
                        if ($store_product->store_id == $coupon->store_id) {
                            if ($coupon->department_id) {
                                if ($store_product->department_id == $coupon->department_id) {
                                    if ($coupon->shelf_id) {
                                        if ($store_product->shelf_id == $coupon->shelf_id) {
                                            if ($coupon->store_product_id) {
                                                if ($store_product->id == $coupon->store_product_id) {
                                                    $is_valid = true;
                                                }
                                            } else {
                                                $is_valid = true;
                                            }
                                        }
                                    } else {
                                        $is_valid = true;
                                    }
                                }
                            } else {
                                $is_valid = true;
                            }
                        }
                        if ($is_valid) {
                            if ($coupon->type == 'Discount Percentage') {
                                $discount_amount += round(($store_product->price * $store_product->cart_quantity) * ($coupon->amount / 100), 0);
                            }
                            if ($coupon->type == 'Credit Amount') {
                                $discount_amount += round($store_product->price * $store_product->cart_quantity, 0);
                            }
                        }
                    }
                    $store = Store::select(
                        'stores.name AS store_name',
                        'departments.name AS department_name',
                        'shelves.name AS shelf_name',
                        DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"),
                        'cities.city'
                    )
                        ->leftJoin('departments', function ($join) use ($coupon) {
                            $join->on('departments.store_id', '=', 'stores.id');
                            $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                        })
                        ->leftJoin('shelves', function ($join) use ($coupon) {
                            $join->on('shelves.department_id', '=', 'departments.id');
                            $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                        })
                        ->leftJoin('store_products', function ($join) use ($coupon) {
                            $join->on('store_products.shelf_id', '=', 'shelves.id');
                            $join->on('store_products.id', '=', DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                        })
                        ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                        ->join('cities', 'cities.id', '=', 'stores.city_id')
                        ->where('stores.id', $coupon->store_id)
                        ->first();

                    $discount_message = '';
                    if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name))
                        $discount_message = $store->department_name;
                    if (!empty($store->shelf_name) && empty($store->product_name))
                        $discount_message = $store->shelf_name;
                    if (!empty($store->product_name))
                        $discount_message = $store->product_name;
                    $discount_message .= ' en ' . $store->city;

                    if ($discount_amount) {
                        if ($coupon->type == 'Discount Percentage') {
                            $response['cart']['stores'][$coupon->store_id]['discount_percentage_amount'] = $coupon->amount;
                            $coupon_discount->percentage_amount = $coupon->amount;
                        } elseif ($discount_amount > $coupon->amount) {
                            $discount_amount = $coupon->amount;
                            $coupon_discount->amount = $coupon->amount;
                        }

                        $response['cart']['stores'][$coupon->store_id]['discount_amount'] = $discount_amount;
                        $response['cart']['discount_amount'] += $discount_amount;
                        $order_discount_data[] = $coupon_discount;
                    } else {
                        throw new Exception('El cupón de descuento <b>' . $coupon->code . '</b> aplica exclusivamente para <b>' . $discount_message . '</b>.');
                    }

                    $response['cart']['stores'][$coupon->store_id]['discount_message'] = $discount_message;
                }
                //validar metodo de pago y tipo de tarjeta
                if ($coupon->payment_method) {
                    if (($is_credit_card && $coupon->payment_method != 'Tarjeta de crédito' && $coupon->payment_method != $user_credit_card->type)
                        || (!$is_credit_card && $coupon->payment_method != $post_data['payment_method'])
                    ) {
                        throw new Exception('Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el metodo de pago debe ser con <b>' . $this->config['payment_methods_names'][$coupon->payment_method] . ' ' . $coupon->cc_bank . '</b>');
                    } else {
                        if ($is_credit_card && !empty($coupon->cc_bin)) {
                            $allowed_bines = explode(',', $coupon->cc_bin);
                            if (!in_array($user_credit_card->bin, $allowed_bines)) {
                                throw new Exception('Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el metodo de pago debe ser con <b>' . $this->config['payment_methods_names'][$coupon->payment_method] . ' ' . $coupon->cc_bank . '</b>');
                            }
                        }
                    }
                }
                //si el cupon no es de productos especificos se aplica el cupon
                if ($coupon->redeem_on != 'Specific Store') {
                    if ($coupon->type == 'Credit Amount') {
                        if ($coupon->amount > $response['cart']['total_amount']) {
                            $coupon->amount = $response['cart']['total_amount'];
                        }
                        $coupon_discount->amount = $coupon->amount;
                        $discount_credit_amount = $coupon->amount;
                    }
                    if ($coupon->type == 'Discount Percentage') {
                        $coupon_discount->percentage_amount = $coupon->amount;
                        $discount_percentage = $coupon->amount;
                    }

                    $order_discount_data[] = $coupon_discount;
                }
            }

            //si no tiene cupon activo y si el total es mayor al requerido se aplica descuento con credito
            if (isset($this->user_discounts) && !isset($this->user_discounts['coupon']) && $this->user_discounts['amount'] && $subtotal_cart > $this->user_discounts['minimum_discount_amount']) {
                if ($this->user_discounts['amount'] > $response['cart']['total_amount']) {
                    $this->user_discounts['amount'] = $response['cart']['total_amount'];
                }
                $credit_discount = new OrderDiscount();
                $credit_discount->type = 'Crédito';
                $credit_discount->amount = $this->user_discounts['amount'];
                $order_discount_data[] = $credit_discount;
                $discount_credit_amount = $this->user_discounts['amount'];
            }

            //obtener descuentos activos en checkout
            $checkout_discounts = Order::getCheckoutDiscounts(Session::get('city_id'));

            if (!count($response['cart']['stores']))
                $response['cart']['is_minimum_reached'] = 0;


            foreach ($response['cart']['stores'] as $store_id => $store) {
                if ($store['sub_total'] < $store['minimum_order_amount'])
                    $response['cart']['is_minimum_reached'] = 0;

                if (Auth::check() && in_array(Auth::user()->type, User::DARKSUPERMARKET_TYPE)) {
                    if ($store['sub_total'] >= Config::get('app.dark_supermarket.minimum_order_amount')) {
                        $response['cart']['is_minimum_reached'] = 1;
                    }
                }

                //valida que todas las tiendas tengan cobertura en la dirección y obtener deliveryWindow
                $delivery_window = $store['original_object']->validateStoreDeliversToAddress(
                    $address->latitude,
                    $address->longitude,
                    new Carbon($post_data["delivery_day_{$store_id}"]),
                    $post_data["delivery_time_{$store_id}"]
                );
                $response['cart']['stores'][$store_id]['delivery_window'] = $delivery_window;
                $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_window->delivery_amount;

                // Validar fecha de entrega
                if (!self::hasValidDeliveryTime($zone, $post_data["delivery_day_{$store_id}"], $delivery_window)) {
                    throw new Exception('Los horarios de entrega han cambiado, por favor vuelve a seleccionar el horario.');
                }

                //domicilio gratis en proximo pedido
                if (
                    isset($this->user_discounts)
                    && ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order'])
                ) {
                    $free_delivery_discount = new FreeDeliveryDiscount(
                        $response['cart']['stores'][$store_id]['delivery_amount']
                    );
                    $free_delivery_discount->setUser($user);
                    $order_discount_data[] = $free_delivery_discount;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                }

                //validar domicilio gratis global
                if ($checkout_discounts['free_delivery']['status']) {
                    $free_delivery_discount = new FreeDeliveryDiscount(
                        $response['cart']['stores'][$store_id]['delivery_amount']
                    );
                    $free_delivery_discount->setUser($user);

                    if (!empty($checkout_discounts['free_delivery']['store_ids'])) {
                        $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                        $store_ids = count($store_ids) > 1 ? $store_ids : array($store_id);
                        if (in_array($store_id, $store_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                            $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                            $free_delivery_discount->setReason(FreeDeliveryDiscount::BY_STORE);
                            $order_discount_data[] = $free_delivery_discount;
                        }
                    } elseif (!empty($checkout_discounts['free_delivery']['city_ids'])) {
                        $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                        $city_ids = count($city_ids) > 1 ? $city_ids : array($checkout_discounts['free_delivery']['city_ids']);
                        if (in_array($response['cart']['stores'][$store_id]['city_id'], $city_ids) && $subtotal_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                            $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                            $free_delivery_discount->setReason(FreeDeliveryDiscount::BY_CITY);
                            $order_discount_data[] = $free_delivery_discount;
                        }
                    }
                }

                //validar descuento en domicilio por productos
                if ($response['cart']['stores'][$store_id]['delivery_amount'] && $product_discount->getTotalDiscount()) {
                    $discount = $product_discount->getTotalDiscount();
                    $delivery_amount = $response['cart']['stores'][$store_id]['delivery_amount'];
                    $delivery_amount = $delivery_amount >= $discount ? $delivery_amount - $discount : 0;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_amount;
                    if ($product_discount->count()) {
                        $order_discount_data[] = $product_discount;
                    }
                }

                $response['cart']['delivery_amount'] += $response['cart']['stores'][$store_id]['delivery_amount'];

                //sino hay cupon activo
                if (!isset($this->user_discounts['coupon'])) {
                    //validar descuento de credito global
                    if ($checkout_discounts['discount_credit']['status']) {
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_credit']['minimum_order_amount'] && !$checkout_discounts['discount_credit']['maximum_order_amount'] && $subtotal_cart < $checkout_discounts['discount_credit']['minimum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_credit']['maximum_order_amount'] && !$checkout_discounts['discount_credit']['minimum_order_amount'] && $subtotal_cart > $checkout_discounts['discount_credit']['maximum_order_amount']) {
                            $is_valid = false;
                        } elseif (
                            $checkout_discounts['discount_credit']['minimum_order_amount'] && $checkout_discounts['discount_credit']['maximum_order_amount'] && ($subtotal_cart < $checkout_discounts['discount_credit']['minimum_order_amount'] || $subtotal_cart > $checkout_discounts['discount_credit']['maximum_order_amount'])
                        ) {
                            $is_valid = false;
                        }

                        if ($is_valid) {
                            $discount_global_credit_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_credit']['store_id']) {
                                foreach ($store['products'] as $store_product) {
                                    $checkout_discount = new OrderDiscount();
                                    $store_product = (object) $store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_credit']['store_id']) {
                                        if ($checkout_discounts['discount_credit']['department_id']) {
                                            if ($store_product->department_id == $checkout_discounts['discount_credit']['department_id']) {
                                                if ($checkout_discounts['discount_credit']['shelve_id']) {
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_credit']['shelve_id']) {
                                                        if ($checkout_discounts['discount_credit']['product_id']) {
                                                            if ($store_product->id == $checkout_discounts['discount_credit']['store_product_id']) {
                                                                $checkout_discount->description = "Descuento en el producto #{$store_product->id}.";
                                                                $is_valid = true;
                                                            }
                                                        } else {
                                                            $checkout_discount->description = "Descuento en el pasillo #{$store_product->shelf_id}.";
                                                            $is_valid = true;
                                                        }
                                                    }
                                                } else {
                                                    $checkout_discount->description = "Descuento en el departamento #{$store_product->department_id}.";
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $checkout_discount->description = "Descuento en la tienda #{$store_product->store_id}.";
                                            $is_valid = true;
                                        }
                                    }
                                    if ($is_valid) {
                                        $discount_amount = round($product_price * $store_product->cart_quantity, 0);
                                        $discount_global_credit_amount += $discount_amount;
                                        $checkout_discount->type = 'Descuento';
                                        $checkout_discount->amount = $discount_amount;
                                        $order_discount_data[] = $checkout_discount;
                                    }
                                }
                            } else {
                                $checkout_discount = new OrderDiscount();
                                $checkout_discount->type = 'Descuento';
                                $checkout_discount->amount = $checkout_discounts['discount_credit']['amount'];
                                $checkout_discount->description = 'Descuento global';
                                $discount_global_credit_amount += $checkout_discounts['discount_credit']['amount'];
                            }

                            if ($discount_global_credit_amount) {
                                if ($discount_global_credit_amount > $checkout_discounts['discount_credit']['amount']) {
                                    $discount_global_credit_amount = $checkout_discounts['discount_credit']['amount'];
                                }
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_credit_amount;
                                $response['cart']['discount_amount'] += $discount_global_credit_amount;
                            }
                        }
                    }

                    //validar descuento por porcentaje global
                    if ($checkout_discounts['discount_percentage']['status']) {
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_percentage']['minimum_order_amount'] && !$checkout_discounts['discount_percentage']['maximum_order_amount'] && $subtotal_cart < $checkout_discounts['discount_percentage']['minimum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_percentage']['maximum_order_amount'] && !$checkout_discounts['discount_percentage']['minimum_order_amount'] && $subtotal_cart > $checkout_discounts['discount_percentage']['maximum_order_amount']) {
                            $is_valid = false;
                        } elseif (
                            $checkout_discounts['discount_percentage']['minimum_order_amount'] && $checkout_discounts['discount_percentage']['maximum_order_amount'] && ($subtotal_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'] || $subtotal_cart > $checkout_discounts['discount_percentage']['maximum_order_amount'])
                        ) {
                            $is_valid = false;
                        }

                        if ($is_valid) {
                            $discount_global_percentage_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_percentage']['store_id']) {
                                foreach ($store['products'] as $store_product) {
                                    $checkout_discount = new OrderDiscount();
                                    $store_product = (object) $store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_percentage']['store_id']) {
                                        if ($checkout_discounts['discount_percentage']['department_id']) {
                                            if ($store_product->department_id == $checkout_discounts['discount_percentage']['department_id']) {
                                                if ($checkout_discounts['discount_percentage']['shelve_id']) {
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_percentage']['shelve_id']) {
                                                        if ($checkout_discounts['discount_percentage']['product_id']) {
                                                            if ($store_product->id == $checkout_discounts['discount_percentage']['store_product_id']) {
                                                                $checkout_discount->description = "Descuento en el producto #{$store_product->id}.";
                                                                $is_valid = true;
                                                            }
                                                        } else {
                                                            $checkout_discount->description = "Descuento en el pasillo #{$store_product->shelf_id}.";
                                                            $is_valid = true;
                                                        }
                                                    }
                                                } else {
                                                    $checkout_discount->description = "Descuento en el departamento #{$store_product->department_id}.";
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $checkout_discount->description = "Descuento en la tienda #{$store_product->store_id}.";
                                            $is_valid = true;
                                        }
                                    }
                                    if ($is_valid) {
                                        $discount_global_percentage_amount += round(($product_price * $store_product->cart_quantity) * ($checkout_discounts['discount_percentage']['amount'] / 100), 0);
                                        $checkout_discount->percentage_amount = $checkout_discounts['discount_percentage']['amount'];
                                        $checkout_discount->type = 'Descuento';
                                        $order_discount_data[] = $checkout_discount;
                                    }
                                }
                            } else {
                                $discount_global_percentage_amount += round($response['cart']['total_amount'] * ($checkout_discounts['discount_percentage']['amount'] / 100), 0);
                                $checkout_discount = new OrderDiscount();
                                $checkout_discount->percentage_amount = $checkout_discounts['discount_percentage']['amount'];
                                $checkout_discount->type = 'Descuento';
                                $checkout_discount->description = 'Descuento global';
                                $order_discount_data[] = $checkout_discount;
                            }

                            if ($checkout_discounts['discount_percentage']['order_for_tomorrow']) {
                                $delivery_day = $post_data['delivery_day_' . $store_id];
                                if (!$delivery_day)
                                    $discount_global_percentage_amount = 0;
                                elseif ($delivery_day < date('Y-m-d', strtotime('+1 day')))
                                    $discount_global_percentage_amount = 0;
                            }

                            if ($discount_global_percentage_amount) {
                                $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $checkout_discounts['discount_percentage']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_percentage_amount;
                                $response['cart']['discount_amount'] += $discount_global_percentage_amount;
                            }
                        }
                    }
                }

                $campaignInput = [
                    'credit_card_id' => $is_credit_card ? $user_credit_card->id : null,
                    'user_id' => $user->id,
                    'total_amount' => $response['cart']['stores'][$store_id]['sub_total'],
                    'payment_method' => $post_data['payment_method'],
                ];

                // Descuento pagos con tarjeta de credito
                if (!isset($this->user_discounts['coupon']) && $is_credit_card && !$response['cart']['stores'][$store_id]['discount_amount']) {
                    $applyDiscount = $this->visaCampaign->validate($campaignInput);

                    if ($applyDiscount) {
                        $discountAmount = $this->visaCampaign->getDiscountAmount(
                            $response['cart']['stores'][$store_id]['sub_total']
                        );

                        $response['cart']['discount_amount'] += $discountAmount;
                        $response['cart']['stores'][$store_id]['discount_amount'] = $discountAmount;
                        $response['cart']['stores'][$store_id]['discount_percentage_amount'] = ($discountAmount / $response['cart']['stores'][$store_id]['sub_total']) * 100;

                        $campaignDiscount = new OrderDiscount();
                        $campaignDiscount->percentage_amount = $this->visaCampaign->getPercentageDiscount();
                        $campaignDiscount->amount = $discountAmount;
                        $campaignDiscount->type = 'Descuento';
                        $campaignDiscount->description = $this->visaCampaign->getName();
                        $order_discount_data[] = $campaignDiscount;

                        $valid_credit_card_discount = true;
                    }
                }

                if ($this->bancolombiaCampaign->validate($campaignInput)) {
                    $this->bancolombiaCampaign->updateUser($user);
                    $user->save();

                    $campaignDiscount = new OrderDiscount();
                    $campaignDiscount->percentage_amount = $this->bancolombiaCampaign->getPercentageDiscount();
                    $campaignDiscount->amount = $response['cart']['delivery_amount'];
                    $campaignDiscount->type = 'Domicilio gratis';
                    $campaignDiscount->description = $this->bancolombiaCampaign->getName();
                    $order_discount_data[] = $campaignDiscount;

                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                    $response['cart']['delivery_amount'] = 0;
                }

                //credito
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                    && isset($discount_credit_amount) && $discount_credit_amount && !$response['cart']['stores'][$store_id]['discount_amount']
                ) {
                    $total_order_store = $response['cart']['stores'][$store_id]['sub_total'] + $response['cart']['stores'][$store_id]['delivery_amount'];
                    $discount_amount = $discount_credit_amount > $total_order_store ? $total_order_store : $discount_credit_amount;
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['discount_amount'] += $discount_amount;
                    $discount_credit_amount -= $discount_amount;
                }
                //porcentaje
                if ((!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                    && isset($discount_percentage) && $discount_percentage && !$response['cart']['stores'][$store_id]['discount_amount']
                ) {
                    $discount_amount = round(($response['cart']['stores'][$store_id]['sub_total'] * $discount_percentage) / 100, 2);
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $discount_percentage;
                    $response['cart']['discount_amount'] += $discount_amount;
                }
            }

            if (isset($response['cart']['is_minimum_reached']) && !$response['cart']['is_minimum_reached'])
                throw new Exception('Valor mínimo de pedido no alcanzado.');

            //cupon en sesion utilizado
            if (isset($coupon)) {
                //cargar cupon como utilizado a usuario
                $user_coupon = new UserCoupon;
                $user_coupon->coupon_id = $coupon->id;
                $user_coupon->user_id = Auth::user()->id;
                $user_coupon->save();
                //sumar credito de cupon a usuario
                $expiration_date = date('Y-m-d H:i:s', strtotime('+1 day'));
                UserCredit::addCredit($user, $response['cart']['discount_amount'], $expiration_date, 'coupon', $coupon);

                if (!$user->first_coupon_used && $coupon->type_use == 'Only The First Order') {
                    $user->first_coupon_used = 1;
                    $user->save();
                }
            }

            // obtener localidad
            $locality_id = null;
            $localities = Locality::where('city_id', $address->city_id)->where('status', 1)->get();
            if ($localities) {
                foreach ($localities as $locality) {
                    if (point_in_polygon($locality->polygon, $address->latitude, $address->longitude)) {
                        $locality_id = $locality->id;
                        break;
                    }
                }
            }

            //armar pedido de merqueo y pedidos de tiendas aliadas (MARKETPLACE)
            $discount_total_amount = 0;
            $total_order_items = 0;
            foreach ($cart_products as $id => $cart_product) {
                if (!empty($cart_product->allied_store_id)) {
                    if (!isset($orders['Marketplace'][$cart_product->allied_store_id])) {
                        $allied_store = AlliedStore::find($cart_product->allied_store_id);
                        $orders['Marketplace'][$cart_product->allied_store_id]['store_id'] = $cart_product->store_id;
                        $orders['Marketplace'][$cart_product->allied_store_id]['delivery_amount'] = 0;
                        $orders['Marketplace'][$cart_product->allied_store_id]['discount_amount'] = 0;
                        $orders['Marketplace'][$cart_product->allied_store_id]['total_amount'] = 0;
                        $orders['Marketplace'][$cart_product->allied_store_id]['allied_store_id'] = $cart_product->allied_store_id;
                        $orders['Marketplace'][$cart_product->allied_store_id]['delivery_date'] = get_delivery_date_marketplace(
                            Carbon::createFromFormat('Y-m-d H:i:s', $post_data['delivery_day_' . $cart_product->store_id] . ' ' . $delivery_window->hour_end),
                            $allied_store->delivery_time_hours
                        );
                    }
                    $type = 'Marketplace';
                    $store_id = $cart_product->allied_store_id;
                } else {
                    if (!isset($orders['Merqueo'][$cart_product->store_id])) {
                        $orders['Merqueo'][$cart_product->store_id]['store_id'] = $cart_product->store_id;
                        if (isset($response['cart']['stores'][$cart_product->store_id]['discount_percentage_amount']))
                            $orders['Merqueo'][$cart_product->store_id]['discount_percentage_amount'] = $response['cart']['stores'][$cart_product->store_id]['discount_percentage_amount'];
                        $orders['Merqueo'][$cart_product->store_id]['discount_amount'] = $response['cart']['stores'][$cart_product->store_id]['discount_amount'];
                        $orders['Merqueo'][$cart_product->store_id]['delivery_amount'] = $response['cart']['stores'][$cart_product->store_id]['delivery_amount'];
                        $orders['Merqueo'][$cart_product->store_id]['total_amount'] = 0;
                        $orders['Merqueo'][$cart_product->store_id]['allied_store_id'] = null;
                    }
                    $type = 'Merqueo';
                    $store_id = $cart_product->store_id;
                }

                //obtener precio de producto (full, especial / primera compra)
                if ($cart_product->special_price > -1) {
                    if ($cart_product->first_order_special_price)
                        if (!$user_has_orders)
                            $price = $cart_product->special_price;
                        else $price = $cart_product->price;
                    else $price = $cart_product->special_price;
                } else $price = $cart_product->price;

                //calcular totales
                $orders[$type][$store_id]['total_amount'] += $cart_product->cart_quantity * $price;
                if ($cart_product->cart_quantity_full_price)
                    $orders[$type][$store_id]['total_amount'] += $cart_product->cart_quantity_full_price * $cart_product->price;
                $orders[$type][$store_id]['products'][] = $cart_product;
            }

            //recalcular descuento
            if (isset($orders['Merqueo'])) {
                reset($orders['Merqueo']);
                $store_id = key($orders['Merqueo']);
                if ($orders['Merqueo'][$store_id]['discount_amount'] > $orders['Merqueo'][$store_id]['total_amount']) {
                    $discount_amount = $orders['Merqueo'][$store_id]['discount_amount'];
                    $discount_amount -= $orders['Merqueo'][$store_id]['total_amount'];
                    $orders['Merqueo'][$store_id]['discount_amount'] = $orders['Merqueo'][$store_id]['total_amount'];
                    //asignar el resto del descuento a los marketplaces
                    foreach ($orders['Marketplace'] as $store_id => $store) {
                        if ($store['total_amount'] > $discount_amount) {
                            $orders['Marketplace'][$store_id]['discount_amount'] = $discount_amount;
                            break;
                        } else {
                            $discount_amount -= $orders['Marketplace'][$store_id]['total_amount'];
                            $orders['Marketplace'][$store_id]['discount_amount'] = $orders['Marketplace'][$store_id]['total_amount'];
                        }
                    }
                }
            } else {
                //si solo es pedido de marketplace se cobra domicilio y se aplica descuento si tiene
                reset($orders['Marketplace']);
                $store_id = key($orders['Marketplace']);
                $orders['Marketplace'][$store_id]['discount_amount'] = $response['cart']['discount_amount'];
                $orders['Marketplace'][$store_id]['delivery_amount'] = $response['cart']['delivery_amount'];
            }

            //debug($orders);

            //crea un nuevo grupo para este pedido
            $order_group = new OrderGroup;
            $order_group->source = 'Web';
            $order_group->user_id = $this->user_id;
            $order_group->user_firstname = $user->first_name;
            $order_group->user_lastname = $user->last_name;
            $order_group->user_email = $user->email;
            $order_group->user_address = $address->address;
            $order_group->user_address_1 = $address->address_1;
            $order_group->user_address_2 = $address->address_2;
            $order_group->user_address_3 = $address->address_3;
            $order_group->user_address_4 = $address->address_4;
            $order_group->user_address_further = $address->address_further;
            $order_group->user_address_neighborhood = $address->neighborhood;
            $order_group->user_address_latitude = $address->latitude;
            $order_group->user_address_longitude = $address->longitude;
            $order_group->user_city_id = $address->city_id;
            $order_group->economic_stratum_id = !is_null($stratum) ? $stratum->id : null;
            $order_group->user_phone = $user->phone;
            $order_group->address_id = $address_id;
            $order_group->zone_id = isset($zone) ? $zone->id : null;
            $order_group->warehouse_id = isset($zone) ? $zone->warehouse_id : null;
            $order_group->locality_id = $locality_id;
            $order_group->discount_amount = $response['cart']['discount_amount'];
            $order_group->delivery_amount = $response['cart']['delivery_amount'];
            $order_group->total_amount = round($response['cart']['total_amount']);
            $order_group->products_quantity = $response['cart']['total_quantity'];
            $order_group->user_comments = isset($post_data['comments']) ? trim($post_data['comments']) : '';
            $order_group->ip = get_ip();
            $order_group->save();

            $order_group_total_amount = 0;
            $product_events = [];
            $sampling_orders = [];
            foreach ($orders as $type => $order_types) {
                foreach ($order_types as $store_id => $order_data) {
                    //inicializar variables para validar campañas de marca
                    //$is_valid_store = false;
                    //$allowed_storess = [36, 10];

                    $order = new Order();
                    $order->type = $type;
                    $order->reference = generate_reference();
                    $order->user()->associate($user);
                    $order->store_id = $order_data['store_id'];
                    $order->date = date("Y-m-d H:i:s");
                    $order->group_id = $order_group->id;
                    $order->total_products = count($order_data['products']);
                    $order->delivery_amount = $order_data['delivery_amount'];
                    $order->discount_amount = $order_data['discount_amount'];
                    $order->first_total_amount = round($order_data['total_amount']);
                    $order->total_amount = round($order_data['total_amount']);
                    $order->allied_store_id = $order_data['allied_store_id'];
                    $order->deliveryWindow()->associate($response['cart']['stores'][$store_id]['delivery_window']);
                    $order->delivery_time = $delivery_window->delivery_window;
                    if ($order->type === 'Merqueo') {
                        $order->first_delivery_date = $post_data['delivery_day_' . $order_data['store_id']] . ' ' . $delivery_window->hour_end;
                        $order->real_delivery_date = $order->delivery_date = $post_data['delivery_day_' . $order_data['store_id']] . ' ' . $delivery_window->hour_end;
                    } else {
                        $order->real_delivery_date = $order->delivery_date = $order->first_delivery_date = $order_data['delivery_date'];
                    }

                    $order->user_score_token = generate_token();
                    $order->payment_method = $post_data['payment_method'];

                    if (isset($order_data['discount_percentage_amount']))
                        $order->discount_percentage_amount = $order_data['discount_percentage_amount'];

                    if ($is_credit_card) {
                        $order->assignCreditCard(
                            $user_credit_card,
                            empty($post_data['installments_cc']) ? 1 : $post_data['installments_cc']
                        );
                    }

                    //datos de facturación
                    if (isset($post_data['invoice']) && !empty($post_data['invoice'])) {
                        if ($post_data['type_invoice'] == 'natural') {
                            $order->user_identity_type = $post_data['invoice_user_identity_type'];
                            $order->user_identity_number = $post_data['invoice_user_identity_number'];
                            $order->user_business_name = $post_data['invoice_user_name'];
                        } else {
                            $nit = get_only_nit_number($post_data['invoice_company_nit']);
                            $digit = get_check_digit_nit($nit);

                            $order->user_identity_type = 'NIT';
                            $order->user_identity_number = $digit ? $nit . "-" . $digit : $nit;
                            $order->user_business_name = $post_data['invoice_company_name'];
                        }
                    }

                    //validar datos de fraude para estado de pedido
                    $order->validateOrderBlackListed($post_data);

                    // el usuario entrará a la pasarela de pago PSE, el usuario puede
                    // que abandone el proceso en la pantalla inicial y no complete
                    // el ciclo en la pasarela, dado este escenario dejamos el pedido
                    // en estado 'Validation' y con el ID del tipo de validación a
                    // en 5 (Pagos mediante PSE fallidos)
                    if ($order->payment_method === 'Débito - PSE') {
                        $order->status = OrderStatus::VALIDATION;
                        $order->order_validation_reason_id = 5;
                    }

                    $order->save();

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'Pedido creado.';
                    $log->user_id = $this->user_id;
                    $log->order_id = $order->id;
                    $log->save();

                    $order_total_amount = 0;
                    foreach ($order_data['products'] as $cart_product) {
                        $store_product = StoreProduct::select(
                            'products.*',
                            'store_products.*',
                            StoreProduct::getRawPublicPriceQuery(),
                            'departments.name as department_name',
                            'shelves.name as shelf_name',
                            StoreProduct::getRawSpecialPriceQuery($info->qty, $info->last_order_date),
                            StoreProduct::getRawDeliveryDiscountByDate()
                        )
                            ->join('products', 'store_products.product_id', '=', 'products.id')
                            ->join('departments', 'store_products.department_id', '=', 'departments.id')
                            ->join('shelves', 'store_products.shelf_id', '=', 'shelves.id')
                            ->where('store_products.id', $cart_product->id)
                            ->first();
                        //no deja que al pedido le aparezcan productos que no pertenecen a la tienda
                        if ($store_product->store_id != $order_data['store_id'])
                            continue;

                        //INSERTAR PRODUCTOS
                        if ($cart_product->cart_quantity) {
                            //obtener precio de producto (full, especial / primera compra)
                            if ($store_product->special_price > -1) {
                                if ($store_product->first_order_special_price)
                                    if (!$user_has_orders)
                                        $price = $store_product->special_price;
                                    else $price = $store_product->price;
                                else $price = $store_product->special_price;
                            } else $price = $store_product->price;

                            $order_product = OrderProduct::saveProduct($price, $cart_product->cart_quantity, $store_product, $order);
                            $total_order_items += $cart_product->cart_quantity;
                            $order_total_amount += $order_product->price * $order_product->quantity;
                            $discount_total_amount += $order_product->original_price - $order_product->price;

                            $product_events[] = [
                                'product_purchased' => [
                                    'orderId' => $order->id,
                                    'department' => $store_product->department_name,
                                    'departmentId' => $store_product->department_id,
                                    'shelf' => $store_product->shelf_name,
                                    'shelfId' => $store_product->shelf_id,
                                    'storeProductId' => $store_product->id,
                                    'name' => $store_product->name,
                                    'price' => $price,
                                    'specialPrice' => $store_product->special_price,
                                    'bestPriceGuaranteed' => $store_product->is_best_price,
                                    'quantity' => $cart_product->cart_quantity,
                                    'screen' => 'checkout'
                                ]
                            ];

                            $products_mail[] = array(
                                'order_type' => $order->type,
                                'img' => $order_product->product_image_url,
                                'name' => $order_product->product_name,
                                'quantity' => $order_product->product_quantity,
                                'unit' => $order_product->product_unit,
                                'qty' => $order_product->quantity,
                                'price' => $order_product->price,
                                'total' => $order_product->quantity * $order_product->price,
                                'type' => $order_product->type
                            );
                        }

                        //INSERTAR PRODUCTOS ADICIONALES CON PRECIO FULL
                        if ($cart_product->cart_quantity_full_price) {
                            $order_product = OrderProduct::saveProduct($store_product->price, $cart_product->cart_quantity_full_price, $store_product, $order);
                            $total_order_items += $cart_product->cart_quantity_full_price;
                            $order_total_amount += $order_product->price * $order_product->quantity;
                            $discount_total_amount += $order_product->original_price - $order_product->price;

                            $product_events[] = [
                                'product_purchased' => [
                                    'orderId' => $order->id,
                                    'department' => $store_product->department_name,
                                    'departmentId' => $store_product->department_id,
                                    'shelf' => $store_product->shelf_name,
                                    'shelfId' => $store_product->shelf_id,
                                    'storeProductId' => $store_product->id,
                                    'name' => $store_product->name,
                                    'price' => $store_product->price,
                                    'specialPrice' => $store_product->special_price,
                                    'bestPriceGuaranteed' => $store_product->is_best_price,
                                    'quantity' => $cart_product->cart_quantity,
                                    'screen' => 'checkout'
                                ]
                            ];

                            $products_mail[] = array(
                                'order_type' => $order->type,
                                'img' => $order_product->product_image_url,
                                'name' => $order_product->product_name,
                                'quantity' => $order_product->product_quantity,
                                'unit' => $order_product->product_unit,
                                'qty' => $order_product->quantity,
                                'price' => $order_product->price,
                                'total' => $order_product->quantity * $order_product->price,
                                'type' => $order_product->type
                            );
                        }

                        //validar stock en promocion de producto
                        $store_product->validateStockDiscount($cart_product->cart_quantity);
                    }

                    if ($user->free_delivery_next_order) {
                        $user->free_delivery_next_order = 0;
                        $user->save();
                    }

                    if ($order->total_amount != $order_total_amount) {
                        $order->total_amount = $order_total_amount;
                        $order->save();
                    }

                    if ($is_credit_card  && Config::get('app.payu.precharge_enable')) {
                        $preCharge = $order->preChargeCreditCard($user->id);
                        $order->payment_date = null;
                        $order->save();
                        if (isset($preCharge['status']) && !$preCharge['status']) {
                            throw new Exception($preCharge['message']);
                        }
                    }
                    $order_group_total_amount += $order_total_amount;
                    $sampling_orders[] = ['order' => $order, 'products' => $order_data['products']];

                    //verificar condiciones de campaña de marca
                    /*if (!$is_valid_store){
                        if (in_array($store_id, $allowed_stores) && !isset($product_found)){
                            $is_valid_store = true;
                            if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'lavomatic')->first()) {
                                $user_brand_campaign = new UserBrandCampaign;
                                $user_brand_campaign->user_id = $this->user_id;
                                $user_brand_campaign->order_id = $order->id;
                                $user_brand_campaign->campaign = 'lavomatic';
                                $user_brand_campaign->save();
                            }
                        }
                    }
                    //verificar condiciones de campaña de marca
                    if ($address->city_id == 1 && isset($product_found) && $product_found) {
                        if (!UserBrandCampaign::where('user_id', $this->user_id)->where('campaign', 'mini chivas')->first()) {
                            $user_brand_campaign = new UserBrandCampaign;
                            $user_brand_campaign->user_id = $this->user_id;
                            $user_brand_campaign->order_id = $order->id;
                            $user_brand_campaign->campaign = 'mini chivas';
                            $user_brand_campaign->save();
                        }
                    }*/

                    //descontar credito utilizado a usuario
                    if (empty($valid_credit_card_discount) && $order_data['discount_amount'] && (isset($coupon) || $this->user_discounts['amount'])) {
                        if (isset($coupon))
                            UserCredit::removeCredit($user, $order_data['discount_amount'], 'order', $order, $coupon->id);
                        else UserCredit::removeCredit($user, $order_data['discount_amount'], 'order', $order);
                    }

                    //registrar log de domicilio gratis
                    if (!$order->delivery_amount && $user->free_delivery_expiration_date > date('Y-m-d')) {
                        $user_free_delivery = new UserFreeDelivery;
                        $user_free_delivery->user_id = $user->id;
                        $user_free_delivery->order_id = $order->id;
                        $user_free_delivery->amount = null;
                        $user_free_delivery->expiration_date = null;
                        $user_free_delivery->description = 'Domicilio gratis en pedido #' . $order->id . '. Fecha de vencimiento: ' . format_date('normal', $user->free_delivery_expiration_date);
                        $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                        $user_free_delivery->save();
                    }

                    if ($order_group->total_amount != $order_group_total_amount) {
                        $order_group->total_amount = $order_group_total_amount;
                        $order_group->save();
                    }

                    //guardar descuento en pedido
                    if (isset($order_discount_data)) {
                        foreach ($order_discount_data as $discount) {
                            $discount->order()->associate($order);
                            $discount->save();
                        }
                    }
                }
            }

            //inactivar productos comprometidos
            $order->validateCommittedStock();
            //valida si el usuario es de domicilios.com
            $order->validateDarkSuperMarketOrder();

            //cuando el medio de pago es PSE
            $url_pse_bank = '';
            if ($post_data['payment_method'] == 'Débito - PSE') {
                $response_payu = $order->chargePSE($order->id, $user->id, $post_data);
                if ($response_payu['status'])
                    $url_pse_bank = $response_payu['response'];
                else throw new Exception($response_payu['message']);
            }

            //insertar productos sampling
            foreach ($sampling_orders as $sampling) {
                Sampling::getSamplingRelations($order_group, $sampling['products'], $sampling['order']);
            }

            // Campañas de regalo
            $gift = "";//CampaignGift::validateCampaignGift($zone->warehouse_id, $order);

            //guardar datos de campaña si existen
            if (Session::has('campaign_log')) {
                $data = Session::get('campaign_log');
                $campaign_log = new CampaignLog;
                $campaign_log->group_id = $order_group->id;
                $campaign_log->source = $data['source'];
                $campaign_log->campaign = $data['campaign'];
                $campaign_log->medium = $data['medium'];
                $campaign_log->save();
                Session::forget('campaign_log');
                Session::save();
            }

            //elimina productos del carrito
            CartProduct::where('cart_id', $cart_id)->delete();
            if (Session::has('tmp_cart')) {
                Session::forget('tmp_cart');
            }
            Session::forget('removed_products');
            Session::forget('referred_code');
            Session::forget('coupon_code');
            Session::forget('facebookData');
            Session::forget('phoneValidation');
            Session::save();

            DB::commit();

            //enviar mails
            $order_data = [
                'is_new_user' => isset($post_data['create_user']) ? 1 : 0,
                'products_mail' => $products_mail,
                'referred' => User::validateReferred($user, null, true),
                'total_products' => count($cart_products),
                'total_items' => $total_order_items,
                'product_events' => $product_events
            ];
            Event::subscribe(new OrderEventHandler);
            Event::fire('order.created', [[$order, $order_data]]);

            $type = isset($post_data['create_user']) ? 'new' : 'old';
            $message = '';
            if (isset($orders['Merqueo']) && isset($orders['Marketplace']))
                $message = 'Algunos productos serán entregados en una fecha diferente a la seleccionada.';
            else if (!isset($orders['Merqueo']) && isset($orders['Marketplace']))
                $message = 'Debido a los productos de tu pedido la fecha de entrega ha cambiado.';

            Session::put('checkout_success', $order_group->id);
            if (empty($gift)) {
               $gift['win_gift'] = false;
            }
            return Redirect::route('frontCheckout.checkout_success', ['order_reference' => $order->reference, $type => ''])
                ->with('gift', $gift)
                ->with('message', $message)
                ->with('url_pse_bank', $url_pse_bank);
        } catch (Exception $exception) {
            DB::rollback();
            //guardar log
            try {
                $response_headers = '';
                $request = json_encode(Input::get());
                $request_headers = '';
                foreach (getallheaders() as $name => $value) {
                    $request_headers .= '
                    ' . $name . ': ' . $value;
                }
            } catch (Exception $e) {
                $request = is_string($request) ? $request : 'Error en json_encode: Invalid UTF-8 sequence in argument';
            }

            //encriptacion de tarjeta de credito
            if (isset($request['number_cc']) && !empty($request['number_cc'])) {
                $request['number_cc'] = substr($request['number_cc'], 0, 6) . '******' . substr($request['number_cc'], 12, 16);
                $request['code_cc'] = '***';
            }

            $log = new CheckoutLog;
            $log->url = URL::current();
            $log->request = "URL: " . $log->url . ' ' . Request::method() . "\r\nREQUEST HEADERS: " . $request_headers . "\r\nREQUEST: " . $request;
            $log->message = "MESSAGE: " . $exception->getMessage() . "\r\nFILE: " . $exception->getFile() . "\r\nLINE: " . $exception->getLine();
            $log->ip = get_ip();
            $log->created_at = $log->updated_at = date('Y-m-d H:i:s');
            $log->save();

            Session::put('address', $current_session_address);

            if ($exception instanceof QueryException && !Config::get('app.debug')) {
                return Redirect::route('frontCheckout.index')
                    ->with('message', 'Ocurrió un error al registrar tu pedido. Por favor intentalo nuevamente.')
                    ->with('post', $post_data);
            } else {
                return Redirect::route('frontCheckout.index')
                    ->with('message', $exception->getMessage()/*.' '.$exception->getLine().' '.$exception->getFile()*/)
                    ->with('post', $post_data);
            }
        }
    }

    /**
     * Pagina de éxito al finalizar compra
     */
    public function success($order_reference)
    {
        if (Session::has('checkout_success')) {
            $order_group_id = Session::get('checkout_success');
            Session::forget('checkout_success');
            $referred = Session::get('referred');
            $url_pse_bank = Session::get('url_pse_bank');
            Session::forget('url_pse_bank');
            Session::forget('referred');
            $gift = Session::get('gift');
            Session::forget('gift');

            $order_group = OrderGroup::find($order_group_id);
            $orders = Order::where('group_id', $order_group->id)->get();
            $order_reference = '';
            $order_data = array();
            foreach ($orders as $order) {
                $order_data['order_id'] = $order->id;
                $order_reference .= $order->reference . ' - ';
                $order_products = OrderProduct::where('order_id', $order->id)->get();
                foreach ($order_products as $product) {
                    $product_ids[] = $product->store_product_id;
                }
            }
            $address = UserAddress::select('user_address.address_further', 'user_address.neighborhood')->where('user_address.id', $order_group->address_id)->first();
            $order_data['reference'] = trim($order_reference, ' - ');
            $order_data['delivery_date'] = $order->delivery_time == 'Immediately' ? format_date('normal_with_time', $order->delivery_date) : format_date('normal', substr($order->delivery_date, 0, 10)) . ' ' . $order->delivery_time;
            $order_data['date'] = format_date('normal_with_time', $order_group->created_at);
            $order_data['total'] = $order_group->total_amount + $order_group->delivery_amount - $order_group->discount_amount;
            $order_data['payment_method'] = $order->payment_method;
            $order_data['address'] = $order_group->user_address . ' ' . $address->address_further . ' ' . $address->neighborhood;
            $order_data['products'] = $order_group->products_quantity;

            $content_ids = implode("', '", $product_ids);
            $content_ids = "'" . $content_ids . "'";

            $survey = false;
            $count_orders = Order::where('user_id', '=', Auth::user()->id)->count();
            if ($count_orders == 1) {
                $survey = Survey::find(1);
                $answers = explode(';', $survey->answers);
                shuffle($answers);
                $survey = array(
                    'id' => $survey->id,
                    'question' => $survey->question,
                    'answers' => $answers
                );
            }

            $user = User::find($order_group->user_id);

            if ($url_pse_bank != '' && $order_data['payment_method'] == 'Débito - PSE') {

                return View::make('checkout_payu_redirect')->with('order_data', $order_data)
                    ->with('url_pse_bank', $url_pse_bank)
                    ->with('footer', $this->getFooter())
                    ->with('user', $user);
            } else {
                return View::make('checkout_success')->with('store', $this->store)
                    ->with('user_discounts', $this->user_discounts)
                    ->with('stores', $this->stores)
                    ->with('order_data', $order_data)
                    ->with('content_ids', $content_ids)
                    ->with('referred', $referred)
                    ->with('url_pse_bank', $url_pse_bank)
                    ->with('gift', $gift)
                    ->with('survey', $survey)
                    ->with('no_address_modal', true)
                    ->with('footer', $this->getFooter())
                    ->with('user', $user);
            }
        } else return Redirect::to('/');
    }

    /**
     * Eliminar cupon en compra
     */
    public function coupon_remove()
    {
        Session::forget('referred_code');
        Session::forget('coupon_code');
        Session::save();

        return Response::json(array('status' => true), 200);
    }

    /**
     * Redimir cupon o codigo de referido en compra
     */
    public function coupon()
    {
        $properties = Auth::check() ? Auth::user()->getAnalyticsBasicProperties() : [];
        // Se debe agregar el Identificador del dispositivo.
        $modifier = function (Track $track) {
            $track->set('deviceId', Input::get('device_id'));
            $track->set('os', Input::get('os'));
        };

        $response = array('status' => 0, 'message' => 'Ocurrió un error al redimir el cupón.');
        if (Input::has('coupon_code')) {
            if (Input::has('user_id')) {
                $user_id = Input::get('user_id');
            } elseif (Auth::check()) {
                $user_id = Auth::user()->id;
            } else {
                $user_id = 0;
            }

            //validar codigo del cupon
            $coupon = Coupon::where('code', Input::get('coupon_code'))->first();
            if (!$coupon) {
                $referrer = User::where('referral_code', Input::get('coupon_code'))->where('id', '<>', $user_id)->where('status', 1)->first();
                $referrerCount = User::where('referral_code', Input::get('coupon_code'))->count();
                if (!$referrer) {
                    $response['message'] = 'El código no es valido.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'referido';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                } else {
                    $response['message'] = 'Para referir a otro usuario debes hacerlo desde nuestra app.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'referido';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                if ($referrer->referral_code_blocked) {
                    $response['message'] = 'El código referido esta bloqueado.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                if (!$referrer->orders()->where('status', 'Delivered')->count()) {
                    $response['message'] = $referrer->first_name . ' debe recibir su primer pedido para que puedas utilizar el beneficio de créditos por referido.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                if($referrerCount >= Config::get('app.merqueo.referred_limit')){
                    $response['message'] = $referrer->first_name.' ha alcanzado el número máximo de referidos.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
            }

            //redimir cupon de descuento
            if ($coupon) {
                //validar estado
                if (!$coupon->status) {
                    $response['message'] = 'El cupón ya no esta activo.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                //validar numero de usos
                $uses = UserCoupon::where('coupon_id', $coupon->id)->count();
                if ($uses >= $coupon->number_uses) {
                    $response['message'] = 'El cupón ya fue utilizado.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                //validar fecha de expiracion
                if (date('Y-m-d') > $coupon->expiration_date) {
                    $response['message'] = 'El cupón ya no esta vigente.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }

                //validar si el usuario ya utilizo el cupon
                $user_coupon = $user_id ? UserCoupon::where('user_id', $user_id)->where('coupon_id', $coupon->id)->first() : false;
                if ($user_coupon) {
                    $response['message'] = 'Ya redimiste este cupón.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                //validar credito de primera compra
                /*if ($user_id){
                    $user = User::find($user_id);
                    if ($user && $user->first_coupon_used){
                        $response['message'] = 'Este tipo de cupón ya fue redimido en tu cuenta.';
                        return Response::json($response, 200);
                    }
                }*/
                //validar el tipo de uso del cupon
                if ($user_id && $coupon->type_use == 'Only The First Order' && !Input::has('is_new_user')) {
                    $count_orders = Order::where('user_id', '=', $user_id);
                    //ignorar pedido especifico
                    if (Input::has('order_id')) {
                        $count_orders->where('id', '<>', Input::get('order_id'));
                    }
                    $count_orders = $count_orders->count();
                    if ($count_orders) {
                        $response['message'] = 'Este cupón solo podías utilizarlo en tu primera compra.';
                        $properties['approved'] = 0;
                        $properties['type'] = 'cupón';
                        track_event('code_entered', $user_id, $properties, $modifier);
                        return Response::json($response, 200);
                    }
                }
                if ($user_id && $coupon->type_use == 'Only New Customers' && !Input::has('is_new_user')) {
                    $response['message'] = 'Este cupón solo podías utilizarlo al crear tu cuenta de Merqueo.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'cupón';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                //validar cupon en grupo de campaña
                if ($user_id && !empty($coupon->campaign_validation)) {
                    $campaign = Coupon::select('coupons.id')
                        ->join('user_coupons', function ($join) use ($user_id) {
                            $join->on('user_coupons.coupon_id', '=', 'coupons.id');
                            $join->on('user_coupons.user_id', '=', DB::raw($user_id));
                        })
                        ->where('coupons.campaign_validation', $coupon->campaign_validation)
                        ->first();

                    if ($campaign) {
                        $response['message'] = 'Ya usaste un cupón de esta campaña.';
                        $properties['approved'] = 0;
                        $properties['type'] = 'cupón';
                        track_event('code_entered', $user_id, $properties, $modifier);
                        return Response::json($response, 200);
                    }
                }
                //validar cupon a tienda / categoria /subcategoria / producto especifico
                if ($coupon->redeem_on == 'Specific Store') {
                    //validar contra pedido
                    if (Input::has('order_id')) {
                        $order_products = OrderProduct::select(
                            'products.id',
                            'store_products.department_id',
                            'store_products.shelf_id',
                            'store_products.store_id',
                            'order_products.price',
                            'order_products.quantity AS cart_quantity'
                        )
                            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                            ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                            ->join('departments', 'departments.id', '=', 'shelves.department_id')
                            ->where('order_id', Input::get('order_id'))->get();
                        $discount_amount = 0;
                        foreach ($order_products as $product) {
                            $is_valid = false;
                            if ($product->store_id == $coupon->store_id) {
                                if ($coupon->department_id) {
                                    if ($product->department_id == $coupon->department_id) {
                                        if ($coupon->shelf_id) {
                                            if ($product->shelf_id == $coupon->shelf_id) {
                                                if ($coupon->store_product_id) {
                                                    if ($product->id == $coupon->store_product_id) {
                                                        $is_valid = true;
                                                    }
                                                } else {
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $is_valid = true;
                                        }
                                    }
                                } else {
                                    $is_valid = true;
                                }
                            }
                            if ($is_valid) {
                                if ($coupon->type == 'Discount Percentage') {
                                    $discount_amount += round(($product->price * $product->cart_quantity) * ($coupon->amount / 100), 0);
                                }
                                if ($coupon->type == 'Credit Amount') {
                                    $discount_amount += round($product->price * $product->cart_quantity, 0);
                                }
                            }
                        }

                        if (!$discount_amount) {
                            $store = Store::select(
                                'stores.name AS store_name',
                                'departments.name AS department_name',
                                'shelves.name AS shelf_name',
                                DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"),
                                'cities.city'
                            )
                                ->leftJoin('departments', function ($join) use ($coupon) {
                                    $join->on('departments.store_id', '=', 'stores.id');
                                    $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                                })
                                ->leftJoin('shelves', function ($join) use ($coupon) {
                                    $join->on('shelves.department_id', '=', 'departments.id');
                                    $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                                })
                                ->leftJoin('store_products', function ($join) use ($coupon) {
                                    $join->on('store_products.shelf_id', '=', 'shelves.id');
                                    $join->on('store_products.id', '=', DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                                })
                                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                ->join('cities', 'cities.id', '=', 'stores.city_id')
                                ->where('stores.id', $coupon->store_id)
                                ->first();
                            $message = '';
                            if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name))
                                $message = $store->department_name;
                            if (!empty($store->shelf_name) && empty($store->product_name))
                                $message = $store->shelf_name;
                            if (!empty($store->product_name))
                                $message = $store->product_name;
                            $message .= ' en ' . $store->city;

                            $response['message'] = 'Este cupón aplica exclusivamente para <b>' . $message . '</b> y deben estar en el pedido.';
                            $properties['approved'] = 0;
                            $properties['type'] = 'cupón';
                            track_event('code_entered', $user_id, $properties, $modifier);
                            return Response::json($response, 200);
                        } else {
                            if ($coupon->type == 'Credit Amount' && $discount_amount > $coupon->amount) {
                                $discount_amount = $coupon->amount;
                            }
                        }
                    } else {
                        //validar contra carrito
                        if ($products = $this->currentCart('getProducts')) {
                            $is_valid = false;
                            foreach ($products as $product) {
                                if ($product->store_id == $coupon->store_id) {
                                    if ($coupon->department_id) {
                                        if ($product->department_id == $coupon->department_id) {
                                            if ($coupon->shelf_id) {
                                                if ($product->shelf_id == $coupon->shelf_id) {
                                                    if ($coupon->store_product_id) {
                                                        if ($product->id == $coupon->store_product_id) {
                                                            $is_valid = true;
                                                        }
                                                    } else {
                                                        $is_valid = true;
                                                    }
                                                }
                                            } else {
                                                $is_valid = true;
                                            }
                                        }
                                    } else {
                                        $is_valid = true;
                                    }
                                }
                            }
                            if (!$is_valid) {
                                $store = Store::select(
                                    'stores.name AS store_name',
                                    'departments.name AS department_name',
                                    'shelves.name AS shelf_name',
                                    DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"),
                                    'cities.city'
                                )
                                    ->leftJoin('departments', function ($join) use ($coupon) {
                                        $join->on('departments.store_id', '=', 'stores.id');
                                        $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                                    })
                                    ->leftJoin('shelves', function ($join) use ($coupon) {
                                        $join->on('shelves.department_id', '=', 'departments.id');
                                        $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                                    })
                                    ->leftJoin('store_products', function ($join) use ($coupon) {
                                        $join->on('store_products.shelf_id', '=', 'shelves.id');
                                        $join->on('store_products.id', '=', DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                                    })
                                    ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                    ->join('cities', 'cities.id', '=', 'stores.city_id')
                                    ->where('stores.id', $coupon->store_id)
                                    ->first();
                                $message = '';
                                if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name))
                                    $message = $store->department_name;
                                if (!empty($store->shelf_name) && empty($store->product_name))
                                    $message = $store->shelf_name;
                                if (!empty($store->product_name))
                                    $message = $store->product_name;
                                $message .= ' en ' . $store->city;

                                $response['message'] = 'Este cupón aplica exclusivamente para <b>' . $message . '</b> y debe estar en el carrito.';
                                $properties['approved'] = 0;
                                $properties['type'] = 'cupón';
                                track_event('code_entered', $user_id, $properties, $modifier);
                                return Response::json($response, 200);
                            }
                        }
                    }
                }

                //validar que el carrito no tenga productos en promocion
                /*if ($this->currentCart('hasSpecialprice')){
                    $response['message'] = 'No puedes redimir el cupón por que ya tienes productos en promoción en el carrito.';
                    return Response::json($response, 200);
                }*/

                //validar antes de procesar compra
                if (Input::has('validate')) {
                    $response = array(
                        'status' => 1,
                        'coupon' => $coupon
                    );
                    if (isset($discount_amount)) {
                        $response['discount_amount'] = $discount_amount;
                    }
                    return Response::json($response, 200);
                }

                $message = 'El cupón ha sido agregado a tu pedido actual.';
                $total = $this->currentCart('getTotal');
                if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount) {
                    $message .= ' Para usarlo el total del pedido debe ser mayor o igual a <b>$' . number_format($coupon->minimum_order_amount, 0, ',', '.') . '</b>.';
                } elseif ($coupon->maximum_order_amount && !$coupon->minimum_order_amount) {
                    $message .= ' Para usarlo el total del pedido debe ser menor o igual a <b>$' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '</b>.';
                } elseif ($coupon->minimum_order_amount && $coupon->maximum_order_amount) {
                    $message .= ' Para usarlo el total del pedido debe estar entre <b>$' . number_format($coupon->minimum_order_amount, 0, ',', '.') . '</b> y <b>$' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '</b>.';
                }
                if ($coupon->payment_method) {
                    $message .= ' El metodo de pago debe ser con <b>' . $this->config['payment_methods_names'][$coupon->payment_method] . ' ' . $coupon->cc_bank . '</b>';
                }

                $html = '<div class="checkout-coupon">
                    <label>Cupón de descuento activo</label>
                    <div>' . $coupon->code . '&nbsp;<button type="button" class="close btn-remove-coupon" aria-label="Close" title="Desmarcar cupón"><span aria-hidden="true">&times;</span></button></div>
                </div>';

                // Track Coupon Applied event
                $properties['approved'] = 1;
                $properties['type'] = 'cupón';
                track_event('code_entered', $user_id, $properties, $modifier);

                Session::forget('referred_code');
                Session::put('coupon_code', $coupon->code);
                Session::save();
            } else {
                //codigo de referido
                if ($user_id) {
                    if ($order = Order::where('user_id', $user_id)->where('status', 'Delivered')->first()) {
                        $response['message'] = 'No puedes agregar un código referido por que ya hiciste tu primer pedido.';
                        $properties['approved'] = 0;
                        $properties['type'] = 'referido';
                        track_event('code_entered', $user_id, $properties, $modifier);
                        return Response::json($response, 200);
                    }
                    if (Auth::check() && !empty(Auth::user()->referred_by)) {
                        $response['message'] = 'Ya redimiste un código de referido.';
                        $properties['approved'] = 0;
                        $properties['type'] = 'referido';
                        track_event('code_entered', $user_id, $properties, $modifier);
                        return Response::json($response, 200);
                    }
                }
                if ($referrer->total_referrals >= Config::get('app.referred.limit')) {
                    $response['message'] = 'El código referido que ingresaste no es válido porque ha alcanzado el limite permitido de referidos por cuenta.';
                    $properties['approved'] = 0;
                    $properties['type'] = 'referido';
                    track_event('code_entered', $user_id, $properties, $modifier);
                    return Response::json($response, 200);
                }
                //validar antes de procesar compra
                if (Input::has('validate')) {
                    return Response::json(array('status' => 1), 200);
                }

                $message = 'Tienes ' . currency_format(Config::get('app.referred.amount')) . ' de crédito, 
                para usarlos el subtotal de tu pedido debe ser mínimo de ' . currency_format(Config::get('app.minimum_discount_amount')) . '.';

                $html = '<div class="checkout-coupon">
                    <label>Código de referido activo</label>
                    <div>' . $referrer->referral_code . '&nbsp;<button type="button" class="close btn-remove-coupon" aria-label="Close" title="Desmarcar código"><span aria-hidden="true">&times;</span></button></div>
                </div>';

                // Track Product Viewed event
                $properties['approved'] = 1;
                $properties['type'] = 'referido';
                track_event('code_entered', $user_id, $properties, $modifier);

                Session::forget('coupon_code');
                Session::put('referred_code', $referrer->referral_code);
                Session::save();
            }

            $response = array(
                'status' => 1,
                'message' => $message,
                'html' => $html
            );
        }

        return Response::json($response, 200);
    }

    /**
     * Confirmar estado de transaccion de pago con tarjeta de credito
     */
    public function payment_confirmation()
    {
        if (Request::isMethod('post')) {
            if (!Input::has('transaction_id') || !Input::has('state_pol')) {
                App::abort(404);
            }

            $order_payment = OrderPayment::where('cc_payment_transaction_id', Input::get('transaction_id'))->first();
            if ($order_payment) {
                $order = Order::find($order_payment->order_id);
                if (!$order) {
                    App::abort(404);
                }

                if (Input::get('state_pol') == 4) {
                    $order_payment->cc_payment_status = 'Aprobada';
                    if (!$order_payment->is_precharge) {
                        if ($order->status == 'Cancelled' && $order->posible_fraud == 'Tarjeta de crédito nueva') {
                            $order->status = 'Validation';
                        } elseif($order->status == 'Cancelled') {
                            $order->status = 'Initiated';
                        }
                        $order->payment_date = date('Y-m-d H:i:s');
                    }
                } else if (Input::get('state_pol') == 6) {
                    $order_payment->cc_payment_status = 'Declinada';
                    $order->payment_date = null;
                } else {
                    if (Input::get('state_pol') == 5) {
                        $order_payment->cc_payment_status = 'Expirada';
                        $order->payment_date = null;
                    }
                }
                if (!$order_payment->cc_payment_description) {
                    $order_payment->cc_payment_description = Input::get('response_message_pol');
                }
                $order_payment->save();
                $order->save();
            }
        } else {
            App::abort(404);
        }

        return Response::json('Ok');
    }

    /**
     * Obtiene informacion del carrito actual
     *
     * @param int $action Accion a ejecutar
     * @param int $store_product_id ID product
     *
     */
    protected function currentCart($action, $store_product_id = null)
    {
        $cart_id = Session::get('cart_id');
        if (Auth::check()) {
            $cart = Cart::where('user_id', $this->user_id)->where('id', $cart_id)->first();
        } else {
            if (Session::has('tmp_cart')) {
                $cart = json_decode(Session::get('tmp_cart'));
            } else {
                //si no hay un carrito, crear un esqueleto del nuevo carrito para la sesión
                $cart = (object) array('products' => (object) array());
                Session::put('tmp_cart', json_encode($cart));
            }
        }

        if (Auth::check()) {
            // Obtiene el carrito por la DB
            $cart_products = DB::table('cart_products')
                ->where('cart_id', $cart_id)
                ->join('store_products', 'cart_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->leftJoin('users', 'cart_products.added_by', '=', 'users.id')
                ->select(
                    'products.*',
                    'store_products.*',
                    'cart_products.quantity AS cart_quantity',
                    'cart_products.product_name',
                    'cart_products.comment AS cart_comment',
                    'cart_products.store_id AS cart_store_id',
                    'store_products.product_id AS product_id',
                    StoreProduct::getRawDeliveryDiscountByDate(),
                    'users.first_name AS added_by'
                )
                ->orderBy('store_id')
                ->get();
        } else {
            // Se simula obtener el carrito por DB para el usuario guest que sólo maneja sesión
            $store_products_ids = array();
            $cart_products = array();
            foreach ($cart->products as $product) {
                if (property_exists($product, 'cart_quantity') && $product->cart_quantity <= 0)
                    continue;
                $store_products_ids[] = $product->store_product_id;
            }
            $store_products = StoreProduct::select('products.*', 'store_products.*', StoreProduct::getRawDeliveryDiscountByDate())
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->whereIn('store_products.id', $store_products_ids)
                ->get();

            // Armo la estructura de los productos:
            foreach ($store_products as $store_product) {
                $cart_products[$store_product->id] = (object) array_merge($store_product->toArray(), (array) $cart->products->{$store_product->id});
            }
        }

        $has_special_price = false;
        $cart = array(
            'total_amount' => 0,
            'delivery_amount' => 0,
            'total_quantity' => 0
        );

        foreach ($cart_products as $cart_product) {
            $store_id = $cart_product->store_id;
            if (!isset($cart['stores']) || !isset($cart['stores'][$store_id])) {
                $store_id = $cart_product->store_id;
                $store = Store::find($store_id)->toArray();
                $store['sub_total'] = 0;
                $cart['stores'][$store_id] = $store;
                $cart['stores'][$store_id]['products'] = array();
                $cart['stores'][$store_id]['delivery_amount'] = 0;
                $cart['stores'][$store_id]['discount_amount'] = 0;
            }

            $store_product = json_decode(json_encode($cart_product), true);
            if ($store_product['special_price'] > -1) {
                $price = $store_product['special_price'];
                $has_special_price = true;
            } else $price = $store_product['price'];
            $store_product['sub_total'] = $store_product['cart_quantity'] * $price;
            $cart['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
            $cart['total_amount'] += $store_product['sub_total'];
            $cart['total_quantity']++;
            array_push($cart['stores'][$store_id]['products'], $store_product);
        }

        //validar si el producto esta agregado en el carrito actual
        if ($action == 'productIsInCart') {
            if (isset($cart_products) && array_key_exists($store_product_id, $cart_products)) {
                return true;
            } else {
                return false;
            }
        }

        //obtener productos en carrito
        if ($action == 'getProducts') {
            return $cart_products;
        }

        //obtener total del carrito
        if ($action == 'getTotal') {
            return $cart['total_amount'] + $cart['delivery_amount'];
        }
        //varificar si tiene precio especial
        if ($action == 'hasSpecialprice') {
            return $has_special_price;
        }

        return false;
    }

    /**
     * Muestra el listado de productos que van a ser eliminados
     * en caso de que haya un cambio en la bodega seleccionada.
     *
     * @return null
     */
    public function get_changed_products()
    {
        $possible_zone = Zone::with('warehouse')->find(Session::get('possible_zone_id'));
        $cart = Cart::find(Session::get('cart_id')) ?: json_decode(Session::get('tmp_cart'));
        Session::forget(self::REMOVED_PRODUCTS_SESSION_KEY);

        if (empty($possible_zone)) {
            $possible_zone = $this->getCurrentWarehouse()->zones()->with('warehouse')->first();
        }

        $warehouse = $possible_zone->warehouse;

        if (!empty($cart) && empty($cart->warehouse_id) && $cart instanceof StdClass) {
            $cart->warehouse_id = 0;
            Session::put('tmp_cart', json_encode($cart));
        }

        if (empty($cart) || (isset($cart->warehouse_id) && $warehouse->id === $cart->warehouse_id)) {
            $warehouse_changed = empty($cart)
                ? $this->getCurrentWarehouse()->id != $warehouse->id
                : $warehouse->id != $cart->warehouse_id;

            $this->alter_cart_content_by_zone();
            $address_text = empty(Session::get('address')->address_text)
                ? '' : Session::get('address')->address_text;

            return [
                'status' => true,
                'message' => '',
                'result' => [
                    'removed_products' => [],
                    'warehouse_changed' => $warehouse_changed,
                    'address_text' => $address_text
                ]
            ];
        }

        $removed_products = Cart::sanitizeByWarehouse($warehouse, $this->sanitize_web_cart($cart));
        if (empty($removed_products)) {
            $this->alter_cart_content_by_zone();
            $address_text = empty(Session::get('address')->address_text)
                ? '' : Session::get('address')->address_text;

            return [
                'status' => true,
                'message' => '',
                'result' => [
                    'removed_products' => [],
                    'warehouse_changed' => true,
                    'address_text' => $address_text
                ]
            ];
        }

        Session::put(self::REMOVED_PRODUCTS_SESSION_KEY, $removed_products);
        $html_content = View::make('removed_products_list', compact('removed_products'))->render();
        $address_text = empty(Session::get('address')->address_text)
            ? '' : Session::get('address')->address_text;

        return [
            'status' => true,
            'message' => 'Al cambiar de ubicación, tu carrito se ha modificado. ' .
                'Los siguientes productos no están disponibles por el momento',
            'result' => compact('html_content', 'address_text', 'removed_products')
        ];
    }

    /**
     * Genera un carrito con la misma estructura de
     * del api para obtener los productos eliminados.
     *
     * @param Cart|StdClass $cart
     * @return StdClass
     */
    private function sanitize_web_cart($cart)
    {
        if ($cart instanceof Cart) {
            $common_cart = new StdClass();
            $cart->load('products.storeProduct.product');
            foreach ($cart->products as $product) {
                $item = new StdClass();
                $was_removed = empty($product->storeProduct->product);
                $item->qty = $product->quantity;
                $item->price = $was_removed
                    ? 'Producto no disponible'
                    : ($product->storeProduct->special_price ?: $product->storeProduct->price) * $product->quantity;
                $item->name = $was_removed
                    ? 'Producto no disponible'
                    : $product->storeProduct->product->name;

                $common_cart->{$product->store_product_id} = $item;
            }

            return $common_cart;
        }

        foreach ($cart->products as $product) {
            $product->qty = $product->cart_quantity;
            // El atributo "price" debe tener un valor asignado
            // en caso contrario no superara la validación basica del
            // método "sanitizeByWarehouse".
            $product->price = 'NOT EMPTY';
        }

        return $cart->products;
    }

    /**
     * Elimina los productos que han cambiado del carrito, asigna
     * las variables de la dirección, asigna el .
     *
     * @return null
     */
    public function alter_cart_content_by_zone()
    {
        $cart = Cart::find(Session::get('cart_id')) ?: json_decode(Session::get('tmp_cart'));
        $is_temporal_cart = $cart instanceof StdClass;
        Session::put('address', Session::pull('possible_address'));
        Session::put('coverage', Session::pull('possible_coverage'));
        Session::put('zone_id', Session::pull('possible_zone_id'));

        if (empty($cart)) {
            // TODO Carrito eliminado o vacio.
            return null;
        }

        $current_warehouse = $this->getCurrentWarehouse();

        if ($current_warehouse->id != $cart->warehouse_id) {
            $cart->warehouse_id = $current_warehouse->id;
            if (!$is_temporal_cart) {
                $cart->save();
            }
        }

        $removed_products = Session::pull(self::REMOVED_PRODUCTS_SESSION_KEY);
        if (!is_array($removed_products)) {
            $removed_products = [];
        }

        foreach ($removed_products as $product) {
            if (!$is_temporal_cart) {
                $cart->products()->where('store_product_id', $product->id)->delete();
            } else if (!empty($cart->products->{$product->id})) {
                unset($cart->products->{$product->id});
            }
        }

        if ($is_temporal_cart) {
            Session::put('tmp_cart', json_encode($cart));
        }
    }

    /**
     * Remueve los productos que no están disponibles en la bodega
     * del carrito, el listado de productos se determina en
     * el checkout mientras se agrega cada producto al pedido.
     */
    public function remove_limited_products()
    {
        $temporal_cart = json_decode(Session::get('tmp_cart'));
        $removed_products = Session::get('removed_products');
        $cart_id = Session::get('cart_id');

        if (!empty($cart_id)) {
            foreach ($removed_products as $product) {
                CartProduct::where('cart_id', $cart_id)
                    ->where('store_product_id', $product->id)
                    ->delete();
            }
        }

        foreach ($removed_products as $product) {
            if (!empty($temporal_cart->products->{$product->id})) {
                unset($temporal_cart->products->{$product->id});
            }
        }

        Session::put('tmp_cart', json_encode($temporal_cart));
    }

    /**
     * Muestra la vista resultado de la transaccion realizada en PSE
     */
    public function payu_response()
    {
        if (!Input::has('referenceCode')) {
            return Redirect::to('/');
        }
        $payment = OrderPayment::where('cc_payment_transaction_id', Input::get('transactionId'))->get()->first();
        $order = Order::find($payment->order_id);
        $user = User::find($order->user_id);
        $payu = new PayU;
        $status_transaction = $payu->getPseTransactionStatus(Input::get('polTransactionState'), Input::get('polResponseCode'), Input::all());
        $transaction_response = $payu->getPseTransactionResponse(Input::get('transactionId'));

        $data = [
            'reference_code' => Input::get('referenceCode'),
            'transaction_id' => Input::get('transactionId'),
            'cus' => Input::get('cus'),
            'pse_bank' => Input::get('pseBank'),
            'tx_value' => number_format(Input::get('TX_VALUE')),
            'currency' => Input::get('currency'),
            'description' => Input::get('description'),
            'ip_origen' => Input::get('pseReference1'),
            'date_payment' => format_date('normal', date('Y-m-d H:i:s')),
            'status_transaction_result' => $status_transaction['result'],
            'status_transaction' => $status_transaction['message'],
            'response_code' => $transaction_response['response']->responseCode,
            'order' => $order,
            'user' => $user,
            'url_retry' => route('PayU.retryTransaction', ['reference' => $order->reference]),
            'url_end_transaction' => route('frontCheckout.checkout_success', ['order_reference' => $order->reference])
        ];

        if ($transaction_response['response']->responseCode == Input::get('lapResponseCode')) {

            switch ($status_transaction['result']) {
                case 'APPROVED':
                    $payment->cc_payment_status = 'Aprobada';

                    // la variable "email_sent_to_client_*" puesta en le sesión previene
                    // que el email de pedido recibido sea enviado muchas veces
                    $sessionVariableName = "email_sent_to_client_{$order->id}";
                    if (!Session::has($sessionVariableName)) {
                        // un usuario es nuevo si tiene un sólo pedido, de ser
                        // así enviamos email de bienvenida
                        if ($order->user->orders()->count() === 1) {
                            $sendWelcomeEmailToUserAction = new SendWelcomeEmailToUserAction($order->user);
                            $sendWelcomeEmailToUserAction->run();
                        }

                        $sendOrderReceivedEmailAction = new SendOrderReceivedEmailAction($order);
                        $sendOrderReceivedEmailAction->run();

                        Session::put($sessionVariableName, true);
                    }

                    break;
                case 'FAIL':
                    $payment->cc_payment_status = 'Fallida';
                    break;
                case 'REJECT':
                    $payment->cc_payment_status = 'Declinada';
                    break;
                case 'PENDING':
                    $payment->cc_payment_status = 'Pendiente';
                    break;
            }

            Session::put('checkout_success', $order->group_id);
            $payment->save();
            $order->save();
            return View::make('checkout_payu_response', $data)
                ->with('footer', $this->getFooter())
                ->with('survey', false);
        } else {
            $data['status_transaction'] = 'Transacción pendiente, por favor revisar si el débito fue realizado en el banco.';
            return View::make('checkout_payu_response', $data)
                ->with('footer', $this->getFooter())
                ->with('survey', false)
                ->with('message', 'Transacción pendiente, por favor revisar si el débito fue realizado en el banco.');
        }
    }

    /**
     * Vista para reintentar transaccion de pago con PSE
     *
     * @param string $reference referencia del pedido
     *
     */
    public function payu_retry_transaction($reference)
    {
        if (Auth::check()) {

            $order = Order::where('reference', $reference)->get()->first();
            $user = User::find($this->user_id);
            $payment_methods = $this->config['payment_methods'];
            $banks = PseBank::where('pseCode', '!=', '0')->orderBy('description')->get();
            $credit_cards = UserCreditCard::where('user_id', $this->user_id)->get()->toArray();

            $discount_percentage = '';
            if (date('Y-m-d') > '2018-08-23' && date('Y-m-d H:i:s') <= '2018-09-15 23:59:59') {
                $discount_percentage = 30;
            }

            $data = [
                'order' => $order,
                'user' => $user,
                'payment_methods' => $payment_methods,
                'post' => [],
                'banks' => $banks,
                'device_session_id' => md5(session_id() . microtime()),
                'credit_cards' => $credit_cards,
                'dates_cc' => get_expiration_credicard_dates(),
                'discount_percentage' => $discount_percentage
            ];

            return View::make('checkout_payu_retry', $data)->with('footer', $this->getFooter());
        }
    }

    /**
     * Procesar reintento de transaccion de pago con PSE
     */
    public function payu_retry_transaction_save()
    {
        $order = Order::find(Input::get('order_id'));
        $user = User::find($this->user_id);
        $payment_methods = $this->config['payment_methods'];
        $banks = PseBank::where('pseCode', '!=', '0')->orderBy('description')->get();
        $credit_cards = UserCreditCard::where('user_id', $this->user_id)->get()->toArray();

        $data = [
            'order' => $order,
            'user' => $user,
            'payment_methods' => $payment_methods,
            'post' => [],
            'banks' => $banks,
            'device_session_id' => md5(session_id() . microtime()),
            'credit_cards' => $credit_cards,
            'dates_cc' => get_expiration_credicard_dates(),
        ];

        try {
            DB::beginTransaction();

            if (Input::has('retry_payment_method')) {
                $order->payment_method = Input::get('retry_payment_method');
                $post_data = Input::get();
                $post_data['user_id'] = $user->id;
                switch (Input::get('retry_payment_method')) {
                    case 'Tarjeta de crédito':
                        $user_credit_card =  UserCreditCard::registerCreditCard($post_data, $order, $this->config);
                        $order->credit_card_id = $user_credit_card->id;
                        $order->cc_token = $user_credit_card->card_token;
                        $order->cc_holder_name = $user_credit_card->holder_name;
                        $order->cc_holder_document_type = $user_credit_card->holder_document_type;
                        $order->cc_holder_document_number = $user_credit_card->holder_document_number;
                        $order->cc_holder_phone = $user_credit_card->holder_phone;
                        $order->cc_last_four = $user_credit_card->last_four;
                        $order->cc_type = $user_credit_card->type;
                        $order->cc_country = $user_credit_card->country;
                        $order->cc_installments = $post_data['installments_cc'];
                        break;

                    case 'Débito - PSE':
                        $response = $order->chargePSE($order->id, $user->id, $post_data);

                        $order->save();
                        DB::commit();

                        return Redirect::to($response['response']);
                        break;

                    default:
                        $order->status = OrderStatus::INITIATED;
                        $order->order_validation_reason_id = null;
                        break;
                }
            }

            $message = 'Se actualizó el metódo de pago a ' . Input::get('retry_payment_method') . ' con éxito.';
            $order->save();
            DB::commit();

            Session::put('checkout_success', $order->group_id);
            $data['success'] = true;

            return Redirect::route('frontCheckout.checkout_success', ['order_reference' => $order->reference])
                ->with('message', $message)
                ->with('footer', $this->getFooter());
        } catch (Exception $e) {

            DB::rollback();

            return View::make('checkout_payu_retry', $data)
                ->with('footer', $this->getFooter())
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Enviar a la vista de checkout_failed cuando la transacción no fue aprobada por Pse.
     * @return mixed
     */
    public function payu_retry_transaction_failed()
    {
        if (Input::get('order_id')) {
            $order = Order::find(Input::get('order_id'));

            $order->status = 'Cancelled';
            $order->management_date = date('Y-m-d H:i:s');
            $order->reject_reason_id = $this->returnIdRejectReason('Cancelado', 'Transferencia rechazada', 'payu');
            $order->save();

            $order_group = OrderGroup::find($order->group_id);
            Event::fire('order.managed', [[$order, $order_group]]);
            $address = UserAddress::select('user_address.address_further', 'user_address.neighborhood')->where('user_address.id', $order_group->address_id)->first();

            $order_data = array();
            $order_data['order_id'] = $order->id;
            $order_data['reference'] = $order->reference;
            $order_data['total'] = $order_group->total_amount + $order_group->delivery_amount - $order_group->discount_amount;
            $order_data['payment_method'] = $order->payment_method;
            $order_data['address'] = $order_group->user_address . ' ' . $address->address_further . ' ' . $address->neighborhood;
            $order_data['products'] = $order_group->products_quantity;

            return View::make('checkout_failed')->with('order_data', $order_data);
        }
    }

    /**
     * Recibe datos de estado de transacción por PSE.
     */
    public function payu_notification()
    {
        $transactionStatus = Input::get('response_message_pol');
        $paymentStatusesMap = [
            'APPROVED' => 'Aprobada',
            'FAIL' => 'Fallida',
            'REJECT' => 'Declinada',
            'PENDING' => 'Pendiente',
        ];
        $paymentStatus = array_get($paymentStatusesMap, $transactionStatus, 'Fallida');

        // obtenemos la última orden pagada con pse del usuario
        $order = Order::where(['reference' => Input::get('reference_sale'), 'payment_method' => 'Débito - PSE'])
            ->first(['id', 'user_id', 'reference', 'total_amount', 'delivery_amount', 'discount_amount', 'status']);

        if (!$order) {
            \Log::error('PayU callback error, la orden no fue contrada!!', Input::get());
            return Response::json(["error" => "Orden no encontrada"], 404);
        }

        $payU = new PayU();
        $safeOrderSignature = $payU->generateTransactionSignature(
            $order->reference,
            $order->total_amount + $order->delivery_amount - $order->discount_amount,
            'COP',
            Input::get('state_pol')
        );

        $signatureIsOk = $safeOrderSignature === Input::get('sign');

        $responseCode = 400;
        $responseContent = ['bad signature'];

        if ($signatureIsOk) {
            $responseCode = 200;
            $responseContent = [];
            $logSuffix = "Estado de pedido = {$order->status}, no se actualiza estado";

            $order->orderPayments()
                ->where(['cc_payment_transaction_id' => Input::get('transaction_id')])
                ->update([
                    'cc_payment_status' => $paymentStatus,
                    'cc_payment_description' => Input::get('response_message_pol'),
                    'bank' => Input::get('pse_bank')
                ]);

            if ($transactionStatus === 'APPROVED' && $order->status === OrderStatus::VALIDATION) {
                $logSuffix = "Estado de pedido = {$order->status}, se actualiza a ".OrderStatus::INITIATED;
                $order->payment_date = date('Y-m-d H:i:s');
                $order->status = OrderStatus::INITIATED;
                $order->order_validation_reason_id = null;
                $order->save();
            }

            OrderLog::create([
                'type'     => "Estado de transacción de PSE actualizada a: {$transactionStatus}. {$logSuffix}",
                'order_id' => $order->id,
            ]);
        }

        OrderPaymentLog::insert([[
            'order_id' => $order->id,
            'user_id' => $order->user->id,
            'method' => __FUNCTION__,
            'url' => route('PayU.notification'),
            'response_http_code' => $responseCode,
            'request' => json_encode(Input::get()),
            'response' => json_encode($responseContent),
            'ip' => get_ip(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]]);

        return Response::json($responseContent, $responseCode);
    }

    /**
     * Obtiene los datos de facturación
     * de la primera orden en que haya diligenciado los datos.
     *
     * @param $userID
     * @return mixed
     */
    private function getInvoiceData($userID)
    {
        $invoice = Order::select('user_identity_type', 'user_identity_number', 'user_business_name')
            ->where('user_id', $userID)
            ->whereNotNull('user_identity_number')
            ->first();

        if ($invoice) {
            $invoice->user_identity_number = get_only_nit_number($invoice->user_identity_number);
        }

        return $invoice;
    }

    public function returnIdRejectReason($status, $reason, $attendant)
    {
        $rejectReason = \RejectReason::where('status', $status)
            ->where('reason', $reason)
            ->where('attendant', $attendant)
            ->pluck('id');
        return $rejectReason;
    }
}
