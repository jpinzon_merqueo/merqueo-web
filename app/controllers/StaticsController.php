<?php
use usecases\StaticsUseCase;

class StaticsController extends BaseController
{
    protected $staticsUseCase;

    public function __construct(StaticsUseCase $staticsUseCase)
    {
        $this->staticsUseCase = $staticsUseCase;
    }

    public function index($slug = null)
    {
        if(empty($slug)){
            $slug = Route::current()->getName();
        }

        $document = $this->staticsUseCase->getOne($slug);
        return View::make('statics', ['document' => $document, 'robots' => '']);
    }
}