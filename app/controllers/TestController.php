<?php

use Carbon\Carbon;
use usecases\contracts\credits\AddCreditToUserUsecaseInterface;

class TestController extends BaseController
{

    /**
     * @var \usecases\orders\AddCreditWhenDeliveryLate
     */
    private $addCreditWhenDeliveryLate;
    /**
     * @var AddCreditToUserUsecaseInterface
     */
    private $addCreditToUserUsecase;

    public function __construct(usecases\orders\AddCreditWhenDeliveryLate $addCreditWhenDeliveryLate, AddCreditToUserUsecaseInterface $addCreditToUserUsecase)
    {

        $this->addCreditWhenDeliveryLate = $addCreditWhenDeliveryLate;
        $this->addCreditToUserUsecase = $addCreditToUserUsecase;
    }

    public function tester()
    {
        /*$order = Order::find(830899);
        $data =  array(
            'type' => $order->status,
            'message' => 'Aprovecha tu quincena con los Super descuentos 🤩 en productos para MASCOTAS 10%🐶, FRUVER 15%🍍, POLLO, CARNE Y PESCADO 10%🐟. ¡Pide ahora en Merqueo!🐽',
            'order_id' => $order->id
        );
        $device_id = !empty($order->orderGroup->device_player_id) ? $order->orderGroup->device_player_id : $order->user_device_id;
        $r = send_notification($order->orderGroup->app_version, $order->orderGroup->source_os, $device_id, $order->orderGroup->device_player_id, $data);
        debug($r, true);*/
        //debug(User::getDiscounts(220659));
        /*$order = Order::find(632855);
        $order->fraudValidation();
        exit;*/

        /*$store_product = StoreProduct::find(250);
        debug($store_product->getOnRouteStock(1));*/

        /*Event::subscribe(new OrderEventHandler);
        $order = Order::find(348886);
        $order_group = OrderGroup::find($order->group_id);
        Event::fire('order.onmyway', [[$order, $order_group]]);
        exit;*/
        //Order::getStatusFlow(456335, 243192, true); exit;
        /*$order = Order::join('order_groups', 'group_id', '=', 'order_groups.id')->where('orders.id', 311341)->first();
        $r = $order->getEstimatedArrivalTime();
        debug($r);*/
        /*$picker = Picker::find(471);
        $orders = PickingGroup::getNextOrder($picker, 2);
        debug($orders);*/
        //debug(Store::getDeliverySlotCheckout(64));
        /*$order = Order::find(171161);
        $order->updateTotals();
        $order->save();*/
        //debug(User::getDiscounts(43));
        //debug(Store::getDeliverySlot(36));
        exit;
        $request = Request::create('/api/location', 'GET', array('address' => 'Carrera 32 # 13-56', 'city' => 'soacha'));
        Request::replace($request->input());
        $result = json_decode(Route::dispatch($request)->getContent(), true);
        debug($result);
        exit;
        /*$timestamp = strtotime(date('Y-m-d H:i:s'));
        $token = $timestamp.'|/api/1.2/checkout|'.Config::get('app.api.key');
        $aes = new Crypt_AES(CRYPT_MODE_CBC);
        $aes->setKey(Config::get('app.api.public_key'));
        $aes->setIV(Config::get('app.api.iv'));
        $token = $aes->encrypt($token);
        //echo $token; exit;
        $token = base64_encode($token);
        echo $token.'<br>';

        //echo $token; exit;
        $aes = new Crypt_AES(CRYPT_MODE_CBC);
        $aes->setKey(Config::get('app.api.public_key'));
        $aes->setIV(Config::get('app.api.iv'));
        //$aes->disablePadding();
        $token = base64_decode($token);
        $token = $aes->decrypt($token);
        var_dump($token).'<br>'; exit*/

        /*$password = 'oOYoCHQgFzQfyZ8BFf6SLusezz3gA23q78BPU\/RIxCnL05LdXFl5HUk3Q3ai1wEGqRD84cfrDC5Xcfp+JfQMZCSZgeUDDav4H6bdrEi1sar86brIjKUyLCMySRq+P6Au7hY4bnWdmUqTQurEDQqwNjD1PO9oLLyMo95sWbZvPhI=';
        $password = base64_decode($password);
            $rsa = new Crypt_RSA();
            $rsa->loadKey(Config::get('app.private_key'));
            $password = $rsa->decrypt($password);
            echo $password = base64_decode($password);
            exit;*/
        /*if (!Input::has('android') && !Input::has('ios'))
        {
            $rsa = new Crypt_RSA();
            $rsa->loadKey(Config::get('app.public_key'));
            $password = $rsa->encrypt(base64_encode('1234'));
            echo $data = base64_encode($password);

            echo '<br><br>';

            $data = base64_decode($data);
            $rsa = new Crypt_RSA();
            $rsa->loadKey(Config::get('app.private_key'));
            $password = $rsa->decrypt($data);
            echo base64_decode($password);
        }*/

        if (Input::has('android')) {
            $user_device_id = 'faff222c-2e9c-41dc-b2ee-283b3fbeca3d';
            $data = array(
                'type' => 'Store',
                /*'product_id' => 5932,
                'department_id' => 33,
                'shelf_id' => 71,*/
                'store_id' => 36,
                'message' => 'Tienda'
            );
            $data = array(
                'type' => 'Delivered',
                'message' => '¡Cuéntanos cómo te fue con el pedido que acabas de recibir!',
                'order_id' => 2231
            );

            $data =  array(
                'type' => 'In Progress',
                'message' => 'Tu pedido en Surtifruver esta siendo procesado y será entregado en 60 minutos aproximadamente.',
                'order_id' => 39151
            );
        }

        if (Input::has('ios')) {
            $user_device_id = 38975;
            $data = array(
                'type' => 'Store',
                /*'product_id' => 5932,
                'department_id' => 33,
                'shelf_id' => 71,*/
                'store_id' => 36,
                'message' => 'Tienda'
            );
            /*$data =  array(
                'type' => 'Delivered',
                'message' => '¿Como te fue con tu pedido?',
                'order_id' => 39146

            );*/
            /*$data =  array(
                'type' => 'In Progress',
                'message' => 'Tu pedido en Surtifruver esta siendo procesado y será entregado en 60 minutos aproximadamente.',
                'order_id' => 39146
            );*/
        }
        $result = send_push_notification($user_device_id, $data);
        debug($result);
        exit;
    }

    public function estimated_arrival_date()
    {
        $stores = DB::table('stores')
            ->select('city_id', 'latitude', 'longitude')
            ->where('status', 1)
            ->get();

        foreach ($stores as $store) {
            $this->config['merqueo_location'][$store->city_id] = [
                'lat' => $store->latitude,
                'lng' => $store->longitude
            ];
        }

        //calcular fecha de entrega estimada
        $current_time = date('Y-m-d 08:30:00');
        $google_api_key = Config::get('app.planning_google_api_key');
        $seconds_to_add_per_order = Config::get('app.transporters.seconds_to_add_per_order');
        $cont = 0;
        $last_order_estimated_arrival_date = false;
        $delivery_windows = Order::select('city_id', 'route_id', 'routes.planning_date', 'delivery_time', 'delivery_date')
            ->join('routes', 'routes.id', '=', 'route_id')
            ->where('route_id', 49830)
            //->where('orders.delivery_date', '>', '2018-08-29 13:00:00')
            ->groupBy('delivery_time')
            ->orderBy('delivery_date')
            ->get();

        $qty_delivery_windows = count($delivery_windows);
        foreach ($delivery_windows as $delivery_window) {
            //$delivery_window->planning_date = '2017-11-15';
            $route_orders = [];
            $cont++;
            $orders = Order::select('orders.*', 'user_address_latitude', 'user_address_longitude')
                ->join('order_groups', 'order_groups.id', '=', 'group_id')
                ->where('route_id', $delivery_window->route_id)
                //->where('orders.delivery_date', '>', '2018-08-29 13:00:00')
                ->where('delivery_time', $delivery_window->delivery_time)
                ->orderBy('planning_sequence')
                ->get();
            $waypoints = '';
            foreach ($orders as $order) {
                $route_orders[] = $order;
                $waypoints .= $order->user_address_latitude . ',' . $order->user_address_longitude . '|';
            }

            //setear origen
            if (isset($last_delivery_window_order_location))
                $origins = $last_delivery_window_order_location; //ultimo pedido de la franja anterior
            else $origins = $this->config['merqueo_location'][$delivery_window->city_id]['lat'] . ', ' . $this->config['merqueo_location'][$delivery_window->city_id]['lng']; //merqueo

            //si la franja cambia
            $delivery_time = explode(' - ', $delivery_window->delivery_time);
            if ($last_order_estimated_arrival_date) {
                //si la hora de entrega del ultimo pedido es mayor a la hora de inicio de la franja tomar esa
                if ($last_order_estimated_arrival_date > date('Y-m-d H:i:s', strtotime($delivery_window->planning_date . ' ' . $delivery_time[0] . ' -20 minutes')))
                    $delivery_window_departure_hour = date('H:i:s', strtotime($last_order_estimated_arrival_date));
                else {
                    //si la hora de entrega del ultimo pedido es menor a la hora de inicio de la franja, tomar el inicio de la nueva franja menos 20 min
                    $delivery_window_departure_hour = date('H:i:s', strtotime($delivery_window->planning_date . ' ' . $delivery_time[0] . ' -20 minutes'));
                    $last_order_estimated_arrival_date = $delivery_window->planning_date . ' ' . $delivery_window_departure_hour;
                }
            } else $delivery_window_departure_hour = date('H:i:s', strtotime($current_time));

            if ($cont != $qty_delivery_windows) { //si no es la ultima franja
                $order = end($route_orders);
                $destination = $order->user_address_latitude . ',' . $order->user_address_longitude;
            } else {
                //ultima franja
                $destination = $this->config['merqueo_location'][$delivery_window->city_id]['lat'] . ', ' . $this->config['merqueo_location'][$delivery_window->city_id]['lng'];
            }

            $params = http_build_query([
                'origin' => $origins,
                'destination' => $destination,
                'waypoints' => 'optimize:false|' . trim($waypoints, '|'),
                //'departure_time' => strtotime($delivery_window->planning_date.' '.$delivery_window_departure_hour),
                'key' => $google_api_key,
                'mode' => 'driving',
                'language' => 'es-Es'
            ]);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/directions/json?' . $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $json_response = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $response = json_decode($json_response, true);
            //debug($response);
            if (isset($response['routes'][0]['waypoint_order'])) {
                foreach ($response['routes'][0]['waypoint_order'] as $key => $value) {
                    $route_orders[$value]->planning_distance = $response['routes'][0]['legs'][$key]['distance']['value'];
                    $route_orders[$value]->planning_duration = $response['routes'][0]['legs'][$key]['duration']['value'] + $seconds_to_add_per_order;

                    //calcular fecha de entrega estimada
                    $minutes = round($route_orders[$value]->planning_duration / 60);
                    if (!$last_order_estimated_arrival_date)
                        $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime($delivery_window->planning_date . ' ' . $delivery_window_departure_hour . ' +' . $minutes . ' minutes'));
                    $route_orders[$value]->estimated_arrival_date = $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime($last_order_estimated_arrival_date . ' +' . $minutes . ' minutes'));
                    $last_delivery_window_order_location = $route_orders[$value]->user_address_latitude . ',' . $route_orders[$value]->user_address_longitude;
                }

                if (count($route_orders)) {
                    foreach ($route_orders as $order) {
                        echo $order->id . ' | ' . $order->planning_sequence . ' | ' . $order->estimated_arrival_date . '<br>';
                    }
                }
            }
        }
    }

    public function import_txt()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        if (isset($_FILES["archivo"])) {
            $cont = 0;
            $archivo = $_FILES["archivo"];
            if (!file_exists($archivo["tmp_name"])) {
                echo "EL ARCHIVO NO EXISTE.";
                exit();
            }

            $texto = fopen($archivo["tmp_name"], "r");
            //fgets($texto, 4096);
            while (!feof($texto)) {
                $linea = trim(fgets($texto, 4096));

                $datos = explode(';', $linea);

                /*DB::statement('UPDATE orders SET user_score = '.$datos[1].' WHERE id = '.$datos[0]);*/

                /*if ($user = User::find(trim($datos[0]))) {
                    UserCredit::addCredit($user, trim($datos[1]), '2019-01-28 23:59:59', 'marketing');
                    $cont++;
                }*/

                /*//GENERAR ARCHIVO PLANO
                $datos = explode(',', $linea);
                $user = User::select('users.id', 'identity_number', 'holder_document_number')
                    ->leftJoin('user_credit_cards', function($join){
                        $join->on('user_id', '=', 'users.id');
                        $join->on('holder_document_number', '<>', DB::raw("''"));
                    })
                    ->where('customer_token', trim($datos[1]))
                    ->first();
                if ($user) {
                    if (empty($user->holder_document_number))
                        if (!empty($user->identity_number))
                            $user->holder_document_number = $user->identity_number;
                        else $user->holder_document_number = '';
                    echo $user->id.','.trim($datos[0]).','.trim($datos[1]).','.trim($datos[2]).','.trim($datos[3]).','.trim($datos[4]).','.trim($datos[5]).','.$user->holder_document_number.'<br>';
                }
                continue;*/

                //ASOCIAR TARJETAS
                /*$datos = explode(';', $linea);
                if (isset($datos[1])) {
                    $user_credit_card = UserCreditCard::where('user_id', trim($datos[0]))->where('card_token', trim($datos[1]))->first();
                    if ($user_credit_card) {
                        $user_credit_card->card_token_old = $user_credit_card->card_token;
                        $user_credit_card->card_token = trim($datos[2]);
                        $user_credit_card->save();
                        $cont++;
                    }else echo $linea . '<br>';
                }else echo $linea . '<br>';*/

                $cont++;

            }
            fclose($texto);
            echo $cont;
        } else { ?>
            <h4>Leer TXT</h4>
            <form name="form" enctype="multipart/form-data" action="" method="POST">
                <b>Importar archivo:</b>
                <input type="file" name="archivo" size="50">
                <input type="submit" name="btnImportar" class="btn" value="Importar"
                       onClick="if (archivo.value==''){
         alert('Hay datos obligatorios vacios.');
         return false;
         }else{
            if (confirm('Esta seguro que desea ejecutar?')) return true;
            else return false;
        }"/></form><?php }

    }

    public function coupons_creator()
    {
        $digit_type = '';
        $result = '';
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        $main_string = '10k';
        $coup_cant = 117250;
        $digit_length = 7;
        for ($i = 0; $i < $coup_cant; $i++) {
            if ($digit_type == 'string') {
                $string = \Illuminate\Support\Str::quickRandom($digit_length);
            } elseif ($digit_type == 'number') {
                $number = rand(1000000, 9999999);
            }
            $code = $main_string . $result;

            $coupon_obj = Coupon::where('code', $code)->first();

            if (empty($coupon_obj)) {
                $coupon_obj = new Coupon;
                $coupon_obj->type = 'Credit Amount';
                $coupon_obj->code = $code;
                $coupon_obj->amount = 10000;
                $coupon_obj->number_uses = 1;
                $coupon_obj->type_use = 'Normal';
                $coupon_obj->minimum_order_amount = 30000;
                $coupon_obj->maximum_order_amount = 0;
                $coupon_obj->campaign_validation = 'BeneficioDomicilios.com';
                // $coupon_obj->payment_method = 0;
                // $coupon_obj->cc_bin = 0;
                // $coupon_obj->cc_bank = 0;
                $coupon_obj->redeem_on = 'Total Cart';
                // $coupon_obj->store_id = 64;
                // $coupon_obj->department_id = 76;
                // $coupon_obj->shelf_id = 301;
                // $coupon_obj->store_product_id = 3238;
                // $coupon_obj->shopper_id = 0;
                $coupon_obj->expiration_date = '2018-05-31';
                $coupon_obj->visible = 1;
                $coupon_obj->status = 1;
                $coupon_obj->save();
            } else {
                $i--;
            }
        }
        echo '<pre>';
        var_dump('$result');
        echo '</pre>';
        exit;
    }

    public function payu()
    {
        $payu = new PayU;

        /*$result = $payu->retriveCreditCard('7b3ab26f-891b-4d30-bc50-75a0a2d2f90a', 43);
        debug($result);

        $data = array(
            'user_id' => 7044,
            //'number_cc' => '5907120000000009',
            'number_cc' => '4111111111111111',
            'name_cc' => 'APPROVED',
            'expiration_year_cc' => 2023,
            'expiration_month_cc' => 10,
            'card_type' => 'VISA'
        );
        $result = $payu->associateCreditCard($data);
        debug($result);*/

        $data = array(
            'order_id' => 56498754,
            'description' => 'Compra en Merqueo',
            'total_amount' => 120000,
            'user_id' => 7044,
            'referenceCode' => generate_reference(),
            'buyer' => [
                'document_number' => '725888585',
                'document_type' => 'CC',
                'fullname' => 'APPROVED',
                'email' => 'test@merqueo.com',
                'phone' => '3002552020',
                'address' => 'Calle 94 #11-32',
                'address_further' => 'Ofi 503',
                'city' => 'Bogota',
                'state' => 'Bogota',
                'city_code' => '10001100',
            ],
            'payer' => [
                'document_number' => '725888585',
                'document_type' => 'CC',
                'fullname' => 'APPROVED',
                'email' => 'test@merqueo.com',
                'phone' => '3002552020',
                'address' => 'Calle 94 #11-32',
                'address_further' => 'Ofi 503',
                'city' => 'Bogota',
                'state' => 'Bogota',
                'city_code' => '10001100',
            ],
            'card_token' => 'f2e49feb-9ae0-4e51-9d7e-c8d70dfdbe84',
            'card_type' => 'CODENSA',
            'installments' => 1,
            'device_session_id' => 'el mismo q se carga en el script sin el 80200',
        );
        $result = $payu->createCreditCardCharge($data);
        debug($result);
    }

    public function import_orders()
    {
        if (Request::isMethod('get')) {
            return View::make('admin.test.import_order_form');
        } else {
            /**
             * 0 => ID usuario, 1 => ID direccion, 2 => ID producto, 3 => cantidad
             */
            $post_data = [
                'delivery_day' => '2017-07-21',
                'delivery_time' => '7:00 AM - 12:00 PM',
                'payment_method' => 'Efectivo'
            ];
            $file = Input::file('orders_file');
            $cont = 0;
            $image_errors = [];
            $row_number = 1;
            $text = fopen($file->getRealPath(), 'r');
            fgets($text, 4096);
            $user_id = null;
            while (!feof($text)) {
                $refresh_cache = false;
                $row_number++;

                $row = trim(fgets($text, 4096));
                $data = explode(';', $row);
                //usuario
                if ($user_id != $data[0]) {
                    $user_id = $data[0];
                    if (!$user = User::find((int)$data[0]))
                        return Redirect::back()->with(['status' => false, 'message' => 'El usuario no existe. ' . $data[0]]);
                    if (!$address = UserAddress::find((int)$data[1]))
                        return Redirect::back()->with(['status' => false, 'message' => 'La dirección del usuario no existe. ' . $data[3]]);
                }
                //productos
                if ($user_id == $data[0]) {
                    $product = null;
                    if (empty($data[2]))
                        return Redirect::back()->with(['status' => false, 'message' => 'El producto no tiene el ID.']);
                    else {
                        if ($store_product = StoreProduct::find($data[2]))
                            return Redirect::back()->with(['status' => false, 'message' => 'El producto no existe ID ' . $data[2]]);
                        if (empty($data[3]))
                            return Redirect::back()->with(['status' => false, 'message' => 'El producto no tiene cantidad.']);

                        $order_products[] = [
                            'id' => $store_product->id,
                            'qty' => $data[3],
                        ];
                        $store_id = $store_product->store_id;
                    }
                }
            }

            if ($user_id == $data[0]) {
                if (isset($user) && isset($order_products)) {
                    //obtener zona
                    $zone_id = null;
                    $zones = Zone::where('city_id', $address->city_id)->where('status', 1)->get();
                    if ($zones) {
                        foreach ($zones as $zone) {
                            if (!empty($zone->stores)) {
                                $zone_store_ids = explode(',', $zone->stores);
                                if (in_array($store_id, $zone_store_ids) && point_in_polygon($zone->delivery_zone, $address->latitude, $address->longitude)) {
                                    $zone_id = $zone->id;
                                    break;
                                }
                            }
                        }
                    }

                    $order_group = new OrderGroup;
                    $order_group->source = 'Callcenter';
                    $order_group->user_id = $user->id;
                    $order_group->user_firstname = $user->first_name;
                    $order_group->user_lastname = $user->last_name;
                    $order_group->user_email = $user->email;
                    $order_group->user_address = $address->address;
                    $order_group->user_address_further = $address->address_further;
                    $order_group->user_address_neighborhood = $address->neighborhood;
                    $order_group->user_address_latitude = $address->latitude;
                    $order_group->user_address_longitude = $address->longitude;
                    $order_group->user_city_id = $address->city_id;
                    $order_group->user_phone = $user->phone;
                    $order_group->address_id = $address->id;
                    $order_group->zone_id = $zone_id;
                    $order_group->discount_amount = 0;
                    $order_group->delivery_amount = 0;
                    $order_group->total_amount = 0;
                    $order_group->products_quantity = count($order_products);
                    $order_group->user_comments = '';
                    $order_group->ip = get_ip();
                    $order_group->save();

                    $delivery_time = explode(' - ', $post_data['delivery_time']);
                    $delivery_time_begin = explode(':', $delivery_time[0]);
                    $hour_begin = new DateTime();
                    $hour_begin->setTime($delivery_time_begin[0], $delivery_time_begin[1]);

                    $delivery_time_end = explode(':', $delivery_time[1]);
                    $hour_end = new DateTime();
                    $hour_end->setTime($delivery_time_end[0], $delivery_time_end[1]);

                    $delivery_time_text = $hour_begin->format('g:i a') . ' - ' . $hour_end->format('g:i a');
                    $hour_begin = $post_data['delivery_day'] . ' ' . $delivery_time[0];
                    if ($hour_end->format('G') < 3)
                        $post_data['delivery_day'] = date('Y-m-d', strtotime($post_data['delivery_day'] . '+1 day'));

                    $hour_end = $real_delivery_date = $post_data['delivery_day'] . ' ' . $delivery_time[1];
                    $delivery_time = get_delivery_time($hour_begin, $hour_end);

                    $order = new Order;
                    $order->reference = generate_reference();
                    $order->user_id = $user_id;
                    $order->store_id = $store_id;
                    $order->date = date("Y-m-d H:i:s");
                    $order->group_id = $order_group->id;
                    $order->total_products = count($order_products);
                    $order->delivery_amount = 0;
                    $order->discount_amount = 0;
                    $order->total_amount = 0;
                    $order->status = 'Initiated';
                    $order->user_score_token = generate_token();
                    $order->first_delivery_date = $post_data['delivery_day'] . ' ' . $delivery_time;
                    $order->delivery_date = $post_data['delivery_day'] . ' ' . $delivery_time;
                    $order->real_delivery_date = $real_delivery_date;
                    $order->delivery_time = $delivery_time_text;
                    $order->payment_method = $post_data['payment_method'];
                    $order->posible_fraud = 0;

                    $order->save();

                    //log de pedido
                    $log = new OrderLog();
                    $log->type = 'Pedido creado.';
                    $log->user_id = $user->id;
                    $log->order_id = $order->id;
                    $log->save();

                    $order_total_amount = 0;

                    foreach ($order_products as $order_product) {
                        $store_product = StoreProduct::select('products.*', 'store_products.*')->join('products', 'store_products.product_id', '=', 'products.id')
                            ->where('store_products.id', $order_product->id)->first();
                        if ($store_product->quantity) {
                            if ($store_product->special_price > -1) {
                                if ($store_product->first_order_special_price)
                                    $price = $store_product->price;
                                else $price = $store_product->special_price;
                            } else $price = $store_product->price;

                            $order_product = OrderProduct::saveProduct($price, $order_product->quantity, $store_product, $order);
                            $order_total_amount += $order_product->price * $order_product->quantity;
                        }
                    }

                    $order_group->total_amount = $order_total_amount;
                    $order_group->save();
                    $order->total_amount = $order_total_amount;
                    $order->save();

                    $user_id = null;
                    unset($user);
                    unset($address);
                    unset($order_products);
                    $cont++;
                }
            }

            echo "Pedidos creados: $cont";
        }
    }

    public function update_total($id)
    {
        $order = Order::find($id);
        $order->updateTotals();
        $order->save();
    }

    /**
     * Funcion para generar csv con los ID y las URL de cada producto
     *
     * @return mixed
     */
    public function get_url_to_csv()
    {
        $products = Product::select('products.*', 'departments.slug AS department_slug', 'shelves.slug AS shelf_slug', 'stores.slug AS store_slug', 'products.slug AS product_slug')
            ->join('departments', 'department_id', '=', 'departments.id')
            ->join('shelves', 'shelf_id', '=', 'shelves.id')
            ->join('stores', 'stores.id', '=', 'products.store_id')
            ->where('products.status', 1)
            ->whereRaw('IF( products.is_visible_stock = 1, IF( products.current_stock > 0, TRUE, FALSE), TRUE )')->get();

        /*$queries = DB::getQueryLog();
            $last_query = end($queries);
            debug($last_query);*/

        if (!count($products))
            return Response::json(['status' => false]);

        $objPHPExcel = new PHPExcel();
        //propiedades
        $objPHPExcel->setActiveSheetIndex(0);

        //titulos
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, 'title');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 1, 'url');

        $j = 2;
        foreach ($products as $product) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $j, $product->id);
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $j, "https://merqueo.com/bogota/domicilios-{$product->store_slug}/{$product->department_slug}/{$product->shelf_slug}/{$product->slug}");
            $j++;
        }

        //crear archivo
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $filename = date('Y-m-d H.i.s', time()) . '.csv';
        $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
        $file['real_path'] = public_path(Config::get('app.download_directory')) . $filename;
        $file['client_original_name'] = $filename;
        $file['client_original_extension'] = 'csv';
        $url = upload_image($file, false, Config::get('app.download_directory_temp'));

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $filename . '.csv"',
        );
        return Response::json(['status' => true, 'url' => $url], 200, $headers);
    }

    public function create_routes()
    {
        set_time_limit(0);
        $cont_global = 0;
        try {

            DB::beginTransaction();

            $routes = Order::select('orders.delivery_time', 'stores.city_id', DB::raw('DATE(orders.delivery_date) AS delivery_date'),
                'zone_id', 'city', 'zones.name AS zone', 'planning_route')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('stores', 'stores.id', '=', 'orders.store_id')
                ->join('zones', 'zones.id', '=', 'order_groups.zone_id')
                ->join('cities', 'cities.id', '=', 'stores.city_id')
                ->where(DB::raw('DATE(orders.delivery_date)'), date('Y-m-d'))
                ->whereNotNull('orders.planning_sequence')
                ->groupBy('planning_route')
                ->orderBy('zone_id')
                ->get();

            foreach ($routes as $route_data) {
                $shift = strstr($route_data->planning_route, 'AM') ? 'AM' : 'PM';

                //validar si ya existe una ruta en la misma zona
                $cont = Routes::where('city_id', $route_data->city_id)->where('planning_date', $route_data->delivery_date)->where('shift', $shift)->where('zone_id', $route_data->zone_id)->count();
                $cont++;
                $route = new Routes;
                $route->status = 'Terminada';
                $route->admin_id = Session::get('admin_id');
                $route->city_id = $route_data->city_id;
                $route->zone_id = $route_data->zone_id;
                $route->planning_date = $route_data->delivery_date;
                $route->shift = $shift;
                $route->route = $route_data->planning_route;
                $route->save();
                $cont_global++;

                $orders = Order::select('orders.*')->where(DB::raw('DATE(orders.delivery_date)'), date('Y-m-d'))
                    ->where('orders.planning_route', $route_data->planning_route)
                    ->whereNotNull('orders.planning_sequence')
                    ->get();
                //echo $route_data->planning_route.' / '.count($orders).'<br>';
                foreach ($orders as $order) {
                    //guardar datos a pedido
                    $order->route_id = $route->id;
                    $order->planning_duration = 0;
                    $order->planning_distance = 0;
                    $order->save();
                }
            }

            DB::commit();

            echo $cont_global;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;

        }
    }

    public function fixCombo()
    {
        $orders = [251596];
        $store_product_ids = [63 => 4383];
        $store_product_group_org = [4383 => [119708]];

        foreach ($orders as $key => $order) {
            $order_obj = Order::find($order);
            $store_product_id = $store_product_ids[$order_obj->store_id];
            $store_product_groups = $store_product_group_org[$store_product_id];
            $order_products = OrderProduct::where('order_id', $order_obj->id)->where('store_product_id', $store_product_id)->get();
            foreach ($order_products as $order_product) {
                $order_product->type = 'Agrupado';
                $order_product->save();
                foreach ($store_product_groups as $store_product_group) {
                    $new_order_product_group = OrderProductGroup::find($store_product_group)->replicate();
                    $new_order_product_group->order_id = $order_obj->id;
                    $new_order_product_group->order_product_id = $order_product->id;
                    $new_order_product_group->fulfilment_status = 'Fullfilled';
                    $new_order_product_group->quantity = $order_product->quantity * 3;
                    $new_order_product_group->save();
                }
            }
        }
    }

    public function importProviderOrderReception()
    {
        if (Request::isMethod('POST')) {
            if (Input::hasFile('file')) {
                ini_set('memory_limit', '512M');
                set_time_limit(0);

                $city_id = Input::get('city_id');
                $invoice_number = Input::get('invoice_number');
                if ($city_id == 1) {
                    $store_id = 63;
                    $fuente = ProviderTax::find(1);
                    $ica = ProviderTax::find(2);
                } else {
                    $fuente = ProviderTax::find(1);
                    $ica = ProviderTax::find(3);
                    $store_id = 64;
                }
                $provider_id = 7;
                $admin_id = 12;
                $provider = Provider::find($provider_id);
                DB::beginTransaction();
                try {
                    $provider_order = new ProviderOrder;
                    $provider_order->city_id = $city_id;
                    $provider_order->provider_id = $provider->id;
                    $provider_order->admin_id = $admin_id;
                    $provider_order->provider_name = $provider->name;
                    $provider_order->status = 'Cerrada';
                    $provider_order->date = Carbon\Carbon::now();
                    $provider_order->delivery_date = Carbon\Carbon::now();
                    $provider_order->management_date = Carbon\Carbon::now();
                    $provider_order->comments = 'Orden creada con relacion a: ' . $invoice_number;
                    $provider_order->reject_comments = null;
                    $provider_order->save();

                    $reception = new ProviderOrderReception;
                    $reception->provider_order_id = $provider_order->id;
                    $reception->admin_id = $admin_id;
                    $reception->status = 'Contabilizado';
                    $reception->date = Carbon\Carbon::now();
                    $reception->received_date = Carbon\Carbon::now();
                    $reception->storage_date = Carbon\Carbon::now();
                    $reception->transporter = 'N.A';
                    $reception->plate = 'N.A';
                    $reception->driver_name = 'N.A';
                    $reception->driver_document_number = 'N.A';
                    $reception->invoice_number = $invoice_number;
                    $reception->observation = '';
                    $reception->expiration_date = Carbon\Carbon::now();
                    $reception->voucher_date = Carbon\Carbon::now();
                    $reception->rete_ica_account = $ica->account;
                    $reception->rete_ica_percentage = $ica->percentage;
                    $reception->rete_fuente_account = $fuente->account;
                    $reception->rete_fuente_percentage = $fuente->percentage;
                    $reception->save();


                    $extension_list = 'xlsx, xls';
                    $file = Input::file('file');
                    if (!file_exists($file->getRealPath()))
                        return Redirect::back()->with('error', 'No file uploaded');

                    $path = public_path(Config::get('app.download_directory'));

                    $extension_obj = explode(',', $extension_list);
                    foreach ($extension_obj as $extension) {
                        foreach (glob($path . '/*.' . $extension) as $file_tmp) {
                            $file_tmp = pathinfo($file_tmp);
                            $files_by_extension[$extension][] = $file_tmp['filename'];
                        }
                    }

                    //obtener archivo
                    $file_path = Input::file('file')->getRealPath();
                    $current_dir = opendir($path);

                    $objReader = new PHPExcel_Reader_Excel2007();
                    $objPHPExcel = $objReader->load($file_path);
                    //abrir la ultima hoja que contiene la informacion
                    $sheetCount = $objPHPExcel->getSheetCount();
                    $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
                    //obtener el numero total de filas y columnas
                    $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
                    $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());
                    $codigos = [];
                    for ($i = 2; $i <= $rows; $i++) {
                        $description_item = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
                        $cantidad_unitaria = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
                        $valor_unitario = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $i)->getValue();
                        $suma_valor = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $i)->getValue();
                        $valor_iva = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $i)->getValue();
                        $tarifa_iva = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $i)->getValue();
                        $valor_neto = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $i)->getValue();
                        $codigo = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $i)->getValue();
                        $product = Product::whereRaw("CONCAT( LPAD(products.accounting_line,3,'0'), LPAD(products.accounting_group, 4, '0'), LPAD(products.accounting_code, 6, '0') ) = '" . $codigo . "'")->join('store_products', 'products.id', '=', 'store_products.product_id')
                            ->where('store_products.store_id', $store_id)
                            // ->where('store_products.provider_id', $provider->id)
                            ->select(
                                'products.id AS product_id',
                                'products.type',
                                'store_products.id AS store_product_id',
                                'store_products.provider_plu',
                                'products.reference',
                                'products.name AS product_name',
                                'products.quantity',
                                'products.unit',
                                'store_products.ideal_stock',
                                'store_products.provider_pack_quantity',
                                'store_products.provider_pack_type',
                                'store_products.base_cost',
                                'store_products.cost',
                                'store_products.iva',
                                'products.image_medium_url'
                            )
                            ->first();

                        if (empty($product)) {
                            echo '<pre>';
                            var_dump($i);
                            echo '</pre>';
                            DB::rollBack();
                            exit;
                        }

                        $provider_order_detail = new ProviderOrderDetail;
                        $provider_order_detail->provider_order_id = $provider_order->id;
                        $provider_order_detail->store_product_id = $product->store_product_id;
                        $provider_order_detail->type = $product->type;
                        $provider_order_detail->plu = $product->provider_plu;
                        $provider_order_detail->reference = $product->reference;
                        $provider_order_detail->product_name = $product->product_name . ' ' . $product->quantity . ' ' . $product->unit;
                        $provider_order_detail->quantity_order = $cantidad_unitaria;
                        $provider_order_detail->ideal_stock = $product->ideal_stock;
                        $provider_order_detail->quantity_pack = $product->provider_pack_quantity;
                        $provider_order_detail->pack_description = $product->provider_pack_type != 'Unidad' ? '1 ' . $product->provider_pack_type . ' x ' . $product->provider_pack_quantity . ' Unidades' : '1 Unidad';
                        $provider_order_detail->iva = $tarifa_iva;
                        $provider_order_detail->cost = $valor_unitario;
                        $provider_order_detail->base_cost = ($valor_unitario / (1 + ($provider_order_detail->iva / 100)));
                        $provider_order_detail->iva_amount = $tarifa_iva;
                        $provider_order_detail->image_url = $product->image_medium_url;
                        $provider_order_detail->save();
                        // $codigos[] = $provider_order_detail->toArray();

                        $reception_detail = new ProviderOrderReceptionDetail;
                        $reception_detail->reception_id = $reception->id;
                        $reception_detail->product_id = 0;
                        $reception_detail->store_product_id = $product->store_product_id;
                        $reception_detail->status = 'Recibido';
                        $reception_detail->accounting_status = 'Contabilizado';
                        $reception_detail->quantity_received = $provider_order_detail->quantity_order;
                        $reception_detail->save();


                        /*$store_product = StoreProduct::find($product->store_product_id);
                        $store_product->current_stock += $reception_detail->quantity_received;
                        $store_product->save();*/

                        unset($reception_detail);
                        unset($provider_order_detail);
                        //guardar producto agrupado

                    }
                    /*echo '<pre>';
                    var_dump( $codigos );
                    echo '</pre>';
                    exit;*/
                    $reception->updateTaxes();

                } catch (Exception $e) {
                    DB::rollBack();
                    echo '<pre>';
                    var_dump($e->getMessage());
                    echo '</pre>';
                    echo '<pre>';
                    var_dump($e->getLine());
                    echo '</pre>';
                    echo '<pre>';
                    var_dump($codigo);
                    echo '</pre>';
                    exit;
                }

                // $this->admin_log('products', null, 'import_update_stock');
                DB::commit();
                echo '<pre>';
                var_dump($reception->toArray());
                echo '</pre>';
                echo '<pre>';
                var_dump($provider_order->toArray());
                echo '</pre>';
                exit;
                return Redirect::back()->with('success', 'Se actualizaron los stock de los productos importados.');
            }
        } else {
            echo '
                <form action="' . route('testController.importProviderOrderReception') . '" method="POST" enctype="multipart/form-data">
                    <input type="file" name="file">
                    <br>
                    <input type="text" name="city_id">
                    <br>
                    <input type="text" name="invoice_number">
                    <br>
                    <input type="submit" value="Enviar">
                </form>
            ';
        }
    }

    public function updateProviderOrderReception()
    {
        if (Request::isMethod('POST')) {
            if (Input::hasFile('file')) {
                ini_set('memory_limit', '512M');
                set_time_limit(0);

                $city_id = Input::get('city_id');
                $invoice_number = Input::get('invoice_number');
                if ($city_id == 1) {
                    $store_id = 63;
                    $fuente = ProviderTax::find(1);
                    $ica = ProviderTax::find(2);
                } else {
                    $fuente = ProviderTax::find(1);
                    $ica = ProviderTax::find(3);
                    $store_id = 64;
                }
                $provider_id = 7;
                $admin_id = 12;
                $provider = Provider::find($provider_id);
                DB::beginTransaction();
                try {
                    $reception = ProviderOrderReception::where('invoice_number', $invoice_number)->first();
                    $provider_order = ProviderOrder::find($reception->provider_order_id);

                    $extension_list = 'xlsx, xls';
                    $file = Input::file('file');
                    if (!file_exists($file->getRealPath()))
                        return Redirect::back()->with('error', 'No file uploaded');

                    $path = public_path(Config::get('app.download_directory'));

                    $extension_obj = explode(',', $extension_list);
                    foreach ($extension_obj as $extension) {
                        foreach (glob($path . '/*.' . $extension) as $file_tmp) {
                            $file_tmp = pathinfo($file_tmp);
                            $files_by_extension[$extension][] = $file_tmp['filename'];
                        }
                    }

                    //obtener archivo
                    $file_path = Input::file('file')->getRealPath();
                    $current_dir = opendir($path);

                    $objReader = new PHPExcel_Reader_Excel2007();
                    $objPHPExcel = $objReader->load($file_path);
                    //abrir la ultima hoja que contiene la informacion
                    $sheetCount = $objPHPExcel->getSheetCount();
                    $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
                    //obtener el numero total de filas y columnas
                    $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
                    $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());
                    for ($i = 2; $i <= $rows; $i++) {
                        $description_item = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
                        $cantidad_unitaria = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
                        $valor_unitario = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $i)->getValue();
                        $suma_valor = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $i)->getValue();
                        $valor_iva = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $i)->getValue();
                        $tarifa_iva = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $i)->getValue();
                        $valor_neto = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $i)->getValue();
                        $codigo = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $i)->getValue();
                        $product = Product::whereRaw("CONCAT( LPAD(products.accounting_line,3,'0'), LPAD(products.accounting_group, 4, '0'), LPAD(products.accounting_code, 6, '0') ) = '" . $codigo . "'")->join('store_products', 'products.id', '=', 'store_products.product_id')
                            ->where('store_products.store_id', $store_id)
                            ->select(
                                'products.id AS product_id',
                                'products.type',
                                'store_products.id AS store_product_id',
                                'store_products.provider_plu',
                                'products.reference',
                                'products.name AS product_name',
                                'products.quantity',
                                'products.unit',
                                'store_products.ideal_stock',
                                'store_products.provider_pack_quantity',
                                'store_products.provider_pack_type',
                                'store_products.base_cost',
                                'store_products.cost',
                                'store_products.iva',
                                'products.image_medium_url'
                            )
                            ->first();

                        if (empty($product)) {
                            DB::rollBack();
                            echo '<pre>';
                            var_dump($codigo);
                            echo '</pre>';
                            exit;
                        }

                        $provider_order_detail = ProviderOrderDetail::where('provider_order_id', $provider_order->id)
                            ->where('store_product_id', $product->store_product_id)
                            ->where('type', $product->type)
                            ->where('plu', $product->provider_plu)
                            ->where('reference', $product->reference)
                            // ->where('product_name', $product->product_name.' '.$product->quantity.' '.$product->unit)
                            ->where('quantity_order', $cantidad_unitaria)
                            // ->where('ideal_stock', $product->ideal_stock)
                            // ->where('quantity_pack', $product->provider_pack_quantity)
                            // ->where('pack_description', $product->provider_pack_type != 'Unidad' ? '1 '.$product->provider_pack_type.' x '.$product->provider_pack_quantity.' Unidades' : '1 Unidad')
                            // ->where('iva', $tarifa_iva)
                            ->first();


                        if (empty($tarifa_iva)) {
                            $tarifa_iva = 0;
                        }

                        if ($codigo == '0030015000201' || $codigo == '0030015000134') {
                            continue;
                        }

                        $provider_order_detail->iva = $tarifa_iva;
                        $provider_order_detail->cost = $valor_unitario;
                        $provider_order_detail->base_cost = ($valor_unitario / (1 + ($provider_order_detail->iva / 100)));
                        $provider_order_detail->iva_amount = $tarifa_iva;
                        $provider_order_detail->image_url = $product->image_medium_url;
                        $provider_order_detail->save();

                        //guardar producto agrupado
                        /*if ($product->type == 'Proveedor')
                        {
                            $products = StoreProductGroup::select('products.*', 'store_products.*', 'store_product_group_id', 'store_product_group.quantity AS group_quantity')
                                                    ->join('store_products', 'store_product_group.store_product_group_id', '=', 'store_products.id')
                                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                                    ->where('store_product_id', $product->id)
                                                    ->get();

                            foreach($products as $store_product_group)
                            {
                                $provider_order_detail_product_group = new ProviderOrderDetailProductGroup;
                                $provider_order_detail_product_group->provider_order_detail_id = $provider_order_detail->id;
                                $provider_order_detail_product_group->store_product_id = $store_product_group->store_product_group_id;
                                $provider_order_detail_product_group->plu = $store_product_group->provider_plu;
                                $provider_order_detail_product_group->product_name = $store_product_group->name.' '.$store_product_group->quantity.' '.$store_product_group->unit;
                                $provider_order_detail_product_group->product_quantity = $store_product_group->quantity;
                                $provider_order_detail_product_group->product_unit = $store_product_group->unit;
                                $provider_order_detail_product_group->quantity_order = $store_product_group->group_quantity;
                                $provider_order_detail_product_group->ideal_stock = $store_product_group->ideal_stock;
                                $provider_order_detail_product_group->base_cost = $store_product_group->base_cost;
                                $provider_order_detail_product_group->cost = $store_product_group->cost;
                                $provider_order_detail_product_group->iva = $store_product_group->iva;
                                $provider_order_detail_product_group->image_url = $store_product_group->image_medium_url;
                                $provider_order_detail_product_group->save();

                                $reception_detail_group = new ProviderOrderReceptionDetailProductGroup;
                                $reception_detail_group->reception_detail_id = $reception_detail;
                                $reception_detail_group->store_product_id = $store_product_group->store_product_group_id;
                                $reception_detail_group->status = 'Recibido';
                                $reception_detail_group->accounting_status = 'Contabilizado';
                                $reception_detail_group->quantity_received = $provider_order_detail_product_group->quantity_order;
                                $reception_detail_group->save();

                                $store_product = StoreProduct::find($store_product_group->store_product_group_id);
                                $store_product->current_stock += $provider_order_detail_product_group->quantity_order;
                                $store_product->save();
                            }
                        }*/
                    }
                    $reception->updateTaxes();

                } catch (Exception $e) {
                    DB::rollBack();
                    echo '<pre>';
                    var_dump($e->getMessage());
                    echo '</pre>';
                    echo '<pre>';
                    var_dump($e->getLine());
                    echo '</pre>';
                    echo '<pre>';
                    var_dump($e->getTraceAsString());
                    echo '</pre>';
                    echo '<pre>';
                    var_dump($codigo);
                    echo '</pre>';
                    exit;
                }

                // $this->admin_log('products', null, 'import_update_stock');
                DB::commit();
                echo '<pre>';
                var_dump($reception->toArray());
                echo '</pre>';
                echo '<pre>';
                var_dump($provider_order->toArray());
                echo '</pre>';
                exit;
                return Redirect::back()->with('success', 'Se actualizaron los stock de los productos importados.');
            }
        } else {
            echo '
                <form action="' . route('testController.updateProviderOrderReception') . '" method="POST" enctype="multipart/form-data">
                    <input type="file" name="file">
                    <br>
                    <input type="text" name="city_id">
                    <br>
                    <input type="text" name="invoice_number">
                    <br>
                    <input type="submit" value="Enviar">
                </form>
            ';
        }
    }

    public function rollbackReceptions()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        if (Request::isMethod('POST')) {
            if (Input::has('reception_id')) {
                $reception_id = Input::get('reception_id');
                $reception = ProviderOrderReception::find($reception_id);
                $reception->rollbackReception();
            }
        } else {
            echo '
                <form action="' . route('testController.rollbackReceptions') . '" method="POST" enctype="multipart/form-data">
                    <input type="text" name="reception_id">
                    <br>
                    <input type="submit" value="Enviar">
                </form>
            ';
        }
    }

    public function validate_large_image()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        $s3 = AWS::get('s3');

        $products = Product::all();
        //$products = Product::where('id', 1914)->get();
        foreach ($products as $key => $value) {
            if (strstr($value->image_large_url, 'https://d343udtxqbhvga.cloudfront.net')) {
                $bucket_name = 'merqueo-dev';
                $cloudfront_url = 'https://d343udtxqbhvga.cloudfront.net';
            } else {
                $bucket_name = 'merqueo';
                $cloudfront_url = 'https://d50xhnwqnrbqk.cloudfront.net';
            }
            $path = str_replace($cloudfront_url, '', $value->image_large_url);
            if (!$s3->doesObjectExist($bucket_name, $path)) {
                echo $value->name . ';' . $value->status;
                echo "<br>";
            }
        }
    }

    public function create_devolutions()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        if (isset($_FILES["archivo"])) {
            $cont = 0;
            $archivo = $_FILES["archivo"];
            if (!file_exists($archivo["tmp_name"])) {
                echo "EL ARCHIVO NO EXISTE.";
                exit();
            }

            $texto = fopen($archivo["tmp_name"], "r");
            while (!feof($texto)) {
                $linea = trim(fgets($texto, 4096));
                if ($linea != '') {
                    if ($order = Order::find($linea)) {
                        if (!$order_return = OrderReturn::where('order_id', $order->id)->first()) {
                            OrderReturn::saveOrderReturn($order->id, 'Dispatched');
                            $cont++;
                        }

                    } else echo $linea . '<br/>';
                }
            }
            fclose($texto);
            echo $cont;
        } else { ?>
            <h4>Leer TXT</h4>
            <form name="form" enctype="multipart/form-data" action="" method="POST">
                <b>Importar archivo:</b>
                <input type="file" name="archivo" size="50">
                <input type="submit" name="btnImportar" class="btn" value="Importar"
                       onClick="if (archivo.value==''){
         alert('Hay datos obligatorios vacios.');
         return false;
         }else{
            if (confirm('Esta seguro que desea ejecutar?')) return true;
            else return false;
        }"/></form><?php }
    }

    /**
     * Exportar en excel el plano E del recibo de bodega
     * @param  int $id identificador del recibo de bodega
     */
    public function exportPFiles()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);
        if (Request::isMethod('GET')) {
            echo '
                <form action="' . route('testController.exportPFiles') . '" method="POST">
                    <label for="warehouse_id">
                        ID de bodega:
                        <input type="text" name="warehouse_id" id="warehouse_id">
                    </label>
                    <br>
                    <label for="start_date">
                        Fecha inicio:
                        <input type="text" name="start_date" id="start_date">
                    </label>
                    <br>
                    <label for="end_date">
                        Fecha final:
                        <input type="text" name="end_date" id="end_date">
                    </label>
                    <br>
                    <input type="submit" value="Enviar">
                </form>
            ';
            return;
        } else {
            if (Input::has('warehouse_id') && Input::has('warehouse_id') && Input::has('start_date') && Input::has('end_date')){
                $warehouse_id = Input::get('warehouse_id');
                $warehouse = Warehouse::find($warehouse_id);
                $city = City::find($warehouse->city_id);
                $start_date = Input::get('start_date');
                $end_date = Input::get('end_date');
                $reception_ids = ProviderOrderReception::join('provider_orders', 'provider_order_receptions.provider_order_id', '=', 'provider_orders.id')
                    ->where('provider_orders.warehouse_id', $warehouse_id)
                    ->whereNotNull('provider_order_receptions.movement_consecutive_p')
                    // ->where('provider_order_receptions.movement_consecutive_p', '>=', 2187)
                    // ->where('provider_order_receptions.movement_consecutive_p', '>=', 3327)
                    // ->whereIn('provider_order_receptions.id', [6425, 6424, 6544, 6542, 6416, 6415, 6095, 6614, 7073, 7025, 7027, 6413, 6400, 6970, 6972, 6760, 6763, 6594, 6629, 6626, 6557, 6555, 6835, 6820, 6961, 6959, 6955, 6950, 6786, 6782, 7018, 6919, 7040])
                    ->whereBetween(DB::raw('DATE(provider_order_receptions.created_at)'), [$start_date, $end_date])
                    // ->where('provider_order_receptions.movement_consecutive_p', 2723)
                    ->select('provider_order_receptions.id')
                    ->orderBy('provider_order_receptions.movement_consecutive_p', 'ASC')
                    ->lists('id');

                $objPHPExcel = new PHPExcel();
                //propiedades
                $objPHPExcel->getProperties()->setCreator('Merqueo');
                $objPHPExcel->getProperties()->setTitle('Orden de Compra');
                $objPHPExcel->getProperties()->setSubject('Orden de Compra');
                $objPHPExcel->setActiveSheetIndex(0);

                //estilos
                $style_header = array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '99ccff'),
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000'),
                        'bold' => true,
                    )
                );
                $objPHPExcel->getActiveSheet()->mergeCells('A1:BH1')->getStyle('A1:BH1')->applyFromArray($style_header);
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, 'MERQUEO S A S');

                $objPHPExcel->getActiveSheet()->mergeCells('A2:BH2')->getStyle('A2:BH2')->applyFromArray($style_header);
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 2, 'MODELO PARA LA IMPORTACIÓN DE MOVIMIENTO CONTABLE');

                $objPHPExcel->getActiveSheet()->mergeCells('A3:BH3')->getStyle('A3:BH3')->applyFromArray($style_header);
                // $now = Carbon::now()->format('M j/Y');
                // $received_date = Carbon::createFromFormat('Y-m-d H:i:s', $reception->created_at);
                $received_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $end_date . ' 00:00:00');
                $header_date = $received_date->format('M j/Y');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 3, 'De: ' . $header_date . ' A: ' . $header_date);

                $objPHPExcel->getActiveSheet()->mergeCells('A4:BH4');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 4, '');

                //titulos
                $objPHPExcel->getActiveSheet()->getStyle('A5:BH5')->applyFromArray($style_header);
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 5, 'TIPO DE COMPROBANTE (OBLIGATORIO)');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 5, 'CÓDIGO COMPROBANTE  (OBLIGATORIO)');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, 5, 'NÚMERO DE DOCUMENTO (OBLIGATORIO)');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, 5, 'CUENTA CONTABLE   (OBLIGATORIO)');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, 5, 'DÉBITO O CRÉDITO (OBLIGATORIO)');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, 5, 'VALOR DE LA SECUENCIA   (OBLIGATORIO)');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, 5, 'AÑO DEL DOCUMENTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, 5, 'MES DEL DOCUMENTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, 5, 'DÍA DEL DOCUMENTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, 5, 'CÓDIGO DEL VENDEDOR');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, 5, 'CÓDIGO DE LA CIUDAD');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, 5, 'CÓDIGO DE LA ZONA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, 5, 'SECUENCIA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, 5, 'CENTRO DE COSTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, 5, 'SUBCENTRO DE COSTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, 5, 'NIT');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, 5, 'SUCURSAL');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, 5, 'DESCRIPCIÓN DE LA SECUENCIA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, 5, 'NÚMERO DE CHEQUE');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, 5, 'COMPROBANTE ANULADO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, 5, 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, 5, 'FORMA DE PAGO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, 5, 'PORCENTAJE DEL IVA DE LA SECUENCIA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, 5, 'VALOR DE IVA DE LA SECUENCIA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, 5, 'BASE DE RETENCIÓN');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, 5, 'BASE PARA CUENTAS MARCADAS COMO RETEIVA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, 5, 'SECUENCIA GRAVADA O EXCENTA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, 5, 'PORCENTAJE AIU');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, 5, 'BASE IVA AIU');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, 5, 'LÍNEA PRODUCTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, 5, 'GRUPO PRODUCTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, 5, 'CÓDIGO PRODUCTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, 5, 'CANTIDAD');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, 5, 'CANTIDAD DOS');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, 5, 'CÓDIGO DE LA BODEGA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, 5, 'CÓDIGO DE LA UBICACIÓN');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, 5, 'CANTIDAD DE FACTOR DE CONVERSIÓN');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, 5, 'OPERADOR DE FACTOR DE CONVERSIÓN');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, 5, 'VALOR DEL FACTOR DE CONVERSIÓN');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, 5, 'GRUPO ACTIVOS');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, 5, 'CÓDIGO ACTIVO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, 5, 'ADICIÓN O MEJORA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, 5, 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, 5, 'VECES A DEPRECIAR NIIF');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, 5, 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, 5, 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, 5, 'AÑO DOCUMENTO DEL PROVEEDOR');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, 5, 'MES DOCUMENTO DEL PROVEEDOR');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, 5, 'DÍA DOCUMENTO DEL PROVEEDOR');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, 5, 'TIPO DOCUMENTO DE PEDIDO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, 5, 'CÓDIGO COMPROBANTE DE PEDIDO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, 5, 'NÚMERO DE COMPROBANTE PEDIDO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, 5, 'SECUENCIA DE PEDIDO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, 5, 'TIPO Y COMPROBANTE CRUCE');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, 5, 'NÚMERO DE DOCUMENTO CRUCE');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, 5, 'NÚMERO DE VENCIMIENTO');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, 5, 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, 5, 'MES VENCIMIENTO DE DOCUMENTO CRUCE');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, 5, 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE');
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, 5, 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE');
                $i = 6;
                ProviderOrderReception::whereIn('id', $reception_ids)->chunk(200, function ($receptions) use (&$objPHPExcel, &$i) {
                    foreach ($receptions as $reception) {
                        $received_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $reception->created_at);
                        $provider_order = ProviderOrder::find($reception->provider_order_id);
                        $provider = Provider::find($provider_order->provider_id);
                        if ( $provider->type == 'Para faltantes' ) {
                            if ( $provider_order->warehouse->city_id == 1 ) {
                                if ( $provider_order->warehouse->id == 1 ) {
                                    $p_code = 3;
                                }else{
                                    $p_code = 8;
                                }
                            }elseif ( $provider_order->warehouse->city_id == 2 ) {
                                $p_code = 5;
                            }
                        }else{
                            if ( $provider_order->warehouse->city_id == 1 ) {
                                if ( $provider_order->warehouse->id == 1 ) {
                                    $p_code = 2;
                                }else{
                                    $p_code = 7;
                                }
                            }elseif ( $provider_order->warehouse->city_id == 2 ) {
                                $p_code = 4;
                            }
                        }

                        $consecutive = DB::table('consecutives')->where('type', 'provider_order_reception_p_' . $p_code)->where('city_id', $provider_order->warehouse->city_id)->first();
                        $reception->generateConsecutiveP();
                        $nit = explode('-', $provider->nit);
                        $nit = str_replace('.', '', $nit[0]);

                        $reception_details = $reception->getProducts();
                        $reception_details = $reception_details->filter(function ($product) {
                            return $product->type == 'Simple';
                        });
                        $grouped_products = $reception->getProductGroupDetails();
                        $key = 1;
                        foreach ($reception_details as $reception_detail) {
                            if ($reception_detail->sub_total_cost != 0) {
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, round($reception_detail->sub_total_cost, 2)); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $reception_detail->product_name); //DESCRIPCIÓN DE LA SECUENCIA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->quantity_received); //CANTIDAD
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->city_id); //CÓDIGO DE LA BODEGA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                                $i++;
                                $key++;

                                if ($reception_detail->consumption_tax) {
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, round($reception_detail->consumption_tax, 2)); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $reception_detail->product_name . ' Imp. consumo'); //DESCRIPCIÓN DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->quantity_received); //CANTIDAD
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->city_id); //CÓDIGO DE LA BODEGA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                                    $i++;
                                    $key++;
                                }
                            }
                        }

                        unset($reception_detail);
                        if (!empty($grouped_products)) {
                            foreach ($grouped_products as $reception_detail) {
                                if ( $reception_detail->sub_total_cost ) {
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception_detail->sub_total_cost); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $reception_detail->product_name); //DESCRIPCIÓN DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->city_id); //CÓDIGO DE LA BODEGA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                                    $i++;
                                    $key++;

                                    if ($reception_detail->consumption_tax) {
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception_detail->consumption_tax); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $reception_detail->product_name . ' Imp. consumo'); //DESCRIPCIÓN DE LA SECUENCIA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->city_id); //CÓDIGO DE LA BODEGA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                                        $i++;
                                        $key++;
                                    }
                                }
                            }
                        }

                        // TOTAL
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 2205010000); //CUENTA CONTABLE   (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'C'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, round($reception->getTotal(), 2)); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $provider->name . ' FV ' . $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, 'P-00' . $p_code); //TIPO Y COMPROBANTE CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 1); //NÚMERO DE VENCIMIENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                        $i++;
                        $key++;

                        // ICA
                        if ($reception->rete_ica_amount) {
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $reception->rete_ica_account); //CUENTA CONTABLE   (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'C'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, round($reception->rete_ica_amount, 2)); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $provider->name . ' FV ' . $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, '-' . $p_code); //TIPO Y COMPROBANTE CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                            $i++;
                            $key++;
                        }

                        // RETE FUENTE
                        if ($reception->rete_fuente_amount) {
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $reception->rete_fuente_account); //CUENTA CONTABLE   (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'C'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, round($reception->rete_fuente_amount, 2)); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $provider->name . ' FV ' . $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, '-' . $p_code); //TIPO Y COMPROBANTE CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                            $i++;
                            $key++;
                        }

                        // IVA's
                        $iva_array = $reception->getIvaDetails();
                        if (count($iva_array)) {
                            foreach ($iva_array as $iva) {
                                if ($iva['total']) {
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $iva['iva_account']); //CUENTA CONTABLE   (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, round($iva['total'], 2)); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->city_id); //CENTRO DE COSTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception->id . ' - ' . $provider->name . ' FV ' . $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, '-' . $p_code); //TIPO Y COMPROBANTE CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                                    $i++;
                                    $key++;
                                }
                            }
                        }
                    }
                });
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $filename = Session::get('admin_username') . ' - ' . date('Y-m-d H.i.s', time()) . '-plano-p-' . utf8_decode($city->city) . '.xlsx';
                $filename = mb_convert_encoding($filename, "UTF-8", "ASCII");
                $path = public_path(Config::get('app.download_directory')) . $filename;
                $objWriter->save($path);
                // chmod($path,0777);
                $headers = [
                    'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                ];
                return Response::download($path, $filename, $headers);
            }
        }
    }


    public function delete_duplicate_products_stock()
    {
        ini_set('memory_limit', '3072M');
        set_time_limit(0);
        DB::statement("SET lc_time_names = 'es_ES'");
        try {
            var_dump(Carbon\Carbon::now());
            $base_rows = DB::select("SELECT store_product_id FROM product_stock_log WHERE created_at BETWEEN '" . Carbon\Carbon::create(2017, 7, 23, 00) . "' AND '" . Carbon\Carbon::create(2017, 7, 23, 23) . "' GROUP BY store_product_id;");
            foreach ($base_rows as $base_row) {
                $items_rows = DB::select("SELECT * FROM product_stock_log WHERE store_product_id = " . $base_row->store_product_id . " AND created_at BETWEEN '2017-07-23 00:00:00' AND '2017-07-23 23:59:59';");
                for ($i = 0; $i < count($items_rows) - 1; $i++) {
                    $item = DB::delete("DELETE FROM product_stock_log WHERE store_product_id = " . $items_rows[$i]->store_product_id . " AND id = " . $items_rows[$i]->id . " AND created_at BETWEEN '2017-07-23 00:00:00' AND '2017-07-23 23:59:59';");
                }
            }
            var_dump(Carbon\Carbon::now());
        } catch (Exception $e) {
            debug($e);
        }
    }

    public function update_special_price_expiration_date()
    {
        $store_products = StoreProduct::whereNotNull('store_products.special_price_expiration_date')
            ->where('store_products.special_price_expiration_date', '<', date('Y-m-d H:i:s'))->get();
        foreach ($store_products as $store_product) {
            try {
                var_dump($store_product->id);
                $store_product->special_price = null;
                $store_product->quantity_special_price = 0;
                $store_product->first_order_special_price = 0;
                $store_product->promo_type_special_price = '';
                $store_product->save();
            } catch (Exception $e) {
                var_dump($e);
            }
        }
    }

    public function copyCostDataReceptions()
    {
        ini_set('memory_limit', '3072M');
        set_time_limit(0);
        $i = 0;


        ProviderOrder::orderBy('created_at', 'DESC')->chunk(50, function ($provider_orders) use (&$i)
        {
            $i++;
            foreach ($provider_orders as $key => $provider_order) {
                $provider_order_details = ProviderOrderDetail::where('provider_order_id', $provider_order->id)->get();

                ProviderOrderReception::where('provider_order_id', $provider_order->id)->chunk(50, function ($receptions) use ($provider_order_details)
                {
                    foreach ($receptions as $reception) {
                        ProviderOrderReceptionDetail::where('reception_id', $reception->id)->chunk(50, function ($details) use ($provider_order_details)
                        {
                            foreach ($details as $detail) {
                                foreach ($provider_order_details as $provider_order_detail) {
                                    if ( $detail->store_product_id == $provider_order_detail->store_product_id ) {
                                        if ( $detail->cost == 0 || is_null($detail->cost) ) {
                                            $detail->base_cost = $provider_order_detail->base_cost;
                                            $detail->cost = $provider_order_detail->cost;
                                            $detail->iva = $provider_order_detail->iva;
                                            $detail->iva_amount = $provider_order_detail->iva_amount;
                                            $detail->consumption_tax = $provider_order_detail->consumption_tax;
                                            $detail->old_cost = $provider_order_detail->old_cost;
                                            $detail->save();
                                        }

                                        if ( $provider_order_detail->type == 'Proveedor' && $detail->type == 'Proveedor' ) {
                                            $provider_order_detail_product_groups = ProviderOrderDetailProductGroup::where('provider_order_detail_id', $provider_order_detail->id)->get();
                                            $provider_order_reception_detail_product_groups = ProviderOrderReceptionDetailProductGroup::where('reception_detail_id', $detail->id)->get();

                                            foreach ($provider_order_detail_product_groups as $provider_order_detail_product_group) {
                                                foreach ($provider_order_reception_detail_product_groups as $provider_order_reception_detail_product_group) {
                                                    if ( $provider_order_detail_product_group->store_product_id == $provider_order_reception_detail_product_group->store_product_id ) {
                                                        if ( $provider_order_reception_detail_product_group->cost == 0 || is_null($provider_order_reception_detail_product_group->cost) ) {
                                                            $provider_order_reception_detail_product_group->cost = $provider_order_detail_product_group->cost;
                                                            $provider_order_reception_detail_product_group->base_cost = $provider_order_detail_product_group->base_cost;
                                                            $provider_order_reception_detail_product_group->iva = $provider_order_detail_product_group->iva;
                                                            $provider_order_reception_detail_product_group->iva_amount = $provider_order_detail_product_group->iva_amount;
                                                            $provider_order_reception_detail_product_group->consumption_tax = $provider_order_detail_product_group->consumption_tax;
                                                            $provider_order_reception_detail_product_group->old_cost = $provider_order_detail_product_group->old_cost;
                                                            $provider_order_reception_detail_product_group->save();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
        echo '<pre>';
        var_dump('finalizado:'. $i);
        echo '</pre>';
        exit;
    }


    public function register_vehicle_movement(){
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        if (isset($_FILES["archivo"])) {
            $cont = 0;
            $archivo = $_FILES["archivo"];
            if (!file_exists($archivo["tmp_name"])) {
                echo "EL ARCHIVO NO EXISTE.";
                exit();
            }

            $texto = fopen($archivo["tmp_name"], "r");
            while (!feof($texto)) {
                $linea = trim(fgets($texto, 4096));
                if ($linea != '') {
                    if ($order = Order::find($linea))
                    {
                        if ($order->payment_method != 'Efectivo y datáfono'){
                            //registrar movimiento de conductor
                            if (VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('status', 1)->first())
                            {
                                $movement = VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('order_id', $order->id)->where('status', 1)->first();
                                if (empty($movement))
                                    $movement = new VehicleMovement;
                                $movement->order_id = $order->id;
                                $movement->vehicle_id = $order->vehicle_id;
                                $movement->driver_id = $order->driver_id;
                                $movement->movement = 'Gestión de pedido';
                                $movement->payment_method = $order->payment_method;
                                $total_order = round($order->total_amount + $order->delivery_amount - $order->discount_amount);

                                if ($order->payment_method == 'Datáfono'){
                                    $movement->user_card_paid = $total_order;
                                    $movement->user_cash_paid = 0;
                                }

                                if ($order->payment_method == 'Tarjeta de crédito'){
                                    $movement->user_card_paid = $total_order;
                                    $movement->user_cash_paid = 0;
                                }

                                VehicleMovement::saveMovement($movement, $order);
                                $cont++;
                            }
                        }else echo $linea . '<br/>';
                    }
                }
            }
            fclose($texto);
            echo $cont;
        } else { ?>
            <h4>Leer TXT</h4>
            <form name="form" enctype="multipart/form-data" action="" method="POST">
                <b>Importar archivo:</b>
                <input type="file" name="archivo" size="50">
                <input type="submit" name="btnImportar" class="btn" value="Importar"
                       onClick="if (archivo.value==''){
         alert('Hay datos obligatorios vacios.');
         return false;
         }else{
            if (confirm('Esta seguro que desea ejecutar?')) return true;
            else return false;
        }"/></form><?php }



    }

    public function fixReceptionFromLog()
    {
        $receptions = ProviderOrderReception::whereBetween(DB::raw('DATE(created_at)'), ['2017-11-03', '2017-11-10'])->chunk(200, function ($receptions)
        // $receptions = ProviderOrderReception::where('id', 6341)->chunk(200, function ($receptions)
        {
            foreach ($receptions as $reception) {
                $logs = ProviderOrderReceptionLog::where('reception_id', $reception->id)->where('type', 'like', '%Costo unitario actualizado a:%')->select('type')->get();
                if ( count($logs) ) {
                    foreach ($logs as $log) {
                        $string_array = explode('/', $log->type);
                        preg_match_all('!\d+!', $string_array[0], $store_product_id);
                        if ( !empty($store_product_id[0][0]) ) {
                            $string_array[1] = str_replace('.', '', $string_array[1]);
                            preg_match_all('!\d+!', $string_array[1], $product_price);
                            $details = ProviderOrderReceptionDetail::where('reception_id', $reception->id)->where('store_product_id', $store_product_id[0][0])->first();
                            $details->cost = $product_price[0][0];
                            $details->base_cost = ($details->cost / (1 + ($details->iva / 100)));
                            $details->save();
                        }
                    }
                }

                $logs = ProviderOrderReceptionLog::where('reception_id', $reception->id)->where('type', 'like', '%Costo unitario de producto%')->select('type')->get();
                if ( count($logs) ) {
                    foreach ($logs as $log) {
                        $string_array = explode(':', $log->type);
                        preg_match_all('!\d+!', $string_array[0], $store_product_id);
                        if ( !empty($store_product_id[0][0]) ) {
                            $string_array[1] = str_replace('.', '', $string_array[1]);
                            preg_match_all('!\d+!', $string_array[1], $product_price);
                            $details = ProviderOrderReceptionDetail::where('reception_id', $reception->id)->where('store_product_id', $store_product_id[0][0])->first();
                            $details->cost = $product_price[0][0];
                            $details->base_cost = ($details->cost / (1 + ($details->iva / 100)));
                            $details->save();
                        }
                    }
                }
            }
        });
        echo '<pre>';
        var_dump('finalizó');
        echo '</pre>';
        exit;
    }


    public function fix_cc_payments($order_id = 0){

        ini_set('memory_limit', '512M');
        set_time_limit(0);
        $orders = Order::select('id', 'cc_payment_status', 'cc_payment_description', 'cc_payment_transaction_id', 'cc_charge_id', 'payment_date', 'cc_refund_status', 'cc_refund_date')
                        ->where('payment_method','Tarjeta de crédito')

                        ->where('id','>',$order_id)
                        ->get();

        if($orders){

            foreach ($orders as $order) {

                if(!is_null($order->cc_charge_id)){

                    //$order_payments = new OrderPayments;
                    $data = array();
                    $data['order_id'] = $order->id;
                    $data['admin_id'] = Session::get('admin_id');
                    $data['cc_payment_status'] = (is_null($order->cc_payment_status) && !is_null($order->payment_date) )?'Aprobada':$order->cc_payment_status;
                    $data['cc_payment_description'] = $order->cc_payment_description;
                    $data['cc_payment_transaction_id'] = $order->cc_payment_transaction_id;
                    $data['cc_charge_id'] = $order->cc_charge_id;
                    $data['cc_payment_date'] = $order->payment_date;
                    $data['created_at'] = $order->payment_date;
                    $data['updated_at'] = $order->payment_date;
                    $id = DB::table('order_payments')->insertGetId( $data);

                    if(!empty($order->cc_refund_date)){
                        $data_refunds = array();
                        //$order_payment_refunds = new OrderPaymentRefunds;
                        $data_refunds['order_payment_id'] = $id;
                        $data_refunds['admin_id'] = Session::get('admin_id');
                        $data_refunds['status'] = $order->cc_refund_status;
                        $data_refunds['date'] = $order->cc_refund_date;
                        $data_refunds['created_at'] = $order->cc_refund_date;
                        $data_refunds['updated_at'] = $order->cc_refund_date;
                        $id_refund = DB::table('order_payment_refunds')->insertGetId( $data_refunds);

                    }
                }
                flush();
                # code...
            }
            echo '<br>Completed';

        }
    }


    public function marketplace_date(){
        $alert = TransporterAlert::leftJoin('admin', 'transporter_alerts.admin_id', '=', 'admin.id')
                                    ->join('vehicles', 'transporter_alerts.vehicle_id', '=', 'vehicles.id')
                                    ->join('drivers', 'vehicles.driver_id', '=', 'drivers.id')
                                    ->join('transporters', 'drivers.transporter_id', '=', 'transporters.id')
                                    ->join('cities', 'transporters.city_id', '=', 'cities.id')
                                    ->select(
                                        'transporter_alerts.id AS id',
                                        'vehicles.plate',
                                        'drivers.cellphone',
                                        DB::raw('CONCAT(drivers.first_name, drivers.last_name) AS driver'),
                                        'transporter_alerts.reason',
                                        'transporter_alerts.status',
                                        'transporter_alerts.created_at',
                                        'transporter_alerts.order_id',
                                        'cities.city',
                                        'cities.id AS city_id'
                                    )
                                    ->where('transporter_alerts.id', 343411)
                                    ->first()
                                    ->toArray();
        if(!is_null($alert['order_id']))
            $alert['order_url'] = route('adminOrderStorage.details', ['id' => $alert['order_id']]);
        $alert['created_at'] = date("d M Y g:i a",strtotime( $alert['created_at']));
        $alert['message'] = 'Prueba de Firebase';
        $device_id = 'APA91bHJq_BDO18zZ5R2j_28ARRziCvbQhhlD2RJSe5KaeCcBg2usxU_dfaEOxHtea6j9U9auP18KxM3QHpvv1SzCPsGgEUoqXFRuXlVTvN1JJX4bOCRZ48';
        $player_id = '5ee4d3d2-5dfe-4f04-a673-ba10a8238586';
        $rs = send_notification('2.1.10', 'Android', $device_id, $player_id, $alert);
        echo '<pre>Resultado::';
        print_r($rs);
        echo '</pre>';
        exit;
        //$detalle = Order::getDetail(47238, 479016);
        /*echo '<pre>';

        print_r($detalle);
        echo '</pre>';
        exit;*/
        /*$marketplace_date = get_delivery_date_marketplace('2018-03-26 08:00:00', 72);
        echo '<pre>';
        print_r($marketplace_date);
        echo '</pre>';
        exit;*/
    }

    /*
    * Metodo para obtener los push de onesignal y exportarlos a .csv
    * @return csv
    */
    public function get_push_onesignal()
    {
        $date = date('Y-m-d H:i:s');
        $start_date = date('Y-m-01 00:00:00', strtotime($date));
        $os =new OneSignal();
        $report = $os->get_report();
        $report = json_decode($report);
        $print = "<table cellspacing='0' cellpadding='0' border='0'>".
            "<thead>".
                "<tr>".
                    "<th align='center' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>registro #</th>".
                    "<th align='center' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>Order id</th>".
                    "<th align='center' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>Estado</th>".
                    "<th align='center' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>Mensaje</th>".
                    "<th align='center' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>Fecha</th>".
                    "<th align='center' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>Click</th>".
                "</tr>".
            "</thead>".
            "<tbody>";
        $i = 1;
        $j = 0;
        $yes = 0;
        $no = 0;
        $contador = 50;
        $notification = $report->notifications;
        while ($i <= $contador) {
                if (strtotime($start_date) > strtotime(date('Y-m-d H:i:s', $notification[$j]->queued_at))) {
                    break;
                }
                $print .= "<tr>";
                    $print .= "<td align='center' width='325' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $i . "</td>";
                    $print .= "<td align='center' width='325' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $notification[$j]->data->order_id . "</td>";
                    $print .= "<td align='center' width='325' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $notification[$j]->data->type . "</td>";
                    $print .= "<td align='center' width='325' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $notification[$j]->data->message . "</td>";
                    $print .= "<td align='center' width='325' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . date('Y-m-d H:i:s', $notification[$j]->queued_at) . "</td>";
                    $print .= "<td align='center' width='325' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>";
                        if ($notification[$j]->converted) {
                            $yes++;
                            $print .= "Si";
                        } else {
                            $no++;
                            $print .="No";
                        }
                    $print .= "</td>";
                    if($i == $contador){
                        if (strtotime($start_date) < strtotime(date('Y-m-d H:i:s', $notification[$j]->queued_at))) {
                             $report = $os->get_report($contador);
                             $report = json_decode($report);
                             $notification = $report->notifications;
                             $contador += 50;
                             $j = 0;
                        } else {
                            $j++;
                        }
                    }
                $print .= "</tr>";
                $i++;
        }
        $print .= "<tr><td colspan='5'  style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>Total push clickeados: </td><td  style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $yes . "</td></tr>";
        $print .= "<tr><td colspan='5'  style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>Total push no clickeados: </td><td  style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $no . "</td></tr>";
        $print .= "<tr><td colspan='5'  style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>Total registros contados: </td><td  style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;'>" . $yes + $no . "</td></tr>";
        $print .= "</tbody></table>";
        $filename = Session::get('admin_username') . ' - ' . date('Y-m-d H.i.s', time()) . '- Historico-de-push.xls';
        header("Content-type: application/vnd.ms-excel");
        mb_convert_encoding($filename, "UTF-16", "ASCII");
        header("Content-disposition: attachment; filename=" . $filename);
        print($print);
        //echo $print;
    }

    /*
    * Metodo para probar el diseño de los email en el navegador
    * @param Input type
    * @return view
    */
    public function test_desing_mail(){
        $type = Input::get('type');
        if($type == 'recibido'){
            //$orders = Order::where('group_id', 428988)->get();
            $orders = Order::where('group_id', 465965)->get();
            foreach ($orders as $order) {
                $order_products = OrderProduct::where('order_id', $order->id)->get();

                foreach($order_products as $order_product){
                    $products_mail[] = array(
                        'order_type' => $order->type,
                        'img' => $order_product->product_image_url,
                        'name' => $order_product->product_name,
                        'quantity' => $order_product->product_quantity,
                        'unit' => $order_product->product_unit,
                        'qty' => $order_product->quantity,
                        'price' => $order_product->price,
                        'total' => $order_product->quantity * $order_product->price,
                        'type' => $order_product->type
                    );
                }                
            }
            $user = User::find(243182);
            //$order_group = OrderGroup::find(428988);
            $order_group = OrderGroup::find(465965);
            $orders = Order::select('orders.*', 'stores.is_hidden AS store_is_hidden', 'user_brand_campaigns.campaign')
                                ->where('group_id', $order_group->id)
                                ->join('stores', 'stores.id', '=', 'orders.store_id')
                                ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                                ->orderBy('stores.is_hidden', 'desc')
                                ->groupBy('type')
                                ->get();

            $mail = array(
                'template_name' => 'emails.order_received',
                'subject' => 'Tu Merqueo',
                'to' => array(array('email' => 'jlara@merqueo.com', 'name' => 'Juan Lara')),
                'vars' => array(
                    'orders' => $orders,
                    'order_group' => $order_group,
                    'products' => $products_mail
                )
            );
            $send = send_mail($mail);
            //$view = "true";
            $view = View::make('emails.order_received', $mail['vars'])->render();
        }else if ($type == 'encamino') {
            $order_group = OrderGroup::find(432457);
            $order = Order::select('orders.*', 'user_firstname', 'user_lastname', 'user_email', 'user_phone', 'delivery_time',
                                'user_address', 'user_address_further', 'source', 'device_player_id', 'user_device_id', 'source_os',
                                'app_version')
                                ->join('order_groups', 'order_groups.id', '=', 'group_id')
                                //->where('source', '<>', 'Reclamo')
                                ->where('orders.id', 445667)
                                ->whereNull('ontheway_notification_date')
                                ->first();
            $products = OrderProduct::where('order_id', 445667)
                          ->where('type', '<>', 'Muestra')
                          ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                          ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                          ->select('order_products.*')
                          ->get();
            $products_mail = [];
            foreach ($products as $product) {
                $products_mail[] = array(
                     'img' => $product->product_image_url,
                     'name' => $product->product_name,
                     'qty' => $product->quantity,
                     'qty_original' => $product->quantity_original,
                     'quantity' => $product->product_quantity,
                     'unit' => $product->product_unit,
                     'price' => $product->price,
                     'total' => $product->quantity * $product->price,
                     'type' => $product->type,
                     'status' => $product->fulfilment_status != 'Not Available' && $product->fulfilment_status != 'Returned' ? 'available.png' : 'no_available.png',
                     'gift' => $product->gift,
                     'sample' => $product->sample
                );
            }
            $products_available = [ 'products' => [], 'subtotal' => 0 ];
            $products_not_available = [ 'products' => [], 'subtotal' => 0 ];
            $delivery_time_text = '30 a 60 mins';
            $i=0;
            foreach ($products_mail as $product) {
                if ($product['status'] == 'available.png') {
                    $products_available['products'][] = $product;
                    $products_available['subtotal'] += $product['total'];
                    $products_not_available['products'][] = $product;
                    $products_not_available['products'][$i]['status'] = 'no_available.png';
                    $products_not_available['products'][$i]['total'] *= -1;
                    $products_not_available['subtotal'] += ($product['total'] * -1);
                }else{
                    $products_not_available['products'][] = $product;
                    $products_not_available['subtotal'] += $product['total'];
                }
                $i++;
            }
            $mail = array(
                'template_name' => 'emails.order_ontheway',
                'subject' => 'Tu pedido en Merqueo',
                'to' => array(array('email' => 'jlara@merqueo.com', 'name' => 'Juan Lara')),
                'vars' => array(
                    'order' => $order,
                    'products' => $products_mail,
                    'products_available' => $products_available,
                    'products_not_available' => $products_not_available,
                    'delivery_time' => $delivery_time_text,
                ),
            ); 
            /*$send = send_mail($mail);
            $view = "true";*/
            $view = View::make('emails.order_ontheway', $mail['vars'])->render();
        }else if ($type == 'cancelado') {
            $order_group = OrderGroup::find(432457);
            $order = Order::select('orders.*', 'stores.is_hidden AS store_is_hidden', 'user_brand_campaigns.campaign')
                                ->where('group_id', $order_group->id)
                                ->join('stores', 'stores.id', '=', 'orders.store_id')
                                ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                                ->orderBy('stores.is_hidden', 'desc')
                                ->first();
            $products = OrderProduct::where('order_id', $order->id)
                          ->where('is_gift', 0)
                          ->where('type', '<>', 'Muestra')
                          ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                          ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                          ->select('order_products.*')
                          ->get();
            foreach ($products as $product) {
                $products_mail[] = array(
                     'img' => $product->product_image_url,
                     'name' => $product->product_name,
                     'qty' => $product->quantity,
                     'qty_original' => $product->quantity_original,
                     'quantity' => $product->product_quantity,
                     'unit' => $product->product_unit,
                     'price' => $product->price,
                     'total' => $product->quantity * $product->price,
                     'type' => $product->type,
                     'status' => $product->fulfilment_status != 'Not Available' && $product->fulfilment_status != 'Returned' ? 'available.png' : 'no_available.png',
                     'gift' => $product->gift,
                     'sample' => $product->sample
                );
            }
            $products_available = [ 'products' => [], 'subtotal' => 0 ];
            $products_not_available = [ 'products' => [], 'subtotal' => 0 ];
            $i=0;
            foreach ($products_mail as $product) {
                if ($product['status'] == 'available.png') {
                    $products_available['products'][] = $product;
                    $products_available['subtotal'] += $product['total'];
                    $products_not_available['products'][] = $product;
                    $products_not_available['products'][$i]['status'] = 'no_available.png';
                    $products_not_available['products'][$i]['total'] *= -1;
                    $products_not_available['subtotal'] += ($product['total'] * -1);
                }else{
                    $products_not_available['products'][] = $product;
                    $products_not_available['subtotal'] += $product['total'];
                }
                $i++;
            }
            $delivery_time_text = '30 a 60 mins';
            $mail = array(
                'template_name' => 'emails.order_rejected',
                'subject' => 'Tu pedido en Merqueo',
                'to' => array(array('email' => 'jlara@merqueo.com', 'name' => 'Juan Lara')),
                'vars' => array(
                    'order' => $order,
                    'order_group' => $order_group,
                    'products' => $products_mail,
                    'products_available' => $products_available,
                    'products_not_available' => $products_not_available,
                    'delivery_time' => $delivery_time_text,
                ),
            ); 
            /*$send = send_mail($mail);
            $view = "true";*/
            $view = View::make('emails.order_rejected', $mail['vars'])->render();
        }else if($type == 'enviado'){
            $order_group = OrderGroup::find(432457);
            $order = Order::select('orders.*', 'stores.is_hidden AS store_is_hidden', 'user_brand_campaigns.campaign')
                                ->where('group_id', $order_group->id)
                                ->join('stores', 'stores.id', '=', 'orders.store_id')
                                ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                                ->orderBy('stores.is_hidden', 'desc')
                                ->first();
            $products = OrderProduct::where('order_id', $order->id)
                          ->where('is_gift', 0)
                          ->where('type', '<>', 'Muestra')
                          ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                          ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                          ->select('order_products.*')
                          ->get();
            foreach ($products as $product) {
                $products_mail[] = array(
                     'img' => $product->product_image_url,
                     'name' => $product->product_name,
                     'qty' => $product->quantity,
                     'qty_original' => $product->quantity_original,
                     'quantity' => $product->product_quantity,
                     'unit' => $product->product_unit,
                     'price' => $product->price,
                     'total' => $product->quantity * $product->price,
                     'type' => $product->type,
                     'status' => $product->fulfilment_status != 'Not Available' && $product->fulfilment_status != 'Returned' ? 'available.png' : 'no_available.png',
                     'gift' => $product->gift,
                     'sample' => $product->sample
                );
            }
            $products_available = [ 'products' => [], 'subtotal' => 0 ];
            $products_not_available = [ 'products' => [], 'subtotal' => 0 ];
            $i=0;
            foreach ($products_mail as $product) {
                if ($product['status'] == 'available.png') {
                    $products_available['products'][] = $product;
                    $products_available['subtotal'] += $product['total'];
                    $products_not_available['products'][] = $product;
                    $products_not_available['products'][$i]['status'] = 'no_available.png';
                    $products_not_available['products'][$i]['total'] *= -1;
                    $products_not_available['subtotal'] += ($product['total'] * -1);
                }else{
                    $products_not_available['products'][] = $product;
                    $products_not_available['subtotal'] += $product['total'];
                }
                $i++;
            }
            $delivery_time_text = '30 y 60 mins';
            $mail = array(
                'template_name' => 'emails.order_delivered',
                'subject' => 'Tu pedido en Merqueo',
                'to' => array(array('email' => 'jlara@merqueo.com', 'name' => 'Juan Lara')),
                'vars' => array(
                    'order' => $order,
                    'order_group' => $order_group,
                    'products' => $products_mail,
                    'products_available' => $products_available,
                    'products_not_available' => $products_not_available,
                    'delivery_time' => $delivery_time_text,
                ),
            ); 
            /*$send = send_mail($mail);
            $view = "true";*/
            $view = View::make('emails.order_delivered', $mail['vars'])->render();
        }
        return $view;
    }



    public function test_send_20000(){


        $users = User::select('first_name','last_name','email','phone','referral_code')->whereIn('id', [1020,1021])->get();
        //243054,242983

        $expiration_days = Config::get('app.referred.amount_expiration_days');
        $minimum_discount_amount = Config::get('app.minimum_discount_amount');

        foreach ($users as $user) {
            
            $mail = [
                'template_name' => 'emails.referreds_new',
                'subject' => 'Invita a tus amigos a ser parte de Merqueo Y gana ',
                'to' => [['email' => $user->email, 'name' => $user->first_name. ' '.$user->last_name ]],
                'vars' => [
                    'name' => $user->first_name. ' '.$user->last_name,
                    'referral_code' => $user->referral_code,
                    'referred' => User::validateReferred($user),
                    'expiration_days' => $expiration_days,
                    'minimum_discount_amount' => $minimum_discount_amount
                ],
            ]; 

            send_mail($mail);

        }




    }


    public function  test_referred(){


        $referrer = User::find(1021);
        $expiration_days = Config::get('app.referred.amount_expiration_days');
        $expiration_date = date('Y-m-d', strtotime('+' . $expiration_days . ' day'));
        $referred_by_amount = Config::get('app.referred.referred_by_amount');

        $user = User::find(1020);

        UserCredit::addCredit($referrer, $referred_by_amount, $expiration_date, 'referred_by',$user);

        $user_discounts = User::getDiscounts($referrer->id);


        //enviar mail de cargo de credito a usuario referido
        $mail = array(
            'template_name' => 'emails.referred',
            'subject' => 'Tienes '.currency_format($referred_by_amount).' de crédito en tu cuenta',
            'to' => array(array('email' => $referrer->email, 'name' => $referrer->first_name.' '.$referrer->last_name)),
            'vars' => array(
                'username' => $referrer->first_name,
                'username_referrer' => $user->first_name,
                'credit_available' => $user_discounts['amount'],
                'referred_by_amount' => $referred_by_amount,
                'referred' => User::validateReferred($referrer),
                'expiration_date' => format_date('normal', $expiration_date),
                'expiration_days' => $expiration_days,
                'referal_code' => $referrer->referral_code,
                'minimum_discount_amount' => $user_discounts['minimum_discount_amount']
            )
        );

        send_mail($mail);
    }

    public function test_get_orders_transporter($id){
        $array_status = ['Dispatched' => 'Despachado','Delivered'=>'Entregado','Cancelled'=>'Cancelado', 'En Camino'=>'En Camino'];
        if (!is_numeric($id))
            return $this->jsonResponse('ID transportador debe ser numérico.', 400);

        if (!$vehicle = Vehicle::find($id))
            return $this->jsonResponse('ID vehiculo no existe.', 400);

        $pending_orders = array();

        //pedidos pendientes
        $orders = Order::select(
            'orders.id',
            DB::raw("IF(orders.status = 'Dispatched' AND orders.driver_cancel_date IS NULL AND orders.onmyway_date IS NOT NULL, 'En Camino', IF(orders.status = 'Dispatched' AND orders.driver_cancel_date IS NOT NULL, 'Cancelled', orders.status)) AS status"),
            'orders.total_products',
            'orders.delivery_amount',
            'orders.discount_amount',
            'orders.total_amount',
            'orders.reject_reason',
            DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS grand_total'),
            'orders.onmyway_date',
            'orders.arrived_date',
            'orders.management_date',
            'orders.picking_baskets',
            'orders.picking_bags',
            'orders.planning_sequence',
            'orders.route_id',
            'routes.route AS planning_route',
            'order_groups.user_firstname',
            'order_groups.user_lastname',
            'order_groups.user_phone',
            'order_groups.user_address',
            'order_groups.user_address_further',
            'order_groups.user_address_neighborhood',
            'cities.city AS user_city',
            'order_groups.user_address_latitude',
            'order_groups.user_address_longitude',
            'orders.delivery_time',
            'payment_method',
            'orders.cc_charge_id',
            'order_groups.user_comments AS user_comments',
            'orders.delivery_date',
            DB::raw('DATE(delivery_date) AS delivery_date_day'),
            DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.user_id <> 5169 AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
            ->join('users','orders.user_id','=','users.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('cities', 'cities.id', '=', 'user_city_id')
            ->join('routes', 'routes.id', '=', 'route_id')
            //->where('orders.type', 'Merqueo')
            ->where('orders.vehicle_id', $vehicle->id)
            ->whereIn('orders.status', ['Dispatched', 'Delivered', 'Cancelled'])
            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime('+1 days')))
            ->orderBy('orders.status', 'DESC')
            ->orderBy('routes.planning_date')
            ->orderBy('routes.id')
            ->orderBy('orders.planning_sequence');

        if (Input::get('order_id'))
            $orders->where('orders.id', Input::get('order_id'));
        if (Input::get('ids'))
            $orders->whereNotIn('orders.id', Input::get('ids'));

        $orders = $orders->get();

        $ids = array();
        foreach($orders as $order)
            $ids[] = $order->id;

        $pending_orders = $orders->toArray();
        $reject_reasons = RejectReason::where('is_storage', 1)->where('status', 'Cancelado temporalmente')->orderBy('reason')->get()->toArray();
        $type_news = \NewReason::select(DB::raw('DISTINCT type'))->where('status', 1)->orderBy('type')->get()->toArray();
        $new_reasons = \NewReason::where('status', 1)->orderBy('reason')->get()->toArray();

        //mezclar arrays
        foreach($pending_orders as $i => $order)
        {
            if ($order['management_date'] && date('Y-m-d 00:00:00') > $order['management_date']){
                unset($pending_orders[$i]);
                continue;
            }
            $news_transportes = \TransporterNew::where('order_id',$order['id'] )->whereIn('status', ['Sin Asignar', 'Pendiente'])->count();
            $pending_orders[$i]['has_news'] = ($news_transportes > 0 );
            $pending_orders[$i]['status_text'] = $array_status[$order['status']];

            //OBTENER PRODUCTOS DEL PEDIDO
            $order_products = [];
            $products = OrderProduct::select('order_products.*', 'products.reference', 'products.description','products.image_large_url', 'store_products.storage', 'orders.arrived_date')
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->orderBy('department_id', 'desc')
                ->orderBy('product_name', 'asc')
                ->where('order_id', $order['id'])
                ->whereIn('order_products.type', ['Product','Muestra', 'Recogida'])
                ->get();

            $products_info = [];
            $order_products = $products->toArray();

            $product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*', 'store_products.storage', 'orders.arrived_date')
                ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->where('order_product_group.order_id', $order['id'])
                ->get();

            if ($product_group){
                $order_products =array_merge($order_products, $product_group->toArray() );
            }

            $pending_orders[$i]['reject_reasons'] = $reject_reasons;
            $pending_orders[$i]['type_news'] = $type_news;
            $pending_orders[$i]['new_reasons'] = $new_reasons;
            if ($order['status'] == 'Cancelled')
                $pending_orders[$i]['reject_reason'] = $order['reject_reason'];

            if (empty($order['cc_charge_id']))
                $pending_orders[$i]['cc_charge_id'] = '';

            $pending_orders[$i]['products'] = $order_products;

            if (empty($pending_orders[$i]['qty_orders']))
                $pending_orders[$i]['qty_orders'] = 0;

            //ocultar direccion teniendo en cuenta la franja de entrega
            if ($pending_orders[$i]['planning_sequence'] > 1){
                $delivery_time = explode(' - ', $pending_orders[$i]['delivery_time']);
                if (date('Y-m-d H:i', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes')) >= date('Y-m-d H:i')){
                    $pending_orders[$i]['user_address'] = 'Es visible a las '.date('g:i a', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes'));
                    $pending_orders[$i]['user_address_neighborhood'] = 'Es visible a las '.date('g:i a', strtotime($pending_orders[$i]['delivery_date_day'].' '.$delivery_time[0].' -30 minutes'));
                    $pending_orders[$i]['user_address_latitude'] = 0;
                    $pending_orders[$i]['user_address_longitude'] = 0;
                }
            }
        }
        $pending_orders = array_values($pending_orders);
        /*echo '<pre>';
        print_r($pending_orders);
        echo '</pre>';
        exit;*/
        /*$this->response = array(
            'status' => true,
            'message' => 'Pedidos obtenidos',
            'result' => array(
                'orders' => $pending_orders
            )
        );*/

        //return $this->jsonResponse();
    }


    public function deeplink_tienda_carrito()
    {
        $store_bogota_id = 63;
        $store_medellin_id = 64;
        $store_bogota = Store::with('city')->find($store_bogota_id);
        $store_medellin = Store::with('city')->find($store_medellin_id);
        Route::get('/{city_slug}/domicilios-{store_slug}', [
            'as' => 'frontStore.all_departments',
            'uses' => 'StoreController@all_departments'
        ]);
        $url_store_bogota = route('frontStore.all_departments', [
            'city_slug' => $store_bogota->city->slug,
            'store_slug' => $store_bogota->slug,
        ]);
        $url_store_medellin = route('frontStore.all_departments', [
            'city_slug' => $store_medellin->city->slug,
            'store_slug' => $store_medellin->slug,
        ]);
        /*$url_store_bogota = "https://merqueo.com/" . $store_bogota->city->slug . "/domicilios-" . $store_bogota->slug;
        $url_store_medellin = "https://merqueo.com/" . $store_medellin->city->slug . "/domicilios-" . $store_medellin->slug;*/

        $url_store_bogota = str_replace("http://devmerqueo.com", "https://merqueo.com", $url_store_bogota);
        $url_store_medellin = str_replace("http://devmerqueo.com", "https://merqueo.com", $url_store_medellin);

        $deeplink_store_url_bogota = \FirebaseClient::generateDynamicLink([
            'url' => $url_store_bogota,
            'type' => 'store',
            'store_id' => $store_bogota->id,
        ]);


        $deeplink_store_url_medellin = \FirebaseClient::generateDynamicLink([
            'url' => $url_store_medellin,
            'type' => 'store',
            'store_id' => $store_medellin->id,
        ]);

        $deeplink_cart_url_bogota = \FirebaseClient::generateDynamicLink([
            'url' => $url_store_bogota,
            'type' => 'cart'
        ]);

        $deeplink_cart_url_medellin = \FirebaseClient::generateDynamicLink([
            'url' => $url_store_medellin,
            'type' => 'cart'
        ]);
        echo "Deeplink store Bogota <br>";
        echo $deeplink_store_url_bogota->shortLink . '<br>';
        echo "Deeplink store Medellin <br>";
        echo $deeplink_store_url_medellin->shortLink . "<br>";
        echo "Deeplink cart Bogota <br>";
        echo $deeplink_cart_url_bogota->shortLink . "<br>";
        echo "Deeplink car Medellin <br>";
        echo $deeplink_cart_url_medellin->shortLink . "<br>";
        exit;
    }


    public function test_mail_referidos(){

        // $mail = [
        //     'template_name' => 'emails.referredes',
        //     'subject' => 'Prueba 20 mil ',
        //     'to' => [ ['email' => 'wblomo22@gmail.com', 'name' => 'Wbeimar' ] ],
        //     'vars' => [ 'referal_code' => 'CodeRef13' ]
        // ];

        // send_mail($mail);


        return View::make('emails.referredes', ['referal_code' => 'CodigoRef', 'referral_url' => '' ])->render();
    }

    public function migrate_banners()
    {
        $banners = Banner::all();
        foreach ($banners as $index => $banner) {
            $warehouses = Warehouse::where('city_id', $banner->city_id)->lists('id');
            if (is_null($banner->department_id)) {
                $banner->is_home = 1;
            } else {
                $banner->departments()->sync([$banner->department_id => ['position' => $banner->position]]);
            }
            $banner->warehouses()->sync($warehouses);
            $banner->save();
        }
    }


    public function update_bin($pasarela){
        $planningDateStart = \Carbon::now()->format('Y-m-d');
        $planningDateEnd = \Carbon::now()->addDay(1)->format('Y-m-d');
        echo '<pre>StartDate::';
        print_r($planningDateStart);
        echo '</pre>';
        echo '<pre>EndDate::';
        print_r($planningDateEnd);
        echo '</pre>';
        exit;
        exit;
    }



    public function paginatorTest()
    {
        $admin = Product::paginate(3);
//        $admin->setBaseUrl('http://Prueba.com');

        return $admin->links()->render();
    }
    public function storeProductStockLog($month){

        $date = new Carbon\Carbon($month);
        $month = $date->toDateString();
        if (!ProductStockLog::where(DB::raw('DATE(created_at)'), $month)->first()) {
            try {
                DB::beginTransaction();
                StoreProduct::join('store_product_warehouses', 'store_product_id', '=', 'store_products.id')
                    ->where(DB::raw('DATE(store_product_warehouses.created_at)'), '<=', $month)
                    ->select('store_products.id', 'store_product_warehouses.warehouse_id')
                    ->chunk(20, function ($product) use ($month) {
                        foreach ($product as $data) {
                            $productStockLog = new ProductStockLog ();
                            $productStockLog->store_product_id = $data->id;
                            $productStockLog->warehouse_id = $data->warehouse_id;
                            $productStockLog->reception_stock = 0;
                            $productStockLog->picking_stock = 0;
                            $productStockLog->picked_stock = 0;
                            $productStockLog->current_stock = 0;
                            $productStockLog->return_stock = 0;
                            $productStockLog->is_visible_stock = 0;
                            $productStockLog->price = 0;
                            $productStockLog->special_price = 0;
                            $productStockLog->cost = 0;
                            $productStockLog->base_cost = 0;
                            $productStockLog->cost_average = 0;
                            $productStockLog->provider_discount = 0;
                            $productStockLog->merqueo_discount = 0;
                            $productStockLog->seller_discount = 0;
                            $productStockLog->discontinued = 0;
                            $productStockLog->status = 0;
                            $productStockLog->created_at = $month . '07:00:00';
                            $productStockLog->updated_at = $month . '07:00:00';
                            $productStockLog->save();


                        }
                    });
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return 'Ocurrio un error.';

            }
        }
        return 'Ok';
    }

    public function checkAddCreditDeliveryLate($order)
    {
        $order->management_date = Carbon::createFromFormat('Y-m-d H:i:s', $order->delivery_date)->addMinutes(10);

        $this->addCreditWhenDeliveryLate->handle($order, Carbon::now()->addYear());

    }

    public function activatePromotion($discount, $star_date, $end_date, $qty, $token)
    {
        if($token == "dd7e58961f1c2a5efb2b400eb5eea04b"){
            Artisan::call("command:store-discounts", ["discount"=>$discount, "start-date"=>$star_date, "limit-date"=>$end_date, "q-special-price"=>$qty]);
        }
    }

}
