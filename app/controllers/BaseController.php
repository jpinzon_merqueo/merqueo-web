<?php

use Carbon\Carbon;
use Merqueo\Backend\Facades\AuthBackend;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller {

    public $config;
    public $store = false;
    public $user_id = false;
    public $cont = 0;

    public function __construct()
    {
        /*DB::listen(function($sql, $bindings, $time) {
            print_r('<br>');
            print_r('-------------------------- INIT -----------------------------------');
            print_r('<br>'); print_r('Consulta:'); print_r($sql);
            print_r('<br>'); print_r('Brindings'); print_r($bindings);
            print_r('<br>'); print_r('Time'); print_r($time);
            print_r('<br>'); echo $this->cont; $this->cont++;
            print_r('<br>');
            print_r('-------------------------- FIN -----------------------------------');
            print_r('<br>'); print_r('<br>'); print_r('<br>');
        });*/
        //mensaje de validacion para el usuario
        $this->config['credit_cards_message'] = 'Visa, Mastercard y American Express.';

        //codigos de tipos de tarjetas aceptadas
        $this->config['credit_cards_types'] = array(
            'VISA',
            'MASTERCARD',
            'AMEX',
            'CREDENCIAL',
            'CODENSA'
        );

        //nombre largo de cada metodo de pago
        $this->config['payment_methods_names'] = OrderPayment::getPaymentMethods();

        // métodos de pago
        $this->config['payment_methods'] = array(
            'Tarjeta de crédito' => 'Tarjeta de crédito',
            'Efectivo' => 'Efectivo',
            'Datáfono' => 'Datáfono',
            'Débito - PSE' => 'Débito - PSE'
        );


        //metodos de pago Darksupermarket
        $this->config['payment_methods_darksupermaket'] = [
            'Efectivo' => 'Efectivo'
        ];

        //traducir origen de pedido
        $this->config['order_sources'] = array(
           'Device' => 'Aplicación Movíl',
           'Callcenter' => 'Callcenter',
           'Web' => ' Pagina Web',
           'Web Service' => 'Domicilios.com'
        );

        $this->config['order_statuses'] = array(
            'Validation' => 'Recibido',
            'Initiated' => 'Recibido',
            'Enrutado' => 'Recibido',
            'In Progress' => 'Recibido',
            'Alistado' => 'Alistado',
            'Dispatched' => 'Despachado',
            'Delivered' => 'Entregado',
            'Cancelled' => 'Cancelado',
        );

        //datos para log de requests
        $this->inputs = Input::get();
        $this->http_method = Request::method();
        $this->response = array('status' => false, 'message' => 'Error generico');

        //registrar log de checkout web con variables iniciales
        $url = URL::current();
        $urls_log = array(
            //web_url().'/api/1.3/checkout',
            web_url().'/checkout',
            //web_url().'/api/1.3/coupon',
            web_url().'/coupon',
        );

        if ($this->http_method == 'POST' && in_array($url, $urls_log))
        {
            try{
                $response_headers = '';
                $request = $this->inputs;
                $request = json_encode($request);
                $request_headers = '';
    foreach (getallheaders() as $name => $value) {
          $request_headers .= '
  '.$name.': '.$value;
    }
            }catch(Exception $e){
                $request = is_string($request) ? $request : 'Error en json_encode: Invalid UTF-8 sequence in argument';
            }

            //encriptacion de tarjeta de credito
            if(isset($request['number_cc']) && !empty($request['number_cc'])){
                $request['number_cc'] = substr($request['number_cc'], 0, 6).'******'.substr($request['number_cc'], 12, 16);
                $request['code_cc'] = '***';
            }

            //guardar log
            $date = date('Y-m-d H:i:s');
            $log = new CheckoutLog;
            $log->url = $url;
            $request_string = "URL: ".$url." ".$this->http_method;
            $request_string .= "\r\nREQUEST HEADERS: ".$request_headers;
            $request_string .= "\r\nREQUEST: ".$request;
            $log->request = $request_string;
            $log->ip = get_ip();
            $log->created_at = $log->updated_at = $date;
            $log->save();
        }

        //solo admin
        if (is_admin_url())
        {
            $stores = DB::table( 'stores' )
                        ->select( 'city_id', 'latitude', 'longitude' )
                        ->where( 'status', 1 )
                        ->get();

            foreach ($stores as $store) {
                $this->config['merqueo_location'][$store->city_id] = [
                    'lat' => $store->latitude,
                    'lng' => $store->longitude
                ];
            }
        }

        //solo pagina web
        $exceptions = array('api', 'feed');
        if (!in_array(Request::segment(1), $exceptions) && !is_admin_url())
        {
            $this->user_id = Auth::check() ? Auth::user()->id : 0;

            //pasar variables a layouts
            View::composer(array('layout', 'layout_home', 'layout_page', 'index'), function($view){
                if (Config::get('app.debug')){
                    $robots = 'noindex,nofollow';
                }else{
                    if (strpos(Route::getCurrentRoute()->getPath(), 'checkout') === false){
                       $robots = 'index,follow';
                    }else $robots = 'noindex,nofollow';
                }
                $view->with('robots', $robots);
            });

            //guardar en sesion datos de campaña
            if (Input::has('utm_source')){
                $campaign_log = array(
                    'source' => Input::get('utm_source'),
                    'campaign' => Input::get('utm_campaign'),
                    'medium' => Input::get('utm_medium')
                );
                Session::put('campaign_log', $campaign_log);
                Session::save();
            }

            //cargar ciudad de cookie
            if (Cookie::get('city_id'))
                $city_id = Cookie::get('city_id');

            //no hay cookie entonces se obtiene ciudad por ip
            if (!isset($city_id) /*&& !$city_id = $this->getCityIdByIp()*/){
                $city_id = 1; //bogota por defecto
                Cookie::queue('city_id', $city_id, 2628000);
                Session::put('choose_city', true);
            }
            Session::put('city_id', $city_id);
        }
    }

    /**
     * Obtiene la bodega actual dependiento de la ubicacion del cliente por zona
     *
     * @param int $store_id ID d ela tienda (opcional)
     * @return Warehouse
     */
    public function getCurrentWarehouse($store_id = null)
    {
        $isApiRequest = $this->isApiEndpoint();
        $cityId = $this->getCurrentCity();

        $zoneId = $isApiRequest
            ? Input::get('zone_id')
            : (Session::get('zone_id') ?: Input::get('zone_id'));

        if (is_numeric($zoneId) && $zoneId) {

            $warehouse = Zone::findOrFail($zoneId)->warehouse;

            if (!$isApiRequest) {
                Session::put('zone_id', $zoneId);
                Session::put('warehouse_id', $warehouse->id);
            }
            if (empty($cityId) || $warehouse->city_id == $cityId) {
                return $warehouse;
            }
        }

        $warehouseQuery = Warehouse::select('warehouses.*')
            ->whereHas('zones', function ($query) {
                $query->where('status', 1);
            })
            ->where('warehouses.status', 1);
        if (!empty($cityId)) {
            $warehouseQuery->where('city_id', $cityId);
        } else if (!empty($store_id)) {
            $warehouseQuery->join('stores', 'stores.city_id', '=', 'warehouses.city_id')
                ->where('stores.id', $store_id)
                ->where('stores.status', 1);
        }

        $warehouse = $warehouseQuery->first();

        if (!$warehouse){
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException("Recurso no encontrado");
        }

        $zone = $warehouse->zones()
            ->where('status', 1)
            ->first();
        Session::put('zone_id', $zone->id);
        Session::put('warehouse_id', $warehouse->id);

        return $warehouse;
    }

    /**
     * Obtiene la zona actual
     *
     * @param $store_id
     * @return null|Zone
     */
    public function getCurrentZone($store_id)
    {
        $zoneId = $this->isApiEndpoint()
            ? Input::get('zone_id')
            : (Session::get('zone_id') ?: Input::get('zone_id'));

        if ($zoneId) {
            return Zone::find($zoneId);
        }

        $warehouse = $this->getCurrentWarehouse($store_id);

        return $warehouse->zones()
            ->where('status', 1)
            ->first();
    }

    /**
     * Retorna el identificador de la ciudad a la cual se hace
     * referencia en la petición actual.
     *
     * @return int|null
     */
    public function getCurrentCity()
    {
        $is_api_request = Request::is('api/*');

        if ($is_api_request) {
            return Input::get('city_id');
        }

        $city_slug = \Route::getCurrentRoute()->getParameter('city_slug');

        if (!$city_slug && Cookie::has('city_id')) {
            return Cookie::get('city_id');
        }

        $city = City::where('slug', $city_slug)->first();

        if (empty($city)) {
            return null;
        }

        $city_id = $city->id;

        if (!$is_api_request) {
            Cookie::queue('city_id', $city->id, 2628000);
            Session::put('city_id', $city->id);
        }

        return $city_id;
    }

    /**
     * Obtiene el id de la ciudad de la tienda que cubre la ciudad enviada
     *
     * @param int $city_id ID de la ciudad
     * @return int $city_id ID de la ciudad padre
     */
    public function getCoverageStoreCityId($city_id)
    {
        if ($store = City::select('stores.city_id')->join('stores', 'coverage_store_id', '=', 'stores.id')->where('cities.id', $city_id)->first())
            return $store->city_id;
        return $city_id;
    }

    /**
     * Obtiene la ciudad por IP publica
     *
     * @return int $city_id Slug de la ciudad
     */
    public function getCityIdByIp()
    {
        if (isset($_SERVER['REMOTE_ADDR'])){
            $region = @geoip_record_by_name('200.116.3.218');
            if ($region && isset($region['region']) && !empty($region['region'])) {
                if ($region['region'] == '02')
                    $city_slug = 'medellin';
                if (isset($city_slug) && $city = City::where('slug', $city_slug)->first())
                    return $city->id;
            }
        }

        return false;
    }

    /**
     * Obtiene datos de footer
     *
     * @return array $footer Datos del footer
     */
    public function getFooter()
    {
        if (!Route::current() || !Route::current()->parameter('store_slug') || !isset($this->store->id))
        {
            $this->store = Store::find(63);
        }
        //enlaces de categorias de tienda
        $departments = Department::select('departments.id', 'departments.name', 'departments.slug')
        ->join('store_products', 'store_products.department_id', '=', 'departments.id')
        ->join('products', 'products.id', '=', 'store_products.product_id')->where('departments.store_id', $this->store->id)
        ->where('departments.status', 1)->groupBy('departments.id')
        ->orderBy('departments.sort_order')->orderBy('departments.name')->limit(4)->get();
        foreach($departments as $department){
            $shelves = Shelf::select('shelves.id', 'shelves.name', 'shelves.slug')
            ->join('store_products', 'shelves.id', '=', 'store_products.shelf_id')->where('shelves.department_id', $department->id)
            ->where('shelves.status', 1)->groupBy('shelves.id')->orderBy('shelves.sort_order')->orderBy('shelves.name')->limit(4)->get();
            if (count($shelves)){
                foreach($shelves as $shelf){
                    $sublinks[] = (object) array(
                        'url' => web_url().'/'.$this->store->city_slug.'/domicilios-'.$this->store->slug.'/'.$department->slug.'/'.$shelf->slug,
                        'name' => $shelf->name
                    );
                }
                $links = array(
                    'url' => web_url().'/'.$this->store->city_slug.'/domicilios-'.$this->store->slug.'/'.$department->slug,
                    'name' => $department->name,
                    'sublinks' => $sublinks
                );
                $footer['links']['store'][] = (object) $links;
                unset($sublinks);
            }
        }

        $links = array(
            'contact_phone' => Config::get('app.admin_phone'),
            'android_url' => Config::get('app.android_url'),
            'ios_url' => Config::get('app.ios_url'),
            'facebook_url' => Config::get('app.facebook_url'),
            'twitter_url' => Config::get('app.twitter_url'),
            'instagram_url' => ''
        );
        $footer['links']['info'] = $links;

        $footer['cities'] = City::select('id', 'city', 'slug')
            ->whereHas('stores', function ($query) {
                $query->where('status', 1);
            })
            ->where('status', 1)
            ->where('is_main', 1)
            ->get();
        return $footer;
    }

    /**
     * Determina si la fecha de entrega es valida.
     *
     * @param Zone $zone
     * @param string $deliveryDate
     * @param DeliveryWindow|null $deliveryWindow
     * @return bool
     */
    protected static function hasValidDeliveryTime(Zone $zone, $deliveryDate, DeliveryWindow $deliveryWindow = null)
    {
        if ($deliveryWindow
            && in_array($deliveryWindow->shifts, [DeliveryWindow::SHIFT_EXPRESS, DeliveryWindow::SHIFT_FAST_DELIVERY])
            && $deliveryDate == date('Y-m-d')
        ) {
            $currentDate = Carbon::now();
            $hourStart =  Carbon::parse($deliveryWindow->hour_start);
            $hourEnd =  Carbon::parse($deliveryWindow->hour_end);
            $validOnSameDay = $currentDate->between($hourStart, $hourEnd);
        } else {
            $validOnSameDay = $deliveryDate == date('Y-m-d') && $zone->canShowSameDay();
        }

        $validOnNextDay = $deliveryDate == date('Y-m-d', strtotime('+1 day'))
            && date('H:i:s') < DeliveryWindow::START_PLANNING;
        $validateOnHour23 = $deliveryDate == date('Y-m-d', strtotime('+1 day'))
            && date('H:i:s') >= DeliveryWindow::START_PLANNING
            && $deliveryWindow
            && $deliveryWindow->shifts == DeliveryWindow::SHIFT_SAME_DAY;
        $validOtherDay = $deliveryDate >= date('Y-m-d', strtotime('+2 day'));

        return $validOnSameDay || $validOnNextDay || $validOtherDay || $validateOnHour23;
    }
    /**
     * Agrega a la sesión información para generar un evento.
     *
     * @param array $data
     */
    public static function trackEventOnBrowser(array $data)
    {
        $session = Session::get('events') ?: [];
        Session::put('events', array_merge($data, $session));
    }

    /**
     * @return bool
     */
    public function isApiEndpoint()
    {
        return Request::is('api/*');
    }

    /**
     * @return bool
     */
    protected function applyFivePercentDiscount()
    {
        return app(SpecialPriceService::class)
            ->shouldApplyFiveSpecialDiscount();
    }
}
