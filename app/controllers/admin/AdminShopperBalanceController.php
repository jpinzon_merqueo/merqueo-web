<?php
namespace admin;

use Config, Request, Input, View, Session, Redirect, Response, DB, Shopper, ShopperMovement, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007, OrderStore;

class AdminShopperBalanceController extends AdminController
{
    /**
     * Grilla de shoppers
     */
    public function index()
    {
        if (!Request::ajax()){
            return View::make('admin.shoppers.balance.index')->with('title', 'Administrar Saldos de Shoppers');
        }else{

            $search = Input::has('s') ? Input::get('s') : '';

            $shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
            ->where(function($query) use ($search){
                $query->where(DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%'.$search.'%');
                $query->orWhere('identity_number', 'LIKE', '%'.$search.'%');
                $query->orWhere('phone', 'LIKE', '%'.$search.'%');
            })/*->where('shoppers.status', 1)*/->select('shoppers.*', 'city');

            if (Session::get('admin_designation') != 'Super Admin')
                $shoppers->where('shoppers.city_id', Session::get('admin_city_id'));

            $shoppers = $shoppers->orderBy('first_name','asc')->limit(200)->get();

            return View::make('admin.shoppers.balance.index')
                    ->with('title', 'Shoppers')
                    ->with('shoppers', $shoppers)
                    ->renderSections()['content'];
        }
    }

	/**
     * Administrar saldos de shopper
     */
    public function edit($id)
    {
        if (!$shopper = Shopper::find($id))
           return Redirect::back()->with('type', 'failed')->with('message', 'El shopper no existe.');

        $movements = ShopperMovement::select('shopper_movements.*',
                                    DB::raw("IF(shopper_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
                                    DB::raw("DATE_FORMAT(shopper_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"), 'orders.payment_method AS user_payment_method',
                                    DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"),
                                    DB::raw("SUM(total_real_amount) AS total_real_amount"), 'shopper_payment_method', 'supermarket')
                                    ->leftJoin('admin', 'shopper_movements.admin_id', '=', 'admin.id')
                                    ->leftJoin('orders', 'shopper_movements.order_id', '=', 'orders.id')
                                    ->leftJoin('order_stores', function($join){
                                         $join->on('order_stores.order_id', '=', 'orders.id');
                                         $join->on('shopper_movements.status', '=', DB::raw('1'));
                                    })
                                    ->join('shoppers', 'shopper_movements.shopper_id', '=', 'shoppers.id')
                                    ->where('shopper_movements.shopper_id', $shopper->id)
                                    ->groupBy('shopper_movements.id')->groupBy('shopper_movements.order_id')->groupBy('shopper_movements.status')->orderBy('shopper_movements.created_at', 'desc')
                                    ->get();
        foreach($movements as $movement){
            $order_stores = OrderStore::select('order_stores.shopper_payment_method', 'stores.name AS store_name')
                    ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $movement->order_id)->get();
            if (count($order_stores) > 1){
                $shopper_payment_method = '';
                foreach($order_stores as $order_store){
                    $shopper_payment_method .= $order_store->store_name.': '.$order_store->shopper_payment_method.' ';
                }
                $movement->shopper_payment_method = trim($shopper_payment_method);
            }else{
                if (count($order_stores) == 1){
                    $order_store = $order_stores->toArray();
                    $movement->shopper_payment_method = $order_store[0]['shopper_payment_method'];
                }
            }
        }

        $movements_html = View::make('admin.shoppers.balance.movements')->with('movements', $movements)->render();

        $balance = Shopper::getBalance($shopper->id);

        return View::make('admin.shoppers.balance.form')
                ->with('title', 'Administrar Saldo de Shopper')
                ->with('shopper', $shopper)
                ->with('balance', $balance)
                ->with('movements_html', $movements_html);
    }

    /**
     * Guardar movimiento de tarjeta a shopper
     */
    public function save_movement_ajax()
    {
        $shopper = Shopper::find(Input::get('shopper_id'));
        if (!$shopper)
            return Response::json(array('status' => false, 'message' => 'Shopper no existe'), 200);

        if (!Input::has('id'))
            $shopper_movement = new ShopperMovement;

        $shopper_movement->admin_id = Session::get('admin_id');
        $shopper_movement->shopper_id = $shopper->id;
        $shopper_movement->movement = Input::get('movement');
        $amount = intval(str_replace ( '.', '', Input::get('amount')));
        if ($shopper_movement->movement == 'Recarga en tarjeta')
            $shopper_movement->card_balance = $amount;
        if ($shopper_movement->movement == 'Deducción')
            $shopper_movement->shopper_balance = $amount;
        if ($shopper_movement->movement == 'Abono en efectivo')
            $shopper_movement->shopper_balance = $amount * -1;
        $shopper_movement->description = Input::get('description');
        $shopper_movement->save();
        $this->admin_log('shopper_movements', $shopper_movement->id, 'insert');

       $movements = ShopperMovement::select('shopper_movements.*', DB::raw("IF(shopper_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
                                    DB::raw("DATE_FORMAT(shopper_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"), 'orders.payment_method AS user_payment_method',
                                    DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"),
                                    DB::raw("SUM(total_real_amount) AS total_real_amount"), 'shopper_payment_method', 'supermarket')
                                    ->leftJoin('admin', 'shopper_movements.admin_id', '=', 'admin.id')
                                    ->leftJoin('orders', 'shopper_movements.order_id', '=', 'orders.id')
                                    ->leftJoin('order_stores', function($join){
                                         $join->on('order_stores.order_id', '=', 'orders.id');
                                         $join->on('shopper_movements.status', '=', DB::raw('1'));
                                    })
                                    ->join('shoppers', 'shopper_movements.shopper_id', '=', 'shoppers.id')
                                    ->where('shopper_movements.shopper_id', $shopper->id)
                                    ->groupBy('shopper_movements.order_id')->groupBy('shopper_movements.status')->orderBy('shopper_movements.created_at', 'desc')
                                    ->get();
        foreach($movements as $movement){
            $order_stores = OrderStore::select('order_stores.shopper_payment_method', 'stores.name AS store_name')
                    ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $movement->order_id)->get();
            if (count($order_stores) > 1){
                $shopper_payment_method = '';
                foreach($order_stores as $order_store){
                    $shopper_payment_method .= $order_store->store_name.': '.$order_store->shopper_payment_method.' ';
                }
                $movement->shopper_payment_method = trim($shopper_payment_method);
            }else{
                if (count($order_stores) == 1){
                    $order_store = $order_stores->toArray();
                    $movement->shopper_payment_method = $order_store[0]['shopper_payment_method'];
                }
            }
        }

        $balance = Shopper::getBalance($shopper->id);

        $response = array(
            'status' => true,
            'message' => 'Movimiento registrado con éxito.',
            'balance' => $balance,
            'html' => View::make('admin.shoppers.balance.movements')->with('movements', $movements)->render()
        );

        return Response::json($response, 200);
    }

/**
     * Elimina movimiento de shopper
     */
    public function delete_movement_ajax()
    {
        $shopper = Shopper::find(Input::get('shopper_id'));
        if (!$shopper)
            return Response::json(array('status' => false, 'message' => 'Shopper no existe'), 200);

        if (!Input::has('id'))
            return Response::json(array('status' => false, 'message' => 'ID es requerido'), 200);

        if (ShopperMovement::where('id', Input::get('id'))->where('shopper_id', $shopper->id)->where('movement', '<>', 'Gestión de pedido')->update(array('status' => 0)))
            $this->admin_log('shopper_movements', Input::get('id'), 'delete');

        $movements = ShopperMovement::select('shopper_movements.*', DB::raw("IF(shopper_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
                                    DB::raw("DATE_FORMAT(shopper_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"), 'orders.payment_method AS user_payment_method',
                                    DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"),
                                    DB::raw("SUM(total_real_amount) AS total_real_amount"), 'shopper_payment_method', 'supermarket')
                                    ->leftJoin('admin', 'shopper_movements.admin_id', '=', 'admin.id')
                                    ->leftJoin('orders', 'shopper_movements.order_id', '=', 'orders.id')
                                    ->leftJoin('order_stores', function($join){
                                         $join->on('order_stores.order_id', '=', 'orders.id');
                                         $join->on('shopper_movements.status', '=', DB::raw('1'));
                                    })
                                    ->join('shoppers', 'shopper_movements.shopper_id', '=', 'shoppers.id')
                                    ->where('shopper_movements.shopper_id', $shopper->id)
                                    ->groupBy('shopper_movements.order_id')->groupBy('shopper_movements.status')->orderBy('shopper_movements.created_at', 'desc')
                                    ->get();
        foreach($movements as $movement){
            $order_stores = OrderStore::select('order_stores.shopper_payment_method', 'stores.name AS store_name')
                    ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $movement->order_id)->get();
            if (count($order_stores) > 1){
                $shopper_payment_method = '';
                foreach($order_stores as $order_store){
                    $shopper_payment_method .= $order_store->store_name.': '.$order_store->shopper_payment_method.' ';
                }
                $movement->shopper_payment_method = trim($shopper_payment_method);
            }else{
                if (count($order_stores) == 1){
                    $order_store = $order_stores->toArray();
                    $movement->shopper_payment_method = $order_store[0]['shopper_payment_method'];
                }
            }
        }

        $balance = Shopper::getBalance($shopper->id);

        $response = array(
            'status' => true,
            'message' => 'Movimiento eliminado con éxito.',
            'balance' => $balance,
            'html' => View::make('admin.shoppers.balance.movements')->with('movements', $movements)->render()
        );

        return Response::json($response, 200);
    }

    /**
     * Exportar a excel saldos de shopper
     */
    public function export_movement_ajax()
    {
        $shopper = Shopper::find(Input::get('shopper_id'));
        if (!$shopper)
            return Response::json(array('status' => false, 'message' => 'Shopper no existe'), 200);

        $movements = ShopperMovement::select(
                    DB::raw("shopper_movements.movement AS 'Tipo movimiento'"),
                    DB::raw(" IF(shopper_movements.order_id IS NOT NULL, shopper_movements.order_id,IF(order_stores.order_id IS NOT NULL ,order_stores.order_id, orders.id)) AS Pedido"),
                    DB::raw(" order_stores.supermarket AS 'Comprado en'"),
                    DB::raw(" orders.payment_method AS 'Metodo de pago Cliente'"),
                    DB::raw(" shopper_movements.user_card_paid AS 'Cliente Pago con tarjeta'"),
                    DB::raw(" shopper_movements.user_cash_paid AS 'Cliente Pago con efectivo'"),
                    DB::raw(" order_stores.shopper_payment_method AS 'Metodo de pago Shopper'"),
                    DB::raw(" (orders.total_amount + orders.delivery_amount - orders.discount_amount) AS 'Total pedido'"),
                    DB::raw(" total_real_amount AS 'Total factura'"),
                    DB::raw(" shopper_movements.shopper_balance AS 'Saldo shopper'"),
                    DB::raw(" shopper_movements.card_balance AS 'Saldo tarjeta'"),
                    DB::raw(" shopper_movements.description AS 'Observacion'"),
                    DB::raw(" shopper_movements.created_at AS Fecha"),
                    DB::raw("IF(order_stores.admin_id IS NULL, IF(shopper_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname), admin.fullname) AS 'Hecho por'"),
                    DB::raw(" IF(shopper_movements.status = 1,'Activo','Inactivo') AS Estado"))
                    ->leftJoin('admin', 'admin_id', '=', 'admin.id')
                    ->leftjoin('orders', 'order_id', '=', 'orders.id')
                    ->leftjoin('order_stores', 'order_stores.order_id', '=', 'orders.id')
                    ->join('shoppers', 'shopper_movements.shopper_id', '=', 'shoppers.id')
                    ->where('shopper_movements.shopper_id', $shopper->id)
                    ->groupBy('shopper_movements.id', 'shopper_movements.order_id', 'shopper_movements.status')
                    ->orderBy('shopper_movements.created_at', 'desc')
                    ->get()->toArray();

        if ($n = count($movements))
        {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)){
                while (false !== ($file = readdir($gestor))){
                    if (strstr($file, Session::get('admin_username')))
                        remove_file($path.$file);
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Movimientos');
            $objPHPExcel->getProperties()->setSubject('Movimientos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb'=>'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:AR1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($movements[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach($movements as $row){
                $i = 0;
                foreach ($row as $key => $value){
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    $i++;
                }
                $j++;
            }

             //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Movimientos');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        }

        $response = array(
            'status' => true,
            'message' => 'Movimientos exportados.',
            'file_url' => $url
        );

        return Response::json($response, 200);
    }

     /**
     * Exportar a excel saldos de shopper
     */
    public function export_balance_ajax()
    {
        $balances = Shopper::select(
                    DB::raw(" CONCAT(shoppers.first_name, ' ', shoppers.last_name) AS SHOPPER"),
                    DB::raw(" SUM(shopper_movements.shopper_balance) AS 'SALDO SHOPPER'"),
                    DB::raw(" SUM(shopper_movements.card_balance) AS 'SALDO TARJETA'"))
                    ->leftJoin(DB::raw('(shopper_movements)'), function($join){
                         $join->on('shopper_movements.shopper_id', '=', 'shoppers.id');
                         $join->on('shopper_movements.status', '=', DB::raw('1'));
                    })
                    ->where('shoppers.status', 1)
                    ->orderBy('SHOPPER', 'ASC')
                    ->groupBy('shoppers.id')
                    ->get()->toArray();


        if ($n = count($balances))
        {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)){
                while (false !== ($file = readdir($gestor))){
                    if (strstr($file, Session::get('admin_username')))
                        remove_file($path.$file);
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Movimientos');
            $objPHPExcel->getProperties()->setSubject('Movimientos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb'=>'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($balances[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach($balances as $row){
                $i = 0;
                foreach ($row as $key => $value){
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    $i++;
                }
                $j++;
            }

             //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Saldos de Shoppers');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        }

        $response = array(
            'status' => true,
            'message' => 'Saldos exportados.',
            'file_url' => $url
        );

        return Response::json($response, 200);
    }

}
