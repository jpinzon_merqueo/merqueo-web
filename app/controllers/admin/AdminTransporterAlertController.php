<?php
namespace admin;

use Request, View, Input, Redirect, Session, DB, Str, Validator, Carbon\Carbon, Queue,
    Store, City, Department, Shelf, Product, Slot, Response, Config,
    Supermarket, StoreBranchSlot, Holidays, TransporterAlert;

class AdminTransporterAlertController extends AdminController {

	protected $app_key = null;
	protected $app_cluster = null;

	public function __construct()
	{
		parent::__construct();
		$this->app_key = Config::get('app.pusherer.transporter.key');
		// $this->app_cluster = Config::get('app.pusherer.transporter.cluster');
		$this->app_cluster = 'mt1';
	}

	/**
	 * Muestra la grilla con las alertas.
	 */
	public function index()
	{
		$cities = $this->get_cities();
		$reasons = TransporterAlert::getReasons();
		$statuses = TransporterAlert::getStatus();
		$data = [
			'title' => 'Alertas de Transportadores',
			'app_key' => $this->app_key,
			'app_cluster' => $this->app_cluster,
			'reasons' => $reasons,
			'statuses' => $statuses,
			'cities' => $cities
		];
		return View::make('admin.transporter_alert.index', $data);
	}

	public function resolved()
	{
		$cities = $this->get_cities();
		$reasons = TransporterAlert::getReasons();
		$statuses = TransporterAlert::getStatus();
		$data = [
			'title' => 'Alertas de Transportadores',
			'cities' => $cities,
			'reasons' => $reasons,
			'statuses' => $statuses
		];
		return View::make('admin.transporter_alert.resolved', $data);
	}

	/**
	 * Obtiene las alertas por status
	 */
	public function get_alerts_ajax()
	{
		$alerts = TransporterAlert::leftJoin('admin', 'transporter_alerts.admin_id', '=', 'admin.id')
									->join('vehicles', 'transporter_alerts.vehicle_id', '=', 'vehicles.id')
									->join('vehicle_drivers', 'vehicle_drivers.vehicle_id', '=', 'vehicles.id')
									->join('drivers', 'vehicle_drivers.driver_id', '=', 'drivers.id')
									->join('transporters', 'drivers.transporter_id', '=', 'transporters.id')
									->join('cities', 'transporters.city_id', '=', 'cities.id')
									->select(
										'transporter_alerts.id AS alert_id',
										'vehicles.plate',
										'drivers.cellphone',
										'admin.fullname AS admin_fullname',
										'transporter_alerts.observation',
										DB::raw('CONCAT(drivers.first_name, drivers.last_name) AS fullname'),
										'transporter_alerts.reason',
										'transporter_alerts.status',
										'transporter_alerts.created_at',
										'transporter_alerts.order_id',
										'cities.city',
										'cities.id AS city_id'
									);

		if  ( Input::has('driver_id') ) {
			$driver_id = Input::get('driver_id');
			$alerts = $alerts->where('drivers.id', $driver_id);
		}
		if ( Input::has('plate') ) {
			$plate = Input::get('plate');
			$alerts = $alerts->where('vehicles.plate', $plate);
		}
		if ( Input::has('city_id') ) {
			$city_id = Input::get('city_id');
			$alerts = $alerts->where('transporters.city_id', $city_id);
		}else{
			$city_id = Session::get('admin_city_id');
			$alerts = $alerts->where('transporters.city_id', $city_id);
		}
		if ( Input::has('status') ) {
			$status = Input::get('status');
			$alerts = $alerts->where('transporter_alerts.status', $status);
			$data['status'] = $status;
		}
		$alerts = $alerts->orderBy('transporter_alerts.created_at', 'DESC')->orderBy('alert_id', 'DESC')->paginate(20);

        $data['links_html'] = $alerts->links()->render();
        $data['products_count'] = $alerts->count();
        $data['products_total'] = $alerts->getTotal();
		$data['alerts'] = $alerts;

		if ( $status == 'Pendiente' ) {
			$json['html'] = View::make('admin.transporter_alert.index', $data)->renderSections()['alerts'];
			$json['count'] = count($alerts);
			return Response::json($json);
		}else{
			return View::make('admin.transporter_alert.resolved', $data)->renderSections()['alerts'];
		}

	}

	/**
	 *
	 * Guarda las observaciones de las alertas y las cierra como resueltas
	 */
	public function save_observation()
	{
		if ( Input::has('observation') && Input::has('alert_id') ) {
			$json_array = json_decode(Input::get('alert_id'));
			if ( is_array($json_array) ) {
				foreach ($json_array as $key => $value) {
					$alert = TransporterAlert::find( $value );
					$alert->status = 'Resuelto';
					$alert->observation = Input::get('observation');
					$alert->admin_id = Session::get('admin_id');
					$alert->save();
				}
				return Redirect::back()->with('success', 'Se han guardado correctamente los datos de la alertas.');
			}else{
				$alert = TransporterAlert::find( Input::get('alert_id') );
				$alert->status = 'Resuelto';
				$alert->observation = Input::get('observation');
				$alert->admin_id = Session::get('admin_id');
				$alert->save();
			}

			return Redirect::back()->with('success', 'Se han guardado correctamente los datos de la alerta.');
		}else
			return Redirect::back()->with('error', 'Ha ocurrido un error al guardar los datos de la alerta. Por favor revise el campo observaciones.');
	}
}