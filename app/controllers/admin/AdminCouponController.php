<?php

namespace admin;

use function foo\func;
use Illuminate\Support\Str;
use Request, Input, View, Session, Redirect, Response, Validator, DB, Config, Coupon, Order, City, Store, Department, Shelf, StoreProduct;
use Zend\Validator\Db\AbstractDb;

class AdminCouponController extends AdminController
{

    /**
     * Grilla de cupones
     */
    public function index()
    {
        $admin_permissions = json_decode(Session::get('admin_permissions'), true);
        if (!Request::ajax()) {
            return View::make('admin.coupons.index')
                        ->with('title','Cupones')
                        ->with('permission_insert', $admin_permissions['admincoupon']['permissions']['insert']);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            $coupons = Coupon::select('coupons.*', 'admin.fullname')
                ->where('visible', 1)
                ->leftJoin('admin', 'admin.id', '=', 'coupons.admin_id')
                ->where('coupons.code', 'LIKE', '%'.$search.'%')
                ->orderBy('coupons.created_at', 'desc');

            if (Session::get('admin_designation') != 'Super Admin')
                $coupons = $coupons->where('admin_id', Session::get('admin_id'));

            $coupons = $coupons->limit(80)->get();

            if ($coupons) {
                foreach($coupons as $coupon) {
                    $coupon->qty_uses = Order::join('user_credits', 'user_credits.order_id', '=', 'orders.id')
                                ->where('orders.status', 'Delivered')
                                ->where('user_credits.coupon_id', $coupon->id)
                                ->where('user_credits.type', 0)
                                ->where('user_credits.status', 1)
                                ->groupBy('group_id')->get()->count();
                }
            }
            //Permisos del usuario para acceder a las acciones
            return View::make('admin.coupons.index')
                        ->with('permission_update', $admin_permissions['admincoupon']['permissions']['update'])
                        ->with('permission_delete', $admin_permissions['admincoupon']['permissions']['delete'])
                        ->with('coupons', $coupons)->renderSections()['content'];
        }
    }

    /**
     * Editar cupon
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Cupón' : 'Nuevo Cupón';
        $coupon = Coupon::find($id);
        $store = $stores = $departments = $shelves = $store_products = array();
        if (!empty($coupon->store_id)){
            $store = Store::find($coupon->store_id);
            $stores = Store::select('id', 'name')->where('city_id', $store->city_id)->where('status', 1)->get();
            $departments = Department::select('id', 'name')->where('store_id', $coupon->store_id)->where('status', 1)->orderBy('departments.name', 'ASC')->get();
            $shelves = Shelf::select('id', 'name')->where('department_id', $coupon->department_id)->where('status', 1)->orderBy('name', 'ASC')->get();
            $store_products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                                ->select('store_products.id', 'products.name')->where('store_products.shelf_id', $coupon->shelf_id)->where('store_product_warehouses.status', 1)->orderBy('products.name', 'ASC')->get();
        }
        $payment_methods = $this->config['payment_methods_names'];
        return View::make('admin.coupons.coupon_form')
                ->with('title', 'Cupón')
                ->with('sub_title', $sub_title)
                ->with('coupon', $coupon)
                ->with('config', $this->config)
                ->with('payment_methods', $payment_methods)
                ->with('store', $store)
                ->with('cities', $this->get_cities())
                ->with('stores', $stores)
                ->with('departments', $departments)
                ->with('shelves', $shelves)
                ->with('store_products', $store_products);
    }

    /**
     * Guardar cupon
     */
    public function save()
    {
        $id = Input::get('id');
        $coupon = Coupon::find($id);
        if(!$coupon){
            $coupon = new Coupon;
            $action = 'insert';
        }else $action = 'update';
        $coupon->code = Input::get('code');
        $coupon->type = Input::get('type');
        $coupon->amount = Input::get('amount');
        $minimum_order_amount = Input::get('minimum_order_amount');
        $maximum_order_amount = Input::get('maximum_order_amount');
        $minimum_order_amount = !empty($minimum_order_amount) ? $minimum_order_amount : 0;
        $maximum_order_amount = !empty($maximum_order_amount) ? $maximum_order_amount : 0;
        $coupon->minimum_order_amount = $minimum_order_amount;
        $coupon->maximum_order_amount = $maximum_order_amount;
        $coupon->number_uses = Input::get('number_uses');
        $coupon->type_use = Input::get('type_use');
        $coupon->redeem_on = Input::get('redeem_on');
        if (Input::get('redeem_on') == 'Specific Store'){
            $coupon->store_id = Input::get('store_id') ? Input::get('store_id') : null;
            $coupon->department_id = Input::get('department_id') ? Input::get('department_id') : null;
            $coupon->shelf_id = Input::get('shelf_id') ? Input::get('shelf_id') : null;
            $coupon->store_product_id = Input::get('store_product_id') ? Input::get('store_product_id') : null;
        }else{
            $coupon->store_id = null;
            $coupon->department_id = null;
            $coupon->shelf_id = null;
            $coupon->store_product_id = null;
        }
        $coupon->payment_method = Input::get('payment_method');
        $coupon->cc_bin = Input::get('cc_bin');
        $coupon->cc_bank = Input::get('cc_bank');

        $expiration_date = format_date('mysql', Input::get('expiration_date'));
        if ($expiration_date < date('Y-m-d'))
            return Redirect::route('adminCoupons.index')->with('type', 'failed')->with('message', 'La fecha de expiración debe ser mayor a la actual.');
        $coupon->expiration_date = $expiration_date;
        $coupon->campaign_validation = Input::get('campaign_validation');
        $coupon->status = Input::get('status');
        $coupon->admin_id = Session::get('admin_id');

        //verifica que el codigo no exista
        $validator = Validator::make(
                    array( 'code' => $coupon->code ),
                    array( 'code' => 'required|unique:coupons,code,'.$coupon->id )
        );

        if ($validator->fails()) {
            $message = 'El código del cupón ya existe.';
            $type = 'failed';
        }else{
            $coupon->save();
            $this->admin_log('coupons', $coupon->id, $action);
            $message = 'Cupón guardado con éxito.';
            $type = 'success';
        }

        return Redirect::route('adminCoupons.index')->with('type', $type)->with('message', $message);
    }

    /**
     * Elimina cupon
     */
    public function delete()
    {
        $id = Request::segment(4);
        $coupon = Coupon::find($id);
        Coupon::where('id', $id)->update(array('status' => 0));
        $this->admin_log('coupons', $coupon->id, 'delete');

        return Redirect::route('adminCoupons.index')->with('type', 'success')->with('message', 'Cupón eliminado con éxito.');
    }

    /**
     * Muestra grilla para cupones con permisos de administrador
     */
    public function index_manager()
    {
        $data['title'] = 'Administrador de Cupones';
        $campaign_validation = Coupon::distinct('campaign_validation')->select('campaign_validation')->get();
        $campaign_validation->each(function ($campaign) {
            if (is_null($campaign->campaign_validation)) {
                $campaign->campaign_validation = 'NULL';
            }
            if ($campaign->campaign_validation == '') {
                $campaign->campaign_validation = 'Campaña sin nombre';
            }
        });
        $data['campaign_validation'] = $campaign_validation;
        return View::make('admin.coupons.manager.index', $data);
    }

    /**
     * Buscador de cupones
     * @return mixed
     */
    public function search_cupons()
    {
        $code = trim(Input::get('code'));
        $campaign_validation = trim(Input::get('campaign_validation'));
        $status = Input::get('status');
        $used = Input::get('used');

        $coupons = Coupon::leftJoin('admin', 'coupons.admin_id', '=', 'admin.id')
            ->leftJoin('user_credits', 'coupons.id', '=', 'user_credits.coupon_id')
            ->leftJoin('users', 'user_credits.user_id', '=', 'users.id')
            ->select(
                'coupons.code',
                'admin.fullname',
                'coupons.type',
                'coupons.type_use',
                'coupons.amount',
                DB::raw('SUM( IF(user_credits.order_id IS NOT NULL, 1, 0) ) AS uses_number'),
                'coupons.campaign_validation',
                DB::raw("DATE_FORMAT(coupons.expiration_date, '%d/%m/%Y') AS expiration_date"),
                'coupons.status'
            );

        if (!empty($code)) {
            $coupons = $coupons->where('coupons.code', $code);
        }
        if (!empty($campaign_validation)) {
            if ($campaign_validation == 'NULL') {
                $coupons = $coupons->orWhereNull('coupons.campaign_validation');
            } else if ($campaign_validation == 'Campaña sin nombre'){
                $coupons = $coupons->orWhere('coupons.campaign_validation', '');
            } else {
                $coupons = $coupons->orWhere('coupons.campaign_validation', $campaign_validation);
            }
        }
        if (!empty($status)) {
            if ($status == 'activo') {
                $coupons = $coupons->where('coupons.status', 1);
            } else {
                $coupons = $coupons->where('coupons.status', 0);
            }
        }
        if (!empty($used)) {
            if ($used == 'unused') {
                $coupons = $coupons->having('uses_number', '=', 0);
            } else {
                $coupons = $coupons->having('uses_number', '>', 0);
            }
        }

        $coupons = $coupons->groupBy('coupons.id')
            ->paginate(100);
        $data = $coupons->toArray();
        $data['code'] = $code;
        $data['campaign_validation'] = $campaign_validation;

        return Response::json($data);
    }

    /**
     * Borra cupónes no usados de la campaña seleccionada
     * @return mixed
     */
    public function delete_unused_coupons()
    {
        $campaign_validation = Input::get('campaign_validation');

        DB::beginTransaction();
        try {
            $coupons = Coupon::leftJoin('user_credits', 'coupons.id', '=', 'user_credits.coupon_id');
            if ($campaign_validation == 'NULL') {
                $coupons = $coupons->whereNull('coupons.campaign_validation');
            } else if ($campaign_validation == 'Campaña sin nombre'){
                $coupons = $coupons->where('coupons.campaign_validation', '');
            } else {
                $coupons = $coupons->where('coupons.campaign_validation', $campaign_validation);
            }
            $coupons = $coupons->whereNull('user_credits.id')
                ->groupBy('coupons.id')
                ->delete();

            $data['coupons'] = $coupons;
            /*DB::rollback();*/
            DB::commit();
        } catch (Exception $e) {
            $data['coupons'] = 0;
            $data['error'] = $e->getMessage();
            DB::rollback();
            return Response::json($data);
        }

        return Response::json($data);
    }

    /**
     * Muestra formulario para el creador masivo de cupones
     * @return mixed
     */
    public function massive_creator_index()
    {
        $data['title'] = 'Creador masivo de cupones';
        $data['cities'] = $this->get_cities();

        return View::make('admin.coupons.manager.index-massive', $data);
    }

    /**
     * Obtiene las tiendas por ciudad
     * @return mixed
     */
    public function get_stores_ajax()
    {
        $city_id = Input::get('city_id');

        if (!empty($city_id)) {
            $stores = Store::where('city_id', $city_id)
                ->where('status', 1)
                ->select(
                    'id',
                    'name'
                )
                ->get();

            return Response::json($stores);
        }
        return Response::json([]);
    }

    /**
     * Obtiene los departamentos por tienda
     * @return mixed
     */
    public function get_departments_ajax()
    {
        $store_id = Input::get('store_id');

        if (!empty($store_id)) {
            $departments = Department::where('store_id', $store_id)
                ->where('status', 1)
                ->select(
                    'id',
                    'name'
                )
                ->get();

            return Response::json($departments);
        }
        return Response::json([]);
    }

    /**
     * Obtiene los pasillos por departamento y tienda
     * @return mixed
     */
    public function get_shelves_ajax()
    {
        $store_id = Input::get('store_id');
        $department_id = Input::get('department_id');

        if (!empty($department_id)) {
            $shelves = Shelf::where('status', 1)
                ->where('store_id', $store_id)
                ->where('department_id', $department_id)
                ->select(
                    'id',
                    'name'
                )
                ->get();

            return Response::json($shelves);
        }
        return Response::json([]);
    }

    /**
     * Obtiene los productos por tienda, departamento y pasillo
     * @return mixed
     */
    public function get_products_ajax()
    {
        $store_id = Input::get('store_id');
        $department_id = Input::get('department_id');
        $shelf_id = Input::get('shelf_id');

        if (!empty($store_id) && !empty($department_id) && !empty($shelf_id)) {
            $products = StoreProduct::where('store_id', $store_id)
                ->where('department_id', $department_id)
                ->where('shelf_id', $shelf_id)
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->select(
                    'store_products.id',
                    'products.name',
                    'products.quantity',
                    'products.unit'
                )
                ->get();

            return Response::json($products);
        }
        return Response::json([]);
    }


    public function create_massive_coupons()
    {
        ini_set('memory_limit', '2024M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $type = Input::get('type');
        $amount = Input::get('amount');

        $maximum_order_amount = Input::get('maximum_order_amount', 0);
        $maximum_order_amount = !empty($maximum_order_amount) ? $maximum_order_amount : 0;

        $minimum_order_amount = Input::get('minimum_order_amount', 0);
        $minimum_order_amount = !empty($minimum_order_amount) ? $minimum_order_amount : 0;

        $number_uses = Input::get('number_uses');
        $type_use = Input::get('type_use');
        $expiration_date = Input::get('expiration_date');
        $campaign_validation = Input::get('campaign_validation');

        $payment_method = Input::get('payment_method', null);
        $payment_method = !empty($payment_method) ? $payment_method : null;

        $cc_bin = Input::get('cc_bin', null);
        $cc_bin = !empty($cc_bin) ? $cc_bin : null;

        $cc_bank = Input::get('cc_bank', null);
        $cc_bank = !empty($cc_bank) ? $cc_bank : null;

        $redeem_on = Input::get('redeem_on');
        $city_id = Input::get('city_id', null);
        $city_id = !empty($city_id) ? $city_id : null;

        $store_id = Input::get('store_id', null);
        $store_id = !empty($store_id) ? $store_id : null;

        $department_id = Input::get('department_id', null);
        $department_id = !empty($department_id) ? $department_id : null;

        $shelf_id = Input::get('shelf_id', null);
        $shelf_id = !empty($shelf_id) ? $shelf_id : null;

        $store_product_id = Input::get('store_product_id', null);
        $store_product_id = !empty($store_product_id) ? $store_product_id : null;

        $main_string = Input::get('main_string');
        $digit_length = Input::get('digit_length');
        $coupon_string_type = Input::get('coupon_string_type');
        $coupon_quantity = Input::get('coupon_quantity');

        $admin_id = Session::get('admin_id');

        try {
            $retry = 0;
            for ($i = 0; $i < $coupon_quantity; $i++) {
                if ($retry == 3){
                    $retry = 0;
                }
                if ($coupon_string_type == 'text') {
                    $subfix = \Illuminate\Support\Str::quickRandom($digit_length);
                } elseif ($coupon_string_type == 'numeric') {
                    $starts = [];
                    $ends = [];
                    for ($i = 0; $i++; $i < $digit_length) {
                        $starts[] = ($i == 0 ? 1 : 0);
                        $ends[] = 9;
                    }
                    $starts = implode('', $starts);
                    $ends = implode('', $ends);
                    $subfix = rand((int)$starts, (int)$ends);
                }
                $code = strtoupper($main_string . $subfix);

                $coupon_obj = Coupon::where('code', $code)->first();

                if (empty($coupon_obj)) {
                    $coupon_obj = new Coupon;
                    $coupon_obj->type = $type;
                    $coupon_obj->admin_id = $admin_id;
                    $coupon_obj->code = $code;
                    $coupon_obj->amount = $amount;
                    $coupon_obj->number_uses = $number_uses;
                    $coupon_obj->type_use = $type_use;
                    $coupon_obj->minimum_order_amount = $minimum_order_amount;
                    $coupon_obj->maximum_order_amount = $maximum_order_amount;
                    $coupon_obj->campaign_validation = $campaign_validation;
                    $coupon_obj->payment_method = $payment_method;
                    $coupon_obj->cc_bin = $cc_bin;
                    $coupon_obj->cc_bank = $cc_bank;
                    $coupon_obj->redeem_on = $redeem_on;
                    $coupon_obj->store_id = $store_id;
                    $coupon_obj->department_id = $department_id;
                    $coupon_obj->shelf_id = $shelf_id;
                    $coupon_obj->store_product_id = $store_product_id;
                    $coupon_obj->expiration_date = $expiration_date;
                    $coupon_obj->visible = 1;
                    $coupon_obj->status = 1;
                    $coupon_obj->save();
                } elseif ($retry != 3) {
                    $i--;
                    $retry++;
                }
            }

            return Redirect::route('adminCouponManager.index')->with('success', "Se han creado {$i}");
        } catch (\Exception $e) {
            return Redirect::route('adminCouponManager.index')->with('error', "No se han creado los cupones debido a un error: {$e->getMessage()}");
        }
    }
}
