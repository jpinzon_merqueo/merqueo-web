<?php

namespace admin;


use Request, Input, View, Session, Redirect, Response, DB, Zone, City, Warehouse, OrderGroup, DeliveryWindow, ZoneLocation, ZoneLocationSlot,ZoneDeliveryWindow;


class AdminZoneController extends AdminController
{
    /**
     * Grilla de zonas
     */
    public function index()
    {
        if (!Request::ajax()) {
            $city = City::find(Session::get('admin_city_id'));
            $zones = Zone::select('zones.*', 'cities.city', 'warehouses.warehouse')
                ->leftJoin('warehouses', 'zones.warehouse_id', '=', 'warehouse_id')
                ->leftJoin('cities', 'warehouses.city_id', '=', 'cities.id')
                ->where('warehouses.city_id', $city->id)
                ->orderBy('zones.status', 'desc')
                ->orderBy('zones.name')
                ->get();
            $zones_map = Zone::leftJoin('warehouses', 'zones.warehouse_id', '=', 'warehouse_id')
                ->where('warehouses.city_id', $city->id)
                ->where('zones.status', 1)
                ->orderBy('zones.name')
                ->get();
            return View::make('admin.zones.index')
                ->with('title', 'Zonas')
                ->with('zones', $zones)
                ->with('zones_map', $zones_map)
                ->with('lat', $city->latitude)
                ->with('lng', $city->longitude)
                ->with('cities', $this->get_cities())
                ->with('store_admin', false);
        } else {
            $city_id = Input::get('city_id');
            $city = City::find($city_id == null ? Session::get('admin_city_id') : $city_id);
            $zones = Zone::select('zones.*', 'warehouses.warehouse', 'cities.city')
                ->join('warehouses', 'warehouses.id', '=', 'zones.warehouse_id')
                ->join('cities', 'cities.id', '=', 'warehouses.city_id')
                ->where('warehouses.city_id', $city->id)
                ->orderBy('zones.status', 'desc')
                ->orderBy('zones.name')
                ->get();
            $zones_map =  Zone::join('warehouses', 'warehouses.id', '=', 'zones.warehouse_id')
                ->where('warehouses.city_id', $city->id)
                ->where('zones.status', 1)->orderBy('zones.name')->get();
            return View::make('admin.zones.index')
                ->with('title', 'Zonas')
                ->with('zones', $zones)
                ->with('zones_map', $zones_map)
                ->with('lat', $city->latitude)
                ->with('lng', $city->longitude)
                ->with('cities', $this->get_cities())
                ->with('store_admin', false)
                ->renderSections()['content'];
        }
    }

    /**
     * Editar zona
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Zona' : 'Nueva Zona';
        $edit = false;
        $orders_per_route = '';
        if (!empty($id)) {
            $zone = Zone::select('zones.*', 'warehouses.city_id')
                ->leftJoin('warehouses', 'zones.warehouse_id', '=', 'warehouses.id')
                ->where('zones.id', $id)
                ->first();
            $edit = true;
            $warehouse = Warehouse::find($zone->warehouse_id);
            $warehouses = Warehouse::where('city_id', $warehouse->city_id)->where('status', 1)->orderBy('warehouse')->get();
            $zone->shift_delivery_windows = json_decode($zone->maximum_orders_per_delivery_window);
            $delivery_windows = DeliveryWindow::getDeliveryWindowsByCity($warehouse->city_id);
            $delivery_windows_same_day = DeliveryWindow::getDeliveryWindowsByCity($warehouse->city_id, true);
            $locations = ZoneLocation::where('zone_id', $id)->get();
        } else {
            $zone = new Zone;
            $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->where('status', 1)->orderBy('warehouse')->get();
            $delivery_windows = DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id'));
            $delivery_windows_same_day = DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id'), true);
            $locations = null;
            $orders_per_route = ZoneDeliveryWindow::where('zone_id', $id)->get();
        }
        return View::make('admin.zones.zone_form')
            ->with('title', 'Zona')
            ->with('locations', $locations)
            ->with('sub_title', $sub_title)
            ->with('cities', $this->get_cities())
            ->with('edit', $edit)
            ->with('zones_map', $zone)
            ->with('zone', $zone)
            ->with('store_admin', false)
            ->with('warehouses', $warehouses)
            ->with('delivery_windows', $delivery_windows)
            ->with('delivery_windows_same_day',$delivery_windows_same_day)
            ->with('orders_per_route', $orders_per_route);
    }

    /**
     * Guardar zona
     */
    public function save()
    {
        try{
            DB::beginTransaction();

            $id = Input::get('id');
            $zone = Zone::find($id);
            if(!$zone) {
                $zone = new Zone;
                $action = 'insert';
            }else $action = 'update';
            $zone->name = Input::get('name_zones');
            $zone->delivery_zone = Input::get('delivery_zone');
            $zone->status = Input::get('status');
            $zone->orders_per_route = Input::get('orders_per_route');
            $zone->delivery_on_same_day = Input::get('delivery_on_same_day');
            //actualizar bodega de pedidos pendientes
            if ($action == 'update' && $zone->warehouse_id != Input::get('warehouse_id')){
                $order_groups = OrderGroup::select('order_groups.*')
                                        ->join('orders', 'order_groups.id', '=', 'orders.group_id')
                                        ->where('order_groups.zone_id', $zone->id)
                                        ->whereIn('orders.status', ['Validation', 'Initiated'])
                                        ->get();
                if ($order_groups){
                    foreach($order_groups as $order_group){
                        $order_group->warehouse_id = Input::get('warehouse_id');
                        $order_group->save();
                    }
                }
            }

            $zone->warehouse_id = Input::get('warehouse_id');
            $zone->save();

            DB::commit();

        }catch(\Exception $e){
            DB::rollback();

            return Redirect::back()->with('type', 'failed')->with('message', $e->getMessage())->withInput();
        }

        $this->admin_log('zones', $zone->id, $action);

        return Redirect::route('adminZones.index')->with('type', 'success')->with('message', 'Zona actualizada satifactoriamente.');
    }

    /**
     * Activar zona
     */
    public function toggle($id)
    {
        $zone = Zone::find($id);
        $zone->status = 1;
        $zone->save();
        $this->admin_log('zones', $zone->id, 'update');

        return Redirect::route('adminZones.index')->with('type', 'success')->with('message', 'Zona actualizada satifactoriamente.');
    }

    /**
     * Eliminar zona
     */
    public function delete($id)
    {
        $zone = Zone::find($id);
        $zone->status = 0;
        $zone->save();

        $this->admin_log('zones', $id, 'delete');
        return Redirect::route('adminZones.index')->with('type', 'success')->with('message', 'Zona desactivada satifactoriamente.');
    }

    /**
     * Obtiene las bodegas por ciudad
     *
     * @return string
     */
    public function get_warehouses_by_city_ajax()
    {
        $warehouses = [];
        $res['status'] = 400;
        $res['message'] = "Error al consultar las bodegas";
        if (Input::has('city_id')) {
            $warehouses = Warehouse::where('status', 1)
                ->where('city_id', Input::get('city_id'))
                ->get();
            $res['status'] = 200;
            $res['message'] = "Bodegas por ciudad";
        }
        $res['result']['warehouses'] = $warehouses;
        return json_encode($res);
    }


    /**
     * Obtiene los horarios restringidos de la zona
     *
     * @param  int id de la zona
     */
    public function get_location_slot_ajax($id)
    {
        $location_slot_id = Input::get('location_slot_id', null);
        $slot_obs = ZoneLocationSlot::join('delivery_windows', 'zone_location_slots.delivery_window_id', '=', 'delivery_windows.id')
            ->select('zone_location_slots.id', 'zone_location_slots.day','delivery_windows.delivery_window')
            ->where('zone_location_id', $location_slot_id)
            ->where('zone_location_slots.status', 1)
            ->orderBy('zone_location_slots.day')
            ->orderBy('delivery_windows.hour_start')->get()->toArray();
        $slots = [];
        foreach ($slot_obs as $key => $slot_item) {
            if ( $slot_item['day'] == 0 ) {
                $slots[0][] = $slot_item;
            }
            if ( $slot_item['day'] == 1 ) {
                $slots[1][] = $slot_item;
            }
            if ( $slot_item['day'] == 2 ) {
                $slots[2][] = $slot_item;
            }
            if ( $slot_item['day'] == 3 ) {
                $slots[3][] = $slot_item;
            }
            if ( $slot_item['day'] == 4 ) {
                $slots[4][] = $slot_item;
            }
            if ( $slot_item['day'] == 5 ) {
                $slots[5][] = $slot_item;
            }
            if ( $slot_item['day'] == 6 ) {
                $slots[6][] = $slot_item;
            }
            if ( $slot_item['day'] == 7 ) {
                $slots[7][] = $slot_item;
            }
        }
        $data = [
            'slot_obs' => $slots,
            'section' => 'zone_location_slots'
        ];
        $view = View::make('admin.zones.zone_form', $data)->renderSections();

        return Response::json(['success' => 'Se guardó exitosamente.', 'html' => $view['zone_location_slots']]);
    }

    /**
     * Obtiene listado de zonas restringidas
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_location_ajax($id)
    {
        $locations = ZoneLocation::where('zone_id', $id)
            ->select('id','name', DB::raw('DATE_FORMAT(created_at,"%d/%m/%Y") AS date'), 'status')
            ->get();

        $data = [
            'section' => 'zone_location_slots_table_container',
            'locations' => $locations
        ];
        $view = View::make('admin.zones.zone_form', $data)->renderSections();

        return Response::json(['success' => 'Se guardó exitosamente.', 'html' => $view['zone_location_slots_table_container']]);
    }

    /**
     * Guardar horario de restriccion de la zona
     */
    public function save_location_slot_ajax($id)
    {
        $day = Input::get('day', null);
        $delivery_window_id = Input::get('shift_delivery', null);
        $location_id = Input::get('location_id', null);
        $validate =  ZoneLocationSlot::where('delivery_window_id', $delivery_window_id)->where('day', $day)->where('zone_location_id', $location_id)->first();
        if(!empty($validate)) {
            $type = "error";
            $message = "La franja horaria ya existe para el día seleccionado.";
        } else {
            $type = "success";
            $message = "Se guardó exitosamente.";
            $slot_ob = new ZoneLocationSlot;
            $slot_ob->zone_location_id = $location_id;
            $slot_ob->day = $day;
            $slot_ob->delivery_window_id = $delivery_window_id;
            $slot_ob->status = 1;
            $slot_ob->save();
        }
        $slot_obs = ZoneLocationSlot::join('delivery_windows', 'zone_location_slots.delivery_window_id', '=', 'delivery_windows.id')
            ->select('zone_location_slots.id', 'zone_location_slots.day', 'delivery_windows.delivery_window')
            ->where('zone_location_id', $location_id)
            ->where('zone_location_slots.status', 1)
            ->orderBy('zone_location_slots.day')
            ->orderBy('delivery_windows.hour_start')->get()->toArray();
        $slots = [];
        foreach ($slot_obs as $key => $slot_item) {
            if ($slot_item['day'] == 0) {
                $slots[0][] = $slot_item;
            }
            if ($slot_item['day'] == 1) {
                $slots[1][] = $slot_item;
            }
            if ($slot_item['day'] == 2) {
                $slots[2][] = $slot_item;
            }
            if ($slot_item['day'] == 3) {
                $slots[3][] = $slot_item;
            }
            if ($slot_item['day'] == 4) {
                $slots[4][] = $slot_item;
            }
            if ($slot_item['day'] == 5) {
                $slots[5][] = $slot_item;
            }
            if ($slot_item['day'] == 6) {
                $slots[6][] = $slot_item;
            }
            if ($slot_item['day'] == 7) {
                $slots[7][] = $slot_item;
            }
        }

        $data = [
            'slot_obs' => $slots,
            'section' => 'zone_location_slots'
        ];
        $view = View::make('admin.zones.zone_form', $data)->renderSections();

        return Response::json([$type => $message, 'html' => $view['zone_location_slots']]);
    }

    /**
     * Borrar un horario restringido de la zona
     *
     * @param  int id de la tienda
     */
    public function delete_location_slot_ajax($id)
    {
        $location_slot_id = Input::get('location_slot_id', null);
        $slot_ob = ZoneLocationSlot::findOrFail($location_slot_id);
        $slot_ob->delete();

        return Response::json(['success' => 'Se ha eliminado exitosamente.']);
    }

    /**
     * Actualiza la cobertura de restriccion de la zona
     *
     * @param string $action Accion a ejecutar
     * @param int $id ID de la tienda
     * @return object JSON array
     */
    public function delivery_zone($action, $id)
    {
        $status = false;
        $created_at = date('Y-m-d H:i:s');
        switch($action) {
            case 'delete':
                ZoneLocationSlot::where('zone_location_id', $id)->delete();
                $status = ZoneLocation::find($id)->delete();
                break;
            case 'update':
                // Crea o actualiza una zona de entrega
                if($id == -1) {
                    $zone_location = new ZoneLocation;

                } else {
                    $zone_location = ZoneLocation::find($id);
                }

                $zone_location->name = Input::get('name');
                $zone_location->delivery_zone = Input::get('delivery_zone');
                $zone_location->zone_id = Input::get('zone_id');
                $zone_location->created_at = $created_at;
                $status = $zone_location->save();
                $id = $zone_location->id;
                $created_at = date('d/m/Y', strtotime($created_at));
                break;
            case 'change-status':
                $zone_location = ZoneLocation::find($id);
                if($zone_location->status) {
                    $zone_location->status = false;
                } else {
                    $zone_location->status = true;
                }
                $status = $zone_location->save();
                $id = $zone_location->id;
                $created_at = date('d/m/Y', strtotime($created_at));
                break;
        }

        return Response::json(array('status' => $status, 'id' => $id, 'created_at' => $created_at), 200);
    }

    /**
     * Guarda maximo de pedidos por franja horaria para planeacion en la zona
     *
     * @return object JSON array
     */
    public function save_orders_per_delivery_window()
    {
        try {
            DB::beginTransaction();

            foreach (Input::get('nro_orders') as $number_orders) {
                $orders_per_route = ZoneDeliveryWindow::where('zone_id', Input::get('zone_id'))->where('delivery_window_id',$number_orders['delivery_window_id'])->get();
                if (count($orders_per_route)) {
                    $orders_per_route = $orders_per_route->first();
                    if ($number_orders['delivery_window_id'] == $orders_per_route->delivery_window_id) {
                        $orders_per_route->maximum_orders_per_route = $number_orders['value'];
                        $orders_per_route->save();
                    }
                }else{
                    $orders_per_route = new ZoneDeliveryWindow;
                    $orders_per_route->zone_id = Input::get('zone_id');
                    $orders_per_route->delivery_window_id = $number_orders['delivery_window_id'];
                    $orders_per_route->maximum_orders_per_route = $number_orders['value'];
                    $orders_per_route->save();
                }
            }
            DB::commit();
            return Response::json(['result' => true, 'message' => 'El máximo de pedidos por franja horaria se actualizo con éxito'], 200);
        }catch (\Exception $e){
            DB::rollback();
            return Response::json(['result' => false, 'message' => $e->getMessage().''.$e->getLine()], 200);

        }
    }

}