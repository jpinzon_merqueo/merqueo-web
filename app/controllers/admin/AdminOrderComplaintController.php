<?php

namespace admin;

use App, Request, View, Input, Route, Redirect, Session, DB, Config, DateTime, Response, Validator, Order, OrderProduct, Shopper,
    UserCredit, UserCreditCard, Store, Product, OrderNoteSms, OrderLog, OrderGroup, RejectReason, City,
    UserFreeDelivery, Zone, Transporter, Vehicle, Log, Event, OrderProductGroup, Fpdf, StoreProduct, StoreProductGroup;

use Merqueo\Backend\Facades\AuthBackend;

class AdminOrderComplaintController extends AdminController {

    const ZERO_PRICE = 0;
    const SELL_PRICE = 1;
    const CURRENT_PRICE = 2;

    /**
     * Muestra formulario para crear nueva orden de reclamo
     * @param  int $id identificador de la orden
     */
    public function order_complaint($id)
    {
        $order = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('cities AS cities_user','cities_user.id','=','order_groups.user_city_id')
            ->join('stores','stores.id','=','orders.store_id')
            ->join('cities AS cities_store','cities_store.id','=','stores.city_id')
            ->join('users', 'order_groups.user_id', '=', 'users.id')
            ->join('order_products','order_products.order_id','=','orders.id')
            ->leftJoin('user_credit_cards', 'orders.credit_card_id', '=', 'user_credit_cards.id')
            ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->leftJoin('pickers AS pickers_dry', 'orders.picker_dry_id', '=', 'pickers_dry.id')
            ->leftJoin('pickers AS pickers_cold', 'orders.picker_cold_id', '=', 'pickers_cold.id')
            ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
            ->leftJoin(DB::raw('(user_credits, coupons)'), function($join){
                $join->on('user_credits.order_id', '=', 'orders.id');
                $join->on('user_credits.type', '=', DB::raw('0'));
                $join->on('user_credits.coupon_id', '=', 'coupons.id');
                $join->on('user_credits.status', '=', DB::raw('1'));
            })
            ->where('orders.id', $id)
            ->where('stores.is_storage', 1)
            ->select('orders.*', 'users.sift_payment_abuse','order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email', 'coupons.code AS coupon_code',
                'order_groups.user_address', 'order_groups.user_phone', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source',
                DB::raw("CONCAT(pickers_dry.first_name, ' ', pickers_dry.last_name) AS picker_dry_name"), DB::raw("CONCAT(pickers_cold.first_name, ' ', pickers_cold.last_name) AS picker_cold_name"),
                'stores.name AS store_name', 'stores.id AS store_id', 'cities_store.city AS store_city', 'zones.id AS zone_id', 'zones.name AS zone', 'orders.invoice_number', 'stores.revision_orders_required',
                'order_groups.source_os', 'order_groups.user_comments', 'cities_user.city', 'cities_user.id AS city_id', DB::raw("SUM(orders.total_amount) AS total_amount_og"),
                'user_brand_campaigns.campaign', 'order_groups.user_address_latitude', 'order_groups.user_address_longitude');

        $order = $order->first();
        $store = Store::find($order->store_id);

        if (!$order->id)
            return App::abort(404);

        //validar pedido sospechoso
        $suspect_credit_card_order_amount = Config::get('app.suspect_credit_card_order_amount');
        if ($order->status != 'Cancelled' && $order->status != 'Delivered' && $order->payment_method == 'Tarjeta de crédito' && $order->total_amount_og > $suspect_credit_card_order_amount)
            $order->suspect = 'El pedido es con tarjeta de crédito y el total agrupado por tienda es mayor a $'.number_format($suspect_credit_card_order_amount, 0, ',', '.');
        else $order->suspect = 0;

        $order_products = OrderProduct::where('order_id', $order->id)
            ->join('stores','order_products.store_id','=','stores.id')
            ->join('store_products','store_products.id','=','order_products.store_product_id')
            ->join('products','store_products.product_id','=','products.id')
            ->select(
                'order_products.*', 'stores.name AS store_name', 'products.reference',
                'products.quantity AS product_quantity', 'products.unit AS product_unit',
                'products.name', 'store_products.special_price', 'store_products.price as current_price'
            )
            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
            ->orderBy('store_products.department_id', 'desc')->orderBy('product_name', 'asc')
            ->get();
        $order_notes = OrderNoteSms::select('order_notes_sms.created_at', 'order_notes_sms.description', 'admin.fullname')
            ->join('admin', 'admin.id', '=', 'admin_id')->where('order_id', $order->id)->where('order_notes_sms.type', 'note')
            ->orderBy('created_at', 'desc')->get();

        $customer_orders = Order::select('orders.*','order_groups.user_firstname','order_groups.user_lastname','order_groups.user_email','order_groups.user_address','order_groups.user_phone',
            'order_groups.user_comments', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source', 'order_groups.source_os', 'stores.name AS store')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->where('orders.user_id', $order->user_id)->where('orders.id', '<>', $order->id)->orderBy('orders.created_at', 'desc')->get();
        $customer_credit_cards = UserCreditCard::where('user_id', $order->user_id)->get();
        $customer_free_deliveries = UserFreeDelivery::where('user_id', $order->user_id)->where('status', 1)->orderBy('created_at', 'desc')->get();

        $order_transporter = null;
        if ($order->vehicle_id){
            $order_transporter = Vehicle::where('vehicles.id', $order->vehicle_id)
                ->join('vehicle_drivers', 'vehicle_drivers.vehicle_id', '=', 'vehicles.id')
                ->join('drivers', 'vehicle_drivers.driver_id', '=', 'drivers.id')
                ->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
                ->select(
                    'drivers.profile AS driver_profile',
                    'drivers.first_name AS driver_first_name',
                    'drivers.last_name AS driver_last_name',
                    'drivers.phone AS driver_phone',
                    'drivers.cellphone AS driver_cellphone',
                    'drivers.category_license AS driver_category_license',
                    'transporters.document_type AS transporter_document_type',
                    'transporters.document_number AS transporter_document_number',
                    'transporters.fullname AS transporter_document_fullname',
                    'transporters.phone AS transporter_phone',
                    'transporters.cellphone AS transporter_cellphone',
                    'transporters.email AS transporter_email',
                    'vehicles.plate AS vehicle_plate',
                    'vehicles.class_type AS vehicle_class_type'
                )->where('drivers.id',$order->driver_id)
                ->first();
        }

        $order_log = OrderLog::select(DB::raw("CONCAT(pickers.first_name,' ', pickers.last_name) AS picker_name"),
            DB::raw("CONCAT(users.first_name,' ', users.last_name) AS user_name"),
            DB::raw("CONCAT(drivers.first_name,' ', drivers.last_name) AS driver_name"),
            DB::raw("admin.fullname AS 'admin_name'"), 'order_logs.type', 'order_logs.created_at')
            ->leftJoin('pickers', 'order_logs.picker_id', '=', 'pickers.id')
            ->leftJoin('drivers', 'order_logs.driver_id', '=', 'drivers.id')
            ->leftJoin('users', 'order_logs.user_id', '=', 'users.id')
            ->leftJoin('admin', 'order_logs.admin_id', '=', 'admin.id')
            ->where('order_logs.order_id', $order->id)
            ->get();

        $transporters = Transporter::where('status', 1)->where('city_id', $order->city_id)->orderBy('fullname', 'asc')->get();


        $session_products = ['products'=>null, 'products_pickup'=>null];
        // Session::forget('complaint_orders');
        if ( Session::has('admin_complaint_orders') && Session::has('admin_complaint_orders.'.$order->id) ) {
            $session_products = $this->get_products_from_session($order->id, $order->store_id);
        }

        if ($order->payment_method === 'Débito - PSE' && $order->order_validation_reason_id === 6) {
            Session::flash('warning', 'Este pedido fue pagado mediante PSE, por favor no fijes valores a los productos que vayas a agregar.');
        }

        $data = [
            'title' => 'Reclamo del pedido',
            'order' => $order,
            'cities' => $this->get_cities(),
            'order_log' => $order_log,
            'order_products' => $order_products,
            'order_notes' => $order_notes,
            'customer_orders' => $customer_orders,
            'customer_credit_cards' => $customer_credit_cards,
            'customer_free_deliveries' => $customer_free_deliveries,
            'order_transporter' => $order_transporter,
            'transporters' => $transporters,
            'reasons' => RejectReason::admin()->orderBy('reason')->get(),
            'session_products' => $session_products['products'],
            'session_products_pickup' => $session_products['products_pickup'],
            'store' => $store
        ];

        // return View::make('admin.orders_storage.details', $data);
        return View::make('admin.orders_complaint.details', $data);
    }

    /**
     * Buscar productos por ajax
     */
    public function search_complaint_product_ajax()
    {
        $search = Input::get('s');
        $store_id = Input::get('store_id');
        $order_id = Input::get('order_id');
        $type = Input::get('type');
        $order_products = OrderProduct::where('order_id', $order_id)->where('is_gift', 0)
            ->join('stores','order_products.store_id','=','stores.id')
            ->join('store_products','store_products.id','=','order_products.store_product_id')
            ->join('products','store_products.product_id','=','products.id')
            ->select(
                'order_products.*', 'stores.name AS store_name', 'products.reference',
                'products.quantity AS product_quantity', 'products.unit AS product_unit',
                'products.name AS name', 'store_products.special_price',
                'store_products.price as current_price'
            )
            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
            ->orderBy('store_products.department_id', 'desc')
            ->orderBy('product_name', 'asc');
        if (!$type) {
            $products = OrderProduct::where(function($query) use ($search) {
                $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
            })
            ->where('order_id', $order_id)->where('is_gift', 0)
            ->join('stores','order_products.store_id','=','stores.id')
            ->join('store_products','store_products.id','=','order_products.store_product_id')
            ->join('products','store_products.product_id','=','products.id')
            ->select(
                'order_products.*', 'stores.name AS store_name', 'products.reference',
                'products.quantity AS product_quantity', 'products.unit AS product_unit',
                'products.name AS name', 'store_products.special_price',
                'store_products.price as current_price'
            )
            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
            ->orderBy('store_products.department_id', 'desc')
            ->orderBy('product_name', 'asc')->get();        


            foreach ($products as $key => &$product) {
                $product->complaint_price = 0;
                $product->product_table = 'order_products';
            }
        }else{
            $products = $order_products->get();
            foreach ($products as $key => &$product) {
                $product->complaint_price = 0;
                $product->product_table = 'order_products';
            }
        }

        $data = [
            'products' => $products,
            'store_id' => $store_id,
            'order_id' => $order_id
        ];
        return View::make('admin.orders_complaint.details', $data)->renderSections()['product-table'];
    }

    /**
     * Agrega productos al pedido de reclamo por medio de sessiones
     */
    public function add_complaint_product_ajax()
    {
        $complaint_status = Input::get('complaint_status');
        $order_id = Input::get('order_id');
        $original_price = Input::get('original_price');
        $price = Input::get('price');
        $product_table = Input::get('product_table');
        $quantity = Input::get('quantity');
        $store_id = Input::get('store_id');
        $is_picking_up = Input::get('is_picking_up');
        $store = Store::find($store_id);
        $order = Order::find($order_id);
        $store_product = StoreProduct::find(Input::get('store_product_id'));

        if (empty($store_product)) {
            return Response::json(['error' => 'El producto no se encuentra disponible.']);
        }

        try {
            $complaint_item = $this->get_product_price_by_selector($order, $store_product, Input::get('complaint_price'));
        } catch (\Exception $exception) {
            return Response::json(['error' => $exception->getMessage()]);
        }

        $order_products = OrderProduct::find(Input::get('order_product_id'));
        if(!empty($order_products) && $order_products->type == 'Agrupado' && Input::get('is_picking_up')){
            return Response::json(['error' => 'No es posible solicitar recogida de un producto tipo combo, debe solictarlos como productos individuales.']);
        }
        // Session::clear();
        // Session::forget('complaint_orders');

        if ( Session::has('admin_complaint_orders') && Session::has('admin_complaint_orders.'.$order_id) ) {
            if(!$is_picking_up){
                if ( Session::has('admin_complaint_orders.'.$order_id.'.products.'.$store_product->id) && !$is_picking_up) {
                    return Response::json(['error' => 'El producto ya ha sido agregado.']);
                }
                $product = [
                    'complaint_price'  => ($complaint_item->special_price ?: $complaint_item->price),
                    'complaint_status' => $complaint_status,
                    'original_price'   => $original_price,
                    'price'            => $price,
                    'store_product_id' => $store_product->id,
                    'product_table'    => $product_table,
                    'quantity'         => $quantity,
                    'merqueo_discount' => $complaint_item->merqueo_discount,
                    'provider_discount'=> $complaint_item->provider_discount,
                    'seller_discount'  => $complaint_item->seller_discount,
                    'is_picking_up'    => $is_picking_up
                ];
                Session::put('admin_complaint_orders.'.$order_id.'.products.'.$store_product->id, $product);
            }else{
                if ( Session::has('admin_complaint_orders.'.$order_id.'.products_pickup.'.$store_product->id) && $is_picking_up) {
                    return Response::json(['error' => 'El producto ya ha sido agregado.']);
                }
                $product = [
                    'complaint_price'  => ($complaint_item->special_price ?: $complaint_item->price),
                    'complaint_status' => $complaint_status,
                    'original_price'   => $original_price,
                    'price'            => $price,
                    'store_product_id' => $store_product->id,
                    'product_table'    => $product_table,
                    'quantity'         => $quantity,
                    'merqueo_discount' => $complaint_item->merqueo_discount,
                    'provider_discount'=> $complaint_item->provider_discount,
                    'seller_discount'  => $complaint_item->seller_discount,
                    'is_picking_up'    => $is_picking_up
                ];
                Session::put('admin_complaint_orders.'.$order_id.'.products_pickup.'.$store_product->id, $product);
            }

            $products = $this->get_products_from_session($order_id, $store_id);
            $data = [
                'order'    => $order,
                'order_id' => $order_id,
                'products' => $products['products'],
                'products_pickup' => $products['products_pickup'],
                'readonly' => true,
                'store'    => $store,
                'store_id' => $store_id
            ];
            $html = View::make('admin.orders_complaint.details', $data)->renderSections()['product-table'];
            $html_pickup = View::make('admin.orders_complaint.details', $data)->renderSections()['product-pickup-table'];
            return Response::json(['success' => 'Producto correctamente agregado.', 'html' => $html, 'html_pickup'=> $html_pickup]);
        } else {
            $complaint_orders[$order_id]['store_id'] = $store_id;
            if(!$is_picking_up){
                $complaint_orders[$order_id]['products'][$store_product->id] = [
                    'complaint_price'  => $complaint_item->special_price ?: $complaint_item->price,
                    'complaint_status' => $complaint_status,
                    'original_price'   => $original_price,
                    'price'            => $price,
                    'store_product_id' => $store_product->id,
                    'product_table'    => $product_table,
                    'quantity'         => $quantity,
                    'merqueo_discount' => $complaint_item->merqueo_discount,
                    'provider_discount'=> $complaint_item->provider_discount,
                    'seller_discount'  => $complaint_item->seller_discount,
                    'is_picking_up'    => $is_picking_up
                ];
                Session::put('admin_complaint_orders', $complaint_orders);
            }else{
                $complaint_orders[$order_id]['products_pickup'][$store_product->id] = [
                    'complaint_price'  => $complaint_item->special_price ?: $complaint_item->price,
                    'complaint_status' => $complaint_status,
                    'original_price'   => $original_price,
                    'price'            => $price,
                    'store_product_id' => $store_product->id,
                    'product_table'    => $product_table,
                    'quantity'         => $quantity,
                    'merqueo_discount' => $complaint_item->merqueo_discount,
                    'provider_discount'=> $complaint_item->provider_discount,
                    'seller_discount'  => $complaint_item->seller_discount,
                    'is_picking_up'    => $is_picking_up
                ];
                Session::put('admin_complaint_orders', $complaint_orders);
            }
            
            $products = $this->get_products_from_session($order_id, $store_id);
            $data = [
                'order'    => $order,
                'order_id' => $order_id,
                'products' => $products['products'],
                'products_pickup' => $products['products_pickup'],
                'readonly' => true,
                'store'    => $store,
                'store_id' => $store_id
            ];
            $html = View::make('admin.orders_complaint.details', $data)->renderSections()['product-table'];
            $html_pickup = View::make('admin.orders_complaint.details', $data)->renderSections()['product-pickup-table'];
            return Response::json(['success' => 'Producto correctamente agregado.', 'html' => $html, 'html_pickup'=> $html_pickup]);
        }
    }

    /**
     * Determina cual es el precio que se debe asignar a producto dentro del pedido reclamo.
     *
     * @param Order $order
     * @param StoreProduct $store_product
     * @param $option
     * @return StoreProduct|OrderProduct
     * @throws \Exception
     */
    private function get_product_price_by_selector(Order $order, StoreProduct $store_product, $option)
    {
        switch (intval($option)) {
            case self::ZERO_PRICE:
                $store_product = new StoreProduct();
                $store_product->price = 0;

                return $store_product;

            case self::SELL_PRICE:
                $order_product = $order->orderProducts()
                    ->where('store_product_id', $store_product->id)
                    ->first();

                if (empty($order_product)) {
                    throw new \Exception('El producto no se agrego al pedido original.');
                }

                return $order_product;

            case self::CURRENT_PRICE:
                if (!empty($store_product->special_price)) {
                    $discount = $store_product->price - $store_product->special_price;
                    $store_product->provider_discount = $discount * $store_product->provider_discount / 100;
                    $store_product->merqueo_discount = $discount * $store_product->merqueo_discount / 100;
                    $store_product->seller_discount = $discount * $store_product->seller_discount / 100;
                }

                return $store_product;

            default:
                throw new \Exception('Debes seleccionar el precio con el cual se debe agregar el producto.');
        }
    }

    /**
     * Quita los productos de la sesión del pedido de reclamo
     */
    public function remove_complaint_product_ajax()
    {
        $order_id = Input::get('order_id');
        $store_product_id = Input::get('store_product_id');
        $is_picking_up = Input::get('is_picking_up');
        if(!$is_picking_up){
            if ( Session::has('admin_complaint_orders') ) {
                Session::forget('admin_complaint_orders.'.$order_id.'.products.'.$store_product_id);
                return Response::json(['success' => 'Producto borrado correctamente.']);
            }
        }else{
            if ( Session::has('admin_complaint_orders') ) {
                Session::forget('admin_complaint_orders.'.$order_id.'.products_pickup.'.$store_product_id);
                return Response::json(['success' => 'Producto borrado correctamente.']);
            }
        }
        return Response::json(['error' => 'No se ha borrado el producto.']);
    }

    /**
     * Obtiene los productos de pedido de reclamo
     */
    protected function get_products_from_session($order_id, $store_id)
    {
        $session_products = [];
        $products_from_session =[];
        $products_pickup_from_session = [];
        if( Session::has('admin_complaint_orders.'.$order_id.'.products')){
            $products_from_session = Session::get('admin_complaint_orders.'.$order_id.'.products');
            $session_products = array_keys($products_from_session);
        }
        if( Session::has('admin_complaint_orders.'.$order_id.'.products_pickup')){
            $products_pickup_from_session = Session::get('admin_complaint_orders.'.$order_id.'.products_pickup');
            $session_products_pickup = array_keys($products_pickup_from_session);
            $session_products = array_merge($session_products,$session_products_pickup);
        }
        $products = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
            ->where('store_id', $store_id)
            ->whereIn('store_products.id', $session_products)
            ->select('products.image_small_url AS product_image_url', 'products.reference', 'products.name', 'products.quantity AS product_quantity', 'products.unit AS product_unit', 'store_products.special_price AS price', 'store_products.price AS original_price', 'store_products.id AS store_product_id')
            ->get();

        foreach ($products as $key => &$product) {
            if (isset($products_from_session) && array_key_exists($product->store_product_id, $products_from_session) ) {
                $products_from_session[$product->store_product_id]['product_image_url'] = $product->product_image_url;
                $products_from_session[$product->store_product_id]['product_table'] = $product->product_table;
                $products_from_session[$product->store_product_id]['reference'] = $product->reference;
                $products_from_session[$product->store_product_id]['name'] = $product->name;
                $products_from_session[$product->store_product_id]['product_quantity'] = $product->product_quantity;
                $products_from_session[$product->store_product_id]['product_unit'] = $product->product_unit;
                $products_from_session[$product->store_product_id]['store_product_id'] = $product->store_product_id;
                $products_from_session[$product->store_product_id]['complaint_price'];
                $products_from_session[$product->store_product_id]['complaint_status'];
                $products_from_session[$product->store_product_id]['original_price'];
                $products_from_session[$product->store_product_id]['price'];
                $products_from_session[$product->store_product_id]['product_table'];
                $products_from_session[$product->store_product_id]['session_quantity'] = $products_from_session[$product->store_product_id]['quantity'];
                $product->is_picking_up = $products_from_session[$product->store_product_id]['is_picking_up'];
            }
            if( Session::has('admin_complaint_orders.'.$order_id.'.products_pickup')){
                
                if ( isset($products_pickup_from_session) && array_key_exists($product->store_product_id, $products_pickup_from_session) ) {
                    $products_pickup_from_session[$product->store_product_id]['product_image_url'] = $product->product_image_url;
                    $products_pickup_from_session[$product->store_product_id]['product_table'] = $product->product_table;
                    $products_pickup_from_session[$product->store_product_id]['reference'] = $product->reference;
                    $products_pickup_from_session[$product->store_product_id]['name'] = $product->name;
                    $products_pickup_from_session[$product->store_product_id]['product_quantity'] = $product->product_quantity;
                    $products_pickup_from_session[$product->store_product_id]['product_unit'] = $product->product_unit;
                    $products_pickup_from_session[$product->store_product_id]['store_product_id'] = $product->store_product_id;
                    $products_pickup_from_session[$product->store_product_id]['complaint_price'];
                    $products_pickup_from_session[$product->store_product_id]['complaint_status'];
                    $products_pickup_from_session[$product->store_product_id]['original_price'];
                    $products_pickup_from_session[$product->store_product_id]['price'];
                    $products_pickup_from_session[$product->store_product_id]['product_table'];
                    $products_pickup_from_session[$product->store_product_id]['session_quantity'] = $products_pickup_from_session[$product->store_product_id]['quantity'];
                }
                
            }
        }


        return ['products'=>$products_from_session, 'products_pickup' => $products_pickup_from_session];
    }

    /**
     * Crea la orden de reclamo
     */
    public function save_order_complaint($id)
    {
        if ( Session::has('admin_complaint_orders') && Session::has('admin_complaint_orders.'.$id) && Session::has('admin_complaint_orders.'.$id.'.products') ) {
            $products = Session::get('admin_complaint_orders.'.$id.'.products');
            $products_pickin_up = Session::get('admin_complaint_orders.'.$id.'.products_pickup');
            if ( !empty($products) ) {
                $total_amount = 0;
                foreach ($products as $key => $product) {
                    $total_amount += $product['quantity'] * $product['price'];
                }

                $comments = Input::get('comments', null);
                $complaint_reason = Input::get('complaint_reason');
                $order = Order::find($id);
                $order_group = OrderGroup::find($order->group_id);

                $delivery_day = Input::get('delivery_day');
                $delivery_window_id = Input::get('delivery_window_id');

                try {
                    //valida que todas las tiendas tengan cobertura en la dirección y obtener deliveryWindow
                    $delivery_window = $order->store->validateStoreDeliversToAddress(
                        $order_group->user_address_latitude,
                        $order_group->user_address_longitude,
                        new \Carbon\Carbon($delivery_day),
                        $delivery_window_id
                    );
                } catch (MerqueoException $exception) {
                    return Redirect::back()->withInput()->with('error', $exception->getMessage());
                }

                // Validar fecha de entrega
                if (!self::hasValidDeliveryTime($order_group->zone, $delivery_day, $delivery_window)) {
                    return Redirect::back()->withInput()->with('error', 'Los horarios de entrega han cambiado, por favor vuelve a seleccionar el horario.');
                }

                //Order Group
                $new_order_group = $order_group->replicate();
                $new_order_group->warehouse_id = $order_group->zone->warehouse_id;
                $new_order_group->source = 'Reclamo';
                $new_order_group->total_amount = $total_amount;
                $new_order_group->delivery_amount = 0;
                $new_order_group->discount_amount = 0;
                $new_order_group->products_quantity = count($products);
                $new_order_group->user_comments = $comments;
                $new_order_group->save();

                //Order
                $new_order = $order->replicate();
                $new_order->reference = generate_reference();
                $new_order->group_id = $new_order_group->id;
                $new_order->delivery_window_id = $delivery_window->id;
                $new_order->real_delivery_date = $new_order->delivery_date = $new_order->first_delivery_date = $delivery_day.' '.$delivery_window->hour_end;
                $new_order->delivery_time = $delivery_window->delivery_window;
                $new_order->parent_order_id = $order->id;
                $new_order->shopper_id = null;
                $new_order->route_id = null;
                $new_order->driver_id = null;
                $new_order->picker_dry_id = null;
                $new_order->picker_cold_id = null;
                $new_order->packing_admin_id = null;
                $new_order->vehicle_id = null;
                $new_order->user_score = null;
                $new_order->user_score_source = null;
                $new_order->user_score_typification = null;
                $new_order->user_score_date = null;
                $new_order->user_score_comments = null;
                $new_order->status = 'Initiated';
                $new_order->total_products = $new_order_group->products_quantity;
                $new_order->delivery_amount = 0;
                $new_order->discount_amount = 0;
                $new_order->total_amount = $total_amount;
                $new_order->management_date = null;
                $new_order->invoice_date = null;
                $new_order->invoice_issuance_date = null;
                $new_order->invoice_number = null;
                $new_order->credit_note_number = null;
                $new_order->credit_note_date = null;
                $new_order->user_identity_type = null;
                $new_order->user_identity_number = null;
                $new_order->user_business_name = null;
                $new_order->planning_route = null;
                $new_order->planning_sequence = null;
                $new_order->planning_duration = null;
                $new_order->planning_distance = null;
                $new_order->planning_date = null;
                $new_order->allocated_date = null;
                $new_order->received_date = null;
                $new_order->picking_dry_start_date = null;
                $new_order->picking_dry_end_date = null;
                $new_order->picking_cold_start_date = null;
                $new_order->picking_cold_end_date = null;
                $new_order->picking_date = null;
                $new_order->picking_baskets = null;
                $new_order->picking_bags = null;
                $new_order->dispatched_date = null;
                $new_order->ontheway_notification_date = null;
                $new_order->onmyway_date = null;
                $new_order->arrived_date = null;
                $new_order->estimated_arrival_date = null;
                $new_order->coupon = null;
                $new_order->payment_method = $order->payment_method === 'Débito - PSE' ? $order->payment_method : 'Efectivo';
                $new_order->payment_date = $order->payment_method === 'Débito - PSE' ? $order->payment_date : null;
                $new_order->cc_charge_id = null;
                $new_order->cc_refund_date = null;
                $new_order->cc_token = null;
                $new_order->cc_holder_name = null;
                $new_order->cc_last_four = null;
                $new_order->cc_type = null;
                $new_order->cc_country = null;
                $new_order->cc_installments = null;
                $new_order->allow_early_delivery = 0;
                $new_order->date = date("Y-m-d H:i:s");
                $new_order->product_return_storage = 0;
                $new_order->complaint_reason = $complaint_reason;
                $new_order->revision_user_id = null;
                $new_order->is_checked = null;
                $new_order->save();

                //order products
                foreach($products as $key => $product)
                {
                    $base_product = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                        ->where('store_products.id', $product['store_product_id'])
                        ->where('store_id', $order->store_id)
                        ->select('products.*', 'store_products.*')->first();
                    if ( $product['product_table'] == 'order_products' ) {
                        $order_product = OrderProduct::where('store_id', $order->store_id)->where('order_id', $order->id)->where('store_product_id', $product['store_product_id'])->first();
                        $order_product = $order_product->replicate();
                        $order_product->type = ($base_product->type == 'Simple' ? 'Product' : $base_product->type);
                        $order_product->type = ($product['is_picking_up'] ? 'Recogida' : $order_product->type);
                        $order_product->parent_id = 0;
                        $order_product->sampling_id = null;
                        $order_product->is_gift = 0;
                        $order_product->order_id = $new_order->id;
                        $order_product->price = $product['complaint_price'];
                        $order_product->quantity_original = $product['quantity'];
                        $order_product->quantity = $product['quantity'];
                        $order_product->fulfilment_status = $product['complaint_status'];
                        $order_product->reason_credit_note = null;
                        $order_product->quantity_credit_note = null;
                        $order_product->save();
                    }else{
                        $order_product = new OrderProduct;
                        $order_product->store_id = $order->store_id;
                        $order_product->order_id = $new_order->id;
                        $order_product->store_product_id = $base_product->id;
                        // $order_product->price = ( !empty($base_product->special_price) ? $base_product->special_price : $base_product->price );
                        $order_product->price = $product['complaint_price'];
                        $order_product->original_price = $base_product->price;
                        $order_product->base_price = $base_product->base_price;
                        $order_product->iva = $base_product->iva;
                        $order_product->quantity_original = $product['quantity'];
                        $order_product->quantity = $product['quantity'];
                        $order_product->provider_discount = $product['provider_discount'];
                        $order_product->merqueo_discount = $product['merqueo_discount'];
                        $order_product->seller_discount = $product['seller_discount'];
                        $order_product->fulfilment_status = $product['complaint_status'];
                        $order_product->type = ($base_product->type == 'Simple' ? 'Product' : $base_product->type);
                        $order_product->type = ($product['is_picking_up'] ? 'Recogida' : $order_product->type);
                        $order_product->parent_id = 0;
                        $order_product->sampling_id = null;
                        $order_product->is_gift = 0;
                        // $order_product->parent_id = 'Product';
                        $order_product->product_name = $base_product->name;
                        $order_product->product_image_url = $base_product->image_medium_url;
                        $order_product->product_quantity = $base_product->quantity;
                        $order_product->product_unit = $base_product->unit;
                        $order_product->reference = $base_product->reference;
                        $order_product->save();
                    }

                    //guardar producto agrupado
                    if ($base_product->type == 'Agrupado') {
                        $store_products_group = StoreProductGroup::select('products.*', 'store_products.*', 'store_product_group_id', 'store_product_group.price AS group_price', 'store_product_group.quantity AS group_quantity')
                            ->join('store_products', 'store_product_group.store_product_group_id', '=', 'store_products.id')
                            ->join('products', 'store_products.product_id', '=', 'products.id')
                            ->where('store_product_id', $base_product->id)
                            ->get();
                        foreach($store_products_group as $store_product_group) {
                            $order_product_group = new OrderProductGroup;
                            $order_product_group->order_id = $order_product->order_id;
                            $order_product_group->order_product_id = $order_product->id;
                            $order_product_group->store_product_id = $store_product_group->store_product_group_id;
                            $order_product_group->fulfilment_status = 'Pending';
                            $order_product_group->product_name = $store_product_group->name;
                            $order_product_group->product_quantity = $store_product_group->quantity;
                            $order_product_group->product_unit = $store_product_group->unit;
                            $order_product_group->quantity_original = $order_product->quantity * $store_product_group->group_quantity;
                            $order_product_group->quantity = $order_product->quantity * $store_product_group->group_quantity;
                            $order_product_group->price = ($order_product->price == 0) ? 0 : $store_product_group->group_price;
                            $order_product_group->reason_credit_note = null;
                            $order_product_group->quantity_credit_note = null;
                            $order_product_group->base_price = $store_product_group->base_price;
                            $order_product_group->iva = $store_product_group->iva;
                            $order_product_group->product_image_url = $store_product_group->image_medium_url;
                            $order_product_group->save();
                        }
                    }
                }
                if(!empty($products_pickin_up)){
                    foreach($products_pickin_up as $key => $product)
                    {
                        $base_product = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                            ->where('store_products.id', $product['store_product_id'])
                            ->where('store_id', $order->store_id)
                            ->select('products.*', 'store_products.*')->first();
                        if ( $product['product_table'] == 'order_products' ) {
                            $order_product = OrderProduct::where('store_id', $order->store_id)->where('order_id', $order->id)->where('store_product_id', $product['store_product_id'])->first();
                            $order_product = $order_product->replicate();
                            $order_product->type = ($base_product->type == 'Simple' ? 'Product' : $base_product->type);
                            $order_product->type = ($product['is_picking_up'] ? 'Recogida' : $order_product->type);
                            $order_product->order_id = $new_order->id;
                            $order_product->price = $product['complaint_price'];
                            $order_product->quantity = $product['quantity'];
                            $order_product->fulfilment_status = $product['complaint_status'];
                            $order_product->reason_credit_note = null;
                            $order_product->quantity_credit_note = null;
                            $order_product->save();
                        }else{
                            $order_product = new OrderProduct;
                            $order_product->store_id = $order->store_id;
                            $order_product->order_id = $new_order->id;
                            $order_product->store_product_id = $base_product->id;
                            // $order_product->price = ( !empty($base_product->special_price) ? $base_product->special_price : $base_product->price );
                            $order_product->price = $product['complaint_price'];
                            $order_product->original_price = $base_product->price;
                            $order_product->base_price = $base_product->base_price;
                            $order_product->iva = $base_product->iva;
                            $order_product->quantity_original = $product['quantity'];
                            $order_product->quantity = $product['quantity'];
                            $order_product->provider_discount = $product['provider_discount'];
                            $order_product->merqueo_discount = $product['merqueo_discount'];
                            $order_product->seller_discount = $product['seller_discount'];
                            $order_product->fulfilment_status = $product['complaint_status'];
                            $order_product->type = ($base_product->type == 'Simple' ? 'Product' : $base_product->type);
                            $order_product->type = ($product['is_picking_up'] ? 'Recogida' : $order_product->type);
                            // $order_product->parent_id = 'Product';
                            $order_product->product_name = $base_product->name;
                            $order_product->product_image_url = $base_product->image_medium_url;
                            $order_product->product_quantity = $base_product->quantity;
                            $order_product->product_unit = $base_product->unit;
                            $order_product->reference = $base_product->reference;
                            $order_product->save();
                        }
                    }
                }

                $new_order->updateTotals();
                $new_order->save();

                // para productos pagados con PSE
                if ($order->payment_method === 'Débito - PSE' && $order->order_validation_reason_id === 6) {
                    Order::whereId($order->id)->update(['order_validation_reason_id' => null]);
                    $log = OrderLog::create([
                        'type'     => "Se genera pedido de reclamo.",
                        'admin_id' => AuthBackend::user()->id,
                        'order_id' => $id,
                    ]);
                }

                //log de pedido
                $log = new OrderLog();
                $log->type = 'Pedido creado.';
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $new_order->id;
                $log->save();

                Session::forget('admin_complaint_orders');

                return Redirect::route('adminOrderStorage.details', ['id' => $new_order->id]);
            }
            return Redirect::back()->withInput()->with('error', 'No se han agregado productos a la orden.');
        }
    }

    public function update_complaint_product_status_ajax()
    {
        $order_id = Input::get('order_id');
        $product_id = Input::get('product_id');
        $status = Input::get('status');
        $store_id = Input::get('store_id');
        $store = Store::find($store_id);

        $order = Order::find($order_id);
        if( Session::has('admin_complaint_orders.'.$order_id.'.products.'.$product_id)){
            $product = Session::get('admin_complaint_orders.'.$order_id.'.products.'.$product_id);
            $product['complaint_status'] = $status;
            Session::put('admin_complaint_orders.'.$order_id.'.products.'.$product_id, $product);
        }
        if( Session::has('admin_complaint_orders.'.$order_id.'.products_pickup.'.$product_id)){
            $product = Session::get('admin_complaint_orders.'.$order_id.'.products_pickup.'.$product_id);
            $product['complaint_status'] = $status;
            Session::put('admin_complaint_orders.'.$order_id.'.products_pickup.'.$product_id, $product);
        }

        $products = $this->get_products_from_session($order_id, $store_id);
        $data = [
            'order'    => $order,
            'order_id' => $order_id,
            'products' => $products['products'],
            'products' => $products['products'],
            'readonly' => true,
            'store'    => $store,
            'store_id' => $store_id
        ];
        $html = View::make('admin.orders_complaint.details', $data)->renderSections()['product-table'];
        $html_pickup = View::make('admin.orders_complaint.details', $data)->renderSections()['product-pickup-table'];
        return Response::json(['success' => 'Producto correctamente actualizado.', 'html' => $html, 'html_pickup'=> $html_pickup]);
    }
}

