<?php

namespace admin;

use Carbon\Carbon;
use DB, View, Input, Session, Request, City, Store, Order, Vehicle;
use Illuminate\Support\Facades\Response;


class AdminVehicleTracingController extends AdminController
{
    private $shift_delivery_windows;
    /**
     * Grilla de rutas del dia
     */
    public function index()
    {
        $this->shift_delivery_windows = [
            '5:00 am - 7:00 am',
            '7:00 am - 9:00 am',
            '9:00 am - 11:00 am',
            '11:00 am - 2:00 pm',
            '2:00 pm - 5:00 pm',
            '6:00 pm - 10:00 pm',
            '6:00 pm - 8:00 pm',
            '8:00 pm - 10:00 pm'
        ];

        if (!Request::ajax()) {
            return View::make('admin.vehicles_tracing.index')
                ->with('title', 'Seguimiento de Vehículos')
                ->with('cities', $this->get_cities());
        }else {
            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $vehicles = Order::join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id')
                ->join('order_groups', 'order_groups.id','=', 'orders.group_id')
                ->join('routes', 'routes.id','=', 'orders.route_id')
                ->join('drivers', 'drivers.id','=', 'orders.driver_id')
                ->join('transporters', 'transporters.id','=', 'vehicles.transporter_id')
                ->where('routes.city_id', $city_id)
                ->whereRaw(DB::raw("orders.delivery_date >= '".date('Y-m-d')."'"))
                ->select('vehicles.id', 'vehicles.plate', 'orders.route_id', DB::raw('CONCAT(drivers.first_name, " ", drivers.last_name) AS driver') ,'routes.route', 'order_groups.user_city_id', 'route', 'plate', 'transporters.fullname','delivery_time',
                    DB::raw("MAX(DATE_FORMAT(orders.dispatched_date, '%h:%i %p')) AS hour_dispatched"),
                    DB::raw('COUNT(orders.id) AS orders_assigned'),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[0]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_5_7"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[1]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_7_9"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[2]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_9_11"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[3]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_11_14"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[4]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_14_17"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[5]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_18_22"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[6]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_18_20"),
                    DB::raw("SUM(CASE WHEN orders.delivery_time = '".$this->shift_delivery_windows[7]."' AND orders.status = 'Dispatched' THEN 1 ELSE 0 END) AS hour_20_22"),
                    DB::raw('SUM(CASE WHEN orders.status = "Delivered" THEN 1 ELSE 0 END) AS total_orders_delivered'),
                    DB::raw('SUM(CASE WHEN orders.status = "Cancelled" THEN 1 ELSE 0 END) AS total_orders_cancelled'),
                    DB::raw('SUM(CASE WHEN orders.status = "Dispatched" THEN 1 ELSE 0 END) AS total_orders_pending'),
                    DB::raw("(SELECT MAX(management_date) FROM orders WHERE vehicles.id = vehicle_id AND route_id = routes.id AND STATUS = 'Delivered') AS last_time_delivered"),
                    DB::raw("(SELECT ROUND(TIME_TO_SEC(TIMEDIFF(MAX(management_date),MIN(management_date))) / COUNT(id)) AS average_second FROM orders WHERE vehicles.id = vehicle_id AND route_id = routes.id AND STATUS = 'Delivered') AS average_delivered_orders"),
                    DB::raw('SUM(CASE WHEN payment_method = "Efectivo" THEN orders.total_amount ELSE 0 END) AS total_amount_route'),
                    DB::raw('SUM(CASE WHEN orders.status = "Delivered" AND payment_method = "Efectivo" THEN orders.total_amount ELSE 0 END) AS total_amount_current_delivered'),
                    DB::raw("(SELECT ROUND(TIME_TO_SEC(TIMEDIFF(MAX(management_date),MIN(management_date))) / COUNT(id)) AS average_time_calculate FROM orders WHERE status = 'Delivered' AND DATE(delivery_date) >= '".date('Y-m-d')."') AS average_delivered_time")
                );

            if ($search != '') {
                $vehicles = $vehicles->where('vehicles.plate', 'LIKE', '%'.$search.'%');
            }

            if (Input::get('route_id')){
                if (Input::get('route_id') != 'NULL')
                    $vehicles->where('routes.id', Input::get('route_id'));
                else $vehicles->whereNull('orders.route_id');
            }
            if(Input::get('transporter_id')){
                $vehicles->where('transporters.id', Input::get('transporter_id'));
            }
            if(Input::get('driver_id')){
                $vehicles->where('drivers.id', Input::get('driver_id'));
            }

            if (Input::has('show_orders') && Input::get('show_orders') == 1) {
                $vehicles->where('orders.status', '<>', 'Dispatched');
            } elseif (Input::has('show_orders') && Input::get('show_orders') == 0) {
                $vehicles->where('orders.status', '=', 'Dispatched');
            }

            $order_by = Input::get('order_by');
            if($order_by == 'plate') {
                $vehicles->orderBy('plate');
            } elseif($order_by == 'status') {
                $vehicles->orderBy("total_orders_pending", 'DESC');
            } elseif($order_by == 'dispatched_date') {
                $vehicles->orderBy("dispatched_date", 'ASC');
            } elseif($order_by == 'last_time') {
                $vehicles->orderBy("last_time_delivered", 'ASC');
            }

            $vehicles = $vehicles->groupBy('orders.vehicle_id', 'orders.route_id')->get();

            return View::make('admin.vehicles_tracing.index')
                ->with('title', 'Seguimiento de Vehículos')
                ->with('cities', $this->get_cities())
                ->with('vehicles', $vehicles)
                ->with('delivery_windows', $this->shift_delivery_windows)
                ->renderSections()['content'];
        }
    }

    /**
     * Mapa de seguimiento en tiempo real de vehiculo
     * @param $city_id
     * @param $vehicle_id
     * @param $route_id
     * @return mixed
     */
    public function map_tracing($city_id, $vehicle_id, $route_id)
    {
        $city_position = City::where('cities.id', $city_id)->select('cities.latitude', 'cities.longitude')->first();
        $vehicle = Vehicle::where('vehicles.id', $vehicle_id)->select('plate')->first();
        $store = City::select('coverage_store_id AS id')->where('id', $city_id)->where('status', 1)->first();
        $cities = City::select('id')->where('coverage_store_id', $store->id)->where('status', 1)->get()->toArray();
        foreach ($cities as $key => &$value)
            $cities[$key] = $value['id'];
        $orders = Order::join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('routes', 'routes.id', '=', 'orders.route_id')
            ->leftjoin('user_address', 'user_address.id', '=', 'order_groups.address_id')
            ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
            ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->whereIn('order_groups.user_city_id', $cities)
            ->where('orders.vehicle_id', $vehicle_id)
            ->where('orders.route_id', $route_id)
            ->select('orders.id',
                    'orders.route_id',
                    'routes.route',
                    'orders.delivery_time',
                    'orders.status',
                    'orders.management_date',
                    'orders.planning_sequence',
                    'order_groups.user_address',
                    'order_groups.user_address_neighborhood',
                    'order_groups.user_address_latitude',
                    'order_groups.user_address_longitude',
                    'zones.name AS zone')
            ->get();
        return View::make('admin.vehicles_tracing.tracing_map')
            ->with('vehicle_id', $vehicle_id)
            ->with('orders', $orders)
            ->with('plate', $vehicle->plate)
            ->with('position', $city_position)
            ->with('title', 'Mapa Seguimiento de Vehículo')
            ->with('city_id', $city_id);
    }

    /**
     * @param $city_id
     * @param $vehicle_id
     * @return View
     */
    public function map_tracing_record($city_id, $vehicle_id)
    {
        $vehicle = Vehicle::where('vehicles.id', $vehicle_id)->select('plate')->first();
        $city_position = City::where('cities.id', $city_id)->select('cities.latitude', 'cities.longitude')->first();
        return View::make('admin.vehicles_tracing.tracing_map_record')
            ->with('vehicle_id', $vehicle_id)
            ->with('plate', $vehicle->plate)
            ->with('position', $city_position)
            ->with('title', 'Historial Mapa Seguimiento de Vehículo');
    }

    public function get_transporters(){
        /*if(!empty(Input::get('route_id'))){
            $route_id = Input::get('route_id');
            $transpoters = \Transporter::select('transporters.id','transporters.fullname')
                ->join('drivers', 'drivers.transporter_id', '=', 'transporters.id')
                ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                ->join('routes', 'routes.id', '=', 'orders.route_id')
                ->where('routes.id', $route_id)
                ->groupBy('transporters.id')
                ->get();
        } else {*/
            $transpoters = \Transporter::select('transporters.id','transporters.fullname')
                ->join('vehicles', 'vehicles.transporter_id', '=', 'transporters.id')
                ->join('orders', 'orders.vehicle_id', '=', 'vehicles.id')
                ->where('transporters.city_id', Input::get('city_id'))
                /*->where('orders.status', 'Dispatched')
                ->whereBetween('delivery_date', [Carbon::now()->format('Y-m-d') . ' 00:00:00', Carbon::now()->format('Y-m-d') . ' 23:59:59'])*/
                ->groupBy('transporters.id')
                ->get();
        //}
        $data = ['transporters' => $transpoters];
        return Response::json($data);
    }

    public function get_drivers(){
        $transporter_id = Input::get('transporter_id');
        $drivers = \Driver::select('id', \DB::raw('CONCAT(first_name, " " , last_name) as fullname'))
            ->where('transporter_id', $transporter_id)
            ->get();
        $data = ['drivers' => $drivers];
        return Response::json($data);
    }
}