<?php

namespace admin;

use Request, Input, Session, View, Redirect, Config, DB, Menu, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007, Order, OrderGroup,
    Store, Warehouse, Response;

class AdminReportController extends AdminController {

	public function __construct()
    {
	    if (!Session::has('admin_id')){
		    return Redirect::route('admin.login');
		} else {
			// Pending orders
			$request = get_current_request();
            $actions = array();
            if (Session::get('admin_designation') != 'Admin' && in_array($request['action'], $actions)){
                return Redirect::route('admin.dashboard')->with('error', 'You do not have permission access to this module.');
            }
		}
    }

	/**
	 * Reporte de pedidos
	 */
	public function orders()
	{
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('is_hidden', 0)->where('stores.status', 1)->where('city_id', 1)->get();
        $cities = $this->get_cities();
        $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->where('status', 1)->get();

	    View::composer('admin.layout', function($view)
        {
            $menu = Menu::find(1);
            $menu_items = json_decode($menu->menu_order, true);
            $view->with('menu', $menu_items);
        });

		ini_set('memory_limit', '1024M');
        set_time_limit(0);

		if (Request::isMethod('post') && Input::has('start_date') && Input::has('end_date')) {
			$start_date = format_date('mysql', Input::get('start_date'));
			$end_date = format_date('mysql', Input::get('end_date'));

            $where = "";
            if(Input::has('city_id')) {
                $cities = implode(",", $this->get_all_city_ids(Input::get('city_id')));
                if(!$cities)
                    $cities = Input::get('city_id');

                $where.= " AND c.id in (".$cities.") ";
            }

            if(Input::has('store_id'))
                $where.= " AND st.id = ".Input::get('store_id');

			if ($start_date > $end_date)
				return Redirect::route('adminReports.orders')->with('error', 'Start Date must be less than End Date.');

            if(Input::has('warehouse_id'))
                $where.= " AND w.id = ".Input::get('warehouse_id');

			DB::statement("SET lc_time_names = 'es_ES'");
			$sql = "SELECT
                    o.id AS PEDIDO,
                    o.user_id AS user_id,
                    o.date AS 'fecha_pedido',
                    w.warehouse AS 'BODEGA',
                    o.status AS ESTADO,
                    CONCAT(user_firstname, ' ', user_lastname) AS CLIENTE,
                    user_email AS EMAIL,
                    CONCAT(s_dry.first_name, ' ', s_dry.last_name) AS 'ALISTADOR EN SECO',
                    CONCAT(s_cold.first_name, ' ', s_cold.last_name) AS 'ALISTADOR EN FRIO',
                    tr.fullname AS TRANSPORTADOR,
                    CONCAT(dr.first_name, ' ',dr.last_name) AS CONDUCTOR,
                    vh.plate AS 'PLACA VEHÍCULO',
                    r.route AS RUTA,
                    o.planning_route AS 'RUTA VIEJA',
                    o.planning_sequence AS SECUENCIA,
                    o.planning_duration AS TIEMPO,
                    o.planning_distance AS DISTANCIA,
                    c.city AS 'CIUDAD',
                    user_address AS DIRECCION,
                    og.user_address_latitude AS LATITUD,
                    og.user_address_longitude AS LONGITUD,
                    z.name AS ZONA,
                    user_phone AS TELEFONO,
                    st.name AS TIENDA, source AS 'ORIGEN PEDIDO',
                    total_products AS 'CANTIDAD DE PRODUCTOS',
                    SUM(IF(fulfilment_status = 'Not Available', 1, 0)) AS 'PRODUCTOS NO DISPONIBLES',
                    payment_method AS 'METODO DE PAGO',
                    cc_charge_id AS 'CARGO ID',
                    o.total_amount AS SUBTOTAL,
                    o.delivery_amount AS DOMICILIO,
                    o.discount_amount AS DESCUENTO,
                    (o.total_amount + o.delivery_amount - o.discount_amount) AS TOTAL,
                    SUM(IF (op.promo_type = 'Merqueo' OR op.promo_type IS NULL OR op.promo_type = '', (op.original_price * op.quantity) - (op.price * op.quantity), 0)) AS 'DESCUENTO PROMOCIONES MERQUEO',
                    SUM(IF (op.promo_type = 'Cliente', (op.original_price * op.quantity) - (op.price * op.quantity), 0)) AS 'DESCUENTO PROMOCIONES CLIENTE',
                    DATE_FORMAT(o.date, '%d/%m/%Y') AS 'FECHA PEDIDO',
                    DATE_FORMAT(o.date, '%H:%i') AS 'HORA PEDIDO',
                    CONCAT(UCASE(LEFT(DATE_FORMAT(o.date, '%W'), 1)), SUBSTRING(DATE_FORMAT(o.date, '%W'), 2)) AS 'DIA PEDIDO',
                    DATE_FORMAT(first_delivery_date, '%d/%m/%Y') AS 'PRIMERA FECHA DE ENTREGA',
                    DATE_FORMAT(first_delivery_date, '%H:%i') AS 'HORA PRIMERA FECHA DE ENTREGA',
                    DATE_FORMAT(allocated_date, '%d/%m/%Y') AS 'FECHA ASIGNACION',
                    DATE_FORMAT(allocated_date, '%H:%i') AS 'HORA ASIGNACION',
                    DATE_FORMAT(o.planning_date, '%d/%m/%Y') AS 'FECHA PLANEACIÓN',
                    DATE_FORMAT(o.planning_date, '%H:%i') AS 'HORA PLANEACIÓN',
                    DATE_FORMAT(o.picking_date, '%d/%m/%Y') AS 'FECHA ALISTAMIENTO',
                    DATE_FORMAT(o.picking_date, '%H:%i') AS 'HORA ALISTAMIENTO',
                    DATE_FORMAT(picking_dry_start_date, '%d/%m/%Y') AS 'FECHA DE INICIO DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_dry_start_date, '%H:%i') AS 'HORA DE INICIO DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_dry_end_date, '%d/%m/%Y') AS 'FECHA DE FIN DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_dry_end_date, '%H:%i') AS 'HORA DE FIN DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_cold_start_date, '%d/%m/%Y') AS 'FECHA DE INICIO DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_cold_start_date, '%H:%i') AS 'HORA DE INICIO DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_cold_end_date, '%d/%m/%Y') AS 'FECHA DE FIN DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_cold_end_date, '%H:%i') AS 'HORA DE FIN DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_revision_date, '%d/%m/%Y') AS 'FECHA DE REVISION DE ALISTAMIENTO',
                    DATE_FORMAT(picking_revision_date, '%H:%i') AS 'HORA DE REVISION DE ALISTAMIENTO',
                    DATE_FORMAT(dispatched_date, '%d/%m/%Y') AS 'FECHA DESPACHO',
                    DATE_FORMAT(dispatched_date, '%H:%i') AS 'HORA DESPACHO',
                    DATE_FORMAT(management_date, '%d/%m/%Y') AS 'FECHA GESTION',
                    DATE_FORMAT(management_date, '%H:%i') AS 'HORA GESTION',
                    DATE_FORMAT(delivery_date, '%d/%m/%Y') AS 'FECHA ENTREGA',
                    DATE_FORMAT(delivery_date, '%H:%i') AS 'HORA ENTREGA',
                    DATE_FORMAT(real_delivery_date, '%d/%m/%Y') AS 'FECHA ENTREGA REAL',
                    DATE_FORMAT(real_delivery_date, '%H:%i') AS 'HORA ENTREGA REAL',
                    reject_reason AS 'MOTIVO DE RECHAZO',
                    IF(product_return_storage = 1, 'Sí', 'No') AS 'CON DEVOLUCION',
                    user_score AS 'CALIFICACION SHOPPER',
                    user_score_comments AS 'COMENTARIO CALIFICACION',
                    o.voucher_image_url AS 'IMAGEN VOUCHER URL',
                    IF (is_checked = 1, 'Revisado', 'No revisado') AS REVISION,
                    admin.fullname AS 'USUARIO REVISION',
                    reject_comments AS 'COMENTARIOS DE CANCELACION',
                    o.invoice_number AS 'NÚMERO DE FACTURA',
                    IF(o.invoice_cancelled = 1,'ANULADA','OK') AS 'ESTADO DE FACTURA',
                    DATE_FORMAT(o.invoice_date, '%d/%m/%Y') AS 'FECHA EMISION FACTURA'
                FROM
                    order_products op
                INNER JOIN orders o ON op.order_id = o.id
                INNER JOIN stores st ON o.store_id = st.id
                INNER JOIN order_groups og ON o.group_id = og.id
                INNER JOIN cities c ON og.user_city_id = c.id
                LEFT JOIN pickers s_cold ON o.picker_cold_id = s_cold.id
                LEFT JOIN pickers s_dry ON o.picker_dry_id = s_dry.id
                LEFT JOIN admin ON o.revision_user_id = admin.id
                LEFT JOIN zones z ON og.zone_id = z.id
                LEFT JOIN drivers dr ON dr.id = o.driver_id
                LEFT JOIN vehicles vh ON vh.id = o.vehicle_id                
                LEFT JOIN transporters tr ON tr.id = vh.transporter_id
                LEFT JOIN routes r ON o.route_id = r.id
                LEFT JOIN warehouses w ON og.warehouse_id = w.id
                WHERE DATE(o.date) BETWEEN DATE('".$start_date."') AND DATE('".$end_date."') ".$where."
                GROUP BY o.id
                ORDER BY o.date DESC;";
			$rows = DB::select(DB::raw($sql));

            if (count($rows)) {
                foreach($rows as &$order){
                    //obtener numero de pedidos por cliente
                    $qty = Order::where('user_id', $order->user_id)->where('user_id', '<>', 5169)->where('status', '=', 'Delivered')->whereRaw('DATE(date) <= DATE("'. $order->fecha_pedido .'")')->count();
                    $order->CANTIDAD_PEDIDOS = $qty;
                    //obtener numero de pedidos agrupados por cliente
                    $row = OrderGroup::select(DB::raw('COUNT(DISTINCT(order_groups.id)) AS qty'))
                                       ->join('orders', 'order_groups.id', '=', 'group_id')->where('orders.user_id', $order->user_id)
                                       ->where('orders.user_id', '<>', 5169)->where('orders.status', '=', 'Delivered')->whereRaw('DATE(orders.date) <= DATE("'. $order->fecha_pedido .'")')->groupBy('orders.user_id')->first();
                    $order->CANTIDAD_PEDIDOS_AGRUPADOS = $row ? $row->qty : 0;
                    unset($order->fecha_pedido);
                    unset($order->user_id);
                }
            }
			if ($n = count($rows))
			{
				//eliminar archivo
                $path = public_path(Config::get('app.download_directory'));
                if ($gestor = opendir($path)){
                    while (false !== ($file = readdir($gestor))){
                        if (strstr($file, Session::get('admin_username')))
                            remove_file($path.$file);
                    }
                }

				$objPHPExcel = new PHPExcel();
	            //propiedades
	            $objPHPExcel->getProperties()->setCreator('Merqueo');
	            $objPHPExcel->getProperties()->setTitle('Pedidos');
	            $objPHPExcel->getProperties()->setSubject('Pedidos');
				$objPHPExcel->setActiveSheetIndex(0);

	            //estilos
	            $style_header = array(
	                'fill' => array(
	                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                    'color' => array('rgb'=>'5178A5'),
	                ),
	                'font' => array(
	                    'color' => array('rgb'=>'FFFFFF'),
	                    'bold' => true,
	                )
	            );
				$objPHPExcel->getActiveSheet()->getStyle('A1:BW1')->applyFromArray($style_header);

				//titulos
				$i = 0;
				foreach ($rows[0] as $key => $value) {
					$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
					$i++;
				}

				$j = 2;
				foreach($rows as $row){
					$i = 0;
					foreach ($row as $key => $value){
						$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
						$i++;
					}
					$j++;
				}

				 //crear archivo
                $objPHPExcel->getActiveSheet()->setTitle('Pedidos');
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				$filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
                $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
				$file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
                $file['client_original_name'] = $filename;
                $file['client_original_extension'] = 'xlsx';
                $url = upload_image($file, false, Config::get('app.download_directory_temp'));

				return Redirect::route('adminReports.orders')->with('success', 'Report generated successfully.')->with('file_url', $url);

			}else return Redirect::route('adminReports.orders')->with('error', 'No Data Found.');

		}

		return View::make('admin.reports.orders')
                    ->with('title', 'Reportes')
                    ->with('sub_title', 'Pedidos')
                    ->with('cities', $cities)
                    ->with('warehouses', $warehouses)
                    ->with('stores', $stores);
	}

    /**
     * Reporte de facturas
     */
    public function invoices()
    {
        View::composer('admin.layout', function($view)
        {
            $menu = Menu::find(1);
            $menu_items = json_decode($menu->menu_order, true);
            $view->with('menu', $menu_items);
        });

        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        if (Request::isMethod('post') && Input::has('start_date') && Input::has('end_date')){
            $start_date = format_date('mysql', Input::get('start_date'));
            $end_date = format_date('mysql', Input::get('end_date'));

            if ($start_date > $end_date)
                return Redirect::route('adminReports.invoices')->with('error', 'Start Date must be less than End Date.');

            DB::statement("SET lc_time_names = 'es_ES'");
            $sql = "SELECT
                    o.id AS ORDEN,
                    IF(o.user_business_name <> '',o.user_business_name,IF(u.business_name <> '',u.business_name,'CLIENTES GENERALES')) AS 'NOMBRE O RAZÓN SOCIAL',
                    IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number,'222.222.222')) AS 'NIT O CÉDULA',
                    og.user_address AS DIRECCION,
                    DATE_FORMAT(o.delivery_date, '%d/%m/%Y') AS 'FECHA DE ENTREGA',
                    DATE_FORMAT(o.invoice_date, '%d/%m/%Y') AS 'FECHA EMISION FACTURA',
                    CONVERT(SUBSTRING(o.invoice_number FROM 4),UNSIGNED INTEGER) AS CONSECUTIVO,
                    o.invoice_number AS 'NÚMERO DE FACTURA',
                    IF(o.invoice_cancelled = 1,'ANULADA',IF(o.invoice_number IS NOT NULL,'OK','')) AS 'ESTADO DE FACTURA',
                    og.user_phone AS TELEFONO,
                    IF(og.source = 'Callcenter', (os.total_real_amount - o.discount_amount - o.delivery_amount), o.total_amount) AS 'TOTAL DE LA COMPRA',
                    o.discount_amount AS 'VALOR DEL DESCUENTO',
                    IF(og.source = 'Callcenter', (os.total_real_amount - o.discount_amount - o.delivery_amount), (o.total_amount - o.discount_amount)) AS SUBTOTAL,
                    ROUND((o.delivery_amount/1.16)) AS BASE,
                    ROUND((ROUND((o.delivery_amount/1.16)))*0.16) AS IVA,
                    IF(og.source = 'Callcenter', os.total_real_amount, (ROUND((ROUND((o.delivery_amount/1.16)))*0.16) + (o.total_amount - o.discount_amount) + ROUND((o.delivery_amount/1.16)))) AS TOTAL
                    FROM
                    order_products op
                    INNER JOIN orders o ON op.order_id = o.id
                    INNER JOIN stores st ON o.store_id = st.id
                    INNER JOIN order_groups og ON o.group_id = og.id
                    LEFT JOIN shoppers s ON o.shopper_id = s.id
                    LEFT JOIN admin ON o.revision_user_id = admin.id
                    LEFT JOIN zones z ON og.zone_id = z.id
                    LEFT JOIN order_stores os ON os.order_id = o.id
                    INNER JOIN users u ON og.user_id = u.id
                    WHERE o.date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59'
                    GROUP BY o.id
                    ORDER BY CONSECUTIVO";
            $rows = DB::select(DB::raw($sql));
            if ($n = count($rows))
            {
                //eliminar archivo
                $path = public_path(Config::get('app.download_directory'));
                if ($gestor = opendir($path)){
                    while (false !== ($file = readdir($gestor))){
                        if (strstr($file, Session::get('admin_username')))
                            remove_file($path.$file);
                    }
                }

                $objPHPExcel = new PHPExcel();
                //propiedades
                $objPHPExcel->getProperties()->setCreator('Merqueo');
                $objPHPExcel->getProperties()->setTitle('Pedidos');
                $objPHPExcel->getProperties()->setSubject('Pedidos');
                $objPHPExcel->setActiveSheetIndex(0);

                //estilos
                $style_header = array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb'=>'5178A5'),
                    ),
                    'font' => array(
                        'color' => array('rgb'=>'FFFFFF'),
                        'bold' => true,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle('A1:AR1')->applyFromArray($style_header);

                //titulos
                $i = 0;
                foreach ($rows[0] as $key => $value) {
                    if($key != 'CONSECUTIVO'){
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                        $i++;
                    }
                }

                $j = 2;
                foreach($rows as $row){
                    $i = 0;
                    foreach ($row as $key => $value){
                        if($key != 'CONSECUTIVO'){
                            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                            $i++;
                        }
                    }
                    $j++;
                }

                 //crear archivo
                $objPHPExcel->getActiveSheet()->setTitle('Pedidos');
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
                $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
                $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
                $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
                $file['client_original_name'] = $filename;
                $file['client_original_extension'] = 'xlsx';
                $url = upload_image($file, false, Config::get('app.download_directory_temp'));

                return Redirect::route('adminReports.invoices')->with('success', 'Report generated successfully.')->with('file_url', $url);

            }else return Redirect::route('adminReports.invoices')->with('error', 'No Data Found.');

        }
        return View::make('admin.reports.invoices')->with('title', 'Reports')->with('sub_title', 'Invoices');
    }

    /**
     * Funcion ajax para obtener el listado de tiendas por bodega
     */
    public function get_stores_by_city_ajax()
    {
        $status = 400;
        if (Input::has('city_id'))
        {
            $stores = Store::select('id', 'name')->where('city_id', Input::get('city_id'))->where('is_hidden', 0)->where('status', 1)->get();
            $response['message'] = 'Tiendas Merqueo';
            $status = 200;
            $response['status'] = $status;
            $response['result']['stores'] = $stores;
        }else {
            $response['result']['stores'] = [];
            $response['message'] = 'Fallo al cargar las tiendas';
        }
        return Response::json($response , $status);
    }
}
