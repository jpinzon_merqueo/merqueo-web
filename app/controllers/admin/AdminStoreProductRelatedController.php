<?php
namespace admin;

use Request, View, Input, Redirect, Session, DB, Str, Store, Response, Config,  Route, PHPExcel, PHPExcel\IOFactory, PHPExcel_Cell, PHPExcel_Reader_Excel2007, Exception, StoreProduct, StoreProductRelated, Warehouse;

class AdminStoreProductRelatedController extends AdminController
{
	/**
     * Grilla de productos relacionados
     */
	public function index()
	{
		if (!Request::ajax()) {
			$status = StoreProductRelated::getStatus();
            return View::make( 'admin.store_product_related.index' )
            			->with( 'title', 'Productos Relacionados' )
            			->with( 'cities', $this->get_cities() )
            			->with( 'status', $status );
        }else{
            $search = Input::has('s') ? Input::get('s') : '';
            $warehouse_id = Input::has('warehouse_id') ? Input::get('warehouse_id') : '';
            $store_product_related = StoreProductRelated::getStoreProductRelatedList( $search, $warehouse_id );
        }

        return View::make( 'admin.store_product_related.index' )
                    ->with( 'title', 'Productos Relacionados' )
                    ->with( 'cities', $this->get_cities() )
            		->with( 'list', $store_product_related )->renderSections()['content'];
	}

    /**
     * Acceso a la vista de edicón de productos relacionados
     * @param int $id contiene el store_product_id del producto principal
     * @return View
     */
	public function edit( $id = 0 )
	{
		$sub_title = $id ? 'Editar Productos relacionados' : 'Nuevo Producto relacionado';

        if (!$id && Session::has('post')){
        	    
        }

        $product = StoreProductRelated::getProduct( $id );

        $data = [
            'title' => 'Productos Relacionados',
            'sub_title' => $sub_title,
            'product' => $product,
            'id' => $id
        ];

        return View::make('admin.store_product_related.related_form', $data);
	}

	/**
     * Funcion que actualiza la prioridad de los productos que estan relacionados
     */
	public function update_priority()
	{
		return StoreProductRelated::updatePriority( Input::get() );
	}

	/**
     * Funcion que añade un nuevo producto a la relación
     */
	public function add_related()
	{

		if ( Request::ajax() ){

			$rows = array();
			$errors = array();
			$row = [
				'priority' => Input::get('priority'),
				'store_product_related_id' => Input::get('store_product_related_id'),
				'store_product_id' => Input::get('store_product_id'),
				'name_product_related' => Input::get('name_product_related'),
				'name_product' => Input::get('name_product')
			];

			try {

	            $error = StoreProductRelated::validate( $row );

	            if( $error != '' ){
	            	return Response::json( [ 'result' => false, 'msg' =>  $error  ] );
	            }

	            DB::beginTransaction();
            	
            	$store_product_related = new StoreProductRelated;
            	$store_product_related->store_product_id = $row['store_product_id'];
            	$store_product_related->store_product_related_id = $row['store_product_related_id'];
            	$store_product_related->priority = $row['priority'];
            	$store_product_related->save();
            	
	            DB::commit();
	            $this->admin_log('store_product_related', Session::get('admin_id'), 'añade el producto '.$row['store_product_related_id'].' a la relacion con '.$row['store_product_id']);
	            return Response::json( [ 'result' => true, 'msg' => 'Se ha agregado el producto sin Errores' ] );
	            
	        }catch(\Exception $exception){

	            DB::rollback();
	            
	            return Response::json( [ 'result' => false, 'msg' => 'Ocurrio un error al guardar el producto ::'.$exception->getMessage() ] );
	        }

		}
	}

	/**
     * Funcion que elimina una relación o un producto con sus relaciones
     */
	public function delete( $store_product_id = 0 )
	{
		if ( Request::ajax() ){

			$id = Input::get( 'id' );

			if( StoreProductRelated::where( 'id', '=', $id )->delete() ){
				$this->admin_log('store_product_related', Session::get('admin_id'), 'Elimina producto de la relacion :: Línea '.$id);
				return Response::json( [ 'result' => true, 'msg' => 'Se elimino el producto de la lista' ] );

			}else{

				return Response::json( [ 'result' => false, 'msg' => 'No es posible eliminar el producto de la lista' ] );

			}
		}else{

			if(StoreProductRelated::where('store_product_id', '=', $store_product_id)
						->delete()){
				$this->admin_log('store_product_related', Session::get('admin_id'), 'Elimina producto con sus relaciones :: Producto: '.$store_product_id);
				return Redirect::route('adminStoreProductRelated.index')->with( 'success', 'El producto fue eliminado.' );
			}else{
				return Redirect::route('adminStoreProductRelated.index')->with( 'error', 'No es posible eliminar el producto.' );
			}
		}
	}

	/**
     * Funcion que permite la carga masiva de productos relacionados 
     */
	public function import()
	{
		if (Input::hasFile('file')){

            ini_set('memory_limit', '1024M');
            set_time_limit(0);

            $extensions = array( 'xls', 'xlsx', 'csv' );
            $file = Input::file('file');
            if ( !file_exists( $file->getRealPath() ) )
                return Redirect::back()->with('error', 'No file uploaded');

            
            
            if ( !$warehouse = Warehouse::find(Input::get('warehouse_id')) )
                return Redirect::back()->with('error', 'Bodega invalida');

            $all_related_id_product = StoreProductRelated::getAllIdRelatedProducts($warehouse->id);

            $extension_obj = explode('.', $file->getClientOriginalName() );

            $is_correct_extension = in_array($extension_obj[1], $extensions);
            
            $errors_in_array = array();

            if( $is_correct_extension ){
            	$errors = [];
	            if ( Input::get('process') == 'import_products' ) {
	            	$file_path = $file->getRealPath();
	                $objReader = new PHPExcel_Reader_Excel2007();
	                $objPHPExcel = $objReader->load($file_path);

	                //Establesco la primera hoja como activa
	                $objPHPExcel->setActiveSheetIndex(0);

	                //obtengo el número de filas
	                $rows = $objPHPExcel->getActiveSheet()->getHighestRow();

	                //obtengo el número de columnas
	                $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn()); 
	               	
	              	//arreglo donde se almacenaran los productos que existen en el archivo
	                $products_in_file = [];
	                $products_parent_ids = [];
	                //recorro las filas del archivo para obtener los datos
	                for ( $i = 2; $i <= $rows; $i++ ){
	                	//obtengo el store_product_id del producto padre
	                    $store_product_id = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
	                    if( !empty($store_product_id) ){
		                    $name_product = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
		                    //obtengo la informaciòn del producto padre
	                    	if(in_array($store_product_id, $all_related_id_product)){
	                    		$errors_in_array[$store_product_id] = "- El producto <b>{$name_product}</b> en la fila <b>{$i}</b> ya se encuentra almacenado.";
	                    		continue;
	                    	}
		                    $store_product = StoreProduct::select('store_products.*', 'store_product_warehouses.warehouse_id')
		                    					->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
		                    					->where( 'store_products.id', $store_product_id )
		                    					->where( 'store_product_warehouses.warehouse_id', $warehouse->id )
		                    					->first();
		                    $products_parent_ids[] = $store_product_id;

		                    //obtengo el store_product_id del producto que se va a relacionar
		                    $store_product_related_id = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
		                    $name_product_related = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $i)->getValue();
		                    //obtengo la informacion del producto a relacionar
		                    $store_product_related = StoreProduct::select('store_product_warehouses.warehouse_id', 'products.type')
		                    							->join( 'products', 'products.id', '=', 'store_products.product_id')
				                    					->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
				                    					->where( 'store_products.id', $store_product_related_id )
				                    					->where( 'store_product_warehouses.warehouse_id', $warehouse->id )
				                    					->first();
		                    /*StoreProduct::select('store_products.store_id', 'products.type')
		                   								->where( 'store_products.id', $store_product_related_id )
		                   								->first();*/
		                   	

		                   
		                    $priority = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $i)->getValue();
		                    if( $store_product_related && $store_product_related->type == 'Agrupado' ){
		                    	$errors[] = "- El producto relacionado <b>{$name_product_related}</b> en la fila <b>{$i}</b> es un producto agrupado";
		                    	continue;
		                    }else{
		                    	if( !$store_product_related ){
		                    		$errors[] = "- El producto relacionado <b>{$name_product_related}</b> en la fila <b>{$i}</b> no existe, favor verificar";
		                    		continue;
		                    	}
		                    }
		                    if( $store_product && $store_product->warehouse_id == $warehouse->id && $store_product_related->warehouse_id == $warehouse->id){

			                    $products_in_file[ $store_product_id ][ $store_product_related_id ] = [
			                    		                    	'store_product_id' => $store_product_id,
			                    								'name_product' => $name_product,
			                    		                        'store_product_related_id' => $store_product_related_id,
			                    		                        'name_product_related' => $name_product_related,
			                    		                        'priority' => $priority,
			                    		                        'excel_row' => $i
			                    		                    ];
		                    }else{
		                    	if( !$store_product ){
		                    		$errors[] = "- El código del producto <b>{$name_product}</b> en la fila <b>{$i}</b> no Existe, favor verificar";
		                    		continue;
		                    	}
		                    	if( $store_product && $store_product->warehouse_id != $warehouse->id ){
		                    		$errors[] = "- El código del producto <b>{$name_product}</b> en la fila <b>{$i}</b> no pertenece a la bodega seleccionada";
		                    	}

		                    	if( $store_product && $store_product_related->warehouse_id != $warehouse->id ){
		                    		$errors[] = "- El código del producto relacionado <b>{$name_product_related}</b> en la fila <b>{$i}</b> no pertenece a la bodega seleccionada";
		                    	}
		                    }
	                    }
	                }

	                try {
	                    DB::beginTransaction();
	                    //StoreProductRelated::whereRaw('1=1')->delete();
						//$this->admin_log('store_product_related', Session::get('admin_id'), 'Vaciar tabla');

	                    $store_product_ids =  [];
	                    
	                    foreach ( $products_in_file as $store_product_id => $rows ){
	                    	list( $rows, $errors ) = StoreProductRelated::validateArray( $rows, $errors, $products_parent_ids );
	                    	
	                    	foreach ($rows as $row ) {
		                    	$store_product_related = new StoreProductRelated;
		                    	$store_product_related->store_product_id = $store_product_id;
		                    	$store_product_related->store_product_related_id = $row['store_product_related_id'];
		                    	$store_product_related->priority = $row['priority'];
		                    	$store_product_related->status = 1;
		                    	$store_product_related->warehouse_id = $warehouse->id;
		                    	$store_product_related->save();
		                    	$store_product_ids[$store_product_id] = 1;
	                    	}
	                    	
	                    }
	                    $this->admin_log('store_product_related', Session::get('admin_id'), 'Importar productos relacionados');

	                    DB::commit();
	                    if(!empty($errors_in_array) ){
	                    	$errors[] = implode( '<br />', $errors_in_array );
	                    }

	                    if( empty( $errors ) ){

	                    	return Redirect::route('adminStoreProductRelated.import')->with( 'success', '<br>Se ha cargado '.count($store_product_ids).' registro del archivo de productos ');
	                    }else{
	                    	return Redirect::route('adminStoreProductRelated.import')->with( 'success', '<br>Se ha cargado '.count($store_product_ids).' registro del archivo de productos relacionados con Errores')->with( 'error', '<br />'.implode( '<br />', $errors ) );
	                    }
	                }catch(\Exception $exception){
	                    DB::rollback();
	                    return Redirect::route('adminStoreProductRelated.import')->with( 'error', 'Ocurrio un error al guardar los productos relacionados ' );
	                }

	            }
            }
            
        }else{

			/*$stores = Store::select('stores.*', 'cities.city', 'cities.city AS city_name')
							->join('cities', 'city_id', '=', 'cities.id')
							->where('stores.status', 1)
							->orderBy('stores.name')
							->get();*/
			return View::make( 'admin.store_product_related.import' )
	            		->with( 'title', 'Productos relacionados' )
	            		->with( 'sub_title','Importar Productos' )
	            		//->with( 'stores', $stores )
	            		->with( 'cities', $this->get_cities());
        }

	}

	/**
     * Busqueda y visualización de la lista de productos para adicionar
     */
	public function search_products_ajax()
	{
		if ( Request::ajax() ){

			if( Input::has( 'search' ) && Input::get( 'search' ) == 1 ){ 
				$term = Input::get( 'term' );
				$warehouse_id = Input::get( 'warehouse_id' );
				$products = StoreProduct::select( 'products.name', 'products.image_small_url', 'products.unit', 'products.reference', 'products.quantity', 'store_products.id', 'cities.city' )
							->join( 'products', 'products.id', '=', 'store_products.product_id' )
							->join( 'stores', 'stores.id', '=', 'store_products.store_id' )
							->join( 'cities', 'stores.city_id', '=', 'cities.id' )
							->join( 'store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
							->where('store_product_warehouses.warehouse_id', $warehouse_id )
							//->where( 'store_products.store_id', $store_id )
							->where( function( $query ) use ( $term, $warehouse_id ) {
								$query->orWhere( 'products.name', 'LIKE', '%'.$term.'%' )
									->orWhere( 'products.reference', 'LIKE', '%'.$term.'%' );
							} )
							->groupBy('store_products.id')
							->get();
							//->toSql();
						
				return View::make( 'admin.store_product_related.related_form' )
	                    ->with( 'title', 'Productos relacionados' )
	            		->with( 'products', $products )->renderSections()['products'];
			}

			if( Input::has( 'related' ) && Input::get( 'related' ) == 1 ){ 
				$store_product_id = Input::get( 'store_product_id' );
				$related_products = StoreProductRelated::getRelatedProducts( $store_product_id );
				return View::make( 'admin.store_product_related.related_form' )
	                    ->with( 'title', 'Productos relacionados' )
	            		->with( 'related_products', $related_products )->renderSections()['products_related'];
			}
		}
	}

	/**
     * Funcion que cambia el estado de un producto o todos los productos relacionados
     */
	public function change_status($status, $store_product_id = null)
	{
		if(!Request::ajax()){
			$all_status = array(0=>'Inactivo', 1=>'Activo');
			$msg = '';
			if($status == 1){
				$status = 0;
				$msg = !is_null($store_product_id)?'Se desactivo el producto':'Se han desactivadio todos los productos';
			}elseif($status == 0){
				$status = 1;
				$msg = !is_null($store_product_id)?'Se activo el producto':'Se han activado todos los productos';
			}

			if(is_null($store_product_id)){
				StoreProductRelated::whereRaw('status IS NOT NULL')->update(array('status'=>$status));
				$this->admin_log('store_product_related', Session::get('admin_id'), 'Cambio el estado del producto '.$store_product_id.' a '.$all_status[$status]);
			}else{
				StoreProductRelated::where('store_product_id', $store_product_id)
					->update(array('status'=>$status));
				$this->admin_log('store_product_related', Session::get('admin_id'), 'Cambio el estado de todos los productos a '.$all_status[$status]);
			}
			return Redirect::route('adminStoreProductRelated.index')->with( 'success', $msg );
		}
	}

}