<?php

namespace admin\callcenter;

use admin\AdminController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use tickets\Ticket;

class CustomerServiceController extends AdminController
{
    /**
     * Home del modulo de servicio al cliente.
     *
     * @return mixed
     */
    public function index()
    {
        $filter = Input::all();
        $title = 'Servicio al cliente';
        $date_filter = Input::has('created_at')
            ? date_create_from_format('d/m/Y', Input::get('created_at'))
            : (isset($filter['created_at'])
                ? ''
                : new \DateTime()
            );
        $status = [
            Ticket::CREATED => Ticket::CREATED,
            Ticket::ASSIGNED => Ticket::ASSIGNED,
            Ticket::PENDING => Ticket::PENDING,
            Ticket::SOLVED => Ticket::SOLVED
        ];
        $admin = \Admin::find(\Session::get('admin_id'));
        $query = Ticket::query();
        if (!empty($filter['status']) && is_array($filter['status'])) {
            $query->whereIn('status', $filter['status']);
        }

        if (!empty($date_filter)) {
            $query->where(\DB::raw('DATE(tickets.created_at)'), '=', $date_filter->format('Y-m-d'));
        }

        if (!empty($filter['status'])) {
            $query->where('tickets.status', $filter['status']);
        }

        if (!empty($filter['admin_id'])) {
            $query->whereHas('lastCall', function ($query) use ($filter) {
                $query->where('admin_id', $filter['admin_id']);
            });
        }

        if (!empty($filter['order_by']) && !empty($filter['order'])) {
            $query->orderBy($filter['order_by'], $filter['order']);
        } else {
            $query->orderBy('tickets.id', 'DESC');
        }

        if (!empty($filter['wildcard'])) {
            $wildcard = $filter['wildcard'];
            $query->select(\DB::raw('tickets.*'))
                ->leftJoin('orders', 'orders.id', '=', 'tickets.order_id')
                ->join('users', 'users.id', '=', 'orders.user_id')
                ->where(function ($query) use ($wildcard) {
                    $wildcard_like = "%{$wildcard}%";
                    $query->where('orders.id', $wildcard)
                        ->orWhere(\DB::raw('CONCAT_WS(" ", users.first_name, users.last_name)'), 'LIKE', $wildcard_like)
                        ->orWhere('users.email', 'LIKE', $wildcard_like);
                });
        }

        $admins = \Admin::where('status', 1)->orderBy('fullname')->hasCustomerServiceRol()->lists('fullname', 'id');
        $tickets = $query->with('activeCall.admin', 'customerService', 'lastCall.admin')
            ->with('order.user', 'order.orderGroup.city')
            ->paginate()
            ->appends(Input::all());

        return View::make(
            'admin.customer_service.admin_index',
            compact('tickets', 'admin', 'title', 'status', 'date_filter', 'admins')
        );
    }

    /**
     * Asigna los servicios disponibles para el "customer service"
     *
     * @param int $admin_id
     * @return mixed
     */
    public function assign_services_ajax($admin_id)
    {
        $admin = \Admin::findOrFail($admin_id);
        $services = \Input::get('services');
        $services = !empty('services') && is_array($services)
            ? $services : [];
        foreach ($services as $service) {
            \CustomerService::findOrFail($service);
        }
        $admin->services_list = implode(',', $services);
        $admin->save();

        return \Response::json($admin);
    }

    /**
     * Obtiene el listado de usuarios de servicio al cliente
     * para poder asignar los permisos asociados.
     *
     * @return mixed
     */
    public function users_table_ajax()
    {
        $users = \Admin::where('status', 1)->hasCustomerServiceRol()->get();
        $render_permissions = true;
        $services = \CustomerService::get();
        return \View::make(
            'admin.customer_service.customer_service_permissions',
            compact('users', 'render_permissions', 'services')
        );
    }
}
