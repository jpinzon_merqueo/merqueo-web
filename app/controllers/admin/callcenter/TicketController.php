<?php

namespace admin\callcenter;

use admin\AdminController;
use Carbon\Carbon;
use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use tickets\Ticket;
use tickets\TicketCall;
use tickets\TicketLayout;
use tickets\TicketSolution;
use tickets\TicketReason;

/**
 * Class TicketController
 * @package admin\callcenter
 */
class TicketController extends AdminController
{
    const RECEIVE_TICKETS = 'admin_user-ready-to-receive-tickets';
    const CALL_PENDING = 'Pendiente';

    /**
     * Muestra el listado de ticket calls asociados al administrador.
     *
     * @return mixed
     */
    public function index()
    {
        $filter = Input::all();
        $title = 'Servicio al cliente';
        $date_filter = Input::has('created_at')
            ? date_create_from_format('d/m/Y', Input::get('created_at'))
            : (isset($filter['created_at'])
                ? ''
                : new \DateTime()
            );
        $status = [
            Ticket::CREATED => Ticket::CREATED,
            Ticket::ASSIGNED => Ticket::ASSIGNED,
            Ticket::PENDING => Ticket::PENDING,
            Ticket::SOLVED => Ticket::SOLVED,
            Ticket::OPEN => Ticket::OPEN,
            Ticket::RESOLVED => Ticket::RESOLVED
        ];

        $admin = \Admin::findOrFail(Session::get('admin_id'));
        $query = TicketCall::join('tickets', 'ticket_id', '=', 'tickets.id');

        if (!empty($filter['status']) && is_array($filter['status'])) {
            $query->whereIn('tickets.status', $filter['status']);
        }

        if (!empty($date_filter)) {
            $query->where(\DB::raw('DATE(tickets.created_at)'), '=', $date_filter->format('Y-m-d'));
        }

        if (!empty($filter['status'])) {
            $query->where('tickets.status', $filter['status']);
        }

        if (!empty($filter['wildcard'])) {
            $wildcard = $filter['wildcard'];
            $query->leftJoin('orders', 'orders.id', '=', 'tickets.order_id')
                ->leftJoin('users', 'users.id', '=', 'orders.user_id')
                ->where(function ($query) use ($wildcard) {
                    $wildcard_like = "%{$wildcard}%";
                    $query->where('orders.id', $wildcard)
                        ->orWhere(\DB::raw('CONCAT_WS(" ", users.first_name, users.last_name)'), 'LIKE', $wildcard_like)
                        ->orWhere('tickets.user_from_email', 'LIKE', $wildcard_like)
                        ->orWhere('tickets.id', $wildcard)
                        ->orWhere('tickets.reference', 'LIKE', $wildcard_like);
                });
        }

        if (!empty($filter['wildcard']) || !empty($filter['status'])) {
            $calls = $query->select('ticket_calls.*')
                ->orderBy('tickets.created_at', 'ASC')
                ->with('ticket.order.orderGroup.city', 'ticket.customerService', 'ticket.order.user')
                ->paginate()
                ->appends(Input::all());
        } else {
            $calls = TicketCall::with('ticket.order.orderGroup.city', 'ticket.customerService', 'ticket.order.user')
                ->userActiveCalls($admin)
                ->paginate()
                ->appends(Input::all());
        }

        return View::make('admin.customer_service.user_index', compact('calls', 'title', 'status', 'date_filter'));
    }

    /**
     * Se asigna un nuevo ticket al usuario
     * con sesión activa.
     *
     * @throws \Exception
     */
    public function user_receive_tickets()
    {
        \Session::put(self::RECEIVE_TICKETS, true);
        $admin = \Admin::findOrFail(\Session::get('admin_id'));
        $total_current_tickets = $admin->calls()
            ->whereHas('ticket', function ($query) {
                $query->where('status', Ticket::ASSIGNED);
            })
            ->pending()
            ->count();

        $status = [];
        $date_filter = new \DateTime();

        if (Ticket::QUEUE_SIZE - $total_current_tickets > 0) {
            $tickets = Ticket::getFreeTickets(
                Ticket::QUEUE_SIZE - $total_current_tickets,
                $admin->getCustomerServicePermissions()
            );

            if (!$tickets->isEmpty()) {
                foreach ($tickets as $ticket) {
                    $ticket->validateTicketAssignment();
                    $ticket->assignTicket($admin);
                }
            }
        }

        $calls = $admin->calls()
            ->with('ticket.order.orderGroup.city', 'ticket.customerService', 'ticket.order.user')
            ->pending()
            ->whereHas('ticket', function ($query) {
                $query->where('status', Ticket::ASSIGNED);
            })
            ->orderBy('id', 'ASC')
            ->get();

        return View::make('admin.customer_service.user_index', compact('calls', 'title', 'status', 'date_filter'));
    }

    public function stop_receiving_tickets()
    {
        \Session::forget(self::RECEIVE_TICKETS);
    }

    /**
     * Muestra el formulario donde el "customer service" actualiza el
     * estado del ticket.
     *
     * @param $ticket_call_id
     * @return mixed
     */
    public function update_status_form($ticket_call_id)
    {
        $orders = '';
        $call = TicketCall::with('ticket.order.user')->findOrFail($ticket_call_id);
        $ticket = Ticket::find($call->ticket_id);

        if ($ticket->user_id) {
            $orders = \Order::where('user_id', $ticket->user_id)
                ->select('id', 'reference', 'date')
                ->orderBy('id', 'DESC')
                ->limit(5)
                ->get();
        }

        $reasons = $solutions = TicketReason::lists('reason', 'id');
        if (empty($ticket->message)) {
            $status_ticket = [
                Ticket::SOLVED => Ticket::SOLVED,
                Ticket::PENDING => Ticket::PENDING
            ];
        } else {
            $status_ticket = [
                Ticket::OPEN => Ticket::OPEN,
                Ticket::PENDING => Ticket::PENDING,
                Ticket::RESOLVED => Ticket::RESOLVED
            ];
        }

        $status_call = [
            TicketCall::ATTENDED => TicketCall::ATTENDED,
            TicketCall::NOT_ATTENDED => TicketCall::NOT_ATTENDED
        ];

        if ($ticket->customerService->description === \CustomerService::TICKET) {
            $solutions = TicketSolution::lists('name', 'id');
        } elseif ($ticket->user_id) {
            $solutions = TicketSolution::lists('name', 'id');
        } else {
            $solutions = TicketSolution::whereNotIn('id', [3, 4])->lists('name', 'id');
        }

        return View::make(
            'admin.customer_service.update_status_form',
            compact('call', 'status_call', 'solutions', 'status_ticket', 'orders', 'reasons')
        );
    }

    /**
     * Actualiza el estado de un ticket asignado.
     *
     * @param $ticket_call_id
     * @throws \Exception
     */
    public function update_status($ticket_call_id)
    {
        $call = TicketCall::with('ticket')->findOrFail($ticket_call_id);
        $ticket_email = Ticket::find($call->ticket_id);
        $reason_id = is_null($ticket_email->message) ? NULL : Input::get('reason_id');
        $status_ticket = Input::get('status_ticket');
        $status_call = Input::get('status_call');
        $comments = Input::get('comments');
        $order = Input::get('solution_identifier');
        $ticket = Ticket::find($call->ticket_id);

        $email = "%" . $ticket->user_from_email . "%";
        $order_user = \User::where('email', 'LIKE', $email)->first();
        if (empty($order_user)) {
            $order_id = \Order::find($order);
            if ($order_id) {
                $ticket->order()->associate($order_id);
                $ticket->user_id = $order_id->user_id;
                $ticket->save();
            } else {
                if (Input::get('solution_id') == TicketSolution::COMPLAINT_ORDER)
                    throw new BadRequestHttpException('La orden no existe');
            }
        } else {
            $order_id = \Order::find($order);
            if ($order_id && $ticket->order_id == 0) {
                $ticket->order()->associate($order_id);
                $ticket->user_id = $order_id->user_id;
                $ticket->save();
            }
        }

        if (!empty($order)) {
            if (Input::has('order_id') && Input::has('reference')) {
                $ticket_parent = Ticket::where('order_id', Input::get('order_id'))
                    ->where('id', '<>', $ticket->id)
                    ->orderBy('id', 'DESC')
                    ->first();

                if (!empty($ticket_parent)) {
                    $ticket_parent->ticket_child_id = (empty($ticket->id) ? NULL : $ticket->id);
                    $ticket_parent->save();
                }

                $ticket->order_id = Input::get('order_id');
                $ticket->reference = Input::get('reference');
                $ticket->ticket_parent_id = (!empty($ticket_parent) ? $ticket_parent->id : NULL);
                $ticket->save();

            }
        } else {
            if ($ticket->order_id) {
                $ticket_parent = Ticket::where('order_id', $ticket->order_id)
                    ->where('id', '<>', $ticket->id)
                    ->orderBy('id', 'DESC')
                    ->first();

                if (!empty($ticket_parent)) {
                    $ticket_parent->ticket_child_id = $ticket->id;
                    $ticket_parent->save();
                }

                $ticket->ticket_parent_id = (!empty($ticket_parent) ? $ticket_parent->id : NULL);
                $ticket->save();
            }

        }

        DB::beginTransaction();
        try {
            //Ticket::validateUserQueue($call->admin, $call);
            $call->ticket->saveStatus($status_ticket);
            $call->saveStatus($status_call, $comments, $reason_id);
            if ($call->ticket->isSolved()) {
                $solution = TicketSolution::findOrFail(Input::get('solution_id'));
                $layout = TicketLayout::findOrFail(Input::get('ticket_layout_id') ?: 0);

                $call->load('ticket.order.user');
                $call->solution()->associate($solution);

                list($day, $month, $year) = explode('/', Input::get('expiration_date') ?: Carbon::create()->addWeek()->format('d/m/Y'));
                $expiration_date = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));

                $solution->manage(
                    $call->ticket->order ?: new \Order(),
                    Input::get('solution_identifier'),
                    Input::get('amount'),
                    $expiration_date
                );

                //$user = $call->ticket->order->user;
                $order = new \Order();
                $user = new \User();
                $order->user()->associate($user);

                $call->ticket->load('order.user');
                $user = $call->ticket->order ? $call->ticket->order->user : $call->ticket->user;
                if (empty($user)) {
                    $user = new \User();
                    $user->email = trim($call->ticket->user_from_email);
                    $user->first_name = trim($call->ticket->user_from_name);
                }
                $content = $call->email_body = $layout->renderTemplate(
                    $user,
                    $call->ticket->order ?: new \Order(),
                    Input::get('content') ?: '',
                    $solution->complaint_order,
                    $solution->coupon
                );
                $call->save();

                $sent = false;
                for ($tries = 0; $tries < 5 && !$sent; $tries++) {
                    $sent = send_mail([
                        'template_name' => 'emails.customer_service_default',
                        'vars' => compact('content', 'call'),
                        'subject' => $layout->email_subject,
                        'to' => [['email' => $user->email, 'name' => $user->first_name]],
                    ]);
                }

                if (!$sent) {
                    $call->comments .= "\n<b style='color:red'>El email no pudo ser enviado al cliente.</b>";
                    $call->save();
                }
            }
            DB::commit();
        } catch (ModelNotFoundException $exception) {
            DB::rollback();
            throw new BadRequestHttpException($message = $this->get_error_message($exception), $exception);
        } catch (MerqueoException $exception) {
            DB::rollback();
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    /**
     * Asigna un ticket manualmente a cualquier usuario.
     *
     * @param $ticket_id
     * @return mixed
     */
    public function assign_ticket($ticket_id)
    {
        $ticket = Ticket::findOrFail($ticket_id);
        $admin_id = Input::get('admin_id') ?: 0;
        $admin = \Admin::hasCustomerServiceRol()->findOrFail($admin_id);
        $call = null;
        if ($ticket->isAssigned()) {
            $call = $ticket->moveTicket($admin);
        } else {
            $call = $ticket->assignTicket($admin);
        }

        return \Response::json([
            'status' => true,
            'message' => $call
        ]);
    }

    /**
     * Obtiene el formulario donde se asigna un ticket a un "customer service".
     *
     * @param $ticket_id
     * @return mixed
     */
    public function assign_ticket_form($ticket_id)
    {
        $ticket = Ticket::findOrFail($ticket_id);
        $query = \Admin::hasCustomerServiceRol()->where('status', 1);

        if ($ticket->isAssigned()) {
            $ticket->load('activeCall.admin');
            $query->where('id', '<>', $ticket->activeCall->admin->id);
        }

        $admins = $query->select('fullname', 'id', 'services_list')->orderBy('fullname')->get();
        $admins = $admins->filter(function (\Admin $user) use ($ticket) {
            return in_array($ticket->customer_service_id, $user->getCustomerServicePermissions());
        });

        return View::make('admin.customer_service.assign_ticket_form', compact('ticket', 'admins'));
    }

    /**
     * Obtiene el listado de "ticket_calls" asociados a un ticket.
     *
     * @param $ticket_id
     * @return mixed
     */
    public function ticket_logs($ticket_id)
    {
        $ticket = Ticket::with('calls')->findOrFail($ticket_id);
        return View::make('admin.customer_service.ticket_log', compact('ticket'));
    }

    /**
     * Obtiene el listado de layouts disponibles para la solución asignada.
     *
     * @param int $solution_id
     * @return mixed
     */
    public function get_layout_by_solution_ajax($solution_id)
    {
        $layouts = TicketSolution::findOrFail($solution_id)
            ->layouts()
            ->orWhere('name', 'LIKE', '%default%')
            ->orderBy('name')
            ->get();

        return View::make('admin.customer_service.ticket_layout_email_form', compact('layouts'));
    }

    /**
     * Obtiene el contenido del email que va a ser enviado al cliente.
     *
     * @param int $call_id
     * @return mixed
     */
    public function get_email_solution_preview_ajax($call_id)
    {
        try {
            $call = TicketCall::findOrFail($call_id);
            $solution = TicketSolution::findOrFail(Input::get('solution_id'));
            $layout = TicketLayout::findOrFail(Input::get('ticket_layout_id'));
            $complaint_order = new \Order();
            $coupon = new \Coupon();

            switch (intval($solution->id)) {
                case TicketSolution::COMPLAINT_ORDER:
                    $complaint_order = \Order::findOrFail(Input::get('solution_identifier') ?: 0);
                    break;

                case TicketSolution::VOUCHER:
                    $coupon = \Coupon::where('code', Input::get('solution_identifier') ?: '')->firstOrFail();
                    break;
            }
        } catch (ModelNotFoundException $exception) {
            $message = $this->get_error_message($exception);
            // Se utiliza el mismo tipo de respuesta del API en caso de que sea un error.
            return \Response::json([
                'status' => false,
                'error' => ['message' => $message]
            ], 404);
        }

        $order = new \Order();
        $user = new \User();
        $order->user()->associate($user);

        $call->ticket->load('order.user');
        $user = $call->ticket->order ? $call->ticket->order->user : $call->ticket->user;
        if (empty($user)) {
            $user = new \User();
            $user->email = trim($call->ticket->user_from_email);
            $user->first_name = trim($call->ticket->user_from_name);
        }

        $content = $call->email_body = $layout->renderTemplate(
            $user,
            $call->ticket->order ?: $order,
            Input::get('content') ?: '',
            $complaint_order,
            $coupon
        );

        $is_mock = true;

        return View::make('emails.customer_service_default', compact('content', 'is_mock', 'call'));
    }

    private function get_error_message(ModelNotFoundException $exception)
    {
        switch ($exception->getModel()) {
            case 'Order':
                $type_not_found = 'pedido';
                break;

            case 'Coupon':
                $type_not_found = 'cupón';
                break;

            default:
                $type_not_found = $exception->getModel();
        }

        return "No se encontraron resultado para el {$type_not_found}.";
    }
}
