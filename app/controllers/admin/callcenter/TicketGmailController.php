<?php

namespace admin\callcenter;

use admin\AdminController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use tickets\Ticket;
use tickets\TicketCall;
use tickets\TicketLayout;
use tickets\TicketSolution;

/**
 * Class TicketGmailController
 * @package admin\callcenter
 */
class TicketGmailController extends AdminController
{

    private $json;

    public function __construct()
    {
        $this->beforeFilter(function(){
        } ,array('except' => array('create_tickets_gmail_api', 'oauth2_callback_gmail_api')));

        $this->json = app_path().'/storage/client_secret_prod.json';
    }

    public function create_tickets_gmail_api()
    {
        $messages = "";
        $userId = 'me';

        $client = new \Google_Client();
        $client->setApplicationName('Gmail api');
        $client->setScopes(implode(' ', array(\Google_Service_Gmail::GMAIL_READONLY, \Google_Service_Gmail::GMAIL_MODIFY)));
        $client->setAuthConfig($this->json);
        $client->setAccessType("offline");
        //$client->setApprovalPrompt('force');

        if (!empty(Session::has('access_token'))) {
            $client->setAccessToken(Session::get('access_token'));
            if ($client->isAccessTokenExpired()) {
                $client->revokeToken();
                return Redirect::route('ticketFromGmail.oauthGmailApi');
            }

            $gmail = new \Google_Service_Gmail($client);
            $messages = $this->listMessages($gmail, $userId);
            $cont = 0;

            foreach ($messages as $message) {
                $message = $this->getMessage($gmail, $userId, $message->getId());
                if(!empty($message) && !empty($message->body)) {
                    $ticket = Ticket::createTicket(NULL, \CustomerService::MAIL_TICKET, $message);
                    $labels = $message->labelIds;
                    $labelsToAdd = [];
                    foreach ($labels as $index => $value) {
                        if ($value == "UNREAD") {
                            unset($labels[$index]);
                            break;
                        } else {
                            $labelsToAdd[] = $value;
                        }
                    }

                    if ($ticket)
                        $this->modifyMessage($gmail, "me", $message->id, $labelsToAdd, ["UNREAD"]);
                }
            }
            if($messages)
                return Redirect::route('customerService.index')->with('success', 'Tickets recibidos desde correo electrónico satifactoriamente.');
            else
                return Redirect::route('customerService.index')->with('error', 'No hay tickets desde correo electrónico a cargar.');

        }else{
            return Redirect::route('ticketFromGmail.oauthGmailApi');
        }
    }

    public function oauth2_callback_gmail_api()
    {
        $url_callback = route('ticketFromGmail.oauthGmailApi');

        $client = new \Google_Client();
        $client->setApplicationName('Gmail api');
        $client->setScopes(implode(' ', array(\Google_Service_Gmail::GMAIL_READONLY, \Google_Service_Gmail::GMAIL_MODIFY)));
        $client->setAuthConfig($this->json);
        $client->setAccessType("offline");

        $code = Input::get('code');
        if (empty($code)) {
          $auth_url = $client->createAuthUrl();
          return Redirect::to($auth_url);
        }else{
          $client->authenticate($code );
          Session::put('access_token', $client->getAccessToken());
          return Redirect::route('ticketFromGmail.createTicketsGmailApi');
        }
    }

    /**
    * Get list of Messages in user's mailbox.
    * @param  Google_Service_Gmail $service Authorized Gmail API instance.
    * @param  string $userId User's email address. The special value 'me'
    * can be used to indicate the authenticated user.
    * @return array Array of Messages.
    */
    private function listMessages($service, $userId) {
        $pageToken = NULL;
        $messages = array();
        $opt_param = array();

        //$current_date = date("Y-m-d 00:00:00");
        //$newTime = strtotime($current_date);
        $after = strtotime(date('Y-m-d 00:00:00'));
        $opt_param['q'] = "after:$after is:unread";

        do {
            try {
                if ($pageToken) {
                    $opt_param['pageToken'] = $pageToken;
                }

                $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
                if ($messagesResponse->getMessages()) {
                    $messages = array_merge($messages, $messagesResponse->getMessages());
                    $pageToken = $messagesResponse->getNextPageToken();
                }
            }catch (Exception $e){
                print 'An error occurred: ' . $e->getMessage();
            }
        } while ($pageToken);
        return $messages;
    }

    /**
    * Get Message with given ID.
    * @param  Google_Service_Gmail $service Authorized Gmail API instance.
    * @param  string $userId User's email address. The special value 'me'
    * can be used to indicate the authenticated user.
    * @param  string $messageId ID of Message to get.
    * @return Google_Service_Gmail_Message Message retrieved.
    */
    private function getMessage($service, $userId, $messageId) {
        try {

            $opt_param['metadataHeaders'] = 'From';
            $message = $service->users_messages->get($userId, $messageId, $opt_param);
            $id = $message->getId();
            $labelIds = $message->labelIds;
            $headers = $message->payload->headers;
            /******************Subject**************************/
            $subject = $this->getHeader($headers , 'Subject');
            /******************User sender**************************/
            $from_user = $this->getHeader($headers , 'From');
            $from_email = $this->getFrom($headers , 'Received-SPF');
            $from = 'de: '.str_replace('"', '', $from_user).' ('.$from_email.')';
            /******************User Received**************************/
            $to_user = explode(',', $this->getHeader($headers , 'To'));
            $to_email = $this->getHeader($headers , 'Delivered-To');
            $to = "para: ".$to_user[0]."(".$to_email.")";
            /******************Date received message**************************/
            $date = explode(';', $this->getHeader($headers , 'Received'));
            $date = trim($date[1]);
            $date = new \DateTime($date);
            $date->setTimezone(new \DateTimeZone('America/Bogota'));
            $date = format_date('normal_long_with_time', $date->format('Y-m-d H:i:s'));
            /******************Body**************************/
            $body = preg_replace("/[\n]/", "<br>", $this->getBody($message->payload->parts));
            $body = is_array($body) && !empty($body) ? $body[0] : "";

            $message = new Ticket();
            if (($from_email != 'support@merqueo.zendesk.com' && (strpos($from_user, 'SAC') === false || strpos($from_user, 'Merqueo') === false)) ||
                ($from_email != 'support@support.zendesk.com' && (strpos($from_user, 'Support') === false))) {

                $message->id = $id;
                $message->subject = "asunto: " . $subject;
                $message->from = $from;
                $message->from_email = $from_email;
                $message->from_user = str_replace('"', '', $from_user);
                $message->to = $to;
                $message->date = 'fecha: ' . $date;
                $message->body = "Mensaje:<br>" . $body;
                $message->labelIds = $labelIds;
            }
            return $message;

        }catch (Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }

    /**
    * Get message subject
    * @param  headers Object
    * @return string Subject
    */
    private function getHeader($headers, $name) {
        foreach($headers as $header) {
            if($header['name'] == $name) {
                return $header['value'];
            }
        }
    }

    /**
    * Get message sender
    * @param  headers Object
    * @return string Subject
    */
    private function getFrom($headers, $name) {
        foreach($headers as $header) {
            if($header['name'] == $name) {
                $matches = [];
                $pattern = '/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
                preg_match($pattern, $header->value, $matches);

                $matches = (!empty($matches) ? $matches[0] : "");
                return $matches;
            }
        }
    }

    private function getBody($dataArr) {
        $outArr = [];
        foreach ($dataArr as $key => $val) {
            $outArr[] = $this->base64url_decode($val->getBody()->getData());
            break; // we are only interested in $dataArr[0]. Because $dataArr[1] is in HTML.
        }
        return $outArr;
    }

    private function base64url_decode($data) {
      return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }


    /**
    * Modify the Labels a Message is associated with.
    * @param  Google_Service_Gmail $service Authorized Gmail API instance.
    * @param  string $userId User's email address. The special value 'me'
    * can be used to indicate the authenticated user.
    * @param  string $messageId ID of Message to modify.
    * @param  array $labelsToAdd Array of Labels to add.
    * @param  array $labelsToRemove Array of Labels to remove.
    * @return Google_Service_Gmail_Message Modified Message.
    */
    private function modifyMessage($service, $userId, $messageId, $labelsToAdd, $labelsToRemove) {
      $mods = new \Google_Service_Gmail_ModifyMessageRequest();
      $mods->setAddLabelIds($labelsToAdd);
      $mods->setRemoveLabelIds($labelsToRemove);

      try {
        $message = $service->users_messages->modify($userId, $messageId, $mods);
        return $message;
      } catch (Exception $e) {
        print 'An error occurred: ' . $e->getMessage();
      }
    }
}
