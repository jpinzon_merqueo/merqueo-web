<?php
namespace admin\callcenter;

use admin\AdminController;
use Input;
use Order;
use Session;
use Store;
use Event;
use OrderEventHandler;
use Redirect;

/**
 * Class OrderValidationController.
 */
class OrderValidationController extends AdminController
{

    /**
     * Muestra lista de órdenes.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data['title']              = 'Validación de pedidos';
        $data['validation_reasons'] = \OrderValidationReason::where('status', 1)->get();
        $data['delivery_windows'] = json_encode(\DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id')));
        $data['delivery_windows_same_day'] = json_encode(\DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id'), true));

        if (Session::get('admin_designation') == 'Super Admin'){
            $data['stores'] = Store::where('status', 1)->where('is_storage', 1)->get();
        }else{
            $data['stores'] = Store::where('status', 1)->where('is_storage', 1)->where('city_id', Session::get('admin_city_id'))->get();
        }
        return \View::make('admin.call_center.order_validation.index', $data);
    }

    /**
     * Filtro de órdenes.
     */
    public function search()
    {
        $validationTypeId = Input::get('validation_type');
        $startDate        = format_date('mysql', Input::get('start_date'));
        $endDate          = format_date('mysql', Input::get('end_date'));
        $search           = Input::get('search');
        $deliveryTime     = Input::get('delivery_time');
        $warehouse        = Input::get('warehouse');
        $storeId          = Input::get('store_id');
        $isInputEmpty     = count(array_filter(Input::except(['page']))) === 0;

        $orders = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->leftJoin(\DB::raw('(user_credits, coupons)'), function ($join) {
                $join->on('user_credits.order_id', '=', 'orders.id');
                $join->on('user_credits.type', '=', \DB::raw('0'));
                $join->on('user_credits.coupon_id', '=', 'coupons.id');
            });

        switch ($validationTypeId) {
            case 3: // cambo de método de pago
                $orders->whereNotIn('orders.status', ['Cancelled', 'Delivered']);
                break;

            case 6: // pagados con PSE y faltantes
                $orders->whereNotIn('orders.status', ['Cancelled'])
                    ->where('orders.payment_method', 'Débito - PSE');
                break;

            case 7: // pagados por PSE y cancelados por cliente
                $orders->whereIn('orders.status', ['Cancelled'])
                    ->where('orders.payment_method', 'Débito - PSE');
                break;

            default:
                $orders->whereRaw("(
                    CASE
                        WHEN order_validation_reason_id IN (3,5) THEN orders.status NOT IN ('Cancelled', 'Delivered')
                        WHEN order_validation_reason_id = 6 THEN orders.status NOT IN ('Cancelled')
                        WHEN order_validation_reason_id = 7 THEN orders.status IN ('Cancelled')
                        ELSE orders.status IN ('Validation')
                    END
                )");
                break;
        }

        !empty($validationTypeId) && $orders->where('orders.order_validation_reason_id', '=', $validationTypeId);

        !empty($warehouse) && $orders->where('warehouse_id', '=', $warehouse);

        (!empty($storeId) && empty($warehouse)) && $orders->where('orders.store_id', '=', $storeId);

        !empty($deliveryTime) && $orders->where('orders.delivery_time', $deliveryTime);

        (!empty($startDate) && !empty($endDate)) && $orders->whereRaw("DATE(orders.delivery_date) BETWEEN '{$startDate}' AND '{$endDate}'");

        !empty($search) && $orders->where(function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                      $query->where(\DB::raw('CONCAT(order_groups.user_firstname, " ", order_groups.user_lastname)'), 'LIKE', "%{$search}%")
                            ->orWhere('order_groups.user_email', 'LIKE', "%{$search}%")
                            ->orWhere('order_groups.user_address', 'LIKE', "%{$search}%")
                            ->orWhere('order_groups.user_phone', 'LIKE', "%{$search}%");
                  })
                  ->orWhere('orders.id', 'LIKE', "%{$search}%")
                  ->orWhere('orders.reference', 'LIKE', "%{$search}%");
        });

        $orders = $orders
            ->whereHas('store', function ($query) {
                $query->where('is_storage', '=', 1);
            })
            ->with([
                'orderGroup', 'orderGroup.zone', 'orderGroup.warehouse', 'user',
                'orderValidationReason', 'route',
            ])
            ->select(
                'orders.*',
                'coupons.code AS coupon_code',
                \DB::raw("SUM(if(store_products.storage = 'Seco', 1, 0)) AS products_storage_dry"),
                \DB::raw("SUM(if(store_products.storage = 'Refrigerado' || store_products.storage = 'Congelado', 1, 0)) AS products_storage_cold")
            )
            ->groupBy('orders.id')
            ->paginate(10);

        foreach ($orders as $order) {
            $order->orders_qty = Order::where('user_id', $order->user_id)->where('user_id', '<>', 5169)->where('status', 'Delivered')->count();
        }

        $data['html']         = \View::make('admin.call_center.order_validation.ajax.grid', ['orders' => $orders])->render();
        $data['links_html']   = $orders->links()->render();
        $data['orders_count'] = $orders->count();
        $data['orders_total'] = $orders->getTotal();

        return \Response::json($data);
    }

    /**
     * Guardar log de gestión Sac
     */
    public function management_sac(){

        $id       = Input::get('id');
        $reasonId = Input::get('reasonId');

        if ($id && $reasonId)
        {
            Event::subscribe(new OrderEventHandler);

            Event::fire('order.managed_sac_log', [$id, Session::get('admin_id'), $reasonId]);

            Order::where('id', $id)
                ->update(['order_validation_reason_id' => null]);

            return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'success')->with('message', 'Este cambio fue exitoso.');
       }

       return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al guardar el Log.');
    }

}
