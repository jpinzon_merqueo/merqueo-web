<?php

namespace admin\callcenter;

use admin\AdminController;
use tickets\TicketLayout;
use tickets\TicketSolution;

/**
 * Class TicketLayoutController
 * @package admin\callcenter
 */
class TicketLayoutController extends AdminController
{
    /**
     * Muestra el listado de layouts disponibles.
     *
     * @return mixed
     */
    public function show_layouts_lists()
    {
        $solutions = TicketSolution::orderBy('name')->lists('name', 'id');
        $query = TicketLayout::query();

        if (\Input::has('ticket_solution_id')) {
            $query->where('ticket_solution_id', \Input::get('ticket_solution_id'));
        }

        $layouts = $query->with('solution')->paginate();

        return \View::make('admin.customer_service.ticket_layout_list', compact('layouts', 'solutions'));
    }

    /**
     * Muestra el formulario donde se genera un nuevo layout
     * o se edita alguno.
     *
     * @param int $layout_id
     * @return mixed
     */
    public function save_layout_form($layout_id)
    {
        $layout = TicketLayout::findOrNew($layout_id);
        $solutions = TicketSolution::orderBy('name')->lists('name', 'id');
        $variables = array_keys(TicketLayout::getReplaceableVariables(new \User(), new \Order()));
        sort($variables);

        return \View::make(
            'admin.customer_service.ticket_layout_form',
            compact('layout', 'variables', 'solutions')
        );
    }

    /**
     * Almacena un layout en la base de datos, en caso de que no
     * se indique un identificador crea uno nuevo.
     *
     * @return mixed
     */
    public function save_layout()
    {
        $layout = TicketLayout::findOrNew(\Input::get('id'));
        $layout->name = \Input::get('name');
        $layout->body = \Input::get('body');
        $layout->email_subject = \Input::get('email_subject');
        $layout->solution()->associate(TicketSolution::findOrFail(\Input::get('ticket_solution_id')));
        $layout->save();

        return \Redirect::action('admin\callcenter\TicketLayoutController@show_layouts_lists');
    }

    /**
     * Elimina un layout
     *
     * @param $layout_id
     * @return mixed
     * @throws \Exception
     */
    public function delete_layout($layout_id)
    {
        TicketLayout::findOrFail($layout_id)->delete();

        return \Redirect::action('admin\callcenter\TicketLayoutController@show_layouts_lists');
    }

    /**
     * Genera un ejemplo del contenido del layout con mocks de
     * los parámetros necesarios.
     *
     * @param int $layout_id
     * @return mixed
     */
    public function render_example_ajax($layout_id = 0)
    {
        $layout = TicketLayout::findOrNew($layout_id);
        $layout->body = empty($layout->body) && \Input::get('content')
            ? \Input::get('content') : $layout->body;

        $user = new \User();
        $user->first_name = 'John';
        $user->last_name = 'Doe';
        $user->email = 'john@evil-company.com';
        $user->phone = '32038972XX';

        $order = new \Order();
        $order->id = '5555XX';
        $order->cc_last_four = '8888';

        $content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi deleniti dolorum eos ' .
            'explicabo illum recusandae repellat! Iusto optio quis quisquam rem? Ab, dolore facilis. A explicabo ' .
            'obcaecati pariatur repudiandae tenetur.';

        $complain_order = new \Order();
        $complain_order->delivery_date = date('Y-m-d G:i:s');
        $complain_order->delivery_time = 'XX:XX AA - XX:XX PP';

        $coupon = new \Coupon();
        $coupon->code = 'C0D3XX';
        $coupon->amount = '50000';

        return $layout->renderTemplate($user, $order, $content, $complain_order, $coupon);
    }
}
