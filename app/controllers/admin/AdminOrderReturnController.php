<?php

namespace admin;

use Illuminate\Database\QueryException;
use Request, View, Input, Redirect, Session, DB, Str, Validator, Carbon\Carbon, City,
Product, Response, Config, AdminLog, Order, Store, Department, Shelf, PHPExcel,PHPExcel_Style_Fill,
PHPExcel_Writer_Excel2007, Pdf, Transporter, Driver, Vehicle, OrderReturn, Routes,
OrderReturnDetail, OrderLog, PHPExcel\IOFactory, PHPExcel_Cell, PHPExcel_Reader_Excel2007, Exception, Event, StoreProduct, Warehouse, StoreProductWarehouse, OrderProduct, Sampling;


class AdminOrderReturnController extends AdminController
{
    /**
     * Muestra la vista para seleccionar el tipo de devolución
     */
    public function index()
    {
        return View::make('admin.orders_return.index')->with('title', 'Devoluciones a Bodega');
    }

    /**
     * Obtener bodegas y transportadores por ciudad ajax
     */
    public function get_transporters_and_warehouses_ajax()
    {

        $warehouses = [];
        $transporters = [];
        if (Input::has('city_id')){
            $warehouses = Warehouse::select('id', 'warehouse')->where('city_id', Input::get('city_id'))->where('status', 1)->get()->toArray();
            $transporters = Transporter::select('id', 'fullname')->where('city_id', Input::get('city_id'))->where('status', 1)->orderBy('fullname')->get()->toArray();
        }

        $response = [
                      'warehouses' => $warehouses,
                      'transporters' => $transporters
                    ];
        return Response::json($response, 200);
    }

    /**
     * Obtener conductores y vehiculos por transportador ajax
     */
    public function get_drivers_vehicles_ajax()
    {
        $data = ['drivers' => [], 'vehicles' => []];
        if (Input::has('transporter_id')){
            $data['drivers'] = Driver::select('id', DB::raw("CONCAT(first_name, ' ', last_name) AS name"))->where('transporter_id', Input::get('transporter_id'))->where('status', 1)->orderBy('first_name')->orderBy('last_name')->get()->toArray();
                $data['vehicles'] = Vehicle::select('vehicles.id', 'vehicles.plate')->where('transporter_id', Input::get('transporter_id'))->where('status', 1)->orderBy('plate')->get()->toArray();
            }
        return Response::json($data, 200);
    }

    /**
     * Obtener rutas por ciudad ajax
     */
    public function get_routes_ajax()
    {
        $routes = [];
        $warehouses = [];
        if (Input::has('city_id')){
            $routes = Routes::select('id', 'route')->where('city_id', Input::get('city_id'))->orderBy('route')->get();
            $warehouses = Warehouse::select('id', 'warehouse')->where('city_id', Input::get('city_id'))->where('status', 1)->get()->toArray();
        }

        $response = [
                      'warehouses' => $warehouses,
                      'routes' => $routes
                    ];

        return Response::json($response, 200);
    }

    /**
     * Grilla de devoluciones de pedidos por ruta
     */
    public function index_route_returns()
    {
        if (!Request::ajax())
        {
            $cities = $this->get_cities();
            $routes = Routes::where('city_id', Session::get('admin_city_id'))->where('planning_date', '>', date('Y-m-d', strtotime('-4 days')))->orderBy('route')->get();
            $data = [
                'title' => 'Devoluciones a Bodega por Ruta',
                'cities' => $cities,
                'warehouses' => $this->get_warehouses(),
                'routes' => $routes
            ];
            return View::make('admin.orders_return.route_returns', $data);
        }
        else
        {
            $search = Input::has('s') ? Input::get('s') : '';
            $warehouse_id = Input::get('warehouse_id');
            $orders = OrderReturn::join('orders', 'order_returns.order_id', '=', 'orders.id')
                ->join('routes', 'routes.id', '=', 'orders.route_id')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->leftJoin('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
                ->where(function($query) use ($search){
                    $query->orWhere('vehicles.plate', 'LIKE', '%'.$search.'%');
                })
                ->where('stores.is_storage', 1)
                ->select('routes.id', 'route', 'order_returns.created_at', 'plate', 'routes.status_return',
                    DB::raw("COUNT(order_returns.id) AS orders,
                    SUM(IF(order_returns.status = 'Pendiente', 1, 0)) AS quantity_orders_pending,
                    SUM(IF(order_returns.status = 'Almacenado', 1, 0)) AS quantity_orders_stored "),
                     DB::raw("(SELECT COUNT(order_return_id)
                                FROM order_returns
                                INNER JOIN orders o ON o.id = order_id
                                INNER JOIN order_return_details ON order_return_id  = order_returns.id
                                INNER JOIN routes r ON r.id = route_id WHERE route_id = routes.id GROUP BY route_id) AS quantity_returned"));

            if (Input::get('status')){
                $status = Input::get('status');
                $statuses = explode(',', $status);
                unset($statuses[count($statuses) - 1]);
                $orders->where(function($query) use ($statuses){
                    foreach($statuses as $status)
                        $query->orWhere('routes.status_return', $status);
                });
            }

            if($warehouse_id)
                $orders->where('order_groups.warehouse_id', $warehouse_id);

            if ($date = format_date('mysql', Input::get('date')))
                $orders->whereBetween('order_returns.created_at', array($date.' 00:00:00', $date.' 23:59:59'));

            if (Input::get('route_id'))
                $orders->where('routes.id', Input::get('route_id'));
            if (Input::get('city_id')) {
                $city_id = Input::get('city_id');
                $orders->whereIn('cities.id', $this->get_all_city_ids($city_id));
            }

            $orders = $orders->groupBy('route', 'plate')->orderBy('status_return')->orderBy('routes.id', 'DESC')->limit(80)->get();

            return View::make('admin.orders_return.route_returns')
                        ->with('order_returns', $orders)
                        ->renderSections()['content'];
        }
    }

    /**
     * Grilla de devoluciones de pedidos
     */
    public function index_order_returns()
    {
        if (!Request::ajax())
        {
            $transporters = Transporter::where('status', 1)->where('city_id', Session::get('admin_city_id'))->orderBy('fullname')->get();
            $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->where('city_id', Session::get('admin_city_id'))->get();
            $cities = $this->get_cities();

            $data = [
                'title' => 'Devoluciones a Bodega',
                'cities' => $cities,
                'transporters' => $transporters,
                'warehouses' => $this->get_warehouses(),
                'stores' => $stores
            ];
            return View::make('admin.orders_return.order_returns', $data);
        }
        else
        {
            $search = Input::has('s') ? Input::get('s') : '';
            $warehouse_id = Input::get('warehouse_id');
            $orders = OrderReturn::join('orders', 'order_returns.order_id', '=', 'orders.id')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('order_return_details', 'order_return_details.order_return_id', '=', 'order_returns.id')
                ->leftJoin('drivers', 'orders.driver_id', '=', 'drivers.id')
                ->leftJoin(DB::raw('(vehicles, transporters)'), function($join){
                     $join->on('orders.vehicle_id', '=', 'vehicles.id');
                     $join->on('drivers.transporter_id', '=', 'transporters.id');
                })
                ->where(function($query) use ($search){
                    $query->where(DB::raw('CONCAT(order_groups.user_firstname, " ", order_groups.user_lastname)'), 'LIKE', '%'.$search.'%');
                    $query->orWhere('orders.id', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_returns.id', 'LIKE', '%'.$search.'%');
                    $query->orWhere('vehicles.plate', 'LIKE', '%'.$search.'%');
                })
                ->where('stores.is_storage', 1)
                ->select('order_returns.*', 'city', 'orders.status AS order_status', 'order_groups.user_firstname','order_groups.user_lastname','order_groups.user_address',
                'transporters.fullname AS transporter_name', DB::raw("CONCAT(drivers.first_name, ' ', drivers.last_name) AS driver_name"), 'vehicles.plate',
                DB::raw("(SELECT COUNT(order_return_id)
                                FROM order_returns orq
                                INNER JOIN orders o ON o.id = order_id
                                INNER JOIN order_return_details ON order_return_id  = orq.id
                                WHERE orq.id = order_returns.id GROUP BY route_id) AS quantity_returned"));

            if($warehouse_id)
                $orders->where('order_groups.warehouse_id', $warehouse_id);

            if (Input::get('status')){
                $status = Input::get('status');
                $statuses = explode(',', $status);
                unset($statuses[count($statuses) - 1]);
                $orders->where(function($query) use ($statuses){
                    foreach($statuses as $status)
                        $query->orWhere('order_returns.status', $status);
                });
            }

            if ($date = format_date('mysql', Input::get('date')))
                $orders->whereBetween('order_returns.created_at', array($date.' 00:00:00', $date.' 23:59:59'));

            if (Input::get('transporter_id'))
                $orders->where('transporters.id', Input::get('transporter_id'));
            if (Input::get('driver_id'))
                $orders->where('orders.driver_id', Input::get('driver_id'));
            if (Input::get('vehicle_id'))
                $orders->where('orders.vehicle_id', Input::get('vehicle_id'));
            if (Input::get('city_id')) {
                $city_id = Input::get('city_id');
                $orders->whereIn('cities.id', $this->get_all_city_ids($city_id));
            }

            $orders = $orders->groupBy('order_returns.id')->orderBy('order_returns.id', 'desc')->limit(80)->get();
            return View::make('admin.orders_return.order_returns')
                        ->with('order_returns', $orders)
                        ->renderSections()['content'];
        }
    }

    /**
    * Editar devolucion a bodega por ruta
    */
    public function edit_route_return($id)
    {
        if (!Request::ajax())
        {
            $route_returns = OrderReturn::select(DB::raw('count(order_returns.id) AS quantity_orders,
                                        SUM(IF(order_returns.status = "Pendiente", 1, 0)) AS quantity_orders_pending,
                                        SUM(IF(order_returns.status = "Almacenado", 1, 0)) AS quantity_orders_stored'),
                                        'order_returns.*', 'routes.id', 'admin.fullname AS admin_name', 'driver_name',
                                        'city', 'routes.status_return', 'route', 'routes.status_return')
                                        ->leftJoin('drivers', 'driver_id', '=', 'drivers.id')
                                        ->join('orders', 'orders.id', '=', 'order_returns.order_id')
                                        ->join('routes', 'routes.id', '=', 'orders.route_id')
                                        ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                                        ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                                        ->leftJoin('admin', 'routes.admin_return_id', '=', 'admin.id')
                                        ->where('routes.id', $id)
                                        ->groupBy('order_returns.id')
                                        ->first();

            $data = [
                'title' => 'Devoluciones por Ruta ',
                'route_returns' => $route_returns
            ];
            return View::make('admin.orders_return.route_return_details', $data);
        }
        else
        {
            $search = Input::has('s') ? Input::get('s') : '';
            $orders = OrderReturn::join('orders', 'order_returns.order_id', '=', 'orders.id')
                ->join('routes', 'routes.id', '=', 'orders.route_id')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('order_return_details', 'order_return_details.order_return_id', '=', 'order_returns.id')
                ->leftJoin('drivers', 'orders.driver_id', '=', 'drivers.id')
                ->leftJoin(DB::raw('(vehicles, transporters)'), function($join){
                     $join->on('orders.vehicle_id', '=', 'vehicles.id');
                     $join->on('drivers.transporter_id', '=', 'transporters.id');
                })
                ->where(function($query) use ($search){
                    $query->orWhere('orders.id', 'LIKE', '%'.$search.'%');
                })
                ->where('stores.is_storage', 1)
                ->where('routes.id', $id)
                ->select('order_returns.*',
                         'city', 'orders.status AS order_status', 'order_groups.user_firstname','order_groups.user_lastname',
                         'order_groups.user_address', 'transporters.fullname AS transporter_name',
                         DB::raw("CONCAT(drivers.first_name, ' ', drivers.last_name) AS driver_name"), 'vehicles.plate',
                         DB::raw("COUNT(order_return_details.id) AS quantity_returned"));

            if (Input::get('status')){
                $status = Input::get('status');
                $statuses = explode(',', $status);
                unset($statuses[count($statuses) - 1]);
                $orders->where(function($query) use ($statuses){
                    foreach($statuses as $status)
                        $query->orWhere('order_returns.status', $status);
                });
            }

            $orders = $orders->groupBy('order_returns.id')->orderBy('order_returns.id', 'desc')->limit(80)->get();

            return View::make('admin.orders_return.route_return_details')
                        ->with('order_returns', $orders)
                        ->renderSections()['content'];
        }
    }

    /**
    * Editar devolucion a bodega
    */
    public function edit_order_return($id)
    {
        $order_return = OrderReturn::select('order_returns.*', 'admin.fullname AS admin_name', 'driver_name', 'city',
                                        DB::raw("SUM(IF(order_return_details.status = 'Devuelto', 1, 0)) AS quantity_returned"), DB::raw("COUNT(order_return_details.id) AS quantity_to_return"))
                                        ->leftJoin('admin', 'admin_id', '=', 'admin.id')
                                        ->leftJoin('drivers', 'driver_id', '=', 'drivers.id')
                                        ->join('order_return_details', 'order_return_details.order_return_id', '=', 'order_returns.id')
                                        ->join('orders', 'orders.id', '=', 'order_returns.order_id')
                                        ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                                        ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                                        ->where('order_returns.id', $id)
                                        ->groupBy('order_returns.id')
                                        ->first();
        if (!$order_return)
            return Redirect::route('adminOrderReturn.orderReturns')->with('error', 'No existe la devolución a bodega.');

        return View::make('admin.orders_return.order_return_details')
                ->with('title', 'Devolución a Bodega')
                ->with('order_return', $order_return);
    }

    /**
     * Obtener productos de una devolucion de bodega
     */
    public function get_products_ajax($id)
    {
        $response = ['status' => false];
        $is_return_products = 1;
        $returned_products = OrderReturnDetail::select('order_return_details.id', 'order_return_details.status', 'reference', 'provider_plu', 'order_return_details.store_product_id',
                                    'product_image_url', 'product_name', 'product_quantity', 'product_unit', 'stock_updated', 'return_stock_updated', 'order_returns.status AS order_return_status')
                                  ->join('order_returns', 'order_returns.id', '=', 'order_return_id')
                                  ->leftJoin(DB::raw('(store_products, products)'), function($join){
                                       $join->on('order_return_details.store_product_id', '=', 'store_products.id');
                                       $join->on('store_products.product_id', '=', 'products.id');
                                  })
                                  ->where('order_return_id', $id);
        if (Input::get('s')){
            $returned_products = $order_return_details = $returned_products->where('product_reference', Input::get('s'))->where('order_return_details.status', 'Pendiente', 'DESC')->orderByRaw("FIELD(order_return_details.status, 'Pendiente') DESC")->limit(1)->get();
            if (count($order_return_details) > 0){
                foreach ($order_return_details as $order_return_detail) {
                    $order_return_detail->status = Input::get('status');
                    $order_return_detail->save();

                    //log de recibido
                    $log = new AdminLog();
                    $log->table = 'order_return_details';
                    $log->row_id = $order_return_detail->id;
                    $log->action = 'update';
                    $log->admin_id = Session::get('admin_id');
                    $log->save();
                }

            }else{
                unset($returned_products);
                $returned_products = OrderReturnDetail::select('order_return_details.id', 'order_return_details.status', 'reference', 'provider_plu', 'order_return_details.store_product_id',
                                    'product_image_url', 'product_name', 'product_quantity', 'product_unit', 'stock_updated', 'return_stock_updated', 'order_returns.status AS order_return_status')
                                  ->join('order_returns', 'order_returns.id', '=', 'order_return_id')
                                  ->leftJoin(DB::raw('(store_products, products)'), function($join){
                                       $join->on('order_return_details.store_product_id', '=', 'store_products.id');
                                       $join->on('store_products.product_id', '=', 'products.id');
                                  })
                                  ->where('order_return_id', $id)
                                  ->where('product_reference', Input::get('s'))
                                  ->orderByRaw("FIELD(order_return_details.status, 'Pendiente') DESC")->limit(1)->get();

                $is_return_products = 0;
            }
        }else{
            $returned_products = $returned_products->orderByRaw("FIELD(order_return_details.status, 'Pendiente') DESC")->get();
        }

        $data = [
            'section' => 'content',
            'returned_products' => $returned_products,
        ];
        $view = View::make('admin.orders_return.order_return_details', $data)->renderSections();
        $data_json['html'] = $view['content'];

        if ( !$is_return_products ) {
            if (!$returned_products && Input::get('s')) {
                $data_json['success'] = true;
                $data_json['message'] = 'Este producto no hace parte de la devolución';
            }else{
                $data_json['error'] = true;
                $data_json['message'] = 'No se encontraron productos.';

            }
        }
        return Response::json($data_json);
    }

    /**
     * Actualiza devolucion de pedido
     */
    public function update_order_return($id)
    {
        //guardar observacion
        if ($order_return = OrderReturn::find($id)){
            $order_return->observation = Input::get('observation');
            $order_return->save();
            return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $order_return->id])->with('success', 'Devolución a bodega actualizada con éxito.');
        }

        return Redirect::route('adminOrderReturn.orderReturns')->with('error', 'No existe la devolución a bodega.');
    }

    /**
     * Actualiza cantidad de unidades devueltas de un producto
     */
    public function update_product_order_return_ajax($id)
    {
        $response = ['status' => false];

        if ($order_return = OrderReturn::where('id', $id)->whereIn('status', ['Pendiente', 'Parcialmente devuelto'])->first())
        {
            $order_return_detail = OrderReturnDetail::where('order_return_id', $order_return->id)->where('id', Input::get('order_return_product_id'))->first();
            $order_return_detail->status = Input::get('status');
            $order_return_detail->save();

            //log de recibido
            $log = new AdminLog();
            $log->table = 'order_return_details';
            $log->row_id = $order_return_detail->id;
            $log->action = 'update';
            $log->admin_id = Session::get('admin_id');
            $log->save();

            $response = ['status' => true, 'product_status' => $order_return_detail->status];
        }

        return Response::json($response , 200);
    }

    /**
     * Actualiza el estado de una devolucion a bodega
     * @param $id
     * @return
     */
    public function update_status_order_return($id)
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $order_return = OrderReturn::select('order_returns.*', 'stores.city_id', 'order_groups.warehouse_id')
            ->join('orders', 'orders.id', '=', 'order_returns.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('order_groups', 'order_groups.id', '=', 'group_id')
            ->where('order_returns.id', $id)
            ->first();

        if ($order_return)
        {
            $array_status = ['Devuelto', 'Dañado'];
            //actualizar estado especifico
            if (Input::has('status'))
            {
                $new_status = Input::get('status');
                if ($new_status == 'Almacenado')
                {
                    if ($order_return->status != 'Devuelto')
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'El estado actual de la devolución a bodega no es valida.');
                    if (get_config_option('block_reception_return_storage', $order_return->warehouse_id))
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'El almacenamiento de productos esta deshabilitado, por favor contacte al administrador.');

                    //validar estado de productos
                    $order_return_products = OrderReturnDetail::where('order_return_id', $id)->where('stock_updated', 0)->get();

                    if (!count($order_return_products))
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'Los productos ya fueron actualizados al stock de bodega.');

                    foreach ($order_return_products as $order_return_product) {
                        $store_product_warehouse = StoreProductWarehouse::join('store_products', 'store_product_id', '=','store_products.id')->where('store_product_warehouses.warehouse_id', $order_return->warehouse_id)
                            ->where('store_product_warehouses.store_product_id', $order_return_product->store_product_id)
                            ->first();
                        $validateBlockedPositions = \Warehouse::validateBlockedPositions($store_product_warehouse->warehouse_id, 'Alistamiento', $store_product_warehouse->storage, $store_product_warehouse->storage_height_position, $store_product_warehouse->storage_position);
                        if ($validateBlockedPositions) {
                            return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'Las posiciones de algunos productos a devolver se encuentran bloqueadas debido a que se encuentran en conteo, intentalo más tarde.');
                        }
                    }

                    //actualizar stock de bodega de productos devueltos
                    try {

                        DB::beginTransaction();
                        $productsToUpdate =[];
                        foreach($order_return_products as $order_return_product)
                        {
                            $order_product = OrderProduct::where('order_id', $order_return->order_id)->where('store_product_id', $order_return_product->store_product_id)->first();
                            if ($order_return_product->status == 'Devuelto' && $order_return_product->return_stock_updated && !$order_return_product->stock_updated)
                            {
                                $productsToUpdate[$order_return_product->store_product_id]['return_stock'] = !isset($productsToUpdate[$order_return_product->store_product_id]['return_stock'])?1 : $productsToUpdate[$order_return_product->store_product_id]['return_stock']+1;
                                $productsToUpdate[$order_return_product->store_product_id]['picking_stock'] = !isset($productsToUpdate[$order_return_product->store_product_id]['picking_stock'])?1 : $productsToUpdate[$order_return_product->store_product_id]['picking_stock']+1;

                                $order_return_product->stock_updated = 1;
                            }
                            /*if ($order_product && $order_product->type == 'Muestra') {
                                $sampling = Sampling::where('store_product_id', $store_product_warehouse->store_product_id)->where('warehouse_id', $order_return->warehouse_id)->first();
                                $sampling->quantity += 1;
                                $sampling->save();
                            }*/
                            $order_return_product->save();
                        }
                        $storeProductsWarehouse = StoreProductWarehouse::getStoreProductsWarehouseWithLocking($order_return->warehouse_id, array_keys($productsToUpdate));
                        foreach($storeProductsWarehouse as $storeProductWarehouse){
                            $product = $productsToUpdate[$storeProductWarehouse->store_product_id];
                            $storeProductWarehouse->return_stock -= $product['return_stock'];
                            $storeProductWarehouse->picking_stock += $product['picking_stock'];
                            $storeProductWarehouse->save();
                        }

                        DB::commit();
                    } catch(QueryException $e){
                        DB::rollback();
                        if($e->getCode() == '40001'){
                            \ErrorLog::add($e,513);
                            return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id])->with('error', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');
                        }else{
                            \ErrorLog::add($e, $e->getCode());
                            echo $e->getMessage();
                            return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id])->with('error', 'Ocurrió un error al actualizar el stock de los productos.');
                        }
                    } catch(\Exception $e){
                        \ErrorLog::add($e, $e->getCode());
                        DB::rollback();
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id])->with('error', 'Ocurrió un error al actualizar el stock de los productos.');
                    }
                    $order_return->storage_date = date('Y-m-d H:i:s');
                    $order_return->status = $new_status;
                }
            }else{
                //validar estado de productos
                $order_return_products = OrderReturnDetail::where('order_return_id', $id)->where('return_stock_updated', 0)->get();
                if (!count($order_return_products))
                    return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'Los productos ya fueron actualizados al stock de devoluciones.');

                foreach ($order_return_products as $order_return_product) {
                    $store_product_warehouse = StoreProductWarehouse::join('store_products', 'store_product_id', '=','store_products.id')->where('store_product_warehouses.warehouse_id', $order_return->warehouse_id)
                        ->where('store_product_warehouses.store_product_id', $order_return_product->store_product_id)
                        ->first();
                    $validateBlockedPositions = \Warehouse::validateBlockedPositions($store_product_warehouse->warehouse_id, 'Alistamiento', $store_product_warehouse->storage, $store_product_warehouse->storage_height_position, $store_product_warehouse->storage_position);
                    if ($validateBlockedPositions) {
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'Las posiciones de algunos productos a devolver se encuentran bloqueadas debido a que se encuentran en conteo, intentalo más tarde.');
                    }
                }

                //actualizar stock de devoluciones de productos devueltos
                try {
                    DB::beginTransaction();
                    $productsToUpdate =[];
                    foreach($order_return_products as $order_return_product)
                    {
                        if ($order_return_product->status == 'Devuelto' && !$order_return_product->return_stock_updated)
                        {
                            $productsToUpdate[$order_return_product->store_product_id]['return_stock'] = !isset($productsToUpdate[$order_return_product->store_product_id])?1 : $productsToUpdate[$order_return_product->store_product_id]['return_stock']+1;

                            $order_return_product->return_stock_updated = 1;
                            $order_return_product->save();
                        }else{
                            if ($order_return_product->status == 'Dañado' && !$order_return_product->return_stock_updated)
                            {
                                $productsToUpdate[$order_return_product->store_product_id]['shrinkage_stock'] = !isset($productsToUpdate[$order_return_product->store_product_id])?1 : $productsToUpdate[$order_return_product->store_product_id]['shrinkage_stock']+1;
                            }
                        }

                    }
                    $storeProductsWarehouse = StoreProductWarehouse::getStoreProductsWarehouseWithLocking($order_return->warehouse_id, array_keys($productsToUpdate));

                    foreach($storeProductsWarehouse as $storeProductWarehouse){
                        $product = $productsToUpdate[$storeProductWarehouse->store_product_id];
                        if(isset($product['return_stock'])){
                            $storeProductWarehouse->return_stock += $product['return_stock'];
                        }
                        if(isset($product['shrinkage_stock'])){
                            $storeProductWarehouse->shrinkage_stock += $product['shrinkage_stock'];
                        }
                        $storeProductWarehouse->save();
                    }

                    $storeProductsWarehouse = StoreProductWarehouse::getStoreProductsWarehouseWithLocking($order_return->warehouse_id, array_keys($productsToUpdate));
                    foreach($storeProductsWarehouse as $storeProductWarehouse){
                        $product = $productsToUpdate[$storeProductWarehouse->store_product_id];
                        if(isset($product['return_stock'])){
                            $storeProductWarehouse->return_stock += $product['return_stock'];
                        }
                        if(isset($product['shrinkage_stock'])){
                            $storeProductWarehouse->shrinkage_stock += $product['shrinkage_stock'];
                        }
                        $storeProductWarehouse->save();
                    }

                    DB::commit();

                } catch(QueryException $e){
                    DB::rollback();
                    if($e->getCode() == '40001'){
                        \ErrorLog::add($e,513);
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id])->with('error', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');
                    }else{
                        \ErrorLog::add($e,$e->getCode());
                        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id])->with('error', 'Ocurrió un error al actualizar el stock de devoluciones de los productos.');
                    }
                } catch(\Exception $e){
                    \ErrorLog::add($e, $e->getCode());
                    DB::rollback();
                    return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id])->with('error', 'Ocurrió un error al actualizar el stock de devoluciones de los productos.');
                }
                sleep(3);
                //actualizar estado dinamicamente
                $order_return_products = OrderReturnDetail::where('order_return_id', $id)->whereIn('status', ['Devuelto', 'Pendiente'])->where('return_stock_updated', 0)->get();
                if (!count($order_return_products)){
                    $order_return->status = 'Devuelto';
                    $order_return->return_date = date('Y-m-d H:i:s');
                }else $order_return->status = 'Parcialmente devuelto';
            }

            $order_return->save();

            $log = new OrderLog();
            $log->type = 'Estado de devolución a bodega # '.$order_return->id.' actualizada: '.$order_return->status;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order_return->order_id ;
            $log->save();

            return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('success', 'Devolución a bodega actualizada con éxito.');
        }
        return Redirect::route('adminOrderReturn.editOrderReturn', ['id' => $id ])->with('error', 'Ocurrió un error al actualizar la devolución a bodega.');
    }


    /**
    * Actualiza el estado de una ruta con devolución
    */
    public function update_status_route_return($id){

        $orders_return = OrderReturn::join('orders', 'orders.id', '=', 'order_returns.order_id')
                                    ->join('routes', 'routes.id', '=', 'orders.route_id')
                                    ->where('routes.id', $id)
                                    ->where('order_returns.status', 'Pendiente')
                                    ->get();
        if (count($orders_return))
            return Redirect::route('adminOrderReturn.editRouteReturn', ['id' =>  $id ])->with('error', 'Existen devoluciones a bodega pendientes.');
        else{

            $route = Routes::find($id);
            $route->status_return = 'Validada';
            $route->admin_return_id = Session::get('admin_id');
            $route->save();

            //log de recibido
            $log = new AdminLog();
            $log->table = 'routes';
            $log->row_id = $route->id;
            $log->action = 'update';
            $log->admin_id = Session::get('admin_id');
            $log->save();

            return Redirect::route('adminOrderReturn.editRouteReturn', ['id' =>  $id ])->with('success', 'Ruta actualizada con éxito.');
        }
    }

    /**
     * Eliminar devolución a bodega
     */
    public function delete_order_return($id)
    {
        $order_return = OrderReturn::select('order_returns.*', 'stores.city_id', 'order_groups.warehouse_id')
            ->join('orders', 'orders.id', '=', 'order_returns.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('order_groups', 'order_groups.id', '=', 'group_id')
            ->where('order_returns.id', $id)
            ->first();

        if (!$order_return)
            return Redirect::route('adminOrderReturn.orderReturns')->with('error', 'No existe la devolución a bodega.');
        if ($order_return->status != 'Pendiente')
            return Redirect::route('adminOrderReturn.orderReturns')->with('error', 'La devolución a bodega esta en un estado no valido.');

        $order_return_products = OrderReturnDetail::where('order_return_id', $id)->where('return_stock_updated', 1)->get();

        if (count($order_return_products)) {
            try {
                DB::beginTransaction();
                foreach ($order_return_products as $order_return_product) {
                    if ($order_return_product->status == 'Devuelto') {
                        $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(
                            $order_return->warehouse_id,
                            $order_return_product->store_product_id
                        );
                        $store_product_warehouse->return_stock -= 1;
                        $store_product_warehouse->save();
                    } else {
                        if ($order_return_product->status == 'Dañado') {
                            $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(
                                $order_return->warehouse_id,
                                $order_return_product->store_product_id
                            );
                            $store_product_warehouse->shrinkage_stock -= 1;
                            $store_product_warehouse->save();
                        }
                    }
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                \ErrorLog::add($e);
                return Redirect::route('adminOrderReturn.orderReturns')->with('error', $e->getMessage());
            }

        }

        OrderReturnDetail::where('order_return_id', $id)->delete();
        OrderReturn::where('id', $id)->delete();

        $log = new AdminLog();
        $log->table = 'order_returns';
        $log->row_id = $order_return->id;
        $log->action = 'delete';
        $log->admin_id = Session::get('admin_id');
        $log->save();

        return Redirect::route('adminOrderReturn.orderReturns')->with('success', 'Devolución a bodega eliminada con éxito.');
    }


    /**
     * Exporta en pdf para imprimir la devolución.
     */
    public function return_order_pdf($id)
    {
        $order_return = OrderReturn::find($id);
        if (!$order_return){
            return Redirect::route('adminOrderReturn.orderReturns', ['id' =>  $id ])->with('type', 'failed')->with('message', 'No existe el pedido.');
        }else{
            Pdf::order_return_bodega_pdf($id);
        }
    }
}
?>