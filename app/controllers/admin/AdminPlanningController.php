<?php

namespace admin;

use Carbon\Carbon;
use ErrorLog;
use Illuminate\Database\QueryException;
use Request, View, Input, Redirect, Session, DB, Response, Order, Vehicle,
    PHPExcel\IOFactory, Event, OrderEventHandler, Transporter, OrderProduct, Warehouse,
    Driver, Routes, Fpdf, Store, City;

class AdminPlanningController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new OrderEventHandler);
    }

    /**
     * Despachar pedidos
     */
    public function transport()
    {
       $warehouse = Warehouse::where('city_id', Session::get('admin_city_id'))->first();
       $routes = Routes::select('routes.id', 'route')->where(function($query){
                            $query->where('orders.status', '=', 'Enrutado')
                            ->orWhere('orders.status', '=', 'In Progress')
                            ->orWhere('orders.status', '=', 'Alistado')
                            ->orWhere('orders.status', '=', 'Dispatched');
                        })
                        ->where('routes.city_id', Session::get('admin_city_id'))
                        ->where('order_groups.warehouse_id', $warehouse->id)
                        ->join('orders', 'orders.route_id', '=', 'routes.id')
                        ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                        ->groupBy('routes.id')
                        ->orderBy('routes.id')
                        ->get();

        $transporters = Transporter::where('city_id', Session::get('admin_city_id'))->where('status', 1)->orderBy('fullname')->get();
        $vehicles = Vehicle::where('status', 1)->orderBy('plate')->get();
        $drivers = Driver::where('status', 1)->where('profile', 'Conductor')->orderBy('first_name')->orderBy('last_name')->get();
        $data = [
            'cities' => $this->get_cities(),
//            'warehouses' => $this->get_warehouses(),
            'warehouses' =>  Warehouse::where('city_id', Session::get('admin_city_id'))
                ->select('warehouses.id', 'warehouses.warehouse')->get(),
            'routes' => $routes,
            'title' => 'Despacho de Pedidos',
            'transporters' => $transporters,
            'vehicles' => $vehicles,
            'drivers' => $drivers
        ];
        return View::make('admin.planning.transport', $data);
    }

    /**
     * Validar pedidos de una ruta
     */
    public function validate_routes()
    {
        $route_id = Input::get('route_id');
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $errors = [];
        $success = 0;

        $orders = Order::where('route_id', $route_id)
                            ->where(function($query){
                                $query->where('orders.status', '=', 'Enrutado')
                                ->orWhere('orders.status', '=', 'In Progress')
                                ->orWhere('orders.status', '=', 'Alistado');
                            })
                            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                            ->leftJoin('stores', 'orders.store_id', '=', 'stores.id')
                            ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                            ->whereIn('cities.id', $this->get_all_city_ids($city_id))
                            ->where('stores.status', 1)
                            ->whereNotNull('orders.route_id')
                            ->select('orders.id', 'orders.cc_charge_id', 'orders.status', 'stores.is_storage')
                            ->orderBy('orders.route_id')
                            ->get();
        if (!count($orders))
            return Redirect::route('adminPlanning.transport')
                           ->with('city_id', $city_id)
                           ->with('warehouse_id', $warehouse_id)
                           ->with('route_id', $route_id)
                           ->with('assign_exception', 'No se encontraron pedidos.');

        foreach ($orders as $key => $order)
        {
            if ($order->status == 'Alistado')
            {
                //validar que los productos no esten pendientes
                $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status', ['Pending', 'Missing'])->count();
                if ($order_products){
                    $errors[$order->id]['products'] = 'Hay productos pendientes o faltantes en el pedido.';
                    $errors[$order->id]['is_storage'] = $order->is_storage;
                }

                if ($order->payment_method == 'Tarjeta de crédito'){
                    $payments = $order->getPayments(true);
                    if (!$payments || ($payments && !empty($payments->cc_refund_date))){
                        $errors[$order->id]['payment_method'] = 'Se debe realizar primero el cobro a la tarjeta de crédito.';
                        $errors[$order->id]['is_storage'] = $order->is_storage;
                    }
                }

                if (empty($errors[$order->id]))
                    $success++;

            }else{
                $errors[$order->id]['status'] = 'El pedido no esta en estado Alistado.';
                $errors[$order->id]['is_storage'] = $order->is_storage;
            }
        }

        return Redirect::route('adminPlanning.transport')
                        ->with('city_id', $city_id)
                        ->with('warehouse_id', $warehouse_id)
                        ->with('route_id', $route_id)
                        ->with('validate_errors', $errors)
                        ->with('validate_success', $success);
    }

    /**
     * Asignar pedidos de una ruta a un transportador
     */
    public function assign_transporters()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(120);

        $route_id = Input::get('route_id');
        $vehicle = Vehicle::find(Input::get('vehicle_id'));
        $driver_id = Driver::find(Input::get('driver_id'));
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $errors = [];
        $success = 0;

        $orders = Order::select('orders.*', 'order_groups.warehouse_id')
                        ->where('orders.route_id', $route_id)
                        ->where(function($query){
                            $query->where('orders.status', 'Enrutado')
                                ->orWhere('orders.status', 'In Progress')
                                ->orWhere('orders.status', 'Alistado');
                        })
                        ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                        ->where('orders.status', '<>', 'Dispatched')
                        ->where('orders.is_hidden', 0)
                        ->whereIn('order_groups.user_city_id', $this->get_all_city_ids($city_id))
                        ->orderBy('orders.delivery_date')
                        ->get();
        if (!$orders)
            return Redirect::back()
                            ->with('city_id', $city_id)
                            ->with('warehouse_id', $warehouse_id)
                            ->with('route_id', $route_id)
                            ->with('assign_exception', 'No hay pedidos para asignar en esa ruta.');

        try {
            DB::beginTransaction();

            foreach ($orders as $key => $order)
            {
                if ($order->status == 'Alistado')
                {
                    //validar que los productos no esten pendientes
                    $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status', ['Pending', 'Missing'])->count();
                    if ($order_products)
                        $errors[$order->id]['products'] = 'Hay productos pendientes o faltantes en el pedido.';

                    if ($order->payment_method == 'Tarjeta de crédito'){
                        $payments = $order->getPayments(true);
                        if (!$payments || ($payments && !empty($payments->cc_refund_date)))
                            $errors[$order->id]['payment_method'] = 'Se debe realizar primero el cobro a la tarjeta de crédito.';
                    }

                    $order_products = OrderProduct::where('order_id', $order->id)->where('fulfilment_status', 'Fullfilled')->count();
                    if (!$order_products)
                        $errors[$order->id]['products'] = 'No se puede despachar por que no hay productos disponibles en el pedido.';

                    if (empty($errors[$order->id]))
                    {
                        $order->status = 'Dispatched';
                        $order->dispatched_date = date('Y-m-d H:i:s');
                        $order->driver_id = $driver_id->id;
                        $order->vehicle_id = $vehicle->id;

                        //$order->updatePickedStock();

                        $order->save();
                        $success++;

                        Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);
                    }
                }else $errors[$order->id]['status'] = 'El pedido no esta en estado Alistado.';
            }

            DB::commit();

            return Redirect::back()
                            ->with('city_id', $city_id)
                            ->with('warehouse_id', $warehouse_id)
                            ->with('route_id', $route_id)
                            ->with('assign_errors', $errors)->with('assign_success', $success);

        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                return Redirect::back()
                    ->with('city_id', $city_id)
                    ->with('warehouse_id', $warehouse_id)
                    ->with('route_id', $route_id)
                    ->with('assign_exception', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');

            }
            return Redirect::back()
                ->with('city_id', $city_id)
                ->with('warehouse_id', $warehouse_id)
                ->with('route_id', $route_id)
                ->with('assign_exception', $e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::back()
                ->with('city_id', $city_id)
                ->with('warehouse_id', $warehouse_id)
                ->with('route_id', $route_id)
                ->with('assign_exception', $e->getMessage());
        }
    }

    /**
     * Reasignar pedidos de una ruta a un transportador
     */
    public function reassign_transporters()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $route_id = Input::get('route_id');
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $driver_id = Input::get('driver_id');
        $vehicle_id = Input::get('vehicle_id');

        if (!$vehicle = Vehicle::find(Input::get('vehicle_id')))
            return Redirect::back()
                            ->with('city_id', $city_id)
                            ->with('warehouse_id', $warehouse_id)
                            ->with('route_id', $route_id)
                            ->with('assign_exception', 'Vehículo no existe.');

        $success = 0;

        $orders = Order::select('orders.*')->where('route_id', $route_id)
                            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                            ->where('orders.status', 'Dispatched')
                            ->whereIn('order_groups.user_city_id', $this->get_all_city_ids($city_id))
                            ->orderBy('orders.route_id')
                            ->get();
        try {
            DB::beginTransaction();

            foreach ($orders as $key => $order)
            {
                $order->driver_id = $driver_id;
                $order->vehicle_id = $vehicle_id;
                $order->save();

                Event::fire('order.transporter_log', [[$order, Session::get('admin_id'), $vehicle->plate, $order->driver_id]]);

                $success++;
            }

            DB::commit();

            return Redirect::back()
                           ->with('city_id', $city_id)
                           ->with('warehouse_id', $warehouse_id)
                           ->with('assign_success', $success);

        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::back()
                            ->with('city_id', $city_id)
                            ->with('warehouse_id', $warehouse_id)
                            ->with('route_id', $route_id)
                            ->with('assign_exception', $e->getMessage());
        }
    }

    /**
     * Obtiene vehiculos de un transportador
     */
    public function get_vehicles_ajax()
    {
        $transporter_id = Input::get('transporter_id');
        $vehicles = Vehicle::select('id', 'plate')->where('status', 1);
        if ( !empty($transporter_id) ) {
            $vehicles->where('transporter_id', $transporter_id);
        }
        $vehicles = $vehicles->orderBy('plate')->get();

        return Response::json($vehicles);
    }

    /**
     * Obtiene conductores de un transportador
     */
    public function get_drivers_ajax()
    {
        $vehicle_id = Input::get('vehicle_id');
        $drivers = Driver::select('drivers.id', 'drivers.first_name', 'drivers.last_name')
            ->join('vehicle_drivers','vehicle_drivers.driver_id','=','drivers.id')
            ->where('drivers.status', 1)
            ->where('drivers.profile', 'Conductor');
        if ( !empty($vehicle_id) ) {
            $drivers->where('vehicle_drivers.vehicle_id', $vehicle_id);
        }
        $drivers = $drivers->orderBy('drivers.first_name')->orderBy('drivers.last_name')->get();

        return Response::json($drivers);
    }

    /**
     * Cambiar rutas dependiendo ciudad
     */
    public function get_change_city()
    {
        $city_id = Request::get('id');

        if (Request::ajax())
        {
            $transporters = Transporter::where('city_id', $city_id)->where('status', 1)->orderBy('fullname')->get();
            $warehouses = Warehouse::where('city_id', $city_id)->select('id', 'warehouse')->get();
            $routes = Routes::select('routes.id', 'route')->where(function($query){
                                $query->where('orders.status', '=', 'Enrutado')
                                ->orWhere('orders.status', '=', 'In Progress')
                                ->orWhere('orders.status', '=', 'Alistado')
                                ->orWhere('orders.status', '=', 'Dispatched');
                            })
                            ->join('orders', 'orders.route_id', '=', 'routes.id')
                            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                            ->whereIn('order_groups.user_city_id', $this->get_all_city_ids($city_id))
                            ->groupBy('routes.id')
                            ->orderBy('routes.id')
                            ->get();
            $data = [
                'routes' => $routes,
                'warehouses' => $warehouses,
                'transporters' => $transporters,
            ];

            return Response::json($data);
        }
    }

    /**
     * Cambiar rutas dependiendo ciudad
     */
    public function get_change_warehouse()
    {
        $city_id = Request::get('id');

        if (Request::ajax())
        {
            $routes = Routes::select('routes.id', 'route')->where(function($query){
                                $query->where('orders.status', '=', 'Enrutado')
                                ->orWhere('orders.status', '=', 'In Progress')
                                ->orWhere('orders.status', '=', 'Alistado')
                                ->orWhere('orders.status', '=', 'Dispatched');
                            })
                            ->join('orders', 'orders.route_id', '=', 'routes.id')
                            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                            ->whereIn('order_groups.user_city_id', $this->get_all_city_ids($city_id));

            if(Request::has('warehouse_id'))
                $routes->where('order_groups.warehouse_id', Request::get('warehouse_id'));

            $routes = $routes->groupBy('routes.id')->orderBy('routes.id')->get();
            $data = [
                'routes' => $routes
            ];

            return Response::json($data);
        }
    }

    /**
     * Cargar informacion de la ruta en los select
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_info_route()
    {
        $route_id = Request::get('id');

        if (Request::ajax())
        {
            $routes = Routes::select('routes.suggested_vehicle_id', 'vehicles.transporter_id','vehicle_drivers.driver_id')
                ->where('routes.id',$route_id)
                ->join('vehicles','vehicles.id', '=','routes.suggested_vehicle_id')
                ->join('vehicle_drivers','vehicles.id', '=','vehicle_drivers.vehicle_id')
                ->first();

            return Response::json($routes);
        }
    }

    /**
     * Generar PDF para orden de salida de cargue
     */
    public function pdf_order_departure()
    {
        //$order_departure = Order::select()->get();
        $layette_number = Input::has('layette_number') ? Input::get('layette_number') : 0;
        $bag_number = Input::has('bag_number') ? Input::get('bag_number') : 0;
        $dataphone = Input::has('dataphone') == 0 ? 'No':'Sí';
        $visiting_card = Input::get('visiting_card') == 0 ? 'No':'Sí';
        $endowment = Input::get('endowment') == 0 ? 'No':'Sí';
        $bags = Input::get('bags') == 0 ? 'No':'Sí';
        $vehicle = Vehicle::select('plate', 'class_type')
            ->where('id', Input::get('order_departure_vehicle_id'))
            ->first();
        $route = Routes::select('route')
            ->where('id', Input::get('order_departure_route_id'))
            ->first();
        $driver = Driver::select('first_name', 'last_name')
            ->where('id', Input::get('order_departure_driver_id'))
            ->first();
        $store = Store::select('id')->where('city_id', Input::get('order_departure_city_id'))->where('status', 1)->first();
        $cities = City::select('id')->where('coverage_store_id', $store->id)->where('status', 1)->get()->toArray();
        foreach ($cities as $key => &$value)
            $cities[$key] = $value['id'];
        $orders = Order::select(DB::raw('COUNT(orders.id) as count'))
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->whereIn('order_groups.user_city_id', $cities)
            ->where('orders.route_id', Input::get('order_departure_route_id'))->first();

        $pdf = new Fpdf('P','mm','letter');
        $pdf::AddPage();
        $pdf::Cell(0,10,$pdf::Image(url('/') . '/admin_asset/img/logo_pdf.png', $pdf::GetX() + 80, $pdf::GetY(), 30),0,2,'C',false);
        $pdf::SetDrawColor(210,210,210);
        $pdf::SetFillColor(241, 241, 241);
        $pdf::SetFont('Arial','B',8);
        $pdf::Cell(0,5,utf8_decode('ORDEN DE SALIDA CARGUE MERQUEO SAS'),1,2,'C',true);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(10,7,utf8_decode('RUTA'),1,0,'C',true);
        $pdf::Cell(60,7,utf8_decode($route['route']),1,0,'L',false);
        $pdf::Cell(18,7,utf8_decode('No. PEDIDOS'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B',12);
        $pdf::Cell(10,7,$orders['count'],1,0,'C',false);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(25,7,utf8_decode('TIPO DE VEHÍCULO'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B',12);
        $pdf::Cell(20,7,utf8_decode($vehicle['class_type']),1,0,'C',false);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(17,7,utf8_decode('FECHA'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(30,7,date('d/m/Y h:i:s A'),1,0,'C',false);
        $pdf::Ln(7);
        $pdf::Cell(20,7,utf8_decode('PLACA'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B', 12);
        $pdf::Cell(20,7,utf8_decode($vehicle['plate']),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(20,7,utf8_decode('CONDUCTOR'),1,0,'C',true);
        $pdf::SetFont('Arial','B',8);
        $pdf::Cell(55,7,utf8_decode($driver['first_name'].' '.$driver['last_name']),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(20,7,utf8_decode('DESPACHADOR'),1,0,'C',true);
        $pdf::SetFont('Arial','B',8);
        $pdf::Cell(55,7,utf8_decode(Session::get('admin_name')),1,0,'C',false);
        $pdf::Ln(7);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(40,7,utf8_decode('No. CANASTILLAS ENTREGADAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,$layette_number,1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(36,7,utf8_decode('No. CANASTILLAS RECIBIDAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,'',1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('No. BOLSAS ENTREGADAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,$bag_number,1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('No. BOLSAS RECIBIDAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,'',1,0,'C',false);
        $pdf::Ln(7);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(0,4,utf8_decode('CHECK LIST ELEMENTOS ENTREGADOS MERQUEO'),1,2,'C',true);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(40,7,utf8_decode('DOTACIÓN'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($endowment),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(36,7,utf8_decode('TARJETAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($visiting_card),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('DATAFONO'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($dataphone),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('BOLSAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($bags),1,0,'C',false);
        $pdf::Ln(7);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(40,7,utf8_decode('OBSERVACIONES CHECK LIST'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(150,7,'',1,0,'C',false);
        $pdf::Ln(7);
        $pdf::Cell(95,10,utf8_decode(''),1,0,'C',false);
        $pdf::Cell(95,10,utf8_decode(''),1,2,'C',false);
        $pdf::Ln(0);
        $pdf::SetFont('Arial','B',7);
        $pdf::Cell(95,5,utf8_decode('FIRMA DEL TRANSPORTADOR'),1,0,'C',true);
        $pdf::Cell(95,5,utf8_decode('FIRMA DEL DESPACHADOR'),1,2,'C',true);
        $pdf::Ln(0);
        $pdf::Cell(0,5,utf8_decode('PEDIDOS DEVUELTOS'),1,2,'C',true);
        $pdf::Ln(0);
        $pdf::SetFont('Arial','B',7);
        $pdf::Cell(20,5,utf8_decode('PEDIDO'),1,0,'C',true);
        $pdf::Cell(170,5,utf8_decode('OBSERVACIONES'),1,2,'C',true);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(95,10,utf8_decode(''),1,0,'C',false);
        $pdf::Cell(95,10,utf8_decode(''),1,2,'C',false);
        $pdf::Ln(0);
        $pdf::SetFont('Arial','B',7);
        $pdf::Cell(95,5,utf8_decode('FIRMA CONTABILIDAD Y/O VÁUCHER'),1,0,'C',true);
        $pdf::Cell(95,5,utf8_decode('FIRMA DEL COORDINADOR SALIDA BODEGA'),1,2,'C',true);
        $pdf::Ln(3);
        // linea de separacion y corte
        $pdf::Cell(0,5,str_repeat('-', 300),0,2,'C',false);

        $pdf::Cell(0,10,$pdf::Image(url('/') . '/admin_asset/img/logo_pdf.png', $pdf::GetX() + 80, $pdf::GetY(), 30),0,2,'C',false);
        $pdf::SetDrawColor(210,210,210);
        $pdf::SetFillColor(241, 241, 241);
        $pdf::SetFont('Arial','B',8);
        $pdf::Cell(0,5,utf8_decode('ORDEN DE SALIDA CARGUE MERQUEO SAS'),1,2,'C',true);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(10,7,utf8_decode('RUTA'),1,0,'C',true);
        $pdf::Cell(60,7,utf8_decode($route['route']),1,0,'L',false);
        $pdf::Cell(18,7,utf8_decode('No. PEDIDOS'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B',12);
        $pdf::Cell(10,7,$orders['count'],1,0,'C',false);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(25,7,utf8_decode('TIPO DE VEHÍCULO'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B',12);
        $pdf::Cell(20,7,utf8_decode($vehicle['class_type']),1,0,'C',false);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(17,7,utf8_decode('FECHA'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B',7);
        $pdf::Cell(30,7,date('d/m/Y h:i:s A'),1,0,'C',false);
        $pdf::Ln(7);
        $pdf::Cell(20,7,utf8_decode('PLACA'),1,0,'C',true);
        $pdf::SetFont('Arial', 'B', 12);
        $pdf::Cell(20,7,utf8_decode($vehicle['plate']),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(20,7,utf8_decode('CONDUCTOR'),1,0,'C',true);
        $pdf::SetFont('Arial','B',8);
        $pdf::Cell(55,7,utf8_decode($driver['first_name'].' '.$driver['last_name']),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(20,7,utf8_decode('DESPACHADOR'),1,0,'C',true);
        $pdf::SetFont('Arial','B',8);
        $pdf::Cell(55,7,utf8_decode(Session::get('admin_name')),1,0,'C',false);
        $pdf::Ln(7);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(40,7,utf8_decode('No. CANASTILLAS ENTREGADAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,$layette_number,1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(36,7,utf8_decode('No. CANASTILLAS RECIBIDAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,'',1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('No. BOLSAS ENTREGADAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,$bag_number,1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('No. BOLSAS RECIBIDAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,'',1,0,'C',false);
        $pdf::Ln(7);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(0,4,utf8_decode('CHECK LIST ELEMENTOS ENTREGADOS MERQUEO'),1,2,'C',true);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(40,7,utf8_decode('DOTACIÓN'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($endowment),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(36,7,utf8_decode('TARJETAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($visiting_card),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('DATAFONO'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($dataphone),1,0,'C',false);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(35,7,utf8_decode('BOLSAS'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(11,7,utf8_decode($bags),1,0,'C',false);
        $pdf::Ln(7);
        $pdf::SetFont('Arial','B',6);
        $pdf::Cell(40,7,utf8_decode('OBSERVACIONES CHECK LIST'),1,0,'C',true);
        $pdf::SetFont('Arial','B',9);
        $pdf::Cell(150,7,'',1,0,'C',false);
        $pdf::Ln(7);
        $pdf::Cell(95,10,utf8_decode(''),1,0,'C',false);
        $pdf::Cell(95,10,utf8_decode(''),1,2,'C',false);
        $pdf::Ln(0);
        $pdf::SetFont('Arial','B',7);
        $pdf::Cell(95,5,utf8_decode('FIRMA DEL TRANSPORTADOR'),1,0,'C',true);
        $pdf::Cell(95,5,utf8_decode('FIRMA DEL DESPACHADOR'),1,2,'C',true);
        $pdf::Ln(0);
        $pdf::Cell(0,5,utf8_decode('PEDIDOS DEVUELTOS'),1,2,'C',true);
        $pdf::Ln(0);
        $pdf::SetFont('Arial','B',7);
        $pdf::Cell(20,5,utf8_decode('PEDIDO'),1,0,'C',true);
        $pdf::Cell(170,5,utf8_decode('OBSERVACIONES'),1,2,'C',true);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(20,5,'',1,0,'C',false);
        $pdf::Cell(170,5,'',1,2,'C',false);
        $pdf::Ln(0);
        $pdf::Cell(95,10,utf8_decode(''),1,0,'C',false);
        $pdf::Cell(95,10,utf8_decode(''),1,2,'C',false);
        $pdf::Ln(0);
        $pdf::SetFont('Arial','B',7);
        $pdf::Cell(95,5,utf8_decode('FIRMA CONTABILIDAD Y/O VÁUCHER'),1,0,'C',true);
        $pdf::Cell(95,5,utf8_decode('FIRMA DEL COORDINADOR SALIDA BODEGA'),1,2,'C',true);
        $pdf::Ln(3);
        $pdf::Output();
        exit;
    }

    /**
     * Método para obtener las ordenes de una ruta en específico y asignarlas a otra.
     *
     * @param  integer $route_id Id de la ruta
     */
    public function reasign_route($route_id)
    {
        if (Request::isMethod('POST')) {
            $input = Input::all();
            $type = strtoupper($input['reasign_route_type']);
            try {
                $orders_ids = $input['order'];
                if (count($orders_ids)) {
                    $route  = Routes::findOrFail($route_id);
                    $oderFirst = Order::where('route_id',$route_id)->where('status','!=','Cancelled')->first();

                    $vehicleID = $input['reasign_route_vehicle_id'];
                    if ($oderFirst->vehicle_id == $vehicleID) {
                        return Response::json([
                            'status' => 0,
                            'message' => 'El vehículo debe ser diferente.',
                        ]);
                    }

                    $vehicleSame = Routes::whereHas('orders', function ($query) use ($vehicleID) {
                        $query->where('vehicle_id', $vehicleID);
                    })->where('shift', $route->shift)->whereNotIn('status', ['Terminada','Cancelada'])
                        ->where(DB::raw('DATE(planning_date)'), '=', Carbon::now()->toDateString())
                        ->get();

                    if (count($vehicleSame)) {
                        return Response::json([
                            'status' => 0,
                            'message' => 'El vehículo ya tiene una ruta para la misma zona horaria.',
                        ]);
                    }
                    $route_copy = $route->replicate();
                    $route_copy->route = $route_copy->route . " - {$type}";
                    $route_copy->push();
                    $update = [
                        'route_id'   => $route_copy->id,
                        'driver_id'  => $input['reasign_route_driver_id'],
                        'vehicle_id' => $vehicleID,
                    ];
                    $updated = Order::whereIn('id', $orders_ids)
                        ->where('route_id', $route_id)
                        ->update($update);
                    return Response::json([
                        'status' => 1,
                        'message' => 'Se creó la ruta ' . $route_copy->route . " con id: {$route_copy->id}",
                    ]);
                }
                return Response::json([
                    'status' => 0,
                    'message' => 'No se seleccionaron ordenes para ser reasignadas.',
                ]);
            } catch (Exception $e) {
                return Response::json([
                    'status' => 0,
                    'message' => 'Ruta no encontrada.'
                ]);
            }
        } else {
            try {
                $route  = Routes::findOrFail($route_id);
                $orders = $route->orders()
                    ->orderBy('planning_sequence')
                    ->get();
                $orders_count = $orders->count();
                $orders_count_valid   = 0;
                $orders_count_invalid = 0;
                $orders->each(function($order) use (&$orders_count_invalid, &$orders_count_valid) {
                    $valid_status   = ['Alistado', 'Dispatched', 'Delivered', 'Cancelled'];
                    $invalid_status = ['Validation', 'Initiated', 'Enrutado', 'In Progress'];
                    if (in_array($order->status, $valid_status)) {
                        $orders_count_valid++;
                    }
                    if (in_array($order->status, $invalid_status)) {
                        $orders_count_invalid++;
                    }
                });
                if (!$orders->count()) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'No se encontraron ordenes asociados a esta ruta',
                    ]);
                }
                if ($orders_count_invalid) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'La ruta tiene ordenes en estado diferente a Alistado, Despachado, Entregado o Cancelado.',
                    ]);
                }
                if (($orders_count_valid + $orders_count_invalid) != $orders_count) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'La ruta tiene ordenes no gestionadas.',
                    ]);
                }
                $has_vehicle  = TRUE;
                $has_driver   = TRUE;
                $orders = $orders->filter(function($order) use (&$has_vehicle, &$has_driver) {
                    if ($order->status != 'Cancelled') {
                        if (empty($order->driver_id)) {
                            $has_driver = FALSE;
                        }
                        if (empty($order->vehicle_id)) {
                            $has_vehicle = FALSE;
                        }
                    }
                    return $order->status == 'Alistado' || $order->status == 'Dispatched';
                });
                if (!$orders->count()) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'La ruta no tiene ordenes en estado Despachado o Alistado para ser reasignada a una nueva ruta.',
                    ]);
                }
                if (!$has_driver) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'Se le debe asignar el conductor a todas las ordenes asociadas a la ruta.',
                    ]);
                }
                if (!$has_vehicle) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'Se le debe asignar un vehículo a todas las ordenes asociadas a la ruta.',
                    ]);
                }
                $orders->each(function($order) {
                    $order->load('orderGroup');
                });
                unset($route->orders);
                return Response::json([
                    'status' => 1,
                    'route'  => $route,
                    'orders' => $orders->toArray(),
                ]);
            } catch (Exception $e) {
                return Response::json([
                    'status' => 0,
                    'message' => 'Ruta no encontrada.'
                ]);
            }
        }
    }

    /**
     * Método para generar recibo de ruta
     * @return Redirect
     */
    public function download_route_voucher() {
        $input = Input::all();
        if(!empty($input['route_id']) && !empty($input['order_id'])) {
            $orders = Order::whereIn('status', ['Alistado', 'Dispatched'])->where('id', $input['order_id'])->where('route_id', $input['route_id'])->orderBy('planning_sequence')->get();
        } else if(!empty($input['order_id'])) {
            $orders = Order::whereIn('status', ['Alistado', 'Dispatched'])->whereRaw("DATE(planning_date) = '" . Carbon::now()->format('Y-m-d') . "'")->where('id', $input['order_id'])->get();
        } else if(!empty($input['route_id'])){
            $orders = Order::whereIn('status', ['Alistado', 'Dispatched'])->where('route_id', $input['route_id'])->orderBy('planning_sequence')->get();
        }else{
            return Redirect::back()->with('error', 'Debes seleccionar una ruta o un pedido para generar el recibo.')->withInput();
        }

        if(!$orders->count()) {
            return Redirect::back()->with('error', 'No existen pedidos con los parametros seleccionados para generar el recibo.')->withInput();
        }

        $pdf = new \PDF_Code128('P', 'mm');
        //Header voucher
        foreach ($orders as $order) {
            $order_products = $order->orderProducts()->where('fulfilment_status', 'Fullfilled')->get();
            $quantity_order_product = (int)$order->orderProducts()->where('fulfilment_status', 'Fullfilled')->where('type', '<>', 'Agrupado')->sum('quantity');
            $quantity_order_product_group = (int)$order->orderProductGroup()->where('fulfilment_status', 'Fullfilled')->sum('quantity');
            $purchase_products = $quantity_order_product + $quantity_order_product_group;
            $height = 300 + ($order_products->count() * 17);
            $pdf->AddPage('P', array(100, $height), 0);
            $pdf->SetMargins(5, 10, 5);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 0, '', 0, 1, 'C');
            $pdf->Cell(0, 4, utf8_decode('MERQUEO SAS'), 0, 1, 'C');
            $pdf->Cell(0, 4, utf8_decode('NIT : 900871444-8'), 0, 1, 'C');
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(0, 4, utf8_decode('CALLE 97a # 9a - 50'), 0, 1, 'C');
            //Lineas de separación
            $pdf->Ln(2);
            $pdf->SetDrawColor(63, 114, 196);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(1);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(2);
            //End lineas de separación
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(0, 4, utf8_decode('Pedido N°: ' . $order->reference), 0, 1, 'L');
            $pdf->Cell(0, 4, utf8_decode('Fecha de entrega: ' . date('d/m/Y', strtotime($order->delivery_date))), 0, 1, 'L');
            $pdf->Cell(0, 4, utf8_decode('Ruta: ' . substr($order->route->route, 11)), 0, 1, 'L');
            $pdf->Cell(0, 4, utf8_decode('Secuencia: ' . $order->planning_sequence), 0, 1, 'L');
            //Lineas de separación
            $pdf->Ln(2);
            $pdf->SetDrawColor(63, 114, 196);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(1);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(2);
            //End lineas de separación
            //End Header voucher
            //Body voucher
            //head table products
            $pdf->Ln(2);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(30, 4, utf8_decode('Producto'), 0, 0, 'C');
            $pdf->Cell(20, 4, utf8_decode('Precio Un.'), 0, 0, 'C');
            $pdf->Cell(20, 4, utf8_decode('Unidades'), 0, 0, 'C');
            $pdf->Cell(20, 4, utf8_decode('Total'), 0, 1, 'C');
            $pdf->Ln(4);
            //End head table products
            //body table products
            $pdf->SetFont('Arial', '', 10);
            foreach ($order_products as $product) {
                if($product->type == 'Agrupado') {
                    $order_product_group = $product->orderProductGroup()->where('fulfilment_status', 'Fullfilled')->get();
                    foreach ($order_product_group as $product_group) {
                        $x = $pdf->GetX();
                        $startY = $pdf->GetY();
                        $pdf->SetXY($x, $startY);
                        $pdf->MultiCell(30, 4, utf8_decode($product_group->reference . ' - ' . $product_group->product_name . ' - ' . $product_group->product_quantity. ' ' . $product_group->product_unit), 0, 'L');
                        $endY =$pdf->GetY();
                        $heightCell = ($endY - $startY);
                        $pdf->SetXY($x + 30, $startY);
                        $pdf->Cell(20, $heightCell, utf8_decode('$ ' . number_format($product_group->price, 0, ',', '.')), 0, 0, 'C');
                        $pdf->Cell(20, $heightCell, utf8_decode($product_group->quantity), 0, 0, 'C');
                        $pdf->Cell(20, $heightCell, utf8_decode('$ ' . number_format(($product_group->quantity * $product_group->price), 0, ',', '.')), 0, 1, 'R');
                        $pdf->Ln(4);
                    }
                } else {
                    $x = $pdf->GetX();
                    $startY = $pdf->GetY();
                    $pdf->SetXY($x, $startY);
                    $pdf->MultiCell(30, 4, utf8_decode($product->reference . ' - ' . $product->product_name . ' - ' . $product->product_quantity. ' ' . $product->product_unit), 0, 'L');
                    $endY =$pdf->GetY();
                    $heightCell = ($endY - $startY);
                    $pdf->SetXY($x + 30, $startY);
                    $pdf->Cell(20, $heightCell, utf8_decode('$ ' . number_format($product->price, 0, ',', '.')), 0, 0, 'C');
                    $pdf->Cell(20, $heightCell, utf8_decode($product->quantity), 0, 0, 'C');
                    $pdf->Cell(20, $heightCell, utf8_decode('$ ' . number_format(($product->quantity * $product->price), 0, ',', '.')), 0, 1, 'R');
                    $pdf->Ln(4);
                }
            }
            //End body table products
            //Footer table products
            $pdf->Ln(2);
            $pdf->Cell(70, 4, utf8_decode('SUBTOTAL: '), 0, 0, 'L');
            $pdf->Cell(20, 4, utf8_decode('$ ' . number_format($order->total_amount, 0, ',', '.')), 0, 1, 'R');
            $pdf->Cell(70, 4, utf8_decode('COSTO DOMICILIO: '), 0, 0, 'L');
            $pdf->Cell(20, 4, utf8_decode('$ ' . number_format($order->delivery_amount, 0, ',', '.')), 0, 1, 'R');
            $pdf->Cell(70, 4, utf8_decode('DESCUENTO: '), 0, 0, 'L');
            $pdf->Cell(20, 4, utf8_decode('$ ' . number_format($order->discount_amount, 0, ',', '.')), 0, 1, 'R');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(70, 4, utf8_decode('TOTAL COMPRA: '), 0, 0, 'L');
            $pdf->Cell(20, 4, utf8_decode('$ ' . number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount), 0, ',', '.')), 0, 1, 'R');
            //Lineas de separación
            $pdf->Ln(4);
            $pdf->SetDrawColor(63, 114, 196);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(1);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(4);
            //End lineas de separación
            $pdf->Cell(70, 5, utf8_decode('TOTAL PRODUCTOS COMPRADOS: '), 0, 0,'L');
            $pdf->Cell(20, 5, utf8_decode($purchase_products), 0, 1,'R');
            $pdf->Cell(70, 5, utf8_decode('TOTAL CONTENEDORES DESPACHADOS: '), 0, 0,'L');
            $pdf->Cell(20, 5, utf8_decode($order->picking_baskets), 0, 1,'R');
            $pdf->Cell(50, 5, utf8_decode('CONSECUTIVO PRECINTOS: '), 0, 0,'L');
            $seals = $order->seals;
            $seals = explode(',', $seals);
            foreach ($seals as $seal) {
                $x = $pdf->GetX();
                $ln = (($x + 20) >= 80) ? 1 : 0;
                $pdf->Cell(20, 4, utf8_decode($seal), 0, $ln, 'R');
            }
            //End footer products
            //Lineas de separación
            $pdf->Ln(4);
            $pdf->SetDrawColor(63, 114, 196);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(1);
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Cell(0, 0, '', 1, 1, 'C');
            $pdf->Ln(4);
            //End lineas de separación
            $pdf->Cell(90, 10, $pdf->Code128($pdf->GetX() + 5, $pdf->GetY(), $order->reference, 80,10), 0 , 1, 'C');
            $pdf->Cell(90, 4,  utf8_decode($order->reference), 0 , 1, 'C');
            $pdf->Ln(4);
            //Lineas de corte
            $pdf->Ln(4);
            $pdf->SetTextColor(63, 114, 196);
            $pdf->Cell(90, 1, '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ', 0, 1, 'C');
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Ln(4);
            //End lineas de corte
            $pdf->Ln(4);

        }
        //Fin body voucher
        $pdf->Output();
        exit;
    }

}
