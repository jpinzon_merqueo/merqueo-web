<?php
namespace admin;

use Aws\Common\Facade\Ses;
use JsonSchema\Constraints\Object;
use phpDocumentor\Reflection\Types\Array_;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Request, Input, View, Session, Redirect, Response, Validator, DB, Driver, Vehicle,
    Transporter, Order, Store, City, PHPExcel, PHPExcel_Style_Fill,
    PHPExcel_Writer_Excel2007, Hash, Routes, Config, SecuritySocial, VehicleDrivers;

class AdminTransporterController extends AdminController
{
	/**
     * Grilla de transportadores
     */
    public function index()
    {
        $city = City::find(Session::get('admin_city_id'));

        if (!Request::ajax()) {
            return View::make('admin.transporters.index')
                ->with('title', 'Transportador')
                ->with('cities', $this->get_cities());
        }else{
            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $transporters = Transporter::join('cities', 'cities.id', '=', 'transporters.city_id')
                ->where('transporters.city_id', $city_id == null ? $city->id : $city_id)
                ->select('transporters.*');

            if ($search != '') {
                $transporters = $transporters->where('transporters.fullname', 'LIKE', '%'.$search.'%');
            }
            $transporters = $transporters->orderBy('transporters.fullname','DESC')->limit(200)->get();

             return View::make('admin.transporters.index')
                    ->with('title', 'Transportadores')
                    ->with('cities', $this->get_cities())
                    ->with('transporters', $transporters)
                    ->renderSections()['content'];
        }
    }

    /**
     * Editar transportador
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Transportador' : 'Nuevo Transportador';

        $transport = (object) Session::get('post');
        $transport->type = $transport->fullname = $transport->plate = $transport->document_number = $transport->document_type = $transport->phone = $transport->cellphone = $transport->address = $transport->address_supplement = $transport->city_id = $transport->email = $transport->id = '';

        $seguritySocial =  SecuritySocial::all();
        if (!$id){
            $drivers =  Driver::all();
            $vehicle =  Vehicle::all();

        }else {
            $transport = Transporter::find($id);
            $drivers = Driver::find($id) ? Driver::find($id) : '';
            $vehicle = Vehicle::find($id) ? Vehicle::find($id) : '';
        }

        return View::make('admin.transporters.transporter_form')
                ->with('title', 'Transportadores')
                ->with('sub_title', $sub_title)
                ->with('cities', $this->get_cities())
                ->with('transport', $transport)
                ->with('drivers', $drivers)
                ->with('seguritySocials', $seguritySocial)
                ->with('vehicle', $vehicle);
    }

    /**
     * Editar transportador
     */
    public function save()
    {
        $id = Input::get('id');
        $transport = Transporter::find($id);
        if (!$transport){
            $transport = new Transporter;
            $transport->created_at = date("Y-m-d H:i:s");
            if (Transporter::where('document_number', Input::get('document_number'))->first()) {
                return Redirect::back()->with('error','Ya existe un transportador con el mismo número de documento.');
            }
            $action = 'insert';
            if (!Input::file('file_rut_url')){
                return Redirect::back()->with('error', 'Error archivo Obligatorio');

            }
        }else{
            $action = 'update';
        }



        $transport->document_number = Input::get('document_number');
        $transport->document_type = Input::get('document_type');
        $transport->fullname = Input::get('fullname');
        $transport->phone = Input::get('phone');
        $transport->cellphone = Input::get('cellphone');
        $transport->address = Input::get('address');
        $transport->address_supplement = Input::get('address_supplement');
        $transport->city_id = Input::get('city');
        $transport->email = Input::get('email');
        $transport->type = Input::get('type');
        $transport->eps_id = Input::get('eps');
        $transport->afp_id = Input::get('afp');
        $transport->arl_id = Input::get('arl');
        $file = Input::file('file_rut_url');
        if($file) {
            if ( $file->getClientOriginalExtension()!= 'pdf') {
                return Redirect::back()->with('error', 'Error archivo no es Formato PDF');
            }
            $dir = 'transporter/rut/' . date('Y-m-d');
            $file_data['real_path'] = $file->getRealPath();
            $file_data['client_original_name'] = $file->getClientOriginalName();
            $file_data['client_original_extension'] = $file->getClientOriginalExtension();
            $transport->file_rut_url =  upload_image($file_data, false, $dir);
        }
        $transport->save();

        $this->admin_log('transporters', $transport->id, $action);

        return Redirect::route('adminTransporter.index')->with('type', 'success')->with('message', 'Transportador guardado con éxito.');
    }

    /**
     * Eliminar Transportador
     */
    public function delete($id)
    {
        $transporter = Transporter::find($id);
        Transporter::where('id', $id)->update(array('status' => !$transporter->status));

        $this->admin_log('transporters', $transporter->id, 'delete');

        return Redirect::back()->with('type', 'success')->with('message', 'Transportador Editado con éxito.');
    }

    /**
     * Grilla de conductores
     */
    public function drivers()
    {
        $city = City::find(Session::get('admin_city_id'));
        $transporters = Transporter::where('city_id', $city->id)->where('status', 1)->select('id', 'fullname')->get();

        if (!Request::ajax()){
            return View::make('admin.transporters.drivers')
                ->with('title', 'Conductores')
                ->with('transporters', $transporters)
                ->with('cities', $this->get_cities());
        }else{
            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $transporter_id = Input::has('transporter_id') ? Input::get('transporter_id') : 0;

            $drivers = Driver::join('transporters', 'transporters.id', '=', 'drivers.transporter_id')
                ->where('transporters.city_id', $city_id == null ? $city->id : $city_id)
                ->join('cities', 'cities.id', '=', 'transporters.city_id')
                ->select('drivers.*', 'cities.city AS city_name');

            if ($search != ''){
                $drivers = $drivers->where('drivers.first_name', 'LIKE', '%'.$search.'%');
            }
            if ($transporter_id != 0)
                $drivers = $drivers->where('drivers.transporter_id', $transporter_id);

            $drivers = $drivers->orderBy('drivers.first_name','DESC')->limit(200)->get();

            $transporters = Transporter::where('city_id', $city_id == null ? $city->id : $city_id)->where('status', 1)->select('id', 'fullname')->get();

            return View::make('admin.transporters.drivers')
                    ->with('title', 'Conductores')
                    ->with('drivers', $drivers)
                    ->with('transporters', $transporters)
                    ->with('cities', $this->get_cities())
                    ->renderSections()['content'];
        }
    }

    /**
     * Editar conductor
     */
    public function edit_driver()
    {
        $id = Request::segment(5) ? Request::segment(5) : 0;
        $sub_title = Request::segment(5) ? 'Editar Conductor' : 'Nuevo Conductor';

        $drivers = (object) Session::get('post');

        $seguritySocial =  SecuritySocial::all();
        $drivers->number_license = $drivers->document_type = $drivers->profile = $drivers->category_license = $drivers->first_name = $drivers->last_name = $drivers->document_number = $drivers->phone = $drivers->cellphone = $drivers->due_date = $drivers->transporter_id = $drivers->id = $drivers->has_cap = $drivers->has_jacket = $drivers->has_tshirt = '';
        if (!$id){
            $transports =  Transporter::where('status',true)->get();
            $vehicle =  Vehicle::all();

        }else {
            $drivers = Driver::find($id);
            if(!$drivers->profile){
                $drivers->profile = 0;
            }

            $transports =  Transporter::where('status',true)->get();
            $vehicle =  Vehicle::all();
        }

        return View::make('admin.transporters.driver_form')
                ->with('title', 'Conductores')
                ->with('sub_title', $sub_title)
                ->with('transporters', $transports)
                ->with('drivers', $drivers)
                ->with('seguritySocials', $seguritySocial)
                ->with('vehicle', $vehicle);
    }

    /**
     * Guardar condutor
     */
    public function save_driver()
    {
        $id = Input::get('id');
        $drivers = Driver::find($id);
        if (!$drivers){
            $drivers = new Driver;
            $drivers->created_at = date("Y-m-d H:i:s");
            if (Driver::where('document_number', Input::get('document_number'))->first())
                return Redirect::back()->with('error', 'Ya existe un conductor con el mismo número de documento.');
            $action = 'insert';
        }else{
            $action = 'update';
        }
        $drivers->profile = 'Conductor';
        $drivers->category_license = Input::get('category_license');
        $drivers->transporter_id = Input::get('transporter_id');
        $drivers->first_name = Input::get('first_name');
        $drivers->last_name = Input::get('last_name');
        $drivers->document_number = Input::get('document_number');
        $drivers->phone = Input::get('phone');
        $drivers->cellphone = Input::get('cellphone');
        $drivers->category_license = Input::get('category_license');
        $drivers->number_license = Input::get('number_license');
        $drivers->has_cap = Input::get('has_cap');
        $drivers->has_jacket = Input::get('has_jacket');
        $drivers->has_tshirt = Input::get('has_tshirt');
        $drivers->due_date = format_date('mysql', Input::get('due_date'));
        $drivers->eps_id = Input::get('eps_id');
        $drivers->afp_id = Input::get('afp_id');
        $drivers->arl_id = Input::get('arl_id');

        $file = Input::file('photo_url');
        if($file) {
            $dir = 'drivers/photos/' . date('Y-m-d');
            $file_data['real_path'] = $file->getRealPath();
            $file_data['client_original_name'] = $file->getClientOriginalName();
            $file_data['client_original_extension'] = $file->getClientOriginalExtension();
            $drivers->photo_url =  upload_image($file_data, false, $dir);
        }
        $drivers->save();

        $this->admin_log('drivers', $drivers->id, $action);

        return Redirect::route('adminTransporter.drivers')->with('type', 'success')->with('message', 'Conductor guardado con éxito.');
    }

    /**
     * Eliminar conductor
     */
    public function delete_driver($id)
    {
        $driver = Driver::find($id);
        Driver::where('id', $id)->update(array('status' => !$driver->status));
        $this->admin_log('drivers', $driver->id, 'delete');

        return Redirect::back()->with('type', 'success')->with('message', 'Conductor Editado con éxito.');
    }


    /**
     * Grilla de vehiculos
     */
    public function vehicles()
    {
        $city = City::find(Session::get('admin_city_id'));
        $transporters = Transporter::where('city_id', $city->id)->where('status', 1)->select('id', 'fullname')->get();

        if (!Request::ajax()) {
            return View::make('admin.transporters.vehicles')
                ->with('title', 'Vehículos')
                ->with('transporters', $transporters)
                ->with('cities', $this->get_cities());
        } else {
            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $transporter_id = Input::has('transporter_id') ? Input::get('transporter_id') : 0;
            $vehicles = Vehicle::with('transporter.city');
            if ($transporter_id != 0) {
                $vehicles = $vehicles->where('transporter_id',$transporter_id);
            } else {
                $city_id = $city_id == null ? $city->id : $city_id;
                $vehicles = $vehicles->whereHas('transporter', function ($q) use ($city_id) {
                    $q->where('city_id', $city_id);
                });
            }
            if ($search != '')
                $vehicles = $vehicles->where('plate', 'LIKE', '%' . $search . '%');

            $vehicles = $vehicles->orderBy('plate', 'DESC')->limit(200)->get();
            $vehicleStruct = [];
            foreach ($vehicles as $vehicle) {
                $name_city = '';
                if(isset($vehicle->transporter)){
                        $name_city = $vehicle->transporter->city->city;
                }
                $data = $vehicle;
                $data->fullname = (isset($vehicle->transporter)) ? $data->transporter->fullname : '';
                $data->city_name = $name_city;

                $vehicleStruct[] = $data;
            }
            $transporters = Transporter::where('city_id', $city_id == null ? $city->id : $city_id)->where('status', 1)->select('id', 'fullname')->get();
            return View::make('admin.transporters.vehicles')
                ->with('title', 'Vehículos')
                ->with('vehicles', $vehicleStruct)
                ->with('transporters', $transporters)
                ->with('cities', $this->get_cities())
                ->renderSections()['content'];
        }
    }

    /**
     * Editar vehiculo
     */
    public function edit_vehicle()
    {
        if (!Request::ajax()) {

            $id = Request::segment(5) ? Request::segment(5) : 0;
            $sub_title = Request::segment(5) ? 'Editar Vehículo' : 'Nuevo Vehículo';
            if (!$id) {
                $vehicles = (object) Session::get('post');
                $vehicles->class_type = $vehicles->plate =  $vehicles->model = $vehicles->id =
                $vehicles->enrollment_date = $vehicles->capacity = $vehicles->total_volume  =
                $vehicles->body_inspection = $vehicles->transporter_id = $vehicles->allow_early_delivery =
                $vehicles->validate_sequence = $vehicles->branding_date = $vehicles->capacity_containers =
                $vehicles->due_date_soat =  $vehicles->due_date_rtm = '';
                $vehicles->status = 1;
                $vehicles->old_version = 0;
                $vehicles->drivers_id = '';
                $vehicles->full_route_cost = $vehicles->payment_type = $vehicles->transporter_id = $vehicles->base_cost = $vehicles->variable_cost = $vehicles->base_order = $vehicles->half_route_cost = $vehicles->partial_route_cost = $vehicles->has_branding = $vehicles->has_branding_check_design = 0;
                if (Session::get('admin_designation') == 'Super Admin')
                    $transports =  Transporter::orderBy('fullname', 'ASC')->where('status',true)->get();
                else
                    $transports =  Transporter::where('city_id', Session::get('admin_city_id'))->where('status',true)->orderBy('fullname', 'ASC')->get();
                $drivers =  Array();

            }else {
                if (Session::get('admin_designation') == 'Super Admin')
                    $transports = Transporter::orderBy('fullname', 'ASC')->where('status',true)->get();
                else
                    $transports = Transporter::where('city_id', Session::get('admin_city_id'))->where('status',true)->orderBy('fullname', 'ASC')->get();
                $drivers = Driver::all();
                $vehicles = Vehicle::where('vehicles.id', $id)->select('vehicles.*')->first();
                $driversids = VehicleDrivers::where('vehicle_id',$id)->get();
                $drivers_ids = [];
                foreach ($driversids as $drv ){
                    $drivers_ids[]=$drv->driver_id;
                }
                $vehicles->drivers_ids = $drivers_ids;
            }

            if(Input::has('password')){
                $password = Input::get('password');
                if (!empty($password))
                    $vehicles->password = Hash::make(Input::get('password'));
            }

            return View::make('admin.transporters.vehicle_form')
                    ->with('title', 'Vehículos')
                    ->with('sub_title', $sub_title)
                    ->with('drivers', $drivers)
                    ->with('transporters', $transports)
                    ->with('vehicles', $vehicles);

        }else{
            $id = Request::segment(5) ? Request::segment(5) : 0;
            $sub_title = Request::segment(5) ? 'Editar Vehículo' : 'Nuevo Vehículo';
            $vehicles = (object) Session::get('post');
            $transportid = Input::has('trasnport_id') ? Input::get('trasnport_id') : '';
            $vehicles->class_type = $vehicles->transporter_id = $vehicles->capacity_containers = $vehicles->plate = $vehicles->model  = $vehicles->id = $vehicles->enrollment_date =  $vehicles->capacity = $vehicles->total_volume = $vehicles->body_inspection =  $vehicles->validate_sequence = $vehicles->branding_date = $vehicles->due_date_soat = $vehicles->due_date_rtm = '';
            $vehicles->has_branding =  $vehicles->base_cost = $vehicles->variable_cost = $vehicles->base_order = $vehicles->has_branding_check_design = 0;
                $vehicles->status = 1;
            if (!$id){
                $transports =  Transporter::find($transportid) ? Transporter::find($transportid) : Transporter::where('status', 1)->orderBy('fullname')->get();
                $drivers =  Driver::where('status', 1)->orderBy('first_name')->orderBy('last_name')->get();
            }else {
                $transports =  Transporter::find($transportid) ? Transporter::find($transportid) : Transporter::where('status', 1)->orderBy('fullname')->get();
            }

            if(Input::has('password')){
                $password = Input::get('password');
                if (!empty($password))
                    $vehicles->password = Hash::make(Input::get('password'));
            }

            return View::make('admin.transporters.vehicle_form')
                    ->with('title', 'Vehículos')
                    ->with('sub_title', $sub_title)
                    ->with('drivers', $drivers)
                    ->with('transporters', $transports)
                    ->with('vehicles', $vehicles)
                    ->renderSections()['content'];
        }
    }

    /**
     * Guardar vehiculo
     */
    public function save_vehicle()
    {
        $id = Input::get('id');
        $vehicle = Vehicle::find($id);
        if (!$vehicle){
            $vehicle = new Vehicle;
            $vehicle->created_at = date("Y-m-d H:i:s");
            if (Vehicle::where('plate', Input::get('plate'))->first())
                return Redirect::back()->with('error', 'Ya existe un vehículo con la misma placa.')->with('post', Input::all());
            $action = 'insert';
        }else{
            $action = 'update';
        }

        if(Input::has('password')){
            $password = Input::get('password');
            if (!empty($password)) {
                $vehicle->password = Hash::make(Input::get('password'));
            }
        }

        $vehicle->old_version = Input::has('old_version') ? Input::get('old_version'):$vehicle->old_version;

        $vehicle->capacity_containers = Input::get('capacity_containers');
        $vehicle->transporter_id = Input::get('transporter_id');
        $vehicle->payment_type = Input::get('payment_type');
        $vehicle->class_type = Input::get('class_type');
        $vehicle->plate = Input::get('plate');
        $vehicle->model = Input::get('model');
        $vehicle->enrollment_date = format_date('mysql', Input::get('enrollment_date'));
        $vehicle->due_date_soat = format_date('mysql', Input::get('due_date_soat'));
        $vehicle->due_date_rtm = format_date('mysql', Input::get('due_date_rtm'));
        $vehicle->capacity = Input::get('capacity');
        $vehicle->total_volume = Input::get('total_volume');
        $vehicle->body_inspection = Input::get('body_inspection');
        $vehicle->updated_at = date("Y-m-d H:i:s");
        $vehicle->status = Input::get('status');
        $vehicle->has_branding = Input::get('has_branding');
        $vehicle->branding_date = format_date('mysql', Input::get('branding_date'));
        $vehicle->has_branding_check_design = Input::get('has_branding_check_design');
        $vehicle->allow_early_delivery = Input::get('allow_early_delivery');
        $vehicle->validate_sequence = Input::get('validate_sequence');
        $vehicle->full_route_cost = (empty(Input::get('full_route_cost'))) ? NULL : Input::get('full_route_cost');
        $vehicle->half_route_cost = (empty(Input::get('half_route_cost'))) ? NULL : Input::get('half_route_cost');
        $vehicle->partial_route_cost = (empty(Input::get('partial_route_cost'))) ? NULL : Input::get('partial_route_cost');
        $vehicle->base_cost = (empty(Input::get('base_cost'))) ? NULL : Input::get('base_cost');
        $vehicle->base_order = (empty(Input::get('base_order'))) ? NULL : Input::get('base_order');
        $vehicle->variable_cost = (empty(Input::get('variable_cost'))) ? NULL : Input::get('variable_cost');
        $vehicle->save();

        $this->admin_log('vehicles', $vehicle->id, $action);

        $drivers = Input::get('drivers_id');
        for ($i = 0; $i < count($drivers); $i++) {
            $vehicleDriver = VehicleDrivers::where('driver_id', $drivers[$i])
                ->where('vehicle_id', $vehicle->id)->get();
            if (count($vehicleDriver)==0) {
                $veDri = new VehicleDrivers;
                $veDri->driver_id = $drivers[$i];
                $veDri->vehicle_id = $vehicle->id;
                $veDri->save();
            }
        }
        VehicleDrivers::where('vehicle_id',$vehicle->id)->whereNotIn('driver_id',$drivers)->delete();


        return Redirect::route('adminTransporter.vehicles')->with('type', 'success')->with('message', 'Vehículo guardado con éxito.');
    }

    /**
     * Eliminar vehiculo
     */
    public function delete_vehicle($id)
    {
        $vehicles = Vehicle::find($id);
        Vehicle::where('id', $id)->update(array('status' => !$vehicles->status));
        $this->admin_log('vehicles', $vehicles->id, 'delete');

        return Redirect::back()->with('type', 'success')->with('message', 'Vehículo editado con éxito.');
    }

    /**
     * Ajax transportador
     */
    public function ajax_transporter()
    {
        $arraycont = Array();
        $arrayaux = Array();
        $idtransport = Input::get('trasnport_id');
        $drivers = Driver::where('drivers.transporter_id', $idtransport)->where('drivers.status', 1)
            ->orderBy('drivers.last_name')->orderBy('drivers.first_name')->select('drivers.*')->get();
        foreach ($drivers as $value) {
            if($value->profile == 'Conductor'){
                $arraytemp = array();
                $arraytemp['id'] = $value->id;
                $arraytemp['name'] = $value->first_name.' '.$value->last_name;
                array_push($arraycont, $arraytemp);
            }
            if($value->profile == 'Auxiliar'){
                $arraytemp = array();
                $arraytemp['id'] = $value->id;
                $arraytemp['name'] = $value->first_name.' '.$value->last_name;
                array_push($arrayaux, $arraytemp);
            }
        }

        return Response::json(array('cond' => $arraycont, 'aux' => $arrayaux));
    }

    /**
     * Ajax para filtrar transportadores por ciudad
     *
     */
    public function ajax_transporters_per_city()
    {
        $city_id = Input::get('city_id');
        $transporters = Array();
        $message = 'Error al consultar los transportadores';
        $status = 400;
        if (Input::has('city_id')) {
            $transporters = Transporter::where('city_id', $city_id)->where('status', 1)->get();
            $message = 'Transportadores por ciudad';
            $status = 200;
        }
        return Response::json(array('result' => array('transporters' => $transporters), 'message' => $message, 'status' => $status), 200);
    }

    /**
     * Ajax para filtrar conductores por transportadora
     *
     */
    public function ajax_drivers_per_transporter()
    {
        $transporter_id = Input::get('transporter_id');
        $drivers = Array();
        $message = 'Error al consultar los conductores';
        $status = 400;
        if (Input::has('transporter_id')) {
            $drivers = Driver::where('transporter_id', $transporter_id)->where('profile','Conductor')
                ->where('status', 1)->get();
            $message = 'Conductores por Transportadores';
            $status = 200;
        }
        return Response::json(array('result' => array('drivers' => $drivers), 'message' => $message, 'status' => $status), 200);
    }

    /**
     * Ajax para filtrar vehiculos sin ruta de hoy por transportadora
     *
     */
    public function ajax_vehicles_per_transporter()
    {
        $transporter_id = Input::get('transporter_id');
        $containers = Input::get('containers');
        $status_containers = Input::get('status_containers');
        $orders_number = Input::get('orders_number');

        if($status_containers == 'false')
            $containers = 0;

        $vehicles = Array();
        $message = 'Error al consultar los vehiculos';
        $status = 400;
        if (Input::has('transporter_id')) {

            $vehiclesRoutes = Routes::where(DB::raw('DATE(dispatch_time)'),Carbon::today()->toDateString())
                ->where('suggested_vehicle_id','!=','')
                ->select('suggested_vehicle_id')->get();
            $vehicles_id =[];

            foreach ($vehiclesRoutes as $data){
                $vehicles_id[] = $data->suggested_vehicle_id;
            }

            $vehicles = Vehicle::where('transporter_id', $transporter_id)
                ->where('capacity_containers', '>=', $containers)
                ->whereNotIn('id', $vehicles_id)
                ->where('status', 1)
                ->get();

            foreach ($vehicles  as $vehicle) {
                $vehicle->value = $vehicle->getCost($orders_number)+0;
                $vehicle->cost = currency_format($vehicle->value);
            }
            $vehicles = $vehicles->sort(
                function ($a, $b) {
                    return  ($a->value - $b->value)
                            ?: ($a->capacity_containers - $b->capacity_containers)
                            ?: strcmp($a->class_type, $b->class_type);
                }
            );
            $vehicles = $vehicles->values()->all();

            $message = 'Vehiculos por Transportadores';
            $status = 200;
        }
        return Response::json(array('result' => array('vehicles' => $vehicles), 'message' => $message, 'status' => $status), 200);
    }

    /**
     * Método que genera el reporte de costos para un vehiculo en particular
     * mediante query string se obtiene el rango de fechas para generarlo.
     *
     * @param int $id Identificador único del vehículo.
     */
    public function cost_report($id) {
        try {
            $vehicle = Vehicle::findOrFail($id);
            $from = Carbon::now();
            $to   = Carbon::now();
            if (Request::get('from', 0) && !Request::get('to', 0)) {
                $from = Carbon::createFromFormat('d-m-Y', Request::get('from'));
                $to   = Carbon::now();
            } else if (!Request::get('from', 0) && Request::get('to', 0)) {
                $from = Carbon::now();
                $to   = Carbon::createFromFormat('d-m-Y', Request::get('to'));
            } else if (Request::get('from', 0) && Request::get('to', 0)) {
                $from = Carbon::createFromFormat('d-m-Y', Request::get('from'));
                $to   = Carbon::createFromFormat('d-m-Y', Request::get('to'));
            }
            //Obtenemos las rutas para el vehiculo.
            $routes = Routes::select('routes.*')
                ->join('orders', 'orders.route_id', '=', 'routes.id')
                ->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id')
                ->where('routes.planning_date', '>=', $from->toDateString())
                ->where('routes.planning_date', '<=', $to->toDateString())
                ->where('routes.status', '=', 'Terminada')
                ->where('routes.status_cost', '=', 'Calculado')
                ->where('orders.vehicle_id', '=', $vehicle->id)
                ->groupBy('routes.id')
                ->orderBy('routes.route')
                ->get();
            if ($routes->count()) {
                $url = $this->generate_report($routes);
                return Redirect::route('adminTransporter.vehicles')
                    ->with('file_url', $url)
                    ->with('type', 'success')
                    ->with('message', 'Report generated successfully.');
            }
            return Redirect::route('adminTransporter.vehicles')
                ->with('type', 'error')
                ->with('message', 'No se encontraron rutas asociadas a ésta placa para el rango de fechas seleccionadas.');
        } catch (ModelNotFoundException $e) {
            return Redirect::route('adminTransporter.vehicles')
                ->with('type', 'error')
                ->with('message', 'No se encontraron rutas asociadas a ésta placa para el rango de fechas seleccionadas.');
        }
    }

    /**
     * Método para generar el reporte de costos, mediante queryString se obtiene el rango de fechas, la ciudad y el transportador.
     *
     */
    public function report_extend() {
        $city        = Request::get('city_report', Session::get('admin_city_id'));
        $created_at  = Request::get('created_at', FALSE);
        $transporter = Request::get('transporter_report', FALSE);
        $range = explode(',', $created_at);
        $from = Carbon::createFromFormat('d-m-Y', $range[0]);
        $to = Carbon::createFromFormat('d-m-Y', $range[1]);
        $routes = Routes::select('routes.*')
            ->join('orders', 'orders.route_id', '=', 'routes.id')
            ->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id')
            ->join('drivers', 'drivers.id', '=', 'orders.driver_id')
            ->join('transporters', 'transporters.id', '=', 'vehicles.transporter_id')
            ->where('routes.status', '=', 'Terminada')
            ->where('routes.status_cost', '=', 'Calculado')
            ->whereDate('routes.planning_date', '>=', $from->toDateString())
            ->whereDate('routes.planning_date', '<=', $to->toDateString())
            ->where('transporters.city_id', '=', $city)
            ->where('transporters.id', '=', $transporter)
            ->groupBy('routes.id')
            ->orderBy('routes.id')
            ->get();
        if ($routes->count()) {
            $url = $this->generate_report($routes);
            return Redirect::route('adminTransporter.vehicles')
                ->with('file_url', $url)
                ->with('type', 'success')
                ->with('message', 'Report generated successfully.');
        }
        return Redirect::action('adminTransporter.vehicles')
            ->with('type', 'error')
            ->with('message', 'No se encontraron costos aplicando los filtros seleccionados.');
    }

    /**
     * Genera el archivo de reporte.
     *
     * @param  Collection $routes Colección de registros para exportar.
     * @return JSON Objeto json
     */
    protected function generate_report($routes) {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        DB::statement("SET lc_time_names = 'es_ES'");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setTitle('Reporte de costos por vehículo');
        $objPHPExcel->getProperties()->setSubject('Reporte de costos por vehículo');
        $objPHPExcel->setActiveSheetIndex(0);
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'5178A5'),
            ),
            'font' => array(
                'color' => array('rgb'=>'FFFFFF'),
                'bold' => true,
            )
        );
        $headers = [
            'RUTA', 'PLACA', 'TIPO DE VEHÍCULO', 'MODELO', 'TOTAL PEDIDOS',
            'PEDIDOS ENTREGADOS', 'PEDIDOS CANCELADOS','COSTO DE LA RUTA',
            'APOYO A LA RUTA', 'VALOR', 'COMENTARIOS', 'RECARGO DISTANCIA EXTRA',
            'VALOR', 'COMENTARIOS', 'RECARGO EXTRA', 'VALOR', 'COMENTARIOS',
            'PEAJE', 'VALOR', 'COMENTARIOS', 'RECARGO MÁS DE 35 PEDIDOS', 'VALOR',
            'COMENTARIOS', 'MULTAS', 'VALOR', 'COMENTARIOS', 'CANGURO', 'VALOR',
            'COMENTARIOS', 'PRODUCTOS Y/O CANASTILLAS NO DEVUELTAS', 'VALOR',
            'COMENTARIOS', 'OTRO', 'VALOR', 'COMENTARIOS', 'TOTAL RUTA'
        ];
        $objPHPExcel->getActiveSheet()->getStyle('A1:AJ1')->applyFromArray($style_header);
        $i = 0;
        $j = 2;
        $yer_or_not = [
            0 => 'No',
            1 => 'Si',
        ];
        foreach ($headers as $key) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }
        foreach ($routes as $route) {
            $total_orders = $route->orders->count();
            $vehicle = $route->orders()
                ->whereStatus('Delivered')
                ->orderBy('planning_sequence')
                ->get()
                ->first()
                ->vehicle;
            $values = [
                $route->route,
                $vehicle->plate,
                $vehicle->class_type,
                $vehicle->model,
                $total_orders,
                $route->orders()->delivered()->count(),
                $route->orders()->cancelled()->count(),
                $route->base_cost,
            ];
            $details_keys = [
                'route_support' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'recharge_extra_distance' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'recharge_extra' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'toll' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'surcharge_max_orders' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'penalty' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'kangaroo' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'baskets' => [
                    'value'   => '',
                    'coments' => '',
                ],
                'other' => [
                    'value'   => '',
                    'coments' => '',
                ],
            ];
            $surcharge_max_orders = FALSE;
            $route->routeCostDetails->each(function($cost_detail) use (&$details_keys, &$surcharge_max_orders) {
                $details_keys[$cost_detail->description]['value']   = $cost_detail->value;
                $details_keys[$cost_detail->description]['coments'] = $cost_detail->coments;
                if ($cost_detail->description == 'surcharge_max_orders') {
                    $surcharge_max_orders = TRUE;
                }
            });
            if ($total_orders >= 36 && !$surcharge_max_orders) {
                $details_keys['surcharge_max_orders']['value'] = ($total_orders - 35) * 5000;
            }
            foreach ($details_keys as $key => $details) {
                $key_in_detail = !empty($details['value']);
                array_push($values, $yer_or_not[$key_in_detail]);
                array_push($values, $details['value']);
                array_push($values, $details['coments']);
            }
            array_push($values, $route->total_cost);
            $i = 0;
            foreach ($values as $key => $value){
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                $i++;
            }
            $j++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
        $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
        $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
        $file['client_original_name'] = $filename;
        $file['client_original_extension'] = 'xlsx';
        return upload_image($file, false, Config::get('app.download_directory_temp'));
    }

}
