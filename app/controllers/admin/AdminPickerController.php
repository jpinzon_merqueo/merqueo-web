<?php

namespace admin;

use DB, Hash, Session, Redirect, View, Request, Input, Picker, City, PickerSession;

class AdminPickerController extends AdminController
{
    /**
     * Grilla de alistadores
     */
    public function index()
    {
        $search = Input::has('s') ? Input::get('s') : '';
        $query = Picker::join('cities', 'cities.id', '=', 'pickers.city_id')
            ->leftJoin('picker_sessions', 'picker_sessions.picker_id', '=', 'pickers.id')
            ->where('pickers.city_id', Input::has('city_id') ? Input::get('city_id') : Session::get('admin_city_id'));

        if (!empty($search)) {
            $query->where(function ($query) use ($search) {
                $query->where(DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%' . $search . '%');
                $query->orWhere('identity_number', 'LIKE', '%' . $search . '%');
                $query->orWhere('phone', 'LIKE', '%' . $search . '%');
            });
        }

        $query->select(
            'pickers.*',
            'city',
            DB::raw(
                "(SELECT  session_actived FROM picker_sessions WHERE picker_id = pickers.id " .
                "ORDER BY picker_sessions.id DESC LIMIT 1) AS session"
            )
        );

        if (Session::get('admin_designation') != 'Super Admin') {
            $query->where('pickers.city_id', Session::get('admin_city_id'));
        }

        $pickers = $query->groupBy('pickers.id')
            ->orderBy('session', 'DESC')
            ->paginate(30);

        return View::make('admin.pickers.index')
            ->with('title', 'Alistadores')
            ->with('pickers', $pickers)
            ->with('cities', $this->get_cities());
    }

    /**
     * Editar Picker
     * @param int $id
     * @return
     */
    public function edit($id = 0)
    {
        $sub_title = $id ? 'Editar Alistador' : 'Nuevo Alistador';
        $picker = Picker::find($id);
        if (!$picker)
            $picker = new Picker;

        if (Session::get('admin_designation') != 'Super Admin')
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        else $cities = $this->get_cities();

        return View::make('admin.pickers.picker_form')
            ->with('title', 'Picker')
            ->with('sub_title', $sub_title)
            ->with('cities', $cities)
            ->with('picker', $picker)
            ->with('warehouses', $this->get_warehouses())
            ->with('store_admin', false);
    }

    /**
     * Guardar Picker
     */
    public function save()
    {
        $id = Input::get('id');
        $picker = Picker::find($id);
        $action = 'update';
        if (!$picker) {
            $picker = new Picker;
            $action = 'insert';
        }

        $picker->first_name = Input::get('first_name');
        $picker->last_name = Input::get('last_name');
        $picker->phone = Input::get('phone');
        $picker->warehouse_id = Input::get('warehouse_id');
        $picker->rol = Input::get('rol');
        $picker->identity_number = str_replace('.', '', Input::get('identity_number'));
        $picker->status = (int)Input::get('status', 0);

        if (Input::has('id')) {
            $picker_finder = Picker::where('identity_number', $picker->identity_number)->where('id', '<>', $id)->first();
            if ($picker_finder) {
                return Redirect::route('adminPickers.index')->with('type', 'error')->with('message', 'Ya existe un Alistador con la misma cédula.');
            }
        } else {
            $picker_finder = Picker::where('identity_number', $picker->identity_number)->first();
            if ($picker_finder) {
                return Redirect::route('adminPickers.index')->with('type', 'error')->with('message', 'Ya existe un Alistador con la misma cédula.');
            }
        }

        $picker->email = Input::get('email');
        $picker->city_id = Input::get('city_id');
        $picker->profile = Input::get('profile');
        $picker->type = Input::get('type');

        if (Input::has('password')) {
            $password = Input::get('password');
            if (!empty($password))
                $picker->password = Hash::make(Input::get('password'));
        }

        $picker->save();
        $this->admin_log('pickers', $picker->id, $action);
        return Redirect::route('adminPickers.index')->with('type', 'success')->with('message', 'Alistador actuzalizado con éxito.');
    }

    /**
     * Eliminar Picker
     */
    public function delete()
    {
        $id = Request::segment(4);
        $picker = Picker::find($id);
        Picker::where('id', $id)->update(array('status' => 0));
        $this->admin_log('pickers', $picker->id, 'delete');
        return Redirect::route('adminPickers.index')->with('type', 'success')->with('message', 'Alistador eliminado con éxito.');
    }

    public function logout($id = 0)
    {
        $picker_session = PickerSession::where('picker_id', '=', $id);
        $picker_session->update(['session_actived' => 0]);
        return Redirect::route('adminPickers.index')->with('type', 'success')->with('message', 'Sessión cerrada con éxito.');
    }
}