<?php

namespace admin\report\transport;

use admin\AdminController, Request, View, Input, Redirect, Session, DB, City, Product, Response, Config, Route, Order, Zone;

class DriversWithoutActivityController extends AdminController
{
	/**
	* Index
	*/
	public function index()
	{
		if(!Input::ajax()){

			return View::make('admin.report.transport.driver_without_activity.index')
                        ->with('title', 'Vehículos sin entregar pedido')
                        ->with('cities', $this->get_cities())
                        ->with('delivery_windows', DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id'))
                        ->with('delivery_windows_same_day', DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id'), true)))
                        ->with('warehouses', $this->get_warehouses());
		}
	}

	/**
     * Funcion que retorna un JSON con la lista de las zonas segun una bodega
     *
     * @return json con listado de las zonas
     */
	public function get_zones()
	{
		if(Input::ajax())
		{
			$warehouse_id = Input::get('warehouse_id');
			$zones = Zone::select('id', 'name')->where('warehouse_id', $warehouse_id)->orderBy('name', 'ASC')->get()->toArray();
			return Response::json(['result'=> ['zones' => $zones ], 'status'=>true]);
		}
	}

	/**
     * Función retorna la lista de los vehiculos que tienen inactividad
     *
     * @return el cambio de poscisión del array actual dentro de la matriz
     */
	public function get_vehicles()
	{
		if (Input::ajax())
		{
			set_time_limit(0);
			$orders = [];
			$warehouse_id = Input::get('warehouse_id');
			$delivery_time = Input::get('delivery_time');
			$zone_id = Input::get('zone_id');
			$date = date('Y-m-d');

			$orders_dispached = Order::select( DB::raw('MAX(orders.planning_sequence) AS planning_sequence'), 'orders.driver_id', 'orders.vehicle_id')
							->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
							->join('routes', 'routes.id', '=', 'orders.route_id')
							->join('zones', 'zones.id', '=', 'routes.zone_id')
							->join('drivers', 'drivers.id', '=', 'orders.driver_id')
                            ->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id' )
							->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
							->where('order_groups.warehouse_id', $warehouse_id)
							->where('orders.delivery_time', $delivery_time)
							->where(DB::raw("DATE(orders.delivery_date)"), $date)
							->where('orders.status', 'Dispatched');

			if($zone_id != 'all')
				$orders_dispached->where('order_groups.zone_id', $zone_id);

			$orders_dispached = $orders_dispached
									->orderBy('orders.planning_sequence', 'DESC')
									->groupBy('orders.driver_id')
									->get();
										
			
			if(count($orders_dispached)){
				
				foreach ($orders_dispached as $order) {
					$last_delivery_order = Order::select('orders.management_date', 'routes.route', 'vehicles.plate', 'transporters.fullname', DB::raw("CONCAT_WS(' ', drivers.first_name, drivers.last_name) AS driver_name"), DB::raw("ROUND(time_to_sec((TIMEDIFF('".date('Y-m-d H:i:s')."', orders.management_date ))) / 60) AS delay") )
									->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
									->join('routes', 'routes.id', '=', 'orders.route_id')
									->join('zones', 'zones.id', '=', 'routes.zone_id')
									->join('drivers', 'drivers.id', '=', 'orders.driver_id')
                                    ->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id' )
									->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
									->where('order_groups.warehouse_id', $warehouse_id)
									->where('orders.delivery_time', $delivery_time)
									->where('orders.status', 'Delivered')
									->where('orders.driver_id', $order->driver_id)
									->where('orders.vehicle_id', $order->vehicle_id)
									->where(DB::raw("DATE(orders.delivery_date)"), $date);

					if($zone_id != 'all')
						$last_delivery_order->where('order_groups.zone_id', $zone_id);

					$last_delivery_order =$last_delivery_order
											->orderBy('orders.management_date', 'DESC')
											->get()
											->first();
					if(count($last_delivery_order)>0){

						$last_delivery_order = $last_delivery_order->toArray();
						$current_date = new \DateTime();
						$management_date = new \DateTime($last_delivery_order['management_date']);
						$diff = ($current_date->getTimestamp()-$management_date->getTimestamp())/60;
						$last_delivery_order['diff'] = $diff;
						$last_delivery_order['formated_inactivity'] = get_time('',$last_delivery_order['management_date'], null, false);
						$last_delivery_order['management_date'] = $management_date->format('G:ia');
						$orders[] = $last_delivery_order; 
					}
					//if($diff >= 45){
					//}
				}
			}
			uasort($orders, 'self::sortByPriority');



			$response =['table' => View::make('admin.report.transport.driver_without_activity.index')
				                        ->with('title', 'Productos No Visibles')
				                        ->with('orders', $orders)
				                        ->renderSections()['content']
				       ];
            return Response::json(['result'=> $response, 'status'=>true]);
        }
	}

	/**
     * Función necesaria para la función de ordenamiento uasort  de PHP,permite ordenar los elementos de una matriz asociativa por uno de los atributos
     *
     * @param array $a posiciòn actual dentro de la matriz
     * @param array $b posiciòn siguiente dentro de la matriz
     * @return el cambio de poscisión del array actual dentro de la matriz
     */
	private static function sortByPriority( $a, $b )
	{
		if ($a['delay'] == $b['delay'])
		{
	        return 0;
	    }

		return ($a['delay'] > $b['delay']) ? -1 : 1;
	}
}
