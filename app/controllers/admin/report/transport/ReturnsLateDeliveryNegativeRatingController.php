<?php

namespace admin\report\transport;

use admin\AdminController, Request, View, Input, Redirect, Session, DB, City, Product, Response, Config, Route, Order, Zone;

class ReturnsLateDeliveryNegativeRatingController extends AdminController
{
	/**
	* Index
	*/
	public function index()
	{
		if(!Input::ajax()){

			return View::make('admin.report.transport.returns_late_delivery_negative_rating.index')
                        ->with('title', 'Devoluciones, entregas tarde y calificación negativa')
                        ->with('delivery_windows', \DeliveryWindow::where('shifts','<>','MD')->where('city_id', Session::get('admin_city_id'))->get())
                        ->with('delivery_windows_same_day', \DeliveryWindow::where('shifts','MD')->where('city_id', Session::get('admin_city_id'))->get())
                        ->with('cities', $this->get_cities())
                        ->with('warehouses', $this->get_warehouses());
		}
	}

	/**
     * Funcion que retorna un JSON con la lista de las zonas segun una bodega
     *
     * @return json con listado de las zonas
     */
	public function get_zones()
	{
		if(Input::ajax())
		{
			$warehouse_id = Input::get('warehouse_id');
			$zones = Zone::select('id', 'name')->where('warehouse_id', $warehouse_id)->orderBy('name', 'ASC')->get()->toArray();
			return Response::json(['result'=> ['zones' => $zones ], 'status'=>true]);
		}
	}

	/**
     * Funcion que retorna la lista de pedidos y los datos para los graficos
     *
     * @return json con listado de las zonas
     */
	public function get_orders()
	{
		if (Input::ajax())
		{
			set_time_limit(0);
			$warehouse_id = Input::get('warehouse_id');
			$delivery_time = Input::get('delivery_time');
			$zone_id = Input::get('zone_id');
			$report_type = Input::get('report_type');
			$date = date('Y-m-d');
			$all_orders = Order::select(
								'orders.id AS order_id','routes.id','orders.planning_sequence', 'orders.delivery_time', 'routes.zone_id', 'zones.name', 'orders.delivery_date','orders.status', 'orders.management_date', DB::raw("CONCAT_WS(' - ',transporters.fullname,CONCAT_WS(' ',drivers.first_name,drivers.last_name), vehicles.plate) AS driver_name"), 'orders.user_score', 'orders.user_score_typification', 'orders.user_score_comments', DB::raw("ROUND(time_to_sec((TIMEDIFF(orders.management_date, orders.delivery_date))) / 60) AS delay_time", 'routes.route')
							)
							->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
							->join('routes', 'routes.id', '=', 'orders.route_id')
							->join('zones', 'zones.id', '=', 'routes.zone_id')
							->join('drivers', 'drivers.id', '=', 'orders.driver_id')
                            ->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id' )
							->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
							->where('order_groups.warehouse_id', $warehouse_id)
							->where('orders.delivery_time', $delivery_time);
			if($zone_id != 'all')
				$all_orders->where('order_groups.zone_id', $zone_id);

			$all_orders->where(DB::raw("DATE(orders.delivery_date)"), $date)
				->where('orders.status', 'Delivered');

			$orders_with_returns = $all_orders;
			$orders_with_late_delivery = $all_orders;
			$orders_with_negative_rating = $all_orders;

			$orders_with_returns = Order::select(
											'orders.id AS order_id','routes.id','orders.planning_sequence', 'orders.delivery_time', 'routes.zone_id', 'zones.name', 'orders.delivery_date','orders.status', 'orders.management_date', DB::raw("CONCAT_WS(' - ',transporters.fullname,CONCAT_WS(' ',drivers.first_name,drivers.last_name), vehicles.plate) AS driver_name"), 'routes.route'
										)
										->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
										->join('routes', 'routes.id', '=', 'orders.route_id')
										->join('zones', 'zones.id', '=', 'routes.zone_id')
										->join('drivers', 'drivers.id', '=', 'orders.driver_id')
										->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id' )
                                        ->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
										->where('order_groups.warehouse_id', $warehouse_id)
										->where('orders.delivery_time', $delivery_time);
			if($zone_id != 'all')
				$orders_with_returns->where('order_groups.zone_id', $zone_id);

			$orders_with_returns = $orders_with_returns->where(DB::raw("DATE(orders.delivery_date)"), $date)
														->where('orders.status', 'Delivered')
														->join('order_products', 'order_products.order_id', '=', 'orders.id')
														->where( 'order_products.fulfilment_status', 'Returned')
														->groupBy('orders.id')
														->get();

			$orders_with_late_delivery = Order::select(
												'orders.id AS order_id','routes.id','orders.planning_sequence', 'orders.delivery_time', 'routes.zone_id', 'zones.name', 'orders.delivery_date','orders.status', 'orders.management_date', DB::raw("CONCAT_WS(' - ',transporters.fullname,CONCAT_WS(' ',drivers.first_name,drivers.last_name), vehicles.plate) AS driver_name"), 'orders.user_score', 'orders.user_score_typification', 'orders.user_score_comments', DB::raw("ROUND(time_to_sec((TIMEDIFF(orders.management_date, orders.delivery_date))) / 60) AS delay_time"), 'routes.route'
											)
											->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
											->join('routes', 'routes.id', '=', 'orders.route_id')
											->join('zones', 'zones.id', '=', 'routes.zone_id')
											->join('drivers', 'drivers.id', '=', 'orders.driver_id')
											->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id' )
                                            ->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
											->where('order_groups.warehouse_id', $warehouse_id)
											->where('orders.delivery_time', $delivery_time);
			if($zone_id != 'all')
				$orders_with_late_delivery->where('order_groups.zone_id', $zone_id);
			
			$orders_with_late_delivery = $orders_with_late_delivery->where(DB::raw("DATE(orders.delivery_date)"), $date)
																	->where('orders.status', 'Delivered')
																	->whereRaw("ROUND(time_to_sec((TIMEDIFF(orders.management_date, orders.delivery_date))) / 60) > 0")
																	->get();

			$orders_with_negative_rating = Order::select(
												'orders.id AS order_id','routes.id','orders.planning_sequence', 'orders.delivery_time', 'routes.zone_id', 'zones.name', 'orders.delivery_date','orders.status', 'orders.management_date', DB::raw("CONCAT_WS(' - ',transporters.fullname,CONCAT_WS(' ',drivers.first_name,drivers.last_name), vehicles.plate) AS driver_name"), 'orders.user_score', 'orders.user_score_typification', 'routes.route',
												DB::raw("IFNULL(
													(SELECT GROUP_CONCAT(CONCAT_WS(': ',CONCAT(p.name,' ',p.quantity,p.unit), ops.reason) SEPARATOR '<br>') AS reason
													FROM order_product_scores ops
													INNER JOIN order_products op ON ops.order_product_id = op.id
													INNER JOIN store_products sp ON sp.id = op.store_product_id
													INNER JOIN products p ON p.id = sp.product_id
													WHERE op.order_id = orders.id),
													`orders`.`user_score_comments`
												) AS user_score_comments")
											)
											->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
											->join('routes', 'routes.id', '=', 'orders.route_id')
											->join('zones', 'zones.id', '=', 'routes.zone_id')
											->join('drivers', 'drivers.id', '=', 'orders.driver_id')
                                            ->join('vehicles', 'vehicles.id', '=', 'orders.vehicle_id' )
											->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
											->where('order_groups.warehouse_id', $warehouse_id)
											->where('orders.delivery_time', $delivery_time);
			if($zone_id != 'all')
				$orders_with_negative_rating->where('order_groups.zone_id', $zone_id);
				$orders_with_negative_rating = $orders_with_negative_rating->where(DB::raw("DATE(orders.delivery_date)"), $date)
																		->where('orders.status', 'Delivered')
																		->where('orders.user_score', 0)
																		->groupBy('orders.id')
																		->get();
			$all_orders = $all_orders->get();

			$series = [
				'returns' => [
					['label'=>'Con devoluciones', 'data' => count($orders_with_returns), 'color'=>'#0073b7'],
					['label'=>'Sin devoluciones', 'data' => count($all_orders) - count($orders_with_returns), 'color'=>'#E91A58']
				],
				'late_delivery' => [
					['label'=>'Con retraso en la entrega', 'data' => count($orders_with_late_delivery), 'color'=>'#0073b7'],
					['label'=>'Sin retraso en la entrega', 'data' => count($all_orders) - count($orders_with_late_delivery), 'color'=>'#E91A58']
				],
				'negative_rating' => [
					['label'=>'Con calificación negativa', 'data' => count($orders_with_negative_rating), 'color'=>'#0073b7'],
					['label'=>'Sin calificación negativa', 'data' => count($all_orders) - count($orders_with_negative_rating), 'color'=>'#E91A58']
				]
			];

			$orders = [];
			switch ($report_type)
			{
				case 'returns':
					$orders = $orders_with_returns;
					break;
				case 'late_delivery':
					$orders = $orders_with_late_delivery;
					break;
				case 'negative_rating':
					$orders = $orders_with_negative_rating;
					break;
			}

			$response =['table' => View::make('admin.report.transport.returns_late_delivery_negative_rating.index')
				                        ->with('title', 'Productos No Visibles')
				                        ->with('orders', $orders)
				                        ->with('type', $report_type)
				                        ->with('series', json_encode($series) )
				                        ->renderSections()['content'],
                        'charts' => $series
				       ];
            return Response::json(['result'=> $response, 'status'=>true]);
        }
	}

    /**
     * @return \Illuminate\Http\JsonResponse
     */
	public function getDeliveryWindows(){
	    $cityId = Input::get('city_id');
        $deliveryWindows = \DeliveryWindow::where('shifts','<>','MD')->where('city_id', $cityId)->get();
        $deliveryWindowsSameDay = \DeliveryWindow::where('shifts','MD')->where('city_id', $cityId)->get();

        return Response::json(['result'=> [
            'deliveryWindows' => $deliveryWindows,
            'deliveryWindowsSameDay' => $deliveryWindowsSameDay
        ], 'status'=>true]);


    }
}
