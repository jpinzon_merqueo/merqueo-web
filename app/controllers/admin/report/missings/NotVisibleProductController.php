<?php

namespace admin\report\missings;

use admin\AdminController, Request, View, Input, Redirect, Session, DB, City, Product, Response, Config, Route, StoreProduct, StoreProductWarehouse, DateTime;

class NotVisibleProductController extends AdminController
{
	public function index()
	{
		if(!Input::ajax()){

			return View::make('admin.report.not_visible_product.index')
                        ->with('title', 'Productos Inactivos')
                        ->with('cities', $this->get_cities())
                        ->with('warehouses', $this->get_warehouses());
		}
	}

	public function get_products_list_ajax()
	{
		if (Input::ajax())
		{
			set_time_limit(0);
			$warehouse_id = Input::get('warehouse_id');
			$type = Input::get('type');
			$products = StoreProductWarehouse::getNotVisibleProducts($warehouse_id, $type);

			return View::make('admin.report.not_visible_product.index')
                        ->with('title', 'Productos No Visibles')
                        ->with('products', $products['not_visibles'])
                        ->with('qty_all_products', $products['qty_all_products'])
                        ->with('qty_not_visibles', $products['qty_not_visibles'])
                        ->renderSections()['content'];
        }
	}
}