<?php

namespace admin\report\missings;

use Carbon\Carbon;
use DB;
use Input;
use OrderProduct;
use OrderProductGroup;
use Product;
use Request;
use Response;
use View;
use Picker;
use admin\AdminController;

class AdminMissingPickingProductController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (!Request::ajax()) {
            return View::make('admin.report.missings.picking_missings')
                        ->with('title', 'Faltantes por Alistador')
                        ->with('cities', $this->get_cities())
                        ->with('warehouses', $this->get_warehouses());
        } else {
            $warehouse_id = Input::get('warehouse_id');
            $date = Carbon::now();
            $today = $date->toDateString();

            $missings_dry = Picker::leftJoin('orders', 'pickers.id', '=', 'orders.picker_dry_id')
                ->leftJoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->leftJoin('order_products', 'orders.id', '=', 'order_products.order_id')
                ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->whereRaw("DATE(orders.delivery_date) = '$today'")
                ->where('order_products.type', 'Product')
                ->where('store_products.storage', 'Seco')
                ->where('order_groups.warehouse_id', $warehouse_id)
                ->where('pickers.status', 1)
                ->select(
                    'orders.picker_dry_id',
                    DB::raw("CONCAT(pickers.first_name, ' ', pickers.last_name) AS picker"),
                    DB::raw("SUM(IF(order_products.fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                    DB::raw("SUM(IF(order_products.fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                )
                ->groupBy('orders.picker_dry_id')
                ->orderBy('quantity_not_available', 'DESC')
                ->get();

            /*$missings_dry = OrderProduct::join('orders', 'orders.id', '=', 'order_products.order_id')
                            ->join('order_groups as og', 'og.id', '=', 'group_id')
                            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                            ->join('pickers AS d', 'd.id', '=', 'orders.picker_dry_id')
                            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
                            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'))
                            ->where('order_products.type', 'Product')
                            ->where('storage', 'Seco')
                            ->where('warehouse_id', Input::get('warehouse_id'))
                            ->select(
                                'orders.picker_dry_id',
                                DB::raw("CONCAT(d.first_name, ' ', d.last_name) AS picker"),
                                DB::raw("SUM(IF(fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                                DB::raw("SUM(IF(fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                            )
                            ->groupBy('orders.picker_dry_id')
                            ->orderBy('quantity_not_available', 'DESC')
                            ->get();*/

            /*var_dump($missings_dry);
            exit;*/

            $missing_dry_groups = Picker::leftJoin('orders', 'pickers.id', '=', 'orders.picker_dry_id')
                ->leftJoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->leftJoin('order_product_group', 'orders.id', '=', 'order_product_group.order_id')
                ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->whereRaw("DATE(orders.delivery_date) = '$today'")
                ->where('store_products.storage', 'Seco')
                ->where('order_groups.warehouse_id', $warehouse_id)
                ->select(
                    'orders.picker_dry_id',
                    DB::raw("CONCAT(pickers.first_name, ' ', pickers.last_name) AS picker"),
                    DB::raw("SUM(IF(order_product_group.fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                    DB::raw("SUM(IF(order_product_group.fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                )
                ->groupBy('orders.picker_dry_id')
                ->orderBy('quantity_not_available', 'DESC')
                ->get();

            /*$missing_dry_groups = OrderProductGroup::join('orders', 'orders.id', '=', 'order_product_group.order_id')
                            ->join('order_groups as og', 'og.id', '=', 'group_id')
                            ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
                            ->join('pickers AS d', 'd.id', '=', 'orders.picker_dry_id')
                            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
                            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'))
                            ->where('storage', 'Seco')
                            ->where('warehouse_id', Input::get('warehouse_id'))
                            ->select(
                                'orders.picker_dry_id',
                                DB::raw("CONCAT(d.first_name, ' ', d.last_name) AS picker"),
                                DB::raw("SUM(IF(fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                                DB::raw("SUM(IF(fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                            )
                            ->groupBy('orders.picker_dry_id')
                            ->orderBy('quantity_not_available', 'DESC')
                            ->get();*/

            foreach ($missings_dry as $missing_dry) {
                foreach ($missing_dry_groups as $missing_dry_group) {
                    if ($missing_dry->picker_dry_id == $missing_dry_group->picker_dry_id) {
                        $missing_dry->quantity_not_available += $missing_dry_group->quantity_not_available;
                        $missing_dry->quantity_fullfilled += $missing_dry_group->quantity_fullfilled;
                    }
                }
            }

            $missings_cold = Picker::leftJoin('orders', 'pickers.id', '=', 'orders.picker_cold_id')
                ->leftJoin('order_products', 'orders.id', '=', 'order_products.order_id')
                ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->leftJoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->whereRaw("DATE(orders.delivery_date) = '$today'")
                ->where('order_products.type', 'Product')
                ->where('store_products.storage', '<>', 'Seco')
                ->where('order_groups.warehouse_id', $warehouse_id)
                ->select(
                    'orders.picker_cold_id',
                    DB::raw("CONCAT(pickers.first_name, ' ', pickers.last_name) AS picker"),
                    DB::raw("SUM(IF(order_products.fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                    DB::raw("SUM(IF(order_products.fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                )
                ->groupBy('orders.picker_cold_id')
                ->orderBy('quantity_not_available', 'DESC')
                ->get();


            /*$missings_cold = OrderProduct::join('orders', 'orders.id', '=', 'order_products.order_id')
                            ->join('order_groups as og', 'og.id', '=', 'group_id')
                            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                            ->join('pickers AS c', 'c.id', '=', 'orders.picker_cold_id')
                            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
                            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'))
                            ->where('order_products.type', 'Product')
                            ->where('storage', '<>', 'Seco')
                            ->where('warehouse_id', Input::get('warehouse_id'))
                            ->select(
                                'orders.picker_cold_id',
                                DB::raw("CONCAT(c.first_name, ' ', c.last_name) AS picker"),
                                DB::raw("SUM(IF(fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                                DB::raw("SUM(IF(fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                            )
                            ->groupBy('orders.picker_cold_id')
                            ->orderBy('quantity_not_available', 'DESC')
                            ->get();*/

            /*$missing_cold_groups = OrderProductGroup::join('orders', 'orders.id', '=', 'order_product_group.order_id')
                            ->join('order_groups as og', 'og.id', '=', 'group_id')
                            ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
                            ->join('pickers AS d', 'd.id', '=', 'orders.picker_cold_id')
                            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'))
                            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'))
                            ->where('storage', '<>', 'Seco')
                            ->where('warehouse_id', Input::get('warehouse_id'))
                            ->select(
                                'orders.picker_cold_id',
                                DB::raw("CONCAT(d.first_name, ' ', d.last_name) AS picker"),
                                DB::raw("SUM(IF(fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                                DB::raw("SUM(IF(fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                            )
                            ->groupBy('orders.picker_cold_id')
                            ->orderBy('quantity_not_available', 'DESC')
                            ->get();*/

            $missing_cold_groups = Picker::leftJoin('orders', 'pickers.id', '=', 'orders.picker_cold_id')
                ->leftJoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->leftJoin('order_product_group', 'orders.id', '=', 'order_product_group.order_id')
                ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->whereRaw("DATE(orders.delivery_date) = '$today'")
                ->where('store_products.storage', '<>', 'Seco')
                ->where('order_groups.warehouse_id', $warehouse_id)
                ->select(
                    'orders.picker_cold_id',
                    DB::raw("CONCAT(pickers.first_name, ' ', pickers.last_name) AS picker"),
                    DB::raw("SUM(IF(order_product_group.fulfilment_status = 'Not Available', 1, 0)) AS quantity_not_available"),
                    DB::raw("SUM(IF(order_product_group.fulfilment_status = 'Fullfilled', 1, 0)) as quantity_fullfilled")
                )
                ->groupBy('orders.picker_cold_id')
                ->orderBy('quantity_not_available', 'DESC')
                ->get();

            foreach ($missings_cold as $missing_cold) {
                foreach ($missing_cold_groups as $missing_cold_group) {
                    if ($missing_cold->picker_cold_id == $missing_cold_group->picker_cold_id) {
                        $missing_cold->quantity_not_available += $missing_cold_group->quantity_not_available;
                        $missing_cold->quantity_fullfilled += $missing_cold_group->quantity_fullfilled;
                    }
                }
            }


            $missings = [];
            /*if ($missings_dry) {
                foreach ($missings_dry as $missing_dry) {
                    $missings[$missing_dry->quantity_not_available] = $missing_dry;
                }
            }

            if ($missings_cold) {
                foreach ($missings_cold as $missing_cold) {
                    $missings[$missing_cold->quantity_not_available] = $missing_cold;
                }
            }*/
            if ($missings_dry) {
                foreach ($missings_dry as $missing_dry) {
                    $missings[] = $missing_dry;
                }
            }

            if ($missings_cold) {
                foreach ($missings_cold as $missing_cold) {
                    $missings[] = $missing_cold;
                }
            }

            return View::make('admin.report.missings.picking_missings')
                        ->with('title', 'Faltantes por Alistador')
                        ->with('missings', $missings)
                        ->renderSections()['content'];
        }
    }
}
