<?php

namespace admin\report\inventory;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Order;
use Routes;
use Transporter;
use Product;
use Store;
use ProviderOrderReception;
use admin\AdminController;

class CeroStockController extends AdminController
{
    public function index()
    {
        $data['title'] = 'Stocks en Cero';
        $data['cities'] = $this->get_cities();
        $data['warehouses'] = $this->get_warehouses(Session::get('admin_city_id'));
        $date = Carbon::now();
        $date_1 = $date->format('d/m/Y');
        $date_2 = $date->addDay()->format('d/m/Y');
        $date_3 = $date->addDay()->format('d/m/Y');
        $date_4 = $date->addDay()->format('d/m/Y');
        $date_5 = $date->addDay()->format('d/m/Y');
        $data['date_1'] = $date_1;
        $data['date_2'] = $date_2;
        $data['date_3'] = $date_3;
        $data['date_4'] = $date_4;
        $data['date_5'] = $date_5;

        return View::make('admin.report.inventory.cero_stock.index', $data);
    }

    public function getCeroStockAjax()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);
        $warehouse_id = Input::get('warehouse_id');
        $city_id = Input::get('city_id');
        $store = Store::getActiveStores($city_id);
        $store = $store->first();
        $stocks = Input::get('stocks');

        $negative_picked_stock = DB::selectOne("
            SELECT
                count(products.id) as cant
            FROM products
            INNER JOIN store_products ON products.id = store_products.product_id
            INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                AND store_product_warehouses.warehouse_id = :warehouse_id
            WHERE store_product_warehouses.picked_stock < 0
        ", ['warehouse_id' => $warehouse_id]);

        $positive_picked_stock = DB::selectOne("
            SELECT
                count(products.id) as cant
            FROM products
            INNER JOIN store_products ON products.id = store_products.product_id
            INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                AND store_product_warehouses.warehouse_id = :warehouse_id
            WHERE store_product_warehouses.picked_stock > 0
        ", ['warehouse_id' => $warehouse_id]);

        $negative_picking_stock = DB::selectOne("
            SELECT
                count(products.id) as cant
            FROM products
            INNER JOIN store_products ON products.id = store_products.product_id
            INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                AND store_product_warehouses.warehouse_id = :warehouse_id
            WHERE store_product_warehouses.picking_stock < 0
        ", ['warehouse_id' => $warehouse_id]);

        $receptions = DB::selectOne("
            SELECT
                COUNT(*) as cant
            FROM provider_order_receptions
            INNER JOIN provider_orders ON provider_order_receptions.provider_order_id = provider_orders.id
                AND provider_orders.warehouse_id = :id
            WHERE provider_order_receptions.status = 'Recibido';
        ", ['id' => $warehouse_id]);

        $picked_stock = $positive_picked_stock->cant + $negative_picked_stock->cant;

        $data['picked'] = $picked_stock;
        $data['picking'] = $negative_picking_stock->cant;
        $data['reception'] = $receptions->cant;

        if ($stocks == 'picked') {
            $products = Product::
            whereHas('storeProduct', function ($query) use ($store) {
                $query->where('store_id', $store->id);
            })
            ->whereHas('storeProduct.storeProductWarehouses', function ($query) use ($warehouse_id) {
                $query->where('warehouse_id', $warehouse_id)
                ->where(function ($q) {
                    $q->where('picked_stock', '<', 0)
                        ->orWhere('picked_stock', '>', 0);
                });
            })
            ->with([
                'storeProduct' => function ($query) use ($store) {
                    $query->where('store_id', $store->id);
                },
                'storeProduct.storeProductWarehouses' => function ($query) use ($warehouse_id) {
                    $query->where('warehouse_id', $warehouse_id)
                    ->where(function ($q) {
                        $q->where('picked_stock', '<', 0)
                        ->orWhere('picked_stock', '>', 0);
                    });
                }
            ])
            ->join('store_products', function ($query) use ($store) {
                $query->on('products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', '=', $store->id);
            })
            ->join('store_product_warehouses', function ($query) use ($warehouse_id) {
                $query->on('store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->where('store_product_warehouses.warehouse_id', '=', $warehouse_id);
            })
            ->where(function ($q) {
                $q->where('store_product_warehouses.picked_stock', '<', 0)
                ->orWhere('store_product_warehouses.picked_stock', '>', 0);
            })
            ->orderBy('store_product_warehouses.picked_stock', 'DESC')
            ->select('products.*')
            ->paginate(20);

            $data['html'] = View::make(
                'admin.report.inventory.cero_stock.ajax.picked',
                [
                    'products' => $products,
                    'warehouse_id' => $warehouse_id
                ]
            )->render();
        }

        if ($stocks == 'picking') {
            $products = Product::
            whereHas('storeProduct', function ($query) use ($store) {
                $query->where('store_id', $store->id);
            })
            ->whereHas('storeProduct.storeProductWarehouses', function ($query) use ($warehouse_id) {
                $query->where('warehouse_id', $warehouse_id)
                ->where(function ($q) {
                    $q->where('picking_stock', '<', 0);
                });
            })
            ->with([
                'storeProduct' => function ($query) use ($store) {
                    $query->where('store_id', $store->id);
                },
                'storeProduct.storeProductWarehouses' => function ($query) use ($warehouse_id) {
                    $query->where('warehouse_id', $warehouse_id)
                    ->where(function ($q) {
                        $q->where('picking_stock', '<', 0);
                    });
                }
            ])
            ->join('store_products', function ($query) use ($store) {
                $query->on('products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', '=', $store->id);
            })
            ->join('store_product_warehouses', function ($query) use ($warehouse_id) {
                $query->on('store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->where('store_product_warehouses.warehouse_id', '=', $warehouse_id);
            })
            ->where(function ($q) {
                $q->where('store_product_warehouses.picking_stock', '<', 0);
            })
            ->orderBy('store_product_warehouses.picking_stock', 'ASC')
            ->select('products.*')
            ->paginate(20);

            $data['html'] = View::make(
                'admin.report.inventory.cero_stock.ajax.picking',
                [
                    'products' => $products,
                    'warehouse_id' => $warehouse_id
                ]
            )->render();
        }

        if ($stocks == 'recibo') {
            $receptions = ProviderOrderReception::whereHas('providerOrder', function ($query) use ($warehouse_id) {
                $query->where('warehouse_id', $warehouse_id);
            })
            ->with([
                'providerOrder' => function ($query) use ($warehouse_id) {
                    $query->where('warehouse_id', $warehouse_id);
                }
            ])
            ->where('status', 'Recibido')
            ->paginate(20);

            $receptions->each(function ($reception) {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $reception->received_date);
                $current_date = Carbon::now();
                $reception->received_date = $date->format('d/m/Y h:i:s A');
                $reception->current_date = $current_date->format('d/m/Y h:i:s A');
            });

            $data['html'] = View::make(
                'admin.report.inventory.cero_stock.ajax.reception',
                [
                    'receptions' => $receptions,
                    'warehouse_id' => $warehouse_id
                ]
            )->render();
        }

        return Response::json($data);
    }
}
