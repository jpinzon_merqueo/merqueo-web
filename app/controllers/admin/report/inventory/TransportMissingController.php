<?php

namespace admin\report\inventory;

use Carbon\Carbon;
use Driver;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Order;
use Routes;
use Transporter;
use admin\AdminController;

class TransportMissingController extends AdminController
{
    public function index()
    {
        $data['title'] = 'Faltantes de Transporte';
        $data['cities'] = $this->get_cities();
        $data['warehouses'] = $this->get_warehouses(Session::get('admin_city_id'));

        return View::make('admin.report.inventory.transport_missing.index', $data);
    }

    public function getTransportMissingAjax()
    {
        $warehouse_id = Input::get('warehouse_id');
        $date = Carbon::now();
        $today = $date->toDateString();
        $yesterday = $date->subDay()->toDateString();

        $today_orders = DB::select(DB::raw("
                SELECT
                    COUNT(DISTINCT routes.id) AS num_routes,
                    SUM( IF(DATE(orders.dispatched_date) = '$today', 1, 0) ) AS dispatched_orders,
                    (
                        SELECT
                            COUNT(DISTINCT orders.id)
                        FROM orders
                        INNER JOIN order_products ON orders.id = order_products.order_id
                            AND order_products.update_stock_back = 1
                        INNER JOIN routes ON orders.route_id = routes.id
                        WHERE
                            DATE(routes.planning_date) = '$today'
                            AND routes.warehouse_id = $warehouse_id

                    ) AS modified_orders,
                    (
                            SELECT
                                COUNT(DISTINCT orders.id)
                            FROM orders
                            INNER JOIN order_product_group ON orders.id = order_product_group.order_id AND order_product_group.update_stock_back = 1
                            INNER JOIN routes ON orders.route_id = routes.id
                            WHERE
                                DATE(routes.planning_date) = '$today'
                                AND routes.warehouse_id = $warehouse_id

                        ) AS modified_orders_group
                FROM orders
                INNER JOIN routes ON orders.route_id = routes.id
                INNER JOIN order_groups ON orders.group_id = order_groups.id
                    AND order_groups.warehouse_id = $warehouse_id
                WHERE
                    DATE(routes.planning_date) = '$today'
                    AND routes.warehouse_id = $warehouse_id;
            "));

        $today_orders = $today_orders[0];
        if ($today_orders->dispatched_orders > 0) {
            $today_orders->modified_orders += $today_orders->modified_orders_group;
            $percentage = number_format((float)($today_orders->modified_orders/$today_orders->dispatched_orders)*100, 2, '.', '');
            $today_orders->modified_orders_percentage = $percentage;
        } else {
            $today_orders->modified_orders_percentage = 0.00;
        }

        $yesterday_orders = DB::select(DB::raw("
                SELECT
                    COUNT(DISTINCT routes.id) AS num_routes,
                    SUM( IF(DATE(orders.dispatched_date) = '$yesterday', 1, 0) ) AS dispatched_orders,
                    (
                        SELECT
                            COUNT(DISTINCT orders.id)
                        FROM orders
                        INNER JOIN order_products ON orders.id = order_products.order_id
                            AND order_products.update_stock_back = 1
                        INNER JOIN routes ON orders.route_id = routes.id
                        WHERE
                            DATE(routes.planning_date) = '$yesterday'
                            AND routes.warehouse_id = $warehouse_id

                    ) AS modified_orders,
                    (
                            SELECT
                                COUNT(DISTINCT orders.id)
                            FROM orders
                            INNER JOIN order_product_group ON orders.id = order_product_group.order_id AND order_product_group.update_stock_back = 1
                            INNER JOIN routes ON orders.route_id = routes.id
                            WHERE
                                DATE(routes.planning_date) = '$yesterday'
                                AND routes.warehouse_id = $warehouse_id

                        ) AS modified_orders_group
                FROM orders
                INNER JOIN routes ON orders.route_id = routes.id
                INNER JOIN order_groups ON orders.group_id = order_groups.id
                    AND order_groups.warehouse_id = $warehouse_id
                WHERE
                    DATE(routes.planning_date) = '$yesterday'
                    AND routes.warehouse_id = $warehouse_id;
            "));

        $yesterday_orders = $yesterday_orders[0];
        if ($yesterday_orders->dispatched_orders > 0) {
            $yesterday_orders->modified_orders += $yesterday_orders->modified_orders_group;
            $percentage = number_format((float)($yesterday_orders->modified_orders/$yesterday_orders->dispatched_orders)*100, 2, '.', '');
            $yesterday_orders->modified_orders_percentage = $percentage;
        } else {
            $yesterday_orders->modified_orders_percentage = 0;
        }

        $data['today_orders'] = $today_orders;
        $data['yesterday_orders'] = $yesterday_orders;

        $html = View::make('admin.report.inventory.transport_missing.ajax.transport-missing-ajax', $data)->render();

        return Response::json(['html' => $html]);
    }

    public function getTransporterMissingAjax()
    {
        $warehouse_id = Input::get('warehouse_id');

        $date = Carbon::now();
        $today = $date->toDateString();
        $yesterday = $date->subDay()->toDateString();

        $today_drivers = DB::select(DB::raw("
            SELECT
                drivers.first_name,
                drivers.last_name,
                routes.id,
                routes.route,
                (
                    SELECT
                        COUNT(*)
                    FROM orders
                    WHERE orders.route_id = routes.id
                ) AS route_orders,
                COUNT(orders.id) AS num_orders_route,
                COUNT(DISTINCT order_products.order_id) AS modified_orders,
                COUNT(DISTINCT order_product_group.order_id) AS modified_orders_group
            FROM routes
            LEFT JOIN orders ON routes.id = orders.route_id
                AND DATE(orders.management_date) = '$today'
            LEFT JOIN drivers ON orders.driver_id = drivers.id AND drivers.id
            LEFT JOIN order_products ON orders.id = order_products.order_id AND order_products.update_stock_back = 1
            LEFT JOIN order_product_group ON orders.id = order_product_group.order_id AND order_product_group.update_stock_back = 1
            WHERE
                DATE(routes.dispatch_time) = '$today'
                AND routes.warehouse_id = $warehouse_id
            GROUP BY routes.id
            ORDER BY modified_orders DESC;
        "));

        foreach ($today_drivers as $key => $today_driver) {
            if ($today_driver->num_orders_route > 0) {
                $today_driver->modified_orders += $today_driver->modified_orders_group;
                $percentage = number_format((float)($today_driver->modified_orders/$today_driver->num_orders_route)*100, 2, '.', '');
                $today_driver->modified_orders_percentage = $percentage;
            } else {
                $today_driver->modified_orders_percentage = 0.00;
            }
        }

        $yesterday_drivers = DB::select(DB::raw("
            SELECT
                drivers.first_name,
                drivers.last_name,
                routes.id,
                routes.route,
                (
                    SELECT
                        COUNT(*)
                    FROM orders
                    WHERE orders.route_id = routes.id
                ) AS route_orders,
                COUNT(orders.id) AS num_orders_route,
                COUNT(DISTINCT order_products.order_id) AS modified_orders,
                COUNT(DISTINCT order_product_group.order_id) AS modified_orders_group
            FROM routes
            LEFT JOIN orders ON routes.id = orders.route_id
                AND DATE(orders.management_date) = '$yesterday'
            LEFT JOIN drivers ON orders.driver_id = drivers.id AND drivers.id
            LEFT JOIN order_products ON orders.id = order_products.order_id AND order_products.update_stock_back = 1
            LEFT JOIN order_product_group ON orders.id = order_product_group.order_id AND order_product_group.update_stock_back = 1
            WHERE
                DATE(routes.dispatch_time) = '$yesterday'
                AND routes.warehouse_id = $warehouse_id
            GROUP BY routes.id
            ORDER BY modified_orders DESC;
        "));

        foreach ($yesterday_drivers as $key => $yesterday_driver) {
            if ($yesterday_driver->num_orders_route > 0) {
                $yesterday_driver->modified_orders += $yesterday_driver->modified_orders_group;
                $percentage = number_format((float)($yesterday_driver->modified_orders/$yesterday_driver->num_orders_route)*100, 2, '.', '');
                $yesterday_driver->modified_orders_percentage = $percentage;
            } else {
                $yesterday_driver->modified_orders_percentage = 0.00;
            }
        }

        foreach ($today_drivers as $key => &$today_driver) {
            foreach ($yesterday_drivers as $key => $yesterday_driver) {
                if ($today_driver->id == $yesterday_driver->id) {
                    $today_driver->yesterday_modified_orders_percentage = $yesterday_driver->modified_orders_percentage;
                } else {
                    $today_driver->yesterday_modified_orders_percentage = 0;
                }
            }
        }

        $data['today_drivers'] = $today_drivers;

        $html = View::make('admin.report.inventory.transport_missing.ajax.transporter-missing-ajax', $data)->render();
        return Response::json(['html' => $html]);
    }
}
