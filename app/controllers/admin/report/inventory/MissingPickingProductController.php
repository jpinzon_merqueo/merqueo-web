<?php

namespace admin\report\inventory;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use OrderProduct;
use OrderProductGroup;
use Order;
use admin\AdminController;

class MissingPickingProductController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['title'] = 'Faltantes en Alistamiento';
        $data['cities'] = $this->get_cities();
        $data['warehouses'] = $this->get_warehouses(Session::get('admin_city_id'));

        $date = Carbon::now();
        $yesterday = $date->subDay()->format('d/m/Y');
        $data['yesterday'] = $yesterday;

        return View::make('admin.report.inventory.missing_picking_product.index', $data);
    }

    /**
     * Obtiene los productos faltantes en pedidos
     *
     * @return Json
     */
    public function getMissingProductsAjax()
    {
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $date = Carbon::now();
        $yesterday = $date->subDay()->toDateString();

        $products =  OrderProduct::with('storeProduct.product')
                        ->join('orders', 'order_products.order_id', '=', 'orders.id')
                        ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                        ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '".$yesterday."'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_products.store_product_id', '=', 'receptions.store_product_id')
                        ->whereIn('orders.status', ['Alistado', 'In Progress'])
                        ->whereIn('order_products.fulfilment_status', ['Not Available', 'Missing'])
                        ->where('order_groups.warehouse_id', $warehouse_id)
                        ->where('order_products.type', 'Product')
                        ->select(
                            'order_products.store_product_id',
                            DB::raw('SUM(quantity) AS quantity'),
                            'receptions.quantity_received',
                            'order_products.type'
                        )
                        ->groupBy('order_products.store_product_id')
                        ->orderBy('quantity', 'DESC')
                        ->get();

        $products->each(function ($product) use ($warehouse_id) {
            $product->current_stock = $product->storeProduct->getCurrentStock($warehouse_id);
        });

        $products_grouped =  OrderProductGroup::with('storeProduct.product')
                        ->join('orders', 'order_product_group.order_id', '=', 'orders.id')
                        ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                        ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '".$yesterday."'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_product_group.store_product_id', '=', 'receptions.store_product_id')
                        ->whereIn('orders.status', ['Alistado', 'In Progress'])
                        ->whereIn('order_product_group.fulfilment_status', ['Not Available', 'Missing'])
                        ->where('order_groups.warehouse_id', $warehouse_id)
                        ->select(
                            'order_product_group.store_product_id',
                            DB::raw('SUM(quantity) AS quantity'),
                            'receptions.quantity_received'
                        )
                        ->groupBy('order_product_group.store_product_id')
                        ->orderBy('quantity', 'DESC')
                        ->get();

        $products_grouped->each(function ($product) use ($warehouse_id, $products) {
            $product->current_stock = $product->storeProduct->getCurrentStock($warehouse_id);
            $products->push($product);
        });

        $products = $products->sortByDesc(function ($product) {
            return $product->quantity;
        });

        $data['products'] = $products;
        $data['city_id'] = $city_id;
        $data['warehouse_id'] = $warehouse_id;
        $response['html'] = View::make('admin.report.inventory.missing_picking_product.ajax.index-ajax', $data)->render();
        return Response::json($response);
    }

    public function getOrdersByMissingProductAjax()
    {
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $store_product_id = Input::get('store_product_id');

        $orders = Order::join('order_products', 'orders.id', '=', 'order_products.order_id')
                        ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                        ->leftJoin('routes', 'orders.route_id', '=', 'routes.id')
                        ->whereIn('orders.status', ['Alistado', 'In Progress'])
                        ->whereIn('order_products.fulfilment_status', ['Not Available', 'Missing'])
                        ->where('order_products.store_product_id', $store_product_id)
                        ->where('order_groups.warehouse_id', $warehouse_id)
                        ->select('orders.id', 'order_products.quantity', 'orders.planning_sequence', 'routes.route')
                        ->orderBy('order_products.quantity', 'DESC')
                        ->get();

        if (!$orders->count()) {
            $orders = Order::join('order_product_group', 'orders.id', '=', 'order_product_group.order_id')
                            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                            ->leftJoin('routes', 'orders.route_id', '=', 'routes.id')
                            ->whereIn('orders.status', ['Alistado', 'In Progress'])
                            ->whereIn('order_product_group.fulfilment_status', ['Not Available', 'Missing'])
                            ->where('order_product_group.store_product_id', $store_product_id)
                            ->where('order_groups.warehouse_id', $warehouse_id)
                            ->select(
                                'orders.id',
                                'order_product_group.quantity',
                                'orders.planning_sequence',
                                'routes.route'
                            )
                            ->orderBy('order_product_group.quantity', 'DESC')
                            ->get();
        }

        $data['orders'] = $orders;
        $response['html'] = View::make('admin.report.inventory.missing_picking_product.ajax.index-modal-ajax', $data)->render();

        return Response::json($response);
    }
}
