<?php

namespace admin\report\inventory;

use DB as DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use Order;
use Routes;
use Transporter;
use admin\AdminController;
use Request, Store;
use OrderProduct;
use OrderProductGroup;

class CommitedProducts extends AdminController
{
    public function index()
    {
        $data['title'] = 'Productos Comprometidos';
        $data['cities'] = $this->get_cities();
        $data['warehouses'] = $this->get_warehouses(Session::get('admin_city_id'));
        $date = Carbon::now();
        $date_1 = $date->format('d/m/Y');
        $date_2 = $date->addDay()->format('d/m/Y');
        $date_3 = $date->addDay()->format('d/m/Y');
        $date_4 = $date->addDay()->format('d/m/Y');
        $date_5 = $date->addDay()->format('d/m/Y');
        $data['date_1'] = $date_1;
        $data['date_2'] = $date_2;
        $data['date_3'] = $date_3;
        $data['date_4'] = $date_4;
        $data['date_5'] = $date_5;

        return View::make('admin.report.inventory.commited_products.index', $data);
    }

    public function getCommitedProductsAjax()
    {
        //return Response::json(['html' => '<p>REPORTE DESHABILITADO POR MANTENIMIENTO.</p>']);
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);
        $warehouse_id = Input::get('warehouse_id');
        $city_id = Input::get('city_id');
        $search = Input::get('search');
        $date = Carbon::now();
        $date_1 = $date->toDateString();
        $date_2 = $date->addDay()->toDateString();
        $date_3 = $date->addDay()->toDateString();
        $date_4 = $date->addDay()->toDateString();
        $date_5 = $date->addDay()->toDateString();

        $commited_products = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->join('order_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('orders', 'order_products.order_id', '=', 'orders.id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->where('order_products.fulfilment_status', 'Pending')
            ->where('order_groups.warehouse_id', $warehouse_id)
            ->whereIn('orders.status', ['Validation', 'Initiated', 'Enrutado', 'In Progress'])
            ->whereRaw("DATE(orders.delivery_date) BETWEEN '{$date_1}' AND '{$date_5}'")
            ->where(function ($where) use ($search) {
                $where->where('products.name', 'LIKE', '%' . $search . '%')
                    ->orWhere('products.id', 'LIKE', '%' . $search . '%')
                    ->orWhere('products.reference', 'LIKE', '%' . $search . '%')
                    ->orWhere('store_products.id', 'LIKE', '%' . $search . '%')
                    ->orWhere('store_products.provider_plu', 'LIKE', '%' . $search . '%');
            })->select('products.id',
                'products.reference',
                'products.image_small_url',
                'store_products.provider_plu',
                DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name, 
                    SUM(order_products.quantity) as total, 
                    (
                    SELECT
                        SUM(order_products.quantity)
                    FROM products AS in_products
                    INNER JOIN store_products ON in_products.id = store_products.product_id
                    INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                        AND store_product_warehouses.warehouse_id = {$warehouse_id}
                    INNER JOIN order_products ON store_products.id = order_products.store_product_id
                        AND order_products.fulfilment_status = 'Pending'
                    INNER JOIN orders ON order_products.order_id = orders.id
                    INNER JOIN order_groups ON orders.group_id = order_groups.id
                        AND order_groups.warehouse_id = {$warehouse_id}
                        AND orders.status IN ('Validation', 'Initiated', 'Enrutado', 'In Progress')
                        AND DATE(orders.delivery_date) = '{$date_1}'
                    WHERE in_products.id = products.id
                    GROUP BY store_products.id
                ) AS 'fecha_1',
                (
                    SELECT
                        SUM(order_products.quantity)
                    FROM products AS in_products
                    INNER JOIN store_products ON in_products.id = store_products.product_id
                    INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                    AND store_product_warehouses.warehouse_id = {$warehouse_id}
                    INNER JOIN order_products ON store_products.id = order_products.store_product_id
                    AND order_products.fulfilment_status = 'Pending'
                    INNER JOIN orders ON order_products.order_id = orders.id
                    INNER JOIN order_groups ON orders.group_id = order_groups.id
                        AND order_groups.warehouse_id = {$warehouse_id}
                        AND orders.status IN ('Validation', 'Initiated', 'Enrutado', 'In Progress')
                        AND DATE(orders.delivery_date) = '{$date_2}'
                    WHERE in_products.id = products.id
                    GROUP BY store_products.id
                ) AS 'fecha_2',
                (
                    SELECT
                        SUM(order_products.quantity)
                    FROM products AS in_products
                    INNER JOIN store_products ON in_products.id = store_products.product_id
                    INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                        AND store_product_warehouses.warehouse_id = {$warehouse_id}
                    INNER JOIN order_products ON store_products.id = order_products.store_product_id
                        AND order_products.fulfilment_status = 'Pending'
                    INNER JOIN orders ON order_products.order_id = orders.id
                    INNER JOIN order_groups ON orders.group_id = order_groups.id
                        AND order_groups.warehouse_id = {$warehouse_id}
                        AND orders.status IN ('Validation', 'Initiated', 'Enrutado', 'In Progress')
                        AND DATE(orders.delivery_date) = '{$date_3}'
                    WHERE in_products.id = products.id
                    GROUP BY store_products.id
                ) AS 'fecha_3',
                (
                    SELECT
                        SUM(order_products.quantity)
                    FROM products AS in_products
                    INNER JOIN store_products ON in_products.id = store_products.product_id
                    INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                        AND store_product_warehouses.warehouse_id = {$warehouse_id}
                    INNER JOIN order_products ON store_products.id = order_products.store_product_id
                        AND order_products.fulfilment_status = 'Pending'
                    INNER JOIN orders ON order_products.order_id = orders.id
                    INNER JOIN order_groups ON orders.group_id = order_groups.id
                        AND order_groups.warehouse_id = {$warehouse_id}
                        AND orders.status IN ('Validation', 'Initiated', 'Enrutado', 'In Progress')
                        AND DATE(orders.delivery_date) = '{$date_4}'
                    WHERE in_products.id = products.id
                    GROUP BY store_products.id
                ) AS 'fecha_4',
                (
                    SELECT
                        SUM(order_products.quantity)
                    FROM products AS in_products
                    INNER JOIN store_products ON in_products.id = store_products.product_id
                    INNER JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                        AND store_product_warehouses.warehouse_id = {$warehouse_id}
                    INNER JOIN order_products ON store_products.id = order_products.store_product_id
                        AND order_products.fulfilment_status = 'Pending'
                    INNER JOIN orders ON order_products.order_id = orders.id
                    INNER JOIN order_groups ON orders.group_id = order_groups.id
                        AND order_groups.warehouse_id = {$warehouse_id}
                        AND orders.status IN ('Validation', 'Initiated', 'Enrutado', 'In Progress')
                        AND DATE(orders.delivery_date) = '{$date_5}'
                    WHERE in_products.id = products.id
                    GROUP BY store_products.id
                ) AS 'fecha_5'"))
            ->groupBy('store_products.id')
            ->orderBy('total', 'DESC')
            ->paginate(30);

        $data['commited_products'] = $commited_products;
        $data['date_1'] = $date_1;
        $data['date_2'] = $date_2;
        $data['date_3'] = $date_3;
        $data['date_4'] = $date_4;
        $data['date_5'] = $date_5;

        $html = View::make('admin.report.inventory.commited_products.ajax.commited-products-ajax', $data)->render();

        return Response::json(['html' => $html]);
    }

    /**
     * Metodo que muestra los productos comprometidos para la siguiente jornada de picking
     *
     * @return \Illuminate\Contracts\View\View
     *
     */

    public function assortment()
    {
        if (!Request::ajax()) {
            $cities = $this->get_cities();
            $warehouses = $this->get_warehouses(Session::get('admin_city_id'));
            $stores = Store::getActiveStores(Session::get('admin_city_id'));

            $data = [
                'title' => 'Productos Comprometidos',
                'cities' => $cities,
                'warehouses' => $warehouses,
                'stores' => $stores,
            ];

            return View::make('admin.report.inventory.commited_products.assortment', $data);
        } else {
            $warehouseId = Input::get('warehouse_id');
            $products = \Warehouse::getCommittedStock($warehouseId, true);
            $response = $this->orderDataForAssortment($products);
            return View::make('admin.report.inventory.commited_products.assortment', ['products' => $response])
                ->renderSections()['products'];
        }
    }


    /**
     * Metodo para ordenar los productos comprometidos para la siguiente jornada de picking
     *
     * @param  $orders
     * @return array
     */

    public function orderDataForAssortment($orders)
    {
        $data = [];
        foreach ($orders as $product) {
            $name = $product->name . " " . $product->product_quantity . " " . $product->unit;
            if (!array_key_exists($name, $data)) {
                $data[$name]['warehouse_position'] = $product->warehouse_position;
                $data[$name]['expiration_date'] = $product->expiration_date;
                $data[$name]['warehouse_position'] = $product->warehouse_position;
                $data[$name]['picking_position'] = $product->picking_position;
                $data[$name]['picking_stock'] = $product->picking_stock;
                $data[$name]['warehouse_storage'] = $product->warehouse_storage;
                $data[$name]['amount_committed'] = $product->quantity;
                $data[$name]['current_stock'] = $product->current_stock;
                $data[$name]['image_small_url'] = $product->image_small_url;
                $data[$name]['maximum_picking_stock'] = $product->maximum_picking_stock;
                $data[$name]['reference'] = $product->reference;
            } else {
                $data[$name]['amount_committed'] += $product->quantity;
            }
        }
        return $data;
    }
}
