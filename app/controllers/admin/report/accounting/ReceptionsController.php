<?php

namespace admin\report\accounting;

use admin\AdminController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use ProviderOrderReception;
use Warehouse;

class ReceptionsController extends AdminController
{

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Reporte de recibos';
        $warehouses = Warehouse::all();
        return View::make('admin.report.accounting.receptions.index', compact('title', 'warehouses'));
    }

    public function exportReceptionReport()
    {
        ini_set('memory_limit', '8024M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $warehouseId = Input::get('warehouse_id');
        $month = Input::get('month');

        $validator = Validator::make(
            [
                'warehouse' => $warehouseId,
                'month' => $month
            ],
            [
                'warehouse' => 'required|exists:warehouses,id',
                'month' => 'required|date_format:Y-m'
            ]
        );

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $warehouse = Warehouse::findOrFail($warehouseId);
        $now = Carbon::now();
        $filename = "{$warehouse->warehouse} - {$now->toDateString()}";

        $query = "SELECT
            provider_order_receptions.id AS id_recibo,
            provider_order_receptions.status AS estado,
            provider_order_receptions.movement_consecutive_p,
            admin.id AS admin_id,
            admin.fullname,
            providers.name AS proveedor,
            provider_order_receptions.created_at
        FROM provider_order_receptions
        LEFT JOIN provider_order_reception_logs 
            ON provider_order_receptions.id = provider_order_reception_logs.reception_id
            AND provider_order_reception_logs.`type` LIKE '%Actualizado: Contabilizado%'
        LEFT JOIN admin ON provider_order_reception_logs.admin_id = admin.id
        INNER JOIN provider_orders ON provider_order_receptions.provider_order_id = provider_orders.id
        INNER JOIN providers ON provider_orders.provider_id = providers.id
        WHERE
            DATE(provider_order_receptions.created_at) BETWEEN '{$month}-01' AND '{$month}-31'
            AND provider_orders.warehouse_id = {$warehouseId}
            AND provider_order_receptions.status <> 'Cancelado';";

        return Excel::create($filename, function ($excel) use ($warehouseId, $month) {
            $receptions = ProviderOrderReception::where('provider_orders.warehouse_id', $warehouseId)
                ->whereRaw("DATE(provider_order_receptions.created_at) BETWEEN '{$month}-01' AND '{$month}-31'")
                ->where('provider_order_receptions.status', '<>', 'Cancelado')
                ->leftJoin('provider_order_reception_logs', function ($query) {
                    $query->on('provider_order_receptions.id', '=', 'provider_order_reception_logs.reception_id')
                        ->where('provider_order_reception_logs.type', 'like', '%Actualizado: Contabilizado%');
                })
                ->leftJoin('admin', 'provider_order_reception_logs.admin_id', '=', 'admin.id')
                ->join('provider_orders', 'provider_order_receptions.provider_order_id', '=', 'provider_orders.id')
                ->join('providers', 'provider_orders.provider_id', '=', 'providers.id')
                ->select(
                    'provider_order_receptions.id AS id_recibo',
                    'provider_order_receptions.status AS estado',
                    'provider_order_receptions.movement_consecutive_p AS consecutivo_P',
                    'admin.fullname AS nombre_completo',
                    'providers.name AS proveedor',
                    'provider_order_receptions.created_at AS fecha'
                );

            // Set the title
            $excel->setTitle('Reporte de recibos');

            // Chain the setters
            $excel->setCreator('Merqueo')
                ->setCompany('Merqueo');

            // Call them separately
            $excel->setDescription('Merqueo');

            $excel->sheet('Recibos', function ($sheet) use ($receptions) {
                $receptions->chunk(100, function ($rows) use ($sheet) {
                    foreach ($rows as $row) {
                        $array_data = $row->toArray();
                        $sheet->appendRow($array_data);
                    }
                });
            });
        })->export('csv');
    }
}
