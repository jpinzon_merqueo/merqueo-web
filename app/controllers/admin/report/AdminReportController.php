<?php
namespace admin\report;

use Illuminate\Support\Facades\View;
use admin\AdminController;

class AdminReportController extends AdminController
{
    public function index()
    {
        $data['title'] = 'Reportes';
        return View::make('admin.report.index', $data);
    }
}
