<?php

namespace admin;

use Illuminate\Database\QueryException;
use Request;
use View; 
use Input;
use Redirect;
use Session;
use DB; 
use Str;
use Validator;
use Carbon\Carbon; 
use City; 
use Product; 
use Response; 
use Config; 
use Provider; 
use ProviderContact;
use ProviderOrder; 
use ProviderOrderUser;
use Route; 
use ProviderOrderDetail; 
use ProviderOrderNote; 
use AdminLog; 
use Order; 
use Store; 
use Department; 
use Shelf; 
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Writer_Excel2007; 
use ProviderOrderReception; 
use ProviderOrderReceptionDetail;  
use Pdf; 
use ProductStockUpdate; 
use PHPExcel_Cell; 
use PHPExcel_Reader_Excel2007;
use Exception;  
use Event;  
use StoreProduct;
use StoreProductWarehouse; 
use WarehouseStorage; 
use InvestigationStock;

class AdminStockManagementController extends AdminController{

	/**
	* Obtener listado de actualizaciones de stock
	*/
	public function index_update_products_stock()
	{
	    if (!Request::ajax())
	    {
			$product = new Product();
			$products = $product->getProductStockStorage();
	        $stores = Store::where('is_hidden', 0)
	                    ->join('cities', 'stores.city_id', '=', 'cities.id')
	                    ->where('is_storage', 1)
	                    ->select('stores.*', 'cities.city AS city_name')
	                    ->get();
	         return View::make('admin.stock_management.index')
	                ->with('title', 'Actualizaciones de stock de alistamiento de productos')
	                ->with('stores', $stores)
	                ->with('cities', $this->get_cities())
	                ->with('products', $products);
	    }else{
	        $search = Input::has('s') ? Input::get('s') : '';
			//obtener productos a actualizar stock
			$product = new Product();
			$products = $product->getProductStockStorage(Input::get('warehouse_id'),Input::get('city_id'),$search);
	        return View::make('admin.stock_management.index')
	                    ->with('title', 'Actualizaciones de stock de alistamiento de productos')
	                    ->with('products', $products)
	                    ->renderSections()['content'];
	    }
	}

	/**
	* Actualiza stock de un producto
	*/
	public function update_stock_product()
	{
	    if(Input::has('store_product_id'))
	    {
	    	try{
	    	    DB::beginTransaction();
		        $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(Input::get('warehouse_id'), Input::get('store_product_id'));
                if ($storeProductWarehouse->storeProduct->product->type == 'Agrupado')
                    return Redirect::back()->with('error', 'Los productos agrupados no manejan stock.');
				if ( is_numeric(Input::get('quantity')) && Input::get('quantity') >= 0 ){	
					$quantity_stock_before = $storeProductWarehouse->picking_stock;
					if(Input::get('type') == 'Aumento de unidades')
						$storeProductWarehouse->picking_stock = $quantity_stock_after = $quantity_stock_before + Input::get('quantity');
					else $storeProductWarehouse->picking_stock = $quantity_stock_after = $quantity_stock_before - Input::get('quantity');
					$storeProductWarehouse->last_updated_stock_date = Carbon::now();
					$storeProductWarehouse->save();
					$productStockUpdate = new ProductStockUpdate();
					$productStockUpdate->admin_id = Session::get('admin_id');
					$productStockUpdate->store_product_id = Input::get('store_product_id');
					$productStockUpdate->quantity = Input::get('quantity');
					$productStockUpdate->quantity_stock_before = $quantity_stock_before;
					$productStockUpdate->quantity_stock_after = $quantity_stock_after;
					$productStockUpdate->type = Input::get('type');
					$productStockUpdate->reason = Input::get('comments');
					$productStockUpdate->warehouse_id = Input::get('warehouse_id');
					$productStockUpdate->save();
					//investigation
					$investigationStock = new InvestigationStock();
					$investigationStock->store_product_id = Input::get('store_product_id');
					$investigationStock->warehouse_id = Input::get('warehouse_id');
					$investigationStock->storage = "Alistamiento";
					$investigationStock->admin_id = Session::get('admin_id');
					$investigationStock->module = "Actualizacion Stock";
					$investigationStock->product_group = 0;
					$investigationStock->quantity = Input::get('type') == 'Aumento de unidades'? Input::get('quantity'):'-'.Input::get('quantity');
					$investigationStock->position = $storeProductWarehouse->storage_position;
					$investigationStock->height_position = $storeProductWarehouse->storage_height_position;
					$investigationStock->inventory_counting_position_id = 0;
					$investigationStock->entity_id = 0;
					$investigationStock->save();
					DB::commit();
					return Redirect::back()->with('success', 'Stock de producto actualizado con éxito.');
				}
				else{
					return Redirect::back()->with('error', 'Las unidades ingresadas para el producto con ID <b>'.Input::get('store_product_id').'</b> no son válidas.<br>');
				}
	    	}catch(Exception $e){
	    		return Redirect::back()->with('error', $e->message());
	    	}
	    }else
	        return Redirect::back()->with('error', 'Ocurrió un error al intentar actualizar el stock del producto.');
	}

	/**
	 * Obtener productos a actualizar stock
	 */
	public function search_products()
	{
	    if (!Request::ajax())
	    {
	        $products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
	        			->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
	                    ->join('stores', 'stores.id', '=', 'store_products.store_id')
	                    ->join('departments', 'departments.id', '=', 'store_products.department_id')
	                    ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
	                    ->where('store_product_warehouses.status', 1)
                        ->where('products.type', 'Simple')
						->select('products.*', 'store_products.*', 
						DB::raw('store_products.id AS store_product_id'), 
						DB::raw('departments.name AS department'))->limit(80)->get();
	         return View::make('admin.stock_management.search')
	                ->with('title', 'Actualización de stock de alistamiento')
	                ->with('cities', $this->get_cities())
	                ->with('products', $products);
	    }else{
			
	        $search = Input::has('s') ? Input::get('s') : '';
			//obtener productos a actualizar stock
			$product = new Product();
			$products = $product->getProductStockStorage(Input::get('warehouse_id'),Input::get('city_id'),$search,true);
	        return View::make('admin.stock_management.search')
	                    ->with('title', 'Actualización de stock de alistamiento')
	                    ->with('products', $products)
	                    ->renderSections()['content'];
	    }
	}

	/**
	 * Importa archivo de actualización de stock de productos
	 */
	public function import_stock_update()
	{
	    if (Input::hasFile('file'))
	    {
	        ini_set('memory_limit', '512M');
	        set_time_limit(0);
	        $extensionList = 'xlsx, xls';
	        $file = Input::file('file');
	        $warehouseId = Input::get('warehouse');
	        $lastDate = date('Y-m-d H:i:s');
	        $typeUpload = Input::get('type_upload');
	        if (!file_exists( $file->getRealPath()))
	            return Redirect::back()->with('error', 'No file uploaded');
	        $path = public_path(Config::get('app.download_directory'));
	        $extensionObj = explode(',', $extensionList);
	        foreach($extensionObj as $extension){
	            foreach(glob($path.'/*.'.$extension) as $fileTmp){
	                $fileTmp = pathinfo($fileTmp);
	                $files_by_extension[$extension][] = $fileTmp['filename'];
	            }
	        }
	        $filePath = Input::file('file')->getRealPath();
	        $currentDir = opendir($path);
	        $objReader = new PHPExcel_Reader_Excel2007();
	        $objPHPExcel = $objReader->load($filePath);
	        //abrir la ultima hoja que contiene la informacion
	        $sheetCount = $objPHPExcel->getSheetCount();
	        $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
	        //obtener el numero total de filas y columnas
	        $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
	        $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());
	        $storeProductIds = [];
	        try{
	        DB::beginTransaction();
	        for($i = 2; $i <= $rows; $i++)
	        {
	        	if($typeUpload == 'inventario'){
		            $storeProductId = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
		            $pickingStock = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
		            $minimumPickingStock = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
		            $maximumPickingStock = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $i)->getValue();
		            $storagePosition = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $i)->getValue();
		            $storageHeightPosition = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $i)->getValue();
	    		}
	    		if($typeUpload == 'compras'){
	    			$storeProductId = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
		            $minimumStock = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
		            $idealStock = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
	    		}
	    		if (in_array($storeProductId, $storeProductIds)){
	                DB::rollback();
	                return Redirect::back()->with('error', 'El producto #'.$storeProductId.' se encuentra duplicado.<br>');
	            }else{
	            	$storeProductIds[] = $storeProductId;
	            }
	            if (empty($storeProductId)){
	                DB::rollback();
	                return Redirect::back()->with('error', 'La fila #'.$i.' no tiene el ID del producto.<br>');
	            }
	            if (!$storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($warehouseId, $storeProductId)){
	                DB::rollback();
	                return Redirect::back()->with('error', 'El producto con ID <b>'.$storeProductId.'</b> no existe.<br>');
	            }
                if ($storeProductWarehouse->storeProduct->product->type == 'Agrupado') {
                    DB::rollback();
                    return Redirect::back()->with('error', 'El producto con ID <b>'.$storeProductId.'</b> no maneja stock.<br>');
                }
            	$storage = $storeProductWarehouse->storeProduct->storage;
            	$storage_type = $storage == 'Seco' ? $storage : 'Frío';
				//investigacion
				$investigationStock = new InvestigationStock();
				$investigationStock->admin_id = Session::get('admin_id');
	            if($typeUpload == 'inventario'){
		            if ( $pickingStock !== '' && !is_null($pickingStock) ) {
			            if ( is_numeric($pickingStock) && $pickingStock >= 0 ) {
		            		$stockBefore = $storeProductWarehouse->picking_stock;
			            	$stockAfter = $storeProductWarehouse->picking_stock = $pickingStock;
		            		$productStockUpdate = new ProductStockUpdate;
		            		$productStockUpdate->admin_id = Session::get('admin_id');
		            		$productStockUpdate->store_product_id = $storeProductWarehouse->store_product_id;
		            		$productStockUpdate->type = ( $stockAfter > $stockBefore ? 'Aumento de unidades' : 'Disminución de unidades' );
		            		$productStockUpdate->quantity = ( $stockAfter > $stockBefore ? $stockAfter - $stockBefore : $stockBefore - $stockAfter );
		            		$productStockUpdate->quantity_stock_before = $stockBefore;
		            		$productStockUpdate->quantity_stock_after = $stockAfter;
		            		$productStockUpdate->reason = 'Ajuste por importación de archivo.';
		            		$productStockUpdate->warehouse_id = $warehouseId;
		            		$productStockUpdate->save();
							$storeProductWarehouse->last_updated_stock_date = $lastDate;
							$investigationStock->quantity = $stockAfter - $stockBefore;
							$investigationStock->store_product_id = $storeProductWarehouse->store_product_id;
							$investigationStock->warehouse_id = $warehouseId;
							$investigationStock->storage = "Alistamiento";
							$investigationStock->module = "Actualizacion Stock";
							$investigationStock->product_group = 0;
							$investigationStock->inventory_counting_position_id = 0;
							$investigationStock->entity_id = 0;
							$investigationStock->position = $storeProductWarehouse->storage_position;
							$investigationStock->height_position = $storeProductWarehouse->storage_height_position;
			            }else{
			            	DB::rollback();
			            	return Redirect::back()->with('error', 'El producto con ID <b>'.$storeProductId.'</b> no tiene un stock válido.<br>');
			            }
		            }
		            if( !is_null($storageHeightPosition) && $storageHeightPosition != '' && is_numeric($storagePosition) && !is_null($storagePosition)  ){
		            	$validatePosition = WarehouseStorage::validatePosition($warehouseId, 'picking', $storage_type, $storagePosition, $storageHeightPosition);
		            	if($validatePosition['status']){
		                	$storeProductWarehouse->storage_position = $storagePosition;
		                	$storeProductWarehouse->storage_height_position = $storageHeightPosition;
		            	}else{
		            		DB::rollback();
			            	return Redirect::back()->with('error', 'El producto con ID <b>'.$storeProductId.'</b>. '.$validatePosition['message']);
		            	}
		            }
		            if ( is_numeric($storagePosition) && !is_null($storagePosition) && is_null($storageHeightPosition)) {
		            	$validatePosition = WarehouseStorage::validatePosition($warehouseId, 'picking', $storage_type, $storagePosition, $storeProductWarehouse->storage_height_position);
		            	if($validatePosition['status']){
							$storeProductWarehouse->storage_position = $storagePosition;
							$investigationStock->position = $storeProductWarehouse->storage_position;
		            	}else{
		            		DB::rollback();
			            	return Redirect::back()->with('error', 'El producto con ID <b>'.$storeProductId.'</b>. '.$validatePosition['message']);
		            	}
		            }
		            if ( !is_null($storageHeightPosition) && $storageHeightPosition != '' && is_null($storagePosition)) {
		            	$validatePosition = WarehouseStorage::validatePosition($warehouseId, 'picking', $storage_type, $storeProductWarehouse->storage_position, $storageHeightPosition);
		            	if($validatePosition['status']){
							$storeProductWarehouse->storage_height_position = $storageHeightPosition;
							$investigationStock->height_position = $storeProductWarehouse->storage_height_position;
		            	}else{
		            		DB::rollback();
			            	return Redirect::back()->with('error', 'El producto con ID <b>'.$storeProductId.'</b>. '.$validatePosition['message']);
		            	}
		            }
		            if ( !is_null($minimumPickingStock) && is_numeric($minimumPickingStock) && $minimumPickingStock < 0 ) {
		            	DB::rollback();
		            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock mínimo de alistamiento es menor a 0.');
		            }
		            if ( !is_null($maximumPickingStock) && is_numeric($maximumPickingStock) && $maximumPickingStock < 0 ) {
		            	DB::rollback();
		            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock máximo de alistamiento es menor a 0.');
		            }
		            if(!is_null($minimumPickingStock) && is_numeric($minimumPickingStock) && !is_null($maximumPickingStock) && is_numeric($maximumPickingStock) ){
		            	if( $minimumPickingStock < $maximumPickingStock ){
		            		$storeProductWarehouse->minimum_picking_stock = $minimumPickingStock;
		            		$storeProductWarehouse->maximum_picking_stock = $maximumPickingStock;
		            	}else{
		            		DB::rollback();
			            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock mínimo de alistamiento es mayor al stock máximo de alistamiento.');
		            	}
		            }else{
			            if ( !is_null($minimumPickingStock) && is_numeric($minimumPickingStock) ) {
			            	if( $minimumPickingStock < $storeProductWarehouse->maximum_picking_stock ){
			            		$storeProductWarehouse->minimum_picking_stock = $minimumPickingStock;
			            	}else{
			            		DB::rollback();
				            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock mínimo de alistamiento es mayor al stock máximo de alistamiento.');
			            	}
			            }
			            if ( !is_null($maximumPickingStock) && is_numeric($maximumPickingStock)  ) {
			            	if( $maximumPickingStock > $storeProductWarehouse->minimum_picking_stock ){
			                	$storeProductWarehouse->maximum_picking_stock = $maximumPickingStock;
			            	}else{
			            		DB::rollback();
				            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock mínimo de alistamiento es mayor al stock máximo de alistamiento.');
			            	}
			            }
					}
	            }
	            if($typeUpload == 'compras'){
	            	if( !is_null($idealStock) && $idealStock < 0 ){
	            		DB::rollback();
		            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock ideal es menor a 0.');
	            	}
	            	if( !is_null($minimumStock) && $minimumStock < 0 ){
	            		DB::rollback();
		            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock minimo es menor a 0.');
	            	}
	            	if( !is_null($idealStock) && is_numeric($idealStock)   && !is_null($minimumStock) && is_numeric($minimumStock) ){
	            		if($idealStock >= $minimumStock){
	            			$storeProductWarehouse->ideal_stock = $idealStock;
	            			$storeProductWarehouse->minimum_stock = $minimumStock;
	            		}else{
	            			DB::rollback();
			            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock ideal es menor a stock mínimo.');
	            		}
	            	}else{

			            if ( !is_null($idealStock) && is_numeric($idealStock) ) {
			            	if( $idealStock >= $storeProductWarehouse->minimum_stock ){
			                	$storeProductWarehouse->ideal_stock = $idealStock;
			            	}else{
			            		DB::rollback();
				            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock ideal es menor a stock mínimo.');
			            	}
			            }
			            if ( !is_null($minimumStock) && is_numeric($minimumStock) ) {
			            	if($storeProductWarehouse->ideal_stock >= $minimumStock){
			                	$storeProductWarehouse->minimum_stock = $minimumStock;
			            	}else{
			            		DB::rollback();
				            	return Redirect::back()->with('error', 'Para el producto con ID <b>'.$storeProductId.'</b>. el stock ideal es menor a stock mínimo.');
			            	}
			            }
	            	}
	            }
				if(!is_null($investigationStock->quantity))
					$investigationStock->save();
	            $storeProductWarehouse->save();
	        }
	        $this->admin_log('products', null, 'import_update_stock');
	        DB::commit();
	        return Redirect::back()->with('success', 'Se actualizaron los stocks de los productos.');
            } catch (QueryException $e) {
	            DB::rollBack();
	            if ($e->getCode() == '40001') {
	                \ErrorLog::add($e, 513);
	                return Redirect::back()->with('error', 'Otro proceso se está ejecutando sobre estos productos,
	                espera un momento para volver a intentarlo.');
	            }
	            \ErrorLog::add($e);
	            return Redirect::back()->with('error',
                    'Ocurrió un error al intentar actualizar el stock del producto.');
	        } catch (\Exception $e) {
	            DB::rollBack();
                \ErrorLog::add($e);
	            return Redirect::back()->with('error',
                    'Ocurrió un error al intentar actualizar el stock del producto.');
	        }

        }
	}
}

