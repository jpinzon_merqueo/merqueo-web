<?php
namespace admin;

use Config, View, ShopperMesh, ShopperMeshDetail, Response, Request, Input, Redirect, Session, Carbon\Carbon,Shopper, TypificationsList,
ShopperMeshDetailTypification, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007, ShopperZone;

class AdminShopperMeshController extends AdminController
{
	/**
	 * Grilla de mallas importadas
	 */
	public function index()
	{
		if ( !Request::ajax() ) {
			$shopper_mesh = ShopperMesh::where('status', 1)->get();

			$data = [
				'title' => 'Mallas de Shoppers',
				'shopper_mesh' => $shopper_mesh
			];
			return View::make('admin.shoppers.mesh.index', $data);
		}else{
			$shopper_mesh = ShopperMesh::where('shopper_meshes.status', 1)
									->join('admin', 'admin.id', '=', 'shopper_meshes.admin_id')
									->select('admin.fullname', 'shopper_meshes.*')
									->orderBy('shopper_meshes.created_at', 'DESC')
									->get();
			$data = [
				'shopper_mesh' => $shopper_mesh
			];
			$view = View::make('admin.shoppers.mesh.index', $data)->renderSections();

			return $view['mesh'];
		}
	}

	/**
	 * Grilla detalle de mallas
	 */
	public function mesh_detail($id)
	{
		if ( !Request::ajax() ) {
			$shopper_mesh = ShopperMesh::find($id);
			$data = [
				'title' => 'Detalle de Mallas',
				'mesh_id' => $id,
				'shopper_mesh' => $shopper_mesh
			];

			return View::make('admin.shoppers.mesh.mesh_detail', $data);
		}else{
			$shopper_details = ShopperMeshDetail::where('mesh_id', $id)
									->join('shoppers', 'shopper_mesh_details.shopper_id', '=', 'shoppers.id')
									->select('shopper_mesh_details.*', 'shoppers.first_name', 'shoppers.last_name')
									->get();
			$data = [
				'shopper_details' => $shopper_details,
				'mesh_id' => $id
			];

			$view = View::make('admin.shoppers.mesh.mesh_detail', $data)->renderSections();
			return $view['mesh'];
		}
	}

	/**
	 * Detalle de la malla de shopper
	 * @param  $id  identificador de la malla
	 * @param  $id_detail id del detalle
	 */
	public function mesh_shopper_detail($id, $id_detail)
	{
		$shopper_mesh = ShopperMesh::find($id);
		$shopper_mesh_detail = ShopperMeshDetail::where('shopper_mesh_details.id', $id_detail)
								->join('shoppers', 'shopper_mesh_details.shopper_id', '=', 'shoppers.id')
								->select('shopper_mesh_details.*', 'shoppers.first_name', 'shoppers.last_name')
								->first();

		$comments = ShopperMeshDetailTypification::where('shopper_mesh_detail_typifications.mesh_detail_id', $id_detail)
					->join('typifications_list', 'shopper_mesh_detail_typifications.typification_id', '=','typifications_list.id')
					->select('shopper_mesh_detail_typifications.*', 'typifications_list.type')
					->get();

		$types = TypificationsList::where('status', 1)->get();

		$data = [
			'title' => 'Detalle de Shopper',
			'id' => $id,
			'id_detail' => $id_detail,
			'shopper' => $shopper_mesh_detail,
			'shopper_mesh' => $shopper_mesh,
			'comments' => $comments,
			'types' => $types
		];

		return View::make('admin.shoppers.mesh.mesh_detail_form', $data);
	}

	public function mesh_shopper_detail_update($id, $id_detail)
	{
		$date = Input::get('date', null);
		$date = Carbon::createFromFormat('d/m/Y', $date)->toDateString();
		// dd($date);
		$entrance_time = Input::get('entrance_time', null);
		$departure_time = Input::get('departure_time', null);
		$shopper_mesh_detail = ShopperMeshDetail::where('shopper_mesh_details.id', $id_detail)->first();
		$shopper_mesh_detail->date = $date;
		$shopper_mesh_detail->entrance_time = $entrance_time;
		$shopper_mesh_detail->departure_time = $departure_time;
		try {
			$shopper_mesh_detail->save();
		} catch (Exception $e) {
			return Redirect::back()->with('error', 'Error al actualizar el detalle');
		}
		return Redirect::back()->with('success', 'Se ha actualizado el detalle.');
	}

	/**
	 * Funcion para obtener los comentarios
	 * @param  $id        malla
	 * @param  $id_detail detalle
	 */
	public function comments($id, $id_detail)
	{
		$comments = ShopperMeshDetailTypification::where('shopper_mesh_detail_typifications.mesh_detail_id', $id_detail)
					->join('typifications_list', 'shopper_mesh_detail_typifications.typification_id', '=','typifications_list.id')
					->join('admin', 'shopper_mesh_detail_typifications.admin_id', '=', 'admin.id')
					->select('shopper_mesh_detail_typifications.*', 'typifications_list.type', 'admin.fullname')
					->get();

		$data = [
			'comments' => $comments
		];

		$view = View::make('admin.shoppers.mesh.mesh_detail_form', $data)->renderSections();
		return $view['comments'];
	}

	/**
	 * Función para guardar comentarios
	 * @param  $id        malla
	 * @param  $id_detail detalle
	 */
	public function save_comment($id, $id_detail)
	{
		$type = Input::get('type', null);
		$comment_input = Input::get('comment', null);

		$comment = new ShopperMeshDetailTypification;
		$comment->admin_id = Session::get('admin_id');
		$comment->mesh_detail_id = $id_detail;
		$comment->typification_id = $type;
		$comment->comment = $comment_input;

		try {
			$comment->save();
		} catch (Exception $e) {
			return Response::json(['error' => 'No se ha guardado el comentario.']);
		}
		$html = $this->comments($id, $id_detail);
		return Response::json(['success' => 'Se ha guardado el comentario.', 'html' => $html]);
	}

	/**
	 * Formulario para cargar la malla
	 */
	public function mesh_loader()
	{
		if ( Request::method() == 'GET' ) {
			$data = [
				'title' => 'Formulario para importar mallas'
			];
			return View::make('admin.shoppers.mesh.mesh_form', $data);
		}else{
			if ( $file = Input::file('mesh_file') ) {
				$file = Input::file('mesh_file')->getRealPath();
				$shopper_mesh = new ShopperMesh;
				$shopper_mesh->admin_id = Session::get('admin_id');
				$shopper_mesh->title = Input::get('title');
				$start_date = Carbon::createFromFormat('d/m/Y', Input::get('start_date'))->toDateString();
				$end_date = Carbon::createFromFormat('d/m/Y', Input::get('end_date'))->toDateString();
				$shopper_mesh->start_date = $start_date;
				$shopper_mesh->end_date = $end_date;
				$shopper_mesh->status = 1;
				$shopper_mesh->save();
				$response = $this->import_mesh($file, $shopper_mesh->id);

				if ( $response ) {
					return Redirect::route('adminShopperMesh.index')->with('success', 'Archivo correctamente importado');
				}

				return View::make('admin.shoppers.mesh.mesh_form', $data);
			}else{
				return Redirect::back()->with('error', 'No se ha cargado el archivo.');
			}
		}
	}

	/**
	 * Funcion para importar el archivo
	 * @param  $file path del archivo
	 * @param  $shopper_mesh_id id de la cabecera de la malla
	 */
	private function import_mesh($file, $shopper_mesh_id)
	{
		$cont = 0;
		$row_number = 1;
		$text = fopen($file, 'r');
		$array_elements = [];
		fgets($text, 4096);
		while (!feof($text))
		{
		    $refresh_cache = false;
		    $row_number++;

		    $row = trim(fgets($text, 4096));
		    if ($row != '')
		    {
		        $data = explode(';', $row);
		        if (count($data) == 7){
	        		$mesh_details = new ShopperMeshDetail;
	        		$mesh_details->mesh_id = $shopper_mesh_id;
	        		$mesh_details->key = $data[0];
	        		$shopper_obj = Shopper::where('identity_number', $data[1])->first();
	        		if (is_null($shopper_obj)) {
	        			continue;
	        		}
	        		$mesh_details->shopper_id = $shopper_obj->id;

	        		$mesh_details->date = Carbon::createFromFormat('d/m/Y', $data[2])->toDateString();
	        		$mesh_details->entrance_time = $data[3];
	        		$mesh_details->departure_time = $data[4];
	        		$mesh_details->save();

	        		$type = TypificationsList::where('type', $data[5])->first();
	        		$comment = 	new ShopperMeshDetailTypification;
	        		$comment->admin_id = Session::get('admin_id');
	        		$comment->mesh_detail_id = $mesh_details->id;
	        		$comment->typification_id = $type->id;
	        		$comment->comment = $data[6];
	        		$comment->save();
		        }
		    }
		}
		return true;
	}

	public function export($id)
	{
		$titles = [
			'Llave',
			'Nombre del shopper',
			'Cédula',
			'Zonas',
			'Fecha',
			'Hora de entrada',
			'Hora de salida',
			'Novedad',
			'Comentarios'
		];

		$shoppers = ShopperMeshDetailTypification::join('shopper_mesh_details AS smd', 'shopper_mesh_detail_typifications.mesh_detail_id', '=', 'smd.id')
						->join('shopper_meshes AS sm', 'smd.mesh_id', '=', 'sm.id')
						->join('shoppers AS sh', 'smd.shopper_id', '=', 'sh.id')
						->join('typifications_list AS tl', 'shopper_mesh_detail_typifications.typification_id', '=', 'tl.id')
						->where('sm.id', $id)
						->select('smd.key', 'sh.first_name', 'sh.last_name', 'sh.identity_number', 'smd.date', 'smd.entrance_time', 'smd.departure_time', 'tl.type', 'shopper_mesh_detail_typifications.comment', 'sh.id' )
						->orderBy('sh.id', 'ASC')
						->get()->toArray();
		if ( count($shoppers) ) {
			foreach ($shoppers as $key => &$shopper) {
				$shopper_zones = ShopperZone::join('zones AS z', 'shopper_zones.zone_id', '=', 'z.id')->where('shopper_id', $shopper['id'])->lists('z.name');
				$shopper['zones'] = implode(',', $shopper_zones);
			}
			// dd($shoppers);
			$path = public_path(Config::get('app.download_directory'));
			if ($gestor = opendir($path)){
			    while (false !== ($file = readdir($gestor))){
			        if (strstr($file, Session::get('admin_username')))
			            remove_file($path.$file);
			    }
			}

			$objPHPExcel = new PHPExcel();
			//propiedades
			$objPHPExcel->getProperties()->setCreator('Merqueo');
			$objPHPExcel->getProperties()->setTitle('Malla');
			$objPHPExcel->getProperties()->setSubject('Malla');
			$objPHPExcel->setActiveSheetIndex(0);

            //estilos
			$style_header = array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb'=>'5178A5'),
					),
				'font' => array(
					'color' => array('rgb'=>'FFFFFF'),
					'bold' => true,
					)
				);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($style_header);

			//titulos
			$i = 0;
			foreach ($titles as $title) {
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $title);
				$i++;
			}

			$j = 2;
			foreach($shoppers as $shopper){
				$i = 0;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['key']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['first_name'].' '.$shopper['last_name']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['identity_number']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['zones']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, format_date('normal', $shopper['date']));
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['entrance_time']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['departure_time']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['type']);
				$i++;
				$objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $shopper['comment']);
				$j++;
			}

			 //crear archivo
			$objPHPExcel->getActiveSheet()->setTitle('Pedidos');
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
			$objWriter->save(public_path(Config::get('app.download_directory')).$filename);
			$url = web_url().'/'.Config::get('app.download_directory').$filename;
			// dd($objWriter);
			return Response::download(public_path(Config::get('app.download_directory')).$filename);
		}


		return Redirect::back()->with('error', 'No se encontraron registros para exportar');
	}
}
