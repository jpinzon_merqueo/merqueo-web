<?php
namespace admin;

use Request, View, Input, Route, Redirect, Session, DB, Config, DateTime, Response, Validator, Order, OrderProduct, Shopper, User, CampaignLog,
UserAddress, Coupon, UserCoupon, Store, Product, OrderNoteSms, OrderLog, OrderGroup, City, Blacklist;

class AdminBlacklistController extends AdminController
{
	/**
	 * Grilla de blacklist
	 */
	public function index()
	{
		if (!Request::ajax()){
			$blacklists = Blacklist::orderBy('blacklist.created_at', 'DESC')
									->join('admin', 'blacklist.admin_id', '=','admin.id')
									->leftJoin('cities', 'blacklist.city_id', '=', 'cities.id')
									->get();
			$cities = City::all();

		    $shoppers = Shopper::where('status', 1);
		    $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1);
		    if (Session::get('admin_designation') != 'Super Admin'){
		        $shoppers->where('city_id', Session::get('admin_city_id'));
		        $stores->where('city_id', Session::get('admin_city_id'));
		    }
		    $shoppers = $shoppers->orderBy('first_name')->orderBy('last_name')->get();
		    $stores = $stores->get();

		    $data = [
				'title'      => 'Blacklist',
				'blacklists' => $blacklists,
				'cities'     => $cities
		    ];

			return View::make('admin.reports.blacklist.index', $data);
		}else{
			$search = Input::has('search') ? Input::get('search') : '';
			$type   = Input::has('type') ? Input::get('type') : '';
			$city 	= Input::has('city') ? Input::get('city') : '';
			$bin 	= Input::has('bin') ? Input::get('bin') : '';
			$four 	= Input::has('four') ? Input::get('four') : '';
			$year 	= Input::has('year') ? Input::get('year') : '';
			$month 	= Input::has('month') ? Input::get('month') : '';

			$blacklists = Blacklist::orderBy('blacklist.created_at', 'DESC')
									->join('admin', 'blacklist.admin_id', '=','admin.id')
									->leftJoin('cities', 'blacklist.city_id', '=', 'cities.id');

			if ( !empty($search) ) {
				$blacklists->where('blacklist.value','like', '%'.trim($search).'%');
			}
			if ( !empty($type) ) {
				$blacklists->where('blacklist.type', trim($type));
			}
			if ( !empty($city) ) {
				$blacklists->where('blacklist.city_id', trim($city));
			}
			if ( !empty($bin) ) {
				$blacklists->where('blacklist.cc_bin', trim($bin));
			}
			if ( !empty($four) ) {
				$blacklists->where('blacklist.cc_last_four', trim($four));
			}
			if ( !empty($year) ) {
				$blacklists->where('blacklist.cc_expiration_year', trim($year));
			}
			if ( !empty($month) ) {
				$blacklists->where('blacklist.cc_expiration_month', trim($month));
			}

			$blacklists->select(
						'blacklist.id',
						'blacklist.type',
						'blacklist.value',
						'cities.city',
						'blacklist.cc_bin',
						'blacklist.cc_last_four',
						'blacklist.cc_expiration_year',
						'blacklist.cc_expiration_month',
						'blacklist.order_id',
						'blacklist.comment',
						'admin.fullname',
						'blacklist.created_at'
					);
			$blacklists = $blacklists->limit(80)->orderBy('id', 'desc')->get();

			$data = [
				'blacklists' => $blacklists
			];

			return View::make('admin.reports.blacklist.index', $data)
			            ->renderSections()['content'];
		}
	}

    /**
     * Editar item
     */
	public function edit($id = null)
	{
		$cities = City::all();

		$data = [
			'title'  => 'Blacklist',
			'cities' => $cities
		];
		if ( !empty($id) ) {
			$blacklist = Blacklist::find((int)$id);
			$data['blacklist'] = $blacklist;
		}

		return View::make('admin.reports.blacklist.form', $data);
	}

    /**
     * Guardar item
     */
	public function save()
	{
		$type = Input::get('type');
		$city = null;
		$bin  = null;
		$last = null;
		$year = null;
		$month = null;
		$value = null;
		$order_id = null;
		$comment = null;

		if (Input::has('direccion')) {
			$value = Input::get('direccion');
			$city = Input::get('city');
		}
		if(Input::has('celular'))
			$value = Input::get('celular');
		if(Input::has('email'))
			$value = Input::get('email');
		if (Input::has('bin'))
			$bin = Input::get('bin');
		if (Input::has('last'))
			$last = Input::get('last');
		if (Input::has('year'))
			$year = Input::get('year');
		if (Input::has('month'))
			$month = Input::get('month');
		if (Input::has('order'))
			$order_id = Input::get('order');
		if(Input::has('comment'))
			$comment = Input::get('comment');
		$error = 0;
		switch ( $type ) {
			case 'Dirección':
				$item = Blacklist::where('type', $type)->where('city_id', $city)->where('value' , $value)->first();
				if (!empty($item)) $error = 1;
			break;
			case 'Celular':
				$item = Blacklist::where('type', $type)->where('value', $value)->first();
				if (!empty($item)) $error = 1;
			break;
			case 'Email':
				$item = Blacklist::where('type', $type)->where('value', $value)->first();
                if (!empty($item)) $error = 1;
            break;
			case 'Tarjeta de crédito':
				$item = Blacklist::where('type', $type)
									->where('cc_bin', $bin)
									->where('cc_last_four', $last)
									->where('cc_expiration_year', $year)
									->where('cc_expiration_month', $month)
									->first();
				if ( !empty($item) ) {
					$error = 1;
				}
			break;

			default:
				$error = 0;
			break;
		}

		if ($error)
			return Redirect::route('adminBlacklist.index')->with('error', 'Ha ocurrido un error al guardar el Item. El item ya está en la base de datos.');

		$blacklist = new Blacklist;
		$blacklist->type = trim($type);
		$blacklist->city_id = trim($city);
		$blacklist->admin_id = trim(Session::get('admin_id'));
		$blacklist->value = trim($value);
		$blacklist->order_id = trim($order_id);
		$blacklist->cc_bin = trim($bin);
		$blacklist->cc_last_four = trim($last);
		$blacklist->cc_expiration_year = trim($year);
		$blacklist->cc_expiration_month = trim($month);
		$blacklist->comment = trim($comment);
		$blacklist->save();
        $this->admin_log('blacklist', $blacklist->id, 'insert');

		return Redirect::route('adminBlacklist.index')->with('success', 'Item guardado exitosamente.');
	}

    /**
     * Eliminar item
     */
	public function delete($id = null)
	{
		if ( !empty($id) ) {
			$blacklist = Blacklist::find($id);
			if ( !empty($blacklist) ) {
				$blacklist->delete();
                $this->admin_log('blacklist', $blacklist->id, 'delete');

				return Redirect::back()->with('success', 'Se ha borrado el item.');
			}
			return Redirect::back()->with('error', 'No se ha encontrado el item.');
		}
		return Redirect::back()->with('error', 'No se ha enviado el id del item.');
	}
}
