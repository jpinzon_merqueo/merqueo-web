<?php

namespace admin\inventory\reports;

use admin\AdminController;
use Carbon\Carbon;
use exceptions\MerqueoException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Product;
use Redirect;
use Request;
use Response;
use Validator;
use Store;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;
use InvestigationStock;
use City;
use StoreProduct;
use StoreProductWarehouse;
use ErrorLog;

class StockReportController extends AdminController
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Reportes';
		return View::make('admin.inventory.reports.index', $data);
	}

	/**
	 * Muestra ventana inicial del listado de productos próximos a vencerse
	 *
	 * @return View
	 */
	public function reportProductExpired()
	{
		$title = 'Reporte de productos próximos a vencer';
		$cities = $this->get_cities();
		$warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->get();
		return View::make('admin.inventory.reports.report_product_expired', compact(
			'title',
			'cities',
			'warehouses'
		));
	}

	/**
	 * Obtiene las bodegas por ciudad.
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getWarehousesAjax()
	{
		$cityId = Input::get('cityId');
		$warehouses = Warehouse::where('city_id', $cityId)->get();
		if (count($warehouses) < 1) {
			return Response::json('No se encontraron bodegas de esta ciudad.', 422);
		}
		return Response::json($warehouses, 200);
	}

	/**
	 * Obtiene el listado de productos próximos a vencerse
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function reportProductExpiredAjax()
	{
		try {
			$input = Input::all();
			$validator = Validator::make($input, [
				'warehouseId' => 'required|exists:warehouses,id'
			]);
			if ($validator->fails()) {
				return Response::json($validator->errors()->all(), 422);
			}
			$warehouseId = Input::get('warehouseId');
			$dateStart = Carbon::now();
			$dateEnd = Carbon::now()->addDay(30);
			$storeProducts = WarehouseStorage::with(['storeProduct', 'storeProduct.product'])
				->where('warehouse_id', $warehouseId)
				->whereBetween(DB::raw('DATE(expiration_date)'), [$dateStart, $dateEnd])
				->whereNotNull('expiration_date')
				->orderBy('expiration_date', 'ASC')
				->get();
			return Response::json($storeProducts, 200);
		} catch (MerqueoException $e) {
			return Response::json($e->getMessage(), 422);
		}
	}

	/**
	 * Muestra ventana inicial del reporte de productos apagados con stock
	 */
	public function reportProductDisabled()
	{
		$title = 'Reporte de productos apagados con stock';
		$cities = $this->get_cities();
		$warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->get();
		return View::make('admin.inventory.reports.report_product_disabled', compact(
			'title',
			'cities',
			'warehouses'
		));
	}

	/**
	 * Obtiene el reporte de productos que están inactivos pero que tienen stock
	 * 
	 */
	public function reportProductDisabledAjax()
	{
		try {
			$warehouseId = Input::get('warehouseId') != null ? Input::get('warehouseId') : Session::get('admin_warehouse_id');
			$storeProducts = $this->getStockProducts($warehouseId, false);
			return Response::json($storeProducts, 200);
		} catch (Exception $e) {
			return Response::json($e->getMessage(), 422);
		}
	}

	/**
	 * Muestra la ventana inicial del reporte de productos activos sin stock
	 */
	public function ReportProductEnabled()
	{
		$title = 'Reporte de productos activos sin stock';
		$cities = $this->get_cities();
		$warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->get();
		return View::make('admin.inventory.reports.report_product_enabled', compact(
			'title',
			'cities',
			'warehouses'
		));
	}

	/**
	 * Obtiene los productos activos que no tienen stock
	 */
	public function reportProductEnabledAjax()
	{
		try {
			$warehouseId = Input::get('warehouseId') != null ? Input::get('warehouseId') : Session::get('admin_warehouse_id');
			$storeProducts = $this->getStockProducts($warehouseId, true);
			return Response::json($storeProducts, 200);
		} catch (Exception $e) {
			return Response::json($e->getMessage(), 422);
		}
	}

	/**
	 * Exportar a excel el reporte de productos apagados con stock
	 */
	public function reportProductDisabledExport()
	{
		try {
			ini_set('memory_limit', '8024M');
			set_time_limit(0);
			ini_set("max_execution_time", -1);

			$input = Input::all();
			$validator = Validator::make($input, [
				'warehouseId' => 'required|exists:warehouses,id'
			]);
			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator);
			}
			$warehouseId = Input::get('warehouseId');
			$warehouse = Warehouse::findOrFail($warehouseId);
			$now = Carbon::now();
			$filename = "{$warehouse->warehouse} - {$now->toDateString()}";

			return Excel::create($filename, function ($excel) use ($warehouseId) {
				$storeProducts = $this->getStockProducts($warehouseId, false);
				$excel->setTitle('Reporte de apagados con stock');
				$excel->setCreator('Merqueo')
					->setCompany('Merqueo');
				$excel->setDescription('Merqueo');
				$excel->sheet('Productos', function ($sheet) use ($storeProducts) {
					$sheet->cell('A1', function ($cell) {
						$cell->setValue('Id');
					});
					$sheet->cell('B1', function ($cell) {
						$cell->setValue('Referencia');
					});
					$sheet->cell('C1', function ($cell) {
						$cell->setValue('Producto');
					});
					$sheet->cell('D1', function ($cell) {
						$cell->setValue('Posición de almacenamiento');
					});
					$sheet->cell('E1', function ($cell) {
						$cell->setValue('Cantidad almacenada');
					});
					$sheet->cell('F1', function ($cell) {
						$cell->setValue('Posición de alistamiento');
					});
					$sheet->cell('G1', function ($cell) {
						$cell->setValue('Cantidad en alistamiento');
					});
					$sheet->cell('H1', function ($cell) {
						$cell->setValue('Cantidad en recibo');
					});
					$sheet->cell('I1', function ($cell) {
						$cell->setValue('Cantidad en devolución');
					});
					$sheet->cell('J1', function ($cell) {
						$cell->setValue('Comprometido');
					});
					$sheet->cells('A1:J1', function ($cells) {
						$cells->setFontWeight('bold');
					});
					$sheet->setColumnFormat(array(
						'B' => '@'
					));

					foreach ($storeProducts as $row => $value) {
						$i = $row + 2;
						$sheet->cell('A' . $i, $value['store_product_id']);
						$sheet->setCellValueExplicit('B' . $i, $value['reference'], \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->cell('C' . $i, $value['name']);
						$position = $value['position'] != null ?
							$value['position'] . $value['position_height'] : 'No tiene';
						$sheet->cell('D' . $i, $position);
						$quantity = $value['quantity'] != null ?
							$value['quantity'] : 'No tiene';
						$sheet->cell('E' . $i, $quantity);
						$storagePosition = $value['storage_position'] != null ?
							$value['storage_position'] . $value['storage_height_position'] : 'No tiene';
						$sheet->cell('F' . $i, $storagePosition);
						$pickingStock = $value['picking_stock'] != null ?
							$value['picking_stock'] : 'No tiene';
						$sheet->cell('G' . $i, $pickingStock);
						$receptionStock = $value['reception_stock'] != null ?
							$value['reception_stock'] : 'No tiene';
						$sheet->cell('H' . $i, $receptionStock);
						$returnStock = $value['return_stock'] != null ?
							$value['return_stock'] : 'No tiene';
						$sheet->cell('I' . $i, $returnStock);
						$committedStock = $value['committed_stock'] != null ?
							$value['committed_stock'] : 'No tiene';
						$sheet->cell('J' . $i, $committedStock);
					}
				});
			})->export('xlsx');
		} catch (\Exception $exception) {
			ErrorLog::add($exception);
			return Redirect::back()->withErrors([
				'Ocurrió un error al exportar los productos apagados con stock'
			]);
		}
	}

	public function reportProductEnabledExport()
	{
		try {
			ini_set('memory_limit', '8024M');
			set_time_limit(0);
			ini_set("max_execution_time", -1);
			$input = Input::all();
			$validator = Validator::make($input, [
				'warehouseId' => 'required|exists:warehouses,id'
			]);
			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator);
			}
			$warehouseId = Input::get('warehouseId');
			$warehouse = Warehouse::findOrFail($warehouseId);
			$now = Carbon::now();
			$filename = "{$warehouse->warehouse} - {$now->toDateString()}";
			$excel = Excel::create($filename, function ($excel) use ($warehouseId) {
				$storeProducts = $this->getStockProducts($warehouseId, true);
				$excel->setTitle('Reporte de activos sin stock');
				$excel->setCreator('Merqueo')
					->setCompany('Merqueo');
				$excel->setDescription('Merqueo');
				$excel->sheet('Productos', function ($sheet) use ($storeProducts) {
					$sheet->cell('A1', function ($cell) {
						$cell->setValue('Id');
					});
					$sheet->cell('B1', function ($cell) {
						$cell->setValue('Referencia');
					});
					$sheet->cell('C1', function ($cell) {
						$cell->setValue('Producto');
					});
					$sheet->cell('D1', function ($cell) {
						$cell->setValue('Posición de alistamiento');
					});
					$sheet->cell('E1', function ($cell) {
						$cell->setValue('Cantidad en alistamiento');
					});
					$sheet->cell('F1', function ($cell) {
						$cell->setValue('Cantidad en recibo');
					});
					$sheet->cell('G1', function ($cell) {
						$cell->setValue('Cantidad en devolución');
					});
					$sheet->cell('H1', function ($cell) {
						$cell->setValue('Comprometido');
					});
					$sheet->cells('A1:H1', function ($cells) {
						$cells->setFontWeight('bold');
					});

					foreach ($storeProducts as $row => $value) {
						$i = $row + 2;
						$sheet->cell('A' . $i, $value['store_product_id']);
						$sheet->setCellValueExplicit('B' . $i, $value['reference'], \PHPExcel_Cell_DataType::TYPE_STRING);
						$sheet->cell('C' . $i, $value['name']);
						$storage_position = $value['storage_position'] != null ?
							$value['storage_position'] . $value['storage_height_position'] : 'No tiene';
						$sheet->cell('D' . $i, $storage_position);
						$reception_stock = $value['picking_stock'] != null ?
							$value['picking_stock'] : 'No tiene';
						$sheet->cell('E' . $i, $reception_stock);
						$reception_stock = $value['reception_stock'] != null ?
							$value['reception_stock'] : 'No tiene';
						$sheet->cell('F' . $i, $reception_stock);
						$return_stock = $value['return_stock'] != null ?
							$value['return_stock'] : 'No tiene';
						$sheet->cell('G' . $i, $return_stock);
						$committedStock = $value['committed_stock'] != null ?
							$value['committed_stock'] : 'No tiene';
						$sheet->cell('H' . $i, $committedStock);

					}
				});
			})->export('xlsx');
			return $excel;
		} catch (\Exception $exception) {
			ErrorLog::add($exception);
			return Redirect::back()->withErrors([
				'Ocurrió un error al exportar los productos activos sin stock'
			]);
		}

	}

	/**
	 * Consulta los productos activos sin stock o inactivos con stock
	 */
	public function getStockProducts($warehouseId, $productEnabled)
	{
		$storeProducts = StoreProduct::join(
			'store_product_warehouses',
			'store_product_warehouses.store_product_id',
			'=',
			'store_products.id'
		)
			->leftjoin('warehouse_storages', function ($join) use ($warehouseId) {
				$join->on(
					'warehouse_storages.store_product_id',
					'=',
					'store_products.id'
				);
				$join->on(
					'warehouse_storages.warehouse_id',
					'=',
					DB::raw($warehouseId)
				);
			})
			->join('products', 'products.id', '=', 'store_products.product_id');
		if ($productEnabled) {
			$storeProducts->where(function ($query) use ($warehouseId) {
				$query->where('reception_stock', '=', 0);
				$query->where('picking_stock', '=', 0);
				$query->where('current_stock', '=', 0);
				$query->where('store_product_warehouses.status', '=', 1);
				$query->where('store_product_warehouses.discontinued', '=', 0);
				$query->where(
					'store_product_warehouses.warehouse_id',
					'=',
					$warehouseId
				);
				$query->whereNotIn('products.type', ['Agrupado', 'Proveedor']);
			});
		} else {
			$storeProducts->where(function ($query) use ($warehouseId) {
				$query->where(function ($query) {
					$query->orwhere('reception_stock', '>', 0);
					$query->orwhere('picking_stock', '>', 0);
					$query->orwhere('current_stock', '>', 0);
				});
				$query->where('store_product_warehouses.status', '=', 0);
				$query->where('store_product_warehouses.discontinued', '=', 0);
				$query->where('store_product_warehouses.warehouse_id', '=', $warehouseId);
			})
				->orwhere(function ($query) use ($warehouseId) {
					$query->where(function ($query) {
						$query->where('reception_stock', '=', 0);
						$query->where('picking_stock', '=', 0);
						$query->where('current_stock', '=', 0);
						$query->whereNotNull('warehouse_storages.quantity');
					});
					$query->where('store_product_warehouses.status', '=', 0);
					$query->where('store_product_warehouses.discontinued', '=', 0);
					$query->where('warehouse_storages.warehouse_id', '=', $warehouseId);
					$query->where(
						'store_product_warehouses.warehouse_id',
						'=',
						'warehouse_storages.warehouse_id'
					);
					$query->whereNotIn('products.type', ['Agrupado', 'Proveedor']);
				});
		}
		$storeProducts = $storeProducts->select(
			'store_products.id as store_product_id',
			'products.reference',
			'products.image_small_url',
			'products.name',
			'store_product_warehouses.status',
			'store_product_warehouses.discontinued',
			'store_product_warehouses.current_stock',
			'store_product_warehouses.picking_stock',
			'store_product_warehouses.reception_stock',
			'store_product_warehouses.return_stock',
			'store_product_warehouses.storage_position',
			'store_product_warehouses.storage_height_position',
			'warehouse_storages.quantity',
			'warehouse_storages.position',
			'warehouse_storages.position_height',
			'warehouse_storages.warehouse_id',
			'store_product_warehouses.warehouse_id'
		)->get()->toArray();
		for ($i = 0; $i < count($storeProducts); $i++) {
			$storeProduct = StoreProduct::find($storeProducts[$i]['store_product_id']);
			$storeProducts[$i]['committed_stock'] = $storeProduct->getCommittedStock($warehouseId);
		}
		return $storeProducts;
	}
}
