<?php

namespace admin\inventory\storage;

use admin\AdminController;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Input;
use PickingSupplier;
use PickingSupplierLog;
use Request;
use Response;
use Session;
use StoreProduct;
use StoreProductWarehouse;
use View;
use Warehouse;
use WarehouseStorage;

class PickingSupplierController extends AdminController
{

    /**
     * Carga la pantalla inicial del modulo
     * @return mixed
     */
    public function index()
    {
        if (!Input::ajax()) {
            $cities = $this->get_cities();
            $types = array('Seco', 'Frío');
            return View::make('admin.inventory.picking_supplier.index')
                ->with('title', 'Surtidor de alistamiento')
                ->with('cities', $cities)
                ->with('types', $types);
        }
    }

    /**
     * devuelve las bodegas segun el id de la ciudad
     *
     */
    public function get_warehouses_ajax()
    {
        if (Input::ajax()) {
            $warehouses = Warehouse::select('*')->where('city_id', Input::get('city_id'))->get();
            $html = '<option value="">Selecciona</option>';
            foreach ($warehouses as $warehouse) {
                $html .= '<option value="' . $warehouse->id . '">' . $warehouse->warehouse . '</option>';
            }

            return Response::json([
                'status' => true,
                'result' => $html,
            ]);
        }
    }

    /**
     * Crea las tareas de alistamiento y asigna tareas al usuario actualemnte logeado
     */
    public function get_supplier_picking_tasks_ajax()
    {
        if (get_config_option('block_reception_return_storage', Input::get('warehouse_id'))) {
            return Response::json(array(
                'status' => false,
                'message' => 'El almacenamiento de productos esta deshabilitado, por favor contacte al administrador.'
            ));
        }

        if (Input::get('warehouse_id') != Session::get('admin_warehouse_id')
            && Session::get('admin_designation') != 'Super Admin') {
            return Response::json([
                'status' => false,
                'message' => 'Usted no se encuentra asignado a la bodega seleccionada. Consulte con el administrador.',
            ]);
        }

        $type_storage = (Input::get('type_storage') == 'Seco') ? array(Input::get('type_storage')) : array(
            'Refrigerado',
            'Congelado'
        );
        $tasks = $this->get_tasks($type_storage);

        $ids = array();
        if (count($tasks)) {
            //$this->update_ajax(Input::get('warehouse_id'));
            foreach ($tasks as $task) {
                $ids[] = $task->store_product_id;
            }
        }

        $products = StoreProduct::select(
            'store_products.id',
            DB::raw("CONCAT_WS(' ',products.name, products.quantity, products.unit) AS name"),
            'products.reference',
            'store_product_warehouses.minimum_picking_stock',
            'store_product_warehouses.maximum_picking_stock',
            'store_product_warehouses.picking_stock',
            'products.image_small_url',
            'store_product_warehouses.warehouse_id',
            'store_product_warehouses.storage_position',
            'store_product_warehouses.storage_height_position',
            DB::raw(
                'store_product_warehouses.maximum_picking_stock - store_product_warehouses.picking_stock AS quantity_to_supply'
            )
        )
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->whereRaw("store_products.id IN (SELECT DISTINCT store_product_id FROM warehouse_storages WHERE warehouse_id =" . Input::get('warehouse_id') . " )")
            ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
            ->whereRaw('store_product_warehouses.picking_stock < store_product_warehouses.minimum_picking_stock ')
            ->where('products.type', 'Simple')
            ->whereIn('store_products.storage', $type_storage)
            ->whereNotIn('store_products.id', $ids)
            ->skip(0)
            ->take(10)
            ->get();

        foreach ($products as $product) {
            if ($product->picking_stock <= $product->minimum_picking_stock) {
                $task = new PickingSupplier;
                $task->store_product_id = $product->id;
                $task->warehouse_id = $product->warehouse_id;
                $task->quantity_picking = $product->picking_stock;
                $task->quantity_to_suppling = $product->maximum_picking_stock - $product->picking_stock;
                $task->picking_position = $product->storage_position;
                $task->status = 'Pending';
                $task->picking_position_height = $product->storage_height_position;
                $task->save();
            }
        }

        $this->asignated_tasks($type_storage, Input::get('warehouse_id'));

        $tasks = $this->get_my_tasks($type_storage, Input::get('warehouse_id'));

        $data = [
            'tasks' => $tasks
        ];

        return Response::json([
            'status' => true,
            'result' => View::make('admin.inventory.picking_supplier.index', $data)->renderSections()['tasks'],
        ]);
    }

    /**
     * obtiene las tareas de alistamiento disponible
     *
     * @param varchar $type_storage tipo de almacenamiento ( Seco , Frío )
     *
     */
    private function get_tasks($type_storage)
    {

        return PickingSupplier::select(
            'picking_supplier.*',
            DB::raw("CONCAT_WS(' ',products.name, products.quantity, products.unit) AS name"),
            'products.reference',
            'products.image_large_url'
        )
            ->join('store_products', 'store_products.id', '=', 'picking_supplier.store_product_id')
            ->join('store_product_warehouses', function ($join) {
                $join->on('store_product_warehouses.store_product_id', '=', 'store_products.id');
                $join->on('store_product_warehouses.warehouse_id', '=', 'picking_supplier.warehouse_id');
            })
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('warehouse_storages', function ($join) {
                $join->on('warehouse_storages.store_product_id', '=', 'store_product_warehouses.store_product_id');
                $join->on('warehouse_storages.warehouse_id', '=', 'store_product_warehouses.warehouse_id');
            })
            ->whereIn('picking_supplier.status', array('Pending', 'In Process'))
            ->whereIn('store_products.storage', $type_storage)
            ->where('picking_supplier.warehouse_id', Session::get('admin_warehouse_id'))
            ->groupby('store_products.id')
            //->toSql();
            ->get();
    }

    /**
     * Asigna una tarea al usuario actual
     *
     * @param varchar $type_storage tipo de almacenamiento ( Seco , Frío )
     *
     */
    public function asignated_tasks($type_storage, $warehouse_id)
    {
        $my_tasks = $this->count_my_tasks($type_storage, $warehouse_id);
        if ($my_tasks > 0) {
            $this->update_ajax($warehouse_id, $type_storage);
        }

        $tasks = $this->get_my_tasks($type_storage, $warehouse_id, true);
        foreach ($tasks as $task) {
            if ($my_tasks < 1) {
                $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse(
                    $task->warehouse_id,
                    $task->store_product_id
                );
                $quantity_to_suppling = 0;
                if ($store_product_warehouse->picking_stock <= $store_product_warehouse->minimum_picking_stock || $store_product_warehouse->picking_stock < $store_product_warehouse->maximum_picking_stock) {
                    $quantity_to_suppling = $store_product_warehouse->maximum_picking_stock - $store_product_warehouse->picking_stock;
                    PickingSupplier::where('id', $task->id)->update([
                        'quantity_picking' => $store_product_warehouse->picking_stock,
                        'quantity_to_suppling' => $quantity_to_suppling
                    ]);
                }

                if (is_null($task->picking_supplier_id) && $quantity_to_suppling > 0) {
                    PickingSupplier::where('id', $task->id)
                        ->update(array('status' => 'In Process', 'picking_supplier_id' => Session::get('admin_id')));
                    $my_tasks++;
                }
            }
            /*if($quantity_to_suppling == 0){
                PickingSupplier::where('id', $task->id)->update(['status'=>'Success', 'quantity_to_suppling'=>$quantity_to_suppling]);
            }*/
        }

    }

    /**
     * Cuenta las tareas de alistamiento del usuario logeado
     *
     * @param varchar $type_storage tipo de almacenamiento ( Seco , Frío )
     *
     */
    private function count_my_tasks($type_storage, $warehouse_id)
    {
        return PickingSupplier::select('picking_supplier.id')
            ->join('store_products', 'store_products.id', '=', 'picking_supplier.store_product_id')
            ->join('store_product_warehouses', function ($join) {
                $join->on('store_product_warehouses.store_product_id', '=', 'store_products.id');
                $join->on('store_product_warehouses.warehouse_id', '=', 'picking_supplier.warehouse_id');
            })
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->whereRaw("store_products.id IN (SELECT DISTINCT store_product_id FROM warehouse_storages WHERE warehouse_id =" . $warehouse_id . " )")
            ->whereIn('picking_supplier.status', array('Pending', 'In Process'))
            ->where('picking_supplier.picking_supplier_id', Session::get('admin_id'))
            ->whereIn('store_products.storage', $type_storage)
            ->where('picking_supplier.warehouse_id', $warehouse_id)
            ->count();
    }

    /**Actualiza las tareas actuales */
    public function update_ajax($warehouse_id = null, $type_storage = null)
    {
        $warehouse_id = Input::has('warehouse_id') ? Input::get('warehouse_id') : $warehouse_id;
        /*$tasks = PickingSupplier::where('warehouse_id', $warehouse_id)
                                ->whereIn('status', array('Pending','In Process'))
                                ->whereRaw("picking_supplier_id IS NOT NULL")
                                ->get();*/
        $tasks = $this->get_my_tasks($type_storage, $warehouse_id);
        if (count($tasks) > 0) {
            foreach ($tasks as $task) {
                $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse(
                    $task->warehouse_id,
                    $task->store_product_id
                );
                if ($store_product_warehouse->picking_stock <= $store_product_warehouse->minimum_picking_stock || $store_product_warehouse->picking_stock < $store_product_warehouse->maximum_picking_stock) {
                    $quantity_to_suppling = $store_product_warehouse->maximum_picking_stock - $store_product_warehouse->picking_stock;
                    PickingSupplier::where('id', $task->id)->update([
                        'quantity_picking' => $store_product_warehouse->picking_stock,
                        'quantity_to_suppling' => $quantity_to_suppling
                    ]);
                }
            }
        }
    }

    /**
     * Devuelve las tareas de alistamiento del usuario logeado
     *
     * @param varchar $type_storage tipo de almacenamiento ( Seco , Frío )
     *
     */
    private function get_my_tasks($type_storage, $warehouse_id, $all_tasks = false)
    {

        return PickingSupplier::select(
            'picking_supplier.*',
            DB::raw("CONCAT_WS(' ',products.name, products.quantity, products.unit) AS name"),
            'products.reference',
            'products.image_large_url as image',
            'store_product_warehouses.minimum_picking_stock',
            'store_product_warehouses.maximum_picking_stock'
        )
            ->join('store_products', 'store_products.id', '=', 'picking_supplier.store_product_id')
            ->join('store_product_warehouses', function ($join) {
                $join->on('store_product_warehouses.store_product_id', '=', 'store_products.id')
                    ->on('store_product_warehouses.warehouse_id', '=', 'picking_supplier.warehouse_id');
            })
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->whereIn('picking_supplier.status', array('Pending', 'In Process'))
            ->where(function ($where) use ($all_tasks) {
                if ($all_tasks) {
                    $where->whereNull('picking_supplier.picking_supplier_id');
                }
                $where->orWhere('picking_supplier.picking_supplier_id', Session::get('admin_id'));
            })
            ->whereRaw("store_products.id IN (SELECT DISTINCT store_product_id FROM warehouse_storages WHERE warehouse_id =" . $warehouse_id . " )")
            ->whereIn('store_products.storage', $type_storage)
            ->where('picking_supplier.warehouse_id', $warehouse_id)
            ->skip(0)
            ->take(10)
            ->orderby('picking_supplier.quantity_to_suppling', 'DESC')
            ->orderby('picking_supplier.picking_supplier_id', 'DESC')
            ->orderby('picking_supplier.id', 'ASC')
            ->get();
    }

    /**
     * Actualiza el estado de la tarea y actualiza los stock de picking y storage en sus respectivas tablas
     * @return \Illuminate\Http\JsonResponse
     */
    public function save_supplier()
    {
        if (Input::ajax()) {
            try {
                DB::beginTransaction();
                $warehouseStorageId = Input::get('storage');
                $warehouseStorage = WarehouseStorage::lockForUpdate()->findOrFail($warehouseStorageId);
                $quantityToSuppling = Input::get('quantity_to_suppling');
                $pickingSupplier = PickingSupplier::lockForUpdate()
                    ->findOrFail(Input::get('id_picking_supplier'));

                if (!is_numeric($quantityToSuppling) || $quantityToSuppling < 1) {
                    return Response::json([
                        'status' => false,
                        'message' => 'La cantidad a sutir debe ser numérica y superior a 1.',
                    ]);
                }
                $storeProduct = StoreProduct::find($pickingSupplier->store_product_id);
                $validateBlockPosition = Warehouse::validateBlockedPositions(
                    $warehouseStorage->warehouse_id,
                    'Almacenamiento',
                    $storeProduct->storage,
                    strtoupper($warehouseStorage->position_height),
                    $warehouseStorage->position
                );
                if ($validateBlockPosition) {
                    return Response::json([
                        'status' => false,
                        'message' => 'La posición de almacenamiento que elegiste se encuentra bloqueda debido a un 
                        conteo de productos intenta mas tarde.',
                    ]);
                }
                $validateBlockPosition = Warehouse::validateBlockedPositions(
                    $pickingSupplier->warehouse_id,
                    'Alistamiento',
                    $storeProduct->storage,
                    strtoupper($pickingSupplier->picking_position_height),
                    $pickingSupplier->picking_position
                );
                if ($validateBlockPosition) {
                    return Response::json([
                        'status' => false,
                        'message' => 'La posición alistamiento que elegiste se encuentra bloqueda debido a un 
                        conteo de productos intenta mas tarde.',
                    ]);
                }

                $storedQuantity = $storeProduct->getStorageStock($pickingSupplier->warehouse_id);
                /*$storedQuantity = StoreProduct::find($pickingSupplier->store_product_id)
                    ->getStorageStock($pickingSupplier->warehouse_id);*/

                if ($quantityToSuppling >= $pickingSupplier->quantity_to_suppling
                    || $quantityToSuppling >= $storedQuantity) {
                    $pickingSupplier->status = 'Success';
                    $pickingSupplier->quantity_supplied += $quantityToSuppling;
                    $pickingSupplier->save();

                    $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(
                        $pickingSupplier->warehouse_id,
                        $pickingSupplier->store_product_id
                    );
                    $storeProductWarehouse->picking_stock += $quantityToSuppling;
                    $storeProductWarehouse->save();

                    $warehouseStorage->quantity -= $quantityToSuppling;
                    $warehouseStorage->save();

                    PickingSupplierLog::log(
                        $pickingSupplier,
                        $warehouseStorageId,
                        'Completa tarea de surtido',
                        $quantityToSuppling
                    );

                    WarehouseStorage::where('quantity', 0)->delete();
                    DB::commit();
                    return Response::json([
                        'status' => true,
                        'message' => 'Se actualizo el stock de alistamiento.',
                        'result' => 'finished'
                    ]);
                } elseif ($quantityToSuppling <= $pickingSupplier->quantity_to_suppling) {
                    $pickingSupplier->quantity_supplied += $quantityToSuppling;
                    $pickingSupplier->quantity_to_suppling -= $quantityToSuppling;
                    $pickingSupplier->save();

                    $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(
                        $pickingSupplier->warehouse_id,
                        $pickingSupplier->store_product_id
                    );
                    $storeProductWarehouse->picking_stock += $quantityToSuppling;
                    $storeProductWarehouse->save();

                    $warehouseStorage->quantity -= $quantityToSuppling;
                    $warehouseStorage->save();

                    PickingSupplierLog::log(
                        $pickingSupplier,
                        $warehouseStorageId,
                        'Actualiza tarea de surtido',
                        $quantityToSuppling
                    );
                    WarehouseStorage::where('quantity', 0)->delete();

                    DB::commit();
                    return Response::json([
                        'status' => true,
                        'message' => 'Se actualizo el stock de alistamiento.',
                        'result' => Input::get('id_picking_supplier')
                    ]);

                } else {
                    DB::rollback();
                    return Response::json([
                        'status' => false,
                        'message' => 'La cantidad a sutir no puede ser menor a la cantidad necesaria en alistamiento.',
                    ]);
                }
            } catch (QueryException $e) {
                DB::rollBack();
                if ($e->getCode() == '40001') {
                    \ErrorLog::add($e, 513);
                    return Response::json([
                        'result' => false,
                        'message' =>  'Otro proceso se está ejecutando sobre estos productos, 
                        espera un momento para volver a intentarlo.',
                    ]);
                }
                \ErrorLog::add($e);
                return Response::json([
                    'result' => false,
                    'message' =>  $e->getMessage(),
                ]);
            } catch (ModelNotFoundException $e) {
                DB::rollBack();
                if ($e->getModel() == WarehouseStorage::class) {
                    return Response::json([
                        'result' => false,
                        'message' => 'La posición en almacenamiento no existe.',
                    ]);
                }
                if ($e->getModel() == PickingSupplier::class) {
                    return Response::json([
                        'result' => false,
                        'message' => 'El surtidor de picking no existe.',
                    ]);
                }
                \ErrorLog::add($e);
            } catch (\Exception $e) {
                DB::rollback();
                if ($e->getCode() == '40001') {
                    \ErrorLog::add($e, 513);
                    return Response::json([
                        'result' => false,
                        'message' => 'Otro proceso se está ejecutando sobre estos productos, 
                        espera un momento para volver a intentarlo.'
                    ]);
                }
                \ErrorLog::add($e);
                return Response::json([
                    'result' => false,
                    'message' => $e->getMessage() . ' ' . $e->getLine(),
                ]);
            }
        }
    }

    /**
     * Actualiza el estado de una tarea
     *
     */
    public function update_task_ajax()
    {
        $task = PickingSupplier::where('id', Input::get('id'))
            ->update(array('status' => Input::get('status'), 'picking_supplier_id' => Session::get('admin_id')));
    }

    /**
     * Devuelve el listado de productos almacenados en bodega cuya fecha de vencimiento sea mayor al dia actual
     *
     */
    public function get_storaged_products_ajax()
    {
        $storaged_products = PickingSupplier::getStoragedProducts(Input::get('id'));

        $quantity = 0;
        $storaged_products_selected = [];
        /*foreach($storaged_products AS $key => $storaged_product){
            if($storaged_product->quantity_to_suppling > $quantity ){
                $storaged_products_selected[] = $storaged_product;
                $quantity += $storaged_product->quantity;
            }
        }*/


        $data = [
            'storaged_products' => $storaged_products,
            'id_picking_supplier' => Input::get('id')
        ];

        return Response::json([
            'status' => true,
            'result' => View::make(
                'admin.inventory.picking_supplier.index',
                $data
            )->renderSections()['storaged-products'],
        ]);
    }

    /**
     * Permite crear una tarea de forma manual para un producto especifico
     *
     */
    public function set_tasks_manually_ajax()
    {

        $type_storage = (Input::get('type_storage') == 'Seco') ? array(Input::get('type_storage')) : array(
            'Refrigerado',
            'Congelado'
        );
        $my_tasks = $this->count_my_tasks($type_storage, Input::get('warehouse_id'));
        if ($my_tasks >= 3) {
            return Response::json([
                'status' => false,
                'message' => 'Tiene demasiadas tareas asignadas, por favor completelas antes de continuar.',
            ]);
        }


        $product = StoreProduct::select(
            'store_products.id',
            DB::raw("CONCAT_WS(' ',products.name, products.quantity, products.unit) AS name"),
            'store_product_warehouses.minimum_picking_stock',
            'store_product_warehouses.maximum_picking_stock',
            'store_product_warehouses.picking_stock',
            'products.image_small_url',
            'store_product_warehouses.warehouse_id',
            'store_product_warehouses.storage_position',
            'store_product_warehouses.storage_height_position'
        )
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
            ->where('store_products.id', Input::get('store_product_id'))
            ->get()
            ->first();

        $tasks = PickingSupplier::select('*')
            ->where('store_product_id', Input::get('store_product_id'))
            ->where('warehouse_id', Input::get('warehouse_id'))
            ->whereIn('status', array('Pending', 'In Process'))
            ->get();


        if (count($tasks) > 0) {
            $task = $tasks->first();
            if (is_null($task->picking_supplier_id) || Input::get('get_task')) {
                $quantity_to_suppling = ($product->picking_stock <= $product->minimum_picking_stock || $product->picking_stock < $product->maximum_picking_stock) ? $product->maximum_picking_stock - $product->picking_stock : 0;
                $task = PickingSupplier::find($task->id);

                $task->picking_supplier_id = Session::get('admin_id');
                $task->status = 'In Process';
                $task->quantity_picking = $product->picking_stock;
                $task->quantity_to_suppling = $quantity_to_suppling;
                $task->automatic = 0;
                $task->save();

            } else {
                return Response::json([
                    'status' => false,
                    'message' => 'Ya existe una tarea con este producto asignada a otro usuario.',
                ]);
            }

        } else {
            $quantity_to_suppling = ($product->picking_stock <= $product->minimum_picking_stock || $product->picking_stock < $product->maximum_picking_stock) ? $product->maximum_picking_stock - $product->picking_stock : 0;
            $task = new PickingSupplier;
            $task->store_product_id = $product->id;
            $task->warehouse_id = $product->warehouse_id;
            $task->quantity_picking = $product->picking_stock;
            $task->quantity_to_suppling = $quantity_to_suppling;
            $task->picking_position = $product->storage_position;
            $task->status = 'In Process';
            $task->automatic = 0;
            $task->picking_position_height = $product->storage_height_position;
            $task->picking_supplier_id = Session::get('admin_id');
            $task->save();
        }
        PickingSupplierLog::log($task, null, 'Se asigno una nueva tarea de alistameinto.');
        return Response::json([
            'status' => true,
            'message' => 'Se asigno una tarea para este producto.',
        ]);

    }

    /**
     * Devuelve los produtos segun un referencia o nombre
     *
     */
    public function search_products()
    {

        $search_product = Input::get('search_product');
        $type_storage = (Input::get('type_storage') == 'Seco') ? array(Input::get('type_storage')) : array(
            'Refrigerado',
            'Congelado'
        );

        $products = PickingSupplier::searchProducts(
            Input::get('search_product'),
            Input::get('warehouse_id'),
            $type_storage
        );


        $data = [
            'searched_products' => $products,
        ];

        return Response::json([
            'status' => true,
            'result' => View::make(
                'admin.inventory.picking_supplier.index',
                $data
            )->renderSections()['search-products'],
        ]);
    }

    /**
     * Actualiza el estado de la tarea y actualiza los stock de picking y storage en sus respectivas tablas
     *
     *
     */
    public function delete_task_supplier()
    {
        if (Request::ajax()) {
            $task = PickingSupplier::find(Input::get('id'));
            if ($task->quantity_supplied == 0) {
                PickingSupplierLog::log($task, null, 'Se elimino una tarea de alistameinto.');
                PickingSupplier::deleteTask(Input::get('id'));
            } else {
                return Response::json([
                    'status' => false,
                    'message' => "No se es posible eliminar esta tarea.",
                ]);
            }
        }
    }

    /**
     * Actualiza el estado de la tarea y actualiza los stock de picking y storage en sus respectivas tablas
     *
     *
     */
    public function close_task_supplier()
    {
        if (Request::ajax()) {
            $task = PickingSupplier::find(Input::get('id'));
            if ($task->quantity_supplied > 0) {
                PickingSupplierLog::log($task, null, 'Se cerro la tarea de alistameinto.');
                $task->status = 'Success';
                $task->save();
            } else {
                return Response::json([
                    'status' => false,
                    'message' => "No se es posible cerrar esta tarea.",
                ]);
            }
        }
    }
}
