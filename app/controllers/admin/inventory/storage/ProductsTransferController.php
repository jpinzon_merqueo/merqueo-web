<?php

namespace admin\inventory\storage;

use admin\AdminController;
use ErrorLog;
use exceptions\MerqueoException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Warehouse;
use Product;
use ProductTransfer;
use ProductTransferDetail;
use Zend\Json\Json;

class ProductsTransferController extends AdminController
{
    /**
     * Función para mostrar la grilla con los traslados registrados en el sistema.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data['title'] = 'Traslado entre bodegas';
        $data['cities'] = $this->get_cities();
        $data['warehouses'] = Warehouse::where('city_id', Session::get('admin_city_id'))
            ->select('warehouses.id', 'warehouses.warehouse')
            ->get();

        return \View::make('admin.inventory.product_transfer.index', $data);
    }

    /**
     * Función para mostrar el formulario para crear un traslado de productos
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $data['title'] = 'Crear traslado de productos';
        $data['cities'] = $this->get_cities();
        $data['warehouses'] = Warehouse::where('city_id', Session::get('admin_city_id'))
            ->select('warehouses.id', 'warehouses.warehouse')
            ->get();
        return \View::make('admin.inventory.product_transfer.create', $data);
    }

    /**
     * Función para crear un traslado
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store()
    {
        $data['origin_warehouse_id'] = Input::get('origin_warehouse_id');
        $data['destiny_warehouse_id'] = Input::get('destiny_warehouse_id');
        $data['admin_id'] = \Session::get('admin_id');

        $products = Input::get('products');
        $products = Json::decode($products, true);

        $data['products'] = $products;

        $rules = [
            'origin_warehouse_id' => 'required|integer|exists:warehouses,id',
            'destiny_warehouse_id' => 'required|integer|exists:warehouses,id',
            'admin_id' => 'required|integer|exists:admin,id',
        ];

        foreach (array_keys($products) as $index) {
            $rules['products.'.$index.'.product_id'] = 'required|integer|exists:products,id';
            $rules['products.'.$index.'.store_product_id'] = 'required|integer|exists:store_products,id';
            $rules['products.'.$index.'.warehouse_storage_id'] = 'required|integer|exists:warehouse_storages,id';
            $rules['products.'.$index.'.transfer_quantity'] = 'required|integer|min:1';
            $rules['products.'.$index.'.position_height_from'] = 'required|alpha|min:1';
            $rules['products.'.$index.'.position_from'] = 'required|integer|min:1';
            $rules['products.'.$index.'.storage_type'] = 'required|in:Seco,Frío';
        }
        $validator = \Validator::make(
            $data,
            $rules
        );

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }

        $admin = \Admin::findOrFail($data['admin_id']);
        $originWarehouse = Warehouse::findOrFail($data['origin_warehouse_id']);
        $destinyWarehouse = Warehouse::findOrFail($data['destiny_warehouse_id']);
        \DB::beginTransaction();
        try {
            $productTransfer = ProductTransfer::createTransfer($admin, $originWarehouse, $destinyWarehouse, $products);
            \DB::commit();
            return \Redirect::route('productTransfer.show', ['id' => $productTransfer->id])
                ->with('success', "Traslado creado con éxito.");
        } catch (QueryException $e) {
            \DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                return \Redirect::back()->withErrors([
                    'Exception',
                    'Otro proceso se está ejecutando sobre estos productos, 
                    espera un momento para volver a intentarlo.'
                ]);
            }
            ErrorLog::add($e);
            return \Redirect::back()->withErrors(['Exception', $e->getMessage()]);
        } catch (MerqueoException $e) {
            \DB::rollBack();
            return \Redirect::back()
                ->with('error', "No se creo el traslado por el siguiente error: {$e->getMessage()}");
        }
    }

    /**
     * Muestra el detalle de una traslado
     *
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $transfer = ProductTransfer::with('productTransferDetails.storeProduct.product')->findOrFail($id);
        $transfer->logs();
        $data['title'] = 'Traslado de producto #';
        $data['transfer'] = $transfer;
        return \View::make('admin.inventory.product_transfer.show', $data);
    }

    /**
     * Función para obtener bodegas con ajax
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function get_warehouses_ajax()
    {
        $city_id = Input::get('city_id');
        $data = Warehouse::where('city_id', $city_id)->select('warehouses.id', 'warehouses.warehouse')->get();

        return \Response::json($data);
    }

    /**
     * Función para buscar los productos para ser trasladados
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search_products_ajax()
    {
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $search = Input::get('search');
        $products_added = Input::get('products_added');
        // Se omiten los productos que ya han sido agregados para la transferencia.
        if (!empty($products_added)) {
            foreach ($products_added as &$item) {
                $item = json_decode($item, true);
            }
            $products_added = array_fetch($products_added, 'store_product_id');
        }

        $products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
            ->join('stores', 'store_products.store_id', '=', 'stores.id')
            ->join('warehouse_storages', 'store_products.id', '=', 'warehouse_storages.store_product_id')
            ->where(function ($query) use ($search) {
                $query->where('products.name', 'like', '%'.$search.'%');
                $query->orWhere('products.id', 'like', '%'.$search.'%');
                $query->orWhere('products.reference', 'like', '%'.$search.'%');
                $query->orWhere('store_products.provider_plu', 'like', '%' . $search . '%');
            })
            ->where('stores.city_id', $city_id)
            ->where('warehouse_storages.warehouse_id', $warehouse_id)
            ->orderBy('warehouse_storages.position', 'ASC')
            ->orderBy('warehouse_storages.position_height', 'ASC')
            ->select(
                'products.id AS product_id',
                'products.reference',
                'products.name',
                \DB::raw('products.quantity AS quantity_unit'),
                'products.unit',
                'store_products.id AS store_product_id',
                'warehouse_storages.id AS warehouse_storage_id',
                'warehouse_storages.storage_type',
                'warehouse_storages.position_height AS position_height_from',
                'warehouse_storages.position AS position_from',
                'warehouse_storages.quantity',
                'warehouse_storages.expiration_date',
                \DB::raw('DATE_FORMAT(warehouse_storages.expiration_date, "%d %b %Y") AS formatted_expiration_date'),
                \DB::raw('0 AS transfer_quantity')
            );

        if (!empty($products_added)) {
            $products = $products->whereNotIn('store_products.id', $products_added);
        }

        $products = $products->get();

        return \Response::json($products);
    }

    /**
     * Obtiene los traslados creados
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_transfers_ajax()
    {
        $warehouse_id = Input::get('warehouse_id');
        $status = Input::get('status');
        $id = Input::get('id');

        $transfers = ProductTransfer::with('originWarehouse', 'destinyWarehouse');
        if (!empty($status)) {
            $transfers = $transfers->where('status', $status);
        }

        if (!empty($id)) {
            $transfers = $transfers->where('id', $id);
        }
        $transfers = $transfers->where('origin_warehouse_id', $warehouse_id);
        $transfers = $transfers->paginate(20);

        return \Response::json($transfers);
    }

    /**
     * Buscador de productos dentro de los traslados
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function search_transfer_products_ajax($id)
    {
        $search = Input::get('search');
        $search = trim($search);
        ProductTransfer::findOrFail($id);
        if (!empty($search)) {
            $products = ProductTransferDetail::with('storeProduct.product')
                ->join('store_products', 'product_transfer_details.store_product_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->where(function ($query) use ($search) {
                    $query->where('products.name', 'like', '%'.$search.'%');
                    $query->orWhere('products.id', 'like', '%'.$search.'%');
                    $query->orWhere('products.reference', 'like', '%'.$search.'%');
                    $query->orWhere('store_products.provider_plu', 'like', '%' . $search . '%');
                })
                ->where('transfer_id', $id)
                ->select('product_transfer_details.*')
                ->get();
        } else {
            $products = ProductTransferDetail::with('storeProduct.product')
                ->where('transfer_id', $id)
                ->select('product_transfer_details.*')
                ->get();
        }

        return \Response::json($products);
    }

    /**
     * Actualiza el estado del traslado
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update_transfer_status($id)
    {
        $status = Input::get('status');
        $status = trim($status);

        $transfer = ProductTransfer::with('productTransferDetails.storeProduct.storeProductWarehouses')
            ->findOrFail($id);
        try {
            \DB::beginTransaction();
            $transfer->updateStatus($status);
            $message = "Se actualizó la transferencia a {$transfer->spanish_status}";
            \DB::commit();
            return Redirect::back()->with('success', $message);
        } catch (\Exception $e) {
            $message = "No se actualizó la transferencia, error: {$e->getMessage()}";

            \DB::rollBack();
            return Redirect::back()->with('error', $message);
        }
    }

    /**
     * Función para actualizar los estados del detalle del traslado
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update_detail_received_status($id)
    {
        $transfer_detail_id = Input::get('detail_id');
        $transfer_detail_quantity = Input::get('quantity_received');
        $transfer_detail_expiration_date = Input::get('expiration_date');
        ProductTransfer::findOrFail($id);
        $transfer_detail = ProductTransferDetail::findOrFail($transfer_detail_id);

        try {
            \DB::beginTransaction();

            $validator = \Validator::make(
                Input::all(),
                [
                    'detail_id' => 'required',
                    'quantity_received' => 'required|min:1',
                    'expiration_date' => 'date'
                ]
            );

            if ($validator->fails()) {
                return \Response::json($validator->messages()->first(), 500);
            }

            $response = $transfer_detail->updateQuantityReceived($transfer_detail_quantity);
            if (!empty($transfer_detail_expiration_date)) {
                $response = $transfer_detail->updateExpirationDate($transfer_detail_expiration_date);
            }
            \DB::commit();
            return \Response::json($response);
        } catch (MerqueoException $e) {
            \DB::rollBack();
            return \Response::json($e->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage_products($id)
    {
        $transfer = ProductTransfer::findOrFail($id);
        $products = Input::get('product');
        $position = Input::get('position');
        $position_height = Input::get('position_height');
        $storage_type = Input::get('storage_type');

        $validator = \Validator::make(
            Input::all(),
            [
                'position' => 'required|min:1|integer',
                'position_height' => 'required|alpha|size:1',
                'storage_type' => 'required|in:Seco,Frío'
            ]
        );

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }

        try {
            \DB::beginTransaction();
            if (empty($products)) {
                \DB::rollBack();
                return Redirect::back()->with('error', 'No hay productos para almacenar.');
            }

            $details = $transfer->productTransferDetails->filter(function ($detail) use (
                $products,
                $position,
                $position_height,
                $storage_type
            ) {
                if (!empty($products[$detail->id])) {
                    $detail->quantity_to_storage = $products[$detail->id];
                    $detail->position = $position;
                    $detail->position_height = $position_height;
                    $detail->storage_type = $storage_type;
                    return true;
                }
            });

            if (empty($details)) {
                \DB::rollBack();
                return Redirect::back()->with('error', 'Los productos enviados no se encuentran en el traslado.');
            }

            $transfer->details_to_storage = $details;
            $transfer->setStatusStoraged();
            \DB::commit();
            return Redirect::back()->with('success', 'Se han almacenado los productos');
        } catch (MerqueoException $e) {
            \DB::rollBack();
            return Redirect::back()->with('error', $e->getMessage());
        } catch (\Exception $e) {
            \DB::rollBack();
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}