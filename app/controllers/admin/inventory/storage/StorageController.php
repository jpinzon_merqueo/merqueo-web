<?php

namespace admin\inventory\storage;

use ErrorLog;
use Illuminate\Database\QueryException;
use Request;
use View;
use Input;
use Route;
use Redirect;
use Session;
use admin\AdminController;
use WarehouseStorage;
use DB;
use StoreProduct;
use WarehouseStorageLog;
use PHPExcel\IOFactory;
use PHPExcel_Cell;
use PHPExcel_Reader_Excel2007;

class StorageController extends AdminController
{
    /**
     * Grilla de almacenamientos
     */
    public function index()
    {
        if (!Request::ajax()) {
            $data = [
                'cities' => $this->get_cities(),
                'warehouses' => $this->get_warehouses(),
                'title' => 'Almacenamiento de Productos'
            ];
            return View::make('admin.inventory.storage.index', $data);
        } else {
            $warehouseId = Input::get('warehouse_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $storages = WarehouseStorage::select('warehouse_storages.*', 'cities.city', 'warehouses.warehouse', 'admin.fullname AS admin', 'products.name', 'products.quantity AS product_quantity', 'products.unit', 'warehouse_storages.quantity')
                ->join('warehouses', 'warehouses.id', '=', 'warehouse_storages.warehouse_id')
                ->join('cities', 'cities.id', '=', 'warehouses.city_id')
                ->join('admin', 'admin.id', '=', 'warehouse_storages.admin_id')
                ->leftJoin('store_products', 'store_products.id', '=', 'warehouse_storages.store_product_id')
                ->leftJoin('products', 'products.id', '=', 'store_products.product_id')
                ->where('warehouses.id', $warehouseId)
                ->where(function ($query) use ($search) {
                    $query->where(DB::raw('CONCAT(warehouse_storages.position, warehouse_storages.position_height)'), 'LIKE', '%' . $search . '%');
                    $query->orWhere('products.name', 'LIKE', '%' . $search . '%');
                    $query->orWhere('products.reference', 'LIKE', '%' . $search . '%');
                })
                ->orderBy('warehouse_storages.id', 'desc')
                ->limit(80)->get();
            $data = [
                'storages' => $storages
            ];
            return View::make('admin.inventory.storage.index', $data)
                ->renderSections()['content'];
        }
    }

    /**
     * Nuevo movimiento de almacenamiento
     * +
     */
    public function add()
    {
        return View::make('admin.inventory.storage.form')
            ->with('title', 'Nuevo Almacenamiento de Productos')
            ->with('cities', $this->get_cities())
            ->with('warehouses', $this->get_warehouses());
    }

    /**
     * Buscar productos por ajax
     */
    public function searchProductsAjax()
    {
        $search = Input::get('s');
        $storeProducts = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
            ->where(function ($query) use ($search) {
                $query->orWhere('products.reference', 'LIKE', '%' . $search . '%');
                $query->orWhere('products.name', 'LIKE', '%' . $search . '%');
            })
            ->where('type', 'Simple')
            ->select('store_products.id', 'store_products.handle_expiration_date', 'products.image_small_url', 'products.reference', 'products.quantity', 'products.unit', 'products.name AS name')
            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
            ->orderBy('store_products.department_id', 'desc')
            ->orderBy('products.name', 'asc');
        if (Input::get('storage_type') == 'Seco')
            $storeProducts->where('store_products.storage', 'Seco');
        else $storeProducts->where(function ($query) use ($search) {
            $query->orWhere('store_products.storage', 'Congelado');
            $query->orWhere('store_products.storage', 'Refrigerado');
        });
        $storeProducts = $storeProducts->get();
        $data = [
            'products' => $storeProducts,
        ];
        return View::make('admin.inventory.storage.form', $data)
            ->renderSections()['product-table'];
    }

    /**
     * Obtener productos en posicion especifica por ajax
     */
    public function getProductsPositionAjax()
    {
        $warehouseId = Input::get('warehouse_id');
        $position = Input::get('position_from');
        $positionHeight = Input::get('position_height_from');
        $storeProducts = WarehouseStorage::getProductsPosition($warehouseId, $position, $positionHeight);
        $data = [
            'products' => $storeProducts,
        ];
        return View::make('admin.inventory.storage.form', $data)
            ->renderSections()['product-position-table'];
    }

    /**
     * Guardar movimiento de almacenamiento
     */
    public function save()
    {
        $input = Input::all();
        if (get_config_option('block_reception_return_storage', Input::get('warehouse_id'))) {
            return Redirect::route('adminInventoryStorage.add')
                ->with('type', 'failed')
                ->with(
                    'message',
                    'El almacenamiento de productos esta deshabilitado, por favor contacte al administrador.'
                )
                ->withInput();
        }
        $result = WarehouseStorage::validatePosition(
            Input::get('warehouse_id'),
            'storage',
            Input::get('storage_type'),
            Input::get('position_from'),
            Input::get('position_height_from')
        );
        if (!$result['status']) {
            return Redirect::route('adminInventoryStorage.add')
                ->with('type', 'failed')
                ->with('message', $result['message'])
                ->withInput();
        }
        if (!empty($input['position_from']) && !empty($input['position_height_from'])) {
            $validateBlockedPositions = \Warehouse::validateBlockedPositions(
                $input['warehouse_id'],
                'Almacenamiento',
                $input['storage_type'],
                strtoupper($input['position_height_from']),
                $input['position_from']
            );
            if ($validateBlockedPositions) {
                return Redirect::route('adminInventoryStorage.add')
                    ->with('type', 'failed')
                    ->with('message', 'La posicion y altura elegida se encuentra bloqueada debido a que se 
                    encuentra en conteo, intentalo más tarde.')
                    ->withInput();
            }
        }
        if (!empty($input['position_to']) && !empty($input['position_height_to'])) {
            $validateBlockedPositions = \Warehouse::validateBlockedPositions(
                $input['warehouse_id'],
                'Almacenamiento',
                $input['storage_type'],
                strtoupper($input['position_height_to']),
                $input['position_to']
            );
            if ($validateBlockedPositions) {
                return Redirect::route('adminInventoryStorage.add')
                    ->with('type', 'failed')
                    ->with(
                        'message',
                        'La posicion y altura elegida se encuentra bloqueada debido a que se encuentra en conteo, 
                        intentalo más tarde.'
                    )
                    ->withInput();
            }
        }
        try {
            DB::beginTransaction();
            //solo almacenar
            if (Input::get('movement') == 'storage') {
                $warehouseStorage = new WarehouseStorage;
                $warehouseStorage->warehouse_id = Input::get('warehouse_id');
                $warehouseStorage->admin_id = Session::get('admin_id');
                $warehouseStorage->storage_type = Input::get('storage_type');
                $warehouseStorage->type = Input::get('type');
                if (Input::get('type') == 'Producto') {
                    $warehouseStorage->store_product_id = Input::get('store_product_id');
                    $warehouseStorage->expiration_date = Input::get('expiration_date') ?
                        format_date('mysql', Input::get('expiration_date')) : null;
                }
                $warehouseStorage->quantity = Input::get('quantity_from');
                $warehouseStorage->position = Input::get('position_from');
                $warehouseStorage->position_height = Input::get('position_height_from');
                $warehouseStorage->save();
                if (Input::get('type') == 'Producto') {
                    $warehouseStorageLog = new WarehouseStorageLog;
                    $warehouseStorageLog->admin_id = $warehouseStorage->admin_id;
                    $warehouseStorageLog->warehouse_id = $warehouseStorage->warehouse_id;
                    $warehouseStorageLog->store_product_id = $warehouseStorage->store_product_id;
                    $warehouseStorageLog->module = 'Almacenamiento';
                    $warehouseStorageLog->movement = Input::get('movement');
                    $warehouseStorageLog->quantity_to = $warehouseStorage->quantity;
                    $warehouseStorageLog->position_to = $warehouseStorage->position;
                    $warehouseStorageLog->position_height_to = $warehouseStorage->position_height;
                    $warehouseStorageLog->save();
                }
            }
            //mover de posicion de almacenamiento
            if (Input::get('movement') == 'move') {
                //validar posicion
                $result = WarehouseStorage::validatePosition(
                    Input::get('warehouse_id'),
                    'storage',
                    Input::get('storage_type'),
                    Input::get('position_to'),
                    Input::get('position_height_to')
                );
                if (!$result['status']) {
                    return Redirect::route('adminInventoryStorage.add')
                        ->with('type', 'failed')
                        ->with('message', $result['message'])
                        ->withInput();
                }
                if (Input::get('type') == 'Producto') {
                    //obtener producto de la posicion origen
                    $warehouseStorage = WarehouseStorage::lockForUpdate()->find(Input::get('store_product_id'));
                    //obtener producto si ya existe en la posicion destino con la misma fecha de vencimiento
                    $storageProduct = WarehouseStorage::where(
                        'warehouse_storages.warehouse_id',
                        Input::get('warehouse_id')
                    )
                        ->where('warehouse_storages.type', Input::get('type'))
                        ->where('warehouse_storages.store_product_id', $warehouseStorage->store_product_id)
                        ->where('warehouse_storages.expiration_date', $warehouseStorage->expiration_date)
                        ->where('warehouse_storages.position', Input::get('position_to'))
                        ->where('warehouse_storages.position_height', Input::get('position_height_to'))
                        ->lockForUpdate()
                        ->first();
                } else {
                    //validar que existan productos en la posicion origen
                    $warehouseStorage = WarehouseStorage::select('quantity', 'expiration_date')
                        ->where('warehouse_storages.warehouse_id', Input::get('warehouse_id'))
                        ->where('warehouse_storages.type', Input::get('type'))
                        ->lockForUpdate()
                        ->first();
                    //obtener si ya existe en la posicion destino
                    $storageProduct = WarehouseStorage::where(
                        'warehouse_storages.warehouse_id',
                        Input::get('warehouse_id')
                    )
                        ->where('warehouse_storages.type', Input::get('type'))
                        ->where('warehouse_storages.position', Input::get('position_to'))
                        ->where('warehouse_storages.position_height', Input::get('position_height_to'))
                        ->lockForUpdate()
                        ->first();
                }
                if (!$warehouseStorage) {
                    return Redirect::route('adminInventoryStorage.add')
                        ->with('type', 'failed')
                        ->with(
                            'message',
                            'No se encontró el producto en la posición origen de almacenamiento para 
                            hacer el movimiento.'
                        )
                        ->withInput();
                }
                $expirationDate = $warehouseStorage->expiration_date;
                $storeProductId = $warehouseStorage->store_product_id;
                //validar unidades a mover
                if (Input::get('quantity_to') > $warehouseStorage->quantity) {
                    return Redirect::route('adminInventoryStorage.add')
                        ->with('type', 'failed')
                        ->with('message', 'Las unidades a mover es mayor a las que hay almacenadas.')
                        ->withInput();
                }
                //mover todas las unidades
                if (Input::get('quantity_to') == $warehouseStorage->quantity) {
                    $warehouseStorage->delete();
                } else {
                    $warehouseStorage->quantity -= Input::get('quantity_to');
                    $warehouseStorage->save();
                }
                //si ya existe en la posicion destino
                if ($storageProduct) {
                    $storageProduct->quantity += Input::get('quantity_to');
                    $storageProduct->save();
                } else {
                    $warehouseStorage = new WarehouseStorage;
                    $warehouseStorage->warehouse_id = Input::get('warehouse_id');
                    $warehouseStorage->admin_id = Session::get('admin_id');
                    if (Input::get('type') == 'Producto') {
                        $warehouseStorage->store_product_id = $storeProductId;
                        $warehouseStorage->expiration_date = $expirationDate;
                    }
                    $warehouseStorage->storage_type = Input::get('storage_type');
                    $warehouseStorage->type = Input::get('type');
                    $warehouseStorage->quantity = Input::get('quantity_to');
                    $warehouseStorage->position = Input::get('position_to');
                    $warehouseStorage->position_height = Input::get('position_height_to');
                    $warehouseStorage->save();
                }
                if (Input::get('type') == 'Producto') {
                    $warehouseStorageLog = new WarehouseStorageLog;
                    $warehouseStorageLog->admin_id = Session::get('admin_id');
                    $warehouseStorageLog->warehouse_id = $warehouseStorage->warehouse_id;
                    $warehouseStorageLog->store_product_id = $warehouseStorage->store_product_id;
                    $warehouseStorageLog->module = 'Almacenamiento';
                    $warehouseStorageLog->movement = Input::get('movement');
                    $warehouseStorageLog->quantity_from = Input::get('quantity_to');
                    $warehouseStorageLog->position_from = Input::get('position_from');
                    $warehouseStorageLog->position_height_from = Input::get('position_height_from');
                    $warehouseStorageLog->quantity_to = Input::get('quantity_to');
                    $warehouseStorageLog->position_to = Input::get('position_to');
                    $warehouseStorageLog->position_height_to = Input::get('position_height_to');
                    $warehouseStorageLog->save();
                }
            }
            //sacar de posicion de almacenamiento
            if (Input::get('movement') == 'remove') {
                //validar posicion
                $result = WarehouseStorage::validatePosition(
                    Input::get('warehouse_id'),
                    'storage',
                    Input::get('storage_type'),
                    Input::get('position_from'),
                    Input::get('position_height_from')
                );
                if (!$result['status']) {
                    return Redirect::route('adminInventoryStorage.add')
                        ->with('type', 'failed')
                        ->with('message', $result['message'])
                        ->withInput();
                }
                if (Input::get('type') == 'Producto') {
                    //obtener producto de la posicion origen
                    $warehouseStorage = WarehouseStorage::lockForUpdate()->find(Input::get('store_product_id'));
                } else {
                    //validar que existan productos en la posicion origen
                    $warehouseStorage = WarehouseStorage::select('id', 'quantity', 'expiration_date')
                        ->where('warehouse_storages.warehouse_id', Input::get('warehouse_id'))
                        ->where('warehouse_storages.type', Input::get('type'))
                        ->where('warehouse_storages.position', Input::get('position_from'))
                        ->where('warehouse_storages.position_height', Input::get('position_height_from'))
                        ->lockForUpdate()
                        ->first();
                }
                if (!$warehouseStorage) {
                    return Redirect::route('adminInventoryStorage.add')
                        ->with('type', 'failed')
                        ->with(
                            'message',
                            'No se encontró el producto en la posición origen de almacenamiento para 
                            hacer el movimiento.'
                        )
                        ->withInput();
                }
                //validar unidades a eliminar
                if (Input::get('quantity_from') > $warehouseStorage->quantity) {
                    return Redirect::route('adminInventoryStorage.add')
                        ->with('type', 'failed')
                        ->with('message', 'Las unidades a eliminar es mayor a las que hay almacenadas.')
                        ->withInput();
                }
                //eliminar todas las unidades
                if (Input::get('quantity_from') == $warehouseStorage->quantity) {
                    $warehouseStorage->delete();
                } else {
                    $warehouseStorage->quantity -= Input::get('quantity_from');
                    $warehouseStorage->save();
                }
                if (Input::get('type') == 'Producto') {
                    $warehouseStorageLog = new WarehouseStorageLog;
                    $warehouseStorageLog->admin_id = Session::get('admin_id');
                    $warehouseStorageLog->warehouse_id = $warehouseStorage->warehouse_id;
                    $warehouseStorageLog->store_product_id = $warehouseStorage->store_product_id;
                    $warehouseStorageLog->module = 'Almacenamiento';
                    $warehouseStorageLog->movement = Input::get('movement');
                    $warehouseStorageLog->quantity_from = Input::get('quantity_from');
                    $warehouseStorageLog->position_from = Input::get('position_from');
                    $warehouseStorageLog->position_height_from = Input::get('position_height_from');
                    $warehouseStorageLog->save();
                }
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                return Redirect::route('adminInventoryStorage.add')
                    ->with('type', 'failed')
                    ->with('message', 'Otro proceso se está ejecutando sobre estos productos, 
                    espera un momento para volver a intentarlo.')
                    ->withInput();
            }
            ErrorLog::add($e);
            return Redirect::route('adminInventoryStorage.add')
                ->with('type', 'failed')
                ->with('message', $e->getMessage())
                ->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                \ErrorLog::add($e, 513);
                return Redirect::route('adminInventoryStorage.add')
                    ->withErrors([
                        'Exception',
                        'Otro proceso se está ejecutando sobre estos productos, 
                    espera un momento para volver a intentarlo.'
                    ]);
            }
            \ErrorLog::add($e);
            return Redirect::route('adminInventoryStorage.add')
                ->with('type', 'failed')
                ->with('message', $e->getMessage())
                ->withInput();
        }
        return Redirect::route('adminInventoryStorage.index')
            ->with('type', 'success')
            ->with('message', 'Cambios de almacenamiento realizados con éxito.');
    }

    /**
     * Importar archivo para almacenamiento o eliminar almacenamiento por posicion
     */
    public function importRemoveStorage()
    {
        if (!Request::isMethod('post')) {
            $cities = $this->get_cities();
            $warehouses = $this->get_warehouses(Session::get('admin_city_id'));
            return View::make('admin.inventory.storage.import_remove_storage')
                ->with('title', 'Importar o Eliminar Almacenamiento')
                ->with('cities', $cities)
                ->with('warehouses', $warehouses);
        } else {
            //eliminar almacenamiento
            if (Input::get('action') == 'remove') {
                //validar posicion
                $result = WarehouseStorage::validatePosition(Input::get('warehouse_id'), 'storage', Input::get('storage_type'), Input::get('position_from'), Input::get('position_height_from'));
                if (!$result['status']) {
                    return Redirect::route('adminInventoryStorage.importRemove')
                        ->with('type', 'failed')
                        ->with('message', $result['message'])
                        ->withInput();
                }
                $validateBlockPosition = \Warehouse::validateBlockedPositions(Input::get('warehouse_id'), 'Almacenamiento', Input::get('storage_type'), Input::get('position_height_from'), Input::get('position_from'));
                if ($validateBlockPosition) {
                    return Redirect::route('adminInventoryStorage.importRemove')
                        ->with('type', 'failed')
                        ->with('message', 'La posición elegida se encuentra bloqueada debido a un conteo por posición activo en el momento, intenta más tarde.')
                        ->withInput();
                }
                //registrar log
                $warehouseStorages = WarehouseStorage::where('warehouse_id', Input::get('warehouse_id'))
                    ->where('storage_type', Input::get('storage_type'))
                    ->where('position', Input::get('position_from'))
                    ->where('position_height', Input::get('position_height_from'))
                    ->get();
                if (count($warehouseStorages)) {
                    foreach ($warehouseStorages as $warehouseStorage) {
                        if ($warehouseStorage->type == 'Producto') {
                            $warehouseStorageLog = new WarehouseStorageLog;
                            $warehouseStorageLog->admin_id = Session::get('admin_id');
                            $warehouseStorageLog->warehouse_id = Input::get('warehouse_id');
                            $warehouseStorageLog->store_product_id = $warehouseStorage->store_product_id;
                            $warehouseStorageLog->module = 'Almacenamiento';
                            $warehouseStorageLog->movement = 'remove';
                            $warehouseStorageLog->quantity_from = $warehouseStorage->quantity;
                            $warehouseStorageLog->position_from = Input::get('position_from');
                            $warehouseStorageLog->position_height_from = Input::get('position_height_from');
                            $warehouseStorageLog->save();
                        }
                    }
                }
                $affectedRows = WarehouseStorage::where('warehouse_id', Input::get('warehouse_id'))
                    ->where('storage_type', Input::get('storage_type'))
                    ->where('position', Input::get('position_from'))
                    ->where('position_height', Input::get('position_height_from'))
                    ->delete();
                return Redirect::route('adminInventoryStorage.importRemove')
                    ->with('type', 'success')
                    ->with('message', 'Se han eliminado <b>' . $affectedRows . '</b> almacenamientos de la posición ' . Input::get('position') . Input::get('position_height') . ' con éxito.');
            }
            //eliminar almacenamiento
            if (Input::get('action') == 'import') {
                if (Input::hasFile('file_storage')) {
                    ini_set('memory_limit', '1024M');
                    set_time_limit(0);
                    $extensionList = ['xlsx', 'xls'];
                    $fileStorage = Input::file('file_storage');
                    if (!file_exists($fileStorage->getRealPath()))
                        return Redirect::back()->with('error', 'No file uploaded');

                    $extensionObj = explode('.', $fileStorage->getClientOriginalName());
                    $isCorrectExtension = in_array($extensionObj[1], $extensionList);
                    if ($isCorrectExtension) {
                        $filePath = $fileStorage->getRealPath();
                        $objReader = new PHPExcel_Reader_Excel2007();
                        $objPHPExcel = $objReader->load($filePath);
                        //Establesco la primera hoja como activa
                        $objPHPExcel->setActiveSheetIndex(0);
                        //obtengo el número de filas
                        $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
                        //obtengo el número de columnas
                        $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel
                            ->getActiveSheet()
                            ->getHighestColumn());
                        //array con el listado de productos y cantidades
                        $storages = array();
                        for ($i = 2; $i <= $rows; $i++) {
                            $storeProductId = $objPHPExcel
                                ->getActiveSheet()
                                ->getCellByColumnAndRow(0, $i)
                                ->getValue();
                            $quantity = $objPHPExcel->getActiveSheet()
                                ->getCellByColumnAndRow(1, $i)
                                ->getValue();
                            $expirationDate = $objPHPExcel->getActiveSheet()
                                ->getCellByColumnAndRow(2, $i)
                                ->getValue();
                            $position = $objPHPExcel->getActiveSheet()
                                ->getCellByColumnAndRow(3, $i)
                                ->getValue();
                            $positionHeight = $objPHPExcel->getActiveSheet()
                                ->getCellByColumnAndRow(4, $i)
                                ->getValue();
                            //validar posicion
                            $result = WarehouseStorage::validatePosition(Input::get('warehouse_id'), 'storage', Input::get('storage_type'), $position, $positionHeight);
                            if (!$result['status'])
                                return Redirect::route('adminInventoryStorage.importRemove')
                                ->with('type', 'failed')
                                ->with('message', $result['message'])
                                ->withInput();
                            //validar producto
                            $storeProduct = StoreProduct::join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                                ->where('store_products.id', $storeProductId)
                                ->where('warehouse_id', Input::get('warehouse_id'))
                                ->first();
                            if (!$storeProduct)
                                return Redirect::route('adminInventoryStorage.importRemove')
                                ->with('type', 'failed')
                                ->with('message', 'El producto con ID ' . $storeProductId . ' no esta asociado a la bodega.')
                                ->withInput();
                            $storages[] = array(
                                'store_product_id' => $storeProductId,
                                'quantity' => $quantity,
                                'expiration_date' => $expirationDate,
                                'position' => $position,
                                'position_height' => $positionHeight,
                            );
                        }
                        $cont = 0;
                        if (count($storages)) {
                            foreach ($storages as $storage) {
                                $warehouseStorage = new WarehouseStorage;
                                $warehouseStorage->warehouse_id = Input::get('warehouse_id');
                                $warehouseStorage->admin_id = Session::get('admin_id');
                                $warehouseStorage->storage_type = Input::get('storage_type');
                                $warehouseStorage->type = 'Producto';
                                $warehouseStorage->store_product_id = $storage['store_product_id'];
                                $warehouseStorage->expiration_date = empty($storage['expiration_date']) ? null : $storage['expiration_date'];
                                $warehouseStorage->quantity = $storage['quantity'];
                                $warehouseStorage->position = $storage['position'];
                                $warehouseStorage->position_height = $storage['position_height'];
                                $warehouseStorage->save();
                                $cont++;
                                $warehouseStorageLog = new WarehouseStorageLog;
                                $warehouseStorageLog->admin_id = Session::get('admin_id');
                                $warehouseStorageLog->warehouse_id = Input::get('warehouse_id');
                                $warehouseStorageLog->store_product_id = $storage['store_product_id'];
                                $warehouseStorageLog->module = 'Almacenamiento';
                                $warehouseStorageLog->movement = 'storage';
                                $warehouseStorageLog->quantity_to = $storage['quantity'];
                                $warehouseStorageLog->position_to = $storage['position'];
                                $warehouseStorageLog->position_height_to = $storage['position_height'];
                                $warehouseStorageLog->save();
                            }
                        }
                        return Redirect::route('adminInventoryStorage.importRemove')
                            ->with('type', 'success')
                            ->with('message', 'Se han importado <b>' . $cont . '</b> almacenamientos con éxito.');
                    } else {
                        return Redirect::route('adminInventoryStorage.importRemove')
                            ->with('type', 'failed')
                            ->with('message', 'La extensión del archivo es invalida.')
                            ->withInput();
                    }
                } else {
                    return Redirect::route('adminInventoryStorage.importRemove')
                        ->with('type', 'failed')
                        ->with('message', 'No se encontro el archivo.')
                        ->withInput();
                }
            }
        }
    }
}
