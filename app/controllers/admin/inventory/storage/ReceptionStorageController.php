<?php

namespace admin\inventory\storage;

use Illuminate\Database\QueryException;
use View;
use Input;
use Request;
use Response;
use Session;
use DB;
use admin\AdminController;
use StoreProduct;
use StoreProductWarehouse;
use WarehouseStorage;
use Warehouse;
use ProviderOrderReceptionDetail;
use ProviderOrderReceptionDetailProductGroup;
use ProviderOrderReception;
use ProviderOrder;
use SupplierStorageLog;
use WarehouseStorageLog;
use ProviderOrderReceptionLog;

class ReceptionStorageController extends AdminController
{

    public function index()
    {
        if (!Request::ajax()) {
            $storages = StoreProduct::select(DB::raw('DISTINCT storage AS name'))->get();
            return View::make('admin.inventory.receptions.index')
                ->with('title', 'Almacenamiento de recibos')
                ->with('storages', $storages);
        }
    }

    /**
     * Función que retorna la información del recibo
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProviderOrderReceptionAjax()
    {
        if (Request::ajax()) {
            $invoiceNumber = Input::get('invoice_number');
            $orderProviderReception = WarehouseStorage::getProviderOrderReception($invoiceNumber);

            if ($orderProviderReception['status']) {
                $orderProviderReception = $orderProviderReception['data'];
                $orderProviderReception['received_date'] = date(
                    'd M Y g:i a',
                    strtotime($orderProviderReception['received_date'])
                );
                $orderProviderReception['storage'] = Input::get('storage');
                return Response::json([
                    'result' => true,
                    'msg' => View::make(
                        'admin.inventory.receptions.index',
                        $orderProviderReception
                    )->renderSections()['order-receptions-details']
                ]);
            } else {
                return Response::json(['result' => false, 'msg' => $orderProviderReception['message']]);
            }
        }
    }

    /**
     * Función que retorna la información de los productos asociados a la orden
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProviderOrderReceptionProductDetailAjax()
    {
        if (Request::ajax()) {
            $search = Input::has('search') ? Input::get('search') : null;
            $products = WarehouseStorage::getProviderOrderReceptionProducts(
                Input::get('id_provider_order_reception'),
                Input::get('storage'),
                $search
            );

            if (empty($products)) {
                return Response::json([
                    'result' => false,
                    'msg' => 'No hay un producto en este recibo que coincida con la busqueda'
                ]);
            }
            $data = [
                'products' => $products
            ];
            return Response::json([
                'result' => true,
                'id' => '',
                'msg' => View::make(
                    'admin.inventory.receptions.index',
                    $data
                )->renderSections()['order-receptions-product-details-search'],
            ]);
        }
    }

    /**
     * Función que realiza los cambios de estado de los productos del recibo
     */
    public function processStorageAjax()
    {
        if (Request::ajax()) {
            try {
                $validatePosition = WarehouseStorage::validatePosition(
                    Input::get('warehouse_id'),
                    'storage',
                    Input::get('storage'),
                    Input::get('position'),
                    strtoupper(Input::get('height_position'))
                );
                if (!$validatePosition['status']) {
                    return Response::json([
                        'result' => false,
                        'msg' => $validatePosition['message'],
                    ]);
                }

                $validateBlockPosition = Warehouse::validateBlockedPositions(
                    Input::get('warehouse_id'),
                    'Almacenamiento',
                    Input::get('storage'),
                    strtoupper(Input::get('height_position')),
                    Input::get('position')
                );
                if ($validateBlockPosition) {
                    return Response::json([
                        'result' => false,
                        'msg' => 'La posición que elegiste se encuentra bloqueda debido a un conteo de productos intenta mas tarde.',
                    ]);
                }

                DB::beginTransaction();
                $storeProductIds = Input::get('product_storage_id');
                foreach ($storeProductIds as $storeProductId) {
                    $expirationDate = empty(Input::get('expiration_date_' . $storeProductId))
                        ? null : date('Y-m-d', strtotime(Input::get('expiration_date_' . $storeProductId)));
                    $positionHeight = trim(strtoupper(Input::get('height_position')));
                    $position = Input::get('position');
                    $warehouseId = Input::get('warehouse_id');

                    $warehouseStorage = WarehouseStorage::getProductLote(
                        $storeProductId,
                        $warehouseId,
                        $position,
                        $positionHeight,
                        $expirationDate
                    );

                    if (!empty($warehouseStorage) > 0) {
                        $warehouseStorage->quantity += Input::get('user_quantity_to_stowage_' . $storeProductId);
                        $warehouseStorage->save();
                    } else {
                        $warehouseStorage = new WarehouseStorage;
                        $warehouseStorage->warehouse_id = $warehouseId;
                        $warehouseStorage->admin_id = Session::get('admin_id');
                        $warehouseStorage->store_product_id = $storeProductId;
                        $warehouseStorage->storage_type = Input::get('storage');
                        $warehouseStorage->type = 'Producto';
                        $warehouseStorage->quantity = Input::get('user_quantity_to_stowage_' . $storeProductId);
                        $warehouseStorage->expiration_date = $expirationDate;
                        $warehouseStorage->position = $position;
                        $warehouseStorage->position_height = $positionHeight;
                        $warehouseStorage->save();
                    }

                    $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(
                        Input::get('warehouse_id'),
                        $storeProductId
                    );

                    $storeProductWarehouse->reception_stock -= Input::get(
                        'user_quantity_to_stowage_' . $storeProductId
                    );
                    $storeProductWarehouse->save();

                    if (Input::get('type_' . $storeProductId) == 'Proveedor') {
                        $parentStoreProductId = Input::get('parent_store_product_id_' . $storeProductId);
                        $providerOrderReceptionDetail = ProviderOrderReceptionDetail::Where(
                            'reception_id',
                            Input::get('order_reception_id')
                        )
                            ->where('store_product_id', $parentStoreProductId)
                            ->get()
                            ->first();

                        $providerOrderReceptionDetailProductGroup = ProviderOrderReceptionDetailProductGroup::Where(
                            'reception_detail_id',
                            $providerOrderReceptionDetail->id
                        )
                            ->where('store_product_id', $storeProductId)
                            ->lockForUpdate()
                            ->first();

                        if ($providerOrderReceptionDetailProductGroup->quantity_storaged
                            >= $providerOrderReceptionDetailProductGroup->quantity_received) {
                            DB::rollback();
                            return Response::json([
                                'result' => false,
                                'msg' => "El producto ya fue almacenado en su totalidad.",
                                'response' => 'clear'
                            ]);
                        }
                        $providerOrderReceptionDetailProductGroup
                            ->quantity_storaged += Input::get('user_quantity_to_stowage_' . $storeProductId);

                        if ($providerOrderReceptionDetailProductGroup->quantity_storaged
                            > $providerOrderReceptionDetailProductGroup->quantity_received) {
                            DB::rollback();
                            return Response::json([
                                'result' => false,
                                'msg' => "Las unidades a almacenar no son consistentes con las recibidas.",
                                'response' => 'clear'
                            ]);
                        }

                        $providerOrderReceptionDetailProductGroup->save();
                        $storagedQuantity = $providerOrderReceptionDetailProductGroup->quantity_storaged;
                    } else {
                        $providerOrderReceptionDetail = ProviderOrderReceptionDetail::Where(
                            'reception_id',
                            Input::get('order_reception_id')
                        )
                            ->where('store_product_id', $storeProductId)
                            ->lockForUpdate()
                            ->first();
                        if ($providerOrderReceptionDetail->quantity_storaged
                            >= $providerOrderReceptionDetail->quantity_received) {
                            DB::rollback();
                            return Response::json([
                                'result' => false,
                                'msg' => "El producto ya fue almacenado en su totalidad.",
                                'response' => 'clear'
                            ]);
                        }
                        $providerOrderReceptionDetail->quantity_storaged += Input::get(
                            'user_quantity_to_stowage_' . $storeProductId
                        );

                        if ($providerOrderReceptionDetail->quantity_storaged
                            > $providerOrderReceptionDetail->quantity_received) {
                            DB::rollback();
                            return Response::json([
                                'result' => false,
                                'msg' => "Las unidades a almacenar no son consistentes con las recibidas.",
                                'response' => 'clear'
                            ]);
                        }
                        $providerOrderReceptionDetail->save();
                        $storagedQuantity = $providerOrderReceptionDetail->quantity_storaged;
                    }

                    SupplierStorageLog::log(
                        Input::get('order_reception_id'),
                        $storeProductWarehouse->id,
                        Input::get('user_quantity_to_stowage_' . $storeProductId),
                        $storagedQuantity,
                        Input::get('storage'),
                        $position,
                        $positionHeight
                    );

                    $warehouseStorageLog = new WarehouseStorageLog;
                    $warehouseStorageLog->admin_id = Session::get('admin_id');
                    $warehouseStorageLog->warehouse_id = $storeProductWarehouse->warehouse_id;
                    $warehouseStorageLog->store_product_id = $storeProductWarehouse->store_product_id;
                    $warehouseStorageLog->module = 'Almacenamiento de Recibos';
                    $warehouseStorageLog->movement = 'storage';
                    $warehouseStorageLog->quantity_to = Input::get('user_quantity_to_stowage_' . $storeProductId);
                    $warehouseStorageLog->position_to = $position;
                    $warehouseStorageLog->position_height_to = $positionHeight;
                    $warehouseStorageLog->save();
                }

                $pendingQuantity = WarehouseStorage::getQuantityProducts(Input::get('order_reception_id'));
                if (is_array($pendingQuantity) && !$pendingQuantity['result']) {
                    DB::rollback();
                    return Response::json([
                        'result' => false,
                        'msg' => $pendingQuantity['message'],
                        'response' => 'clear'
                    ]);
                }
                if ($pendingQuantity <= 0) {
                    $providerOrderReception = ProviderOrderReception::find(Input::get('order_reception_id'));
                    $providerOrderReception->status = 'Almacenado';
                    $providerOrderReception->storage_date = date('Y-m-d H:i:s');
                    $providerOrderReception->save();

                    $log = new ProviderOrderReceptionLog();
                    $log->type = "Estado de recibo de bodega # {$providerOrderReception->id} actualizado: Almacenado";
                    $log->admin_id = Session::get('admin_id');
                    $log->reception_id = $providerOrderReception->id;
                    $log->save();


                    ProviderOrder::updateStatus($providerOrderReception->provider_order_id);

                    $this->admin_log(
                        'provider_order_reception',
                        Session::get('admin_id'),
                        'Actualiza el estado del recibo ' . Input::get('order_reception_id') . ' a Almacenado '
                    );
                }

                DB::commit();

                return Response::json([
                    'result' => true,
                    'msg' => 'Los productos se almacenaron con éxito.',
                ]);
            } catch (QueryException $e) {
                DB::rollBack();
                if ($e->getCode() == '40001') {
                    \ErrorLog::add($e, 513);
                    return Response::json([
                        'result' => false,
                        'msg' =>  'Otro proceso se está ejecutando sobre estos productos, 
                    espera un momento para volver a intentarlo.',
                    ]);
                }
                \ErrorLog::add($e, 513);
                return Response::json([
                    'result' => false,
                    'msg' =>  $e->getMessage(),
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json([
                    'result' => false,
                    'msg' => $e->getMessage() . ' ' . $e->getLine(),
                ]);
            }
        }
    }
}

//'Alistamiento','Almacenamiento'

//'En proceso','En alistamiento','Surtido','Almacenado','No requiere surtir'