<?php
namespace admin\inventory\provider_order;


use admin\AdminController;
use Request;
use View;
use Input;
use Redirect;
use Session;
use DB;
use City;
use Product;
use Response;
use Config;
use Provider;
use ProviderOrderReceptionDetail;
use ProviderOrder;
use Route;
use ProviderOrderDetail;
use ProviderOrderNote;
use ProviderOrderLog;
use Order;
use Store;
use Department;
use Shelf;
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Writer_Excel2007;
use Illuminate\Support\Facades\Validator;
use ProviderOrderReception;
use Pdf;
use PHPExcel_Cell_DataType;
use ProviderOrderInvoice;
use ProviderOrderDetailProductGroup;
use Event;
use ProviderOrderEventHandler;
use StoreProduct;
use StoreProductGroup;
use PHPExcel_Reader_Excel2007;
use PHPExcel_Cell;
use Warehouse;
use ProviderContact;
use Zend\Json\Json;

class AdminProviderOrderController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new ProviderOrderEventHandler);
    }

    /**
     * Grilla de ordenes de compra
     */
    public function index()
    {
        if (!Request::ajax()) {
            if (Session::has('provider_order_id')) {
                $providerOrderId = Session::get('provider_order_id');
                $providerOrders = ProviderOrder::with('provider')
                ->join('providers', 'providers.id', '=', 'provider_orders.provider_id')
                ->join('provider_order_details', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
                ->join('warehouses', 'provider_orders.warehouse_id', '=', 'warehouses.id')
                ->where('provider_orders.id', $providerOrderId)
                ->select(
                    'provider_orders.id',
                    DB::raw("SUM(provider_order_details.quantity_order) AS quantity"),
                    'provider_orders.status',
                    DB::raw("DATE_FORMAT(provider_orders.delivery_date, '%d/%m/%Y %H:%i:%s') AS delivery_date"),
                    'providers.name',
                    'providers.provider_type',
                    'providers.maximum_order_report_time',
                    'providers.delivery_time_hours',
                    'providers.provider_delivery_time',
                    'warehouses.warehouse',
                    'provider_orders.warehouse_id'
                )
                ->groupBy('provider_order_id')
                ->get()
                ->first();
                $deliverWindows = \ProviderOrderDeliveryWindow::where('status', 1)->where('warehouse_id', $providerOrders->warehouse_id)->get();
                return View::make('admin.providers.orders.orders')
                    ->with('title', 'Ordenes de Compra - Fechas de Entrega')
                    ->with('providerOrders', $providerOrders)
                    ->with('deliveryWindows', $deliverWindows);
            }

            $warehouses = $this->get_warehouses();
            $users = \Admin::select('fullname', 'id')->orderBy('fullname')->get();
            $providers = Provider::where('status', 1)->orderBy('name')->get();

            return View::make('admin.providers.orders.orders')
                ->with('title', 'Ordenes de Compra')
                ->with('cities', $this->get_cities())
                ->with('providers', $providers)
                ->with('warehouses', $warehouses)
                ->with('users', $users);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            $provider_orders = ProviderOrder::with('provider')
            ->join('providers', 'providers.id', '=', 'provider_orders.provider_id')
            ->join('provider_order_details', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
            ->join('admin', 'provider_orders.admin_id', '=', 'admin.id')
            ->join('warehouses', 'provider_orders.warehouse_id', '=', 'warehouses.id')
            ->join('cities', 'warehouses.city_id', '=', 'cities.id')
            ->where(function ($query) use ($search) {
                $query->where('provider_orders.id', 'LIKE', '%'.$search.'%');
                $query->orWhere('provider_order_details.product_name', 'LIKE', '%'.$search.'%');
            })
            ->select(
                'provider_orders.id',
                DB::raw("SUM(provider_order_details.quantity_order) AS quantity"),
                'provider_orders.status',
                'admin.fullname AS admin',
                'provider_orders.date',
                'provider_orders.delivery_date',
                'providers.name',
                'providers.address',
                'providers.provider_type',
                'cities.city',
                'warehouses.warehouse'
            );

            //filtros
            if (Input::has('provider')) {
                $provider_orders->where('providers.id', Input::get('provider'));
            }

            if (Input::has('status')) {
                $status = Input::get('status');
                $statuses = explode(',', $status);
                unset($statuses[count($statuses) - 1]);
                $provider_orders->where(function ($query) use ($statuses) {
                    foreach ($statuses as $status) {
                        $query->orWhere('provider_orders.status', $status);
                    }
                });
            }

            if (Input::get('city_id')) {
                $provider_orders->where('warehouses.city_id', Input::get('city_id'));
            }

            if (Input::get('warehouse_id')) {
                $provider_orders->where('provider_orders.warehouse_id', Input::get('warehouse_id'));
            }

            if (Input::has('start_delivery_date') && Input::has('end_delivery_date')) {
                $start_delivery_date = format_date('mysql', Input::get('start_delivery_date'));
                $end_delivery_date = format_date('mysql', Input::get('end_delivery_date'));
                $provider_orders->whereBetween(DB::raw('DATE(provider_orders.delivery_date)'), [$start_delivery_date, $end_delivery_date]);
            }

            if (Input::has('provider_type')) {
                $provider_orders->where('providers.provider_type', Input::get('provider_type'));
            }

            if (Input::get('user_id')) {
                $provider_orders->where('admin.id', Input::get('user_id'));
            }

            $provider_orders = $provider_orders->groupBy('provider_orders.id')->orderBy('provider_orders.id', 'desc')->paginate(50);

        }

        return View::make('admin.providers.orders.ajax.orders-grid', ['provider_orders' => $provider_orders])->render();
    }

    /**
     * Detalle de una orden de servicio
     */
    public function details($id)
    {
        $providerOrder = ProviderOrder::with(
            'warehouse.city',
            'provider.contacts',
            'admin',
            'providerOrderReception',
            'providerOrderDeliveryWindow'
        )
        ->find($id);
        $providerOrder->provider->contacts = $providerOrder
        ->provider
        ->contacts
        ->filter(function ($contact) {
            return $contact->status == 1;
        });

        if (!$providerOrder) {
            return Redirect::route('adminProviderOrders.index')->with('error', 'No existe la orden de compra.');
        }

        $providerOrderNotes = ProviderOrderNote::join('admin', 'admin.id', '=', 'provider_order_notes.admin_id')
                                                 ->join('provider_orders', 'provider_order_notes.provider_order_id', '=', 'provider_orders.id')
                                                 ->where('provider_order_notes.provider_order_id', $providerOrder->id)
                                                 ->select('provider_order_notes.created_at', 'provider_order_notes.description', 'admin.fullname')
                                                 ->orderBy('provider_order_notes.created_at', 'desc')->get();

        $providerOrderLog = ProviderOrderLog::select(DB::raw("admin.fullname AS 'admin_name'"), 'provider_order_logs.type', 'provider_order_logs.created_at')
                                              ->leftJoin('admin', 'provider_order_logs.admin_id', '=', 'admin.id')
                                              ->where('provider_order_logs.provider_order_id', $providerOrder->id)
                                              ->get();

        $providerOrderInvoices = ProviderOrderInvoice::where('provider_order_id', $providerOrder->id)->get();
        $providerOrderDeliveryWindows = \ProviderOrderDeliveryWindow::where('warehouse_id', $providerOrder->warehouse_id)->where('status', 1)->get();

        $cities = City::all();
        $stores = Store::getActiveStores($providerOrder->warehouse->city->id);

        $data = [
            'title' => 'Orden de Compra',
            'provider_order' => $providerOrder,
            'cities' => $cities,
            'provider_order_log' => $providerOrderLog,
            'provider_order_notes' => $providerOrderNotes,
            'provider_order_invoices' => $providerOrderInvoices,
            'provider_order_delivery_windows' => $providerOrderDeliveryWindows,
            'provider_order_reject_reasons' => \ProviderOrderRejectReason::where('status', 1)->get(),
            'store_id' => $stores->first()->id
        ];

        return View::make('admin.providers.orders.order_details', $data);
    }

    /**
     * Funcion para envio de correo con adjuntos
     */
    public function send_provider_email($id)
    {
        $provider_order = $provider_order = ProviderOrder::with(
            'warehouse.city',
            'provider.contacts',
            'admin',
            'providerOrderReception'
        )
        ->find($id);

        $new_status = 'Enviada';
        if ($new_status == 'Enviada') {
            if ($provider_order->status != 'Iniciada') {
                return Redirect::back()->with('error', 'El estado actual de la orden de compra no es valido.');
            }
        }

        $contacts = Input::get('contact');
        $add_contact = Input::get('add_contact');
        $message = nl2br(Input::get('message'));

        $contacts = ProviderContact::whereIn('id', $contacts)
        ->select(DB::raw('fullname AS name'), 'email')
        ->get()
        ->toArray();

        if (!empty($add_contact)) {
            $add_contact = explode(',', $add_contact);
        }

        foreach ($contacts as $key => $contact) {
            $data['contacto_'.$key] = $contact['email'];
            $rule['contacto_'.$key] = 'required|email';
        }

        $mail['to'] = $contacts;

        if (is_array($add_contact)) {
            foreach ($add_contact as $key => $contact) {
                $mail['to'][] = [
                    'email' => $contact,
                    'name' => $contact
                ];
                $data['contacto_adicional_'.$key] = $contact;
                $rule['contacto_adicional_'.$key] = 'required|email';
            }
        }

        $validator = Validator::make(
            $data,
            $rule
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($validator);
        }

        $mail['template_name'] = 'emails.provider_order_module';
        $mail['subject'] = "{$provider_order->provider->name} - Órden de compra #{$provider_order->id} - {$provider_order->warehouse->warehouse}";
        $mail['vars']['content'] = $message;

        $file = $this->get_excel_provier_order($provider_order);
        $pdf = Pdf::generate_provider_order($provider_order->id, null, false);
        $files = [
            [
                'type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'name' => $file['client_original_name'],
                'content' => base64_encode(file_get_contents($file['real_path']))
            ],
            [
                'type' => 'application/pdf',
                'name' => 'orden_de_compra',
                'content' => base64_encode($pdf)
            ]
        ];
        $result = send_mail($mail, $files, false, 'compras@merqueo.com');


        $provider_order->status = $new_status;
        $provider_order->save();

        //log
        $evet_result = Event::fire('providerOrder.updatedStatus', [[
                                                                    'status' => $new_status,
                                                                    'admin_id' => Session::get('admin_id'),
                                                                    'provider_order_id' => $provider_order->id
                                                                ]]);

        return Redirect::back()->with('success', 'Correo enviado.');
    }

    /**
     * Función para obtener el archivo excel
     */
    public function get_excel_provier_order($provider_order)
    {
        $provider_order_products = ProviderOrderDetail::where('provider_orders.id', $provider_order->id)
                                                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
                                                        ->select(
                                                            DB::raw('provider_orders.id AS "Orden de compra"'),
                                                            DB::raw("DATE_FORMAT(provider_orders.date, '%d/%m/%Y %H:%i:%s') AS Fecha"),
                                                            DB::raw('provider_order_details.product_name AS Producto'),
                                                            DB::raw('provider_order_details.reference AS Referencia'),
                                                            DB::raw('provider_order_details.plu AS PLU'),
                                                            DB::raw('provider_order_details.quantity_pack AS "Embalaje solicitado"'),
                                                            DB::raw('provider_order_details.pack_description AS "Tipo de embalaje"'),
                                                            DB::raw('provider_order_details.quantity_order AS "Unidades solicitadas"')
                                                        )
                                                        ->get()->toArray();
        if ($n = count($provider_order_products)) {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)) {
                while (false !== ($file = readdir($gestor))) {
                    if (strstr($file, Session::get('admin_username'))) {
                        remove_file($path.$file);
                    }
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Orden de compra');
            $objPHPExcel->getProperties()->setSubject('Orden de compra');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb'=>'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($provider_order_products[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach ($provider_order_products as $row) {
                $i = 0;
                $type = PHPExcel_Cell_DataType::TYPE_STRING;
                foreach ($row as $key => $value) {
                    if ($key == "Referencia") {
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    }
                    $i++;
                }
                $objPHPExcel->getActiveSheet()->getCell("D".$j)->setDataType($type);
                $j++;
            }
            //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Orden de compra #'.$provider_order->id);
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s', time()).'.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            return $file;
            // $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        }
    }

    /**
     * Formulario de creación de órdenes de compra
     * @return View
     */
    public function creation_form()
    {
        $admin_city = Session::get('admin_city_id');
        $cities = $this->get_cities();
        $first_city = $cities[0]->id;
        foreach ($cities as $key => $city) {
            if ($city->id == $admin_city) {
                $first_city = $admin_city;
            }
        }

        $providers = Provider::where('status', 1)
            ->where('type', 'Para bodega')
            ->orderBy('name')
            ->select('id', 'name')
            ->get();

        $providers->each(function ($provider) use ($first_city) {
            $provider_order_today = $provider->providerOrders()
                ->whereDate('date', '=', date('Y-m-d'))
                ->whereStatus('Enviada')
                ->whereHas('warehouse', function ($q) use ($first_city) {
                    $q->where('city_id', $first_city);
                })
                ->get()
                ->count();
            $provider->count_today = $provider_order_today;
        });

        $providers_missing = Provider::where('status', 1)
            ->where('type', 'Para faltantes')
            ->orderBy('name')
            ->get();

        $stores = Store::select('stores.*', 'cities.city AS city_name')
            ->join('cities', 'city_id', '=', 'cities.id')
            ->where('stores.status', 1)
            ->where('city_id', $first_city)
            ->get();

        $warehouses = Warehouse::where('city_id', $first_city)
            ->select('warehouses.id', 'warehouses.warehouse')
            ->get();

        $departments = Department::select('id', 'name')
            ->where('store_id', $stores->first()->id)
            ->where('status', 1)->orderBy('name')
            ->get();

        $data = [
            'title' => 'Crear Ordenes de Compra',
            'first_city' => $first_city,
            'providers' => $providers,
            'providers_missing' => $providers_missing,
            'cities' => $cities,
            'stores' => $stores,
            'warehouses' => $warehouses,
            'departments' => $departments
        ];

        return View::make('admin.providers.orders.order_form', $data);
    }

    /**
     * Función para obtener las bodegas por ciudad
     * @return Warehouse
     */
    public function get_warehouses_ajax()
    {
        $city_id = Input::get('city_id');
        $warehouses = Warehouse::where('city_id', $city_id)
            ->select('warehouses.id', 'warehouses.warehouse')
            ->get();

        return Response::json($warehouses);
    }

    /**
     * Función para obtener las tiendas por ciudad
     * @return Store
     */
    public function get_stores_ajax()
    {
        $city_id = Input::get('city_id');
        $stores = Store::select('stores.*', 'cities.city AS city_name')
            ->join('cities', 'city_id', '=', 'cities.id')
            ->where('stores.status', 1)
            ->where('city_id', $city_id)
            ->get();

        return Response::json($stores);
    }

    /**
     * Función para obtener los proveedores por bodega
     * @return Provider
     */
    public function get_providers_ajax_2()
    {
        $warehouse_id = Input::get('warehouse_id');
        $providers = Provider::where('status', 1)
            ->where('type', 'Para bodega')
            ->orderBy('name')
            ->select('id', 'name');
            
        if (Input::has('provider_type')) {
            $providers->where('provider_type', Input::get('provider_type'));
        }
        $providers = $providers->get();
            
        $providers->each(function ($provider) use ($warehouse_id) {
            $provider_order_today = $provider->providerOrders()
                ->whereDate('date', '=', date('Y-m-d'))
                ->whereStatus('Enviada');
            if ($warehouse_id) {
                $provider_order_today = $provider_order_today->where('warehouse_id', '=', $warehouse_id);
            }
            $provider_order_today = $provider_order_today->get()
                ->count();
            $provider->count_today = $provider_order_today;
        });

        return Response::json($providers);
    }

    /**
     * Función para obtener los departamentos por tienda.
     * @return Department
     */
    public function get_departments_ajax()
    {
        $store_id = Input::get('store_id');
        $departments = Department::select('id', 'name')
            ->where('store_id', $store_id)
            ->where('status', 1)
            ->orderBy('name')
            ->get();

        return Response::json($departments);
    }

    /**
     * Función para obtener los pasillos por departamento y tienda.
     * @return Shelf
     */
    public function get_shelves_ajax()
    {
        $department_id = Input::get('department_id');
        $store_id = Input::get('store_id');
        $shelves = Shelf::select('id', 'name')
            ->where('department_id', $department_id)
            ->where('store_id', $store_id)
            ->where('status', 1)
            ->orderBy('name')
            ->get();

        return Response::json($shelves);
    }

    /**
     * Función para obtener los productos que se deben pedir
     */
    public function get_form_products_ajax()
    {
        $order_type = Input::get('order_type');
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $provider_id = Input::get('provider_id');
        $show_all_products = Input::get('show_all_products');
        $store_id = Input::get('store_id');
        $department_id = Input::get('department_id');
        $shelf_id = Input::get('shelf_id');
        $ignore_on_route_stock = Input::get('ignore_on_route_stock');
        $show_commited_products_only = Input::get('show_commited_products_only');
        $search = Input::get('search');
        $products_added = Input::get('products_added');

        // Se obtienen los store_product_id de los productos ya agregados para ser ignorados de una nueva búsqueda.
        $products_added = Json::decode($products_added, true);
        $ignore_store_products_id = [];
        if (!empty($products_added)) {
            $ignore_store_products_id = array_map(function($product) {return $product['store_product_id'];}, $products_added);
        }

        $products = Product::where('type', '<>', 'Agrupado')
            ->whereHas('storeProduct', function ($query) use ($store_id, $department_id, $provider_id, $shelf_id, $search, $show_commited_products_only, $warehouse_id, $ignore_store_products_id) {
                $query->where('store_id', $store_id)
                    ->whereNotIn('id', $ignore_store_products_id);
                if (!empty($department_id)) {
                    $query->where('department_id', $department_id);
                }
                if (!empty($shelf_id)) {
                    $query->where('shelf_id', $shelf_id);
                }

                $query->whereHas('provider', function ($query) use ($provider_id) {
                    $query->where('id', $provider_id);
                });
                $query->whereHas('storeProductWarehouses', function ($query) use ($warehouse_id) {
                    $query->where('warehouse_id', $warehouse_id)
                        ->where('discontinued', 0);
                });
                if ($show_commited_products_only) {
                    $query->whereHas('orderProducts.order', function ($query) use ($warehouse_id) {
                            $query->whereIn('status', ['Initiated', 'Enrutado', 'In Progress', 'In Progress', 'Alistado'])
                                ->whereHas('orderGroup', function ($query) use ($warehouse_id) {
                                    $query->where('warehouse_id', $warehouse_id);
                                });
                        });
                }
            })
            ->where(function ($query) use ($search) {
                $query->orWhere('reference', 'LIKE', "%{$search}%")
                    ->orWhere('name', 'LIKE', "%{$search}%")
                    ->orWhereHas('storeProduct', function ($query) use ($search) {
                        $query->where('provider_plu', 'LIKE', "%{$search}%");
                    });
            });

        $products = $products->with([
            'storeProduct' => function($query) use ($store_id, $department_id, $shelf_id, $provider_id, $warehouse_id, $search) {
                $query->where('store_id', $store_id);
                if (!empty($department_id)) {
                    $query->where('department_id', $department_id);
                }
                if (!empty($shelf_id)) {
                    $query->where('shelf_id', $shelf_id);
                }
                $query->with(['provider' => function ($query) use ($provider_id) {
                    $query->where('id', $provider_id);
                }])
                    ->with(['storeProductWarehouses' => function ($query) use ($warehouse_id) {
                        $query->where('warehouse_id', $warehouse_id)
                            ->where('discontinued', 0);
                    }]);
            }
        ])
            ->get();

        // Fórmula para el no: stock_actual + stock_solicitado - stock_comprometido. Si el resultado es menor al stock mínimo, mostrar productos
        /*FORMULA VER TODOS LOS PRODUCTOS :
        OPCION NO : STOCK ACTUAL + STOCK SOLICITADO - COMPROMETIDO ESE RESULTADO DEBE SER MENOR A EL STOCK MINIMO.*/
        if (!$show_all_products) {
            $products = $products->filter(function ($product) use ($warehouse_id, $ignore_on_route_stock) {
                $response = $product->storeProduct->first()->recommendedQuantityToRequest($warehouse_id);
                $product->current_stock = $response['current_stock'];
                $product->on_route_stock = $ignore_on_route_stock ? 0 : $response['on_route_stock'];
                $product->committed_stock = $response['committed_stock'];
                $product->quantity_to_request = $response['quantity_to_request'];
                $product->quantity_to_package_request = $response['quantity_to_package_request'];

                if ($product->type == 'Proveedor') {
                    return true;
                }
                if (($product->current_stock + $product->on_route_stock - $product->committed_stock) < $product->storeProduct->first()->storeProductWarehouses->first()->ideal_stock) {
                    return true;
                }
            });
        } else {
            $products = $products->each(function ($product) use ($warehouse_id, $ignore_on_route_stock) {
                $response = $product->storeProduct->first()->recommendedQuantityToRequest($warehouse_id);
                $product->current_stock = $response['current_stock'];
                $product->on_route_stock = $ignore_on_route_stock ? 0 : $response['on_route_stock'];
                $product->committed_stock = $response['committed_stock'];
                $product->quantity_to_request = $response['quantity_to_request'];
                $product->quantity_to_package_request = $response['quantity_to_package_request'];
            });
        }
        $products = $products->each(function ($product) {
            $product->is_added = 0;
        });
        $products = array_values($products->toArray());
        return Response::json($products);
    }

    /**
     *  Filtro y grilla con listado de productos a solicitar
     */
    public function add()
    {
        if (!Request::ajax()) {
        } else {
            //cargar ids de productos de sesion
            $products_session = $store_product_ids = array();
            if (Session::has('admin_provider_order_products')) {
                $products_session = json_decode(Session::get('admin_provider_order_products'), true);
                $store_product_ids = array_keys($products_session);
            }

            //agregar nuevos productos a sesion
            if (Input::has('products')) {
                $products_request = json_decode(Input::get('products'), true);
                foreach ($products_request as $id => $quantity) {
                    $store_product_ids[] = $id;
                }

                $products_session = Session::has('admin_provider_order_products') ? $products_request + $products_session : $products_request;

                Session::forget('admin_provider_order_products');
                Session::put('admin_provider_order_products', json_encode($products_session));
            }

            $search = Input::has('s') ? Input::get('s') : '';
            $warehouse_id = Input::get('warehouse_id');
            //obtener productos que hay que solicitar
            $products = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
                        ->join('providers', 'store_products.provider_id', '=', 'providers.id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->join('cities', 'cities.id', '=', 'stores.city_id')
                        ->join('departments', 'departments.id', '=', 'store_products.department_id')
                        ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                        ->leftJoin(DB::raw('(provider_order_details, provider_orders)'), function ($join) use ($warehouse_id) {
                            $join->on('provider_orders.provider_id', '=', 'providers.id');
                            $join->on('provider_orders.status', '=', DB::raw('"Enviada"'));
                            $join->where('provider_orders.warehouse_id', '=', $warehouse_id);
                            $join->on('provider_order_details.store_product_id', '=', 'store_products.id');
                            $join->on('provider_order_details.provider_order_id', '=', 'provider_orders.id');
                        })
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->whereNull('store_products.allied_store_id')
                        ->where('products.type', '<>', DB::raw('"Agrupado"'))
                        ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                        ->where(function ($query) use ($search) {
                            $query->where('store_products.provider_plu', 'LIKE', '%'.$search.'%');
                            $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                            $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                        })
                        // ->where('warehouse_storages.type', '=', DB::raw('"Producto"'))
                        ->where('store_product_warehouses.discontinued', 0);

            //si es busqueda desde modal select de cantidad de unidades a solicitar en cero
            if (Input::has('modal_form')) {
                $products->select(
                    'products.*',
                    'store_products.*',
                    DB::raw('SUM(IF(provider_order_details.quantity_order IS NULL, 0, provider_order_details.quantity_order)) AS on_route_stock'),
                    DB::raw('providers.name AS provider'),
                    DB::raw('providers.id AS provider_id'),
                    DB::raw('"1" AS is_added_by_query'),
                    DB::raw('SUM(store_product_warehouses.picking_stock) + SUM(store_product_warehouses.reception_stock) + SUM(store_product_warehouses.return_stock) AS current_stock'),
                    'store_product_warehouses.warehouse_id',
                    'store_product_warehouses.ideal_stock',
                    'store_product_warehouses.minimum_stock'
                );
            } else {
                $on_route_stock = !Input::get('ignore_on_route_stock') ? 'SUM(IF(provider_order_details.quantity_order IS NULL, 0, provider_order_details.quantity_order)) AS on_route_stock' : '0 AS on_route_stock';
                $products->select(
                    'products.*',
                    'store_products.*',
                    DB::raw($on_route_stock),
                    DB::raw('providers.name AS provider'),
                    DB::raw('providers.id AS provider_id'),
                    DB::raw('SUM(store_product_warehouses.picking_stock) + SUM(store_product_warehouses.reception_stock) + SUM(store_product_warehouses.return_stock) AS current_stock'),
                    'store_product_warehouses.warehouse_id',
                    'store_product_warehouses.ideal_stock',
                    'store_product_warehouses.minimum_stock',
                    'store_products.id AS store_product_id'
                );
            }

            if (!empty($store_product_ids)) {
                $products->whereNotIn('store_products.id', $store_product_ids);
            }

            //filtros
            if (Input::has('show_commited_products_only') && Input::get('show_commited_products_only')) {
                $start_date = Input::has('start_date') ? format_date('mysql_with_time', Input::get('start_date')) : false;
                $end_date = Input::has('end_date') ? format_date('mysql_with_time', Input::get('end_date')) : false;
                if ($start_date && $end_date) {
                    $products->whereBetween('orders.delivery_date', array($start_date, $end_date));
                }
                $products->join('order_products', 'order_products.store_product_id', '=', 'store_products.id');
                $products->join('orders', 'orders.id', '=', 'order_products.order_id');
                $products->join('order_groups', 'orders.group_id', '=', 'order_groups.id');
                $products->where('order_groups.warehouse_id', $warehouse_id);
                $products->where(function ($query) {
                    $query->where('orders.status', '=', 'Initiated');
                    $query->orWhere('orders.status', '=', 'Enrutado');
                    $query->orWhere('orders.status', '=', 'In Progress');
                    $query->orWhere('orders.status', '=', 'Alistado');
                });
            }

            if (Input::has('city_id')) {
                $products->where('cities.id', Input::get('city_id'));
            }

            if (Input::has('provider_id')) {
                $products->where('providers.id', Input::get('provider_id'));
            }

            if (Input::get('store_id')) {
                $products->where('stores.id', Input::get('store_id'));
            }

            if (Input::has('department_id')) {
                $products->where('departments.id', Input::get('department_id'));
            }

            if (Input::has('shelve_id')) {
                $products->where('shelves.id', Input::get('shelve_id'));
            }

            $products = $products->groupBy('products.id')->get();
            $products->each(function (&$product) {
                $store_product_obj = StoreProduct::find($product->store_product_id);
                $product->current_stock = $store_product_obj->getCurrentStock($product->warehouse_id);
            });
            $products = $this->get_quantity_to_request($products);

            $section = !Input::has('modal_form') ? 'content_products_query' : 'content_products_query_modal';

            return View::make('admin.providers.orders.order_form')
                        ->with('products', $products)
                        ->renderSections()[$section];
        }
    }

    /**
     * Funcion para agregar nuevo producto a orden de compra
     */
    public function add_product_to_order($id)
    {
        $provider_order = ProviderOrder::find($id);
        $store_product_id =Input::get('store_product_id');
        $quantity_unit_to_request =Input::get('quantity_unit_to_request');

        $store_product = StoreProduct::findOrFail($store_product_id);

        $provider_order->addProductToProviderOrder($store_product_id, $quantity_unit_to_request);

        //log de pedido
        $evet_result = Event::fire('providerOrder.addedProduct', [[
            'store_product_id' => $store_product_id,
            'admin_id' => Session::get('admin_id'),
            'provider_order_id' => $id
        ]]);
        return Response::json(['success' => 'Se han agregado los productos']);
    }

    /**
     * Obtener productos agregados para generar ordenes de compra
     */
    public function get_products_order_ajax()
    {
        //cargar ids de productos de sesion
        $store_product_ids = array();
        if (Session::has('admin_provider_order_products')) {
            $products_session = json_decode(Session::get('admin_provider_order_products'), true);
            foreach ($products_session as $id => $quantity) {
                $store_product_ids[] = $id;
            }
        }

        //obtener productos a solicitar desde sesion
        $products_added = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
                            ->join('providers', 'store_products.provider_id', '=', 'providers.id')
                            ->leftJoin(DB::raw('(provider_order_details, provider_orders)'), function ($join) {
                                $join->on('provider_orders.provider_id', '=', 'providers.id');
                                $join->on('provider_orders.status', '=', DB::raw('"Enviada"'));
                                $join->on('provider_order_details.store_product_id', '=', 'store_products.id');
                                $join->on('provider_order_details.provider_order_id', '=', 'provider_orders.id');
                            })
                            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                            ->whereIn('store_products.id', $store_product_ids)
                            ->select(
                                'products.*',
                                'store_products.*',
                                DB::raw('IF(provider_order_details.quantity_order IS NULL, 0, provider_order_details.quantity_order) AS on_route_stock'),
                                DB::raw('SUM(store_product_warehouses.picking_stock) + SUM(store_product_warehouses.reception_stock) + SUM(store_product_warehouses.return_stock) AS current_stock'),
                                DB::raw('providers.name AS provider'),
                                DB::raw('providers.id AS provider_id'),
                                'store_products.id AS store_product_id',
                                'store_product_warehouses.warehouse_id',
                                'store_product_warehouses.ideal_stock',
                                'store_product_warehouses.minimum_stock'
                            )->groupBy('products.id')->get();

        $products_added->each(function (&$product) {
            $store_product_obj = StoreProduct::find($product->store_product_id);
            $product->current_stock = $store_product_obj->getCurrentStock($product->warehouse_id);
        });

        foreach ($products_added as $store_product) {
            $store_product->quantity_unit_to_request = $products_session[$store_product->id]['quantity'];
        }

        $products_added = count($products_added) ? $this->get_quantity_to_request($products_added, true) : array();

        return View::make('admin.providers.orders.order_form')
                        ->with('products_added', $products_added)
                        ->renderSections()['content_products_order'];
    }

    /**
     *  Elimina un producto de las ordenes de compra a crear
     */
    public function delete_product_ajax()
    {
        if (Input::has('id')) {
            $products_session = json_decode(Session::get('admin_provider_order_products'), true);
            unset($products_session[Input::get('id')]);

            $store_product_ids = array_keys($products_session);

            //obtener productos a solicitar desde sesion
            $products_added = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
                                ->join('providers', 'store_products.provider_id', '=', 'providers.id')
                                ->leftJoin(DB::raw('(provider_order_details, provider_orders)'), function ($join) {
                                    $join->on('provider_orders.provider_id', '=', 'providers.id');
                                    $join->on('provider_orders.status', '=', DB::raw('"Enviada"'));
                                    $join->on('provider_order_details.store_product_id', '=', 'store_products.id');
                                    $join->on('provider_order_details.provider_order_id', '=', 'provider_orders.id');
                                })
                                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                                ->whereIn('store_products.id', $store_product_ids)
                                ->select(
                                    'products.*',
                                    'store_products.*',
                                    DB::raw('IF(provider_order_details.quantity_order IS NULL, 0, provider_order_details.quantity_order) AS on_route_stock'),
                                    DB::raw('providers.name AS provider'),
                                    // DB::raw('SUM( IF(warehouse_storages.quantity IS NULL, 0, warehouse_storages.quantity) ) + SUM(store_product_warehouses.picking_stock) + SUM(store_product_warehouses.reception_stock) + SUM(store_product_warehouses.return_stock) AS current_stock'),
                                    DB::raw('SUM(store_product_warehouses.picking_stock) + SUM(store_product_warehouses.reception_stock) + SUM(store_product_warehouses.return_stock) AS current_stock'),
                                    'store_products.id AS store_product_id',
                                    'store_product_warehouses.warehouse_id',
                                    'store_product_warehouses.ideal_stock',
                                    'store_product_warehouses.minimum_stock'
                                )->groupBy('products.id')->get();

            $products_added->each(function (&$product) {
                $store_product_obj = StoreProduct::find($product->store_product_id);
                $product->current_stock = $store_product_obj->getCurrentStock($product->warehouse_id);
            });

            foreach ($products_added as $store_product) {
                $store_product->quantity_unit_to_request = $products_session[$store_product->id]['quantity'];
            }

            $products_added = count($products_added) ? $this->get_quantity_to_request($products_added, true) : array();

            Session::forget('admin_provider_order_products');
            Session::put('admin_provider_order_products', json_encode($products_session));

            return View::make('admin.providers.orders.order_form')
                          ->with('products_added', $products_added)
                          ->renderSections()['content_products_order'];
        }
    }

    /**
     * Crea las ordenes de compra
     */
    public function save()
    {
        $products_added = Input::get('products_added');
        $products_added = json_decode($products_added, true);
        $order_city_id = Input::get('order_city_id');
        $order_warehouse_id = Input::get('order_warehouse_id');
        $provider_id = Input::get('order_provider_id');

        $validator = Validator::make(
            [
                'ciudad' => $order_city_id,
                'bodega' => $order_warehouse_id,
                'proveedor' => $provider_id
            ],
            [
                'ciudad' => 'required|min:1|integer|exists:cities,id',
                'bodega' => 'required|min:1|integer|exists:warehouses,id',
                'proveedor' => 'required|min:1|integer|exists:providers,id'
            ]
        );

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }

        try {
            DB::beginTransaction();
            $warehouse = Warehouse::findOrFail($order_warehouse_id);
            $provider = Provider::findOrFail($provider_id);
            $provider_order = ProviderOrder::createProviderOrder($warehouse, $provider, $products_added);
            $provider_order->save();

            $html = '<p><strong>Orden de compra #:</strong> '.$provider_order->id.'</p>
                              <p><strong>Ciudad:</strong> '.$warehouse->city->city.'</p>
                              <p><strong>Bodega:</strong> '.$warehouse->warehouse.'</p>
                              <p><strong>Proveedor:</strong> '.$provider_order->provider_name.'</p>
                              <p><strong>Estado:</strong> '.$provider_order->status.'</p>
                              <p><strong>Fecha de creaci&oacute;n:</strong> '.date("d M Y g:i a", strtotime($provider_order->date)).'</p>
                              <p><strong>Fecha de entrega:</strong> '.date("d M Y g:i a", strtotime($provider_order->delivery_date)).'</p>
                              <p><strong>Unidades solicitadas:</strong> '.$provider_order->getQuantityRequested().'</p>
                              <p><strong>Creada por:</strong> '.Session::get('admin_name').'</p>';
            $mail = array(
                'template_name' => 'emails.daily_report',
                'subject' => 'Nueva orden de compra # '.$provider_order->id.' de '.$provider_order->provider_name,
                'to' => array(
                    array('email' => 'zcastillo@merqueo.com', 'name' => 'Zaira Castillo'),
                    array('email' => 'arojas@merqueo.com', 'name' => 'Angelica Rojas'),
                    array('email' => 'gvargas@merqueo.com', 'name' => 'Gladys Vargas'),
                ),
                'vars' => array(
                    'title' => 'Se ha creado una nueva orden de compra.',
                    'html' => $html
                )
            );
            send_mail($mail);
            DB::commit();
            return Redirect::route('adminProviderOrders.index')->with('success', 'Se han creado 1 ordenes de compra.')->with('provider_order_id', $provider_order->id);
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()
                ->with(
                    'error',
                    "{$e->getMessage()} - {$e->getFile()}: {$e->getLine()}"
                );
        }
    }

    /**
     * Obtener productos de la orden de compra
     */
    public function get_products_ajax($id)
    {
        $products = [];
        $search = Input::get('s');
        $provider_order_products = ProviderOrderDetail::where('provider_orders.id', $id)
                                                    ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
                                                    ->leftJoin(DB::raw('(provider_order_receptions, provider_order_reception_details)'), function ($join) {
                                                        $join->on('provider_order_receptions.provider_order_id', '=', 'provider_orders.id');
                                                        $join->on('provider_order_receptions.status', '<>', DB::raw('"En proceso"'));
                                                        $join->on('provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id');
                                                        $join->on('provider_order_reception_details.store_product_id', '=', 'provider_order_details.store_product_id');
                                                    })
                                                    ->where(function ($query) use ($search) {
                                                        $query->where('reference', 'LIKE', '%'.$search.'%');
                                                        $query->orWhere('plu', 'LIKE', '%'.$search.'%');
                                                        $query->orWhere('product_name', 'LIKE', '%'.$search.'%');
                                                    })
                                                    ->select('provider_order_details.*', 'provider_order_reception_details.status', DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received'))
                                                    ->groupBy('provider_order_details.store_product_id')
                                                    ->get();

        $total_cost = 0;
        foreach ($provider_order_products as $order_product) {
            if ($order_product->quantity_received) {
                $total_cost += $order_product->cost * $order_product->quantity_received;
            } else {
                $total_cost += $order_product->cost * $order_product->quantity_order;
            }
        }

        $provider_order = ProviderOrder::find($id);

        $data = [
            'section' => 'products_table_container',
            'provider_order' => $provider_order,
            'provider_order_products' => $provider_order_products,
            'total_cost' => $total_cost
        ];
        $view = View::make('admin.providers.orders.order_details', $data)->renderSections();

        return Response::json(['success' => 'Productos obtenidos.', 'html' => $view['products_table_container']]);
    }

    /**
    * Actualiza la fecha de entrega de las ordenes de servicio recien creadas
    */
    public function update_delivery_dates()
    {
        if (Input::has('delivery_date')) {
            $deliveryDates = Input::get('delivery_date');
            $deliveryWindows = Input::get('delivery_window');
            foreach ($deliveryDates as $orderId => $deliveryDate) {
                if (empty($deliveryDate)) {
                    return Redirect::route('adminProviderOrders.index')->with('error', 'La fecha de entrega no puede estar vacia.');
                }
                $deliveryWindow = \ProviderOrderDeliveryWindow::find($deliveryWindows[$orderId]);

                $providerOrder = ProviderOrder::find($orderId);
                $providerOrder->delivery_date = format_date('mysql_with_time', $deliveryDate.' '.$deliveryWindow->hour_end);
                $providerOrder->provider_order_delivery_window_id = $deliveryWindow->id;
                $providerOrder->provider_order_delivery_window_text = $deliveryWindow->delivery_window;
                $providerOrder->save();

                //log
                $evet_result = Event::fire('providerOrder.updatedDeliveryDate', [[
                    'delivery_date' => $deliveryDate,
                    'admin_id' => Session::get('admin_id'),
                    'provider_order_id' => $orderId
                ]]);
            }

            // return Redirect::route('adminProviderOrders.index')->with('success', 'Ordenes de compra actualizadas con éxito.');
            return Redirect::route('adminProviderOrders.details', ['id' => $providerOrder->id])
                ->with('success', 'Ordenes de compra actualizadas con éxito.');
        }

        return Redirect::route('adminProviderOrders.index')->with('error', 'Ocurrió un error al actualizar las ordenes de compra.');
    }

    /**
    * Actualiza la fecha de entrega de una orden de servicio
    */
    public function update_delivery_date($id)
    {
        if (Input::has('delivery_date')) {
            $delivery_date = Input::get('delivery_date');

            if ($provider_order = ProviderOrder::find($id)) {
                $provider_order->delivery_date = format_date('mysql_with_time', $delivery_date);
                $provider_order->save();

                //log
                $evet_result = Event::fire('providerOrder.updatedDeliveryDate', [[
                                                                                    'delivery_date' => $delivery_date,
                                                                                    'admin_id' => Session::get('admin_id'),
                                                                                    'provider_order_id' => $id
                                                                                ]]);
            }

            return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('success', 'Orden de compra actualizada con éxito.');
        }

        return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('error', 'Ocurrió un error al actualizar la orden de compra.');
    }

    /**
    * Actualiza el estado de una orden de compra
    */
    public function update_status($id)
    {
        if ($provider_order = ProviderOrder::find($id)) {
            $new_status = Input::get('status');
            if ($new_status == 'Enviada') {
                if ($provider_order->status != 'Iniciada') {
                    return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('error', 'El estado actual de la orden de compra no es valido.');
                }
            }

            if ($new_status == 'Cancelada') {
                if ($provider_order->status == 'Cerrada' && ProviderOrderReception::where('provider_id', $provider_order->id)->count()) {
                    return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('error', 'El estado actual de la orden de compra no es valido.');
                } else {
                    $provider_order->reject_comments = Input::get('reject_comments');
                    //$provider_order->provider_order_reject_reason_id = Input::get('reject_typification');
                }
            }

            if ($new_status == 'Cerrada') {
                if ($provider_order->status == 'Cerrada') {
                    return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('error', 'El estado actual de la orden de compra no es valido.');
                }

                $products = ProviderOrderReceptionDetail::join('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
                                                    ->where('provider_order_receptions.provider_order_id', $provider_order->id)
                                                    ->where('provider_order_reception_details.status', 'Pendiente')
                                                    ->groupBy('provider_order_reception_details.store_product_id')
                                                    ->get();
                if (count($products)) {
                    return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('error', 'Hay productos pendientes en recibos de bodega.');
                }
            }

            $provider_order->status = $new_status;
            $provider_order->save();

            //log
            $evet_result = Event::fire('providerOrder.updatedStatus', [[
                                                                        'status' => Input::get('status'),
                                                                        'admin_id' => Session::get('admin_id'),
                                                                        'provider_order_id' => $id
                                                                    ]]);

            //cancelar recibos de bodega
            if ($provider_order->status == 'Cancelada') {
                ProviderOrderReception::where('provider_order_id', $provider_order->id)->update(['status' => 'Cancelado']);
            }

            return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('success', 'Orden de compra actualizada con éxito.');
        }
        return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with('error', 'Ocurrió un error al actualizar la orden de compra.');
    }

    /**
     * Elimina producto en orden de compra ya creada
     */
    public function delete_product($id)
    {
        $message = 'Ocurrió un error al eliminar el producto de la orden de compra.';
        $type = 'failed';

        if ($provider_order = ProviderOrder::find($id)) {
            if ($provider_order->status == 'Iniciada' && Input::has('store_product_id')) {
                $store_product_id = Input::get('store_product_id');
                $provider_order_detail = ProviderOrderDetail::with('providerOrderDetailProductGroup')->where('provider_order_id', $id)->where('store_product_id', $store_product_id)->first();

                if ($provider_order_detail->type == 'Proveedor') {
                    $grouped_products = $provider_order_detail->providerOrderDetailProductGroup;
                    foreach ($grouped_products as $key => $grouped_product) {
                        $grouped_product->delete();
                    }
                }
                $provider_order_detail->delete();

                //log de pedido
                $evet_result = Event::fire('providerOrder.deletedProduct', [[
                                                                            'store_product_id' => $store_product_id,
                                                                            'admin_id' => Session::get('admin_id'),
                                                                            'provider_order_id' => $id
                                                                        ]]);

                $type = 'success';
                $message = 'Producto eliminado con éxito.';
            }
        }

        return Redirect::route('adminProviderOrders.details', ['id' => $id ])->with($type, $message);
    }

    /**
    * Actualiza cantidad de unidades de producto en orden de compra
    */
    public function update_product($id)
    {
        $message = 'Ocurrió un error al actualizar el producto de la orden de compra.';
        $type = 'error';
        $store_product_id = Input::get('store_product_id');
        $quantity = Input::get('quantity');
        $provider_order = ProviderOrder::findOrFail($id);

        if (!empty($store_product_id) && !empty($quantity)) {
            if ($quantity < 1) {
                return Redirect::route('adminProviderOrders.details', ['id' =>  $id ])->with('error', 'La cantidad de unidades debe ser diferente de cero.');
            }



            if ($provider_order_detail = ProviderOrderDetail::select('provider_orders.provider_id', 'provider_order_details.*', 'providers.type')
                                                            ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
                                                            ->join('providers', 'provider_orders.provider_id', '=', 'providers.id')
                                                            ->where('provider_order_id', $id)->where('provider_order_details.store_product_id', $store_product_id)->groupBy('provider_order_id')
                                                            ->first()) {
                $quantity_unit_to_request = Input::get('quantity');
                $store_product = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')->where('store_products.id', $store_product_id)->first();

                //valida y calcula cantidad de productos a solicitar proveedor con aproximación o no
                if (!$store_product->provider_pack_quantity_approach && $provider_order_detail->type != 'Para faltantes') {
                    if ($quantity_unit_to_request > $store_product->provider_pack_quantity) {
                        $quantity_pack = ceil($quantity_unit_to_request / $store_product->provider_pack_quantity);
                        $provider_order_detail->quantity_pack  = $quantity_pack;
                        $provider_order_detail->quantity_order = $quantity_pack * $store_product->provider_pack_quantity;
                    } else {
                        $provider_order_detail->quantity_pack = 1;
                        $provider_order_detail->quantity_order = $store_product->provider_pack_quantity;
                    }
                } else {
                    $provider_order_detail->quantity_pack = ceil($quantity_unit_to_request / $store_product->provider_pack_quantity);
                    $provider_order_detail->quantity_order = $quantity_unit_to_request;
                }
                $provider_order_detail->save();

                //log de pedido
                $evet_result = Event::fire('providerOrder.updatedProductQuantity', [[
                                                                            'store_product_id' => $store_product_id,
                                                                            'quantity_unit_to_request' => $quantity_unit_to_request,
                                                                            'admin_id' => Session::get('admin_id'),
                                                                            'provider_order_id' => $id
                                                                        ]]);
                $type = 'success';
                $message = 'Producto actualizado satifactoriamente.';
            }
        }

        return Redirect::route('adminProviderOrders.details', ['id' =>  $id ])->with($type, $message);
    }

    /**
     * Guardar nota de orden
     */
    public function save_note($id)
    {
        $provider_order = ProviderOrder::find($id);
        if ($provider_order) {
            $order_note = new ProviderOrderNote;
            $order_note->provider_order_id = $provider_order->id;
            $order_note->description = Input::get('note');
            $order_note->admin_id = Session::get('admin_id');
            $order_note->save();

            return Redirect::route('adminProviderOrders.details', ['id' => $provider_order->id])->with('type', 'success')->with('message', 'La nota fue guardada con éxito.');
        }

        return Redirect::route('adminProviderOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al guardar la nota.');
    }

    /**
     * Obtener detalle de producto agrupado ajax
     */
    public function get_product_group_ajax($id)
    {
        if (!$provider_order = ProviderOrder::find($id)) {
            return Response::json(['status' => false], 400);
        }

        if (Input::has('provider_order_detail_id')) {
            $provider_order_detail = ProviderOrderDetail::with('providerOrderDetailProductGroup.storeProduct.product')->find(Input::get('provider_order_detail_id'));
            // debug($provider_order_detail);
            /*echo '<pre>';
            var_dump( $provider_orde_detail );
            echo '</pre>';
            exit;
            $provider_order_detail_product_group = ProviderOrderDetailProductGroup::where('provider_order_detail_id', Input::get('provider_order_detail_id'))
                                            ->leftJoin('store_products', 'store_products.id', '=', 'provider_order_detail_product_group.store_product_id')
                                            ->leftJoin('products', 'products.id', '=', 'store_products.product_id')
                                            ->join('provider_order_details','provider_order_details.id','=','provider_order_detail_product_group.provider_order_detail_id')
                                            ->select('provider_order_detail_product_group.*', 'provider_order_details.reference')
                                            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
                                            ->orderBy('department_id', 'desc')->orderBy('provider_order_detail_product_group.product_name', 'asc')
                                            ->get();*/
        }
        $data['provider_order_detail'] = $provider_order_detail;
        return View::make('admin.providers.orders.ajax.provider_order_detail_product_group', $data)->render();

        /*return View::make('admin.providers.orders.order_details')
                    ->with('provider_order_detail_product_group', $provider_order_detail_product_group)
                    ->with('section', 'provider_order_detail_product_group')
                    ->renderSections()['provider_order_detail_product_group'];*/
    }

    /**
     * Exporta en un archivo en excel una orden de compra.
     */
    public function export_ajax($get_file = false)
    {
        $provider_order = ProviderOrder::find(Input::get('id'));
        if (!$provider_order) {
            return Response::json(array('status' => false, 'message' => 'Orden de compra no existe'), 200);
        }

        $provider_order_products = ProviderOrderDetail::where('provider_orders.id', $provider_order->id)
                                                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
                                                        ->select(
                                                            DB::raw('provider_orders.id AS "Orden de compra"'),
                                                            DB::raw("DATE_FORMAT(provider_orders.date, '%d/%m/%Y %H:%i:%s') AS Fecha"),
                                                            DB::raw('provider_order_details.product_name AS Producto'),
                                                            DB::raw('provider_order_details.reference AS Referencia'),
                                                            DB::raw('provider_order_details.plu AS PLU'),
                                                            DB::raw('provider_order_details.quantity_pack AS "Embalaje solicitado"'),
                                                            DB::raw('provider_order_details.pack_description AS "Tipo de embalaje"'),
                                                            DB::raw('provider_order_details.quantity_order AS "Unidades solicitadas"')
                                                        )
                                                        ->get()->toArray();
        if ($n = count($provider_order_products)) {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)) {
                while (false !== ($file = readdir($gestor))) {
                    if (strstr($file, Session::get('admin_username'))) {
                        remove_file($path.$file);
                    }
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Orden de compra');
            $objPHPExcel->getProperties()->setSubject('Orden de compra');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb'=>'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($provider_order_products[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach ($provider_order_products as $row) {
                $i = 0;
                $type = PHPExcel_Cell_DataType::TYPE_STRING;
                foreach ($row as $key => $value) {
                    if ($key == "Referencia") {
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    }
                    $i++;
                }
                $objPHPExcel->getActiveSheet()->getCell("D".$j)->setDataType($type);
                $j++;
            }
            //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Orden de compra #'.$provider_order->id);
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s', time()).'.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        }

        if ($get_file) {
            return $file['real_path'];
        }
        $response = array(
            'status' => true,
            'message' => 'Orden de compra exportada.',
            'file_url' => $url
        );
        return Response::json($response, 200);
    }

    /**
    * Obtiene la cantidad de unidades y cantidad de embalaje a solicitar de los productos
    *
    * @param array $store_products Arreglo de objetos de productos
    * @param int $show_custom_quantity_unit_to_request Mostrar unidades agregadas manualmente
    */
    private function get_quantity_to_request($store_products, $show_custom_quantity_unit_to_request = false)
    {
        $product_list = array();
        $start_date = Input::has('start_date') ? format_date('mysql_with_time', Input::get('start_date')) : false;
        $end_date = Input::has('end_date') ? format_date('mysql_with_time', Input::get('end_date')) : false;
        $committed_stock = 0;

        foreach ($store_products as $key => $store_product) {
            //PRODUCTO AGRUPADO
            if ($store_product->type == 'Proveedor') {
                $store_product->on_route_stock = 0;
                $quantity_unit_to_request = $final_quantity_unit_to_request = 0;
                $store_products_group = Product::select(
                                            'products.*',
                                            'store_products.*',
                                            'store_product_group.quantity',
                                            // DB::raw('SUM(store_product_warehouses.picking_stock) + SUM(store_product_warehouses.reception_stock) + SUM(store_product_warehouses.return_stock) AS current_stock'),
                                            'store_products.id AS store_product_id',
                                            'store_product_warehouses.warehouse_id',
                                            'store_product_warehouses.ideal_stock',
                                            'store_product_warehouses.minimum_stock'
                                        )
                                        ->join('store_products', 'store_products.product_id', '=', 'products.id')
                                        ->join('store_product_group', 'store_product_group.store_product_group_id', '=', 'store_products.id')
                                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                                        ->where('store_product_warehouses.store_product_id', $store_product->id)
                                        ->get();

                $store_products_group->each(function (&$product) {
                    $store_product_obj = StoreProduct::find($product->store_product_id);
                    $product->current_stock = $store_product_obj->getCurrentStock($product->warehouse_id);
                });

                foreach ($store_products_group as $store_product_group) {
                    //obtener de la cantidad de unidades comprometidas del producto
                    $order_product = Order::where(function ($query) {
                        $query->where('orders.status', '=', 'Initiated');
                        $query->orWhere('orders.status', '=', 'Enrutado');
                        $query->orWhere('orders.status', '=', 'In Progress');
                        $query->orWhere('orders.status', '=', 'Alistado');
                    })
                                      ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                                      ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                                      ->where('order_groups.warehouse_id', $store_product_group->warehouse_id);

                    if ($start_date && $end_date) {
                        $order_product->whereBetween('orders.delivery_date', array($start_date, $end_date));
                    }

                    $order_product = $order_product->join('order_product_group', 'order_product_group.order_product_id', '=', 'order_products.id')
                                      ->where(function ($query) {
                                          $query->where('order_product_group.fulfilment_status', '=', 'Not Available');
                                          $query->orWhere('order_product_group.fulfilment_status', '=', 'Pending');
                                          $query->orWhere('order_product_group.fulfilment_status', '=', 'Fullfilled');
                                      })
                                      ->select(DB::raw("SUM(order_product_group.quantity) AS committed_stock"))
                                      ->where('order_products.store_product_id', $store_product->id)
                                      ->where('order_product_group.store_product_id', $store_product_group->id)
                                      ->first();

                    //obtener stock solicitado
                    $on_route_stock = !Input::get('ignore_on_route_stock') ? 'IF(provider_order_detail_product_group.quantity_order IS NULL, 0, provider_order_detail_product_group.quantity_order) AS on_route_stock' : '0 AS on_route_stock';

                    $product_on_route = ProviderOrder::select(DB::raw($on_route_stock))
                                    ->join('provider_order_details', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
                                    ->join('provider_order_detail_product_group', 'provider_order_detail_product_group.provider_order_detail_id', '=', 'provider_order_details.id')
                                    ->where('provider_orders.status', 'Enviada')
                                    ->where('provider_orders.warehouse_id', $store_product_group->warehouse_id)
                                    ->where('provider_order_details.store_product_id', $store_product->id)
                                    ->where('provider_order_detail_product_group.store_product_id', $store_product_group->id)
                                    ->first();

                    if ($product_on_route) {
                        $order_product->on_route_stock = $product_on_route->on_route_stock;
                    } else {
                        $order_product->on_route_stock = 0;
                    }

                    $order_product->committed_stock = $order_product->committed_stock ? $order_product->committed_stock : 0;

                    //si el stock actual es cero se solicita stock ideal mas lo comprometido menos lo solicitado
                    if ($store_product_group->current_stock <= 0) {
                        $store_product_group->current_stock = 0;
                    }

                    $current_stock = $store_product_group->current_stock + $order_product->on_route_stock - $order_product->committed_stock;
                    //si el actual es menor al minimo
                    if ($current_stock < $store_product_group->minimum_stock) {
                        $quantity_unit_to_request = $store_product_group->ideal_stock - $current_stock;
                    }


                    if ($quantity_unit_to_request > $final_quantity_unit_to_request) {
                        $final_quantity_unit_to_request = $quantity_unit_to_request;

                        $store_product->current_stock = 0;
                        $store_product->minimum_stock = 0;
                        $store_product->ideal_stock = 0;
                    }
                    $store_product->committed_stock += $order_product->committed_stock;
                    $store_product->on_route_stock += $order_product->on_route_stock;
                }

                if (!$final_quantity_unit_to_request) {
                    unset($product_list[$key]);
                    $store_product->quantity_unit_to_request = 1;
                } else {
                    $final_quantity_unit_to_request = $store_product->provider_pack_quantity ? ceil($final_quantity_unit_to_request / $store_product->provider_pack_quantity) : 1;
                    if (!$show_custom_quantity_unit_to_request) {
                        $store_product->quantity_unit_to_request = $final_quantity_unit_to_request;
                    }
                }

                if (isset($order_product)) {
                    $order_product->committed_stock = $store_product->committed_stock;
                }
            } else {
                //PRODUCTO SIMPLE

                //obtener la cantidad de unidades comprometidas del producto
                //como producto simple
                $order_product = Order::where(function ($query) {
                    $query->where('orders.status', '=', 'Initiated');
                    $query->orWhere('orders.status', '=', 'Enrutado');
                    $query->orWhere('orders.status', '=', 'In Progress');
                    $query->orWhere('orders.status', '=', 'Alistado');
                })
                    ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                    ->where('order_groups.warehouse_id', $store_product->warehouse_id);

                if ($start_date && $end_date) {
                    $order_product->whereBetween('orders.delivery_date', array($start_date, $end_date));
                }
                $order_product = $order_product->where(function ($query) {
                    $query->where('order_products.fulfilment_status', '=', 'Not Available');
                    $query->orWhere('order_products.fulfilment_status', '=', 'Pending');
                    $query->orWhere('order_products.fulfilment_status', '=', 'Fullfilled');
                })
                ->select(DB::raw("SUM(order_products.quantity) AS committed_stock"))
                ->where('order_products.store_product_id', $store_product->id)
                ->first();

                $committed_stock = $order_product->committed_stock ? $order_product->committed_stock : 0;
                //como producto en combo
                $order_product = Order::where(function ($query) {
                    $query->where('orders.status', '=', 'Initiated');
                    $query->orWhere('orders.status', '=', 'Enrutado');
                    $query->orWhere('orders.status', '=', 'In Progress');
                    $query->orWhere('orders.status', '=', 'Alistado');
                })
                ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->where('order_groups.warehouse_id', $store_product->warehouse_id);

                if ($start_date && $end_date) {
                    $order_product->whereBetween('orders.delivery_date', array($start_date, $end_date));
                }
                $order_product = $order_product
                    ->join('order_product_group', 'order_product_group.order_product_id', '=', 'order_products.id')
                    ->where(function ($query) {
                        $query->where('order_product_group.fulfilment_status', '=', 'Not Available');
                        $query->orWhere('order_product_group.fulfilment_status', '=', 'Pending');
                        $query->orWhere('order_product_group.fulfilment_status', '=', 'Fullfilled');
                    })
                    ->select(DB::raw("SUM(order_product_group.quantity) AS committed_stock"))
                    ->where('order_product_group.store_product_id', $store_product->id)
                    ->first();

                $committed_stock += $order_product->committed_stock ? $order_product->committed_stock : 0;
            }

            $product_list[$key] = $store_product->toArray();

            //unidades comprometidas del producto en pedidos pendientes para entregar
            $product_list[$key]['committed_stock'] = $committed_stock;

            //sino tiene cantidad de unidades a solicitar se calcula
            if (!isset($store_product->quantity_unit_to_request) || isset($store_product->is_added_by_query)) {
                if (isset($store_product->is_added_by_query)) {
                    $quantity_unit_to_request = 0;
                }

                //si el stock actual es cero se solicita stock ideal mas lo comprometido menos lo solicitado
                if ($store_product->current_stock <= 0) {
                    $store_product->current_stock = 0;
                }
                //$quantity_unit_to_request = $store_product->ideal_stock + $committed_stock - $store_product->on_route_stock;
                /*}
                else
                {*/
                $current_stock = $store_product->current_stock + $store_product->on_route_stock - $committed_stock;
                //si el actual es menor al minimo
                if ($current_stock < $store_product->minimum_stock) {
                    $quantity_unit_to_request = $store_product->ideal_stock - $current_stock;
                }
                /*else{
                     //si hay productos comprometidos se calcula cuanto hay que solicitar
                     if ($committed_stock){
                         //si el actual es menor al comprometido
                         if ($current_stock < $committed_stock){
                             //si el comprometido menos el actual es menor al minimo
                             if ($committed_stock - $current_stock < $store_product->minimum_stock)
                                $quantity_unit_to_request = $store_product->ideal_stock + $committed_stock - $current_stock;
                             else $quantity_unit_to_request = $committed_stock - $current_stock;
                               //si el actual es mayor o igual al comprometido
                         }else if ($current_stock >= $committed_stock){
                                    //si el actual menos el comprometido es menor al minimo
                                    if ($current_stock - $committed_stock < $store_product->minimum_stock)
                                        $quantity_unit_to_request = $store_product->ideal_stock + $committed_stock - $current_stock;
                                    else $quantity_unit_to_request = 0;//$current_stock - $committed_stock;
                                }
                     }
                }*/
                //}
            } else {
                $quantity_unit_to_request = $store_product->quantity_unit_to_request;
            }

            //si hay unidades a solicitar
            if ((!empty($quantity_unit_to_request) && $quantity_unit_to_request > 0) || isset($store_product->is_added_by_query)) {
                //calcula cantidad de unidades con base a embalaje del proveedor
                //si el proveedor no ofrece aproximacion de unidades
                if (!$store_product->provider_pack_quantity_approach) {
                    if ($store_product->type == 'Proveedor') {
                        $product_list[$key]['quantity_pack_to_request'] = $store_product->quantity_unit_to_request;
                        $product_list[$key]['quantity_unit_to_request'] = $store_product->quantity_unit_to_request;
                    } else {
                        if ($quantity_unit_to_request > $store_product->provider_pack_quantity) {
                            // debug($quantity_unit_to_request);
                            $quantity_pack_to_request = $store_product->provider_pack_quantity ? ceil($quantity_unit_to_request / $store_product->provider_pack_quantity) : 1;
                            $product_list[$key]['quantity_pack_to_request'] = $quantity_pack_to_request;
                            $product_list[$key]['quantity_unit_to_request'] = $show_custom_quantity_unit_to_request ? $store_product->quantity_unit_to_request : $quantity_pack_to_request * $store_product->provider_pack_quantity;
                        } else {
                            $product_list[$key]['quantity_pack_to_request'] = 1;
                            $product_list[$key]['quantity_unit_to_request'] = $show_custom_quantity_unit_to_request ? $store_product->quantity_unit_to_request : $store_product->provider_pack_quantity;
                        }
                    }
                } else {
                    //el proveedor si ofrece aproximacion de unidades
                    $product_list[$key]['quantity_pack_to_request'] = floor($quantity_unit_to_request / $store_product->provider_pack_quantity);
                    $product_list[$key]['quantity_unit_to_request'] = $quantity_unit_to_request;
                    $product_list[$key]['product_quantity_unit'] = $quantity_unit_to_request - ($store_product->provider_pack_quantity * floor($quantity_unit_to_request / $store_product->provider_pack_quantity));
                }
                unset($quantity_unit_to_request);
            } else {
                //eliminar producto de lista si no hay que pedir
                if (Input::has('show_all_products') && !Input::get('show_all_products')) {
                    unset($product_list[$key]);
                } else {
                    //mostrar todos los productos aunque no haya que pedir
                    $product_list[$key]['quantity_pack_to_request'] = 1;
                    $product_list[$key]['quantity_unit_to_request'] = $store_product->provider_pack_quantity;
                    $product_list[$key]['product_quantity_unit'] = $store_product->provider_pack_quantity;
                }
            }
        }

        return $product_list;
    }

    /**
     * Exporta en un archivo en pdf una orden de compra.
     */
    public function provider_order_pdf($id)
    {
        $provider_order = ProviderOrder::find($id);
        if (!$provider_order) {
            return Redirect::route('adminProviderOrders.details', ['id' =>  $id ])->with('type', 'failed')->with('message', 'No existe el pedido.');
        } else {
            Pdf::generate_provider_order($id);
        }
    }

    /**
     * Subir imagen de factura
     */
    public function upload_image($id)
    {
        if (Input::has('type') && Input::hasFile('image')) {
            $file = Input::file('image');
            $file_data['real_path'] = $file->getRealPath();
            $file_data['client_original_name'] = $file->getClientOriginalName();
            $file_data['client_original_extension'] = $file->getClientOriginalExtension();

            if (Input::get('type') == 'invoice') {
                $dir = 'orders/providers/'.date('Y-m-d');
            }

            $url = upload_image($file, false, $dir);

            if ($url) {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');

                $provider_order_invoice = new ProviderOrderInvoice;
                $provider_order_invoice->provider_order_id = $id;
                $provider_order_invoice->invoice_image_url = $url;
                $provider_order_invoice->save();

                return Redirect::route('adminProviderOrders.details', ['id' => $id])->with('type', 'success')->with('message', 'Imagen subida con éxito.');
            }
        }
        return Redirect::route('adminProviderOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al subir la imagen.');
    }

    /**
    * Eliminar imagen de factura.
    */
    public function delete_image($id, $image)
    {
        $cloudfront_url = Config::get('app.aws.cloudfront_url');

        if ($provider_order_invoice = ProviderOrderInvoice::find($image)) {
            if (!empty($provider_order_invoice->invoice_image_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $provider_order_invoice->invoice_image_url);
                remove_file($path);
                remove_file_s3($provider_order_invoice->invoice_image_url);
                $image = ProviderOrderInvoice::find($image);
                $image->delete();
            }
        }
        return Redirect::route('adminProviderOrders.details', ['id' => $id])->with('type', 'success')->with('message', 'Factura eliminada con éxito.');
    }


    public function import_remission($id)
    {
        $provider_order = ProviderOrder::with('warehouse.city')->find($id);
        if ($provider_order) {
            $store_obj = Store::getActiveStores($provider_order->warehouse->city_id);
            $store_id = $store_obj[0]->id;
            if (Input::hasFile('remission_file')) {
                ini_set('memory_limit', '512M');
                set_time_limit(0);

                $extension_list = ['xlsx', 'xls'];
                $remission_file = Input::file('remission_file');

                if (!file_exists($remission_file->getRealPath())) {
                    return Redirect::back()->with('error', 'No file uploaded');
                }

                $extension_obj = explode('.', $remission_file->getClientOriginalName());
                $is_correct_extension = in_array($extension_obj[1], $extension_list);
                if ($is_correct_extension) {
                    //obtener archivo
                    $file_path = $remission_file->getRealPath();
                    // $current_dir = opendir($path);

                    $objReader = new PHPExcel_Reader_Excel2007();
                    $objPHPExcel = $objReader->load($file_path);
                    //abrir la ultima hoja que contiene la informacion
                    $sheetCount = $objPHPExcel->getSheetCount();
                    $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
                    //obtener el numero total de filas y columnas
                    $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
                    $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());
                    $error = '';
                    $cont = $cont_error = 0;

                    DB::beginTransaction();
                    for ($i = 3; $i <= $rows; $i++) {
                        $product_plu = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
                        $quantity_expected = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $i)->getValue();
                        $storage = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
                        if (!empty($storage)) {
                            $storage = intval(preg_replace('/[^0-9]+/', '', $storage), 10);
                        } else {
                            $storage = 1;
                        }

                        if (empty($product_plu)) {
                            $cont_error++;
                            $error = 'La fila #'.$i.' no tiene PLU del producto.<br>';
                            DB::rollback();
                            return Redirect::back()->with('error', $error);
                        }
                        if ($quantity_expected == '') {
                            $cont_error++;
                            $error = 'La fila #'.$i.' no tiene cantidad esperada. <br>';
                            DB::rollBack();
                            return Redirect::back()->with('error', $error);
                        }
                        if ($reception_detail = ProviderOrderDetail::where('provider_order_id', $id)->where('plu', $product_plu)->first()) {
                            $reception_detail->quantity_expected = $quantity_expected;
                            $reception_detail->save();
                        } else {
                            $store_product = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
                                                            ->where('store_products.provider_plu', $product_plu)
                                                            ->where('store_products.store_id', $store_id)
                                                            ->where('store_products.provider_id', $provider_order->provider_id)
                                                            ->whereIn('products.type', ['Simple', 'Proveedor'])
                                                            ->select(
                                                                'products.id',
                                                                'products.type',
                                                                'products.name',
                                                                'store_products.*'
                                                            )
                                                            ->first();

                            if ($store_product) {
                                $result = $provider_order->addProduct($store_product->id, $quantity_expected);
                                if (!$result['status']) {
                                    $cont_error++;
                                    $error = 'La fila #'.$i.': '.$result['message'].'.<br>';
                                    DB::rollBack();
                                    return Redirect::back()->with('error', $error);
                                } else {
                                    if ($reception_detail = ProviderOrderDetail::where('provider_order_id', $id)->where('plu', $product_plu)->first()) {
                                        $reception_detail->quantity_expected = $quantity_expected;
                                        $reception_detail->save();
                                    }
                                }
                            } else {
                                $cont_error++;
                                $error = 'La fila #'.$i.' tiene un producto que no fue encontrado. Es posible que el producto no exista o que no pertenezca a este proveedor.<br>';
                                DB::rollBack();
                                return Redirect::back()->with('error', $error);
                            }
                        }
                    }
                    $evet_result = Event::fire('providerOrder.uploadedRemission', [[
                                                                                        'admin_id' => Session::get('admin_id'),
                                                                                        'provider_order_id' => $id
                                                                                    ]]);
                    DB::commit();
                    return Redirect::back()->with('success', 'Se ha importado la remisión.');
                }
                return Redirect::back()->with('error', 'El formato del archivo no es válido.');
            }
        }
    }

    /**
    * Importar orden de compra
    */
    public function import_product_missings()
    {
        if (Input::hasFile('file_missing_products')) {
            ini_set('memory_limit', '1024M');
            set_time_limit(0);

            $extension_list = ['xlsx', 'xls'];
            $file_missing_products = Input::file('file_missing_products');

            if (!file_exists($file_missing_products->getRealPath())) {
                return Redirect::back()->with('error', 'No file uploaded');
            }

            $extension_obj = explode('.', $file_missing_products->getClientOriginalName());
            $is_correct_extension = in_array($extension_obj[1], $extension_list);
            if ($is_correct_extension) {

                //obtener archivo
                $file_path = $file_missing_products->getRealPath();

                $objReader = new PHPExcel_Reader_Excel2007();
                $objPHPExcel = $objReader->load($file_path);

                //Establesco la primera hoja como activa
                $objPHPExcel->setActiveSheetIndex(0);

                //obtengo el número de filas
                $rows = $objPHPExcel->getActiveSheet()->getHighestRow();

                //obtengo el número de columnas
                $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());

                //array con el listado de productos y cantidades
                $products_in_file = array();

                for ($i = 2; $i <= $rows; $i++) {
                    $stored_product_id = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
                    if (!empty($stored_product_id)) {
                        $quantity_solicited = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();
                        $products_in_file[$stored_product_id] = array(
                            'quantity'      => $quantity_solicited
                        );
                    }
                }
                $stored_product_ids = array_keys($products_in_file);
                $provider_id_merqueo = Input::get('provider_id');
                $provider = Provider::find($provider_id_merqueo);

                $store_products_group = Product::select('products.*', 'store_products.*', 'store_products.id as stored_product_id')
                        ->join('store_products', 'store_products.product_id', '=', 'products.id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->whereIn('store_products.id', $stored_product_ids)
                        ->where('stores.city_id', Input::get('city_id'));

                if ($provider->id != 17 && $provider->provider_type != 'Marketplace') {
                    $store_products_group = $store_products_group->where('store_products.provider_id', $provider->id);
                }

                $store_products_group = $store_products_group->get();

                if (!$store_products_group->count()) {
                    return Redirect::back()->with('error', 'No se encontraron los productos.');
                }

                if ($store_products_group->count() != count($products_in_file)) {
                    return Redirect::back()->with('error', 'Algunos productos del archivo no fueron encontrados en el sistema ó no pertenecen al proveedor seleccionado.');
                }

                //agrego la cantidad solicitada del producto
                foreach ($store_products_group as $product) {
                    $product->quantity_unit_to_request = $products_in_file[ $product->stored_product_id ]['quantity'];
                }

                try {
                    DB::beginTransaction();
                    $provider_orders = [];

                    //orden de compra a proveedor
                    $provider_order = new ProviderOrder;
                    $provider_order->provider_id = $provider->id;
                    $provider_order->warehouse_id = Input::get('warehouse_id');
                    $provider_order->provider_name = $provider->name;
                    $provider_order->admin_id = Session::get('admin_id');
                    $provider_order->status = 'Iniciada';
                    $provider_order->date = date('Y-m-d H:i:s');
                    $provider_order->missing_products = Input::get('missing_products');
                    $provider_order->save();
                    $provider_orders[$provider_id_merqueo] = $provider_order->id;

                    foreach ($store_products_group as $key => $store_product) {

                        //inicio del guardado del detalle de la orden

                        $provider_order_detail = new ProviderOrderDetail;


                        $quantity_unit_to_request = $store_product['quantity_unit_to_request'];
                        $store_product_tmp = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')->where('store_products.id', $store_product['stored_product_id'])->first();

                        //valida y calcula cantidad de productos a solicitar proveedor con aproximación o no
                        if (!$store_product_tmp->provider_pack_quantity_approach && Input::get('provider_id') != 17) {
                            if ($quantity_unit_to_request > $store_product_tmp->provider_pack_quantity) {
                                $quantity_pack = ceil($quantity_unit_to_request / $store_product_tmp->provider_pack_quantity);
                                $provider_order_detail->quantity_pack  = $quantity_pack;
                                $provider_order_detail->quantity_order = $quantity_pack * $store_product_tmp->provider_pack_quantity;
                            } else {
                                $provider_order_detail->quantity_pack = 1;
                                $provider_order_detail->quantity_order = $store_product_tmp->provider_pack_quantity;
                            }
                        } else {
                            if ($store_product_tmp->provider_pack_quantity == 0) {
                                $provider_order_detail->quantity_pack = $quantity_unit_to_request;
                            } else {
                                $provider_order_detail->quantity_pack = ceil($quantity_unit_to_request / $store_product_tmp->provider_pack_quantity);
                            }
                            $provider_order_detail->quantity_order = $quantity_unit_to_request;
                        }


                        $provider_order_detail->provider_order_id = $provider_orders[Input::get('provider_id') ? $provider_id_merqueo : $store_product['provider_id']];
                        $provider_order_detail->store_product_id = $store_product['stored_product_id'];
                        $provider_order_detail->type = $store_product['type'];
                        $provider_order_detail->reference = $store_product['reference'];
                        $provider_order_detail->plu = $store_product['provider_plu'];
                        $provider_order_detail->product_name = $store_product['name'].' '.$store_product['quantity'].' '.$store_product['unit'];
                        $provider_order_detail->ideal_stock = $store_product['ideal_stock'];
                        $provider_order_detail->pack_description = $store_product['provider_pack_type'] != 'Unidad' ? '1 '.$store_product['provider_pack_type'].' x '.$store_product['provider_pack_quantity'].' Unidades' : '1 Unidad';
                        $provider_order_detail->base_cost = $store_product['base_cost'];
                        $provider_order_detail->cost = $store_product['cost'];
                        $provider_order_detail->iva = $store_product['iva'];
                        $provider_order_detail->image_url = $store_product['image_medium_url'];
                        $provider_order_detail->save();

                        //guardar producto agrupado
                        if ($store_product['type'] == 'Proveedor') {
                            $products = StoreProductGroup::select('products.*', 'store_products.*', 'store_product_group_id', 'store_product_group.quantity AS group_quantity')
                                                    ->join('store_products', 'store_product_group.store_product_group_id', '=', 'store_products.id')
                                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                                    ->where('store_product_id', $store_product['id'])
                                                    ->get();
                            foreach ($products as $store_product_group) {
                                $provider_order_detail_product_group = new ProviderOrderDetailProductGroup;
                                $provider_order_detail_product_group->provider_order_detail_id = $provider_order_detail->id;
                                $provider_order_detail_product_group->store_product_id = $store_product_group->store_product_group_id;
                                $provider_order_detail_product_group->plu = $store_product_group->provider_plu;
                                $provider_order_detail_product_group->product_name = $store_product_group->name.' '.$store_product_group->quantity.' '.$store_product_group->unit;
                                $provider_order_detail_product_group->product_quantity = $store_product_group->quantity;
                                $provider_order_detail_product_group->product_unit = $store_product_group->unit;
                                $provider_order_detail_product_group->quantity_order = $store_product_group->group_quantity * $provider_order_detail->quantity_order;
                                $provider_order_detail_product_group->ideal_stock = $store_product_group->ideal_stock;
                                $provider_order_detail_product_group->base_cost = $store_product_group->base_cost;
                                $provider_order_detail_product_group->cost = $store_product_group->cost;
                                $provider_order_detail_product_group->iva = $store_product_group->iva;
                                $provider_order_detail_product_group->image_url = $store_product_group->image_medium_url;
                                $provider_order_detail_product_group->save();
                            }
                        }
                    }
                    DB::commit();

                    //guardado de log
                    $evet_result = Event::fire('providerOrder.updatedStatus', [[
                                                    'status' => 'Iniciada',
                                                    'admin_id' => Session::get('admin_id'),
                                                    'provider_order_id' => $provider_orders[$provider_id_merqueo]
                                                ]]);
                    //redireccion a la orden creada
                    return Redirect::route('adminProviderOrders.details', [ 'id' => $provider_orders[$provider_id_merqueo] ])->with('success', 'Se ha creado la orden de compra satisfactoriamente.');
                } catch (\Exception $exception) {
                    DB::rollback();
                    return Redirect::back()->with('error', 'Ocurrió un error al importar las ordenes de compra. '.$exception->getMessage().' - '. $exception->getLine());
                }
            }
            // Error en caso de archivo no valido
            return Redirect::back()->with('error', 'El formato del archivo no es válido.');
        } else {
            $cities = $this->get_cities();
            $warehouses = $this->get_warehouses(Session::get('admin_city_id'));
            $providers = Provider::where('status', 1)->orderBy('name')->get();
            return View::make('admin.providers.import_produts_missings')
                    ->with('title', 'Importar orden')
                    ->with('sub_title', 'Importar Productos')
                    ->with('cities', $cities)
                    ->with('warehouses', $warehouses)
                    ->with('providers', $providers);//id proveedor merqueo
                    // ->with( 'providers', 17 );//id proveedor merqueo
        }
    }

    /**
     * Función para obtener los proveedores por tipo
     * @return Response jsons
     */
    public function get_providers_ajax()
    {
        $provider_type = Input::get('provider_type');
        if (!empty($provider_type)) {
            $providers = Provider::where('provider_type', $provider_type)->orderBy('name')->get();
            return Response::json(['providers' => $providers]);
        }
        return Response::json(['providers' => false]);
    }
}
