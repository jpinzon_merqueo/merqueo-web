<?php

namespace admin\inventory\massive_transfer_picking;

use admin\AdminController;
use City;
use DB;
use ErrorLog;
use Event;
use exceptions\MerqueoException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use Redirect;
use Response;
use Store;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;

class MassiveTransferPickingController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new WarehouseStorageEventHandler());
    }

    /**
     * Muestra inicio del módulo.
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Traslado de productos de picking';
        $cities = $this->get_cities();
        $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'));
        if (Session::get('admin_designation') != 'Super Admin') {
            $warehouses = $warehouses->where('id', Session::get('admin_warehouse_id'));
        }
        $warehouses = $warehouses->get();

        return View::make('admin.inventory.massive_transfer_picking.index', compact(
            'title',
            'cities',
            'warehouses'
        ));
    }

    /**
     * Obtiene las bodegas por ciudad.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesAjax()
    {
        $cityId = Input::get('city_id');
        $warehouses = Warehouse::where('city_id', $cityId)->get();

        try {
            if (count($warehouses) < 1) {
                throw new MerqueoException('No se encontraron bodegas de esta ciudad.');
            }
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 400);
        }

        return Response::json($warehouses, 200);
    }

    /**
     * Obtiene los productos por posición en las bodegas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsPickingAjax()
    {
        $warehouseId = Input::get('warehouse_id');
        $position = Input::get('position');
        $positionHeight = Input::get('position_height');

        $products = \StoreProductWarehouse::with('storeProduct.product')
            ->where('warehouse_id', $warehouseId)
            ->where('storage_position', $position)
            ->where('storage_height_position', $positionHeight)
            ->where('status', 1)
            ->orderBy('picking_stock', 'Desc')
            ->get();

        return Response::json($products, 200);
    }

    /**
     * Realiza el traslado entre posiciones.
     * @return \Illuminate\Http\JsonResponse
     */
    public function transferProductsPicking()
    {
        $warehouseId = Input::get('warehouse_id');
        $storageType = Input::get('storage_type');
        $positionFrom = Input::get('from_position');
        $positionHeightFrom = Input::get('from_height_Position');
        $positionTo = Input::get('to_position');
        $positionHeightTo = Input::get('to_height_Position');
        $productsChecked = Input::get('productsChecked');

        try {
            if (!count($productsChecked)) {
                throw new MerqueoException('Debe seleccionar al menos un producto.');
            }
            $isOriginBlocked = Warehouse::validateBlockedPositions(
                $warehouseId,
                'Alistamiento',
                $storageType,
                $positionHeightFrom,
                $positionFrom
            );
            if ($isOriginBlocked) {
                throw new MerqueoException('La posición de origen está bloqueada por conteo.');
            }

            $isDestinyBlocked = Warehouse::validateBlockedPositions(
                $warehouseId,
                'Alistamiento',
                $storageType,
                $positionHeightTo,
                $positionTo
            );
            if ($isDestinyBlocked) {
                throw new MerqueoException('La posición de destino está bloqueada por conteo.');
            }
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 400);
        }

        try {
            DB::beginTransaction();
            foreach ($productsChecked as $data) {
                $storeProductWarehouse = \StoreProductWarehouse::lockForUpdate()->find($data["id"]);
                $storeProductWarehouse->storage_position = $positionTo;
                $storeProductWarehouse->storage_height_position = $positionHeightTo;
                $storeProductWarehouse->save();
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                return Response::json('Otro proceso se está ejecutando sobre estos productos, 
            espera un momento para volver a intentarlo.', 400);
            }
            return Response::json($e->getMessage(), 400);
        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json($e->getMessage(), 400);
        }
        return Response::json('Se ha generado el traslado correctamente.', 200);
    }
}
