<?php

namespace admin\inventory\massive_counting;

use Carbon\Carbon;
use ErrorLog;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use ProductStockUpdate;
use Store;
use StoreProductWarehouse;
use WarehouseStorage;
use WarehouseStorageLog;
use admin\AdminController;

class MassiveCountingController extends AdminController
{

    /**
     * Pantalla inicial del módulo
     * @return [type] [description]
     */
    public function index()
    {
        $data['title'] = 'Conteo Masivo';
        $data['cities'] = $this->get_cities();
        $data['first_city'] = $data['cities']->first();
        $data['warehouses'] = $this->get_warehouses(Session::get('admin_city_id'));
        $data['first_warehouse'] = $data['warehouses']->first();
        $data['date'] = Carbon::now()->toDateString();

        return View::make('admin.inventory.massive_counting.index', $data);
    }

    /**
     * Obtiene los productos para mostrar en el buscador
     * @return Json Productos encontrados
     */
    public function getProductsAjax()
    {
        $city_id = Input::get('city_id');
        $store = Store::getActiveStores($city_id)->first();
        $warehouse_id = Input::get('warehouse_id');
        $storage = Input::get('storage');
        $search = Input::get('search');
        $action = Input::get('action');
        $position = Input::get('position');
        $position_height = Input::get('position_height');
        $is_added = DB::raw('0 AS added');


        $products = Product::join('store_products', function ($join) use ($store, $storage) {
            $join->on('products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', '=', $store->id)
                ->where('store_products.storage', '=', $storage);
        });

        if ($action == 'Alistamiento') {
            $products = $products->join('store_product_warehouses',
                function ($join) use ($warehouse_id, $position, $position_height) {
                    $join->on('store_products.id', '=', 'store_product_warehouses.store_product_id');
                    $join->where('store_product_warehouses.warehouse_id', '=', $warehouse_id);
                    $join->where('store_product_warehouses.storage_position', '=', $position);
                    $join->where('store_product_warehouses.storage_height_position', '=', $position_height);
                });
            $is_added = DB::raw('1 AS added');
        }

        $products = $products->select(
            'products.id AS product_id',
            'products.name',
            'products.quantity',
            'products.unit',
            'products.image_small_url',
            'store_products.id AS store_products_id',
            'store_products.handle_expiration_date',
            $is_added,
            DB::raw('"" AS quantity_counted'),
            DB::raw('"" AS expiration_date')
        )
            ->where(function ($query) use ($search) {
                $query->where('products.name', 'like', '%' . $search . '%');
                $query->orWhere('products.id', 'like', '%' . $search . '%');
                $query->orWhere('products.reference', 'like', '%' . $search . '%');
            })
            ->get();

        return Response::json($products);
    }

    public function createMassiveCountingAjax()
    {
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $action = Input::get('action');
        $storage = Input::get('storage');
        $storage_letter = Input::get('storage_letter');
        $storage_number = Input::get('storage_number');
        $products_added = Input::get('products_added');
        $admin_id = Session::get('admin_id');

        try {
            DB::beginTransaction();
            if ($action == 'Alistamiento') {
                foreach ($products_added as $key => $product) {
                    $store_product_warehouse = StoreProductWarehouse::where('warehouse_id', $warehouse_id)
                        ->where('store_product_id', $product['store_products_id'])
                        ->where('storage_position', $storage_number)
                        ->where('storage_height_position', $storage_letter)
                        ->lockForUpdate()
                        ->first();
                    if ($store_product_warehouse->picking_stock != $product['quantity_counted']) {
                        $product_stock_update = new ProductStockUpdate;
                        $product_stock_update->admin_id = $admin_id;
                        $product_stock_update->store_product_id = $product['store_products_id'];
                        $product_stock_update->warehouse_id = $warehouse_id;

                        if ($store_product_warehouse->picking_stock < $product['quantity_counted']) {
                            $type = 'Aumento de unidades';
                            $difference = $product['quantity_counted'] - $store_product_warehouse->picking_stock;
                        } elseif ($store_product_warehouse->picking_stock > $product['quantity_counted']) {
                            $type = 'Disminución de unidades';
                            $difference = $store_product_warehouse->picking_stock - $product['quantity_counted'];
                        }
                        $product_stock_update->type = $type;
                        $product_stock_update->quantity = $difference;
                        $product_stock_update->quantity_stock_before = $store_product_warehouse->picking_stock;
                        $product_stock_update->quantity_stock_after = $product['quantity_counted'];
                        $product_stock_update->reason = 'Módulo Conteo Masivo';
                        $product_stock_update->save();

                        $store_product_warehouse->picking_stock = $product['quantity_counted'];
                        $store_product_warehouse->save();
                    }
                }
            } elseif ($action == 'Almacenamiento') {
                $warehouse_storages = WarehouseStorage::where('warehouse_id', $warehouse_id)
                    ->where('position', $storage_number)
                    ->where('position_height', $storage_letter)
                    ->lockForUpdate()
                    ->get();

                if (!empty($warehouse_storages)) {
                    foreach ($warehouse_storages as $key => $warehouse_storage) {
                        $warehouse_storage_log = new WarehouseStorageLog;
                        $warehouse_storage_log->admin_id = $admin_id;
                        $warehouse_storage_log->warehouse_id = $warehouse_storage->warehouse_id;
                        $warehouse_storage_log->store_product_id = $warehouse_storage->store_product_id;
                        $warehouse_storage_log->module = 'Conteo Masivo';
                        $warehouse_storage_log->movement = 'remove';
                        $warehouse_storage_log->quantity_from = $warehouse_storage->quantity;
                        $warehouse_storage_log->position_from = $warehouse_storage->position;
                        $warehouse_storage_log->position_height_from = $warehouse_storage->position_height;
                        $warehouse_storage_log->save();
                        $warehouse_storage->delete();
                    }
                }

                foreach ($products_added as $key => $product) {
                    $warehouse_storage = new WarehouseStorage;
                    $warehouse_storage->admin_id = $admin_id;
                    $warehouse_storage->warehouse_id = $warehouse_id;
                    $warehouse_storage->store_product_id = $product['store_products_id'];

                    if ($storage == 'Refrigerado' || $storage == 'Congelado') {
                        $storage = 'Frio';
                    }

                    $warehouse_storage->storage_type = $storage;
                    $warehouse_storage->type = 'Producto';
                    $warehouse_storage->quantity = $product['quantity_counted'];

                    if ((int)$product['handle_expiration_date']) {
                        $warehouse_storage->expiration_date = $product['expiration_date'];
                    }

                    $warehouse_storage->position = $storage_number;
                    $warehouse_storage->position_height = $storage_letter;
                    $warehouse_storage->save();

                    $warehouse_storage_log = new WarehouseStorageLog;
                    $warehouse_storage_log->admin_id = $admin_id;
                    $warehouse_storage_log->warehouse_id = $warehouse_id;
                    $warehouse_storage_log->store_product_id = $product['store_products_id'];
                    $warehouse_storage_log->module = 'Conteo Masivo';
                    $warehouse_storage_log->movement = 'storage';
                    $warehouse_storage_log->quantity_to = $product['quantity_counted'];
                    $warehouse_storage_log->position_to = $storage_number;
                    $warehouse_storage_log->position_height_to = $storage_letter;
                    $warehouse_storage_log->save();
                }
            }
            $response['status'] = true;
            $response['message'] = 'Se han actualizado las cantidades en las posisiones';
            DB::commit();
            return Response::json($response);
        } catch (QueryException $e) {
            DB::rollBack();
            $response['status'] = false;
            $response['message'] = 'Ocurrió in error al actualizar las cantidades en ' . $action;
            $response['error'] = $e->getMessage();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                $response['error'] = 'Otro proceso se está ejecutando sobre estos productos, 
            espera un momento para volver a intentarlo.';
            }
            return Response::json($response);
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = 'Ocurrió in error al actualizar las cantidades en ' . $action;
            $response['error'] = $e->getMessage();
            DB::rollback();

            return Response::json($response);
        }

        exit;
    }
}
