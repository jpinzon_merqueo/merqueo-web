<?php

namespace admin\inventory\investigation;

use admin\AdminController;
use Carbon\Carbon;
use City;
use DB;
use Event;
use exceptions\MerqueoException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use Redirect;
use Response;
use Validator;
use Store;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;
use InvestigationStock;

class InvestigationStockController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Muestra inicio del módulo.
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Investigación del inventario';
        $cities = $this->get_cities();
        $warehouses = $this->get_warehouses();

        return View::make('admin.inventory.investigation.index', compact(
            'title',
            'cities',
            'warehouses'
        ));
    }

    /**
     * Obtiene las bodegas por ciudad.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesAjax()
    {
        $cityId = Input::get('cityId');
        $warehouses = Warehouse::where('city_id', $cityId)->get();

        if (count($warehouses) < 1) {
            return Response::json('No se encontraron bodegas de esta ciudad.', 422);
        }


        return Response::json($warehouses, 200);
    }

    /**
     * Consulta el listado de investigation stock
     * @return \Illuminate\Http\JsonResponse
     */
    public function showInvestigationStock()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'warehouseId' => 'required|exists:warehouses,id',
                'startDate' => 'required|date',
                'endDate' => 'required|date'
            ]);
            if ($validator->fails()) {
                return Response::json($validator->errors()->all(), 422);
            }
            $warehouseId = Input::get('warehouseId');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $product = Input::get('product');
            $status = Input::get('status');
            $module = Input::get('module');
            $investigationStock = new InvestigationStock();
            $investigationStocks = $investigationStock->getStockInvestigation($warehouseId, $module, $status, $product,
                $startDate, $endDate);

            return Response::json($investigationStocks, 200);
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 422);
        }
    }

    /**
     * Obtiene un registro específico de investigación stock
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInvestigationStock()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'warehouseId' => 'required|exists:warehouses,id',
                'storeProductId' => 'required',
                'startDate' => 'required|date',
                'endDate' => 'required|date'
            ]);
            if ($validator->fails()) {
                return Response::json($validator->errors()->all(), 422);
            }
            $warehouseId = Input::get('warehouseId');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $storeProductId = Input::get('storeProductId');
            $investigationStock = new InvestigationStock();
            $investigationStockMovements = $investigationStock->getStorageStockInvestigation($storeProductId,
                $warehouseId, $startDate, $endDate);
            $dataResponse = [
                'tipifications' => $investigationStock::TIPIFICATION_LIST,
                'investigationStock' => $investigationStockMovements
            ];
            return Response::json($dataResponse, 200);
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 422);
        }
    }

    /**
     * Actualiza el estado y clasificación de un stock en investigación
     * @param array investigationStock con id del stock de investigación, el nuevo estado y la clasificación
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateInvestigationStock()
    {
        try {
            $input = Input::all();
            DB::beginTransaction();
            foreach ($input as $data) {
                $validator = Validator::make($data, [
                    'id' => 'required|exists:investigation_stocks,id',
                    'status_new' => 'required',
                    'typification_new' => 'required_if:status_new,Cerrado',
                ]);
                if ($validator->fails()) {
                    throw new MerqueoException($validator->errors()->all());
                }
                $investigationStock = new InvestigationStock();
                $investigationStockResponse = $investigationStock->updateInvestigationStock($data["id"],
                    $data["status_new"], $data["typification_new"]);
            }

            DB::commit();
            return Response::json($investigationStockResponse, 200);
        } catch (MerqueoException $e) {
            DB::rollback();
            return Response::json($e->getMessage(), 422);
        }
    }

}
