<?php

namespace admin\inventory\shrinkage;

use Illuminate\Database\QueryException;
use Request, View, Input, Redirect, Session, DB, City, Product, Response, Config, Route, ProviderOrderDetail, Order, Store, Department, Shelf, PHPExcel, PHPExcel_Style_Fill,
    PHPExcel_Writer_Excel2007, Pdf, PHPExcel_Cell_DataType, Shrinkage, ShrinkageDetail, Provider, StoreProduct, AdminLog, Warehouse, Exception, StoreProductWarehouse,
    ShrinkageOriginLog, WarehouseStorage, WarehouseStorageLog, admin\AdminController;

class AdminShrinkageController extends AdminController
{
    /**
     * Grilla de mermas
     */
    public function index()
    {
        if (!Request::ajax()) {
            $shrinkages = Shrinkage::join('shrinkage_details', 'shrinkages.id', '=', 'shrinkage_details.shrinkage_id')
                ->join('admin', 'admin.id', '=', 'shrinkages.admin_id')
                ->join('warehouses', 'warehouses.id', '=', 'shrinkages.warehouse_id')
                ->whereIn('shrinkages.status', ['Cerrada', 'Pendiente'])
                ->select('shrinkages.id',
                    DB::raw("DATE_FORMAT(shrinkages.created_at, '%d/%m/%Y %H:%i:%s') AS created_date"),
                    DB::raw("DATE_FORMAT(shrinkages.close_date, '%d/%m/%Y %H:%i:%s') AS close_date"),
                    DB::raw("SUM(shrinkage_details.quantity_reported) AS quantity_reported"),
                    'shrinkages.status',
                    'admin.fullname')
                ->groupBy('shrinkage_id')
                ->get();

            return View::make('admin.inventory.shrinkage.shrinkages')
                ->with('title', 'Mermas')
                ->with('cities', $this->get_cities())
                ->with('warehouses', $this->get_warehouses())
                ->with('shrinkages', $shrinkages);
        } else {

            $search = Input::has('s') ? Input::get('s') : '';
            $shrinkages = Shrinkage::join('shrinkage_details', 'shrinkages.id', '=', 'shrinkage_details.shrinkage_id')
                ->join('admin', 'admin.id', '=', 'shrinkages.admin_id')
                ->join('warehouses', 'warehouses.id', '=', 'shrinkages.warehouse_id')
                ->join('cities', 'cities.id', '=', 'warehouses.city_id')
                ->Where('shrinkages.id', 'LIKE', '%' . $search . '%')
                ->whereIn('shrinkages.status', ['Cerrada', 'Pendiente'])
                ->select('shrinkages.id', 'city',
                    DB::raw("DATE_FORMAT(shrinkages.created_at, '%d/%m/%Y %H:%i:%s') AS created_date"),
                    DB::raw("DATE_FORMAT(shrinkages.close_date, '%d/%m/%Y %H:%i:%s') AS close_date"),
                    DB::raw("SUM(shrinkage_details.quantity_reported) AS quantity_reported"),
                    'shrinkages.status',
                    'admin.fullname');
            //filtros
            if (Input::get('status'))
                $shrinkages->where('shrinkages.status', Input::get('status'));

            if (Input::get('city_id'))
                $shrinkages->where('cities.id', Input::get('city_id'));

            if (Input::get('warehouse_id'))
                $shrinkages->where('shrinkages.warehouse_id', Input::get('warehouse_id'));

            $shrinkages = $shrinkages->groupBy('shrinkages.id')->orderBy('shrinkages.id', 'desc')->limit(80)->get();

        }

        return View::make('admin.inventory.shrinkage.shrinkages')
            ->with('title', 'Mermas')
            ->with('shrinkages', $shrinkages)
            ->renderSections()['content'];
    }

    /**
     * Detalle de una orden de servicio
     */
    public function details($id)
    {
        $shrinkage = Shrinkage::join('shrinkage_details', 'shrinkages.id', '=', 'shrinkage_details.shrinkage_id')
            ->join('admin', 'shrinkages.admin_id', '=', 'admin.id')
            ->join('warehouses', 'warehouses.id', '=', 'shrinkages.warehouse_id')
            ->join('cities', 'warehouses.city_id', '=', 'cities.id')
            ->where('shrinkages.id', '=', $id)
            ->select('shrinkages.*', 'city', DB::raw('SUM(shrinkage_details.quantity_reported) AS quantity_reported'), DB::raw('admin.fullname AS admin'), DB::raw('shrinkages.status AS status'))
            ->first();

        $shrinkage_products = ShrinkageDetail::join('shrinkages', 'shrinkages.id', '=', 'shrinkage_details.shrinkage_id')
            ->join('store_products', 'shrinkage_details.store_product_id', '=', 'store_products.id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('providers', 'store_products.provider_id', '=', 'providers.id')
            ->join('admin', 'shrinkages.admin_id', '=', 'admin.id')
            ->leftJoin('shrinkage_typifications', 'shrinkage_typifications.id', '=', 'shrinkage_subconcept_id')
            ->where('shrinkage_details.shrinkage_id', $id)
            ->select('shrinkages.*', 'shrinkage_details.*', DB::raw('shrinkage_typifications.name AS typifications'), DB::raw('admin.fullname AS admin'), DB::raw('providers.name AS provider'), DB::raw("DATE_FORMAT(shrinkages.created_at, '%d/%m/%Y %H:%i:%s') AS created_date"))
            ->get();

        if (!$shrinkage)
            return Redirect::route('adminShrinkages.index')->with('error', 'No existe la merma.');

        $data = [
            'title' => 'Merma',
            'shrinkage' => $shrinkage,
            'shrinkage_products' => $shrinkage_products,
            'typifications' => \ShrinkageTypifications::where('parent_id', 0)->get()
        ];

        return View::make('admin.inventory.shrinkage.shrinkage_details', $data);
    }

    /**
     * formulario de registro de merma por origen
     */
    public function shrinkage_origin($warehouse_id, $origin)
    {
        $data = [
            'title' => 'Origen de Merma de Productos',
            'origin' => $origin,
            'origin_warehouse_id' => $warehouse_id
        ];
        return View::make('admin.inventory.shrinkage.shrinkage_origin_form', $data);
    }

    /**
     * registro de cantidad de un producto en merma
     */
    public function save_shrinkage_origin()
    {
        $data = [
            'origin' => Input::get('origin'),
            'origin_warehouse_id' => Input::get('origin_warehouse_id')
        ];

        if (get_config_option('block_reception_return_storage', Input::get('origin_warehouse_id')))
            return Redirect::route('adminShrinkages.Origin', $data)->with('type', 'error')->with('message', 'Reporte de merma de almacenamiento esta bloqueado temporalmente, por favor contacte al administrador.');
        try{
            DB::beginTransaction();
            if (Input::get('origin') == 'Almacenamiento') {
                $warehouse_storage = WarehouseStorage::find(Input::get('id'));

                if(!$warehouse_storage) {
                    return Redirect::route('adminShrinkages.Origin', $data)->with('title', 'Origen de Merma de Productos')->with('type', 'error')->with('message', 'No existe registro del producto a reportar.');
                }
                if ($warehouse_storage->quantity < Input::get('quantity')) {
                    return Redirect::route('adminShrinkages.Origin', $data)->with('type', 'error')->with('message', 'Las unidades reportadas es mayor a las almacenadas en la posición.');
                }
                $validate_block_position = Warehouse::validateBlockedPositions(Input::get('origin_warehouse_id'), 'Almacenamiento', Input::get('storage_type'), $warehouse_storage->position_height, $warehouse_storage->position);
                if($validate_block_position) {
                    return Redirect::route('adminShrinkages.Origin', $data)->with('type', 'error')->with('message', 'La posición del producto seleccionado se encuentra bloqueada debido a un conteo por posición pendiente, intenta más tarde.');
                }
                $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($warehouse_storage->warehouse_id, $warehouse_storage->store_product_id);
                $store_product_warehouse->shrinkage_stock += Input::get('quantity');
                $store_product_warehouse->save();



                if ($warehouse_storage->quantity > Input::get('quantity')) {
                    $warehouse_storage->quantity -= Input::get('quantity');
                    $warehouse_storage->save();
                    $movement = 'update';
                } elseif ($warehouse_storage->quantity == Input::get('quantity')) {
                    $warehouse_storage->delete();
                    $movement = 'remove';
                }

                //log
                $warehouse_storage_log = new WarehouseStorageLog;
                $warehouse_storage_log->admin_id = $warehouse_storage->admin_id;
                $warehouse_storage_log->warehouse_id = $warehouse_storage->warehouse_id;
                $warehouse_storage_log->store_product_id = $warehouse_storage->store_product_id;
                $warehouse_storage_log->module = 'Merma';
                $warehouse_storage_log->movement = $movement;
                $warehouse_storage_log->quantity_from = Input::get('quantity');
                $warehouse_storage_log->position_from = $warehouse_storage->position;
                $warehouse_storage_log->position_height_from = $warehouse_storage->position_height;
                $warehouse_storage_log->save();
            } else {

                if (!$store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking(Input::get('origin_warehouse_id'), Input::get('store_product_id'))) {
                    return Redirect::route('adminShrinkages.Origin', $data)->with('type', 'error')->with('message', 'No existe el producto en la bodega.');
                }
                if ($store_product_warehouse->picking_stock < Input::get('quantity')) {
                    return Redirect::route('adminShrinkages.Origin', $data)->with('type', 'error')->with('message', 'Las unidades reportadas es mayor a las que estan en la posición de alistamiento.');
                }
                $validate_block_position = Warehouse::validateBlockedPositions(Input::get('origin_warehouse_id'), 'Alistamiento', Input::get('storage_type'), $store_product_warehouse->storage_height_position, $store_product_warehouse->storage_position);
                if($validate_block_position) {
                    return Redirect::route('adminShrinkages.Origin', $data)->with('type', 'error')->with('message', 'La posición del producto seleccionado se encuentra bloqueada debido a un conteo por posición pendiente, intenta más tarde.');
                }
                $store_product_warehouse->shrinkage_stock += Input::get('quantity');
                $store_product_warehouse->picking_stock -= Input::get('quantity');
                $store_product_warehouse->save();

                //log
                $shrinkage_origin_log = new ShrinkageOriginLog();
                $shrinkage_origin_log->admin_id = Session::get('admin_id');
                $shrinkage_origin_log->warehouse_id = Input::get('origin_warehouse_id');
                $shrinkage_origin_log->store_product_id = $store_product_warehouse->store_product_id;
                $shrinkage_origin_log->quantity = Input::get('quantity');
                $shrinkage_origin_log->origin = Input::get('origin');
                $shrinkage_origin_log->position = $store_product_warehouse->storage_position;
                $shrinkage_origin_log->position_height = $store_product_warehouse->storage_height_position;
                $shrinkage_origin_log->save();
            }
            DB::commit();
            return Redirect::route('adminShrinkages.Origin', $data)->with('title', 'Origen de Merma de Productos')->with('type', 'success')->with('message', 'Producto reportado con éxito.');

        }catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                \ErrorLog::add($e, 513);
                return Redirect::route('adminShrinkages.Origin', $data)->with('title', 'Origen de Merma de Productos')->with('type', 'error')->with('message', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');
            }
            \ErrorLog::add($e);
            return Redirect::back()->withErrors([
                'Exception',
                $e->getMessage()
            ]);
        } catch(Exception $e){
            DB::rollBack();
            \ErrorLog::add($e);
            return Redirect::route('adminShrinkages.Origin', $data)->with('title', 'Origen de Merma de Productos')->with('type', 'error')->with('message', 'Ocurrio un error al reportar el producto.');
        }


    }

    /**
     * Obtener productos en posicion especifica por ajax
     */
    public function get_products_position_ajax()
    {
        $warehouse_id = Input::get('warehouse_id');
        $position = Input::get('position');
        $position_height = Input::get('position_height');
        $search = Input::get('s');
        $origin = Input::get('origin');

        if ($origin == 'Almacenamiento') {
            $warehouse_storage = WarehouseStorage::where('warehouse_storages.warehouse_id', $warehouse_id)
                ->where('storage_type', Input::get('storage_type'))
                ->leftJoin('store_products', 'warehouse_storages.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->select('warehouse_storages.id', 'warehouse_storages.type', 'products.image_small_url', 'products.reference', 'products.quantity AS product_quantity', 'products.unit',
                    'products.name AS name', 'warehouse_storages.position', 'warehouse_storages.position_height', 'warehouse_storages.expiration_date', 'warehouse_storages.quantity',
                    'warehouse_storages.created_at');

            if ($position && $position_height) {
                $warehouse_storage->where('position', $position);
                $warehouse_storage->whereRaw("position_height = '" . $position_height . "'");
            }

            if ($search)
                $warehouse_storage->where('products.name', 'like', '%' . $search . '%');

            $warehouse_storage = $warehouse_storage->orderBy(DB::raw("FIELD(warehouse_storages.type, 'Producto', 'Sampling', 'Canastilla merqueo', 'Canastilla proveedor', 'Estiva')"))
                ->orderBy('products.name')
                ->orderBy('warehouse_storages.expiration_date')
                ->orderBy('warehouse_storages.created_at')
                ->limit(80)
                ->get();

            $products = $warehouse_storage;
        } else {
            $store_product_warehouse = StoreProductWarehouse::where('store_product_warehouses.warehouse_id', $warehouse_id)
                ->where('store_products.storage', Input::get('storage_type'))
                ->join('store_products', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->select('store_products.id', DB::raw('"" AS type'), 'products.image_small_url', 'products.reference', 'products.quantity AS product_quantity', 'products.unit',
                    'products.name AS name', 'store_product_warehouses.storage_position AS position', 'store_product_warehouses.storage_height_position AS position_height', DB::raw('"" AS expiration_date'), 'store_product_warehouses.picking_stock AS quantity',
                    'store_product_warehouses.created_at');

            if ($position && $position_height) {
                $store_product_warehouse->where('store_product_warehouses.storage_position', $position);
                $store_product_warehouse->whereRaw("store_product_warehouses.storage_height_position = '" . $position_height . "'");
            }

            if ($search)
                $store_product_warehouse->where('products.name', 'like', '%' . $search . '%');

            $store_product_warehouse = $store_product_warehouse->orderBy('products.name')
                ->orderBy('store_product_warehouses.created_at')
                ->limit(80)
                ->get();

            $products = $store_product_warehouse;
        }

        $data = [
            'products' => $products,
        ];

        return View::make('admin.inventory.shrinkage.shrinkage_origin_form', $data)->renderSections()['product-position-table'];
    }

    /**
     * Buscar productos por ajax
     */
    public function search_products_ajax()
    {
        $search = Input::get('s');
        $store_products = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
            ->where(function ($query) use ($search) {
                $query->orWhere('products.reference', 'LIKE', '%' . $search . '%');
                $query->orWhere('products.name', 'LIKE', '%' . $search . '%');
            })
            ->where('type', 'Simple')
            ->select('store_products.id', 'store_products.handle_expiration_date', 'products.image_small_url', 'products.reference', 'products.quantity', 'products.unit', 'products.name AS name')
            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
            ->orderBy('store_products.department_id', 'desc')
            ->orderBy('products.name', 'asc');

        if (Input::get('storage_type') == 'Seco')
            $store_products->where('store_products.storage', 'Seco');
        else $store_products->where(function ($query) use ($search) {
            $query->orWhere('store_products.storage', 'Congelado');
            $query->orWhere('store_products.storage', 'Refrigerado');
        });

        $store_products = $store_products->get();
        $data = [
            'products' => $store_products,
        ];

        return View::make('admin.inventory.shrinkage.shrinkage_origin_form', $data)->renderSections()['product-table'];
    }

    /**
     *  Filtro y grilla con listado de productos a solicitar
     */
    public function add($type)
    {
        $section = 'content_products_query';
        if (!Request::ajax()) {
            //Session::forget('shrinkages_products');
            $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->where('city_id', 1)->get();

            return View::make('admin.inventory.shrinkage.shrinkage_form')
                ->with('title', 'Crear Merma')
                ->with('type', $type)
                ->with('cities', $this->get_cities())
                ->with('warehouses', $this->get_warehouses())
                ->with('typifications', \ShrinkageTypifications::where('parent_id', 0)->get())
                ->with('stores', $stores);
        } else {
            //cargar ids de productos de sesion
            $store_products_session = $store_product_ids = array();
            if (Session::has('admin_shrinkages_products')) {
                $store_products_session = json_decode(Session::get('admin_shrinkages_products'), true);
                $store_product_ids = array_keys($store_products_session);
            }

            //agregar nuevos productos a sesion
            if (Input::has('products')) {
                $store_products_request = json_decode(Input::get('products'), true);
                foreach ($store_products_request as $id => $product) {
                    $store_product_ids[] = $id;
                }
                $store_products_session = Session::has('admin_shrinkages_products') ? $store_products_request + $store_products_session : $store_products_request;
                Session::forget('admin_shrinkages_products');
                Session::put('admin_shrinkages_products', json_encode($store_products_session));
            }

            $search = Input::has('s') ? Input::get('s') : '';
            //obtener productos que hay que solicitar
            $store_products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                ->join('providers', 'store_products.provider_id', '=', 'providers.id')
                ->join('stores', 'stores.id', '=', 'store_products.store_id')
                ->join('cities', 'cities.id', '=', 'stores.city_id')
                ->join('departments', 'departments.id', '=', 'store_products.department_id')
                ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                ->where('products.type', 'Simple')
                ->select('store_products.id',
                    'store_product_warehouses.warehouse_id',
                    'store_product_warehouses.shrinkage_stock',
                    'products.name',
                    'products.image_small_url',
                    'products.reference',
                    'products.quantity AS product_quantity',
                    'products.unit',
                    'departments.name AS department',
                    'store_products.*',
                    'store_product_warehouses.shrinkage_stock AS current_stock',
                    'providers.name AS provider');

            if(!empty($search)) {
                $store_products->where(function ($query) use ($search) {
                    $query->where('store_products.provider_plu', 'LIKE', '%' . $search . '%');
                    $query->orWhere('products.reference', 'LIKE', '%' . $search . '%');
                    $query->orWhere('products.name', 'LIKE', '%' . $search . '%');
                });
            }

            if (!empty($store_product_ids))
                $store_products->whereNotIn('store_products.id', $store_product_ids);

            if (Input::has('city_id'))
                $store_products->where('cities.id', Input::get('city_id'));

            if (Input::has('warehouse_id'))
                $store_products->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'));

            if (Input::get('store_id'))
                $store_products->where('stores.id', Input::get('store_id'));

            if ($type == 'Automatica')
                $store_products->where('store_product_warehouses.shrinkage_stock', '>', '0');

            $store_products = $store_products->groupBy('store_products.id')->limit(80)->orderBy('store_products.created_at', 'DESC')->get();
            $data = [
                'section' => $section,
                'products' => $store_products
            ];

            $view = View::make('admin.inventory.shrinkage.shrinkage_form', $data)->renderSections();
            $data_json['html'] = $view['content_products_query'];
            $data_json['success'] = true;
            $data_json['message'] = '';
            return Response::json($data_json);
        }
    }

    /**
     * Obtener productos agregados para generar merma
     */
    public function get_products_ajax()
    {
        //cargar ids de productos de sesion
        $store_product_ids = $store_products_session = array();
        if (Session::has('admin_shrinkages_products')) {
            $store_products_session = json_decode(Session::get('admin_shrinkages_products'), true);
            foreach ($store_products_session as $id => $quantity)
                $store_product_ids[] = $id;
        }

        //obtener productos a solicitar desde sesion
        $store_products_added = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
            ->join('providers', 'store_products.provider_id', '=', 'providers.id')
            ->join('stores', 'stores.id', '=', 'store_products.store_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->whereIn('store_products.id', $store_product_ids)
            ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
            ->select('store_products.id',
                'store_product_warehouses.warehouse_id',
                'products.name',
                'products.image_small_url',
                'products.reference',
                'products.quantity AS product_quantity',
                'products.unit',
                'store_products.*',
                'departments.name AS department',
                'store_product_warehouses.shrinkage_stock AS current_stock',
                'providers.name AS provider')
            ->limit(80)
            ->groupBy('store_product_warehouses.store_product_id')
            ->orderBy('store_products.created_at', 'DESC')
            ->get();

        if (Session::has('admin_shrinkages_products') && count($store_products_added)) {
            foreach ($store_products_added as $store_product) {
                $store_product->quantity_unit_to_report = $store_products_session[$store_product->id][0];
                $store_product->product_status = $store_products_session[$store_product->id][1];
                $store_product->product_reason = \ShrinkageTypifications::where('id', $store_products_session[$store_product->id][2])->pluck('name');
            }
        }

        return View::make('admin.inventory.shrinkage.shrinkage_form')
            ->with('products_added', $store_products_added)
            ->renderSections()['content_products_shrinkage'];
    }

    /**
     *  Elimina un producto de la merma a crear
     */
    public function delete_product_ajax()
    {
        if (Input::has('id')) {
            $store_products_session = json_decode(Session::get('admin_shrinkages_products'), true);
            unset($store_products_session[Input::get('id')]);

            $store_product_ids = array_keys($store_products_session);

            //obtener productos a solicitar desde sesion
            $store_products_added = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
                ->join('providers', 'store_products.provider_id', '=', 'providers.id')
                ->join('stores', 'stores.id', '=', 'store_products.store_id')
                ->join('departments', 'departments.id', '=', 'store_products.department_id')
                ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_id')
                ->whereIn('store_products.id', $store_product_ids)
                ->select('store_products.id',
                    'store_product_warehouses.warehouse_id',
                    'products.name',
                    'products.image_small_url',
                    'products.reference',
                    'products.quantity AS product_quantity',
                    'products.unit',
                    'store_products.*',
                    'departments.name AS department',
                    'providers.name AS provider')
                ->orderBy('products.created_at', 'DESC')
                ->get();

            Session::forget('admin_shrinkages_products');
            Session::put('admin_shrinkages_products', json_encode($store_products_session));

            if (Session::has('admin_shrinkages_products')) {
                foreach ($store_products_added as $store_product)
                    $store_product->quantity_unit_to_report = $store_products_session[$store_product->id][0];
            }

            return View::make('admin.inventory.shrinkage.shrinkage_form')
                ->with('products_added', $store_products_added)
                ->renderSections()['content_products_shrinkage'];
        }
    }

    /**
     * Elimina producto de la merma
     *
     * @param  int $id ID de la merma
     * @param  int $product_id ID del producto en tabla shrinkage_details
     */
    public function remove_product($id, $product_id)
    {
        $shrinkage_detail = ShrinkageDetail::where('shrinkage_id', $id)->where('id', $product_id)->first();
        if ($shrinkage_detail) {
            $shrinkage_detail->delete();

            $log = new AdminLog();
            $log->table = 'shrinkage_details';
            $log->row_id = $shrinkage_detail->id;
            $log->action = 'delete_product';
            $log->admin_id = Session::get('admin_id');
            $log->save();

            return Redirect::back()->with('success', 'Producto eliminado con éxito.');
        }
        return Redirect::back()->with('error', 'No se puede eliminar este producto.');
    }

    /**
     * Actualizar cantidad de producto en merma
     */
    public function update_product()
    {
        if (Input::has('id')) {
            //update price or replace product
            $id = Input::get('id');
            if ($shrinkage_detail = ShrinkageDetail::find($id)) {
                if (Input::has('quantity')) {
                    $shrinkage_detail->quantity_reported = Input::get('quantity');
                    $shrinkage_detail->quantity_stock_after = $shrinkage_detail->quantity_stock_before - $shrinkage_detail->quantity_reported;
                }
                $shrinkage_detail->save();

                $log = new AdminLog();
                $log->table = 'shrinkage_details';
                $log->row_id = $shrinkage_detail->id;
                $log->action = 'update_quantity_product';
                $log->admin_id = Session::get('admin_id');
                $log->save();

                return Redirect::back()->with('success', 'Cantidad de producto actualizada con éxito.');
            }
        }

        return Redirect::back()->with('error', 'No se puede editar la cantidad de este producto.');
    }


    /**
     * Actualizar estado de producto en merma
     */
    public function update_product_ajax()
    {
        if (Input::has('id')) {
            //update price or replace product
            $id = Input::get('id');
            if ($shrinkage_detail = ShrinkageDetail::find($id)) {
                if (Input::has('product_status')) {
                    $shrinkage_detail->status = Input::get('product_status');
                    $shrinkage_detail->shrinkage_subconcept_id = Input::get('product_typifications_status');

                }
                $shrinkage_detail->save();

                $log = new AdminLog();
                $log->table = 'shrinkage_details';
                $log->row_id = $shrinkage_detail->id;
                $log->action = 'update_status_product';
                $log->admin_id = Session::get('admin_id');
                $log->save();

            }
        } else return Redirect::back()->with('error', 'No se puede editar el estado de este producto.');

        return Redirect::back()->with('success', 'Estado de producto actualizado con éxito.');
    }

    /**
     * Crea la merma
     */
    public function save()
    {
        $error = false;
        if (Session::has('admin_shrinkages_products')) {

            try {
                DB::beginTransaction();
                $store_products_session = json_decode(Session::get('admin_shrinkages_products'), true);
                $store_product_ids = array_keys($store_products_session);
                $store_products_added = StoreProduct::join('products', 'store_products.product_id', '=', 'products.id')
                    ->join('providers', 'store_products.provider_id', '=', 'providers.id')
                    ->join('stores', 'stores.id', '=', 'store_products.store_id')
                    ->join('departments', 'departments.id', '=', 'store_products.department_id')
                    ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_id')
                    ->whereIn('store_products.id', $store_product_ids)
                    ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
                    ->select('store_products.id',
                        'store_product_warehouses.warehouse_id',
                        'products.name',
                        'products.image_large_url',
                        'products.reference',
                        'products.quantity',
                        'products.unit',
                        'store_products.*',
                        'departments.name AS department',
                        'store_product_warehouses.picking_stock',
                        'store_product_warehouses.shrinkage_stock',
                        'providers.name AS provider',
                        'store_products.storage',
                        'store_product_warehouses.storage_height_position',
                        'store_product_warehouses.storage_position')
                    ->orderBy('products.created_at','DESC')
                    ->get();
                $products_id_postion_block = '';
                $flag = false;
                foreach($store_products_added as $store_product){
                    $validate_block_position = Warehouse::validateBlockedPositions($store_product->warehouse_id, 'Alistamiento', $store_product->storage, $store_product->storage_height_position, $store_product->storage_position);
                    if($validate_block_position) {
                        $products_id_postion_block .= $store_product->id . ', ';
                        $flag = true;
                    }
                    $store_product->quantity_unit_to_report = $store_products_session[$store_product->id][0];
                    $store_product->status = $store_products_session[$store_product->id][1];
                    $store_product->product_typifications_id = $store_products_session[$store_product->id][2];
                }
                if($flag) {
                    $error = true;
                    throw new Exception('No es posible registrar merma de los productos con <b>ID: ' . $products_id_postion_block . '</b> debido a que se encuentran en conteo por posición.');
                }

                //crear encabezado merma
                $shrinkage = new Shrinkage;
                $shrinkage->admin_id = Session::get('admin_id');
                $shrinkage->warehouse_id = Input::get('warehouse_id');
                $shrinkage->status = 'Pendiente';
                $shrinkage->type = Input::get('type');
                $shrinkage->save();



                //crear detalle de merma
                foreach ($store_products_added as $key => $store_product) {
                    if ($shrinkage->warehouse_id == $store_product->warehouse_id) {
                        $shrinkage_details = new ShrinkageDetail;
                        $shrinkage_details->shrinkage_id = $shrinkage->id;
                        $shrinkage_details->store_product_id = $store_product['id'];
                        $shrinkage_details->product_name = $store_product['name'] . ' ' . $store_product['quantity'] . ' ' . $store_product['unit'];
                        $shrinkage_details->plu = $store_product['provider_plu'] ? $store_product['provider_plu'] : $store_product['reference'];
                        $shrinkage_details->reference = $store_product['reference'];
                        $shrinkage_details->product_quantity = $store_product['quantity'] ? $store_product['quantity'] : 0;
                        $shrinkage_details->product_unit = $store_product['unit'] ? $store_product['unit'] : 'Gr';
                        $shrinkage_details->product_price = $store_product['price'];
                        $shrinkage_details->quantity_reported = $store_product['quantity_unit_to_report'];
                        if ($shrinkage->type == 'Automatica') {
                            $shrinkage_details->quantity_stock_before = $store_product['shrinkage_stock'];
                            $shrinkage_details->quantity_stock_after = $store_product['shrinkage_stock'] - $store_product['quantity_unit_to_report'];
                        }

                        if ($shrinkage->type == 'Manual') {
                            $shrinkage_details->quantity_stock_before = $store_product['picking_stock'];
                            $shrinkage_details->quantity_stock_after = $store_product['picking_stock'] - $store_product['quantity_unit_to_report'];
                        }
                        $shrinkage_details->status = $store_product['status'];
                        $shrinkage_details->shrinkage_subconcept_id = $store_product['product_typifications_id'];
                        $shrinkage_details->image_url = $store_product['image_large_url'];
                        $shrinkage_details->save();

                    } else {
                        $error = true;
                        throw new Exception('No es posible registrar merma del siguiente producto <b>' . $store_product['name'] . ' ' . $store_product['quantity'] . ' ' . $store_product['unit'] . '</b>, no pertenece a la bodega seleccionada.');
                    }
                }

                DB::commit();

                Session::forget('admin_shrinkages_products');

                $log = new AdminLog();
                $log->table = 'shrinkages';
                $log->row_id = $shrinkage->id;
                $log->action = 'insert_shrinkage';
                $log->admin_id = Session::get('admin_id');
                $log->save();

                return Redirect::route('adminShrinkages.index')->with('success', 'Merma guardada con éxito.');

            } catch(QueryException $e){
                DB::rollBack();
                if ($e->getCode() == '40001') {
                    \ErrorLog::add($e, 513);
                    return Redirect::route('adminShrinkages.index')->with('type', 'error')->with('message', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');
                }
                return Redirect::back()->withErrors([
                    'Exception',
                    $e->getMessage()
                ]);
                return Redirect::route('adminShrinkages.Origin', $data)->with('title', 'Origen de Merma de Productos')->with('type', 'error')->with('message', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo..');
            } catch (\Exception $exception) {
                DB::rollback();
                \ErrorLog::add($exception);
                return Redirect::back()->with('error', $exception->getMessage());
                if ($error)
                    return Redirect::back()->with('error', $exception->getMessage());
                else
                    return Redirect::route('adminShrinkages.index')->with('error', 'Ocurrió un error al procesar la merma.');
            }
        } else
            return Redirect::route('adminShrinkages.index')->with('error', 'No se encontraron productos para crear merma.');
    }

    /**
     * Actualiza el estado de una merma
     */
    public function update_status($id)
    {
        try{
            if ($shrinkage = Shrinkage::find($id)) {
                DB::beginTransaction();
                $new_status = Input::get('status');
                if ($new_status == 'Cerrada') {
                    if ($shrinkage->status == 'Cerrada')
                    return Redirect::route('adminShrinkages.details', ['id' => $id])->with('error', 'El estado actual de la merma no es valido.');
                }

                $shrinkage_details = ShrinkageDetail::where('shrinkage_id', $id)->get();
                foreach ($shrinkage_details as $shrinkage_detail) {
                    $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($shrinkage->warehouse_id, $shrinkage_detail->store_product_id);
                    if ($shrinkage->type == 'Manual') {
                        $validate_block_position = Warehouse::validateBlockedPositions($store_product_warehouse->warehouse_id, 'Alistamiento', $store_product_warehouse->storeProduct->storage, $store_product_warehouse->storage_height_position, $store_product_warehouse->storage_position);
                        if($validate_block_position) {
                            return Redirect::route('adminShrinkages.details', ['id' => $id ])->with('error', 'Ocurrió un error al actualizar la merma ya hay algunos productos en conteo por posición.');
                        }
                    } else {
                        break;
                    }
                }

                foreach ($shrinkage_details as $shrinkage_detail)
                {
                    $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($shrinkage->warehouse_id, $shrinkage_detail->store_product_id);
                    if ($store_product_warehouse) {
                        if ($shrinkage->type == 'Automatica') {
                            $shrinkage_detail->quantity_stock_before = $store_product_warehouse->shrinkage_stock;
                            $store_product_warehouse->shrinkage_stock -= $shrinkage_detail->quantity_reported;
                            $shrinkage_detail->quantity_stock_after = $store_product_warehouse->shrinkage_stock;
                        }

                        if ($shrinkage->type == 'Manual') {
                            $shrinkage_detail->quantity_stock_before = $shrinkage_detail->picking_stock;
                            $store_product_warehouse->picking_stock -= $shrinkage_detail->quantity_reported;
                            $shrinkage_detail->quantity_stock_after = $shrinkage_detail->picking_stock;
                        }
                        $store_product_warehouse->save();
                        $shrinkage_detail->save();
                    }
                }

                $shrinkage->status = $new_status;
                $shrinkage->close_date = date('Y-m-d H:i:s');
                $shrinkage->save();

                $log = new AdminLog();
                $log->table = 'shrinkages';
                $log->row_id = $shrinkage->id;
                $log->action = 'update_shrinkage';
                $log->admin_id = Session::get('admin_id');
                $log->save();

                DB::commit();
                return Redirect::route('adminShrinkages.details', ['id' => $id])->with('success', 'Merma actualizada con éxito.');
            }else{
               throw new Exception('La merma no existe', 500);
            }
        } catch(QueryException $e){
            DB::rollBack();
            if ($e->getCode() == '40001') {
                \ErrorLog::add($e, 513);
                return Redirect::route('adminShrinkages.details', ['id' => $id])->with('error', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');
            }
            return Redirect::back()->withErrors([
                'Exception',
                $e->getMessage()
            ]);
            return Redirect::route('adminShrinkages.Origin', $data)->with('title', 'Origen de Merma de Productos')->with('type', 'error')->with('message', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo..');
        } catch(Exception $e){
            DB::rollBack();
            \ErrorLog::add($e);
            return Redirect::route('adminShrinkages.details', ['id' => $id])->with('error', 'Ocurrió un error al actualizar la merma. '.$e->getMessage());
        }

    }

    /**
     * Elimina una merma
     */
    public function delete($id)
    {
        if ($shrinkage = Shrinkage::find($id)) {
            $shrinkage->status = 'Eliminada';
            $shrinkage->save();

            return Redirect::route('adminShrinkages.index')->with('success', 'Merma eliminada con éxito.');
        }
        return Redirect::route('adminShrinkages.index')->with('error', 'Ocurrió un error al eliminar la merma.');
    }

    /**
     * Función ajax para obtener el listado de tiendas de una ciudad
     */
    public function get_stores_ajax()
    {
        if (Input::has('city_id')) {
            $city_id = Input::get('city_id');
            $warehouses = Warehouse::select('id', 'warehouse')->where('city_id', Input::get('city_id'))->where('status', 1)->get()->toArray();
            $stores = Store::select('id', 'name')->where('city_id', $city_id)->where('is_hidden', 0)->where('status', 1)->get();
            $providers = Provider::select('id', 'name')->where('city_id', $city_id)->where('status', 1)->orderBy('name')->get();

            $response['success'] = true;
            $response['warehouses'] = $warehouses;
            $response['stores'] = $stores;
            $response['providers'] = $providers;

        } else $response['success'] = false;

        return Response::json($response, 200);
    }

    /**
     * Función ajax para obtener el listado de tiendas de una ciudad
     */
    public function get_typifications()
    {
        if (Input::has('name')) {
            $parent_id = \ShrinkageTypifications::where('name', Input::get('name'))->pluck('id');
            $typifications = \ShrinkageTypifications::where('parent_id', $parent_id)->get();

            $response['success'] = true;
            $response['typifications'] = $typifications;
        } else $response['success'] = false;

        return Response::json($response, 200);
    }

    /**
     *  Genera pdf de merma
     *
     * @param  $reference Número de refencia de una merma
     * @return $file PDF generado
     */
    public function shrinkage_pdf($id)
    {
        $shrinkage = Shrinkage::find($id);

        if ($shrinkage) {
            Shrinkage::generatePdf($shrinkage);
        } else {
            return Redirect::route('adminShrinkages.index')->with('type', 'failed')->with('message', 'No existe la merma.');
        }
    }
}
