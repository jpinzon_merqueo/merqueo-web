<?php

namespace admin\inventory\product;

use admin\AdminController;
use City;
use DB;
use Event;
use exceptions\MerqueoException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use Redirect;
use Response;
use Store;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;

class ProductInfoController extends AdminController
{
    /**
     * Muestra inicio del módulo.
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Información del producto';
        $cities = $this->get_cities();
        $warehouses = $this->get_warehouses();

        return View::make('admin.inventory.product.index', compact(
            'title',
            'cities',
            'warehouses'
        ));
    }

    /**
     * Obtiene las bodegas por ciudad.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesAjax()
    {
        $cityId = Input::get('cityId');
        $warehouses = Warehouse::where('city_id', $cityId)->get();

        if (count($warehouses) < 1) {
            return Response::json('No se encontraron bodegas de esta ciudad.', 422);
        }

        return Response::json($warehouses, 200);
    }

    /**
     * Obtiene la informacion del producto en las bodegas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductInfo()
    {
        $warehouseId = Input::get('warehouseId');
        $product = Input::get('product');

        $products = \StoreProduct::with([
            'storeProductWarehouses' => function ($query) use ($warehouseId) {
                $query->where('warehouse_id', $warehouseId);
            },
            'warehouseStorages' => function ($query) use ($warehouseId) {
                $query->where('warehouse_id', $warehouseId);
            },
            'product'
        ])->whereHas('product', function ($query) use ($product) {
            $query->where('reference', $product);
        })->whereHas('storeProductWarehouses', function ($query) use ($warehouseId) {
            $query->where('warehouse_id', $warehouseId);
        })->first();

        if (count($products) < 1) {
            return Response::json('No se encontro el producto.', 422);
        }

        return Response::json($products, 200);
    }

}
