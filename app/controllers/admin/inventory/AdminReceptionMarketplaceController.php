<?php

namespace admin\inventory;

use Request, Input, Session, View, Redirect, Config, DB, Menu, PHPExcel, PHPExcel_Style_Alignment, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007,
Order, OrderGroup, Store, City, Response, OrderProduct, admin\AdminController, Provider, ProviderOrder, ProviderOrderReception, ProviderOrderReceptionDetail, Carbon\Carbon, Event, ReceptionEventHandler;

class AdminReceptionMarketplaceController extends AdminController
{

	public function __construct()
	{
		parent::__construct();
		Event::subscribe(new ReceptionEventHandler);
	}

	public function index()
	{
		$admin_city_id = Session::get('admin_city_id');
		$providers = Provider::where('status', 1)->where('provider_type', 'Marketplace')->where('city_id', $admin_city_id)->orderBy('name')->get();
		$warehouses = $this->get_warehouses($admin_city_id);
		$data = [
			'title' => 'Recogida de Productos Markeplace',
			'providers' => $providers,
			'warehouses' => $warehouses,
			'cities' => $this->get_cities()
		];

		return View::make('admin.inventory.reception_marketplace.index', $data);
	}

	/**
	 * Funcion para filtrar y/o buscar ordenes de compra
	 * @return Json datos para la vista cargada por ajax
	 */
	public function get_provider_orders_ajax()
	{
		if ( Request::ajax() ) {
			$now = Carbon::now()->toDateString();
			$search_terms = Input::all();

			$providerOrders = ProviderOrder::with('admin','provider','city');

			$providerOrders = $providerOrders->whereHas('city', function ($q) use ($search_terms)
			{
			    $q->where('id', '=', $search_terms['city_id']);
			});

			if ( !empty($search_terms['provider_id']) ) {
			    $providerOrders = $providerOrders->whereHas('provider', function ($q) use ($search_terms)
			    {
			        $q->where('id', '=', $search_terms['provider_id']);
			    });
			}

			if ( !empty($search_terms['search_term']) ) {
			    $providerOrders = $providerOrders->where('id', $search_terms['search_term']);
			}

			if ( !empty($search_terms['warehouse_id']) ) {
			    $providerOrders = $providerOrders->where('warehouse_id', $search_terms['warehouse_id']);
			}

		   $providerOrders = $providerOrders->whereIn('status', ['Enviada', 'Pendiente']);

			$providerOrders = $providerOrders->whereHas('provider', function ($q) use ($search_terms)
			{
			    $q->where('provider_type', '=', 'Marketplace');
			});

			$providerOrders = $providerOrders->whereRaw('DATE(delivery_date) = "'.$now.'"');

			$providerOrders = $providerOrders->orderBy('provider_orders.id', 'desc')->paginate(20);

			$data = [
			  'providerOrders' => $providerOrders
			];
			$providerOrders_html = View::make('admin.inventory.reception_marketplace.ajax.orders-grid', $data)->render();

			$links_html = $providerOrders->links()->render();
			return Response::json(['providerOrders' => $providerOrders_html, 'providerOrders_count' => $providerOrders->count(), 'providerOrders_total' => $providerOrders->getTotal(), 'links' => $links_html]);
		}else{
			\App::abort(404);
		}
	}

	/**
	 * Obtiene los datos del recibo como productos
	 * @param  Int $provider_order_id
	 */
	public function get_receptions($provider_order_id)
	{
		$providerOrder = ProviderOrder::with('reception.providerOrderReceptionDetail.storeProduct.product', 'reception.providerOrderReceptionDetail.storeProduct.store', 'providerOrderDetails')->find($provider_order_id);
		// Validar si exsiste la orden de compra
		if ( $providerOrder ) {
			$receptions = $providerOrder->reception->filter(function ($q)
			{
				return $q->status == 'En proceso' || $q->status == 'Iniciado';
			});


			// Si la orden de compra tiene más de un recibo de bodega
			if ( $receptions->count() > 0 ) {
				if ( $receptions->count() == 1 ) {
					$reception = $receptions->first();
					$reception_details = $reception->providerOrderReceptionDetail->each(function (&$detail)
					{
						$store_product_id = $detail->store_product_id;
						$order_product = $detail->providerOrderReception->providerOrder->providerOrderDetails->filter(function ($order_detail) use ($store_product_id)
						{
							return $order_detail->store_product_id == $store_product_id;
						})->first();
						$detail->quantity_order = $order_product->quantity_order;
						if ( $detail->quantity_order == $detail->quantity_expected ) {
							$status = 'Recibido';
						}else if ( $detail->quantity_expected == 0 ) {
							$status = 'No recibido';
						}else if ( $detail->quantity_order > $detail->quantity_expected ) {
							$status = 'Parcialmente recibido';
						}else if ( $detail->quantity_order < $detail->quantity_expected ){
							$status = 'Recibido';
						}
						$detail->received_status = $status;
					});
					$data = [
						'title' => 'Recibo #'.$reception->id,
						'reception' => $reception,
						'reception_details' => $reception_details,
					];
					return View::make('admin.inventory.reception_marketplace.reception', $data);
				}else if ( $receptions->count() > 1 ) {
					return Redirect::back()->with('error', 'La orden de compra tiene más de un recibo de bodega en proceso, por favor contacte con el administrador.');
				}
			}else{
				if ( $receptions->count() == 0 ) {
					if ( empty(Input::all()) ) {
						return Redirect::back()->with('error', 'Debe ingresar primero los datos requeridos para el recibo de bodega.')->with('show_modal', true)->with('provider_order_id', $providerOrder->id);
					}

					$invoice_number = Input::get('invoice_number');
					$transporter = Input::get('transporter');
					$plate = Input::get('plate');
					$driver_name = Input::get('driver_name');
					$response = $providerOrder->createReception($invoice_number, $transporter, $plate, $driver_name);
					if ( $response['status'] ) {
						return Redirect::route('adminReceptionMarketplace.getReceptions', ['provider_order_id' => $providerOrder->id]);
					}else{
						return Redirect::back()->with('error', $response['message']);
					}
				}
				return Redirect::back()->with('error', 'La orden de compra tiene varios recibos de compra sin cerrar. Debe contactar con el administrador.');
			}
		}
		return Redirect::back()->with('error', 'La orden de compra no existe.');
	}

	/**
	 * Actualiza las cantidades recibidas de un producto en el recibo de bodega
	 * @param  Int $reception_id id del recibo
	 */
	public function update_product_quantity_ajax($reception_id)
	{
		$reception = ProviderOrderReception::find($reception_id);
		if ( $reception ) {
			$detail_id = Input::get('detail_id');
			$quantity = Input::get('quantity');

			$detail = ProviderOrderReceptionDetail::find($detail_id);
			$providerOrder = ProviderOrder::with('providerOrderDetails.storeProduct.product')->find($reception->provider_order_id);
			$order_detail = $providerOrder->providerOrderDetails->filter(function ($q) use ($detail)
			{
				return $q->store_product_id == $detail->store_product_id;
			})->first();

			if ( $order_detail->quantity_order == $quantity ) {
				$status = 'Recibido';
				if ( $reception->status == 'Iniciado' ) {
					$reception->status = 'En proceso';
					$data['reception_status'] = 'En proceso';
					$reception->save();
				}
			}else if ( $quantity == 0 ) {
				$status = 'No recibido';
			}else if ( $order_detail->quantity_order > $quantity ) {
				$status = 'Parcialmente recibido';
				if ( $reception->status == 'Iniciado' ) {
					$reception->status = 'En proceso';
					$data['reception_status'] = 'En proceso';
					$reception->save();
				}
			}else if ( $order_detail->quantity_order < $quantity ){
				$status = 'Recibido';
				if ( $reception->status == 'Iniciado' ) {
					$reception->status = 'En proceso';
					$data['reception_status'] = 'En proceso';
					$reception->save();
				}
			}
			$detail->quantity_expected = $quantity;
			$detail->save();
			Event::fire('reception.updatedProductQuantityExpected', [[
			    'reception_id' => $reception->id,
			    'store_product_id' => $detail->store_product_id,
			    'quantity_expected' => $quantity,
			    'admin_id' => Session::get('admin_id')
			]]);

			$data['status'] = $status;
			$data['quantity'] = $detail->quantity_expected;
			$data['quantity_order'] = $order_detail->quantity_order;

			return Response::json($data);
		}
		return \App::abort(404);
	}

	public function update_reception_status($reception_id)
	{
		$reception = ProviderOrderReception::find($reception_id);
		if ( $reception ) {
			$status = Input::get('status');
			$response = $reception->updateStatus($status);
			if ( $response ) {
				$response = $reception->updateStatus('En proceso');
				if ( $response )
					return Redirect::route('adminReceptionMarketplace.index')->with('success', 'Se ha actualizado el recibo de bodega a: '.$status);
			}
			return Redirect::route('adminReceptionMarketplace.index')->with('error', 'No se ha actualizado el recibo de bodega a: '.$status);
		}
		return \App::abort(404);
	}
}