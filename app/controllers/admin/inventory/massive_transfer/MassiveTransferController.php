<?php

namespace admin\inventory\massive_transfer;

use admin\AdminController;
use City;
use DB;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Redirect;
use Response;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;

class MassiveTransferController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        Event::subscribe(new WarehouseStorageEventHandler());
    }

    /**
     * Muestra inicio del módulo.
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Traslado masivo de posiciones';
        $cities = City::getMainCities();
        $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->get();
        return View::make('admin.inventory.massive_transfer.index', compact(
            'title',
            'cities',
            'warehouses'
        ));
    }

    /**
     * Obtiene las bodegas por ciudad.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesAjax()
    {
        $cityId = Input::get('city_id');
        $warehouses = Warehouse::where('city_id', $cityId)->get();

        if (count($warehouses) < 1) {
            return Response::json('No se encontraron bodegas de esta ciudad.', 400);
        }
        return Response::json($warehouses, 200);
    }

    /**
     * Obtiene los productos por posición en las bodegas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsAjax()
    {
        $warehouseId = Input::get('warehouse_id');
        $storage = Input::get('storage');
        $position = Input::get('position');
        $positionHeight = Input::get('position_height');
        $warehouse = Warehouse::findOrFail($warehouseId);
        $products = WarehouseStorage::with('storeProduct.product')
            ->where('warehouse_id', $warehouse->id)
            ->where('storage_type', $storage)
            ->where('position', $position)
            ->where('position_height', $positionHeight)
            ->get();
        return Response::json($products, 200);
    }

    /**
     * Realiza el traslado entre posiciones.
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function transferProducts()
    {
        $warehouseId = Input::get('warehouse_id');
        $storageType = Input::get('storage_type');
        $positionFrom = Input::get('position_from');
        $positionHeightFrom = Input::get('position_height_from');
        $positionTo = Input::get('position_to');
        $positionHeightTo = Input::get('position_height_to');
        $productsChecked = Input::get('productsChecked');
        try {
            $warehouse = Warehouse::findOrFail($warehouseId);
        } catch (ModelNotFoundException $e) {
            return Response::json('La bodega no existe.', 400);
        }
        try {
            if (!count($productsChecked)) {
                return Response::json('Debe seleccionar al menos un producto.', 400);
            }
            $isOriginBlocked = Warehouse::validateBlockedPositions(
                $warehouse->id,
                'Almacenamiento',
                $storageType,
                $positionHeightFrom,
                $positionFrom
            );
            if ($isOriginBlocked) {
                return Response::json('La posición de origen está bloqueada por conteo.', 400);
            }
            $isDestinyBlocked = Warehouse::validateBlockedPositions(
                $warehouse->id,
                'Almacenamiento',
                $storageType,
                $positionHeightTo,
                $positionTo
            );
            if ($isDestinyBlocked) {
                return Response::json('La posición de destino está bloqueada por conteo.', 400);
            }
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 400);
        }
        try {
            DB::beginTransaction();
            foreach ($productsChecked as $productChecked) {
                $warehouseStorage = WarehouseStorage::lockForUpdate()->find($productChecked['id']);
                $quantityPulled = WarehouseStorage::pullFrom($warehouseStorage, 'Traslado de posiciones');
                WarehouseStorage::pushTo(
                    $warehouseStorage,
                    'Traslado de posiciones',
                    $positionTo,
                    $positionHeightTo,
                    $quantityPulled
                );
            }
            DB::commit();
            return Redirect::back()->with('success', 'Se ha generado el traslado correctamente.');
            return Response::json('Se ha generado el traslado correctamente.', 200);
        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                \ErrorLog::add($e, 513);
                return Response::json('Otro proceso se está ejecutando sobre estos productos, 
                espera un momento para volver a intentarlo.', 400);
            }
            \ErrorLog::add($e);
            return Response::json($e->getMessage(), 400);
        } catch (\Exception $e) {
            DB::rollBack();
            \ErrorLog::add($e);
            return Response::json($e->getMessage(), 400);
        }
    }
}
