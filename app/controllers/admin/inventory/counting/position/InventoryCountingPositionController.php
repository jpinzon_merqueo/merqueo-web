<?php

namespace admin\inventory\counting\position;

use Carbon\Carbon;
use Config;
use DB;
use Event;
use Exception;
use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\App;
use Excel;
use Illuminate\Support\MessageBag;
use InvestigationStockEventHandler;
use StoreProductWarehouseEventHandler;
use View;
use Illuminate\Support\Facades\Input;
use Redirect;
use Response;
use Session;
use Validator;
use InventoryCountingPosition;
use InventoryCountingPositionDetail;
use Warehouse;
use StoreProductWarehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;
use admin\AdminController;
use Zend\Json\Json;

class InventoryCountingPositionController extends AdminController
{
    private $adminEmails = [
        [
            'email' => 'ileal@merqueo.com',
            'name' => 'Ingrid Leal'
        ],
        [
            'email' => 'amontoya@merqueo.com',
            'name' => 'Adriana Herrera'
        ],
        [
            'email' => 'gmartinez@merqueo.com',
            'name' => 'Gustavo Martínez'
        ],
        [
            'email' => 'jarevalo@merqueo.com',
            'name' => 'Jaime Arevalo Vargas'
        ],
        [
            'email' => 'fcamacho@merqueo.com',
            'name' => 'Luis Fernando Camacho'
        ],
        [
            'email' => 'agutierrez@merqueo.com',
            'name' => 'Alejandro Gutierrez'
        ],
        [
            'email' => 'yguerrero@merqueo.com',
            'name' => 'Yohanrry Guerrero'
        ],
        [
            'email' => 'jherrera@merqueo.com',
            'name' => 'John Jairo Herrera Weeber'
        ],
        [
            'email' => 'aosorio@merqueo.com',
            'name' => 'Alex Osorio'
        ],
        [
            'email' => 'emaldonado@merqueo.com',
            'name' => 'Mauricio Maldonado'
        ],
        [
            'email' => 'jmorales@merqueo.com',
            'name' => 'Felipe Morales'
        ],
        [
            'email' => 'jvasco@merqueo.com',
            'name' => 'Eduardo Vasco'
        ],
        [
            'email' => 'hbeltran@merqueo.com',
            'name' => 'Harvey Beltran'
        ],
        [
            'email' => 'alardila@merqueo.com',
            'name' => 'Alexander Ardila'
        ],
        [
            'email' => 'almartinez@merqueo.com',
            'name' => 'Alexander Martinez'
        ],
        [
            'email' => 'rvelez@merqueo.com',
            'name' => 'Rafael Velez'
        ],
        [
            'email' => 'jfarevalo@merqueo.com',
            'name' => 'Javier Arevalo'
        ],
        [
            'email' => 'jgarcia@merqueo.com',
            'name' => 'Juan Gabriel Garcia'
        ],
        [
            'email' => 'lmarin@merqueo.com',
            'name' => 'Lady Katerine Marin'
        ],
        [
            'email' => 'flucas@merqueo.com',
            'name' => 'flucas@merqueo.com'
        ],
        [
            'email' => 'aherrera@merqueo.com',
            'name' => 'aherrera@merqueo.com'
        ],
        [
            'email' => 'kaparicio@merqueo.com',
            'name' => 'kaparicio@merqueo.com'
        ],
        [
            'email' => 'ccalderon@merqueo.com',
            'name' => 'ccalderon@merqueo.com'
        ],
        [
            'email' => 'jsalas@merqueo.com',
            'name' => 'jsalas@merqueo.com'
        ],
        [
            'email' => 'nhernandez@merqueo.com',
            'name' => 'nhernandez@merqueo.com'
        ],
        [
            'email' => 'dianar@merqueo.com',
            'name' => 'dianar@merqueo.com'
        ],
        [
            'email' => 'javierl@merqueo.com',
            'name' => 'javierl@merqueo.com'
        ],
        [
            'email' => 'kevine@merqueo.com',
            'name' => 'kevine@merqueo.com'
        ],
        [
            'email' => 'mvillada@merqueo.com',
            'name' => 'mvillada@merqueo.com'
        ],
        [
            'email' => 'egarcia@merqueo.com',
            'name' => 'egarcia@merqueo.com'
        ],
        [
            'email' => 'yagudelo@merqueo.com',
            'name' => 'yagudelo@merqueo.com'
        ],
        [
            'email' => 'ogomez@merqueo.com',
            'name' => 'ogomez@merqueo.com'
        ],
        [
            'email' => 'ehernandez@merqueo.com',
            'name' => 'ehernandez@merqueo.com'
        ],
        [
            'email' => 'dduque@merqueo.com',
            'name' => 'dduque@merqueo.com'
        ],
        [
            'email' => 'rramirez@merqueo.com',
            'name' => 'rramirez@merqueo.com'
        ],
        [
            'email' => 'enino@merqueo.com',
            'name' => 'enino@merqueo.com'
        ],
        [
            'email' => 'jacuna@merqueo.com',
            'name' => 'jacuna@merqueo.com'
        ],
        [
            'email' => 'kcastro@merqueo.com',
            'name' => 'kcastro@merqueo.com'
        ],
        [
            'email' => 'jlopez@merqueo.com',
            'name' => 'jlopez@merqueo.com'
        ],
        [
            'email' => 'amartinezb@merqueo.com',
            'name' => 'amartinezb@merqueo.com'
        ]
    ];

    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new WarehouseStorageEventHandler());
        Event::subscribe(new StoreProductWarehouseEventHandler());
        Event::subscribe(new InvestigationStockEventHandler());
    }

    /**
     * Obtiene todos los conteos por posicion creados
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Conteos por Posición';
        $cities = $this->get_cities();
        $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))
            ->active()
            ->get();
        $statuses = [
            'Pendiente' => 'Pendiente',
            'Cerrado' => 'Cerrado',
        ];

        return View::make(
            'admin.inventory.counting.position.index',
            compact('title', 'cities', 'warehouses', 'statuses')
        );
    }

    /**
     * Relaciona los productos al conteo creado
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function view($id)
    {
        try {
            $inventoryCountingPosition = InventoryCountingPosition::with('warehouse')->with([
                'inventoryCountingPositionDetails' => function ($query) {
                    $query->orderByRaw('position_height ASC')->orderByRaw('position ASC')
                        ->with('storeProduct.product');
                }
            ])->findOrFail($id);
            $title = 'Conteo por posición #' . $id;

            $storagePositions = $inventoryCountingPosition->warehouse
                ->getStoragePosition($inventoryCountingPosition->action, $inventoryCountingPosition->storage_type);

            $storagePositions = array_combine(
                $storagePositions['height_positions'],
                $storagePositions['height_positions']
            );

            $storagePositionsSelected = array_combine(
                explode(',', $inventoryCountingPosition->height_positions),
                explode(',', $inventoryCountingPosition->height_positions)
            );

            $data = [
                'title' => $title,
                'inventoryCountingPosition' => $inventoryCountingPosition,
                'storagePositions' => $storagePositions,
                'storagePositionsSelected' => $storagePositionsSelected
            ];
            return View::make('admin.inventory.counting.position.view', $data);
        } catch (ModelNotFoundException $e) {
            App::abort(404);
        }
    }

    /**
     * Obtiene la vista con el formulario de creacion del conteo por posicion
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $title = 'Crear Conteo por Posición';
        $cities = $this->get_cities();
        $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))
            ->active()
            ->get();

        return View::make(
            'admin.inventory.counting.position.create',
            compact('title', 'cities', 'warehouses')
        );
    }

    /**
     * Guarda el conteo por posición
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save()
    {
        $input = Input::all();

        $validator = Validator::make($input, [
            'city_id' => 'required|exists:cities,id',
            'warehouse_id' => 'required|exists:warehouses,id',
            'action' => 'required|string|max:1000|in:Almacenamiento,Alistamiento',
            'storage' => 'required|in:Seco,Frío',
            'height_positions' => 'required|array',
            'positions' => 'required|string|regex:/^([0-9])+(\-)([0-9])+/'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $warehouse = Warehouse::find($input['warehouse_id']);
        $positions = explode("-", $input['positions']);

        try {
            $warehouse->validateHeightPositions($input['height_positions'], $input['action'], $input['storage']);
        } catch (MerqueoException $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }

        try {
            DB::beginTransaction();

            $inventoryCountingPosition = new InventoryCountingPosition;
            $inventoryCountingPosition->admin_id = Session::get('admin_id');
            $inventoryCountingPosition->warehouse_id = $warehouse->id;
            $inventoryCountingPosition->action = $input['action'];
            $inventoryCountingPosition->storage_type = $input['storage'];
            $inventoryCountingPosition->height_positions = implode(',', $input['height_positions']);
            $inventoryCountingPosition->positions = $input['positions'];
            $inventoryCountingPosition->count_date = Carbon::now();
            $inventoryCountingPosition->save();

            $storage = $input['storage'] == 'Seco' ? [$input['storage']] : ['Refrigerado','Congelado'];

            if ($inventoryCountingPosition->action == 'Almacenamiento') {
                $warehouseStorages = WarehouseStorage::where('warehouse_id', $inventoryCountingPosition->warehouse_id)
                    ->where('type', 'Producto')
                    ->where('storage_type', $inventoryCountingPosition->storage_type)
                    ->where('quantity', '>', 0)
                    ->whereIn('position_height', $input['height_positions'])
                    ->whereBetween('position', $positions)
                    ->get();

                foreach ($warehouseStorages as $warehouseStorage) {
                    $inventoryCountingPositionDetail = new InventoryCountingPositionDetail;
                    $inventoryCountingPositionDetail->store_product_id = $warehouseStorage->store_product_id;
                    $inventoryCountingPositionDetail->quantity_original = $warehouseStorage->quantity;
                    $inventoryCountingPositionDetail->position = $warehouseStorage->position;
                    $inventoryCountingPositionDetail->position_height = $warehouseStorage->position_height;
                    $inventoryCountingPositionDetail->expiration_date = (is_null($warehouseStorage->expiration_date)) ?
                        null : new Carbon($warehouseStorage->expiration_date);
                    $inventoryCountingPosition->inventoryCountingPositionDetails()
                        ->save($inventoryCountingPositionDetail);
                }
            } else {
                $storeProductWarehouses = StoreProductWarehouse::join(
                    'store_products',
                    'store_product_id',
                    '=',
                    'store_products.id'
                )
                    ->whereIn('store_products.storage', $storage)
                    ->where('warehouse_id', $inventoryCountingPosition->warehouse_id)
                    ->where('picking_stock', '<>', 0)
                    ->whereIn('storage_height_position', $input['height_positions'])
                    ->whereBetween('storage_position', $positions)
                    ->select('store_product_warehouses.*')
                    ->get();

                foreach ($storeProductWarehouses as $storeProductWarehouse) {
                    $inventoryCountingPositionDetail = new InventoryCountingPositionDetail;
                    $inventoryCountingPositionDetail->inventory_counting_position_id = $inventoryCountingPosition->id;
                    $inventoryCountingPositionDetail->store_product_id = $storeProductWarehouse->store_product_id;
                    $inventoryCountingPositionDetail->picking_original = $storeProductWarehouse->picking_stock;
                    $inventoryCountingPositionDetail->position = $storeProductWarehouse->storage_position;
                    $inventoryCountingPositionDetail->position_height = $storeProductWarehouse->storage_height_position;
                    $inventoryCountingPosition->inventoryCountingPositionDetails()
                        ->save($inventoryCountingPositionDetail);
                }
            }
            if ($inventoryCountingPosition->inventoryCountingPositionDetails()->count() < 1) {
                DB::rollBack();
                return Redirect::back()->with('error', 'No hay productos en las posiciones ingresadas.');
            }
            DB::commit();
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }

        return Redirect::route('adminPositionCounting.view', ['id' => $inventoryCountingPosition->id])
            ->with('success', 'Se creó el conteo exitosamente');
    }

    /**
     * Actualiza el conteo por posición
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function updateDetail()
    {
        try {
            DB::beginTransaction();
            $detailId = Input::get('id');
            $inventoryCountingId = Input::get('counting_id');
            $quantity = Input::get('contado');

            InventoryCountingPosition::where('status', 'Pendiente')
                ->findOrFail($inventoryCountingId);

            $validator = Validator::make(Input::all(), [
                'contado' => 'required|integer|regex:/^[0-9]*+$/'
            ]);
            if ($validator->fails()) {
                return Response::json($validator->getMessageBag()->all(), 400);
            }

            $inventoryCountingPositionDetail = InventoryCountingPositionDetail::with('inventoryCountingPosition')
                ->where('inventory_counting_position_id', $inventoryCountingId)
                ->findOrFail($detailId);

            $inventoryCountingPositionDetail->count_admin_id = Session::get('admin_id');
            $inventoryCountingPositionDetail->start_date = Carbon::now();
            if ($inventoryCountingPositionDetail->inventoryCountingPosition->action == 'Alistamiento') {
                $inventoryCountingPositionDetail->picking_counted = $quantity;
                $inventoryCountingPositionDetail->picking_updated = 1;
            } else {
                $inventoryCountingPositionDetail->quantity_counted = $quantity;
                $inventoryCountingPositionDetail->quantity_updated = 1;
            }
            if ($inventoryCountingPositionDetail->save()) {
                $successBag = new MessageBag;
                $successBag->add('success', 'El registro se guardó con éxito.');
                DB::commit();
                return Response::json($successBag->all(), 200);
            } else {
                $errorBag = new MessageBag;
                $errorBag->add('error', 'El registro no se guardó con éxito.');
                DB::rollBack();
                return Response::json($errorBag->all(), 500);
            }
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            $model = $e->getModel();
            if ($model === 'InventoryCountingPosition') {
                $errorBag = new MessageBag;
                $errorBag->add('exception', 'El conteo no existe ó ya no está activo para ser modificado.');
                return Response::json($errorBag->all(), 500);
            }
            if ($model === 'InventoryCountingPositionDetail') {
                $errorBag = new MessageBag;
                $errorBag->add('exception', 'El producto contado no se encuentra en este conteo.');
                return Response::json($errorBag->all(), 500);
            }
        }
    }

    /**
     * Cierra el conteo y actualiza stock
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function closeCount($id)
    {
        try {
            $inventoryCountingPosition = InventoryCountingPosition::with(
                'inventoryCountingPositionDetails.storeProduct.product',
                'warehouse'
            )
                ->findOrFail($id);

            if ($inventoryCountingPosition->action == 'Alistamiento') {
                DB::beginTransaction();

                if (!$inventoryCountingPosition->isCountingDifferent('Alistamiento')) {
                    $inventoryCountingPosition->status = 'Cerrado';
                    $inventoryCountingPosition->admin_close = Session::get('admin_id');
                    $inventoryCountingPosition->end_count_date = Carbon::now();
                    $inventoryCountingPosition->save();
                    DB::commit();
                    return Redirect::route('adminPositionCounting.index')
                        ->with('success', 'El conteo se cerró exitosamente.');
                }

                $this->closePickingCounting($inventoryCountingPosition);
                DB::commit();
                return Redirect::route('adminPositionCounting.index')
                    ->with('success', 'El conteo se cerró exitosamente.');
            }

            if ($inventoryCountingPosition->action == 'Almacenamiento') {
                DB::beginTransaction();

                if (!$inventoryCountingPosition->isCountingDifferent('Almacenamiento')) {
                    $inventoryCountingPosition->status = 'Cerrado';
                    $inventoryCountingPosition->admin_close = Session::get('admin_id');
                    $inventoryCountingPosition->end_count_date = Carbon::now();
                    $inventoryCountingPosition->save();
                    DB::commit();
                    return Redirect::route('adminPositionCounting.index')
                        ->with('success', 'El conteo se cerró exitosamente.');
                }

                $this->closeStorageCounting($inventoryCountingPosition);
                DB::commit();
                return Redirect::route('adminPositionCounting.index')
                    ->with('success', 'El conteo se cerró exitosamente.');
            }
        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                \ErrorLog::add($e, 513);
                $errorBag = new MessageBag;
                $errorBag->add(
                    'exception',
                    'Otro proceso se está ejecutando sobre estos productos, 
                espera un momento para volver a intentarlo.'
                );
                $errorBag->add('exception', "{$e->getFile()}: {$e->getLine()}");
                return Redirect::back()->withErrors($errorBag->all());
            }
            \ErrorLog::add($e);
            $errorBag = new MessageBag;
            $errorBag->add(
                'exception',
                $e->getMessage()
            );
            $errorBag->add('exception', "{$e->getFile()}: {$e->getLine()}");
            return Redirect::back()->withErrors($errorBag->all());
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            $errorBag = new MessageBag;
            $errorBag->add('exception', $e->getMessage());
            $errorBag->add('exception', "{$e->getFile()}: {$e->getLine()}");
            return Redirect::back()->withErrors($errorBag->all());
        } catch (MerqueoException $e) {
            DB::rollBack();
            $errorBag = new MessageBag;
            $errorBag->add('exception', $e->getMessage());
            $errorBag->add('exception', "{$e->getFile()}: {$e->getLine()}");
            return Redirect::back()->withErrors($errorBag->all());
        } catch (Exception $e) {
            DB::rollBack();
            $errorBag = new MessageBag;
            $errorBag->add('exception', $e->getMessage());
            $errorBag->add('exception', "{$e->getFile()}: {$e->getLine()}");
            return Redirect::back()->withErrors($errorBag->all());
        }
        $errorBag = new MessageBag;
        $errorBag->add('exception', "No se realizó ninguna acción.");
        return Redirect::back()->withErrors($errorBag->all());
    }

    /**
     * @param $inventoryCountingPosition
     * @throws Exception
     * @throws MerqueoException
     * @throws ModelNotFoundException
     */
    private function closePickingCounting($inventoryCountingPosition)
    {
        $adminRole = Session::get('admin_role_id');

        if (in_array($adminRole, InventoryCountingPosition::CLOSE_COUNTING_ROLE_IDS)) {
            try {
                $this->closePickingCountingEditing($inventoryCountingPosition);
            } catch (QueryException $e) {
                throw $e;
            } catch (MerqueoException $e) {
                throw $e;
            } catch (ModelNotFoundException $e) {
                throw $e;
            } catch (Exception $e) {
                throw $e;
            }
        } else {
            try {
                $this->closePickingCountingWithoutEditing($inventoryCountingPosition);
            } catch (ModelNotFoundException $e) {
                throw $e;
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    /**
     * Cierra el conteo editando cantidades
     * @param $inventoryCountingPosition
     * @return mixed
     * @throws MerqueoException
     * @throws ModelNotFoundException
     */
    private function closePickingCountingEditing($inventoryCountingPosition)
    {
        foreach ($inventoryCountingPosition->inventoryCountingPositionDetails as $inventoryCountingPositionDetail) {
            if (!is_null($inventoryCountingPositionDetail->picking_counted)) {
                try {
                    $storeProductWarehouse = StoreProductWarehouse::where(
                        'warehouse_id',
                        $inventoryCountingPosition->warehouse_id
                    )
                        ->where('store_product_id', $inventoryCountingPositionDetail->store_product_id)
                        ->where('storage_position', $inventoryCountingPositionDetail->position)
                        ->where('storage_height_position', $inventoryCountingPositionDetail->position_height)
                        ->where('picking_stock', $inventoryCountingPositionDetail->picking_original)
                        ->lockForUpdate()
                        ->firstOrFail();
                } catch (QueryException $e) {
                    throw $e;
                } catch (ModelNotFoundException $e) {
                    throw $e;
                }

                if ($storeProductWarehouse->picking_stock != $inventoryCountingPositionDetail->picking_counted) {
                    try {
                        Event::fire('investigationStock.onUpdatePickingQuantity', [
                            $inventoryCountingPositionDetail,
                            $storeProductWarehouse,
                            $inventoryCountingPosition,
                            'Conteo por Posición'
                        ]);
                        StoreProductWarehouse::pullFrom($storeProductWarehouse, 'Conteo por Posiciones');
                        if ($inventoryCountingPositionDetail->picking_counted !== 0) {
                            StoreProductWarehouse::pushTo(
                                $storeProductWarehouse,
                                'Conteo por Posiciones',
                                $inventoryCountingPositionDetail->picking_counted
                            );
                        }
                    } catch (MerqueoException $e) {
                        throw $e;
                    }

                }
            }
        }
        $inventoryCountingPosition->status = 'Cerrado';
        $inventoryCountingPosition->admin_close = Session::get('admin_id');
        $inventoryCountingPosition->end_count_date = Carbon::now();
        return $inventoryCountingPosition->save();
    }

    /**
     * Cierra el conteo sin editar y enviando el informe a quien lo requiera
     * @param $inventoryCountingPosition
     * @throws Exception
     * @throws ModelNotFoundException
     */
    private function closePickingCountingWithoutEditing($inventoryCountingPosition)
    {
        $filename = "Reporte de conteo por posiciones ({$inventoryCountingPosition->warehouse->warehouse}) 
        #{$inventoryCountingPosition->id}";

        foreach ($this->adminEmails as $index => $adminEmail) {
            $admin = \Admin::where('username', $adminEmail['email'])->first();
            if (!empty($admin) && $admin->warehouse_id !== $inventoryCountingPosition->warehouse_id) {
                unset($this->adminEmails[$index]);
            }
        }

        $this->adminEmails = array_values($this->adminEmails);

        $path = public_path(Config::get('app.download_directory'));
        $mail['template_name'] = 'emails.counting_report';
        $mail['subject'] = $filename;
        $mail['to'] = $this->adminEmails;
        $mail['vars'] = [
            'id' => $inventoryCountingPosition->id,
            'warehouse' => $inventoryCountingPosition->warehouse->warehouse,
            'countingType' => $inventoryCountingPosition->action,
            'storageType' => $inventoryCountingPosition->storage_type,
        ];

        $file = Excel::create($filename, function ($excel) use ($inventoryCountingPosition) {
            // Set the title
            $excel->setTitle(`Reporte del conteo {$inventoryCountingPosition->id}`);

            // Chain the setters
            $excel->setCreator('Merqueo')
                ->setCompany('Merqueo');

            // Call them separately
            $excel->setDescription('Merqueo');

            $excel->sheet('Recibos', function ($sheet) use ($inventoryCountingPosition) {
                $titles = [
                    'Referencia',
                    'Nombre',
                    'Posición',
                    'Posición altura',
                    'Costo',
                    'Cantidad sistema',
                    'Cantidad contada',
                    'Costo de diferencia'
                ];
                $sheet->appendRow($titles);
                $inventoryCountingPositionDetails = $inventoryCountingPosition->inventoryCountingPositionDetails();
                $inventoryCountingPositionDetails->chunk(
                    100,
                    function ($inventoryCountingPositionDetails) use ($sheet, $inventoryCountingPosition) {
                        foreach ($inventoryCountingPositionDetails as $inventoryCountingPositionDetail) {
                            if (!is_null($inventoryCountingPositionDetail->picking_counted)) {
                                try {
                                    $storeProductWarehouse = StoreProductWarehouse::where(
                                        'warehouse_id',
                                        $inventoryCountingPosition->warehouse_id
                                    )
                                        ->where('store_product_id', $inventoryCountingPositionDetail->store_product_id)
                                        ->where('storage_position', $inventoryCountingPositionDetail->position)
                                        ->where(
                                            'storage_height_position',
                                            $inventoryCountingPositionDetail->position_height
                                        )
                                        ->where('picking_stock', $inventoryCountingPositionDetail->picking_original)
                                        ->firstOrFail();
                                } catch (ModelNotFoundException $e) {
                                    throw $e;
                                }

                                if ($storeProductWarehouse->picking_stock
                                    != $inventoryCountingPositionDetail->picking_counted) {
                                    $costDifference = $storeProductWarehouse->picking_stock -
                                        $inventoryCountingPositionDetail->picking_counted;
                                    $productsToEmail = [
                                        'referencia' => $storeProductWarehouse->storeProduct->product->reference,
                                        'nombre' => $storeProductWarehouse->storeProduct->product->name,
                                        'posicion' => $inventoryCountingPositionDetail->position,
                                        'posicion_altura' => $inventoryCountingPositionDetail->position_height,
                                        'costo' => $storeProductWarehouse->storeProduct->cost,
                                        'cantidad_sistema' => $storeProductWarehouse->picking_stock,
                                        'cantidad_conteo' => $inventoryCountingPositionDetail->picking_counted,
                                        'costo_diferencia' => $costDifference * $storeProductWarehouse
                                                ->storeProduct->cost
                                    ];
                                    $sheet->appendRow($productsToEmail);
                                }
                            }
                        }
                    }
                );
            });
        })->store('xls', $path, true);

        try {
            $data = file_get_contents($file['full']);
            $base64 = base64_encode($data);
            $attachments[0]['type'] = 'application/vnd.ms-excel';
            $attachments[0]['name'] = $file['file'];
            $attachments[0]['content'] = $base64;
            send_mail($mail, $attachments, true);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Cierra los conteos de almacenamiento.
     * @param $inventoryCountingPosition
     * @throws MerqueoException
     * @throws Exception
     * @throws ModelNotFoundException
     */
    private function closeStorageCounting($inventoryCountingPosition)
    {
        $adminRole = Session::get('admin_role_id');

        try {
            if (in_array($adminRole, InventoryCountingPosition::CLOSE_COUNTING_ROLE_IDS)) {
                $this->closeStorageCountingEditing($inventoryCountingPosition);
            } else {
                $this->closeStorageCountingWithoutEditing($inventoryCountingPosition);
            }
        } catch (QueryException $e) {
            throw $e;
        } catch (MerqueoException $e) {
            throw $e;
        } catch (ModelNotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Cierra el conteo editando las cantidades
     * @param $inventoryCountingPosition
     * @return mixed
     * @throws MerqueoException
     * @throws ModelNotFoundException
     */
    private function closeStorageCountingEditing($inventoryCountingPosition)
    {
        foreach ($inventoryCountingPosition->inventoryCountingPositionDetails as $inventoryCountingPositionDetail) {
            if (!is_null($inventoryCountingPositionDetail->quantity_counted)) {
                try {
                    $warehouseStorage = WarehouseStorage::where(
                        'warehouse_id',
                        $inventoryCountingPosition->warehouse_id
                    )
                        ->where('store_product_id', $inventoryCountingPositionDetail->store_product_id)
                        ->where('quantity', $inventoryCountingPositionDetail->quantity_original)
                        ->where('expiration_date', $inventoryCountingPositionDetail->expiration_date)
                        ->where('position', $inventoryCountingPositionDetail->position)
                        ->where('position_height', $inventoryCountingPositionDetail->position_height)
                        ->lockForUpdate()
                        ->firstOrFail();
                } catch (QueryException $e) {
                    throw $e;
                } catch (ModelNotFoundException $e) {
                    throw $e;
                }


                if ($warehouseStorage->quantity != $inventoryCountingPositionDetail->quantity_counted) {
                    try {
                        Event::fire('investigationStock.onUpdateStorageQuantity', [
                            $inventoryCountingPositionDetail,
                            $warehouseStorage,
                            $inventoryCountingPosition,
                            'Conteo por Posición'
                        ]);
                        WarehouseStorage::pullFrom($warehouseStorage, 'Conteo por Posiciones');
                        if ($inventoryCountingPositionDetail->quantity_counted !== 0) {
                            WarehouseStorage::pushTo(
                                $warehouseStorage,
                                'Conteo por Posiciones',
                                $inventoryCountingPositionDetail->position,
                                $inventoryCountingPositionDetail->position_height,
                                $inventoryCountingPositionDetail->quantity_counted
                            );
                        }
                    } catch (MerqueoException $e) {
                        throw $e;
                    }

                }
            }
        }

        $inventoryCountingPosition->status = 'Cerrado';
        $inventoryCountingPosition->admin_close = Session::get('admin_id');
        $inventoryCountingPosition->end_count_date = Carbon::now();
        return $inventoryCountingPosition->save();
    }

    /**
     * Cierra el conteo sin editar y enviando el informe a quien lo requiera.
     * @param $inventoryCountingPosition
     * @throws ModelNotFoundException
     * @throws Exception
     */
    private function closeStorageCountingWithoutEditing($inventoryCountingPosition)
    {
        $filename = "Reporte de conteo por posiciones ({$inventoryCountingPosition->warehouse->warehouse}) 
        #{$inventoryCountingPosition->id}";

        foreach ($this->adminEmails as $index => $adminEmail) {
            $admin = \Admin::where('username', $adminEmail['email'])->first();
            if (!empty($admin)
                && $admin->warehouse_id !== $inventoryCountingPosition->warehouse_id) {
                unset($this->adminEmails[$index]);
            }
        }

        $this->adminEmails = array_values($this->adminEmails);

        $path = public_path(Config::get('app.download_directory'));
        $mail['template_name'] = 'emails.counting_report';
        $mail['subject'] = $filename;
        $mail['to'] = $this->adminEmails;
        $mail['vars'] = [
            'id' => $inventoryCountingPosition->id,
            'warehouse' => $inventoryCountingPosition->warehouse->warehouse,
            'countingType' => $inventoryCountingPosition->action,
            'storageType' => $inventoryCountingPosition->storage_type,
        ];



        $file = Excel::create($filename, function ($excel) use ($inventoryCountingPosition) {
            // Set the title
            $excel->setTitle(`Reporte del conteo {$inventoryCountingPosition->id}`);

            // Chain the setters
            $excel->setCreator('Merqueo')
                ->setCompany('Merqueo');

            // Call them separately
            $excel->setDescription('Merqueo');

            $excel->sheet('Recibos', function ($sheet) use ($inventoryCountingPosition) {
                $titles = [
                    'Referencia',
                    'Nombre',
                    'Posición',
                    'Posición altura',
                    'Costo',
                    'Cantidad sistema',
                    'Cantidad contada',
                    'Costo de diferencia'
                ];
                $sheet->appendRow($titles);
                $inventoryCountingPositionDetails = $inventoryCountingPosition->inventoryCountingPositionDetails();
                $inventoryCountingPositionDetails->chunk(
                    100,
                    function ($inventoryCountingPositionDetails) use ($sheet, $inventoryCountingPosition) {
                        foreach ($inventoryCountingPositionDetails as $inventoryCountingPositionDetail) {
                            if (!is_null($inventoryCountingPositionDetail->quantity_counted)) {
                                try {
                                    $warehouseStorage = WarehouseStorage::with('storeProduct.product')
                                        ->where(
                                            'warehouse_id',
                                            $inventoryCountingPosition->warehouse_id
                                        )
                                        ->where('store_product_id', $inventoryCountingPositionDetail->store_product_id)
                                        ->where('quantity', $inventoryCountingPositionDetail->quantity_original)
                                        ->where('expiration_date', $inventoryCountingPositionDetail->expiration_date)
                                        ->where('position', $inventoryCountingPositionDetail->position)
                                        ->where('position_height', $inventoryCountingPositionDetail->position_height)
                                        ->firstOrFail();
                                } catch (ModelNotFoundException $e) {
                                    throw $e;
                                }

                                if ($warehouseStorage->quantity != $inventoryCountingPositionDetail->quantity_counted) {
                                    $costoDiferencia = abs($warehouseStorage->quantity
                                        - $inventoryCountingPositionDetail->quantity_counted);
                                    $productsToEmail = [
                                        'referencia' => $warehouseStorage->storeProduct->product->reference,
                                        'nombre' => $warehouseStorage->storeProduct->product->name,
                                        'posicion' => $inventoryCountingPositionDetail->position,
                                        'posicion_altura' => $inventoryCountingPositionDetail->position_height,
                                        'costo' => $inventoryCountingPositionDetail->storeProduct->cost,
                                        'cantidad_sistema' => $warehouseStorage->quantity,
                                        'cantidad_conteo' => $inventoryCountingPositionDetail->quantity_counted,
                                        'costo_diferencia' => $costoDiferencia * $inventoryCountingPositionDetail
                                                ->storeProduct->cost
                                    ];
                                    $sheet->appendRow($productsToEmail);
                                }
                            }
                        }
                    }
                );
            });
        })->store('xls', $path, true);

        try {
            $data = file_get_contents($file['full']);
            $base64 = base64_encode($data);
            $attachments[0]['type'] = 'application/vnd.ms-excel';
            $attachments[0]['name'] = $file['file'];
            $attachments[0]['content'] = $base64;
            send_mail($mail, $attachments, true);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Obtiene las bodegas por id de la ciudad
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesByCityId()
    {
        $cityId = Input::get('city_id');
        $warehouses = Warehouse::where('city_id', $cityId)->active()->get();
        return Response::json($warehouses);
    }

    /**
     * Obtiene los conteos por ajax para la grilla.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountingsAjax()
    {
        $warehouseId = Input::get('warehouse_id');
        $status = Input::get('status');

        $inventoryCountingPosition = InventoryCountingPosition::with([
            'admin',
            'warehouse' => function ($query) use ($warehouseId) {
                $query->where('id', $warehouseId)
                    ->with('city');
            },
            'inventoryCountingPositionDetails'
        ])
            ->whereHas('warehouse', function ($query) use ($warehouseId) {
                $query->where('id', $warehouseId);
            });

        if (!empty($status)) {
            $inventoryCountingPosition->where('status', $status);
        }

        $inventoryCountingPosition = $inventoryCountingPosition->orderBy('id', 'DESC')
            ->paginate(40);

        if (count($inventoryCountingPosition)) {
            $inventoryCountingPosition->each(function ($counting) {
                $counting->url_open = route('adminPositionCounting.view', ['id' => $counting->id]);
                $counting->url_delete = route('adminPositionCounting.delete', ['id' => $counting->id]);
                $counting->total_quantity = $counting->total_quantity;
                $counting->total_counted = $counting->total_counted;
            });
        }

        return Response::json($inventoryCountingPosition);
    }

    /**
     * Obtiene las posiciones de la bodega seleccionada
     *
     * @return json listado de posiciones
     */
    public function getPositionsWarehouseAjax()
    {
        $warehouse = Warehouse::find(Input::get('warehouse'));
        $storage_positions = $warehouse->storage_positions;
        return $storage_positions;
    }

    /**
     * Elimina el conteo por posición.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function delete($id)
    {
        $inventory_counting_position = InventoryCountingPosition::find($id);
        $inventory_counting_position->delete();
        return Redirect::route('adminPositionCounting.index')
            ->with('type', 'success')
            ->with(
                'message',
                'El conteo se eliminó correctamente.'
            );
    }
}