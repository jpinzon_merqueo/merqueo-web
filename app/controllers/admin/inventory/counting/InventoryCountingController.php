<?php

namespace admin\inventory\counting;

use Auth;
use Config;
use View;
use Input;
use Redirect;
use Request;
use Response;
use Session;
use Validator;
use DB;
use admin\AdminController;
use ProductStockUpdate;
use StoreProduct;
use StoreProductWarehouse;
use InventoryCounting;
use InventoryCountingDetail;
use InventoryCountingDetailCount;
use InventoryCountingSurvey;
use ProviderOrderReception;
use OrderReturn;
use WarehouseStorage;
use WarehouseStorageLog;
use Carbon\Carbon;
use PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007;

class InventoryCountingController extends AdminController {

    /**
     * InventoryCounting Repository
     *
     * @var InventoryCounting $inventoryCounting
     */
    protected $inventoryCounting;

    /**
     * InventoryCountingDetail Repository
     *
     * @var InventoryCountingDetail $inventoryCountingDetail
     */
    protected $inventoryCountingDetail;


    public function __construct(InventoryCounting $inventory_counting, InventoryCountingDetail $inventory_counting_detail) {
        parent::__construct();
        $this->inventoryCounting = $inventory_counting;
        $this->inventoryCountingDetail = $inventory_counting_detail;
    }

    /**
     * Obtiene los conteos del día
     *
     * @return Response
     */
    public function index() {
        if (!$this->admin_permissions['permission1']) {
            return Redirect::action('AdminInventoryCounting.count');
        }
        $input     = Input::all();
        $from      = Carbon::now();
        $to        = Carbon::now();
        $city      = isset($input['city_id']) ? $input['city_id'] : Session::get('admin_city_id');
        $warehouse = isset($input['warehouse']) ? $input['warehouse'] : '';
        $statuses  = [
            'Pendiente' => 'Pendiente',
            'En validación primer conteo' => 'En validación primer conteo',
            'Primer conteo realizado' => 'Primer conteo realizado',
            'En validación segundo conteo' => 'En validación segundo conteo',
            'Segundo conteo realizado' => 'Segundo conteo realizado',
            'Cerrado' => 'Cerrado',
        ];
        $status = (isset($input['status'])) ? $input['status'] : '';
        $inventory_counting = $this->inventoryCounting->with('Admin', 'Warehouse');
        if (Request::get('from', 0) && !Request::get('to', 0)) {
            $from = Carbon::createFromFormat('Y-m-d', Request::get('from'));
            $to   = Carbon::now();
        } else if (!Request::get('from', 0) && Request::get('to', 0)) {
            $from = Carbon::now();
            $to   = Carbon::createFromFormat('Y-m-d', Request::get('to'));
        } else if (Request::get('from', 0) && Request::get('to', 0)) {
            $from = Carbon::createFromFormat('Y-m-d', Request::get('from'));
            $to   = Carbon::createFromFormat('Y-m-d', Request::get('to'));
        }
        if (!empty($warehouse)) {
            $inventory_counting = $inventory_counting->where('warehouse_id', '=', $warehouse);
        }
        if (!empty($status)) {
            $inventory_counting = $inventory_counting->where('status', '=', $status);
        }
        //$inventory_counting = $inventory_counting->where('city_id', '=', $city)
        $inventory_counting = $inventory_counting->orderBy('created_at', 'desc')
            /*->whereNested(function($query) use ($from, $to)
            {
                $query->where('created_at', '>=', $from->startOfDay());
                $query->where('created_at', '<=', $to->endOfDay());
            })*/
            ->get();
        if (Request::ajax()) {
            return View::make('admin.inventory.counting.index')
                ->with('statuses', $statuses)
                ->with('status', $status)
                ->with('inventory_counting', $inventory_counting)
                ->renderSections()['content'];
       }
        $cities_object = $this->get_cities();
        $cities = [];
        foreach ($cities_object as $city_object) {
            $cities[$city_object->id] = $city_object;
        }
        return View::make('admin.inventory.counting.index')
            ->with('title', 'Conteo de inventario')
            ->with('cities', $cities)
            ->with('statuses', $statuses)
            ->with('status', $status)
            ->with('city', $city)
            ->with('inventory_counting', $inventory_counting);
    }

    /**
     * Ver un conteo específico
     *
     * @param integer $id Id del conteo
     */
    public function view($id) {
        if ($this->admin_permissions['permission1']) {
            $inventory_counting = $this->inventoryCounting->findOrFail($id);
            $inventory_counting_details = $inventory_counting->inventoryCountingDetails()
                ->orderBy('first_start_date', 'DESC')
                ->orderBy('first_end_date', 'DESC')
                ->get();
            $inventory_counting_details->each(function(&$inventory_counting_detail) {
                $first_total_count  = 0;
                $current_stock = 0;
                $second_total_count = 0;
                $first_snapshot_stock = 0;
                $second_snapshot_stock = 0;
                $stock_updated = TRUE;
                $inventory_counting_details_count = $inventory_counting_detail->inventoryCountingDetailsCount;
                $inventory_counting_details_count->each(function(&$inventory_counting_detail_count) use (&$first_total_count, &$second_total_count, &$current_stock, &$first_snapshot_stock, &$second_snapshot_stock, &$stock_updated) {
                    $first_total_count  += !empty($inventory_counting_detail_count->first_quantity) ? $inventory_counting_detail_count->first_quantity : 0;
                    $second_total_count += !empty($inventory_counting_detail_count->second_quantity) ? $inventory_counting_detail_count->second_quantity : 0;
                    $first_snapshot_stock += !empty($inventory_counting_detail_count->first_snapshot_quantity) ? $inventory_counting_detail_count->first_snapshot_quantity : 0;
                    $second_snapshot_stock += !empty($inventory_counting_detail_count->second_snapshot_quantity) ? $inventory_counting_detail_count->second_snapshot_quantity : 0;
                    if ($stock_updated && !$inventory_counting_detail_count->stock_updated) {
                        $stock_updated = FALSE;
                    }
                });
                $warehousestorage = WarehouseStorage::where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                    ->where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)->get();
                $current_stock += ($warehousestorage) ? $warehousestorage->sum('quantity') : 0;
                $first_total_count  += !empty($inventory_counting_detail->first_picking_counted) ? $inventory_counting_detail->first_picking_counted : 0;
                $second_total_count += !empty($inventory_counting_detail->second_picking_counted) ? $inventory_counting_detail->second_picking_counted : 0;
                $first_snapshot_stock += !empty($inventory_counting_detail->first_picking_snapshot) ? $inventory_counting_detail->first_picking_snapshot : 0;
                $second_snapshot_stock += !empty($inventory_counting_detail->second_picking_snapshot) ? $inventory_counting_detail->second_picking_snapshot : 0;
                $store_product_warehouse = StoreProductWarehouse::where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                    ->where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
                    ->get();
                if ($store_product_warehouse && $inventory_counting_detail->inventoryCounting->status != 'Cerrado') {
                    $current_stock += $store_product_warehouse->sum('picking_stock');
                }
                if ($stock_updated && !$inventory_counting_detail->picking_updated) {
                    $stock_updated = FALSE;
                }
                $inventory_counting_detail->first_total_count  = $first_total_count;
                $inventory_counting_detail->second_total_count = $second_total_count;
                $inventory_counting_detail->first_snapshot_stock = $first_snapshot_stock;
                $inventory_counting_detail->second_snapshot_stock = $second_snapshot_stock;
                $inventory_counting_detail->current_stock = $current_stock;
                $inventory_counting_detail->stock_updated = $stock_updated;
            });
            return View::make('admin.inventory.counting.view')
                ->with('title', 'Conteo de productos #' .$inventory_counting->id)
                ->with('inventory_counting', $inventory_counting)
                ->with('inventory_counting_details', $inventory_counting_details);
        }
        return Redirect::action('AdminInventoryCounting.count');
    }

    /**
     * Se obtiene la vista para iniciar el conteo de productos
     *
     */
    public function count() {
        $from = Carbon::now();
        $warehouse_id = Session::get('admin_warehouse_id');
        $counting = [];
        if (Request::ajax()) {
            if($this->admin_permissions['permission2']) {
                $first_validation_count = $this->getActiveCounts($warehouse_id, $from->toDateString(), 'first', 'En validación primer conteo');
                $second_validation_count = $this->getActiveCounts($warehouse_id, $from->toDateString(), 'second', 'En validación segundo conteo');
            }
            $first_count       = $this->getActiveCounts($warehouse_id, $from->toDateString(), 'first', 'Pendiente');
            $second_count     = $this->getActiveCounts($warehouse_id, $from->toDateString(), 'second', 'Primer conteo realizado');
            if($this->admin_permissions['permission2']) {
                $inventory_countings = $first_validation_count->merge($second_validation_count->merge($first_count->merge($second_count)));
            } else {
                $inventory_countings = $first_count->merge($second_count);
            }
            foreach ($inventory_countings as $count) {
                $counting[$count->id] = 'Conteo #' . $count->id . ' - ' . date("d/m/Y", strtotime($count->created_at));
            }
            return View::make('admin.inventory.counting.count_parts.select_count')
                ->with('counting', $counting);
        }
        return View::make('admin.inventory.counting.count')
            ->with('title', 'Conteo de unidades')
            ->with('counting', $counting);
    }

    /**
     * Obtiene los conteos activos del día basado en los parámetros.
     *
     * @param  int $city Id de la ciudad
     * @param  datetime $from Fecha de inicio formato: 'YYYY/MM/DD'
     * @param  String $step Paso a verificar 'first'/'second'
     * @param  String $status El estado del conteo
     *
     */
    protected function getActiveCounts($city, $date, $step, $status) {
        $inventory_countings = $this->inventoryCounting;
        $inventory_countings = $inventory_countings->where('warehouse_id', '=', $city)
            ->where('status', '=', $status);
        if ('first' == $step) {
            $inventory_countings = $inventory_countings->whereRaw("'" . $date . "' BETWEEN first_count_date AND first_end_count_date");
        } else {
            $inventory_countings = $inventory_countings->whereDate('second_count_date', '=', $date);
        }
        $inventory_countings = $inventory_countings->orderBy('created_at')
            ->get();
        return $inventory_countings;
    }

    /**
     * Obtiene los detalles de un conteo
     *
     * @param  integer $id id del conteo
     * @return html detalles del conteo
     */
    public function get_count($id) {
        $inventory_counting = $this->inventoryCounting->find($id);
        return View::make('admin.inventory.counting.count_parts.count')
                ->with('count_details', $inventory_counting);
    }

    /**
     * Obtiene un producto asociado a un conteo especifíco.
     *
     * @param  integer $id Id del conteo.
     * @param  String $step paso del conteo ya sea el primero o segundo.
     * @return json un detalle del conteo.
     */
    public function get_detail($id, $step) {
        $inventory_counting = $this->inventoryCounting->find($id);
        if (($inventory_counting->status == 'En validación primer conteo' || $inventory_counting->status == 'En validación segundo conteo') && $this->admin_permissions['permission2']) {
            $flag = true;
        } elseif ($inventory_counting->status != 'En validación primer conteo' && $inventory_counting->status != 'En validación segundo conteo') {
            $flag = true;
        } else {
            $flag = false;
        }
        if ($flag) {
            $admin_id = Session::get('admin_id');
            $status = 'En validación primer conteo';
            $detail = FALSE;
            switch ($step) {
                case 'first':
                    $detail = $this->get_active_store_product_counting_detail_user($id, $admin_id);
                    break;
                case 'second':
                    $status = 'En validación segundo conteo';
                    $detail = $this->get_active_store_product_counting_detail_user($id, $admin_id, $step);
                    break;
                default:
                    return Response::json([
                        'status' => 0,
                        'message' => 'Caso no contemplado',
                    ]);
                    break;
            }
            if ($detail->count()) {
                return [
                    'status' => 1,
                    'product' => $detail->first()->id,
                ];
            }
            return DB::transaction(function () use ($id, $admin_id, $status, $step) {
                $count_admin = $step . '_count_admin_id';
                $inventory_counting_details = $this->inventoryCountingDetail;
                $inventory_counting_details = $inventory_counting_details->select(
                    'inventory_counting_details.id'
                )
                    ->join('store_products', 'store_products.id', '=', 'inventory_counting_details.store_product_id')
                    ->where('inventory_counting_id', '=', $id);
                if ('second' == $step) {
                    $inventory_counting_details = $inventory_counting_details->where('is_marked_second_count', '=', 1);
                }
                $inventory_counting_details = $inventory_counting_details->whereNull($count_admin)
                    ->whereNull($step . '_picking_counted')
                    ->whereNull($step . '_quantity_counted')
                    ->orderBy(DB::raw("FIELD(storage, 'Seco', 'Congelado', 'Refrigerado')"))
                    ->lockForUpdate()
                    ->get();
                if ($inventory_counting_details->count()) {
                    $inventory_counting_detail = $inventory_counting_details->first();
                    $inventory_counting_detail_loaded = $this->inventoryCountingDetail;
                    $inventory_counting_detail_loaded = $inventory_counting_detail_loaded->find($inventory_counting_detail->id);
                    $inventory_counting_detail_loaded->{$count_admin} = $admin_id;
                    $inventory_counting_detail_loaded->{$step . '_start_date'} = Carbon::now();
                    $inventory_counting_detail_loaded->save();
                    return Response::json([
                        'status' => 1,
                        'product' => $inventory_counting_detail_loaded->id,
                    ]);
                } else {
                    $inventory_counting = $this->inventoryCounting;
                    $inventory_counting = $inventory_counting->find($id);
                    $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
                    $count = $inventory_counting_details->filter(function ($inventory_counting_detail) use ($step) {
                        if ('second' == $step) {
                            if ($inventory_counting_detail->is_marked_second_count
                                && !is_numeric($inventory_counting_detail->second_picking_counted)
                                && !is_numeric($inventory_counting_detail->second_quantity_counted)) {
                                return TRUE;
                            }
                            return FALSE;
                        } else if (!is_numeric($inventory_counting_detail->{$step . '_picking_counted'})
                            && !is_numeric($inventory_counting_detail->{$step . '_quantity_counted'})) {
                            return TRUE;
                        }
                    });
                    if (!$count->count()) {
                        $inventory_counting->status = $status;
                        $inventory_counting->save();
                        if ($inventory_counting->status == 'En validación primer conteo') {
                            $can_close = TRUE;
                            $inventory_counting = $this->inventoryCounting->with('inventoryCountingDetails')->findOrFail($id);
                            foreach ($inventory_counting->inventoryCountingDetails as $inventory_counting_detail) {
                                $inventory_counting_detail->is_marked_second_count = FALSE;
                                $inventory_counting->second_count_date = NULL;
                                $inventory_counting_detail->save();
                                $stock_updated = ($step == 'first') ? $inventory_counting_detail->first_quantity_counted : $inventory_counting_detail->second_quantity_counted;
                                $stock_updated += $inventory_counting_detail->{$step . '_picking_counted'};
                                $current_stock = 0;
                                $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
                                $inventory_counting_detail_counts->each(function($inventory_counting_detail_count) use (&$current_stock, $step) {
                                    if(isset($inventory_counting_detail_count->warehouseStorage->quantity)){
                                        $current_stock += $inventory_counting_detail_count->warehouseStorage->quantity;
                                    }
                                });
                                $store_product_warehouse = StoreProductWarehouse::where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                                    ->where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
                                    ->get()
                                    ->first();
                                $current_stock += ($store_product_warehouse) ? $store_product_warehouse->picking_stock : 0;
                                $percentage = ($current_stock != 0) ? (($stock_updated * 100) / $current_stock) : 0;
                                $round = ($percentage > 100) ? $percentage - 100 : 100 - $percentage;
                                if (!($round <= $inventory_counting->threshold || ($current_stock == 0 && $stock_updated == 0))) {
                                    if ('first' == $step) {
                                        $inventory_counting_detail->is_marked_second_count = TRUE;
                                        $inventory_counting_detail->save();
                                        $can_close = FALSE;
                                    }
                                }
                            }
                            if ('first' == $step) {
                                $inventory_counting->show_stock = FALSE;
                                if (!$can_close) {
                                    $inventory_counting->second_count_date = Carbon::now();
                                }
                                $inventory_counting->save();
                            }
                        }
                        return Response::json([
                            'status' => 2,
                            'message' => 'No éxisten más productos por contar, por favor seleccione un nuevo conteo.',
                        ]);
                    } else {
                        return Response::json([
                            'status' => 2,
                            'message' => 'Por favor selecciona un nuevo conteo.',
                        ]);
                    }
                }
            });
        } else {
            return Response::json([
                'status' => 0,
                'message' => 'Este conteo debe ser validado y no cuentas con permisos suficientes para esta accíón.',
            ]);
        }
    }

    /**
     * Obtiene el detalle de un conteo.
     *
     * @param  integer $id Id del conteo.
     * @param  integer $user_id Id del usuario que está contando.
     * @param  string $step El paso por el que va el conteo (first|second).
     */
    protected function get_active_store_product_counting_detail_user($id, $user_id, $step ='first') {
        $input = Input::all();
        $admin_count = $step . '_count_admin_id';
        $inventory_counting_detail = $this->inventoryCountingDetail;
        $inventory_counting_detail = $inventory_counting_detail->where('inventory_counting_id', '=', $id);
        if(!$this->admin_permissions['permission2']){
            $inventory_counting_detail->where($admin_count, '=', $user_id);
        }
        if(!empty($input['details'])){
            $details = json_decode($input['details']);
            $inventory_counting_detail->whereNotIn('id',$details);
        }
        if ('second' == $step) {
            $inventory_counting_detail = $inventory_counting_detail->where('is_marked_second_count', '=', 1);
        }
        return $inventory_counting_detail
            ->limit(1)
            ->get();
    }

    /**
     * Método para actualizar automáticamente el stock de los productos en la tienda.
     *
     * @param  integer $inventory_counting_id Id del conteo
     * @param  string  $step                  Paso del conteo (first|second)
     */
    protected function update_stock_counting($inventory_counting_id, $step = 'first') {
        $admin_id = Session::get('admin_id');
        $inventory_counting = $this->inventoryCounting->with('inventoryCountingDetails.inventoryCountingDetailsCount')->find($inventory_counting_id);
        $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
        $update_counting_detail = function(&$inventory_counting_detail, $step, $current_stock, $stock_updated) use ($admin_id) {
            $type = ($stock_updated > $current_stock) ? 'Aumento de unidades' : 'Disminución de unidades';
            $difference = ($type == 'Aumento de unidades') ? $stock_updated - $current_stock : $current_stock - $stock_updated;
            $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
            $inventory_counting_detail_counts->each(function(&$inventory_counting_detail_count) use ($step, $admin_id) {
                $warehouse_storage_log = new WarehouseStorageLog;
                $warehouse_storage_log->admin_id = $admin_id;
                $warehouse_storage_log->warehouse_id = $inventory_counting_detail_count->warehouseStorage->warehouse_id;
                $warehouse_storage_log->store_product_id = $inventory_counting_detail_count->warehouseStorage->store_product_id;
                $snapshot = $inventory_counting_detail_count->warehouseStorage->quantity;
                $quantity = $inventory_counting_detail_count->{$step . '_quantity'};
                if ($quantity == 0) {
                    $warehouse_storage_log->module = 'Conteo';
                    $warehouse_storage_log->movement = 'remove';
                    $warehouse_storage_log->quantity_from = $inventory_counting_detail_count->warehouseStorage->quantity;
                    $warehouse_storage_log->position_from = $inventory_counting_detail_count->warehouseStorage->position;
                    $warehouse_storage_log->position_height_from = $inventory_counting_detail_count->warehouseStorage->position_height;
                    $warehouse_storage_log->save();
                    $inventory_counting_detail_count->warehouseStorage->delete();
                    $inventory_counting_detail_count->stock_updated = 2;
                } else {
                    if ($quantity != $snapshot) {
                        $warehouse_storage_log->module = 'Conteo';
                        $warehouse_storage_log->movement = 'update';
                        $warehouse_storage_log->quantity_from = $snapshot;
                        $warehouse_storage_log->position_from = $inventory_counting_detail_count->warehouseStorage->position;
                        $warehouse_storage_log->position_height_from = $inventory_counting_detail_count->warehouseStorage->position_height;
                        $warehouse_storage_log->quantity_to = $quantity;
                        $warehouse_storage_log->position_to = $inventory_counting_detail_count->warehouseStorage->position;
                        $warehouse_storage_log->position_height_to = $inventory_counting_detail_count->warehouseStorage->position_height;
                        $warehouse_storage_log->save();
                        $inventory_counting_detail_count->warehouseStorage->quantity = $quantity;
                        $inventory_counting_detail_count->warehouseStorage->save();
                        $inventory_counting_detail_count->stock_updated = TRUE;
                    } else {
                        $inventory_counting_detail_count->stock_updated = FALSE;
                    }
                }
                $inventory_counting_detail_count->{$step . '_snapshot_quantity'} = $snapshot;
                $inventory_counting_detail_count->save();
            });
            $inventory_counting_detail->save();
            $store_product_warehouse = StoreProductWarehouse::where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
                ->where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                ->get()
                ->first();
            if ($store_product_warehouse) {
                $current = $store_product_warehouse->picking_stock;
                $update = $inventory_counting_detail->{$step . '_picking_counted'};
                $snapshot = $store_product_warehouse->picking_stock;
                $type = ($update > $current) ? 'Aumento de unidades' : 'Disminución de unidades';
                $difference = ($type == 'Aumento de unidades') ? $update - $current : $current - $update;
                $store_product_warehouse->picking_stock = $inventory_counting_detail->{$step . '_picking_counted'};
                $store_product_warehouse->save();
                $inventory_counting_detail->{$step . '_picking_snapshot'} = $snapshot;
                if ($update != $current) {
                    //Log de actualización
                    $product_stock_update = new ProductStockUpdate;
                    $product_stock_update->admin_id = $admin_id;
                    $product_stock_update->warehouse_id = $inventory_counting_detail->inventoryCounting->warehouse->id;
                    $product_stock_update->store_product_id = $inventory_counting_detail->store_product_id;
                    $product_stock_update->type = $type;
                    $product_stock_update->quantity = $difference;
                    $product_stock_update->quantity_stock_before = $current;
                    $product_stock_update->quantity_stock_after = $update ? $update : 0;
                    $product_stock_update->reason = 'Conteo de productos';
                    $product_stock_update->save();
                    $inventory_counting_detail->picking_updated = TRUE;
                } else {
                    $inventory_counting_detail->picking_updated = FALSE;
                }
            } else {
                $inventory_counting_detail->picking_updated = FALSE;
            }
            $inventory_counting_detail->save();
        };
        if ('second' == $step) {
            $inventory_counting_details = $inventory_counting_details->filter(function($inventory_counting_detail){
                if ($inventory_counting_detail->is_marked_second_count) {
                    return TRUE;
                }
                return FALSE;
            });
        }
        if ($inventory_counting_details->count()) {
            $status = ($step == 'first') ? 'Primer conteo realizado' : 'Cerrado';
            $can_close = TRUE;
            foreach ($inventory_counting_details as $inventory_counting_detail) {
                $stock_updated = ($step == 'first') ? $inventory_counting_detail->first_quantity_counted : $inventory_counting_detail->second_quantity_counted;
                $stock_updated += $inventory_counting_detail->{$step . '_picking_counted'};
                $current_stock = 0;
                $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
                $inventory_counting_detail_counts->each(function($inventory_counting_detail_count) use (&$current_stock, $step) {
                    if(isset($inventory_counting_detail_count->warehouseStorage->quantity)){
                        $current_stock += $inventory_counting_detail_count->warehouseStorage->quantity;
                    }
                });

                $store_product_warehouse = StoreProductWarehouse::where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                    ->where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
                    ->get()
                    ->first();
                $current_stock += ($store_product_warehouse) ? $store_product_warehouse->picking_stock : 0;

                $percentage = ($current_stock != 0) ? (($stock_updated * 100) / $current_stock) : 0;
                $round = ($percentage > 100) ? $percentage - 100 : 100 - $percentage;
                if ($round <= $inventory_counting->threshold || ($current_stock == 0 && $stock_updated == 0)) {
                    $update_counting_detail($inventory_counting_detail, $step, $current_stock, $stock_updated);
                } else {
                    if ('first' == $step) {
                        $inventory_counting_detail->is_marked_second_count = TRUE;
                        $inventory_counting_detail->save();
                        $can_close = FALSE;
                    } else {
                        $update_counting_detail($inventory_counting_detail, $step, $current_stock, $stock_updated);
                    }
                }
            }
            if ('first' == $step) {
                $inventory_counting->show_stock = FALSE;
                if ($can_close) {
                    $inventory_counting->status = 'Cerrado';
                } else {
                    $inventory_counting->second_count_date = Carbon::now();
                }
                $inventory_counting->save();
            }
            if ('second' == $step && $can_close) {
                $inventory_counting->status = $status;
                $inventory_counting->save();
            }
        }
    }

    /**
     * Obtiene la vista html de un detalle del conteo.
     *
     * @param  inst $id Id del conteo
     *
     */
    public function get_html_detail($id) {
        $admin_id = Session::get('admin_id');
        $step = Input::query('step', 'first');
        $inventory_counting_detail = $this->inventoryCountingDetail;
        $inventory_counting_detail = $inventory_counting_detail->find($id);
        $store_product_warehouse = StoreProductWarehouse::where('store_product_id', '=', $inventory_counting_detail->store_product_id)
            ->where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
            ->get()
            ->first();
        if ($inventory_counting_detail->inventoryCounting->status != 'Pendiente') {
            $inventory_counting_detail_counts = WarehouseStorage::leftJoin('inventory_counting_details_count', 'warehouse_storages.id', '=', 'inventory_counting_details_count.warehouse_storage_id')
                ->select('warehouse_storages.*', 'inventory_counting_details_count.first_quantity', 'inventory_counting_details_count.second_quantity')
                ->where('store_product_id', $inventory_counting_detail->store_product_id)
                ->where('warehouse_id', $inventory_counting_detail->inventoryCounting->warehouse_id)
                ->where('inventory_counting_details_count.inventory_counting_detail_id', $inventory_counting_detail->id)
                ->orderBy('position')
                ->orderBy('position_height')
                ->get();
        } else {
            $inventory_counting_detail_counts = WarehouseStorage::leftJoin('inventory_counting_details_count', 'warehouse_storages.id', '=', 'inventory_counting_details_count.warehouse_storage_id')
                ->select('warehouse_storages.*', 'inventory_counting_details_count.first_quantity', 'inventory_counting_details_count.second_quantity')
                ->where('store_product_id', $inventory_counting_detail->store_product_id)
                ->where('warehouse_id', $inventory_counting_detail->inventoryCounting->warehouse_id)
                ->orderBy('position')
                ->orderBy('position_height')
                ->groupBy('id')
                ->get();
        }
        $has_survey = $this->has_survey($inventory_counting_detail->inventory_counting_id);
        if (!$has_survey) {
            if (is_numeric($inventory_counting_detail->inventoryCounting->survey_admin_id)) {
                if ($inventory_counting_detail->inventoryCounting->survey_admin_id == $admin_id) {
                    $has_survey = TRUE;
                } else {
                    $has_survey = FALSE;
                }
            } else {
                $inventory_counting_detail->inventoryCounting->survey_admin_id = $admin_id;
                $inventory_counting_detail->inventoryCounting->save();
                $has_survey = TRUE;
            }
        } else {
            $has_survey = FALSE;
        }
        return View::make('admin.inventory.counting.count_parts.unit')
            ->with('inventory_counting_detail', $inventory_counting_detail)
            ->with('inventory_counting_detail_counts', $inventory_counting_detail_counts)
            ->with('store_product_warehouse', $store_product_warehouse)
            ->with('show_stock', !$inventory_counting_detail->inventoryCounting->show_stock)
            ->with('step', $step)
            ->with('has_survey', $has_survey);
    }

    protected function has_survey($inventory_counting_id) {
        return InventoryCountingSurvey::where('inventory_counting_id', '=', $inventory_counting_id)
            ->get()
            ->count();
    }

    /**
     * Actualiza un conteo.
     *
     * @param  int $id Id del conteo.
     */
    public function update($id) {
        $input  = array_only(Input::all(), array('show_stock'));
        $inventory_counting = $this->inventoryCounting->findOrFail($id);
        $update = FALSE;
        $return = [
            'status' => 1,
        ];
        if (isset($input['show_stock'])) {
            $inventory_counting->show_stock = TRUE;
            $update = TRUE;
        } else {
            $inventory_counting->show_stock = FALSE;
            $update = TRUE;
        }
        if ($update) {
            $inventory_counting->save();

        }
        return Response::json($return);
    }

    /**
     * Actualiza un detalle del conteo.
     *
     * @param  int $id Id del detalle del conteo.
     */
    public function update_detail($id) {
        $input  = Input::all();
        $step   = $input['step'];
        $count_picking   = isset($input['picking_counted']) ? $input['picking_counted'] : 0;
        $warehouse_storage_store_products = isset($input['warehouse_storage_store_product']) ? $input['warehouse_storage_store_product'] : [];
        $values_counted = isset($input['count']) ? $input['count'] : [];
        $bad_ubications = isset($input['is_bad_ubication']) ? $input['is_bad_ubication'] : [];
        $products_position = isset($input['product_position']) ? $input['product_position'] : [];
        $products_height   = isset($input['product_height']) ? $input['product_height'] : [];
        $inventory_counting_detail = $this->inventoryCountingDetail;
        $inventory_counting_detail = $inventory_counting_detail->find($id);
        $status = $inventory_counting_detail->inventoryCounting->status;
        if ($status == 'Cerrado') {
            return Response::json([
                'status' => 0,
                'message' => 'La información no se puede guardar porque el conteo ya se encuentra en estado Cerrado.'
             ]);
        }
        $inventory_counting_detail->{$step . '_picking_counted'} = $count_picking;
        $inventory_counting_detail->{$step . '_end_date'} = Carbon::now();
        $quantity_counted = 0;
        if ($step == 'first') {
            $inventory_counting_detail->is_bad_reference = isset($input['is_bad_reference']) ? TRUE : FALSE;
            $inventory_counting_detail->is_bad_image     = isset($input['is_bad_image']) ? TRUE : FALSE;
        }
        foreach ($warehouse_storage_store_products as $key => $warehouse_storage_store_product) {
            $inventory_counting_detail_count = InventoryCountingDetailCount::where('inventory_counting_detail_id', '=', $id)
                ->where('warehouse_storage_id', '=', $warehouse_storage_store_product)
                ->get()
                ->first();
            if (is_null($inventory_counting_detail_count)) {
                $inventory_counting_detail_count = new InventoryCountingDetailCount;
            }
            if ($step != 'second') {
                $inventory_counting_detail_count->is_bad_ubication = (isset($bad_ubications[$key]) && $bad_ubications[$key] == $warehouse_storage_store_product) ? TRUE : FALSE;
                $inventory_counting_detail_count->position = $products_position[$key];
                $inventory_counting_detail_count->position_height = $products_height[$key];
                $inventory_counting_detail_count->warehouse_storage_id = $warehouse_storage_store_product;
            }
            $inventory_counting_detail_count->inventory_counting_detail_id = $id;
            $inventory_counting_detail_count->{$step . '_quantity'} = isset($values_counted[$warehouse_storage_store_product]) ? $values_counted[$warehouse_storage_store_product] : NULL;
            $inventory_counting_detail_count->save();
            $quantity_counted += $values_counted[$warehouse_storage_store_product];
        }
        $inventory_counting_detail->{$step . '_quantity_counted'} = $quantity_counted;
        $inventory_counting_detail->save();
        return Response::json([
            'status' => 1,
        ]);
    }

    /**
     * Método para liberar una referencia del conteo
     *
     * @param  integer $id_detail id de detalle del conteo.
     * @param  string  $step      Paso del conteo (first|second)
     */
    public function release_detail($id_detail, $step = 'first') {
        $input  = Input::all();
        $inventory_counting_detail = $this->inventoryCountingDetail;
        $inventory_counting_detail = $inventory_counting_detail->findOrFail($id_detail);
        $method = Request::method();
        if ('POST' == $method) {
            switch ($step) {
                case 'first':
                    $inventory_counting_detail->{$step . '_count_admin_id'}   = NULL;
                    $inventory_counting_detail->{$step . '_picking_counted'}  = NULL;
                    $inventory_counting_detail->{$step . '_quantity_counted'} = NULL;
                    $inventory_counting_detail->{$step . '_start_date'}       = NULL;
                    $inventory_counting_detail->{$step . '_end_date'}         = NULL;
                    $inventory_counting_detail->is_bad_image                  = 0;
                    $inventory_counting_detail->is_bad_reference              = 0;
                    $inventory_counting_detail->is_marked_second_count        = 0;
                    $inventory_counting_detail->save();
                    $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
                    foreach ($inventory_counting_detail_counts as $inventory_counting_detail_count) {
                        $inventory_counting_detail_count->delete();
                    }
                    return Redirect::route('AdminInventoryCounting.view', ['id' => $inventory_counting_detail->inventoryCounting->id])
                        ->with('type', 'success')
                        ->with('message', 'Liberación de la referencia: ' . $inventory_counting_detail->storeProduct->product->reference . ' para el primer conteo éxitosa.');
                case 'second':
                    $inventory_counting_detail->{$step . '_count_admin_id'}   = NULL;
                    $inventory_counting_detail->{$step . '_picking_counted'}  = NULL;
                    $inventory_counting_detail->{$step . '_quantity_counted'} = NULL;
                    $inventory_counting_detail->{$step . '_start_date'}       = NULL;
                    $inventory_counting_detail->{$step . '_end_date'}         = NULL;
                    $inventory_counting_detail->save();
                    $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
                    foreach ($$inventory_counting_detail_counts as $inventory_counting_detail_count) {
                        $inventory_counting_detail_count->second_quantity = NULL;
                        $inventory_counting_detail_count->stock_updated   = NULL;
                        $inventory_counting_detail_count->second_snapshot_quantity = NULL;
                        $inventory_counting_detail_count->save();
                    }
                    return Redirect::route('AdminInventoryCounting.view', ['id' => $inventory_counting_detail->inventoryCounting->id])
                        ->with('type', 'success')
                        ->with('message', 'Liberación de la referencia: ' . $inventory_counting_detail->storeProduct->product->reference . ' para el segundo conteo éxitosa.');
                default:
                    return Redirect::route('AdminInventoryCounting.view', ['id' => $inventory_counting_detail->inventoryCounting->id])
                        ->with('type', 'error')
                        ->with('message', 'Paso no soportado');
            }
        }
        return View::make('admin.inventory.counting.count_parts.release_detail')
            ->with('inventory_counting_detail', $inventory_counting_detail);
    }

    /**
     * Guardar conteo
     */
    public function save() {
        if (Input::has('city_id_form') && Input::hasFile('csv_file')) {
            $admin_id = Session::get('admin_id');
            $file = Input::file('csv_file')->getRealPath();
            $input = Input::all();
            $handle = fopen($file, 'r');
            $rows = 0;
            $rows_error = [
                'not_exits'  => 0,
                'not_simple' => 0,
                'empty'      => 0,
                'repeated'   => 0,
            ];
            $processed = array();
            // Se salta la cabecera del csv.
            fgets($handle, 4096);
            DB::beginTransaction();
            $from = Carbon::createFromFormat('Y/m/d', $input['first_count_date']);
            $to   = Carbon::createFromFormat('Y/m/d', $input['first_end_count_date']);
            $inventory_counting = new InventoryCounting;
            $inventory_counting->admin_id = $admin_id;
            $inventory_counting->warehouse_id = $input['warehouse_id_form'];
            $inventory_counting->status = 'Pendiente';
            $inventory_counting->first_count_date = $from->startOfDay();
            $inventory_counting->first_end_count_date = $to->endOfDay();
            $inventory_counting->threshold = $input['threshold'];
            $inventory_counting->save();
            while (!feof($handle)) {
                $row = trim(fgets($handle));
                $store_product = StoreProduct::find($row);
                if ($store_product) {
                    if ($store_product->product->type == 'Simple') {
                        if (!in_array($row, $processed)) {
                            $inventory_counting_detail = new InventoryCountingDetail;
                            $inventory_counting_detail->inventory_counting_id = $inventory_counting->id;
                            $inventory_counting_detail->store_product_id = $store_product->id;
                            $inventory_counting_detail->save();
                            $rows++;
                            array_push($processed, $row);
                        } else {
                            $rows_error['repeated'] = $rows_error['repeated'] + 1;
                        }
                    } else {
                        $rows_error['not_simple'] = $rows_error['not_simple'] + 1;
                    }
                } else {
                    if (strlen($row) != 0) {
                        $rows_error['not_exits'] = $rows_error['not_exits'] + 1;
                    } else {
                        $rows_error['empty'] = $rows_error['empty'] + 1;
                    }
                }
            }
            fclose($handle);
            if ($rows_error['not_simple'] || $rows_error['not_exits'] || $rows_error['repeated']) {
                DB::rollback();
                if ($rows_error['repeated']) {
                    return Redirect::route('AdminInventoryCounting.index')
                        ->with('type', 'error')
                        ->with('message', 'Se encontraron: ' . $rows_error['repeated'] . ' registros repetidos');
                }
                return Redirect::route('AdminInventoryCounting.index')
                    ->with('type', 'error')
                    ->with('message', 'El archivo no se importó debido a que se encontraroón los siguientes errores: ' . $rows_error['not_simple'] . ' PRODUCTOS QUE NO SON DE TIPO SIMPLE Y/O ' . $rows_error['not_exits'] . ' PRODUCTOS QUE NO EXISTEN ASOCIADOS A NINGÚNA TIENDA.');
            } else {
                if ($rows) {
                    $today = Carbon::now();
                    $errors = [];
                    $receipt = ProviderOrderReception::select('provider_order_receptions.*')
                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
                        ->where('provider_order_receptions.status', '=', 'Recibido')
                        ->whereDate('provider_order_receptions.received_date', '<>', $today->toDateString())
                        ->where('provider_orders.warehouse_id', '=', $input['warehouse_id_form'])
                        ->get()
                        ->count();
                    if($receipt) {
                        $errors[] = "Tienen: {$receipt} recibos de bodega en estado RECIBIDO";
                    }
                    $order_returns = OrderReturn::select('order_returns.*')
                        ->join('orders', 'orders.id', '=', 'order_returns.order_id')
                        ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                        ->where('order_returns.status', '=', 'Devuelto')
                        ->whereDate('order_returns.return_date', '<>', $today->toDateString())
                        ->where('order_groups.warehouse_id', '=', $input['warehouse_id_form'])
                        ->get()
                        ->count();
                    if ($order_returns) {
                        $errors[] = "Tienen: {$order_returns} ordenes en estado DEVUELTO";
                    }
                    DB::commit();
                    return Redirect::route('AdminInventoryCounting.view', ['id' => $inventory_counting->id])
                        ->withErrors($errors)
                        ->with('type', 'success')
                        ->with('message', 'Se importaron correctamente ' . $rows . ' registros de productos');
                }
                DB::rollback();
                return Redirect::route('AdminInventoryCounting.index')
                    ->with('type', 'error')
                    ->with('message', "No se suministraron id's válidos para realizar la importación.");
            }
        }
        return Redirect::route('AdminInventoryCounting.index')
            ->with('type', 'error')
            ->with('message', 'Se debe suministrar el archivo de productos y/o la ciudad.');
    }

    /**
     * Método para generar reportes.
     *
     */
    public function generate_report($id) {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        DB::statement("SET lc_time_names = 'es_ES'");
        $yer_or_not = [
            0 => 'No',
            1 => 'Si',
        ];
        $inventory_counting = $this->inventoryCounting->findOrFail($id);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setSubject('Reporte de conteo');
        $objPHPExcel->getProperties()->setTitle('Información del conteo');
        $objPHPExcel->setActiveSheetIndex(0);
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'5178A5'),
            ),
            'font' => array(
                'color' => array('rgb'=>'FFFFFF'),
                'bold' => true,
            )
        );
        $headers = [ 'ID DEL CONTEO', 'NOMBRE QUIEN GENERÓ PRIMER CONTEO', 'CIUDAD',
            'BODEGA', 'FECHA INICIO PRIMER CONTEO', 'FECHA FINALIZACIÓN PRIMER CONTEO',
            'FECHA INICIO SEGUNDO CONTEO', 'FECHA FINALIZACIÓN SEGUNDO CONTEO',
            'UMBRAL',
        ];
        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($style_header);
        $i = 0;
        foreach ($headers as $key) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }
        $j = 2;
        $values = [
            $inventory_counting->id, $inventory_counting->admin->fullname,
            $inventory_counting->warehouse->city->city,
            $inventory_counting->warehouse->warehouse,
            $inventory_counting->first_count_date,
            $inventory_counting->first_end_count_date,
            $inventory_counting->second_count_date,
            $inventory_counting->updated_at,
            $inventory_counting->threshold . '%',
        ];
        $i = 0;
        foreach ($values as $key => $value){
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
            $i++;
        }

        //Detalles generales del producto
        $objPHPExcel->getActiveSheet()->setTitle('Detalle del conteo');
        $headers = ['ID PRODUCTO', 'PRODUCTO', 'NOMBRE DE QUIEN REALIZÓ PRIMER CONTEO',
            'FECHA INICIO PRIMER CONTEO', 'FECHA FINAL PRIMER CONTEO', 'PRIMER PICKING CONTADO',
            'TOTAL PRIMER CONTADO', 'PRIMER PICKING SNAPSHOT', 'NOMBRE DE QUIEN REALIZÓ SEGUNDO CONTEO',
            'FECHA INICIO SEGUNDO CONTEO', 'FECHA FINAL SEGUNDO CONTEO', 'SEGUNDO PICKING CONTADO',
            'TOTAL SEGUNDO CONTADO', 'SEGUNDO PICKING SNAPSHOT', 'ACTUALIZÓ PICKING',
            '¿FUE MARCADO PARA SEGUNDO CONTEO?', 'IMAGEN ERRADA', 'REFERENCIA ERRADA',
        ];
        $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->setActiveSheetIndex(1);
        $sheet->getStyle('A1:R1')->applyFromArray($style_header);
        $i = 0;
        foreach ($headers as $key) {
            $sheet->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }
        $j = 2;
        foreach ($inventory_counting_details as &$inventory_counting_detail) {
            $values = [
                $inventory_counting_detail->storeProduct->id,
                $inventory_counting_detail->storeProduct->product->name . ' ' . $inventory_counting_detail->storeProduct->product->quantity . ' ' . $inventory_counting_detail->storeProduct->product->unit,
                is_numeric($inventory_counting_detail->first_count_admin_id) ? $inventory_counting_detail->firstCountAdmin->fullname : '',
                !empty($inventory_counting_detail->first_start_date) ? $inventory_counting_detail->first_start_date : '',
                !empty($inventory_counting_detail->first_end_date) ? $inventory_counting_detail->first_end_date : '',
                is_numeric($inventory_counting_detail->first_picking_counted) ? $inventory_counting_detail->first_picking_counted : '' ,
                $inventory_counting_detail->first_quantity_counted + $inventory_counting_detail->first_picking_counted,
                is_numeric($inventory_counting_detail->first_picking_snapshot) ? $inventory_counting_detail->first_picking_snapshot : '' ,
                is_numeric($inventory_counting_detail->second_count_admin_id) ? $inventory_counting_detail->secondCountAdmin->fullname : '',
                !empty($inventory_counting_detail->second_start_date) ? $inventory_counting_detail->second_start_date : '',
                !empty($inventory_counting_detail->second_end_date) ? $inventory_counting_detail->second_end_date : '',
                is_numeric($inventory_counting_detail->second_picking_counted) ? $inventory_counting_detail->second_picking_counted : '' ,
                $inventory_counting_detail->second_quantity_counted + $inventory_counting_detail->second_picking_counted,
                is_numeric($inventory_counting_detail->second_picking_snapshot) ? $inventory_counting_detail->second_picking_snapshot : '' ,
                $yer_or_not[$inventory_counting_detail->picking_updated],
                $yer_or_not[$inventory_counting_detail->is_marked_second_count],
                $yer_or_not[$inventory_counting_detail->is_bad_image],
                $yer_or_not[$inventory_counting_detail->is_bad_reference],
            ];
            $i = 0;
            foreach ($values as $key => $value){
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                $i++;
            }
            $j++;
        }
        $sheet->setTitle('Detalle del producto contado');

        //Posiciones del producto
        $headers = ['ID PRODUCTO', 'PRODUCTO', 'UBICACIÓN', 'ALTURA',
            'PRIMERA CANTIDAD CONTADA', 'PRIMER SNAPSHOT', 'SEGUNDA CANTIDAD CONTADA',
            'SEGUNDO SNAPSHOT', 'UBICACIÓN ERRADA', '¿ACTUALIZÓ EL STOCK?'
        ];
        $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->setActiveSheetIndex(2);
        $sheet->getStyle('A1:J1')->applyFromArray($style_header);
        $i = 0;
        foreach ($headers as $key) {
            $sheet->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }
        $j = 2;
        foreach ($inventory_counting_details as &$inventory_counting_detail) {
            $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
            if ($inventory_counting_detail_counts->count()) {
                foreach ($inventory_counting_detail_counts as &$inventory_counting_detail_count) {
                    $values = [
                        $inventory_counting_detail_count->inventoryCountingDetail->storeProduct->id,
                        $inventory_counting_detail_count->inventoryCountingDetail->storeProduct->product->name . ' ' . $inventory_counting_detail_count->inventoryCountingDetail->storeProduct->product->quantity . ' ' . $inventory_counting_detail_count->inventoryCountingDetail->storeProduct->product->unit,
                        $inventory_counting_detail_count->position,
                        $inventory_counting_detail_count->position_height,
                        $inventory_counting_detail_count->first_quantity,
                        $inventory_counting_detail_count->first_snapshot_quantity,
                        $inventory_counting_detail_count->second_quantity,
                        $inventory_counting_detail_count->second_snapshot_quantity,
                        $yer_or_not[$inventory_counting_detail_count->is_bad_ubication],
                        ($inventory_counting_detail_count->stock_updated == 2) ? 'Posición en Bodega eliminada' : $yer_or_not[$inventory_counting_detail_count->stock_updated],
                    ];
                    $i = 0;
                    foreach ($values as $key => $value){
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                        $i++;
                    }
                    $j++;
                }
            }
        }
        $sheet->setTitle('Posiciones del producto');

        //Encuesta
        $questions = $inventory_counting->inventoryCountingSurveys;
        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->setActiveSheetIndex(3);
        $headers = ['NOMBRE QUIEN LLENÓ LA ENCUESTA', 'PREGUNTA', 'RESPUESTA', 'ORDEN DE COMPRA'];
        $sheet->getStyle('A1:D1')->applyFromArray($style_header);
        $i = 0;
        foreach ($headers as $key) {
            $sheet->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }
        $j = 2;
        foreach ($questions as $question) {
            $i = 0;
            $values = [$question->admin->fullname, $question->question, $yer_or_not[$question->answer],  $question->purchase_order];
            foreach ($values as $key => $value){
                $sheet->SetCellValueByColumnAndRow($i, $j, $value);
                $i++;
            }
            $j++;
        }
        $sheet->setTitle('Encuesta');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
        $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
        $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
        $file['client_original_name'] = $filename;
        $file['client_original_extension'] = 'xlsx';
        $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        return Redirect::route('AdminInventoryCounting.view', array('id' => $id))
            ->with('file_url', $url)
            ->with('type', 'success')
            ->with('message', 'Report generated successfully.');
    }

    /**
     * Guardar encuesta del conteo.
     *
     * @param  integer $id Id del conteo
     */
    public function save_survey($id) {
        $input = array_only(Input::all(), array(
            'answer_1',
            'answer_2',
            'answer_3',
            'answer_4',
            'answer_5',
            'answer_6',
            'question_1',
            'question_2',
            'question_3',
            'question_4',
            'question_5',
            'question_6',
            'purchase_order_answer_1',
            'purchase_order_answer_2',
            'purchase_order_answer_4',
            'purchase_order_answer_5'
        ));
        $admin_id = Session::get('admin_id');
        for ($i=1; $i <= 6; $i++) {
            $inventory_counting_survey = new InventoryCountingSurvey;
            $inventory_counting_survey->admin_id = $admin_id;
            $inventory_counting_survey->inventory_counting_id = $id;
            $inventory_counting_survey->question = $input['question_' . $i];
            $inventory_counting_survey->answer = $input['answer_' . $i];
            if ($i == 1 || $i == 2 || $i == 4 || $i == 5) {
                $inventory_counting_survey->purchase_order = $input['purchase_order_answer_' . $i];
            }
            $inventory_counting_survey->save();
        }

        return Response::json([
            'status' => 1,
        ]);
    }

    /**
     * Método usado para actualiza una columna en particular o realizar una acción personalizada asociada al conteo
     *
     * @param  integer $id     Id del conteo
     * @param  String $action Accion a realizar
     */
    public function update_column($id, $action) {
        $inventory_counting = $this->inventoryCounting->findOrFail($id);
        switch ($action) {
            case 'first':
                $inventory_counting->first_count_date = Carbon::now();
                $inventory_counting->save();
                return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                    ->with('type', 'success')
                    ->with('message', 'Unidades listas para el primer conteo.');
            case 'second':
                $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
                $inventory_counting_details = $inventory_counting_details->filter(function($inventory_counting_detail) {
                    if ($inventory_counting_detail->is_marked_second_count && !is_numeric($inventory_counting_detail->second_quantity_counted)) {
                        return TRUE;
                    }
                    return FALSE;
                });
                if ($inventory_counting_details->count()) {
                    $inventory_counting->second_count_date = Carbon::now();
                    $inventory_counting->save();
                    return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                        ->with('type', 'success')
                        ->with('message', 'Unidades listas para el segundo conteo.');
                } else {
                    return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                        ->with('type', 'error')
                        ->with('message', 'Debe marcar al menos un producto para habilitar el segundo conteo.');
                }
            case 'close':
                $inventory_counting->status = 'Cerrado';
                $inventory_counting->save();
                return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                    ->with('type', 'success')
                    ->with('message', 'Conteo de unidades cerrado.');
        }
    }

    /**
     * Método usado para cerrar un conteo.
     *
     * @param  integer $id Id del conteo
     */
    public function close($id) {
        $admin_id = Session::get('admin_id');
        $status_to_close = [
            'Pendiente',
            'Primer conteo realizado',
            'Segundo conteo realizado',
        ];
        $inventory_counting = $this->inventoryCounting->findOrFail($id);
        if (Input::has('is_marked_close_count') && in_array($inventory_counting->status, $status_to_close)) {
            $input = Input::all();
            $ids = $input['is_marked_close_count'];
            $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
            $update_counting_detail = function(&$inventory_counting_detail, $step, $current_stock, $stock_updated) use ($admin_id) {
                $type = ($stock_updated > $current_stock) ? 'Aumento de unidades' : 'Disminución de unidades';
                $difference = ($type == 'Aumento de unidades') ? $stock_updated - $current_stock : $current_stock - $stock_updated;
                $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
                if (count($inventory_counting_detail_counts)) {
                    $inventory_counting_detail_counts->each(function (&$inventory_counting_detail_count) use ($step, $admin_id) {
                        $warehouse_storage_log = new WarehouseStorageLog;
                        $warehouse_storage_log->admin_id = $admin_id;
                        $warehouse_storage_log->warehouse_id = $inventory_counting_detail_count->warehouseStorage->warehouse_id;
                        $warehouse_storage_log->store_product_id = $inventory_counting_detail_count->warehouseStorage->store_product_id;
                        $snapshot = $inventory_counting_detail_count->warehouseStorage->quantity;
                        $quantity = $inventory_counting_detail_count->{$step . '_quantity'};
                        if ($quantity == 0) {
                            $warehouse_storage_log->module = 'Conteo';
                            $warehouse_storage_log->movement = 'remove';
                            $warehouse_storage_log->quantity_from = $inventory_counting_detail_count->warehouseStorage->quantity;
                            $warehouse_storage_log->position_from = $inventory_counting_detail_count->warehouseStorage->position;
                            $warehouse_storage_log->position_height_from = $inventory_counting_detail_count->warehouseStorage->position_height;
                            $warehouse_storage_log->save();
                            $inventory_counting_detail_count->warehouseStorage->delete();
                            $inventory_counting_detail_count->stock_updated = 2;
                        } else {
                            $warehouse_storage_log->module = 'Conteo';
                            $warehouse_storage_log->movement = 'update';
                            $warehouse_storage_log->quantity_from = $inventory_counting_detail_count->warehouseStorage->quantity;
                            $warehouse_storage_log->position_from = $inventory_counting_detail_count->warehouseStorage->position;
                            $warehouse_storage_log->position_height_from = $inventory_counting_detail_count->warehouseStorage->position_height;
                            $warehouse_storage_log->quantity_to = $quantity;
                            $warehouse_storage_log->position_to = $inventory_counting_detail_count->warehouseStorage->position;
                            $warehouse_storage_log->position_height_to = $inventory_counting_detail_count->warehouseStorage->position_height;
                            $warehouse_storage_log->save();
                            $inventory_counting_detail_count->warehouseStorage->quantity = $quantity;
                            $inventory_counting_detail_count->warehouseStorage->save();
                            $inventory_counting_detail_count->stock_updated = TRUE;
                        }
                        $inventory_counting_detail_count->{$step . '_snapshot_quantity'} = $snapshot;
                        $inventory_counting_detail_count->save();
                    });
                }
                $store_product_warehouse = StoreProductWarehouse::where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
                    ->where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                    ->get()
                    ->first();
                if ($store_product_warehouse) {
                    $current = $store_product_warehouse->picking_stock;
                    $update = $inventory_counting_detail->{$step . '_picking_counted'};
                    $snapshot = $store_product_warehouse->picking_stock;
                    $type = ($update > $current) ? 'Aumento de unidades' : 'Disminución de unidades';
                    $difference = ($type == 'Aumento de unidades') ? $update - $current : $current - $update;
                    $store_product_warehouse->picking_stock = $inventory_counting_detail->{$step . '_picking_counted'};
                    $store_product_warehouse->save();
                    $inventory_counting_detail->{$step . '_picking_snapshot'} = $snapshot;
                    //Log de actualización
                    $product_stock_update = new ProductStockUpdate;
                    $product_stock_update->admin_id = $admin_id;
                    $product_stock_update->warehouse_id = $inventory_counting_detail->inventoryCounting->warehouse->id;
                    $product_stock_update->store_product_id = $inventory_counting_detail->store_product_id;
                    $product_stock_update->type = $type;
                    $product_stock_update->quantity = $difference;
                    $product_stock_update->quantity_stock_before = $current;
                    $product_stock_update->quantity_stock_after = $update ? $update : 0;
                    $product_stock_update->reason = 'Conteo de productos';
                    $product_stock_update->save();
                    $inventory_counting_detail->picking_updated = TRUE;
                }
                $inventory_counting_detail->save();
            };
            $inventory_counting_details = $inventory_counting_details->filter(function($inventory_counting_detail) use ($ids) {
                if (in_array($inventory_counting_detail->id, $ids)) {
                    return TRUE;
                }
                return FALSE;
            });
            if ($inventory_counting_details->count()) {
                foreach ($inventory_counting_details as $inventory_counting_detail) {
                    $step = (empty($inventory_counting_detail->second_quantity_counted)) ? 'first' : 'second';
                    $stock_updated = ($step == 'first') ? $inventory_counting_detail->first_quantity_counted : $inventory_counting_detail->second_quantity_counted;
                    $stock_updated += $inventory_counting_detail->{$step . '_picking_counted'};
                    $current_stock = 0;
                    $inventory_counting_detail_counts = $inventory_counting_detail->inventoryCountingDetailsCount;
                    if (count($inventory_counting_detail_counts)) {
                        $inventory_counting_detail_counts->each(function ($inventory_counting_detail_count) use (&$current_stock) {
                            $current_stock += $inventory_counting_detail_count->warehouseStorage->quantity;
                        });
                    }
                    $picking = StoreProductWarehouse::where('warehouse_id', '=', $inventory_counting_detail->inventoryCounting->warehouse_id)
                        ->where('store_product_id', '=', $inventory_counting_detail->store_product_id)
                        ->get()
                        ->first();
                    $current_stock += ($picking) ? $picking->picking_stock : 0;
                    $update_counting_detail($inventory_counting_detail, $step, $current_stock, $stock_updated);
                }
                $inventory_counting->status = 'Cerrado';
                $inventory_counting->admin_close = $admin_id;
                $inventory_counting->save();
                return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                    ->with('type', 'success')
                    ->with('message', 'Conteo de unidades cerrado.');
            } else {
                return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                    ->withErrors(['Al realizar el filtro no quedaron productos para ser actualizados.']);
            }
        } else if (!Input::has('is_marked_close_count')) {
            return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                ->withErrors(['No se puede cerrar el conteo por que no se enviaron los productos que se desean cerrar.']);
        } else if (!in_array($inventory_counting->status, $status_to_close)) {
            return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
                ->withErrors(['No se puede cerrar el conteo por que se encuentra en aprobación o cerrado.']);
        }
    }

    /**
     * Eliminar conteo
     */
    public function delete($id) {
        $inventory_counting = $this->inventoryCounting
            ->findOrFail($id);
        $inventory_counting->inventoryCountingSurveys()
            ->delete();
        $inventory_counting_details = $inventory_counting->inventoryCountingDetails;
        foreach ($inventory_counting_details as $inventory_counting_detail) {
            $inventory_counting_detail->inventoryCountingDetailsCount()
                ->delete();
        }
        $inventory_counting->inventoryCountingDetails()
            ->delete();
        $inventory_counting->delete();
        return Redirect::action('AdminInventoryCounting.index')
            ->with('type', 'success')
            ->with('message', 'Conteo de productos eliminado.');
    }

    /**
     * Obtiene las bodegas por id de la ciudad
     *
     * @return json listado de bodegas
     */
    public function get_warehouses_by_city_id() {
        return Response::json($this->get_warehouses_ajax());
    }

    /**
     * Funcion para aprobar el conteo por parte de los lideres de inventario
     * @param int $id id del conteo
    */
    public function approve_counting($id) {
        $inventory_counting = $this->inventoryCounting->with('inventoryCountingDetails')->findOrFail($id);
        if($inventory_counting->status == 'En validación primer conteo') {
            $step = 'first';
            $inventory_counting->status = 'Primer conteo realizado';
            $inventory_counting->save();
            $this->update_stock_counting($inventory_counting->id, $step);
        } else if($inventory_counting->status == 'En validación segundo conteo') {
            $step = 'second';
            $inventory_counting->status = 'Segundo conteo realizado';
            $inventory_counting->save();
            $this->update_stock_counting($inventory_counting->id, $step);
        }

        return Redirect::action('AdminInventoryCounting.view', ['id' => $id])
            ->with('type', 'success')
            ->with('message', 'Conteo aprobado exitosamente.');
    }
}
