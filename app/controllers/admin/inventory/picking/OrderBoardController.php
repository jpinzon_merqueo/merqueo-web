<?php

namespace admin\inventory\picking;

use admin\AdminController;
use Carbon\Carbon;
use City;
use DB;
use Event;
use exceptions\MerqueoException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use Redirect;
use Response;
use Store;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;

class OrderBoardController extends AdminController
{
    /**
     * Muestra inicio del módulo.
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Tablero de pedidos';
        $cities = $this->get_cities();
        $warehouses = $this->get_warehouses();

        return View::make('admin.inventory.order.index', compact(
            'title',
            'cities',
            'warehouses'
        ));
    }

    /**
     * Obtiene las bodegas por ciudad.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesAjax()
    {
        $cityId = Input::get('cityId');
        $warehouses = Warehouse::where('city_id', $cityId)->get();

        try {
            if (count($warehouses) < 1) {
                throw new MerqueoException('No se encontraron bodegas de esta ciudad.');
            }
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 400);
        }

        return Response::json($warehouses, 200);
    }

    /**
     * Consulta el listado de picking del dia
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOrderBoard()
    {
        try {
            $input = Input::all();
            $validator = \Validator::make($input, [
                'warehouseId' => 'required|exists:warehouses,id'
            ]);
            if ($validator->fails()) {
                return Response::json($validator->errors()->all(), 422);
            }
            $warehouseId = Input::get('warehouseId');
            $status = Input::get('status');
            $orders = \Routes::with([
                'orders' => function ($query) use ($status) {
                    $query->where(DB::raw('DATE(delivery_date)'), Carbon::now()->format('Y-m-d'))
                        ->where('status', '<>', 'Cancelled');
                    if ($status) {
                        $query->where('status', $status);
                    }
                    $query->orderBy('planning_sequence');
                },
                'orders.pickerDry',
                'orders.pickerCold'
            ])->whereHas('orders', function ($query) use ($status) {
                $query->where(DB::raw('DATE(delivery_date)'), Carbon::now()->format('Y-m-d'))
                    ->where('status', '<>', 'Cancelled');
                if ($status) {
                    $query->where('status', $status);
                }
            })->where('warehouse_id', $warehouseId)
                ->where('status', '<>', 'Cancelada')
                ->orderBy(DB::raw("position( shift in 'AM,PM,MD')"))
                ->orderBy('picking_priority')
                ->paginate(3);


            return Response::json($orders, 200);
        } catch (MerqueoException $e) {
            return Response::json($e->getMessage(), 422);
        }
    }
}
