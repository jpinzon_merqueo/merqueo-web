<?php

namespace admin\inventory\picking;

use admin\AdminController;
use City;
use DB;
use ErrorLog;
use Event;
use exceptions\MerqueoException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Product;
use Redirect;
use Response;
use Store;
use Warehouse;
use WarehouseStorage;
use WarehouseStorageEventHandler;

class TransferPickingWarehouseController extends AdminController
{
    /**
     * Muestra inicio del módulo.
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $title = 'Traslado de productos de picking entre bodegas';
        $cities = City::getMainCities();
        $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->get();

        return View::make('admin.inventory.transfer_picking_warehouse.index', compact(
            'title',
            'cities',
            'warehouses'
        ));
    }

    /**
     * Obtiene las bodegas por ciudad.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehousesAjax()
    {
        $cityId = Input::get('cityId');
        $warehouses = Warehouse::where('city_id', $cityId)->get();

        if (count($warehouses) < 1) {
            return Response::json('No se encontraron bodegas de esta ciudad.', 422);
        }

        return Response::json($warehouses, 200);
    }

    /**
     * Obtiene la informacion del producto de las bodegas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductInfo()
    {
        $fromWarehouseId = Input::get('fromwarehouseId');
        $toWarehouseId = Input::get('towarehouseId');
        $product = Input::get('product');

        $fromproducts = \StoreProduct::with([
            'storeProductWarehouses' => function ($query) use ($fromWarehouseId) {
                $query->where('warehouse_id', $fromWarehouseId)->with('warehouse');
            },
            'warehouseStorages' => function ($query) use ($fromWarehouseId) {
                $query->where('warehouse_id', $fromWarehouseId);
            },
            'product'
        ])->whereHas('product', function ($query) use ($product) {
            $query->where('reference', $product);
        })->whereHas('storeProductWarehouses', function ($query) use ($fromWarehouseId) {
            $query->where('warehouse_id', $fromWarehouseId);
        })->first();

        if (count($fromproducts) < 1) {
            return Response::json('No se encontro el producto en bodega origen.', 422);
        }
        $toproducts = \StoreProduct::with([
            'storeProductWarehouses' => function ($query) use ($toWarehouseId) {
                $query->where('warehouse_id', $toWarehouseId)->with('warehouse');
            },
            'warehouseStorages' => function ($query) use ($toWarehouseId) {
                $query->where('warehouse_id', $toWarehouseId);
            },
            'product'
        ])->whereHas('product', function ($query) use ($product) {
            $query->where('reference', $product);
        })->whereHas('storeProductWarehouses', function ($query) use ($toWarehouseId) {
            $query->where('warehouse_id', $toWarehouseId);
        })->first();

        if (count($toproducts) < 1) {
            return Response::json('No se encontro el producto en bodega destino.', 422);
        }

        return Response::json(['from' => $fromproducts, 'to' => $toproducts], 200);
    }

    /**
     * Realiza el traslado entre posiciones entre bodegas.
     * @return \Illuminate\Http\JsonResponse
     */
    public function transferProductsPicking()
    {
        $validator = \Validator::make(Input::all(), [
            'fromWarehouseId' => 'required|exists:warehouses,id',
            'toWarehouseId' => 'required|exists:warehouses,id',
            'fromStoreProductWarehouseId' => 'required|exists:store_product_warehouses,id',
            'toStoreProductWarehouseId' => 'required|exists:store_product_warehouses,id',
            'quantity' => 'required',
        ]);
        if ($validator->fails()) {
            return Response::json($validator->errors()->all(), 400);
        }

        try {
            DB::beginTransaction();
            $fromWarehouseId = Input::get('fromWarehouseId');
            $toWarehouseId = Input::get('toWarehouseId');
            $fromStoreProductWarehouseId = Input::get('fromStoreProductWarehouseId');
            $toStoreProductWarehouseId = Input::get('toStoreProductWarehouseId');
            $quantity = Input::get('quantity');

            $fromStoreProductWarehouse = \StoreProductWarehouse::with('storeProduct')
                ->lockForUpdate()
                ->findOrFail($fromStoreProductWarehouseId);
            $toStoreProductWarehouse = \StoreProductWarehouse::with('storeProduct')
                ->lockForUpdate()
                ->findOrFail($toStoreProductWarehouseId);
            $isOriginBlocked = Warehouse::validateBlockedPositions(
                $fromWarehouseId,
                'Alistamiento',
                $fromStoreProductWarehouse->storeProduct->storage,
                $fromStoreProductWarehouse->storage_height_position,
                $fromStoreProductWarehouse->storage_position
            );
            if ($isOriginBlocked) {
                throw new MerqueoException('La posición de origen está bloqueada por conteo.');
            }

            $isDestinyBlocked = Warehouse::validateBlockedPositions(
                $toWarehouseId,
                'Alistamiento',
                'Alistamiento',
                $toStoreProductWarehouse->storeProduct->storage,
                $toStoreProductWarehouse->storage_height_position,
                $toStoreProductWarehouse->storage_position
            );
            if ($isDestinyBlocked) {
                throw new MerqueoException('La posición de destino está bloqueada por conteo.');
            }

            if ($fromStoreProductWarehouse->picking_stock < $quantity) {
                throw new MerqueoException('El producto de origen contiene menos cantidades de las que se van a trasladar.');
            }

            $productStockUpdate = new \ProductStockUpdate();
            $productStockUpdate->admin_id = Session::get('admin_id');
            $productStockUpdate->warehouse_id = $fromWarehouseId;
            $productStockUpdate->store_product_id = $fromStoreProductWarehouse->store_product_id;
            $productStockUpdate->quantity = $quantity;
            $productStockUpdate->quantity_stock_before = $fromStoreProductWarehouse->picking_stock;
            $productStockUpdate->quantity_stock_after = $fromStoreProductWarehouse->picking_stock - $quantity;
            $productStockUpdate->type = 'Disminución de unidades';
            $productStockUpdate->reason = 'Traslado de picking entre bodegas';
            $productStockUpdate->save();

            $fromStoreProductWarehouse->picking_stock -= $quantity;
            $fromStoreProductWarehouse->save();


            $productStockUpdate = new \ProductStockUpdate();
            $productStockUpdate->admin_id = Session::get('admin_id');
            $productStockUpdate->warehouse_id = $toWarehouseId;
            $productStockUpdate->store_product_id = $toStoreProductWarehouse->store_product_id;
            $productStockUpdate->quantity = $quantity;
            $productStockUpdate->quantity_stock_before = $toStoreProductWarehouse->picking_stock;
            $productStockUpdate->quantity_stock_after = $toStoreProductWarehouse->picking_stock + $quantity;
            $productStockUpdate->type = 'Aumento de unidades';
            $productStockUpdate->reason = 'Traslado de picking entre bodegas';
            $productStockUpdate->save();

            $toStoreProductWarehouse->picking_stock += $quantity;
            $toStoreProductWarehouse->save();

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                return Response::json('Otro proceso se está ejecutando sobre estos productos, 
            espera un momento para volver a intentarlo.', 400);
            }
            ErrorLog::add($e);
            return Response::json($e->getMessage(), 400);
        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json($e->getMessage(), 400);
        }
        return Response::json('Se ha generado el traslado correctamente.', 200);
    }
}
