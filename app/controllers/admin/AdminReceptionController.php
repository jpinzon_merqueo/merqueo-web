<?php

namespace admin;

use ErrorLog;
use Illuminate\Database\QueryException;
use Request;
use View;
use Input;
use Redirect;
use Session;
use Carbon\Carbon;
use Response;
use Provider;
use ProviderOrderReception;
use Event;
use ProviderOrderEventHandler;
use ReceptionEventHandler;
use ProviderOrderReceptionDetailProductGroup;
use Admin;
use ProviderOrderReceptionValidate;

class AdminReceptionController extends AdminController
{
    private $admin_name;

    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new ProviderOrderEventHandler);
        Event::subscribe(new ReceptionEventHandler);
        if (Session::has('admin_id')) {
            $this->admin_name = Admin::find(Session::get('admin_id'))->fullname;
        }
    }

    /**
     * Grilla de recibos de bodega
     */
    public function index()
    {
        $providers = Provider::where('status', 1)->orderBy('name')->get();

        $data = [
            'title' => 'Recibo Físico de Bodega',
            'providers' => $providers,
            'cities' => $this->get_cities()
        ];
        return View::make('admin.reception.index', $data);
    }

    /**
     * Funcion para filtrar y/o buscar recibos de bodega
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_receptions_ajax()
    {
        if (Request::ajax()) {
            $search_terms = Input::all();
            $receptions = ProviderOrderReception::with(
                'admin',
                'providerOrder.provider',
                'providerOrder.warehouse.city'
            );

            $receptions = $receptions->whereHas('providerOrder', function ($q) use ($search_terms) {
                $q->where('warehouse_id', '=', Session::get('admin_warehouse_id'));
            });

            $receptions = $receptions->whereHas('providerOrder.warehouse', function ($q) use ($search_terms) {
                $q->where('city_id', '=', $search_terms['city_id']);
            });

            if (!empty($search_terms['provider_id'])) {
                $receptions = $receptions->whereHas('providerOrder.provider', function ($q) use ($search_terms) {
                    $q->where('id', '=', $search_terms['provider_id']);
                });
            }

            if (!empty($search_terms['search_term'])) {
                $receptions = $receptions->where('id', $search_terms['search_term']);
            }

            if (!empty($search_terms['status'])) {
                $receptions = $receptions->whereIn('status', $search_terms['status']);
            }

            if (!empty($search_terms['provider_type'])) {
                $receptions = $receptions->whereHas('providerOrder.provider', function ($q) use ($search_terms) {
                    $q->where('provider_type', '=', $search_terms['provider_type']);
                });
            }

            $receptions = $receptions->orderBy('provider_order_receptions.id', 'desc')->paginate(20);

            $data = [
                'receptions' => $receptions
            ];
            $reception_html = View::make('admin.reception.ajax.reception-grid', $data)->render();

            $links_html = $receptions->links()->render();

            return Response::json([
                'receptions' => $reception_html,
                'receptions_count' => $receptions->count(),
                'receptions_total' => $receptions->getTotal(),
                'links' => $links_html
            ]);
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para mostrar el detalle de un recibo de bodega
     *
     * @param int $reception_id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($reception_id)
    {
        $reception = ProviderOrderReception::with(
            'admin',
            'providerOrder.warehouse.city',
            'providerOrder.provider'
        )->find((int)$reception_id);

        if ($reception) {
            $data = [
                'title' => 'Recibo de Bodega',
                'reception' => $reception
            ];
            return View::make('admin.reception.edit', $data);
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para obtener los productos búscados en el recibo de bodega
     *
     * @param int $reception_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_products_ajax($reception_id)
    {
        $reception = ProviderOrderReception::with(
            'providerOrderReceptionDetail.storeProduct.product',
            'providerOrder.providerOrderDetails.storeProduct'
        )->find((int)$reception_id);

        if ($reception) {
            $search_term = trim(Input::get('s'));
            if (empty($search_term)) {
                $data_json['error'] = true;
                $data_json['message'] = 'Debe ingresar los datos para la búsqueda.';
                return Response::json($data_json);
            }
            // Busca el producto según el dato ingresado (referencia, PLU, nombre del producto)
            // return $this->get_products_html($reception ,$search_term);
            $html = $this->get_products_html($reception, $search_term);

            $data_json['html'] = $html;
            $data_json['success'] = true;
            $data_json['message'] = 'Se han encontrado los productos.';
            return Response::json($data_json);
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para obtener el html del buscador de productos
     * @param  ProviderOrderReception $reception Objeto del recibo
     * @param  String $search_term término de búsqueda
     * @param  Int $product_id Identificador del producto en el recibo
     * @return String Html del producto
     */
    private function get_products_html($reception, $search_term = null, $product_id = null)
    {
        // Busca el producto según el dato ingresado (referencia, PLU, nombre del producto)
        if (!empty($search_term)) {
            $products = $reception->providerOrderReceptionDetail->filter(function ($detail) use ($search_term) {
                return (string)$detail->storeProduct->product->reference == (string)$search_term
                    || (string)$detail->storeProduct->provider_plu == (string)$search_term
                    || str_contains(strtolower($detail->storeProduct->product->name), strtolower($search_term));
            });
        } else {
            $products = $reception->providerOrderReceptionDetail->filter(function ($detail) use ($product_id) {
                return (int)$detail->id == (int)$product_id;
            });
        }

        $orderDetails = $reception->providerOrder->providerOrderDetails;

        // Se obtienen los datos de la orden de compra (Cantidad ordenada,
        // descripción del embalaje, cantidad del embalaje)
        $products = $products->each(function (&$detail) use ($orderDetails, $reception) {
            $product = $orderDetails->filter(function ($od) use ($detail, $reception) {
                return $od->store_product_id == $detail->store_product_id;
            })->first();
            if ($product) {
                $detail->quantity_order = $product->quantity_order;
                $detail->product_name = $product->product_name;
                $detail->pack_description = $product->pack_description;
                $detail->quantity_pack = $product->quantity_pack;
                $detail->store_product_id = $product->store_product_id;
                $detail->handle_expiration_date = $product->storeProduct->handle_expiration_date;
            }
        });

        $section = View::make('admin.reception.ajax.products-grid', ['products' => $products])->render();
        return $section;
    }

    /**
     * Función para obtener la información de los productos tipo proveedor del recibo de bodega
     *
     * @param $reception_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_product_group_ajax($reception_id)
    {
        $reception = ProviderOrderReception::with(
            'providerOrderReceptionDetail.providerOrderReceptionDetailProductGroup',
            'providerOrder.providerOrderDetails.storeProduct'
        )->find((int)$reception_id);

        if ($reception) {
            $product_id = Input::get('product_id');

            $html = $this->get_product_group_html($reception, $product_id);

            $data_json['html'] = $html;
            $data_json['success'] = true;
            $data_json['message'] = 'Se han encontrado los productos.';
            return Response::json($data_json);
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para obtener la grilla de en html de los productos
     *
     * @param $reception
     * @param $product_id
     * @return mixed
     */
    private function get_product_group_html($reception, $product_id)
    {
        $product_reception = $reception->providerOrderReceptionDetail
            ->filter(function ($detail) use ($product_id) {
                return $detail->id == $product_id;
            })->first();

        $product_order = $reception->providerOrder->providerOrderDetails
            ->filter(function ($detail) use ($product_reception) {
                return $detail->store_product_id == $product_reception->store_product_id;
            })->first();

        $products_order_group = $product_order->providerOrderDetailProductGroup;

        $products = $product_reception->providerOrderReceptionDetailProductGroup
            ->each(function (&$groupDetail) use ($products_order_group) {
                $product = $products_order_group->filter(
                    function ($odg) use ($groupDetail) {
                        return $odg->store_product_id == $groupDetail->store_product_id;
                    }
                )->first();

                $groupDetail->quantity_order = $product->quantity_order;
                $groupDetail->product_name = $product->product_name;
                $groupDetail->pack_description = $product->product_quantity . ' ' . $product->product_unit;
                $groupDetail->handle_expiration_date = intval($product->storeProduct->handle_expiration_date);
            });

        $view = View::make('admin.reception.ajax.grouped-products-grid', ['products' => $products])->render();
        return $view;
    }

    /**
     * Función para actualizar las cantidades recibidas de los productos normales
     *
     * @param $reception_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_product_ajax($reception_id)
    {
        $product_id = Input::get('product_id');
        $quantity_received = Input::get('quantity_received');

        $reception = ProviderOrderReception::with([
            'providerOrderReceptionDetail' => function ($query) use ($product_id) {
                $query->where('id', $product_id);
                $query->with('storeProduct.product');
            },
            'providerOrder' => function ($query) {
                $query->with('providerOrderDetails');
                $query->with('provider');
            },
        ])
            ->whereHas('providerOrderReceptionDetail', function ($query) use ($product_id) {
                $query->where('id', $product_id);
            })
            ->find((int)$reception_id);

        if ($reception) {
            $product = $reception->providerOrderReceptionDetail->first();
            if ($quantity_received != 0) {
                $quantity_received += $product->quantity_received;
            }

            if ($reception->providerOrder->provider_id != 17
                && $reception->providerOrder->provider->provider_type != 'Marketplace') {
                // Cuando llegan unidades de más
                if ($quantity_received > $product->quantity_expected) {
                    $difference = ((int)$quantity_received - (int)$product->quantity_expected);
                    $data_json['error'] = true;
                    $data_json['message'] = "<b>{$this->admin_name}</b> debes recibir: <b style='font-size: 1.8rem;'>{$product->quantity_expected}</b> y dejar en <b style='font-size: 1.8rem;'>{$difference}</b>";
                }

                // Cuando llegan unidades menos a las esperadas
                if ($quantity_received < $product->quantity_expected) {
                    $difference = ((int)$product->quantity_expected - (int)$quantity_received);
                }
            }

            $product->quantity_received = $quantity_received;
            if ((int)$product->quantity_expected == (int)$product->quantity_received
                || (int)$product->quantity_expected < (int)$product->quantity_received) {
                $product->status = 'Recibido';
            } elseif ((int)$product->quantity_received == 0) {
                $product->status = 'No recibido';
            } elseif ((int)$product->quantity_expected > (int)$product->quantity_received) {
                $product->status = 'Parcialmente recibido';
            }

            $product->save();
            Event::fire('reception.updatedProductStatus', [
                [
                    'reception_id' => $reception->id,
                    'admin_id' => Session::get('admin_id'),
                    'reception_detail' => $product
                ]
            ]);

            $html = $this->get_products_html($reception, null, $product_id);
            $data_json['handle_expiration_date'] = $product->storeProduct->handle_expiration_date;
            $data_json['html'] = $html;

            if (isset($data_json['error'])) {
                return Response::json($data_json);
            } else {
                $data_json['success'] = true;
                $data_json['message'] = 'Se ha actualizado la cantidad del producto: <b>' . $product->storeProduct->product->name . '</b>';
                return Response::json($data_json);
            }
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para actualizar las cantidades recibidas de los productos agrupados
     * @param  Int $reception_id Identificador del recibo de bodega
     */
    public function update_grouped_product_ajax($reception_id)
    {
        $reception = ProviderOrderReception::with('providerOrderReceptionDetail.providerOrderReceptionDetailProductGroup',
            'providerOrder.providerOrderDetails')->find((int)$reception_id);
        if ($reception) {
            $product_id = Input::get('product_id');
            $quantity_received = Input::get('quantity_received');
            $quantity_expected = Input::get('quantity_expected');

            $product_reception_group_detail = ProviderOrderReceptionDetailProductGroup::with('providerOrderReceptionDetail.providerOrderReception')->find($product_id);
            if ($quantity_received != 0) {
                $quantity_received += $product_reception_group_detail->quantity_received;
            }

            if ($reception->providerOrder->provider_id != 17) {
                // Cuando llegan unidades de más
                if ($quantity_received > $quantity_expected) {
                    $difference = ((int)$quantity_received - (int)$quantity_expected);

                    $data_json['error'] = true;
                    $data_json['message'] = '<b>' . $this->admin_name . '</b> debes recibir: <b style="font-size: 1.8rem;">' . (int)$quantity_expected . '</b> y dejar en <b style="font-size: 1.8rem;">validación: ' . ($difference) . '</b>';
                }

                if ($quantity_received < $quantity_expected) {
                    $difference = ((int)$quantity_expected - (int)$quantity_received);
                }
            }

            $product_reception_group_detail->quantity_received = $quantity_received;
            if ((int)$quantity_expected == (int)$product_reception_group_detail->quantity_received || (int)$product_reception_group_detail->quantity_received > (int)$quantity_expected) {
                $product_reception_group_detail->status = 'Recibido';
            } elseif ((int)$product_reception_group_detail->quantity_received == 0) {
                $product_reception_group_detail->status = 'No recibido';
            } elseif ((int)$quantity_expected > (int)$product_reception_group_detail->quantity_received) {
                $product_reception_group_detail->status = 'Parcialmente recibido';
            }
            $product_reception_group_detail->save();

            Event::fire('reception.updatedGroupedProductStatus', [
                [
                    'reception_id' => $reception->id,
                    'admin_id' => Session::get('admin_id'),
                    'reception_detail' => $product_reception_group_detail
                ]
            ]);
            $product_id = $product_reception_group_detail->providerOrderReceptionDetail->id;
            $product_reception_group_detail->providerOrderReceptionDetail->updateProductGroupedStatus();
            Event::fire('reception.updatedProductStatus', [
                [
                    'reception_id' => $reception->id,
                    'admin_id' => Session::get('admin_id'),
                    'reception_detail' => $product_reception_group_detail->providerOrderReceptionDetail
                ]
            ]);

            $reception = ProviderOrderReception::with('providerOrderReceptionDetail.providerOrderReceptionDetailProductGroup',
                'providerOrder.providerOrderDetails')->find((int)$reception_id);
            $html = $this->get_product_group_html($reception, $product_id);
            $html_single = $this->get_products_html($reception, null, $product_id);

            $data_json['handle_expiration_date'] = $product_reception_group_detail->storeProduct->handle_expiration_date;
            $data_json['html_single'] = $html_single;
            $data_json['html'] = $html;
            if (!isset($data_json['error'])) {
                $data_json['success'] = true;
                $data_json['message'] = 'Se ha actualizado la cantidad del producto: <b>' . $product_reception_group_detail->storeProduct->product->name . '</b>';
            }
            return Response::json($data_json);
        } else {
            \App::abort(404);
        }
    }

    /**
     * Actualiza el estado del recibo de bodega
     * @param $reception_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_reception_status($reception_id)
    {
        $reception = ProviderOrderReception::with(
            'providerOrderReceptionDetail.providerOrderReceptionDetailProductGroup',
            'providerOrder.provider'
        )
            ->find($reception_id);
        if ($reception) {
            $new_status = Input::get('status');
            if ($new_status == 'Recibido') {
                if ($reception->status == 'En proceso' && $reception->status != 'Validación') {
                    if ($reception->providerOrder->provider_id == 17
                        || $reception->providerOrder->provider->provider_type == 'Marketplace') {
                        $response = false;
                    } else {
                        $response = $this->validate_reception($reception);
                    }

                    if ($response) {
                        $reception->status = 'Validación';
                        $reception->save();
                        return Redirect::back()
                            ->with(
                                'error',
                                'Se ha actualizado se encuenta ahora en "Validación", hasta que no se resuelvan los inconvenientes no será posible cambiarlo de estado.'
                            );
                    }
                } elseif ($reception->status != 'En proceso') {
                    return Redirect::back()
                        ->with('error', 'El recibo no tiene el estado adecuado para hacer esta actualización.');
                }

                if ($reception->providerOrder->provider_id != 17
                    && $reception->providerOrder->provider->provider_type != 'Marketplace') {
                    $product_count = $reception->providerOrderReceptionDetail->filter(function ($detail) {
                        return $detail->quantity_expected > 0 && $detail->type == 'Simple';
                    })->count();

                    $product_grouped_count = $reception->providerOrderReceptionDetail->filter(function ($detail) {
                        return $detail->type == 'Proveedor'
                            && $detail->providerOrderReceptionDetailProductGroup->filter(function ($q) {
                                return $q->quantity_expected > 0;
                            })->count();
                    })->count();
                } else {
                    $product_count = $reception->providerOrderReceptionDetail->filter(function ($detail) {
                        return $detail->type == 'Simple';
                    })->count();

                    $product_grouped_count = $reception->providerOrderReceptionDetail->filter(function ($detail) {
                        return $detail->type == 'Proveedor';
                    })->count();
                }

                $received_products = $reception->providerOrderReceptionDetail->filter(function ($detail) {
                    return $detail->status == 'Parcialmente recibido' || $detail->status == 'Recibido';
                })->count();

                $validate_count = $reception->providerOrderReceptionValidate->count();

                $validate_resolve = $reception->providerOrderReceptionValidate->filter(function ($validate) {
                    return !is_null($validate->tipification);
                })->count();

                if ($validate_count != $validate_resolve) {
                    return Redirect::back()->with('error', 'Hay casos con productos que deben ser validados primero.');
                }

                if (($product_count + $product_grouped_count) == $received_products) {
                    try {
                        \DB::beginTransaction();
                        $response = $reception->updateStatus($new_status);
                        \DB::commit();
                    } catch (QueryException $e) {
                        \DB::rollBack();
                        if ($e->getCode() == '40001') {
                            ErrorLog::add($e, 513);
                            return Redirect::back()->with('error', 'Otro proceso se está ejecutando sobre estos 
                            productos, espera un momento para volver a intentarlo.');
                        }
                        ErrorLog::add($e);
                        return Redirect::back()->with('error', $e->getMessage());
                    } catch (\Exception $e) {
                        \DB::rollBack();
                        return Redirect::back()->with('error', $e->getMessage());
                    }
                    if ($response['status']) {
                        return Redirect::back()->with('success', $response['message']);
                    } else {
                        return Redirect::back()->with('error', $response['message']);
                    }
                }

                return Redirect::back()->with('error', 'No se ha actualizado el recibo de bodega.');
            }
            \App::abort(404);
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para guardar sugerencia de cambio de referencia en productos del recibo
     * @param ProviderOrderReception $reception_id [description]
     */
    public function add_suggested_reference_ajax($reception_id)
    {
        $product_id = Input::get('product_id');
        $reference = Input::get('reference');
        $reception = ProviderOrderReception::with('providerOrderReceptionDetail')->find($reception_id);

        if ($reception) {
            $provider_order_reception_details = $reception->providerOrderReceptionDetail->filter(function ($detail) use
            (
                $product_id
            ) {
                return $detail->id == $product_id;
            })->first();

            if ($provider_order_reception_details) {
                $provider_order_reception_validate = new ProviderOrderReceptionValidate;
                $provider_order_reception_validate->reception_id = $reception->id;
                $provider_order_reception_validate->reception_detail_id = $provider_order_reception_details->id;
                $provider_order_reception_validate->product_type = 'Simple';
                $provider_order_reception_validate->type = 'Actualización de referencia';
                $provider_order_reception_validate->product_reference = $reference;
                $provider_order_reception_validate->tipification = 'Error de recibo virtual';
                $provider_order_reception_validate->save();

                return Response::json(['success' => 'Se ha guardado la sugerencia de código de barras.']);
            } else {
                return Response::json(['error' => 'Ocurrió un problema al guardar la sugerencia.']);
                \App::abort(404);
            }
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para guardar referencia desconocida en recibo de bodega
     * @param ProviderOrderReception $reception_id [description]
     */
    public function add_unknow_reference_ajax($reception_id)
    {
        $reception = ProviderOrderReception::find($reception_id);
        if ($reception) {
            $reference = Input::get('reference');

            if ($reference) {
                $provider_order_reception_validate = new ProviderOrderReceptionValidate;
                $provider_order_reception_validate->reception_id = $reception->id;
                $provider_order_reception_validate->product_type = 'Simple';
                $provider_order_reception_validate->type = 'Producto no encontrado';
                $provider_order_reception_validate->product_reference = $reference;
                $provider_order_reception_validate->save();

                return Response::json(['success' => 'Se ha guardado la referencia del producto no encontrado.']);
            } else {
                return Response::json(['error' => 'Ocurrió un problema al guardar la referencia.']);
                \App::abort(404);
            }
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para validar los productos del recibo de bodega después de ser pistoleado
     * @param  ProviderOrderReception $reception
     */
    private function validate_reception(&$reception)
    {
        // Productos simples
        $simple_products = $reception->providerOrderReceptionDetail->filter(function ($q) {
            return $q->type == 'Simple';
        });

        if ($simple_products->count()) {
            $simple_products = $simple_products->each(function (&$q) use ($reception) {
                if ($q->quantity_received > $q->quantity_expected) {
                    $difference = ((int)$q->quantity_received - (int)$q->quantity_expected);
                    $provider_order_reception_validate = new ProviderOrderReceptionValidate;
                    $provider_order_reception_validate->reception_id = $reception->id;
                    $provider_order_reception_validate->product_type = 'Simple';
                    $provider_order_reception_validate->type = 'Unidades adicionales';
                    $provider_order_reception_validate->quantity_difference = $difference;
                    $q->providerOrderReceptionValidate()->save($provider_order_reception_validate);
                    if ($q->quantity_expected != 0) {
                        $q->quantity_received = $q->quantity_expected;
                    }
                    $q->save();
                }

                if ($q->quantity_received < $q->quantity_expected) {
                    $difference = ((int)$q->quantity_expected - (int)$q->quantity_received);
                    $provider_order_reception_validate = new ProviderOrderReceptionValidate;
                    $provider_order_reception_validate->reception_id = $reception->id;
                    $provider_order_reception_validate->product_type = 'Simple';
                    $provider_order_reception_validate->type = 'Unidades faltantes';
                    $provider_order_reception_validate->quantity_difference = $difference;
                    $q->providerOrderReceptionValidate()->save($provider_order_reception_validate);
                }
            });
        }

        $groupe_products = $reception->providerOrderReceptionDetail->filter(function ($q) {
            return $q->type == 'Proveedor';
        });

        if ($groupe_products->count()) {
            $groupe_products = $groupe_products->each(function (&$q) use ($reception) {
                $q->providerOrderReceptionDetailProductGroup->each(function (&$p) use ($reception) {
                    if ($p->quantity_received > $p->quantity_expected) {
                        $difference = ((int)$p->quantity_received - (int)$p->quantity_expected);
                        $provider_order_reception_validate = new ProviderOrderReceptionValidate;
                        $provider_order_reception_validate->reception_id = $reception->id;
                        $provider_order_reception_validate->product_type = 'Proveedor';
                        $provider_order_reception_validate->type = 'Unidades adicionales';
                        $provider_order_reception_validate->quantity_difference = $difference;
                        $provider_order_reception_validate->save();
                        if ($p->quantity_expected != 0) {
                            $p->quantity_received = $p->quantity_expected;
                        }
                        $p->providerOrderReceptionValidate()->save($provider_order_reception_validate);
                    }

                    if ($p->quantity_received < $p->quantity_expected) {
                        $difference = ((int)$p->quantity_expected - (int)$p->quantity_received);
                        $provider_order_reception_validate = new ProviderOrderReceptionValidate;
                        $provider_order_reception_validate->reception_id = $reception->id;
                        $provider_order_reception_validate->product_type = 'Proveedor';
                        $provider_order_reception_validate->type = 'Unidades faltantes';
                        $provider_order_reception_validate->quantity_difference = $difference;
                        $p->providerOrderReceptionValidate()->save($provider_order_reception_validate);
                    }
                });
            });
        }

        $errors = $reception->providerOrderReceptionValidate->count();

        if ($errors) {
            return true;
        }

        return false;
    }

    /**
     * Funcion para actualizar fecha de vencimiento de los productos simples
     * @param  int $reception_id identificador del recibo
     */
    public function update_product_expiration_date_ajax($reception_id)
    {

        $expiration_date = Input::get('expiration_date_day').'/'.Input::get('expiration_date_month').'/'.Input::get('expiration_date_year');
        $expiration_date = Carbon::createFromFormat('d/m/Y', $expiration_date);
        $reception = ProviderOrderReception::with('providerOrderReceptionDetail')->find($reception_id);

        if ($reception) {
            $detail_id = Input::get('detail_id');
            $provider_order_reception_detail = $reception->providerOrderReceptionDetail->filter(function ($detail) use (
                $detail_id
            ) {
                return $detail->id == $detail_id;
            })->first();

            // TODO: se desactiva validacion de fechas de vencimiento
            /*$last_reception = ProviderOrderReceptionDetail::whereHas('providerOrderReception', function($q) {
                    $q->whereIn('status', ['Recibido', 'Almacenado', 'Contabilizado']);
                })
                ->with([
                    'providerOrderReception' => function($q) {
                        $q->whereIn('status', ['Recibido', 'Almacenado', 'Contabilizado']);
                    }
                ])
                ->where('store_product_id', $provider_order_reception_detail->store_product_id)
                ->whereIn('status', ['Recibido', 'Contabilizado', 'Parcialmente recibido'])
                ->orderBy('expiration_date', 'DESC')
                ->first();

            if ($expiration_date->lt($last_reception->expiration_date)) {
                $provider_order_reception_detail->quantity_received = 0;
                $provider_order_reception_detail->status = 'No recibido';
                $provider_order_reception_detail->save();
                return Response::json(['error' => 'La fecha de vencimiento es inferior a la última fecha recibida de este producto.']);
            }*/

            if ($provider_order_reception_detail) {
                $expiration_date = Input::get('expiration_date_day').'/'.Input::get('expiration_date_month').'/'.Input::get('expiration_date_year');
                $expiration_date = Carbon::createFromFormat('d/m/Y', $expiration_date);
                $useful_life_days = $provider_order_reception_detail->storeProduct->useful_life_days;
                $now_date = Carbon::now()->addDays($useful_life_days);
                if ($expiration_date->gte($now_date)) {
                    $expiration_date = $expiration_date->toDateTimeString();
                    $provider_order_reception_detail->expiration_date = $expiration_date;
                    $provider_order_reception_detail->save();

                    return Response::json(['success' => 'Se ha guardado la fecha de vencimiento.']);
                }
                $provider_order_reception_detail->quantity_received = 0;
                $provider_order_reception_detail->status = 'No recibido';
                $provider_order_reception_detail->save();
                return Response::json(['error' => 'La fecha de vencimiento es inferior a los días de vida útil asignados al producto y por lo tanto no se debe recibir este producto.']);
            } else {
                return Response::json(['error' => 'Ocurrió un problema al guardar la fecha de venciemiento.']);
                \App::abort(404);
            }
        } else {
            \App::abort(404);
        }
    }

    /**
     * Funcion para actualizar fecha de vencimiento de los productos agrupados
     * @param  int $reception_id
     */
    public function update_product_grouped_expiration_date_ajax($reception_id)
    {
        $expiration_grouped_date = Input::get('expiration_date_day').'/'.Input::get('expiration_date_month').'/'.Input::get('expiration_date_year');;
        $expiration_grouped_date = Carbon::createFromFormat('d/m/Y', $expiration_grouped_date);
        $reception = ProviderOrderReception::with('providerOrderReceptionDetail')->find($reception_id);

        if ($reception) {
            $grouped_detail_id = Input::get('grouped_detail_id');
            $provider_order_reception_detail_product_group = ProviderOrderReceptionDetailProductGroup::find($grouped_detail_id);

            //TODO: Se desactiva la validación de las fechas de vencimiento
            /*$last_reception = ProviderOrderReceptionDetailProductGroup::whereHas('providerOrderReceptionDetail.providerOrderReception', function($q) {
                $q->whereIn('status', ['Recibido', 'Almacenado', 'Contabilizado']);
            })
                ->with([
                    'providerOrderReceptionDetail.providerOrderReception' => function($q) {
                        $q->whereIn('status', ['Recibido', 'Almacenado', 'Contabilizado']);
                    }
                ])
                ->where('store_product_id', $provider_order_reception_detail_product_group->store_product_id)
                ->whereIn('status', ['Recibido', 'Contabilizado', 'Parcialmente recibido'])
                ->orderBy('expiration_date', 'DESC')
                ->first();

            if (!empty($last_reception) && $expiration_grouped_date->lt($last_reception->expiration_date)) {
                $provider_order_reception_detail_product_group->quantity_received = 0;
                $provider_order_reception_detail_product_group->status = 'No recibido';
                $provider_order_reception_detail_product_group->save();
                $provider_order_reception_detail_product_group->providerOrderReceptionDetail->updateProductGroupedStatus();
                return Response::json(['error' => 'La fecha de vencimiento es inferior a la última fecha recibida de este producto.', 'grouped_detail_id' => $provider_order_reception_detail_product_group->id]);
            }*/

            if ($provider_order_reception_detail_product_group) {
                $useful_life_days = $provider_order_reception_detail_product_group->storeProduct->useful_life_days;
                $now_date = Carbon::now()->addDays($useful_life_days);
                if (!$now_date->gt($expiration_grouped_date)) {
                    $expiration_grouped_date = $expiration_grouped_date->toDateTimeString();
                    $provider_order_reception_detail_product_group->expiration_date = $expiration_grouped_date;
                    $provider_order_reception_detail_product_group->save();

                    return Response::json(['success' => 'Se ha guardado la fecha de vencimiento.']);
                }

                $provider_order_reception_detail_product_group->quantity_received = 0;
                $provider_order_reception_detail_product_group->status = 'No recibido';
                $provider_order_reception_detail_product_group->providerOrderReceptionDetail->updateProductGroupedStatus();
                $provider_order_reception_detail_product_group->save();
                return Response::json(['error' => 'La fecha de vencimiento es inferior a los días de vida útil asignados al producto y por lo tanto no se debe recibir este producto.']);
            } else {
                return Response::json(['error' => 'Ocurrió un problema al guardar la fecha de venciemiento.']);
                \App::abort(404);
            }
        } else {
            \App::abort(404);
        }
    }
}
