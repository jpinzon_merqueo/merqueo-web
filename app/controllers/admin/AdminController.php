<?php

namespace admin;

use Request, Input, Redirect, View, Session, Hash, DB, Admin, Menu, City, RolePermission, MenuItem, AdminLog, Warehouse, BackendAuth;
use Illuminate\Support\Facades\Log;

class AdminController extends \BaseController {

    public $admin_permissions;

    public function __construct()
    {
        parent::__construct();

        $this->beforeFilter(function()
        {
            if (BackendAuth::check()) {
                $admin = BackendAuth::user();
                Log::info("usuario activo en antiguo dashboard {$admin->username}");
            }

            if (!BackendAuth::check() && !Input::has('is_webservice')){
                return Redirect::route('admin.login');
            } else {
                $user = BackendAuth::user();

                $admin_url = getenv('URL_ADMIN') ?: 'admin';
                if (empty($user->status) && Request::path() !== "$admin_url/logout") {
                    return Redirect::action('admin.logout');
                }

                if ($user->update_menu) {
                    $this->menu_tree($user);
                    $user->update_menu = FALSE;
                    $user->save();
                }

                $request = get_current_request();
                $request['controller'] = str_replace('admin\\', '', $request['controller']);
                $admin_permissions = json_decode(Session::get('admin_permissions'), true);
                //debug($admin_permissions);

                $found_index = strpos($request['controller'], 'report\\');
                if (!Input::has('is_webservice'))
                {
                    //validar acceso - controlador
                    if ($found_index === false && $found_index !== 0) {
                        if (!isset($admin_permissions[$request['controller']]))
                            die('Acceso denegado');

                        //validar acceso - action
                        if (is_array($admin_permissions[$request['controller']]['actions'])){
                            if (!in_array($request['action'], $admin_permissions[$request['controller']]['actions']))
                                die('Acceso denegado');
                        }else die('Acceso denegado');
                    }
                }
                if ($found_index === false && $found_index !== 0) {
                    $permissions = $admin_permissions[$request['controller']]['permissions'];
                    //debug($permissions);

                    View::share('admin_permissions', $permissions);
                    $this->admin_permissions = $permissions;
                }

                View::composer('admin.layout', function($view){
                    $view->with('menu', json_decode(Session::get('admin_menu'), true));
                });
            }
        }, array('except' => array('login', 'post_login', 'update_locations_ajax', 'get_warehouses_ajax', 'deleteNotAvailableProducts')));
    }

    /**
     * Login
     */
    public function login()
    {
        if (BackendAuth::check()) {
            return Redirect::action('admin.dashboard');
        }
        $button = !Admin::count() ? 'Create User' : $button = 'Sign me in';
        $error = Input::get('error');
        return View::make('admin.login')->with('title','Login')->with('error',$error)->with('button',$button);
    }

    /**
     * Validar login de usuario
     */
    public function post_login()
    {
        $username = Input::get('username');
        $password = Input::get('password');

        $admin = Admin::where('username', $username)->first();
        if ($admin){
            if (!$admin->status)
                return Redirect::route('admin.login')->with('error', 'Your user is inactive, please contact the Administrator');

            if (Hash::check($password, $admin->password)) {

                BackendAuth::login($admin, TRUE);

                $admin->last_logged_in = date('Y-m-d H:i:s');
                Session::put('admin_id', $admin->id);
                Session::put('admin_username', $admin->username);
                Session::put('admin_name', $admin->fullname);
                Session::put('admin_city_id', $admin->city_id);
                Session::put('admin_warehouse_id', $admin->warehouse_id);
                Session::put('admin_image_url', $admin->image_url);
                Session::put('admin_designation', $admin->designation);
                Session::put('admin_role_id', $admin->role_id);
                Session::put('admin_designation_store_id', $admin->designation_store_id);

                if ( !empty($admin->rules) ) {
                    $admin->is_paused = 1;
                    $admin->is_logged_in = 1;
                    Session::put('admin_rules', $admin->rules);
                }
                if ( !empty($admin->config) ) {
                    Session::put('admin_config', $admin->config);
                }
                $admin->save();
                /*$menu = $admin->designation == 'Call Center' ? Menu::find(2) : Menu::find(1);
                $menu_items = json_decode($menu->menu_order, true);
                Session::put('admin_menu', $menu_items);*/

                //cargar menu de usuario y permisos por modulo
                $this->menu_tree($admin);

                return Redirect::route('admin.dashboard');
            }else return Redirect::route('admin.login')->with('error', 'Invalid password');
        }else return Redirect::route('admin.login')->with('error', 'Invalid email');
    }

    /**
     * Construye el arbol del menú para el usuario logueado.
     *
     * @param  \app\models\Admin $admin Objeto Admin del usuario.
     *
     */
    protected function menu_tree($admin) {
        $menu = Menu::find(1);
        $menu = json_decode($menu->menu_order);
        $admin_permissions = json_decode($admin->permissions, true);
        $admin_menu = array();
        $admin_permissions_access['admin']['actions'] = array('index', 'logout');
        $permissions = RolePermission::all();
        foreach($menu as $menu_item)
        {
            if (array_key_exists($menu_item->id, $admin_permissions))
            {
                $admin_menu[$menu_item->id] = $menu_item;
                $menu_item_data = MenuItem::where('status', 1)
                ->find($menu_item->id);
                
                foreach($permissions as $permission){
                    if ($admin_permissions[$menu_item->id][$permission->name] && !empty($menu_item_data->{$permission->name.'_actions'})){
                        $controller = explode('@', $menu_item_data->controller_action);
                        $controller = strtolower(str_replace('Controller', '', str_replace('admin\\', '', $controller[0])));
                        $data = explode(',', $menu_item_data->{$permission->name.'_actions'});
                        foreach($data as $action)
                            $admin_permissions_access[$controller]['actions'][] = $action;
                    }
                    $admin_permissions_access[$controller]['permissions'][$permission->name] = $admin_permissions[$menu_item->id][$permission->name];
                }
            }
            if (isset($menu_item->children))
            {
                foreach($menu_item->children as $menu_item_children)
                {
                    if (array_key_exists($menu_item_children->id, $admin_permissions))
                    {
                        $menu_item_data = MenuItem::where('status', 1)
                        ->whereIn('title',[])
                        ->find($menu_item_children->id);
                        foreach($permissions as $permission){
                            if ($admin_permissions[$menu_item_children->id][$permission->name] && !empty($menu_item_data->{$permission->name.'_actions'})){
                                if (!array_key_exists($menu_item->id, $admin_menu)){
                                    $menu_item_tmp = $menu_item;
                                    unset($menu_item_tmp->children);
                                    $admin_menu[$menu_item->id] = $menu_item_tmp;
                                }
                                if (!array_key_exists($menu_item_children->id, $admin_menu[$menu_item->id])){
                                    $admin_menu[$menu_item->id]->children[$menu_item_children->id] = $menu_item_children;
                                }
                                $controller = explode('@', $menu_item_data->controller_action);
                                $controller = strtolower(str_replace('Controller', '', str_replace('admin\\', '', $controller[0])));
                                $data = explode(',', $menu_item_data->{$permission->name.'_actions'});
                                foreach($data as $action)
                                    $admin_permissions_access[$controller]['actions'][] = $action;
                            }
                            $admin_permissions_access[$controller]['permissions'][$permission->name] = $admin_permissions[$menu_item_children->id][$permission->name];
                        }
                    }
                }
            }
        }
        //eliminar valores duplicados
        foreach ($admin_permissions_access as $controller => $data){
            $admin_permissions_access[$controller]['actions'] = array_unique($admin_permissions_access[$controller]['actions']);
        }

        Session::put('admin_menu', json_encode($admin_menu));
        Session::put('admin_permissions', json_encode($admin_permissions_access));
    }

    /** radicacion@afiansa.com 51500 colpatria 0502183591
     * Cambiar clave
     */
    public function password()
    {
        return View::make('admin.password')->with('title','Change Password');
    }

    /**
     * Guardar clave
     */
    public function save_password()
    {
        $admin = Admin::find(Session::get('admin_id'));
        $password = Hash::make(Input::get('password'));
        $admin->password = $password;
        $admin->save();
        return Redirect::route('admin.password')->with('type', 'success')->with('message', 'Successfully updated the password');
    }

    /**
     * Cerrar sesion
     */
    public function logout()
    {
        $admin = Admin::find(Session::get('admin_id'));
        if ( !empty($admin->rules) ) {
            $admin->is_paused = 1;
            $admin->is_logged_in = 0;
            $admin->save();
        }
        $all = Session::all();
        $admin_keys = preg_filter('/^admin_(.*)/', '$1', array_keys($all));
        foreach ($admin_keys as $key) {
            $keys[] = $key;
            Session::forget('admin_' . $key);
        }
        BackendAuth::logout();
        return Redirect::route('admin.login');
    }

    /**
     * Dashboard
     */
    public function dashboard()
    {
        if (Session::get('admin_role_id') == 1) {
            $total_sale = DB::table('orders')->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->where('source', '<>', 'Reclamo')->where('status', '!=', 'Cancelled')
                ->sum(DB::raw('(orders.total_amount + orders.delivery_amount - orders.discount_amount)'));
            $total_orders = DB::table('orders')->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->where('source', '<>', 'Reclamo')->where('status', '<>', 'Cancelled')->count();
            $total_users = DB::table('users')->where('status', 1)->count();
            $total_products = DB::table('products')->join('store_products', 'products.id', '=', 'store_products.product_id')->join('stores', 'store_products.store_id', '=', 'stores.id')
                ->where('stores.status', 1)->where('is_hidden', 0)->count();
            $total_stores = DB::table('stores')->where('is_hidden', 0)->where('status', 1)->count();
            $total_cities = DB::table('cities')->where('is_main', 1)->count();
            $total_orders_current_day = DB::table('orders')->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->where('source', '<>', 'Reclamo')->where('status', '<>', 'Cancelled')
                ->whereBetween('orders.created_at', array(date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')))->count();

            return View::make('admin.dashboard')
                ->with('title', 'Dashboard')
                ->with('total_sale', $total_sale)
                ->with('total_products', $total_products)
                ->with('total_users', $total_users)
                ->with('total_orders', $total_orders)
                ->with('total_stores', $total_stores)
                ->with('total_cities', $total_cities)
                ->with('total_orders_current_day', $total_orders_current_day);
        }else{
            return View::make('admin.dashboard')
                ->with('title', 'Dashboard');
        }
    }

    /**
     * Registra log de admin
     *
     * @param string $table Nombre de la tabla
     * @param int $id ID del registro
     * @param string $action Tipo de accion
     */
    public function admin_log($table, $id, $action)
    {
        $admin_log = new AdminLog;
        $admin_log->table = $table;
        $admin_log->row_id = $id;
        $admin_log->action = $action;
        $admin_log->admin_id = Session::has('admin_id') ? Session::get('admin_id') : 133;
        $admin_log->ip = get_ip();
        $admin_log->save();
    }

    /**
     * Obtener ciudades por usuario
     *
     * @param string $surrounded_cities obtener ciudades aledañas y asociadas
     * @return array
     */
    public function get_cities($surrounded_cities = false)
    {
        if (Session::get('admin_designation') == 'Super Admin')
            $cities = City::join('stores', 'cities.id', '=', 'city_id')->where('stores.status', 1)->select('cities.id', 'cities.city')->groupBy('cities.id')->get();
        else
            $cities = City::join('stores', 'cities.id', '=', 'city_id')->where('city_id', Session::get('admin_city_id'))
                            ->where('stores.status', 1)->select('cities.id', 'cities.city')->groupBy('cities.id')->get();

        if ($surrounded_cities){
            foreach ($cities as $city){
                //ciudades aledañas
                $surrounded_cities = City::where('parent_city_id', $city->id)->where('status', 1)->select('id', 'city')->get();
                if (count($surrounded_cities)){
                    foreach ($surrounded_cities as $key => $value) {
                        $cities[] = $value;
                    }
                }
                //ciudades por tienda
                $surrounded_cities = City::where('coverage_store_id', $city->id)->whereNull('parent_city_id')->where('status', 1)->select('id', 'city')->get();
                if (count($surrounded_cities)){
                    foreach ($surrounded_cities as $key => $value) {
                        $cities[] = $value;
                    }
                }
            }
        }

        return $cities;
    }

    /**
     * Obtener bodegas por usuario
     *
     * @return array
     */
    public function get_warehouses($city_id = null)
    {
        if (Session::get('admin_designation') == 'Super Admin'){
            $warehouses = $cities = Warehouse::where('city_id', Session::get('admin_city_id'))->select('warehouses.id', 'warehouses.warehouse')->get();
        }else
            $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->where('id', Session::get('admin_warehouse_id'))->select('warehouses.id', 'warehouses.warehouse')->get();

        return $warehouses;
    }

    /**
     * Obtener bodegas por ciudad
     *
     * @return array
     */
    public function get_warehouses_ajax()
    {
        //$data = [];
        if (Session::get('admin_designation') == 'Super Admin'){
            $warehouses = $cities = Warehouse::where('city_id', Input::get('city_id'))->lists('warehouse','id');
        }else{
            $warehouses = Warehouse::where('city_id', Input::get('city_id'))->where('id', Session::get('admin_warehouse_id'))->lists('warehouse','id');
        }
        /*foreach($warehouses as $warehouse){
            $data[$warehouse->id] = $warehouse->warehouse;
        }*/
        return $warehouses;
    }

    /**
     * Obtener los ids de todas las ciudades aledañas o padres
     *
     * @param int $city_id ID de la ciudad
     * @return array
     */
    public function get_all_city_ids($city_id)
    {
        $city_ids= [];
        $city = City::find($city_id);
        if (!empty($city->parent_city_id))
            $city_ids[] = $city->parent_city_id;

        $cities = City::where('parent_city_id', $city_id)->where('status', 1)->select('id')->get();
        if (count($cities))
            foreach ($cities as $obj)
                $city_ids[] = $obj->id;

        $cities = City::where('coverage_store_id', $city->coverage_store_id)
                        ->whereNotIn('id', $city_ids)
                        ->where('status', 1)
                        ->select('id')
                        ->get();
        if (count($cities))
            foreach ($cities as $obj)
                $city_ids[] = $obj->id;

        return $city_ids;
    }
}
