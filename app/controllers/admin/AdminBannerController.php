<?php
namespace admin;

use function foo\func;
use Request, View, Input, Route, Redirect, Session, DB, Config, Response, Validator,
    Store, City, Department, Shelf, Banner, BannerStore, File, StoreProduct, BannerLog,
    BannerStoreProduct, Carbon\Carbon, Warehouse;

use BannerDepartment, BannerWarehouse;

class AdminBannerController extends AdminController
{
    /**
     * Listado de banners
     */
    public function index()
    {
        $city = City::find(Session::get('admin_city_id'));

        if (!Request::ajax()) {
            $stores = Store::getActiveStores($city->id);
            $data = [
                'title'  => 'Administrador de Banners',
                'stores' => $stores,
                'cities' => $this->get_cities()
            ];

            return View::make('admin.banners.index', $data);
        }
        else {
            $department_id = Input::get('department_id');
            $city_id = Input::get('city_id');
            $store_id = Input::get('store_id');
            $search = Input::has('search') ? Input::get('search') : '';
            $status = Input::has('status') ? Input::get('status') : 1;
            $location = Input::get('location');

            $banners = Banner::with('departments')
                ->where('banners.title', 'like', '%'.$search.'%')
                ->where('banners.city_id', $city_id)
                ->where('banners.status', $status)
                ->join('stores', 'banners.store_id', '=', 'stores.id')
                ->leftJoin('store_products', 'banners.store_product_id', '=', 'store_products.id')
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->select(
                    'products.image_small_url',
                    'banners.category',
                    'banners.id',
                    'banners.title',
                    'banners.status',
                    'banners.city_id',
                    'banners.store_id',
                    'banners.position',
                    'banners.image_web_url',
                    'banners.image_app_url',
                    'banners.is_home',
                    'banners.position',
                    'stores.name AS store_name'
                );

            if ($store_id) {
                $banners->where('banners.store_id', $store_id);
            }
            if ($department_id) {
                $banners->join('banner_departments', 'banners.id', '=', 'banner_departments.banner_id');
                $banners->where('banner_departments.department_id', $department_id);
            } else {
                $banners->leftJoin('banner_departments', 'banners.id', '=', 'banner_departments.banner_id');
            }
            if ($location == 'home') {
                $banners->where('banners.is_home', 1);
            }

            $banners = $banners
                ->groupBy('banners.id')
                ->orderBy('banners.updated_at', 'DESC')
                ->take(60)
                ->get();

            return Response::json($banners);
        }
    }

    /**
     * Editar banner
     * @param null $id
     * @return
     */
    public function edit($id = null)
    {
        $cities = $this->get_cities();
        if (!$id) {
            $data = [
                'title' => 'Agregar Nuevo Banner',
                'cities' => $cities,
            ];
            return View::make('admin.banners.form', $data);
        } else {
            $banner = Banner::with('storeProduct.product', 'bannerStoreProduct', 'warehouses')->find($id);

            if (!$banner) {
                return Redirect::route('adminBanners.banners')
                    ->with('error', 'No se encontró el banner.');
            }

            $image_product = ($banner->category == 'Producto') ? $banner->storeProduct->product : '';

            $stores = Store::whereCityId($banner->city_id)
                ->whereIsHidden(0)
                ->whereStatus(1)
                ->get();

            $warehouses = Warehouse::where('city_id', $banner->city_id)->select('warehouses.id AS value', 'warehouses.warehouse AS label')->get();

            if (!empty($banner->department_id)) {
                $data['departments'] = $departments = Department::where('store_id', $banner->store_id)->where('status', 1)->orderBy('departments.name', 'ASC')->get();
            }

            $store_products = StoreProduct::with([
                'product' => function ($query) {
                    $query->orderBy('name');
                }
            ])
                ->has('product')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->orderBy('products.name')
                ->where('store_id', $banner->store_id)
                ->select(DB::raw('CONCAT(products.name," - ",products.unit, " ", products.quantity) AS label'), 'store_products.id AS value')
                ->get();

            $store_product_id = null;
            if (!empty($banner->store_product_id)) {
                $store_product_id = StoreProduct::with([
                    'product' => function ($query) {
                        $query->orderBy('name');
                    }
                ])
                    ->has('product')
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->orderBy('products.name')
                    ->where('store_id', $banner->store_id)
                    ->where('store_products.id', $banner->store_product_id)
                    ->select(DB::raw('CONCAT(products.name," - ",products.unit, " ", products.quantity) AS label'), 'store_products.id AS value')
                    ->first();
            }

            $related_products = null;
            if ($banner->bannerStoreProduct->count()) {
                $related_products = $banner->bannerStoreProducts()
                    ->with('product')
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->orderBy('banner_store_product.id')
                    ->where('store_id', $banner->store_id)
                    ->select(DB::raw('CONCAT(products.name," - ",products.unit, products.quantity) AS label'), 'store_products.id AS value')
                    ->get();
            }

            $data['title'] = 'Editar Banner';
            $data['cities'] = $cities;
            $data['city_id'] = !empty($banner->city_id) ? $banner->city_id : '';
            $data['banner'] = $banner;
            $data['stores'] = $stores;
            $data['image_product'] = $image_product;
            $data['store_products'] = $store_products;
            $data['store_product_id'] = $store_product_id;
            $data['related_products'] = $related_products;
            $data['warehouses'] = $warehouses;
            $selected_warehouses_temp = $banner->warehouses()->get()->toArray();
            $selected_warehouses = [];
            foreach ($selected_warehouses_temp as $index => $selected_warehouse) {
                $selected_warehouses[] = [
                  'value' => $selected_warehouse['id'],
                  'label' => $selected_warehouse['warehouse']
                ];
            }
            $data['selected_warehouses'] = json_encode($selected_warehouses);

            $departments_added = $banner->departments()->get();
            $departments_location = [];
            if ($departments_added->count()) {
                $departments_added_tmp = [];
                $not_in = [];
                foreach ($departments_added as $index => $department) {
                    $departments_added_tmp[$index]['id'] = $department->id;
                    $departments_added_tmp[$index]['name'] = $department->name;
                    $departments_added_tmp[$index]['position'] = $department->pivot->position;
                    $not_in[] = $department->id;
                }
                $departments_added = $departments_added_tmp;
                $departments_location = Department::where('store_id', $banner->store_id)
                    ->where('status', 1)
                    ->whereNotIn('id', $not_in)
                    ->orderBy('departments.name', 'ASC')
                    ->select(
                        'id',
                        'name',
                        DB::raw('0 AS position')
                    )
                    ->get();
            }
            $data['departments_added'] = $departments_added;
            $data['departments_location'] = $departments_location;

            if (!empty($banner->deeplink_city_id)) {
                $data['stores_deeplink'] = Store::where('city_id', $banner['deeplink_city_id'])->where('status', 1)->where('is_hidden', 0)->get();
            }
            if (!empty($banner['deeplink_store_id'])) {
                $data['departments_deeplink'] = Department::where('store_id', $banner['deeplink_store_id'])->where('status', 1)->orderBy('departments.name', 'ASC')->get();
            }
            if (!empty($banner['deeplink_department_id'])) {
                $data['shelves_deeplinks'] = Shelf::where('department_id', $banner['deeplink_department_id'])->where('status', 1)->orderBy('shelves.name', 'ASC')->get();
            }
            if (!empty($banner['deeplink_shelf_id'])) {
                $data['deeplink_store_products'] = StoreProduct::select('products.name', 'store_products.id')
                    ->join('products', 'products.id', '=', 'store_products.product_id')
                    ->where('store_products.shelf_id', $banner['deeplink_shelf_id'])
                    ->orderBy('products.name', 'ASC')
                    ->get();
            }

            return View::make('admin.banners.form', $data);
        }
    }

    /**
     * Guardar banner
     */
    public function save()
    {
        $title = Input::get('title');
        $content_title = Input::get('content_title', NULL);
        $content_subtitle = Input::get('content_subtitle', NULL);
        $description = Input::get('description', NULL);
        $content = Input::get('content', NULL);
        $is_home = Input::get('is_home');
        $position = Input::get('position');
        $status = Input::get('status');
        $platforms = Input::get('platforms');
        $city_id = Input::get('city_id');
        $store_id = Input::get('store_id');
        $warehouses = Input::get('warehouses');
        $department_id = Input::get('department_id');
        $url = Input::get('url');
        $category = Input::get('category');
        $target = Input::get('target');
        $show_start_date = Carbon::createFromFormat('d/m/Y H:i', substr(Input::get('show_start_date'), 0, 16));
        $show_end_date = Carbon::createFromFormat('d/m/Y H:i', substr(Input::get('show_end_date'), 0, 16));
        $store_product_id = Input::has('store_product_id') ? Input::get('store_product_id') : NULL;
        $store_product_ids = Input::has('store_product_ids') ? Input::get('store_product_ids') : NULL;
        $departments = Input::get('departments');

        // Deeplinks
        $deeplink_type = Input::has('deeplink_type') ? Input::get('deeplink_type') : NULL;
        $deeplink_city_id = Input::has('deeplink_city_id') ? Input::get('deeplink_city_id') : NULL;
        $deeplink_store_id = Input::has('deeplink_store_id') ? Input::get('deeplink_store_id') : NULL;
        $deeplink_department_id = Input::has('deeplink_department_id') ? Input::get('deeplink_department_id') : NULL;
        $deeplink_shelf_id = Input::has('deeplink_shelf_id') ? Input::get('deeplink_shelf_id') : NULL;
        $deeplink_store_product_id = Input::has('deeplink_store_product_id') ? Input::get('deeplink_store_product_id') : NULL;

        if (Input::has('id')) {
            $id = Input::get('id');
            $banner = Banner::with('city', 'store', 'storeProduct', 'bannerStoreProduct', 'warehouses')->find($id);
            $action = 'update';
        } else {
            $banner = new Banner;
            $action = 'insert';
        }

        if ('update' == $action) {
            $banner_store_products = $banner->bannerStoreProduct;
            if ($banner_store_products->count()) {
                $banner->bannerStoreProduct()->delete();
            }
        }

        if ($category == 'Producto') {
            $content_subtitle = null;
            $content = null;
        } else if ($category == 'Informativo') {
            $store_product_id = null;
        } else if ($category == 'Conjunto de productos') {
            $store_product_id = null;
        }

        $banner->title = $title;
        $banner->content_title = empty($content_title) ? NULL : $content_title;
        $banner->content_subtitle = empty($content_subtitle) ? NULL : $content_subtitle;
        $banner->description = empty($description) ? NULL : $description;
        $banner->content = empty($content) ? NULL : $content;
        $banner->category = empty($category) ? NULL : $category;
        $banner->is_home = $is_home;
        $banner->position = $position;
        $banner->status = empty($status) ? 0 : $status;
        $banner->platforms = empty($platforms) ? '' : implode(',', $platforms) ;
        $banner->show_start_date = $show_start_date;
        $banner->show_end_date = $show_end_date;
        $banner->url = empty($url) ? NULL : $url;
        $banner->target = empty($target) ? '_self' : $target;
        $banner->store_product_id = empty($store_product_id) ? NULL : $store_product_id;
        $banner->user_orders_quantity_condition = Input::get('user_orders_quantity_condition');
        $banner->user_orders_quantity = Input::get('user_orders_quantity');
        if ($category == 'Conjunto de productos') {
            $banner->store_product_id = NULL;
        } else {
            $banner->store_product_id = empty($store_product_id) ? NULL : $store_product_id;
        }

        if (Input::has('deeplink') && Input::get('deeplink') == 1) {
            $banner->deeplink_type = $deeplink_type;
            switch ($deeplink_type) {
                case 'Store':
                    $banner->deeplink_city_id = $deeplink_city_id;
                    $banner->deeplink_store_id = $deeplink_store_id;
                    $banner->deeplink_department_id = NULL;
                    $banner->deeplink_shelf_id = NULL;
                    $banner->deeplink_store_product_id = NULL;
                    break;
                case 'Department':
                    $banner->deeplink_city_id = $deeplink_city_id;
                    $banner->deeplink_store_id = $deeplink_store_id;
                    $banner->deeplink_department_id = $deeplink_department_id;
                    $banner->deeplink_shelf_id = NULL;
                    $banner->deeplink_store_product_id = NULL;
                    break;
                case 'Shelf':
                    $banner->deeplink_city_id = $deeplink_city_id;
                    $banner->deeplink_store_id = $deeplink_store_id;
                    $banner->deeplink_department_id = $deeplink_department_id;
                    $banner->deeplink_shelf_id = $deeplink_shelf_id;
                    $banner->deeplink_store_product_id = NULL;
                    break;
                case 'Product':
                    $banner->deeplink_city_id = $deeplink_city_id;
                    $banner->deeplink_store_id = $deeplink_store_id;
                    $banner->deeplink_department_id = $deeplink_department_id;
                    $banner->deeplink_shelf_id = $deeplink_shelf_id;
                    $banner->deeplink_store_product_id = $deeplink_store_product_id;
                    break;
                default:
                    $banner->deeplink_type = NULL;
                    $banner->deeplink_city_id = NULL;
                    $banner->deeplink_store_id = NULL;
                    $banner->deeplink_department_id = NULL;
                    $banner->deeplink_shelf_id = NULL;
                    $banner->deeplink_store_product_id = NULL;
                    break;
            }
        } else {
            $banner->deeplink_type = NULL;
            $banner->deeplink_city_id = NULL;
            $banner->deeplink_store_id = NULL;
            $banner->deeplink_department_id = NULL;
            $banner->deeplink_shelf_id = NULL;
            $banner->deeplink_store_product_id = NULL;
        }

        if (empty($city_id)) {
            $banner->city_id = NULL;
        } else {
            $banner->city_id = $city_id;
        }
        if (empty($store_id)) {
            $banner->store_id = null;
        } else {
            $banner->store_id = $store_id;
        }

        if (empty($department_id)) {
            $banner->department_id = null;
        } else {
            $banner->department_id = $department_id;
        }

        $images = ['image_web_url', 'image_web_url_temp', 'image_app_url', 'image_app_modal_url'];

        foreach ($images as $image) {
            if (!Input::hasFile($image)) {
                continue;
            }

            $result = $this->saveBannerImage($banner, Input::file($image), $image);

            if ($result){
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');
            }
        }

        $store = Store::with('city')
            ->find($banner->store_id);
        $banner->save();
        if ($category == 'Conjunto de productos' && count($store_product_ids)) {
            foreach ($store_product_ids as $value) {
                $banner_store_product = new BannerStoreProduct;
                $banner_store_product->banner_id = $banner->id;
                $banner_store_product->store_product_id = $value;
                $banner_store_product->save();
            }
            $banner->save();
        }

        if ($departments) {
            $departments_sync = [];
            foreach ($departments as $index => $department) {
                $departments_sync[$index]['position'] = $department;
            }
            $banner->departments()->sync($departments_sync);
        } else {
            $banner->departments()->detach();
        }

        if (!empty($warehouses)) {
            $banner->warehouses()->sync($warehouses);
        }
        $this->admin_log('banners', $banner->id, $action);

        if ($category == 'Informativo' || $category  == 'Conjunto de productos') {
            $url_store = route('frontStore.all_departments', [
                'city_slug'       => $store->city->slug,
                'store_slug'      => $store->slug,
            ]);

            $url_store  = str_replace(Config::get('app.url'), 'https://merqueo.com', $url_store);
            $deeplink_url =  \FirebaseClient::generateDynamicLink([
                'url'           => $url_store,
                'type'          => 'banner',
                'banner_id'     => $banner->id,
                'store_id'      => $banner->store_id
            ]);

            $leanplum_url = 'mrq://merqueo?type=banner&store_id='.$banner->store_id.'&banner_id='.$banner->id;
            $banner->deeplink_url = $deeplink_url->shortLink;
            $banner->leanplum_url = $leanplum_url;
        } else {
            $banner->deeplink_url = null;
            $banner->leanplum_url = null;
        }
        $banner->save();
        return Redirect::back()->with('success', 'Banner guardado con éxito');
    }

    /**
     * Almacena la url de la imagen en el campo del banner seleccionado.
     *
     * @param Banner $banner
     * @param $image
     * @param $fieldName
     * @return \Illuminate\Http\RedirectResponse|null
     */
    private function saveBannerImage(Banner $banner, $image, $fieldName)
    {
        $validator = Validator::make(
            ['ima' => $image],
            ['ima' => 'required|mimes:jpeg,bmp,png,gif']
        );

        if ($validator->fails()) {
            return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');
        }

        $cloudfront_url = Config::get('app.aws.cloudfront_url');

        if (!empty($banner->{$fieldName})) {
            $path = str_replace($cloudfront_url, uploads_path(), $banner->{$fieldName});
            remove_file($path);
            remove_file_s3($banner->{$fieldName});
        }

        $banner->{$fieldName} = upload_image($image, false, 'banners/web');

        return null;
    }

    /**
     * Eliminar banner
     */
    public function delete($id)
    {
        $banner = Banner::find((int)$id);
        if (empty($banner))
            return Redirect::route('adminBanners.banners')->with('error', 'No se encontró el banner.');

        $cloudfront_url = Config::get('app.aws.cloudfront_url');
        if (!empty($banner->image_web_url)) {
            $path = str_replace($cloudfront_url, uploads_path(), $banner->image_web_url);
            remove_file($path);
            remove_file_s3($banner->image_web_url);
        }
        if (!empty($banner->image_app_url)) {
            $path = str_replace($cloudfront_url, uploads_path(), $banner->image_app_url);
            remove_file($path);
            remove_file_s3($banner->image_app_url);
        }

        if ($banner->category == 'Conjunto de productos' && $banner->bannerStoreProduct->count()) {
            $banner->bannerStoreProduct()->delete();
        }

        $banner->delete();
        $this->admin_log('banners', $banner->id, 'delete');

        return Redirect::route('adminBanners.banners')->with('success', 'Banner eliminado con éxito.');
    }

    /**
     * Obtener tiendas por ciudad ajax
     */
    public function get_stores_ajax()
    {
        $stores = array();
        $associated_store_products = array();
        $message = 'Error al cargar';
        $status = 400;
        if (Input::has('city_id')) {
            $stores = Store::where('city_id', Input::get('city_id'))
                ->where('status', 1)
                ->where('is_hidden', 0)
                ->select('id', 'name')
                ->get();

            $message = 'Tiendas';
            $status = 200;
        }
        return Response::json($stores);
    }

    public function get_products_ajax()
    {
        $products = array();
        $message = 'Error al cargar';
        $status = 400;
        if (Input::has('store_id')) {
            $products = StoreProduct::where('store_id', Input::get('store_id'));
            if (Input::has('category') && Input::get('category') == 'En promoción') {
                //$products = $products->whereNotNull('special_price');
            }
            $products = $products->join('products', 'store_products.product_id', '=', 'products.id')
//                ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit')
                ->select(DB::raw('CONCAT(products.name," - ", products.quantity, " ", products.unit) AS label'), 'store_products.id AS value')
                ->orderBy('products.name')
                ->groupBy('store_products.product_id')
                ->get()
                ->toArray();
            $message = 'Productos';
            $status = 200;
        }
        return Response::json(['result' => ['products' => $products], 'status' => $status, 'message' => $message], 200);
    }

    public function get_logs_ajax($banner_id)
    {
        $banner = Banner::findOrFail($banner_id);
        $banner_logs = BannerLog::getFormattedLogs($banner->id);

        $data = [
            'banner_logs' => $banner_logs
        ];
        return View::make('admin.banners.form', $data)
            ->renderSections()['logs'];
    }


    public function duplicate($id)
    {
        $images = ['image_web_url', 'image_web_url_temp', 'image_app_url', 'image_app_url'];
        $banner = Banner::find($id);
        $banner_store_products = BannerStoreProduct::where('banner_id', '=', $id)->get();
        $banner_warehouses = BannerWarehouse::where('banner_id', '=', $id)->get();
        $banner_departments = BannerDepartment::where('banner_id', '=', $id)->get();

        $new_banner = $banner->replicate();

        foreach ($images as $image) {
            // duplicar y subir imagen del banner web
            if (!empty($banner->{$image})) {
                $info_img_web = pathinfo($banner->{$image});
                $contents_img_web = file_get_contents($banner->{$image});
                $file_img_web['client_original_extension'] = $info_img_web['extension'];
                $new_banner->{$image} = upload_image($file_img_web, false, 'banners/web', $contents_img_web);
            }
        }

        $new_banner->status = 0;
        $new_banner->save();

        // nuevos campos, dynamic link y deeplink
        if ($new_banner->category == 'Conjunto de productos' || $new_banner->category == 'Informativo') {
            $store = Store::with('city')->find($new_banner->store_id);
            $url_store = route('frontStore.all_departments', [
                'city_slug'       => $store->city->slug,
                'store_slug'      => $store->slug,
            ]);

            $url_store  = str_replace(Config::get('app.url'), 'https://merqueo.com', $url_store);
            $deeplink_url =  \FirebaseClient::generateDynamicLink([
                'url'           => $url_store,
                'type'          => 'banner',
                'banner_id'     => $new_banner->id,
                'store_id'      => $new_banner->store_id
            ]);

            $leanplum_url = 'mrq://merqueo?type=banner&store_id='.$new_banner->store_id.'&banner_id='.$new_banner->id;

            $new_banner->deeplink_url = $deeplink_url->shortLink;
            $new_banner->leanplum_url = $leanplum_url;
            $new_banner->save();
        }

        
        // duplicar productos del banner
        foreach($banner_store_products as $banner_store_product){
            $new_banner_store_product = $banner_store_product->replicate();
            $new_banner_store_product->banner_id = $new_banner->id;
            $new_banner_store_product->save();
        }

        // duplicar bodegas del banner
        foreach($banner_warehouses as $banner_warehouse){
            $new_banner_warehouse = $banner_warehouse->replicate();
            $new_banner_warehouse->banner_id = $new_banner->id;
            $new_banner_warehouse->save();
        }

        // duplicar departamentos del banner
        foreach($banner_departments as $banner_department){
            $new_banner_department = $banner_department->replicate();
            $new_banner_department->banner_id  = $new_banner->id;
            $new_banner_department->save(); 
        }

        $this->admin_log('banners', $new_banner->id, 'duplicate');

        return Redirect::route('adminBanners.edit', ['id' => $new_banner->id])->with('success', 'Banner duplicado con éxito');

    } 

    public function get_warehouses_ajax()
    {
        $city_id = Input::get('city_id');
        $warehouses = Warehouse::where('city_id', $city_id)->select('warehouses.id AS value', 'warehouses.warehouse AS label')->get();

        return Response::json($warehouses);
    }

    public function get_departments_ajax()
    {
        $store_id = Input::get('store_id');
        $departments = Department::where('store_id', $store_id)
            ->where('status', 1)
            ->orderBy('departments.name', 'ASC')
            ->select(
                'id',
                'name',
                DB::raw('0 AS position')
            )
            ->get();

        return Response::json($departments);
    }
    
}
