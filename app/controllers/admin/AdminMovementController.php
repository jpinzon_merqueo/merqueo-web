<?php

namespace admin;

use Egulias\EmailValidator\Exception\ConsecutiveAt;
use ErrorLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Request;
use View;
use Input;
use Redirect;
use Session;
use DB;
use Carbon\Carbon;
use City;
use Product;
use Response;
use Config;
use AdminLog;
use Provider;
use ProviderOrder;
use Route;
use ProviderOrderDetail;
use Store;
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Writer_Excel2007;
use ProviderOrderReception;
use ProviderOrderReceptionDetail;
use Pdf;
use Transporter;
use Driver;
use Vehicle;
use ProductStockUpdate;
use ProviderOrderDetailProductGroup;
use PHPExcel_Cell;
use PHPExcel_Reader_Excel2007;
use Exception;
use ProductGroup;
use Event;
use ProviderOrderEventHandler;
use ReceptionEventHandler;
use ProviderOrderReceptionLog;
use ProviderTax;
use StoreProduct;
use ProviderOrderReceptionDetailProductGroup;
use ProviderOrderReceptionValidate;

class AdminMovementController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new ProviderOrderEventHandler);
        Event::subscribe(new ReceptionEventHandler);
    }
    /**
     * Grilla de recibos de bodega
     */
    public function index_reception()
    {
        if (Request::ajax()) {
            $search_terms = Input::all();

            $receptions = ProviderOrderReception::with('providerOrder.provider',
                'providerOrder.warehouse',
                'provider') //Esta relación solo se utilizara en caso que contabilidad desee asignar un proveedor diferente a Merqueo S.A.S)
                ->join('admin', 'admin.id', '=', 'provider_order_receptions.admin_id')
                ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
                ->join('warehouses', 'provider_orders.warehouse_id', '=', 'warehouses.id')
                ->join('cities', 'cities.id', '=', 'warehouses.city_id')
                ->leftjoin('provider_order_reception_details', 'provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id')
                ->select(
                    'provider_order_receptions.*',
                    'city',
                    DB::raw('provider_order_receptions.status AS status'),
                    'admin.fullname',
                    'provider_name',
                    DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received')
                );

            $provider_order_id = $provider_order = Route::current()->parameter('order_id') ? Route::current()->parameter('order_id') : 0;
            if ($provider_order) {
                $provider_order = ProviderOrder::find($provider_order);
                $receptions->where('provider_order_receptions.provider_order_id', $provider_order->id);
            }
            if (!empty($search_terms['provider_order_id'])) {
                $provider_order = ProviderOrder::find($provider_order);
                $receptions->where('provider_order_receptions.provider_order_id', $search_terms['provider_order_id']);
            }
            if (!empty($search_terms['provider_id'])) {
                $receptions->where('provider_orders.provider_id', $search_terms['provider_id']);
            }

            if (!empty($search_terms['status'])) {
                $receptions->whereIn('provider_order_receptions.status', $search_terms['status']);
            }
            if (!empty($search_terms['search_term'])) {
                $receptions->where('provider_order_receptions.id', $search_terms['search_term']);
            }

            if (!empty($search_terms['provider_type'])) {
                $receptions = $receptions->whereHas('providerOrder.provider', function ($q) use ($search_terms) {
                    $q->where('provider_type', '=', $search_terms['provider_type']);
                });
            }

            if (!empty($search_terms['warehouse_id'])) {
                $receptions = $receptions->whereHas('providerOrder.warehouse', function ($q) use ($search_terms) {
                    $q->where('id', '=', $search_terms['warehouse_id']);
                });
            }

            if (!empty($search_terms['movement_consecutive_p'])) {
                $receptions->where('provider_order_receptions.movement_consecutive_p', $search_terms['movement_consecutive_p']);
            }

            $receptions = $receptions->where('warehouses.city_id', $search_terms['city_id'])
                                 ->limit(80)->groupBy('provider_order_receptions.id')
                                 ->orderBy('provider_order_receptions.id', 'desc')
                                 ->get();

            $data = [
          'receptions' => $receptions
        ];
            return View::make('admin.providers.movements.receptions', $data)->renderSections()['grid'];
        } else {
            $receptions = ProviderOrderReception::with('providerOrder.provider',
                'provider') //Esta relación solo se utilizara en caso que contabilidad desee asignar un proveedor diferente a Merqueo S.A.S)
                ->join('admin', 'admin.id', '=', 'provider_order_receptions.admin_id')
                ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
                ->join('warehouses', 'provider_orders.warehouse_id', '=', 'warehouses.id')
                ->join('cities', 'cities.id', '=', 'warehouses.city_id')
                ->leftjoin('provider_order_reception_details', 'provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id')
                ->select(
                    'provider_order_receptions.*',
                    'city',
                    DB::raw('provider_order_receptions.status AS status'),
                    'admin.fullname',
                    'provider_name',
                    DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received')
                );

            $provider_order_id = $provider_order = Route::current()->parameter('order_id') ? Route::current()->parameter('order_id') : 0;
            if ($provider_order) {
                $provider_order = ProviderOrder::find($provider_order);
                $receptions->where('provider_order_receptions.provider_order_id', $provider_order->id);
            }

            $receptions = $receptions->where('warehouses.city_id', Session::get('admin_city_id'))
                                 ->limit(80)->groupBy('provider_order_receptions.id')
                                 ->orderBy('provider_order_receptions.id', 'desc')
                                 ->get();

            $providers = Provider::where('status', 1)->orderBy('name')->get();

            $data = [
                'title' => 'Recibos de Bodega',
                'provider_order' => $provider_order,
                'provider_order_id' => $provider_order_id,
                'receptions' => $receptions,
                'providers' => $providers,
                'cities' => $this->get_cities(),
                'warehouses' => $this->get_warehouses(Session::get('admin_city_id'))
            ];
            return View::make('admin.providers.movements.receptions', $data);
        }
    }

    /**
     * Nuevo recibo de bodega
     */
    public function add_reception($order_id)
    {
        $post = Session::has('post') ? Session::get('post') : [];

        return View::make('admin.providers.movements.reception_form')
                ->with('title', 'Recibo de Bodega')
                ->with('sub_title', 'Nuevo Recibo de Bodega')
                ->with('provider_order_id', $order_id)
                ->with('reception', false)
                ->with('post', $post);
    }

    /**
    * Editar recibo de bodega
    */
    public function edit_reception($id)
    {
        $reception = ProviderOrderReception::with(
            'providerOrderReceptionDetail.providerOrderReceptionValidate',
            'providerOrderReceptionDetail.providerOrderReceptionDetailProductGroup.providerOrderReceptionValidate',
            'providerOrder.provider',
            'providerOrder.warehouse',
            'provider' //Esta relación solo se utiliza en caso de que contabilidad desee actualizar el proveedor real
        )->select(
            'provider_order_receptions.*',
            'admin.fullname AS admin_name',
            'provider_orders.provider_name',
            'provider_orders.provider_id',
            'city'
        )
            ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
            ->join('warehouses', 'provider_orders.warehouse_id', '=', 'warehouses.id')
            ->join('cities', 'cities.id', '=', 'warehouses.city_id')
            ->join('admin', 'provider_order_receptions.admin_id', '=', 'admin.id')
            ->where('provider_order_receptions.id', $id)
            ->groupBy('provider_orders.id')
            ->first();

        if (empty($reception)) {
            \App::abort(404);
        }

        $providers = Provider::whereStatus(1)
            ->orderBy('name')
            ->lists('name', 'id');
        $provider = (!$reception->real_provider_id) ? $reception->providerOrder->provider : $reception->provider;
        $taxes = ProviderTax::where('status', 1)->get();
        $ica_taxes = $taxes->filter(function ($tax) {
            return $tax->type == 'ica';
        });
        $fuente_taxes = $taxes->filter(function ($tax) {
            return $tax->type == 'fuente';
        });
        $store_products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
            ->where('provider_id', $reception->provider_id)
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->where('store_product_warehouses.status', 1)->where('type', '<>', 'Agrupado')->orderBy('name', 'ASC')
            ->select('products.name', 'products.quantity', 'products.unit', 'store_products.*')
            ->get();

        // Función para cargar únicamente las entidades anidadas
        $simple_errors = $reception->providerOrderReceptionValidate;

        if (!$reception) {
            return Redirect::route('adminMovements.reception')->with('error', 'No existe el recibo de bodega.');
        }

        $logs = ProviderOrderReceptionLog::where('reception_id', $reception->id)
            ->join('admin', 'provider_order_reception_logs.admin_id', '=', 'admin.id')
            ->select('provider_order_reception_logs.*', 'admin.fullname')
            ->get();

        $data = [
            'title' => 'Recibo de Bodega',
            'sub_title' => 'Editar Recibo de Bodega',
            'provider_order_id' => $id ? $id : 0,
            'provider_order_reject_reasons' => \ProviderOrderRejectReason::where('status', 1)->get(),
            'reception' => $reception,
            'products' => $store_products,
            'providers' => $providers,
            'provider' => $provider,
            'logs' => $logs,
            'ica_taxes' => $ica_taxes,
            'fuente_taxes' => $fuente_taxes,
            'simple_validates' => $simple_errors
        ];

        return View::make('admin.providers.movements.reception_form', $data);
    }

    /**
     * Guardar encabezado de recibo de bodega
     */
    public function save_reception()
    {
        if (!Input::has('provider_order_id')) {
            return Redirect::route('adminMovements.addReception')->with('error', 'Es necesario el número de la orden de compra.')->with('post', Input::all());
        }

        $id = Input::get('provider_order_id');
        $provider_order = ProviderOrder::where('id', $id)->whereIn('provider_orders.status', ['Enviada', 'Pendiente'])->first();

        if (!$provider_order) {
            return Redirect::route('adminMovements.addReception')->with('error', 'No existe la orden de compra o se encuentra en un estado no válido para recibir productos.')->with('post', Input::all());
        } else {
            //validar que no haya recibos de bodega de la orden de compra en proceso
            if (ProviderOrderReception::where('provider_order_id', $id)->where('status', 'En proceso')->first()) {
                return Redirect::route('adminMovements.addReception', ['id' => $provider_order->id])->with('error', 'Hay un recibo de bodega de la orden de compra en proceso.')->with('post', Input::all());
            }

            //validar numero de factura
            if (ProviderOrderReception::where('provider_order_id', $id)->where('invoice_number', Input::get('invoice_number'))->first()) {
                return Redirect::route('adminMovements.addReception', ['id' => $provider_order->id])->with('error', 'Ya existe un recibo de bodega con el mismo número de factura en la misma orden de compra.')->with('post', Input::all());
            }

            //obtener cantidad de recibos de bodega de la orden de compra
            $receptions = ProviderOrderReception::where('provider_order_id', $id)->where('status', '<>', 'En proceso')->count();
            if (!$receptions) {
                //crear detalle con todos los productos
                $products = ProviderOrderDetail::where('provider_order_id', $id)->get();
            } else {
                //crear detalle solo con productos no recibidos o parcialmente recibidos
                $products = ProviderOrderReceptionDetail::select(
                                                        'provider_order_reception_details.store_product_id',
                                                        'provider_order_details.product_name',
                                                        'provider_order_details.base_cost',
                                                        'provider_order_details.cost',
                                                        'provider_order_details.iva',
                                                        'provider_order_details.iva_amount',
                                                        'provider_order_details.consumption_tax',
                                                        'provider_order_details.old_cost',
                                                        DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received'),
                                                        'provider_order_details.quantity_order',
                                                        'provider_order_details.id',
                                                        'provider_order_details.type'
                                                    )
                                                    ->join('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
                                                    ->join('provider_order_details', function ($join) {
                                                        $join->on('provider_order_details.provider_order_id', '=', 'provider_order_receptions.provider_order_id');
                                                        $join->on('provider_order_details.store_product_id', '=', 'provider_order_reception_details.store_product_id');
                                                    })
                                                    ->where('provider_order_receptions.provider_order_id', $id)
                                                    ->whereIn('provider_order_receptions.status', ['Recibido', 'Almacenado', 'Revisado', 'Contabilizado'])
                                                    ->groupBy('provider_order_reception_details.store_product_id')
                                                    ->havingRaw('quantity_received < quantity_order')
                                                    ->get();
            }

            if ($products) {
                DB::beginTransaction();
                //crear encabezado
                $provider_order_reception = new ProviderOrderReception();
                $provider_order_reception->provider_order_id = $id;
                $provider_order_reception->admin_id = Session::get('admin_id');
                $provider_order_reception->date = date('Y-m-d H:i:s');
                $provider_order_reception->status = 'Iniciado';
                $provider_order_reception->transporter = Input::get('transporter');
                $provider_order_reception->plate = Input::get('plate');
                $provider_order_reception->driver_name = Input::get('driver_name');
                $provider_order_reception->driver_document_number = Input::get('driver_document_number');
                $provider_order_reception->invoice_number = Input::get('invoice_number');
                $provider_order_reception->observation = Input::get('observation');
                $provider_order_reception->save();
                //crear detalle
                foreach ($products as $product) {
                    if ($product->quantity_received < $product->quantity_order) {
                        $reception_detail = new ProviderOrderReceptionDetail();
                        $reception_detail->reception_id = $provider_order_reception->id;
                        $reception_detail->store_product_id = $product->store_product_id;
                        $reception_detail->type = $product->type;
                        $reception_detail->status = 'No recibido';
                        $reception_detail->quantity_received = 0;
                        $reception_detail->base_cost = $product->base_cost;
                        $reception_detail->cost = $product->cost;
                        $reception_detail->iva = $product->iva;
                        $reception_detail->iva_amount = $product->iva_amount;
                        $reception_detail->consumption_tax = $product->consumption_tax;
                        $reception_detail->old_cost = $product->old_cost;
                        $reception_detail->save();

                        //crear detalle de productos Proveedor
                        if ($reception_detail->type == 'Proveedor') {
                            $order_detail_groups = ProviderOrderDetailProductGroup::where('provider_order_detail_id', $product->id)->get();
                            foreach ($order_detail_groups as $key => $order_detail_group) {
                                $old_reception_detail_group = ProviderOrderReceptionDetailProductGroup::where('reception_detail_id', $reception_detail->id)->where('store_product_id', $order_detail_group->store_product_id)->first();
                                $reception_detail_group = new ProviderOrderReceptionDetailProductGroup;
                                $reception_detail_group->reception_detail_id = $reception_detail->id;
                                $reception_detail_group->store_product_id = $order_detail_group->store_product_id;
                                $reception_detail_group->status = 'No recibido';
                                $reception_detail_group->quantity_received = 0;
                                $reception_detail_group->cost = $order_detail_group->cost;
                                $reception_detail_group->base_cost = $order_detail_group->base_cost;
                                $reception_detail_group->iva = $order_detail_group->iva;
                                $reception_detail_group->iva_amount = $order_detail_group->iva_amount;
                                $reception_detail_group->consumption_tax = $order_detail_group->consumption_tax;
                                $reception_detail_group->old_cost = $order_detail_group->old_cost;
                                $reception_detail_group->save();
                            }
                        }
                    }
                }
                DB::commit();
                //log
                Event::fire('reception.created', [[
                    'reception_id' => $provider_order_reception->id,
                    'provider_order_id' => $provider_order_reception->provider_order_id,
                    'admin_id' => Session::get('admin_id')
                ]]);
            }
        }


        return Redirect::route('adminMovements.editReception', ['id' => $provider_order_reception->id])->with('success', 'Recibo de bodega generado con éxito.');
    }

    /**
     * Actualiza encabezado de recibo de bodega
     */
    public function update_reception($id)
    {
        //crear encabezado
        if ($provider_order_reception = ProviderOrderReception::find($id)) {
            $provider_order_reception->transporter = Input::get('transporter');
            $provider_order_reception->plate = Input::get('plate');
            $provider_order_reception->driver_name = Input::get('driver_name');
            $provider_order_reception->driver_document_number = Input::get('driver_document_number');
            $provider_order_reception->invoice_number = Input::get('invoice_number');
            $provider_order_reception->observation = Input::get('observation');
            $provider_order_reception->save();

            //log
            Event::fire('reception.updated', [[
                'reception_id' => $provider_order_reception->id,
                'provider_order_id' => $provider_order_reception->provider_order_id,
                'admin_id' => Session::get('admin_id')
            ]]);
        }

        return Redirect::route('adminMovements.editReception', ['id' => $provider_order_reception->id])->with('success', 'Recibo de bodega actualizado con éxito.');
    }

    /**
     * Obtener productos de un recibo de bodega
     */
    public function get_products_ajax($id)
    {
        $reception = ProviderOrderReception::find($id);
        $provider_order = ProviderOrder::with('warehouse.city')->find($reception->provider_order_id);
        $search = Input::get('s');
        $products = $reception->getProducts($search);
        $is_reception_products = 1;
        if (!$products->count()) {
            $is_reception_products = 0;
            $stores = Store::getActiveStores($provider_order->warehouse->city_id);
            $store_ids = $stores->lists('id');
            $products = StoreProduct::searchProductsByProvider($search, $provider_order, $store_ids);
        }
        $total_iva = 0;
        $total_ivas = [];
        $total_consumption_tax = 0;
        $total_cost = $reception->getSubTotal();
        $total = 0;

        if ($total_cost != 0) {
            $total_ivas = $reception->getIvaDetails();

            $total_iva = $reception->getIva();
            $total_consumption_tax = $reception->getConsumptionTax();
            $total = $reception->getTotal();
        }

        $productsCreditNote = $reception->getProducts($search, [], false);
        $total_cost_credit_note = $reception->getSubTotal(false);
        $total_credit_note = 0;
        if ($total_cost_credit_note != 0) {
            $total_credit_note = $reception->getTotal(false);
        }

        $data = [
            'section' => 'products_table_container',
            'is_search' => Input::has('s'),
            'total_cost' => $total_cost,
            'products' => $products,
            'provider_order_id' => $reception->provider_order_id,
            'reception' => $reception,
            'provider_order' => $provider_order,
            'total_ivas' => $total_ivas,
            'total_consumption_tax' => $total_consumption_tax,
            'total_credit_note' => $total_credit_note,
            'productsCreditNote' => $productsCreditNote,
            'total' => $total
        ];
        $view = View::make('admin.providers.movements.reception_form', $data)->renderSections();

        $data_json['html'] = $view['products_table_container'];

        if (!$is_reception_products) {
            if (count($products)) {
                $data_json['success'] = true;
                $data_json['message'] = 'Este producto no hace parte de la orden de compra';
            } else {
                $data_json['error'] = true;
                $data_json['message'] = 'No se encontraron productos.';
            }
        }
        return Response::json($data_json);
    }

    /**
     * Obtener detalle de producto agrupado en recibo de bodega ajax
     */
    public function get_product_group_ajax($id)
    {
        if (!$reception = ProviderOrderReception::find($id)) {
            return Response::json(['status' => false], 400);
        }

        if (Input::has('provider_order_reception_detail_id')) {
            $provider_order_detail_product_group = $reception->getProductGroups((int)Input::get('provider_order_reception_detail_id'));
        }

        $action = Input::get('action');
        $element = reset($provider_order_detail_product_group);
        $element = reset($element);
        $data = [
            'provider_order_detail_product_group' => $element,
            'section' => 'provider_order_detail_product_group',
            'action' => $action,
            'reception' => $reception,
            'id' => $id
        ];

        return View::make('admin.providers.movements.reception_form', $data)->renderSections()['provider_order_detail_product_group'];
    }

    /**
     * Actualiza cantidad completa de unidades recibida de un producto
     */
    public function update_product_reception_ajax($id)
    {
        $response = ['success' => false];

        if ($reception = ProviderOrderReception::where('id', $id)->where('status', 'En proceso')->first()) {
            $provider_order_detail = ProviderOrderDetail::select('provider_order_details.*', DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received'))
                                                 ->join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
                                                 ->join('provider_order_receptions', 'provider_order_receptions.provider_order_id', '=', 'provider_orders.id')
                                                 ->leftJoin(DB::raw('(provider_order_receptions AS provider_order_receptions_aux, provider_order_reception_details)'), function ($join) {
                                                     $join->on('provider_order_receptions_aux.provider_order_id', '=', 'provider_orders.id');
                                                     $join->on('provider_order_receptions_aux.status', '=', DB::raw('"Recibido"'));
                                                     $join->on('provider_order_reception_details.reception_id', '=', 'provider_order_receptions_aux.id');
                                                     $join->on('provider_order_reception_details.store_product_id', '=', 'provider_order_details.store_product_id');
                                                     $join->on('provider_order_reception_details.status', '=', DB::raw('"Parcialmente recibido"'));
                                                     $join->on('provider_order_reception_details.reception_id', '<>', 'provider_order_receptions.id');
                                                 })
                                                 ->where('provider_order_receptions.provider_order_id', $reception->provider_order_id)
                                                 ->where('provider_order_details.store_product_id', Input::get('store_product_id'))
                                                 ->whereIn('provider_orders.status', ['Enviada', 'Pendiente'])
                                                 ->groupBy('provider_order_details.store_product_id')
                                                 ->first();

            if ($provider_order_detail) {
                $reception_detail = ProviderOrderReceptionDetail::with('storeProduct')
                    ->where('reception_id', $reception->id)
                    ->where('store_product_id', Input::get('store_product_id'))
                    ->first();

                if (!$reception_detail) {
                    $response = ['success' => false, 'message' => 'El detalle del producto no se encuentra en el recibo.'];
                    return Response::json($response, 200);
                }

                $quantity_order = 0;

                //validar si el producto esta parcialmente recibido solo guardar lo que falta
                if ($provider_order_detail->quantity_received) {
                    $quantity_order = $provider_order_detail->quantity_order - $provider_order_detail->quantity_received;
                }

                if ($quantity_order == Input::get('quantity_received')) {
                    $reception_detail->quantity_received = Input::get('quantity_received');
                    $reception_detail->status = 'Recibido';
                }

                if ($quantity_order > Input::get('quantity_received')) {
                    $reception_detail->quantity_received = Input::get('quantity_received');
                    $reception_detail->status = 'Parcialmente recibido';
                }

                if (Input::get('quantity_received') == 0) {
                    $reception_detail->quantity_received = Input::get('quantity_received');
                    $reception_detail->status = 'No recibido';
                }

                if ($quantity_order < Input::get('quantity_received')) {
                    if ((int)Input::get('quantity_received') >= 6000) {
                        $response = ['success' => false, 'message' => 'La cantidad ingresada es superior a seis mil unidades, no es posible ejecutar esta acción con esta cantidad.'];
                        return Response::json($response, 200);
                    }
                    $reception_detail->quantity_received = Input::get('quantity_received');
                    $reception_detail->status = 'Recibido';
                }
                if (Input::has('expiration_date')) {
                    $expiration_date = Carbon::createFromFormat('d/m/Y', Input::get('expiration_date'));
                    $useful_life_days = $reception_detail->storeProduct->useful_life_days;
                    $now_date = Carbon::now()->addDays($useful_life_days);
                    if ($now_date->gt($expiration_date)) {
                        $reception_detail->quantity_received = 0;
                        $reception_detail->status = 'No recibido';
                        $reception_detail->save();
                        return Response::json(['message' => 'La fecha de vencimiento es inferior a los días de vida útil asignados al producto y por lo tanto no se debe recibir este producto.']);
                    }
                    $expiration_date = $expiration_date->toDateTimeString();
                    $reception_detail->expiration_date = $expiration_date;
                }
                // $reception_detail->quantity_received = Input::get('status') == 'No recibido' ? 0 : $provider_order_detail->quantity_order;
                $reception_detail->save();

                //log
                Event::fire('reception.updatedProductStatus', [[
                    'reception_detail' => $reception_detail,
                    'admin_id' => Session::get('admin_id'),
                    'reception_id' => $id
                ]]);

                //log de recibido
                $log = new AdminLog();
                $log->table = 'provider_order_reception_details';
                $log->row_id = $reception_detail->id;
                $log->action = 'update';
                $log->admin_id = Session::get('admin_id');
                $log->save();
                $response = ['success' => true, 'status' => $reception_detail->status, 'quantity_received' => $reception_detail->quantity_received, 'message' => 'El producto ha sido actualizado'];
            } else {
                if (Input::has('store_product_id') && Input::has('quantity_received')) {
                    $provider_order = ProviderOrder::find($reception->provider_order_id)->whereIn('provider_orders.status', ['Enviada', 'Pendiente'])->first();
                    if ($provider_order) {
                        Input::merge(['cant' => Input::get('quantity_received')]);
                        $response = $this->reception_add_product_ajax($reception->id, $provider_order->provider_id);
                        return $response;
                    } else {
                        $response['message'] = 'La orden de compra no esta pendiente o no existe.';
                    }
                } else {
                    $response['message'] = 'La orden de compra no esta pendiente o no existe.';
                }
            }
        } else {
            $response['message'] = 'El recibo de bodega no esta en proceso o no existe.';
        }

        return Response::json($response, 200);
    }

    /**
     * Actualiza costo, iva e impuesto al consumo del producto
     */
    public function update_product_cost($id)
    {
        try {
            DB::beginTransaction();
            $reception = ProviderOrderReception::with([
                'admin',
                'providerOrder' => function ($q) {
                    $q->with('provider');
                    $q->with('warehouse.city');
                },
                'providerOrderReceptionDetail.providerOrderReceptionDetailProductGroup'
            ])
                ->findOrFail($id);

            $providerOrderDetail = ProviderOrderDetail::select('provider_order_details.*')
                ->join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
                ->where('provider_order_id', $reception->provider_order_id)
                ->where('store_product_id', Input::get('store_product_id'))
                ->first();

            if (empty($providerOrderDetail) && Input::has('store_product_id')) {
                DB::rollBack();
                return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                    ->with('error', 'No existe el producto como detalle de la órden de compra.');
            }

            $providerOrderReceptionDetail = ProviderOrderReceptionDetail::with(
                'storeProduct.storeProductWarehouses'
            )
                ->where('store_product_id',Input::get('store_product_id'))
                ->where('reception_id', $reception->id)
                ->first();

            // si se recibe el id para cambiar por nuevo producto.
            if (Input::has('provider_product')) {
                $this->change_product(Input::get('provider_product'));
            }

            if ($providerOrderDetail && $providerOrderDetail->type == 'Simple') {
                $cost = Input::get('cost');
                $cost = str_replace(',', '.', $cost);
                $cost = (float)$cost;
                if (Input::has('iva') || Input::has('consumption_tax')
                    || Input::has('quantity_expected') || Input::has('quantity_received')) {
                    if (empty($providerOrderReceptionDetail)) {
                        DB::rollBack();
                        return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                            ->with('error', 'No existe el producto como detalle del recibo.');
                    }

                    if ($reception->status == 'En proceso') {
                        DB::rollBack();
                        return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                            ->with('error', 'El estado del recibo de bodega no es valido.');
                    }

                    if (Input::has('iva')) {
                        $iva = Input::get('iva');
                        if ($iva == '' || $iva > 100 || $iva < 0) {
                            DB::rollBack();
                            return Redirect::route('adminMovements.editReception',['id' => $reception->id])
                                ->with('error', 'El IVA del producto no es valido.');
                        }

                        $providerOrderReceptionDetail->iva = $iva;
                    }

                    if (Input::has('consumption_tax')) {
                        $consumptionTax = Input::get('consumption_tax');
                        if ($consumptionTax == '') {
                            DB::rollBack();
                            return Redirect::route('adminMovements.editReception',
                                ['id' => $reception->id])->with('error', 'El impuesto al consumo no es valido.');
                        }
                        $consumptionTax = str_replace(',', '.', $consumptionTax);
                        $providerOrderReceptionDetail->consumption_tax = $consumptionTax;
                    }

                    // Se guarda el cambio de  las cantidades en recibo
                    if (Input::has('quantity_expected')) {
                        $quantityExpected = Input::get('quantity_expected');
                        $providerOrderReceptionDetail->quantity_expected = $quantityExpected;
                    }

                    if (Input::has('quantity_received')) {
                        $quantityReceived = Input::get('quantity_received');
                    }

                    if ($quantityReceived < 0) {
                        DB::rollBack();
                        return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                            ->with('error', 'La cantidad a recibir no puede ser menor a cero.');
                    }

                    // Si se actualiza la cantidad recibida, se descuenta o se suma al inventario.
                    if ((int)$providerOrderReceptionDetail->quantity_received != (int)$quantityReceived
                        && $reception->providerOrder->provider->provider_type == 'Merqueo'
                        && $reception->status != 'Validación') {
                        $quantityReceivedBefore = $providerOrderReceptionDetail->quantity_received;
                        $quantityReceivedAfter = $quantityReceived;
                    }
                    // Se guarda el cambio de las cantidades recibidas
                    $providerOrderReceptionDetail->quantity_received = $quantityReceived;

                    //Se actualizan los estados del producto dependiendo de las cantidades recibidas
                    if ($providerOrderReceptionDetail->quantity_received == 0) {
                        $providerOrderReceptionDetail->status = 'No recibido';
                    } elseif ($providerOrderReceptionDetail->quantity_received
                        < $providerOrderDetail->quantity_order) {
                        $providerOrderReceptionDetail->status = 'Parcialmente recibido';
                    } elseif ($providerOrderReceptionDetail->quantity_received
                        >= $providerOrderDetail->quantity_order) {
                        $providerOrderReceptionDetail->status = 'Recibido';
                    }

                    $providerOrderReceptionDetail->is_given = Input::get('is_given');
                    if ($providerOrderReceptionDetail->is_given == 1) {
                        $providerOrderReceptionDetail->given_quantity = Input::get('given_quantity');
                    } else {
                        $providerOrderReceptionDetail->given_quantity = 0;
                    }

                    $providerOrderReceptionDetail->save();
                    $reception->updateTaxes();

                    if (isset($quantityReceivedBefore) && isset($quantityReceivedAfter)) {
                        $quantityReceived = [
                            'quantity_received_before' => $quantityReceivedBefore,
                            'quantity_received_after' => $quantityReceivedAfter
                        ];
                    }

                    Event::fire('reception.updatedFullProductCost', [
                        [
                            'admin_id' => Session::get('admin_id'),
                            'reception_id' => $reception->id,
                            'store_product_id' => Input::get('store_product_id'),
                            'cost' => $cost,
                            'quantity_received' => $quantityReceived
                        ]
                    ]);
                } else {
                    Event::fire('reception.updatedProductCost', [
                        [
                            'admin_id' => Session::get('admin_id'),
                            'reception_id' => $reception->id,
                            'store_product_id' => Input::get('store_product_id'),
                            'cost' => $cost
                        ]
                    ]);
                }

                $providerOrderReceptionDetail->base_cost = $cost;
                $providerOrderReceptionDetail->cost = ($cost * (1 + ($providerOrderReceptionDetail->iva / 100)));
                $providerOrderReceptionDetail->save();
                DB::commit();
                return Redirect::route('adminMovements.editReception', ['id' => $reception->id])->with('success',
                    'Producto actualizado con éxito.');
            } elseif (Input::has('provider_order_details_id') && Input::has('provider_order_detail_product_group_id')
                && Input::has('provider_order_reception_detail_product_group_id')
                && Input::has('provider_order_reception_detail_group_id')) {
                $providerOrderDetailsId = Input::get('provider_order_details_id');
                $providerOrderDetailProductGroupId = Input::get('provider_order_detail_product_group_id');
                $providerOrderReceptionDetailProductGroupId = Input::get('provider_order_reception_detail_product_group_id');

                $cost = Input::get('cost');
                $cost = str_replace(',', '.', $cost);

                $providerOrderDetail = ProviderOrderDetail::find($providerOrderDetailsId);
                if (!$providerOrderDetail) {
                    DB::rollBack();
                    return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                        ->with(
                            'error',
                            'El producto no se encuentra en los detalles de la orden de compra.'
                        );
                }

                $providerOrderDetailProductGroup = ProviderOrderDetailProductGroup::find($providerOrderDetailProductGroupId);
                if (!$providerOrderDetailProductGroup) {
                    DB::rollBack();
                    return Redirect::route('adminMovements.editReception', ['id' => $reception->id])->with(
                        'error',
                        'El producto no se encuentra en los detalles de producto agrupado de la orden de compra.'
                    );
                }

                $providerOrderReceptionDetailProductGroup = ProviderOrderReceptionDetailProductGroup::with(
                    'storeProduct.storeProductWarehouses'
                )
                    ->find($providerOrderReceptionDetailProductGroupId);

                if (!$providerOrderReceptionDetailProductGroup) {
                    DB::rollBack();
                    return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                        ->with(
                            'error',
                            'El producto no se encuentra en los detalles de producto agrupado del recibo de bodega.'
                        );
                }

                if (!$providerOrderReceptionDetail = ProviderOrderReceptionDetail::find(
                    Input::get('provider_order_reception_detail_group_id')
                )) {
                    DB::rollBack();
                    return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                        ->with(
                            'error',
                            'El producto no existe en el recibo de bodega.'
                        );
                }

                if ($reception->status == 'En proceso') {
                    DB::rollBack();
                    return Redirect::route('adminMovements.editReception', ['id' => $reception->id])->with(
                        'error',
                        'El estado del recibo de bodega no es valido.'
                    );
                }

                if (Input::has('iva')) {
                    $iva = Input::get('iva');
                    if ($iva === '' || $iva > 100 || $iva < 0) {
                        DB::rollBack();
                        return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                            ->with(
                                'error',
                                'El IVA del producto no es valido.'
                            );
                    }
                    $providerOrderReceptionDetailProductGroup->iva = $iva;
                }

                if (Input::has('consumption_tax')) {
                    $consumptionTax = Input::get('consumption_tax');
                    if ($consumptionTax == '') {
                        DB::rollBack();
                        return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                            ->with(
                                'error',
                                'El impuesto al consumo no es valido.'
                            );
                    }
                    $consumptionTax = str_replace(',', '.', $consumptionTax);
                    $providerOrderReceptionDetailProductGroup->consumption_tax = $consumptionTax;
                }

                if (Input::has('quantity_expected')) {
                    $quantityExpected = Input::get('quantity_expected');
                    $providerOrderReceptionDetailProductGroup->quantity_expected = $quantityExpected;
                }

                $quantityReceived = Input::get('quantity_received');
                if (Input::has('quantity_received')) {
                    // Si se actualiza la cantidad recibida, se descuenta o se suma al inventario.
                    if ((int)$providerOrderReceptionDetailProductGroup->quantity_received
                        != (int)$quantityReceived && $reception->providerOrder->provider->provider_type == 'Merqueo') {
                        if ((int)$providerOrderReceptionDetailProductGroup->quantity_received
                            < (int)$quantityReceived
                            && (int)$providerOrderReceptionDetailProductGroup->quantity_received != 0) {
                            $providerOrderDetailProductGroup->quantity_order = $quantityReceived;
                            $providerOrderDetailProductGroup->save();
                        }
                        $quantityReceivedBefore = $providerOrderReceptionDetailProductGroup->quantity_received;
                        $quantityReceivedAfter = $quantityReceived;
                    }

                    $providerOrderReceptionDetailProductGroup->quantity_received = $quantityReceived;

                    //Se actualizan los estados del producto dependiendo de las cantidades recibidas
                    if ($providerOrderReceptionDetailProductGroup->quantity_received == 0) {
                        $providerOrderReceptionDetailProductGroup->status = 'No recibido';
                    } elseif ($providerOrderReceptionDetailProductGroup->quantity_received
                        < $providerOrderDetailProductGroup->quantity_order) {
                        $providerOrderReceptionDetailProductGroup->status = 'Parcialmente recibido';
                    } elseif ($providerOrderReceptionDetailProductGroup->quantity_received
                        >= $providerOrderDetailProductGroup->quantity_order) {
                        $providerOrderReceptionDetailProductGroup->status = 'Recibido';
                    }
                }

                $providerOrderReceptionDetailProductGroup->save();
                $reception->updateTaxes();
                $providerOrderReceptionDetail->updateProductGroupedStatus();

                if (isset($quantityReceivedBefore) && isset($quantityReceivedAfter)) {
                    $quantityReceived = [
                        'quantity_received_before' => $quantityReceivedBefore,
                        'quantity_received_after' => $quantityReceivedAfter
                    ];
                    Event::fire('reception.updatedFullGroupedProductCost', [
                        [
                            'admin_id' => Session::get('admin_id'),
                            'reception_id' => $reception->id,
                            'store_product_id' => Input::get('provider_order_reception_detail_group_id'),
                            'cost' => $cost,
                            'quantity_received' => $quantityReceived
                        ]
                    ]);
                } else {
                    Event::fire('reception.updatedGroupedProductCost', [
                        [
                            'admin_id' => Session::get('admin_id'),
                            'reception_id' => $reception->id,
                            'store_product_id' => Input::get('provider_order_reception_detail_group_id'),
                            'cost' => $cost
                        ]
                    ]);
                }

                $providerOrderReceptionDetailProductGroup->base_cost = $cost;
                $providerOrderReceptionDetailProductGroup->cost = ($cost * (1 + ($providerOrderReceptionDetailProductGroup->iva / 100)));
                $providerOrderReceptionDetailProductGroup->save();
                DB::commit();
                return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                    ->with(
                        'success',
                        'Producto actualizado con éxito.'
                    );
            } else {
                return Redirect::route('adminMovements.editReception', ['id' => $reception->id])
                    ->with(
                        'error',
                        'No existe el producto en recibo de bodega.'
                    );
            }
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            if ($e->getModel() == 'ProviderOrderReception') {
                return Redirect::route('adminMovements.reception')->with(
                    'error',
                    'No existe el recibo de bodega o no esta en el estado valido.'
                );
            }
        } catch (Exception $e) {
            DB::rollBack();
            return Redirect::route('adminMovements.editReception', ['id' => $id])
                ->with('error', $e->getMessage());
        }
    }

    public function change_product()
    {
        $new_product = Input::get('provider_product');
        $new_product = StoreProduct::find($new_product);
        if ($new_product->type == 'Proveedor') {
            ProviderOrderDetailProductGroup::where('provider_order_detail_id', $provider_order_detail->id)->delete();
            $products = ProductGroup::select('products.*', 'store_products.*', 'product_group_id', 'product_group.quantity AS group_quantity')
                                    ->join('store_products', 'product_group.store_product_group_id', '=', 'store_products.id')
                                    ->join('products', 'store_products.product_id', '=', 'products.id')
                                    ->where('store_product_id', $new_product->id)
                                    ->get();
            foreach ($products as $product_group) {
                $provider_order_detail_product_group = new ProviderOrderDetailProductGroup;
                $provider_order_detail_product_group->provider_order_detail_id = $provider_order_detail->id;
                $provider_order_detail_product_group->store_product_id = $product_group->store_product_group_id;
                $provider_order_detail_product_group->plu = $product_group->provider_plu;
                $provider_order_detail_product_group->product_name = $product_group->name.' '.$product_group->quantity.' '.$product_group->unit;
                $provider_order_detail_product_group->product_quantity = $product_group->quantity;
                $provider_order_detail_product_group->product_unit = $product_group->unit;
                $provider_order_detail_product_group->quantity_order = $product_group->group_quantity;
                $provider_order_detail_product_group->ideal_stock = $product_group->ideal_stock;
                $provider_order_detail_product_group->base_cost = $product_group->base_cost;
                $provider_order_detail_product_group->cost = $product_group->cost;
                $provider_order_detail_product_group->iva = $product_group->iva;
                $provider_order_detail_product_group->image_url = $product_group->image_medium_url;
                $provider_order_detail_product_group->save();
            }
        }
        Event::fire('reception.changedProduct', [[
            'new_product' => $new_product,
            'provider_order_detail' => $provider_order_detail,
            'reception_id' => $reception,
            'admin_id' => Session::get('admin_id'),
        ]]);
        // Si se cambia un producto tipo proveedor, por uno simple, se borran todos los productos que lo componen.
        if ($provider_order_detail->type == 'Proveedor' && $new_product->type == 'Simple') {
            ProviderOrderDetailProductGroup::where('provider_order_detail_id', $provider_order_detail->id)->delete();
        }
        $provider_order_detail->store_product_id = $new_product->id;
        $provider_order_detail->type = $new_product->type;
        $provider_order_detail->plu = $new_product->provider_plu;
        $provider_order_detail->reference = $new_product->reference;
        $provider_order_detail->product_name = $new_product->name;
        $provider_order_detail->ideal_stock = $new_product->ideal_stock;
        $provider_order_detail->pack_description = $new_product->provider_pack_type != 'Unidad' ? '1 '.$new_product->provider_pack_type.' x '.$new_product->provider_pack_quantity.' Unidades' : '1 Unidad';
        $provider_order_detail->base_cost = $new_product->base_cost;
        $provider_order_detail->image_url = $new_product->image_medium_url;
        $provider_order_detail->save();
    }

    /**
    * Actualiza el estado de un recibo de bodega
    */
    public function update_status_reception($id)
    {
        $reception = ProviderOrderReception::with(
            'admin',
            'providerOrder.provider',
            'providerOrder.warehouse.city'
        )
            ->find($id);

        if ($reception) {
            $new_status = Input::get('status');

            try {
                DB::beginTransaction();

                if ($new_status == 'Contabilizado') {
                    $response = $reception->updateStatus($new_status, Input::all());
                } else {
                    if ($new_status == 'Revisado' && $reception->status == 'Contabilizado') {
                        $response = $reception->rollbackToReviewStatus();
                    } else {
                        $response = $reception->updateStatus($new_status);
                    }
                }
                if ($response['status'] == true) {
                    $event_response = Event::fire('reception.updatedStatus', [
                        [
                            'reception_id' => $reception->id,
                            'status' => Input::get('status'),
                            'admin_id' => Session::get('admin_id')
                        ]
                    ]);
                    DB::commit();
                    return Redirect::back()->with('success', $response['message']);
                } else {
                    DB::commit();
                    return Redirect::back()->with('error', $response['message']);
                }
            } catch (QueryException $e) {
                DB::rollBack();
                if ($e->getCode() == '40001') {
                    ErrorLog::add($e, 513);
                    return Redirect::route('adminMovements.editReception', ['id' => $id])
                        ->with(
                            'error',
                            'Otro proceso se está ejecutando sobre estos productos, 
                            espera un momento para volver a intentarlo.'
                        );
                }
                ErrorLog::add($e, 513);
                return Redirect::route('adminMovements.editReception', ['id' => $id])
                    ->with('error', $e->getMessage());
            } catch (\Exception $e) {
                DB::rollback();
                echo '<pre>';
                var_dump($e);
                echo '</pre>';
                exit;
                return Redirect::route('adminMovements.editReception', ['id' => $id])
                    ->with('error', $e->getMessage(). ' '. $e->line);
            }

            return Redirect::route('adminMovements.editReception', ['id' => $id ])
                ->with('success', 'Recibo de bodega actualizado con éxito.');
        }
        return Redirect::route('adminMovements.editReception', ['id' => $id ])
            ->with('error', 'Ocurrió un error al actualizar el recibo de bodega.');
    }

    /**
    * Eliminar recibo de bodega
    */
    public function delete_reception($id)
    {
        if (!$reception = ProviderOrderReception::where('id', $id)->whereIn('status', ['En proceso', 'Iniciado'])->first()) {
            return Redirect::route('adminMovements.reception')->with('error', 'No existe el recibo de bodega.');
        }

        ProviderOrderReceptionDetail::where('reception_id', $id)->delete();
        ProviderOrderReception::where('id', $id)->whereIn('status', ['En proceso', 'Iniciado'])->delete();

        $log = new AdminLog();
        $log->table = 'provider_order_reception';
        $log->row_id = $reception->id;
        $log->action = 'delete';
        $log->admin_id = Session::get('admin_id');
        $log->save();

        return Redirect::route('adminMovements.reception')->with('success', 'Recibo de bodega eliminado con éxito.');
    }

    /**
     * Imprimir pdf de recibo de bodega
     */
    public function order_reception_pdf($id)
    {
        if (!$reception = ProviderOrderReception::find($id)) {
            return Redirect::route('adminMovements.reception', ['id' =>  $id ])->with('error', 'No existe el recibo de bodega.');
        }

        Pdf::generate_order_reception($id);
    }

    /**
     * Actualiza los datos para generar el archivo p de recibos de bodega
     */
    public function update_reception_p_data($id)
    {
        if (!$reception = ProviderOrderReception::find($id)) {
            return Redirect::route('adminMovements.reception', ['id' =>  $id ])->with('error', 'No existe el recibo de bodega.');
        }

        if (empty($reception->expiration_date) && empty($reception->voucher_date)) {
            $reception->expiration_date = Carbon::createFromFormat('d/m/Y', Input::get('reception_expiration_date'))->toDateTimeString();
            $reception->voucher_date = Carbon::createFromFormat('d/m/Y', Input::get('reception_voucher_date'))->toDateTimeString();
            $reception->invoice_number = Input::get('invoice_number');

            $reception->save();
        }
        return $this->order_reception_p_pdf($id);
    }

    /**
     * Imprimir archivo P para contabilidad en recibo de bodega
     */
    public function order_reception_p_pdf($id)
    {
        if (!$order_reception = ProviderOrderReception::find($id)) {
            return Redirect::route('adminMovements.reception', ['id' =>  $id ])->with('error', 'No existe el recibo de bodega.');
        }

        Pdf::generate_order_reception_p($id);
    }

    /**
     * Obtener transportadores por ciudad ajax
     */
    public function get_city_transporters_ajax()
    {
        $transporters = [];
        if (Input::has('city_id')) {
            $transporters = Transporter::select('id', 'fullname')->where('city_id', Input::get('city_id'))->where('status', 1)->orderBy('fullname')->get()->toArray();
        }
        return Response::json($transporters, 200);
    }

    /**
     * Obtener conductores y vehiculos por transportador ajax
     */
    public function get_drivers_vehicles_ajax()
    {
        $data = ['drivers' => [], 'vehicles' => []];
        if (Input::has('transporter_id')){
            $data['drivers'] = Driver::select('id', DB::raw("CONCAT(first_name, ' ', last_name) AS name"))->where('transporter_id', Input::get('transporter_id'))->where('status', 1)->orderBy('first_name')->orderBy('last_name')->get()->toArray();
            $data['vehicles'] = Vehicle::select('vehicles.id', 'vehicles.plate')->leftjoin('drivers', 'drivers.id', '=', 'vehicles.driver_id' )->where('drivers.transporter_id', Input::get('transporter_id'))->where('vehicles.status', 1)->orderBy('plate')->get()->toArray();
        }
        return Response::json($data, 200);
    }

    /**
     * Obtener productos a actualizar stock
     */
    public function search_products()
    {
        if (!Request::ajax()) {
            $products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->join('departments', 'departments.id', '=', 'store_products.department_id')
                        ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->where('store_product_warehouses.status', 1)
                        ->where('products.type', 'Simple')
                        ->select('products.*', 'store_products.*', DB::raw('store_products.id AS store_product_id'), DB::raw('departments.name AS department'))->limit(80)->get();

            return View::make('admin.products.movements.search')
                    ->with('title', 'Actualización de Stock')
                    ->with('cities', $this->get_cities())
                    ->with('products', $products);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';

            //obtener productos a actualizar stock
            $products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->join('departments', 'departments.id', '=', 'store_products.department_id')
                        ->leftJoin('providers', 'providers.id', '=', 'store_products.provider_id')
                        ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                        ->join('cities', 'cities.id', '=', 'stores.city_id')
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->where(function ($query) use ($search) {
                            $query->where('store_products.provider_plu', 'LIKE', '%'.$search.'%');
                            $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                            $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                        })
                        ->where('store_product_warehouses.status', 1)
                        ->where('products.type', 'Simple')
                        ->select('products.*', 'store_products.*', 'cities.city', DB::raw('store_products.id AS store_product_id'), 'providers.name AS provider_name', DB::raw('departments.name AS department'));

            if (Input::get('city_id')) {
                $products->where('cities.id', Input::get('city_id'));
            }

            $products = $products->limit(80)->get();

            return View::make('admin.products.movements.search')
                        ->with('title', 'Actualización de Stock')
                        ->with('products', $products)
                        ->renderSections()['content'];
        }
    }

    /**
    * Actualiza stock de un producto
    */
    public function update_stock_product()
    {
        if (Input::has('store_product_id')) {
            $store_product = StoreProduct::find(Input::get('store_product_id'));

            if ($store_product->product->type == 'Agrupado')
                return Redirect::route('adminMovements.updateProductsStock')->with('error', 'Los productos agrupados no manejan stock.');

            $quantity_stock_before = $store_product->current_stock;
            if (Input::get('type') == 'Aumento de unidades') {
                $store_product->current_stock = $quantity_stock_after = $quantity_stock_before + Input::get('quantity');
            } else {
                $store_product->current_stock = $quantity_stock_after = $quantity_stock_before - Input::get('quantity');
            }
            $store_product->save();

            $product_stock_update = new ProductStockUpdate();
            $product_stock_update->admin_id = Session::get('admin_id');
            $product_stock_update->store_product_id = Input::get('store_product_id');
            $product_stock_update->quantity = Input::get('quantity');
            $product_stock_update->quantity_stock_before = $quantity_stock_before;
            $product_stock_update->quantity_stock_after = $quantity_stock_after;
            $product_stock_update->type = Input::get('type');
            $product_stock_update->reason = Input::get('comments');
            $product_stock_update->save();

            return Redirect::route('adminMovements.updateProductsStock')->with('success', 'Stock de producto actualizado con éxito.');
        } else {
            return Redirect::route('adminMovements.updateProductsStock')->with('error', 'Ocurrió un error al intentar actualizar el stock del producto.');
        }
    }

    /**
    * Obtener listado de actualizaciones de stock
    */
    public function index_update_products_stock()
    {
        if (!Request::ajax()) {
            $products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
                    ->join('stores', 'stores.id', '=', 'store_products.store_id')
                    ->join('departments', 'departments.id', '=', 'store_products.department_id')
                    ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                    ->join('product_stock_updates', 'product_stock_updates.store_product_id', '=', 'store_products.id')
                    ->join('admin', 'admin.id', '=', 'product_stock_updates.admin_id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->where('store_product_warehouses.status', 1)
                    ->select(
                        'products.*',
                        'store_products.*',
                        'store_products.id AS store_product_id',
                        'departments.name AS department',
                        'products.quantity AS product_quantity',
                        'product_stock_updates.quantity',
                        'product_stock_updates.quantity_stock_before',
                        'product_stock_updates.quantity_stock_after',
                        'fullname',
                        DB::raw('DATE_FORMAT(product_stock_updates.created_at, "%d/%m/%Y %H:%i:%s %p") AS date')
                    )
                    ->limit(80)
                    ->orderBy('product_stock_updates.created_at', 'DESC')
                    ->get();

            $stores = Store::where('is_hidden', 0)
                        ->join('cities', 'stores.city_id', '=', 'cities.id')
                        ->where('is_storage', 1)
                        ->select('stores.*', 'cities.city AS city_name')
                        ->get();

            return View::make('admin.products.movements.index')
                    ->with('title', 'Actualizaciones de Stock de Productos')
                    ->with('stores', $stores)
                    ->with('cities', $this->get_cities())
                    ->with('products', $products);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';

            //obtener productos a actualizar stock
            $products = Product::join('store_products', 'store_products.product_id', '=', 'products.id')
                        ->join('providers', 'providers.id', '=', 'store_products.provider_id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->join('departments', 'departments.id', '=', 'store_products.department_id')
                        ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
                        ->join('product_stock_updates', 'product_stock_updates.store_product_id', '=', 'store_products.id')
                        ->join('admin', 'admin.id', '=', 'product_stock_updates.admin_id')
                        ->join('cities', 'cities.id', '=', 'stores.city_id')
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->where(function ($query) use ($search) {
                            $query->where('store_products.provider_plu', 'LIKE', '%'.$search.'%');
                            $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                            $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                        })
                        ->where('store_product_warehouses.status', 1)
                         ->select(
                             'products.*',
                             'store_products.*',
                                 'store_products.id AS store_product_id',
                                 'departments.name AS department',
                                 'products.quantity AS product_quantity',
                                 'product_stock_updates.quantity',
                                 'product_stock_updates.quantity_stock_before',
                                 'product_stock_updates.quantity_stock_after',
                                 'fullname',
                                 'cities.city',
                                 DB::raw('providers.name AS provider_name'),
                                 DB::raw('DATE_FORMAT(product_stock_updates.created_at, "%d/%m/%Y %H:%i:%s %p") AS date')
                         );

            if (Input::get('city_id')) {
                $products->where('cities.id', Input::get('city_id'));
            }

            $products = $products->limit(80)->orderBy('product_stock_updates.created_at', 'DESC')->get();

            return View::make('admin.products.movements.index')
                        ->with('title', 'Actualizaciones de Stock de Productos')
                        ->with('products', $products)
                        ->renderSections()['content'];
        }
    }

    /**
     * Importa archivo de actualización de stock de productos
     */
    public function import_stock_update()
    {
        if (Input::hasFile('file')) {
            ini_set('memory_limit', '512M');
            set_time_limit(0);

            $extension_list = 'xlsx, xls';
            $file = Input::file('file');
            $store_id = Input::get('store_id');
            if (!file_exists($file->getRealPath())) {
                return Redirect::back()->with('error', 'No file uploaded');
            }

            $path = public_path(Config::get('app.download_directory'));

            $extension_obj = explode(',', $extension_list);
            foreach ($extension_obj as $extension) {
                foreach (glob($path.'/*.'.$extension) as $file_tmp) {
                    $file_tmp = pathinfo($file_tmp);
                    $files_by_extension[$extension][] = $file_tmp['filename'];
                }
            }

            //obtener archivo
            $file_path = Input::file('file')->getRealPath();
            $current_dir = opendir($path);

            $objReader = new PHPExcel_Reader_Excel2007();
            $objPHPExcel = $objReader->load($file_path);
            //abrir la ultima hoja que contiene la informacion
            $sheetCount = $objPHPExcel->getSheetCount();
            $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
            //obtener el numero total de filas y columnas
            $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
            $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());
            $error = '';
            $cont = $cont_error = 0;

            DB::beginTransaction();
            for ($i = 2; $i <= $rows; $i++) {
                $reference = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
                $current_stock = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
                $storage_position = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();

                if (empty($reference)) {
                    $cont_error++;
                    $error = 'La fila #'.$i.' no tiene Referencia del producto.<br>';
                    DB::rollback();
                    return Redirect::back()->with('error', $error);
                }
                if (!is_numeric($current_stock) || is_null($current_stock)) {
                    $cont_error++;
                    $error = 'La fila #'.$i.' no tiene stock para el producto.<br>';
                    DB::rollback();
                    return Redirect::back()->with('error', $error);
                }

                /*if ( !is_numeric($storage_position) || is_null($storage_position) ){
                    $cont_error++;
                    $error = 'La fila #'.$i.' no tiene posición para el producto.<br>';
                    DB::rollback();
                    return Redirect::back()->with('error', $error);
                }*/

                if (!$store_product = StoreProduct::where('products.reference', $reference)
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->where('store_products.store_id', $store_id)->where('products.type', 'Simple')->first()) {
                    $cont_error++;
                    $error = 'El producto con referencia <b>'.$reference.'</b> no existe o es un producto agrupado.<br>';
                    DB::rollback();
                    return Redirect::back()->with('error', $error);
                }
                $store_product->current_stock = $current_stock;
                if (is_numeric($storage_position) || !is_null($storage_position)) {
                    $store_product->storage_position = $storage_position;
                }
                $store_product->save();
            }
            $this->admin_log('products', null, 'import_update_stock');
            DB::commit();
            return Redirect::back()->with('success', 'Se actualizaron los stock de los productos importados.');
        }
    }

    /**
     * Buscar productos en recibo de bodega
     */
    public function reception_search_product_ajax($id, $provider_id)
    {
        $search = Input::get('s');
        $provider_order_reception = ProviderOrderReception::find($id);
        $provider_order = ProviderOrder::with('warehouse')->find($provider_order_reception->provider_order_id);
        $provider = Provider::find($provider_id);
        $store_id = 63;
        $products = Product::with('storeProduct')->select(
                        'products.*',
                        'store_products.*',
                        'store_products.id AS store_product_id'
                    )
                    ->join('store_products', 'products.id', '=', 'store_products.product_id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->where('store_product_warehouses.warehouse_id', $provider_order->warehouse_id)
                    ->where(function ($query) use ($search) {
                        $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                        $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                    });
        if (Session::get('admin_id') != 34) {
            if ($provider->type != 'Para faltantes') {
                $products = $products->where('store_products.provider_id', $provider_id);
            }
        }
        if (Session::get('admin_id') != 34) {
            $products = $products->whereNull('store_products.allied_store_id');
            $products = $products->where('store_product_warehouses.discontinued', 0);
        }
        if ($provider_order->warehouse->city_id == 2) {
            $store_id = 64;
        }
        $products = $products->where('products.type', '<>', 'Agrupado')
                    ->where('store_products.store_id', $store_id)
                    ->get();
        $products->each(function (&$product) use ($provider_order) {
            $store_product = StoreProduct::find($product->store_product_id);
            $product->current_stock += $store_product->getCurrentStock($provider_order->warehouse_id);
        });
        $data = [
            'products' => $products,
            'provider_id' => $provider_id,
            'reception_id' => $id,
            'section' => 'product-table'
        ];
        return View::make('admin.providers.movements.reception_form', $data)->renderSections()['product-table'];
    }

    /**
     * Agregar producto a recibo de bodega
     */
    public function reception_add_product_ajax($id, $provider_id)
    {
        $store_product_id = Input::get('store_product_id');
        $cant = (int)Input::get('cant');
        $show_custom_quantity_unit_to_request = false;

        $provider_order_reception = ProviderOrderReception::find($id);
        $provider_order = ProviderOrder::find($provider_order_reception->provider_order_id);
        $provider_order_details = ProviderOrderDetail::where('provider_order_id', $provider_order->id)->where('store_product_id', $store_product_id)->count();

        if (!$provider_order_details) {
            $store_product = StoreProduct::select('products.*', 'store_products.*')->join('products', 'store_products.product_id', '=', 'products.id')->where('store_products.id', $store_product_id)->first();

            $provider_order_detail = new ProviderOrderDetail;
            $provider_order_detail->provider_order_id = $provider_order_reception->provider_order_id;
            $provider_order_detail->store_product_id = $store_product->id;
            $provider_order_detail->type = $store_product->type;
            $provider_order_detail->plu = $store_product->provider_plu;
            $provider_order_detail->reference = $store_product->reference;
            $provider_order_detail->product_name = $store_product->name.' '.$store_product->quantity.' '.$store_product->unit;
            $provider_order_detail->ideal_stock = $store_product->ideal_stock;

            if (!$store_product->provider_pack_quantity_approach) {
                //si el proveedor no ofrece aproximacion de unidades
                if ($cant > $store_product->provider_pack_quantity) {
                    $quantity_pack_to_request = $store_product->provider_pack_quantity ? ceil($cant / $store_product->provider_pack_quantity) : 1;
                    $quantity_pack_to_request = $quantity_pack_to_request;
                    $quantity_unit_to_request = $show_custom_quantity_unit_to_request ? $store_product->quantity_unit_to_request : $quantity_pack_to_request * $store_product->provider_pack_quantity;
                } else {
                    $quantity_pack_to_request = 1;
                    $quantity_unit_to_request = $show_custom_quantity_unit_to_request ? $store_product->quantity_unit_to_request : $store_product->provider_pack_quantity;
                }
            } else {
                //el proveedor si ofrece aproximacion de unidades
                $quantity_pack_to_request = floor($cant / $store_product->provider_pack_quantity);
                $quantity_unit_to_request = $cant;
                $product_quantity_unit = $cant - ($store_product->provider_pack_quantity * floor($cant / $store_product->provider_pack_quantity));
            }

            $provider_order_detail->quantity_order = $cant;
            $provider_order_detail->quantity_pack = $quantity_pack_to_request ? $quantity_pack_to_request : $quantity_unit_to_request;
            $provider_order_detail->pack_description = $store_product->provider_pack_type != 'Unidad' ? '1 '.$store_product->provider_pack_type.' x '.$store_product->provider_pack_quantity.' Unidades' : '1 Unidad';
            $provider_order_detail->base_cost = $store_product->base_cost;
            $provider_order_detail->cost = $store_product->cost;
            $provider_order_detail->iva = $store_product->iva;
            $provider_order_detail->image_url = $store_product->image_medium_url;
            $provider_order_detail->save();

            $provider_order_reception_detail = new ProviderOrderReceptionDetail;
            $provider_order_reception_detail->reception_id = $id;
            $provider_order_reception_detail->store_product_id = $store_product_id;
            $provider_order_reception_detail->status = 'Recibido';
            $provider_order_reception_detail->type = $provider_order_detail->type;
            $provider_order_reception_detail->quantity_received = $provider_order_detail->quantity_order;
            $provider_order_reception_detail->base_cost = $store_product->base_cost;
            $provider_order_reception_detail->cost = $store_product->cost;
            $provider_order_reception_detail->iva = $store_product->iva;
            $provider_order_reception_detail->save();

            //guardar producto agrupado
            if ($store_product->type == 'Proveedor') {
                $store_products_group = ProductGroup::select('products.*', 'product_group_id', 'product_group.quantity AS group_quantity')
                                        ->join('products', 'product_group.product_group_id', '=', 'products.id')
                                        ->where('product_group.product_id', $store_product->id)
                                        ->get();
                foreach ($store_products_group as $store_product_group) {
                    $provider_order_detail_product_group = new ProviderOrderDetailProductGroup;
                    $provider_order_detail_product_group->provider_order_detail_id = $provider_order_detail->id;
                    $provider_order_detail_product_group->store_product_id = $store_product_group->store_product_group_id;
                    $provider_order_detail_product_group->plu = $store_product_group->provider_plu;
                    $provider_order_detail_product_group->product_name = $store_product_group->name.' '.$store_product_group->quantity.' '.$store_product_group->unit;
                    $provider_order_detail_product_group->product_quantity = $store_product_group->quantity;
                    $provider_order_detail_product_group->product_unit = $store_product_group->unit;
                    $provider_order_detail_product_group->quantity_order = $store_product_group->group_quantity;
                    $provider_order_detail_product_group->ideal_stock = $store_product_group->ideal_stock;
                    $provider_order_detail_product_group->base_cost = $store_product_group->base_cost;
                    $provider_order_detail_product_group->cost = $store_product_group->cost;
                    $provider_order_detail_product_group->iva = $store_product_group->iva;
                    $provider_order_detail_product_group->image_url = $store_product_group->image_medium_url;
                    $provider_order_detail_product_group->save();

                    $provider_order_reception_detail_product_group = new ProviderOrderReceptionDetailProductGroup;
                    $provider_order_reception_detail_product_group->reception_detail_id = $provider_order_reception_detail->id;
                    $provider_order_reception_detail_product_group->store_product_id = $provider_order_detail_product_group->store_product_id;
                    $provider_order_reception_detail_product_group->status = 'Recibido';
                    $provider_order_reception_detail_product_group->quantity_received = $provider_order_detail_product_group->quantity_order;
                    $provider_order_reception_detail_product_group->base_cost = $store_product_group->base_cost;
                    $provider_order_reception_detail_product_group->cost = $store_product_group->cost;
                    $provider_order_reception_detail_product_group->iva = $store_product_group->iva;
                    $provider_order_reception_detail_product_group->save();
                    if (in_array($provider_order_reception->status, ['Almacenado', 'Revisado', 'Contabilizado'])) {
                        $new_store_product = StoreProduct::find($provider_order_detail_product_group->store_product_id);
                        $new_store_product->current_stock += $provider_order_detail_product_group->quantity_order;
                        $new_store_product->save();
                    }
                }
            } else {
                /*if ( in_array($provider_order_reception->status, ['Almacenado', 'Revisado', 'Contabilizado']) ) {
                    $store_product->current_stock += $provider_order_reception_detail->quantity_received;
                    $store_product->save();
                }*/
            }

            //log
            Event::fire('reception.addedProduct', [[
                'provider_order_detail' => $provider_order_detail,
                'reception_id' => $id,
                'admin_id' => Session::get('admin_id')
            ]]);

            return Response::json(['status' => true, 'message' => 'El producto se ha agregado exitosamente.']);
        }
        return Response::json(['status' => false, 'message' => 'El producto ya ha sido agregado anteriormente.']);
    }

    /**
     * Función para actualizar el estatus contable de los productos
     * @param int $id del recibo
     */
    public function reception_update_product_accounting_status($id)
    {
        $reception_detail = ProviderOrderReceptionDetail::where('reception_id', $id)->where('store_product_id', (int)Input::get('store_product_id'))->first();
        if ($reception_detail) {
            if ($reception_detail->accounting_status != Input::get('accounting_status')) {
                $reception_detail->accounting_status = Input::get('accounting_status');
                $reception_detail->save();

                //log $reception_detail
                Event::fire('reception.updatedProductAccountingStatus', [[
                            'reception_detail' => $reception_detail,
                            'reception_id' => $id,
                            'admin_id' => Session::get('admin_id')
                        ]]);
            }

            return Response::json(['status' => true, 'message' => 'Se ha actualizado el estado del producto.']);
        }
        return Response::json(['status' => true, 'message' => 'No se ha encontrado el producto.']);
    }

    /**
     * Exportar en excel el plano E del recibo de bodega
     * @param  int $id identificador del recibo de bodega
     */
    public function reception_export_e_plane($id)
    {
        $reception = ProviderOrderReception::find($id);
        $provider_order = ProviderOrder::with('warehouse.city')->find($reception->provider_order_id);
        $provider = Provider::find($provider_order->provider_id);
        $consecutive = DB::table('consecutives')->where('type', 'provider_order_reception_e')->where('city_id', $provider_order->warehouse->city_id)->first();
        $reception->generateConsecutiveE();
        $nit = explode('-', $provider->nit);
        $nit = str_replace('.', '', $nit[0]);

        $reception_details = $reception->getProducts();
        $reception_details = $reception_details->filter(function ($product) {
            return $product->type == 'Simple';
        });
        $grouped_products = $reception->getProductGroupDetails();

        $objPHPExcel = new PHPExcel();
        //propiedades
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setTitle('Orden de Compra');
        $objPHPExcel->getProperties()->setSubject('Orden de Compra');
        $objPHPExcel->setActiveSheetIndex(0);

        //estilos
        $style_header = [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb'=>'99ccff'],
            ],
            'font' => [
                'color' => ['rgb'=>'000000'],
                'bold' => true,
            ]
        ];
        $objPHPExcel->getActiveSheet()->mergeCells('A1:BH1')->getStyle('A1:BH1')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, 'MERQUEO S A S');

        $objPHPExcel->getActiveSheet()->mergeCells('A2:BH2')->getStyle('A2:BH2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 2, 'MODELO PARA LA IMPORTACIÓN DE MOVIMIENTO CONTABLE');

        $objPHPExcel->getActiveSheet()->mergeCells('A3:BH3')->getStyle('A3:BH3')->applyFromArray($style_header);
        $now = Carbon::now()->format('M j/Y');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 3, 'De: '.$now.' A: '.$now);

        $objPHPExcel->getActiveSheet()->mergeCells('A4:BH4');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 4, '');

        //titulos
        $objPHPExcel->getActiveSheet()->getStyle('A5:BH5')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 5, 'TIPO DE COMPROBANTE (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 5, 'CÓDIGO COMPROBANTE  (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, 5, 'NÚMERO DE DOCUMENTO (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, 5, 'CUENTA CONTABLE   (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, 5, 'DÉBITO O CRÉDITO (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, 5, 'VALOR DE LA SECUENCIA   (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, 5, 'AÑO DEL DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, 5, 'MES DEL DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, 5, 'DÍA DEL DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, 5, 'CÓDIGO DEL VENDEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, 5, 'CÓDIGO DE LA CIUDAD');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, 5, 'CÓDIGO DE LA ZONA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, 5, 'SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, 5, 'CENTRO DE COSTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, 5, 'SUBCENTRO DE COSTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, 5, 'NIT');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, 5, 'SUCURSAL');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, 5, 'DESCRIPCIÓN DE LA SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, 5, 'NÚMERO DE CHEQUE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, 5, 'COMPROBANTE ANULADO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, 5, 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, 5, 'FORMA DE PAGO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, 5, 'PORCENTAJE DEL IVA DE LA SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, 5, 'VALOR DE IVA DE LA SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, 5, 'BASE DE RETENCIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, 5, 'BASE PARA CUENTAS MARCADAS COMO RETEIVA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, 5, 'SECUENCIA GRAVADA O EXCENTA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, 5, 'PORCENTAJE AIU');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, 5, 'BASE IVA AIU');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, 5, 'LÍNEA PRODUCTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, 5, 'GRUPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, 5, 'CÓDIGO PRODUCTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, 5, 'CANTIDAD');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, 5, 'CANTIDAD DOS');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, 5, 'CÓDIGO DE LA BODEGA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, 5, 'CÓDIGO DE LA UBICACIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, 5, 'CANTIDAD DE FACTOR DE CONVERSIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, 5, 'OPERADOR DE FACTOR DE CONVERSIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, 5, 'VALOR DEL FACTOR DE CONVERSIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, 5, 'GRUPO ACTIVOS');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, 5, 'CÓDIGO ACTIVO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, 5, 'ADICIÓN O MEJORA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, 5, 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, 5, 'VECES A DEPRECIAR NIIF');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, 5, 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, 5, 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, 5, 'AÑO DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, 5, 'MES DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, 5, 'DÍA DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, 5, 'TIPO DOCUMENTO DE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, 5, 'CÓDIGO COMPROBANTE DE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, 5, 'NÚMERO DE COMPROBANTE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, 5, 'SECUENCIA DE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, 5, 'TIPO Y COMPROBANTE CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, 5, 'NÚMERO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, 5, 'NÚMERO DE VENCIMIENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, 5, 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, 5, 'MES VENCIMIENTO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, 5, 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, 5, 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE');

        $i = 6;
        $now = Carbon::now();
        foreach ($reception_details as $key => $reception_detail) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'E'); //TIPO DE COMPROBANTE (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $provider_order->warehouse->city_id); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, '0.000000'); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $now->year); //AÑO DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $now->month); //MES DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $now->day); //DÍA DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 1); //CÓDIGO DEL VENDEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key + 1)); //SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, 0); //CENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception_detail->product_name); //DESCRIPCIÓN DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->quantity_received); //CANTIDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->warehouse->city_id); //CÓDIGO DE LA BODEGA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 0); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, ''); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, ''); //AÑO DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, ''); //MES DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, ''); //DÍA DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
            $i++;
        }
        unset($key);
        unset($reception_detail);
        foreach ($grouped_products as $key => $reception_detail) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'E'); //TIPO DE COMPROBANTE (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $provider_order->warehouse->city_id); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, '0.000000'); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $now->year); //AÑO DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $now->month); //MES DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $now->day); //DÍA DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 1); //CÓDIGO DEL VENDEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key + 1)); //SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, 0); //CENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception_detail->product_name); //DESCRIPCIÓN DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->quantity_received); //CANTIDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->warehouse->city_id); //CÓDIGO DE LA BODEGA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 0); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, ''); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, ''); //AÑO DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, ''); //MES DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, ''); //DÍA DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
            $i++;
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s', time()).'-plano-e.xlsx';
        $filename = mb_convert_encoding($filename, "UTF-8", "ASCII");
        $path = public_path(Config::get('app.download_directory')).$filename;
        $objWriter->save($path);
        // chmod($path,0777);
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];
        return Response::download($path, $filename, $headers);
    }

    /**
     * Funcion para marcar el recibo de bodega como cargado al sistema contable
     * @param  int $id indetificador
     * @return [type]        [description]
     */
    public function reception_uploaded_e_plane($id)
    {
        $reception = ProviderOrderReception::find($id);
        if (empty($reception->plane_e_upload_date)) {
            $now = Carbon::now()->toDateTimeString();
            $reception->plane_e_upload_date = $now;
            $reception->save();

            return Redirect::back()->with('success', 'Se ha actualizado el estado de carga del plano E para el recibo de bodega.');
        }
        return Redirect::back()->with('error', 'El recibo de bodega ya había sido actualizado en su archivo plano E.');
    }

    /**
     * Exportar en excel el plano E del recibo de bodega
     * @param  int $id identificador del recibo de bodega
     */
    public function reception_export_p_plane($id)
    {
        $reception = ProviderOrderReception::find($id);
        $provider_order = ProviderOrder::with('warehouse.city')->find($reception->provider_order_id);
        $provider = Provider::find($provider_order->provider_id);
        if ($provider->type == 'Para faltantes') {
            if ($provider_order->warehouse->city_id == 1) {
                if ($provider_order->warehouse->id == 1) {
                    $p_code = 3;
                } else {
                    $p_code = 8;
                }
            } elseif ($provider_order->warehouse->city_id == 2) {
                $p_code = 5;
            }
        } else {
            if ($provider_order->warehouse->city_id == 1) {
                if ($provider_order->warehouse->id == 1) {
                    $p_code = 2;
                } else {
                    $p_code = 7;
                }
            } elseif ($provider_order->warehouse->city_id == 2) {
                $p_code = 4;
            }
        }

        $consecutive = DB::table('consecutives')->where('type', 'provider_order_reception_p_'.$p_code)->where('city_id', $provider_order->warehouse->city_id)->first();

        $nit = explode('-', $provider->nit);
        $nit = str_replace('.', '', $nit[0]);

        $reception_details = $reception->getProducts();
        $reception_details = $reception_details->filter(function ($product) {
            return $product->type == 'Simple';
        });
        $grouped_products = $reception->getProductGroupDetails();

        $objPHPExcel = new PHPExcel();
        //propiedades
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setTitle('Orden de Compra');
        $objPHPExcel->getProperties()->setSubject('Orden de Compra');
        $objPHPExcel->setActiveSheetIndex(0);

        //estilos
        $style_header = [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb'=>'99ccff'],
            ],
            'font' => [
                'color' => ['rgb'=>'000000'],
                'bold' => true,
            ]
        ];
        $objPHPExcel->getActiveSheet()->mergeCells('A1:BH1')->getStyle('A1:BH1')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, 'MERQUEO S A S');

        $objPHPExcel->getActiveSheet()->mergeCells('A2:BH2')->getStyle('A2:BH2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 2, 'MODELO PARA LA IMPORTACIÓN DE MOVIMIENTO CONTABLE');

        $objPHPExcel->getActiveSheet()->mergeCells('A3:BH3')->getStyle('A3:BH3')->applyFromArray($style_header);
        // $now = Carbon::now()->format('M j/Y');
        $received_date = Carbon::createFromFormat('Y-m-d H:i:s', $reception->created_at);
        $header_date = $received_date->format('M j/Y');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 3, 'De: '.$header_date.' A: '.$header_date);

        $objPHPExcel->getActiveSheet()->mergeCells('A4:BH4');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 4, '');

        //titulos
        $objPHPExcel->getActiveSheet()->getStyle('A5:BH5')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 5, 'TIPO DE COMPROBANTE (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 5, 'CÓDIGO COMPROBANTE  (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, 5, 'NÚMERO DE DOCUMENTO (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, 5, 'CUENTA CONTABLE   (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, 5, 'DÉBITO O CRÉDITO (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, 5, 'VALOR DE LA SECUENCIA   (OBLIGATORIO)');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, 5, 'AÑO DEL DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, 5, 'MES DEL DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, 5, 'DÍA DEL DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, 5, 'CÓDIGO DEL VENDEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, 5, 'CÓDIGO DE LA CIUDAD');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, 5, 'CÓDIGO DE LA ZONA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, 5, 'SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, 5, 'CENTRO DE COSTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, 5, 'SUBCENTRO DE COSTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, 5, 'NIT');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, 5, 'SUCURSAL');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, 5, 'DESCRIPCIÓN DE LA SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, 5, 'NÚMERO DE CHEQUE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, 5, 'COMPROBANTE ANULADO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, 5, 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, 5, 'FORMA DE PAGO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, 5, 'PORCENTAJE DEL IVA DE LA SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, 5, 'VALOR DE IVA DE LA SECUENCIA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, 5, 'BASE DE RETENCIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, 5, 'BASE PARA CUENTAS MARCADAS COMO RETEIVA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, 5, 'SECUENCIA GRAVADA O EXCENTA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, 5, 'PORCENTAJE AIU');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, 5, 'BASE IVA AIU');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, 5, 'LÍNEA PRODUCTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, 5, 'GRUPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, 5, 'CÓDIGO PRODUCTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, 5, 'CANTIDAD');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, 5, 'CANTIDAD DOS');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, 5, 'CÓDIGO DE LA BODEGA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, 5, 'CÓDIGO DE LA UBICACIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, 5, 'CANTIDAD DE FACTOR DE CONVERSIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, 5, 'OPERADOR DE FACTOR DE CONVERSIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, 5, 'VALOR DEL FACTOR DE CONVERSIÓN');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, 5, 'GRUPO ACTIVOS');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, 5, 'CÓDIGO ACTIVO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, 5, 'ADICIÓN O MEJORA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, 5, 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, 5, 'VECES A DEPRECIAR NIIF');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, 5, 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, 5, 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, 5, 'AÑO DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, 5, 'MES DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, 5, 'DÍA DOCUMENTO DEL PROVEEDOR');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, 5, 'TIPO DOCUMENTO DE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, 5, 'CÓDIGO COMPROBANTE DE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, 5, 'NÚMERO DE COMPROBANTE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, 5, 'SECUENCIA DE PEDIDO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, 5, 'TIPO Y COMPROBANTE CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, 5, 'NÚMERO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, 5, 'NÚMERO DE VENCIMIENTO');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, 5, 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, 5, 'MES VENCIMIENTO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, 5, 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE');
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, 5, 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE');

        $i = 6;
        $key = 1;
        foreach ($reception_details as $reception_detail) {
            if ($reception_detail->sub_total_cost != 0) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception_detail->sub_total_cost); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception_detail->product_name); //DESCRIPCIÓN DE LA SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->quantity_received); //CANTIDAD
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->warehouse->city_id); //CÓDIGO DE LA BODEGA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->warehouse->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                $i++;
                $key++;

                if ($reception_detail->consumption_tax) {
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception_detail->consumption_tax); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception_detail->product_name.' Imp. consumo'); //DESCRIPCIÓN DE LA SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->quantity_received); //CANTIDAD
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->warehouse->city_id); //CÓDIGO DE LA BODEGA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->warehouse->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                    $i++;
                    $key++;
                }
            }
        }
        unset($reception_detail);
        if (!empty($grouped_products)) {
            foreach ($grouped_products as $reception_detail) {
                if ($reception_detail->sub_total_cost > 0) {
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception_detail->sub_total_cost); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception_detail->product_name); //DESCRIPCIÓN DE LA SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->warehouse->city_id); //CÓDIGO DE LA BODEGA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->warehouse->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                    $i++;
                    $key++;

                    if ($reception_detail->consumption_tax) {
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 1435050000); //CUENTA CONTABLE   (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception_detail->consumption_tax); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $reception_detail->product_name.' Imp. consumo'); //DESCRIPCIÓN DE LA SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, 0); //PORCENTAJE DEL IVA DE LA SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, 0); //VALOR DE IVA DE LA SECUENCIA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, 'N'); //SECUENCIA GRAVADA O EXCENTA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, $reception_detail->accounting_line); //LÍNEA PRODUCTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, $reception_detail->accounting_group); //GRUPO PRODUCTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, $reception_detail->accounting_code); //CÓDIGO PRODUCTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, $provider_order->warehouse->city_id); //CÓDIGO DE LA BODEGA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, $reception_detail->reception_detail_product_group_quantity_received); //CANTIDAD DE FACTOR DE CONVERSIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, 0); //ADICIÓN O MEJORA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, 'E'); //TIPO DOCUMENTO DE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, $provider_order->warehouse->city_id); //CÓDIGO COMPROBANTE DE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, $reception->movement_consecutive_e); //NÚMERO DE COMPROBANTE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, ($key)); //SECUENCIA DE PEDIDO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, ''); //TIPO Y COMPROBANTE CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, 0); //NÚMERO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, ''); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, ''); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, ''); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                        $i++;
                        $key++;
                    }
                }
            }
        }
        // TOTAL
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, 2205010000); //CUENTA CONTABLE   (OBLIGATORIO)
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'C'); //DÉBITO O CRÉDITO (OBLIGATORIO)
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception->getTotal()); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $provider->name.' FV '. $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, 'P-00'.$p_code); //TIPO Y COMPROBANTE CRUCE
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 1); //NÚMERO DE VENCIMIENTO
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
        $i++;
        $key++;

        // ICA
        if ($reception->rete_ica_amount) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $reception->rete_ica_account); //CUENTA CONTABLE   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'C'); //DÉBITO O CRÉDITO (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception->rete_ica_amount); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $provider->name.' FV '. $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, '-'.$p_code); //TIPO Y COMPROBANTE CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
            $i++;
            $key++;
        }

        // RETE FUENTE
        if ($reception->rete_fuente_amount) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $reception->rete_fuente_account); //CUENTA CONTABLE   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'C'); //DÉBITO O CRÉDITO (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $reception->rete_fuente_amount); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $provider->name.' FV '. $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, '-'.$p_code); //TIPO Y COMPROBANTE CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
            $i++;
            $key++;
        }

        // IVA's
        $iva_array = $reception->getIvaDetails();
        if (count($iva_array)) {
            foreach ($iva_array as $iva) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, 'P'); //TIPO DE COMPROBANTE (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $p_code); //CÓDIGO COMPROBANTE  (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $reception->movement_consecutive_p); //NÚMERO DE DOCUMENTO (OBLIGATORIO) // consecutivo que me entrega marisol solo aumenta para diferente recibo de bodega, no se aumenta cuando se generan varios archivos por recibo de bodega.
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $iva['iva_account']); //CUENTA CONTABLE   (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, 'D'); //DÉBITO O CRÉDITO (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $iva['total']); //VALOR DE LA SECUENCIA   (OBLIGATORIO)
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $received_date->year); //AÑO DEL DOCUMENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $received_date->month); //MES DEL DOCUMENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $received_date->day); //DÍA DEL DOCUMENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, 0); //CÓDIGO DEL VENDEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, $i, $consecutive->prefix); //CÓDIGO DE LA CIUDAD
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, $i, 0); //CÓDIGO DE LA ZONA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, $i, ($key)); //SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, $i, $provider_order->warehouse->city_id); //CENTRO DE COSTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, $i, 0); //SUBCENTRO DE COSTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, $i, $nit); //NIT no lleva ni punto ni guión ni digito de verificación
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, $i, 0); //SUCURSAL
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, $i, $provider->name.' FV '. $reception->invoice_number); //DESCRIPCIÓN DE LA SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, $i, ''); //NÚMERO DE CHEQUE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(19, $i, 'N'); //COMPROBANTE ANULADO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(20, $i, 0); //CÓDIGO DEL MOTIVO DE DEVOLUCIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(21, $i, 0); //FORMA DE PAGO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(22, $i, '0.00'); //PORCENTAJE DEL IVA DE LA SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(23, $i, '0.00'); //VALOR DE IVA DE LA SECUENCIA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(24, $i, ''); //BASE DE RETENCIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(25, $i, '0.00'); //BASE PARA CUENTAS MARCADAS COMO RETEIVA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(26, $i, ''); //SECUENCIA GRAVADA O EXCENTA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(27, $i, ''); //PORCENTAJE AIU
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(28, $i, ''); //BASE IVA AIU
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(29, $i, ''); //LÍNEA PRODUCTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(30, $i, ''); //GRUPO PRODUCTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(31, $i, ''); //CÓDIGO PRODUCTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(32, $i, '0.00000'); //CANTIDAD
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(33, $i, '0.00000'); //CANTIDAD DOS
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(34, $i, 0); //CÓDIGO DE LA BODEGA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(35, $i, 0); //CÓDIGO DE LA UBICACIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(36, $i, '0.00000'); //CANTIDAD DE FACTOR DE CONVERSIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(37, $i, 0); //OPERADOR DE FACTOR DE CONVERSIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(38, $i, '0.00000'); //VALOR DEL FACTOR DE CONVERSIÓN
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(39, $i, ''); //GRUPO ACTIVOS
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(40, $i, ''); //CÓDIGO ACTIVO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(41, $i, ''); //ADICIÓN O MEJORA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(42, $i, 0); //VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(43, $i, 0); //VECES A DEPRECIAR NIIF
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(44, $i, 193); //NÚMERO DEL DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(45, $i, 2); //PREFIJO DEL DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(46, $i, $received_date->year); //AÑO DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(47, $i, $received_date->month); //MES DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(48, $i, $received_date->day); //DÍA DOCUMENTO DEL PROVEEDOR
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(49, $i, ''); //TIPO DOCUMENTO DE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(50, $i, 0); //CÓDIGO COMPROBANTE DE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(51, $i, 0); //NÚMERO DE COMPROBANTE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(52, $i, 0); //SECUENCIA DE PEDIDO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(53, $i, '-'.$p_code); //TIPO Y COMPROBANTE CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(54, $i, $reception->movement_consecutive_e); //NÚMERO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(55, $i, 0); //NÚMERO DE VENCIMIENTO
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(56, $i, $received_date->year); //AÑO VENCIMIENTO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(57, $i, $received_date->month); //MES VENCIMIENTO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(58, $i, $received_date->day); //DÍA VENCIMIENTO DE DOCUMENTO CRUCE
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(59, $i, 0); //NÚMERO DE CAJA ASOCIADA AL COMPROBANTE
                $i++;
                $key++;
            }
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s', time()).'-plano-p.xlsx';
        $filename = mb_convert_encoding($filename, "UTF-8", "ASCII");
        $path = public_path(Config::get('app.download_directory')).$filename;
        $objWriter->save($path);
        // chmod($path,0777);
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];
        return Response::download($path, $filename, $headers);
    }

    /**
     * Actualizar cantidades recibidas para productos proveedor
     * @deprecated
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function reception_update_product_group($id)
    {
        try {
            DB::beginTransaction();
            $reception = ProviderOrderReception::findOrFail($id);

            if (Input::has('status') && Input::has('product') && Request::ajax()) {
                $product = ProviderOrderReceptionDetailProductGroup::find(Input::get('product'));
                if (Input::get('status') == 'Dañado') {
                    if ($product->quantity_received > 0) {
                        $store_product = StoreProduct::find($product->store_product_id);
                        if ($reception->status == 'Recibido') {
                            $store_product->reception_stock -= $product->quantity_received;
                        } elseif (in_array($reception->status, ['Almacenado', 'Contabilizado', 'Revisado'])) {
                            $store_product->current_stock -= $product->quantity_received;
                        }
                        $store_product->save();
                    }
                    $product->quantity_received = 0;
                }
                $product->status = Input::get('status');
                $product->save();
                $reception_detail = ProviderOrderReceptionDetail::find($product->reception_detail_id);
                $reception_detail->updateProductGroupedStatus();
                DB::commit();
                return Response::json(['status' => true, 'message' => 'Se ha actualizado el estado del producto']);
            } else {
                $quantity_received_input = Input::get('quantity_received_input');
                $quantity_ordered_input = Input::get('quantity_ordered_input');
                $provider_ordered_input = Input::get('provider_ordered_input');
                $main_product_status = [];
                if (Input::has('expiration_date_input')) {
                    $expiration_date_input = Input::get('expiration_date_input');
                    foreach ($expiration_date_input as $key => $date) {
                        $product = ProviderOrderReceptionDetailProductGroup::find($key);
                        $product->expiration_date = Carbon::createFromFormat('d/m/Y', $date)->toDateTimeString();
                        ;
                        $product->save();
                    }
                }

                foreach ($quantity_received_input as $key => $quantity) {
                    $product = ProviderOrderReceptionDetailProductGroup::find($key);
                    $product->quantity_received = $quantity;
                    if ($quantity_ordered_input[$key] == $product->quantity_received) {
                        $product->status = 'Recibido';
                    }
                    if ($quantity_ordered_input[$key] < $product->quantity_received) {
                        $provider_order_product = ProviderOrderDetailProductGroup::find($provider_ordered_input[$key]);
                        $provider_order_product->quantity_order = $product->quantity_received;
                        $provider_order_product->save();
                        $product->status = 'Recibido';
                    }
                    if ($quantity_ordered_input[$key] > $product->quantity_received) {
                        $product->status = 'Parcialmente recibido';
                    }
                    if ($product->quantity_received == 0) {
                        $product->status = 'No recibido';
                    }
                    $main_product_status[] = $product->status;
                    $product->save();
                }
            }
            $reception_detail = ProviderOrderReceptionDetail::find($product->reception_detail_id);
            $reception_detail->updateProductGroupedStatus();
            DB::commit();
            return Redirect::back()->with('success', 'Se han actualizado las cantidades de los productos');
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            if ($e->getModel() == 'ProviderOrderReception') {
                return Redirect::back()->with('error', 'El recibo de bodega no existe');
            }
        } catch (Exception $e) {
            DB::rollBack();
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Funcion para actualizar las cantidades esperadas de un producto según la remisión.
     * @param  Int $id Identificador del recibo de bodega
     * @return Redirect     redirecciona a la url anterior
     */
    public function update_product_expected_quantities($id)
    {
        $reception = ProviderOrderReception::find($id);
        if (!empty($reception)) {
            DB::beginTransaction();
            try {
                if (Input::has('quantity_expected')) {
                    if (Input::get('quantity_expected') < 0) {
                        DB::rollBack();
                        return Redirect::back()->with('error', 'Se ha generado un error, la cantidad esperada debe ser mayor a cero.');
                    }
                }
                if (Input::has('reception_detail_id')) {
                    $reception_detail = ProviderOrderReceptionDetail::find((int)Input::get('reception_detail_id'));
                    if ($reception_detail->type == 'Simple') {
                        $reception_detail->quantity_expected = (int)Input::get('quantity_expected');
                        $quantity_order = (int)Input::get('quantity_order');
                        if ($reception_detail->quantity_expected > $quantity_order) {
                            DB::rollBack();
                            //log
                            Event::fire('reception.errorProductQuantityExpected', [[
                                'reception_id' => $id,
                                'store_product_id' => $reception_detail->store_product_id,
                                'quantity_expected' => $reception_detail->quantity_expected,
                                'quantity_order' => $quantity_order,
                                'admin_id' => Session::get('admin_id')
                            ]]);
                            return Redirect::back()->with('error', 'Se ha generado un error, la cantidad esperada es mayor a la cantidad solicitada.');
                        }
                        $reception_detail->save();
                        //log
                        Event::fire('reception.updatedProductQuantityExpected', [[
                            'reception_id' => $id,
                            'store_product_id' => $reception_detail->store_product_id,
                            'quantity_expected' => $reception_detail->quantity_expected,
                            'admin_id' => Session::get('admin_id')
                        ]]);
                    }
                } elseif (Input::has('reception_detail_product_group_id')) {
                    $reception_detail_product_group = ProviderOrderReceptionDetailProductGroup::find((int)Input::get('reception_detail_product_group_id'));
                    $reception_detail_product_group->quantity_expected = (int)Input::get('quantity_expected');
                    $quantity_order = (int)Input::get('quantity_order');
                    if ($reception_detail_product_group->quantity_expected > $quantity_order) {
                        DB::rollBack();
                        Event::fire('reception.errorProductQuantityExpected', [[
                            'reception_id' => $id,
                            'store_product_id' => $reception_detail_product_group->store_product_id,
                            'quantity_expected' => $reception_detail_product_group->quantity_expected,
                            'quantity_order' => $quantity_order,
                            'admin_id' => Session::get('admin_id')
                        ]]);
                        return Redirect::back()->with('error', 'Se ha generado un error, la cantidad esperada es mayo a la cantidad solicitada.');
                    }
                    $reception_detail_product_group->save();
                }
                DB::commit();
                return Redirect::back()->with('success', 'Se han actualizado las cantidades esperadas.');
            } catch (Exception $e) {
                DB::rollBack();
                echo '<pre>';
                var_dump($e);
                echo '</pre>';
                exit;
                return Redirect::back()->with('error', 'Se ha generado un error al guardar las cantidades esperadas.');
            }
        } else {
            \App::abort(404);
        }
    }

    /**
     * Actualiza todos los productos del recibo a un estado determinado
     * @param  Int $id identificador del recibo
     */
    public function update_products_status($id)
    {
        $reception = ProviderOrderReception::find($id);
        if ($reception) {
            $status = Input::get('status');
            if ($reception->status == 'Almacenado' && $status == 'Revisado') {
                $reception_products = ProviderOrderReceptionDetail::where('reception_id', $reception->id)->get();
                $reception_products->each(function ($product) use ($status) {
                    $product->accounting_status = $status;
                    $product->save();
                });
                return Redirect::back()->with('success', 'Se actualizaron los estados contables de todos los productos a: '.$status);
            } elseif ($reception->status == 'Revisado' && $status == 'Contabilizado') {
                $reception_products = ProviderOrderReceptionDetail::where('reception_id', $reception->id)->get();
                $reception_products->each(function ($product) use ($status) {
                    $product->accounting_status = $status;
                    $product->save();
                });
                return Redirect::back()->with('success', 'Se actualizaron los estados contables de todos los productos a: '.$status);
            } else {
                return Redirect::back()->with('error', 'El recibo de bodega no tiene el estado correcto.');
            }
        } else {
            \App::abort(404);
        }
    }

    /**
     * Función para actualizar las cantidades esperadas de los productos del recibo de bodega según la remisión.
     * La actualización de los productos se realiza mediante la importación de un archivo excel.
     * @param  Int $id Identificador del recibo de bodega
     * @return Redirect     redirecciona a la url anterior
     */
    public function import_product_expected_quantities($id)
    {
        if (Input::hasFile('file')) {
            ini_set('memory_limit', '512M');
            set_time_limit(0);

            $extension_list = 'xlsx, xls';
            $file = Input::file('file');

            $reception = ProviderOrderReception::find($id);
            $reception_products = $reception->getProducts();

            if (!file_exists($file->getRealPath())) {
                return Redirect::back()->with('error', 'No file uploaded');
            }

            $path = public_path(Config::get('app.download_directory'));

            $extension_obj = explode(',', $extension_list);
            foreach ($extension_obj as $extension) {
                foreach (glob($path.'/*.'.$extension) as $file_tmp) {
                    $file_tmp = pathinfo($file_tmp);
                    $files_by_extension[$extension][] = $file_tmp['filename'];
                }
            }

            //obtener archivo
            $file_path = Input::file('file')->getRealPath();
            $current_dir = opendir($path);

            $objReader = new PHPExcel_Reader_Excel2007();
            $objPHPExcel = $objReader->load($file_path);
            //abrir la ultima hoja que contiene la informacion
            $sheetCount = $objPHPExcel->getSheetCount();
            $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
            //obtener el numero total de filas y columnas
            $rows = $objPHPExcel->getActiveSheet()->getHighestRow();
            $columns = PHPExcel_Cell::columnIndexFromString($objPHPExcel->getActiveSheet()->getHighestColumn());
            $errors = [];
            $cont = $cont_error = 0;

            DB::beginTransaction();
            for ($i = 2; $i <= $rows; $i++) {
                $plu = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
                $reference = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $i)->getValue();
                $quantity_expected = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $i)->getValue();

                if (!empty($plu) && !empty($reference)) {
                    $errors[] = 'La fila #'.$i.' no tiene Referencia ni PLU del producto.<br>';
                } else {
                    if ($quantity_expected < 0) {
                        $errors[] = 'La fila #'.$i.' no tiene una cantidad menor a cero.<br>';
                    } else {
                        $product = $reception_products->filter(function ($reception_product) use ($plu, $reference) {
                            if (!empty($plu)) {
                                return $reception_product->plu == $plu && $reception_product->type == 'Simple';
                            } else {
                                return $reception_product->reference == $reference && $reception_product->type == 'Simple';
                            }
                        })->first();

                        if ($product) {
                            if ($quantity_expected > $product->quantity_order) {
                                $errors[] = 'La fila #'.$i.' la cantidad esperada es superior a la cantidad ordenada de ese producto.<br>';
                            } else {
                                $product->quantity_expected = $quantity_expected;
                                $product->save();
                            }
                        } else {
                            $errors[] = 'La fila #'.$i.' no se encontró un producto con esa información.<br>';
                        }
                    }
                }
            }
            if (count($errors)) {
                DB::rollback();
                return Redirect::back()->with('errors', $errors);
            }
            $this->admin_log('products', null, 'import_update_stock');
            DB::commit();
            return Redirect::back()->with('success', 'Se actualizaron los stock de los productos importados.');
        }
    }

    public function update_reception_validate($id)
    {
        $reception = ProviderOrderReception::find($id);
        if ($reception) {
            $validates = Input::get('validate');
            if (count($validates)) {
                $solved = 0;
                foreach ($validates as $key => $validate) {
                    if (!empty($validate)) {
                        $solved++;
                        $valid = ProviderOrderReceptionValidate::find($key);
                        $valid->tipification = $validate;
                        $valid->save();
                    }
                }
                if ($solved) {
                    return Redirect::back()->with('success', 'Se actualizaron las incidencias.');
                }
            }
            return Redirect::back()->with('error', 'No se actualizaron las incidencias.');
        } else {
            \App::abort(404);
        }
    }

    public function upload_invoice($id)
    {
        if (Input::hasFile('invoice_url'))
        {
            $reception = ProviderOrderReception::find($id);

            if ($reception->status == 'Revisado'  ) {
                $image = Input::file('invoice_url');
                $validator = \Validator::make(
                    array('ima' => $image),
                    array('ima' => 'required|mimes:jpeg,bmp,png,gif')
                );
                if ($validator->fails())
                    return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

                if (!empty($reception->invoice_url)) {
                    $path = str_replace($cloudfront_url, uploads_path(), $reception->invoice_url);
                    remove_file($path);
                    remove_file_s3($reception->invoice_url);
                }
                $reception->invoice_url = upload_image($image, false, 'banners/app');
                $reception->save();

                return Redirect::back()->with('success', 'Se ha cargado la factura al recibo.');
            } else {
                return Redirect::back()->with('error', 'El estado del recibo no es el indicado para subir la factura.');
            }
        }
    }

    /**
     *  Obtiene todos los productos del recibo y los productos de la nota credito
     *
     * @param $id ProviderOrderReception
     * @return \Illuminate\Http\JsonResponse
     */
    public function  get_products_credit_note($id)
    {
        $reception = ProviderOrderReception::find($id);

        $search = (Input::has('productName')) ? Input::get('productName') : '';
        $excludeIds = (Input::has('excludeIds')) ? json_decode(Input::get('excludeIds')) : [];

        $productSearch = $reception->getProducts($search, $excludeIds);
        $productsCreditNote = $reception->getProducts('', [], false);

        $data = [
            'productSearch' => $productSearch,
            'productsCreditNote' => $productsCreditNote,
        ];

        return Response::json($data);
    }

    /**
     * Metodo para actualizar la nota credito de los productos
     *
     * @param $id ProviderOrderReception
     * @return \Illuminate\Http\JsonResponse
     */
    public function reception_update_credit_note($id)
    {
        $input = Input::all();
        $reception = ProviderOrderReception::findOrFail($id);

        if(is_null($reception->credit_note_number)) {
            $consecutive = DB::table('consecutives')->where('type', 'credit_note_purchase')->first();
            $consecutive->consecutive += 1;
            $reception->credit_note_number = $consecutive->consecutive;
            $reception->save();
            DB::table('consecutives')->where('type', 'credit_note_purchase')->update(['consecutive' => $consecutive->consecutive]);
            Event::fire('reception.create_credit_note_log', [[$reception, Session::get('admin_id')]]);
        }
        if($input['action'] == 'edit') {
            foreach ($input['products'] as $product) {
                if ($product['type'] == 'Simple') {
                    $provider_order_reception_detail = ProviderOrderReceptionDetail::findOrFail($product['id']);
                    $provider_order_reception_detail->quantity_credit_note = $product['quantity_calculate'];
                    $provider_order_reception_detail->reason_credit_note = $product['reason_credit_note'];
                    $provider_order_reception_detail->consumption_tax_credit_note = $product['consumption_tax_credit_note'];
                    $provider_order_reception_detail->save();
                    Event::fire('reception.update_credit_note_log', [[$provider_order_reception_detail, Session::get('admin_id'), 'agregado a']]);
                } else {
                    $provider_order_reception_detail_product_group = ProviderOrderReceptionDetailProductGroup::findOrFail($product['id']);
                    $provider_order_reception_detail_product_group->quantity_credit_note = $product['quantity_calculate'];
                    $provider_order_reception_detail_product_group->reason_credit_note = $product['reason_credit_note'];
                    $provider_order_reception_detail_product_group->consumption_tax_credit_note = $product['consumption_tax_credit_note'];
                    $provider_order_reception_detail_product_group->save();
                    Event::fire('reception.update_credit_note_log', [[$provider_order_reception_detail_product_group, Session::get('admin_id'), 'agregado a']]);
                }
            }
        } else {
            $product = $input['products'];
            if($product['type'] == 'Simple') {
                $provider_order_reception_detail = ProviderOrderReceptionDetail::findOrFail($product['id']);
                $provider_order_reception_detail->quantity_credit_note = null;
                $provider_order_reception_detail->reason_credit_note = null;
                $provider_order_reception_detail->consumption_tax_credit_note = 0;
                $provider_order_reception_detail->save();
                Event::fire('reception.update_credit_note_log', [[$provider_order_reception_detail, Session::get('admin_id'), 'eliminado de']]);
            } else {
                $provider_order_reception_detail_product_group = ProviderOrderReceptionDetailProductGroup::findOrFail($product['id']);
                $provider_order_reception_detail_product_group->quantity_credit_note = null;
                $provider_order_reception_detail_product_group->reason_credit_note = null;
                $provider_order_reception_detail_product_group->consumption_tax_credit_note = 0;
                $provider_order_reception_detail_product_group->save();
                Event::fire('reception.update_credit_note_log', [[$provider_order_reception_detail_product_group, Session::get('admin_id'), 'eliminado de']]);
            }
        }
        $reception->credit_note_date = Carbon::now();
        $reception->save();
        $productsCreditNote = $reception->getProducts('', [], false);
        $resposeData = [
            'addProductsCreditNote' => [],
            'productsCreditNote' => $productsCreditNote,
        ];

        return Response::json($resposeData);
    }

    public function reception_update_real_provider($id)
    {
        //Se capturan los parametros
        $input = Input::all();
        $reception = ProviderOrderReception::findOrFail($id);

        try {
            DB::beginTransaction();
            //Se actualiza proveedor real de la factura
            $reception->real_provider_id = $input['real_provider_id'];
            $reception->save();

            //Se registra log del movimiento
            Event::fire('reception.update_real_provider_log', [[$reception, Session::get('admin_id')]]);

            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
            //Se retorna a la vista inicial de la factura
            return Redirect::back()->with('error', 'Se ha actualizado el proveedor de la factura.');
        }

        //Se retorna a la vista inicial de la factura
        return Redirect::back()->with('success', 'Se ha actualizado el proveedor de la factura.');
    }
}
