<?php

namespace admin;

use Request; 
use View; 
use Input; 
use Redirect; 
use Session; 
use DB; 
use Str; 
use Validator; 
use Carbon\Carbon; 
use City; 
use Product; 
use Response; 
use Config; 
use Driver; 
use Vehicle; 
use App;
use AdminLog;
use Order;  
use Exception; 
use Event; 
use StoreProduct; 
use OrderProduct; 
use OrderProductGroup; 
use OrderPickingQuality; 
use OrderPickingQualityDetail; 
use OrderLog; 
use OrderGroup; 
use StoreProductWarehouse;
use InvestigationStock;

class AdminPickingQualityController extends AdminController
{
    /**
     * Grilla de pedidos validados
     */
    public function index()
    {
        if (!Request::ajax()) {
            $data = [
                'title' => 'Calidad en Pedidos Alistados',
                'cities' => $this->get_cities(),
                'warehouses' => $this->get_warehouses()
            ];
            return View::make('admin.picking_quality.index', $data);
        }
        else {
            $cityId = Input::get('city_id');
            $warehouseId = Input::get('warehouse_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $orderPickingQuality = new OrderPickingQuality();
            $orders =  $orderPickingQuality->getOrders($cityId, $warehouseId, $search);
            $data = [
                'orders' => $orders,
            ];
            return View::make('admin.picking_quality.index', $data)->renderSections()['content'];
        }
    }

    /**
     * Registra una validación de alistamiento de productos
     * @param $id
     * @return $order
     */
    public function add()
    {
        $id = Input::get('id');
        $order = Order::join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                      ->where('orders.id', $id)
                      ->where('orders.status', 'Alistado')
                      ->select('orders.*', 'order_groups.warehouse_id')
                      ->first();

        if($order && $order->warehouse_id != Session::get('warehouse_id') && Session::get('admin_designation') != 'Super Admin')
            return Redirect::route('AdminPickingQuality.index')->with('error', 'El pedido pertence a otra bodega.');
        elseif(!$order)
            return Redirect::route('AdminPickingQuality.index')->with('error', 'No es posible validar el pedido por que se encuentra en un estado diferente a Alistado');
        elseif($order->payment_method == 'Tarjeta de crédito')
            return Redirect::route('AdminPickingQuality.index')->with('error', 'No es posible validar el pedido por que se encuentra con método de pago tarjeta de crédito');


        $orderPickingQuality = OrderPickingQuality::where('order_id', $order->id)->first();
        if(!$orderPickingQuality){

            $orderPickingQuality = new OrderPickingQuality();
            $orderPickingQuality->order_id = $id;
            $orderPickingQuality->validation_date = Carbon::now()->format('Y-m-d H:i:s');
            $orderPickingQuality->admin_id = Session::get('admin_id');
            $orderPickingQuality->status_dry = 'Pendiente';
            $orderPickingQuality->status_cold = 'Pendiente';
            $orderPickingQuality->save();

            $log = new AdminLog();
            $log->table = 'order_picking_quality';
            $log->row_id = $orderPickingQuality->id;
            $log->action = 'insert';
            $log->admin_id = Session::get('admin_id');
            $log->save();

            return Redirect::route('AdminPickingQuality.edit', ['id' => $order->id, 'validation' => 'seco']);
        }else
            return Redirect::route('AdminPickingQuality.index')->with('error', 'El pedido #'.$order->id.' ingresado fue validado.');
    }

    /**
     * Despliega formulario de preguntas de validación de alistamiento
     * @param $id
     * @return $order
     */
    public function edit($id, $validation)
    {
        $orderPickingQuality = new OrderPickingQuality();
        $order = $orderPickingQuality->getOrder($id);
        if (!$order)
            return Redirect::route('AdminPickingQuality.index')->with('error', 'No existe el pedido ingresado.');

        $orderTransporter = null;
        if ($order->vehicle_id){
            $orderTransporter = Vehicle::where('vehicles.id', $order->vehicle_id)
                                ->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
                                ->select('transporters.fullname AS transporter_fullname', 'vehicles.plate AS vehicle_plate')
                                ->first();
            $driver = Driver::where('drivers.id', $order->driver_id)
                            ->select('drivers.first_name AS driver_first_name', 'drivers.last_name AS driver_last_name', 'drivers.phone AS driver_phone')
                            ->first();
            if($driver){
                $orderTransporter->driver_first_name = $driver->driver_first_name;
                $orderTransporter->driver_last_name = $driver->driver_last_name;
                $orderTransporter->driver_phone = $driver->driver_phone;
            }                 
        }
        $productsQuality = [
            'productsResult' => [],
            'productsValidated' => [],
            'productsPending' => [],
            'productsAditional' => [],
            'productsChanged' => [],
            'productsExpired' => [],
            'productsPoorCondition' => [],
        ];

        if($order->status_validation_dry == 'Validado' || $order->status_validation_cold == 'Validado'){
            $orderPickingQualityDetail = new OrderPickingQualityDetail();
            $productsValidated = $orderPickingQualityDetail->getProductInOrderValidated($order->id,$validation);
            if(count($productsValidated)){
                foreach ($productsValidated as $product) {
                    if($product['status_product_validation'] == 'Faltante' && !$product['status_shrinkage'])
                        array_push($productsQuality['productsValidated'],$product);

                    if($product['status_product_validation'] == 'Trocado' && !$product['status_shrinkage'])
                        array_push($productsQuality['productsChanged'],$product);

                    if($product['status_product_validation'] == 'Adicional' && !$product['status_shrinkage'])
                        array_push($productsQuality['productsAditional'],$product);

                    if($product['status_product_validation'] == 'Validado' && !$product['status_shrinkage'])
                        array_push($productsQuality['productsResult'],$product);

                    if($product['status_shrinkage'] == 'Mal estado')
                        array_push($productsQuality['productsPoorCondition'],$product);

                    if($product['status_shrinkage'] == 'Vencido')
                        array_push($productsQuality['productsExpired'],$product);
                }
            }
        }
        $data = [
            'title' => 'Calidad en Alistamiento Pedido',
            'validation' => $validation,
            'order' => $order,
            'order_transporter' => $orderTransporter,
            'products_validated'=> $productsQuality['productsValidated'],
            'products_pending'=> $productsQuality['productsPending'],
            'products_aditional'=> $productsQuality['productsAditional'],
            'products_changed'=> $productsQuality['productsChanged'],
            'products_expired'=> $productsQuality['productsExpired'],
            'products_poor_condition'=> $productsQuality['productsPoorCondition'],
        ];
        return View::make('admin.picking_quality.edit', $data);
    }

    /**
     * Valida y despliega productos
     * @param $id
     * @return array products
     */
    public function validateProductAjax($id)
    {

        try {
            DB::beginTransaction();
            $response = ['status' => false];
            $order = Order::find($id);
            $validationType = Input::get('validation_type');
            $validationProduct = Input::get('validation_product');
            $validation = Input::get('validation');
            $productId = Input::get('product_id');
            $search = Input::get('s');
            $productsValidated = [];
            $errorValidation = false;
            if (!$order) {
                return Redirect::route('AdminPickingQuality.index')->with('error', 'No existe el pedido ingresado.');
            }

            $orderPickingQuality = OrderPickingQuality::where('order_id', $id)->first();
            if ($orderPickingQuality && Input::has('s')) {
                $error = false;
                if ($validation == 'seco' && is_null($orderPickingQuality->check_pack_dry) && is_null($orderPickingQuality->check_gift_dry) && (!Input::has('check_pack_dry') || !Input::has('check_gift_dry'))) {
                    $error = true;
                }

                if ($validation == 'frio' && is_null($orderPickingQuality->check_pack_cold) && is_null($orderPickingQuality->check_gift_cold) && (!Input::has('check_pack_cold') || !Input::has('check_gift_cold'))) {
                    $error = true;
                }

                if ($error) {
                    $data = [
                        'section' => 'content',
                        'products' => $productsValidated,
                    ];
                    $view = View::make('admin.picking_quality.edit', $data)->renderSections();
                    $data_json['html'] = $view['content'];
                    $data_json['success'] = true;
                    $data_json['message'] = 'Por favor validar las preguntas de alistamiento.';
                    return Response::json($data_json);
                }
                if ($validation == 'seco' && (is_null($orderPickingQuality->check_pack_dry) || is_null($orderPickingQuality->check_gift_dry))) {
                    $orderPickingQuality->check_pack_dry = Input::get('check_pack_dry');
                    $orderPickingQuality->check_gift_dry = Input::get('check_gift_dry');
                    $orderPickingQuality->validation_date_start_dry = Carbon::now()->format('Y-m-d H:i:s');
                    $orderPickingQuality->save();
                }
                if ($validation == 'frio' && (is_null($orderPickingQuality->check_pack_cold) || is_null($orderPickingQuality->check_gift_cold))) {
                    $orderPickingQuality->check_pack_cold = Input::get('check_pack_cold');
                    $orderPickingQuality->check_gift_cold = Input::get('check_gift_cold');
                    $orderPickingQuality->validation_date_start_cold = Carbon::now()->format('Y-m-d H:i:s');
                    $orderPickingQuality->save();
                }
                if ($validation) {
                    $log = new AdminLog();
                    $log->table = 'order_picking_quality';
                    $log->row_id = $orderPickingQuality->id;
                    $log->action = 'update question';
                    $log->admin_id = Session::get('admin_id');
                    $log->save();
                }
            }
            $orderPickingQualityDetail = new OrderPickingQualityDetail();
            if ($search || $productId) {
                $products = $orderPickingQualityDetail->getProductsInValidation($order->id, $productId, $search);
                if ($products && count($products) > 1) {
                    $quantity = 0;
                    foreach ($products as $product) {
                        $quantity += $product->quantity;
                    }
                    $product = $products->first();
                    $product->quantity = $quantity;
                } else {
                    $product = $products->first();
                }
                $productGroups = $orderPickingQualityDetail->getProductGroupsInValidation($order->id, $productId,
                    $search);
                if ($productGroups && count($productGroups) > 1) {
                    $quantity = 0;
                    foreach ($productGroups as $product) {
                        $quantity += $product->quantity;
                    }
                    $productGroup = $productGroups->first();
                    $productGroup->quantity = $quantity;
                } else {
                    $productGroup = $productGroups->first();
                }
                if ($product) {
                    if ($product && $productGroup) {
                        $product->quantity = $product->quantity + $productGroup->quantity;
                    }

                    $orderPickingQualityId = OrderPickingQualityDetail::join('order_picking_quality',
                        'order_picking_quality.id', '=', 'order_picking_quality_id')
                        ->where('type', 'Agrupado')
                        ->where('store_product_id', $product->store_product_id)
                        ->where('order_id', $order->id)
                        ->count();
                    if ($productGroup && $productGroup->quantity > $orderPickingQualityId) {
                        $product->type = 'Agrupado';
                    } else {
                        $product->type = 'Product';
                    }

                    $product->validation_product = $validationProduct;
                    $product->validation_type = $validationType;
                    if ($validationType == 'reference_validate' || $productId) {
                        $productsValidated = $this->saveValidation($product, $validation);
                    } else {
                        $productsValidated = $products;
                    }

                    if (!$productsValidated) {
                        $errorValidation = true;
                    }
                } else {
                    if ($productGroup) {
                        $productGroup->validation_product = $validationProduct;
                        $productGroup->validation_type = $validationType;
                        $productGroup->type = 'Agrupado';
                        if ($validationType == 'reference_validate' || $productId) {
                            $productsValidated = $this->saveValidation($productGroup, $validation);
                        } else {
                            $productsValidated = $productGroups;
                        }

                        if (!$productsValidated) {
                            $errorValidation = true;
                        }
                    } else {
                        $orderPickingQualityId = OrderPickingQuality::where('order_id', $order->id)->pluck('id');
                        $additionalProducts = $orderPickingQualityDetail->getAditionalProductsForQuality($order,
                            $orderPickingQualityId, $search);
                        $additionalProducts = $additionalProducts->get();
                        $additionalProduct = $additionalProducts->first();
                        if ($additionalProduct) {
                            $additionalProduct->validation_product = $validationProduct;
                            $additionalProduct->validation_type = $validationType;
                            $additionalProduct->type = 'Product';
                            if (!$additionalProduct->order_picking_quality_id) {
                                $additionalProduct->order_picking_quality_id = $orderPickingQualityId;
                            }

                            if ($validationType == 'reference_validate' || $productId) {
                                $productsValidated = $this->saveValidation($additionalProduct, $validation);
                            } else {
                                $productsValidated = $additionalProducts;
                            }

                            if (!$productsValidated) {
                                $errorValidation = true;
                            }
                        } else {
                            $productsValidated = "";
                        }
                    }
                }
                if ($validationType == 'reference_validate' || $productId) {
                    $productsValidated = $orderPickingQuality->getProductsValidated($order->id);
                    if ($validation == 'seco') {
                        $productsValidated->where('store_products.storage', 'Seco');
                    } else {
                        $productsValidated->where('store_products.storage', '<>', 'Seco');
                    }

                    $productsValidated = $productsValidated->groupBy('order_picking_quality_details.id')
                        ->orderBy('product_validation_id', 'DESC')
                        ->get();
                }
            } else {
                $productsValidated = OrderPickingQuality::join('order_picking_quality_details',
                    'order_picking_quality_id', '=', 'order_picking_quality.id')
                    ->join('store_products', 'store_products.id', '=', 'order_picking_quality_details.store_product_id')
                    ->join('products', 'products.id', '=', 'store_products.product_id')
                    ->where('order_picking_quality.order_id', $order->id)
                    ->select('products.quantity AS product_quantity',
                        'products.unit AS product_unit',
                        'order_picking_quality_id',
                        'order_picking_quality_details.id AS product_validation_id',
                        'store_products.id AS store_product_id',
                        DB::raw('COUNT(order_picking_quality_details.store_product_id) AS quantity'),
                        'products.reference AS reference',
                        'products.name AS product_name',
                        'image_medium_url AS product_image_url');


                if ($validation == 'seco') {
                    $productsValidated->where('store_products.storage', 'Seco');
                } else {
                    $productsValidated->where('store_products.storage', '<>', 'Seco');
                }

                $productsValidated = $productsValidated->groupBy('order_picking_quality_details.id')
                    ->orderBy('product_validation_id', 'DESC')
                    ->get();
            }
            $data = [
                'section' => 'content',
                'products' => $productsValidated,
            ];
            $view = View::make('admin.picking_quality.edit', $data)->renderSections();
            $data_json['html'] = $view['content'];
            if ($search) {
                $verifyProduct = Product::where(function ($query) use ($search) {
                    $query->where('products.reference', 'LIKE', '%' . $search . '%');
                    $query->orWhere('products.name', 'LIKE', '%' . $search . '%');
                })
                    ->first();
                if ($errorValidation) {
                    $msg = $validation == 'seco' ? 'frio' : 'seco';
                    $data_json['success'] = true;
                    $data_json['message'] = 'Este producto se debe validar en ' . $msg;
                }
                if (!$verifyProduct) {
                    $data_json['success'] = true;
                    $data_json['message'] = 'Este producto no existe por favor validar manualmente.';
                }
            }

            DB::commit();
            return Response::json($data_json);
        } catch (Exception $exception) {
            DB::rollback();
            \ErrorLog::add($exception, 500);

            return Redirect::route('AdminPickingQuality.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $product = OrderPickingQualityDetail::where('order_picking_quality_details.id', Input::get('product_id'))
                                            ->join('order_picking_quality', 'order_picking_quality.id', '=', 'order_picking_quality_details.order_picking_quality_id')
                                            ->select('order_picking_quality_details.*', 'order_picking_quality.order_id')
                                            ->first();
        if($product){
            $orderProduct = OrderProduct::where('order_id', $product->order_id)
                                          ->where('store_product_id', $product->store_product_id)
                                          ->where('type', 'Product')
                                          ->where('is_gift', 1)
                                          ->first();
            if($orderProduct)
            {
                $productParent = OrderProduct::where('store_product_id', $orderProduct->parent_id)
                                              ->where('order_id', $orderProduct->order_id)
                                              ->where('type', 'Product')
                                              ->where('is_gift', 0)
                                              ->first();
                if($productParent){
                    $productParent->fulfilment_status = 'Fullfilled';
                    $productParent->save();
                }
                $orderProduct->delete();
            }
            $product->delete();
        }
        $orderPickingQuality = new OrderPickingQuality();
        $productsValidated = $orderPickingQuality ->getProductsValidated($id);
        if(Input::get('validation') == 'seco')
            $productsValidated->where('store_products.storage', 'Seco');
        else
            $productsValidated->where('store_products.storage', '<>', 'Seco');

        $productsValidated = $productsValidated->groupBy('order_picking_quality_details.id')->get();
        $data = [
            'section' => 'content',
            'products' => $productsValidated,
        ];
        $view = View::make('admin.picking_quality.edit', $data)->renderSections();
        $dataJson['html'] = $view['content'];
        return Response::json($dataJson);
    }

    /**
     * Almacena producto validado
     * @param object $product
     * @return array object product
     */
    protected function saveValidation($product, $validation){

            if($product->product_validation_id){
                $storeProduct = StoreProduct::find($product->store_product_id);
                if(strtolower($storeProduct->storage) != $validation &&  $validation == 'seco'){
                    return false;
                }elseif($validation == 'frio' && !in_array($storeProduct->storage, ['Refrigerado','Congelado'])){
                    return false;
                }
                $totalProduct = OrderPickingQualityDetail::where('order_picking_quality_id', $product->order_picking_quality_id)
                                                           ->where('store_product_id', $product->store_product_id)
                                                           ->count();

                $productValidation = new OrderPickingQualityDetail();
                $productValidation->order_picking_quality_id = $product->order_picking_quality_id;
                $productValidation->store_product_id = $product->store_product_id;
                $productValidation->product_unit = $product->product_unit;
                $productValidation->product_quantity = $product->product_quantity;
                $productValidation->product_name = $product->product_name;
                $productValidation->type = $product->type;

                if($product->validation_product == 'Vencido' || $product->validation_product == 'Mal estado'){
                   $productValidation->status_shrinkage = $product->validation_product;
                   if(!$product->quantity)
                        $productValidation->status = 'Trocado';
                    elseif($product->quantity && ($product->fulfilment_status == 'Not Available' || $product->fulfilment_status == 'Fullfilled') && $product->quantity > $totalProduct)
                        $productValidation->status = 'Validado';
                    elseif($product->quantity && ($product->fulfilment_status == 'Not Available' || $product->fulfilment_status == 'Fullfilled') && $product->quantity <= $totalProduct)
                        $productValidation->status = 'Adicional';
                }
                elseif(!$product->quantity)
                        $productValidation->status = 'Trocado';
                    elseif($product->quantity && ($product->fulfilment_status == 'Not Available' || $product->fulfilment_status == 'Fullfilled') && $product->quantity > $totalProduct){
                        $productValidation->status = 'Validado';
                        $this->updateStatusProduct($product);
                    }
                    elseif($product->quantity && ($product->fulfilment_status == 'Not Available' || $product->fulfilment_status == 'Fullfilled') && $product->quantity <= $totalProduct)
                        $productValidation->status = 'Adicional';
            }else{
                $storeProduct = StoreProduct::find($product->store_product_id);
                if(strtolower($storeProduct->storage) != $validation &&  $validation == 'seco'){
                    return false;
                }elseif($validation == 'frio' && !in_array($storeProduct->storage, ['Refrigerado','Congelado'])){
                    return false;
                }
                $productValidation = new OrderPickingQualityDetail();
                $productValidation->order_picking_quality_id = $product->order_picking_quality_id;
                $productValidation->store_product_id = $product->store_product_id;
                $productValidation->product_unit = $product->product_unit;
                $productValidation->product_quantity = $product->product_quantity;
                $productValidation->product_name = $product->product_name;
                $productValidation->type = $product->type;

                if($product->validation_product == 'Vencido' || $product->validation_product == 'Mal estado'){
                   $productValidation->status_shrinkage = $product->validation_product;
                   if(!$product->quantity || $product->validation_product != 'Vencido' && $product->validation_product != 'Mal estado')
                        $productValidation->status = 'Trocado';
                    elseif($product->quantity && ($product->fulfilment_status == 'Fullfilled' || $product->fulfilment_status == 'Not Available'))
                        $productValidation->status = 'Validado';
                }
                else{
                    if(!$product->quantity && $product->validation_product != 'Vencido' && $product->validation_product != 'Mal estado'){
                        $productValidation->status = 'Trocado';
                    }elseif($product->quantity && ($product->fulfilment_status == 'Fullfilled' || $product->fulfilment_status == 'Not Available')){
                            $productValidation->status = 'Validado';
                            $this->updateStatusProduct($product);
                    }
                }
            }
            $productValidation->save();

            $product = [$product];
            return $product;


    }

    /***
     * @param $product
     * @return bool
     */
    private function updateStatusProduct($product)
    {
        if($product->fulfilment_status == 'Not Available'){
            if($product->type == 'Agrupado'){
                $orderProductGroup = OrderProductGroup::where('order_id', $product->order_id)->where('store_product_id', $product->store_product_id)->first();
                $orderProductGroup->fulfilment_status = 'Fullfilled';
                $orderProductGroup->save();

                $orderProduct = OrderProduct::find($orderProductGroup->order_product_id);
                $orderProduct->fulfilment_status = 'Fullfilled';
                $orderProduct->save();
            }else{
                $orderProduct = OrderProduct::where('order_id', $product->order_id)->where('store_product_id', $product->store_product_id)->first();
                $orderProduct->fulfilment_status = 'Fullfilled';
                $orderProduct->save();
            }
            return true;
        }else
            return false;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateValidation($id)
    {

        try {

            DB::beginTransaction();

            $order = Order::find($id);
            if (!$order) {
                DB::rollback();
                return Redirect::route('AdminPickingQuality.index')->with('error', 'No existe el pedido ingresado.');
            }

            if ($order->status == 'Dispatched' || $order->status == 'Delivered') {
                DB::rollback();
                return Redirect::route('AdminPickingQuality.index')->with('error',
                    'No es posible validar calidad de alistamiento al pedido por que se encuentra en estado.' . $order->status);
            }

            $orderProducts = [];
            $orderGroup = OrderGroup::find($order->group_id);
            $validated = 1;
            $validation = Input::get('validation');
            $orderPickingQualityDetail = new OrderPickingQualityDetail();
            $products = $orderPickingQualityDetail->getProductsToUpdateValidate($order, $validation);
            $productGroups = $orderPickingQualityDetail->getProductGroupToUpdateValidate($order, $validation);
            $productsPickingValidated = $orderPickingQualityDetail->getTotalProductsValidatedInOrder($order->id, $validation);

            foreach ($products as $product) {
                foreach ($productGroups as $key => $value) {
                    if($product->store_product_id == $value->store_product_id)
                    {
                        $product->quantity += $value->quantity;
                        unset($productGroups[$key]);
                    }
                }
            }
            foreach($products as $product){
                $orderProducts[] = $product->toArray();
            }
            if ($productGroups){
                foreach($productGroups as $key => $product_group_item){
                    $orderProducts[] = $product_group_item->toArray();
                }
            }
            if(count($productsPickingValidated)){
                foreach ($orderProducts as $product) {
                    if($product['quantity'] > $product['quantity_product_validated'])
                    {
                        $quantity = $product['quantity'] - $product['quantity_product_validated'];
                        if($product['type'] == 'Product'){
                            if(count($product['store_product_id'] > 1 ) && $quantity > 1){
                                $orderProducts = OrderProduct::where('store_product_id', $product['store_product_id'])->where('order_id', $order->id)->where('type', 'Product')->orderBy('quantity', 'DESC')->orderBy('price', 'DESC')->get();
                                foreach ($orderProducts as $orderProduct) {
                                    if($quantity <= $orderProduct['quantity']){
                                        $orderProduct->fulfilment_status = 'Missing';
                                        $orderProduct->save();
                                        break;
                                    }else{
                                        $orderProduct->fulfilment_status = 'Missing';
                                        $orderProduct->save();
                                    }
                                }
                            }else{
                                $orderProduct = OrderProduct::find($product['id']);
                                $orderProduct->fulfilment_status = 'Missing';
                                $orderProduct->save();
                            }
                        }else{
                            $orderProductGroup = OrderProductGroup::find($product['id']);
                            $orderProductGroup->fulfilment_status = 'Missing';
                            $orderProductGroup->save();
                            $orderProduct = OrderProduct::find($orderProductGroup->order_product_id);
                            $orderProduct->fulfilment_status = 'Missing';
                            $orderProduct->save();
                        }
                        for ($i = 1; $i <= $quantity; $i++) {
                            $productValidation = new OrderPickingQualityDetail();
                            $productValidation->type = $product['type'];
                            $productValidation->order_picking_quality_id = $product['order_picking_quality_id'];
                            $productValidation->store_product_id = $product['store_product_id'];
                            $productValidation->product_unit = $product['product_unit'];
                            $productValidation->product_quantity = $product['product_quantity'];
                            $productValidation->product_name = $product['product_name'];
                            $productValidation->status = 'Faltante';
                            $productValidation->save();
                        }
                        $validated = 0;
                        $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($orderGroup->warehouse_id, $product['store_product_id']);
                        $storeProductWarehouse->picked_stock = $storeProductWarehouse->picked_stock - $quantity;
                        $storeProductWarehouse->picking_stock = $storeProductWarehouse->picking_stock + $quantity;
                        $storeProductWarehouse->save();
                        InvestigationStock::create(
                            ['quantity' => $quantity,
                                'warehouse_id' => $orderGroup->warehouse_id,
                                'store_product_id' => $product['store_product_id'],
                                'entity_id' => $id,
                                'module' => "Calidad",
                                'inventory_counting_position_id' => 0,
                                'storage' => "Alistamiento",
                                'position' => $storeProductWarehouse->storage_position,
                                'height_position' => $storeProductWarehouse->storage_height_position,
                                'admin_id' => Session::get('admin_id'),
                                'product_group' => 0]
                        );
                    }elseif($product['status_shrinkage'] == 'Mal estado' || $product['status_shrinkage'] == 'Vencido'){
                        if($product['type'] == 'Product'){
                            $orderProduct = OrderProduct::find($product['id']);
                            $orderProduct->fulfilment_status = 'Missing';
                            $orderProduct->save();
                        }else{
                            $orderProductGroup = OrderProductGroup::find($product['id']);
                            $orderProductGroup->fulfilment_status = 'Missing';
                            $orderProductGroup->save();
                            $orderProduct = OrderProduct::find($orderProductGroup->order_product_id);
                            $orderProduct->fulfilment_status = 'Missing';
                            $orderProduct->save();
                        }
                        $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($orderGroup->warehouse_id, $product['store_product_id']);
                        $storeProductWarehouse->picked_stock = $storeProductWarehouse->picked_stock - $product['quantity_product_validated'];
                        $storeProductWarehouse->shrinkage_stock = $storeProductWarehouse->shrinkage_stock + $product['quantity_product_validated'];
                        $storeProductWarehouse->save();
                        InvestigationStock::create(
                            ['quantity' => $product['quantity_product_validated'],
                                'warehouse_id'=> $orderGroup->warehouse_id,
                                'store_product_id'=> $product['store_product_id'],
                                'entity_id'=> $id,
                                'module'=> "Calidad",
                                'inventory_counting_position_id'=> 0,
                                'storage'=> "Alistamiento",
                                'position'=> $storeProductWarehouse->storage_position,
                                'height_position'=> $storeProductWarehouse->storage_height_position,
                                'admin_id'=> Session::get('admin_id'),
                                'product_group'=> 0]
                        );
                    }
                }
                if(!$validated){
                    $order->status = 'In Progress';
                    $order->save();
                }

                $orderPickingQuality = OrderPickingQuality::where('order_id', $order->id)->first();
                if($validation == 'seco'){
                    $orderPickingQuality->status_dry = 'Validado';
                    $orderPickingQuality->validation_date_end_dry = Carbon::now()->format('Y-m-d H:i:s');
                }
                if($validation == 'frio'){
                    $orderPickingQuality->status_cold = 'Validado';
                    $orderPickingQuality->validation_date_end_cold = Carbon::now()->format('Y-m-d H:i:s');
                }
                $orderPickingQuality->save();

                $log = new OrderLog();
                $log->type = 'Validación de calidad';
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                DB::commit();
                \Log::info('Modificando estado de pedido: '.$order->id. ' a '. $order->status.' desde mòdulo de calidad');
                return Redirect::route('AdminPickingQuality.edit', ['id' => $order->id, 'validation'=> $validation])->with('success', 'Validación de alistamiento actualizada con éxito.');
            }else
                return Redirect::route('AdminPickingQuality.edit', ['id' => $order->id, 'validation'=> $validation])->with('error', 'No es posible actualizar el estado de Validación de Alistamiento. No exiten productos validados.');

        }catch (Exception $e){
            DB::rollback();
            \ErrorLog::add($e, 500);
            \Log::info('Rollback de pedido: '.$id. ' desde mòdulo de calidad');
       }
    }
}
?>
