<?php

namespace admin\config;

use Request, Input, View, Session, Redirect, DB, Response, Validator, AWS, Str, Queue, Config,
    admin\AdminController, ElasticsearchConfig, AWSElasticsearch;

class ElasticSearchConfigController extends AdminController
{
	/**
     * Cargar formulario para editar conf de elasticsearch
     */
    public function index()
    {
        if (!Request::ajax())
        {
        	$configurations = ElasticsearchConfig::get();
        	foreach ($configurations as $conf) {
        		switch($conf->meta_key){
        			case 'create_index':
        				$create_index  = json_encode(json_decode($conf->meta_value), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES );
        				break;
        			case 'mapping_index':
        				$mapping_index  = json_encode(json_decode($conf->meta_value), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES );
        				break;
                    case 'mapping_fields':
                        $mapping_fields  = json_encode(json_decode($conf->meta_value), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES );
                        break;
        		}
        	}
        	
            return View::make('admin.config.elasticsearch.index')
	            		->with('title', 'Configurarción de Elasticsearch')
	            		->with('create_index', $create_index)
	            		->with('mapping_index', $mapping_index);
                        //->with('mapping_fields', $mapping_fields);

        }
    }

    /**
     * Guardar configuración de elasticsearch en bd
     */
    public function save()
    {
    	try{
    		DB::beginTransaction();
	    	$configurations = ElasticsearchConfig::get();
	    	foreach ($configurations as $conf)
	    	{
	    		switch($conf->meta_key)
                {
	    			case 'create_index':
	    				$conf->meta_value = json_encode(json_decode(Input::get('create_index')));
	    				break;
	    			case 'mapping_index':
	    				$conf->meta_value  = json_encode(json_decode(Input::get('mapping_index')));
	    				break;
                    case 'mapping_fields':
                        $conf->meta_value  = json_encode(json_decode(Input::get('mapping_fields')));
                        break;
	    		}
                if($conf->meta_value == "null"){
                    DB::rollBack();
                    return Redirect::route('adminConfig.elasticSearchConfig')->with('error', 'El formato de JSON es incorrecto.');
                }
	    		$conf->save();
	    	}

    		DB::commit();
    		return Redirect::route('adminConfig.elasticSearchConfig')->with('success', 'Se actualizo la configuración de elasticsearch con éxito.');

    	}catch(\Exception $e){
    		DB::rollBack();
    		return Redirect::route('adminConfig.elasticSearchConfig')->with('error', 'Ocurrio un error al actualizar la configuración de elasticsearch.');
    	}
    }

    /**
     * Sincronizar configuración de elasticsearch y actualizar todos los productos en el indice
     */
    public function sync()
    {
        try{
            $AWSElasticsearch = new AWSElasticsearch;
            $configurations = ElasticsearchConfig::get();
            foreach ($configurations as $configuration)
                $data[$configuration->meta_key] = json_decode($configuration->meta_value, true);

            //eliminar y crear indice
            $result = $AWSElasticsearch->delete_index();
            if (array_key_exists('acknowledged', $result) && $result['acknowledged']){
                $result = $AWSElasticsearch->create_index($data['create_index']);
                if (array_key_exists('acknowledged', $result) && $result['acknowledged']){
                    $result = $AWSElasticsearch->mapping_index($data['mapping_index']);
                    if(array_key_exists('acknowledged', $result) && $result['acknowledged']) {

                        //actualizar los productos
                        $AWSElasticsearch->update_products(['all_products' => true]);
                        return Redirect::route('adminConfig.elasticSearchConfig')->with('success', 'Se actualizo el indice de elasticsearch con éxito.');

                    }else throw new Exception('Ocurrio un error al mapear el indice en elasticsearch.', 500);
                }else throw new Exception('Ocurrio un error al crear el indice en elasticsearch.', 500);
            }else throw new Exception('Ocurrio un error al eliminar el indice en elasticsearch.', 500);

        }catch(\Exception $e){
            return Redirect::route('adminConfig.elasticSearchConfig')->with('error', $e->getMessage());
        }
    }

    /*public function set_products($start, $limit = 50){
        $AWSElasticsearch = new AWSElasticsearch;
        echo '<pre>set_products::';
        print_r($result);
        echo '</pre>';
        exit;
        if($result != false){
            $this->set_products($start+$limit, $limit);
        }
    }*/

}