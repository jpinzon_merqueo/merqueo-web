<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 29/05/2018
 * Time: 14:47
 */

namespace admin\config;

use admin\AdminController, Request, Input, View, Response, DeliveryWindow, Order;

class DeliveryWindowController extends AdminController
{
    /**
     * Muestra el listado de las franjas existentes
     *
     * @return mixed
     */
    public function index(){

        if(!Request::ajax()){
            $data = [
                'title' => 'Franjas Horarias',
                'cities' => $this->get_cities()
            ];
            return View::make('admin.config.delivery_windows.index', $data);
        }else{
            $delivery_windows = DeliveryWindow::select('delivery_windows.*', 'cities.city')
                ->join('cities', 'cities.id','=', 'delivery_windows.city_id')
                ->orderBy('delivery_windows.city_id')
                ->orderByRaw("FIELD(delivery_windows.shifts, '".implode("', '", DeliveryWindow::SHIFTS)."')")
                ->orderBy('delivery_windows.hour_start')
                ->orderBy('delivery_windows.status', 'desc');
            if(!empty(Input::get('city_id'))){
                $delivery_windows->where('city_id', Input::get('city_id'));
            }
            if(!empty(Input::get('hour_start'))){
                $delivery_windows->where('hour_start', '>=', Input::get('hour_start'));
            }
            if(!empty(Input::get('hour_end'))){
                $delivery_windows->where('hour_end', '<=', Input::get('hour_end'));
            }
            if(!empty(Input::get('shift'))){
                if(Input::get('shift') != 'MD' && Input::get('shift') != 'EX'){
                    $delivery_windows->whereIn('shifts', [Input::get('shift'), 'AM y PM']);
                }else{
                    $delivery_windows->where('shifts', Input::get('shift'));
                }
            }
            if(Input::has('status') && (Input::get('status') == 1 || Input::get('status') == 0)){
                $delivery_windows->where('delivery_windows.status', Input::get('status'));
            }

            $delivery_windows = $delivery_windows->get();
            $data = [
                'title' => 'Franjas Horarias',
                'delivery_windows' => $delivery_windows
            ];
            return Response::json([
                'status'=> true,
                'result' => View::make('admin.config.delivery_windows.index', $data)->renderSections()['list'],
            ]);
        }
    }

    /**
     * Guarda una nueva franja o edita una existente
     *
     * @return mixed
     */
    public function save()
    {
        if(Input::ajax()){
            try{
                $delivery_window = DeliveryWindow::exist( Input::get('hour_start'), Input::get('hour_end'),Input::get('shifts'), Input::get('city_id'), Input::get('id') );

                if(!$delivery_window['status']){
                    throw new \Exception($delivery_window['message'], 200);
                }

                if(Input::has('id') && !empty(Input::get('id')) ){
                    $delivery_window = DeliveryWindow::find(Input::get('id'));
                    $start_date = new \DateTime( date('Y-m-d').' '.Input::get('hour_start') );
                    $end_date = new \DateTime( date('Y-m-d').' '.Input::get('hour_end') );
                    $delivery_window->city_id = Input::get('city_id');
                    $delivery_window->hour_start = Input::get('hour_start');
                    $delivery_window->hour_end = Input::get('hour_end');
                    $delivery_window->shifts = Input::get('shifts');
                    $delivery_window->delivery_window = Input::get('shifts') == 'EX' ? Input::get('delivery_express') : $start_date->format('g:i a').' - '.$end_date->format('g:i a');
                    $delivery_window->delivery_amount = Input::get('delivery_amount');
                    $delivery_window->status = 1;
                    $delivery_window->save();
                    return Response::json([
                        'status'=> true,
                        'message' => 'Se actualizó la franja con éxito.'
                    ]);
                }else{

                    $delivery_window = new DeliveryWindow;
                    $start_date = new \DateTime( date('Y-m-d').' '.Input::get('hour_start') );
                    $end_date = new \DateTime( date('Y-m-d').' '.Input::get('hour_end') );
                    $delivery_window->city_id = Input::get('city_id');
                    $delivery_window->hour_start = Input::get('hour_start');
                    $delivery_window->hour_end = Input::get('hour_end');
                    $delivery_window->shifts = Input::get('shifts');
                    $delivery_window->delivery_window = Input::get('shifts') == 'EX' ? Input::get('delivery_express') : $start_date->format('g:i a').' - '.$end_date->format('g:i a');
                    $delivery_window->delivery_amount = Input::get('delivery_amount');
                    $delivery_window->status = 1;
                    $delivery_window->save();
                    return Response::json([
                        'status'=> true,
                        'message' => 'Se guardó la franja con éxito.'
                    ]);
                }
            }catch(\Exception $e){
                return Response::json([
                    'status'=> false,
                    'message' => 'Ocurrio un error al guardar la franja. '.$e->getMessage()
                ]);
            }
        }
    }

    /**
     * Actualiza el estado de una franja horaria
     *
     * @return mixed
     */
    public function status()
    {
        if(Request::ajax()){
            try{
                $delivery_window = DeliveryWindow::find(Input::get('id'));
                $delivery_window->status = Input::get('status');
                $delivery_window->save();
                return Response::json([
                    'status'=> true,
                    'message' => 'Se actualizó el estado de la franja con éxito.'
                ]);
            }catch(\Exception $e){
                return Response::json([
                    'status'=> false,
                    'message' => 'Ocurrio un error al actualizar el estado de la franja. '.$e->getMessage()
                ]);
            }
        }
    }

    /**
     * Elimina una franja horaria
     *
     * @return mixed
     */
    public function delete()
    {
        if(Request::ajax()){
            try{
                $orders = Order::where('delivery_window_id', Input::get('id'))->count();
                if ($orders){
                    throw new \Exception('Hay pedidos que tienen asignada esta franja horaria.', 200);
                }
                DeliveryWindow::where('id', Input::get('id'))->delete();
                return Response::json([
                    'status'=> true,
                    'message' => 'Se eliminó la franja con éxito.'
                ]);
            }catch(\Exception $e){
                return Response::json([
                    'status'=> false,
                    'message' => 'Ocurrió un error al eliminar la franja. '.$e->getMessage()
                ]);
            }
        }
    }
}