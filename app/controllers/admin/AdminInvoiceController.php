<?php

namespace admin;

use Anouar\Fpdf\Fpdf;
use Carbon\Carbon;
use exceptions\MerqueoException;
use function foo\func;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use View;
use Response;
use Request;
use Redirect;
use Input;
use Invoice;
use InvoiceDetail;
use CustomerInvoice;
use ProductInvoice;

/**
 * Class AdminInvoiceController
 * @package admin
 */
class AdminInvoiceController extends AdminController
{
    /**
     * @var Almacena la instancia del modelo Invoice
     */
    protected $invoices;

    /**
     * @var Almacena la instancia del modelo InvoiceDetail
     */
    protected $invoiceDetail;

    /**
     * @var Almacena la instancia del modelo Customer
     */
    protected $customerInvoices;

    /**
     * @var Almacena ka instancia del modelo ProductInvoice
     */
    protected $productInvoice;

    /**
     * AdminInvoiceController constructor.
     * @param Invoice $invoice
     * @param CustomerInvoice $customer_invoices
     * @param ProductInvoice $products
     */
    public function __construct(Invoice $invoice, InvoiceDetail $invoice_details, CustomerInvoice $customer_invoices, ProductInvoice $products)
    {
        parent::__construct();
        $this->invoices = $invoice;
        $this->invoiceDetail = $invoice_details;
        $this->customerInvoices = $customer_invoices;
        $this->productInvoice = $products;
    }

    /**
     * Visualiza las facturas manuales
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     * @throws MerqueoException
     */
    public function index()
    {
        if (!Request::ajax()) {
            $responseArray = ['title' => 'Facturas'];
            return View::make('admin.invoices.index', $responseArray);
        }
        try {
            $input = Input::all();
            $filter = $input['filter'];
            $invoices = $this->invoices;
            $invoices = $invoices::join('customer_invoice', 'invoices.customer_invoice_id', '=', 'customer_invoice.id')
                ->select('invoices.*', 'customer_invoice.fullname')
                ->where('invoices.number', 'LIKE', '%' . $filter . '%')
                ->orWhere('customer_invoice.fullname', 'LIKE', '%' . $filter . '%')
                ->get();
            if (!$invoices->keys()) {
                throw new MerqueoException('No se encontraron elementos', 204);
            }
            $responseArray = ['invoices' => $invoices];
            return Response::json($responseArray, 200);
        } catch (MerqueoException $exception) {
            return Response::json($exception->getMessage(), $exception->getCode());
        }
    }

    public function findInvoiceDetails($invoice_id){
        $input = Input::all();
        $excludeIds = (Input::has('excludeIds')) ? json_decode($input['excludeIds']) : [];
        $invoiceDetails = $this->invoiceDetail;
        $invoiceDetails = $invoiceDetails::with('productInvoice')
            ->where('invoice_id', $invoice_id)
            ->where('description', 'LIKE', '%' . $input['boxFilter'] . '%')
            ->whereNotIn('id', $excludeIds)
            ->get();
        $arrayResponse = [
            'productsSearch' => $invoiceDetails
        ];
        return Response::json($arrayResponse);
    }

    /**
     * Visualiza el formulario de creacion de factura manual
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $invoce = $this->invoices;
        $customerInvoices = $this->customerInvoices;
        $products = $this->productInvoice;
        $customerInvoices = $customerInvoices::select('id', 'document_number', 'fullname')->get();
        $products = $products::select('id', 'name')->get();
        $responseArray = ['title' => 'Crear Factura',
            'invoice' => json_encode($invoce),
            'customerInvoices' => json_encode($customerInvoices),
            'products' => json_encode($products),
            'action' => 'create'];
        return View::make('admin.invoices.form', $responseArray);
    }

    /**
     * Crea la factura manual
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     * @throws \PDOException
     */
    public function store()
    {
        try {
            DB::beginTransaction();
            $input = Input::all();
            if(empty($input['products'])) {
                throw new \Exception('Debes añadir al menos un producto a la factura.');
            }
            $invoice = new $this->invoices;
            $invoice->number = $this->generateInvoiceConsecutive();
            $invoice->customer_invoice_id = $input['customer_invoice_id'];
            $invoice->date = $input['date'];
            $invoice->expiration_date = $input['expiration_date'];
            $invoice->payment_conditions = $input['payment_conditions'];
            $invoice->save();
            $products = json_decode($input['products']);
            if($this->storeDetails($products, $invoice->id)) {
                DB::commit();
                return Redirect::route('adminInvoice.index')
                    ->with('success', true)
                    ->with('message', 'Factura generada con exito.');
            }
        } catch (\Exception $exception) {
            $responseArray = [
                'error' => true,
                'message' => $exception->getMessage(),
            ];
            DB::rollBack();
            return Redirect::back()->with($responseArray)->withInput();
        } catch (\PDOException $exception) {
            $responseArray = [
                'error' => true,
                'message' => $exception->getMessage(),
            ];
            DB::rollBack();
            return Redirect::back()->with($responseArray)->withInput();
        }
    }

    /**
     * Crea los productos de la factura
     * @param $products
     * @param $invoice_id
     * @return bool
     */
    private function storeDetails($products, $invoice_id)
    {
        foreach ($products as $product) {
            $invoiceDetail = new $this->invoiceDetail;
            $invoiceDetail->invoice_id = $invoice_id;
            $invoiceDetail->product_invoice_id = $product->id;
            $invoiceDetail->description = $product->description;
            $invoiceDetail->quantity = $product->quantity;
            $invoiceDetail->price = $product->price;
            $invoiceDetail->iva = $product->iva;
            $invoiceDetail->price_iva = $product->price_iva;
            $invoiceDetail->price_tax_fuente = $product->price_tax_fuente;
            $invoiceDetail->price_tax_ica = $product->price_tax_ica;
            $invoiceDetail->price_tax_iva = $product->price_tax_iva;
            $invoiceDetail->save();
        }
        return true;
    }

    /**
     * Visualiza la factura manual
     * @param $id
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     * @throws ModelNotFoundException
     */
    public function view($id)
    {
        try {
            $invoce = $this->invoices;
            $invoce = $invoce::with(['invoiceDetails' => function ($relation) {
                return $relation->with('productInvoice');
            }])->with('customerInvoice')->find($id);
            $invoce->date = format_date('normal', explode(' ', $invoce->date)[0]);
            $invoce->expiration_date = format_date('normal', explode(' ', $invoce->expiration_date)[0]);
            $customerInvoices = $this->customerInvoices;
            $products = $this->productInvoice;
            $customerInvoices = $customerInvoices::select('id', 'document_number', 'fullname')->get();
            $products = $products::select('id', 'name')->get();
            $responseArray = ['title' => 'Factura #' . $invoce->number,
                'invoice_id' => $invoce->id,
                'invoice' => json_encode($invoce),
                'customerInvoices' => json_encode($customerInvoices),
                'products' => json_encode($products)];
            return View::make('admin.invoices.view', $responseArray);
        } catch (\Exception $exception) {
            return Redirect::route('adminInvoice.index')
                ->with('error', true)
                ->with('message', $exception->getMessage());
        } catch (ModelNotFoundException $exception) {
            return Redirect::route('adminInvoice.index')
                ->with('error', true)
                ->with('message', $exception->getMessage());
        }
    }

    /**
     * Genera el consecutivo de la factura manual
     * @return int
     */
    private function generateInvoiceConsecutive()
    {
        $consecutive = DB::table('consecutives')
            ->where('type', 'invoice')
            ->lockForUpdate()
            ->first();
        $returnConsecutive = $consecutive->consecutive;
        $consecutive->consecutive += 1;
        DB::table('consecutives')
            ->where('id', $consecutive->id)
            ->update(['consecutive' => $consecutive->consecutive]);
        return $returnConsecutive;
    }

    /**
     * Actualiza el estado de la factura dinamicamente
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     * @throws ModelNotFoundException
     */
    public function change_status($id)
    {
        try {
            DB::beginTransaction();
            $invoice = $this->invoices;
            $invoice = $invoice::find($id);
            $invoice->status = ($invoice->status) ? 0 : 1;
            $invoice->save();
            DB::commit();
            $responseArray = [
                'message' => 'Se actualizó correctamente el estado de la factura.',
                'success' => true
            ];
            return Redirect::route('adminInvoice.index')->with($responseArray);
        } catch (\Exception $exception) {
            DB::rollBack();
            $responseArray = [
                'message' => $exception->getMessage(),
                'error' => true
            ];
            return Redirect::route('aminInvoice.index')->with($responseArray);
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            $responseArray = [
                'message' => $exception->getMessage(),
                'error' => true
            ];
            return Redirect::route('aminInvoice.index')->with($responseArray);
        }
    }

    /**
     * Exportar factura generada en pdf
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     * @throws ModelNotFoundException
     */
    public function printPdf($id)
    {
        try {
            $invoice = $this->invoices;
            $invoice = $invoice::with('customerInvoice')->find($id);
            if(!$invoice){
                throw new ModelNotFoundException('No se encontro un registro con el id especificado.');
            }
            $products = $invoice->invoiceDetails()->with('productInvoice')->get();

            $fpdf = new Fpdf();
            $fpdf->AddPage();
            $fpdf->SetLeftMargin(6);

            // Logo
            $fpdf->Image(base_path('admin_asset/img/logo-fucsia.png'),10,10,50);
            $fpdf->SetFont('Arial','B',10);
            $fpdf->Cell(75);
            $fpdf->Cell(30,5,'MERQUEO S A S',0,0,'C');
            $fpdf->SetFont('Arial','B',8);
            $fpdf->SetDrawColor(237,237,237);
            $fpdf->Cell(33);
            $fpdf->Cell(52,6,'FACTURA DE VENTA ','LTR',0,'C');
            $fpdf->Ln(6);

            // Información de contacto
            $fpdf->SetFont('Arial','',9);
            $fpdf->Cell(76);
            $fpdf->Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
            $fpdf->Cell(31);
            $fpdf->SetFont('Arial','',8);
            $fpdf->Cell(52,9, $invoice->number,'LBR',0,'C');
            $fpdf->Ln(4);

            $fpdf->Cell(75);
            $fpdf->Cell(40,5,'CLL 97A # 9A - 50 PISO 2',0,0,'C');
            $fpdf->Ln(4);

            $fpdf->Cell(78);
            $fpdf->Cell(30,5,'TEL: 7561938',0,0,'C');
            $fpdf->Ln(4);

            $fpdf->Cell(78);
            $fpdf->Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
            $fpdf->Ln(15);

            /////////////Cliente:////////////
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->SetFillColor(237, 237, 237);
            $fpdf->Cell(15, 5, utf8_decode('Señores'), 0, 0, 'L', true);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->Cell(49, 5, strtoupper_utf8($invoice->customerInvoice->fullname), 1, 0, 'L', false);
            $fpdf->Cell(15);
            $fpdf->Cell(49);
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->Cell(33, 5, utf8_decode('Fecha Comprobante'), 0, 0, 'C', false);
            $fpdf->Cell(33, 5, utf8_decode('Fecha Vencimiento'), 0, 0, 'C', false);
            $fpdf->Ln(5);

            /////////////Transportador//////////////
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->SetFillColor(237, 237, 237);
            $fpdf->Cell(15, 5, utf8_decode($invoice->customerInvoice->document_type), 0, 0, 'L', true);
            $fpdf->SetFont('Arial', '', 8);
            $fpdf->Cell(49, 5, utf8_decode($invoice->customerInvoice->document_number), 1, 0, 'L', false);

            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->SetFillColor(237, 237, 237);
            $fpdf->Cell(15, 5, utf8_decode('Teléfono'), 0, 0, 'L', true);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->Cell(49, 5, utf8_decode($invoice->customerInvoice->phone), 1, 0, 'L', false);
            $fpdf->Cell(33, 5, utf8_decode(Carbon::parse($invoice->date)->toDateString()), 0, 0, 'C', false);
            $fpdf->Cell(33, 5, utf8_decode(Carbon::parse($invoice->expiration_date)->toDateString()), 0, 0, 'C', false);
            $fpdf->Ln(5);


            /////////////Placa//////////////////
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->SetFillColor(237, 237, 237);
            $fpdf->Cell(15, 5, utf8_decode('Dirección'), 0, 0, 'L', true);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->Cell(49, 5, utf8_decode($invoice->customerInvoice->address), 1, 0, 'L', false);

            $fpdf->Ln(5);
            $fpdf->Ln(14);

            /////////////Detalle de la factura ////////
            $fpdf->SetDrawColor(210, 210, 210);
            $fpdf->SetFillColor(241, 241, 241);
            $fpdf->SetFont('Arial', 'B', 8);

            $fpdf->Cell(40, 5, utf8_decode('Producto'), 1, 0, 'C', true);
            $fpdf->Cell(15, 5, utf8_decode('Cantidad'), 1, 0, 'C', true);
            $fpdf->Cell(15, 5, utf8_decode('Valor'), 1, 0, 'C', true);
            $fpdf->Cell(10, 5, utf8_decode('IVA'), 1, 0, 'C', true);
            $fpdf->Cell(20, 5, utf8_decode('Valor IVA'), 1, 0, 'C', true);
            $fpdf->Cell(25, 5, utf8_decode('Valor Rete Fuente'), 1, 0, 'C', true);
            $fpdf->Cell(25, 5, utf8_decode('Valor Rete ICA'), 1, 0, 'C', true);
            $fpdf->Cell(25, 5, utf8_decode('Valor Rete IVA'), 1, 0, 'C', true);
            $fpdf->Cell(20, 5, utf8_decode('Valor Total'), 1, 0, 'C', true);
            $fpdf->Ln(5);

            $subTotal = 0;
            $totalIva = 0;
            $totalTaxFuente = 0;
            $totalTaxica = 0;
            $totalTaxIva = 0;
            $totalAmount = 0;
            foreach ($products as $product) {
                $subTotal += $product->quantity * $product->price;
                $totalIva += $product->price_iva;
                $totalTaxFuente += $product->price_tax_fuente;
                $totalTaxica += $product->price_tax_ica;
                $totalTaxIva += $product->price_tax_iva;
                $total_product = ($product->quantity * $product->price) + $product->price_iva -
                    ($product->price_tax_fuente + $product->price_tax_ica + $product->price_tax_iva);
                $totalAmount += $total_product;
                $fpdf->SetFont('Arial', '', 7);
                $fpdf->Cell(40, 5, utf8_decode($product->productInvoice->name), 1, 0, 'L', false);
                $fpdf->Cell(15, 5, utf8_decode($product->quantity), 1, 0, 'C', false);
                $fpdf->Cell(15, 5, (number_format($product->price, 2)), 1, 0, 'L', false);
                $fpdf->Cell(10, 5, (number_format($product->iva, 2)), 1, 0, 'R', false);
                $fpdf->Cell(20, 5, (number_format($product->price_iva, 2)), 1, 0, 'C', false);
                $fpdf->Cell(25, 5, (number_format($product->price_tax_fuente, 2)), 1, 0, 'C', false);
                $fpdf->Cell(25, 5, (number_format($product->price_tax_ica, 2)), 1, 0, 'C', false);
                $fpdf->Cell(25, 5, (number_format($product->price_tax_iva, 2)), 1, 0, 'C', false);
                $fpdf->Cell(20, 5, (number_format($total_product, 2)), 1, 0, 'C', false);
                $fpdf->Ln(5);
            }

            $fpdf->Ln(5);
            $fpdf->Cell(115);
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->Cell(51, 5, 'SubTotal', 1, 0, 'L', true);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->Cell(29, 5, number_format($subTotal, 2), 1, 0, 'R', false);
            $fpdf->Ln(5);
            if ($totalIva > 0) {
                $fpdf->Cell(115);
                $fpdf->SetFont('Arial', 'B', 8);
                $fpdf->Cell(51, 5, utf8_decode('IVA'), 1, 0, 'L', true);
                $fpdf->SetFont('Arial', '', 7);
                $fpdf->Cell(29, 5, number_format($totalIva, 2), 1, 0, 'R', false);
                $fpdf->Ln(5);
            }
            if ($totalTaxFuente > 0) {
                $fpdf->Cell(115);
                $fpdf->SetFont('Arial', 'B', 8);
                $fpdf->Cell(51, 5, utf8_decode('Retención en la Fuente'), 1, 0, 'L', true);
                $fpdf->SetFont('Arial', '', 7);
                $fpdf->Cell(29, 5, number_format($totalTaxFuente, 2), 1, 0, 'R', false);
                $fpdf->Ln(5);
            }
            if ($totalTaxica > 0) {
                $fpdf->Cell(115);
                $fpdf->SetFont('Arial', 'B', 8);
                $fpdf->Cell(51, 5, utf8_decode('Retención ICA'), 1, 0, 'L', true);
                $fpdf->SetFont('Arial', '', 7);
                $fpdf->Cell(29, 5, number_format($totalTaxica, 2), 1, 0, 'R', false);
                $fpdf->Ln(5);
            }
            if ($totalTaxIva > 0) {
                $fpdf->Cell(115);
                $fpdf->SetFont('Arial', 'B', 8);
                $fpdf->Cell(51, 5, utf8_decode('Retención IVA'), 1, 0, 'L', true);
                $fpdf->SetFont('Arial', '', 7);
                $fpdf->Cell(29, 5, number_format($totalTaxIva, 2), 1, 0, 'R', false);
                $fpdf->Ln(5);
            }
            $fpdf->Cell(115);
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->Cell(51, 5, 'Total', 1, 0, 'L', true);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->Cell(29, 5, number_format($totalAmount, 2), 1, 0, 'R', false);
            $fpdf->Ln(10);


            $fpdf->SetFont('Arial', 'B', 7);
            $fpdf->Cell(40, 5, utf8_decode('CONDICION DE PAGO'), 0, 0, 'L', false);
            $fpdf->Ln(4);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->MultiCell(195, 5, utf8_decode($invoice->payment_conditions), 0, 'L', false);
            $fpdf->Ln(5);


            $fpdf->SetFont('Arial', 'B', 7);
            $fpdf->Cell(40, 5, utf8_decode('VALOR EN LETRAS'), 0, 0, 'L', false);
            $fpdf->Ln(4);
            $fpdf->SetFont('Arial', '', 7);
            $fpdf->MultiCell(195, 5, utf8_decode(convert_number_to_word($totalAmount) . ' M/Cte'), 0, 'L', false);
            $fpdf->Ln(5);

            $fpdf->SetY(-56);
            $fpdf->SetLeftMargin(20);
            $fpdf->SetFont('Arial', 'B', 7);
            $fpdf->Cell(120, 5, utf8_decode('ELABORADO POR'), 0, 0, 'L', false);
            $fpdf->SetFont('Arial', 'B', 7);
            $fpdf->Cell(65, 5, utf8_decode('APROBADO POR'), 0, 0, 'L', false);
            $fpdf->Ln(10);

            $fpdf->Cell(120, 5, '___________________________________', 0, 0, 'L');
            $fpdf->Cell(65, 5, '___________________________________', 0, 0, 'L');
            $fpdf->Ln(5);

            $fpdf->Ln(7);

            $fpdf->SetY(-38);
            $fpdf->SetAutoPageBreak(true, 1);
            $fpdf->SetLeftMargin(6);
            $fpdf->Cell(170,5,'FACTURA IMPRESA POR COMPUTADOR GRACIAS POR PREFERIRNOS',0,0,'C');
            $fpdf->Ln(7);

            $fpdf->SetFont('Arial','',8);
            $fpdf->SetFillColor(237, 237, 237);
            $resolution = '18762006848873';

            $fpdf->MultiCell(195,5,utf8_decode('A esta factura de venta aplican las normas relativas a la letra de cambio (artículo 5 Ley 1231 de 2008). Con esta el Comprador declara haber
    recibido real y materialmente las mercancías o prestación de servicios descritos en este título - Valor. N° de Resolución '.$resolution.'
    aprobado en 2017-03-08 desde el N° 1 al 400000 y N° Resolución '.$resolution.' 
    aprobado en 2018-02-09 desde el N° 400001 al 600000.
    Regimen Común - Actividad Económica 4631 Tarifa 004.140 y 7310 Tarifa 009.660'),1,'C',true);

            $fpdf->SetFont('Arial','B',8);
            $fpdf->Cell(175,5,'Original',0,0,'C');
            $fpdf->SetFont('Arial','I',8);
            $fpdf->SetAutoPageBreak(true, 1);
            $fpdf->Cell(20,5,utf8_decode('Página ') . $fpdf->PageNo(),0,0,'C');
            $fpdf->Output();
            exit;
        } catch (\Exception $exception) {
            $responseArray = [
                'message' => $exception->getMessage(),
                'error' => true
            ];
            return Redirect::route('adminInvoice.index')->with($responseArray);
        } catch (ModelNotFoundException $exception) {
            $responseArray = [
                'message' => $exception->getMessage(),
                'error' => true
            ];
            return Redirect::route('adminInvoice.index')->with($responseArray);
        }
    }
}
