<?php

namespace admin;

use Request, Input, View, Session, Redirect, DB, City, Admin, Role, Hash, RolePermission, Menu, MenuItem,
    CustomerService, Store, Response, Warehouse;

class AdminUserController extends AdminController
{
	/**
     * Grilla de usuarios administradores
     */
    public function index()
    {
        if (!Request::ajax()){
            $user_admins = Admin::select('admin.*', 'cities.city')->join('cities', 'cities.id', '=', 'admin.city_id');

            if (Session::get('admin_designation') != 'Super Admin')
                $user_admins->where('admin.city_id', Session::get('admin_city_id'));

            $user_admins = $user_admins->get();

            $data = [
                'title' => 'Usuarios',
                'user_admins' => $user_admins
            ];

            return View::make('admin.users.index', $data);
        }else{
            $search = Input::has('s') ? Input::get('s') : '';
            $user_admins = Admin::select('admin.*', 'cities.city')
                                ->where(function($query) use ($search){
                                    $query->where('admin.fullname', 'like', '%'.$search.'%');
                                    $query->orWhere('admin.username', 'like', '%'.$search.'%');
                                })
                                ->where('admin.status', '=', Input::get('status'))
                                ->join('cities', 'cities.id', '=', 'admin.city_id');

            if (Session::get('admin_designation') != 'Super Admin')
                $user_admins->where('admin.city_id', Session::get('admin_city_id'));

            $user_admins = $user_admins->limit(80)->get();

            $data = [
                'title' => 'User Admins',
                'user_admins' => $user_admins
            ];
            return View::make('admin.users.index', $data)->renderSections()['content'];
        }
    }

    /**
     * Editar usuario administrador
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Usuario Administrador' : 'Agregar Usuario Administrador';
        $user_admin = Admin::find($id);
        $roles = Role::all();
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->get();
        if (Session::get('admin_designation') != 'Super Admin')
            $cities = $this->get_cities();
        else $cities = $this->get_cities();
        if ($id)
            $warehouses = Warehouse::where('city_id', $user_admin->city_id)->where('status', 1)->orderBy('warehouse')->get();
        else $warehouses = Warehouse::where('city_id', Session::get('admin_city_id'))->where('status', 1)->orderBy('warehouse')->get();
        return View::make('admin.users.user_form')
                ->with('title', 'Usuario Administrador')
                ->with('sub_title', $sub_title)
                ->with('cities', $cities)
                ->with('roles', $roles)
                ->with('stores', $stores)
                ->with('warehouses', $warehouses)
                ->with('user_admin', $user_admin);
    }

    /**
     * Guardar usuario administrador
     */
    public function save()
    {
        $id = Input::get('id');
        $user_admin = Admin::find($id);
        if (!$user_admin){
            $user_admin = new Admin;
            if (Admin::where('username', Input::get('username'))->first())
                return Redirect::back()->with('error', 'Ya existe un usuario con el mismo email.')->with('post', Input::all());
            $action = 'insert';
        }else $action = 'update';
        $user_admin->fullname = Input::get('fullname');
        $user_admin->phone = Input::get('phone');
        $user_admin->username = Input::get('username');
        $user_admin->designation = Input::get('designation');
        $user_admin->city_id = Input::get('city_id');
        $user_admin->designation_store_id = Input::get('store_id');
        $user_admin->warehouse_id = Input::get('warehouse_id');

        if (in_array(Input::get('role_id'), Admin::ACCOUNTING_ROLE_IDS) && !in_array(Session::get('admin_id'), Admin::ALLOWED_ADMIN_IDS)){
            return Redirect::back()->with('error', 'No tienes permisos para asignar este rol, por favor contacta con el administrador.')->with('post', Input::all());
        }

        if ( ($rules = Input::get('rules')) && ($orders_limit = Input::get('orders_limit')) ) {
            $json_rules['rules'] = $rules;
            $json_rules['orders_limit'] = $orders_limit;
            $json_rules = json_encode($json_rules);
            $user_admin->rules = $json_rules;
        }

        if (Input::get('role_id') != $user_admin->role_id && $role = Role::find(Input::get('role_id')))
            $user_admin->permissions = $role->permissions;
        $user_admin->role_id = Input::get('role_id');
        $user_admin->image_url = web_url().'/admin_asset/img/avatar.jpg';
        if(Input::has('password')){
            $password = Input::get('password');
            if (!empty($password))
                $user_admin->password = Hash::make(Input::get('password'));
        }
        $user_admin->status = Input::get('status');
        $user_admin->save();
        $this->admin_log('admin', $user_admin->id, $action);

        return Redirect::route('adminUsers.index')->with('type', 'success')->with('message', 'Usuario actualizado con éxito.');
    }

    /**
     * Eliminar usuario administrador
     */
    public function delete()
    {
        $id = Request::segment(4);
        $user_admin = Admin::find($id);
        if ($user_admin){
            Admin::where('id', $id)->update(array('status' => 0));
            $this->admin_log('admin', $id, 'delete');
            $message = 'Usuario eliminado con éxito.';
            $type = 'success';
        }else{
            $message = 'El usuario no existe.';
            $type = 'failed';
        }

        return Redirect::route('adminUsers.index')->with('type', $type)->with('message', $message);
    }

    /**
     * Editar permisos de usuario
     */
    public function edit_permissions()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $user_admin = Admin::find($id);
        $sub_title = 'Edit Permissions';
        $menu = Menu::find(1);
        $permissions = RolePermission::all();
        $permission_descriptions = array();
        $rows = MenuItem::select('id', 'permission1_description', 'permission2_description', 'permission3_description')->get();
        foreach($rows as $row)
            $permission_descriptions[$row->id] = $row->toArray();

        $data = [
            'title' => $sub_title,
            'user_admin' => $user_admin,
            'user_permissions' => json_decode($user_admin->permissions, true),
            'permissions' => $permissions,
            'menu' => json_decode($menu->menu_order),
            'permission_descriptions' => $permission_descriptions
        ];

        return View::make('admin.users.form_permissions', $data);
    }

    /**
     * Guardar role
     */
    public function save_permissions()
    {
        $id = Input::get('id');
        $user_admin = Admin::find($id);
        $permissions = RolePermission::all();
        $menu_items  = MenuItem::all();
        $admin_permissions = [];
        foreach ($menu_items as $key => $item) {
            if (Input::get('permissions_'.$item->id)) {
                $modules = Input::get('permissions_'.$item->id);
                foreach ($permissions as $key2 => $permission) {
                    if (in_array($permission->id, $modules))
                        $admin_permissions[$item->id][strtolower($permission->name)] = 1;
                    else $admin_permissions[$item->id][strtolower($permission->name)] = 0;
                }
            }
        }
        $user_admin->permissions = json_encode($admin_permissions);
        $user_admin->update_menu = TRUE;
        $user_admin->save();
        $this->admin_log('admin', $user_admin->id, 'update_permissions');

        return Redirect::route('adminUsers.edit', ['id' => $user_admin->id])->with('success', 'Permisos actualizados con éxito.');
    }

    /**
     * Editar reglas para el usuario
     * @param  int $id Identificador del usuario
     */
    public function edit_rules($id)
    {
        $rules = CustomerService::all();
        $user_admin = Admin::find($id);
        $user_rules = json_decode($user_admin->rules, true);
        $user_config = json_decode($user_admin->config, true);
        $sub_title = 'Editar Reglas';
        $data = [
            'title' => $sub_title,
            'user_admin' => $user_admin,
            'user_rules' => $user_rules,
            'user_config' => $user_config,
            'rules' => $rules
        ];
        return View::make('admin.users.form_rules', $data);
    }

    /**
     * Guardar reglas de usuario
     * @param  int $id identificador de usuario
     * @return [type]     [description]
     */
    public function save_rules($id)
    {
        $user_admin = Admin::find($id);
        if ( ($rules = Input::get('rules')) && ($orders_limit = Input::get('orders_limit')) ) {
            for ($i=0; $i < count($rules) ; $i++)
                $rules[$i] = (int)$rules[$i];

            $json_rules['rules'] = $rules;
            $json_config['orders_limit'] = (int)$orders_limit;
            $json_rules = json_encode($json_rules);
            $json_config = json_encode($json_config);
            $user_admin->rules = $json_rules;
            $user_admin->config = $json_config;
            $user_admin->save();
        }
        return Redirect::route('adminUsers.edit', ['id' => $user_admin->id])->with('success', 'Reglas actualizadas con éxito.');
    }

    /**
     * Grilla de roles
     */
    public function roles()
    {
        $roles = Role::all();
        $permissions = RolePermission::all();
        $data = [
            'title'       => 'Roles',
            'roles'       => $roles,
            'permissions' => $permissions
        ];

        return View::make('admin.users.roles', $data);
    }


    /**
     * Editar role
     */
    public function edit_role()
    {
        $permissions = RolePermission::all();
        $menu = Menu::find(1);
        $menu = json_decode($menu->menu_order);

        $id = Request::segment(5) ? Request::segment(5) : 0;
        $sub_title = Request::segment(5) ? 'Editar Rol' : 'Nuevo Rol';
        $role = Role::find($id);

        $data = [
            'title'       => $sub_title,
            'role'        => $role,
            'permissions' => $permissions,
            'menu'        => $menu
        ];
        return View::make('admin.users.form_role', $data);
    }

    /**
     * Guardar role
     */
    public function save_role()
    {
        $id = Input::get('id');
        $role = Role::find($id);
        if(!$role){
            $role = new Role;
            $action = 'insert';
        }else $action = 'update';
        $name = Input::get('name');
        $description = Input::get('description');
        $permissions = Input::get('permissions');

        $role->name = $name;
        $role->description = $description;
        $permissions = RolePermission::all();
        $menu_items  = MenuItem::all();
        $arr = [];
        foreach ($menu_items as $key => $item) {
            if (Input::get('permissions_'.$item->id)) {
                $modules = Input::get('permissions_'.$item->id);
                foreach ($permissions as $key2 => $permission) {
                    if ( in_array($permission->id, $modules) ) {
                        $arr[$item->id][strtolower($permission->name)] = 1;
                    }else{
                        $arr[$item->id][strtolower($permission->name)] = 0;
                    }
                }
            }
        }
        $role->permissions = json_encode($arr);
        $role->save();
        $this->admin_log('roles', $role->id, $action);

        return Redirect::route('adminUsers.roles')->with('success', 'Rol actualizado con éxito.');
    }

    /**
     * Eliminar role
     */
    public function delete_role()
    {
        $id = Request::segment(5);
        $role = Role::find($id);
        if ($role){
            Role::where('id', $id)->delete();
            $this->admin_log('roles', $id, 'delete');
            $message = 'Rol eliminado con éxito.';
            $type = 'success';
        }else{
            $message = 'El rol no existe.';
            $type = 'failed';
        }
        return Redirect::route('adminUsers.roles')->with('type', $type)->with('message', $message);
    }

     /**
     * Obtener tiendas por ciudad ajax
     */
    public function get_stores_ajax()
    {
        $stores = array();
        if (Input::has('city_id'))
            $stores = Store::select('id', 'name')->where('city_id', Input::get('city_id'))->where('status', 1)->get()->toArray();
        return Response::json($stores, 200);
    }

    /**
     * Obtener las bodegas por ciudad
     * @return string
     */
    public function get_warehouses_by_city_ajax()
    {
        $warehouses = [];
        $res['status'] = 400;
        $res['message'] = "Error al consultar las bodegas";
        if (Input::has('city_id')) {
            $warehouses = Warehouse::where('status', 1)
                ->where('city_id', Input::get('city_id'))
                ->get();
            $res['status'] = 200;
            $res['message'] = "Bodegas por ciudad";
        }
        $res['result']['warehouses'] = $warehouses;
        return json_encode($res);
    }
}
