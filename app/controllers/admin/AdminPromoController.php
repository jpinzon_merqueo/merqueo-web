<?php

namespace admin;

use Request, Input, View, Session, Redirect, Response, Product, City, DB, Store, Discount, Queue, StoreProduct, AWSElasticsearch, Config;

class AdminPromoController extends AdminController
{
    /**
     * Grilla de log de promociones
     */
    public function index()
    {
        $admin_permissions = json_decode(Session::get('admin_permissions'), true);
        $permission = empty($admin_permissions['adminproduct']['permissions']['permission1'])
            ? false : $admin_permissions['adminproduct']['permissions']['permission1'];
        $cities = $this->get_cities();
        $stores = Store::where('status', 1)->get();
        $discounts = Discount::leftJoin('departments', 'discounts.department_id', '=', 'departments.id')
                                ->leftJoin('shelves', 'discounts.shelf_id', '=', 'shelves.id')
                                ->leftJoin('store_products', 'discounts.store_product_id', '=', 'store_products.id')
                                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                ->select('discounts.*', 'departments.name AS department', 'shelves.name AS sherlf', 'products.name AS product')
                                ->get()->toArray();

        foreach ($discounts as $key => &$discount) {
            if ( !empty($discount['store_ids']) ) {
                $discount['store_ids'] = explode(',', $discount['store_ids']);
                $stores = Store::whereIn('id', $discount['store_ids'])->select('name')->get()->toArray();
                $discount['store'] = $stores;
            }
            if ( !empty($discount['city_id']) ) {
                $discount['city_id'] = explode(',', $discount['city_id']);
                $cities_discount = City::whereIn('id', $discount['city_id'])->select('city')->get()->toArray();
                $discount['city'] = $cities_discount;
            }
        }

        $data = [
            'title'     => 'Promociones',
            'cities'    => $cities,
            'stores'    => $stores,
            'discounts' => $discounts,
            'permission'=> $permission
        ];
        return View::make('admin.promo.index', $data);
    }

    /**
     * Función que agregar o elimina promociones desde un CSV o desde formulario html
     */
    public function save()
    {
        $inputs = Input::all();
        $admin_permissions = json_decode(Session::get('admin_permissions'), true);

        if ((int)$inputs['action'] === 1) {
            $permission = empty($admin_permissions['adminproduct']['permissions']['permission1'])
                ? false : $admin_permissions['adminproduct']['permissions']['permission1'];
            if($permission){
                $merqueo_discount = intval(Input::get('merqueo_discount')[0]);
            } else {
                $merqueo_discount = 0;
            }
            $provider_discount = intval(Input::get('provider_discount')[0]);
            $seller_discount = intval(Input::get('seller_discount')[0]);

            if ($merqueo_discount + $provider_discount + $seller_discount !== 100) {
                return Redirect::back()->with('error', 'La distribución de la promoción no es correcta.');
            }
        }


        // Cargar promociones desde un CSV
        if ( !empty($inputs['csv_file']) ) {
            $path = Input::file('csv_file')->getRealPath();
            $text = fopen($path, 'r');
            $row_number = 0;
            $row_number_error = 0;
            DB::beginTransaction();
            while (!feof($text))
            {
                $row = fgetcsv($text, 1000, ';');
                if (!empty($row))
                {
                    $data = array_map(function ($item) {
                        return trim($item);
                    }, $row);

                    if (
                        count($data) == 12
                        && !empty($data[0])
                        && !empty($data[1])
                        && !empty($data[2])
                        && !empty($data[3])
                        && isset($data[4])
                        && isset($data[5])
                        && isset($data[6])
                        && isset($data[7])
                        && isset($data[8])
                        && isset($data[9])
                        && isset($data[10])
                        && isset($data[11])
                    ) {
                        $store_product = StoreProduct::select('store_products.*')
                            ->join('products', 'products.id', '=', 'store_products.product_id')
                            ->where('products.type', 'Simple')
                            ->where('products.reference', $data[0])
                            ->where('store_id', (int)$data[1])
                            ->first();

                        // Configurar procentaje de descuento
                        if ( (int)$inputs['action'] === 1 && !is_null($store_product) ) {
                            $store_product->department_id = $data[2];
                            $store_product->shelf_id = $data[3];
                            if((int)$inputs['typeImportation'] === 1){
                                if ( $data[5] + $data[6] + $data[7] > 100 ) {
                                    DB::rollback();
                                    return Redirect::back()->with('error', 'No puede asignar un porcentaje de descuento superior al 100%.');
                                }
                                $percentage = 1 - ($data[4]/100);
                                $store_product->special_price = round($store_product->price * $percentage);
                            }elseif((int)$inputs['typeImportation'] === 2){
                                $store_product->special_price = round_price($data[4]);
                            }

                            $store_product->quantity_special_price = $data[8];
                            $store_product->first_order_special_price = $data[9];
                            $store_product->merqueo_discount = $data[5];
                            $store_product->provider_discount = $data[6];
                            $store_product->seller_discount = $data[7];
                            $store_product->special_price_starting_date= !empty($data[10]) ? $data[10] : NULL;
                            $store_product->special_price_expiration_date = !empty($data[11]) ? $data[11] : NULL;
                            $store_product->save();
                            $row_number++;

                            //Queue::push('UpdateIndex', array('productId' => $store_product->id));

                        }else if( (int)$inputs['action'] === 0 && !is_null($store_product) ){
                            // Quitar precio de descuento
                            $store_product->department_id = $data[2];
                            $store_product->shelf_id = $data[3];
                            $store_product->special_price = NULL;
                            $store_product->quantity_special_price = NULL;
                            $store_product->first_order_special_price = 0;
                            $store_product->merqueo_discount = null;
                            $store_product->provider_discount = null;
                            $store_product->seller_discount = null;
                            $store_product->special_price_expiration_date = NULL;
                            $store_product->special_price_starting_date = NULL;
                            $store_product->save();
                            $row_number++;

                            //Queue::push('UpdateIndex', array('productId' => $store_product->id));

                        }else{
                            DB::rollback();
                            $product = Product::where('reference', $data[0])->first();

                            return Redirect::back()
                                ->with(
                                    'error',
                                    !empty($product->type) && $product->type !== 'Simple'
                                        ? "En los combos se debe asignar manualmente la promoción ({$product->fullName})."
                                        : 'No se encontró el producto con referencia '.$data[0].' en la tienda '.$data[1].'.'
                                );
                        }
                        unset($product);
                    }else{
                        $row_number_error++;
                    }
                }
            }
            fclose($text);
            if ( $row_number_error > 0 ) {
                if ($row_number_error == 1){
                    DB::rollback();
                    return Redirect::back()->with('error', 'Se encontró '.$row_number_error.' error en tu archivo.');
                }
                DB::rollback();
                return Redirect::back()->with('error', 'Se encontraron '.$row_number_error.' errores en tu archivo.');
            }
            $this->admin_log('store_products', NULL, 'update_promo');
            DB::commit();
            if ( (int)$inputs['action'] === 1 )
                return Redirect::back()->with('success', 'Se ha aplicado el descuento a '.$row_number.' productos.');
            return Redirect::back()->with('success', 'Se ha eliminado el descuento a '.$row_number.' productos.');
        }

        // Cargar promociones desde formulario html
        $store_product = DB::table('store_products');
        if (!empty($inputs['store_id'])) {
            $store_product = $store_product->where('store_id', (int)$inputs['store_id']);
            $aws_elasticsearch_data = ['field' => 'store_id', 'value' => (int)$inputs['store_id']];
        }
        if (!empty($inputs['department_id'])) {
            $store_product = $store_product->where('department_id', (int)$inputs['department_id']);
            $aws_elasticsearch_data = ['field' => 'department_id', 'value' => (int)$inputs['department_id']];
        }
        if (!empty($inputs['shelf_id'])) {
            $store_product = $store_product->where('shelf_id', (int)$inputs['shelf_id']);
            $aws_elasticsearch_data = ['field' => 'shelf_id', 'value' => (int)$inputs['shelf_id']];
        }
        if ( (int)$inputs['action'] === 1) {
            if ( !empty($inputs['quantity_special_price']) && isset($inputs['first_order_special_price']) && (!empty($inputs['percentage']) || !empty($inputs['fixed_price'])) ) {
                $update['quantity_special_price'] = $inputs['quantity_special_price'];
                $update['first_order_special_price'] = $inputs['first_order_special_price'];
                $update['merqueo_discount'] = $merqueo_discount;
                $update['provider_discount'] = $provider_discount;
                $update['seller_discount'] = $seller_discount;
                if(!empty($inputs['percentage'])){
                    $percentage = 1 - ($inputs['percentage']/100);
                    $update['special_price'] = DB::raw('ROUND(price * '.$percentage.')');
                } else {
                    if (!empty($inputs['fixed_price'])) {
                        $update['special_price'] = $inputs['fixed_price'];
                    }
                }
            } else {
                return Redirect::back()->with('error', 'No se tienen los campos obligatorios, intentelo de nuevo.');
            }

            if (!empty($inputs['special_price_expiration_date'])) {
                $update['special_price_expiration_date'] = $inputs['special_price_expiration_date'];
            }
            if (!empty($inputs['special_price_starting_date'])) {
                $update['special_price_starting_date'] = $inputs['special_price_starting_date'];
            }
            $resul = $store_product->update($update);
            $store_product_id = $store_product->select('id')->get();
            if ( count($store_product_id) ) {
                $ids_array = json_decode(json_encode($store_product_id), True);
                $ids_array = array_fetch($ids_array, 'id');
                //Queue::push('BatchUpdateIndex', array('store_products' => $ids_array ));
            }
            $this->admin_log('store_products', NULL, 'update_promo');
            return Redirect::back()->with('success', 'Se ha aplicado el descuento a '.$resul.' productos.');
        } else {
            $update['quantity_special_price'] = NULL;
            $update['first_order_special_price'] = 0;
            $update['merqueo_discount'] = 0;
            $update['seller_discount'] = 0;
            $update['provider_discount'] = 0;
            $update['special_price'] = NULL;
            $update['special_price_expiration_date'] = NULL;
            $update['special_price_starting_date'] = NULL;
            $result = $store_product->update($update);
            $store_product_id = $store_product->select('id')->get();
            if ( count($store_product_id) ) {
                $ids_array = json_decode(json_encode($store_product_id), True);
                $ids_array = array_fetch($ids_array, 'id');
                //Queue::push('BatchUpdateIndex', array('store_products' => $ids_array ));
            }
            $this->admin_log('store_products', NULL, 'update_promo');
            return Redirect::back()->with('success', 'Se ha BORRADO el descuento a '.$result.' productos.');
        }

        $result = $store_product->update($update);
        $this->admin_log('store_products', NULL, 'update_promo');

        if (Config::get('app.aws.elasticsearch.is_enable')){
            $aws_elasticsearch = new AWSElasticsearch();
            $aws_elasticsearch->update_products([$aws_elasticsearch_data['field'] => $aws_elasticsearch_data['value']]);
        }

        return Redirect::back()->with('success', 'Cambio realizado con exitó a '.$result.' productos.');
    }

    /**
     *
     * Guarda los descuentos configurados
     */
    public function save_discount()
    {
        $type = Input::get('type');
        $order_for_tomorrow = Input::get('order_for_tomorrow', null);
        $city_id = Input::get('city_id', null);
        $city_id = ( empty($city_id) ? null : $city_id );
        $store_id = Input::get('store_id', null);
        $store_id = ( empty($store_id) ? null : $store_id );
        $department_id = Input::get('department_id', null);
        $department_id = ( empty($department_id) ? null : $department_id );
        $shelf_id = Input::get('shelf_id', null);
        $shelf_id = ( empty($shelf_id) ? null : $shelf_id );
        $store_product_id = Input::get('store_product_id', null);
        $store_product_id = ( empty($store_product_id) ? null : $store_product_id );
        $minimum_order_amount = Input::get('minimum_order_amount', null);
        $minimum_order_amount = ( empty($minimum_order_amount) ? null : $minimum_order_amount );
        $maximum_order_amount = Input::get('maximum_order_amount', null);
        $maximum_order_amount = ( empty($maximum_order_amount) ? null : $maximum_order_amount );
        $order_amount_discount = Input::get('order_amount_discount', null);
        $order_amount_discount = ( empty($order_amount_discount) ? null : $order_amount_discount );
        if ( !empty($store_id) && is_array($store_id) ) {
            $store_id = implode(',', $store_id);
        }
        $get_converage = function(&$city) {
            $extra = City::where('coverage_store_id', '=', $city->coverage_store_id)
                ->whereNotIn('id', array($city->id))
                ->orderBy('id', 'ASC')
                ->get()
                ->toArray();
            $city->coverage = count($extra) ? implode(',', array_pluck($extra, 'id')) : '';
        };
        if (!empty($city_id) && is_array($city_id)) {
            $cities = City::whereIn('id', $city_id)
                ->orderBy('id', 'ASC')
                ->get();
            $cities->each(function($city) use ($get_converage) {
                $get_converage($city);
            });
            $cities = $cities->toArray();
            $coverage_cities = implode(',', array_pluck($cities, 'id'));
            $coverage_cities .= ',' . implode(',', array_pluck($cities, 'coverage'));
            $city_id = $coverage_cities; //implode(',', $city_id);
        } else if (!empty($city_id) && is_numeric($city_id)) {
            $city = City::find($city_id);
            $get_converage($city);
            $city_id = $city->id . (!empty($city->coverage) ? ',' . $city->coverage : '');
        }

        if ( $type == 'free_delivery' ) {
            $discount = new Discount;
            $discount->type = $type;
            $discount->store_ids = $store_id;
            $discount->city_id = $city_id;
            $discount->minimum_order_amount = $minimum_order_amount;
            try {
                $discount->save();
            } catch (\Illuminate\Database\QueryException $e){
                if ($e->getCode()) {
                    return Redirect::back()->with('error', 'Ha ocurrido un error al aplicar el descuento. El descuento ya existe.');
                }
                return Redirect::back()->with('error', $e->getMessage());
            }
            return Redirect::back()->with('success', 'Se ha aplicado el descuento');
        }

        if ( $type == 'discount_credit' || $type == 'discount_percentage') {
            $discount = new Discount;
            $discount->type = $type;
            $discount->order_for_tomorrow = $order_for_tomorrow;
            $discount->store_ids = $store_id;
            $discount->city_id = $city_id;
            $discount->department_id = $department_id;
            $discount->shelf_id = $shelf_id;
            $discount->store_product_id = $store_product_id;
            $discount->minimum_order_amount = $minimum_order_amount;
            $discount->maximum_order_amount = $maximum_order_amount;
            $discount->amount = $order_amount_discount;
            try {
                $discount->save();
            } catch (\Illuminate\Database\QueryException $e){
                if ($e->getCode()) {
                    return Redirect::back()->with('error', 'Ha ocurrido un error al aplicar el descuento. El descuento ya existe.');
                }
                return Redirect::back()->with('error', $e->getMessage());
            }
            return Redirect::back()->with('success', 'Se ha aplicado el descuento');
        }
        return Redirect::back()->with('error', 'Ha ocurrido un error al aplicar el descuento');
    }

    /**
     * Funcion para borrar los descuentos configurados
     */
    public function delete_discount($value='')
    {
        $id = Input::get('id', null);
        $discount = Discount::find($id);
        if ( $discount ) {
            $discount->delete();
            return Response::json(['success'=>'Se ha borrado la promoción correctamente']);
        }
        return Response::json(['error'=>'No se ha encontrado la promoción']);
    }

    /**
     * Obtener tiendas por ciudad ajax
     */
    public function get_stores_ajax()
    {
        $stores = array();
        if (Input::has('city_id')) {
            $stores = Store::select('id', 'name')->where('city_id', Input::get('city_id'))->where('status', 1)->where('is_hidden', 0)->get()->toArray();
        }
        return Response::json($stores, 200);
    }
}
