<?php
namespace admin;
use View, Session, DB, OrderGroup, Order, City, Store, CustomerService, OrderProduct, Response, Input, OrderGroupCallcenter, Redirect, Request, Admin, CallcenterAdmin;
class AdminCallCenterController extends AdminController
{
	protected $allowed_attemps = 3;
	protected $allowed_mins = 5;
	protected $rules;
	protected $setup;
	protected $admin;
	protected $orders = [];

	function __construct()
	{
		parent::__construct();
		$this->admin = Admin::find(Session::get('admin_id'));
		$this->rules = json_decode(Session::get('admin_rules'), true);
		$this->setup = json_decode(Session::get('admin_config'), true);
		if ( count($this->orders) < $this->setup['orders_limit'] && $this->admin->is_paused == 0 ) {
			$this->assign_orders();
		}elseif($this->admin->is_paused == 1){
			$orders = CallcenterAdmin::where('admin_id', $this->admin->id)->select('group_id', 'customer_service_id')->get()->toArray();
			if ($orders) {
				$orders = $this->get_formatted_orders($orders);
				$this->orders = $orders;
			}
		}
	}

	/**
	 * Muestra la grilla con los pedidos de usuarios nuevos primera llamada
	 */
	public function index()
	{
		$customer_services = CustomerService::all()->toArray();
		// Pedidos para Adquisicion llamada inicial
		// $first_calling = $this->acquisitions($customer_services[0]);
		// $second_calling = $this->acquisitions_last_call($customer_services[1]);
		// $order_groups = array_merge($first_calling, $second_calling);
		$data = [
			'title' => 'Callcenter',
			'order_groups' => $this->orders,
			'admin' => $this->admin
		];

		return View::make('admin.call_center.index', $data);
	}

	/**
	 * Funcion para cambiar de estado al agente de call center
	 * @param  int $admin_id identificador del agente
	 */
	public function status_update($admin_id)
	{
		$status = Input::get('status');
		$this->admin->is_paused = $status;
		$this->admin->save();
		return Redirect::back();
	}

	/**
	 * Funcion para asignar las ordenes
	 */
	public function assign_orders()
	{
		$limit = $this->setup['orders_limit'];
		$rules = $this->rules;
		$first_calling = [];
		$second_calling = [];
		$orders = [];
		$customer_services_objs = CustomerService::orderBy('priority', 'ASC')->whereIn('id', $rules['rules'])->get();
		$customer_services_array = $customer_services_objs->toArray();
		$orders_array = CallcenterAdmin::where('admin_id', $this->admin->id)->select('group_id', 'customer_service_id')->get()->toArray();
		$orders_array2 = CallcenterAdmin::select('group_id', 'customer_service_id')->get()->toArray();

		if ( !empty($orders_array) ) {
			$orders = $this->get_formatted_orders($orders_array);
			$orders_array = array_fetch($orders_array, 'group_id');
		}

		if ( count($orders_array) < $limit ) {
			foreach ($customer_services_objs as $key => $customer_service) {
				if ( $customer_service->id == 1 && count($orders_array) < $limit ) {
					$first_calling = $this->acquisitions($customer_service->toArray(), ( empty($limit) ? NULL : ($limit - count($orders_array))), ( empty($orders_array2) ? NULL : $orders_array2 ) );
					if ( !empty($first_calling) ) {
						$orders = array_merge($orders, $first_calling);
						if (!empty($orders)) {
							foreach ($orders as $key => $order_array) {
								$og = CallcenterAdmin::where('group_id', $order_array->order_id)->first();
								if ( !$og ) {
									$og = new CallcenterAdmin();
									$og->group_id = $order_array->order_id;
									$og->admin_id = $this->admin->id;
									$og->customer_service_id = $customer_service->id;
									$og->save();
								}
							}
						}
					}
				}
				if ( $customer_service->id == 2 && count($orders) < $limit ) {
					$second_calling = $this->acquisitions_last_call($customer_service->toArray(), ( empty($limit) ? NULL : $limit - count($orders_array)), ( empty($orders_array2) ? NULL : $orders_array2 ) );
					if ( !empty($second_calling) ) {
						if ( !empty($second_calling) ) {
							$orders = array_merge($orders, $second_calling);
							if (!empty($orders)) {
								foreach ($orders as $key => $order_array) {
									$og = CallcenterAdmin::where('group_id', $order_array->order_id)->first();
									if ( !$og ) {
										$og = new CallcenterAdmin();
										$og->group_id = $order_array->order_id;
										$og->admin_id = $this->admin->id;
										$og->customer_service_id = $customer_service->id;
										$og->save();
									}
								}
							}
						}
					}
				}
				if ( $customer_service->id == 8 && count($orders) < $limit ) {
					$orders_late = $this->order_delay($customer_service->toArray(), ( empty($limit) ? NULL : $limit - count($orders_array)), ( empty($orders_array2) ? NULL : $orders_array2 ) );
					if ( !empty($orders_late) ) {
						if ( !empty($orders_late) ) {
							$orders = array_merge($orders, $orders_late);
							if (!empty($orders)) {
								foreach ($orders as $key => $order_array) {
									$og = CallcenterAdmin::where('group_id', $order_array->order_id)->first();
									if ( !$og ) {
										$og = new CallcenterAdmin();
										$og->group_id = $order_array->order_id;
										$og->admin_id = $this->admin->id;
										$og->customer_service_id = $customer_service->id;
										$og->save();
									}
								}
							}
						}
					}
				}
				if ( $customer_service->id == 8 && count($orders) < $limit ) {
					$orders_late = $this->order_delivery_delay($customer_service->toArray(), ( empty($limit) ? NULL : $limit - count($orders_array)), ( empty($orders_array2) ? NULL : $orders_array2 ) );
					if ( !empty($orders_late) ) {
						if ( !empty($orders_late) ) {
							$orders = array_merge($orders, $orders_late);
							if (!empty($orders)) {
								foreach ($orders as $key => $order_array) {
									$og = CallcenterAdmin::where('group_id', $order_array->order_id)->first();
									if ( !$og ) {
										$og = new CallcenterAdmin();
										$og->group_id = $order_array->order_id;
										$og->admin_id = $this->admin->id;
										$og->customer_service_id = $customer_service->id;
										$og->save();
									}
								}
							}
						}
					}
				}
				if ( $customer_service->id == 8 && count($orders) < $limit ) {
					$orders_late = $this->order_shopper_delivery_time_delay($customer_service->toArray(), ( empty($limit) ? NULL : $limit - count($orders_array)), ( empty($orders_array2) ? NULL : $orders_array2 ) );
					if ( !empty($orders_late) ) {
						if ( !empty($orders_late) ) {
							$orders = array_merge($orders, $orders_late);
							if (!empty($orders)) {
								foreach ($orders as $key => $order_array) {
									$og = CallcenterAdmin::where('group_id', $order_array->order_id)->first();
									if ( !$og ) {
										$og = new CallcenterAdmin();
										$og->group_id = $order_array->order_id;
										$og->admin_id = $this->admin->id;
										$og->customer_service_id = $customer_service->id;
										$og->save();
									}
								}
							}
						}
					}
				}
				if ( $customer_service->id == 8 && count($orders) < $limit ) {
					$orders_late = $this->order_dispatch_delay($customer_service->toArray(), ( empty($limit) ? NULL : $limit - count($orders_array)), ( empty($orders_array2) ? NULL : $orders_array2 ) );
					if ( !empty($orders_late) ) {
						if ( !empty($orders_late) ) {
							$orders = array_merge($orders, $orders_late);
							if (!empty($orders)) {
								foreach ($orders as $key => $order_array) {
									$og = CallcenterAdmin::where('group_id', $order_array->order_id)->first();
									if ( !$og ) {
										$og = new CallcenterAdmin();
										$og->group_id = $order_array->order_id;
										$og->admin_id = $this->admin->id;
										$og->customer_service_id = $customer_service->id;
										$og->save();
									}
								}
							}
						}
					}
				}
			}
		}
		$this->orders = $orders;
		return;
	}

	public function get_formatted_orders($orders2)
	{
		$order_groups = DB::table(
				DB::raw('
					(
						SELECT
							og.id AS order_id,
							u.id AS user_id,
							u.first_name,
							u.last_name,
							u.phone,
							u.email,
							c.city,
							og.user_address AS "user_address",
							DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at,
							og.created_at AS "create_date",
							COUNT(og.id) AS "count"
						FROM users u
						INNER JOIN order_groups og ON u.id = og.user_id
						INNER JOIN cities c ON og.user_city_id = c.id
						WHERE og.id IN ('.implode(',', array_fetch($orders2, 'group_id')).')
						GROUP BY u.id
						HAVING COUNT(og.id) = 1
						ORDER BY og.created_at ASC
					) sub '
				)
			)
		->join('orders AS o', 'o.group_id', '=', 'sub.order_id')
		->leftJoin('order_group_callcenter AS oc', function ($join)
		{
			$join->on('sub.order_id', '=', 'oc.group_id');
		})
		/*->where('o.status', '<>', DB::raw('"Delivered"'))
		->where('o.status', '<>', DB::raw('"Cancelled"'))*/
		->whereRaw('DATE(sub.create_date) = DATE("'.date('Y-m-d H:i:s').'")')
		->havingRaw('IF( COUNT(oc.id) >= '.($this->allowed_attemps - 1).' , IF( updated IS NOT NULL, TIMESTAMPDIFF(MINUTE,updated,"'.date('Y-m-d H:i:s').'") > '.$this->allowed_mins.', TRUE), TRUE )')
		->groupBy('sub.create_date')
		->orderBy('attemps', 'ASC')
		->orderBy('oc.created_at', 'ASC')
		->select(
			'sub.*',
			'o.id',
			'o.status',
			DB::raw('COUNT(oc.id) AS "attemps"'),
			DB::raw('MAX(oc.updated_at) AS "updated"')
		);
		$order_groups = $order_groups->get();

		foreach ($order_groups as $key => &$order_group) {
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount', DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			foreach ($orders2 as $key3 => $order2) {
				if ($order_group->order_id == $order2['group_id']) {
					$order_group->customer_services = CustomerService::find($order2['customer_service_id'])->toArray();
				}
			}
		}
		return $order_groups;
	}

	/**
	 * Función para obtener las ordenes de primera adquisición de los usuarios (PRIMERA LLAMADA).
	 * @param  int $service_id Identificador del servicio.
	 * @return Array Arreglo con las ordenes encontradas
	 */
	private function acquisitions($service_id, $limit = null, $orders_array = null)
	{
		$order_groups = DB::table(
				DB::raw('
					(
						SELECT
							og.id AS order_id,
							u.id AS user_id,
							u.first_name,
							u.last_name,
							u.phone,
							u.email,
							c.city,
							og.user_address AS "user_address",
							DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at,
							og.created_at AS "create_date",
							COUNT(og.id) AS "count"
						FROM users u
						INNER JOIN order_groups og ON u.id = og.user_id
						INNER JOIN cities c ON og.user_city_id = c.id
						GROUP BY u.id
						HAVING COUNT(og.id) = 1
						ORDER BY og.created_at ASC
					) sub '
				)
			)
		->join('orders AS o', 'o.group_id', '=', 'sub.order_id')
		->leftJoin('order_group_callcenter AS oc', function ($join)
		{
			$join->on('sub.order_id', '=', 'oc.group_id');
		})
		->where('o.status', '<>', DB::raw('"Delivered"'))
		->where('o.status', '<>', DB::raw('"Cancelled"'))
		->whereRaw('DATE(sub.create_date) = DATE("'.date('Y-m-d H:i:s').'")')
		->havingRaw('IF( COUNT(oc.id) >= '.($this->allowed_attemps - 1).' , IF( updated IS NOT NULL, TIMESTAMPDIFF(MINUTE,updated,"'.date('Y-m-d H:i:s').'") > '.$this->allowed_mins.', TRUE), TRUE )')
		->groupBy('sub.create_date')
		->orderBy('attemps', 'ASC')
		->orderBy('oc.created_at', 'ASC')
		->select(
			'sub.*',
			'o.id',
			'o.status',
			DB::raw('COUNT(oc.id) AS "attemps"'),
			DB::raw('MAX(oc.updated_at) AS "updated"')
		);
		if ( !empty($limit) ) {
			if (!empty( $orders_array )) {
				$order_groups->whereNotIn('sub.order_id', array_fetch($orders_array, 'group_id'));
			}
			$order_groups->limit($limit);
		}
		$order_groups = $order_groups->get();

		$order_call_answered = OrderGroupCallcenter::where('status', '=', 'Contesta')->where('costumer_service_id', $service_id['id'])->select('group_id')->get()->toArray();
		$order_call_not_answered = OrderGroupCallcenter::where('status', '=', 'No contesta')->where('costumer_service_id', $service_id['id'])->select('group_id', DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->havingRaw('attemps = '.$this->allowed_attemps)->get()->toArray();

		$order_call_answered = array_fetch($order_call_answered, 'group_id');
		$order_call_not_answered = array_fetch($order_call_not_answered, 'group_id');

		foreach ($order_groups as $key => &$order_group) {
			if ( in_array($order_group->order_id, $order_call_answered) || in_array($order_group->order_id, $order_call_not_answered) ) {
				unset($order_groups[$key]);
				continue;
			}
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount', DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			$order_group->customer_services = $service_id;
		}
		return $order_groups;
	}

	/**
	 * Función para obtener las ordenes de primera adquisición de los usuarios (SEGUNDA LLAMADA) donde las ordenes ya han sido entregadas.
	 * @param  int $service_id Identificador del servicio.
	 * @return Array Arreglo con las ordenes encontradas
	 */
	private function acquisitions_last_call($service_id, $limit = null, $orders_array = null)
	{
		$order_groups = DB::table(
				DB::raw('
					(
						SELECT
							og.id AS order_id,
							u.id AS user_id,
							u.first_name,
							u.last_name,
							u.phone,
							u.email,
							c.city,
							og.user_address AS "user_address",
							DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at,
							og.created_at AS "create_date",
							COUNT(og.id) AS "count"
						FROM users u
						INNER JOIN order_groups og ON u.id = og.user_id
						INNER JOIN cities c ON og.user_city_id = c.id
						GROUP BY u.id
						HAVING COUNT(og.id) = 1
						ORDER BY og.id DESC
					) sub '
				)
			)
		->select(
			'sub.*',
			'o.id',
			'o.status',
			'o.updated_at',
			DB::raw('COUNT(o.id) AS "count_orders"'),
			DB::raw('SUM(IF(o.status = "Delivered", 1, 0)) AS "orders_delivered"'),
			DB::raw('COUNT(oc.id) AS "attemps"'),
			DB::raw('MAX(oc.updated_at) AS "updated"')
		)
		->join('orders AS o', 'o.group_id', '=', 'sub.order_id')
		->leftJoin('order_group_callcenter AS oc', function ($join)
		{
			$join->on('o.group_id', '=', 'oc.group_id')
			->where('oc.status', '=', DB::raw('"Contesta"'));
		})
		->havingRaw('IF( COUNT(oc.id) >= '.($this->allowed_attemps - 1).' , IF( updated IS NOT NULL, TIMESTAMPDIFF(MINUTE,updated,"'.date('Y-m-d H:i:s').'") > '.$this->allowed_mins.', TRUE), TRUE )')
		->whereRaw('DATE(o.date) = DATE("'.date('Y-m-d H:i:s').'")')
		->orderBy('sub.create_date', 'ASC')
		->orderBy('oc.created_at', 'ASC')
		->groupBy('sub.user_id')
		->havingRaw('count_orders = orders_delivered');
		if ( !empty($limit) ) {
			if (!empty( $orders_array )) {
				$order_groups->whereNotIn('sub.order_id', array_fetch($orders_array, 'group_id'));
			}
			$order_groups->limit($limit);
		}
		$order_groups = $order_groups->get();

		$order_call_answered = OrderGroupCallcenter::where('status', '=', 'Contesta')->where('costumer_service_id', $service_id['id'])->select('group_id')->get()->toArray();
		$order_call_not_answered = OrderGroupCallcenter::where('status', '=', 'No contesta')->where('costumer_service_id', $service_id['id'])->select('group_id', DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->havingRaw('attemps = '.$this->allowed_attemps)->get()->toArray();

		$order_outof_time = OrderGroupCallcenter::where('status', '=', 'No contesta')->
			where('costumer_service_id', $service_id['id'])
			->select(
					'group_id',
					DB::raw('COUNT(*) AS "attemps"'),
					DB::raw('MAX(updated_at) AS "updated"')
				)
			->groupBy('group_id')
			->havingRaw('attemps = '.($this->allowed_attemps - 1))
			->havingRaw('IF( updated IS NOT NULL AND TIMESTAMPDIFF(MINUTE,updated,"'.date('Y-m-d H:i:s').'") < '.$this->allowed_mins.', TRUE, FALSE)')
			->get()->toArray();
		$order_call_answered = array_fetch($order_call_answered, 'group_id');
		$order_call_not_answered = array_fetch($order_call_not_answered, 'group_id');
		$order_outof_time = array_fetch($order_outof_time, 'group_id');

		foreach ($order_groups as $key => &$order_group) {
			if ( in_array($order_group->order_id, $order_call_answered) || in_array($order_group->order_id, $order_call_not_answered) || in_array($order_group->order_id, $order_outof_time) ) {
				unset($order_groups[$key]);
				continue;
			}
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount',  DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			$order_group->customer_services = $service_id;
		}
		return $order_groups;
	}

	/**
	 * Funcion para obtener las ordenes que no estan delivered luego de que el tiempo de entrega se cumplió
	 * @param  Int $service_id   identificador del servicio
	 * @param  Int $limit        limite de las ordenes
	 * @param  Array $orders_array Arreglo de ordenes que no deben ser incluidas
	 * @return Array Arreglo de ordenes que se deben mostrar.
	 */
	public function order_delay($service_id, $limit = null, $orders_array = null)
	{
		$order_groups = DB::table('order_groups AS og')->select(
											'og.id AS order_id',
											'u.id AS user_id',
											'u.first_name',
											'u.last_name',
											'u.phone',
											'u.email',
											'c.city',
											'og.user_address AS "user_address"',
											DB::raw('DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at'),
											'og.created_at AS "create_date"',
											DB::raw('COUNT(oc.id) AS "attemps"'),
											DB::raw('MAX(oc.updated_at) AS "updated"')
										)->join(DB::raw("(
															SELECT
																orders.*
															FROM
																orders
															WHERE
																`status` <> 'Delivered'
																AND `status` <> 'Cancelled'
																AND DATE(delivery_date) = DATE('".date('Y-m-d H:i:s')."')
																AND TIMESTAMPDIFF(MINUTE, '".date('Y-m-d H:i:s')."',delivery_date) < 0
																AND shopper_id = 0
														) AS sub"), 'og.id', '=', 'sub.group_id')
										->join('users AS u', 'og.user_id', '=', 'u.id')
										->join('cities AS c', 'og.user_city_id', '=', 'c.id')
										->leftJoin('order_group_callcenter AS oc', 'og.id', '=', 'oc.group_id');

		if ( !empty($limit) ) {
			if (!empty( $orders_array )) {
				$order_groups->whereNotIn('sub.group_id', array_fetch($orders_array, 'group_id'));
			}
			$order_groups->limit($limit);
		}
		$result = $order_groups->exists();
		if ( !$result ) {
			return null;
		}
		$order_groups = $order_groups->get();
		$order_call_answered = OrderGroupCallcenter::where('status', '=', 'Contesta')->where('costumer_service_id', $service_id['id'])->select('group_id')->get()->toArray();
		$order_call_not_answered = OrderGroupCallcenter::where('status', '=', 'No contesta')->where('costumer_service_id', $service_id['id'])->select('group_id', DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->havingRaw('attemps = '.$this->allowed_attemps)->get()->toArray();

		$order_call_answered = array_fetch($order_call_answered, 'group_id');
		$order_call_not_answered = array_fetch($order_call_not_answered, 'group_id');

		foreach ($order_groups as $key => &$order_group) {
			if ( in_array($order_group->order_id, $order_call_answered) || in_array($order_group->order_id, $order_call_not_answered) ) {
				unset($order_groups[$key]);
				continue;
			}
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount', DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			$order_group->customer_services = $service_id;
		}
		return $order_groups;
	}

	/**
	 * Funcion para obtener las ordenes que faltando 20 minutos para la fecha de entrega, aún no han sido asignadas
	 * @param  Int $service_id   identificador del servicio
	 * @param  Int $limit        limite de las ordenes
	 * @param  Array $orders_array Arreglo de ordenes que no deben ser incluidas
	 * @return Array Arreglo de ordenes que se deben mostrar.
	 */
	public function order_delivery_delay($service_id, $limit = null, $orders_array = null)
	{
		$order_groups = DB::table('order_groups AS og')->select(
											'og.id AS order_id',
											'u.id AS user_id',
											'u.first_name',
											'u.last_name',
											'u.phone',
											'u.email',
											'c.city',
											'og.user_address AS "user_address"',
											DB::raw('DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at'),
											'og.created_at AS "create_date"',
											DB::raw('COUNT(oc.id) AS "attemps"'),
											DB::raw('MAX(oc.updated_at) AS "updated"')
										)->join(DB::raw("(
															SELECT
																orders.*
															FROM
																orders
															WHERE
																`status` = 'Initiated'
																AND DATE(delivery_date) = DATE('".date('Y-m-d H:i:s')."')
																AND TIME(delivery_date) > TIME('".date('Y-m-d H:i:s')."')
																AND TIMESTAMPDIFF(MINUTE, '".date('Y-m-d H:i:s')."',delivery_date) < 20
																AND shopper_id = 0
														) AS sub"), 'og.id', '=', 'sub.group_id')
										->join('users AS u', 'og.user_id', '=', 'u.id')
										->join('cities AS c', 'og.user_city_id', '=', 'c.id')
										->leftJoin('order_group_callcenter AS oc', 'og.id', '=', 'oc.group_id');

		if ( !empty($limit) ) {
			if (!empty( $orders_array )) {
				$order_groups->whereNotIn('sub.group_id', array_fetch($orders_array, 'group_id'));
			}
			$order_groups->limit($limit);
		}
		$result = $order_groups->exists();
		if ( !$result ) {
			return null;
		}
		$order_groups = $order_groups->get();
		$order_call_answered = OrderGroupCallcenter::where('status', '=', 'Contesta')->where('costumer_service_id', $service_id['id'])->select('group_id')->get()->toArray();
		$order_call_not_answered = OrderGroupCallcenter::where('status', '=', 'No contesta')->where('costumer_service_id', $service_id['id'])->select('group_id', DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->havingRaw('attemps = '.$this->allowed_attemps)->get()->toArray();

		$order_call_answered = array_fetch($order_call_answered, 'group_id');
		$order_call_not_answered = array_fetch($order_call_not_answered, 'group_id');

		foreach ($order_groups as $key => &$order_group) {
			if ( in_array($order_group->order_id, $order_call_answered) || in_array($order_group->order_id, $order_call_not_answered) ) {
				unset($order_groups[$key]);
				continue;
			}
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount', DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			$order_group->customer_services = $service_id;
		}
		return $order_groups;
	}

	/**
	 * Funcion para obtener las ordenes que sumando el tiempo del shopper no alcanzan a llegar a tiempo.
	 * @param  Int $service_id   identificador del servicio
	 * @param  Int $limit        limite de las ordenes
	 * @param  Array $orders_array Arreglo de ordenes que no deben ser incluidas
	 * @return Array Arreglo de ordenes que se deben mostrar.
	 */
	public function order_shopper_delivery_time_delay($service_id, $limit = null, $orders_array = null)
	{
		$order_groups = DB::table('order_groups AS og')->select(
											'og.id AS order_id',
											'u.id AS user_id',
											'u.first_name',
											'u.last_name',
											'u.phone',
											'u.email',
											'c.city',
											'og.user_address AS "user_address"',
											DB::raw('DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at'),
											'og.created_at AS "create_date"',
											DB::raw('COUNT(oc.id) AS "attemps"'),
											DB::raw('MAX(oc.updated_at) AS "updated"')
										)->join(DB::raw("(
															SELECT
																orders.*
															FROM
																orders
															WHERE
																`status` = 'In progress'
																AND shopper_id <> 0
																AND DATE(delivery_date) = DATE('".date('Y-m-d H:i:s')."')
																AND '".date('Y-m-d H:i:s')."' + INTERVAL shopper_delivery_time_minutes MINUTE > delivery_date
														) AS sub"), 'og.id', '=', 'sub.group_id')
										->join('users AS u', 'og.user_id', '=', 'u.id')
										->join('cities AS c', 'og.user_city_id', '=', 'c.id')
										->leftJoin('order_group_callcenter AS oc', 'og.id', '=', 'oc.group_id');

		if ( !empty($limit) ) {
			if (!empty( $orders_array )) {
				$order_groups->whereNotIn('sub.group_id', array_fetch($orders_array, 'group_id'));
			}
			$order_groups->limit($limit);
		}
		$result = $order_groups->exists();
		if ( !$result ) {
			return null;
		}
		$order_groups = $order_groups->get();
		$order_call_answered = OrderGroupCallcenter::where('status', '=', 'Contesta')->where('costumer_service_id', $service_id['id'])->select('group_id')->get()->toArray();
		$order_call_not_answered = OrderGroupCallcenter::where('status', '=', 'No contesta')->where('costumer_service_id', $service_id['id'])->select('group_id', DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->havingRaw('attemps = '.$this->allowed_attemps)->get()->toArray();

		$order_call_answered = array_fetch($order_call_answered, 'group_id');
		$order_call_not_answered = array_fetch($order_call_not_answered, 'group_id');

		foreach ($order_groups as $key => &$order_group) {
			if ( in_array($order_group->order_id, $order_call_answered) || in_array($order_group->order_id, $order_call_not_answered) ) {
				unset($order_groups[$key]);
				continue;
			}
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount', DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			$order_group->customer_services = $service_id;
		}
		return $order_groups;
	}

	/**
	 * Funcion para obtener las ordenes que estando en estado dispatch.
	 * @param  [type] $service_id   [description]
	 * @param  [type] $limit        [description]
	 * @param  [type] $orders_array [description]
	 * @return [type]               [description]
	 */
	public function order_dispatch_delay($service_id, $limit = null, $orders_array = null)
	{
		$order_groups = DB::table('order_groups AS og')->select(
											'og.id AS order_id',
											'u.id AS user_id',
											'u.first_name',
											'u.last_name',
											'u.phone',
											'u.email',
											'c.city',
											'og.user_address AS "user_address"',
											DB::raw('DATE_FORMAT(og.created_at, "%d/%m/%Y %h:%i %p") AS created_at'),
											'og.created_at AS "create_date"',
											DB::raw('COUNT(oc.id) AS "attemps"'),
											DB::raw('MAX(oc.updated_at) AS "updated"')
										)->join(DB::raw("(
															SELECT
																orders.*
															FROM
																orders
															WHERE
																`status` = 'Dispatched'
																AND shopper_id <> 0
																AND DATE(delivery_date) = DATE('".date('Y-m-d H:i:s')."')
																AND TIMESTAMPDIFF(MINUTE, '".date('Y-m-d H:i:s')."',delivery_date) < 20
														) AS sub"), 'og.id', '=', 'sub.group_id')
										->join('users AS u', 'og.user_id', '=', 'u.id')
										->join('cities AS c', 'og.user_city_id', '=', 'c.id')
										->leftJoin('order_group_callcenter AS oc', 'og.id', '=', 'oc.group_id');

		if ( !empty($limit) ) {
			if (!empty( $orders_array )) {
				$order_groups->whereNotIn('sub.group_id', array_fetch($orders_array, 'group_id'));
			}
			$order_groups->limit($limit);
		}
		$result = $order_groups->exists();
		if ( !$result ) {
			return null;
		}
		$order_groups = $order_groups->get();
		$order_call_answered = OrderGroupCallcenter::where('status', '=', 'Contesta')->where('costumer_service_id', $service_id['id'])->select('group_id')->get()->toArray();
		$order_call_not_answered = OrderGroupCallcenter::where('status', '=', 'No contesta')->where('costumer_service_id', $service_id['id'])->select('group_id', DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->havingRaw('attemps = '.$this->allowed_attemps)->get()->toArray();

		$order_call_answered = array_fetch($order_call_answered, 'group_id');
		$order_call_not_answered = array_fetch($order_call_not_answered, 'group_id');

		foreach ($order_groups as $key => &$order_group) {
			if ( in_array($order_group->order_id, $order_call_answered) || in_array($order_group->order_id, $order_call_not_answered) ) {
				unset($order_groups[$key]);
				continue;
			}
			$orders = Order::where('group_id', $order_group->order_id)->select('id', 'store_id', 'total_products', 'total_amount', 'discount_amount', 'delivery_amount', DB::raw('DATE_FORMAT(delivery_date, "%d/%m/%Y %h:%i %p") AS "delivery_date"'))->get()->toArray();
			if ( count($orders) > 0 )
				foreach ($orders as $key2 => &$order) {
					$store = Store::find($order['store_id']);
					$order['store_name'] = $store->name;
				}

			$order_group->orders = $orders;
			$order_group->customer_services = $service_id;
		}
		return $order_groups;
	}



	/**
	 * Muestra el detalle del pedido del usuario
	 * @param  int $group_id identificador del order group del usuario.
	 * @param  int $service_id identificador del servicio.
	 */
	public function details($group_id, $service_id)
	{
		$order_group = OrderGroup::find($group_id);
		$customer_service = CustomerService::find((int)$service_id);
		if ( $order_group ) {
			$city = City::find($order_group->user_city_id);
			$orders = Order::where('group_id', $group_id)
							->join('stores', 'orders.store_id', '=', 'stores.id')
							->select(
								'orders.id',
								'orders.reference',
								'stores.name',
								'orders.shopper_id',
								'orders.status',
								'orders.total_products',
								'orders.delivery_amount',
								'orders.discount_amount',
								'orders.total_amount',
								'orders.delivery_date',
								'orders.delivery_time',
								'orders.payment_method'
							)
							->get();
			$delivery_amount = 0;
			$discount_amount = 0;
			$total_amount = 0;
			foreach ($orders as $key => $order) {
				$delivery_amount += $order->delivery_amount;
				$discount_amount += $order->discount_amount;
				$total_amount += $order->total_amount;
			}

			$order_call_center = OrderGroupCallcenter::where('group_id', $group_id)
						->where('costumer_service_id', $service_id)->select(DB::raw('COUNT(*) AS "attemps"'))->groupBy('group_id')->first();

			$data = [
				'order_group' => $order_group,
				'orders' => $orders,
				'city' => $city,
				'service_id' => $service_id,
				'customer_service' => $customer_service,
				'order_call_center' => $order_call_center,
				'allowed_attemps' => $this->allowed_attemps,
				'delivery_amount' => '$'.number_format($delivery_amount, 0, ',', '.'),
				'discount_amount' => '$'.number_format($discount_amount, 0, ',', '.'),
				'total_amount' => '$'.number_format($total_amount, 0, ',', '.'),
				'grand_total' => '$'.number_format($total_amount + $delivery_amount - $discount_amount, 0, ',', '.')
			];

			return View::make('admin.call_center.details', $data);
		}
		return Redirect::back()->with('error', 'No existe el order group');
	}

	/**
	 * Funcion para obtener los productos por ajax
	 */
	public function get_order_products_ajax()
	{
		$order_id = Input::get('order_id');
		$order = Order::find($order_id);
		$products = OrderProduct::where('order_id', $order_id)->get();
		if ( $products && $order ) {
			$products = $products->toArray();
			foreach ($products as $key => &$product) {
				$product['price'] = '$'.number_format($product['price'], 0, ',', '.');
				$product['original_price'] = '$'.number_format($product['original_price'], 0, ',', '.');
			}
			$array = [
				'products' => $products,
				'total_amount' => '$'.number_format($order->total_amount, 0, ',', '.'),
				'discount_amount' => '$'.number_format($order->discount_amount, 0, ',', '.'),
				'delivery_amount' => '$'.number_format($order->delivery_amount, 0, ',', '.'),
				'total' => '$'.number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.'),
			];
			return Response::json($array);
		}
		return Redirect::back()->with('error', 'No se encontró la orden.');
	}

	/**
	 * Funcion para guardar los intentos de las llamadas
	 */
	public function save_attemps($group_id, $service_id)
	{
		$admin_id = Session::get('admin_id');
		$attemps = (Input::get('costumer-not-answer') ? Input::get('costumer-not-answer') : Input::get('costumer-answer'));
		$status = Input::get('status');
		$comments = Input::get('comments');
		$user_score = Input::get('user_score');

		$order = new OrderGroupCallcenter();

		$order->group_id = $group_id;
		$order->admin_id = $admin_id;
		$order->costumer_service_id = $service_id;
		$order->status = trim($status);
		$order->comments = trim($comments);
		$order->user_score = ( $user_score ? $user_score : 0 );

		$order->save();
		$redirect_url = false;
		if ( $attemps == 2 || $attemps == 3 )
			$redirect_url = route('AdminCallCenter.index');

		if ($attemps == 3 || $status == 'Contesta'){
			$order_group_admin_obj = CallcenterAdmin::where('group_id', $group_id)->delete();
		}

		if ( Request::ajax() )
			return Response::json(['message' => 'Intento guardado.', 'redirect_url' => $redirect_url]);

		return Redirect::route('AdminCallCenter.index');
	}

	public function get_ids_from_objects($objects)
	{
		$ids = [];
		foreach ($objects as $key => $object) {
			$ids[] = $object->order_id;
		}
		return $ids;
	}
}
?>