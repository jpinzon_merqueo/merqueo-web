<?php

namespace admin;

use exceptions\MerqueoException;
use Merqueo\Backend\Facades\AuthBackend;
use Request;
use Response;
use View;
use Input;
use Redirect;
use Session;
use DB;
use Customer;
use User;
use UserCredit;
use UserCreditCard;
use ExternalService;
use exceptions\ExternalServiceException;
use Config;

/**
 * Class AdminExternalServicesController
 * @package admin
 */
class AdminExternalServicesController extends AdminController
{
    /**
     * Lista de Registros de External Services
     */
    public function index()
    {
        $title = 'Servicios Externos';

        if (!Request::ajax()) {
            return View::make('admin.external_services.index', compact('title'));
        }

        $search = Input::get('s') ?: '';
        $externals = ExternalService::where(function ($query) use ($search) {
            $query->where('user_name', 'like', "%{$search}%")
                ->orWhere('user_email', 'like', "%{$search}%")
                ->orWhere('user_phone', 'like', "%{$search}%");
        })
            ->orderBy('created_at', 'desc')
            ->limit(40)
            ->get();

        return View::make('admin.external_services.index', compact('externals', 'title'))
            ->renderSections()['content'];
    }


    /**
     * Pagar
     * @param ExternalService $payment
     * @return
     */
    public function validate_pay(ExternalService $payment)
    {
        try {
            DB::beginTransaction();
            $data = json_decode($payment->params, true);
            unset($data['runt']['dateStart']);
            unset($data['runt']['dateEnd']);

            $soat = new \Soat();
            // verificamos si es posible emitir SOAT antes de continuar con el cobro
            $verifica_emitir = $soat->sendSoat($data, true);

            if (!isset($verifica_emitir->mimotorId)) {
                $result = $verifica_emitir->error;

                return Redirect::back()->with('type', 'error')->with('message', $result);
            }

            $credit_card = UserCreditCard::findOrFail($payment->credit_card_id);
            $payment->userCreditCard()->associate($credit_card);

            $charge_card = $payment->chargeCreditCard(false);
            $status = $charge_card['status'];
            $result = $charge_card['result'];
            $message = $charge_card['message'];

            if ($status && $data['type'] == 'soat') {
                $result_soat = $soat->sendSoat($data, Config::get('app.soat.test'));

                if (isset($result_soat->mimotorId)) {
                    $payment->external_id = $result_soat->mimotorId;
                    $payment->soat_form_number = $result_soat->soatFormNumber;
                    $payment->save();

                    $user = User::find($payment->user_id);

                    $expiration_date = date('Y-m-d', strtotime('+' . Config::get('app.referred.amount_expiration_days') . ' day'));
                    $amount_buy = Config::get('app.soat.amount_buy');
                    UserCredit::addCredit($user, $amount_buy, $expiration_date, 'external_service', $payment);
                    $soat->sendEmail($data);
                } else {
                    $status = false;
                    $result = 'ERROR';
                    $message = 'Error emitir';
                }
            }

            if (!$status) {
                throw new ExternalServiceException($message, $result);
            }

            $type = $status ? 'success' : 'error';
            DB::commit();
            return Redirect::back()->with(compact('type', 'message'));
        } catch (ExternalServiceException $e) {
            $message = $e->message;
            $type = 'error';
            DB::rollback();
            \ExternalServiceLog::make($payment, 'Transacción rechazada.');
            return Redirect::back()->with(compact('type', 'message'));
        }
    }

    /**
     * @param ExternalService $external_service
     * @return mixed
     */
    public function get_external_service_detail(ExternalService $external_service)
    {
        \Event::subscribe(new \SoatEventHandler(true));

        $external_service->load('city', 'payments.refunds')
            ->load([
                'user.externalServices' => function ($query) use ($external_service) {
                    $query->where('type', 'SOAT')
                        ->where('id', '<>', $external_service->id);
                }
            ]);

        $payment_manager = new \PayU();
        $title = 'Detalle servicio';
        $reasons = \RejectReason::service()->get();
        $refund_reasons = \RejectReason::refund()->get();

        $have_payments_approved = $external_service->payments()->where('cc_payment_status', 'Aprobada')->count() > 0;

        try {
            if ($external_service->status === 'Aprobada') {
                foreach ($external_service->payments as $payment) {
                    foreach ($payment->refunds as $refund) {
                        if ($refund->status === 'Pendiente') {
                            $external_service->validateStateFromRefund($refund, $payment_manager);
                            Session::flash(
                                'success',
                                "Se ha actualizado el estado de un reembolso pendiente. " .
                                "El estado del reembolso es '{$refund->status}'."
                            );
                            break;
                        }
                    }
                }
            }
        } catch (MerqueoException $exception) {
            Session::flash('error', $exception);
        }

        return View::make(
            'admin.external_services.soat-detail',
            compact('external_service', 'title', 'reasons', 'refund_reasons', 'have_payments_approved')
        );
    }

    /**
     * Cancelación del servicio.
     *
     * @param ExternalService $external_service
     * @return mixed
     */
    public function cancel_service(ExternalService $external_service)
    {
        \Event::subscribe(new \SoatEventHandler());

        try {
            \DB::beginTransaction();
            $reject_reason = \RejectReason::findOrFail(\Input::get('reject_reason'));
            $comments = Input::get('reject_comments');
            $external_service->cancel($reject_reason, $comments);
            \DB::commit();

            $type = 'success';
            $message = 'Servicio cancelado';
        } catch (\Exception $exception) {
            \DB::rollback();
            $type = 'error';
            $message = $exception->getMessage();
        }

        return Redirect::back()->with(compact('type', 'message'));
    }

    /**
     * Genera un reembolso.
     *
     * @param ExternalService $service
     * @return mixed
     */
    public function make_refund(\ExternalService $service)
    {
        \Event::subscribe(new \SoatEventHandler(\Input::get('notify_user') === 'true'));
        $reason = \RejectReason::find(Input::get('reject_reason'));

        try {
            \DB::beginTransaction();
            $refund = $service->makeRefund($reason, AuthBackend::user());
            $service->validateStateFromRefund($refund, new \PayU());
            $type = 'success';
            $message = 'Se ha generado el reembolso.';
            \DB::commit();
        } catch (MerqueoException $e) {
            $type = 'error';
            $message = $e->getMessage();
            \DB::rollback();
        }

        return Redirect::back()->with(compact('type', 'message'));
    }

    /**
     * Retorna los detalles del pago.
     *
     * @param \ExternalServicePayment $payment
     * @return mixed
     */
    public function get_refund_detail_ajax(\ExternalServicePayment $payment)
    {
        $payment->load('refunds.admin');

        return View::make('admin.external_services.refunds', compact('payment'));
    }

    /**
     * Funcion que emite el soat manualmente.
     */
    public function soat_emit(ExternalService $payment)
    {

        try{
            $have_payment = $payment->payments()->where('cc_payment_status', 'Aprobada')->count() > 0;

            if(empty($payment->soat_form_number) && $have_payment){

                $data = json_decode($payment->params, true);
                unset($data['runt']['dateStart']);
                unset($data['runt']['dateEnd']);

                $soat = new \Soat();

                $result_soat = $soat->sendSoat($data, Config::get('app.soat.test'));

                if (isset($result_soat->mimotorId)) {
                    $payment->external_id = $result_soat->mimotorId;
                    $payment->soat_form_number = $result_soat->soatFormNumber;
                    $payment->status = 'Aprobada';
                    $payment->save();

                    $user = User::find($payment->user_id);

                    $expiration_date = date('Y-m-d', strtotime('+' . Config::get('app.referred.amount_expiration_days') . ' day'));
                    $amount_buy = Config::get('app.soat.amount_buy');
                    UserCredit::addCredit($user, $amount_buy, $expiration_date, 'external_service', $payment);
                    $soat->sendEmail($data);
                    $status = true;
                    $message = "Soat Emitido Manualmente";
                    \ExternalServiceLog::make($payment, $message );

                } else {
                    $status = false;
                    $result = 'ERROR';
                    $message = 'Error emitir';
                }
                $type = 'success';

            }else{
                $status = false;
                $result = 'ERROR';
                $message = 'No es posible emitir';
            }

            if (!$status) {
                throw new ExternalServiceException($message, $result);
            }

            return Redirect::back()->with(compact('type', 'message'));

        }catch (ExternalServiceException $e) {
            $message = $e->message;
            $type = 'error';
            \ExternalServiceLog::make($payment, 'Error Emitir Manual. '.$e->message );
            return Redirect::back()->with(compact('type', 'message'));
        }

        
    }
}