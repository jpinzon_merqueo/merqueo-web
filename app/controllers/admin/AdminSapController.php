<?php

namespace admin;

use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use jobs\GenerateSapReportsJob;
use Input;
use Session;
use View;
use Redirect;

/**
 * Class AdminSapController
 *
 * @package admin
 */
class AdminSapController extends AdminController
{

    /**
     * AdminSapController constructor.
     */
    public function __construct()
    {
        if (!Session::has('admin_id')) {
            return Redirect::route('admin.login');
        } else {
            // Pending orders
            $request = get_current_request();
            $actions = [];
            if (Session::get('admin_designation') != 'Admin' && in_array($request['action'], $actions)) {
                return Redirect::route('admin.dashboard')->with('error', 'You do not have permission access to this module.');
            }
            parent::__construct();
        }
    }

    /**
     * Función para mostrar vista index
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function index()
    {
        $listSapTemplates = [
            'clients'              => '1. Clientes',
            'providers'            => '1. Proveedores',
            'retention-providers'  => '2. Retenciones de proveedores',
            'products'             => '3. Productos',
            'purchase-invoices'    => '4. Facturas de compra',
            'sale-invoices'        => '5. Facturas de venta',
            'credit-note-sale'     => '6. Notas crédito ventas',
            'credit-note-purchase' => '7. Notas crédito compras',
            'inventory-entry'      => '8. Entradas de inventario',
            'inventory-output'     => '9. Salidas de inventario',
            'payments-received'    => '10. Pagos recibidos',
        ];
        $arrayResponse = ['title' => 'Plantillas SAP', 'sapTemplates' => $listSapTemplates];

        return View::make('admin.sap.index', $arrayResponse);
    }

    /**
     * Genera procesos en cola segun sea el template solicitado para generar un plantilla en excel
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function templates()
    {
        $template = Input::get('type_template');
        $start_date = explode('/', Input::get('start_date'));
        $start_date = Carbon::create($start_date[2], $start_date[1], $start_date[0]);
        $end_date = explode('/', Input::get('end_date'));
        $end_date = Carbon::create($end_date[2], $end_date[1], $end_date[0]);
        $paramsQueue = ['template' => $template, 'start_date' => $start_date->format('Y-m-d'),
            'end_date' => $end_date->format('Y-m-d'), 'mail' => Session::get('admin_username')];

        if ($start_date > $end_date) {
            return Redirect::back()->with('error', 'La fecha inicial no puede ser mayor a la fecha final.');
        }

        if ($start_date->diff($end_date)->days > 5) {
            return Redirect::back()
                ->with('error', 'El rango de fechas no debe superar 5 días entre la fecha inicial y la fecha final.');
        }

        $templates_queue_high_array = ['purchase-invoices', 'sale-invoices', 'payments-received'];
        $templates_queue_slow_array = ['clients', 'providers', 'products'];

        if(in_array($template, $templates_queue_high_array)) {
            \Queue::push(GenerateSapReportsJob::class, $paramsQueue, 'slow');
            $message = 'La plantilla solicitada se esta generando en cola de procesos y puede tardar entre 30 minutos y 2 horas, al terminar se enviará un correo electrónico a '
            . Session::get('admin_username') . ' con el link de descarga.';
        } elseif (in_array($template, $templates_queue_slow_array)) {
            \Queue::push(GenerateSapReportsJob::class, $paramsQueue, 'quick');
            $message = 'La plantilla solicitada se esta generando en cola de procesos y puede tardar hasta 10 minutos, al terminar se enviará un correo electrónico a '
                . Session::get('admin_username') . ' con el link de descarga.';
        } else {
            \Queue::push(GenerateSapReportsJob::class, $paramsQueue, 'medium');
            $message = 'La plantilla solicitada se esta generando en cola de procesos y puede tardar entre 20 minutos y 1 hora, al terminar se enviará un correo electrónico a '
                . Session::get('admin_username') . ' con el link de descarga.';
        }

        return Redirect::back()->with('success', $message);
    }
}
