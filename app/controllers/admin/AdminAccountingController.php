<?php

namespace admin;

use Request, Input, Session, View, Redirect, Config, DB, Menu, PHPExcel, PHPExcel_Style_Alignment, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007,
Order, OrderGroup, Store, City, Response, OrderProduct;

class AdminAccountingController extends AdminController {

    public function __construct()
    {
        if (!Session::has('admin_id')){
            return Redirect::route('admin.login');
        } else {
            // Pending orders
            $request = get_current_request();
            $actions = array();
            if (Session::get('admin_designation') != 'Admin' && in_array($request['action'], $actions)){
                return Redirect::route('admin.dashboard')->with('error', 'You do not have permission access to this module.');
            }
        }
    }

    public function pad($str, $length, $pad_str = '0', $pad_left = true){
        $str = str_pad($str, $length, $pad_str, $pad_left ? STR_PAD_LEFT : STR_PAD_RIGHT);

        return substr($str, 0, $length);

    }

    /**
     * Archivos planos de contabilidad
     */
    public function invoice_file()
    {
        View::composer('admin.layout', function($view)
        {
            $menu = Menu::find(1);
            $menu_items = json_decode($menu->menu_order, true);
            $view->with('menu', $menu_items);
        });

        ini_set('memory_limit', '4096M');
        set_time_limit(0);

        if (Request::isMethod('post') && Input::has('start_date') && Input::has('end_date'))
        {
            $start_date = format_date('mysql', Input::get('start_date'));
            $end_date = format_date('mysql', Input::get('end_date'));
            $store_id = Input::get('store_id');

            if ($start_date > $end_date)
                return Redirect::route('adminAccounting.invoices')->with('error', 'La fecha de inicio debe ser menor a la fecha final.');


            if (Input::has('type_report'))
            {
                $type = Input::get('type_report');
                switch ($type)
                {
                    case 'invoice_client':
                        DB::statement("SET lc_time_names = 'es_ES'");
                        $sql = "SELECT
                                o.id,
                                op.type,
                                op.store_product_id,
                                op.iva,
                                op.id AS order_product_id,
                                /*'F' AS 'TIPO DE COMPROBANTE',
                                SUBSTRING(o.invoice_number,2,1) AS 'CÓDIGO COMPROBANTE',
                                SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO',*/
                                'F' AS 'TIPO DE COMPROBANTE',
                                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,2,2), SUBSTRING(o.invoice_number,2,1))  AS 'CÓDIGO COMPROBANTE',
                                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,5,3), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,4,3), SUBSTRING(o.invoice_number,4,2))) AS 'PREFIJO',
                                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,8,10), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,7,10), SUBSTRING(o.invoice_number,6,10))) AS 'NÚMERO DE DOCUMENTO',
                                p.accounting_account AS 'CUENTA CONTABLE',
                                'C' AS 'DÉBITO O CRÉDITO',
                                SUM((op.quantity * op.price / ((op.iva / 100) + 1))) AS 'VALOR DE LA SECUENCIA',
                                DATE_FORMAT(management_date, '%Y') AS 'AÑO DEL DOCUMENTO',
                                DATE_FORMAT(management_date, '%m') AS 'MES DEL DOCUMENTO',
                                DATE_FORMAT(management_date, '%d') AS 'DÍA DEL DOCUMENTO',
                                '0'  AS 'CÓDIGO DEL VENDEDOR',
                                og.user_city_id AS 'CÓDIGO DE LA CIUDAD',
                                '0'  AS 'CÓDIGO DE LA ZONA',
                                '' AS 'SECUENCIA',
                                '0' AS 'CENTRO DE COSTO',
                                '0' AS 'SUBCENTRO DE COSTO',
                                SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ),1, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ))-1) AS 'NIT',
                                '0' AS SUCURSAL,
                                CONCAT('PEDIDO',' ', o.id, '-', (SELECT product_name FROM provider_order_details WHERE store_product_id = op.store_product_id LIMIT 1)) AS 'DESCRIPCIÓN DE LA SECUENCIA',
                                ''  AS 'NÚMERO DE CHEQUE',
                                'N' AS 'COMPROBANTE ANULADO',
                                '0' AS 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN',
                                '0' AS 'FORMA DE PAGO',
                                CONCAT('0,','',LPAD(0, 2, '0'))  AS 'PORCENTAJE DEL IVA DE LA SECUENCIA',
                                '0,00' AS 'VALOR DE IVA DE LA SECUENCIA',
                                '' AS 'BASE DE RETENCIÓN',
                                '0,00' AS 'BASE PARA CUENTAS MARCADAS COMO RETEIVA',
                                'N' AS 'SECUENCIA GRAVADA O EXCENTA',
                                ''  AS 'PORCENTAJE AIU',
                                ''  AS 'BASE IVA AIU',
                                p.accounting_line AS 'LÍNEA PRODUCTO',
                                p.accounting_group AS 'GRUPO PRODUCTO',
                                p.accounting_code AS 'CÓDIGO PRODUCTO',
                                op.quantity AS CANTIDAD,
                                '0,00000' AS 'CANTIDAD DOS',
                                '1' AS 'CÓDIGO DE LA BODEGA',
                                '0' AS 'CÓDIGO DE LA UBICACIÓN',
                                '0,00000' AS 'CANTIDAD DE FACTOR DE CONVERSIÓN',
                                '0' AS 'OPERADOR DE FACTOR DE CONVERSIÓN',
                                '0,00000' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                                '' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                                '' AS 'GRUPO ACTIVOS',
                                '' AS 'CÓDIGO ACTIVO',
                                '0' AS 'ADICIÓN O MEJORA',
                                '0' AS 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA',
                                '0' AS 'VECES A DEPRECIAR NIIF',
                                '0' AS 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR',
                                '' AS 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR',
                                '' AS 'AÑO DOCUMENTO DEL PROVEEDOR',
                                '' AS 'MES DOCUMENTO DEL PROVEEDOR',
                                '' AS 'DÍA DOCUMENTO DEL PROVEEDOR',
                                '0' AS 'TIPO DOCUMENTO DE PEDIDO',
                                '0' AS 'CÓDIGO COMPROBANTE DE PEDIDO',
                                '0' AS 'NÚMERO DE COMPROBANTE PEDIDO',
                                '0' AS 'SECUENCIA DE PEDIDO',
                                '' AS 'TIPO Y COMPROBANTE CRUCE',
                                '0' AS 'NÚMERO DE DOCUMENTO CRUCE',
                                '0' AS 'NÚMERO DE VENCIMIENTO',
                                DATE_FORMAT(management_date, '%Y') AS 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE',
                                DATE_FORMAT(management_date, '%m') AS 'MES VENCIMIENTO DE DOCUMENTO CRUCE',
                                DATE_FORMAT(management_date, '%d') AS 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE',
                                '0' AS 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE'
                            FROM
                                order_products op
                            LEFT JOIN store_products sp ON sp.id = op.store_product_id
                            INNER JOIN products p ON p.id = sp.product_id
                            INNER JOIN orders o ON op.order_id = o.id
                            LEFT JOIN departments ON departments.id = sp.department_id
                            INNER JOIN stores st ON o.store_id = st.id
                            INNER JOIN order_groups og ON o.group_id = og.id
                            INNER JOIN cities c ON og.user_city_id = c.id
                            LEFT JOIN shoppers s ON o.shopper_id = s.id
                            LEFT JOIN admin ON o.revision_user_id = admin.id
                            LEFT JOIN zones z ON og.zone_id = z.id
                            LEFT JOIN drivers dr ON dr.id = o.driver_id
                            LEFT JOIN vehicles vh ON vh.id = o.vehicle_id                            
                            LEFT JOIN transporters tr ON tr.id = vh.transporter_id
                            INNER JOIN users u ON og.user_id = u.id
                            WHERE is_storage = 1
                            AND DATE(o.management_date) BETWEEN '".$start_date."' AND '".$end_date."'
                            AND o.status = 'Delivered' AND st.id = ".$store_id."
                            AND fulfilment_status = 'Fullfilled' AND o.id NOT IN (188400,186854) AND o.user_id <> 86147
                            AND user_comments NOT LIKE '%El día%'
                            AND o.total_amount > 0
                            GROUP BY o.id, op.id, op.store_product_id, op.price
                            ORDER BY o.management_date ASC";

                        $rows = DB::select(DB::raw($sql));
                        if (count($rows))
                        {
                            $k = $l = 1;
                            $order = null;
                            $filename = date('Y-m-d H.i.s',time()).'.csv';
                            $fp = fopen(public_path(Config::get('app.download_directory')).$filename, 'w');

                            //titulos
                            $titles = "";
                            foreach ($rows as $key => $value)
                            {
                                $value = json_decode(json_encode($value), true);
                                $titles = array_keys($value);

                                $id = array_search('id', $titles);
                                unset($titles[$id]);
                                $type = array_search('type', $titles);
                                unset($titles[$type]);
                                $iva = array_search('iva', $titles);
                                unset($titles[$iva]);
                                $product_id = array_search('store_product_id', $titles);
                                unset($titles[$product_id]);
                                $order_product_id = array_search('order_product_id', $titles);
                                unset($titles[$order_product_id]);
                                break;
                            }
                            fputcsv($fp, $titles);


                            $order_id = 0;
                            foreach($rows as $key =>$row)
                            {
                                $row = json_decode(json_encode($row), true);
                                if ($order_id != $row['id']){
                                    $k = 1;
                                    $i = 1;
                                    //obtener numero de productos del pedido
                                    $order_products_quantity = OrderProduct::where('order_id', $row['id'])->where('fulfilment_status', 'Fullfilled')->count();
                                }//else $k++;

                                $order_product_id = $row['order_product_id'];
                                $order_id = $row['id'];

                                if($row['type'] == 'Agrupado')
                                {
                                    $product_group = $this->get_product_group($order_product_id);
                                    if(count($product_group))
                                    {
                                        $total_with_iva = $total_without_iva = 0;
                                        foreach ($product_group as $key => $value)
                                        {
                                            $value = json_decode(json_encode($value), true);

                                            $total_product = $value['price'] * $value['quantity'];
                                            $total_product_individuals = $value['total'];
                                            $total_combo = $value['price_combo'];

                                            #echo 'Total combo: '.$total_combo.'<br>';
                                            #echo 'Total individuales: '.$total_product_individuals.'<br>';
                                            $percentage = $total_combo / $total_product_individuals;
                                            $percentage = 1 - $percentage;
                                            #echo 'Porcentaje: '.$percentage.'<br>';
                                            $percentage_product = $total_product * $percentage * -1;
                                            #echo 'Porcentaje producto: '.$percentage_product.'<br>';
                                            $product_price = $total_product + $percentage_product;
                                            #echo 'Precio producto con iva: '.$product_price.'<br>';
                                            $total_with_iva += $product_price;
                                            $product_price = $product_price / (($value['iva'] / 100) + 1);
                                            #echo 'Precio producto sin iva: '.$product_price.'<br><br>';
                                            $total_without_iva += $product_price;


                                            unset($value['price_combo']);
                                            unset($value['price']);
                                            unset($value['quantity_combo']);
                                            unset($value['total']);
                                            unset($value['iva']);
                                            unset($value['quantity']);

                                            $value['VALOR DE LA SECUENCIA'] = number_format($product_price, 5, ',', '.');
                                            $value['SECUENCIA'] = $k;
                                            $k++;
                                            fputcsv($fp, $value);
                                        }
                                        #echo 'Total sin iva: '.$total_without_iva.'<br>';
                                        #echo 'Total con iva: '.$total_with_iva.'<br><br><br>';
                                    }
                                }else{
                                    $row['VALOR DE LA SECUENCIA'] = number_format($row['VALOR DE LA SECUENCIA'], 5, ',', '.');
                                    $row['SECUENCIA'] = $k;

                                    unset($row['id']);
                                    unset($row['iva']);
                                    unset($row['type']);
                                    unset($row['store_product_id']);
                                    unset($row['order_product_id']);
                                    $k++;
                                    fputcsv($fp, $row);
                                }

                                if ($order_products_quantity == $i)
                                {
                                   //escribir IVA
                                    $rows_iva = $this->get_iva($order_id);
                                    foreach ($rows_iva as $row_iva){
                                        $row_iva = json_decode(json_encode($row_iva), true);
                                        $row_iva['VALOR DE LA SECUENCIA'] = number_format($row_iva['VALOR DE LA SECUENCIA'],5,",",".");
                                        $row_iva['SECUENCIA'] = $k;
                                        $k++;
                                        fputcsv($fp, $row_iva);
                                    }

                                    //escribir domicilio
                                    $rows_delivery = $this->get_delivery($order_id);
                                    foreach ($rows_delivery as $row_delivery){
                                        $row_delivery = json_decode(json_encode($row_delivery), true);
                                        $row_delivery['VALOR DE LA SECUENCIA'] = number_format($row_delivery['VALOR DE LA SECUENCIA'],5,",",".");
                                        $row_delivery['SECUENCIA'] = $k;
                                        $k++;
                                        fputcsv($fp, $row_delivery);
                                    }

                                    //escribir descuento
                                    $rows_discount = $this->get_discount($order_id);
                                    foreach ($rows_discount as $row_discount){
                                        $row_discount = json_decode(json_encode($row_discount), true);
                                        if($row_discount['VALOR DE LA SECUENCIA'] > 0){
                                            $row_discount['VALOR DE LA SECUENCIA'] = number_format($row_discount['VALOR DE LA SECUENCIA'],5,",",".");
                                            $row_discount['SECUENCIA'] = $k;
                                            $k++;
                                            fputcsv($fp, $row_discount);
                                        }
                                    }

                                    //escribir cuenta de cliente
                                    $client_account = $this->get_client_account($order_id);
                                    foreach ($client_account as $rowa){
                                        $rowa = json_decode(json_encode($rowa), true);
                                        $rowa['VALOR DE LA SECUENCIA'] = number_format($rowa['VALOR DE LA SECUENCIA'],5,",",".");
                                        $rowa['SECUENCIA'] = $k;
                                        $k++;
                                        fputcsv($fp, $rowa);
                                    }

                                    $i = 1;
                                    $k = 1;
                                }
                                $i++;
                            }

                            fclose($fp);
                            $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
                            $file['client_original_name'] = $filename;
                            $file['client_original_extension'] = 'csv';
                            $url = upload_image($file, false, Config::get('app.download_directory_temp'));

                            #exit;

                            return Redirect::route('adminAccounting.invoices')->with('success', 'Archivo plano generado con éxito.')->with('file_url', $url);

                        }else return Redirect::route('adminAccounting.invoices')->with('error', 'Datos no encontrado.');
                    break;

                    default:
                        return Redirect::back()->with('error', 'Tipo de archivo plano es requerido.');
                    break;
                }
            }else return Redirect::back()->with('error', 'No se envio tipo de archivo plano a descargar.');
        }

        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->where('is_storage', 1)->get();
        return View::make('admin.accounting.invoices')
                    ->with('title', 'Archivo contable')
                    ->with('sub_title', 'Facturación')
                    ->with('stores', $stores);
    }

    private function get_delivery($order_id){

        $sql = "SELECT
                    /*'F' AS 'TIPO DE COMPROBANTE',
                    SUBSTRING(o.invoice_number,2,1) AS 'CÓDIGO COMPROBANTE',
                    SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO',*/
                    'F' AS 'TIPO DE COMPROBANTE',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,2,2), SUBSTRING(o.invoice_number,2,1))  AS 'CÓDIGO COMPROBANTE',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,5,3), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,4,3), SUBSTRING(o.invoice_number,4,2))) AS 'PREFIJO',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,8,10), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,7,10), SUBSTRING(o.invoice_number,6,10))) AS 'NÚMERO DE DOCUMENTO',
                    '4145510000' AS 'CUENTA CONTABLE',
                    'C' AS 'DÉBITO O CRÉDITO',
                    o.delivery_amount AS 'VALOR DE LA SECUENCIA',
                    DATE_FORMAT(management_date, '%Y') AS 'AÑO DEL DOCUMENTO',
                    DATE_FORMAT(management_date, '%m') AS 'MES DEL DOCUMENTO',
                    DATE_FORMAT(management_date, '%d') AS 'DÍA DEL DOCUMENTO',
                    '0'  AS 'CÓDIGO DEL VENDEDOR',
                    og.user_city_id AS 'CÓDIGO DE LA CIUDAD',
                    '0'  AS 'CÓDIGO DE LA ZONA',
                    '' AS 'SECUENCIA',
                    '0' AS 'CENTRO DE COSTO',
                    '0' AS 'SUBCENTRO DE COSTO',
                    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ),1, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ))-1) AS 'NIT',
                    '0' AS SUCURSAL,
                    CONCAT(CONCAT('PEDIDO',' ', o.id), ' ', 'DOMICILIO') AS 'DESCRIPCIÓN DE LA SECUENCIA',
                    ''  AS 'NÚMERO DE CHEQUE',
                    'N' AS 'COMPROBANTE ANULADO',
                    '0' AS 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN',
                    '0' AS 'FORMA DE PAGO',
                    '0,00'  AS 'PORCENTAJE DEL IVA DE LA SECUENCIA',
                    '0,00' AS 'VALOR DE IVA DE LA SECUENCIA',
                    '' AS 'BASE DE RETENCIÓN',
                    '0,00' AS 'BASE PARA CUENTAS MARCADAS COMO RETEIVA',
                    'N' AS 'SECUENCIA GRAVADA O EXCENTA',
                    ''  AS 'PORCENTAJE AIU',
                    ''  AS 'BASE IVA AIU',
                    '' AS 'LÍNEA PRODUCTO',
                    '' AS 'GRUPO PRODUCTO',
                    '' AS 'CÓDIGO PRODUCTO',
                    0 AS CANTIDAD,
                    '' AS 'CANTIDAD DOS',
                    '1' AS 'CÓDIGO DE LA BODEGA',
                    '0' AS 'CÓDIGO DE LA UBICACIÓN',
                    '0' AS 'CANTIDAD DE FACTOR DE CONVERSIÓN',
                    '0' AS 'OPERADOR DE FACTOR DE CONVERSIÓN',
                    '0' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                    '' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                    '' AS 'GRUPO ACTIVOS',
                    '' AS 'CÓDIGO ACTIVO',
                    '0' AS 'ADICIÓN O MEJORA',
                    '0' AS 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA',
                    '0' AS 'VECES A DEPRECIAR NIIF',
                    '0' AS 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR',
                    '' AS 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR',
                    '' AS 'AÑO DOCUMENTO DEL PROVEEDOR',
                    '' AS 'MES DOCUMENTO DEL PROVEEDOR',
                    '' AS 'DÍA DOCUMENTO DEL PROVEEDOR',
                    '0' AS 'TIPO DOCUMENTO DE PEDIDO',
                    '0' AS 'CÓDIGO COMPROBANTE DE PEDIDO',
                    '0' AS 'NÚMERO DE COMPROBANTE PEDIDO',
                    '0' AS 'SECUENCIA DE PEDIDO',
                    '' AS 'TIPO Y COMPROBANTE CRUCE',
                    '0' AS 'NÚMERO DE DOCUMENTO CRUCE',
                    '0' AS 'NÚMERO DE VENCIMIENTO',
                    DATE_FORMAT(management_date, '%Y') AS 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE',
                    DATE_FORMAT(management_date, '%m') AS 'MES VENCIMIENTO DE DOCUMENTO CRUCE',
                    DATE_FORMAT(management_date, '%d') AS 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE',
                    '0' AS 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE'
                    FROM orders o
                    INNER JOIN order_groups og ON og.id = o.group_id
                    INNER JOIN cities c ON og.user_city_id = c.id
                    INNER JOIN order_products op ON op.order_id = o.id
                    LEFT JOIN store_products sp ON sp.id = op.store_product_id
                    LEFT JOIN products p ON p.id = sp.product_id
                    LEFT JOIN shelves s ON s.id = sp.shelf_id
                    INNER JOIN users u ON og.user_id = u.id
                    WHERE o.id = ".$order_id." AND op.fulfilment_status = 'Fullfilled'
                    GROUP BY o.id
                    ORDER BY o.id
               ";
        $rows = DB::select(DB::raw($sql));
        return $rows;

    }

    private function get_discount($order_id){

        $sql = "SELECT
                    /*'F' AS 'TIPO DE COMPROBANTE',
                    SUBSTRING(o.invoice_number,2,1) AS 'CÓDIGO COMPROBANTE',
                    SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO',*/
                    'F' AS 'TIPO DE COMPROBANTE',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,2,2), SUBSTRING(o.invoice_number,2,1))  AS 'CÓDIGO COMPROBANTE',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,5,3), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,4,3), SUBSTRING(o.invoice_number,4,2))) AS 'PREFIJO',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,8,10), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,7,10), SUBSTRING(o.invoice_number,6,10))) AS 'NÚMERO DE DOCUMENTO',
                    '5295850200' AS 'CUENTA CONTABLE',
                    'D' AS 'DÉBITO O CRÉDITO',
                    o.discount_amount AS 'VALOR DE LA SECUENCIA',
                    DATE_FORMAT(management_date, '%Y') AS 'AÑO DEL DOCUMENTO',
                    DATE_FORMAT(management_date, '%m') AS 'MES DEL DOCUMENTO',
                    DATE_FORMAT(management_date, '%d') AS 'DÍA DEL DOCUMENTO',
                    '0'  AS 'CÓDIGO DEL VENDEDOR',
                    og.user_city_id AS 'CÓDIGO DE LA CIUDAD',
                    '0'  AS 'CÓDIGO DE LA ZONA',
                    '' AS 'SECUENCIA',
                    '0' AS 'CENTRO DE COSTO',
                    '0' AS 'SUBCENTRO DE COSTO',
                    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ),1, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ))-1) AS 'NIT',
                    '0' AS SUCURSAL,
                    CONCAT(CONCAT('PEDIDO',' ', o.id), ' ', 'DESCUENTO') AS 'DESCRIPCIÓN DE LA SECUENCIA',
                    ''  AS 'NÚMERO DE CHEQUE',
                    'N' AS 'COMPROBANTE ANULADO',
                    '0' AS 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN',
                    '0' AS 'FORMA DE PAGO',
                    '0,00'  AS 'PORCENTAJE DEL IVA DE LA SECUENCIA',
                    '0,00' AS 'VALOR DE IVA DE LA SECUENCIA',
                    '' AS 'BASE DE RETENCIÓN',
                    '0,00' AS 'BASE PARA CUENTAS MARCADAS COMO RETEIVA',
                    'N' AS 'SECUENCIA GRAVADA O EXCENTA',
                    ''  AS 'PORCENTAJE AIU',
                    ''  AS 'BASE IVA AIU',
                    '' AS 'LÍNEA PRODUCTO',
                    '' AS 'GRUPO PRODUCTO',
                    '' AS 'CÓDIGO PRODUCTO',
                    '0,00000' AS CANTIDAD,
                    '' AS 'CANTIDAD DOS',
                    '1' AS 'CÓDIGO DE LA BODEGA',
                    '0' AS 'CÓDIGO DE LA UBICACIÓN',
                    '0,00000' AS 'CANTIDAD DE FACTOR DE CONVERSIÓN',
                    '0' AS 'OPERADOR DE FACTOR DE CONVERSIÓN',
                    '0' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                    '' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                    '' AS 'GRUPO ACTIVOS',
                    '' AS 'CÓDIGO ACTIVO',
                    '0' AS 'ADICIÓN O MEJORA',
                    '0' AS 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA',
                    '0' AS 'VECES A DEPRECIAR NIIF',
                    '0' AS 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR',
                    '' AS 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR',
                    '' AS 'AÑO DOCUMENTO DEL PROVEEDOR',
                    '' AS 'MES DOCUMENTO DEL PROVEEDOR',
                    '' AS 'DÍA DOCUMENTO DEL PROVEEDOR',
                    '0' AS 'TIPO DOCUMENTO DE PEDIDO',
                    '0' AS 'CÓDIGO COMPROBANTE DE PEDIDO',
                    '0' AS 'NÚMERO DE COMPROBANTE PEDIDO',
                    '0' AS 'SECUENCIA DE PEDIDO',
                    '' AS 'TIPO Y COMPROBANTE CRUCE',
                    '0' AS 'NÚMERO DE DOCUMENTO CRUCE',
                    '0' AS 'NÚMERO DE VENCIMIENTO',
                    DATE_FORMAT(management_date, '%Y') AS 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE',
                    DATE_FORMAT(management_date, '%m') AS 'MES VENCIMIENTO DE DOCUMENTO CRUCE',
                    DATE_FORMAT(management_date, '%d') AS 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE',
                    '0' AS 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE'
                    FROM orders o
                    INNER JOIN order_groups og ON og.id = o.group_id
                    INNER JOIN cities c ON og.user_city_id = c.id
                    INNER JOIN order_products op ON op.order_id = o.id
                    LEFT JOIN store_products sp ON sp.id = op.store_product_id
                    LEFT JOIN products p ON p.id = sp.product_id
                    LEFT JOIN shelves s ON s.id = sp.shelf_id
                    INNER JOIN users u ON og.user_id = u.id
                    WHERE o.id = ".$order_id." AND op.fulfilment_status = 'Fullfilled'
                    GROUP BY o.id
                    ORDER BY o.id
               ";
        $rows = DB::select(DB::raw($sql));
        return $rows;

    }


    private function get_iva($order_id){

        $sql = "SELECT
                /*'F' AS 'TIPO DE COMPROBANTE',
                SUBSTRING(o.invoice_number,2,1) AS 'CÓDIGO COMPROBANTE',
                SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO',*/
                'F' AS 'TIPO DE COMPROBANTE',
                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,2,2), SUBSTRING(o.invoice_number,2,1))  AS 'CÓDIGO COMPROBANTE',
                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,5,3), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,4,3), SUBSTRING(o.invoice_number,4,2))) AS 'PREFIJO',
                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,8,10), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,7,10), SUBSTRING(o.invoice_number,6,10))) AS 'NÚMERO DE DOCUMENTO',
                IF(op.iva = 19, 2408050100, 2408050200 ) AS 'CUENTA CONTABLE',
                'C' AS 'DÉBITO O CRÉDITO',
                SUM((op.price - (op.price / ((op.iva / 100) + 1))) * op.quantity) AS 'VALOR DE LA SECUENCIA',
                DATE_FORMAT(management_date, '%Y') AS 'AÑO DEL DOCUMENTO',
                DATE_FORMAT(management_date, '%m') AS 'MES DEL DOCUMENTO',
                DATE_FORMAT(management_date, '%d') AS 'DÍA DEL DOCUMENTO',
                '0'  AS 'CÓDIGO DEL VENDEDOR',
                og.user_city_id AS 'CÓDIGO DE LA CIUDAD',
                '0'AS 'CÓDIGO DE LA ZONA',
                '' AS 'SECUENCIA',
                '0' AS 'CENTRO DE COSTO',
                '0' AS 'SUBCENTRO DE COSTO',
                SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ),1, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ))-1) AS 'NIT',
                '0' AS SUCURSAL,
                CONCAT(CONCAT('PEDIDO',' ', o.id), ' ', 'IVA') AS 'DESCRIPCIÓN DE LA SECUENCIA',
                '' AS 'NÚMERO DE CHEQUE',
                'N' AS 'COMPROBANTE ANULADO',
                '0' AS 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN',
                '0' AS 'FORMA DE PAGO',
                 CONCAT('0,','',LPAD(IF(op.iva IS NULL, sp.iva, op.iva), 2, '0'))  AS 'PORCENTAJE DEL IVA DE LA SECUENCIA',
                '0,00' AS 'VALOR DE IVA DE LA SECUENCIA',
                '' AS 'BASE DE RETENCIÓN',
                '0,00' AS 'BASE PARA CUENTAS MARCADAS COMO RETEIVA',
                'N' AS 'SECUENCIA GRAVADA O EXCENTA',
                ''  AS 'PORCENTAJE AIU',
                ''  AS 'BASE IVA AIU',
                '' AS 'LÍNEA PRODUCTO',
                '' AS 'GRUPO PRODUCTO',
                '' AS 'CÓDIGO PRODUCTO',
                '0' AS CANTIDAD,
                '0' AS 'CANTIDAD DOS',
                '1' AS 'CÓDIGO DE LA BODEGA',
                '0' AS 'CÓDIGO DE LA UBICACIÓN',
                '0,00000' AS 'CANTIDAD DE FACTOR DE CONVERSIÓN',
                '0' AS 'OPERADOR DE FACTOR DE CONVERSIÓN',
                '0,00000' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                '0,00000' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                '' AS 'GRUPO ACTIVOS',
                '' AS 'CÓDIGO ACTIVO',
                '0' AS 'ADICIÓN O MEJORA',
                '0' AS 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA',
                '0' AS 'VECES A DEPRECIAR NIIF',
                '0' AS 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR',
                '' AS 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR',
                '' AS 'AÑO DOCUMENTO DEL PROVEEDOR',
                '' AS 'MES DOCUMENTO DEL PROVEEDOR',
                '' AS 'DÍA DOCUMENTO DEL PROVEEDOR',
                '' AS 'TIPO DOCUMENTO DE PEDIDO',
                '0' AS 'CÓDIGO COMPROBANTE DE PEDIDO',
                '0' AS 'NÚMERO DE COMPROBANTE PEDIDO',
                '0' AS 'SECUENCIA DE PEDIDO',
                '' AS 'TIPO Y COMPROBANTE CRUCE',
                '0' AS 'NÚMERO DE DOCUMENTO CRUCE',
                '0' AS 'NÚMERO DE VENCIMIENTO',
                '' AS 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE',
                '' AS 'MES VENCIMIENTO DE DOCUMENTO CRUCE',
                '' AS 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE',
                '0' AS 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE'
                FROM order_products op
                LEFT JOIN store_products sp ON sp.id = op.store_product_id
                LEFT JOIN products p ON p.id = sp.product_id
                LEFT JOIN shelves s ON s.id = sp.shelf_id
                INNER JOIN orders o ON op.order_id = o.id
                INNER JOIN stores st ON o.store_id = st.id
                INNER JOIN order_groups og ON o.group_id = og.id
                INNER JOIN cities c ON og.user_city_id = c.id
                INNER JOIN users u ON og.user_id = u.id
                WHERE o.id = ".$order_id." AND op.fulfilment_status = 'Fullfilled'
                GROUP BY op.order_id, op.iva
                ORDER BY o.id ASC";
        $rows = DB::select(DB::raw($sql));
        return $rows;

    }

    private function get_client_account($order_id)
    {

        $sql = "SELECT
                /*'F' AS 'TIPO DE COMPROBANTE',
                SUBSTRING(o.invoice_number,2,1) AS 'CÓDIGO COMPROBANTE',
                SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO',*/
                'F' AS 'TIPO DE COMPROBANTE',
                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,2,2), SUBSTRING(o.invoice_number,2,1))  AS 'CÓDIGO COMPROBANTE',
                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,5,3), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,4,3), SUBSTRING(o.invoice_number,4,2))) AS 'PREFIJO',
                IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,8,10), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,7,10), SUBSTRING(o.invoice_number,6,10))) AS 'NÚMERO DE DOCUMENTO',
                1305050000 AS 'CUENTA CONTABLE',
                'D' AS 'DÉBITO O CRÉDITO',
                o.total_amount + o.delivery_amount - o.discount_amount AS 'VALOR DE LA SECUENCIA',
                DATE_FORMAT(management_date, '%Y') AS 'AÑO DEL DOCUMENTO',
                DATE_FORMAT(management_date, '%m') AS 'MES DEL DOCUMENTO',
                DATE_FORMAT(management_date, '%d') AS 'DÍA DEL DOCUMENTO',
                '0'  AS 'CÓDIGO DEL VENDEDOR',
                og.user_city_id AS 'CÓDIGO DE LA CIUDAD',
                '0'  AS 'CÓDIGO DE LA ZONA',
                '' AS 'SECUENCIA',
                '0' AS 'CENTRO DE COSTO',
                '0' AS 'SUBCENTRO DE COSTO',
                SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ),1, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ))-1) AS 'NIT',
                '0' AS SUCURSAL,
                CONCAT(CONCAT('CLIENTES ',' ', o.id), ' ', 'X COBRAR') AS 'DESCRIPCIÓN DE LA SECUENCIA',
                ''  AS 'NÚMERO DE CHEQUE',
                'N' AS 'COMPROBANTE ANULADO',
                '0' AS 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN',
                '0' AS 'FORMA DE PAGO',
                '0,00'  AS 'PORCENTAJE DEL IVA DE LA SECUENCIA',
                '0,00' AS 'VALOR DE IVA DE LA SECUENCIA',
                '' AS 'BASE DE RETENCIÓN',
                '0,00' AS 'BASE PARA CUENTAS MARCADAS COMO RETEIVA',
                'N' AS 'SECUENCIA GRAVADA O EXCENTA',
                ''  AS 'PORCENTAJE AIU',
                ''  AS 'BASE IVA AIU',
                '' AS 'LÍNEA PRODUCTO',
                '' AS 'GRUPO PRODUCTO',
                '' AS 'CÓDIGO PRODUCTO',
                '0' AS CANTIDAD,
                '0' AS 'CANTIDAD DOS',
                '1' AS 'CÓDIGO DE LA BODEGA',
                '0' AS 'CÓDIGO DE LA UBICACIÓN',
                '0,00000' AS 'CANTIDAD DE FACTOR DE CONVERSIÓN',
                '0' AS 'OPERADOR DE FACTOR DE CONVERSIÓN',
                '0,00000' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                '' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                '' AS 'GRUPO ACTIVOS',
                '' AS 'CÓDIGO ACTIVO',
                '0' AS 'ADICIÓN O MEJORA',
                '0' AS 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA',
                '0' AS 'VECES A DEPRECIAR NIIF',
                '0' AS 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR',
                '' AS 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR',
                '' AS 'AÑO DOCUMENTO DEL PROVEEDOR',
                '' AS 'MES DOCUMENTO DEL PROVEEDOR',
                '' AS 'DÍA DOCUMENTO DEL PROVEEDOR',
                '' AS 'TIPO DOCUMENTO DE PEDIDO',
                '0' AS 'CÓDIGO COMPROBANTE DE PEDIDO',
                '0' AS 'NÚMERO DE COMPROBANTE PEDIDO',
                '0' AS 'SECUENCIA DE PEDIDO',
                CONCAT('F-','00',IF(og.user_city_id = 1, 3, 2)) AS 'TIPO Y COMPROBANTE CRUCE',
                SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO CRUCE',
                '5' AS 'NÚMERO DE VENCIMIENTO',
                DATE_FORMAT(management_date, '%Y') AS 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE',
                DATE_FORMAT(management_date, '%m') AS 'MES VENCIMIENTO DE DOCUMENTO CRUCE',
                DATE_FORMAT(management_date, '%d') AS 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE',
                '0' AS 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE'
                FROM orders o
                INNER JOIN order_groups og ON og.id = o.group_id
                INNER JOIN order_products op ON op.order_id = o.id
                INNER JOIN cities c ON og.user_city_id = c.id
                LEFT JOIN store_products sp ON sp.id = op.store_product_id
                LEFT JOIN products p ON p.id = sp.product_id
                LEFT JOIN shelves s ON s.id = sp.shelf_id
                INNER JOIN users u ON og.user_id = u.id
                WHERE o.id = ".$order_id." AND op.fulfilment_status = 'Fullfilled'
                GROUP BY o.id
                ORDER BY o.id;
               ";

        $rows = DB::select(DB::raw($sql));
        return $rows;
    }

    private function get_product_group($product_id)
    {
        ini_set('memory_limit', '4096M');
        set_time_limit(0);
        
        if($product_id){
        DB::statement("SET lc_time_names = 'es_ES'");
            $sql = "SELECT
                    /*'F' AS 'TIPO DE COMPROBANTE',
                    SUBSTRING(o.invoice_number,2,1) AS 'CÓDIGO COMPROBANTE',
                    SUBSTRING(o.invoice_number,4) AS 'NÚMERO DE DOCUMENTO',*/
                    'F' AS 'TIPO DE COMPROBANTE',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,2,2), SUBSTRING(o.invoice_number,2,1))  AS 'CÓDIGO COMPROBANTE',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,5,3), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,4,3), SUBSTRING(o.invoice_number,4,2))) AS 'PREFIJO',
                    IF(SUBSTRING(o.invoice_number,2,2) = 10 , SUBSTRING(o.invoice_number,8,10), IF(SUBSTRING(o.invoice_number,2,1) > 4 ,SUBSTRING(o.invoice_number,7,10), SUBSTRING(o.invoice_number,6,10))) AS 'NÚMERO DE DOCUMENTO',
                    p.accounting_account AS 'CUENTA CONTABLE',
                    'C' AS 'DÉBITO O CRÉDITO',
                    op.iva,
                    oc.price,
                    oc.quantity,
                    (SELECT SUM(order_product_group.quantity) AS total_products FROM order_product_group WHERE order_product_id = $product_id) AS quantity_combo,
                    (SELECT SUM(order_product_group.price * order_product_group.quantity) AS total_products FROM order_product_group WHERE order_product_id = $product_id) AS total,
                    op.price * op.quantity AS price_combo,
                    '' AS 'VALOR DE LA SECUENCIA',
                    DATE_FORMAT(management_date, '%Y') AS 'AÑO DEL DOCUMENTO',
                    DATE_FORMAT(management_date, '%m') AS 'MES DEL DOCUMENTO',
                    DATE_FORMAT(management_date, '%d') AS 'DÍA DEL DOCUMENTO',
                    '0'  AS 'CÓDIGO DEL VENDEDOR',
                    og.user_city_id AS 'CÓDIGO DE LA CIUDAD',
                    '0'  AS 'CÓDIGO DE LA ZONA',
                    '' AS 'SECUENCIA',
                    '0' AS 'CENTRO DE COSTO',
                    '0' AS 'SUBCENTRO DE COSTO',
                    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ),1, CHAR_LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(IF(o.user_identity_number <> '',o.user_identity_number,IF(u.identity_number <> '',u.identity_number, IF(u.identity_number <> 0 ,u.identity_number,'2222222222')))), '-', ''),',', ''),'.',''),' ','' ))-1) AS 'NIT',
                    '0' AS SUCURSAL,
                    CONCAT('PEDIDO',' ', o.id, '-', (SELECT product_name FROM provider_order_details WHERE store_product_id = oc.store_product_id LIMIT 1)) AS 'DESCRIPCIÓN DE LA SECUENCIA',
                    ''  AS 'NÚMERO DE CHEQUE',
                    'N' AS 'COMPROBANTE ANULADO',
                    '0' AS 'CÓDIGO DEL MOTIVO DE DEVOLUCIÓN',
                    '0' AS 'FORMA DE PAGO',
                    oc.iva  AS 'PORCENTAJE DEL IVA DE LA SECUENCIA',
                    '0,00' AS 'VALOR DE IVA DE LA SECUENCIA',
                    '' AS 'BASE DE RETENCIÓN',
                    '0,00' AS 'BASE PARA CUENTAS MARCADAS COMO RETEIVA',
                    'N' AS 'SECUENCIA GRAVADA O EXCENTA',
                    ''  AS 'PORCENTAJE AIU',
                    ''  AS 'BASE IVA AIU',
                    p.accounting_line AS 'LÍNEA PRODUCTO',
                    p.accounting_group AS 'GRUPO PRODUCTO',
                    p.accounting_code AS 'CÓDIGO PRODUCTO',
                    /*CONCAT('0,','',LPAD(op.quantity, 5, '0')) AS CANTIDAD,*/
                    op.quantity AS CANTIDAD,
                    '0,00000' AS 'CANTIDAD DOS',
                    '1' AS 'CÓDIGO DE LA BODEGA',
                    '0' AS 'CÓDIGO DE LA UBICACIÓN',
                    '0,00000' AS 'CANTIDAD DE FACTOR DE CONVERSIÓN',
                    '0' AS 'OPERADOR DE FACTOR DE CONVERSIÓN',
                    '0,00000' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                    '' AS 'VALOR DEL FACTOR DE CONVERSIÓN',
                    '' AS 'GRUPO ACTIVOS',
                    '' AS 'CÓDIGO ACTIVO',
                    '0' AS 'ADICIÓN O MEJORA',
                    '0' AS 'VECES ADICIONALES A DEPRECIAR POR ADICIÓN O MEJORA',
                    '0' AS 'VECES A DEPRECIAR NIIF',
                    '0' AS 'NÚMERO DEL DOCUMENTO DEL PROVEEDOR',
                    '' AS 'PREFIJO DEL DOCUMENTO DEL PROVEEDOR',
                    '' AS 'AÑO DOCUMENTO DEL PROVEEDOR',
                    '' AS 'MES DOCUMENTO DEL PROVEEDOR',
                    '' AS 'DÍA DOCUMENTO DEL PROVEEDOR',
                    '0' AS 'TIPO DOCUMENTO DE PEDIDO',
                    '0' AS 'CÓDIGO COMPROBANTE DE PEDIDO',
                    '0' AS 'NÚMERO DE COMPROBANTE PEDIDO',
                    '0' AS 'SECUENCIA DE PEDIDO',
                    '' AS 'TIPO Y COMPROBANTE CRUCE',
                    '0' AS 'NÚMERO DE DOCUMENTO CRUCE',
                    '0' AS 'NÚMERO DE VENCIMIENTO',
                    DATE_FORMAT(management_date, '%Y') AS 'AÑO VENCIMIENTO DE DOCUMENTO CRUCE',
                    DATE_FORMAT(management_date, '%m') AS 'MES VENCIMIENTO DE DOCUMENTO CRUCE',
                    DATE_FORMAT(management_date, '%d') AS 'DÍA VENCIMIENTO DE DOCUMENTO CRUCE',
                    '0' AS 'NÚMERO DE CAJA ASOCIADA AL COMPROBANTE'
                    FROM order_product_group oc
                    INNER JOIN orders o ON o.id = oc.order_id
                    INNER JOIN order_groups og ON og.id = o.group_id
                    INNER JOIN cities c ON og.user_city_id = c.id
                    INNER JOIN users u ON og.user_id = u.id
                    LEFT JOIN store_products sp ON sp.id = oc.store_product_id
                    LEFT JOIN products p ON p.id = sp.product_id
                    INNER JOIN order_products op ON op.id = oc.order_product_id
                    WHERE order_product_id = ".$product_id;

        $rows = DB::select(DB::raw($sql));
        return $rows;

        }else
            return false;
    }
}
