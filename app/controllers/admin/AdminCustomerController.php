<?php

namespace admin;

use Aws\CloudFront\Exception\Exception;
use Request, Response, View, Input, Redirect, Session, DB, Store, City, Customer, User, UserCreditCard, PayU,
    OrderGroup, OrderPayment, UserCredit, Order;

class AdminCustomerController extends AdminController
{
	/**
     * Grilla de clientes
     */
    public function index()
    {
        if (!Request::ajax()){
            return View::make('admin.customers.index')->with('title', 'Clientes');
        }else{
            $search = Input::has('s') ? Input::get('s') : '';

            $customers = User::where(function($query) use ($search){
                $query->where(DB::raw('CONCAT(first_name, " ", last_name)'), 'like', '%'.$search.'%');
                $query->orWhere('email', 'like', '%'.$search.'%');
                $query->orWhere('phone', 'like', '%'.$search.'%');
                $query->orWhere('referral_code', 'like', '%'.$search.'%');
                $query->orWhere('id', 'like', '%'.$search.'%');
            })->orderBy('first_name','asc')->limit(40)->get();
        }

        return View::make('admin.customers.index')
                    ->with('title', 'Clientes')
                    ->with('customers', $customers)
                    ->renderSections()['content'];
    }

    /**
     * Editar clientes
     */
    public function edit()
    {
        $user_credit_cars = [];
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Cliente' : 'Agregar Cliente';
        if ($id) {
            $customer = User::with('orders', 'addresses', 'referrals')->find($id);
            $customer->orders_count = $customer->orders->count();
            $customer->orders_delivered_count = $customer->orders()->where('status', 'Delivered')->get()->count();
            $customer->orders_cancelled_count = $customer->orders()->where('status', 'Cancelled')->get()->count();
            $customer->prom_orders = ($customer->orders->count() != 0) ? $customer->orders->sum('total_amount') / $customer->orders->count() : 0;
            $last_order = Order::with('store.city')->where('user_id', $id)->orderByRaw('orders.created_at DESC')->first();
            $customer->last_order = $last_order;
            $customer->current_credits = User::getDiscounts($customer->id)['amount'];
            $user_credit_cars = UserCreditCard::select('user_credit_cards.id','user_credit_cards.type', 'user_credit_cards.last_four', 'user_credit_cards.created_at', 'user_credit_cards.holder_document_number')
                ->join('users', 'users.id', '=', 'user_credit_cards.user_id')
                ->where('users.id', $id)
                ->get();
        }
        else $customer = false;
        $cities = City::all();
        return View::make('admin.customers.customer_form')
                ->with('title', 'Cliente')
                ->with('sub_title', $sub_title)
                ->with('cities', $cities)
                ->with('user_credit_cars', $user_credit_cars)
                ->with('customer', $customer);
    }

    /**
     * Guardar cliente
     */
    public function save()
    {
        $id = Input::get('id');
        $customer = User::find($id);
        if(!$customer){
            $customer = new User;
            $customer->type = 'callcenter';
            $action = 'insert';
        }else $action = 'update';
        $customer->first_name = Input::get('first_name');
        $customer->last_name = Input::get('last_name');
        if ($customer->phone != Input::get('phone')){
            if ($user = User::where('phone', Input::get('phone'))->first()){
                $user->phone_validated_date = null;
            }
            $customer->phone_validated_date = null;
        }
        $customer->phone = Input::get('phone');
        $customer->email = Input::get('email');
        $customer->free_delivery_expiration_date = ( empty(Input::get('free_delivery_expiration_date')) ? NULL : format_date("mysql", Input::get('free_delivery_expiration_date')) );
        if(!$customer->referral_code){
            $customer->referral_code = User::generateReferralCode(Input::get('first_name'),Input::get('last_name'));
        }
        if(Input::has('password')){
            $customer->password = Hash::make(Input::get('password'));
        }
        $customer->free_delivery_next_order = Input::get('free_delivery_next_order');
        $customer->send_advertising = Input::get('send_advertising');
        $customer->status = Input::get('status');
        $customer->free_delivery_next_order_expiration_date = ( empty(Input::get('free_delivery_next_order_expiration_date')) ? NULL : format_date("mysql", Input::get('free_delivery_next_order_expiration_date')) );
        $customer->save();
        $this->admin_log('users', $customer->id, $action);

        return  Redirect::route('adminCustomers.index')->with('type', 'success')->with('message', 'Successfully updated the customer');
    }

    /**
     * Elimina cliente
     */
    public function delete()
    {
        $id = Request::segment(4);
        $customer = User::find($id);
        User::where('id', $id)->update(array('status' => 0));
        return  Redirect::route('adminCustomers.index')->with('type', 'success')->with('message', 'Successfully deleted the customer');
    }

    /**
     * Realizar cobro a tarjeta de credito
     */
    public function charge()
    {
        $post = Session::has('post') ? Session::get('post') : array();

        if (Request::isMethod('post') && Input::has('id'))
        {
            $user = User::find(Input::get('id'));
            $credit_card = UserCreditCard::where('user_id', $user->id)
                         ->where('id', '=', Input::get('credit_card_id'))
                         ->first();
            if ($credit_card){

                if (!$order = Order::find(Input::get('order_id'))){
                    return Redirect::back()->with('type', 'error')->with('message', 'El pedido no existe.')
                        ->with('post', Input::all());
                }

                $total_amount = Input::get('total_amount');
                $installments = Input::get('installments');

                $data = array(
                    'user_id' => $user->id,
                    'total_amount' => floatval($total_amount),
                    'tax_amount' => 0, //Input::get('total_amount') * 0.16,
                    'card_token' => $credit_card->card_token,
                    'installments_cc' => intval($installments),
                    'order_id' => Input::get('order_id'),
                    'description' => Input::get('description'),
                );

                $data['total_base'] = 0;
                $data['iva'] = 0;
                $data['card_type'] = $order->cc_type;
                $data['security_code'] = $order->cc_type == 'AMEX' ? '0000' : '000';
                $data['device_session_id'] = md5(session_id().microtime());
                $data['referenceCode'] = $order->reference;

                $user = User::find($order->user_id);
                $order_group = OrderGroup::find($order->group_id);
                $data['buyer']['document_number'] = $user->identity_number;
                $data['buyer']['fullname'] = $order_group->user_firstname.' '.$order_group->user_lastname;
                $data['buyer']['email'] = $order_group->user_email;
                $data['buyer']['phone'] = $order_group->user_phone;
                $data['buyer']['address'] = $order_group->user_address;
                $data['buyer']['address_further'] = $order_group->user_address_further;
                $city = City::find($order_group->user_city_id);
                $data['buyer']['city'] = $city->city;
                $data['buyer']['state'] = $city->state;

                if (!$user_credit_card = UserCreditCard::find(Input::get('credit_card_id'))){
                    return Redirect::back()->with('type', 'error')->with('message', 'La tarjeta no existe.')
                        ->with('post', Input::all());
                }

                $data['payer']['document_number'] = $order->cc_holder_document_number;
                $data['payer']['document_type'] = $order->cc_holder_document_type;
                $data['payer']['fullname'] = $order->cc_holder_name;
                $data['payer']['email'] = !empty($order->cc_holder_email) ? $order->cc_holder_email : $user_credit_card->holder_email;
                $data['payer']['phone'] = $order->cc_holder_phone;
                $data['payer']['address'] = !empty($order->cc_holder_address) ? $order->cc_holder_address : $user_credit_card->holder_address;
                $data['payer']['address_further'] = $user_credit_card->holder_address_further;
                $data['payer']['city'] = !empty($order->cc_holder_city) ? $order->cc_holder_city : $user_credit_card->holder_city;

                $payu = new PayU;
                $result = $payu->createCreditCardCharge($data);
                if (!$result['status']){
                    $msg = isset($result['response']->transactionResponse->responseCode) ? $payu->getErrorMessage($result['response']->transactionResponse->responseCode) : 'No especificado.';
                    return Redirect::back()->with('type', 'error')->with('message', 'Cobro a tarjeta rechazado. Motivo: '.$msg)
                        ->with('post', Input::all());
                }

                $order_payment = new OrderPayment;
                $order_payment->order_id = $order->id;
                if ($result['response']->transactionResponse->state == 'APPROVED'){
                    $order_payment->cc_payment_status = 'Aprobada';
                    $order->payment_date = date('Y-m-d H:i:s');
                }else if ($result['response']->transactionResponse->state == 'DECLINED')
                    $order_payment->cc_payment_status = 'Declinada';
                else if ($result['response']->transactionResponse->state == 'EXPIRED')
                    $order_payment->cc_payment_status = 'Expirada';
                $order_payment->cc_payment_description = $result['response']->transactionResponse->responseCode;
                $order_payment->cc_payment_transaction_id = $result['response']->transactionResponse->transactionId;
                $order_payment->cc_charge_id = $result['response']->transactionResponse->orderId;
                $order_payment->cc_payment_date = date('Y-m-d H:i:s');
                $order_payment->admin_id = Session::get('admin_id');
                $order_payment->save();

                return Redirect::back()->with('type', 'success')->with('message', 'Se ha realizado el cobro con éxito.');

            }else{
                return Redirect::back()->with('type', 'error')->with('message', 'Datos no validos.')
                                       ->with('post', Input::all());
            }
        }else{
            $id = Request::segment(4) ? Request::segment(4) : 0;
            $user = User::find($id);
        }
        $stores = Store::where('status', '=', 1)->get();
        $credit_cards = UserCreditCard::where('user_id', '=', $user->id)->get();
        return View::make('admin.customers.charge_form')
                    ->with('user', $user)
                    ->with('credit_cards', $credit_cards)
                    ->with('stores', $stores)
                    ->with('post', $post);
    }

    /**
     * @description Cambiar o adicionar cedula a tarjeta de credito
     * @return mixed
     */
    public function change_identity_number_ajax()
    {
        $status = 502;
        try {
            DB::beginTransaction();

            if (Input::has('user_id') && Input::has('identity_number') && Input::has('last_numbers')) {
                $user = User::find(Input::get('user_id'));
                $credit_cart = UserCreditCard::find(Input::get('last_numbers'));
                $credit_cart->holder_document_number = Input::get('identity_number');
                $credit_cart->save();
                DB::commit();
                $response['status'] = 'true';
                $response['result']['user'] = $user;
                $response['message'] = 'Usuario actualizado con éxito';
                $status = 200;
            } else {
                $response['status'] = 'false';
                $response['message'] = 'Error al actualizar el documento de identidad';
                $status = 401;
            }
            return Response::json($response, $status);
        } catch (Exception $e) {
            return Response::json($e, $status);
        }
    }

    /**
    * Adicionar credito a los clientes
    *
    * @param int $user_id ID de usuario
    */
    public function add_credit($id)
    {
        $user = User::find($id);

        if (!Request::ajax())
        {
            $discounts = User::getDiscounts($id);
                   
            return View::make( 'admin.customers.add_credit_form' )
                ->with( 'title', 'Agregar crédito a cliente' )
                ->with( 'customer', $user )
                ->with( 'credit_available', $discounts['amount'] );
        }else{
            try {
                DB::beginTransaction();

                if (Input::has('amount'))
                {
                    if (Input::get('type') == 1) {
                        list($day, $month, $year) = explode('/', Input::get('expiration_date'));
                        $expiration_date = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));

                        if ($expiration_date < date('Y-m-d')) {
                            return Response::json(['status' => false, 'message' => 'La fecha de expiración debe ser mayor a la actual.'], 200);
                        }
                        if (Input::has('products_related')){
                            UserCredit::addCredit($user, Input::get('amount'), $expiration_date, 'admin', null, Input::get('claim_motive'), Input::get('products_related'));
                        } else {
                            UserCredit::addCredit($user, Input::get('amount'), $expiration_date, 'admin', null, Input::get('claim_motive'));
                        }
                    } else {
                        if (Input::has('products_related')){
                            UserCredit::removeCredit($user, Input::get('amount'), 'admin', null, null,null, Input::get('claim_motive'), Input::get('products_related'));
                        } else {
                            UserCredit::removeCredit($user, Input::get('amount'), 'admin', null, null,null, Input::get('claim_motive'));
                        }
                    }

                    DB::commit();

                    $this->admin_log('user_credits', Session::get('admin_id'), 'insert');
                    
                    $response['status'] = true;
                    $response['message'] = 'Crédito procesado con éxito.';
                    $discounts = User::getDiscounts($id);
                    $response['result'] = number_format($discounts['amount'], 0, ',', '.');
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Error al procesar el crédito.';
                }
                return Response::json($response, 200);

            } catch (Exception $e) {
                DB::rollback();
                return Response::json($e->getMessage(), 500);
            }
           
        }
    }

    /**
     * Agregar credito Masivamente 
     */
    public function add_credit_multiple()
    {

        if(!Request::ajax())
        {
            return View::make( 'admin.customers.add_credit_multiple' )
                        ->with('title', 'Agregar Crédito Masivamente');
        }else{
            try{

                DB::beginTransaction();

                if (Input::hasFile('file')) {
                    ini_set('memory_limit', '1024M');
                    set_time_limit(0);
        
                    $file = Input::file('file');
                    if (!file_exists($file->getRealPath())) {
                        return Redirect::back()->with('error', 'No file uploaded');
                    }

                    $type = Input::get('type');
                    $amount = Input::get('amount');
                    $expiration_date = Input::get('expiration_date');

                    $types = [1 => 'Cargo de credito ', 0 => 'Deducción de credito ' ];

                    list($day, $month, $year) = explode('/', $expiration_date );
                    $expiration_date = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));


                    $path = $file->getRealPath();
                    $text = fopen($path, 'r');
                    $row_number = 0;
                    $row_number_error = 0;
                    while (!feof($text))
                    {
                        $row = fgetcsv($text, 1000, ';');
                        
                        if (!empty($row))
                        {
                            $data = array_map(function ($item) {
                                return trim($item);
                            }, $row);

                            if( !empty($data[0]) ){
                                $id_user = $data[0];
                                $user = User::find($id_user);

                                if ($expiration_date < date('Y-m-d'))
                                    return Response::json(['status' => false, 'message' => 'La fecha de expiración debe ser mayor a la actual.'], 200);

                                if($user){
                                    if($type==1){
                                        UserCredit::addCredit($user, $amount, $expiration_date, 'admin');
                                    }elseif($type==0){
                                        UserCredit::removeCredit($user, $amount, 'admin');
                                    }
                                    $row_number ++;
                                }else{
                                    $row_number_error ++;
                                }

                            } 

                        }
                        
                    }
                    fclose($text);
                    DB::commit();
                    return Response::json(
                        [
                            'status' => true, 
                            'message' => $types[$type]. ', Total: '. $row_number. ' Usuarios'
                        ], 200);

                }

            } catch(Exception $e) {
                DB::rollback();
                return Response::json($e->getMessage(), 500);
            }

            
        }

    }

    /**
     * Bloquea o desbloquea el código de referido
     * @param int $id id del cliente
    */
    public function block_referral_code($id) {
        $user = User::find($id);
        $user->referral_code_blocked = ($user->referral_code_blocked == 0) ? 1: 0;
        $user->save();
        return Redirect::back()->with('type', 'success')->with('message', 'Acción realizada exitosamente.');
    }
}