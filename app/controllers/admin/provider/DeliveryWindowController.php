<?php

namespace admin\provider;

use admin\AdminController, Request, Input, View, Response, ProviderOrderDeliveryWindow, Order;

class DeliveryWindowController extends AdminController
{
    /**
     * Muestra el listado de las franjas existentes
     *
     * @return mixed
     */
    public function index(){
        try{
            if(!Request::ajax()){
                $data = [
                    'title' => 'Franjas horarias para provedores',
                    'cities' => $this->get_cities()
                ];
                return View::make('admin.providers.delivery_windows.index', $data);
            }else{

                if(Input::get('hour_start') > Input::get('hour_end')){
                    return Response::json([
                        'status'=> false,
                        'message' => "La hora de inicio no puede ser mayor que la hora final",
                    ]);
                }
                $deliveryWindows = ProviderOrderDeliveryWindow::orderBy('warehouse_id')
                    ->orderBy('hour_start')
                    ->orderBy('status', 'desc');
                if(Input::has('warehouse_id') && !empty(Input::get('warehouse_id'))){
                    $deliveryWindows->where('warehouse_id', Input::get('warehouse_id'));
                }
                if(!empty(Input::get('hour_start'))){
                    $deliveryWindows->where('hour_start', '>=', Input::get('hour_start'));
                }
                if(!empty(Input::get('hour_end'))){
                    $deliveryWindows->where('hour_end', '<=', Input::get('hour_end'));
                }
                if(Input::has('status') && (Input::get('status') == 1 || Input::get('status') == 0)){
                    $deliveryWindows->where('status', Input::get('status'));
                }

                $deliveryWindows = $deliveryWindows
                    ->with('warehouse')
                    ->get();
                $data = [
                    'title' => 'Franjas Horarias',
                    'deliveryWindows' => $deliveryWindows
                ];

                return Response::json([
                    'status'=> true,
                    'result' => View::make('admin.providers.delivery_windows.index', $data)->renderSections()['list'],
                ]);
            }
        }catch(\exception $exception){
            \ErrorLog::add($exception);
            $data = [
                'title' => 'Franjas horarias para provedores',
                'cities' => $this->get_cities()
            ];
            return View::make('admin.providers.delivery_windows.index', $data);
        }

        
    }

    /**
     * Guarda una nueva franja o edita una existente
     *
     * @return mixed
     */
    public function save()
    {
        if(Input::ajax()){
            try{
                $deliveryWindow = ProviderOrderDeliveryWindow::exist( Input::get('hour_start'), Input::get('hour_end'),Input::get('warehouse_id'), Input::get('city_id'), Input::get('id') );
               
                if(!$deliveryWindow['status']){
                    throw new \Exception($deliveryWindow['message'], 200);
                }

                $deliveryWindow = new ProviderOrderDeliveryWindow;
                if (Input::has('id') && !empty(Input::get('id')) ){
                    $deliveryWindow = ProviderOrderDeliveryWindow::find(Input::get('id'));
                    $orders = \ProviderOrder::where('provider_order_delivery_window_id', Input::get('id'))->whereIn('status', ['Iniciada','Enviada','Pendiente'])->count();
                    if ($orders && $deliveryWindow->warehouse_id != Input::get('warehouse_id')){
                        throw new \Exception('Hay ordenes de compra activas que tienen asignada esta franja horaria, por tanto no es posible cambiar la bodega de esta franja horaria.', 200);
                    }
                }


                $start_date = new \DateTime( date('Y-m-d').' '.Input::get('hour_start') );
                $end_date = new \DateTime( date('Y-m-d').' '.Input::get('hour_end') );
                $deliveryWindow->warehouse_id = Input::get('warehouse_id');
                $deliveryWindow->hour_start = Input::get('hour_start');
                $deliveryWindow->hour_end = Input::get('hour_end');
                $deliveryWindow->delivery_window = $start_date->format('g:i a').' - '.$end_date->format('g:i a');
                $deliveryWindow->status = 1;
                $deliveryWindow->save();
                return Response::json([
                    'status'=> true,
                    'message' => 'Se guardó la franja con éxito.'
                ]);

            }catch(\Exception $e){
                return Response::json([
                    'status'=> false,
                    'message' => 'Ocurrio un error al guardar la franja. '.$e->getMessage()
                ]);
            }
        }
    }

    /**
     * Actualiza el estado de una franja horaria
     *
     * @return mixed
     */
    public function status()
    {
        if(Request::ajax()){
            try{
                $orders = \ProviderOrder::where('provider_order_delivery_window_id', Input::get('id'))->whereIn('status', ['Iniciada','Enviada','Pendiente'])->count();
                if ($orders){
                    throw new \Exception('Hay ordenes de compra activas que tienen asignada esta franja horaria.', 200);
                }
                $delivery_window = ProviderOrderDeliveryWindow::find(Input::get('id'));
                $delivery_window->status = Input::get('status');
                $delivery_window->save();
                return Response::json([
                    'status'=> true,
                    'message' => 'Se actualizó el estado de la franja con éxito.'
                ]);
            }catch(\Exception $e){
                return Response::json([
                    'status'=> false,
                    'message' => 'Ocurrio un error al actualizar el estado de la franja. '.$e->getMessage()
                ]);
            }
        }
    }

    /**
     * Elimina una franja horaria
     *
     * @return mixed
     */
    public function delete()
    {
        if(Request::ajax()){
            try{
                $orders = \ProviderOrder::where('provider_order_delivery_window_id', Input::get('id'))->whereIn('status', ['Iniciada','Enviada','Pendiente'])->count();
                if ($orders){
                    throw new \Exception('Hay ordenes de compra que tienen asignada esta franja horaria.', 200);
                }
                ProviderOrderDeliveryWindow::where('id', Input::get('id'))->delete();
                return Response::json([
                    'status'=> true,
                    'message' => 'Se eliminó la franja con éxito.'
                ]);
            }catch(\Exception $e){
                return Response::json([
                    'status'=> false,
                    'message' => 'Ocurrió un error al eliminar la franja. '.$e->getMessage()
                ]);
            }
        }
    }
}