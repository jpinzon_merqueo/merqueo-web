<?php
namespace admin;

use Request, Input, View, Session, Redirect, Response, Hash, Validator, DB, Config,
Order, Store,Shopper, Zone, ShopperZone, City, Carbon\Carbon, ShopperSchedule, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007;

class AdminShopperTracingController extends AdminController
{
	/**
	 * Seguimiento de shoppers
	 */
	public function index()
	{
		if (!Request::ajax()){
			$shoppers = Shopper::where('status', 1);
			if (Session::get('admin_designation') != 'Super Admin'){
				$shoppers->where('city_id', Session::get('admin_city_id'));
			}
			$shoppers = $shoppers->orderBy('first_name')->orderBy('last_name')->get();
			$cities = City::all();

			return View::make('admin.shoppers.reports.tracing')
						->with('title', 'Shoppers Tracing')
						->with('cities', $cities)
						->with('shoppers', $shoppers);
		}else{

			$search = Input::has('s') ? Input::get('s') : '';

			$now = date('Y-m-d H:i:s');
			$shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
			->where(function($query) use ($search){
				$query->where(DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%'.$search.'%');
				$query->orWhere('identity_number', 'LIKE', '%'.$search.'%');
				$query->orWhere('shoppers.phone', 'LIKE', '%'.$search.'%');
			})->where('shoppers.status', 1)
			->leftJoin(DB::raw('(orders, order_groups)'), function($join){
				 $join->on('shoppers.id', '=', 'orders.shopper_id');
				 $join->on('orders.group_id', '=', 'order_groups.id');
				 $join->on('orders.management_date', '>', DB::raw("'".date('Y-m-d 00:00:00')."'"));
				 $join->on('orders.management_date', '<', DB::raw("'".date('Y-m-d 23:59:59', strtotime('+1 day'))."'"));
			})
			->leftJoin('shopper_schedule','shoppers.id', '=', 'shopper_schedule.shopper_id')
			->select('shoppers.*', 'city', DB::raw("SUM(IF(orders.status = 'Delivered', 1, 0)) AS orders_qty"),
			DB::raw("(SELECT id FROM orders WHERE shopper_id = shoppers.id AND status NOT IN ('Delivered', 'Cancelled') GROUP BY shopper_id) AS order_id"),
			DB::raw('
				(
					SELECT
						SUM(ROUND(TIME_TO_SEC(TIMEDIFF(IF(shopper_schedule.deactivated_at IS NULL, "'.$now.'", shopper_schedule.deactivated_at ), shopper_schedule.activated_at ) )/3600, 1 ) ) AS diff_hours
					FROM
						`shopper_schedule`
					WHERE
						`shopper_id` = shoppers.id
						AND DATE(`shopper_schedule`.`activated_at`) = DATE("'.$now.'")
					GROUP BY shopper_id
					ORDER BY `activated_at` DESC
				) AS business_hours
			'),
			DB::raw("ROUND(SUM(IF(orders.status = 'Delivered', 1, 0)) /
				SUM(
					ROUND(
						TIME_TO_SEC(
							TIMEDIFF(
								IF(
									shopper_schedule.deactivated_at IS NULL,
									'".$now."',
									shopper_schedule.deactivated_at
								),
								shopper_schedule.activated_at
							)
						)/3600,
						1
					)
				),
				1
			) AS productivity"))
			->groupBy('shoppers.id')->orderBy('order_id', 'asc');

			if (Input::get('shopper_id'))
				$shoppers->where('shoppers.id', Input::get('shopper_id'));
			if (Input::get('city_id'))
				$shoppers->where('shoppers.city_id', Input::get('city_id'));
			if (Input::has('is_active'))
				$shoppers->where('shoppers.is_active', Input::get('is_active'));
			if (Input::get('order_by'))
				$shoppers->orderBy(Input::get('order_by'), Input::get('sort_order_by'));

			if (Session::get('admin_designation') != 'Super Admin')
				$shoppers->where('order_groups.user_city_id', Session::get('admin_city_id'));

			$shoppers_tmp = $shoppers->limit(80)->get();

			if (count($shoppers_tmp)){
				$shoppers = array();
				foreach($shoppers_tmp as $key => $shopper){
					//zonas
					$zones = Zone::select('zones.name')->join('shopper_zones', 'zone_id', '=', 'zones.id')
					->join('cities', 'zones.city_id', '=', 'cities.id')->where('shopper_id', $shopper->id)->get();
					$shopper->zones = $zones;

					//pedido pendiente
					if ($shopper->order_id){
						$order = Order::where('orders.id', $shopper->order_id)
						->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
						->join('stores', 'orders.store_id', '=', 'stores.id')
						->select('orders.status', 'order_groups.source', 'order_groups.source_os', 'stores.name AS store_name',
						'order_groups.user_address', 'orders.allocated_date', 'orders.delivery_date');

						if (Input::get('status')){
							$status = Input::get('status');
							$statuses = explode(',', $status);
							unset($statuses[count($statuses) - 1]);
							$order->where(function($query) use ($statuses){
								foreach($statuses as $status)
									$query->orWhere('orders.status', $status);
							});
						}

						if (Input::get('source'))
							$order->where('source', Input::get('source'));
						if (Input::get('source_os'))
							$order->where('source_os', Input::get('source_os'));

						$order = $order->first();
						if ($order)
							$shopper->order_pending = $order;
						else $shopper->order_id = null;
					}

					if (!(Input::get('status') || Input::get('source') || Input::get('source_os')))
						$shoppers[] = $shopper;
					else if ($shopper->order_id)
						   $shoppers[] = $shopper;
				}
			}

			return View::make('admin.shoppers.reports.tracing')
					->with('title', 'Shoppers Tracing')
					->with('shoppers', $shoppers)
					->renderSections()['content'];
		}
	}

	/**
	 * Exporta seguimiento de shoppers
	 */
	public function shoppers_tracing_export()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$original_date = $start_date = Input::get('start_date');
		$original_end_date = $end_date = Input::get('end_date');

		$start_date = str_replace('/', '-', $start_date);
		$start_date = date("Y-m-d", strtotime($start_date) );
		$end_date   = str_replace('/', '-', $end_date);
		$end_date   = date("Y-m-d", strtotime($end_date) );

		$new_start_date = new Carbon($start_date);
		$new_end_date   = new Carbon($end_date);

		$interval = new \DateInterval('P1D');
		$new_end_date->add($interval);
		$daterange = new \DatePeriod($new_start_date, $interval ,$new_end_date);

		foreach($daterange as $date){
			$rows = Shopper::join('cities', 'shoppers.city_id', '=', 'cities.id')
							->join('shopper_schedule', 'shoppers.id', '=', 'shopper_schedule.shopper_id')
							->where('shoppers.status', 1)
							->whereRaw('DATE(shopper_schedule.activated_at) = "'.$date->format('Y-m-d').'"')
							->orderBy('shoppers.id', 'ASC')
							->groupBy('shoppers.id')
							->select(
								"shoppers.identity_number AS Número de identidad",
								"shoppers.first_name AS Nombre",
								"shoppers.last_name AS Apellido",
								"shoppers.email AS Email",
								"cities.city AS Ciudad",
								DB::raw("(
											SELECT
												ROUND((SUM( TIME_TO_SEC(TIMEDIFF(shopper_schedule.deactivated_at, shopper_schedule.activated_at)) )/3600.0), 1 )
											FROM shopper_schedule
											WHERE shopper_schedule.shopper_id = shoppers.id
											GROUP BY shopper_schedule.shopper_id
										) AS 'Horas trabajadas'"),
								DB::raw("
									(
										SELECT
											COUNT(*)
										FROM orders
										WHERE orders.shopper_id = shoppers.id
											AND orders.status LIKE 'Delivered'
											AND orders.management_date BETWEEN '".$date->format('Y-m-d')."' AND (
																		SELECT
																			MAX(shopper_schedule.deactivated_at)
																		FROM shopper_schedule
																		WHERE DATE(shopper_schedule.activated_at) = DATE('".$start_date."')
																		AND shopper_schedule.shopper_id = shoppers.id
																	)
										GROUP BY orders.shopper_id
									) AS 'Pedidos entregados'
								")
							)->get();
			$books[$date->format('Y-m-d')] = json_decode($rows, true);
		}

		if ($n = count($books))
		{
			//eliminar archivo
			$path = public_path(Config::get('app.download_directory'));
			if ($gestor = opendir($path)){
				while (false !== ($file = readdir($gestor))){
					if (strstr($file, Session::get('admin_username')))
						remove_file($path.$file);
				}
			}

			$objPHPExcel = new PHPExcel();
			//propiedades
			$objPHPExcel->getProperties()->setCreator('Merqueo');
			$objPHPExcel->getProperties()->setTitle('Reporte shoppers');
			$objPHPExcel->getProperties()->setSubject('Reporte shoppers');
			$objPHPExcel->removeSheetByIndex(0);
			$sheet_index = 0;
			foreach ($books as $key => $book) {
				if (count($book)) {
					$sheet_title = $key;
					$myWorkSheet = new \PHPExcel_Worksheet($objPHPExcel, $sheet_title);
					$objPHPExcel->addSheet($myWorkSheet, $sheet_index);
					$sheet_index++;

					//estilos
					$style_header = array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb'=>'5178A5'),
						),
						'font' => array(
							'color' => array('rgb'=>'FFFFFF'),
							'bold' => true,
						)
					);
					$myWorkSheet->getStyle('A2:H2')->applyFromArray($style_header);
					foreach ($book as $key => &$value) {
						$value['Productividad'] = round($value['Pedidos entregados']/$value['Horas trabajadas'], 1);
					}
					unset($value);
					unset($key);

					//titulos
					$myWorkSheet->SetCellValueByColumnAndRow(0, 1, 'Reporte de fecha: '.$sheet_title);
					$i = 0;
					foreach ($book[0] as $key => $value) {
						$myWorkSheet->SetCellValueByColumnAndRow($i, 2, $key);
						$i++;
					}


					$j = 3;
					foreach($book as $row){
						$i = 0;
						foreach ($row as $key => $value) {
							$myWorkSheet->SetCellValueByColumnAndRow($i, $j, $value);
							$i++;
						}
						$j++;
					}
					$myWorkSheet->setTitle($sheet_title);

				}
			}


			//crear archivo
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
			$objWriter->save(public_path(Config::get('app.download_directory')).$filename);
			$url = web_url().'/'.Config::get('app.download_directory').$filename;
			return Response::download(public_path(Config::get('app.download_directory')).$filename);

		}else return Redirect::back()->with('error', 'No Data Found.');
	}

	/**
	 * Exporta seguimiento de shoppers por sessiones de trabajo
	 */
	public function shoppers_tracing_export2()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$original_date = $start_date = Input::get('start_date');
		$original_end_date = $end_date = Input::get('end_date');

		$start_date = str_replace('/', '-', $start_date);
		$start_date = date("Y-m-d", strtotime($start_date) );
		$end_date   = str_replace('/', '-', $end_date);
		$end_date   = date("Y-m-d", strtotime($end_date) );

		$new_start_date = new Carbon($start_date);
		$new_end_date   = new Carbon($end_date);

		$interval = new \DateInterval('P1D');
		$new_end_date->add($interval);
		$daterange = new \DatePeriod($new_start_date, $interval ,$new_end_date);

		foreach($daterange as $date){
			$rows = Shopper::join('cities', 'shoppers.city_id', '=', 'cities.id')
							->join('shopper_schedule', 'shoppers.id', '=', 'shopper_schedule.shopper_id')
							->where('shoppers.status', 1)
							->whereRaw('DATE(shopper_schedule.activated_at) = "'.$date->format('Y-m-d').'"')
							->orderBy('shoppers.id', 'ASC')
							->select(
								// "shoppers.id AS id",
								"shoppers.identity_number AS Número de identidad",
								"shoppers.first_name AS Nombre",
								"shoppers.last_name AS Apellido",
								"shoppers.email AS Email",
								"cities.city AS Ciudad",
								DB::raw("shopper_schedule.activated_at AS 'Hora de entrada'"),
								DB::raw("shopper_schedule.deactivated_at AS 'Hora de salida'"),
								DB::raw("(
											ROUND((TIME_TO_SEC(TIMEDIFF(shopper_schedule.deactivated_at, shopper_schedule.activated_at))/3600.0), 1 )
										) AS 'Horas trabajadas'"),
								DB::raw("
									(
										SELECT
											COUNT(*)
										FROM orders
										WHERE orders.shopper_id = shoppers.id
										AND orders.status LIKE 'Delivered'
										AND orders.management_date BETWEEN shopper_schedule.activated_at AND shopper_schedule.deactivated_at
									) AS 'Pedidos entregados'
								")
							)->get();
			$books[$date->format('Y-m-d')] = json_decode($rows, true);
		}

		if ($n = count($books))
		{
			//eliminar archivo
			$path = public_path(Config::get('app.download_directory'));
			if ($gestor = opendir($path)){
				while (false !== ($file = readdir($gestor))){
					if (strstr($file, Session::get('admin_username')))
						remove_file($path.$file);
				}
			}

			$objPHPExcel = new PHPExcel();
			//propiedades
			$objPHPExcel->getProperties()->setCreator('Merqueo');
			$objPHPExcel->getProperties()->setTitle('Reporte shoppers');
			$objPHPExcel->getProperties()->setSubject('Reporte shoppers');
			$objPHPExcel->removeSheetByIndex(0);
			$sheet_index = 0;

			foreach ($books as $key => $book) {
				if (count($book)) {
					$sheet_title = $key;
					$myWorkSheet = new \PHPExcel_Worksheet($objPHPExcel, $sheet_title);
					$objPHPExcel->addSheet($myWorkSheet, $sheet_index);
					$sheet_index++;

					//estilos
					$style_header = array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb'=>'5178A5'),
						),
						'font' => array(
							'color' => array('rgb'=>'FFFFFF'),
							'bold' => true,
						)
					);
					$myWorkSheet->getStyle('A2:J2')->applyFromArray($style_header);
					foreach ($book as $key => &$value) {
						$value['Productividad'] = round($value['Pedidos entregados']/$value['Horas trabajadas'], 1);
					}
					unset($value);
					unset($key);

					//titulos
					$myWorkSheet->SetCellValueByColumnAndRow(0, 1, 'Reporte de fecha: '.$sheet_title);
					$i = 0;
					foreach ($book[0] as $key => $value) {
						$myWorkSheet->SetCellValueByColumnAndRow($i, 2, $key);
						$i++;
					}


					$j = 3;
					foreach($book as $row){
						$i = 0;
						foreach ($row as $key => $value) {
							$myWorkSheet->SetCellValueByColumnAndRow($i, $j, $value);
							$i++;
						}
						$j++;
					}

					 //crear archivo
					$myWorkSheet->setTitle($sheet_title);
				}
			}

			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
			$objWriter->save(public_path(Config::get('app.download_directory')).$filename);
			$url = web_url().'/'.Config::get('app.download_directory').$filename;
			return Response::download(public_path(Config::get('app.download_directory')).$filename);

		}else return Redirect::back()->with('error', 'No Data Found.');
	}

	/**
     * Funcion para exportar informe base shoppers
     */
	public function shoppers_tracing_export3()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);

		if ( Input::has('start_date') && Input::has('end_date') && Input::has('report_city_id') ) {
			$start_date = Carbon::createFromFormat('d/m/Y', Input::get('start_date'));
			$end_date = Carbon::createFromFormat('d/m/Y', Input::get('end_date'));
			$city = Input::get('report_city_id');
			if ( Carbon::now() == $start_date ) {
				return Redirect::back()->with('error', 'La fecha debe ser menor al día de hoy.');
			}
		}else{
			return Redirect::back()->with('error', 'Debe enviar una fecha y una ciudad para filtrar.');
		}

		$stores = Store::where('status', '=', 1)->where('city_id', $city)->get();

		DB::select("SET lc_time_names = 'es_CO'");

		$select = [
			DB::raw('DATE(shopper_schedule.activated_at) AS "Fecha de Activación"'),
			DB::raw('DATE(shopper_schedule.deactivated_at) AS "Fecha de Desactivación"'),
			DB::raw('DAYNAME(shopper_schedule.activated_at) AS "Día"'),
			'shoppers.identity_number AS Cédula',
			DB::raw('CONCAT(shoppers.first_name, " ", shoppers.last_name) AS "Nombre"'),
			'shoppers.phone AS Teléfono',
			'shoppers.profile AS Perfil',
			DB::raw('DATE_FORMAT(shopper_schedule.activated_at, "%H:%i") AS "Hora de activación"'),
			DB::raw('DATE_FORMAT(shopper_schedule.deactivated_at, "%H:%i") AS "Hora de desactivación"'),
			DB::raw('ROUND( TIME_TO_SEC( TIMEDIFF( shopper_schedule.deactivated_at, shopper_schedule.activated_at ) ) / 3600, 1 )AS "Total Horas"')
		];

		foreach ($stores as $key => $store) {
			$select[] = DB::raw('( SELECT count(*) FROM orders WHERE orders.shopper_id = shoppers.id AND orders.management_date BETWEEN shopper_schedule.activated_at AND shopper_schedule.deactivated_at AND orders.store_id = '.$store->id.' ) AS "'.$store->name.'"');
		}

		$select[] = DB::raw('( SELECT count(*) FROM orders WHERE orders.shopper_id = shoppers.id AND orders.management_date BETWEEN shopper_schedule.activated_at AND shopper_schedule.deactivated_at ) AS "Ordenes Totales"');

		$shopper_reports = Shopper::select($select)
									->join('cities', 'shoppers.city_id', '=', 'cities.id')
									->join('shopper_schedule', 'shoppers.id', '=', 'shopper_schedule.shopper_id')
									->whereRaw('DATE(shopper_schedule.activated_at) BETWEEN "'.$start_date->toDateString().'" AND "'.$end_date->toDateString().'"')
									->orderBy('shoppers.identity_number', 'ASC')
									->orderBy('shopper_schedule.id', 'ASC')->get()->toArray();

		$objPHPExcel = new PHPExcel();
		if ( $n = count($shopper_reports) ) {
			//propiedades
			$objPHPExcel->getProperties()->setCreator('Merqueo');
			$objPHPExcel->getProperties()->setTitle('Reporte shoppers');
			$objPHPExcel->getProperties()->setSubject('Reporte shoppers');
			$objPHPExcel->removeSheetByIndex(0);

			$myWorkSheet = new \PHPExcel_Worksheet($objPHPExcel, 'informe shoppers');
			$objPHPExcel->addSheet($myWorkSheet, 0);

			$style_header = array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb'=>'5178A5'),
				),
				'font' => array(
					'color' => array('rgb'=>'FFFFFF'),
					'bold' => true,
				)
			);
			$myWorkSheet->getStyle('A1:AY1')->applyFromArray($style_header);

			$i = 0;
			foreach ($shopper_reports[0] as $title => $value) {
				$myWorkSheet->SetCellValueByColumnAndRow($i, 1, $title);
				$i++;
			}
			$myWorkSheet->SetCellValueByColumnAndRow($i, 1, 'Productividad');

			// debug($shopper_reports);
			foreach ($shopper_reports as $key => &$report) {
				if ( $report['Total Horas'] != 0 ) {
					$report['productividad'] = round($report['Ordenes Totales']/$report['Total Horas'], 2);
				}else{
					$report['productividad'] = 0;
				}
				$j = 0;
				foreach ($report as $key2 => $repo) {
					$myWorkSheet->SetCellValueByColumnAndRow($j, $key+2, $repo);
					$j++;
				}
			}

			$myWorkSheet->setTitle('informe shoppers');
		}

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->setOffice2003Compatibility(true);
		$filename = "reporte_3_shoppers.xlsx";
		$objWriter->save(public_path(Config::get('app.download_directory')).$filename);
		$url = web_url().'/'.Config::get('app.download_directory').$filename;
		return Response::download(public_path(Config::get('app.download_directory')).$filename);
	}
}
?>
