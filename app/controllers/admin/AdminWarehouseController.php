<?php

namespace admin;

use Request, Session, View, Input, Redirect, DB, Str, Response, Config, Route, Warehouse, City,
    \GuzzleHttp\json_decode, \GuzzleHttp\json_encode;

class AdminWarehouseController extends AdminController
{
    /**
     * Grilla de Bodegas
     */
    public function index()
    {
        $warehouses = Warehouse::select('warehouses.*', 'cities.city')
            ->leftJoin('cities', 'warehouses.city_id', '=', 'cities.id');
        if (!Request::ajax()) {
            $warehouses = $warehouses->get();
            return View::make('admin.warehouses.index')
                ->with('title', 'Bodegas')
                ->with('warehouses', $warehouses);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            if ($search != '')
                $warehouses = $warehouses->where('warehouses.warehouse', 'LIKE', '%' . $search . '%');
            $warehouses = $warehouses->orderBy('cities.city')->get();

            return View::make('admin.warehouses.index')
                ->with('title', 'Bodegas')
                ->with('warehouses', $warehouses)
                ->renderSections()['content'];
        }
    }

    /**
     * Editar bodega
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Bodega' : 'Nueva Bodega';
        $warehouse = new Warehouse;

        if ($id) {
            $warehouse = Warehouse::find($id);
            $warehouse->storage_positions = json_decode($warehouse->storage_positions);
            foreach ($warehouse->storage_positions as $key => &$storage_position)
                foreach ($storage_position as $index => &$item)
                    foreach ($item as $iter => &$value)
                        if (is_array($value))
                            $value = implode(",", $value);
        } else {
            $warehouse->storage_positions = array(
                'picking' =>
                    array(
                        'Seco' =>
                            array(
                                'start_position' => 0,
                                'end_position' => 0,
                                'height_positions' =>
                                    '',
                            ),
                        'Frío' =>
                            array(
                                'start_position' => 0,
                                'end_position' => 0,
                                'height_positions' =>
                                    '',
                            ),
                    ),
                'storage' =>
                    array(
                        'Seco' =>
                            array(
                                'start_position' => 0,
                                'end_position' => 0,
                                'height_positions' =>
                                    '',
                            ),
                        'Frío' =>
                            array(
                                'start_position' => 0,
                                'end_position' => 0,
                                'height_positions' =>
                                    '',
                            ),
                    ),
            );
        }

        if (Session::get('admin_designation') != 'Super Admin')
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        else $cities = $this->get_cities();

        return View::make('admin.warehouses.warehouse_form')
            ->with('title', $sub_title)
            ->with('cities', $cities)
            ->with('warehouse', $warehouse);
    }

    /**
     * Guardar tienda
     */
    public function save()
    {
        $id = Input::get('id');
        $warehouse = Warehouse::find($id);
        if (!$warehouse) {
            $warehouse = new Warehouse;
            $action = 'insert';
        } else $action = 'update';

        $warehouse->warehouse = Input::get('warehouse');
        $warehouse->orders_limit = Input::get('orders_limit');
        $warehouse->address = Input::get('address');
        $warehouse->city_id = Input::get('city_id');
        $warehouse->status = Input::get('status');
        if (Input::has('latlng')) {
            $latlng = explode(',', trim(Input::get('latlng')));
            if (count($latlng) == 2 && validate_longitude(trim($latlng[0])) && validate_latitude(trim($latlng[1]))) {
                $warehouse->latitude = trim($latlng[0]);
                $warehouse->longitude = trim($latlng[1]);
            } else {
                if ($id)
                    return Redirect::route('adminWarehouses.edit', ['id' => $id])->with('type', 'error')->with('message', 'La coordenada de la bodega no es valida.');
                else
                    return Redirect::route('adminWarehouses.add')->with('type', 'error')->with('message', 'La coordenada de la bodega no es valida.');
            }
        }
        if (Input::has('storage_positions')) {
            $storage_positions = Input::get('storage_positions');
            foreach ($storage_positions as $key => &$storage_position) {
                foreach ($storage_position as $index => &$item)
                    foreach ($item as $iter => &$value)
                        if ($iter == 'height_positions')
                            $value = explode(',', trim($value));
            }
            $warehouse->storage_positions = json_encode($storage_positions);
        }
        $warehouse->save();
        $this->admin_log('warehouses', $warehouse->id, $action);
        $warehouse->city_id = Input::get('city_id');

        return Redirect::route('adminWarehouses.index')->with('type', 'success')->with('message', 'Bodega guardada con éxito.');
    }

    /**
     * Eliminar bodega
     */
    public function delete()
    {
        $id = Request::segment(4);
        try {
            DB::beginTransaction();

            Warehouse::where('id', $id)->update(array('status' => 0));
            $this->admin_log('warehouses', $id, 'delete');

            DB::commit();
            return Redirect::route('adminWarehouses.index')->with('type', 'success')->with('message', 'Bodega eliminada con éxito.');

        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::route('adminWarehouses.index')->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar la bodega.');
        }
    }

    /**
     * Esta función carga la vista principal de la configuracion adicional de una bodega
     * this function charge de main view of additional configuration of a warehouse
     *
     * @return mixed
     */
    public function config()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $warehouse = Warehouse::find($id);
        return View::make('admin.warehouses.config')->with('warehouse', $warehouse);
    }

    /**
     * Esta función guarga la configuración adicional de una bodega
     * This function save the additional configuration of a warehouse
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveConfig()
    {
        try {
            $warehouse = Warehouse::find(Input::get('id'));
            $warehouse->without_planning = Input::get('withoutPlanning');
            $warehouse->use_packing = Input::get('usePacking');
            $warehouse->shift_planning = Input::get('shift_planning');
            $warehouse->save();
            return Redirect::route('adminWarehouses.index')->with('type', 'success')->with('message', 'Datos actualizados con éxito.');
        } catch (\Exception $e) {
            return Redirect::route('adminWarehouses.index')->with('type', 'failed')->with('message', 'Ocurrió un error al actualzar los datos.');
        }
    }

    /**
     * Esta funcion genera un código de barras diario para cada bodega para que los pickers cierren sesión
     * This function generate a daily barcode for each warehouse for pickers logout
     */

    public function generatePdfLogout(){
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $barcodeString = md5($id. (date('Y') * (intval(date('m')) - 1) * intval(date('d'))));
        $pdf = new \PDF_Code128('L', 'mm');
        $pdf->AddPage('P', array(400, 400), 0);
        $pdf->SetMargins(15, 0, 0);
        $pdf->SetFont('Arial', 'B', 20);
        $pdf->Cell(150, 50, $pdf->Code128($pdf->GetX() + 5, $pdf->GetY(), $barcodeString, 150,40), 0 , 1, 'C');
        $pdf->Cell(150,0, utf8_decode('LÍDER DE BODEGA'), 0,1,'C');
        $pdf->Output();
        exit;
    }

}