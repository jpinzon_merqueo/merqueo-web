<?php
namespace admin;

use Request, View, Input, Route, Redirect, Session, DB, Config, Response, Validator,Store, City, Department, Shelf, Notification, File, StoreProduct,
NotificationLog, OneSignal, FirebaseClient;

class AdminNotificationController extends AdminController
{
    /**
     * Listado de notificaciones
     */
	public function index()
	{
        if (!Request::ajax()) {
            return View::make('admin.notifications.index')->with('title','Administrador de Notificaciones')->with('cities', $this->get_cities());
		}
		else {
		    $search = Input::has('s') ? Input::get('s') : '';
			$notifications = Notification::join('cities', 'cities.id', '=', 'city_id')
							 ->select('notifications.*', 'cities.city')
							 ->where(function($query) use ($search){
                                    $query->orWhere('push_subtitle', 'LIKE', '%'.$search.'%');
                                    $query->orWhere('push_message', 'LIKE', '%'.$search.'%');
                                    $query->orWhere('campaign', 'LIKE', '%'.$search.'%');
                                })
                             ->orderBy('id', 'desc');
            if (Input::get('city_id'))
                $notifications->where('cities.id', Input::get('city_id'));
			$notifications = $notifications->limit(80)->get();

			$data = [
				'title' => 'Administrador de Notificaciones',
				'notifications' => $notifications
            ];
			return View::make('admin.notifications.index', $data)->renderSections()['content'];
		}
	}

    /**
     * Editar notificación
     * @param null $id
     * @param $store_id
     * @return
     */
	public function edit($id = null, $store_id = null)
	{
        if (empty($id) && empty($store_id)) {
            $cities = $this->get_cities();
            $data = [
				'title' => 'Agregar Nueva Notificación',
				'cities' => $cities,
			];
			return View::make('admin.notifications.form', $data);
		}else{
			$notification = Notification::find((int)$id);
			$cities = $this->get_cities();
			if (empty($notification))
				return Redirect::route('adminNotifications.index')->with('error', 'No se encontró la notificación.');

			$stores = '';
			if($notification->deeplink){
				$deeplink = json_decode($notification->deeplink);
				$notification->deeplink_type 	= $deeplink->type;
				$notification->deeplink_city_id = $deeplink->city_id;
				$notification->store_id = $deeplink->store_id;

				if(isset($deeplink->department_id))
					$notification->department_id = $deeplink->department_id;

				if(isset($deeplink->shelf_id))
					$notification->shelf_id = $deeplink->shelf_id;

				if(isset($deeplink->store_product_id))
					$notification->store_product_id = $deeplink->store_product_id;

			}

			$data = [
				'title' => 'Editar Notificación',
				'notification' => $notification,
				'cities' => $cities
			];

			if (!empty($notification->deeplink_city_id)) {
				$data['stores'] = Store::where('city_id', $notification->deeplink_city_id)->where('is_hidden', 0)->where('status', 1)->get();
			}
			if (!empty($notification->store_id)) {
				$data['departments'] = Department::where('store_id', $notification->store_id)->get();
			}
			if (!empty($notification->department_id)) {
				$data['shelves'] = Shelf::where('department_id', $notification->department_id)->get();
			}
			if (!empty($notification->shelf_id)) {
				$data['store_products'] = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                        ->join('stores', 'stores.id', '=', 'store_products.store_id')
                        ->join('store_product_warehouses', 'store_product_id', '=', 'store_products.id')
                        ->where('stores.id', $notification->store_id)
                        ->where('store_product_warehouses.status', 1)
                        ->OrderBy('products.name', 'ASC')
                        ->select('products.name', 'store_products.id')
                        ->where('store_products.shelf_id', $notification->shelf_id)
                        ->get();
            }

			return View::make('admin.notifications.form', $data);
		}
	}

    /**
     * Guardar notificación
     */
	public function save()
	{
	   if (Input::has('id')) {
			$id = Input::get('id');
			$notification = Notification::find($id);
            $action = 'update';
		}else{
			$notification = new Notification;
            $action = 'insert';
		}

        $deeplink = '';
        if(Input::has('add_deeplink') && Input::get('add_deeplink')){
            $deeplink =  ['type' => Input::get('deeplink_type'),
              'city_id' => Input::get('deeplink_city_id'),
              'store_id' => Input::get('store_id'),
              'department_id' => Input::get('department_id'),
              'shelf_id' => Input::get('shelf_id'),
              'store_product_id' => Input::get('store_product_id')
            ];
            $deeplink = json_encode($deeplink);
        }

		$notification->city_id = Input::get('city_id');
		$notification->type = Input::get('type_push');
		$notification->campaign = Input::get('campaign');
        $notification->push_subtitle = trim(Input::get('push_subtitle'));
        $notification->push_message = trim(Input::get('push_message'));
		$notification->title = trim(Input::get('title'));
		$notification->text_button =  Input::get('text_button');
		$notification->url = Input::get('url');
		$notification->description = trim(Input::get('description'));
		$notification->terms_conditions = trim(Input::get('terms_conditions'));
		$notification->deeplink = $deeplink;
		$notification->last_order_date = empty(Input::get('last_order_date')) ? null : format_date('mysql', Input::get('last_order_date'));
        $notification->show_until_date = empty(Input::get('show_until_date')) ? null : format_date('mysql', Input::get('show_until_date'));

		$cloudfront_url = Config::get('app.aws.cloudfront_url');

        if(Input::hasFile('image_url'))
		{
		    $image = Input::file('image_url');
		    $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png,gif')
            );
            if ($validator->fails())
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

			if (!empty($notification->image_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $notification->image_url);
                remove_file($path);
                remove_file_s3($notification->image_url);
            }
            $notification->image_url = upload_image($image, false, 'notifications');
		}

        if ($notification->type == 'push')
            $notification->image_url = null;

		$notification->save();
        $this->admin_log('notifications', $notification->id, $action);

		return Redirect::route('adminNotifications.edit', ['id' => $notification->id])->with('success', 'Notificación guardada con éxito.');
	}

    /**
     * Eliminar notificación
     */
	public function delete($id)
	{
		$notification = Notification::find((int)$id);
		if (empty($notification))
			return Redirect::route('adminNotifications.index')->with('error', 'No se encontró la notificación.');

        $cloudfront_url = Config::get('app.aws.cloudfront_url');
        if (!empty($notification->image_url)) {
            $path = str_replace($cloudfront_url, uploads_path(), $notification->image_url);
            remove_file($path);
            remove_file_s3($notification->image_url);
        }

		$notification->delete();
        $this->admin_log('notifications', $notification->id, 'delete');

		return Redirect::route('adminNotifications.index')->with('success', 'Notificación eliminada con éxito.');
	}

	/**
     * formulario para enviar notificación
     * @param $id
     * @return
     */
	public function send_form_notification($id)
	{

		$notification = Notification::select('notifications.*', 'city')->join('cities', 'cities.id', '=', 'city_id')->where('notifications.id', $id)->first();
		if (empty($notification))
			return Redirect::route('adminNotifications.index')->with('error', 'No se ha encontró la notificación.');

		$data = [
			'title' => 'Enviar Notificación',
			'notification' => $notification
		];

		return View::make('admin.notifications.send_form', $data);
	}

	/**
     * Método para enviar notificación
     * @param $id notificación
     * @param $type notificación
     * @return message
     */
	public function send_notification()
	{
		$notification = Notification::find(Input::get('id'));
		if (empty($notification))
			return Redirect::route('adminNotifications.index')->with('error', 'No se ha encontró la notificación.');

		$data = ['type_push' => $notification->type];
		if($notification->type == 'inapp'){
			$data['city_id'] = $notification->city_id;
			$data['inapp_id'] = $notification->id;
        }

        if ($notification->deeplink && $notification->type == 'push')
            $data['deeplink'] = $notification->deeplink;

        $onesignal = new OneSignal();
        $send_data = [
            'subtitle' => $notification->push_subtitle,
            'message'=> $notification->push_message,
            'data' => $data
        ];

        //fecha de envio de notificacion
        $show_after_date = '';
        if (Input::get('send') == 'after'){
            if (!Input::has('show_after_date'))
                return Redirect::route('adminNotifications.sendFormNotification', ['id' => $notification->id])->with('error', 'La fecha de envio de la notificación es requerida.');
            $send_after = explode(' ', trim(Input::get('show_after_date')));
            $show_after_date = format_date('mysql', $send_after[0]);
            $show_after_date = date('Y-m-d H:i:s', strtotime($show_after_date.' '.date('H:i:s', strtotime($send_after[1].' '.$send_after[2]))));
            if ($show_after_date < date('Y-m-d H:i:s'))
                return Redirect::route('adminNotifications.sendFormNotification', ['id' => $notification->id])->with('error', 'La fecha de envio de la notificación esta en el pasado.');
            $send_data['send_after'] = $show_after_date.' GMT-0500';
        }

        if ($notification->type == 'inapp')
        {
            $segment = Input::get('segment');
            $is_test = strstr(strtolower($segment), 'prueba') ? 1 : 0;
            $description = str_replace('<p>', '', str_replace('</p>', '', $notification->description));
            $description = str_replace('&quot;', '"', $description);

            $firebase = new FirebaseClient('push');
            //Almacena el envio de la notificación enviada
            $firebase->setData(Config::get('app.firebase.push.db_index').'/'.$notification->city_id.'/'.$notification->id, [
                'is_test' => $is_test,
                'title' => $notification->title,
                'image_url' => $notification->image_url,
                'description' => $description,
                'terms_conditions' => $notification->terms_conditions,
                'text_button' => $notification->text_button,
                'show_after_date' => !empty($show_after_date) ? date('Y-m-d H:i:s', strtotime($show_after_date)) : '',
                'show_until_date' => !empty($notification->show_until_date) ? $notification->show_until_date : '',
                'last_order_date' => !empty($notification->last_order_date) ? date('Ymd', strtotime($notification->last_order_date)) : '',
                'deeplink' => $notification->deeplink,
                'created_at' => time()
            ]);
        }

        if (Input::has('send_push') && !Input::get('send_push'))
            return Redirect::route('adminNotifications.sendFormNotification', ['id' => $notification->id])->with('El InApp fue enviado con éxito.');

		if (Input::hasFile('device'))
		{
			//envia notificaciones por archivo importado csv de dispositivos
			$file = $_FILES['device'];

			if (!file_exists($file['tmp_name']))
                return Redirect::route('adminNotifications.sendFormNotification', ['id' => $notification->id])->with('error', 'el archivo no existe.');

            $total_data = 1;
            $cont = $recipients = 0;
            $player_ids = [];
            $text = fopen($file['tmp_name'], 'r');
            $rows = count(file($file['tmp_name']));
            while (!feof($text))
            {
                $row = trim(fgets($text, 4096));
                if ($row != '')
                {
                	if($cont <= 2000){
                		$player_ids[] = $row;
                		$cont++;
                	}

                	if ($cont == 2000 || $rows == $total_data)
                	{
                	    $send_data['include_player_ids'] = $player_ids;
                        $result = $onesignal->sendMessage($send_data, 'include_player_ids');
						$cont = 0;
						$player_ids = [];
						$recipients = $result['status'] ? $recipients + $result['recipients'] : 0;
					}
	               	$total_data++;
                }
            }
            fclose ($text);
	    }else{
	    	//envia notificaciones por segmento
	    	$send_data['segment'] = Input::get('segment');
            //debug($send_data);
            $result = $onesignal->sendMessage($send_data);
			$recipients = $result['status'] ? $result['recipients'] : 0;
		}

		//almacena log de las notificaciones enviadas
		$log = new NotificationLog;
        $log->notification_id = $notification->id;
        $log->push_subtitle = $notification->push_subtitle;
        $log->push_message = $notification->push_message;
        $log->segment = Input::get('segment');
        $log->recipients = isset($recipients) ? $recipients : 0;
        $log->admin_id = Session::get('admin_id');
        $log->save();

        if($recipients > 0){
            $message = !empty($show_after_date) ? 'La notificación fue programada y será enviada en la fecha y hora seleccionada.' : 'Se envió la notificación a '.$recipients.' dispositivos.';
			$result = ['success' => $message];
		}else{
		    $message = 'No se envió la notificación a ningún dispositivo.';
            $message .= !Input::hasFile('device') ? ' Verifica que el segmento exista y los filtros del mismo.' : '';
		    $message .= isset($result['message']) ? ' Error One Signal: '.$result['message'] : '';
            $result = ['error' => $message];
        }

		return Redirect::route('adminNotifications.sendFormNotification', ['id' => $notification->id])->with($result);
	}

	/**
	 * Obtener tiendas por ciudad ajax
	 */
	public function get_stores_ajax()
	{
	    $stores = array();
	    $message = 'Error al cargar';
	    $status = 400;
	    if (Input::has('city_id')) {
	        $stores = Store::select('id', 'name')
                ->where('city_id', Input::get('city_id'))
                ->where('status', 1)
                ->where('is_hidden', 0)
                ->get()->toArray();

	        $message = 'Tiendas';
	        $status = 200;
        }
        return Response::json(['result' => ['stores' => $stores], 'status' => $status, 'message' => $message], 200);
    }

    /**
     * Vista previa de la notificación
     * @param $id
     * @return
     */
    public function preview_ajax()
    {
    	$message = 'Error al cargar';
	    $status = 400;
    	$notification = Notification::find(Input::get('id'));
    	if ($notification){
    		$status = 'OK';
			$message = 'notification encontrada';
    	}
		return Response::json(['result' => ['notification' => $notification], 'status' => $status, 'message' => $message], 200);
    }
}
