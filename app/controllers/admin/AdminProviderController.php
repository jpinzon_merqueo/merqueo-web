<?php

namespace admin;

use Request, View, Input, Redirect, Session, DB, Str, Validator, Provider, City, Response, Config, ProviderContact, ProviderTax;

class AdminProviderController extends AdminController
{
	/**
     * Grilla de proveedores
     */
    public function index()
    {
        $city = City::find(Session::get('admin_city_id'));
        $cities = $this->get_cities();

        if (!Request::ajax())
        {
            $providers = Provider::select('providers.*', 'cities.city')->join('cities', 'cities.id', '=', 'providers.city_id');

            if (Session::get('admin_designation') != 'Super Admin')
                $providers->where('providers.city_id', Session::get('admin_city_id'));

            $providers = $providers->get();
            $data = [
                'title' => 'Proveedores',
                'providers' => $providers
            ];
            return View::make('admin.providers.index', $data)->with('cities', $cities);
        } else {
            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $providers = Provider::select('providers.*', 'cities.city')
                                ->where('providers.name', 'like', '%'.$search.'%')
                                ->where('providers.city_id', $city_id == null ? $city->id : $city_id)
                                ->join('cities', 'cities.id', '=', 'providers.city_id');

            if (Session::get('admin_designation') != 'Super Admin')
                $providers->where('providers.city_id', Session::get('admin_city_id'));

            $providers = $providers->limit(80)->get();

            $data = [
                'title' => 'Proveedores',
                'providers' => $providers
            ];
            return View::make('admin.providers.index', $data)->with('cities', $cities)->renderSections()['content'];
        }

    }

    /**
     * Editar proveedor
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Proveedor' : 'Nuevo Proveedor';
        $financial_condition = [
            'Contado' => 'Contado',
            'Consignación' => 'Consignación',
            'Crédito' => 'Crédito'
        ];
        $payment_days = array_combine($payment_days = range(1, 90), $payment_days);
        list($keys, $values) = array_divide($payment_days);
        array_unshift($keys, '');
        array_unshift($values, 'Seleccione');
        $payment_days = array_combine($keys, $values);

        if (!$id && Session::has('post')){
            $provider = (object) Session::get('post');
            $provider->logo_url = $provider->logo_small_url = $provider->id = 0;
        }else $provider = Provider::find($id);

        if (Session::get('admin_designation') != 'Super Admin')
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        else $cities = $this->get_cities();

        $provider_taxes = ProviderTax::all();
        $ica_taxes = $provider_taxes->filter(function($tax)
        {
            return $tax->type == 'ica';
        });

        $fuente_taxes = $provider_taxes->filter(function($tax)
        {
            return $tax->type == 'fuente';
        });


        return View::make('admin.providers.provider_form')
            ->with('title', 'Proveedor')
            ->with('financial_condition', $financial_condition)
            ->with('payment_days', $payment_days)
            ->with('sub_title', $sub_title)
            ->with('cities', $cities)
            ->with('provider', $provider)
            ->with('ica_taxes', $ica_taxes)
            ->with('fuente_taxes', $fuente_taxes);

    }

    /**
     * Guardar proveedor
     */
    public function save()
    {
        $id = Input::get('id');
        $provider = Provider::find($id);
        $flag = 0;
        $action = 'update';

        if (!$provider){
            $provider = new Provider;
            $flag = 1;
            $action = 'insert';
        }

        $financial_condition = Input::get('financial_condition');

        $provider->type = Input::get('type');
        $provider->name = Input::get('name');
        $provider->nit = Input::get('nit');
        $provider->code = Input::get('code');
        $provider->city_id = Input::get('city_id');
        $provider->address = Input::get('address');
        $provider->schedule = Input::get('schedule');
        $provider->transporter = Input::get('transporter');
        $provider->maximum_order_report_time = Input::get('maximum_order_report_time');
        $provider->delivery_time_hours = Input::get('delivery_time_hours');
        $provider->provider_type = Input::get('provider_type');
        $provider->observations = Input::get('observations');
        $provider->status = Input::get('status');
        $provider->order_days = Input::get('order_days');
        $provider->delivery_days = Input::get('delivery_days');
        $provider->lead_time = Input::get('lead_time');
        $provider->financial_condition = $financial_condition;
        $provider->payment_days = ($financial_condition == 'Crédito') ? Input::get('payment_days') : NULL;
        $provider->credit_limit = ($financial_condition == 'Crédito' && Input::get('due_quota') == 1) ? Input::get('credit_limit') : NULL;
        $provider->due_quota = ($financial_condition == 'Crédito') ? Input::get('due_quota') : NULL;

        if(Input::has('transporter') && Input::get('transporter') == 'Merqueo' )
        {
            $provider->collection_time = Input::get('collection_time');
            $provider->delivery_time = Input::get('delivery_time');

        } else if(Input::has('transporter') && Input::get('transporter') == 'Proveedor' ){
            $provider->provider_delivery_time = Input::get('provider_delivery_time');
        }

        $validate_image = function($input_name) {
            $errors = array();
            $image = Input::file($input_name);
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png')
            );
            if (Input::hasFile($input_name) && $validator->fails()){
               $error_messages = $validator->messages();
               foreach ($error_messages->all() as $message) {
                    array_push($errors, $message);
               }
            }
        };

        $associate_image = function($input_name, $field, $flag, $default, $directory = 'providers/logos') use (&$provider) {
            $cloudfront_url = Config::get('app.aws.cloudfront_url');
            if (Input::hasFile($input_name)) {
                if (!empty($provider->{$field}) && !strstr($provider->{$field}, 'default')){
                    $path = str_replace($cloudfront_url, uploads_path(), $provider->{$field});
                    remove_file($path);
                    remove_file_s3($provider->{$field});
                }
                $provider->{$field} = upload_image(Input::file($input_name), FALSE, $directory);
            } else if ($flag == 1) {
                $provider->{$field} = $cloudfront_url . $default;
            }
        };

        $logo_errors = $validate_image('logo');
        if (count($logo_errors)) {
            return Redirect::route('adminProviders.index')
                ->with('type', 'failed')
                ->with('message', $logo_errors);
        }

        $logo_small_errors = $validate_image('logo_small');
        if (count($logo_small_errors)) {
            return Redirect::route('adminProviders.index')
                ->with('type', 'failed')
                ->with('message', $logo_small_errors);
        }

        $rut_errors = $validate_image('rut');
        if (count($rut_errors)) {
            return Redirect::route('adminProviders.index')
                ->with('type', 'failed')
                ->with('message', $rut_errors);
        }

        $bank_certification_errors = $validate_image('bank_certification_url');
        if (count($bank_certification_errors)) {
            return Redirect::route('adminProviders.index')
                ->with('type', 'failed')
                ->with('message', $bank_certification_errors);
        }

        $associate_image('logo', 'logo_url', $flag, '/providers/logos/default_provider_logo.jpg');
        $associate_image('logo_small', 'logo_small_url', $flag, '/providers/logos/default_small_provider_logo.jpg');
        $associate_image('rut', 'rut_url', $flag, '/providers/logos/default_provider_logo.jpg');
        $associate_image('bank_certification_url', 'bank_certification_url', $flag, '/providers/logos/default_provider_logo.jpg');

        $provider->save();
        $this->admin_log('provider', $provider->id, $action);

        return Redirect::route('adminProviders.index')->with('type', 'success')->with('message', 'Proveedor guardado con éxito.');
    }

    /**
     * Eliminar proveedor
     */
    public function delete()
    {
        $id = Request::segment(4);
        try {
            DB::beginTransaction();

            Provider::where('id', $id)->update(array('status' => 0));
            $this->admin_log('Provider', $id, 'delete');

            DB::commit();
            return Redirect::route('adminProviders.index')->with('type', 'success')->with('message', 'Proveedor eliminada con éxito.');

        }catch(\Exception $e){
            DB::rollback();
            return Redirect::route('adminProviders.index')->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar al Proveedor.');
        }
    }

    /**
     * Obtener contactos de un proveedor
     */
    public function get_contact_ajax($id)
    {
        $provider_contacts = ProviderContact::where('provider_id', $id)->select('provider_contacts.*', 'providers.id AS providerId')->join('providers', 'providers.id', '=', 'provider_contacts.provider_id')->get();

        $data = [
            'section' => 'contacts_table_container',
            'provider_contacts' => $provider_contacts
        ];
        $view = View::make('admin.providers.provider_form', $data)->renderSections();

        return Response::json(['success' => 'Se guardó exitosamente.', 'html' => $view['contacts_table_container']]);

    }

    /**
     * Guardar contacto
     */
    public function save_contact_ajax($id)
    {
        $contact_id = Input::get('contact_id', null);
        $fullname = Input::get('name', null);
        $phone = Input::get('phone', null);
        $email = Input::get('email', null);
        $position = Input::get('position', null);
        $status = Input::get('status', 1);

        if (empty($contact_id)) {
            $contact = new ProviderContact();
        }else{
            $contact = ProviderContact::find($contact_id);
            if (!$contact) {
                return Redirect::back()->with('error', 'No se encontró el contacto.');
            }
        }

        $contact->fullname = $fullname;
        $contact->provider_id = $id;
        $contact->phone = $phone;
        $contact->email = $email;
        $contact->position = $position;
        $contact->status = $status;
        $contact->save();

        $contacts = ProviderContact::where('provider_id', $id)->select('provider_contacts.*')->get();

        $data = [
            'section' => 'contacts_table_container',
            'provider_contacts' => $contacts
        ];
        $view = View::make('admin.providers.provider_form', $data)->renderSections();

        return Response::json(['success' => 'Se guardó exitosamente.', 'html' => $view['contacts_table_container']]);

    }

    /**
     * Eliminar contacto
     */
    public function delete_contact_ajax($id)
    {
        $contact = ProviderContact::findOrFail($id);
        $contact->delete();

        $contacts = ProviderContact::where('provider_id', Input::get('provider_id'))->select('provider_contacts.*')->get();
        $data = [
            'section' => 'contacts_table_container',
            'provider_contacts' => $contacts
        ];
        $view = View::make('admin.providers.provider_form', $data)->renderSections();
        return Response::json(['success' => 'Se ha borrado exitosamente', 'html' => $view['contacts_table_container']]);
    }
}
