<?php
namespace admin;

use Request, Input, View, Session, Redirect, Response, Hash, Validator, DB, Config, Order, Carbon\Carbon;

class AdminShopperReportController extends AdminController
{
	/**
	 * Reporte en graficas de pedidos y shoppers
	 */
	public function index()
	{
		Session::forget('start_date');
		Session::forget('end_date');
		$now = Carbon::now();
		// Ordenes no asignadas - ASIGNACION
		$data['graphic_1'] = $this->shoppers_get_orders_assigment();
		// Ordenes asignadas - ALISTAMINETO
		$data['graphic_2'] = $this->shoppers_get_orders_enlistment();
		// Ordenes asignadas con estado dispatch - EN RUTA
		$data['graphic_3'] = $this->shoppers_get_orders_route();
		// Cumplimiento
		$data['graphic_4'] = $this->shoppers_get_orders_fulfillment();
		// Cancelados
		$data['graphic_5'] = $this->shoppers_get_orders_cancelled();

		$data = [
			'title' => 'Shoppers Reports',
			'data'  => $data
		];

		return View::make('admin.shoppers.reports.shoppers_reports', $data);
	}

	/**
	 * Funcion para consultar las ordenes no asignadas en la base de datos
	 */
	public function shoppers_get_orders_assigment()
	{
		$now  = Carbon::now();
		$data = [];
		$data['orders_not_assigned'] = $this->shoppers_query_builder('assignment', true);

		$orders = [];
		if ( count($data['orders_not_assigned']) ) {
			// Ordenes que se encuentran dentro del rango de 75 minutos antes de la fecha de entrega y la fecha de entrega.
			$data['red_75'] = [];
			foreach ($data['orders_not_assigned'] as $key => $order) {
				$delivery_date = Carbon::parse($order->ordergroups_delivery_date);
				$delivery_date_75 = Carbon::parse($order->ordergroups_delivery_date);
				$delivery_date_75 = $delivery_date_75->subMinutes(75);
				if ( $now->between($delivery_date, $delivery_date_75) || $now >= $delivery_date ) {
					$data['red_75'][] = $order;
				}else{
					$orders[] = $order;
				}
			}
			unset($delivery_date);
			unset($order);

			//Ordenes que se encuentran dentro del rango de 90 minutos y son de entrega inmediata.
			$data['inmediatamente_15'] = [];
			foreach ($data['orders_not_assigned'] as $key => $order) {
				if ( $order->delivery_time == 'Immediately' ) {
					$delivery_date = Carbon::parse($order->ordergroups_delivery_date);
					$delivery_date_15 = Carbon::parse($order->ordergroups_delivery_date);
					$delivery_date_15 = $delivery_date_15->subMinutes(15);
					if ( $now->between($delivery_date, $delivery_date_15) ) {
						$data['inmediatamente_15'][] = $order;
					}else{
						$orders[] = $order;
					}
				}
			}
			unset($order);
			unset($delivery_date);

			$data['immediately_15_percentage']      = (count($data['inmediatamente_15']) * 100) / count($data['orders_not_assigned']);
			$data['red_75_percentage']              = (count($data['red_75']) * 100) / count($data['orders_not_assigned']);
			$data['orders_not_assigned_percentage'] = (count($orders) * 100) / count($data['orders_not_assigned']);
			$data['orders_not_assigned']            = $orders;
			$data['red_75_plus_im15']               = count($data['inmediatamente_15']) + count($data['red_75']);
			$data['red_75_plus_im15_percentage']    = $data['immediately_15_percentage'] + $data['red_75_percentage'];
		}else{
			$data['red_75']                         = [];
			$data['inmediatamente_15']              = [];
			$data['immediately_15_percentage']      = 0;
			$data['red_75_percentage']              = 0;
			$data['orders_not_assigned_percentage'] = 0;
			$data['red_75_plus_im15']               = 0;
			$data['red_75_plus_im15_percentage']    = 0;
		}

		return $data;
	}

	/**
	 * Funcion para consultar las ordenes no asignadas en la base de datos
	 */
	public function shoppers_get_orders_enlistment()
	{
		$now  = Carbon::now();
		$data = [];
		$data['assigned_orders'] = $this->shoppers_query_builder('enlistment');

		if ( count($data['assigned_orders']) ) {
			$data['assigned_orders_more_than_20'] = [];
			$data['assigned_orders_less_than_20'] = [];
			foreach ($data['assigned_orders'] as $key => $order) {
				$delivery_date    = Carbon::parse($order->ordergroups_delivery_date);
				$delivery_date_20 = Carbon::parse($order->ordergroups_delivery_date);
				$delivery_date_20 = $delivery_date_20->subMinutes(20);

				if ( $now->lte($delivery_date_20) ) {
					$data['assigned_orders_more_than_20'][] = $order;
				}else{
					$data['assigned_orders_less_than_20'][] = $order;
				}
			}

			$data['assigned_orders_more_than_20_percentage'] = (count($data['assigned_orders_more_than_20']) * 100) / count($data['assigned_orders']);
			$data['assigned_orders_less_than_20_percentage'] = (count($data['assigned_orders_less_than_20']) * 100) / count($data['assigned_orders']);
			$data['assigned_orders_percentage']              = 100 - $data['assigned_orders_less_than_20_percentage'] - $data['assigned_orders_more_than_20_percentage'];
		}else{
			$data['assigned_orders_more_than_20']            = [];
			$data['assigned_orders_less_than_20']            = [];
			$data['assigned_orders']                         = [];
			$data['assigned_orders_more_than_20_percentage'] = 0;
			$data['assigned_orders_less_than_20_percentage'] = 0;
			$data['assigned_orders_percentage']              = 0;
		}

		return $data;
	}

	/**
	 * Funcion para consultar las ordenes asignadas en la base de datos
	 */
	public function shoppers_get_orders_route()
	{
		$now  = Carbon::now();
		$data = [];
		$data['assigned_orders_dispatched'] = $this->shoppers_query_builder('route');

		if ( count($data['assigned_orders_dispatched']) ) {
			$data['orders_before_delivery_date'] = [];
			$data['orders_after_delivery_date']  = [];

			foreach ($data['assigned_orders_dispatched'] as $key => $order) {
				$delivery_date = Carbon::parse($order->ordergroups_delivery_date);
				if ( $now->lte($delivery_date) ) {
					$data['orders_before_delivery_date'][] = $order;
				}else{
					$data['orders_after_delivery_date'][] = $order;
				}
			}

			$data['orders_before_delivery_date_percentage'] = (count($data['orders_before_delivery_date']) * 100) / count($data['assigned_orders_dispatched']);
			$data['orders_after_delivery_date_percentage']  = (count($data['orders_after_delivery_date']) * 100) / count($data['assigned_orders_dispatched']);
			$data['assigned_orders_dispatched_percentage']  = 100 - $data['orders_after_delivery_date_percentage'] - $data['orders_before_delivery_date_percentage'];
		}else{
			$data['assigned_orders_dispatched']             = [];
			$data['orders_before_delivery_date']            = [];
			$data['orders_after_delivery_date']             = [];
			$data['orders_before_delivery_date_percentage'] = 0;
			$data['orders_after_delivery_date_percentage']  = 0;
			$data['assigned_orders_dispatched_percentage']  = 0;
		}

		return $data;
	}

	/**
	 * Obtiene información de cumplimiento
	 */
	public function shoppers_get_orders_fulfillment($start_date = NULL, $end_date = NULL)
	{
		$now  = Carbon::now();
		$data = [];
		if ( Session::has('start_date') && Session::has('end_date') ) {
			$start_date = Session::get('start_date', NULL);
			$end_date   = Session::get('end_date', NULL);
		}
		$var  = [
			'start_date' => $start_date,
			'end_date'   => $end_date
		];
		$data['orders_delivered_in_time']     = $this->shoppers_query_builder('fulfillment_gt', $var);
		$data['orders_delivered_not_in_time'] = $this->shoppers_query_builder('fulfillment_lt', $var);

		if ( count($data['orders_delivered_in_time']) && count($data['orders_delivered_not_in_time']) ) {
			$data['sum_orders'] = count($data['orders_delivered_in_time']) + count($data['orders_delivered_not_in_time']);
			$data['orders_delivered_in_time_percentage']     = (count($data['orders_delivered_in_time']) * 100) / $data['sum_orders'];
			$data['orders_delivered_not_in_time_percentage'] = (count($data['orders_delivered_not_in_time']) * 100) / $data['sum_orders'];
		}else{
			$data['orders_delivered_in_time']                = [];
			$data['orders_delivered_not_in_time']            = [];
			$data['sum_orders']                              = 0;
			$data['orders_delivered_in_time_percentage']     = 0;
			$data['orders_delivered_not_in_time_percentage'] = 0;
		}

		return $data;
	}

	/**
	 * Funcion para consultar las ordenes canceladas en la base de datos
	 */
	public function shoppers_get_orders_cancelled($start_date = NULL, $end_date = NULL)
	{
		$now = Carbon::now();
		$data = [];

		$query = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
						->where('orders.status', 'Cancelled');

		if ( !is_null($start_date) && !is_null($end_date) ) {
			$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$start_date.'\') AND DATE(\''.$end_date.'\') )');
		}else{
			$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
		}

		$query->groupBy('orders.reject_reason')
				->select(DB::raw('count(*) AS cancelled_count, reject_reason AS reason'));

		$data['cancelled_orders_per_reason'] = $query->get()
													->toArray();

		$data['cancelled_orders_total'] = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
									->where('orders.status', 'Cancelled');
		if ( !is_null($start_date) && !is_null($end_date) ) {
			$data['cancelled_orders_total']->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$start_date.'\') AND DATE(\''.$end_date.'\') )');
		}else{
			$data['cancelled_orders_total']->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
		}
		$data['cancelled_orders_total'] = $data['cancelled_orders_total']->count('*');

		for ($i=0; $i < count($data['cancelled_orders_per_reason']); $i++) {
			$data['colors'][] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
		}

		$data['orders_day'] = $this->shoppers_query_builder('cancelled', ['start_date' => $start_date, 'end_date' => $end_date]);

		if ( count($data['cancelled_orders_per_reason']) ) {
			foreach ($data['cancelled_orders_per_reason'] as $key => &$order) {
				$order['percentage'] = number_format(($order['cancelled_count'] * 100) / $data['cancelled_orders_total'], 2);
				$order['value']      = number_format(($order['cancelled_count'] * 100) / $data['cancelled_orders_total'], 2);
				$order['label']      = $order['reason'];

			}
		}
		return $data;
	}

	/**
	 * Funcion para obtener ordenes canceladas por razon de cancelamiento.
	 */
	public function shoppers_get_orders_cancelados_reason($reason, $start_date = NULL, $end_date = NULL)
	{
		$now = Carbon::now();
		$data = [];
		$var = [
			'reason'     => $reason,
			'start_date' => $start_date,
			'end_date'   => $end_date
		];
		$data['cancelled_orders_per_reason'] = $this->shoppers_query_builder('cancelled_reasons', $var);

		$data['cancelled_orders_total'] = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
									->where('orders.status', 'Cancelled');

		if ( !is_null($start_date) && !is_null($end_date) ) {
			$data['cancelled_orders_total']->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$start_date.'\') AND DATE(\''.$end_date.'\') )');
		}else{
			$data['cancelled_orders_total']->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
		}

		$data['cancelled_orders_total'] = $data['cancelled_orders_total']->count('*');

		return $data;
	}

	/**
	 * Funcion para mostrar en detalle la información de las gráficas.
	 */
	public function shoppers_graphics_details($section, $id)
	{
		switch ( $section ) {
			case 'orders-not-assigned':
				return $this->shoppers_not_assigned($id);
			break;
			case 'preparing-orders':
				return $this->shoppers_preparing_orders($id);
			break;
			case 'route':
				return $this->shoppers_route($id);
			break;
			case 'fulfillment':
				return $this->shoppers_fulfillment($id);
			break;
			case 'cancelled-orders':
				return $this->shoppers_cancelled_orders($id);
			break;
			default:
				App::abort(404);
			break;
		}
	}

	/**
	 * Funcion para mostrar las ordenes por razon de cancelamiento.
	 */
	public function shoppers_cancelled_orders($id)
	{

		if ( !empty($id) ) {
			if ( Session::has('start_date') && Session::has('end_date') ) {
				$start_date = Session::get('start_date', NULL);
				$end_date   = Session::get('end_date', NULL);
				$orders = $this->shoppers_get_orders_cancelados_reason($id, $start_date, $end_date);
				$data = [
					'title'           => 'Orders',
					'payment_methods' => $this->config['payment_methods'],
					'orders'          => $orders['cancelled_orders_per_reason'],
					'start_date'      => $start_date,
					'end_date'        => $end_date
				];
			}else{
				$orders = $this->shoppers_get_orders_cancelados_reason($id);
				$data = [
					'title'           => 'Orders',
					'payment_methods' => $this->config['payment_methods'],
					'orders'          => $orders['cancelled_orders_per_reason']
				];
			}


			return View::make('admin.shoppers.reports.orders', $data);
		}

		App::abort(404);
	}

	/**
	 * Funcion para mostrar las ordenes que estan a tiempo de ser cumplidas y las que se han dejado de cumplir.
	 */
	public function shoppers_fulfillment($id)
	{
		$orders = $this->shoppers_get_orders_fulfillment();

		if ( $id == 'in-time' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders['orders_delivered_in_time']
			];

			return View::make('admin.shoppers.reports.orders', $data);

		}elseif ( $id == 'out-of-time' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders['orders_delivered_not_in_time']
			];

			return View::make('admin.shoppers.reports.orders', $data);
		}else{
			App::abort(404);
		}
	}

	/**
	 * Funcion para mostrar las ordenes que están en ruta.
	 */
	public function shoppers_route($id)
	{
		$orders = $this->shoppers_get_orders_route();

		if ( $id == 'in-time' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders['orders_before_delivery_date']
			];

			return View::make('admin.shoppers.reports.orders', $data);

		}elseif ( $id == 'out-of-time' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders['orders_after_delivery_date']
			];

			return View::make('admin.shoppers.reports.orders', $data);
		}else{
			App::abort(404);
		}

	}

	/**
	 * Funcion para mostrar las ordenes que estan en alsitamiento.
	 */
	public function shoppers_preparing_orders($id)
	{
		$orders_less = $this->shoppers_get_orders_enlistment();
		if ( $id == 'less-than-20-mins' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders_less['assigned_orders_less_than_20']
			];

			return View::make('admin.shoppers.reports.orders', $data);

		}elseif ( $id == 'more-than-20-mins' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders_less['assigned_orders_more_than_20']
			];

			return View::make('admin.shoppers.reports.orders', $data);
		}
	}

	/**
	 * Función para mostrar los registros encontrados de ordenes no asignadas
	 */
	public function shoppers_not_assigned($id)
	{
		$orders_not_assigned = $this->shoppers_get_orders_assigment();
		if ( $id == 'out-of-time' ) {

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders_not_assigned['red_75']
			];

			return View::make('admin.shoppers.reports.orders', $data);

		}elseif( $id == 'in-time' ){

			$data = [
				'title'           => 'Orders',
				'payment_methods' => $this->config['payment_methods'],
				'orders'          => $orders_not_assigned['orders_not_assigned']
			];

			return View::make('admin.shoppers.reports.orders', $data);
		}else{
			App::abort(404);
		}
	}

	/**
	 * Constructor de queries para reportes.
	 */
	public function shoppers_query_builder( $action, $data = NULL, $debug = false)
	{
		$now = Carbon::now();

		$query = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
						->join('cities','order_groups.user_city_id','=','cities.id')
						->join('stores','orders.store_id','=','stores.id')
						->leftJoin('shoppers', 'orders.shopper_id', '=', 'shoppers.id');

		if ($action == 'assignment') {
			$query->where('orders.shopper_id', 0)
				->where('orders.status', 'Initiated');

			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}
		if ($action == 'enlistment') {
			$query->where('orders.shopper_id', '!=', 0)
				->where('orders.status', 'In Progress');

			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}
		if ($action == 'route') {
			$query->where('orders.shopper_id', '!=', 0)
				->where('orders.status', 'Dispatched');

			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}
		if ($action == 'fulfillment_gt') {
			$query->where('orders.shopper_id', '<>', 0)
				->where('orders.status', 'Delivered')
				->where('orders.delivery_date', '>', DB::raw('orders.management_date'));

			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}
		if ($action == 'fulfillment_lt') {
			$query->where('orders.shopper_id', '<>', 0)
				->where('orders.status', 'Delivered')
				->where('orders.delivery_date', '<', DB::raw('orders.management_date'));

			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}
		/*if ($action != 'cancelled') {
			$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
		}*/
		if ($action == 'cancelled_reasons') {
			$query->where('orders.status', 'Cancelled')
				->where('orders.reject_reason', 'like', $data['reason']);
			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}
		if ( $action != 'fulfillment_gt' && $action != 'fulfillment_lt' ) {
			if ( !is_null($data['start_date']) && !is_null($data['end_date']) ) {
				$query->whereRaw('( orders.delivery_date BETWEEN DATE(\''.$data['start_date'].'\') AND DATE(\''.$data['end_date'].'\') )');
			}else{
				$query->where(DB::raw('DATE(orders.delivery_date)'), '=', $now->toDateString());
			}
		}

		$result = $query->select(
			'orders.id AS order_id',
			'orders.group_id AS order_group_id',
			'orders.user_id AS order_user_id',
			'orders.store_id AS order_store_id',
			'orders.shopper_id AS order_shopper_id',
			'orders.status AS order_status',
			'orders.date AS order_date',
			'orders.total_products AS order_total_products',
			'orders.total_amount AS order_total_amount',
			'orders.delivery_amount AS order_delivery_amount',
			'orders.discount_amount AS order_discount_amount',
			'orders.user_score AS order_user_score',
			'orders.is_checked AS order_is_checked',
			'order_groups.id AS ordergroups_id',
			'order_groups.user_id AS ordergroups_user_id',
			'order_groups.user_firstname AS ordergroups_user_firstname',
			'order_groups.user_lastname AS ordergroups_user_lastname',
			'order_groups.user_email AS ordergroups_user_email',
			'order_groups.user_phone AS ordergroups_user_phone',
			'order_groups.user_address AS ordergroups_user_address',
			'orders.payment_method AS ordergroups_payment_method',
			'orders.delivery_date AS ordergroups_delivery_date',
			'orders.delivery_time AS ordergroups_delivery_time',
			'order_groups.source AS ordergroups_source',
			'order_groups.source_os AS ordergroups_source_os',
			'shoppers.id AS shoppers_id',
			'shoppers.first_name AS shoppers_first_name',
			'shoppers.first_name AS shoppers_first_name',
			'shoppers.last_name AS shoppers_last_name',
			'cities.city AS cities_city',
			'stores.name AS stores_name'
		)
		->orderBy('orders.delivery_date', 'ASC');

		if ( $debug ) {
			$result = $result->toSql();
		}else{
			$result = $result->get();
		}

		return $result;
	}

	/**
	 * Filtrar hitoríco por fechas
	 */
	public function shoppers_filter()
	{
	    $start_date  = Input::get('start_date');
	    $end_date    = Input::get('end_date');
	    Session::set('start_date', $start_date);
	    Session::set('end_date', $end_date);
	    $fulfillment = $this->shoppers_get_orders_fulfillment($start_date, $end_date);
	    $cancelled   = $this->shoppers_get_orders_cancelled($start_date, $end_date);

	    $data = [
	        'title'       => 'Shoppers Filtered by date ranges report.',
	        'fulfillment' => $fulfillment,
	        'cancelled'   => $cancelled
	    ];

	    return View::make('admin.shoppers.reports.shoppers_filtered_report', $data);
	}

	/**
     * Reporte en graficas de pedidos y shoppers
     */
    public function shoppers_graphics()
    {
        Session::forget('start_date');
        Session::forget('end_date');
        $now = Carbon::now();
        // Ordenes no asignadas - ASIGNACION
        $data['graphic_1'] = $this->shoppers_get_orders_assigment();
        // Ordenes asignadas - ALISTAMINETO
        $data['graphic_2'] = $this->shoppers_get_orders_enlistment();
        // Ordenes asignadas con estado dispatch - EN RUTA
        $data['graphic_3'] = $this->shoppers_get_orders_route();
        // Cumplimiento
        $data['graphic_4'] = $this->shoppers_get_orders_fulfillment();
        // Cancelados
        $data['graphic_5'] = $this->shoppers_get_orders_cancelled();

        $data = [
            'title' => 'Shoppers Reports',
            'data'  => $data
        ];

        return View::make('admin.shoppers_reports', $data);
    }
}
?>