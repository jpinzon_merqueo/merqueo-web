<?php

namespace admin;

use URL, Request, View, Input, Route, Redirect, DB, MenuItem, Menu, Store, Response, Configuration, Config, Warehouse; //, Sitemap;

class AdminConfigController extends AdminController
{
    /**
	 * Menu
	 */
	public function menu()
	{
		$routes    = Route::getRoutes()->getRoutes();
		$menuItems = MenuItem::all();
		$menu      = Menu::find(1);

		$data = [
			'title'     => 'Menu Manager',
			'routes'    => $routes,
			'menuItems' => $menuItems,
			'menu'      => json_decode($menu->menu_order, true)
		];

		return View::make('admin.config.menu', $data);
	}

	/**
	 * Borrar items del menú
     */
	public function delete_menu_item()
	{
		$id = Input::get('id');

		$menuItem = MenuItem::find((int)$id);

		if ( $menuItem ) {
			$menu = Menu::find(1);
			$arr = json_decode($menu->menu_order, true);
			foreach ($arr as $key => $value) {
				if ( $value['id'] == $id ) {
					unset($arr[$key]);
				}
			}
			$arr = array_values($arr);
			$menu->menu_order = json_encode($arr);
			$menu->save();
			$menuItem->delete();
			return Redirect::back()->with('success', 'The menu item has been deleted.');
		}
		return Redirect::back()->with('error', 'There is a problem deleting the menu item.');
	}

	/**
	 * Agregar items al menu
	 */
	public function add_menu_item()
	{
		$menu              = Menu::find(1);
		$item              = [];
		$item['id']        = (int)Input::get('id');
		$item['title']     = Input::get('title');
		$item['iconclass'] = Input::get('iconclass');
		$item['action']    = Input::get('action');
		$menuItems         = MenuItem::find($item['id']);

		if ( $menuItems ) {
			$arr    = json_decode($menu->menu_order, true);
			$arr[]  = $item;
			$menu->menu_order = json_encode($arr);
			$menu->save();
            $this->admin_log('menu', $menu->id, 'update');

			return Redirect::back()->with('success', 'Your item has been added.');
		}else{
			return Redirect::back()->with('error', 'Error adding your item.');
		}
	}

	/**
	 * Guardar menu
     */
	public function save_menu()
	{
		$menu  = Menu::find(1);
		$order = Input::get('nestable-output');

		$menu->menu_order = $order;
		$menu->save();
        $this->admin_log('menu', $menu->id, 'update');

		return Redirect::back()->with('success', 'Menu order has been saved.');
	}

    /**
     * Obtener tiendas por ciudad ajax
     */
    public function get_stores_ajax()
    {
        $stores = array();
        if (Input::has('city_id')) {
            $stores = Store::select('id', 'name')->where('city_id', Input::get('city_id'))->where('status', 1)->get()->toArray();
        }
        return Response::json($stores, 200);
    }

    /**
     * @description : Cargando la pagina de configuraciones generales
     *
     * @return mixed
     */
    public function general()
    {
        $data = [
            'title' => 'General',
            'options' => Warehouse::select('value', 'type', 'cities.city', 'cities.id AS city_id', 'warehouses.warehouse', 'warehouses.id AS warehouse_id')
                ->join('cities', 'cities.id', '=', 'warehouses.city_id')
                ->leftJoin('config', function($join){
                    $join->on('cities.id', '=', 'config.city_id');
                    $join->on('warehouses.id', '=', 'config.warehouse_id');
                })
                ->orderBy('cities.id')
                ->orderBy('warehouses.id')
                ->get()
        ];

        return View::make('admin.config.general', $data);
    }

    /**
     * @description : eliminacion de archivos temporales de la carpeta downloads en el bucket dependiendo el ambiente
     *
     * @return mixed
     */
    public function delete_tmp_files_ajax()
    {
        remove_folder_s3(Config::get('app.download_directory'));
        return Response::json(['status' => true, 'result' => 'Ok'], 200);
    }

    /**
     * @return mixed
     */
    public function change_status_general_config_ajax()
    {
        try {
            DB::beginTransaction();

            Configuration::where('type', Input::get('type'))->where('warehouse_id', Input::get('warehouse_id'))->update(array('value' => (int)Input::get('value')));

            DB::commit();

            return Response::json(['status' => true], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(['status' => false], 500);
        }
    }

    public function change_compensation(){
        try{
            DB::beginTransaction();
            foreach (Input::all() as $key => $value){
                Configuration::where('key', $key)->update(['value'=> $value]);
            }
            DB::commit();
            return Response::json(['status' => true], 200);
        } catch (\Exception $e){
            DB::rollback();
            return Response::json(['status' => false, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * Metodo para generar el sitemap.xml dinamicamente
     *
     * @return mixed
     */
    /*public function generate_sitemaps_xml()
    {
        $cities = City::all();
        $products = new Product();
        /*$shelves = Shelf::all();
        $stores = Store::all();
        $departments = Department::all();

        foreach ($cities as $city)
        {
            $products = Product::select('products.*', 'departments.slug AS department_slug', 'shelves.slug AS shelf_slug', 'stores.slug AS store_slug', 'products.slug AS product_slug')
                ->join('departments', 'department_id', '=', 'departments.id')
                ->join('shelves', 'shelf_id', '=', 'shelves.id')
                ->join('stores', 'stores.id', '=', 'products.store_id')
                ->where('products.status', 1)
                ->whereRaw('IF( products.is_visible_stock = 0, IF( products.current_stock > 0, TRUE, FALSE), TRUE )')->get();

            foreach ($products as $product)
                //Sitemap::addTag(URL::to("/{$city->slug}/domicilios-{$product->store_slug}/{$product->department_slug}/{$product->shelf_slug}/{$product->slug}"), $product->created_at, 'week', '0.2');


        }
        return Sitemap::renderSitemap();
    }*/
}
