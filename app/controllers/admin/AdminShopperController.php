<?php
namespace admin;

use Request, Input, View, Session, Redirect, Response, Hash, Validator, DB, Config,
Order, Store,Shopper, Zone, ShopperZone, City, ShopperSchedule;

class AdminShopperController extends AdminController
{
	/**
     * Grilla de shoppers
     */
    public function index()
    {
        if (!Request::ajax()){
            $shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
            ->where('shoppers.status', 1)->select('shoppers.*');
            if (Session::get('admin_designation') != 'Super Admin')
                $shoppers->where('shoppers.city_id', Session::get('admin_city_id'));
            $shoppers = $shoppers->orderBy('first_name','asc')->limit(200)->get();

            return View::make('admin.shoppers.index')->with('title', 'Shoppers');
        }else{

            $search = Input::has('s') ? Input::get('s') : '';

            $shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
            ->where(function($query) use ($search){
                $query->where(DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%'.$search.'%');
                $query->orWhere('identity_number', 'LIKE', '%'.$search.'%');
                $query->orWhere('phone', 'LIKE', '%'.$search.'%');
            })->select('shoppers.*', 'city', DB::raw('DATE_FORMAT((SELECT activated_at FROM shopper_schedule WHERE shopper_schedule.shopper_id = shoppers.id ORDER BY shopper_schedule.id DESC LIMIT 1), "%d/%m/%Y %h:%i %p") AS schedule_activated_at'), DB::raw('DATE_FORMAT((SELECT deactivated_at FROM shopper_schedule WHERE shopper_schedule.shopper_id = shoppers.id ORDER BY shopper_schedule.id DESC LIMIT 1), "%d/%m/%Y %h:%i %p") AS schedule_deactivated_at'));

            if (Session::get('admin_designation') != 'Super Admin')
                $shoppers->where('shoppers.city_id', Session::get('admin_city_id'));

            $shoppers = $shoppers->orderBy('shoppers.status','DESC')->orderBy('first_name','asc')->limit(200)->get();

            $zones = Zone::where('zones.status', 1)->get();

             return View::make('admin.shoppers.index')
                    ->with('title', 'Shoppers')
                    ->with('shoppers', $shoppers)
                    ->with('zones', $zones)
                    ->renderSections()['content'];
        }
    }

    /**
     * Mapa de shoppers
     */
    public function locations()
    {
        $shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
                            ->where('shoppers.status', 1)
                            ->where('is_active', 1)
                            ->select('shoppers.*');
        if (Session::get('admin_designation') != 'Super Admin')
            $shoppers->where('shoppers.city_id', Session::get('admin_city_id'));
        $shoppers = $shoppers->orderBy('first_name','asc')->limit(200)->get();

        $city = City::find(Session::get('admin_city_id'));

        $shoppers_locations = array();
        foreach($shoppers as $shopper) {
            $shoppers_locations[] = array(
                'name' => $shopper->first_name . ' ' . $shopper->last_name,
                'location' => explode(' ', $shopper->location),
                'date' => get_time('left', $shopper->updated_at, false, false)
            );
        }
        $shoppers_locations = json_encode($shoppers_locations);

        return View::make('admin.shoppers.locations')->with('title', 'Úbicación de Shoppers')
                    ->with('shoppers', $shoppers)->with('city', $city)->with('shoppers_locations', $shoppers_locations);
    }

    /**
     * Editar shopper
     */
    public function edit($id = 0)
    {
        $sub_title = $id ? 'Editar Shopper' : 'Nuevo Shopper';
        $shopper = Shopper::find($id);
        if(!$shopper)
            $shopper = new Shopper;

        if (Session::get('admin_designation') != 'Super Admin')
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        else $cities = City::all();

        $zones = Zone::where('zones.status', '=', 1);
        if ( !empty( $shopper->city_id ) ) {
            $zones = $zones->where('city_id', $shopper->city_id);
        }

        $stores = Store::where('status', 1);
        if ( !empty( $shopper->city_id ) ) {
            $stores = $stores->where('city_id', $shopper->city_id);
        }
        $stores = $stores->get();
        $stores_selected = explode(',', $shopper->stores);
        $zones = $zones->get();

        return View::make('admin.shoppers.shopper_form')
                ->with('title', 'Shopper')
                ->with('sub_title', $sub_title)
                ->with('cities', $cities)
                ->with('shopper', $shopper)
                ->with('zones', $zones)
                ->with('stores', $stores)
                ->with('stores_selected', $stores_selected)
                ->with('store_admin', false);
    }

    /**
     * Activar o desactivar shopper
     */
    public function toggle()
    {
        $id = Input::get('shopper_id');
        $shopper = Shopper::find($id);
        if ($shopper){
            $shopper->is_active = ($shopper->is_active + 1 ) % 2;
            if ($shopper->is_active){
                $input_time = date('Y-m-d H:i:s', strtotime(Input::get('working_time')));
                $shopper_schedule_finder = ShopperSchedule::where('shopper_id', $id)->where('deactivated_at', '!=', 'NULL')->orderBy('created_at', 'DESC')->first();

                if (!$shopper_schedule_finder || $shopper_schedule_finder->deactivated_at < $input_time) {
                    $shopper_schedule = new ShopperSchedule();
                    $shopper_schedule->shopper_id   = $shopper->id;
                    $shopper_schedule->activated_at = $input_time;
                    $shopper_schedule->save();
                }else{
                    return Redirect::back()->with('type', 'failed')->with('message', 'La fecha de activación del shopper debe ser mayor a la fecha de desactivación del rango de trabajo anterior.');
                }
            }
            else{
                $shopper_schedule = ShopperSchedule::where('shopper_id', $id)->whereNull('deactivated_at')->orderBy('created_at', 'DESC')->first();
                $input_time = date('Y-m-d H:i:s', strtotime(Input::get('working_time')));
                if (!$shopper_schedule) {
                    $shopper_schedule = new ShopperSchedule();
                    $shopper_schedule->shopper_id   = $shopper->id;
                    $shopper_schedule->activated_at = $shopper->activated_at;
                }
                if ($shopper_schedule->activated_at > $input_time) {
                    return Redirect::back()->with('type', 'failed')->with('message', 'La fecha de desactivación del shopper debe ser mayor a la fecha de activación.');
                }
                $shopper_schedule->deactivated_at = $input_time;
                $shopper_schedule->save();
            }

            $shopper->save();

            return Redirect::back()->with('type', 'success')->with('message', 'Shopper actualizado con éxito.');
        }else return Redirect::back()->with('type', 'failed')->with('message', 'Ocurrió un error al actualizar el Shopper.');
    }

    /**
     * Guardar shopper
     */
    public function save()
    {
        $id = Input::get('id');
        $shopper = Shopper::find($id);
        if(!$shopper){
            $shopper = new Shopper;
            $action = 'insert';
        }else{
            ShopperZone::where('shopper_id', '=', $id)->delete();
            $action = 'update';
        }

        $shopper->first_name = Input::get('first_name');
        $shopper->last_name = Input::get('last_name');
        $shopper->phone = Input::get('phone');
        $shopper->identity_number = str_replace ( '.', '', Input::get('identity_number'));
        $shopper->status = (int)Input::get('status', 0);

        if (Input::has('id')) {
            $shopper_finder = Shopper::where('identity_number', $shopper->identity_number)->where('id', '<>', $id)->first();
            if ($shopper_finder) {
                return Redirect::route('adminShoppers.index')->with('type', 'error')->with('message', 'Ya existe un Shopper con la misma cédula.');
            }
        }else{
            $shopper_finder = Shopper::where('identity_number', $shopper->identity_number)->first();
            if ($shopper_finder) {
                return Redirect::route('adminShoppers.index')->with('type', 'error')->with('message', 'Ya existe un Shopper con la misma cédula.');
            }
        }

        $shopper->email = Input::get('email');
        $shopper->city_id = Input::get('city_id');
        $shopper->profile = Input::get('profile');

        if (Input::hasFile('photo'))
        {
            $cloudfront_url = Config::get('app.aws.cloudfront_url');
            $image = Input::file('photo');
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png')
            );
            if ($validator->fails())
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

            if (!empty($shopper->photo_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $shopper->photo_url);
                remove_file($path);
                remove_file_s3($shopper->photo_url);
            }
            $shopper->photo_url = upload_image($image, false, 'shoppers/photos');
        }
        if(Input::has('password')){
            $password = Input::get('password');
            if (!empty($password))
                $shopper->password = Hash::make(Input::get('password'));
        }

        if ($stores_id = Input::get('store_id')){
            $shopper->stores = implode(',', $stores_id);
        }else{
            $shopper->stores = NULL;
        }

        $zone_ids = Input::get('zone_id');
        if (count($zone_ids) > 0){
            $shopper->save();
            foreach ($zone_ids as $zone_id) {
                $zones = Zone::find($zone_id);
                $zones->created_at = date('Y-m-d H:i:s');
                $shopper->zonerel()->save($zones);
            }
        }else{
            $shopper->save();
            $this->admin_log('shoppers', $shopper->id, $action);
        }
        return Redirect::route('adminShoppers.index')->with('type', 'success')->with('message', 'Shopper actuzalizado con éxito.');
    }

    /**
     * Eliminar shopper
     */
    public function delete()
    {
        $id = Request::segment(4);
        $shopper = Shopper::find($id);
        Shopper::where('id', $id)->update(array('status' => 0));
        $this->admin_log('shoppers', $shopper->id, 'delete');
        return Redirect::route('adminShoppers.index')->with('type', 'success')->with('message', 'Shopper eliminado con éxito.');
    }

    /**
     * Obtener las zonas, tiendas por ciudad
     */
    public function get_zones_by_city_ajax()
    {
        $city_id = Input::get('city_id');
        if ( Input::has('shopper_id') ) {
            $shopper_id = Input::get('shopper_id');
            $shopper = Shopper::find($shopper_id);
        }

        $zones = Zone::where('zones.type_zone', '=', 'Shopper')
                    ->where('zones.city_id', $city_id)
                    ->where('zones.status', '=', 1)
                    ->get()->toArray();
        $stores = Store::where('status', 1)
                        ->where('city_id', $city_id)
                        ->get()->toArray();

        $result['zones'] = $zones;
        $result['stores'] = $stores;

        return json_encode($result);
    }

    /**
     * Mapa de shoppers
     */
    public function shoppers_tracking()
    {
        $shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
                    ->where('shoppers.status', 1)
                    ->select('shoppers.*', 'city');

        if (Session::get('admin_designation') != 'Super Admin')
            $shoppers->where('shoppers.city_id', Session::get('admin_city_id'));

        if (Session::get('admin_city_id') == 1){ //bogota
            $lat = 4.653197;
            $lng = -74.0989046;
        }else if (Session::get('admin_city_id') == 2){ //medellin
            $lat = 6.248076;
            $lng = -75.580142;
        }

        $shoppers = $shoppers->get();

        $locations = StoreLocation::join('stores', 'stores.id', '=', 'store_locations.store_id')
                        ->select('store_locations.*', 'stores.name AS store_name')
                        ->orderBy('stores.name')
                        ->get();

        $zonelocation = Zone::join('cities', 'cities.id', '=', 'zones.city_id')
                    ->select('zones.*', 'cities.city AS namecity')
                    ->where('status', '=', 1)
                    ->get();

        $zones = Zone::where('zones.status', '=', 1)
                    ->get();


        return View::make('admin.shoppers.reports.tracking')
                    ->with('title','Shoppers')
                    ->with('shoppers',$shoppers)
                    ->with('lat',$lat)
                    ->with('lng',$lng)
                    ->with('zones', $zones)
                    ->with('locations', $locations)
                    ->with('zonelocations', $zonelocation)
                    ->with('store_admin', false);
    }

    /**
     * Obtiene ubicacion de shoppers
     */
    public function get_locations()
    {
        $shoppers = Shopper::join('cities', 'cities.id', '=', 'shoppers.city_id')
        ->where('shoppers.status', 1)->select('shoppers.*')
        ->where('shoppers.city_id', Session::get('admin_city_id'))
        ->where('is_active', 1)
        ->orderBy('first_name','asc')->limit(200)->get();

        $shoppers_locations = array();
        foreach($shoppers as $shopper) {
            $shoppers_locations[] = array(
                'name' => $shopper->first_name . ' ' . $shopper->last_name,
                'location' => explode(' ', $shopper->location),
                'date' => get_time('left', $shopper->last_date_location, false, false)
            );
        }
        $shoppers_locations = json_encode($shoppers_locations);
        return Response::json($shoppers_locations, 200);
    }

    /**
     * Obtiene ubicacion de shopper en pedido
     */
    public function get_shopper_location($order_id)
    {
        if ($order_id){
            $shopper = Order::join('shoppers', 'shoppers.id', '=', 'orders.shopper_id')
                        ->where('orders.id', $order_id)
                        ->select('shoppers.location')->first();
            if ($shopper)
                return Response::json(json_encode($shopper->toArray()), 200);
        }
        return Response::json(array(), 200);
    }

    /**
     * Muestra reporte de horas trabajadas
     */
    public function shoppers_working_times($id)
    {
        $num = 20;
        if ( Input::has('id') ) {
            $id = (int)Input::get('id');
            $shopper = Shopper::join('cities', 'shoppers.city_id', '=', 'cities.id')
                            ->select(
                                'shoppers.id AS id',
                                'shoppers.first_name AS first_name',
                                'shoppers.last_name AS last_name',
                                'shoppers.phone AS phone',
                                'shoppers.email AS email',
                                'cities.city AS city'
                            )
                            ->find($id);
        }else{
            $shopper = Shopper::join('cities', 'shoppers.city_id', '=', 'cities.id')
                            ->select(
                                'shoppers.id AS id',
                                'shoppers.first_name AS first_name',
                                'shoppers.last_name AS last_name',
                                'shoppers.phone AS phone',
                                'shoppers.email AS email',
                                'cities.city AS city'
                            )
                            ->find((int)$id);
        }

        if ( Input::has('date') ) {
            $date = Input::get('date');
            $date = explode('/', $date);
            $date = $date[2].'-'.$date[1].'-'.$date[0];
            $working_times = ShopperSchedule::where('shopper_id', (int)$id)
                                                // ->where(DB::raw('DATE(shopper_schedule.activated_at) = "'.$date.'"'))
                                                ->whereDate('shopper_schedule.activated_at', '=', $date)
                                                ->orderBy('activated_at', 'DEC')
                                                ->select('shopper_schedule.id','shopper_schedule.shopper_id',DB::raw('DATE_FORMAT(shopper_schedule.activated_at, "%d/%m/%Y %h:%i:%s %p") as activated_at'),DB::raw('IF( shopper_schedule.deactivated_at IS NULL, DATE_FORMAT("'.date('Y-m-d H:i:s').'", "%d/%m/%Y %h:%i:%s %p"), DATE_FORMAT(shopper_schedule.deactivated_at, "%d/%m/%Y %h:%i:%s %p")) AS deactivated_at'),DB::raw('ROUND(TIME_TO_SEC(TIMEDIFF(IF(shopper_schedule.deactivated_at IS NULL, "'.date('Y-m-d H:i:s').'", shopper_schedule.deactivated_at ), shopper_schedule.activated_at ) )/3600, 1 ) AS diff_hours'), 'shopper_schedule.created_at','shopper_schedule.updated_at')
                                                ->paginate($num)
                                                ->toArray();
            // dd(DB::getQueryLog());
            $working_times2 = ShopperSchedule::where('shopper_id', (int)$id)
                                                // ->where(DB::raw('DATE(shopper_schedule.activated_at) = "'.$date.'"'))
                                                ->whereDate('shopper_schedule.activated_at', '=', $date)
                                                ->orderBy('activated_at', 'DEC')
                                               ->select('shopper_schedule.id','shopper_schedule.shopper_id',DB::raw('DATE_FORMAT(shopper_schedule.activated_at, "%d/%m/%Y %h:%i:%s %p") as activated_at'),DB::raw('IF( shopper_schedule.deactivated_at IS NULL, DATE_FORMAT("'.date('Y-m-d H:i:s').'", "%d/%m/%Y %h:%i:%s %p"), DATE_FORMAT(shopper_schedule.deactivated_at, "%d/%m/%Y %h:%i:%s %p")) AS deactivated_at'),DB::raw('ROUND(TIME_TO_SEC(TIMEDIFF(IF(shopper_schedule.deactivated_at IS NULL, "'.date('Y-m-d H:i:s').'", shopper_schedule.deactivated_at ), shopper_schedule.activated_at ) )/3600, 1 ) AS diff_hours'), 'shopper_schedule.created_at','shopper_schedule.updated_at')
                                                ->paginate($num);
        }else{
            $working_times = ShopperSchedule::where('shopper_id', (int)$id)
                                                ->orderBy('activated_at', 'DEC')
                                               ->select('shopper_schedule.id','shopper_schedule.shopper_id',DB::raw('DATE_FORMAT(shopper_schedule.activated_at, "%d/%m/%Y %h:%i:%s %p") as activated_at'),DB::raw('IF( shopper_schedule.deactivated_at IS NULL, DATE_FORMAT("'.date('Y-m-d H:i:s').'", "%d/%m/%Y %h:%i:%s %p"), DATE_FORMAT(shopper_schedule.deactivated_at, "%d/%m/%Y %h:%i:%s %p")) AS deactivated_at'),DB::raw('ROUND(TIME_TO_SEC(TIMEDIFF(IF(shopper_schedule.deactivated_at IS NULL, "'.date('Y-m-d H:i:s').'", shopper_schedule.deactivated_at ), shopper_schedule.activated_at ) )/3600, 1 ) AS diff_hours'), 'shopper_schedule.created_at','shopper_schedule.updated_at')
                                                ->paginate($num)
                                                ->toArray();
            $working_times2 = ShopperSchedule::where('shopper_id', (int)$id)
                                                ->orderBy('activated_at', 'DEC')
                                               ->select('shopper_schedule.id','shopper_schedule.shopper_id',DB::raw('DATE_FORMAT(shopper_schedule.activated_at, "%d/%m/%Y %h:%i:%s %p") as activated_at'),DB::raw('IF( shopper_schedule.deactivated_at IS NULL, DATE_FORMAT("'.date('Y-m-d H:i:s').'", "%d/%m/%Y %h:%i:%s %p"), DATE_FORMAT(shopper_schedule.deactivated_at, "%d/%m/%Y %h:%i:%s %p")) AS deactivated_at'),DB::raw('ROUND(TIME_TO_SEC(TIMEDIFF(IF(shopper_schedule.deactivated_at IS NULL, "'.date('Y-m-d H:i:s').'", shopper_schedule.deactivated_at ), shopper_schedule.activated_at ) )/3600, 1 ) AS diff_hours'), 'shopper_schedule.created_at','shopper_schedule.updated_at')
                                                ->paginate($num);
        }

        $data = [
            'title' => 'Shopper working times',
            'shopper' => $shopper,
            'working_times' => $working_times,
            'working_times2' => $working_times2
        ];

        return View::make('admin.shoppers.reports.working_times', $data);
    }

    /**
     * Actualiza ubicacion de shoppers
     */
    public function update_locations_ajax()
    {
        if (!Input::has('shopper_id'))
            return Response::json('ID shopper es requerido.', 400);
        if (!Input::has('location'))
            return Response::json('Location es requerido.', 400);

        $shopper = Shopper::select('shoppers.*')
                    ->where('shoppers.id', Input::get('shopper_id'))
                    ->first();

        if($shopper){
           $shopper->location = Input::get('location');
           $shopper->last_date_location = date('Y-m-d H:i:s');
           $shopper->save();
           $result = array('status' => 'OK');
        }else $result = array('status' => 'ERROR');

        return Response::json($result, 200);
    }
}