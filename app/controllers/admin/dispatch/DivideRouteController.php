<?php

namespace admin\dispatch;

use admin\AdminController;

use Illuminate\Routing\Route;
use Request, View, Input, Redirect, Session, DB, Config, Response, Order, Vehicle,
    PHPExcel\IOFactory, Event, OrderEventHandler, Transporter, OrderProduct, Warehouse,
    Driver, Routes, Fpdf, Store, City, OrderGroup;

class DivideRouteController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new OrderEventHandler);
    }

    /**
     * Despachar pedidos
     */
    public function index()
    {
        $warehouse = Warehouse::where('city_id', Session::get('admin_city_id'))->first();
        $routes = Routes::select('routes.id', 'route')->where(function ($query) {
            $query->where('orders.status', '=', 'Enrutado')
                ->orWhere('orders.status', '=', 'In Progress')
                ->orWhere('orders.status', '=', 'Alistado')
                ->orWhere('orders.status', '=', 'Dispatched');
        })
            ->where('routes.city_id', Session::get('admin_city_id'))
            ->where('order_groups.warehouse_id', $warehouse->id)
            ->join('orders', 'orders.route_id', '=', 'routes.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->groupBy('routes.id')
            ->orderBy('routes.id')
            ->get();

        $transporters = Transporter::where('city_id', Session::get('admin_city_id'))->where('status',
            1)->orderBy('fullname')->get();
        $vehicles = Vehicle::where('status', 1)->orderBy('plate')->get();
        $drivers = [];
        $data = [
            'cities' => $this->get_cities(),
            //            'warehouses' => $this->get_warehouses(),
            'warehouses' =>  Warehouse::where('city_id', Session::get('admin_city_id'))
                ->select('warehouses.id', 'warehouses.warehouse')->get(),
            'routes' => $routes,
            'tipification_list' => Routes::TIPIFICATION_LIST,
            'title' => 'Dividir y Reasignar Ruta',
            'transporters' => $transporters,
            'vehicles' => $vehicles,
            'drivers' => $drivers
        ];
        return View::make('admin.dispatch.divide_route', $data);
    }


    /**
     * Método para obtener los pedidos de una ruta en específico y asignarlas a otra.
     *
     * @param  integer $routeId Id de la ruta
     */
    public function divideRoute($routeId)
    {
        if (Request::isMethod('POST')) {
            return Routes::storeRouteDivision($routeId, Input::all());
        } else {
            return Routes::getRouteDivision($routeId);
        }
    }


    /**
     * Validar pedidos de una ruta
     */
    public function validateRoutes()
    {
        $routeId = Input::get('route_id');
        $cityId = Input::get('city_id');
        $warehouseId = Input::get('warehouse_id');
        $errors = [];
        $success = 0;

        $orders = Order::where('route_id', $routeId)
            ->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress')
                    ->orWhere('orders.status', '=', 'Alistado');
            })
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->leftJoin('stores', 'orders.store_id', '=', 'stores.id')
            ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
            ->whereIn('cities.id', $this->get_all_city_ids($cityId))
            ->where('stores.status', 1)
            ->whereNotNull('orders.route_id')
            ->select('orders.id', 'orders.cc_charge_id', 'orders.status', 'stores.is_storage')
            ->orderBy('orders.route_id')
            ->get();
        if (!count($orders)) {
            return Redirect::route('adminPlanning.transport')
                ->with('city_id', $cityId)
                ->with('warehouse_id', $warehouseId)
                ->with('route_id', $routeId)
                ->with('assign_exception', 'No se encontraron pedidos.');
        }

        foreach ($orders as $key => $order) {
            if ($order->status == 'Alistado') {
                //validar que los productos no esten pendientes
                $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                    ['Pending', 'Missing'])->count();
                if ($order_products) {
                    $errors[$order->id]['products'] = 'Hay productos pendientes o faltantes en el pedido.';
                    $errors[$order->id]['is_storage'] = $order->is_storage;
                }

                if ($order->payment_method == 'Tarjeta de crédito') {
                    $payments = $order->getPayments(true);
                    if (!$payments || ($payments && !empty($payments->cc_refund_date))) {
                        $errors[$order->id]['payment_method'] = 'Se debe realizar primero el cobro a la tarjeta de crédito.';
                        $errors[$order->id]['is_storage'] = $order->is_storage;
                    }
                }

                if (empty($errors[$order->id])) {
                    $success++;
                }

            } else {
                $errors[$order->id]['status'] = 'El pedido no esta en estado Alistado.';
                $errors[$order->id]['is_storage'] = $order->is_storage;
            }
        }

        return Redirect::route('adminDispatchPlanning.transport')
            ->with('city_id', $cityId)
            ->with('warehouse_id', $warehouseId)
            ->with('route_id', $routeId)
            ->with('validate_errors', $errors)
            ->with('validate_success', $success);
    }

    /**
     * Obtiene conductores de un vehículo
     */
    public function getDriversAjax()
    {
        $vehicleId = Input::get('vehicle_id');
        $drivers = Driver::select('drivers.id', 'drivers.first_name', 'drivers.last_name')
            ->join('vehicle_drivers', 'drivers.id', '=', 'vehicle_drivers.driver_id')
            ->where('drivers.status', 1)->where('drivers.profile', 'Conductor');
        if (!empty($vehicleId)) {
            $drivers->where('vehicle_drivers.vehicle_id', $vehicleId);
        }
        $drivers = $drivers->orderBy('drivers.first_name')->orderBy('drivers.last_name')->get();

        return Response::json($drivers);
    }

    /**
     * Reasignar pedidos de una ruta a un transportador
     */
    public function reassignTransporters()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $routeId = Input::get('reasign_route_id');
        $cityId = Input::get('reasign_city_id');
        $warehouseId = Input::get('reasign_warehouse_id');
        $driverId = Input::get('reasign_driver_id');
        $vehicleId = Input::get('reasign_vehicle_id');

        if (!$vehicle = Vehicle::find(Input::get('reasign_vehicle_id'))) {
            return Redirect::back()
                ->with('city_id', $cityId)
                ->with('warehouse_id', $warehouseId)
                ->with('route_id', $routeId)
                ->with('assign_exception', 'Vehículo no existe.');
        }
        $route  = Routes::findOrFail($routeId);
        $oderFirst = Order::where('route_id',$routeId)->where('status','!=','Cancelled')->first();

        if ($oderFirst->vehicle_id == $vehicleId) {
            return Response::json([
                'status' => 0,
                'message' => 'El vehículo debe ser diferente.',
            ]);
        }

        $vehicleDay = Routes::whereHas('orders', function ($query) use ($vehicleId) {
            $query->where('vehicle_id', $vehicleId)
                ->whereNotIn('status', ['Delivered','Cancelled']);

        })->where('shift', $route->shift)
            ->where(DB::raw('DATE(planning_date)'), '=', \Carbon\Carbon::now()->toDateString())
            ->get();

        if (count($vehicleDay)) {
            return Response::json([
                'status' => 0,
                'message' => 'El vehículo ya tiene una ruta para la misma zona horaria.',
            ]);
        }

        $success = 0;

        $orders = Order::select('orders.*')->where('route_id', $routeId)
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->where('orders.status', 'Dispatched')
            ->whereIn('order_groups.user_city_id', $this->get_all_city_ids($cityId))
            ->orderBy('orders.route_id')
            ->get();
        if (count($orders)<1) {
            return Response::json([
                'status' => 0,
                'message' => 'La ruta no contiene pedidos para reasignar.',
            ]);
        }
        try {
            DB::beginTransaction();

            foreach ($orders as $key => $order) {
                $order->driver_id = $driverId;
                $order->vehicle_id = $vehicleId;
                $order->save();

                Event::fire('order.transporter_log',
                    [[$order, Session::get('admin_id'), $vehicle->plate, $order->driver_id]]);

                $success++;
            }

            DB::commit();

            return Response::json([
                'status' => 1,
                'message' => 'Se reasigno la ruta.',
            ]);

        } catch (\Exception $e) {
            DB::rollback();

            return Response::json([
                'status' => 0,
                'message' => '¡Al parecer tenemos problemas tratando de obtener la información!',
            ]);
        }
    }

}
