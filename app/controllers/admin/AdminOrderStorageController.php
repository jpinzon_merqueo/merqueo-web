<?php

namespace admin;

use App, Request, View, Input, Route, Redirect, Session, DB, Config, DateTime, Response, Validator, Order, OrderProduct,
    Picker, User, UserCredit, UserCreditCard, UserAddress, UserCoupon, Store, Product, OrderNoteSms, OrderLog, OrderGroup,
    RejectReason, City, Payment, UserBrandCampaign, UserFreeDelivery, Zone, Pdf, Exception, Transporter, Routes, Warehouse,
    Vehicle, Driver, OrderReturn, VehicleMovement, Event, OrderEventHandler, OrderProductGroup, DeliveryWindow, OrderProductScore,
    OrderReturnDetail, StoreProduct, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007, Pusherer, AlliedStore, Locality,
    OrderPayment, OrderPaymentRefund, PayU, Sampling, OrderDiscount;
use ErrorLog;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Zend\Json\Json;
use Carbon\Carbon;
use exceptions\MerqueoException;
use Illuminate\Http\Request as HttpRequest;
use Merqueo\Backend\Facades\AuthBackend;
use exceptions\PayUException;

class AdminOrderStorageController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        Event::subscribe(new OrderEventHandler);
    }

    /**
     * Grilla de pedidos
     */
    public function index()
    {
        if (!Request::ajax()) {
            $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=',
                'cities.id')->where('stores.status', 1)->where('is_storage', 1);
            $warehouses = Warehouse::select('warehouses.warehouse', 'warehouses.id')->where('city_id',
                Session::get('admin_city_id'))->get();
            $allied_stores = AlliedStore::select('allied_stores.name', 'allied_stores.id', 'allied_stores.city_id')
                ->join('cities', 'city_id', '=', 'cities.id')
                ->where('allied_stores.status', 1)->get();

            $data = [
                'title' => 'Pedidos de Bodega',
                'stores' => $stores,
                'payment_methods' => $this->config['payment_methods'],
                'cities' => $this->get_cities(),
                'allied_stores' => $allied_stores,
                'warehouses' => $warehouses,
                'delivery_windows' => DeliveryWindow::getDeliveryWindowsByCity(Session::get('admin_city_id')),
                'delivery_windows_same_day' => DeliveryWindow::getDeliveryWindowsByCity(
                    Session::get('admin_city_id'),
                    true
                ),
                'sources' => array_merge([
                    'Web',
                    'Device',
                    'Reclamo',
                    'Web Service',
                    'Callcenter'
                ], User::DARKSUPERMARKET_TYPE)
            ];

            return View::make('admin.orders_storage.index', $data);

        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            $orders = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->leftJoin('warehouses', 'order_groups.warehouse_id', '=', 'warehouses.id')
                ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                ->leftJoin('order_product_group', function ($join) {
                    $join->on('order_product_group.order_id', '=', 'orders.id');
                    $join->on('store_products.id', '=', 'order_product_group.store_product_id');
                })
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->leftJoin('drivers', 'orders.driver_id', '=', 'drivers.id')
                ->leftJoin('vehicles', 'orders.vehicle_id', '=', 'vehicles.id')
                ->leftJoin('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
                ->leftJoin('pickers', 'orders.picker_dry_id', '=', 'pickers.id')
                ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
                ->leftJoin('routes', 'orders.route_id', '=', 'routes.id')
                ->leftJoin(DB::raw('(user_credits, coupons)'), function ($join) {
                    $join->on('user_credits.order_id', '=', 'orders.id');
                    $join->on('user_credits.type', '=', DB::raw('0'));
                    $join->on('user_credits.coupon_id', '=', 'coupons.id');
                })
                ->where(function ($query) use ($search) {
                    $query->where(DB::raw('CONCAT(order_groups.user_firstname, " ", order_groups.user_lastname)'),
                        'LIKE', '%' . $search . '%');
                    $query->orWhere('orders.id', 'LIKE', '%' . $search . '%');
                    $query->orWhere('orders.reference', 'LIKE', '%' . $search . '%');
                    $query->orWhere('order_groups.user_email', 'LIKE', '%' . $search . '%');
                    $query->orWhere('order_groups.user_address', 'LIKE', '%' . $search . '%');
                    $query->orWhere('order_groups.user_phone', 'LIKE', '%' . $search . '%');
                })
                ->select(
                    'orders.*',
                    'users.sift_payment_abuse',
                    'order_groups.user_firstname',
                    'order_groups.user_lastname',
                    'order_groups.user_email',
                    'order_groups.user_address',
                    'order_groups.user_address_further',
                    'order_groups.user_address_neighborhood',
                    'order_groups.user_phone',
                    'order_groups.user_comments',
                    'orders.delivery_date AS customer_delivery_date',
                    'order_groups.source',
                    'order_groups.source_os',
                    'zones.name AS zone',
                    'transporters.fullname',
                    'vehicles.plate',
                    'drivers.id AS driver_id',
                    'drivers.first_name AS driver_first_name',
                    'drivers.last_name AS driver_last_name',
                    'pickers.first_name AS picker_first_name',
                    'pickers.last_name AS picker_last_name',
                    'cities.city',
                    'stores.name AS store',
                    'coupons.code AS coupon_code',
                    'routes.route',
                    'orders.planning_route',
                    'planning_sequence',
                    'delivery_time_minutes AS store_delivery_time_minutes',
                    DB::raw("ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), orders.delivery_date))/3600, 1) AS diff"),
                    'posible_fraud',
                    DB::raw("SUM(IF(store_products.storage = 'Seco', 1, 0)) AS products_storage_dry"),
                    DB::raw("SUM(IF(store_products.storage = 'Refrigerado' || store_products.storage = 'Congelado', 1, 0)) AS products_storage_cold"),
                    'warehouses.warehouse AS warehouse'
                );
            //filtros
            if (Input::get('warehouse_id')) {
                $orders->where('warehouses.id', Input::get('warehouse_id'));
            }
            if (Input::get('transporter_id')) {
                $orders->where('transporters.id', Input::get('transporter_id'));
            }
            if (Input::get('vehicle_id')) {
                $orders->where('vehicles.id', Input::get('vehicle_id'));
            }
            if (Input::get('driver_id')) {
                $orders->where('drivers.id', Input::get('driver_id'));
            }
            if (Input::get('store_id')) {
                $orders->where('stores.id', Input::get('store_id'));
            }
            if (Input::get('route_id')) {
                if (Input::get('route_id') != 'NULL') {
                    $orders->where('routes.id', Input::get('route_id'));
                } else {
                    $orders->whereNull('orders.route_id');
                }
            }

            if (Input::get('allied_stores')) {
                $orders->where('orders.allied_store_id', Input::get('allied_stores'));
            }

            if (Input::get('zone_id')) {
                if (Input::get('zone_id') != 'NULL') {
                    $orders->where('zones.id', Input::get('zone_id'));
                } else {
                    $orders->whereNull('order_groups.zone_id');
                }
            }
            if (Input::get('picker_dry_id')) {
                $orders->where('pickers.id', Input::get('picker_dry_id'));
            }
            if (Input::get('delivery_date') && Input::get('delivery_date_end')) {
                $orders->where('orders.delivery_date', '>=',
                    format_date('mysql', Input::get('delivery_date')) . ' 00:00:00');
                $orders->where('orders.delivery_date', '<=',
                    format_date('mysql', Input::get('delivery_date_end')) . ' 23:59:59');
            }
            if (Input::get('delivery_window_id')) {
                $orders->where('orders.delivery_window_id', Input::get('delivery_window_id'));
            }
            /*if (Input::get('order_date')){
                if (Input::get('order_date') == 'today'){
                    $orders->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'));
                    $orders->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'));
                }else{
                    if (Input::get('show_orders') == 'update_stock_back'){
                        $orders->where('order_products.update_stock_back', 1);
                        $orders->orWhere('order_product_group.update_stock_back', 1);
                    }else{
                        if (Input::get('show_orders') == 'no_paid')
                            $orders->where('orders.status', 'Delivered')->whereNull('orders.payment_date');
                    }
                }
            }*/

            if (Input::get('show_orders') == 'update_stock_back') {
                $orders->where('order_products.update_stock_back', 1);
                $orders->orWhere('order_product_group.update_stock_back', 1);
            } else {
                if (Input::get('show_orders') == 'no_paid') {
                    $orders->where('orders.status', 'Delivered')->whereNull('orders.payment_date');
                }
            }

            if (Input::get('status')) {
                $status = Input::get('status');
                $statuses = explode(',', $status);
                unset($statuses[count($statuses) - 1]);
                $orders->where(function ($query) use ($statuses) {
                    foreach ($statuses as $status) {
                        $query->orWhere('orders.status', $status);
                    }
                });
            }

            if (Input::get('reject_reason')) {
                if (Input::get('reject_reason') == 'Cancelado por conductor') {
                    $orders->whereNotNull('orders.driver_cancel_date');
                } else {
                    $orders->where('orders.status', 'Cancelled');
                    $orders->Where('orders.reject_reason', 'LIKE', '%' . Input::get('reject_reason') . '%');
                }
            }

            if (Input::get('city_id')) {
                $orders->where('stores.city_id', Input::get('city_id'));
            } else {
                if (Session::get('admin_designation') != 'Super Admin') {
                    $orders->where('stores.city_id', Session::get('admin_city_id'));
                }
            }
            if (Input::get('source') && !empty(Input::get('source'))) {
                $orders->where('source', Input::get('source'));
            }
            if (Input::get('source_os')) {
                $orders->where('source_os', Input::get('source_os'));
            }
            if (Input::get('type') && !empty(Input::get('type'))) {
                $orders->where('orders.type', Input::get('type'));
            }
            if (Input::get('order_by')) {
                if (Input::get('order_by') == 'unfulfilled_orders') {
                    $orders->orderBy(DB::raw("FIELD(orders.status, 'Validation', 'Initiated', 'Enrutado', 'In Progress', 'Alistado', 'Dispatched', 'Delivered', 'Cancelled')"))->orderBy('diff',
                        'desc');
                } else {
                    $orders->orderBy(Input::get('order_by'), 'desc');
                }
            }
            if (Input::get('payment_method')) {
                $orders->where('orders.payment_method', Input::get('payment_method'));
            }

            $orders = $orders->where('stores.is_storage', 1)->groupBy('orders.id')->limit(80)->get();

            if (count($orders)) {
                $suspect_credit_card_order_amount = Config::get('app.suspect_credit_card_order_amount');
                foreach ($orders as $order) {
                    //validar pedidos sospechosos
                    if ($order->status != 'Cancelled' && $order->status != 'Delivered' && $order->payment_method == 'Tarjeta de crédito' && $order->total_amount_og > $suspect_credit_card_order_amount) {
                        $order->suspect = 1;
                    } else {
                        $order->suspect = 0;
                    }

                    //obtener numero de pedidos por cliente
                    $qty = Order::where('user_id', $order->user_id)->where('user_id', '<>', 5169)->where('status',
                        'Delivered')->count();
                    $order->orders_qty = $qty;
                }
            }
            /*$queries = DB::getQueryLog();
            $last_query = end($queries);
            debug($last_query);*/
        }
        $data = [
            'title' => 'Pedidos de Bodega',
            'orders' => $orders
        ];

        return View::make('admin.orders_storage.index', $data)
            ->renderSections()['content'];
    }

    /**
     * Detalle de un pedido
     */
    public function details($id)
    {
        $order = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('cities AS cities_user', 'cities_user.id', '=', 'order_groups.user_city_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('cities AS cities_store', 'cities_store.id', '=', 'stores.city_id')
            ->join('users', 'order_groups.user_id', '=', 'users.id')
            ->leftJoin('user_address', 'user_address.user_id', '=', 'users.id')
            ->join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
            ->leftJoin('allied_stores', 'allied_stores.id', '=', 'orders.allied_store_id')
            ->leftJoin('user_credit_cards', 'orders.credit_card_id', '=', 'user_credit_cards.id')
            ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->leftJoin('pickers AS pickers_dry', 'orders.picker_dry_id', '=', 'pickers_dry.id')
            ->leftJoin('pickers AS pickers_cold', 'orders.picker_cold_id', '=', 'pickers_cold.id')
            ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
            ->leftJoin('vehicle_movements', 'vehicle_movements.order_id', '=', 'orders.id')
            ->leftJoin('routes', 'orders.route_id', '=', 'routes.id')
            ->leftJoin('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
            ->leftJoin(DB::raw('(user_credits, coupons)'), function ($join) {
                $join->on('user_credits.order_id', '=', 'orders.id');
                $join->on('user_credits.type', '=', DB::raw('0'));
                $join->on('user_credits.coupon_id', '=', 'coupons.id');
                $join->on('user_credits.status', '=', DB::raw('1'));
            })
            ->leftJoin('order_returns', function ($join) {
                $join->on('order_returns.order_id', '=', 'orders.id');
                $join->where('order_returns.old_order_status', '=', 'Dispatched');
                $join->where('order_returns.status', '=', 'Pendiente');
            })
            ->leftJoin('orders AS child_orders', 'orders.id', '=', 'child_orders.parent_order_id')
            ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
            ->leftJoin('localities', 'order_groups.locality_id', '=', 'localities.id')
            ->leftJoin('user_devices', 'order_groups.user_device_id', '=', 'user_devices.id')
            ->where('orders.id', $id)
            ->where('stores.is_storage', 1)
            ->select('orders.*', 'routes.route', 'order_returns.id AS order_return_id', 'users.sift_payment_abuse',
                'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email',
                'coupons.code AS coupon_code',
                'order_groups.user_address', 'order_groups.user_address_neighborhood',
                'order_groups.user_address_further', 'order_groups.user_phone',
                'orders.delivery_date AS customer_delivery_date', 'order_groups.source', 'order_groups.app_version',
                DB::raw("CONCAT(pickers_dry.first_name, ' ', pickers_dry.last_name) AS picker_dry_name"),
                DB::raw("CONCAT(pickers_cold.first_name, ' ', pickers_cold.last_name) AS picker_cold_name"),
                'user_card_paid', 'order_groups.ip',
                'stores.name AS store_name', 'stores.id AS store_id', 'cities_store.city AS store_city',
                'zones.id AS zone_id', 'zones.name AS zone', 'orders.invoice_number', 'stores.revision_orders_required',
                'order_groups.source_os', 'order_groups.user_comments', 'cities_user.city', 'cities_user.id AS city_id',
                DB::raw("SUM(orders.total_amount) AS total_amount_og"),
                'user_brand_campaigns.campaign', 'order_groups.user_address_latitude',
                'order_groups.user_address_longitude', DB::raw("COUNT(child_orders.id) AS num_child_orders"),
                'picking_group', 'order_picking_quality.id AS order_picking_quality_id',
                'order_picking_quality.validation_date', DB::raw("allied_stores.name AS allied_store_name"),
                'localities.name AS locality, user_devices.regid AS user_device', 'users.referred_by',
                'user_address.address_1', 'user_address.address_2', 'user_address.address_3', 'user_address.address_4')
            ->groupBy('orders.id');

        if (Session::get('admin_designation') != 'Super Admin') {
            $order->where('stores.city_id', Session::get('admin_city_id'));
        }

        $order = $order->with('orderGroup.warehouse')->first();

        if (!$order) {
            return App::abort(404);
        }

        $user_promoter = null;
        if (!is_null($order->referred_by)) {
            $user_promoter = User::leftJoin('order_groups', 'users.id', '=', 'order_groups.user_id')
                ->where('users.id', $order->referred_by)
                ->select('users.id', 'users.first_name', 'users.last_name', 'users.phone', 'users.email',
                    'order_groups.user_address', 'order_groups.user_address_further', 'order_groups.source',
                    'order_groups.ip',
                    'order_groups.source_os', 'order_groups.user_device_id')
                ->first();
        }

        //validar pedido sospechoso
        $suspect_credit_card_order_amount = Config::get('app.suspect_credit_card_order_amount');
        if ($order->status != 'Cancelled' && $order->status != 'Delivered' && $order->payment_method == 'Tarjeta de crédito' && $order->total_amount_og > $suspect_credit_card_order_amount) {
            $order->suspect = 'El pedido es con tarjeta de crédito y el total agrupado por tienda es mayor a $' . number_format($suspect_credit_card_order_amount,
                    0, ',', '.');
        } else {
            $order->suspect = 0;
        }

        $order_products = OrderProduct::where('order_id', $order->id)
            ->where('is_gift', 0)
            //->where('type', '<>', 'Muestra')
            ->where('parent_id', 0)
            ->join('stores', 'order_products.store_id', '=', 'stores.id')
            ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
            ->select('order_products.*', 'stores.name AS store_name')
            ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
            ->orderBy('store_products.department_id', 'desc')
            ->orderBy('product_name')
            ->get();

        //obtener productos regalo
        $assigned_gifts = [];
        if ($order->date > '2018-01-01 00:00:00') {
            foreach ($order_products as $order_product) {
                if (empty($assigned_gifts[$order_product->store_product_id])) {
                    $assigned_gifts[$order_product->store_product_id] = 1;

                    $order_product_samplings = OrderProduct::select('order_products.id AS order_product_id',
                        'order_products.type', 'store_product_id AS id', 'price', 'quantity AS quantity_cart',
                        'quantity_original AS quantity_cart_original', 'product_name AS name',
                        'order_products.fulfilment_status',
                        'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit',
                        'fulfilment_status', 'reference', 'sampling_id', 'campaign_gift_id', 'is_gift',
                        'order_products.type')
                        ->where('parent_id', $order_product->store_product_id)
                        ->where('order_id', $order->id)
                        ->whereIn('type', ['Muestra', 'Product'])
                        ->get();

                } else {
                    $order_product_sampling = [];
                }

                if (count($order_product_samplings)) {

                    foreach ($order_product_samplings as $order_product_sampling) {

                        if (!$order_product_sampling->is_gift && $order_product_sampling->type == 'Muestra') {
                            $sampling = Sampling::find($order_product_sampling->sampling_id);
                            $samplings[] = $order_product_sampling;
                        }

                        if ($order_product_sampling->is_gift) {
                            $sampling = \CampaignGift::find($order_product_sampling->campaign_gift_id);
                            $order_product->gift = $order_product_sampling;
                        }

                        if (isset($sampling) && $sampling) {
                            $order_product_sampling->message = 'Merqueo y ' . $sampling->brand . ' te regalan:';
                        }
                    }

                    if (isset($samplings)) {
                        $order_product->samplings = $samplings;
                        unset($samplings);
                    }
                }

                if ($order_product->fulfilment_status == 'Not Available') {
                    $order_product_gift = OrderProduct::select('order_products.id AS order_product_id',
                        'store_product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name',
                        'order_products.fulfilment_status',
                        'product_image_url AS image_url', 'product_quantity AS quantity', 'product_unit AS unit',
                        'fulfilment_status', 'reference')
                        ->where('parent_id', $order_product->store_product_id)
                        ->where('order_id', $order->id)
                        ->where('is_gift', 1)
                        ->first();
                    if ($order_product_gift) {
                        $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                        $order_product->gift = $order_product_gift;
                    }
                }
            }
        }

        $order_notes = OrderNoteSms::select('order_notes_sms.created_at', 'order_notes_sms.description',
            'admin.fullname')
            ->join('admin', 'admin.id', '=', 'admin_id')->where('order_id', $order->id)->where('order_notes_sms.type',
                'note')
            ->orderBy('created_at', 'desc')->get();

        $customer_orders = Order::select('orders.*', 'order_groups.user_firstname', 'order_groups.user_lastname',
            'order_groups.user_email', 'order_groups.user_address',
            'order_groups.user_address_further', 'order_groups.user_phone', 'order_groups.user_comments',
            'orders.delivery_date AS customer_delivery_date',
            'order_groups.source', 'order_groups.source_os', 'stores.name AS store')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->where('orders.user_id', $order->user_id)->where('orders.id', '<>',
                $order->id)->orderBy('orders.created_at', 'desc')->get();
        $customer_credits = UserCredit::getCreditMovements($order->user_id);
        $customer_credit_cards = UserCreditCard::where('user_id', $order->user_id)->get();
        $customer_free_deliveries = UserFreeDelivery::where('user_id', $order->user_id)->where('status',
            1)->orderBy('created_at', 'desc')->get();

        $order_transporter = null;
        if ($order->vehicle_id) {
            $order_transporter = Vehicle::where('vehicles.id', $order->vehicle_id)
                ->join('transporters', 'vehicles.transporter_id', '=', 'transporters.id')
                ->select('transporters.fullname AS transporter_fullname', 'vehicles.plate AS vehicle_plate')
                ->first();

            $driver = Driver::where('drivers.id', $order->driver_id)
                ->select('drivers.first_name AS driver_first_name', 'drivers.last_name AS driver_last_name',
                    'drivers.phone AS driver_phone')
                ->first();

            $order_transporter = new Driver();
            $order_transporter->driver_first_name = $driver->driver_first_name;
            $order_transporter->driver_last_name = $driver->driver_last_name;
            $order_transporter->driver_phone = $driver->driver_phone;
        }

        $order_log = OrderLog::select(DB::raw("CONCAT(pickers.first_name,' ', pickers.last_name) AS picker_name"),
            DB::raw("CONCAT(users.first_name,' ', users.last_name) AS user_name"),
            DB::raw("CONCAT(drivers.first_name,' ', drivers.last_name) AS driver_name"),
            DB::raw("admin.fullname AS 'admin_name'"), 'order_logs.type', 'order_logs.created_at')
            ->leftJoin('pickers', 'order_logs.picker_id', '=', 'pickers.id')
            ->leftJoin('drivers', 'order_logs.driver_id', '=', 'drivers.id')
            ->leftJoin('users', 'order_logs.user_id', '=', 'users.id')
            ->leftJoin('admin', 'order_logs.admin_id', '=', 'admin.id')
            ->where('order_logs.order_id', $order->id)
            ->get();
        $cities = City::all();
        $city_id = $this->getCoverageStoreCityId($order->city_id);

        $transporters = Transporter::where('status', 1)->where('city_id', $city_id)->orderBy('fullname', 'asc')->get();
        $pickers['dry'] = Picker::where('status', 1)->where('profile', 'Alistador Seco')->where('city_id',
            $city_id)->orderBy('first_name', 'asc')->get();
        $pickers['cold'] = Picker::where('status', 1)->where('profile', 'Alistador Frío')->where('city_id',
            $city_id)->orderBy('first_name', 'asc')->get();
        $routes = Routes::select('routes.id', 'routes.route')
            ->join('orders', 'orders.route_id', '=', 'routes.id')
            ->whereNotNull('orders.route_id')
            ->where('routes.city_id', $city_id)
            ->groupBy('routes.id')
            ->orderBy('routes.id');

        //mostrar rutas de planeacion no terminadas
        $admin_permissions = json_decode(Session::get('admin_permissions'), true);
        if (isset($admin_permissions['adminroute']) && $admin_permissions['adminroute']['permissions']['update']) {
            $routes = $routes->where(function ($query) {
                $query->where('orders.status', 'Initiated')
                    ->orWhere('orders.status', 'Enrutado')
                    ->orWhere('orders.status', 'In Progress')
                    ->orWhere('orders.status', 'Dispatched')
                    ->orWhere('orders.status', 'Alistado');
            })->get();
        } else {
            $routes = $routes->where(function ($query) {
                $query->where('orders.status', 'Enrutado')
                    ->orWhere('orders.status', 'In Progress')
                    ->orWhere('orders.status', 'Dispatched')
                    ->orWhere('orders.status', 'Alistado');
            })->get();
        }

        //$this->update_refund_status($order);

        $order_payments = OrderPayment::select('order_payments.id', 'order_payments.order_id',
            'order_payments.cc_payment_status', 'order_payments.cc_payment_description',
            'order_payments.cc_payment_transaction_id', 'order_payments.cc_charge_id',
            DB::raw('order_payment_refunds.status AS cc_refund_status'),
            DB::Raw('order_payment_refunds.date AS cc_refund_date'),
            'order_payments.cc_payment_date', 'order_payments.admin_id',
            DB::raw("IFNULL(admin.fullname, concat_WS(' ', pickers.first_name, pickers.last_name)) AS fullname"))
            ->where('order_id', $order->id)
            ->leftJoin('admin', 'admin.id', '=', 'order_payments.admin_id')
            ->leftJoin('pickers', 'pickers.id', '=', 'order_payments.picker_id')
            ->leftJoin('order_payment_refunds', function ($leftJoin) {
                $leftJoin->on('order_payment_refunds.order_payment_id', '=', 'order_payments.id');
                //->where('order_payment_refunds.status', '=', 'Aprobada');
                //->orwhere('order_payment_refunds.status', '=', 'Pending');
            })
            ->groupBy('order_payments.id')
            ->orderBy('order_payments.cc_payment_date', 'DESC')
            ->get();

        $last_order_payment = [];
        $last_refund = [];
        if (count($order_payments)) {
            foreach ($order_payments as $order_payment) {
                if ($order_payment->cc_payment_status == 'Aprobada' && empty($order_payment->cc_refund_date)) {
                    $last_order_payment = $order_payment;
                } elseif (!empty($order_payment->cc_refund_date)) {
                    $last_refund = $order_payment;
                }
            }
        }

        if ($order->payment_method == 'Tarjeta de crédito') {
            if (!empty($order->cc_bin)) {
                $order->cc_bin = $order->cc_bin;
            } else {
                if ($user_credit_card = UserCreditCard::find($order->credit_card_id)) {
                    $order->cc_bin = $user_credit_card->bin;
                } else {
                    $order->cc_bin = '';
                }
            }
        }
        $discounts = User::getDiscounts($order->user_id);
        $reasons = RejectReason::admin()->active()->whereNotNull('status')->groupBy('status')->orderBy('status',
            'ASC')->get();
        $reasons = $reasons->sortBy('status');

        $data = [
            'title' => 'Pedido',
            'order' => $order,
            'cities' => $cities,
            'order_log' => $order_log,
            'order_products' => $order_products,
            'order_notes' => $order_notes,
            'customer_orders' => $customer_orders,
            'customer_credits' => $customer_credits,
            'customer_credit_cards' => $customer_credit_cards,
            'customer_free_deliveries' => $customer_free_deliveries,
            'order_transporter' => $order_transporter,
            'routes' => $routes,
            'user_discounts' => $discounts,
            'order_payments' => $order_payments,
            'last_order_payment' => $last_order_payment,
            'last_refund' => $last_refund,
            'transporters' => $transporters,
            'pickers' => $pickers,
            'delivery_windows' => DeliveryWindow::getDeliveryWindowsByCity($order->orderGroup->warehouse->city_id),
            'delivery_windows_same_day' => DeliveryWindow::getDeliveryWindowsByCity($order->orderGroup->warehouse->city_id,
                true),
            'reasons' => $reasons,
            'user_promoter' => $user_promoter,
        ];

        return View::make('admin.orders_storage.details', $data);
    }

    /**
     * Consulta y actualiza el estado de los reembolsos
     * @param obj $order objeto que contiene la información de la orden
     */
    private function update_refund_status($order)
    {
        $refunds = OrderPayment::select('order_payments.*', 'order_payment_refunds.id AS refund_id',
            'order_payment_refunds.status as refund_status')
            ->where('order_id', $order->id)
            ->where('order_payment_refunds.status', 'Pendiente')
            ->join('order_payment_refunds', 'order_payment_refunds.order_payment_id', '=', 'order_payments.id')
            ->get();
        if (count($refunds) > 0) {
            $user_credit_card = UserCreditCard::find($order->credit_card_id);
            //PayU
            if (strlen($user_credit_card->card_token) == Config::get('app.payu.token_length')) {
                $payu = new PayU;
                foreach ($refunds as $refund) {
                    $response = $payu->getRefundResponse($refund->cc_charge_id, $order);

                    if ($response['status'] && $response['response']->code = 'SUCCESS') {
                        $payload = $response['response']->result->payload;
                        $transaction = $payload->transactions->transaction;
                        if (is_array($transaction)) {
                            $status = $payload->status;
                            $type = $transaction[0]->type;
                            $state = $transaction[0]->transactionResponse->state;
                        } else {
                            $status = $payload->status;
                            $type = $transaction->type;
                            $state = $transaction->transactionResponse->state;
                        }


                        $order_payment_refunds = OrderPaymentRefund::find($refund->refund_id);
                        if ($status == 'CAPTURED' && $state == 'APPROVED' && $type == 'AUTHORIZATION_AND_CAPTURE') {
                            $order_payment_refunds->status = 'Pendiente';
                        }
                        if ($status == 'CAPTURED' && $state == 'DECLINED' && $type == ($type == 'REFUND' || $type == 'VOID')) {
                            $order_payment_refunds->status = 'Declinada';
                        }
                        if (($status == 'CANCELLED' || $status == 'REFUNDED') && $state == 'APPROVED' && ($type == 'REFUND' || $type == 'VOID')) {
                            $order_payment_refunds->status = 'Aprobada';
                        }
                        $order_payment_refunds->save();
                    }
                }
            }
        }
    }

    /**
     * Realizar cobro del pedido a tarjeta de crédito
     */
    public function charge($id)
    {
        $message = 'Ocurrió un error al realizar el cobro a la tarjeta de crédito.';
        $type = 'failed';

        $order = Order::find($id);

        $payments = $order->getPayments(true);

        $order_products = OrderProduct::where('order_id', $order->id)->where('fulfilment_status', 'Pending')->count();
        if ($order_products) {
            return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'error')->with('message',
                'Debes asignar un estado a los productos.');
        }

        if ($order && in_array($order->status, array(
                'In Progress',
                'Alistado',
                'Dispatched',
                'Delivered'
            )) && (!$payments || empty($payments->cc_charge_id) || !empty($payments->cc_refund_date))) {
            $log = new OrderLog();
            $result = Order::chargeCreditCard($order->id, $order->user_id);
            $message = $result['message'];
            if ($result['status']) {
                $type = 'success';
                $log->type = 'Cobro a tarjeta exitoso.';
            } else {
                $log->type = $message;
            }

            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', $type)->with('message',
            $message);
    }

    /**
     * Realizar reembolso del pedido a tarjeta de crédito
     */
    public function refund($id)
    {
        $message = 'Ocurrió un error al hacer el reembolso a la tarjeta de crédito.';
        $type = 'failed';

        $order_payments = OrderPayment::find($id);
        $order = Order::find($order_payments->order_id);

        if ($order_payments && !empty($order_payments->cc_charge_id)) {
            $order_payment_refunds = new OrderPaymentRefund;
            $log = new OrderLog();
            $user_credit_card = UserCreditCard::find($order->credit_card_id);
            //Payu
            if (strlen($user_credit_card->card_token) == Config::get('app.payu.token_length')) {
                $payu = new PayU;
                $result = $payu->refundCreditCardCharge(Input::get('reason'), $order, $order_payments);

                if ($result['status']) {
                    if (isset($result['response']->result->payload->status) && $result['response']->result->payload->status == 'CANCELLED') {
                        $order_payment_refunds->status = 'Aprobada';
                    } else {
                        $order_payment_refunds->status = 'Pendiente';
                    }
                }

                $order_payment_refunds->order_payment_id = $order_payments->id;
                $order_payment_refunds->date = date('Y-m-d H:i:s');
                $order_payment_refunds->admin_id = Session::get('admin_id');
                $order_payment_refunds->reason = Input::get('reason');
                $order->payment_date = null;
            } else {
                $payment = new Payment;
                $result = $payment->refundCreditCardCharge($order->user_id, $order_payments->cc_charge_id, $order->id);
                if ($result['status']) {
                    $order_payment_refunds->order_payment_id = $order_payments->id;
                    $order_payment_refunds->status = 'Aprobada';
                    $order_payment_refunds->date = date('Y-m-d H:i:s');
                    $order_payment_refunds->admin_id = Session::get('admin_id');
                    $order_payment_refunds->reason = Input::get('reason');
                    $order->payment_date = null;
                    /*$order_payments->cc_charge_id = null;
                    $order_payment_refunds->cc_payment_transaction_id = null;
                    $order_payment_refunds->payment_date = null;*/
                }
            }

            $order_payment_refunds->save();

            if ($result['status']) {
                $order->save();

                //enviar mensaje de texto
                $order_group = OrderGroup::find($order->group_id);
                $message = 'Hemos reembolsado a tu tarjeta $ ' . number_format($order->total_amount + $order->delivery_amount - $order->discount_amount,
                        0, ',', '.') . ' del pago de tu pedido ' . $order->reference . ' en Merqueo.com';
                send_sms($order_group->user_phone, $message);

                $message = 'El reembolso a la tarjeta de crédito fue realizado con éxito.';
                $type = 'success';
                $log->type = 'Reembolso a tarjeta realizado con éxito.';
            } else {
                $log->type = 'Reembolso a tarjeta rechazado.';
            }

            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type', $type)->with('message',
            $message);
    }

    /**
     * Obtener razon de rechazo del pedido
     */
    public function get_reject_reason_ajax()
    {
        $reason = false;
        if (Input::has('status')) {
            $reason = RejectReason::active()
                ->admin()
                ->whereNotNull('status')
                ->where('status', Input::get('status'))
                ->orderBy('reason')
                ->get();
        }

        return Response::json($reason, 200);
    }

    /**
     * Actualizar estado de un pedido
     */
    public function update_status($id)
    {
        $order = Order::find($id);
        $order_group = OrderGroup::find($order->group_id);
        $payment = $order->getPayments(true);

        if (Input::get('status') == 'Alistado' && $order->status == 'Alistado') {
            \Log::info('Rollback en actualización de estado de pedido: '.$order->id.' - desde dash contenia estado a asignar');
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])
                ->with('type', 'failed')
                ->with('message', 'El pedido ya tiene el estado a asignar.');
        }

        if($order->status == 'Delivered' && $order->invoice_number) {
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])
                ->with('type', 'failed')
                ->with('message', 'El pedido se encuentra entregado y facturado, no puedes realizar esta acción.');
        }

        $reject_reason = '';
        if (Input::has('reject_reason')) {
            $reject_reason = strpos($order->reject_reason, Input::get('reject_reason'));
        }

        if ($order->status == Input::get('status') && $reject_reason) {
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])
                ->with('type', 'failed')
                ->with('message', 'El pedido ya tiene el estado a asignar.');
        }
        try {
            $order->validatePreviousStatus(Input::get('status'));
            $old_order_status = $order->status;
            $order->status = Input::get('status');
            DB::beginTransaction();

            // no se debe cambiar el estado del pedido a 'Iniciado' si pago mediante
            // PSE no ha sido realizado
            if (Input::get('status') == 'Initiated' && $order->payment_method == 'Débito - PSE' && $payment->cc_payment_status != 'Aprobada') {
                throw new Exception('Pago mediante PSE no ha sido realizado.');
            }

            if ($order->status == 'Enrutado') {
                if ($order->payment_method === 'Débito - PSE' && empty($order->payment_date)) {
                    return Redirect::back()->with('error',
                        'No se puede enrutar este pedido porque tiene método de pago PSE y no ha sido pagado.');
                }

                if (!$order->route_id || !$order->planning_sequence) {
                    throw new Exception('Se debe primero asignar el pedido a una ruta y secuencia.');
                }
                $order->planning_date = date('Y-m-d H:i:s');
            }

            if ($order->status == 'In Progress')
            {
                $order->validatePicker();
                $order->received_date = date('Y-m-d H:i:s');
            }

            if ($order->status == 'Alistado') {
                $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                    ['Pending', 'Missing'])->where('type', '<>', 'Recogida')->count();
                if ($order_products) {
                    throw new Exception('Hay productos pendientes en el pedido.');
                }

                // si pedido es pagado mediante PSE y tiene faltantes, le fijo
                // tipo de validación a 6 (Pagados con PSE y con faltantes),
                // pero sin cambiar el estado del pedido a validación para no
                // interrumpir el flujo normal de estados en este punto
                if ($order->payment_method === 'Débito - PSE' && $order->orderProducts()->where('fulfilment_status',
                        '!=', 'Fullfilled')->count() > 0) {
                    $this->sendNotificationAboutMissingProductsOnOrder($order);
                    $order->order_validation_reason_id = 6;
                    $order->save();
                }

                $order->updateTotals();
                $order->updatePickingStock();

                //cobrar pedido a tarjeta
                $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && empty($order->payment_date) &&
                    (
                        ($payment && $payment->cc_payment_status == 'Aprobada' && !empty($payment->cc_refund_date)) ||
                        ($payment && $payment->cc_payment_status != 'Aprobada' && empty($payment->cc_refund_date)) ||
                        !$payment
                    )) {
                    $result = Order::chargeCreditCard($order->id, $order->user_id);

                    if (!$result['status']) {
                        throw new PayUException($result['message']);
                    }

                    $log = new OrderLog();
                    $log->type = 'Cobro a tarjeta exitoso.';
                    $log->admin_id = Session::get('admin_id');
                    $log->order_id = $order->id;
                    $log->save();
                }

                $order->picking_baskets = Input::get('picking_baskets');
                $order->picking_bags = Input::get('picking_bags');
                $order->picking_date = date('Y-m-d H:i:s');

            }

            if ($order->status == 'Dispatched') {
                //validar que el pedido tenga un transportador
                if (!$order->vehicle_id) {
                    throw new Exception('Se debe primero asignar el pedido a un transportador.');
                }

                //validar que el pedido tenga alistador asignado
                $order->validatePicker();
                //validar que los productos no esten pendientes
                $order_products = OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                    ['Pending', 'Missing'])->where('type', '<>', 'Recogida')->count();
                if ($order_products) {
                    throw new Exception('Hay productos pendientes o faltantes en el pedido.');
                }

                $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;

                if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && (!$payment || empty($payment->cc_charge_id) || !empty($payment->cc_refund_date))) {
                    throw new Exception('Se debe realizar primero el cobro a la tarjeta de crédito.');
                }

                $order->dispatched_date = date('Y-m-d H:i:s');

                //registrar salida de bodega
                $order_products = OrderProduct::where('order_id', $order->id)->where('fulfilment_status',
                    'Fullfilled')->get();
                if (!$order_products) {
                    throw new Exception('No se puede despachar por que no hay productos disponibles en el pedido.');
                }
                //$order->updatePickedStock();
            }

            if ($order->status == 'Delivered') {
                $order->management_date = date('Y-m-d H:i:s');
                //actualizar estado de productos
                OrderProduct::where('order_id', $order->id)->where('fulfilment_status',
                    'Pending')->update(array('fulfilment_status' => 'Fullfilled'));

                if ($order->payment_method != 'Tarjeta de crédito' && $order->payment_method != 'Débito - PSE') {
                    $order->updateTotals();

                    if (!Input::has('order_was_paid') || (Input::has('order_was_paid') && Input::get('order_was_paid'))) {
                        $order->payment_date = date('Y-m-d H:i:s');
                    }

                    $customer_payment_method = Input::get('customer_payment_method');
                    if (empty($customer_payment_method)) {
                        throw new Exception('El método de pago del cliente es requerido.');
                    }

                    if ($order->payment_method != Input::get('customer_payment_method')) {
                        $order->payment_method = $customer_payment_method;

                        $log = new OrderLog();
                        $log->type = 'Método de pago actualizado: ' . $order->payment_method . ' a ' . $customer_payment_method;
                        $log->admin_id = Session::get('admin_id');
                        $log->order_id = $order->id;
                        $log->save();
                    }
                } else {
                    if (!$payment || empty($payment->cc_charge_id) || !empty($payment->cc_refund_date)) {
                        throw new Exception('El método de pago del cliente es con tarjeta de crédito o PSE y no se ha cobrado.');
                    }
                }

                //registrar movimiento de conductor
                if (VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('status', 1)->first()) {
                    $movement = VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('order_id',
                        $order->id)->where('status', 1)->first();
                    if (empty($movement)) {
                        $movement = new VehicleMovement;
                    }
                    $movement->order_id = $order->id;
                    $movement->vehicle_id = $order->vehicle_id;
                    $movement->driver_id = $order->driver_id;
                    $movement->movement = 'Gestión de pedido';
                    $movement->payment_method = $order->payment_method;
                    $total_order = round($order->total_amount + $order->delivery_amount - $order->discount_amount);

                    if ($order->payment_method == 'Efectivo y datáfono') {
                        $movement->user_card_paid = Input::get('customer_card_paid');
                        $movement->user_cash_paid = Input::get('customer_cash_paid');
                        if ($total_order != (Input::get('customer_cash_paid') + Input::get('customer_card_paid'))) {
                            throw new Exception('El total pagado en efectivo y con datáfono no coincide con el total del pedido/factura.');
                        }
                    }

                    if ($order->payment_method == 'Datáfono') {
                        $movement->user_card_paid = $total_order;
                        $movement->user_cash_paid = 0;
                    }

                    if ($order->payment_method == 'Tarjeta de crédito') {
                        $movement->user_card_paid = $total_order;
                        $movement->user_cash_paid = 0;
                        VehicleMovement::saveMovement($movement, $order);
                    }

                    if ((!Input::has('order_was_paid') || Input::get('order_was_paid')) && $order->payment_method != 'Tarjeta de crédito') {
                        VehicleMovement::saveMovement($movement, $order);
                    }
                }

                //eliminar devolucion a bodega si aplica
                OrderReturn::deleteOrderReturn($order->id, $order->status);
                //registrar devolucion a bodega si aplica
                OrderReturn::saveOrderReturn($order->id, $old_order_status);

                // Alertas para vehiculos
                $vehicle = Vehicle::find($order->vehicle_id);
                $vehicle->onDeliveredOrder($order);

                $order->updateInvestigationStockBack($order->status);

                // Genera consecutivo de factura
                $order->generateConsecutive();
            }

            if ($order->status == 'Cancelled') {
                $order->management_date = date('Y-m-d H:i:s');
                $order->reject_reason = Input::get('reject_reason');

                if (empty($order->payment_date)) {
                    $order->order_validation_reason_id = null;
                }

                // puede que la transacción de pago mediante tarjeta haya sido
                // exitoso, pero por algún motivo se haya cambiado el estado de
                // pago a "no pagado", no se deben cancelar pedidos ya pagados,
                // excepto los pagados mediante PSE
                if ($order->payment_method !== 'Débito - PSE' && str_contains($order->reject_reason,
                        "Cancelar") && $order->lastPaymentByCardWasSucceed() && !empty($order->payment_date)) {
                    throw new Exception('El total del pedido ya fue cobrado a la tarjeta.');
                }

                if ($order->reject_reason == 'Pausar' || $order->reject_reason == 'Rescue' || $order->reject_reason == 'Reprogramar') {
                    $order->temporarily_cancelled = 1;
                } else {
                    $order->temporarily_cancelled = 0;
                }

                if ($order->reject_reason == 'Reprogramar') {
                    $order->scheduled_delivery = 1;
                }

                $order->reject_reason .= ' - ' . Input::get('reject_reason_futher');
                $reject_reason = Input::get('reject_reason');
                $further_reject_reason = Input::get('reject_reason_futher');

                if (empty($order->reject_comments)) {
                    $order->reject_comments = Input::has('reject_comments') ? Input::get('reject_comments') : null;
                } else {
                    $order->reject_comments = Input::has('reject_comments') ? $order->reject_comments . " - " . Input::get('reject_comments') : $order->reject_comments;
                }

                // TODO: Cuando el cliente no se encuentra se sube imagen de la fachada de la dirección.
                if (in_array($reject_reason, ['Pausar', 'Rescue']) && in_array($further_reject_reason,
                        ['Fuera de franja programada', 'No se encuentra usuario en franja horaria'])) {
                    $cloudfront_url = Config::get('app.aws.cloudfront_url');
                    if (Input::hasFile('arrival_image_url')) {
                        $image = Input::file('arrival_image_url');
                        $validator = Validator::make(
                            array('ima' => $image),
                            array('ima' => 'required|mimes:jpeg,bmp,png,gif')
                        );
                        if ($validator->fails()) {
                            return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');
                        }

                        if (!empty($order->arrival_image_url)) {
                            $path = str_replace($cloudfront_url, uploads_path(), $order->arrival_image_url);
                            remove_file($path);
                            remove_file_s3($order->arrival_image_url);
                        }
                        $order->arrival_image_url = upload_image($image, false, 'orders/arrivals/' . date('Y-m-d'));
                    }
                }

                if (!in_array($old_order_status, ['Dispatched', 'Delivered'])) {
                    $order->product_return_storage = 0;
                    $order_product = OrderProduct::where('order_id', $order->id)->where('type', 'Muestra')->first();
                    if ($order_product) {
                        if ($sampling = Sampling::where('store_product_id',
                            $order_product->store_product_id)->where('warehouse_id',
                            $order_group->warehouse_id)->first()) {
                            $sampling->quantity += 1;
                            $sampling->save();
                        }
                    }
                    if ($old_order_status == 'Alistado') {
                        $order->updateInvestigationStockBack($old_order_status);
                    }
                } else {
                    //eliminar devolucion a bodega si aplica
                    OrderReturn::deleteOrderReturn($order->id, $order->status);

                    if ($old_order_status == 'Dispatched') {
                        $order->updateInvestigationStockBack($old_order_status);
                    }
                }

                //registrar devolucion a bodega
                if (($reject_reason == 'Cancelar' || $reject_reason == 'Rescue') && !empty($order->dispatched_date)) {
                    //eliminar devolucion a bodega si aplica
                    $order_return_deleted = OrderReturn::deleteOrderReturn($order->id, $order->status);
                    //actualizar estado de productos
                    OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                        ['Fullfilled', 'Returned'])->update(array('fulfilment_status' => 'Returned'));
                    OrderProductGroup::where('order_id', $order->id)->whereIn('fulfilment_status',
                        ['Fullfilled', 'Returned'])->update(array('fulfilment_status' => 'Returned'));
                    $order->product_return_storage = 1;
                    if ($order_return_deleted) {
                        OrderReturn::saveOrderReturn($order->id, $old_order_status);
                    }
                } else {
                    if ($reject_reason == 'Pausar' && !empty($order->dispatched_date)) {
                        //eliminar devolucion a bodega si aplica
                        $order_return_deleted = OrderReturn::deleteOrderReturn($order->id, $order->status);
                        //actualizar estado de productos
                        OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                            ['Fullfilled', 'Returned'])->update(array('fulfilment_status' => 'Returned'));
                        OrderProductGroup::where('order_id', $order->id)->whereIn('fulfilment_status',
                            ['Fullfilled', 'Returned'])->update(array('fulfilment_status' => 'Returned'));
                        $order->product_return_storage = 1;
                        if ($order_return_deleted) {
                            OrderReturn::saveOrderReturn($order->id, $old_order_status);
                        }
                    } else {
                        $order->product_return_storage = 0;
                    }
                }

                //retornar credito a cuenta de usuario
                if (UserCredit::validateReturnCredit($order, $reject_reason, $further_reject_reason)) {
                    UserCredit::returnCredit($order);
                }

                //eliminar registro de campaña de marca
                UserBrandCampaign::where('order_id', $order->id)->delete();

                if ($movement = VehicleMovement::where('order_id', $order->id)->where('status', 1)->first()) {
                    $driver_balance = 0;
                    $description = 'Pedido actualizado a Cancelado ';
                    //con devolucion
                    if ($order->product_return_storage) {
                        $description .= 'con devolución';
                    } else {
                        $description .= 'sin devolución';
                    }
                    $description .= ' desde el administrador.';
                    $movement->description .= $description;
                    $movement->status = 0;
                    $movement->save();
                }

                //enviar push a app
                if (!empty($order->vehicle_id) && $old_order_status == 'Dispatched') {
                    $order_group = OrderGroup::find($order->group_id);
                    if ($route = Routes::find($order->route_id)) {
                        $data = [
                            'order_id' => $order->id,
                            'user_address' => $order_group->user_address,
                            'route' => $route->route,
                            'planning_sequence' => $order->planning_sequence,
                        ];
                        Pusherer::send_push('private-driver-' . $order->vehicle_id, 'order-cancelled', $data,
                            'transporter');
                    }
                }

                if ((!empty($order->picker_dry_id) || !empty($order->picker_cold_id))  && in_array($old_order_status, ['In Progress', 'Enrutado', 'Initiated'])) {
                    $data = $order->id;
                    Pusherer::send_push( 'private-picker-'.$order->picker_dry_id, 'order-cancelled', $data, 'picker' );
                    Pusherer::send_push( 'private-picker-'.$order->picker_cold_id, 'order-cancelled', $data, 'picker' );
                }
            }
            $order->save();
            DB::commit();

            if ($order->status == 'Cancelled') {
                Event::fire('order.reject_log',
                    [$order->id, Input::get('reject_reason_futher_id'), Session::get('admin_id')]);

                // Se eliminan los registros de user_coupon ya que la orden se canceló
                if( Input::has('reject_reason') && Input::get('reject_reason') == 'Cancelar' ){
                    UserCredit::removeRedeemedCoupons($order);
                }
            }
            Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);
            \Log::info('Modificando estado de pedido: '.$order->id. ' a '.$order->status.' desde dashboard');
            if ($order->status == 'Delivered') {
                $order->validateReferred();
            }

        } catch (PayUException $e) {
            DB::rollback();
            $log = new OrderLog();
            $log->type = $e->getMessage();
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'failed')->with('message', $e->getMessage());

        } catch (\exceptions\PseOrderStatusUpdateRulesException $e) {
            DB::rollback();

            // 6 = Pagados con PSE y con faltantes
            $order->order_validation_reason_id = 6;
            $order->status = \orders\OrderStatus::VALIDATION;
            $order->save();

            \Log::info('Rollback en actualización de estado de pedido: '.$order->status.' - desde  Pagados con PSE y con faltantes');
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'failed')->with('message', $e->getMessage());
        } catch (QueryException $e) {
            DB::rollBack();
            if ($e->getCode() == '40001') {
                ErrorLog::add($e, 513);
                \Log::info('Rollback en actualización de estado de pedido: '.$order->status.' - desde  actualizacioón de inventarios');
                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type', 'failed')
                    ->with('message', 'Otro proceso se está ejecutando sobre estos productos, 
            espera un momento para volver a intentarlo.');
            }
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type', 'failed')->with('message',
                $e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            \Log::info('Rollback en actualización de estado de pedido: '.$order->id.' - desde dash');
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'failed')->with('message', $e->getMessage()/*.' '.$e->getFile().'  '.$e->getLine()*/);
        }

        if ($order->status == 'Delivered' || $order->status == 'Cancelled') {
            Event::fire('order.managed', [[$order, $order_group]]);
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'success')->with('message',
            'Pedido actualizado con éxito.');
    }

    /**
     * Actualizar estado de pedido
     */
    public function update_status_admin($id, $status = null)
    {
        $message = 'Ocurrió un error al actualizar el estado.';
        $type = 'failed';

        if ($id) {
            if ($order = Order::find($id)) {
                if ($order->status == 'Delivered' && $order->invoice_number) {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])
                        ->with('type', 'failed')
                        ->with('message',
                            'El pedido se encuentra entregado y facturado, no puedes realizar esta acción.');
                }

                if (Input::get('status') == 'Alistado' && $order->status == 'Alistado') {
                    \Log::info('Rollback en actualización de estado de pedido: '.$order->id.' - contenia estado a asignar ejecutada desde funcion update_status_admin');
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', 'El pedido ya tiene el estado a asignar.');
                }

                try {
                    DB::beginTransaction();

                    $status = Input::has('status') ? Input::get('status') : $status;

                    if (!$status) {
                        return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                            'failed')->with('message', 'Estado es requerido.');
                    }

                    $reject_reason = '';
                    if (Input::has('reject_reason')) {
                        $reject_reason = strpos($order->reject_reason, Input::get('reject_reason'));
                    }

                    // no se debe cancelar pedidos que ya están pagados
                    if ($status == 'Cancelar' && $order->lastPaymentByCardWasSucceed()) {
                        return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                            'failed')->with('message', 'El total del pedido ya fue cobrado a la tarjeta.');
                    }

                    if ($order->status == $status && $reject_reason) {
                        return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                            'failed')->with('message', 'El pedido ya tiene el estado a asignar.');
                    }

                    $old_order_status = $order->status;
                    $new_status = $status;
                    $picker_dry_id = $order->picker_dry_id;

                    if ($new_status == 'Validation' || $new_status == 'Initiated' || $new_status == 'Enrutado' || $new_status == 'In Progress') {
                        if ($order->status == 'Dispatched' || $order->status == 'Delivered') {
                            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                'failed')->with('message',
                                'Se debe cancelar el pedido para generar la devolución a bodega.');
                        }

                        if ($order->status == 'Alistado') {
                            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                'failed')->with('message', 'Se debe cancelar el pedido.');
                        }

                        if ($new_status == 'Validation') {
                            if ($order->payment_method != 'Tarjeta de crédito') {
                                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                    'failed')->with('message', 'El método de pago debe ser Tarjeta de crédito.');
                            }
                        }
                        if ($new_status == 'In Progress') {
                            if (empty($order->picker_dry_id)) {
                                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                    'failed')->with('message', 'Se debe asignar un alistador al pedido primero.');
                            }
                            if (!empty($order->picking_date)) {
                                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                    'failed')->with('message', 'Se debe cancelar el pedido.');
                            }
                        }
                        if ($order->status == 'Cancelled') {
                            $order_product = OrderProduct::with('order.orderGroup')->where('order_id',
                                $order->id)->where('type', 'Muestra')->first();
                            if ($order_product) {
                                if ($sampling = Sampling::where('store_product_id',
                                    $order_product->store_product_id)->where('warehouse_id',
                                    $order_product->order->orderGroup->warehouse_id)->first()) {
                                    $sampling->quantity -= 1;
                                    $sampling->save();
                                }
                            }
                        }
                        if ($new_status == 'Validation' || $new_status == 'Initiated') {
                            $order->allow_early_delivery = 0;
                            $order->planning_date = null;
                            $order->allocated_date = null;
                            $order->received_date = null;
                            $order->picking_dry_start_date = null;
                            $order->picking_dry_end_date = null;
                            $order->picking_cold_start_date = null;
                            $order->picking_cold_end_date = null;
                            $order->picking_date = null;
                            $order->picking_baskets = null;
                            $order->picking_bags = null;
                            $order->dispatched_date = null;
                            $order->onmyway_date = null;
                            $order->ontheway_notification_date = null;
                            $order->arrived_date = null;
                            $order->picker_dry_id = null;
                            $order->picker_cold_id = null;
                            $order->driver_id = null;
                            $order->vehicle_id = null;
                            $order->route_id = null;
                            $order->planning_route = null;
                            $order->planning_sequence = null;
                            $order->planning_duration = null;
                            $order->planning_distance = null;
                            OrderProduct::where('order_id', $order->id)->where('is_gift', 1)->delete();
                        }
                        if ($new_status == 'Enrutado') {
                            $order->allocated_date = null;
                            $order->received_date = null;
                            $order->picking_dry_start_date = null;
                            $order->picking_dry_end_date = null;
                            $order->picking_cold_start_date = null;
                            $order->picking_cold_end_date = null;
                            $order->picking_date = null;
                            $order->picking_baskets = null;
                            $order->picking_bags = null;
                            $order->dispatched_date = null;
                            $order->onmyway_date = null;
                            $order->ontheway_notification_date = null;
                            $order->arrived_date = null;
                            $order->picker_dry_id = null;
                            $order->picker_cold_id = null;
                            $order->driver_id = null;
                            $order->vehicle_id = null;
                            OrderProduct::where('order_id', $order->id)->where('is_gift', 1)->delete();
                        }
                        if ($new_status == 'In Progress') {
                            $order->picking_date = null;
                            $order->picking_baskets = null;
                            $order->picking_bags = null;
                            if (empty($order->allocated_date)) {
                                $order->allocated_date = date('Y-m-d H:i:s');
                            }
                            if (empty($order->received_date)) {
                                $order->received_date = date('Y-m-d H:i:s');
                            }
                            OrderProduct::where('order_id', $order->id)->where('is_gift', 1)->delete();
                        }

                        $order->pending_payment_response = 0;
                        $order->management_date = null;
                        $order->reject_reason = null;
                        $order->temporarily_cancelled = 0;
                        $order->product_return_storage = 0;
                        $order->driver_cancel_date = null;
                        $order->reject_comments = null;

                        if ($new_status != 'Alistado' && $old_order_status != 'Cancelled') {
                            $cloudfront_url = Config::get('app.aws.cloudfront_url');
                            if (!empty($order->voucher_image_url)) {
                                $path = str_replace($cloudfront_url, uploads_path(), $order->voucher_image_url);
                                remove_file($path);
                                remove_file_s3($order->voucher_image_url);
                            }
                            $order->voucher_image_url = null;
                            OrderProduct::where('order_id',
                                $order->id)->update(array('fulfilment_status' => 'Pending'));
                            OrderProductGroup::where('order_id',
                                $order->id)->update(array('fulfilment_status' => 'Pending'));
                        }

                        //crear nuevo pedido cuando se cancela
                        $child_orders = Order::where('parent_order_id', $order->id)->count();
                        if ($old_order_status == 'Cancelled' && $status != 'Cancelled' && $child_orders == 0) {
                            $order_group = OrderGroup::find($order->group_id);
                            $new_order_group = $order_group->replicate();
                            $new_order_group->warehouse_id = $order_group->zone->warehouse_id;
                            $new_order_group->save();
                            $new_order = $order->replicate();
                            $new_order->status = $status;
                            $new_order->group_id = $new_order_group->id;
                            $new_order->order_validation_reason_id = null;
                            $new_order->payment_date = $order->payment_date;
                            $new_order->parent_order_id = $order->id;
                            $new_order->reference = generate_reference();
                            $new_order->invoice_number = null;
                            $new_order->invoice_resolution = null;
                            $new_order->invoice_date = null;
                            $new_order->invoice_issuance_date = null;
                            $new_order->invoice_sent = null;
                            $new_order->credit_note_number = null;
                            $new_order->credit_note_date = null;
                            $new_order->save();

                            // replicamos transacciones de pago realizadas en orden antigua a nueva orden
                            foreach ($order->orderPayments as $payment) {
                                $replicatedPayment = $payment->replicate();
                                $replicatedPayment->order_id = $new_order->id;
                                $replicatedPayment->save();
                            }

                            $order = Order::find($id);
                            $order->is_hidden = 1;
                            $order->child_order_id = $new_order->id;
                            $order->temporarily_cancelled = 0;
                            $order->reject_reason = str_replace('Cancelado temporalmente', 'Reprogramación',
                                $order->reject_reason);
                            $order->reject_reason = str_replace('Cancelado definitivamente', 'Reprogramación',
                                $order->reject_reason);
                            $order->reject_reason = str_replace('Pausar', 'Cancelado definitivamente',
                                $order->reject_reason);
                            $order->reject_reason = str_replace('Rescue', 'Cancelado definitivamente',
                                $order->reject_reason);
                            $order->save();

                            $order_products = OrderProduct::where('order_id', $order->id)->get();
                            foreach ($order_products as $key => $order_product) {
                                // si pedido fue pagado mediante PSE, los productos/grupos de productos no disponibles son ignorados
                                if ($order->payment_method === 'Débito - PSE' && $order_product->fulfilment_status === 'Not Available') {
                                    continue;
                                }

                                $new_order_product = $order_product->replicate();
                                $new_order_product->order_id = $new_order->id;
                                $new_order_product->fulfilment_status = 'Pending';
                                $new_order_product->reason_credit_note = null;
                                $new_order_product->quantity_credit_note = null;
                                $new_order_product->save();
                                OrderProduct::where('order_id', $new_order->id)->where('is_gift', 1)->delete();

                                if ($new_order_product->type == 'Agrupado') {
                                    $order_product_groups = OrderProductGroup::where('order_id',
                                        $order->id)->where('order_product_id', $order_product->id)->get();
                                    if (count($order_product_groups)) {
                                        foreach ($order_product_groups as $key => $order_product_group) {
                                            $new_order_product_group = $order_product_group->replicate();
                                            $new_order_product_group->id = null;
                                            $new_order_product_group->order_id = $new_order->id;
                                            $new_order_product_group->fulfilment_status = 'Pending';
                                            $new_order_product_group->order_product_id = $new_order_product->id;
                                            $new_order_product_group->reason_credit_note = null;
                                            $new_order_product_group->quantity_credit_note = null;
                                            $new_order_product_group->save();
                                        }
                                    }
                                }
                            }

                            $new_order->updateTotals();
                            $new_order->save();

                            //log de pedido
                            $log = new OrderLog();
                            $log->type = 'Pedido hijo creado #' . $new_order->id;
                            $log->admin_id = Session::get('admin_id');
                            $log->order_id = $order->id;
                            $log->save();

                            $log = new OrderLog();
                            $log->type = 'Pedido creado a traves de padre #' . $order->id;
                            $log->admin_id = Session::get('admin_id');
                            $log->order_id = $new_order->id;
                            $log->save();

                            $order_notes = OrderNoteSms::where('order_id', $order->id)->get();
                            if (count($order_notes)) {
                                foreach ($order_notes as $key => $order_note) {
                                    $new_order_note = $order_note->replicate();
                                    $new_order_note->order_id = $new_order->id;
                                    $new_order_note->created_at = $order_note->created_at;
                                    $new_order_note->updated_at = $order_note->updated_at;
                                    $new_order_note->save(['timestamps' => false]);
                                }
                            }
                            DB::commit();

                            return Redirect::route('adminOrderStorage.details', ['id' => $new_order->id])->with('type',
                                'success')->with('message',
                                'Se ha creado un pedido nuevo con base al que estaba cancelado.');
                        }
                    }

                    if ($new_status == 'Cancelled') {
                        $order_product = OrderProduct::with('order.orderGroup')->where('order_id',
                            $order->id)->where('type', 'Muestra')->first();
                        if ($order_product) {
                            if ($sampling = Sampling::where('store_product_id',
                                $order_product->store_product_id)->where('warehouse_id',
                                $order_product->order->orderGroup->warehouse_id)->first()) {
                                $sampling->quantity += 1;
                                $sampling->save();
                            }
                        }
                        $order->management_date = date('Y-m-d H:i:s');
                        $order->reject_reason = Input::get('reject_reason');
                        $order->reject_comments = Input::has('reject_comments') ? Input::get('reject_comments') : null;

                        if ($order->reject_reason == 'Cancelado temporalmente' || $order->reject_reason == 'Reprogramación' || $order->reject_reason == 'Pausar') {
                            $order->temporarily_cancelled = 1;
                        } else {
                            $order->temporarily_cancelled = 0;
                        }

                        $order->reject_reason .= ' - ' . Input::get('reject_reason_futher');

                        if ($order->reject_reason == 'Reprogramación') {
                            $order->scheduled_delivery = 1;
                        }

                        if (!in_array($order->status, ['Dispatched', 'Delivered'])) {
                            $order->product_return_storage = 0;
                        } else {
                            //actualizar estado de productos
                            OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                                ['Fullfilled', 'Returned'])->update(array('fulfilment_status' => 'Returned'));
                            $order->product_return_storage = 1;
                            //registrar devolucion a bodega
                            $result = OrderReturn::saveOrderReturn($order->id, $old_order_status);
                            if (!$result['status']) {
                                throw new Exception($result['message']);
                            }
                        }

                        //retornar credito a cuenta de usuario
                        if (UserCredit::validateReturnCredit($order, Input::get('reject_reason'),
                            Input::get('reject_reason_futher'))) {
                            UserCredit::returnCredit($order);
                        }

                        //eliminar registro de campaña de marca
                        UserBrandCampaign::where('order_id', $order->id)->delete();

                        //enviar push a app
                        $order_group = OrderGroup::find($order->group_id);
                        $data = [
                            'order_id' => $order->id,
                            'user_address' => $order_group->user_address,
                            'planning_route' => $order->planning_route,
                            'planning_sequence' => $order->planning_sequence,
                            'order_id' => $order->id,
                        ];
                        if (!empty($order->vehicle_id) && $order->status == 'Dispatched') {
                            Pusherer::send_push('private-driver-' . $order->vehicle_id, 'order-cancelled', $data,
                                'transporter');
                        }
                        if ((!empty($order->picker_dry_id) || !empty($order->picker_cold_id)) && $order->status == 'In Progress') {
                            Pusherer::send_push('private-picker-' . $order->picker_dry_id, 'order-cancelled', $data,
                                'picker');
                            Pusherer::send_push('private-picker-' . $order->picker_cold_id, 'order-cancelled', $data,
                                'picker');
                        }
                    }

                    $order->status = $new_status;

                    if ($old_order_status == 'Alistado') {
                        $order->updateInvestigationStockBack($old_order_status);
                    }

                    $order->updateTotals();

                    $order->save();

                    if ($movement = VehicleMovement::where('order_id', $order->id)->where('status', 1)->first()) {
                        if ($order->status == 'Cancelled') {
                            $movement->status = 0;
                            $movement->save();

                            $new_movement = new VehicleMovement;
                            $new_movement->order_id = $order->id;
                            $new_movement->vehicle_id = $order->vehicle_id;
                            $new_movement->driver_id = $order->driver_id;
                            $new_movement->admin_id = Session::get('admin_id');
                            $new_movement->movement = 'Gestión de pedido';
                            $new_movement->status = 0;

                            $driver_balance = 0;
                            $description = 'Pedido actualizado a Cancelado ';

                            if ($order->product_return_storage) {
                                $description .= 'con devolución';
                            }
                            if (!$order->product_return_storage) {
                                $description .= 'sin devolución';
                            }

                            $new_movement->driver_balance = $driver_balance;
                            $description .= ' desde el administrador.';
                            $new_movement->save();

                        } else {
                            VehicleMovement::where('order_id', $order->id)->where('status', 1)->update(array(
                                'description' => 'Pedido actualizado a ' . $order->status . ' desde el Administrador.',
                                'status' => 0
                            ));
                        }
                    }

                    Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);
                    Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);
                    \Log::info('Modificando estado de pedido: '.$order->id. ' a '.$order->status.' desde dashboard metodo update_status_admin');

                    $message = 'Estado de pedido actualizado con éxito.';
                    $type = 'success';
                    DB::commit();
                } catch (QueryException $e) {
                    DB::rollBack();
                    if ($e->getCode() == '40001') {
                        ErrorLog::add($e, 513);
                        \Log::info('Rollback en actualización de estado de pedido: '.$id.' - desde dash update_status_admin cuando se ejecuta actualizacion de inventarios');
                        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                            'failed')->with('message', 'Otro proceso se está ejecutando sobre estos productos, 
            espera un momento para volver a intentarlo.');
                    }
                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        'failed')->with('message',
                        $e->getMessage());
                } catch (\Exception $e) {
                    DB::rollback();
                    \Log::info('Rollback en actualización de estado de pedido: '.$id.' - desde dash update_status_admin');
                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        'failed')->with('message',
                        $e->getMessage());
                }
            }
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', $type)->with('message',
            $message);
    }

    /**
     * Reversar estado de pedido gestionado a despachado
     */
    public function reverse_status($id)
    {
        $message = 'Ocurrió un error al actualizar el estado.';
        $type = 'failed';

        if ($id) {
            try {
                DB::beginTransaction();

                $order = Order::where('orders.id', $id)
                    ->where(function ($query) {
                        $query->where('orders.status', 'Cancelled');
                        $query->orWhere('orders.status', 'Delivered');
                    })
                    ->whereNull('child_order_id')
                    ->leftJoin('order_returns', function ($join) {
                        $join->on('order_returns.order_id', '=', 'orders.id');
                        $join->where('order_returns.old_order_status', '=', 'Dispatched');
                        $join->where('order_returns.status', '=', 'Pendiente');
                    })
                    ->select(DB::raw('orders.*, order_returns.id AS order_return_id,
                    order_returns.status AS order_return, order_returns.old_order_status'))
                    ->first();

                if ($order->status == 'Delivered' && $order->invoice_number) {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])
                        ->with('type', 'failed')
                        ->with('message', 'El pedido se encuentra entregado y facturado, no puedes realizar esta acción.');
                }

                if ($order)
                {
                    //reprogramar pedido
                    if (Input::has('reschedule_delivery_day')) {
                        //para el mismo dia
                        if (Input::get('reschedule_delivery_day') == 'today' && $order->temporarily_cancelled) {
                            $delivery_day = date('Y-m-d');
                            $delivery_window_id = Input::get('reschedule_delivery_window_id');

                            //valida que todas las tiendas tengan cobertura en la dirección y obtener deliveryWindow
                            $delivery_window = $order->store->validateStoreDeliversToAddress(
                                $order->orderGroup->user_address_latitude,
                                $order->orderGroup->user_address_longitude,
                                new \Carbon\Carbon($delivery_day),
                                $delivery_window_id
                            );

                            $log_type = 'Reprogramado: ' . format_date('normal_with_time',
                                    $order->delivery_date) . ' a ' . format_date('normal_with_time',
                                    $delivery_day . ' ' . $delivery_window->hour_end);
                            $order->real_delivery_date = $order->delivery_date = $delivery_day . ' ' . $delivery_window->hour_end;
                            $order->delivery_time = $delivery_window->delivery_window;
                            $order->scheduled_delivery = 1;
                            $order->save();

                            $log = new OrderLog();
                            $log->type = $log_type;
                            $log->admin_id = Session::get('admin_id');
                            $log->order_id = $order->id;
                            $log->save();

                            //eliminar devolucion
                            if ($order->order_return_id) {
                                OrderReturnDetail::where('order_return_id', $order->order_return_id)->delete();

                                OrderReturn::where('order_id', $order->id)
                                    ->where('order_returns.status', 'Pendiente')
                                    ->where('order_returns.old_order_status', 'Dispatched')
                                    ->delete();

                                $log = new OrderLog();
                                $log->type = 'Devolución a bodega # ' . $order->order_return_id . ' eliminada.';
                                $log->admin_id = Session::get('admin_id');
                                $log->order_id = $order->id;
                                $log->save();

                                OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                                    ['Fullfilled', 'Returned'])->update(['fulfilment_status' => 'Fullfilled']);
                                OrderProductGroup::where('order_id', $order->id)->whereIn('fulfilment_status',
                                    ['Fullfilled', 'Returned'])->update(['fulfilment_status' => 'Fullfilled']);
                                $order->product_return_storage = 1;
                                $order->save();
                            }
                        }
                        //para otro dia
                        if (Input::get('reschedule_delivery_day') == 'tomorrow') {
                            if (!empty($order->dispatched_date)) {
                                //eliminar devolucion a bodega si aplica
                                $returnOrder = OrderReturn::where('order_id', $order->id)->first();
                                if (empty($returnOrder)) {
                                    OrderReturn::deleteOrderReturn($order->id, $order->status);
                                    //actualizar estado de productos
                                    OrderProduct::where('order_id', $order->id)->whereIn('fulfilment_status',
                                        ['Fullfilled', 'Returned'])
                                        ->update(array('fulfilment_status' => 'Returned'));
                                    OrderProductGroup::where('order_id', $order->id)->whereIn('fulfilment_status',
                                        ['Fullfilled', 'Returned'])
                                        ->update(array('fulfilment_status' => 'Returned'));
                                    $order->product_return_storage = 1;
                                    $order->save();
                                    $result = OrderReturn::saveOrderReturn($order->id, $order->status);
                                    if (!$result['status']) {
                                        throw new Exception($result['message']);
                                    }
                                }
                            }
                            DB::commit();
                            return Redirect::route('adminOrderStorage.updateStatusAdmin',
                                ['id' => $id, 'status' => 'Initiated']);
                        }
                    }

                    //eliminar calificacion
                    if (!empty($order->user_score_date)) {
                        OrderProductScore::join('order_products', 'order_products.id', '=',
                            'order_product_scores.order_product_id')
                            ->where('order_products.order_id', $order->order_id)->delete();
                    }

                    if ($order->status == 'Delivered') {
                        $vehicle_movement = VehicleMovement::where('order_id', $order->id)->first();
                        if ($vehicle_movement) {
                            $vehicle_movement->status = 0;
                            $vehicle_movement->description = 'Movimiento de pedido anulado por cambio de estado de Delivered a Dispatched';
                            $vehicle_movement->save();
                        }
                    }

                    OrderProduct::where('order_id', $order->id)->where('fulfilment_status',
                        'Returned')->update(array('fulfilment_status' => 'Fullfilled'));
                    OrderProductGroup::where('order_id', $order->id)->where('fulfilment_status',
                        'Returned')->update(array('fulfilment_status' => 'Fullfilled'));

                    $order = Order::find($order->id);
                    if (empty($order->dispatched_date)) {
                        $order->status = 'Alistado';
                        $order->updatePickingStock();
                    } else {
                        $order->status = 'Dispatched';
                        $order->updateInvestigationStockBack($order->status);
                    }

                    $order->ontheway_notification_date = null;
                    $order->onmyway_date = null;
                    $order->arrived_date = null;
                    $order->estimated_arrival_date = null;
                    $order->reject_reason = null;
                    $order->reject_comments = null;
                    $order->temporarily_cancelled = 0;
                    $order->product_return_storage = 0;
                    $order->user_score = null;
                    $order->user_score_source = null;
                    $order->user_score_typification = null;
                    $order->user_score_comments = null;
                    $order->user_score_date = null;

                    $order->updateTotals();
                    Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                    $order->save();
                    Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);

                    $message = 'Estado de pedido actualizado con éxito.';
                    $type = 'success';
                }

                DB::commit();

            } catch (QueryException $e) {
                DB::rollBack();
                if ($e->getCode() == '40001') {
                    ErrorLog::add($e, 513);
                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        $type)->with('message', 'Otro proceso se está ejecutando sobre estos productos, 
            espera un momento para volver a intentarlo.');
                }
                return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', $type)->with('message',
                    $e->getMessage());
            } catch (\Exception $e) {
                DB::rollback();
                return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', $type)->with('message',
                    $e->getMessage());
            }
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', $type)->with('message',
            $message);
    }

    /**
     * Actualizar alistador en pedido
     */
    public function update_picker($id)
    {
        $picker_dry = Picker::find(Input::get('picker-dry-id'));
        $picker_cold = Picker::find(Input::get('picker-cold-id'));
        if ($picker_dry && $picker_cold) {
            $order = Order::find($id);
            $order->picker_dry_id = $picker_dry->id;
            $order->picker_cold_id = $picker_cold->id;
            $order->picking_dry_start_date = $order->picking_cold_start_date = $order->allocated_date = date('Y-m-d H:i:s');
            $order->save();
            Event::fire('order.picker_log', [[$order, Session::get('admin_id')]]);

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado con éxito.');
        } else {
            return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                'error')->with('message', 'No se encontro el alistador.');
        }
    }

    /**
     * Actualizar transportador en pedido
     */
    public function update_transporter($id)
    {
        $order = Order::find($id);
        $vehicle = Vehicle::find(Input::get('vehicle_id'));

        $vehicle_drivers = \VehicleDrivers::where('vehicle_id', $vehicle->id)->first();
        if ($order && $vehicle) {
            $order->driver_id = $vehicle_drivers->driver_id;
            $order->vehicle_id = $vehicle->id;
            $order->save();
            Event::fire('order.transporter_log',
                [[$order, Session::get('admin_id'), $vehicle->plate, $vehicle_drivers->driver_id]]);

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado con éxito.');
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
            'error')->with('message', 'No se encontro el pedido o el transportador.');
    }

    /**
     * Actualizar ruta en pedido
     */
    public function update_route($id)
    {
        $order = Order::find($id);
        $route = Routes::find(Input::get('route_id'));
        if ($order && $route) {
            $order_group = OrderGroup::find($order->group_id);
            $order_group->warehouse_id = $route->warehouse_id;
            $order_group->save();

            $order->route_id = $route->id;
            $order->planning_sequence = Input::get('planning_sequence');
            $order->planning_date = date('Y-m-d H:i:s');
            $order->save();

            Event::fire('order.route_log', [[$order, Session::get('admin_id')]]);

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado con éxito.');
        }

        return Redirect::route('adminOrderStorage.index')->with('type', 'error')->with('message',
            'No se encontro el pedido o la ruta.');
    }

    /**
     * Permitir entrega anticipada de pedido
     */
    public function allow_early_delivery($id)
    {
        if ($order = Order::find($id)) {
            $order->allow_early_delivery = 1;
            $order->save();
            Event::fire('order.allow_early_delivery_log', [[$order, Session::get('admin_id')]]);

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado con éxito.');

        } else {
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'error')->with('message', 'No se encontro el pedido.');
        }
    }

    /**
     * Actualizar estado de pago del pedido
     */
    public function update_order_payment_status($id, $status)
    {
        if ($order = Order::find($id)) {
            $order->payment_date = $status ? date('Y-m-d H:i:s') : null;
            $order->save();
            Event::fire('order.payment_status_log', [[$order, Session::get('admin_id')]]);

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado con éxito.');

        } else {
            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'error')->with('message', 'No se encontro el pedido.');
        }
    }

    /**
     * Actualizar producto en pedido
     */
    public function update_product()
    {
        if (Input::has('id')) {
            //update price or replace product
            $id = Input::get('id');
            if ($order_product = OrderProduct::find($id)) {
                $order = Order::find($order_product->order_id);
                $payments = $order->getPayments(true);
                if ($payments && $payments->cc_payment_status == 'Aprobada' && empty($payments->cc_refund_date) && $order->payment_method == 'Tarjeta de crédito') {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', 'El pedido ya fue cobrado.');
                }

                try {
                    DB::beginTransaction();

                    if (Input::has('quantity')) {
                        $old_quantity = $order_product->quantity;
                        $order_product->quantity = Input::get('quantity');

                        Event::fire('order.product_quantity_log', [
                            [
                                (Object)[
                                    'id' => $order->id,
                                    'name' => $order_product->product_name,
                                    'quantity' => $order_product->quantity,
                                    'old_quantity' => $old_quantity,
                                    'order_status' => $order->status,
                                ],
                                Session::get('admin_id'),
                                'admin'
                            ]
                        ]);
                    }

                    $order_product->save();

                    $order->updateTotals();
                    $order->save();
                    Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();

                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', 'Ocurrió un error al actualizar el pedido.');
                }

                return Redirect::route('adminOrderStorage.details', ['id' => $order->id]);
            }
        }

        return Redirect::route('adminOrderStorage.index');
    }

    /**
     * Actualizar producto en pedido
     */
    public function update_product_ajax()
    {
        if (Input::has('id') && Input::has('type') && Input::has('status')) {
            try {
                DB::beginTransaction();

                $id = Input::get('id');

                if (Input::get('type') == 'Agrupado') {
                    $order_product_group = OrderProductGroup::find($id);
                    $order = Order::find($order_product_group->order_id);
                    if($order->status == 'In Progress' || (Input::get('status') != "Missing" && Input::get('status') != "Pending" ) ) {

                        \Log::info('Modificando estado de producto: ' . $order_product_group->product_name . ' a ' . Input::get('status') . ' para pedido #' . $order_product_group->order_id);

                        if ($order_product_group->fulfilment_status == Input::get('status')) {
                            return Response::json(['status' => false, 'message' => 'El producto ya tiene el estado a asignar.'], 200);
                        }

                        if ($order->status == 'Delivered' && $order->invoice_number) {
                            return Response::json(['status' => false,
                                'message' => 'El pedido se encuentra entregado y facturado, no puedes realizar esta acción.'],
                                200);
                        }

                        if ($order->status == 'Dispatched' && $order_product_group->fulfilment_status == 'Fullfilled' && Input::get('status') == 'Not Available') {
                            $order_product_group->update_stock_back = 1;
                        } else {
                            $order_product_group->update_stock_back = 0;
                        }

                        if ($order->status == 'Alistado') {
                            $order->updateStockPickingPicked($order_product_group->store_product_id,
                                $order_product_group->quantity, Input::get('status'));
                        }

                        $order_product_group->fulfilment_status = Input::get('status');
                        $order_product_group->quantity_returned = null;
                        $order_product_group->save();
                        Event::fire('order.product_log', [[$order_product_group, Session::get('admin_id'), 'admin']]);
                        OrderProductGroup::updateProductStatus($order_product_group->order_product_id);

                        \Log::info('Modificado estado de producto: ' . $order_product_group->product_name . ' a ' . Input::get('status') . ' para pedido #' . $order_product_group->order_id);
                    }else{
                        return Response::json(['status' => false,
                            'message' => 'El pedido no se encuentra In progress, no puedes realizar esta acción.'],
                            200);
                    }
                } else {
                    $order_product = OrderProduct::find($id);
                    $order = Order::find($order_product->order_id);
                    if($order->status == 'In Progress' || (Input::get('status') != "Missing" && Input::get('status') != "Pending" ) ) {

                        \Log::info('Modificando estado de producto: ' . $order_product->product_name . ' a ' . Input::get('status') . ' para pedido #' . $order_product->order_id);

                        if ($order_product->fulfilment_status == Input::get('status')) {
                            return Response::json(['status' => false, 'message' => 'El producto ya tiene el estado a asignar.'], 200);
                        }


                        if ($order->status == 'Delivered' && $order->invoice_number) {
                            return Response::json(['status' => false,
                                'message' => 'El pedido se encuentra entregado y facturado, no puedes realizar esta acción.'],
                                200);
                        }

                        if ($order->status == 'Dispatched' && $order_product->fulfilment_status == 'Fullfilled' && Input::get('status') == 'Not Available') {
                            $order_product->update_stock_back = 1;
                        } else {
                            $order_product->update_stock_back = 0;
                        }

                        if ($order->status == 'Alistado' && (($order_product->fulfilment_status == 'Fullfilled' && Input::get('status') == 'Not Available') || (
                                    $order_product->fulfilment_status == 'Not Available' && Input::get('status') == 'Fullfilled'))) {
                            $order->updateStockPickingPicked($order_product->store_product_id, $order_product->quantity,
                                Input::get('status'));
                        }
                        $order_product->fulfilment_status = Input::get('status');
                        $order_product->quantity_returned = null;
                        $order_product->save();
                        Event::fire('order.product_log', [[$order_product, Session::get('admin_id'), 'admin']]);

                        \Log::info('Modificado estado de producto: ' . $order_product->product_name . ' a ' . Input::get('status') . ' para pedido #' . $order_product->order_id);
                    }else{
                        return Response::json(['status' => false,
                            'message' => 'El pedido no se encuentra In progress, no puedes realizar esta acción.'],
                            200);
                    }
                }

                $payments = $order->getPayments(true);

                if ($payments && $payments->status == 'Aprobada' && !empty($payments->cc_charge_id) && empty($payments->cc_refund_date) && $order->payment_method == 'Tarjeta de crédito') {
                    return Response::json(['status' => false, 'message' => 'El pedido ya fue cobrado.'], 200);
                }

                if ($order->status == 'Delivered') {
                    if (!$this->admin_permissions['permission2']) {
                        return Response::json(['status' => false, 'message' => 'El pedido ya esta entregado.'], 200);
                    }
                }
                $order->updateTotals();
                $order->save();

                //actualizar movimiento de vehiculo
                $movement = VehicleMovement::where('order_id', $order->id)->where('status', 1)->first();
                if (count($movement) && ($order->payment_method == 'Efectivo y datáfono' || $order->payment_method == 'Efectivo')) {
                    $total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                    if ($order->payment_method == 'Efectivo y datáfono') {
                        $movement->driver_balance = ($total_amount - $movement->user_card_paid) * -1;
                    } else {
                        $movement->driver_balance = $total_amount * -1;
                    }
                    $movement->save();
                }

                Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                DB::commit();

                \Log::info('Estado de productos modificados exitosamente para pedido #'.$order->id);

                $response['status'] = true;
                $response['product_status'] = Input::get('status');
                $response['payment_data'] = array(
                    'total_amount' => '$ ' . number_format($order->total_amount, 0, ',', '.'),
                    'delivery_amount' => '$ ' . number_format($order->delivery_amount, 0, ',', '.'),
                    'discount_amount' => '$ ' . number_format($order->discount_amount, 0, ',', '.'),
                    'total' => '$ ' . number_format($order->total_amount + $order->delivery_amount - $order->discount_amount,
                            0, ',', '.')
                );

            } catch (\Exception $e) {
                DB::rollback();
                \Log::info('Rollback a estado de productos para pedido #'.$order->order_id);
                throw $e;
            }
        } else {
            $response['status'] = false;
        }

        return Response::json($response, 200);
    }

    /**
     * Actualiza producto con devolución total parcial
     */
    public function update_product_status_returns()
    {
        if (Input::has('product_id') && Input::has('type')) {
            try {
                DB::beginTransaction();

                $id = Input::get('product_id');
                if (Input::get('type') == 'Agrupado') {
                    $order_product_group = OrderProductGroup::find($id);
                    $order = Order::find($order_product_group->order_id);

                    if ($order->status == 'Delivered' && $order->invoice_number) {
                        throw new Exception('El pedido se encuentra entregado y facturado, no puedes realizar esta acción.');
                    }

                    if(Input::has('quantity') && $order_product_group->quantity < Input::get('quantity')) {
                        throw new Exception('La cantidad de unidades del producto a devolver es mayor a la registrada en el pedido.');
                    }

                    $order_product_group->update_stock_back = 0;
                    $order_product_group->fulfilment_status = Input::has('quantity') && Input::get('quantity') == $order_product_group->quantity ? 'Returned' : 'Fullfilled';

                    if(Input::has('quantity') && $order_product_group->fulfilment_status != 'Returned') {
                        $order_product_group->quantity = $order_product_group->quantity - Input::get('quantity');
                    } elseif($order_product_group->quantity_returned) {
                        $order_product_group->quantity = $order_product_group->quantity_returned + $order_product_group->quantity;
                    }

                    if(Input::has('quantity') && $order_product_group->fulfilment_status != 'Returned') {
                        $order_product_group->quantity_returned = $order_product->quantity_returned + Input::get('quantity');
                    } else {
                        $order_product_group->quantity_returned = NULL;
                    }

                    $order_product_group->reason_returned = Input::get('reason_returned');
                    if(Input::has('reference_returned') && Input::get('reason_returned') == 'Errado') {
                        $order_product_group->reference_returned = Input::get('reference_returned');
                        $product = Product::join('store_products', 'products.id', '=', 'product_id')
                            ->where('reference', Input::get('reference_returned'))
                            ->where('store_id', $order->store_id)
                            ->first();
                        if (!$product) {
                            throw new Exception('La referencia del producto ingresado no existe.');
                        }
                    }

                    $order_product_group->save();
                    Event::fire('order.product_log', [[$order_product_group, Session::get('admin_id'), 'admin']]);
                    OrderProductGroup::updateProductStatus($order_product_group->order_product_id);

                } else {
                    $order_product = OrderProduct::find($id);
                    $order = Order::find($order_product->order_id);

                    if ($order->status == 'Delivered' && $order->invoice_number) {
                        throw new Exception('El pedido se encuentra entregado y facturado, no puedes realizar esta acción.');
                    }

                    if(Input::has('quantity') && Input::get('quantity') > $order_product->quantity) {
                        throw new Exception('La cantidad de unidades del producto a devolver es mayor a la registrada en el pedido.');
                    }
                    $order_product->update_stock_back = 0;
                    $order_product->fulfilment_status = Input::has('quantity') && Input::get('quantity') == $order_product->quantity ? 'Returned' : 'Fullfilled';

                    if(Input::has('quantity') && $order_product->fulfilment_status != 'Returned') {
                        $order_product->quantity = $order_product->quantity - Input::get('quantity');
                    } elseif($order_product->quantity_returned) {
                        $order_product->quantity = $order_product->quantity_returned + $order_product->quantity;
                    }

                    if(Input::has('quantity') && $order_product->fulfilment_status != 'Returned') {
                        $order_product->quantity_returned = $order_product->quantity_returned + Input::get('quantity');
                    } else {
                        $order_product->quantity_returned = NULL;
                    }

                    $order_product->reason_returned = Input::get('reason_returned');
                    if(Input::has('reference_returned') && Input::get('reason_returned') == 'Errado') {
                        $order_product->reference_returned = Input::get('reference_returned');
                        $product = Product::join('store_products', 'products.id', '=', 'product_id')
                            ->where('reference', Input::get('reference_returned'))
                            ->where('store_id', $order->store_id)
                            ->first();
                        if (!$product) {
                            throw new Exception('La referencia del producto ingresado no existe.');
                        }
                    }

                    $order_product->save();
                    Event::fire('order.product_log', [[$order_product, Session::get('admin_id'), 'admin']]);
                }

                $payments = $order->getPayments(true);

                if ($payments && !empty($payments->cc_charge_id) && empty($payments->cc_refund_date) && $order->payment_method == 'Tarjeta de crédito') {
                    throw new Exception('El pedido ya fue cobrado.');
                }

                if ($order->status == 'Delivered') {
                    if (!$this->admin_permissions['permission2']) {
                        throw new Exception('El pedido ya esta entregado.');
                    }
                }

                $order->updateTotals();
                $order->save();

                //actualizar movimiento de vehiculo
                $movement = VehicleMovement::where('order_id', $order->id)->where('status', 1)->first();
                if (count($movement) && ($order->payment_method == 'Efectivo y datáfono' || $order->payment_method == 'Efectivo')) {
                    $total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                    if($order->payment_method == 'Efectivo y datáfono') {
                        $movement->driver_balance = ($total_amount - $movement->user_card_paid) * -1;
                    } else {
                        $movement->driver_balance = $total_amount * -1;
                    }
                    $movement->save();
                }

                Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);
                DB::commit();

                return Redirect::route('adminOrderStorage.details', ['id' => Input::get('id')])->with('type',
                    'success')->with('message', 'Producto actualizado satifactoriamente');

            } catch (\Exception $e) {
                DB::rollback();
                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                    'failed')->with('message', $e->getMessage());
            }
        } else {
            return Redirect::route('adminOrderStorage.details', ['id' => Input::get('id')])->with('type',
                'failed')->with('message', 'Ocurrió un error al actualizar devolución.');
        }
    }

    /**
     * Añadir cupon a pedido
     */
    public function save_coupon()
    {
        $message = 'Ocurrió un error al añadir el cupón al pedido.';
        $type = 'failed';

        $id = Input::get('id');

        if (Input::has('id') && Input::has('coupon_code')) {
            $order = Order::find($id);
            if ($order) {
                if ($order->status == 'Delivered' || $order->status == 'Cancelled') {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', 'El pedido ya fue entregado o cancelado.');
                }

                if (!empty($order->discount_amount)) {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', 'El pedido ya tiene un descuento aplicado.');
                }

                $coupon_code = Input::get('coupon_code');
                $request = Request::create('/checkout/coupon', 'POST', array(
                    'coupon_code' => $coupon_code,
                    'user_id' => $order->user_id,
                    'order_id' => $order->id,
                    'validate' => 1,
                    'is_new_user' => 1
                ));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if ($result['status']) {
                    if (isset($result['coupon'])) {
                        $coupon = (Object)$result['coupon'];
                        $total_cart = $order->total_amount + $order->delivery_amount;
                        //si el cupon cumple con totales requerido
                        if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $total_cart < $coupon->minimum_order_amount) {
                            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                'failed')->with('message',
                                'Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el total del pedido sin incluir el domicilio debe ser mayor o igual a <b>$' . number_format($coupon->minimum_order_amount,
                                    0, ',', '.') . '</b>');
                        } else {
                            if ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $total_cart > $coupon->maximum_order_amount) {
                                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                                    'failed')->with('message',
                                    'Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el total del pedido sin incluir el domicilio debe ser menor o igual a <b>$' . number_format($coupon->maximum_order_amount,
                                        0, ',', '.') . '</b>');
                            } else {
                                if ($coupon->minimum_order_amount && $coupon->maximum_order_amount && $total_cart < $coupon->minimum_order_amount && $total_cart > $coupon->maximum_order_amount) {
                                    return Redirect::route('adminOrderStorage.details',
                                        ['id' => $order->id])->with('type', 'failed')->with('message',
                                        'Para redimir el cupón de descuento <b>' . $coupon->coupon . '</b> el total del pedido sin incluir el domicilio debe estar entre <b>$' . number_format($coupon->maximum_order_amount,
                                            0, ',', '.') . '</b> y <b>$' . number_format($coupon->minimum_order_amount,
                                            0, ',', '.') . '</b>');
                                }
                            }
                        }

                        $amount = 0;
                        if ($coupon->redeem_on == 'Total Cart') {
                            if ($coupon->type == 'Discount Percentage') {
                                $amount = round($order->total_amount * ($coupon->amount / 100), 0);
                            } else {
                                if ($coupon->type == 'Credit Amount') {
                                    $amount = $order->total_amount >= $coupon->amount ? $coupon->amount : $order->total_amount;
                                }
                            }
                        } else {
                            if ($coupon->redeem_on == 'Specific Store') {
                                $amount = $result['discount_amount'];
                            }
                        }
                        try {
                            DB::beginTransaction();

                            //cargar cupon como utilizado a usuario
                            $user_coupon = new UserCoupon;
                            $user_coupon->coupon_id = $coupon->id;
                            $user_coupon->user_id = $order->user_id;
                            $user_coupon->save();

                            //registrar movimiento de crédito a usuario
                            $user = User::find($order->user_id);
                            $expiration_date = date('Y-m-d H:i:s', strtotime('+1 day'));
                            UserCredit::addCredit($user, $amount, $expiration_date, 'coupon', $coupon);
                            //descontar credito utilizado a usuario
                            UserCredit::removeCredit($user, $amount, 'order', $order, $coupon->id);

                            $order->discount_amount = $amount;
                            $order->save();

                            //log de pedido
                            $log = new OrderLog();
                            $log->type = 'Descuento actualizado: ' . currency_format($order->discount_amount);
                            $log->admin_id = Session::get('admin_id');
                            $log->order_id = $order->id;
                            $log->save();

                            DB::commit();

                            $message = 'Cupón añadido al pedido con éxito.';
                            $type = 'success';

                        } catch (\Exception $e) {
                            DB::rollback();
                            throw $e;
                        }
                    } else {
                        return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                            'failed')->with('message',
                            'No se puede agregar un codigo referido a un pedido manualmente.');
                    }
                } else {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', $result['message']);
                }
            }
        }
        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', $type)->with('message',
            $message);
    }

    /**
     * Eliminar descuento de pedido
     */
    public function delete_discount_amount()
    {
        $id = Request::segment(3);
        $order = Order::find($id);

        if ($order) {
            try {
                DB::beginTransaction();

                $discount_amount = $order->discount_amount;
                $order->discount_amount = 0;
                $order->discount_percentage_amount = null;
                $order->save();

                OrderDiscount::removeDiscount($order);
                UserCredit::removeCreditDiscount($order);

                $log = new OrderLog();
                $log->type = 'Descuento eliminado: ' . currency_format($discount_amount);
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'El descuento fue eliminado del pedido con éxito.');
        }
        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al eliminar el descuento al pedido.');
    }

    /**
     * Eliminar domicilio de pedido
     */
    public function delete_delivery_amount()
    {
        $id = Request::segment(3);
        $order = Order::find($id);

        if ($order) {
            try {
                DB::beginTransaction();

                $delivery_amount = $order->delivery_amount;
                $order->delivery_amount = 0;
                $order->save();

                $log = new OrderLog();
                $log->type = 'Domicilio eliminado: ' . currency_format($delivery_amount);
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'El valor del domicilio fue eliminado del pedido con éxito.');
        }
        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al eliminar valor del domicilio al pedido.');
    }

    /**
     * Editar fecha de entrega de pedido a cliente
     */
    public function update_delivery_date()
    {
        $ids = json_decode(Input::get('order_id'), true);
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as $id) {
            $order = Order::select('orders.*', 'city_id')
                ->join('stores', 'store_id', '=', 'stores.id')
                ->whereNotIn('orders.status', ['Delivered', 'Cancelled'])
                ->where('orders.id', $id)
                ->with('store', 'orderGroup')
                ->first();

            if ($order) {
                $delivery_day = Input::get('delivery_day');

                $order_group = $order->orderGroup;
                try {
                    $delivery_window = $order->store->validateStoreDeliversToAddress(
                        $order_group->user_address_latitude,
                        $order_group->user_address_longitude,
                        new Carbon($delivery_day),
                        Input::get('delivery_time')
                    );
                    $order->deliveryWindow()->associate($delivery_window);
                } catch (MerqueoException $exception) {

                    return Redirect::route('adminOrderStorage.details', compact('id'))
                        ->with('type', 'failed')
                        ->with('message', $exception->getMessage());
                }

                $log_type = 'Fecha de entrega actualizada: ' . format_date('normal_with_time',
                        $order->delivery_date) . ' a ' . format_date('normal_with_time',
                        $delivery_day . ' ' . $delivery_window->hour_end);
                $order->real_delivery_date = $order->delivery_date = $delivery_day . ' ' . $delivery_window->hour_end;
                $order->delivery_time = $delivery_window->delivery_window;
                $order->scheduled_delivery = 1;
                $order->save();

                $log = new OrderLog();
                $log->type = $log_type;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                if (Input::has('order_detail')) {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'success')->with('message', 'La fecha de entrega fue actualizada con éxito.');
                }
            } else {
                if (Input::has('order_detail')) {
                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        'failed')->with('message', 'Ocurrió un error al actualizar la fecha de entrega del pedido.');
                }
            }
        }
        return Redirect::route('adminOrderStorage.index')->with('type', 'success')->with('success',
            'Fecha de entrega actualizada en ' . count($ids) . ' pedidos.');
    }

    /**
     * Editar estado de revision de pedido
     */
    public function update_revision_checked()
    {
        $id = Request::segment(3);

        if ($order = Order::find($id)) {
            $store = Store::find($order->store_id);
            if (!$store->revision_orders_required) {
                return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                    'failed')->with('message', 'El pedido no permite revisión.');
            }

            $order->is_checked = 1;
            $order->revision_user_id = Session::get('admin_id');
            $order->save();

            $log = new OrderLog();
            $log->type = 'Pedido revisado.';
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'El pedido fue actualizado con éxito.');
        }
        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al actualizar el pedido.');
    }

    /**
     * Editar direccion de entrega de pedido
     */
    public function update_delivery_address()
    {
        $id = Request::segment(3);
        $order = Order::find($id);
        $order_group = OrderGroup::find($order->group_id);

        $old_address = $order_group->user_address;
        $old_city = $order_group->user_city_id;
        $old_city = City::find($old_city);
        if ($order) {
            $address = Input::get('address_1') . ' ' . Input::get('address_2') . ' # ' . Input::get('address_3') . ' - ' . Input::get('address_4');
            $address_further = Input::get('delivery_address_further');
            $address_neighborhood = Input::get('delivery_address_neighborhood');
            $latlng = Input::get('latlng');
            $city_id = Input::get('city');

            if (!empty($latlng)) {
                $latlng = explode(',', trim($latlng));
                if (count($latlng) == 2 && validate_longitude(trim($latlng[0])) && validate_latitude(trim($latlng[1]))) {
                    $order_group->user_address_latitude = trim($latlng[0]);
                    $order_group->user_address_longitude = trim($latlng[1]);
                    $ignore_sitimapas = true;
                } else {
                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        'failed')->with('message', 'La coordenada de la dirección no es valida.');
                }
            }

            if (!empty($latlng) && Input::has('update_user_address')) {
                $user_address = UserAddress::find($order_group->address_id);
                $user_address->city_id = $city_id;
                $user_address->latitude = trim($latlng[0]);
                $user_address->longitude = trim($latlng[1]);
                if ($address_neighborhood != null)
                    $user_address->neighborhood = $address_neighborhood;
                if ($address_further != null)
                    $user_address->address_further = $address_further;
                $user_address->save();
            }elseif(Input::has('update_user_address')){
                $user_address = UserAddress::find($order_group->address_id);
                if ($user_address) {
                    $user_address->address = $address;
                    $user_address->address_further = $address_further;
                    $user_address->neighborhood = $address_neighborhood;
                    $user_address->city_id = $city_id;
                    $user_address->address_1 = trim(Input::get('address_1'));
                    $user_address->address_2 = trim(Input::get('address_2'));
                    $user_address->address_3 = trim(Input::get('address_3'));
                    $user_address->address_4 = trim(Input::get('address_4'));
                    $user_address->save();
                }
            }

            $order_group->user_address = $address;
            $order_group->user_address_further = $address_further;
            $order_group->user_address_neighborhood = $address_neighborhood;
            $order_group->user_city_id = $city_id;

            $city = City::find($city_id);

            if (!isset($ignore_sitimapas)) {
                $request = Request::create('/api/location', 'GET', array('address' => $address, 'city' => $city->city));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent());

                if ($result->status) {
                    $order_group->user_address_latitude = $result->result->latitude;
                    $order_group->user_address_longitude = $result->result->longitude;

                    try {
                        $order->store->validateStoreDeliversToAddress(
                            $order_group->user_address_latitude,
                            $order_group->user_address_longitude,
                            new Carbon($order->delivery_date),
                            $order->delivery_time
                        );
                    } catch (MerqueoException $exception) {
                        return Redirect::route('adminOrderStorage.details', ['id' => $id])
                            ->with('type', 'failed')
                            ->with('message', $exception->getMessage());
                    }
                } else {
                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        'failed')->with('message', 'No se pudo ubicar la dirección por favor verificala.');
                }
            }

            $city_id = $this->getCoverageStoreCityId($order_group->user_city_id);

            //obtener zona
            $zone = Zone::getByLatLng($city_id, $order_group->user_address_latitude,
                $order_group->user_address_longitude);
            if (!$zone) {
                return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                    'failed')->with('message', 'No hay cobertura en zonas, por favor verifica la dirección.');
            }
            $order_group->zone_id = $zone->id;
            $order_group->warehouse_id = $zone->warehouse_id;

            // obtener localidad
            $locality_id = null;
            $localities = Locality::where('city_id', $city_id)->where('status', 1)->get();
            if ($localities) {
                foreach ($localities as $locality) {
                    if (point_in_polygon($locality->polygon, $order_group->user_address_latitude,
                        $order_group->user_address_longitude)) {
                        $locality_id = $locality->id;
                        break;
                    }
                }
            }
            $order_group->locality_id = $locality_id;

            $order_group->save();

            $log = new OrderLog();
            $log->type = 'Dirección de entrega actualizada: ' . $old_city->city . ' ' . $old_address . ' a ' . $city->city . ' ' . $order_group->user_address;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'La dirección de entrega del pedido fue actualizada con éxito.');
        }
        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al actualizar la dirección de entrega del pedido.');
    }

    /**
     * Editar metodo de pago del cliente
     */
    public function update_payment_method($id)
    {
        if ($order = Order::find($id)) {
            if ((!$this->admin_permissions['permission2'] && $order->status == 'Delivered') || $order->status == 'Cancelled') {
                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                    'failed')->with('message', 'El estado del pedido no es valido.');
            }

            $payments = $order->getPayments(true);
            if ($payments && $payments->status == 'Aprobada' && !empty($payments->cc_charge_id) && empty($payments->cc_refund_date) && $order->payment_method == 'Tarjeta de crédito') {
                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                    'failed')->with('message', 'El pedido ya fue cobrado.');
            }

            $order_group = OrderGroup::find($order->group_id);
            $total_order = round($order->total_amount + $order->delivery_amount - $order->discount_amount);
            $old_payment_method = $order->payment_method;

            /*if (Input::get('payment_method') != 'Tarjeta de crédito')
            {
                $order->credit_card_id = null;
                $order->cc_token = null;
                $order->cc_installments = null;
                $order->cc_last_four = null;
                $order->cc_type = null;
                $order->cc_holder_name = null;
                $order->cc_country = null;
            }*/

            $movement = VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('order_id',
                $order->id)->where('status', 1)->first();
            if (!$movement) {
                $movement = new VehicleMovement;
                $movement->vehicle_id = $order->vehicle_id;
                $movement->driver_id = $order->driver_id;
                $movement->order_id = $order->id;
                $movement->admin_id = Session::get('admin_id');
                $movement->movement = 'Gestión de pedido';
            }

            if (Input::get('payment_method') == 'Efectivo y datáfono') {
                if ($movement) {
                    $movement->payment_method = Input::get('payment_method');
                    $movement->user_card_paid = Input::get('user_card_paid');
                    $movement->user_cash_paid = Input::get('user_cash_paid');
                    $movement->status = 1;

                    if ($total_order != (Input::get('user_cash_paid') + Input::get('user_card_paid'))) {
                        return Redirect::back()->with('type', 'failed')->with('message',
                            'El total pagado en efectivo y con datáfono no coincide con el total del pedido/factura.');
                    }
                }
            }

            if (Input::get('payment_method') == 'Efectivo') {
                //$movement = VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('order_id', $order->id)->OrderBy('created_at', 'DESC')->first();
                if ($movement) {
                    $movement->payment_method = Input::get('payment_method');
                    $movement->user_card_paid = 0;
                    $movement->user_cash_paid = $total_order;
                    $movement->status = 1;
                }
            }

            if (Input::get('payment_method') == 'Tarjeta de crédito') {
                $user_credit_card = UserCreditCard::where('id', Input::get('credit_card_id'))->where('user_id',
                    $order->user_id)->first();
                if (!$user_credit_card) {
                    return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                        'failed')->with('message', 'Tarjeta de crédito invalida.');
                }

                $order->assignCreditCard($user_credit_card, 1);

                if ($movement) {
                    $movement->payment_method = Input::get('payment_method');
                    $movement->user_card_paid = $total_order;
                    $movement->user_cash_paid = 0;
                    $movement->status = 1;
                }
            }

            if (Input::get('payment_method') == 'Datáfono') {
                if ($movement) {
                    $movement->payment_method = Input::get('payment_method');
                    $movement->user_card_paid = $total_order;
                    $movement->user_cash_paid = 0;
                    $movement->status = 1;
                }
            }

            if (Input::get('payment_method') == 'Efectivo y datáfono' || Input::get('payment_method') == 'Efectivo') {
                if ($movement) {
                    $movement->admin_id = Session::get('admin_id');
                    $movement->description = trim('Método de pago actualizado a ' . Input::get('payment_method') . ' desde el Administrador.');
                    VehicleMovement::saveMovement($movement, $order);
                }
            } else {
                if (VehicleMovement::where('vehicle_id', $order->vehicle_id)->where('order_id', $order->id)->first()) {
                    if ($movement) {
                        $movement->admin_id = Session::get('admin_id');
                        $movement->description = trim('Método de pago actualizado a ' . Input::get('payment_method') . ' desde el Administrador.');
                        VehicleMovement::saveMovement($movement, $order);
                    }
                }
            }

            if ($old_payment_method == 'Tarjeta de crédito' && Input::get('payment_method') != 'Tarjeta de crédito') {
                $schedule_date = date('G') >= 6 && date('G') <= 22 ? null : date('d/m/Y 06:00',
                    strtotime($order->delivery_date));
                $payment_method = Input::get('payment_method');
                $payment_method = str_replace('á', 'a', $payment_method);
                $message = 'Tu pedido de Merqueo fue cambiado de pago online con Tarjeta de credito a ' . $payment_method;
                send_sms($order_group->user_phone, $message, $schedule_date);
                $order->order_validation_reason_id = 3;
            }

            //validar datos de fraude
            $city = City::find($order_group->user_city_id);
            $post_data = array(
                'address' => $order_group->user_address,
                'city' => $city->slug,
                'phone' => $order_group->user_phone,
                'email' => $order_group->user_email,
                'payment_method' => Input::get('payment_method')
            );

            if (Input::get('payment_method') == 'Tarjeta de crédito') {
                $post_data['credit_card_id'] = $order->credit_card_id;
            }

            $result = $order->validateOrderFraud($post_data);
            $order->posible_fraud = empty($result['posible_fraud']) ? 0 : $result['posible_fraud'];

            $order->payment_method = Input::get('payment_method');
            $order->save();

            $log = new OrderLog();
            $log->type = 'Método de pago actualizado: ' . $old_payment_method . ' a ' . Input::get('payment_method');
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado con éxito.');
        }
        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Error actualizando el método de pago.');
    }

    /**
     * Guardar nota de cliente en pedido
     */
    public function save_note($id)
    {
        $order = Order::find($id);
        if ($order) {
            $order_note = new OrderNoteSms;
            $order_note->order_id = $order->id;
            $order_note->description = Input::get('note');
            $order_note->admin_id = Session::get('admin_id');
            $order_note->save();

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'La nota fue guardada con éxito.');
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al guardar la nota.');
    }

    /**
     * Subir imagen de factura y voucher
     */
    function upload_image($id)
    {
        if (Input::has('type') && Input::hasFile('image')) {
            $file = Input::file('image');
            $file_data['real_path'] = $file->getRealPath();
            $file_data['client_original_name'] = $file->getClientOriginalName();
            $file_data['client_original_extension'] = $file->getClientOriginalExtension();

            if (Input::get('type') == 'invoice') {
                $dir = 'orders/invoices/' . date('Y-m-d');
            }
            if (Input::get('type') == 'voucher') {
                $dir = 'orders/vouchers/' . date('Y-m-d');
            }

            $url = upload_image($file, false, $dir);

            if ($url) {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                if (Input::get('type') == 'voucher') {
                    if ($order = Order::find($id)) {
                        if (!empty($order->voucher_image_url)) {
                            $path = str_replace($cloudfront_url, uploads_path(), $order->voucher_image_url);
                            remove_file($path);
                            remove_file_s3($order->voucher_image_url);
                        }
                        $order->voucher_image_url = $url;
                        $log_type = 'Imagen de voucher subida.';
                        $order->save();
                    }
                }

                if (isset($log_type)) {
                    //log de pedido
                    $log = new OrderLog();
                    $log->type = $log_type;
                    $log->admin_id = Session::get('admin_id');
                    $log->order_id = $id;
                    $log->save();

                    return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type',
                        'success')->with('message', 'Imagen subida con éxito.');
                }
            }
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al subir la imagen.');
    }

    /**
     * Actualiza devolucion a bodega de pedido
     */
    public function update_order_return($id)
    {
        //actualizar devolucion a bodega
        $result = OrderReturn::updateOrderReturn($id);
        if (!$result['status']) {
            return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
                $result['message']);
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'success')->with('message',
            $result['message']);
    }

    /**
     * Obtener detalle de producto agrupado ajax
     */
    public function get_product_group_ajax($id)
    {
        $order_products = [];

        if (!$order = Order::find($id)) {
            return Response::json(['status' => false], 400);
        }

        $order_group = OrderGroup::find($order->group_id);

        if (Input::has('order_product_id')) {
            $order_product_group = OrderProductGroup::where('order_product_id', Input::get('order_product_id'))
                ->where('warehouse_id', $order_group->warehouse_id)
                ->leftJoin('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                ->join('stores', 'order_products.store_id', '=', 'stores.id')
                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=',
                    'store_products.id')
                ->select('order_products.*', 'order_product_group.*', 'stores.name AS store_name')
                ->orderBy(DB::raw("FIELD(store_products.storage, 'Seco', 'Congelado', 'Refrigerado')"))
                ->orderBy('storage_position')
                ->orderBy('store_products.department_id', 'desc')->orderBy('order_product_group.product_name', 'asc')
                ->get();
        }

        return View::make('admin.orders_storage.details')
            ->with('order_product_group', $order_product_group)
            ->with('order', $order)
            ->renderSections()['order_product_group'];
    }

    /**
     * Muestra el listado de commentarios disponibles
     * para un pedido por producto
     *
     * @param int $order_id
     * @return mixed
     */
    public function get_order_products_comments_ajax($order_id)
    {
        $render_order_product_comments = true;
        $order = Order::with('orderGroup')
            ->with([
                'orderProducts' => function ($query) {
                    $query->has('OrderProductScore')->with('orderProductScore');
                }
            ])
            ->findOrFail($order_id);
        $sections = View::make('admin.orders_storage.details', compact('order', 'render_order_product_comments'))
            ->renderSections();

        return $sections['comments'];
    }

    /**
     * Obtener tiendas por ciudad ajax
     */
    public function get_stores_ajax()
    {
        $stores = array();
        if (Input::has('city_id')) {
            $stores = Store::select('id', 'name')->where('city_id', Input::get('city_id'))->where('status',
                1)->get()->toArray();
        }
        return Response::json($stores, 200);
    }

    /**
     * Obtener horarios de tienda ajax
     */
    public function get_store_delivery_slot_ajax()
    {
        $deliverySlot = [];
        if (Input::has('store_id')) {
            if (Input::has('zone_id')) {
                $zoneId = Input::get('zone_id');
            } else {
                $zoneId = 0;
                $zone = Zone::select('zones.*')
                    ->join('warehouses', 'warehouses.id', '=', 'zones.warehouse_id')
                    ->join('stores', 'stores.city_id', '=', 'warehouses.city_id')
                    ->where('warehouses.status', 1)
                    ->where('stores.id', Input::get('store_id'))
                    ->where('stores.status', 1)
                    ->first();
                if ($zone) {
                    $zoneId = $zone->id;
                }
            }

            $store = Store::find(Input::get('store_id'));
            $showSameDay = empty(Input::get('show_today'));
            $deliverySlot = $store->getDeliverySlotCheckout(Zone::find($zoneId), false, 6, true, $showSameDay,
                $showSameDay);
        }

        return Response::json($deliverySlot, 200);
    }

    /**
     * Filtra las tiendas, transportadores, rutas, zonas y alistadores por ciudad
     */
    public function get_stores_transporters_by_city_ajax()
    {
        $city_id = Input::get('city_id');

        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=',
            'cities.id')->where('is_storage', 1)->where('stores.status', 1);

        $allied_stores = AlliedStore::select('allied_stores.name', 'allied_stores.id',
            'allied_stores.city_id')->join('cities', 'city_id', '=', 'cities.id')->where('allied_stores.status',
            1)->where('allied_stores.city_id', $city_id)->get();

        $transporters = Transporter::select('id', 'fullname')->where('status', 1)->orderBy('fullname');
        $pickers = Picker::select('pickers.id',
            DB::raw("CONCAT(first_name, ' ', last_name) AS fullname"))->join('cities', 'city_id', '=', 'cities.id')
            ->where('profile', 'Alistador Seco')->where('pickers.status',
                1)->orderBy('first_name')->orderBy('last_name');
        $routes = Routes::select('routes.id', 'routes.route')
            ->join('orders', 'orders.route_id', '=', 'routes.id')
            ->whereNotNull('orders.route_id')
            ->groupBy('routes.id')
            ->orderBy('routes.id');
        //mostrar rutas de planeacion no termninada
        $admin_permissions = json_decode(Session::get('admin_permissions'), true);
        if (isset($admin_permissions['adminroute']) && $admin_permissions['adminroute']['permissions']['update']) {
            $routes = $routes->where(function ($query) {
                $query->where('orders.status', 'Initiated')
                    ->orWhere('orders.status', 'Enrutado')
                    ->orWhere('orders.status', 'In Progress')
                    ->orWhere('orders.status', 'Dispatched')
                    ->orWhere('orders.status', 'Alistado');
            });
        } else {
            $routes = $routes->where(function ($query) {
                $query->where('orders.status', 'Enrutado')
                    ->orWhere('orders.status', 'In Progress')
                    ->orWhere('orders.status', 'Dispatched')
                    ->orWhere('orders.status', 'Alistado');
            });
        }
        $zones = Zone::select('zones.id', 'zones.name')->join('warehouses', 'warehouses.id', '=',
            'warehouse_id')->where('zones.status', 1)->orderBy('zones.name');

        if ($city_id) {
            $transporters->where('city_id', $city_id);
            $stores->where('city_id', $city_id);
            $pickers->where('city_id', $city_id);
            $routes->where('routes.city_id', $city_id);
            $zones->where('warehouses.city_id', $city_id);
        }

        $transporters = $transporters->get();
        $stores = $stores->get();
        $pickers = $pickers->get();
        $routes = $routes->get();
        $zones = $zones->get();

        $result['transporters'] = $transporters;
        $result['stores'] = $stores;
        $result['pickers'] = $pickers;
        $result['routes'] = $routes;
        $result['zones'] = $zones;
        $result['allied_stores'] = $allied_stores;

        return Response::json($result);
    }

    /**
     * Funcion para obtener los vehiculos por transportador
     */
    public function get_drivers_vehicles_by_transporters_ajax()
    {
        $transporter_id = Input::get('transporter_id', null);
        if ($transporter_id) {
            $vehicles = Vehicle::where('status', 1)
                ->where('transporter_id', $transporter_id)
                ->select('id', 'plate')
                ->orderBy('plate')
                ->get();
            $drivers = Driver::where('drivers.transporter_id', $transporter_id)
                ->select('drivers.id', 'drivers.first_name', 'drivers.last_name')
                ->orderBy('first_name', 'last_name')
                ->get();
            $result['vehicles'] = $vehicles;
            $result['drivers'] = $drivers;
            return Response::json($result);
        }
        $result['error'] = 'No se encontraro vehiculos para este transportador.';
        return Response::json($result);
    }

    /**
     * Obtener el conductor de un vehiculo
     */
    public function get_driver_by_vehicle_ajax()
    {
        $vehicle_id = Input::get('vehicle_id', null);

        if ($vehicle_id) {
            $driver = Driver::join('vehicle_drivers', 'drivers.id', '=', 'vehicle_drivers.driver_id')
                ->where('vehicle_drivers.vehicle_id', $vehicle_id)
                ->select('drivers.id', 'drivers.first_name', 'drivers.last_name')
                ->orderBy('first_name', 'last_name')
                ->get();
            $result['drivers'] = $driver;
            return Response::json($result);
        }
        $result['error'] = 'No se encontraron conductores asociados a este vehiculo.';
        return Response::json($result);
    }

    /**
     *  Genera factura de pedido
     *
     * @param  $reference Número de refencia de un pedido
     * @return $file PDF generado
     */
    public function generate_invoice($id)
    {
        $order = Order::select('orders.*', 'stores.name AS store_name')
            ->join('stores', 'store_id', '=', 'stores.id')
            ->where('orders.id', $id)->first();
        if ($order) {
            if ($order->status === 'Delivered') {
                Pdf::generateInvoice($order);
            } else {
                return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                    'failed')->with('message', 'El pedido no esta entregado.');
            }
        } else {
            return Redirect::route('adminOrderStorage.index')->with('type', 'failed')->with('message',
                'No existe el pedido.');
        }
    }

    /**
     *  Guardar datos para facturacion
     */
    public function save_invoice_data()
    {
        $id = Request::segment(3);
        $order = Order::find($id);

        if ($order) {
            if (Input::has('user_identity_type')) {
                $order->user_identity_type = Input::get('user_identity_type');
            }
            if (Input::has('user_identity_number')) {
                $order->user_identity_number = Input::get('user_identity_number');
            }
            if (Input::has('user_business_name')) {
                $order->user_business_name = Input::get('user_business_name');
            }

            $order->save();

            $log = new OrderLog();
            $log->type = 'Datos de factura actualizados: Nombre:' . Input::get('user_business_name') . ', Documento:' . Input::get('user_identity_number');
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrderStorage.details', ['id' => $order->id])->with('type',
                'success')->with('message', 'Pedido actualizado, ya puedes descargar la factura.');
        }

        return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
            'Ocurrió un error al actualizarlos datos.');
    }

    /**
     *  Validar razón social y documento para factura
     */
    public function verify_data_invoice()
    {
        $id = Input::get('id');
        $order = Order::select('orders.*', 'stores.name AS store_name')
            ->join('stores', 'store_id', '=', 'stores.id')
            ->where('reference', $id)->first();

        if (!empty($order->user_identity_type) && !empty($order->user_identity_type) && !empty($order->user_business_name)) {
            $response['success'] = 'true';
        } else {
            if ($user = User::find($order->user_id)) {
                if (!empty($user->identity_number) && !empty($user->identity_type) && !empty($user->business_name)) {
                    $response['success'] = 'true';
                } else {
                    $response['success'] = 'false';
                    $response['error'] = 'Ocurrió un error al validar los datos de usuario';
                    $response['error_code'] = 413;
                }
            } else {
                $response['success'] = 'false';
                $response['error'] = 'Ocurrió un error al validar los datos de usuario';
                $response['error_code'] = 413;
            }
        }
        return Response::json($response, 200);
    }

    /**
     * Reenviar email de entregado
     */
    public function re_send_delivery_email($id)
    {
        $order = Order::find($id);
        $order_group = OrderGroup::find($order->group_id);
        $store = Store::find($order->store_id);
        $user = User::find($order->user_id);
        $products = OrderProduct::where('order_id',
            $order->id)->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available','Fullfilled', 'Replacement')"))->get();
        foreach ($products as $key => $product) {
            $products_mail[$order->id][] = [
                'img' => $product->product_image_url,
                'name' => $product->product_name,
                'qty' => $product->quantity,
                'qty_original' => $product->quantity_original,
                'quantity' => $product->product_quantity,
                'unit' => $product->product_unit,
                'price' => $product->price,
                'total' => $product->quantity * $product->price,
                'type' => $product->type,
                'status' => $product->fulfilment_status != 'Not Available' ? 'available.png' : 'no_available.png'
            ];
        }

        $products_available = [ 'products' => null, 'subtotal' => 0 ];
        $products_not_available = [ 'products' => null, 'subtotal' => 0 ];
        foreach ($products_mail[$order->id] as $product) {
            if ($product['status'] == 'available.png') {
                $products_available['products'][] = $product;
                $products_available['subtotal'] += $product['total'];
            }else{
                $products_not_available['products'][] = $product;
                $products_not_available['subtotal'] += $product['total'];
            }
        }

        $mail = [
            'template_name' => 'emails.order_delivered',
            'subject' => 'Tu pedido en ' . $store->name . ' ha sido entregado',
            'to' => [
                [
                    'email' => $order_group->user_email,
                    'name' => $order_group->user_firstname . ' ' . $order_group->user_lastname
                ]
            ],
            'vars' => [
                'order' => $order,
                'order_group' => $order_group,
                'store' => $store,
                'reject_reason' => $order->reject_reason,
                'products' => $products_mail[$order->id],
                'products_available' => $products_available,
                'products_not_available' => $products_not_available
            ]
        ];

        $result = send_mail($mail);
        if ($result) {
            return Redirect::back()->with('message', 'Correo enviado.')->with('type', 'success');
        }

        return Redirect::back()->with('message', 'Ha ocurrido un error al enviar el correo.')->with('type', 'error');
    }

    /**
     * Envía mensajes de texto
     * @param  int $id Identificador de órdenes
     */
    public function send_sms($id)
    {
        $message = trim(Input::get('sms', null));
        $order = Order::where('orders.id', $id)->join('users AS u', 'orders.user_id', '=',
            'u.id')->select('u.phone')->first();
        if (!empty($order) && !empty($message)) {
            $result = send_sms($order->phone, $message);
            if ($result) {
                $order_notes_sms = new OrderNoteSms;
                $order_notes_sms->order_id = $id;
                $order_notes_sms->admin_id = Session::get('admin_id');
                $order_notes_sms->type = 'sms';
                $order_notes_sms->description = $order->phone . ' || ' . $message;
                $order_notes_sms->save();
                return Redirect::back()->with('success', 'El mensaje de texto se ha enviado correctamente.');
            }
            return Redirect::back()->with('error', 'Ha ocurrido un error al enviar el mensaje de texto.');
        }
        return Redirect::back()->with('error', 'No se encontró la orden.');
    }

    /**
     * Generar rotulos
     */
    public function order_label_pdf($id)
    {
        $order = Order::find($id);
        if (!$order) {
            return Redirect::route('adminOrderStorage.details', ['id' => $id])->with('type', 'failed')->with('message',
                'No existe el pedido.');
        } else {
            Pdf::generate_order_label($id);
        }
    }

    /**
     * Buscar productos por ajax
     */
    public function search_product_ajax()
    {
        $search = Input::get('s');
        $store_id = Input::get('store_id');
        $order_id = Input::get('order_id');
        $order = Order::find($order_id);
        $order_group = OrderGroup::find($order->group_id);
        $store_products = Product::select('products.*', 'store_products.*', 'store_product_warehouses.*',
            'store_products.id')
            ->join('store_products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_product_id', '=', 'store_products.id')
            ->where(function ($query) use ($search) {
                $query->orWhere('products.reference', 'LIKE', '%' . $search . '%');
                $query->orWhere('products.name', 'LIKE', '%' . $search . '%');
            })
            //->whereNull('store_products.allied_store_id')
            ->where('store_product_warehouses.warehouse_id', $order_group->warehouse_id)
            ->where('store_products.store_id', $store_id);
        if ($order->status == 'Dispatched') {
            $store_products = $store_products->where('products.type', 'Simple');
        }
        $store_products = $store_products->get();
        $data = [
            'store_products' => $store_products,
            'store_id' => $store_id,
            'order_id' => $order_id
        ];
        return View::make('admin.orders_storage.details', $data)->renderSections()['product-table'];
    }

    /**
     * Agrega producto al pedido
     */
    public function add_product_ajax()
    {
        $store_product_id = Input::get('store_product_id');
        $order_id = Input::get('order_id');
        $store_id = Input::get('store_id');
        $quantity = Input::get('cant');
        $order = Order::findOrFail($order_id);
        $order_group = OrderGroup::findOrFail($order->group_id);

        // Cuando el pedido no esta despachado agrega un producto con el proceso normal
        if (in_array($order->status, ['Validation', 'Initiated', 'Enrutado'])) {
            // actualiza la cantidad de un producto que ya fue agregado a la orden
            if (Input::has('update_quantity') && Input::get('update_quantity')) {
                if ($order_product = OrderProduct::where('order_id', $order->id)->where('store_id',
                    $store_id)->where('store_product_id', $store_product_id)->first()) {
                    $order_product->quantity = $quantity;
                    $order_product->save();
                    if ($order_product->type == 'Agrupado') {
                        $order_product_groups = OrderProductGroup::where('order_product_id', $order_product->id)->get();
                        foreach ($order_product_groups as $order_product_group) {
                            $order_product_group->quantity = $quantity;
                            $order_product_group->save();
                        }
                    }

                    Event::fire('order.product_log', [[$order_product, Session::get('admin_id'), 'admin']]);

                    $order->updateTotals();
                    $order->save();
                    Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                    $response = [
                        'status' => true,
                        'message' => 'La cantidad del producto fue actualizada.'
                    ];
                    return Response::json($response);
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'El producto no existe.'
                    ];
                    return Response::json($response);
                }
            }

            //agrega un producto al pedido
            $order_product = OrderProduct::where('order_id', $order->id)
                ->where('store_id', $store_id)
                ->where('store_product_id', $store_product_id)
                ->count();

            if ($order_product == 0) {
                $store_product = StoreProduct::select(
                    'products.*',
                    'store_products.*',
                    'store_product_warehouses.*',
                    'store_products.id'
                )
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->join('store_product_warehouses', 'store_product_id', '=', 'store_products.id')
                    ->where('store_product_warehouses.warehouse_id', $order_group->warehouse_id)
                    ->where('store_products.id', $store_product_id)->first();

                if ($store_product->type == 'Agrupado') {
                    DB::beginTransaction();
                    $order_product = OrderProduct::addProduct($store_product, $order, $store_id, $quantity, 'Pending');
                    if (isset($order_product['status']) && isset($order_product['obj']) && $order_product['status']) {
                        $result = true;
                        if ($result) {
                            DB::commit();
                        } else {
                            DB::rollback();
                            return Response::json([
                                'status' => false,
                                'message' => 'Ocurrió un error al agregar los productos del combo.'
                            ]);
                        }
                    } else {
                        DB::rollback();
                        return Response::json([
                            'status' => $order_product['status'],
                            'message' => $order_product['message']
                        ]);
                    }
                } else {
                    $order_product = OrderProduct::addProduct($store_product, $order, $store_id, $quantity, 'Pending');
                }

                if ($order_product['status'] && $order_group->source == 'Reclamo') {
                    $order_product['obj']->price = 0;
                    $order_product['obj']->save();
                }
                if ($order_product['status']) {
                    Event::fire('order.product_log', [[$order_product['obj'], Session::get('admin_id'), 'admin']]);
                } else {
                    return Response::json([
                        'status' => $order_product['status'],
                        'message' => $order_product['message']
                    ]);
                }

                $order->updateTotals();
                $order->save();
                Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                $response = [
                    'status' => true,
                    'message' => 'Producto agregado con éxito.'
                ];
                return Response::json($response);
            } else {
                $response = [
                    'status' => false,
                    'message' => '¿El producto ya fue agregado al pedido, deseas actualizar la cantidad?'
                ];
                return Response::json($response);
            }
        } elseif ($order->status == 'Dispatched') {
            // En caso de que el pedido esté despachado,
            // se agrega el producto con status Returned
            // y con reason_returned Despachado
            $store_product = StoreProduct::select('products.*', 'store_products.*')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->where('store_products.id', $store_product_id)
                ->first();

            $order_product = OrderProduct::addProduct($store_product, $order, $store_id, $quantity, 'Returned',
                'Adicional');
            if ($order_product['status']) {
                Event::fire('order.product_log', [[$order_product['obj'], Session::get('admin_id'), 'admin']]);
            } else {
                return Response::json([
                    'status' => $order_product['status'],
                    'message' => $order_product['message']
                ]);
            }


            $order->updateTotals();
            $order->save();
            Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

            $response = [
                'status' => true,
                'message' => 'Producto agregado con éxito.'
            ];
            return Response::json($response);
        }
    }

    /**
     * Elimina producto del pedido
     *
     * @param int $order_id ID del pedido
     * @param int $order_product_id ID del producto en tabla order_products
     * @return Redirect
     */
    public function remove_product($order_id, $order_product_id)
    {
        $order = Order::find($order_id);
        $order_products = OrderProduct::where('order_id', $order_id)->count();

        if ($order_products > 1) {
            $order_product = OrderProduct::find($order_product_id);

            try {
                DB::beginTransaction();

                if ($order_product->type == 'Agrupado') {
                    OrderProductGroup::where('order_product_id', $order_product->id)
                        ->where('order_id', $order_id)
                        ->delete();
                }
                //quitar productos de sampling
                $order_product->deleteSamplingProductChild();
                $order_product->delete();
                $order->updateTotals();
                $order->save();

                Event::fire('order.product_remove_log', [[$order_product, Session::get('admin_id')]]);
                Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();

                return Redirect::back()->with('error', 'Ocurrió un error al eliminar el producto.');
            }

            return Redirect::back()->with('success', 'Producto eliminado con éxito.');
        }

        return Redirect::back()->with('error',
            'No se puede eliminar este producto, el pedido no puede quedar sin productos.');
    }

    /**
     * @description : Obtiener el listado de
     * @return mixed
     */
    public function get_orders_payment_refunds_ajax()
    {
        if (Request::ajax()) {
            $order_payment_id = Input::get('order_payment_id');
            $order_payment = OrderPayment::find($order_payment_id);
            $order = Order::find($order_payment->order_id);
            $this->update_refund_status($order);
            $order_payment_refunds = OrderPaymentRefund::select('order_payment_refunds.*',
                'order_payments.cc_charge_id', 'admin.fullname')
                ->join('order_payments', 'order_payments.id', '=', 'order_payment_refunds.order_payment_id')
                ->join('admin', 'admin.id', '=', 'order_payment_refunds.admin_id')
                ->where('order_payment_id', $order_payment_id)
                ->orderBy('order_payment_refunds.date', 'ASC')
                ->get();
            $data = [
                'order_payment_refunds' => $order_payment_refunds
            ];
            return View::make('admin.orders_storage.refunds', $data)->renderSections()['table_refunds'];
        }
    }

    /**
     * @description : Descarga de reporte de ordenes a traves de AJAX en el index
     * @return mixed
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function get_orders_report_ajax()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $where = '';
        DB::statement("SET lc_time_names = 'es_ES'");

        $search = Input::has('s') ? Input::get('s') : '';
        if (Input::get('city_id')) {
            $where .= ' WHERE c.id = ' . Input::get('city_id');
        }
        if (Input::get('warehouse_id')) {
            $where .= ' AND w.id = ' . Input::get('warehouse_id');
        }
        if (Input::get('transporter_id')) {
            $where .= ' AND tr.id = ' . Input::get('transporter_id');
        }
        if (Input::get('vehicle_id')) {
            $where .= ' AND vh.id = ' . Input::get('vehicle_id');
        }
        if (Input::get('driver_id')) {
            $where .= ' AND dr.id = ' . Input::get('driver_id');
        }
        if (Input::get('store_id')) {
            $where .= ' AND st.id = ' . Input::get('store_id');
        }
        if (Input::get('route_id')) {
            if (Input::get('route_id') != 'NULL') {
                $where .= ' AND r.id = ' . Input::get('route_id');
            }
        }
        if (Input::get('allied_stores')) {
            $where .= ' AND o.allied_store_id = ' . Input::get('allied_stores');
        }

        if (Input::get('zone_id')) {
            if (Input::get('zone_id') != 'NULL') {
                $where .= ' AND z.id = ' . Input::get('zone_id');
            }
        }
        if (Input::get('type') && !empty(Input::get('type'))) {
            $where .= " AND o.type = '" . Input::get('type') . "'";
        }
        if (Input::get('payment_method')) {
            $where .= ' AND o.payment_method = "' . Input::get('payment_method') . '" ';
        }
        if (Input::get('picker_dry_id')) {
            $where .= ' AND o.picker_dry_id = ' . Input::get('picker_dry_id');
        }
        if (Input::get('delivery_date') && Input::get('delivery_date_end')) {
            $where .= " AND DATE(o.delivery_date) >= '" . format_date('mysql', Input::get('delivery_date')) . "'";
            $where .= " AND DATE(o.delivery_date) <= '" . format_date('mysql', Input::get('delivery_date_end')) . "'";
        }

        if (Input::get('delivery_window_id')) {
            $where .= " AND o.delivery_window_id = " . Input::get('delivery_window_id');
        }
        if (Input::get('show_orders')) {
            if (Input::get('show_orders') == 'update_stock_back') {
                $where .= ' AND op.update_stock_back=1 ';
            } else {
                if (Input::get('show_orders') == 'no_paid') {
                    $where .= ' AND o.payment_date IS NULL AND o.status = "Delivered"';
                }
            }
        }
        if (Input::get('s')) {
            $where .= ' AND (o.id = ' . intval($search) . ' OR o.reference LIKE "%' . $search . '%" OR og.user_email LIKE "%' . $search . '%" OR og.user_address LIKE "%' . $search . '%" OR og.user_phone LIKE "%' . $search . '%") ';
        }
        if (Input::get('status')) {
            $where .= ' AND o.status IN(' . Input::get('status') . ')';
        }

        $sql = "SELECT
                    o.id AS PEDIDO,
                    o.user_id AS user_id,
                    o.date AS 'fecha_pedido',
                    o.status AS ESTADO,
                    CONCAT(user_firstname, ' ', user_lastname) AS CLIENTE,
                    user_email AS EMAIL,
                    CONCAT(s_dry.first_name, ' ', s_dry.last_name) AS 'ALISTADOR EN SECO',
                    CONCAT(s_cold.first_name, ' ', s_cold.last_name) AS 'ALISTADOR EN FRIO',
                    tr.fullname AS TRANSPORTADOR,
                    CONCAT(dr.first_name, ' ',dr.last_name) AS CONDUCTOR,
                    vh.plate AS 'PLACA VEHÍCULO',
                    r.route AS RUTA,
                    o.planning_route AS 'RUTA VIEJA',
                    o.planning_sequence AS SECUENCIA,
                    o.planning_duration AS TIEMPO,
                    o.planning_distance AS DISTANCIA,
                    c.city AS 'CIUDAD',
                    user_address AS DIRECCION,
                    og.user_address_latitude AS LATITUD,
                    og.user_address_longitude AS LONGITUD,
                    z.name AS ZONA,
                    user_phone AS TELEFONO,
                    st.name AS TIENDA, source AS 'ORIGEN PEDIDO',
                    total_products AS 'CANTIDAD DE PRODUCTOS',
                    SUM(IF(fulfilment_status = 'Not Available', 1, 0)) AS 'PRODUCTOS NO DISPONIBLES',
                    payment_method AS 'METODO DE PAGO',
                    cc_charge_id AS 'CARGO ID',
                    o.total_amount AS SUBTOTAL,
                    o.delivery_amount AS DOMICILIO,
                    o.discount_amount AS DESCUENTO,
                    (o.total_amount + o.delivery_amount - o.discount_amount) AS TOTAL,
                    SUM(IF (op.promo_type = 'Merqueo' OR op.promo_type IS NULL OR op.promo_type = '', (op.original_price * op.quantity) - (op.price * op.quantity), 0)) AS 'DESCUENTO PROMOCIONES MERQUEO',
                    SUM(IF (op.promo_type = 'Cliente', (op.original_price * op.quantity) - (op.price * op.quantity), 0)) AS 'DESCUENTO PROMOCIONES CLIENTE',
                    DATE_FORMAT(o.date, '%d/%m/%Y') AS 'FECHA PEDIDO',
                    DATE_FORMAT(o.date, '%H:%i') AS 'HORA PEDIDO',
                    CONCAT(UCASE(LEFT(DATE_FORMAT(o.date, '%W'), 1)), SUBSTRING(DATE_FORMAT(o.date, '%W'), 2)) AS 'DIA PEDIDO',
                    DATE_FORMAT(first_delivery_date, '%d/%m/%Y') AS 'PRIMERA FECHA DE ENTREGA',
                    DATE_FORMAT(first_delivery_date, '%H:%i') AS 'HORA PRIMERA FECHA DE ENTREGA',
                    DATE_FORMAT(allocated_date, '%d/%m/%Y') AS 'FECHA ASIGNACION',
                    DATE_FORMAT(allocated_date, '%H:%i') AS 'HORA ASIGNACION',
                    DATE_FORMAT(o.planning_date, '%d/%m/%Y') AS 'FECHA PLANEACIÓN',
                    DATE_FORMAT(o.planning_date, '%H:%i') AS 'HORA PLANEACIÓN',
                    DATE_FORMAT(o.picking_date, '%d/%m/%Y') AS 'FECHA ALISTAMIENTO',
                    DATE_FORMAT(o.picking_date, '%H:%i') AS 'HORA ALISTAMIENTO',
                    DATE_FORMAT(picking_dry_start_date, '%d/%m/%Y') AS 'FECHA DE INICIO DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_dry_start_date, '%H:%i') AS 'HORA DE INICIO DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_dry_end_date, '%d/%m/%Y') AS 'FECHA DE FIN DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_dry_end_date, '%H:%i') AS 'HORA DE FIN DE ALISTAMIENTO EN SECO',
                    DATE_FORMAT(picking_cold_start_date, '%d/%m/%Y') AS 'FECHA DE INICIO DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_cold_start_date, '%H:%i') AS 'HORA DE INICIO DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_cold_end_date, '%d/%m/%Y') AS 'FECHA DE FIN DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_cold_end_date, '%H:%i') AS 'HORA DE FIN DE ALISTAMIENTO EN FRÍO',
                    DATE_FORMAT(picking_revision_date, '%d/%m/%Y') AS 'FECHA DE REVISION DE ALISTAMIENTO',
                    DATE_FORMAT(picking_revision_date, '%H:%i') AS 'HORA DE REVISION DE ALISTAMIENTO',
                    DATE_FORMAT(dispatched_date, '%d/%m/%Y') AS 'FECHA DESPACHO',
                    DATE_FORMAT(dispatched_date, '%H:%i') AS 'HORA DESPACHO',
                    DATE_FORMAT(management_date, '%d/%m/%Y') AS 'FECHA GESTION',
                    DATE_FORMAT(management_date, '%H:%i') AS 'HORA GESTION',
                    DATE_FORMAT(delivery_date, '%d/%m/%Y') AS 'FECHA ENTREGA',
                    DATE_FORMAT(delivery_date, '%H:%i') AS 'HORA ENTREGA',
                    DATE_FORMAT(real_delivery_date, '%d/%m/%Y') AS 'FECHA ENTREGA REAL',
                    DATE_FORMAT(real_delivery_date, '%H:%i') AS 'HORA ENTREGA REAL',
                    o.delivery_time AS 'FRANJA DE ENTREGA',
                    reject_reason AS 'MOTIVO DE RECHAZO',
                    IF(product_return_storage = 1, 'Sí', 'No') AS 'CON DEVOLUCION',
                    user_score AS 'CALIFICACION',
                    user_score_comments AS 'COMENTARIO CALIFICACION',
                    o.voucher_image_url AS 'IMAGEN VOUCHER URL',
                    IF (is_checked = 1, 'Revisado', 'No revisado') AS REVISION,
                    admin.fullname AS 'USUARIO REVISION',
                    reject_comments AS 'COMENTARIOS DE CANCELACION',
                    o.invoice_number AS 'NÚMERO DE FACTURA',
                    IF(o.invoice_cancelled = 1,'ANULADA','OK') AS 'ESTADO DE FACTURA',
                    DATE_FORMAT(o.invoice_date, '%d/%m/%Y') AS 'FECHA EMISION FACTURA'
                FROM
                    order_products op
                INNER JOIN orders o ON op.order_id = o.id
                INNER JOIN stores st ON o.store_id = st.id
                INNER JOIN store_products sp ON op.store_product_id = sp.id
                INNER JOIN order_groups og ON o.group_id = og.id
                INNER JOIN warehouses w ON og.warehouse_id = w.id
                INNER JOIN cities c ON st.city_id = c.id
                LEFT JOIN pickers s_cold ON o.picker_cold_id = s_cold.id
                LEFT JOIN pickers s_dry ON o.picker_dry_id = s_dry.id
                LEFT JOIN admin ON o.revision_user_id = admin.id
                LEFT JOIN zones z ON og.zone_id = z.id
                LEFT JOIN drivers dr ON dr.id = o.driver_id
                LEFT JOIN vehicles vh ON vh.id = o.vehicle_id                
                LEFT JOIN transporters tr ON tr.id = vh.transporter_id
                LEFT JOIN routes r ON o.route_id = r.id
                " . $where . " GROUP BY o.id
                ORDER BY o.date DESC;";

        $rows = DB::select(DB::raw($sql));

        if ($n = count($rows)) {
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)) {
                while (false !== ($file = readdir($gestor))) {
                    if (strstr($file, Session::get('admin_username'))) {
                        remove_file($path . $file);
                    }
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Pedidos');
            $objPHPExcel->getProperties()->setSubject('Pedidos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb' => 'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:BX1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($rows[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach ($rows as $row) {
                $i = 0;
                foreach ($row as $key => $value) {
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    $i++;
                }
                $j++;
            }

            //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Pedidos');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username') . ' - ' . date('Y-m-d H.i.s', time()) . '.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')) . $filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));
            //eliminar archivo
            return Response::json(['status' => true, 'url' => $url]);

        } else {
            return Response::json(['status' => false]);
        }
    }

    public function remove_product_gift($id, $order_product_id)
    {
        $order = Order::find($id);
        $order_products = OrderProduct::where('order_id', $id)->count();

        if ($order_products > 1) {
            $order_product = OrderProduct::find($order_product_id);
            $order_product->delete();

            Event::fire('order.product_remove_log', [[$order_product, Session::get('admin_id')]]);
            Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

            return Redirect::back()->with('success', 'Producto de obsequio eliminado con éxito.');
        }
        return Redirect::back()->with('error',
            'No se puede eliminar este producto, el pedido no puede quedar sin productos.');
    }

    public function remove_product_sampling($id, $order_product_id)
    {
        $order = Order::find($id);
        $order_products = OrderProduct::where('order_id', $id)->count();

        if ($order_products > 1) {
            $order_product = OrderProduct::find($order_product_id);
            $order_product->delete();

            Event::fire('order.product_remove_log', [[$order_product, Session::get('admin_id')]]);
            Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

            return Redirect::back()->with('success', 'Producto de muestra eliminado con éxito.');
        }
        return Redirect::back()->with('error',
            'No se puede eliminar este producto, el pedido no puede quedar sin productos.');
    }

    /**
     * Dependiendo la accion retorna las ordenes del promotor o retorna los referidos del promotor
     *
     * @param string $reference identificador para realizar las busquedas
     * @param string $action nombre de la accion que se esta realizando
     */
    public function get_data_promoter_ajax($reference, $action)
    {
        $message = 'No se encontraron registros.';
        $status = false;
        if ($action == 'promoter-orders') {
            $data = Order::leftjoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->leftjoin('user_devices', 'order_groups.user_device_id', '=', 'user_devices.id')
                ->leftjoin('stores', 'orders.store_id', '=', 'stores.id')
                ->select('order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_address',
                    'order_groups.user_address_further',
                    'orders.type', 'order_groups.source', 'order_groups.source_os', 'order_groups.app_version',
                    'orders.date', 'stores.name',
                    'order_groups.ip', 'user_devices.regid', 'orders.id', 'orders.total_amount',
                    'orders.delivery_amount', 'orders.discount_amount', 'orders.status')
                ->where('orders.user_id', $reference)->orderByRaw('orders.created_at DESC')->limit(30)->get();
            $message = 'Se encontraron los siguientes registros para el promotor.';
            $status = true;
        } else {
            if ($action == 'referrals-of-promoter' || $action == 'referrals-this-client') {
                $data = User::leftJoin('order_groups', 'order_groups.user_id', '=', 'users.id')
                    ->leftjoin('orders', 'order_groups.id', '=', 'orders.group_id')
                    ->leftjoin('user_devices', 'order_groups.user_device_id', '=', 'user_devices.id')
                    ->select('users.id', 'users.first_name', 'users.last_name', 'users.email',
                        'users.phone',
                        DB::raw("DATE_FORMAT(users.phone_validated_date, '%d/%m/%Y %h:%i %p') AS phone_validated_date"),
                        'orders.type', 'order_groups.user_address', 'order_groups.user_address_further',
                        'order_groups.source',
                        'order_groups.source_os', 'order_groups.app_version', 'order_groups.ip', 'user_devices.regid')
                    ->groupBy('users.id')
                    ->where('referred_by', $reference)->orderByRaw('users.created_at DESC')->limit(80)->get();
                $message = 'Se encontraron los siguientes referidos.';
                $status = true;
            } else {
                if ($action == 'orders-per-address') {
                    $order = Order::with('orderGroup', 'store')->find($reference);
                    $data = OrderGroup::join('orders', 'order_groups.id', '=', 'orders.group_id')
                        ->join('user_devices', 'order_groups.user_device_id', '=', 'user_devices.id')
                        ->join('stores', 'orders.store_id', '=', 'stores.id')
                        ->join('users', 'orders.user_id', '=', 'users.id')
                        ->select('orders.id', 'orders.user_id', 'orders.date', 'stores.name', 'orders.total_amount',
                            'orders.delivery_amount', 'orders.discount_amount', 'orders.status', 'orders.type',
                            'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.source',
                            'order_groups.source_os', 'order_groups.app_version', 'order_groups.ip',
                            'user_devices.regid')
                        ->where('order_groups.user_address_latitude', $order->orderGroup->user_address_latitude)
                        ->where('order_groups.user_address_longitude', $order->orderGroup->user_address_longitude)
                        ->where('stores.id', $order->store_id)
                        ->where('stores.city_id', $order->orderGroup->user_city_id)
                        ->where('order_groups.id', '<>', $order->orderGroup->id)
                        ->groupBy('order_groups.id')
                        ->orderBy('order_groups.created_at', 'desc')
                        ->limit(30)->get();
                    $message = 'Se encontraron los siguientes pedidos para esta dirección.';
                    $status = true;
                }
            }
        }
        $return = ['status' => $status, 'message' => $message, 'result' => $data, 'action' => $action];
        return Response::json($return);
    }

    public function heatmap()
    {
        $month = 'julio';
        $city = 'bogota';
        if (Input::has('month')) {
            $month = Input::get('month');
        }
        if (Input::has('city')) {
            $city = Input::get('city');
        }

        $data = [
            'title' => 'Mapa de calor mes de ' . $month,
            'data' => Order::heatMapData($month, $city)
        ];

        return View::make('admin.report.heatmap', $data);
    }

    /**
     * Envía notificación a cliente informando que el pedido tiene productos
     * faltantes.
     *
     * @param Order $order
     * @return void
     */
    private function sendNotificationAboutMissingProductsOnOrder($order)
    {
        $customerServiceEmail = Config::get('app.admin_email');
        // template del memnsaje
        $msgTemplate = "Hola {client}, {missing-products-intro} {products}. " .
            "No te preocupes, uno de nuestros asesores se pondrá en contacto contigo " .
            "o si deseas puedes escribirnos a $customerServiceEmail.\n" .
            "Gracias por preferir Merqueo.";

        // mensaje para un solo producto
        $singularIntro = "un producto de tu pedido no fue despachado, el producto es:";
        // mesanje para varios productos
        $pluralIntro = "algunos productos de tu pedido no fueron despachados, los productos son:";
        $missedProducts = $order->orderProducts()->where('fulfilment_status', '!=', 'Fullfilled')->get();

        $patternsMap = [
            "{client}" => $order->user->first_name,
            "{missing-products-intro}" => $missedProducts->count() > 1 ? $pluralIntro : $singularIntro,
            "{products}" => implode(', ', $missedProducts->lists('product_name')),
        ];

        $msg = str_replace(array_keys($patternsMap), $patternsMap, $msgTemplate);
        $phoneNumber = $order->user->phone;

        send_sms($phoneNumber, $msg);
    }

    /**
     * Elimina los productos no disponibles la orden especificada.
     */
    public function deleteNotAvailableProducts($orderId)
    {
        $order = Order::with('orderProductsNotAvailable')->findOrFail($orderId);

        if ($order->payment_method !== 'Débito - PSE') {
            return Redirect::back()
                ->with('error', 'Esta acción está disponible sólo para pedidos pagados mediante PSE.');
        }

        $adminId = AuthBackend::user()->id;

        $log = OrderLog::create([
            'type' => 'Productos no disponibles reembolsados.',
            'admin_id' => $adminId,
            'order_id' => $order->id,
        ]);

        // si el pedido tiene tipo de validación en 6 (Pagados con PSE y con faltanes),
        // actualizamos a null, pues los faltantes fueron eliminados
        if ($order->order_validation_reason_id === 6) {
            $order->order_validation_reason_id = null;
            $order->save();
        }

        // gestionado por SAC
        Event::fire('order.managed_sac_log', [$order->id, $adminId, 6]);

        return Redirect::back()
            ->with('success', 'Productos no disponibles eliminados exitosamente.');
    }

    /**
     * Obtiene todos los productos de la orden y los productos en la nota credito
     * @param $id Order
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_products_credit_note($id)
    {
        $order = Order::findOrFail($id);
        $search = (Input::has('productName')) ? Input::get('productName') : '';
        $excludeIds = (Input::has('excludeIds')) ? json_decode(Input::get('excludeIds')) : [];

        $productSearch = $order->getOrderProducts($search, $excludeIds);
        $ProductGroupSearch = $order->getOrderProductGroup($search, $excludeIds);

        $productSearch = array_merge($productSearch->toArray(), $ProductGroupSearch->toArray());

        $productsCreditNote = $order->getOrderProducts('', [], false);
        $productGroupCreditNote = $order->getOrderProductGroup('', [], false);

        $productsCreditNote = array_merge($productsCreditNote->toArray(), $productGroupCreditNote->toArray());

        $data = [
            'productSearch' => $productSearch,
            'productsCreditNote' => $productsCreditNote,
        ];

        return Response::json($data);
    }

    /**
     * Metodo para actualizar la nota credito de los productos
     * @param $id Order
     * @return \Illuminate\Http\JsonResponse
     */
    public function order_update_credit_note($id)
    {
        $input = Input::all();
        $order = Order::findOrFail($id);

        if (is_null($order->credit_note_number)) {
            $consecutive = DB::table('consecutives')->where('type', 'credit_note_sale')->first();
            $consecutive->consecutive += 1;
            $order->credit_note_number = $consecutive->consecutive;
            $order->save();
            DB::table('consecutives')->where('type',
                'credit_note_sale')->update(['consecutive' => $consecutive->consecutive]);
            Event::fire('order.create_credit_note_log', [[$order, Session::get('admin_id')]]);
        }
        if ($input['action'] == 'edit') {
            foreach ($input['products'] as $product) {
                if ($product['type'] == 'Product') {
                    $order_product = OrderProduct::findOrFail($product['id']);
                    $order_product->quantity_credit_note = $product['quantity_calculate'];
                    $order_product->reason_credit_note = $product['reason_credit_note'];
                    $order_product->save();
                    Event::fire('order.update_credit_note_log',
                        [[$order_product, Session::get('admin_id'), 'agregado a']]);
                } else {
                    $order_product_group = OrderProductGroup::findOrFail($product['id']);
                    $order_product_group->quantity_credit_note = $product['quantity_calculate'];
                    $order_product_group->reason_credit_note = $product['reason_credit_note'];
                    $order_product_group->save();
                    Event::fire('order.update_credit_note_log',
                        [[$order_product_group, Session::get('admin_id'), 'agregado a']]);
                }
            }
        } else {
            $product = $input['products'];
            if ($product['type'] == 'Product') {
                $order_product = OrderProduct::findOrFail($product['id']);
                $order_product->quantity_credit_note = null;
                $order_product->reason_credit_note = null;
                $order_product->save();
                Event::fire('order.update_credit_note_log',
                    [[$order_product, Session::get('admin_id'), 'eliminado de']]);
            } else {
                $order_product_group = OrderProductGroup::findOrFail($product['id']);
                $order_product_group->quantity_credit_note = null;
                $order_product_group->reason_credit_note = null;
                $order_product_group->save();
                Event::fire('order.update_credit_note_log',
                    [[$order_product_group, Session::get('admin_id'), 'eliminado de']]);
            }
        }

        $order->credit_note_date = Carbon::now();
        $order->save();

        $productsCreditNote = $order->getOrderProducts('', [], false);
        $productGroupCreditNote = $order->getOrderProductGroup('', [], false);

        $productsCreditNote = array_merge($productsCreditNote->toArray(), $productGroupCreditNote->toArray());

        $resposeData = [
            'addProductsCreditNote' => [],
            'productsCreditNote' => $productsCreditNote,
        ];

        return Response::json($resposeData);
    }
}
