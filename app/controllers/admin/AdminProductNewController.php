<?php

namespace admin;

use AWS;
use AlliedStore;
use Brand;
use Carbon\Carbon;
use CartProduct;
use Category;
use City;
use Config;
use DB ;
use Department;
use Excel;
use Illuminate\Support\Facades\Redirect;
use Input;
use Maker;
use Product;
use ProductGroup;
use ProductImage;
use Provider;
use Queue;
use Request;
use Response;
use Session;
use Shelf;
use ShelvesSuggestedProducts;
use Store;
use StoreProduct;
use StoreProductGroup;
use StoreProductWarehouse;
use Str;
use Subbrand;
use Subcategory;
use Validator;
use View;
use Warehouse;

class AdminProductNewController extends AdminController
{
    /**
     * Grilla de productos
     */
    public function index()
    {
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('is_hidden', 0)->where('stores.status', 1)->get();

        if (!Request::ajax()) {
            $cities = $this->get_cities();
            return View::make('admin.products.index')->with('stores', $stores)->with('title', 'Productos')->with('cities', $cities);
        } else {
            $search_term = Input::all();
            $search = !empty($search_term['s']) ? $search_term['s'] : '';

            $products = Product::leftJoin('store_products', 'store_products.product_id', '=', 'products.id')
                ->where(function ($query) use ($search) {
                    $query->where('products.name', 'like', '%'.$search.'%');
                    $query->orWhere('products.id', 'like', '%'.$search.'%');
                    $query->orWhere('products.reference', 'like', '%'.$search.'%');
                    $query->orWhere('store_products.provider_plu', 'like', '%' . $search . '%');
                });

            if (!empty($search_term['city_id'])) {
                $stores = Store::getActiveStores($search_term['city_id']);
                $products = $products->whereHas('storeProduct.store', function ($q) use ($search_term) {
                    $q->where('city_id', '=', $search_term['city_id']);
                });
            }

            if(!empty($search_term['area'])){
                $products = $products->where($search_term['area'], 1);
            }

            if (isset($search_term['order_by'])) {
                $products->orderBy($search_term['order_by'], 'desc');
            }
            $products->groupBy('products.id');
            $products->select('products.*');
            $products = $products->paginate(20);

            $data['html'] = View::make('admin.products.index', ['products' => $products, 'stores' => $stores])->renderSections()['content'];
            $data['links_html'] = $products->links()->render();
            $data['products_count'] = $products->count();
            $data['products_total'] = $products->getTotal();

            return Response::json($data);
        }
    }

    /**
     * Editar producto
     * @param int $id
     * @return
     */
    public function edit($id = 0)
    {

        $sub_title = $id ? 'Editar Producto' : 'Nuevo Producto';

        if (!$id && Session::has('post')) {
            $product = (object) Session::get('post');
            $product->image_medium_url = 0;
        } else {
            $product = Product::find($id);
        }
        if ($id != 0) {
            $product->images = ProductImage::where('product_id', $product->id)->get();
        }
        $categories = Category::orderBy('name')->get();
        $makers = Maker::orderBy('name')->get();
        $brands = Brand::orderBy('name')->get();
        $providers = Provider::orderBy('name')->get();
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->orderBy('name')->get();
        $detail_types = [
            \ProductDetail::SPECIFICATION => \ProductDetail::SPECIFICATION
        ];
        $selected_store_id = 0;
        $selected_department_id = 0;
        $selected_shelf_id = 0;
        foreach ($stores as $store) {
            $selected_store_id = $store->id;
            break;
        }
        if ($product && $product->store_id) {
            $selected_store_id = $product->store_id;
        }
        $departments = Department::where('store_id', $selected_store_id)->orderBy('name')->get();
        foreach ($departments as $department) {
            $selected_department_id = $department->id;
            break;
        }
        if ($product && $product->department_id) {
            $selected_department_id = $product->department_id;
        }
        $shelves = Shelf::where('department_id', $selected_department_id)->orderBy('name')->get();
        foreach ($shelves as $shelf) {
            $selected_shelf_id = $shelf->id;
            break;
        }
        $subcategories = null;
        if ($product && !is_null($product->category_id)) {
            $subcategories = Subcategory::where('category_id', $product->category_id)->orderBy('name')->get();
        }
        $subbrands = null;
        if ($product && !is_null($product->brand_id)) {
            $subbrands = Subbrand::where('brand_id', $product->brand_id)->orderBy('name')->get();
        }

        $data = [
            'title' => 'Producto',
            'sub_title' => $sub_title,
            'product' => $product,
            'brands' => $brands,
            'subbrands' => $subbrands,
            'stores' => $stores,
            'departments' => $departments,
            'shelves' => $shelves,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'makers' => $makers,
            'providers' => $providers,
            'selected_store_id' => $selected_store_id,
            'selected_department_id' => $selected_department_id,
            'selected_shelf_id' => $selected_shelf_id,
            'detail_types' => $detail_types,
            'fields_permissions' => Product::getStatusFields($this->admin_permissions)
        ];

        return View::make('admin.products.product_form', $data);
    }

    /**
     * Guardar producto
     */
    public function save()
    {
        
        $id = Input::get('id');

        if ($product = Product::where('reference', Input::get('reference'))->where('id', '<>', $id)->first()) {
            return Redirect::back()->with('error', 'Ya existe un producto con la misma referencia.')->withInput();
        }

        $product = Product::find($id);
        $product_slug = Input::get('slug');
        $default_image = 0;
        $is_new = 0;
        if (!$product) {
            $product = new Product;
            $is_new = 1;
            if( !$this->admin_permissions['permission1']  && !$this->admin_permissions['permission2'] && !$this->admin_permissions['permission3']){
                if (empty($product_slug)) {
                    $product->name = trim(str_replace('"', '', Input::get('name')));
                    $product->quantity = Input::get('quantity');
                    $product->unit = Input::get('unit');
                    $product->slug = $product->getSlug();
                } else {
                    $product->slug = Str::slug(trim($product_slug));
                }
                if (Product::where('slug', $product->slug)->first()) {
                    return Redirect::back()->with('error', 'Ya existe un producto con el mismo slug.')->withInput();
                }
            }

            $default_image = 1;
            $action = 'insert';
        } else {
            if (Input::has('type')) {
                if( !$this->admin_permissions['permission1']  && !$this->admin_permissions['permission2'] && !$this->admin_permissions['permission3']){
                    $product->type = (Input::get('type') == '' ? 'Simple' : Input::get('type'));
                    if ($product->type == 'Agrupado') {
                        $product_groups = ProductGroup::where('product_id', $product->id)->count();
                        if ($product_groups == 0) {
                            return Redirect::back()->with('error', 'No se han agregado productos.');
                        }
                    } elseif ($product->type == 'Simple') {
                        $product_groups = ProductGroup::where('product_id', $product->id)->get();
                        $store_products = StoreProduct::where('product_id', $product->id)->get();
                        foreach ($product_groups as $product_group) {
                            foreach ($store_products as $store_product) {
                                $store_product_groups = StoreProductGroup::where('store_product_id', $store_product->id)->get();
                                foreach ($store_product_groups as $store_product_group) {
                                    $store_product_group->delete();
                                }
                            }
                            if ($store_product_group) {
                                $store_product_group->delete();
                            }
                            $product_group->delete();
                        }
                    }
                    $product->provider_type_product = Input::get('provider_type_product');
                }

                $product->save();

                return Redirect::back()->with('success', 'Se actualizó el tipo del producto.');
            }
            if( !$this->admin_permissions['permission1']  && !$this->admin_permissions['permission2'] && !$this->admin_permissions['permission3']){
                if (empty($product_slug)) {
                    $product->name = str_replace('"', '', Input::get('name'));
                    $product->quantity = Input::get('quantity');
                    $product->unit = Input::get('unit');
                    $product->slug = $product->getSlug();
                } else {
                    if ($product_slug != $product->slug) {
                        $product->slug = Str::slug(trim($product_slug));
                        if (Product::where('slug', $product->slug)->where('id', '<>', $id)->first()) {
                            return Redirect::back()->with('error', 'Ya existe un producto con el mismo slug.')->withInput();
                        }
                    }
                }
            }

            $action = 'update';
        }
        if( !$this->admin_permissions['permission1']  && !$this->admin_permissions['permission2'] && !$this->admin_permissions['permission3']){
            $product->reference = Input::get('reference');
            $product->name = trim(str_replace('"', '', Input::get('name')));
            $product->quantity = Input::get('quantity');
            $product->unit = Input::get('unit');
            $product->presentation = Input::get('presentation');
            $product->subpresentation = Input::get('subpresentation');
            $product->brand_id = Input::get('brand_id');
            $product->subbrand_id = Input::get('subbrand_id');
            $product->maker_id = Input::get('maker_id');
            $product->category_id = Input::get('category_id');
            $product->subcategory_id = Input::get('subcategory_id');
            $product->description = empty(Input::get('description')) ? '' : Input::get('description');
            $product->nutrition_facts = empty(Input::get('nutrition_facts')) ? '' : Input::get('nutrition_facts');
            $product->height = Input::get('height');
            $product->width = Input::get('width');
            $product->length = Input::get('length');
            $product->has_barcode = Input::get('has_barcode');
            $product->is_perishable = Input::get('is_perishable');
            $product->can_mix = Input::get('can_mix');
            if ($product->type == 'Simple') {
                $product_groups = ProductGroup::where('product_id', $product->id)->get();
                $store_products = StoreProduct::where('product_id', $product->id)->get();
                foreach ($product_groups as $product_group) {
                    foreach ($store_products as $store_product) {
                        $store_product_group = StoreProductGroup::where('store_product_id', $store_product->id)->where('store_product_group_id', $product_group->id)->first();
                        $store_product_group->delete();
                    }
                }
            }
            $image = Input::file('image');
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png')
            );
            if (Input::hasFile('image') && $validator->fails()) {
                return Redirect::back()->with('error', 'Imagen invalida.')->withInput();
            } else {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                if (Input::hasFile('image')) {
                    //eliminar imagen si existe
                    if (!empty($id) && !empty($product->image_large_url) && !strstr($product->image_large_url, 'no_disponible')) {
                        $absolute_path_images = Config::get('app.absolute_path_images');
                        if (!empty($product->image_large_url)) {
                            $path = str_replace($cloudfront_url.'images/', '', $product->image_large_url);
                            if (file_exists($absolute_path_images.'/'.$path)) {
                                remove_file($absolute_path_images.'/'.$path);
                                remove_file_s3($product->image_large_url);
                            }
                        }
                        if (!empty($product->image_medium_url)) {
                            $path = str_replace($cloudfront_url.'images/', '', $product->image_medium_url);
                            if (file_exists($absolute_path_images.'/'.$path)) {
                                remove_file($absolute_path_images.'/'.$path);
                                remove_file_s3($product->image_medium_url);
                            }
                        }
                        if (!empty($product->image_app_url)) {
                            $path = str_replace($cloudfront_url.'images/', '', $product->image_app_url);
                            if (file_exists($absolute_path_images.'/'.$path)) {
                                remove_file($absolute_path_images.'/'.$path);
                                remove_file_s3($product->image_app_url);
                            }
                        }
                        if (!empty($product->image_small_url)) {
                            $path = str_replace($cloudfront_url.'images/', '', $product->image_small_url);
                            if (file_exists($absolute_path_images.'/'.$path)) {
                                remove_file($absolute_path_images.'/'.$path);
                                remove_file_s3($product->image_small_url);
                            }
                        }
                    }
                    $file = Input::file('image');
                    $url = upload_image($file, true);
                    $product->image_large_url = $url['large'];
                    $product->image_medium_url = $url['medium'];
                    $product->image_app_url = $url['app'];
                    $product->image_small_url = $url['small'];
                } else {
                    if ($default_image) {
                        $product->image_large_url = $cloudfront_url.'/images/no_disponible.jpg';
                        $product->image_medium_url = $cloudfront_url.'/images/no_disponible.jpg';
                        $product->image_app_url = $cloudfront_url.'/images/no_disponible.jpg';
                        $product->image_small_url = $cloudfront_url.'/images/no_disponible.jpg';
                    }
                }
                $product->save();
            }
            if (Input::hasFile('images') && count(Input::file('images')) > 0) {
                $images = Input::file('images');
                foreach ($images as $index => $value) {
                    $item_image = $value;
                    $product_image = new ProductImage();
                    $saved_images = ProductImage::where('product_id', $id)->get();
                    $validator = Validator::make(
                        array('ima' => $item_image),
                        array('ima' => 'required|mimes:jpeg,bmp,png')
                    );
                    if ($item_image && $validator->fails()) {
                        return Redirect::back()->with('error', 'Imagen invalida.')->withInput();
                    } else {
                        $cloudfront_url = Config::get('app.aws.cloudfront_url');
                        if ($item_image && isset($saved_images[$index])) {
                            if ($saved_images[$index]) {
                                //eliminar imagen si existe
                                if (!empty($saved_images[$index]->image_large_url) && !strstr($saved_images[$index]->image_large_url, 'no_disponible')) {
                                    $absolute_path_images = Config::get('app.absolute_path_images');
                                    if (!empty($saved_images[$index]->image_large_url)) {
                                        $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_large_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            remove_file_s3($saved_images[$index]->image_large_url);
                                        }
                                    }
                                    if (!empty($saved_images[$index]->image_medium_url)) {
                                        $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_medium_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            remove_file_s3($saved_images[$index]->image_medium_url);
                                        }
                                    }
                                    if (!empty($saved_images[$index]->image_app_url)) {
                                        $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_app_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            remove_file_s3($saved_images[$index]->image_app_url);
                                        }
                                    }
                                    if (!empty($saved_images[$index]->image_small_url)) {
                                        $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_small_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            remove_file_s3($saved_images[$index]->image_small_url);
                                        }
                                    }
                                }
                            }
                            $file = $item_image;
                            $url = upload_image($file, true);
                            $saved_images[$index]->image_large_url = $url['large'];
                            $saved_images[$index]->image_medium_url = $url['medium'];
                            $saved_images[$index]->image_app_url = $url['app'];
                            $saved_images[$index]->image_small_url = $url['small'];
                            $saved_images[$index]->save();
                        } else {
                            $file = $item_image;
                            $url = upload_image($file, true);
                            $product_image->image_large_url = $url['large'];
                            $product_image->image_medium_url = $url['medium'];
                            $product_image->image_app_url = $url['app'];
                            $product_image->image_small_url = $url['small'];
                            $product_image->product_id = $product->id;
                        }
                        $product_image->save();
                    }
                }
            }
        }elseif($this->admin_permissions['permission2']){
            $product->accounting_account = Input::get('accounting_account');
            $product->accounting_account_type = Input::get('accounting_account_type');
            $product->accounting_line = Input::get('accounting_line');
            $product->accounting_group = Input::get('accounting_group');
            $product->accounting_code = Input::get('accounting_code');
            $product->status_accounting = 1;
            $product->save();
            $this->admin_log('products', $product->id, $action);
            return Response::json(['result' => '', 'status' => 200, 'message' => 'Datos guardados correctamente.'], 200);
        }

        if (!Config::get('app.debug')) {
            Queue::push('UpdateIndex', array('productId' => $product->id));
        }

        if ($is_new) {
            return Redirect::route('adminProducts.edit', ['id' => $product->id]);
        }
        return Redirect::back()->with('success', 'Producto guardado con éxito.');
    }

    /**
     * Eliminar imagen del producto
     */
    public function delete_product_image_ajax()
    {
        $message = 'Error al eliminar imagen del producto';
        $status = 400;
        if (Input::has('product_image_id')) {
            $product_image = ProductImage::find(Input::get('product_image_id'));
            if (!empty($product_image->image_large_url)) {
                remove_file_s3($product_image->image_large_url);
            }
            if (!empty($product_image->image_medium_url)) {
                remove_file_s3($product_image->image_medium_url);
            }
            if (!empty($product_image->image_app_url)) {
                remove_file_s3($product_image->image_app_url);
            }
            if (!empty($product_image->image_small_url)) {
                remove_file_s3($product_image->image_small_url);
            }
            $this->admin_log('product_images', $product_image->id, 'delete');
            DB::delete('DELETE FROM product_images WHERE id = '.$product_image->id);
            $message = 'Imagen de producto eliminada con exito';
            $status = 200;
        }
        return Response::json(['result' => ['product_image' => 'Eliminar imagen de producto'], 'status' => $status, 'message' => $message], $status);
    }

    /**
     * Eliminar producto
     */
    public function delete($id)
    {
        return false;
        if ($product = Product::find($id)) {
            //eliminar imagen si existe
            if (!empty($product->image_large_url) && !strstr($product->image_large_url, 'no_disponible')) {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                $absolute_path_images = Config::get('app.absolute_path_images');
                if (!empty($product->image_large_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_large_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_large_url);
                        remove_file_s3($path);
                    }
                }
                if (!empty($product->image_medium_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_medium_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_medium_url);
                        remove_file_s3($path);
                    }
                }
                if (!empty($product->image_app_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_app_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_app_url);
                        remove_file_s3($path);
                    }
                }
                if (!empty($product->image_small_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_small_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_small_url);
                        remove_file_s3($path);
                    }
                }
            }
            CartProduct::where('product_id', $id)->delete();
            Product::where('id', $id)->delete();
            $this->admin_log('products', $product->id, 'delete');

            if (!Config::get('app.debug')) {
                Queue::push('PruneIndex', array('productId' => $product->id));
            }

            return Redirect::route('adminProducts.index')->with('type', 'success')->with('message', 'Producto eliminado con éxito.');
        }
        return Redirect::route('adminProducts.index')->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar  el producto.');
    }


    /**
     * Funcion para crear categorías desde la página de productos
     */
    public function add_category_ajax()
    {
        $new_category = trim(Input::get('new_category', null));
        $category_obj = Category::where('name', $new_category)->first();
        if (!empty($category_obj)) {
            return Response::json(['status' => false, 'message' => 'La categoría ya existe.']);
        } else {
            $category_obj = new Category;
            $category_obj->name = $new_category;
            $category_obj->save();
            $categories = Category::orderBy('name')->get();
            $data = [
                'categories' => $categories
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['categories'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Funcion para crear marcas desde la página de productos
     */
    public function add_brand_ajax()
    {
        $new_brand = trim(Input::get('new_brand', null));
        $brand_obj = Brand::where('name', $new_brand)->first();
        if (!empty($brand_obj)) {
            return Response::json(['status' => false, 'message' => 'La marca ya existe.']);
        } else {
            $brand_obj = new Brand;
            $brand_obj->name = $new_brand;
            $brand_obj->save();
            $brands = Brand::orderBy('name')->get();
            $data = [
                'brands' => $brands
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['brands'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Agregar productos a grupo de producto
     */
    public function add_product_group_ajax()
    {
        $product_id = Input::get('product_id');
        $product_group_id = Input::get('product_group_id');
        $quantity = Input::get('quantity');
        $price = Input::get('price');
        $type = Input::get('type');

        $product_obj = Product::find($product_id);
        if ($product_obj->type != $type) {
            $product_obj->type = $type;
            $this->admin_log('products', $product_obj->id, 'update_type');
            $product_obj->save();
            if ($product_obj->type == 'Simple') {
                $product_group = ProductGroup::where('product_id', $product_obj->id)->delete();
            }
        }


        $product_group = ProductGroup::where('product_id', $product_id)->where('product_group_id', $product_group_id)->first();
        if (empty($product_group)) {
            $product_group = new ProductGroup;
            $product_group->product_id = $product_id;
            $product_group->product_group_id = $product_group_id;
            $product_group->quantity = $quantity;
            $product_group->save();
            $this->admin_log('product_group', $product_group->id, 'insert');

            $store_products = StoreProduct::where('product_id', $product_id)->get();
            if (!empty($store_products)) {
                foreach ($store_products as $store_product) {
                    $store_product_gp = StoreProduct::where('product_id', $product_group->product_group_id)->where('store_id', $store_product->store_id)->first();
                    $store_product_groups = StoreProductGroup::where('store_product_id', $store_product->id)->where('store_product_group_id', $store_product_gp->id)->first();
                    if (empty($store_product_groups)) {
                        $store_product_group = new StoreProductGroup;
                        $store_product_group->store_product_id = $store_product->id;
                        $store_product_group->store_product_group_id = $store_product_gp->id;
                        $store_product_group->quantity = $product_group->quantity;
                        $store_product_group->price = $store_product->price;
                        $store_product_group->save();
                        $this->admin_log('store_product_group', $store_product_group->id, 'insert');
                    }
                    $store_product->calculatePriceOnGroup()->save();
                }
            }

            $data = [
                'status' => true,
                'message' => 'El producto agregado exitosamente.'
            ];
            return Response::json($data);
        } else {
            $data = [
                'status' => false,
                'message' => 'Este producto fue agregado anteriormente.'
            ];
            return Response::json($data);
        }
    }

    /**
     * Eliminar productos del grupo de producto
     */
    public function remove_product_group_ajax()
    {
        $product_id = Input::get('product_id');
        $product_group_id = Input::get('product_group_id');
        $type = Input::get('type');

        try {
            \DB::beginTransaction();
            $product_obj = Product::find($product_id);
            if ($product_obj->type != $type) {
                $product_obj->type = $type;
                $product_obj->save();
                if ($product_obj->type == 'Simple') {
                    $store_products = StoreProduct::where('product_id', $product_id)->get();
                    $product_groups = ProductGroup::where('product_id', $product_obj->id)->get();

                    foreach ($product_groups as $product_group) {
                        foreach ($store_products as $store_product) {
                            $store_product_gp = StoreProduct::where(
                                'product_id',
                                $product_group->product_group_id
                            )->where('store_id', $store_product->store_id)->first();
                            if (!empty($store_product_gp)) {
                                $store_product_groups = StoreProductGroup::where(
                                    'store_product_id',
                                    $store_product->id
                                )->where(
                                        'store_product_group_id',
                                    $store_product_gp->id
                                    )->first();
                                if (!empty($store_product_groups)) {
                                    $store_product_groups->delete();
                                }
                            }
                            $store_product->calculatePriceOnGroup()->save();
                        }
                    }
                }
            }

            $product_group = ProductGroup::where('product_id', $product_id)
                ->where('product_group_id', $product_group_id)
                ->first();
            if (!empty($product_group)) {
                $store_products = StoreProduct::where('product_id', $product_id)->get();
                foreach ($store_products as $store_product) {
                    if (!empty($store_product->special_price)) {
                        throw new \Exception('Debes remover la promoción del producto para actualizar el combo.');
                    }

                    $store_product_gp = StoreProduct::where('product_id', $product_group->product_group_id)
                        ->where('store_id', $store_product->store_id)
                        ->first();

                    if (!empty($store_product_gp)) {
                        $store_product_groups = StoreProductGroup::where('store_product_id', $store_product->id)
                            ->where('store_product_group_id', $store_product_gp->id)
                            ->first();
                        if (!empty($store_product_groups)) {
                            if (!empty($store_product_groups->discount_amount)) {
                                throw new \Exception('Debes remover las promociones del producto para actualizar el combo.');
                            }
                            $this->admin_log('store_product_group', $store_product_gp->id, 'delete');
                            $store_product_groups->delete();
                        }
                    }
                    $store_product->calculatePriceOnGroup()->save();
                }
                if ($product_group->delete()) {
                    $data = [
                        'status' => true,
                        'message' => 'El producto fue borrado exitosamente'
                    ];
                } else {
                    $data = [
                        'status' => false,
                        'message' => 'Ocurrió un error al borrar el producto'
                    ];
                }
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Ocurrió un error, no se encontró el producto'
                ];
            }

            \DB::commit();
        } catch (\Exception $exception) {
            \DB::rollback();

            $data = [
                'status' => false,
                'message' => $exception->getMessage()
            ];
        }

        return Response::json($data);
    }

    /**
     * Funcion para obtener los productos relacionados
     */
    public function get_product_group_ajax()
    {
        $product_id = Input::get('product_id');
        // $product_group = ProductGroup::where('product_id', $product_id)->get();
        $product_group = Product::join('product_group', 'products.id', '=', 'product_group.product_group_id')
                            ->where('product_group.product_id', $product_id)
                            ->select(
                                'products.id',
                                'products.reference',
                                'products.name',
                                'products.quantity',
                                'products.unit',
                                'products.image_small_url',
                                'product_group.quantity AS product_group_quantity',
                                'product_group.price AS product_group_price'
                            )
                            ->get();
        $data = [
            'product_group' => $product_group
        ];
        return View::make('admin.products.product_form', $data)->renderSections()['product_group'];
    }

    /**
     * Guardad producto sugerido desde admin
    */
    public function save_suggested_product()
    {
        $input = Input::all();
        //Valida campos requeridos
        $validator = Validator::make($input,[
            'promo' => 'required',
            'priority' => 'required',
            'status' => 'required',
            'store_product_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        //redirige si incumple las reglas
        if($validator->fails()) {
            return Redirect::back()->with('type', 'error')->with('message', $validator->errors())->withInput();
        }
        //obtiene los campos
        $promo = $input['promo'];
        $priority = $input['priority'];
        $status = $input['status'];
        $store_product_id = $input['store_product_id'];
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $shelves_suggested_products = Session::get('admin_shelves_suggested_products');

        if ($start_date < date("Y/m/d")) {
            return Redirect::back()->with('type', 'error')->with('message', 'La fecha inicial debe ser mayor o igual a la fecha actual.')->withInput();
        }

        if ($start_date > $end_date) {
            return Redirect::back()->with('type', 'error')->with('message', 'La fecha inicial debe ser mayor a la fecha final.')->withInput();
        }
        if (!isset($input['update'])) {
            $exist_storeProduct_or_promo = ShelvesSuggestedProducts::where('store_product_id', $store_product_id)->orWhere('promo', $promo)->first();
            if(!empty($exist_storeProduct_or_promo)) {
                return Redirect::back()->with('type', 'error')->with('message', 'El producto sugerido ya existe.')->withInput();
            }
        }

        //Actualiza el producto sugerido
        if (isset($input['update'])) {
            $suggested_products = ShelvesSuggestedProducts::where('store_product_id', $store_product_id)->where('promo', $promo)->get();
            foreach ($suggested_products as $suggested_product) {
                $suggested_product->priority = $priority;
                $suggested_product->start_date = $start_date;
                $suggested_product->end_date = $end_date;
                $suggested_product->status = $status;
                $suggested_product->save();
            }
        }
        if ($shelves_suggested_products) {
            if (count($shelves_suggested_products) > 0) {
                foreach ($shelves_suggested_products as $key => $value) {
                    foreach ($value['shelves'] as $item_key => $item_value) {
                        if (isset($input['update'])) {
                            $exist_shelf_id = ShelvesSuggestedProducts::where('store_product_id', $store_product_id)->where('promo', $promo)->where('shelf_id', $item_value)->first();
                        }
                        if(empty($exist_shelf_id)) {
                            $suggested_product = new ShelvesSuggestedProducts();
                            $suggested_product->shelf_id = $item_value;
                            $suggested_product->store_product_id = $store_product_id;
                            $suggested_product->promo = $promo;
                            $suggested_product->priority = $priority;
                            $suggested_product->start_date = $start_date;
                            $suggested_product->end_date = $end_date;
                            $suggested_product->status = $status;
                            $suggested_product->save();
                        }
                    }
                }
            } else {
                if (!isset($input['update'])) {
                    return Redirect::back()->with('type', 'error')->with('message', 'Debe seleccionar más de un pasillo para el producto sugerido.')->withInput();
                } else {
                    return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido actualizado con éxito.');
                }
            }
        } else {
            if (!isset($input['update'])) {
                return Redirect::back()->with('type', 'error')->with('message', 'Debe seleccionar más de un pasillo para el producto sugerido.');
            } else {
                return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido actualizado con éxito.');
            }
        }
        Session::forget('admin_shelves_suggested_products');
        if (!isset($input['update'])) {
            return Redirect::route('adminProducts.editSuggestedProduct', ['id' => $suggested_product->store_product_id, 'promo' => $suggested_product->promo])
                ->with('type', 'success')
                ->with('message', 'Producto sugerido creado con éxito.');
        } else {
            return Redirect::route('adminProducts.editSuggestedProduct', ['id' => $suggested_product->store_product_id, 'promo' => $suggested_product->promo])
                ->with('type', 'success')
                ->with('message', 'Producto sugerido actualizado con éxito.');
        }
        /*return Redirect::route('adminProducts.getSuggestedProducts')
            ->with('type', 'success')
            ->with('message', 'Producto sugerido creado con éxito.');*/
    }

    /**
     * Obtiene los productos por store_id en ajax
     */
    public function get_products_by_store_ajax()
    {
        $products = [];
        if (Input::has('store_id')) {
            $products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', Input::get('store_id'))
                ->select('store_products.*', 'products.name')
                ->orderBy('products.name', 'ASC')
                ->get();
            return Response::json(['result' => ['products' => $products], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['products' => $products], 'message'=> '', 'status' => '400'], 400);
        }
    }

    /**
     * Obtiene los departamentos por store_id en ajax
     */
    public function get_departments_by_store_ajax()
    {
        $departments = [];
        if (Input::has('store_id')) {
            $departments = Department::where('store_id', Input::get('store_id'))
                ->select('id', 'name')
                ->orderBy('name', 'ASC')
                ->get();
            return Response::json(['result' => ['departments' => $departments], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['departments' => $departments], 'message'=> '', 'status' => '400'], 400);
        }
    }

    public function get_shelves_by_department_ajax()
    {
        $shelves = [];
        if (Input::has('department_id')) {
            $shelves = Shelf::where('status', 1)
                ->where('department_id', Input::get('department_id'))
                ->orderBy('name', 'ASC')
                ->get();
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '400'], 400);
        }
    }

    public function add_shelves_suggested_products_ajax()
    {
        $shelves = [];
        Session::forget('admin_shelves_suggested_products');
        if (Input::has('shelves_suggested_products')) {
            $shelves_suggested_products = Input::get('shelves_suggested_products');
            Session::put('admin_shelves_suggested_products', $shelves_suggested_products);
            foreach ($shelves_suggested_products as $key => $item) {
                $shelves_obj = [];
                $dpt = Department::select('name')->where('id', $item['department_id'])->first();
                $shelves[$key]['department'] = $dpt->name;
                foreach ($item['shelves'] as $item_key => $item_shelf) {
                    $shelf = Shelf::find($item_shelf);
                    $shelves_obj[$item_key] = $shelf->name;
                }
                $shelves[$key]['items'] = $shelves_obj;
            }
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '400'], 400);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete_suggested_products($id)
    {
        if ($id) {
            DB::delete('DELETE FROM shelves_suggested_products WHERE store_product_id = '.$id);
            $this->admin_log('shelves_suggested_products', $id, 'delete');
            return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido eliminado con éxito.');
        } else {
            return Redirect::back()->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar el producto sugerido.');
        }
    }

    /**
     * Listado de productos sugeridos
     */
    public function get_suggested_products()
    {
        $suggested_products = ShelvesSuggestedProducts::join('store_products', 'store_products.id', '=', 'shelves_suggested_products.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->select('shelves_suggested_products.id', 'products.name', DB::raw('count(shelves_suggested_products.shelf_id) AS shelf'), 'promo', 'shelves_suggested_products.start_date', 'shelves_suggested_products.end_date', 'shelves_suggested_products.status', 'store_product_id', 'priority')
            ->groupBy('promo', 'store_product_id')
            ->orderBy('shelves_suggested_products.status', 'DESC')
            ->orderBy('priority', 'ASC')
            /*->limit(80)*/->get();

        return View::make('admin.products.suggested.index')->with('title', 'Productos Sugeridos En Carrito')->with('suggested_products', $suggested_products);
    }

    /**
     * Formulario para crear o editar productos sugeridos
     */
    public function edit_suggested_product()
    {
        Session::forget('admin_shelves_suggested_products');
        $cities = $this->get_cities();
        $suggested_product = [];
        if(!Input::has('id')) {
            $title = 'Agregar Nuevo Producto Sugerido';
            $store = Store::where('status', 1)->where('city_id', Session::get('admin_city_id'))->first();
            $departments = Department::where('status', 1)->where('store_id', $store->id)->get();
            //$shelves = Shelf::where('status', 1)->where('store_id', $store->id)->get();
            $shelves = [];
            return View::make('admin.products.suggested.form')
                ->with('title', $title)
                ->with('suggested_product', $suggested_product)
                ->with('departments', $departments)
                ->with('shelves', $shelves)
                ->with('cities', $cities);
        } else {
            $input =Input::all();
            $suggested_product = ShelvesSuggestedProducts::where('store_product_id', $input['id'])->where('promo', $input['promo'])->first();
            $suggested_products = ShelvesSuggestedProducts::where('store_product_id', $input['id'])->where('promo', $input['promo'])->get();
            $title = 'Editar Producto Sugerido';
            $products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', $suggested_product->shelf->store->id)
                ->select('store_products.*', 'products.name')
                ->orderBy('products.name', 'ASC')
                ->get();
            $stores = Store::where('status', 1)->where('city_id', $suggested_product->shelf->store->city->id)->get();
            $departments = Department::where('status', 1)->where('store_id', $suggested_product->shelf->store->id)->get();
            $shelves = Shelf::where('status', 1)->where('department_id', $suggested_product->shelf->department->id)->get();
            return View::make('admin.products.suggested.form')
                ->with('title', $title)
                ->with('suggested_product', $suggested_product)
                ->with('suggested_products', $suggested_products)
                ->with('stores', $stores)
                ->with('products', $products)
                ->with('departments', $departments)
                ->with('shelves', $shelves)
                ->with('cities', $cities);
        }
    }

    /**
     * Activar producto sugerido
     * @param $id
     * @return
     */
    public function toggle($id)
    {
        $suggested_products = ShelvesSuggestedProducts::where('store_product_id', $id)->first();
        if ($suggested_products) {
            $suggested_products->status = ($suggested_products->status + 1) % 2;
            if ($suggested_products->status) {
                DB::update('UPDATE shelves_suggested_products SET status = 1 WHERE store_product_id = '.$id);
            } else {
                DB::update('UPDATE shelves_suggested_products SET status = 0 WHERE store_product_id = '.$id);
            }

            return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido actualizado con éxito.');
        } else {
            return Redirect::back()->with('type', 'failed')->with('message', 'Ocurrió un error al actualizar el producto sugerido.');
        }
    }

    /**
     * Obtener los productos de la tienda
     */
    public function get_store_product_ajax()
    {
        $store_id = Input::get('store_id');
        $product_id = Input::get('product_id');
        $store = Store::with('city.warehouses')->find($store_id);
        $product = Product::find($product_id);
        $store_product = StoreProduct::where('store_id', $store_id)->where('product_id', $product_id)->first();
        $allied_stores = AlliedStore::where('city_id', $store->city_id)->where('status', 1)->get();
        if (!empty($store_product->delivery_discount_start_date)) {
            $store_product->delivery_discount_start_date = Carbon::parse($store_product->delivery_discount_start_date)->format('d/m/Y H:i:s');
        }
        if (!empty($store_product->special_price_expiration_date)) {
            $store_product->org_special_price_expiration_date = $store_product->special_price_expiration_date;
            $store_product->special_price_expiration_date = Carbon::parse($store_product->special_price_expiration_date)->format('d/m/Y H:i:s');
        }
        if (!empty($store_product->special_price_starting_date)) {
            $store_product->org_special_price_starting_date = $store_product->special_price_starting_date;
            $store_product->special_price_starting_date = Carbon::parse($store_product->special_price_starting_date)->format('d/m/Y H:i:s');
        }
        if (!empty($store_product)) {
            $departments = Department::where('status', 1)->where('store_id', $store_id)->get();
            $departments = $departments->each(function (&$department) use ($store_product) {
                if ($department->id == $store_product->department_id) {
                    $department->selected = true;
                    return;
                }
            });

            $shelves = Shelf::where('status', 1)->where('store_id', $store_id)->where('department_id', $store_product->department_id)->get();
            $shelves = $shelves->each(function (&$shelf) use ($store_product) {
                if ($shelf->id == $store_product->shelf_id) {
                    $shelf->selected = true;
                    return;
                }
            });

            $providers = Provider::where('status', 1)->get();
            $providers = $providers->each(function (&$provider) use ($store_product) {
                if ($provider->id == $store_product->provider_id) {
                    $provider->selected = true;
                    return;
                }
            });

            $data = [
                'store_product' => $store_product,
                'departments' => $departments,
                'shelves' => $shelves,
                'providers' => $providers,
                'store_products' => true,
                'store_id' => $store_id,
                'product_id' => $product_id,
                'product' => $product,
                'allied_stores' => $allied_stores,
                'warehouses' => $store->city->warehouses,
                'fields_permissions' => Product::getStatusFields($this->admin_permissions)
            ];

            if ($product->type == 'Agrupado' || $product->type == 'Proveedor') {
                $product_groups = ProductGroup::where('product_id', $product_id)->get();
                $product_groups_ids = $product_groups->lists('product_group_id');
                if (count($product_groups)) {
                    $products = Product::whereIn('products.id', $product_groups_ids)
                                            ->join('store_products', 'products.id', '=', 'store_products.product_id')
                                            ->join('product_group', function ($join) use ($product_id) {
                                                $join->on('products.id', '=', 'product_group.product_group_id')
                                                ->where('product_group.product_id', '=', $product_id);
                                            })
                                            ->join('store_product_group', function ($join) use ($store_product) {
                                                $join->on('store_products.id', '=', 'store_product_group.store_product_group_id')
                                                ->where('store_product_group.store_product_id', '=', $store_product->id);
                                            })
                                            ->where('store_id', $store_id)
                                            ->select(
                                                    'products.id',
                                                    'products.image_small_url',
                                                    'products.reference',
                                                    'products.name',
                                                    'products.quantity',
                                                    'products.unit',
                                                    'store_products.id AS store_product_id',
                                                    'store_products.product_id',
                                                    'product_group.price',
                                                    'product_group.quantity as group_quantity',
                                                    'product_group.price as group_price',
                                                    'store_product_group.id as store_product_group_id',
                                                    'store_product_group.quantity as store_product_group_quantity',
                                                    'store_products.price as store_product_group_price'
                                                )
                                            ->whereNotNull('store_product_group.id')
                                            ->get();

                    if (count($product_groups) != count($products)) {
                        $store_product_group_ids = $products->lists('product_id');

                        $products = Product::whereIn('products.id', $product_groups_ids)
                                                ->join('store_products', 'products.id', '=', 'store_products.product_id')
                                                ->select(
                                                    'products.id',
                                                    'products.image_small_url',
                                                    'products.reference',
                                                    'products.name',
                                                    'products.quantity',
                                                    'products.unit',
                                                    'store_products.id AS store_product_id',
                                                    'store_products.product_id',
                                                    'store_products.price as store_product_group_price'
                                                )
                                                ->get();

                        $products = $products->filter(function ($product) use ($store_product_group_ids) {
                            return !in_array($product->product_id, $store_product_group_ids);
                        });
                        $data['store_product_group_errors'] = $products;
                    } else {
                        $data['store_product_groups'] = $products;
                    }
                }
            }

            return View::make('admin.products.product_form', $data)->renderSections()['store_products'];
        }

        $departments = Department::where('status', 1)->where('store_id', $store_id)->get();
        $providers = Provider::where('status', 1)->get();

        $data = [
            'store_product' => $store_product,
            'departments' => $departments,
            'providers' => $providers,
            'store_products' => true,
            'store_id' => $store_id,
            'product_id' => $product_id,
            'allied_stores' => $allied_stores,
            'warehouses' => $store->city->warehouses,
            'product' => $product,
            'fields_permissions' => Product::getStatusFields($this->admin_permissions)
        ];

        return View::make('admin.products.product_form', $data)->renderSections()['store_products'];
    }

    public function save_store_product()
    {
        \DB::beginTransaction();
        try {
            if (Input::has('id')) {
                $id = Input::get('id');
                $store_product = StoreProduct::with('product')->find($id);
                $action = 'update';
            } else {
                $store_product = new StoreProduct();
                $store_product->store_id = Input::get('store_id');
                $product = Product::find(Input::get('product_id'));
                $store_product->product()->associate($product);
                $action = 'insert';
            }
            if (Input::has('store_product_group_price')) {
                // cuando no hay productos en la table relacionados
                if (Input::has('store_product_id')) {
                    $store_product_ids = Input::get('store_product_id');
                    $store_product_group_prices = Input::get('store_product_group_price');
                    $store_product_group_quantities = 0;
                    if (Input::has('store_product_group_quantity')) {
                        $store_product_group_quantities = Input::get('store_product_group_quantity');
                    }
                    foreach ($store_product_ids as $key => $store_product_id) {
                        $store_product_group = new StoreProductGroup();
                        if (Input::has('store_product_group_quantity')) {
                            $store_product_group->quantity = $store_product_group_quantities[$key];
                        }
                        $store_product_group->storeProductGroup()->associate($store_product);
                        $store_product_group->price = $store_product->price;
                        $store_product_group->store_product_group_id = $store_product_id;
                        $store_product_group->save();
                    }
                }

                if (Input::has('store_product_group_id')) {
                    $total = 0;
                    $store_product_group_ids = Input::get('store_product_group_id');
                    $store_product_group_prices = Input::get('store_product_group_price');
                    foreach ($store_product_group_ids as $key => $store_product_group_id) {
                        $store_product_group = StoreProductGroup::find($store_product_group_id);
                        $store_product_group->price = $store_product_group_prices[$key];
                        $total += $store_product_group->price * $store_product_group->quantity;

                        $store_product_group->save();
                    }

                    if ($total !== intval(Input::get('price'))) {
                        throw new \Exception('El precio total de productos no es igual precio del combo.');
                    }
                }
            }

            $is_simple = $store_product->product->type === 'Simple';

            $store_product->department_id = Input::get('department_id');
            $store_product->shelf_id = Input::get('shelf_id');
            $store_product->sort_order = Input::get('sort_order');
            $store_product->is_best_price = Input::get('is_best_price');
            //if($this->admin_permissions['permission1']) {
                $store_product->public_price = Input::get('public_price') ?: null;
            //}
            $store_product->price = $is_simple ? Input::get('price') : ($store_product->price ?: 0);

            $store_product->iva = Input::get('iva');
            $store_product->base_price = $store_product->price / (1 + $store_product->iva / 100);
            $store_product->consumption_tax = Input::get('consumption_tax');

            $store_product->delivery_discount_amount = (!is_null(Input::get('delivery_discount_amount'))) ? Input::get('delivery_discount_amount') : 0;
            $store_product->delivery_maximum_amount = Input::get('delivery_maximum_amount');
            $delivery_discount_start_date = Input::get('delivery_discount_start_date');
            if (empty($delivery_discount_start_date)) {
                $store_product->delivery_discount_start_date = null;
            } else {
                $store_product->delivery_discount_start_date = Carbon::createFromFormat(
                    'd/m/Y H:i:s',
                    $delivery_discount_start_date
                )->toDateTimeString();
            }

            if (empty(Input::get('has_special_price'))) {
                $store_product->special_price = null;
                $store_product->quantity_special_price = 0;
            } else {
                $total_discount = 0;
                $store_product->special_price = Input::get('special_price') ?: null;
                $store_product->quantity_special_price = Input::get('quantity_special_price') ?: 0;
                $store_product->quantity_special_price_stock = Input::get('quantity_special_price_stock');
                $store_product->has_quantity_special_price_stock = Input::get('has_quantity_special_price_stock');
                $store_product->first_order_special_price = Input::get('first_order_special_price');
                $special_price_expiration_date = Input::get('special_price_expiration_date');
                $special_price_starting_date = Input::get('special_price_starting_date');
                if (empty($special_price_expiration_date)) {
                    $store_product->special_price_expiration_date = null;
                } else {
                    $store_product->special_price_expiration_date = Carbon::createFromFormat(
                        'd/m/Y H:i:s',
                        $special_price_expiration_date
                    )->toDateTimeString();
                }

                if (empty($special_price_starting_date)) {
                    $store_product->special_price_starting_date = null;
                } else {
                    $store_product->special_price_starting_date = Carbon::createFromFormat(
                        'd/m/Y H:i:s',
                        $special_price_starting_date
                    )->toDateTimeString();
                }

                $product_items = $is_simple
                    ? [$store_product]
                    : StoreProductGroup::where('store_product_id', $store_product->id)->get();

                foreach ($product_items as $index => $product_item) {
                    if (!$is_simple) {
                        $product_item->discount_amount = Input::get('discount_amount')[$index];
                        $total_discount += Input::get('discount_amount')[$index];
                    }

                    $accept_discount = $is_simple || !empty($product_item->discount_amount);

                    //if($this->admin_permissions['permission1']) {
                        $product_item->merqueo_discount = $accept_discount
                            ? Input::get('merqueo_discount')[$index] : 0;
                    //}
                    $product_item->provider_discount = $accept_discount ? Input::get('provider_discount')[$index] : 0;
                    $product_item->seller_discount = $accept_discount ? Input::get('seller_discount')[$index] : 0;

                    $total_distribution = $product_item->provider_discount + $product_item->merqueo_discount + $product_item->seller_discount;

                    if (intval($total_distribution) !== 100 && (!empty($product_item->discount_amount) || $is_simple)) {
                        throw new \Exception('La distribución del producto no es correcta.');
                    }

                    $product_item->save();
                }

                if (!$is_simple && $total_discount !== intval($store_product->price - $store_product->special_price)) {
                    throw new \Exception('La distribución del descuento en los productos no es correcta.');
                }
            }

            $store_product->provider_plu = Input::get('provider_plu');
            $store_product->provider_id = Input::get('provider_id');
            $store_product->provider_pack_type = Input::get('provider_pack_type');
            $store_product->provider_pack_quantity = Input::get('provider_pack_quantity');
            $store_product->provider_pack_quantity_approach = Input::get('provider_pack_quantity_approach');
            $store_product->cost = Input::get('cost') ?: 0;
            $store_product->base_cost = $store_product->cost / (1 + $store_product->iva / 100);
            $store_product->storage = Input::get('storage');
            $associate_file = function($input_name, $field, $directory = 'stores/products') use (&$store_product) {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                if (Input::hasFile($input_name)) {
                    if (!empty($store_product->{$field}) && !strstr($store_product->{$field}, 'default')){
                        $path = str_replace($cloudfront_url, uploads_path(), $store_product->{$field});
                        remove_file($path);
                        remove_file_s3($store_product->{$field});
                    }
                    $store_product->{$field} = upload_image(Input::file($input_name), FALSE, $directory);
                }
            };
            $associate_file('guarantee_policies', 'guarantee_policies');
            $store_product->useful_life_days = Input::get('useful_life_days');
            $store_product->handle_expiration_date = Input::get('handle_expiration_date');
            $store_product->allied_store_id = empty(Input::get('allied_store_id')) ? null : Input::get('allied_store_id');
            //$store_product->save();

            $url_product = route('frontStoreProducts.single_product', [
                'city_slug'       => $store_product->store->city->slug,
                'store_slug'      => $store_product->store->slug,
                'department_slug' => $store_product->department->slug,
                'shelf_slug'      => $store_product->shelf->slug,
                'product_slug'    => $store_product->product->slug,
            ]);

            $url_product  = str_replace(Config::get('app.url'), 'https://merqueo.com', $url_product);
            $deeplink_product_url =  \FirebaseClient::generateDynamicLink([
                'url'           => $url_product,
                'type'          => 'product',
                'store_id'      => $store_product->store_id,
                'department_id' => $store_product->department_id,
                'shelf_id'      => $store_product->shelf_id,
                'product_id'    => $store_product->id,
            ]);
            $store_product->deeplink = $deeplink_product_url->shortLink;
            $store_product->save();

            if ($store_product->product->type == 'Agrupado' || $store_product->product->type == 'Proveedor') {
                $store_product_group = StoreProductGroup::where('store_product_id', $store_product->id)->count();
                if (!$store_product_group) {
                    $product_groups = ProductGroup::where('product_id', $store_product->product_id)->get();
                    foreach ($product_groups as $product_group) {
                        $store_product_gp = StoreProduct::where('product_id', $product_group->product_group_id)
                            ->where('store_id', $store_product->store_id)
                            ->first();
                        $store_product_group = new StoreProductGroup;
                        $store_product_group->store_product_id = $store_product->id;
                        $store_product_group->store_product_group_id = $store_product_gp->id;
                        $store_product_group->quantity = $product_group->quantity;
                        $store_product_group->price = $store_product_gp->price;
                        $store_product_group->save();
                    }
                }
                $store_product->calculatePriceOnGroup()->save();
            }

            if ($is_simple) {
                $store_product->updateAncestorsPrice();
            }

            DB::commit();
        } catch (\Exception $exception) {
            \DB::rollback();
            return Redirect::back()->with('error', $exception->getMessage());
        }


        $this->admin_log('store_products', $store_product->id, $action);
        return Redirect::back()->with('success', 'Producto guardado con éxito.');
    }

    /**
     * Funcion para exportar productos de tiendas con el mismo formato del importador
     */
    public function export_products()
    {
        ini_set('memory_limit', '2024M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $store_id = Input::get('export_store_id');
        $store = Store::find($store_id);
        $city_id = Input::get('export_city_id');
        $city = City::find($city_id);

        $filename = $store->name.' '.$city->city.' '.date('Y-m-d H.i.s', time());
        return Excel::create($filename, function ($excel) {
            $excel->sheet('Productos', function ($sheet) {
                $sheet->setColumnFormat(array(
                    'B' => '@',
                    'C' => '@',
                    'K' => '@',
                    'L' => '@',
                    'V' => '@',
                    'AL' => '@',
                    'AW' => 'yyyy-mm-dd h:mm:ss;@',
                    'AX' => 'yyyy-mm-dd h:mm:ss;@',
                    'BC' => 'yyyy-mm-dd h:mm:ss;@',
                ));

                $titles = [
                    'PRODUCT_ID', // A
                    'PRODUCTO', // B
                    'REFERENCIA', // C
                    'ID_CATEGORIA', // D
                    'NOMBRE_CATEGORÍA', // E
                    'ID_SUBCATEGORIA', // F
                    'NOMBRE_SUBCATEGORIA', // G
                    'MARCA', // H
                    'SUBMARCA', // I
                    'FABRICANTE', // J
                    'DESCRIPCION', // K
                    'INFORMACION_NUTRICIONAL', // L
                    'CODIGO_BARRAS', // M
                    'ES_PERECEDERO', // N
                    'PRESENTACION', // O
                    'SUBPRESENTACION', // P
                    'CANTIDAD', // Q
                    'UNIDAD', // R
                    'DIMENSIONES_ALTO', // S
                    'DIMENSIONES_ANCHO', // T
                    'DIMENSIONES_LARGO', // U
                    'DIMENSIONES_TAMANO', // V
                    'CUENTA_CONTABLE', // W
                    'TIPO_CUENTA_CONTABLE', // X
                    'LINEA_CONTABLE', // Y
                    'GRUPO_CONTABLE', // Z
                    'CODIGO_CONTABLE', // AA
                    'TIPO_PRODUCTO', // AB
                    'ID_DEPARTAMENTO', // AC
                    'DEPARTAMENTO', // AD
                    'ID_PASILLO', // AE
                    'PASILLO', // AF
                    'POSICION_EN_PAGINA_WEB', // AG
                    'MANEJA_FECHA_DE_VENCIMIENTO', // AH
                    'ES_MEJOR_PRECIO_GARANTIZADO', // AI
                    'ID_PROVEEDOR', // AJ
                    'ID_TIENDA_ALIADA', // AK
                    'PLU_PROVEEDOR', // AL
                    'TIPO_EMBALAJE_DE_PROVEEDOR', // AM
                    'CANTIDAD_EMBALAJE', // AN
                    'COSTO_IVA', // AO
                    'COSTO_BASE', // AP
                    'PRECIO', // AQ
                    'IVA', // AR
                    'IMPUESTO_AL_CONSUMO', // AS
                    'PRECIO_BASE', // AT
                    'PRECIO_ESPECIAL', // AU
                    'PRECIO_MOSTRAR_PAGINA', // AV
                    'FECHA_INICIO_PRECIO_ESPECIAL', //AW
                    'FECHA_EXPIRACION_PRECIO_ESPECIAL', // AX
                    'CANTIDAD_PRECIO_ESPECIAL', // AY
                    'PRIMERA_COMPRA_PRECIO_ESPECIAL', // AZ
                    'DESCUENTO_EN_DOMICILIO', // BA
                    'DESCUENTO_TOTAL_MAXIMO_DOMICILIO', // BB
                    'FECHA_INICIO_DESCUENTO_DOMICILIO', // BC
                    'MANEJA_STOCK_CON_PRECIO_ESPECIAL', // BD
                    'STOCK_CON_PRECIO_ESPECIAL', // BE
                    'PRECIO_DESCUENTO_MERQUEO', // BF
                    'PRECIO_DESCUENTO_PROVEEDOR', // BG
                    'PRECIO_DESCUENTO_VENDEDOR', // BH
                    'ALMACENAMIENTO', // BI
                    'DIAS_VIDA_UTIL', // BJ
                    'MANAGE_STOCK', // BK
                    'PRODUCTO_VISIBLE', // BL
                    'STATUS', // BM
                    'DESCONTINUADO', // BN
                    'IS_VISIBLE_STOCK', // BO
                    'MINIMUM_STOCK', // BP
                    'IDEAL_STOCK', // BQ
                    'STOCK_MINIMO_EN_ALISTAMIENTO', // BR
                    'STOCK_MAXIMO_EN_ALISTAMIENTO', // BS
                    'POSICION_EN_BODEGA', // BT
                    'POSICION_EN_BODEGA_ALTURA', // BU
                    'APROXIMACION_A_EMBALAJE', // BV
                    'PRECIO_NEGOCIADO' // BW
                ];

                $sheet->appendRow($titles);

                $city_id = Input::get('export_city_id');
                $city = City::find($city_id);
                $store_id = Input::get('export_store_id');
                $store = Store::find($store_id);
                $warehouse_id = Input::get('export_warehouse_id');


                $products = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
                                ->leftJoin('categories', 'products.category_id', '=', 'categories.id')
                                ->leftJoin('subcategories', 'products.subcategory_id', '=', 'subcategories.id')
                                ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
                                ->leftJoin('subbrands', 'products.subbrand_id', '=', 'subbrands.id')
                                ->leftJoin('makers', 'products.maker_id', '=', 'makers.id')
                                ->leftJoin('departments', 'store_products.department_id', '=', 'departments.id')
                                ->leftJoin('shelves', 'store_products.shelf_id', '=', 'shelves.id')
                                ->leftJoin('providers', 'store_products.provider_id', '=', 'providers.id')
                                // ->leftJoin('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                                ->join('store_product_warehouses', function ($q) use ($warehouse_id) {
                                    $q->on('store_products.id', '=', 'store_product_warehouses.store_product_id')
                                        ->where('store_product_warehouses.warehouse_id', '=', $warehouse_id);
                                })
                                ->where('store_products.store_id', '=', $store_id)
                                ->orderBy('departments.sort_order', 'shelves.sort_order', 'store_products.sort_order')
                                ->select(
                                    // Información básica
                                    'products.id AS PRODUCT_ID',
                                    'products.name AS PRODUCTO',
                                    'products.reference AS REFERENCIA',
                                    'products.category_id AS ID_CATEGORIA',
                                    'categories.name AS NOMBRE_CATEGORÍA',
                                    'products.subcategory_id AS ID_SUBCATEGORIA',
                                    'subcategories.name AS NOMBRE_SUBCATEGORIA',
                                    'brands.name AS MARCA',
                                    'subbrands.name AS SUBMARCA',
                                    'makers.name AS FABRICANTE',
                                    'products.description AS DESCRIPCION',
                                    'products.nutrition_facts AS INFORMACION_NUTRICIONAL',
                                    'products.has_barcode AS CODIGO_BARRAS',
                                    'products.is_perishable AS ES_PERECEDERO',
                                    // Presentación
                                    'products.presentation AS PRESENTACION',
                                    'products.subpresentation AS SUBPRESENTACION',
                                    'products.quantity AS CANTIDAD',
                                    'products.unit AS UNIDAD',
                                    // Dimensiones
                                    'products.height AS DIMENSIONES_ALTO',
                                    'products.width AS DIMENSIONES_ANCHO',
                                    'products.length AS DIMENSIONES_LARGO',
                                    'products.size AS DIMENSIONES_TAMANO',
                                    // Contabilidad
                                    'products.accounting_account AS CUENTA_CONTABLE',
                                    'products.accounting_account_type AS TIPO_CUENTA_CONTABLE',
                                    'products.accounting_line AS LINEA_CONTABLE',
                                    'products.accounting_group AS GRUPO_CONTABLE',
                                    'products.accounting_code AS CODIGO_CONTABLE',
                                    'products.type AS TIPO_PRODUCTO',
                                    // Productoen tiendas
                                    'store_products.department_id AS ID_DEPARTAMENTO',
                                    'departments.name AS DEPARTAMENTO',
                                    'store_products.shelf_id AS ID_PASILLO',
                                    'shelves.name AS PASILLO',
                                    'store_products.sort_order AS POSICION_EN_PAGINA_WEB',
                                    'store_products.handle_expiration_date AS MANEJA_FECHA_DE_VENCIMIENTO',
                                    'store_products.is_best_price AS ES_MEJOR_PRECIO_GARANTIZADO',
                                    // Proveedor
                                    'store_products.provider_id AS ID_PROVEEDOR',
                                    'store_products.allied_store_id AS ID_TIENDA_ALIADA',
                                    'store_products.provider_plu AS PLU_PROVEEDOR',
                                    'store_products.provider_pack_type AS TIPO_EMBALAJE_DE_PROVEEDOR',
                                    'store_products.provider_pack_quantity AS CANTIDAD_EMBALAJE',
                                    'store_products.cost AS COSTO_IVA',
                                    'store_products.base_cost AS COSTO_BASE',
                                    // Precio
                                    'store_products.price AS PRECIO',
                                    'store_products.iva AS IVA',
                                    'store_products.consumption_tax AS IMPUESTO_AL_CONSUMO',
                                    'store_products.base_price AS PRECIO_BASE',
                                    // Promoción
                                    'store_products.special_price AS PRECIO_ESPECIAL',
                                    'store_products.public_price AS PRECIO_MOSTRAR_PAGINA',
                                    'store_products.special_price_starting_date AS FECHA_INICIO_PRECIO_ESPECIAL',
                                    'store_products.special_price_expiration_date AS FECHA_EXPIRACION_PRECIO_ESPECIAL',
                                    'store_products.quantity_special_price AS CANTIDAD_PRECIO_ESPECIAL',
                                    'store_products.first_order_special_price AS PRIMERA_COMPRA_PRECIO_ESPECIAL',
                                    'store_products.delivery_discount_amount AS DESCUENTO_EN_DOMICILIO',
                                    'store_products.delivery_maximum_amount AS DESCUENTO_TOTAL_MAXIMO_DOMICILIO',
                                    'store_products.delivery_discount_start_date AS FECHA_INICIO_DESCUENTO_DOMICILIO',
                                    'store_products.has_quantity_special_price_stock AS MANEJA_STOCK_CON_PRECIO_ESPECIAL',
                                    'store_products.quantity_special_price_stock AS STOCK_CON_PRECIO_ESPECIAL',
                                    // Distribución de promoción
                                    'store_products.merqueo_discount AS PRECIO_DESCUENTO_MERQUEO',
                                    'store_products.provider_discount AS PRECIO_DESCUENTO_PROVEEDOR',
                                    'store_products.seller_discount AS PRECIO_DESCUENTO_VENDEDOR',
                                    // Almacenamiento
                                    'store_products.storage AS ALMACENAMIENTO',
                                    'store_products.useful_life_days AS DIAS_VIDA_UTIL',
                                    // Inventario
                                    'store_product_warehouses.manage_stock AS MANAGE_STOCK',
                                    'store_product_warehouses.is_visible AS PRODUCTO_VISIBLE',
                                    'store_product_warehouses.status AS STATUS',
                                    'store_product_warehouses.discontinued AS DESCONTINUADO',
                                    'store_product_warehouses.is_visible_stock AS IS_VISIBLE_STOCK',
                                    'store_product_warehouses.minimum_stock AS MINIMUM_STOCK',
                                    'store_product_warehouses.ideal_stock AS IDEAL_STOCK',
                                    'store_product_warehouses.minimum_picking_stock AS STOCK_MINIMO_EN_ALISTAMIENTO',
                                    'store_product_warehouses.maximum_picking_stock AS STOCK_MAXIMO_EN_ALISTAMIENTO',
                                    'store_product_warehouses.storage_position AS POSICION_EN_BODEGA',
                                    'store_product_warehouses.storage_height_position AS POSICION_EN_BODEGA_ALTURA',
                                    'store_products.provider_pack_quantity_approach AS APROXIMACION_A_EMBALAJE',
                                    'store_products.negociated_price AS PRECIO_NEGOCIADO'
                                );

                $products->chunk(50, function ($rows) use ($sheet) {
                    foreach ($rows as $key => $row) {
                        if (!empty($row->DESCRIPCION)) {
                            $row->DESCRIPCION = html_entity_decode($row->DESCRIPCION);
                        }
                        if (!empty($row->INFORMACION_NUTRICIONAL)) {
                            $row->INFORMACION_NUTRICIONAL = html_entity_decode($row->INFORMACION_NUTRICIONAL);
                        }

                        $array_data = $row->toArray();
                        $sheet->appendRow($array_data);
                    }
                });
                // $sheet->fromArray($products->toArray());
            });
        })->export('xlsx');

        return Redirect::back()->with('error', 'No se encontraron productos para exportar.');
    }

    /**
     * Genera un archivo CSV con las cabeceras necesarias
     * para poder un listado de productos al catalogo
     * de productos mostrado en Facebook.
     *
     * @param Store $store
     * @return Illuminate\Http\JsonResponse
     */
    public function download_make_facebook_feed(Store $store)
    {
        $producs_feed_closure = function ($query) {
            $query->select('id', 'slug', 'name');
        };
        $products = $store->products()
                    ->active()
                    ->with([
                        'shelf' => $producs_feed_closure,
                        'department' => $producs_feed_closure,
                        'brand'
                    ])
                    ->get();
        if (count($products) === 0) {
            return Response::json(['message' => 'No hay productos disponibles.'], 404);
        }

        $memory = fopen('php://memory', 'w');
        $headers = [
            'id', 'title', 'description', 'link', 'image_link', 'availability',
            'price', 'brand', 'condition',
            // Tienda
            'custom_label_0', 'sale_price',
            // Descuento
            'custom_label_1', 'google_product_category', 'product_type',
            // Tipo de cantidades del producto eg. 1 gr
            'custom_label_2',
            // Unidades en descuento
            'custom_label_3',
        ];
        fputcsv($memory, $headers);
        foreach ($products as $product) {
            fputcsv($memory, [
                $product->id,
                $product->name,
                $product->name,
                action('StoreController@single_product', [
                    'city_slug' => $store->city->slug,
                    'store_slug' => $store->slug,
                    'department_slug' => $product->pivot->department->slug,
                    'shelf_slug' => $product->pivot->shelf->slug,
                    'product_slug' => $product->slug
                ]),
                $product->image_medium_url,
                $product->pivot->current_stock > 0
                    ? 'In stock'
                    : 'Out of stock',
                $product->pivot->price,
                $product->brand->name,
                'New',
                $store->name,
                $product->pivot->special_price
                    ? $product->pivot->special_price
                    : $product->pivot->price,
                // Porcentaje de descuento
                $product->pivot->discount * 100,
                $product->pivot->department->name,
                $product->pivot->shelf->name,
                "{$product->quantity} {$product->unit}",
                $product->pivot->quantity_special_price
            ]);
        }
        $url = upload_image(
            ['client_original_extension' => 'csv'],
            false,
            Config::get('app.download_directory_temp'),
            $memory
        );

        return Response::json(compact('url'));
    }

    /**
     * Genera el archivo con los datos de los productos, este
     * es utilizado por elastic search.
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function download_elastic_search_json_products()
    {
        $json_text = '';
        $products = Product::elasticSearchProducts();
        foreach ($products as $product) {
            $json_text .= json_encode([
                'index' => [
                    '_index' => 'catalog',
                    '_type' => 'product',
                    '_id' => $product->id
                ]
            ]);
            $json_text .= "\n";
            $json_text .= json_encode($product);
            $json_text .= "\n";
        }

        $url = upload_image(
            ['client_original_extension' => 'json'],
            false,
            Config::get('app.download_directory_temp'),
            $json_text
        );

        return Response::json(compact('url'));
    }

    /**
     * Muestra el formulario correspondiente al "store_product_warehouses".
     *
     * @param int $store_product_id
     * @param int $warehouse_id
     * @return mixed
     */
    public function get_store_product_warehouse_form_ajax($store_product_id, $warehouse_id)
    {
        $store_product = \StoreProduct::findOrFail($store_product_id);
        $warehouse = \Warehouse::findOrFail($warehouse_id);
        $boolean_field_values = ['No', 'Sí'];
        $states = ['Inactivo', 'Activo'];
        $store_product_warehouse = \StoreProductWarehouse::firstOrNew([
            'store_product_id' => $store_product_id,
            'warehouse_id' => $warehouse_id
        ]);

        return View::make(
            'admin.products.store_product_warehouse',
            compact('store_product_warehouse', 'store_product', 'warehouse', 'boolean_field_values', 'states')
        );
    }

    /**
     * Almacena un "store_product_warehouses"
     *
     * @return mixed
     */
    public function save_store_product_warehouse_ajax()
    {
        $rules = [];
        if ($this->admin_permissions['permission1']) {
            $rules = [
                'id' => 'sometimes|required|numeric|digits_between:1,10|exists:store_product_warehouses',
                'warehouse_id' => 'required|numeric|digits_between:1,10|exists:warehouses,id',
                'store_product_id' => 'required|numeric|digits_between:1,10|exists:store_products,id',
                'minimum_picking_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                'maximum_picking_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                'storage_position' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                'storage_height_position' => 'required_if:manage_stock,1|string|max:5',
            ];
        } else if($this->admin_permissions['permission3']){
            $rules =[
                'id' => 'sometimes|required|numeric|digits_between:1,10|exists:store_product_warehouses',
                'warehouse_id' => 'required|numeric|digits_between:1,10|exists:warehouses,id',
                'store_product_id' => 'required|numeric|digits_between:1,10|exists:store_products,id',
                'minimum_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                'ideal_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
            ];
        } else if (!$this->admin_permissions['permission1'] && !$this->admin_permissions['permission2'] && !$this->admin_permissions['permission3']) {
            $rules = [
                'id' => 'sometimes|required|numeric|digits_between:1,10|exists:store_product_warehouses',
                'warehouse_id' => 'required|numeric|digits_between:1,10|exists:warehouses,id',
                'store_product_id' => 'required|numeric|digits_between:1,10|exists:store_products,id',
                'manage_stock' => 'required|boolean',
                'is_visible_stock' => 'required|boolean',
                'status' => 'required|boolean',
                'discontinued' => 'required|boolean'
            ];
        }
        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 422);
        }

        $store_product_warehouse = \StoreProductWarehouse::findOrNew(\Input::get('id') ?: null);
        $store_product_warehouse->warehouse()->associate(\Warehouse::find(\Input::get('warehouse_id')));
        $store_product_warehouse->storeProduct()->associate(\StoreProduct::find(\Input::get('store_product_id')));
        $product = \Product::find($store_product_warehouse->storeProduct->product->id);
        $is_marketplace = !empty($store_product_warehouse->storeProduct->allied_store_id);
        if ($this->admin_permissions['permission1']) {
            $store_product_warehouse->storage_position = Input::get('storage_position');
            $store_product_warehouse->storage_height_position = Input::get('storage_height_position');
            $store_product_warehouse->minimum_picking_stock = Input::get('minimum_picking_stock');
            $store_product_warehouse->maximum_picking_stock = Input::get('maximum_picking_stock');
            $product->status_warehouse = 1;
            $product->save();
        }
        /*elseif (!$store_product_warehouse->exists && $is_marketplace) {
            $store_product_warehouse->is_visible_stock = 1;
        }
        if (!$this->admin_permissions['permission1'] && !$store_product_warehouse->exists){
            $store_product_warehouse->status = 1;
        }*/
        if ($this->admin_permissions['permission3']) {
            $store_product_warehouse->ideal_stock = Input::get('ideal_stock');
            $store_product_warehouse->minimum_stock = Input::get('minimum_stock');
            $product->status_purchases = 1;
            $product->save();
        }
        if (!$this->admin_permissions['permission1'] && !$this->admin_permissions['permission2'] && !$this->admin_permissions['permission3']) {
            $store_product_warehouse->manage_stock = Input::get('manage_stock');
            $store_product_warehouse->is_visible = Input::get('is_visible');
            $store_product_warehouse->is_visible_stock = Input::get('is_visible_stock');
            $store_product_warehouse->status = Input::get('status');
            $store_product_warehouse->discontinued = Input::get('discontinued');
        }

        if (!empty($store_product_warehouse->manage_stock) && $this->admin_permissions['permission1']) {
            $result = \WarehouseStorage::validatePosition(
                $store_product_warehouse->warehouse->id,
                'picking',
                $store_product_warehouse->storeProduct->storage === 'Seco' ? 'Seco' : 'Frío',
                $store_product_warehouse->storage_position,
                $store_product_warehouse->storage_height_position
            );
            if (!$result['status']) {
                return Response::json(['Posición' => $result['message']], 422);
            }
        }

        $store_product_warehouse->save();
        $this->admin_log('store_product_warehouses', $store_product_warehouse->id, 'update');

        return Response::json([
            'status' => true,
            'message' => 'Datos guardados correctamente.'
        ]);
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function save_product_detail_ajax($product_id)
    {
        $validator = \Validator::make(\Input::all(), [
            'type' => 'required|string|max:50',
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:1000'
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => false,
                'message' => 'Error en los campos',
                'details' => \Config::get('debug') ? $validator->errors() : new \StdClass()
            ]);
        }

        $product = \Product::findOrFail($product_id);
        $detail = \ProductDetail::findOrNew(\Input::get('id') ?: 0);
        $detail->type = \Input::get('type');
        $detail->name = \Input::get('name');
        $detail->description = \Input::get('description');
        $product->details()->save($detail);

        return \Redirect::back()->with('message', 'Detalle agregado.');
    }

    /**
     * @param $product_detail_id
     * @return mixed
     * @throws \Exception
     */
    public function delete_product_detail_ajax($product_detail_id)
    {
        $detail = \ProductDetail::findOrFail($product_detail_id);
        $detail->delete();

        return Redirect::back()->with('message', 'Detalle eliminado.');
    }

    /**
     * @param $id
     */
    public function delete_suggested_product_applyed_to_shelf($id) {
        $shelf_suggested_product = ShelvesSuggestedProducts::findOrFail($id);
        $shelf_suggested_product->delete();
        return Redirect::back()->with('type', 'success')->with('message', 'Registro eliminado.');
    }

}
