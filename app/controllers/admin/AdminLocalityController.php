<?php

namespace admin;

use Request, Input, View, Session, Redirect, Response, Validator, DB, Config, Locality, City;

class AdminLocalityController extends AdminController
{
    /**
     * Grilla de localidades
     */
    public function index()
    {
        if (!Request::ajax()) {
            $city = City::find(Session::get('admin_city_id'));
            $localities = Locality::where('city_id', $city->id)->orderBy('status', 'desc')->orderBy('name')->get();
            $localities_map = Locality::where('city_id', $city->id)->where('status', 1)->orderBy('name')->get();
            return View::make('admin.localities.index')
                ->with('title', 'Localidades')
                ->with('localities', $localities)
                ->with('localities_map', $localities_map)
                ->with('lat', $city->latitude)
                ->with('lng', $city->longitude)
                ->with('cities', $this->get_cities())
                ->with('store_admin', false);
        } else {
            $city_id = Input::get('city_id');
            $city = City::find($city_id == null ? Session::get('admin_city_id') : $city_id);
            $localities = Locality::where('city_id', $city->id)->orderBy('status', 'desc')->orderBy('name')->get();
            $localities_map = Locality::where('city_id', $city->id)->where('status', 1)->orderBy('name')->get();
            foreach ($localities as $locality) {
                $locality->city = $city;
            }
            return View::make('admin.localities.index')
                ->with('title', 'Localidades')
                ->with('localities', $localities)
                ->with('localities_map', $localities_map)
                ->with('lat', $city->latitude)
                ->with('lng', $city->longitude)
                ->with('cities', $this->get_cities())
                ->with('store_admin', false)
                ->renderSections()['content'];
        }
    }

    /**
     * Editar localidad
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Localidad' : 'Nueva Localidad';
        $edit = false;
        if (!empty($id)) {
            $locality = Locality::find($id);
            $edit = true;
        } else {
            $locality = new Locality();
            $locality->polygon = '';
        }
        return View::make('admin.localities.locality_form')
            ->with('title', 'Localidades')
            ->with('sub_title', $sub_title)
            ->with('cities', $this->get_cities())
            ->with('edit', $edit)
            ->with('locality_map', $locality)
            ->with('locality', $locality)
            ->with('store_admin', false);
    }

    /**
     * Guardar localidad
     */
    public function save()
    {
        $id = Input::get('id');
        $locality = Locality::find($id);
        if(!$locality) {
            $locality = new Locality;
            $action = 'insert';
        }else $action = 'update';
        $locality->name = Input::get('name_locality');
        $locality->polygon = Input::get('polygon');
        $locality->city_id = Input::get('city_id');
        $locality->status = Input::get('status');
        $locality->save();

        $this->admin_log('localities', $locality->id, $action);

        return Redirect::route('adminLocalities.index')->with('type', 'success')->with('message', 'Localidad actualizada satifactoriamente.');
    }

    /**
     * Activar localidad
     */
    public function toggle($id)
    {
        $locality = Locality::find($id);
        $locality->status = 1;
        $locality->save();
        $this->admin_log('localities', $locality->id, 'update');

        return Redirect::route('adminLocalities.index')->with('type', 'success')->with('message', 'Localidad actualizada satifactoriamente.');
    }

    /**
     * Eliminar localidad
     */
    public function delete($id)
    {
        $locality = Locality::find($id);
        $locality->status = 0;
        $locality->save();

        $this->admin_log('localities', $id, 'delete');
        return Redirect::route('adminLocalities.index')->with('type', 'success')->with('message', 'Localidad desactivada satifactoriamente.');
    }

    /**
     * Obtener las coordenadas por ciudad
     */
    public function get_coordinates_by_city_ajax()
    {
        $coordinates = City::select('latitude', 'longitude')
            ->where('status', 1)
            ->where('id', Input::get('city_id'))
            ->first();

        $result['latitude'] = $coordinates->latitude;
        $result['longitude'] = $coordinates->longitude;

        return json_encode($result);
    }
}