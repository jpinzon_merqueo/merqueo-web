<?php

namespace admin\catalog;

use Request, Input, Session, View, Redirect, Config, DB, Menu, Response, admin\AdminController, Carbon\Carbon, Sampling, StoreProduct, StoreProductWarehouse, Shelf, Department;

class SamplingController extends AdminController
{

    /**
     * Funcion que muestra la lista de los productos de muestra
     */
    public function index()
    {
        if (!Request::ajax()) {
            $data = [
                'cities' => $this->get_cities(),
                'title' => 'Productos de Muestra',
                'types' => array('Seco', 'Frío')
            ];
            return View::make('admin.catalog.sampling.index', $data);
        } else {
            $sample_products = Sampling::getSampleProducts(Input::get('warehouse_id'));
            $samplings = [];
            foreach ($sample_products AS $sampling)
            {
                $sampling->type = \SampligDetail::where('sampling_id', $sampling->id)->get();
                $sampling->delivered = Sampling::getSampligDelivered(Input::get('warehouse_id'), $sampling->store_product_id);
                $samplings[] = $sampling;
            }

            $data = [
                'samples_products' => $samplings
            ];
            return View::make('admin.catalog.sampling.index', $data)->renderSections()['table'];
        }
    }

    /**
     * Funcion que muestra el formulario para agregar un nuevo producto de muestra
     */
    public function add()
    {
        if (!Request::ajax()) {
            $data = [
                'title' => 'Agregar Producto de Muestra',
                'action' => 'add',
                'cities' => $this->get_cities(),
                'products' => ''
            ];
            return View::make('admin.catalog.sampling.add_form', $data);
        }
    }

    /**
     * Funcion que muestra el formulario para editar un producto de muestra
     */

    public function edit($id)
    {
        if (!Request::ajax()) {
            $product = Sampling::find($id);
            $product->delivery = Sampling::getSampligDelivered($product->warehouse_id, $product->store_product_id);
            $types = \SampligDetail::where('sampling_id', $product->id)->get();
            //$product->zone_ids = explode(',', $product->zone_ids);
            $data = [
                'title' => 'Editar Productos de Muestra',
                'action' => 'edit',
                'cities' => $this->get_cities(),
                'product' => $product,
                'types' => $types
            ];
            return View::make('admin.catalog.sampling.add_form', $data);
        }
    }

    /**
     * Funcion que permite eliminar un producto de muestra
     */

    public function delete($id)
    {
        if (!Request::ajax()) {
            try {
                DB::beginTransaction();
                $sample_product = Sampling::find($id);
                if ($sample_product->status == 1) {
                    $sample_product->status = 0;
                    $this->admin_log('sampling', $id, "disabled");
                } else {
                    $this->admin_log('sampling', $id, "activated");
                    $sample_product->status = 1;
                }
                $sample_product->save();
                
                DB::commit();
                return Redirect::route('samplingProducts.index')->with('type', 'success')->with('message', 'Se actualizo el estado del producto de muestra con éxito.');
            } catch (\Exception $e) {
                DB::rollBack();
                return Redirect::route('samplingProducts.index')->with('type', 'error')->with('message', 'Ocurrio un error al actualizar el estado del producto de muestra.');
            }
        }
    }

    /**
     * Funcion que guarda y actualiza un producto de muestra
     */

    public function save()
    {
        try {

            DB::beginTransaction();
            if (Input::get('sampling_product_id') != "") {
                $sampling = Sampling::find(Input::get('sampling_product_id'));

            } else {
                $sampling = new Sampling;
            }
            $sampling->store_product_id = Input::get('store_product_id');
            $sampling->warehouse_id = Input::get('warehouse_id');
            $sampling->quantity = Input::get('quantity');
            $sampling->brand = Input::get('brand');
            $sampling->zone_ids = !empty(Input::get('zone_ids')) ? implode(',', Input::get('zone_ids')) : null;
            $sampling->status = 1;
            $sampling->save();
                        

            if (Input::has('type_association_products')) {
                $asociation_products = implode(',', Input::get('asociation_products'));
                $this->save_sampling_detail($sampling, Input::get('type_association_products'), $asociation_products);
            }

            if (!Input::has('type_association_products')) {
                $this->delete_sampling_detail($sampling, 'Productos');
            }

            if (Input::has('type_association_shelves')) {
                $asociation_shelves = implode(',', Input::get('asociation_shelves'));
                $this->save_sampling_detail($sampling, Input::get('type_association_shelves'), $asociation_shelves);
            }

            if (!Input::has('type_association_shelves')) {
                $this->delete_sampling_detail($sampling, 'Pasillos');
            }

            if (Input::has('type_association_departments')) {
                $asociation_departments = implode(',', Input::get('asociation_departments'));
                $this->save_sampling_detail($sampling, Input::get('type_association_departments'), $asociation_departments);
            }

            if (!Input::has('type_association_departments')) {
                $this->delete_sampling_detail($sampling, 'Departamentos');
            }

            if (Input::has('type_association_products_bougth')) {
                $asociation_products_bougth = implode(',', Input::get('asociation_products_bougth'));
                $this->save_sampling_detail($sampling, Input::get('type_association_products_bougth'), $asociation_products_bougth);
            }

            if (!Input::has('type_association_products_bougth')) {
                $this->delete_sampling_detail($sampling, 'Producto Comprado');
            }
            


            DB::commit();

            if (Input::get('sampling_product_id') != "") {
                $action = 'update';                    
                $this->admin_log('sampling', $sampling->id, $action);
                return Redirect::route('samplingProducts.index')->with('type', 'success')->with('message', 'Se Actualizo el producto de muestra con éxito.');
            } else {
                $action = 'insert';                    
                $this->admin_log('sampling', $sampling->id, $action);
                return Redirect::route('samplingProducts.index')->with('type', 'success')->with('message', 'Se guardo el producto de muestra con éxito.');
            }

        } catch (Exception $e) {
            DB::rollBack();
            return Redirect::route('samplingProducts.index')->with('type', 'error')->with('message', 'Ocurrio un error al guardar el producto de muestra.');
        }
    }

    private function save_sampling_detail($sampling, $type, $asociation)
    {
        $sampling_detail = \SampligDetail::where('sampling_id', $sampling->id)
            ->where('type', $type)
            ->first();

        if ($sampling_detail) {
            $sampling_detail->related_ids = $asociation;
        } else {
            $sampling_detail = new \SampligDetail();
            $sampling_detail->sampling_id = $sampling->id;
            $sampling_detail->type = $type;
            $sampling_detail->related_ids = $asociation;
        }
        
        $sampling_detail->save();
    }

    private function delete_sampling_detail($sampling, $type)
    {
        $sampling_detail = \SampligDetail::where('sampling_id', $sampling->id)
            ->where('type', $type)
            ->first();

        if($sampling_detail) {
            $sampling_detail->delete();
        }
    }

    /**
     * Funcion que realiza la busqueda de un producto para ser guardado como muestra
     */

    public function search_products_ajax()
    {
        if (Request::ajax()) {
            $products = StoreProductWarehouse::select('store_products.id', 'products.image_medium_url', 'products.reference', DB::raw("CONCAT( products.name, ' ',products.quantity, products.unit) AS name"))
                ->join('store_products', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->join('products', 'products.id', '=', 'store_products.product_id')
                ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
                ->where('type', 'Simple')
                ->where(function ($query) {
                    $query->where('products.name', 'LIKE', '%' . Input::get('term') . '%')
                        ->orWhere('products.reference', 'LIKE', '%' . Input::get('term') . '%')
                        ->orWhere('store_products.id', (int)Input::get('term'));
                })
                ->get();
            $data = [
                'products' => $products,
                'action' => 'add',
                'warehouse_id' => Input::get('warehouse_id')
            ];
            return View::make('admin.catalog.sampling.add_form', $data)->renderSections()['search-products'];
        }

    }

    /**
     * Funcion que retorna informacion de un producto según el tipo de busqueda
     */

    public function get_data_ajax()
    {
        switch (Input::get('type')) {
            case 'product':
                $store_product_id = Input::get('store_product_id');
                $warehouse_id = Input::get('warehouse_id');
                $product = StoreProduct::select('store_products.id', 'products.image_medium_url', 'products.reference', DB::raw("CONCAT( products.name, ' ',products.quantity, products.unit) AS name"))
                    ->join('products', 'products.id', '=', 'store_products.product_id')
                    ->where('store_products.id', $store_product_id)
                    ->get()
                    ->first();
                $product->delivery = Sampling::getSampligDelivered($warehouse_id, $store_product_id);
                $data = [
                    'product' => $product,
                    'warehouse_id' => $warehouse_id,
                    'action' => 'edit',
                    'warehouse' => \Warehouse::with('city')->find($warehouse_id),
                    'zones' => \Zone::where('warehouse_id', $warehouse_id)->orderBy('name', 'ASC')->get()
                ];
                return Response::json([
                    'status' => true,
                    'result' => View::make('admin.catalog.sampling.add_form', $data)->renderSections()['selected-product'],
                ]);
                break;
            //'Productos','Pasillos','Departamentos', 'Producto Comprado'
            case 'Producto Comprado':
            case 'Productos':
                $products = StoreProductWarehouse::select('store_products.id', 'products.image_medium_url', 'products.reference', DB::raw("CONCAT( products.name, ' ',products.quantity, products.unit) AS name"))
                    ->join('store_products', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->join('products', 'products.id', '=', 'store_products.product_id')
                    ->where('store_product_warehouses.warehouse_id', Input::get('warehouse_id'))
                    ->where('store_product_warehouses.status', 1)
                    ->get()
                    ->toArray();
                return Response::json([
                    'status' => true,
                    'result' => $products,
                ]);
                break;
            case 'Pasillos':
                $shelves = Shelf::select('shelves.id', 'shelves.name')
                    ->join('cities', 'shelves.store_id', '=', 'cities.coverage_store_id')
                    ->join('warehouses', 'cities.id', '=', 'warehouses.city_id')
                    ->where('warehouses.id', Input::get('warehouse_id'))
                    ->where('shelves.status', 1)
                    ->orderBy('shelves.name', 'ASC')
                    ->get()
                    ->toArray();

                return Response::json([
                    'status' => true,
                    'result' => $shelves,
                ]);
                break;
            case 'Departamentos':
                $deparments = Department::select('departments.id', 'departments.name')
                    ->join('cities', 'departments.store_id', '=', 'cities.coverage_store_id')
                    ->join('warehouses', 'cities.id', '=', 'warehouses.city_id')
                    ->where('warehouses.id', Input::get('warehouse_id'))
                    ->where('departments.status', 1)
                    ->orderBy('departments.name', 'ASC')
                    ->get()
                    ->toArray();

                return Response::json([
                    'status' => true,
                    'result' => $deparments,
                ]);
                break;
        }

    }
}