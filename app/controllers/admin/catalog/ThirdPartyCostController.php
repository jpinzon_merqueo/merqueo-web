<?php
namespace admin\catalog;

use admin\AdminController;
use Request;
use View;
use Input;
use Response;
use Warehouse;
use StoreProductWarehouse;
use Product;
use DB;
use StoreProduct;
use Redirect;


class ThirdPartyCostController extends AdminController
{
    /**
     * Listado de productos del convenio
     */
    public function index(){
        if (!Request::ajax()){
            $data = [
                'cities' => $this->get_cities(),
                'title' => 'Precios de productos para terceros',
                'types' => array('Seco', 'Frío')
            ];
            return View::make('admin.catalog.third_party.index', $data);
        }else{
            $warehouseId = Input::get('warehouse_id');
            $search = Input::get('search');
            $products  = Product::select('products.*', 'store_products.id as store_product_id')
                ->join('store_products','products.id','=','store_products.product_id')
                ->join('store_product_warehouses','store_product_warehouses.store_product_id','=','store_products.id')
                ->where('store_product_warehouses.warehouse_id', $warehouseId);

            if(!empty($search)){
                $products->where(function($query)use($search){
                    $query->orWhere('reference', $search)
                        ->orWhere('name', 'LIKE', '%'.$search.'%' );
                    if(is_numeric($search)){
                        $query->orWhere('store_products.id', $search);
                    }
                });
            }

            $products = $products->get();

            $data = [
                'products' => $products
            ];
            return View::make('admin.catalog.third_party.index', $data)->renderSections()['table'];
        }
    }

    /**
     * Retorna el listado de bodegas asociadas a Darksupermarket
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDarkSupermarketWarehouses(){
        $warehouses = Warehouse::where('city_id', Input::get('city_id'))->whereIn('id', Warehouse::WAREHOUSE_DARKSUPERMARKET_ID)->get();
        return Response::json([
            'status' => true,
            'result' => $warehouses
        ], 200);
    }

    /**
     * Muestra el formulario de actualizacion de costos de DarkSupermarket
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function updateThirdPartyCostForm(){
        $data = [
            'title' => 'Actualizar precios de productos para terceros'
        ];
        return View::make('admin.catalog.third_party.price', $data);
    }

    /**
     * Procesa el archivo e costos de darkSupermarket
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateThirdPartyCost(){
        set_time_limit(3600);
        $data = [
            'title' => 'Actualizar precios de productos para terceros'
        ];
        try{
            DB::beginTransaction();

            $file = Input::file('file');

            $file = fopen($file->getRealPath(), 'r');
            $i=0;
            while (!feof($file)){
                $line = fgets($file);
                if($i>0 && !empty(trim($line))){
                    $line = explode(';', trim($line));
                    $storeProduct = StoreProduct::find($line[0]);
                    $storeProduct->third_party_cost = $line[1];
                    $storeProduct->save();
                }
                $i++;
            }
            DB::commit();
            return Redirect::route('adminThirdPartyProducts.updateThirdPartyCostForm')
                ->with('type', 'success')
                ->with('message', 'Se actualizo el costo de los productos.');

        }catch(\Exception $e){
            DB::rollBack();
            return Redirect::route('adminThirdPartyProducts.updateThirdPartyCostForm')
                ->with('type', 'error')
                ->with('message', 'Ocurrio un error al actualizar el costo de los productos: '.$e->getMessage());
            
        }

    }

    /**
     * Formulario para añadir los productos a la bodega de DarkSupermarket
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function addThirdPartyProducts(){
        $data = [
            'title' => 'Añadir productos para terceros'
        ];
        return View::make('admin.catalog.third_party.form', $data);
    }

    /**
     * Procesa el archivo de productos de DarkSupermarket
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveThirdPartyProducts(){
        set_time_limit(3600);
        ini_set('max_execution_time', 3600);
        $data = [
            'title' => 'Añadir productos para terceros'
        ];
        try{
            DB::beginTransaction();

            $file = Input::file('file');

            $file = fopen($file->getRealPath(), 'r');
            $i=0;
            while (!feof($file)){
                $line = fgets($file);
                if($i>0 && !empty(trim($line))){
                    $line = explode(';', trim($line));
                    $storeProductWarehouse = StoreProductWarehouse::where('store_product_id', $line[0])->where('warehouse_id', $line[3])->count();
                    if(!$storeProductWarehouse){
                        $newStoreProductWarehouse = new StoreProductWarehouse();
                        $newStoreProductWarehouse->warehouse_id = $line[3];
                        $newStoreProductWarehouse->store_product_id = $line[0];
                        $newStoreProductWarehouse->storage_position = $line[2];
                        $newStoreProductWarehouse->storage_height_position = $line[1];
                        $newStoreProductWarehouse->discontinued = 0;
                        $newStoreProductWarehouse->minimum_picking_stock = 100;
                        $newStoreProductWarehouse->maximum_picking_stock = 1500;
                        $newStoreProductWarehouse->manage_stock = 1;
                        $newStoreProductWarehouse->is_visible_stock = 0;
                        $newStoreProductWarehouse->picking_stock = $line[4];
                        $newStoreProductWarehouse->current_stock = $line[4];
                        $newStoreProductWarehouse->return_stock = 0;
                        $newStoreProductWarehouse->shrinkage_stock = 0;
                        $newStoreProductWarehouse->is_visible = 1;
                        $newStoreProductWarehouse->status = 1;
                        $newStoreProductWarehouse->save();
                    }

                }
                $i++;
            }
            DB::commit();

            return Redirect::route('adminThirdPartyProducts.addThirdPartyProducts')
                ->with('type', 'success')
                ->with('message', 'Se añadieron los productos a la bodega.');
        }catch(\Exception $e){
            DB::rollBack();
            return Redirect::route('adminThirdPartyProducts.addThirdPartyProducts')
                ->with('type', 'error')
                ->with('message', 'Ocurrio un error al agregar los productos a la bodega: '.$e->getMessage());
        }

    }

}