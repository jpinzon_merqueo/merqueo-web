<?php

namespace admin\catalog;

use Request, View, Input, Route, Redirect, Session, DB, Config, Response, Validator, admin\AdminController, 
    Store, City, Department, Shelf, File, StoreProduct, Carbon\Carbon;

class AdminGenerateDeeplinkController extends AdminController
{
	/**
	* carga formulario para generar deeplink y copiarlo
	*/
	public function index()
	{
		$city = City::find(Session::get('admin_city_id'));
		if (!Request::ajax()) {
            $stores = Store::getActiveStores($city->id);
            $data = [
                'title'  => 'Generar Deeplink',
                'stores' => $stores,
                'cities' => $this->get_cities()
            ];

            return View::make('admin.catalog.deeplink.index', $data);
        }
	}

	/**
	* Obtener tiendas por ciudad ajax
	*/
    public function get_stores_ajax()
    {
        $stores = array();
        $associated_store_products = array();
        $message = 'Error al cargar';
        $status = 400;
        if (Input::has('city_id')) {
            $stores = Store::select('id', 'name')
                ->where('city_id', Input::get('city_id'))
                ->where('status', 1)
                ->where('is_hidden', 0)
                ->get()->toArray();

            $message = 'Tiendas';
            $status = 200;
            $response = Response::json(['result' => ['stores' => $stores], 'status' => $status, 'message' => $message], 200);
        } else {
            $type = Input::get('type');
            $store_id = Input::get('store_id');
            if ($type == 'store') {
                $deeplink = Store::select('deeplink_store')->find($store_id);
                $deeplink = $deeplink->deeplink_store;
            } else if ($type == 'cart') {
                $deeplink = Store::select('deeplink_cart')->find($store_id);
                $deeplink = $deeplink->deeplink_cart;
            } else {
                $deeplink = Store::select('deeplink_banner')->find($store_id);
                $deeplink = $deeplink->deeplink_banner;
            }
            $response = Response::json($deeplink, 200);
        }
        return $response;
    }

	/**
	* Obtener categorias por ajax
	*/
    public function get_departments_ajax()
    {
        $store_id = Input::get('store_id');
        $department_id = Input::get('department_id');
        if(!empty($store_id)){
	        $departments = Department::where('store_id', $store_id)->orderBy('departments.name', 'ASC')->get();
	        $first_department = 0;
	        foreach ($departments as $department) {
	            $first_department = $department->id;
	            break;
	        }
	        $departments_data = array();
	        foreach ($departments as $department) {
	            $data = array();
	            $data['id'] = $department->id;
	            $data['name'] = $department->name;
	            $data['status'] = $department->status;
	            $data['shelves'] = Shelf::where('department_id', $department->id)->orderBy('name', 'ASC')->get()->toArray();
	            array_push($departments_data, $data);
	        }
	        $response_code = 200;
	        $response = Response::json($departments_data, $response_code);
	    }else{
            if(!empty($department_id)) {
                $deeplink_department = Department::select('deeplink')->find($department_id);
                $response_code = 200;
                $response = (!is_null($deeplink_department->deeplink)) ? Response::json($deeplink_department->deeplink, $response_code) : Response::json('El deeplink del departamento no se encuentra regitrado en la base de datos.', 500);
            } else {
                $response = Response::json('Error, debes seleccionar una tienda.', 500);
            }
	    }
	    return $response;
    }

    /**
     * Obtener subcategorias por ajax
     */
    public function get_shelves_ajax()
    {
        $department_id = Input::get('department_id');
        $shelf_id = Input::get('shelf_id');
        if(!empty($department_id)){
        	$shelves = Shelf::where('department_id', $department_id)->orderBy('name', 'ASC')->get()->toArray();
        	$response = Response::json($shelves, 200);
    	}else{
            if(!empty($shelf_id)) {
    		    $shelf = Shelf::select('deeplink')->find($shelf_id);
    		    $response = (!is_null($shelf->deeplink)) ? Response::json($shelf->deeplink, 200) : Response::json("El deeplink del pasillo no se encuentra en la base de datos.", 500);
    		} else {
    		    $response = Response::json('Error, debes seleccionar un departamento.', 500);
    		}
    	}
        return $response;
    }

	/**
	* Obtener productos ajax
	*/
	public function get_products_ajax(){
		$shelf_id = Input::get('shelf_id');
		$store_product_id = Input::get('store_product_id');
		if(!empty($shelf_id)) {
        	$products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                                ->where('store_products.shelf_id', $shelf_id)
                                ->select('store_products.*', 'products.name', 'store_product_warehouses.status')
                                ->orderBy('products.name', 'ASC')
                                ->groupBy('store_products.id')
                                ->get();
			$response = Response::json($products, 200);
        } else {
		     if(!empty($store_product_id)) {
                $product = StoreProduct::select('deeplink')->find($store_product_id);
                 $response = (!is_null($product->deeplink)) ? Response::json($product->deeplink, 200) : Response::json('El deeplink del producto no se encuentra registrado en la base de datos.', 500);
        	} else {
        	    $response = Response::json('Error, debes seleccionar un pasillo y un producto.', 500);
        	}
		}
        return $response;
	}

}