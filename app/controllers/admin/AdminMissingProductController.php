<?php

namespace admin;

use Illuminate\Database\QueryException;
use View, Store, Request, Order, DB, Input, Response, Session, OrderProductGroup, OrderProduct,
    MissignStoreProductLog, Redirect, OrderLog, Event, OrderEventHandler, Exception;
use Carbon\Carbon;
use exceptions\PayUException;

class AdminMissingProductController extends AdminController
{
    /**
     * Obtiene la cantidad de productos faltantes por ruta
     *
     * @return Response
     */
    public function index()
    {
        if (!Request::ajax()) {
            $message = Session::get('message');
            $error = Session::get('error');
            Session::set('message', null);
            Session::set('error', null);
            $cities = $this->get_cities();
            $warehouses = $this->get_warehouses(Session::get('admin_city_id'));
            $stores = Store::getActiveStores(Session::get('admin_city_id'));
            $data = [
                'title' => 'Validación de pedidos',
                'cities' => $cities,
                'warehouses' => $warehouses,
                'stores' => $stores,
                'message' => $message,
                'error' => $error
            ];
            return View::make('admin.missing_product.index', $data);
        } else {
            $date = Carbon::now();
            $yesterday = $date->subDay()->toDateString();
            $warehouseId = Input::get('warehouse_id', null);
            $products = OrderProduct::with('storeProduct.product')
                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('routes', 'orders.route_id', '=', 'routes.id')
                ->join('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
                ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_products.store_product_id', '=', 'receptions.store_product_id')
                ->whereIn('orders.status', ['In Progress'])
                ->whereIn('order_products.fulfilment_status', ['Not Available', 'Missing'])
                ->where('order_groups.warehouse_id', $warehouseId)
                ->whereIn('order_products.type', ['Product', 'Muestra'])
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +2 days')))
                ->select(
                    'picking_group.picking_group',
                    'order_products.store_product_id',
                    'routes.route',
                    'routes.shift',
                    'orders.id',
                    'orders.delivery_time',
                    DB::raw('SUM(quantity) AS quantity'),
                    DB::raw('SUM(quantity_original) AS quantity_original'),
                    DB::raw('COUNT(DISTINCT(orders.id)) AS orders'),
                    'receptions.quantity_received',
                    DB::raw('routes.id route_id'),
                    'order_products.type'
                )
                ->groupBy('orders.id')
                ->orderBy('quantity', 'DESC')
                ->get();
            $products->each(function ($product) use ($warehouseId) {
                $product->current_stock = $product->storeProduct->getCurrentStock($warehouseId);
            });

            $products_grouped = OrderProductGroup::with('storeProduct.product')
                ->join('orders', 'order_product_group.order_id', '=', 'orders.id')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('routes', 'orders.route_id', '=', 'routes.id')
                ->join('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
                ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_product_group.store_product_id', '=', 'receptions.store_product_id')
                ->whereIn('orders.status', ['In Progress'])
                ->whereIn('order_product_group.fulfilment_status', ['Not Available', 'Missing'])
                ->where('order_groups.warehouse_id', $warehouseId)
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +2 days')))
                ->select(
                    DB::raw('DISTINCT(order_product_group.store_product_id) store_product_id'),
                    'routes.route',
                    'routes.shift',
                    'orders.id',
                    'picking_group.picking_group',
                    'orders.delivery_time',
                    DB::raw('routes.id route_id'),
                    DB::raw('COUNT(DISTINCT(orders.id)) AS orders'),
                    DB::raw('SUM(order_product_group.quantity) AS quantity'),
                    DB::raw('SUM(order_product_group.quantity_original) AS quantity_original'),
                    'receptions.quantity_received'
                )
                ->groupBy('orders.id')
                ->orderBy('quantity', 'DESC')
                ->get();
            $products_grouped->each(function ($product) use ($warehouseId, $products) {
                $product->current_stock = $product->storeProduct->getCurrentStock($warehouseId);
                $products->push($product);
            });

            $products = $products->sortByDesc(function ($product) {
                return $product->quantity;
            });

            $productsWithoutPlannign = $this->getMissingProductsFromOrdersWithoutPlannnig($warehouseId);
            $ordersWithoutPlannign = $this->sortResultsGeneralWithoutPlanning($productsWithoutPlannign);
            if (count($products) > 0 || count($ordersWithoutPlannign) > 0) {
                $data = $this->sortResultsGeneral($products);
                return View::make('admin.missing_product.index', ['orders' => $data, 'ordersWitoutPlannign' => $ordersWithoutPlannign, 'message' => null])
                    ->renderSections()['orders'];
            }
            return View::make('admin.missing_product.index', ['orders' => [], 'ordersWitoutPlannign' => [], 'message' => null])
                ->renderSections()['orders'];
        }
    }

    /**
     * Devuelve la lista de tiendas de una ciudad via ajax
     *
     * @return Response
     */
    public function getStoresAjax()
    {
        $city_id = Input::get('city_id', null);

        if (!empty($city_id)) {
            $stores = Store::where('city_id', $city_id)->where('status', 1)->get();
        } else {
            $stores = Store::where('status', 1)->get();
        }
        $data = ['stores' => $stores];
        return Response::json($data);
    }

    /**
     * Ordena los productos faltantes por jornada de entrega y ruta ruta
     *
     * @param products
     * @return Array $data
     */
    public function sortResultsGeneral($products)
    {
        $data = [];
        $orders = [];
        $ids = [];
        foreach ($products as $product) {
            if (!array_key_exists($product->shift, $orders) || !array_key_exists($product->route, $orders[$product->shift])) {
                $orders[$product->shift][$product->route] = 1;
                $ids[$product->id] = $product->id;
            } else {
                if (!array_key_exists($product->id, $ids)) {
                    $orders[$product->shift][$product->route]++;
                    $ids[$product->id] = $product->id;
                }
            }
        }
        foreach ($products as $product) {
            if (!array_key_exists($product->shift, $data) || !array_key_exists($product->route, $data[$product->shift]) || !array_key_exists($product->picking_group, $data[$product->shift][$product->route])) {
                $data[$product->shift][$product->route][$product->picking_group]['missing_products'] = $product->quantity;
                $data[$product->shift][$product->route][$product->picking_group]['orders'] = $orders[$product->shift][$product->route];
                $data[$product->shift][$product->route][$product->picking_group]['route_id'] = $product->route_id;
                $data[$product->shift][$product->route][$product->picking_group]['delivery_time'] = $product->delivery_time;
            } else {
                $data[$product->shift][$product->route][$product->picking_group]['missing_products'] += $product->quantity;
            }
        }
        return $data;
    }


    /**
     * Ordena los productos faltantes sin planeación
     *
     * @param products
     * @return Array $data
     */
    public function sortResultsGeneralWithoutPlanning($products)
    {
        $data = [];
        $orders = [];
        $ids = [];
        foreach ($products as $product) {
            if (!array_key_exists('SP', $orders) || !array_key_exists($product->zone, $orders['SP'])) {
                $orders['SP'][$product->zone] = 1;
                $ids[$product->id] = $product->id;
            } else {
                if (!array_key_exists($product->id, $ids)) {
                    $orders['SP'][$product->zone]++;
                    $ids[$product->id] = $product->id;
                }
            }
        }
        foreach ($products as $product) {
            if (!array_key_exists('SP', $data) || !array_key_exists($product->zone, $data['SP'])) {
                $data['SP'][$product->zone]['Rojo']['missing_products'] = $product->quantity;
                $data['SP'][$product->zone]['Rojo']['orders'] = $orders['SP'][$product->zone];
                $data['SP'][$product->zone]['Rojo']['zone_id'] = $product->zone_id;
                $data['SP'][$product->zone]['Rojo']['delivery_time'] = $product->delivery_time;
            } else {
                $data['SP'][$product->zone]['Rojo']['missing_products'] += $product->quantity;
            }
        }
        return $data;
    }


    /**
     * Obtiene la cantidad de productos faltantes por pedido de una ruta
     *
     * @param  route_id
     * @return Response
     */
    public function getOrdersFromRoute($routeId = null)
    {
        $data = $this->getOrdersFromRouteOrZone($routeId, null);
        return View::make('admin.missing_product.orders_from_route', $data);
    }


    /**
     * Obtiene la cantidad de productos faltantes por pedido de una ruta
     *
     * @param  zoneId
     * @return Response
     */
    public function getOrdersFromZone($zoneId = null)
    {
        $data = $this->getOrdersFromRouteOrZone(null, $zoneId);
        return View::make('admin.missing_product.orders_from_zone', $data);
    }

    public function getOrdersFromRouteOrZone($routeId = null, $zoneId = null)
    {
        $date = Carbon::now();
        $yesterday = $date->subDay()->toDateString();
        $warehouseId = Input::get('warehouse_id', null);
        $products = OrderProduct::with('storeProduct.product')
            ->join('orders', 'order_products.order_id', '=', 'orders.id')
            ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_products.store_product_id', '=', 'receptions.store_product_id');
        if ($routeId) {
            $products = $products
                ->select('routes.warehouse_id', 'routes.route')
                ->join('routes', 'orders.route_id', '=', 'routes.id')
                ->where('orders.route_id', $routeId);
        }
        if ($zoneId) {
            $products = $products
                ->select('zones.warehouse_id', 'zones.name AS route')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('zones', 'order_groups.zone_id', "=", 'zones.id')
                ->where('zones.id', '=', $zoneId)
                ->whereNull('orders.route_id');
        }
        $products = $products
            ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
            ->whereIn('orders.status', ['In Progress'])
            ->whereIn('order_products.fulfilment_status', ['Not Available', 'Missing'])
            ->whereIn('order_products.type', ['Product', 'Muestra'])
            ->select(
                'orders.id',
                'order_products.store_product_id',
                'orders.planning_sequence',
                'orders.delivery_time',
                DB::raw('SUM(IF(store_products.storage = "Seco", order_products.quantity, 0)) AS dry_products'),
                DB::raw('SUM(IF(store_products.storage != "Seco", order_products.quantity,0)) AS cold_products'),
                DB::raw('SUM(order_products.quantity) AS quantity'),
                DB::raw('SUM(order_products.quantity_original) AS quantity_original'),
                DB::raw('SUM(IF(store_products.storage = "Seco", order_products.quantity_original, 0)) AS quantity_original_dry'),
                DB::raw('SUM(IF(store_products.storage != "Seco", order_products.quantity_original, 0)) AS quantity_original_cold'),
                'receptions.quantity_received'
            )
            ->groupBy('orders.id')
            ->orderBy('quantity', 'DESC')
            ->get();

        $products->each(function ($product) {
            $product->current_stock = $product->storeProduct->getCurrentStock($product->warehouse_id);
        });

        $products_grouped = OrderProductGroup::with('storeProduct.product')
            ->join('orders', 'order_product_group.order_id', '=', 'orders.id')
            ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_product_group.store_product_id', '=', 'receptions.store_product_id');
        if ($routeId) {
            $products_grouped = $products_grouped
                ->select('routes.warehouse_id', 'routes.route', DB::raw('routes.id AS route_id'))
                ->join('routes', 'orders.route_id', '=', 'routes.id')
                ->join('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
                ->where('orders.route_id', $routeId);
        }
        if ($zoneId) {
            $products_grouped = $products_grouped
                ->select('zones.warehouse_id', 'zones.name AS route')
                ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('zones', 'order_groups.zone_id', "=", 'zones.id')
                ->where('zones.id', '=', $zoneId)
                ->whereNull('orders.route_id');
        }
        $products_grouped = $products_grouped
            ->whereIn('orders.status', ['In Progress'])
            ->whereIn('order_product_group.fulfilment_status', ['Not Available', 'Missing'])
            ->join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
            ->select(
                DB::raw('DISTINCT(order_product_group.store_product_id) store_product_id'),
                'orders.id',
                'order_product_group.store_product_id',
                'orders.planning_sequence',
                'orders.delivery_time',
                DB::raw('SUM(order_product_group.quantity) AS quantity'),
                DB::raw('SUM(order_product_group.quantity_original) AS quantity_original'),
                DB::raw('SUM(IF(store_products.storage = "Seco", order_product_group.quantity_original, 0)) AS quantity_original_dry'),
                DB::raw('SUM(IF(store_products.storage != "Seco",order_product_group.quantity_original,0)) AS quantity_original_cold'),
                DB::raw('SUM(IF(store_products.storage = "Seco", order_product_group.quantity, 0)) AS dry_products'),
                DB::raw('SUM(IF(store_products.storage != "Seco",order_product_group.quantity,0)) AS cold_products'),
                'receptions.quantity_received'
            )
            ->groupBy('orders.id')
            ->orderBy('quantity', 'DESC')
            ->get();

        $products_grouped->each(function ($product) use ($warehouseId, $products) {
            $product->current_stock = $product->storeProduct->getCurrentStock($product->warehouse_id);
            $products->push($product);
        });

        $products = $products->sortByDesc(function ($product) {
            return $product->quantity;
        });

        $orderedData = $this->sortResultOrders($products);
        $route = \Routes::find($routeId);
        $zone = \Zone::find($zoneId);
        $data ['data'] = $orderedData;
        if ($route) {
            $data['title'] = 'Faltantes de la ruta: ' . $route->route;
        } else {
            $data['title'] = 'Faltantes de la zona: ' . $zone->name;
        }
        return $data;
    }

    /**
     * Ordena los productos faltantes por pedido de una ruta
     *
     * @param orders
     * @return array
     */
    public function sortResultOrders($orders)
    {
        $data = [];
        foreach ($orders as $key => $order) {
            if (!array_key_exists($order->id, $data)) {
                $data[$order->id]["quantity"] = $order->quantity_original - $order->quantity;
                $data[$order->id]["planning_sequence"] = $order->planning_sequence;
                $data[$order->id]["delivery_time"] = $order->delivery_time;
                $data[$order->id]["cold_products"] = $order->quantity_original_cold - $order->cold_products;
                $data[$order->id]["dry_products"] = $order->quantity_original_dry - $order->dry_products;
                $data[$order->id]["quantity_original_dry"] = $order->quantity_original_dry;
                $data[$order->id]["quantity_original_cold"] = $order->quantity_original_cold;
            }
        }
        return $data;
    }

    /**
     * obtiene la lista de productos faltantes de una ruta
     *
     * @param route_id
     * @return Response
     */
    public function getProductsFromRoute($route_id = null)
    {
        $products = $this->getDataForRenderView(null, $route_id);
        $route = \Routes::find($route_id);
        $data = [
            'title' => '<div class="duo"><div>Faltantes de la ruta ' . $route->route . '</div><div class="padding-right Rojo">Faltan ' . $products['quantity'] . ' productos</div></div>',
            'products' => $products['products'],
        ];
        return View::make('admin.missing_product.products_from_route', $data);
    }

    /**
     * obtiene la lista de productos faltantes de una ruta
     *
     * @param route_id
     * @return Response
     */
    public function getProductsFromZone($zoneId = null)
    {
        $products = $this->getDataForRenderView(null, null, $zoneId);
        $zone = \Zone::find($zoneId);
        $data = [
            'title' => '<div class="duo"><div>Faltantes de la zona ' . $zone->name . '</div><div class="padding-right Rojo">Faltan ' . $products['quantity'] . ' productos</div></div>',
            'products' => $products['products'],
        ];
        return View::make('admin.missing_product.products_from_zone', $data);
    }

    /**
     * obtiene la lista de productos faltantes de una Orden
     *
     * @param order_id
     * @param type ['Seco', 'frio']
     * @return Response
     */
    public function getProductsFromOrder($order_id = null, $type = null)
    {

        $products = $this->getDataForRenderView($order_id, null, $type);
        $data = [
            'title' => '<div class="duo"><div>Pedido #' . $order_id . ' Secuenacia: ' . $products['planning_sequence'] . '</div><div class="padding-right Rojo">Faltan ' . $products['quantity'] . ' productos</div></div>',
            'products' => $products['products'],
            'order_id' => $order_id,
            'message' => null

        ];
        return View::make('admin.missing_product.products_from_order', $data);
    }

    /**
     * obtiene la lista de productos faltantes de una Orden o ruta
     *
     * @param order_id
     * @param route_id
     * @param zone_id
     * @return Response
     */
    public function getDataForRenderView($orderId = null, $routeId = null, $zoneId = null)
    {
        $date = Carbon::now();
        $yesterday = $date->subDay()->toDateString();
        $quantity = 0;
        $products = OrderProduct::with('storeProduct.product')
            ->join('orders', 'order_products.order_id', '=', 'orders.id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details 
                                       INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id 
                                       WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "' 
                                       GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_products.store_product_id', '=', 'receptions.store_product_id')
            ->whereIn('orders.status', ['In Progress'])
            ->whereIn('order_products.fulfilment_status', ['Not Available', 'Missing']);
        if ($orderId) {
            $products->where('orders.id', $orderId);
        }
        if ($routeId) {
            $products = $products
                ->select('routes.warehouse_id', 'routes.route')
                ->join('routes', 'orders.route_id', '=', 'routes.id')
                ->where('orders.route_id', $routeId);
        }
        if ($zoneId) {
            $products = $products
                ->select('zones.warehouse_id', 'zones.name AS route')
                ->join('zones', 'order_groups.zone_id', "=", 'zones.id')
                ->where('zones.id', '=', $zoneId)
                ->whereNull('orders.route_id');
        }
        $products = $products->whereIn('order_products.type', ['Product', 'Muestra'])
            ->select(
                'order_products.store_product_id',
                DB::raw('SUM(order_products.quantity) AS quantity'),
                DB::raw('SUM(order_products.quantity_original) AS quantity_original'),
                DB::raw('CONCAT(store_product_warehouses.storage_position,"-", store_product_warehouses.storage_height_position) picking_position'),
                'receptions.quantity_received',
                'order_products.type',
                'order_groups.warehouse_id',
                'store_products.storage',
                'products.id AS product_id',
                'products.reference',
                'products.quantity AS product_quantity',
                'products.unit',
                'products.name',
                'products.image_medium_url',
                'store_product_warehouses.picking_stock',
                'store_product_warehouses.maximum_picking_stock',
                'orders.planning_sequence'
            )
            ->where('order_groups.warehouse_id', DB::raw('store_product_warehouses.warehouse_id'))
            ->groupBy('order_products.store_product_id')
            ->orderBy('store_product_warehouses.storage_position', 'ASC')
            ->get();

        $products->each(function ($product) {
            $product->current_stock = $product->storeProduct->getCurrentStock($product->warehouse_id);
            $product = \WarehouseStorage::getPositionNextExpiration($product, $product->warehouse_id);
        });
        $products_grouped = OrderProductGroup::with('storeProduct.product')
            ->join('orders', 'order_product_group.order_id', '=', 'orders.id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_product_group.store_product_id', '=', 'receptions.store_product_id')
            ->whereIn('orders.status', ['In Progress'])
            ->whereIn('order_product_group.fulfilment_status', ['Not Available', 'Missing']);
        if ($orderId) {
            $products_grouped = $products_grouped->where('orders.id', $orderId);
        }
        if ($routeId) {
            $products_grouped = $products_grouped
                ->select('routes.warehouse_id', 'routes.route')
                ->join('routes', 'orders.route_id', '=', 'routes.id')
                ->where('orders.route_id', $routeId);
        }
        if ($zoneId) {
            $products_grouped = $products_grouped
                ->select('zones.warehouse_id', 'zones.name AS route')
                ->join('zones', 'order_groups.zone_id', "=", 'zones.id')
                ->where('zones.id', '=', $zoneId)
                ->whereNull('orders.route_id');
        }
        $products_grouped = $products_grouped->select(
            'order_product_group.store_product_id',
            DB::raw('SUM(order_product_group.quantity_original) AS quantity_original'),
            DB::raw('SUM(order_product_group.quantity) AS quantity'),
            DB::raw('CONCAT(store_product_warehouses.storage_position,"-", store_product_warehouses.storage_height_position) picking_position'),
            'receptions.quantity_received',
            'store_products.storage',
            'products.id AS product_id',
            'products.reference',
            'products.quantity AS product_quantity',
            'products.unit',
            'products.name',
            'products.image_medium_url',
            'store_product_warehouses.picking_stock',
            'store_product_warehouses.maximum_picking_stock',
            'orders.planning_sequence'
        )
            ->where('order_groups.warehouse_id', DB::raw('store_product_warehouses.warehouse_id'))
            ->groupBy('order_product_group.store_product_id')
            ->orderBy('store_product_warehouses.storage_position', 'ASC')
            ->get();


        $products_grouped->each(function ($product) use ($products) {
            $product->current_stock = $product->storeProduct->getCurrentStock($product->warehouse_id);
            $product = \WarehouseStorage::getPositionNextExpiration($product, $product->warehouse_id);
            $products->push($product);
        });

        $products = $products->sortBy(function ($product) {
            return $product->picking_position;
        });
        $planning_sequence = "";
        foreach ($products as $product) {
            $planning_sequence = $product->planning_sequence;
            $quantity += $product->quantity_original == $product->quantity ? $product->quantity : $product->quantity_original == null ? $product->quantity : $product->quantity_original - $product->quantity;
        }
        return ["products" => $products, "quantity" => $quantity, "planning_sequence" => $planning_sequence];
    }

    /**
     * Registra los datos de los faltantes ingresados
     *
     */
    public function completeOrder()
    {
        Event::subscribe(new OrderEventHandler);

        $request = input::all();
        $orderId = $request['order_id'];
        $products = $request['product']['reference'];
        $quantities = $request['product']['quantities'];
        try {
            DB::beginTransaction();
            $order = Order::find($orderId);
            if ($order->status != 'Alistado') {
                $date = Carbon::now();
                Event::fire('order.missing_products_log', [$orderId, Session::get('admin_id'), Session::get('user_id')]);
                \Log::info('Se comienza a modificar el pedido: ' . $orderId . ' desde el modulo de faltantes.');

                // Primero se busca en los combos
                $order_product_groups = OrderProductGroup::with('storeProduct.product')
                    ->join('orders', 'order_product_group.order_id', '=', 'orders.id')
                    ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                    ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_product_group.store_product_id', '=', 'receptions.store_product_id')
                    ->whereIn('orders.status', ['In Progress'])
                    ->whereIn('order_product_group.fulfilment_status', ['Not Available', 'Missing', 'Pending'])
                    ->where('orders.id', $orderId)
                    ->where('orders.delivery_date', '>', $date->format('Y-m-d 00:00:00'))
                    ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +2 days')))
                    ->select(
                        'order_product_group.*',
                        DB::raw('SUM(order_product_group.quantity_original) AS quantity_original'),
                        DB::raw('SUM(order_product_group.quantity) AS quantity'),
                        'receptions.quantity_received',
                        'order_groups.warehouse_id'
                    )
                    ->groupBy('order_product_group.id')
                    ->orderBy('quantity', 'DESC')
                    ->get();

                foreach ($order_product_groups as $order_product_group) {
                    $order_product = OrderProduct::find($order_product_group->order_product_id);
                    foreach ($products as $key => $reference) {
                        if ($reference == $order_product_group->storeProduct->product->reference && ($order_product_group->fulfilment_status == 'Missing' || $order_product_group->fulfilment_status == 'Pending')) {
                            if ($quantities[$key] <= 0 && $order_product_group->quantity == $order_product_group->quantity_original ) {
                                $order_product_group->fulfilment_status = 'Not Available';
                                $order_product->fulfilment_status = 'Not Available';
                                $order_product->save();

                            } else {
                                $order_product_group->fulfilment_status = 'Fullfilled';
                                $order_product->fulfilment_status = 'Fullfilled';
                                $order_product->save();
                            }
                            if ($quantities[$key] > 0) {
                                $oldQuantity = $order_product_group->quantity;
                                $order_product_group->quantity = ($order_product_group->quantity_original - $order_product_group->quantity) > $quantities[$key] ? $order_product_group->quantity + $quantities[$key] : $order_product_group->quantity_original < $quantities[$key] ? $order_product_group->quantity_original : $quantities[$key];;
                                $quantities[$key] -= ($order_product_group->quantity_original - $order_product_group->quantity) > $quantities[$key] ? $order_product_group->quantity + $quantities[$key] : $order_product_group->quantity_original < $quantities[$key] ? $order_product_group->quantity_original : $quantities[$key];;
                                \Log::info('Modificando estado de productos agrupados desde el modulo de faltantes del pedido: ' . $orderId . ' cambiada cantidad del producto ' . $reference . ' de ' . $oldQuantity . ' a ' . $order_product_group->quantity . " y se pasa a estado " . $order_product_group->fulfilment_status);
                            }
                            $order_product_group->save();
                            Event::fire('order.product_log', [[$order_product_group, Session::get('admin_id'), 'admin']]);
                        }
                    }
                }

                $order_products = OrderProduct::with('storeProduct.product')
                    ->join('orders', 'order_products.order_id', '=', 'orders.id')
                    ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                    ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id  GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_products.store_product_id', '=', 'receptions.store_product_id')
                    ->whereIn('orders.status', ['In Progress'])
                    ->whereIn('order_products.fulfilment_status', ['Not Available', 'Missing', 'Pending'])
                    ->where('orders.id', $orderId)
                    ->whereIn('order_products.type', ['Product', 'Muestra'])
                    ->where('orders.delivery_date', '>', $date->format('Y-m-d 00:00:00'))
                    ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +2 days')))
                    ->select(
                        'order_products.*',
                        DB::raw('SUM(order_products.quantity_original) AS quantity_original'),
                        DB::raw('SUM(order_products.quantity) AS quantity'),
                        'receptions.quantity_received',
                        'order_groups.warehouse_id'
                    )
                    ->groupBy('order_products.id')
                    ->orderBy('price')
                    ->get();

                $order_products->each(function ($product) use ($order_product_groups) {
                    $order_product_groups->push($product);
                });

                foreach ($order_products as $order_product) {
                    foreach ($products as $key => $reference) {
                        if ($reference == $order_product->storeProduct->product->reference && ($order_product->fulfilment_status == 'Missing' || $order_product->fulfilment_status == 'Pending')) {
                            if ($quantities[$key] <= 0 && $order_product->quantity == $order_product->quantity_original)
                                $order_product->fulfilment_status = 'Not Available';
                            else
                                $order_product->fulfilment_status = 'Fullfilled';
                            if ($quantities[$key] > 0) {
                                $oldQuantity = $order_product->quantity;
                                $order_product->quantity = ($order_product->quantity_original - $order_product->quantity) > $quantities[$key] ? $order_product->quantity + $quantities[$key] : $order_product->quantity_original < $quantities[$key] ? $order_product->quantity_original : $quantities[$key];
                                $quantities[$key] -= ($order_product->quantity_original - $order_product->quantity) > $quantities[$key] ? $order_product->quantity + $quantities[$key] : $quantities[$key] ? $order_product->quantity_original : $quantities[$key];
                                \Log::info('Modificando estado de productos desde el modulo de faltantes del pedido: ' . $orderId . ' cambiada cantidad del producto ' . $reference . ' de ' . $oldQuantity . ' a ' . $order_product->quantity . " y se pasa a estado " . $order_product->fulfilment_status);
                            }
                            $order_product->save();

                            Event::fire('order.product_log', [[$order_product, Session::get('admin_id'), 'admin']]);
                        }
                    }
                }

                $has_pending_products = false;
                foreach ($order_product_groups as $product) {
                    if ($product->fulfilment_status == "Pending" || $product->fulfilment_status == "Missing") {
                        $has_pending_products = true;
                    }
                }
                //Si el pedido no tiene pendientes se actualiza de lo contrario se informa al usuario
                if (!$has_pending_products) {
                    $order->updateTotals();

                    Event::fire('order.total_log', [[$order, Session::get('admin_id'), 'admin']]);

                    //actualizar pedido a Alistado
                    if ($order->status == 'Enrutado' || $order->status == 'In Progress') {

                        $order->updatePickingStock();
                        if ($order->payment_method == 'Tarjeta de crédito') {
                            $order->save();
                            $this->chargeOrder($order);
                        }
                        $order->status = 'Alistado';
                    } elseif ($order->status != 'Alistado') {
                        $order->status = 'In Progress';
                    }
                    $order->status = 'Alistado';
                    $order->picking_date = Carbon::now();
                    Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);
                    $order->save();
                    \Log::info('Modificando estado de pedido: ' . $order->id . ' a ' . $order->status . ' desde app faltantes');

                    //Se crea el log
                    foreach ($products as $key => $reference) {
                        $log = new MissignStoreProductLog();
                        $log->order_id = $orderId;
                        $log->user_id = Session::get('admin_id');
                        $log->reference = $reference;
                        $log->quantity_add = $quantities[$key];
                        $log->data = json_encode($request);
                        $log->save();
                    }

                    DB::commit();

                    Session::set('message', 'Pedido actualizado con éxito');
                    return Redirect::route('adminMissingProduct.index');

                } else {
                    DB::rollback();
                    \Log::info('Se hace rolback desde el modulo faltantes al pedido: ' . $orderId . ' por que el pedido tiene productos pendientes.');
                    Session::set('message', 'No se pueden alistar los faltantes por que el pedido tiene productos pendientes.');
                    Session::set('error', "si");
                    return Redirect::route('adminMissingProduct.index');
                }
            } else {
                DB::rollback();
                \Log::info('Se hace rolback desde el modulo faltantes al pedido: ' . $orderId . ' por que el pedido ya esta en estado Alistado.');
                Session::set('message', 'No se pueden alistar los faltantes por que el pedido ya esta en estado Alistado.');
                Session::set('error', "si");
                return Redirect::route('adminMissingProduct.index');
            }

        } catch (PayUException $e) {
            DB::rollback();

            $log = new OrderLog();
            $log->type = $e->getMessage();
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminMissingProduct.index', ['id' => $order->id])->with('error', 'si')->with('message', $e->getMessage());

        } catch (QueryException $e) {
            DB::rollBack();

            \ErrorLog::add($e, $e->getCode() == '40001' ? 513 : 500);

            if ($e->getCode() == '40001') {

                return Redirect::route('adminMissingProduct.index', ['id' => $order->id])->with('error', 'si')
                    ->with('message', 'Otro proceso se está ejecutando sobre estos productos, espera un momento para volver a intentarlo.');
            }

            return Redirect::route('adminMissingProduct.index', ['id' => $order->id])->with('error', 'si')->with('message', $e->getMessage());
        } catch (Exception $e) {
            DB::rollback();

            \ErrorLog::add($e);

            \Log::info('Se hace rolback desde el modulo faltantes al pedido: ' . $orderId . ' Por la siguiente excepción: ' . $e->getMessage() . " en la linea: " . $e->getLine());
            $products = $this->getDataForRenderView($orderId, null);
            $data = [
                'title' => '<div class="duo"><div>Pedido #' . $orderId . '</div><div class="padding-right Rojo">Faltan ' . $products['quantity'] . ' productos</div></div>',
                'products' => $products['products'],
                'order_id' => $orderId,
                'message' => 'Ocurrió un error al actualizar el pedido: ' . $e->getMessage()
            ];
            return View::make('admin.missing_product.products_from_order', $data);
        }
    }


    /**
     *
     * @author Alexander Correa
     *
     * getMissingProductsFromOrdersWithoutPlannnig
     *
     * Función para obtener los faltantes de pedidos sin planeación para una bodega
     * Function yo get missing products from orders without planning
     *
     * @param  int $warehouseId id de la bodega a consutar
     * @return Object $products
     */
    public function getMissingProductsFromOrdersWithoutPlannnig($warehouseId)
    {
        $yesterday = Carbon::now()->subDay()->toDateString();
        $products = OrderProduct::with('storeProduct.product')
            ->join('orders', 'order_products.order_id', '=', 'orders.id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('zones', 'order_groups.zone_id', "=", 'zones.id')
            ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_products.store_product_id', '=', 'receptions.store_product_id')
            ->whereIn('orders.status', ["In Progress"])
            ->whereIn('order_products.fulfilment_status', ["Not Available", "Missing"])
            ->where('order_groups.warehouse_id', $warehouseId)
            ->whereIn('order_products.type', ["Product", "Muestra"])
            ->where('orders.delivery_date', '>', date("Y-m-d 00:00:00", strtotime(Carbon::now())))
            ->where('orders.delivery_date', '<', date("Y-m-d 23:59:59", strtotime(Carbon::now())))
            ->whereNull('orders.route_id')
            ->select(
                'order_products.store_product_id',
                'orders.id',
                'orders.delivery_time',
                'zones.name AS zone',
                'zones.id AS zone_id',
                DB::raw('SUM(quantity) AS quantity'),
                DB::raw('SUM(quantity_original) AS quantity_original'),
                DB::raw('COUNT(DISTINCT(orders.id)) AS orders'),
                'receptions.quantity_received',
                'order_products.type'
            )
            ->groupBy('orders.id')
            ->orderBy('quantity', 'DESC')
            ->get();

        $products->each(function ($product) use ($warehouseId) {
            $product->current_stock = $product->storeProduct->getCurrentStock($warehouseId);
        });

        $products_grouped = OrderProductGroup::with('storeProduct.product')
            ->join('orders', 'order_product_group.order_id', '=', 'orders.id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->leftJoin(DB::raw("(SELECT provider_order_reception_details.store_product_id, SUM(provider_order_reception_details.quantity_received) AS quantity_received FROM provider_order_reception_details INNER JOIN provider_order_receptions ON provider_order_reception_details.reception_id = provider_order_receptions.id WHERE DATE(provider_order_receptions.received_date) = '" . $yesterday . "'GROUP BY provider_order_reception_details.store_product_id HAVING quantity_received > 0 ) AS receptions"), 'order_product_group.store_product_id', '=', 'receptions.store_product_id')
            ->whereIn('orders.status', ["In Progress"])
            ->whereIn('order_product_group.fulfilment_status', ["Not Available", "Missing"])
            ->where('order_groups.warehouse_id', $warehouseId)
            ->where('orders.delivery_date', '>', date("Y-m-d 00:00:00", strtotime(Carbon::now())))
            ->where('orders.delivery_date', '<', date("Y-m-d 23:59:59", strtotime(Carbon::now())))
            ->whereNull('orders.route_id')
            ->select(
                DB::raw('DISTINCT(order_product_group.store_product_id) store_product_id'),
                'orders.id',
                'orders.delivery_time',
                'zones.name AS zone',
                'zones.id AS zone_id',
                DB::raw('COUNT(DISTINCT(orders.id)) AS orders'),
                DB::raw('SUM(order_product_group.quantity) AS quantity'),
                DB::raw('SUM(order_product_group.quantity_original) AS quantity_original'),
                'receptions.quantity_received'
            )
            ->groupBy('orders.id')
            ->orderBy('quantity', 'DESC')
            ->get();
        $products_grouped->each(function ($product) use ($warehouseId, $products) {
            $product->current_stock = $product->storeProduct->getCurrentStock($warehouseId);
            $products->push($product);
        });

        $products = $products->sortByDesc(function ($product) {
            return $product->quantity;
        });

        return $products;
    }


    /**
     * Cobrar total de pedido a tarjeta de cliente
     *
     * @param Object $order
     *
     * @throws {
     *      Si ocurre un error en el cobro con tarjeta el pedido dejará de aparecer en faltantes, pero no será marcado como alistado,
     *      esto deberá hacerse por dashborad
     * }
     */
    private function chargeOrder($order)
    {
        //cobrar pedido a tarjeta
        $payment = $order->getPayments(true);
        $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
        if ($total > 0 && $order->payment_method == 'Tarjeta de crédito' && empty($order->payment_date) &&
            (
                ($payment && $payment->cc_payment_status == 'Aprobada' && !empty($payment->cc_refund_date)) ||
                ($payment && $payment->cc_payment_status != 'Aprobada' && empty($payment->cc_refund_date)) ||
                !$payment
            )) {
            $result = Order::chargeCreditCard($order->id, $order->user_id);

            if (!$result['status']) {
                throw new PayUException("{$result['message']} Este pedido no aparecerá más como faltante en este módulo y debe gestionarse manualmente.");
            }

            $log = new OrderLog();
            $log->type = 'Cobro a tarjeta exitoso.';
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();
        }
    }
}
