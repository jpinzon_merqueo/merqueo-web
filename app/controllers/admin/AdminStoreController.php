<?php

namespace admin;

use Request, View, Input, Redirect, Session, DB, Str, Validator, Carbon\Carbon, Queue,
    Store, City, Department, Shelf, Product, Slot, Response, Config,
    Supermarket, StoreBranchSlot, Holidays;

class AdminStoreController extends AdminController
{
    /**
     * Grilla de tiendas
     */
    public function index()
    {
        $stores = Store::select('stores.*', 'cities.city', 'cities.city')->join('cities', 'cities.id', '=', 'stores.city_id');

        if (!Request::ajax()) {
            $stores = $stores->where('stores.status', 1)->get();
            return View::make('admin.stores.index')
                ->with('title', 'Tiendas')
                ->with('stores', $stores);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            if ($search != '')
                $stores = $stores->where('stores.name', 'LIKE', '%'.$search.'%');

            $stores = $stores->where('stores.status', 1)->get();

            return View::make('admin.stores.index')
                ->with('title', 'Tiendas Merqueo')
                ->with('stores', $stores)
                ->renderSections()['content'];
        }
    }

    /**
     * Editar tienda
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Tienda' : 'Nueva Tienda';

        if (!$id && Session::has('post')){
            $store = (object) Session::get('post');
            $store->logo_url = $store->logo_small_url = $store->app_logo_large_url = $store->app_logo_small_url = $store->id = 0;
        }else $store = Store::find($id);

        if (Session::get('admin_designation') != 'Super Admin')
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        else $cities = City::all();

        return View::make('admin.stores.store_form')
            ->with('title', 'Tienda')
            ->with('sub_title', $sub_title)
            ->with('cities', $cities)
            ->with('store', $store);
    }

    /**
     * Guardar tienda
     */
    public function save()
    {
        $id = Input::get('id');
        $store = Store::find($id);
        $flag = 0;
        if (!$store){
            $store = new Store;
            $flag = 1;
            $cond = 'required|unique:stores';
            $store->slug = Str::slug(Input::get('name'));
            if (Store::where('slug', $store->slug)->where('city_id', Input::get('city_id'))->first())
                return Redirect::back()->with('error', 'A store with the same slug and city already exists.')->with('post', Input::all());
            $action = 'insert';
        }else{
            $cond = 'required|unique:stores,slug,'.$store->id;
            $action = 'update';
        }
        $store->name = Input::get('name');
        $store->minimum_order_amount = Input::get('minimum_order_amount');
        $store->nit = Input::get('nit');
        $store->phone = Input::get('phone');
        $store->city_id = Input::get('city_id');
        $store->schedule = Input::get('schedule');
        $store->description = Input::get('description');
        $store->description_app = Input::get('description_app');
        $store->description_web = Input::get('description_web');
        $store->delivery_time = Input::get('delivery_time');
        $store->delivery_time_minutes = Input::get('delivery_time_minutes');
        $store->revision_orders_required = Input::get('revision_orders_required');
        $app_background_color = Input::get('app_background_color');

        $store->app_background_color = trim(str_replace('#', '', $app_background_color));

        $latlng = Input::get('latlng');
        if (!empty($latlng)) {
            $latlng = explode(',', trim($latlng));

            if (count($latlng) == 2 && validate_longitude(trim($latlng[0])) && validate_latitude(trim($latlng[1]))) {
                $store->latitude = trim($latlng[0]);
                $store->longitude = trim($latlng[1]);
            }else return Redirect::back()->with('error', 'La coordenada de la dirección no es valida.')->with('post', Input::all());
        }

        if (strlen($store->app_background_color) != 6)
            return Redirect::back()->with('error', 'El color de fondo para app debe ser de 6 caracteres.')->with('post', Input::all());
        $store->sort_order = Input::get('sort_order');
        $store->status = Input::get('status');

        $image = Input::file('logo');
        $validator = Validator::make(
            array('ima' => $image),
            array('ima' => 'required|mimes:jpeg,bmp,png')
        );
        if (Input::hasFile('logo') && $validator->fails()){
            $error_messages = $validator->messages();
            foreach ($error_messages->all() as $message)
                return Redirect::route('adminStores.index')->with('type', 'failed')->with('message', $message);
        }

        $image = Input::file('logo_small');
        $validator = Validator::make(
            array('ima' => $image),
            array('ima' => 'required|mimes:jpeg,bmp,png')
        );
        if (Input::hasFile('logo_small') && $validator->fails()){
            $error_messages = $validator->messages();
            foreach ($error_messages->all() as $message)
                return Redirect::route('adminStores.index')->with('type', 'failed')->with('message', $message);
        }

        $image = Input::file('app_logo_large');
        $validator = Validator::make(
            array('ima' => $image),
            array('ima' => 'required|mimes:jpeg,bmp,png')
        );
        if (Input::hasFile('app_logo_large') && $validator->fails()){
            $error_messages = $validator->messages();
            foreach ($error_messages->all() as $message)
                return Redirect::route('adminStores.index')->with('type', 'failed')->with('message', $message);
        }

        $image = Input::file('app_logo_small');
        $validator = Validator::make(
            array('ima' => $image),
            array('ima' => 'required|mimes:jpeg,bmp,png')
        );
        if (Input::hasFile('app_logo_small') && $validator->fails()){
            $error_messages = $validator->messages();
            foreach ($error_messages->all() as $message)
                return Redirect::route('adminStores.index')->with('type', 'failed')->with('message', $message);
        }

        $cloudfront_url = Config::get('app.aws.cloudfront_url');

        if (Input::hasFile('logo')){
            if (!empty($store->logo_url) && !strstr($store->logo_url, 'default')){
                $path = str_replace($cloudfront_url, uploads_path(), $store->logo_url);
                remove_file($path);
                remove_file_s3($store->logo_url);
            }
            $store->logo_url = upload_image(Input::file('logo'), false, 'stores/logos');
        }else if ($flag == 1)
            $store->logo_url = $cloudfront_url.'/store/logos/default_store_logo.png';

        if (Input::hasFile('logo_small')){
            if (!empty($store->logo_small) && !strstr($store->logo_small, 'default')){
                $path = str_replace($cloudfront_url, uploads_path(), $store->logo_small);
                remove_file($path);
                remove_file_s3($store->logo_small);
            }
            $store->logo_small_url = upload_image(Input::file('logo_small'), false, 'stores/logos');
        }else if ($flag == 1)
            $store->logo_small_url = $cloudfront_url.'/stores/logos/default_small_store_logo.png';

        if (Input::hasFile('app_logo_large')){
            if (!empty($store->app_logo_large_url) && !strstr($store->app_logo_large_url, 'default')){
                $path = str_replace($cloudfront_url, uploads_path(), $store->app_logo_large_url);
                remove_file($path);
                remove_file_s3($store->app_logo_large_url);
            }
            $store->app_logo_large_url = upload_image(Input::file('app_logo_large'), false, 'stores/logos');
        }else if ($flag == 1)
            $store->app_logo_large_url = $cloudfront_url.'/stores/logos/default_app_store_logo.png';

        if (Input::hasFile('app_logo_small')){
            if (!empty($store->app_logo_small_url) && !strstr($store->app_logo_small_url, 'default')){
                $path = str_replace($cloudfront_url, uploads_path(), $store->app_logo_small_url);
                remove_file($path);
                remove_file_s3($store->app_logo_small_url);
            }
            $store->app_logo_small_url = upload_image(Input::file('app_logo_small'), false, 'stores/logos');
        }else if ($flag == 1)
            $store->app_logo_small_url = $cloudfront_url.'/stores/logos/default_app_store_logo.png';

        $store->save();
        $this->admin_log('stores', $store->id, $action);

        $store->city_id = Input::get('city_id');

        return Redirect::route('adminStores.index')->with('type', 'success')->with('message', 'Tienda guardada con éxito.');
    }

    /**
     * Eliminar tienda
     */
    public function delete()
    {
        $id = Request::segment(4);
        try {
            DB::beginTransaction();

            Store::where('id', $id)->update(array('status' => 0));
            Department::where('store_id', $id)->update(array('status' => 0));
            Shelf::where('store_id', $id)->update(array('status' => 0));
            Product::where('store_id', $id)->update(array('status' => 0));
            $this->admin_log('stores', $id, 'delete');

            DB::commit();
            return Redirect::route('adminStores.index')->with('type', 'success')->with('message', 'Tienda eliminada con éxito.');

        }catch(\Exception $e){
            DB::rollback();
            return Redirect::route('adminStores.index')->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar la tienda.');
        }
    }

    /**
     * Grilla de categorias
     */
    public function departments()
    {
        $store_id = Request::segment(3);
        $departments = Department::where('store_id', $store_id)->get();
        return View::make('admin.stores.departments')
            ->with('title', 'Departamentos')
            ->with('departments', $departments)
            ->with('store_id', $store_id);
    }

    /**
     * Eliminar categoria
     */
    public function delete_department()
    {
        $id = Request::segment(6);
        Department::where('id', $id)->update(array('status' => 0));
        Shelf::where('department_id', $id)->update(array('status' => 0));
        $this->admin_log('departments', $id, 'delete');

        return Redirect::back()->with('type', 'success')->with('message', 'Departamento guardado con éxito.');
    }

    /**
     * Editar categoria
     */
    public function edit_department()
    {
        $id = Request::segment(6) ? Request::segment(6) : 0;
        $store_id = Request::segment(3);
        $sub_title = Request::segment(6) ? 'Editar Departamento' : 'Nuevo Departamento';

        if (!$id && Session::has('post'))
            $department = (object) Session::get('post');
        else $department = Department::find($id);

        return View::make('admin.stores.department_form')
            ->with('title', 'Tienda')
            ->with('sub_title', $sub_title)
            ->with('department', $department)
            ->with('store_id', $store_id);
    }

    /**
     * Guardar categoria
     */
    public function save_department()
    {
        $id = Input::get('id');
        $store_id = Input::get('store_id');
        $department = Department::find($id);
        $department_slug = Input::get('slug');
        if (!$department){
            $department = new Department;
            if (empty($department_slug))
                $department->slug = Str::slug(trim(Input::get('name')));
            else $department->slug = Str::slug(trim($department_slug));
            if (Department::where('slug', $department->slug)->where('store_id', $store_id)->first())
                return Redirect::back()->with('error', 'Ya existe un departamento con el mismo slug en esta tienda.')->with('post', Input::all());
            $action = 'insert';
        }else{
            if (empty($department_slug)) {
                $department->slug = Str::slug(trim(Input::get('name')));
            }else{
                if ( $department_slug != $department->slug ) {
                    $department->slug = Str::slug(trim($department_slug));
                    if (Department::where('slug', $department->slug)->where('id', '<>', $department->id)->where('store_id', $store_id)->first())
                        return Redirect::back()->with('error', 'Ya existe un departamento con el mismo slug en esta tienda.')->with('post', Input::all());
                }
            }
            $action = 'update';
        }
        $department->name = trim(Input::get('name'));
        $department->description = Input::get('description');
        $department->store_id = $store_id;
        $department->status = Input::get('status');
        $department->sort_order = Input::get('sort_order');
        $department->sap_item_group_code = Input::get('sap_item_group_code');

        $cloudfront_url = Config::get('app.aws.cloudfront_url');

        if(Input::hasFile('image_list_app_url'))
        {
            $image = Input::file('image_list_app_url');
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png,gif')
            );
            if ($validator->fails())
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

            if (!empty($department->image_list_app_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $department->image_list_app_url);
                remove_file($path);
                remove_file_s3($department->image_list_app_url);
            }
            $department->image_list_app_url = upload_image($image, false, 'stores/images/app');
        }
        if (Input::hasFile('image_header_app_url'))
        {
            $image = Input::file('image_header_app_url');
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png,gif')
            );
            if ($validator->fails())
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

            if (!empty($department->image_header_app_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $department->image_header_app_url);
                /*remove_file($path);
                remove_file_s3($department->image_header_app_url);*/
            }
            $department->image_header_app_url = upload_image($image, false, 'stores/images/app');
        }

        if (Input::hasFile('image_icon_url'))
        {
            $image = Input::file('image_icon_url');
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png,gif')
            );
            if ($validator->fails())
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

            if (!empty($department->image_icon_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $department->image_icon_url);
                /*remove_file($path);
                remove_file_s3($department->image_icon_url);*/
            }
            $department->image_icon_url = upload_image($image, false, 'stores/images/app');
        }

        $url_department = route('frontStoreDepartment.single_department', [
            'city_slug'       => $department->store->city->slug,
            'store_slug'      => $department->store->slug,
            'department_slug' => $department->slug
        ]);

        $url_department  = str_replace(Config::get('app.url'), 'https://merqueo.com', $url_department);
        $deeplink_department_url =  \FirebaseClient::generateDynamicLink([
            'url'           => $url_department,
            'type'          => 'department',
            'store_id'      => $department->store_id,
            'department_id' => $department->id,
        ]);
        $department->deeplink = $deeplink_department_url->shortLink;
        $department->save();

        $this->admin_log('departments', $department->id, $action);

        if (!Config::get('app.debug'))
            Queue::push('BatchUpdateIndex', array('department' => $department->id));

        if ('update' == $action) {
            return Redirect::route('adminStores.departments', ['id' => $store_id])->with('type', 'success')->with('message', 'Departamento guardado con éxito. DEBE ACTUALIZAR MANUALMENTE TODOS LOS DEEPLINKS DE ESTE DEPARTAMENTO.');
        }

        return Redirect::route('adminStores.departments', ['id' => $store_id])->with('type', 'success')->with('message', 'Departamento guardado con éxito.');
    }

    /**
     * Grilla de subcategorias
     */
    public function shelves()
    {
        $store_id = Request::segment(3);
        $department_id = Request::segment(5);
        $shelves = Shelf::where('department_id', $department_id)->get();
        return View::make('admin.stores.shelves')
            ->with('title', 'Pasillos')
            ->with('shelves', $shelves)
            ->with('store_id', $store_id)
            ->with('department_id', $department_id);
    }

    /**
     * Editar subcategoria
     */
    public function edit_shelf()
    {
        $id = Request::segment(8) ? Request::segment(8) : 0;
        $store_id = Request::segment(3);
        $department_id = Request::segment(5);
        $sub_title = Request::segment(8) ? 'Editar Pasillo' : 'Nuevo Pasillo';
        $sub_title_position = Request::segment(8) ? 'Editar Ubicación en bodega' : 'Ubicación en bodega';
        $category =  Request::segment(8) ? Shelf::where('id', $id)->first()->category :  'Abarrotes';
        //dd($category);
        $shelf_category = [
            'Abarrotes' => 'Abarrotes',
            'Aseo' => 'Aseo',
            'Bazar' => 'Bazar',
            'Bebidas' => 'Bebidas',
            'Bebidas calientes y cereales' => 'Bebidas calientes y cereales',
            'Lácteos' => 'Lácteos',
            'Natural' => 'Natural',
            'Panadería' => 'Panadería',
            'Refrigerados y conservas' => 'Refrigerados y conservas',
            'Lácteos y refrigerados' => 'Lácteos y refrigerados',
            'Cuidado Personal y bebés' => 'Cuidado Personal y bebés',
            'Embutidos y Enlatados' => 'Embutidos y Enlatados',
            'Panadería y Congelados' => 'Panadería y Congelados',
            'Bebidas y Bazar' => 'Bebidas y Bazar',
            'Lácteos y derivados' => 'Lácteos y derivados',
            'Naturales' => 'Naturales'

        ];

        if (!$id && Session::has('post'))
            $shelf = (object) Session::get('post');
        else $shelf = Shelf::find($id);

        return View::make('admin.stores.shelf_form')
            ->with('title','Pasillo')
            ->with('sub_title',$sub_title)
            ->with('sub_title_position',$sub_title_position)
            ->with('shelf',$shelf)
            ->with('department_id',$department_id)
            ->with('store_id',$store_id)
            ->with('category',$category)
            ->with('shelf_category', $shelf_category);
    }

    /**
     * Guardar subcategoria
     */
    public function save_shelf()
    {
        $shelf = Shelf::find(Input::get('id'));
        $shelf_slug = Input::get('slug');
        if(!$shelf){
            $shelf = new Shelf;
            if (empty($shelf_slug))
                $shelf->slug = Str::slug(trim(Input::get('name')));
            else $shelf->slug = Str::slug(trim($shelf_slug));
            if (Shelf::where('slug', $shelf->slug)->where('department_id', Input::get('department_id'))->where('store_id', Input::get('store_id'))->first())
                return Redirect::back()->with('error', 'Ya existe un pasillo con el mismo slug en el departamento padre.')->with('post', Input::all());
            $action = 'insert';
        }else{
            if (empty($shelf_slug))
                $shelf->slug = Str::slug(trim(Input::get('name')));
            else{
                if ( $shelf->slug != $shelf_slug ) {
                    $shelf->slug = Str::slug(trim($shelf_slug));
                    if (Shelf::where('slug', $shelf->slug)->where('id', '<>', $shelf->id)->where('department_id', Input::get('department_id'))->where('store_id', Input::get('store_id'))->first())
                        return Redirect::back()->with('error', 'Ya existe un pasillo con el mismo slug en el departamento padre.')->with('post', Input::all());
                }
            }
            $action = 'update';
        }
        $shelf->name = trim(Input::get('name'));
        $shelf->category = Input::get('category');
        $shelf->store_id = Input::get('store_id');
        $shelf->department_id = Input::get('department_id');
        $shelf->description = Input::get('description');
        $shelf->sort_order = Input::get('sort_order');
        $shelf->status = Input::get('status');
        $shelf->storage_shelf = Input::get('storage_shelf');
        $shelf->storage_rack = Input::get('storage_rack');
        $shelf->storage_position = Input::get('storage_position');
        $shelf->storage_height = Input::get('storage_height');

        $cloudfront_url = Config::get('app.aws.cloudfront_url');

        if(Input::hasFile('image_menu_app_url'))
        {
            $image = Input::file('image_menu_app_url');
            $validator = Validator::make(
                array('ima' => $image),
                array('ima' => 'required|mimes:jpeg,bmp,png,gif')
            );
            if ($validator->fails())
                return Redirect::back()->with('error', 'El formato de la imagen no está permitido.');

            if (!empty($shelf->image_menu_app_url)) {
                $path = str_replace($cloudfront_url, uploads_path(), $shelf->image_menu_app_url);
                remove_file($path);
                remove_file_s3($shelf->image_menu_app_url);
            }
            $shelf->image_menu_app_url = upload_image($image, false, 'stores/images/app');
        }

        $url_shelf = route('frontStoreShelf.single_shelf', [
            'city_slug'       => $shelf->department->store->city->slug,
            'store_slug'      => $shelf->department->store->slug,
            'department_slug' => $shelf->department->slug,
            'shelf_slug'      => $shelf->slug,
        ]);

        $url_shelf  = str_replace(Config::get('app.url'), 'https://merqueo.com', $url_shelf);
        $deeplink_shelf_url =  \FirebaseClient::generateDynamicLink([
            'url'           => $url_shelf,
            'type'          => 'shelf',
            'store_id'      => $shelf->store_id,
            'department_id' => $shelf->department_id,
            'shelf_id'      => $shelf->id,
        ]);
        $shelf->deeplink = $deeplink_shelf_url->shortLink;
        $shelf->save();
        $this->admin_log('shelves', $shelf->id, $action);

        if (!Config::get('app.debug'))
            Queue::push('BatchUpdateIndex', array('shelf' => $shelf->id));

        if ('update' == $action) {
            return Redirect::route('adminStores.shelves', ['store_id' => $shelf->store_id, 'department_id' => $shelf->department_id])->with('type', 'success')->with('message', 'Pasillo guardado con éxito. DEBE ACTUALIZAR MANUALMENTE TODOS LOS DEEPLINKS DE ESTE PASILLO.');
        }

        return Redirect::route('adminStores.shelves', ['store_id' => $shelf->store_id, 'department_id' => $shelf->department_id])->with('type', 'success')->with('message', 'Pasillo guardado con éxito.');
    }

    /**
     * Eliminar subcategoria
     */
    public function delete_shelf()
    {
        $id = Request::segment(7);
        Shelf::where('id', $id)->update(array('status' => 0));
        //Product::where('shelf_id',$id)->delete();
        $this->admin_log('shelves', $id, 'delete');

        return Redirect::back()->with('type', 'success')->with('message', 'Pasillo eliminado con éxito.');
    }

    /**
     * Grilla horarios de tienda
     */
    public function slots()
    {
        $store_id = Request::segment(3);
        if(Input::has('id')){
            $slot_id = Input::get('id');
            $slot = Slot::find($slot_id);
            $start_time = $slot->start_time;
            $end_time = $slot->end_time;
        } else {
            $slot_id = 0;
            $start_time = date("Y-m-d H:i:s");
            $end_time = date("Y-m-d H:i:s");
        }
        $slots = Slot::where('store_id', $store_id)
            ->orderBy('start_time')->get();
        return View::make('admin.stores.slots')
            ->with('title', 'Horarios')
            ->with('slots', $slots)
            ->with('store_id', $store_id)
            ->with('slot_id', $slot_id)
            ->with('start_time', $start_time)
            ->with('end_time', $end_time);
    }

    /**
     * Grilla horarios de tienda
     */
    public function update_slots()
    {
        $store_id = Request::segment(3);
        if($store_id){
            $date = date('Y-m-d');
            $end_date = Input::get('end_date');

            Slot::where('store_id',$store_id)->delete();
            while (strtotime($date) <= strtotime($end_date)) {
                $open = $closed = false;
                $date_data = explode('-', $date);
                $festivo = new Holidays;
                $festivo->festivos(date('Y'));

                if ($festivo->esFestivo($date_data[2], $date_data[1])){
                    if (Input::has('holiday_start') && Input::has('holiday_end')){ //holiday
                        $open = $date.' '.Input::get('holiday_start');
                        $closed = $date.' '.Input::get('holiday_end').':00';
                    }
                }else{
                    if (date('w', strtotime($date)) == 0){ //sunday
                        if (Input::has('sunday_start') && Input::has('sunday_end')){
                            $open = $date.' '.Input::get('sunday_start');
                            $closed = $date.' '.Input::get('sunday_end').':00';
                        }
                    }else{ //any other day
                        if (date('w', strtotime($date)) == 1){ //monday
                            if (Input::has('monday_start') && Input::has('monday_end')){
                                $open = $date.' '.Input::get('monday_start');
                                $closed = $date.' '.Input::get('monday_end').':00';
                            }
                        }
                        if (date('w', strtotime($date)) == 2){ //tuesday
                            if (Input::has('tuesday_start') && Input::has('tuesday_end')){
                                $open = $date.' '.Input::get('tuesday_start');
                                $closed = $date.' '.Input::get('tuesday_end').':00';
                            }
                        }
                        if (date('w', strtotime($date)) == 3){ //wednesday
                            if (Input::has('wednesday_start') && Input::has('wednesday_end')){
                                $open = $date.' '.Input::get('wednesday_start');
                                $closed = $date.' '.Input::get('wednesday_end').':00';
                            }
                        }
                        if (date('w', strtotime($date)) == 4){ //thursday
                            if (Input::has('thursday_start') && Input::has('thursday_end')){
                                $open = $date.' '.Input::get('thursday_start');
                                $closed = $date.' '.Input::get('thursday_end').':00';
                            }
                        }
                        if (date('w', strtotime($date)) == 5){ //friday
                            if (Input::has('friday_start') && Input::has('friday_end')){
                                $open = $date.' '.Input::get('friday_start');

                                if(Input::has('friday_end_next_day'))
                                    $date = date ('Y-m-d', strtotime('+1 day', strtotime($date)));
                                $closed = $date.' '.Input::get('friday_end').':00';
                            }
                        }
                        if (date('w', strtotime($date)) == 6){ //saturday
                            if (Input::has('saturday_start') && Input::has('saturday_end')){
                                $open = $date.' '.Input::get('saturday_start');
                                $closed = $date.' '.Input::get('saturday_end').':00';
                            }
                        }
                    }
                }
                if ($open && $closed){
                    $slot =  new Slot();
                    $slot->store_id = $store_id;
                    $slot->day = $date;
                    $slot->start_time = $open;
                    $slot->end_time = $closed;
                    $slot->is_available = 1;
                    $slot->save();
                }
                $date = date ('Y-m-d', strtotime('+1 day', strtotime($date)));
            }

            $type = 'success';
            $message = 'Horario de atención actualizado con éxito.';
        }
        else
        {
            $type = 'error';
            $message = 'Horario de atención no es posible actualizarlo.';
        }

        return Redirect::route('adminStores.slots', ['id' => $store_id])->with('type', $type)->with('message',$message);
    }

    /**
     * Muestra Formulario que actualiza los horarios de una tienda
     */
    public function form_update_slots()
    {
        $store_id = Request::segment(3);
        $slot_id = 0;
        return View::make('admin.stores.slots_form_update')
            ->with('title', 'Actualización de Horarios de Tienda')
            ->with('slot_id', $slot_id)
            ->with('store_id', $store_id);
    }

    /**
     * Actualizar horario de tienda
     */
    public function save_slot($store_id)
    {
        $time_str = Input::get('time');
        $time_str = str_replace(' - ', '|', $time_str);
        $time_str = explode('|', $time_str);
        $start_time = Carbon::createFromFormat('Y-m-d h:i:s A', $time_str[0]);
        $end_time = Carbon::createFromFormat('Y-m-d h:i:s A', $time_str[1]);
        if(Input::has('slot_id') && Input::get('slot_id') != 0){
            $slot = Slot::find(Input::get('slot_id'));
            $message = 'Horario de atención creado con éxito.';
            $action = 'insert';
        } else {
            $slot = new Slot;
            $slot->day = $start_time->toDateString();
            $slot->is_available = 1;
            $slot->store_id = $store_id;
            $message = 'Horario de atención actualizado con éxito.';
            $action = 'update';
        }
        $slot->start_time = $start_time->toDateTimeString();
        $slot->end_time = $end_time->toDateTimeString();
        $slot->save();
        $this->admin_log('slots', $slot->id, $action);

        return Redirect::route('adminStores.slots', ['id' => $store_id])->with('type', 'success')->with('message',$message);
    }

    /**
     * Eliminar horario de tienda
     */
    public function delete_slot()
    {
        $id = Request::segment(6);
        $slot = Slot::find($id);
        $store_id = Request::segment(3);
        $slot->delete();
        $this->admin_log('slots', $slot->id, 'delete');

        return Redirect::route('adminStores.slots', ['id' => $store_id])->with('type', 'success')->with('message', 'Horario de atención eliminado con éxito.');
    }
}
