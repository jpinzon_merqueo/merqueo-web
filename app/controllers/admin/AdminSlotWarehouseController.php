<?php
namespace admin;

use View;
use Input;
use Session;
use Redirect;
use Response;
use Request;
use SlotWarehouse;
use Warehouse;

class AdminSlotWarehouseController extends AdminController
{
    /**
     * Muestra los horarios por ciudad y slots por bodega
     *
     * @return View
     */
	public function index()
	{
        $slotWarehouse = new SlotWarehouse;
	    if(!Request::ajax()){
            $warehouses = Warehouse::select('warehouses.*')
                ->where('warehouses.city_id', Session::get('admin_city_id'))
                ->where('warehouses.status', 1)
                ->get();
            $slotWarehouse = $slotWarehouse->getSlots();

            $data = [
                'title'      => 'Cantidad de productos por franja horaria',
                'cities'     => $this->get_cities(),
                'warehouses' => $warehouses,
                'cityId'    => Session::get('admin_city_id'),
                'slotWarehouse' => $slotWarehouse,
            ];

            return View::make('admin.slot_warehouses.index', $data);
        }else{
            $data = [
                'slotWarehouse' => $slotWarehouse->getSlots(Input::get('warehouse_id'))
            ];

            return Response::json([
                'status'=> true,
                'result' => View::make('admin.slot_warehouses.index', $data)->renderSections()['list_slot'],
            ]);
        }
	}

	/**
	 * Funcion para guardar los datos
	 */
	public function save()
	{
        $warehouseId = Input::get('warehouse_id');
        $times = Input::get('time');

        foreach ($times as $deliveryWindow => $slotWarehouse) {
            foreach ($slotWarehouse as $day => $data) {
                $slot = SlotWarehouse::where('warehouse_id', $warehouseId)->where('day', $day)->where('delivery_window_id', $deliveryWindow)->first();
                if (empty($slot))
                    $slot = new SlotWarehouse;

                $slot->warehouse_id = $warehouseId;
                $slot->day = $day;
                $slot->delivery_window_id = $deliveryWindow;
                $slot->number_products = (empty($data['numberProducts'])? NULL : $data['numberProducts']);

                $slot->save();
                unset($slot);
            }
        }

        $this->admin_log('slot_warehouses', $warehouseId, 'update');

		return Redirect::back()->with('type', 'success')->with('message', 'Se han guardado los horarios.');
	}

    /**
     * Obtener las bodegas según la ciudad
     */
    public function getWarehousesByCityAjax()
    {
        $warehouses = array();
        $message = 'Error al cargar bodegas.';
        $status = 400;
        if (Input::has('city_id')) {
            $warehouses = Warehouse::select('id', 'warehouse')->where('warehouses.city_id', Input::get('city_id'))->where('status', 1)->get()->toArray();
            $message = 'Bodegas obtenidas.';
            $status = 200;
        }
        return Response::json(['result' => ['warehouses' => $warehouses], 'status' => $status, 'message' => $message], $status);
    }

    /**
     * Obtener los slots por zona
     */
    public function get_slots_by_zone_ajax()
    {
        $dates = [];
        $today = [];
        $message = 'Error al cargar slots por zona';
        $status = 400;
        if (Input::has('zone_id')) {

            $slotWarehouse = new SlotWarehouse;
            $slotWarehouse = $slotWarehouse->getSlotWarehouses( null, Input::get('zone_id'));
            $dates = $slotWarehouse['dates'];
            $today = $slotWarehouse['today'];
            $message = 'Slots por zona';
            $status = 200;
        }

        $result = compact('dates', 'today');

        return Response::json(compact('status', 'message', 'result'), $status);
    }
}