<?php

namespace admin;

use AWS;
use AlliedStore;
use Brand;
use Carbon\Carbon;
use CartProduct;
use Category;
use City;
use Config;
use DB ;
use Department;
use Excel;
use Illuminate\Support\Facades\Redirect;
use Input;
use Maker;
use Product;
use ProductGroup;
use ProductImage;
use Provider;
use Queue;
use Request;
use Response;
use Session;
use Shelf;
use ShelvesSuggestedProducts;
use Store;
use StoreProduct;
use StoreProductGroup;
use StoreProductWarehouse;
use Str;
use Subbrand;
use Subcategory;
use Validator;
use View;
use Warehouse;

class AdminProductController extends AdminController
{
    /**
     * Grilla de productos
     */
    public function index()
    {
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('is_hidden', 0)->where('stores.status', 1)->get();

        if (!Request::ajax()) {
            $cities = $this->get_cities();
            return View::make('admin.products.index')->with('stores', $stores)->with('title', 'Productos')->with('cities', $cities);
        } else {
            $search_term = Input::all();
            $search = !empty($search_term['s']) ? $search_term['s'] : '';

            $products = Product::leftJoin('store_products', 'store_products.product_id', '=', 'products.id');

            if (!empty($search_term['city_id'])) {
                $stores = Store::getActiveStores($search_term['city_id']);
                $products = $products->whereHas('storeProduct.store', function ($query) use ($search_term, $search) {
                    $query->where('city_id', '=', $search_term['city_id']);
                    $query->where('products.name', 'like', '%'.$search.'%');
                    $query->orWhere('products.id', 'like', '%'.$search.'%');
                    $query->orWhere('products.reference', 'like', '%'.$search.'%');
                    $query->orWhere('store_products.provider_plu', 'like', '%' . $search . '%');
                });
            } else {
                $products->where(function ($query) use ($search) {
                    $query->where('products.name', 'like', '%'.$search.'%')
                        ->orWhere('products.id', 'like', '%'.$search.'%')
                        ->orWhere('products.reference', 'like', '%'.$search.'%')
                        ->orWhere('store_products.provider_plu', 'like', '%' . $search . '%');
                });
            }

            if (isset($search_term['order_by'])) {
                $products->orderBy($search_term['order_by'], 'desc');
            }
            $products->groupBy('products.id');
            $products->select('products.*');
            $products = $products->paginate(20);

            $data['html'] = View::make('admin.products.index', ['products' => $products, 'stores' => $stores])->renderSections()['content'];
            $data['links_html'] = $products->links()->render();
            $data['products_count'] = $products->count();
            $data['products_total'] = $products->getTotal();

            return Response::json($data);
        }
    }

    /**
     * Editar producto
     * @param int $id
     * @return
     */
    public function edit($id = 0)
    {
        $sub_title = $id ? 'Editar Producto' : 'Nuevo Producto';

        if (!$id && Session::has('post')) {
            $product = (object) Session::get('post');
            $product->image_medium_url = 0;
        } else {
            $product = Product::find($id);
        }
        if ($id != 0) {
            $product->images = ProductImage::where('product_id', $product->id)->get();
        }
        $categories = Category::orderBy('name')->get();
        $makers = Maker::orderBy('name')->get();
        $brands = Brand::orderBy('name')->get();
        $providers = Provider::orderBy('name')->get();
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->orderBy('name')->get();
        $detail_types = [
            \ProductDetail::SPECIFICATION => \ProductDetail::SPECIFICATION
        ];
        $selected_store_id = 0;
        $selected_department_id = 0;
        $selected_shelf_id = 0;
        foreach ($stores as $store) {
            $selected_store_id = $store->id;
            break;
        }
        if ($product && $product->store_id) {
            $selected_store_id = $product->store_id;
        }
        $departments = Department::where('store_id', $selected_store_id)->orderBy('name')->get();
        foreach ($departments as $department) {
            $selected_department_id = $department->id;
            break;
        }
        if ($product && $product->department_id) {
            $selected_department_id = $product->department_id;
        }
        $shelves = Shelf::where('department_id', $selected_department_id)->orderBy('name')->get();
        foreach ($shelves as $shelf) {
            $selected_shelf_id = $shelf->id;
            break;
        }
        $subcategories = null;
        if ($product && !is_null($product->category_id)) {
            $subcategories = Subcategory::where('category_id', $product->category_id)->orderBy('name')->get();
        }
        $subbrands = null;
        if ($product && !is_null($product->brand_id)) {
            $subbrands = Subbrand::where('brand_id', $product->brand_id)->orderBy('name')->get();
        }

        $data = [
            'title' => 'Producto',
            'sub_title' => $sub_title,
            'product' => $product,
            'brands' => $brands,
            'subbrands' => $subbrands,
            'stores' => $stores,
            'departments' => $departments,
            'shelves' => $shelves,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'makers' => $makers,
            'providers' => $providers,
            'selected_store_id' => $selected_store_id,
            'selected_department_id' => $selected_department_id,
            'selected_shelf_id' => $selected_shelf_id,
            'detail_types' => $detail_types
        ];

        return View::make('admin.products.product_form', $data);
    }

    /**
     * Guardar producto
     */
    public function save()
    {
        $id = Input::get('id');

        if ($product = Product::where('reference', Input::get('reference'))->where('id', '<>', $id)->first()) {
            return Redirect::back()->with('error', 'Ya existe un producto con la misma referencia.')->withInput();
        }

        $product = Product::find($id);
        $product_slug = Input::get('slug');
        $default_image = 0;
        $is_new = 0;
        if (!$product) {
            $product = new Product;
            $is_new = 1;
            if (empty($product_slug)) {
                $product->name = trim(str_replace('"', '', Input::get('name')));
                $product->quantity = Input::get('quantity');
                $product->unit = Input::get('unit');
                $product->slug = $product->getSlug();
            } else {
                $product->slug = Str::slug(trim($product_slug));
            }
            if (Product::where('slug', $product->slug)->first()) {
                return Redirect::back()->with('error', 'Ya existe un producto con el mismo slug.')->withInput();
            }
            $default_image = 1;
            $action = 'insert';
        } else {
            if (Input::has('type')) {
                $product->type = (Input::get('type') == '' ? 'Simple' : Input::get('type'));
                if ($product->type == 'Agrupado') {
                    $product_groups = ProductGroup::where('product_id', $product->id)->count();
                    if ($product_groups == 0) {
                        return Redirect::back()->with('error', 'No se han agregado productos.');
                    }
                } elseif ($product->type == 'Simple') {
                    $product_groups = ProductGroup::where('product_id', $product->id)->get();
                    $store_products = StoreProduct::where('product_id', $product->id)->get();
                    foreach ($product_groups as $product_group) {
                        foreach ($store_products as $store_product) {
                            $store_product_groups = StoreProductGroup::where('store_product_id', $store_product->id)->get();
                            foreach ($store_product_groups as $store_product_group) {
                                $store_product_group->delete();
                            }
                        }
                        if ($store_product_group) {
                            $store_product_group->delete();
                        }
                        $product_group->delete();
                    }
                }
                $product->provider_type_product = Input::get('provider_type_product');
                $product->save();

                return Redirect::back()->with('success', 'Se actualizó el tipo del producto.');
            }
            if (empty($product_slug)) {
                $product->name = str_replace('"', '', Input::get('name'));
                $product->quantity = Input::get('quantity');
                $product->unit = Input::get('unit');
                $product->slug = $product->getSlug();
            } else {
                if ($product_slug != $product->slug) {
                    $product->slug = Str::slug(trim($product_slug));
                    if (Product::where('slug', $product->slug)->where('id', '<>', $id)->first()) {
                        return Redirect::back()->with('error', 'Ya existe un producto con el mismo slug.')->withInput();
                    }
                }
            }
            $action = 'update';
        }
        $product->reference = Input::get('reference');
        $product->name = trim(str_replace('"', '', Input::get('name')));
        $product->quantity = Input::get('quantity');
        $product->unit = Input::get('unit');
        $product->presentation = Input::get('presentation');
        $product->subpresentation = Input::get('subpresentation');
        $product->brand_id = Input::get('brand_id');
        $product->subbrand_id = Input::get('subbrand_id');
        $product->maker_id = Input::get('maker_id');
        $product->category_id = Input::get('category_id');
        $product->subcategory_id = Input::get('subcategory_id');
        $product->description = empty(Input::get('description')) ? '' : Input::get('description');
        $product->nutrition_facts = empty(Input::get('nutrition_facts')) ? '' : Input::get('nutrition_facts');
        $product->accounting_account = Input::get('accounting_account');
        $product->accounting_account_type = Input::get('accounting_account_type');
        $product->accounting_line = Input::get('accounting_line');
        $product->accounting_group = Input::get('accounting_group');
        $product->accounting_code = Input::get('accounting_code');
        $product->net_quantity_pum = (Input::get('has_pum')) ? Input::get('net_quantity_pum') : '';
        $product->unit_pum = (Input::get('has_pum')) ? Input::get('unit_pum') : NULL;
        $product->height = Input::get('height');
        $product->width = Input::get('width');
        $product->length = Input::get('length');
        $product->weight = Input::get('weight');
        $product->volume = Input::get('volume');
        $product->has_barcode = Input::get('has_barcode');
        $product->is_perishable = Input::get('is_perishable');
        $product->can_mix = Input::get('can_mix');
        if ($product->type == 'Simple') {
            $product_groups = ProductGroup::where('product_id', $product->id)->get();
            $store_products = StoreProduct::where('product_id', $product->id)->get();
            foreach ($product_groups as $product_group) {
                foreach ($store_products as $store_product) {
                    $store_product_group = StoreProductGroup::where('store_product_id', $store_product->id)->where('store_product_group_id', $product_group->id)->first();
                    $store_product_group->delete();
                }
            }
        }
        $image = Input::file('image');
        $validator = Validator::make(
            array('ima' => $image),
            array('ima' => 'required|mimes:jpeg,bmp,png')
        );
        if (Input::hasFile('image') && $validator->fails()) {
            return Redirect::back()->with('error', 'Imagen invalida.')->withInput();
        } else {
            $cloudfront_url = Config::get('app.aws.cloudfront_url');
            if (Input::hasFile('image')) {
                //eliminar imagen si existe
                if (!empty($id) && !empty($product->image_large_url) && !strstr($product->image_large_url, 'no_disponible')) {
                    $absolute_path_images = Config::get('app.absolute_path_images');
                    if (!empty($product->image_large_url)) {
                        $path = str_replace($cloudfront_url.'images/', '', $product->image_large_url);
                        if (file_exists($absolute_path_images.'/'.$path)) {
                            remove_file($absolute_path_images.'/'.$path);
                            remove_file_s3($product->image_large_url);
                        }
                    }
                    if (!empty($product->image_medium_url)) {
                        $path = str_replace($cloudfront_url.'images/', '', $product->image_medium_url);
                        if (file_exists($absolute_path_images.'/'.$path)) {
                            remove_file($absolute_path_images.'/'.$path);
                            remove_file_s3($product->image_medium_url);
                        }
                    }
                    if (!empty($product->image_app_url)) {
                        $path = str_replace($cloudfront_url.'images/', '', $product->image_app_url);
                        if (file_exists($absolute_path_images.'/'.$path)) {
                            remove_file($absolute_path_images.'/'.$path);
                            remove_file_s3($product->image_app_url);
                        }
                    }
                    if (!empty($product->image_small_url)) {
                        $path = str_replace($cloudfront_url.'images/', '', $product->image_small_url);
                        if (file_exists($absolute_path_images.'/'.$path)) {
                            remove_file($absolute_path_images.'/'.$path);
                            remove_file_s3($product->image_small_url);
                        }
                    }
                }
                $file = Input::file('image');
                $url = upload_image($file, true);
                $product->image_large_url = $url['large'];
                $product->image_medium_url = $url['medium'];
                $product->image_app_url = $url['app'];
                $product->image_small_url = $url['small'];
            } else {
                if ($default_image) {
                    $product->image_large_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->image_medium_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->image_app_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->image_small_url = $cloudfront_url.'/images/no_disponible.jpg';
                }
            }
            $product->save();
        }
        if (Input::hasFile('images') && count(Input::file('images')) > 0) {
            $images = Input::file('images');
            foreach ($images as $index => $value) {
                $item_image = $value;
                $product_image = new ProductImage();
                $saved_images = ProductImage::where('product_id', $id)->get();
                $validator = Validator::make(
                    array('ima' => $item_image),
                    array('ima' => 'required|mimes:jpeg,bmp,png')
                );
                if ($item_image && $validator->fails()) {
                    return Redirect::back()->with('error', 'Imagen invalida.')->withInput();
                } else {
                    $cloudfront_url = Config::get('app.aws.cloudfront_url');
                    if ($item_image && isset($saved_images[$index])) {
                        if ($saved_images[$index]) {
                            //eliminar imagen si existe
                            if (!empty($saved_images[$index]->image_large_url) && !strstr($saved_images[$index]->image_large_url, 'no_disponible')) {
                                $absolute_path_images = Config::get('app.absolute_path_images');
                                if (!empty($saved_images[$index]->image_large_url)) {
                                    $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_large_url);
                                    if (file_exists($absolute_path_images.'/'.$path)) {
                                        remove_file($absolute_path_images.'/'.$path);
                                        remove_file_s3($saved_images[$index]->image_large_url);
                                    }
                                }
                                if (!empty($saved_images[$index]->image_medium_url)) {
                                    $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_medium_url);
                                    if (file_exists($absolute_path_images.'/'.$path)) {
                                        remove_file($absolute_path_images.'/'.$path);
                                        remove_file_s3($saved_images[$index]->image_medium_url);
                                    }
                                }
                                if (!empty($saved_images[$index]->image_app_url)) {
                                    $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_app_url);
                                    if (file_exists($absolute_path_images.'/'.$path)) {
                                        remove_file($absolute_path_images.'/'.$path);
                                        remove_file_s3($saved_images[$index]->image_app_url);
                                    }
                                }
                                if (!empty($saved_images[$index]->image_small_url)) {
                                    $path = str_replace($cloudfront_url.'images/', '', $saved_images[$index]->image_small_url);
                                    if (file_exists($absolute_path_images.'/'.$path)) {
                                        remove_file($absolute_path_images.'/'.$path);
                                        remove_file_s3($saved_images[$index]->image_small_url);
                                    }
                                }
                            }
                        }
                        $file = $item_image;
                        $url = upload_image($file, true);
                        $saved_images[$index]->image_large_url = $url['large'];
                        $saved_images[$index]->image_medium_url = $url['medium'];
                        $saved_images[$index]->image_app_url = $url['app'];
                        $saved_images[$index]->image_small_url = $url['small'];
                        $saved_images[$index]->save();
                    } else {
                        $file = $item_image;
                        $url = upload_image($file, true);
                        $product_image->image_large_url = $url['large'];
                        $product_image->image_medium_url = $url['medium'];
                        $product_image->image_app_url = $url['app'];
                        $product_image->image_small_url = $url['small'];
                        $product_image->product_id = $product->id;
                    }
                    $product_image->save();
                }
            }
        }
        $this->admin_log('products', $product->id, $action);

        if (!Config::get('app.debug')) {
            Queue::push('UpdateIndex', array('productId' => $product->id));
        }

        if ($is_new) {
            return Redirect::route('adminProducts.edit', ['id' => $product->id]);
        }
        return Redirect::back()->with('success', 'Producto guardado con éxito.');
    }

    /**
     * Eliminar imagen del producto
     */
    public function delete_product_image_ajax()
    {
        $message = 'Error al eliminar imagen del producto';
        $status = 400;
        if (Input::has('product_image_id')) {
            $product_image = ProductImage::find(Input::get('product_image_id'));
            if (!empty($product_image->image_large_url)) {
                remove_file_s3($product_image->image_large_url);
            }
            if (!empty($product_image->image_medium_url)) {
                remove_file_s3($product_image->image_medium_url);
            }
            if (!empty($product_image->image_app_url)) {
                remove_file_s3($product_image->image_app_url);
            }
            if (!empty($product_image->image_small_url)) {
                remove_file_s3($product_image->image_small_url);
            }
            $this->admin_log('product_images', $product_image->id, 'delete');
            DB::delete('DELETE FROM product_images WHERE id = '.$product_image->id);
            $message = 'Imagen de producto eliminada con exito';
            $status = 200;
        }
        return Response::json(['result' => ['product_image' => 'Eliminar imagen de producto'], 'status' => $status, 'message' => $message], $status);
    }

    /**
     * Eliminar producto
     */
    public function delete($id)
    {
        return false;
        if ($product = Product::find($id)) {
            //eliminar imagen si existe
            if (!empty($product->image_large_url) && !strstr($product->image_large_url, 'no_disponible')) {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                $absolute_path_images = Config::get('app.absolute_path_images');
                if (!empty($product->image_large_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_large_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_large_url);
                        remove_file_s3($path);
                    }
                }
                if (!empty($product->image_medium_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_medium_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_medium_url);
                        remove_file_s3($path);
                    }
                }
                if (!empty($product->image_app_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_app_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_app_url);
                        remove_file_s3($path);
                    }
                }
                if (!empty($product->image_small_url)) {
                    $path = str_replace($cloudfront_url.'images/', '', $product->image_small_url);
                    if (file_exists($absolute_path_images.'/'.$path)) {
                        remove_file($absolute_path_images.'/'.$path);
                        $path = str_replace($cloudfront_url.'/', '', $product->image_small_url);
                        remove_file_s3($path);
                    }
                }
            }
            CartProduct::where('product_id', $id)->delete();
            Product::where('id', $id)->delete();
            $this->admin_log('products', $product->id, 'delete');

            if (!Config::get('app.debug')) {
                Queue::push('PruneIndex', array('productId' => $product->id));
            }

            return Redirect::route('adminProducts.index')->with('type', 'success')->with('message', 'Producto eliminado con éxito.');
        }
        return Redirect::route('adminProducts.index')->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar  el producto.');
    }

    /**
     * Obtiene las tiendas por ciudad
     */
    public function get_stores_by_city_ajax()
    {
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1);
        if (Input::has('city_id')) {
            $city_id = Input::get('city_id');
            $stores = $stores->where('stores.city_id', $city_id);
        }
        $stores = $stores->get();
        return Response::json($stores);
    }

    /**
     * Obtiene los productos por shelf id en ajax
     */
    public function get_products_ajax()
    {
        $shelf_id = Input::get('shelf_id');
        $products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->where('store_products.shelf_id', $shelf_id)
            ->select('store_products.*', 'products.name', 'products.quantity', 'products.unit', 'store_product_warehouses.status')
            ->orderBy('products.name', 'ASC')
            ->groupBy('store_products.id')
            ->get();
        return Response::json($products, 200);
    }

    /**
     * Obtiene las ciudades por ajax
     */
    public function get_stores_ajax()
    {
        $city_id = Input::get('city_id');
        $stores = Store::where('status', 1)->where('city_id', $city_id)->get();
        return Response::json($stores, 200);
    }

    /**
     * Obtener categorias por ajax
     */
    public function get_departments_ajax()
    {
        $store_id = Input::get('store_id');
        $departments = Department::where('store_id', $store_id)->orderBy('departments.name', 'ASC')->get();
        $first_department = 0;
        foreach ($departments as $department) {
            $first_department = $department->id;
            break;
        }
        $departments_data = array();
        foreach ($departments as $department) {
            $data = array();
            $data['id'] = $department->id;
            $data['name'] = $department->name;
            $data['status'] = $department->status;
            $data['shelves'] = Shelf::where('department_id', $department->id)->orderBy('name', 'ASC')->get()->toArray();
            array_push($departments_data, $data);
        }
        $response_code = 200;
        $response = Response::json($departments_data, $response_code);
        return $response;
    }

    /**
     * Obtener subcategorias por ajax
     */
    public function get_shelves_ajax()
    {
        $department_id = Input::get('department_id');
        $shelves = Shelf::where('department_id', $department_id)->orderBy('name', 'ASC')->get()->toArray();
        return Response::json($shelves, 200);
    }

    /**
     * Importa productos a una tienda
     */
    public function import()
    {
        if (Input::hasFile('file')) {
            ini_set('memory_limit', '2024M');
            set_time_limit(0);
            ini_set("max_execution_time", -1);

            $extensions_img = array('jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG');
            $file = Input::file('file');
            if (!file_exists($file->getRealPath())) {
                return Redirect::back()->with('error', 'No file uploaded');
            }

            if (!$store = Store::find(Input::get('store_id'))) {
                return Redirect::back()->with('error', 'Invalid Store ID');
            }

            if (!$warehouse = Warehouse::find(Input::get('warehouse_id'))) {
                return Redirect::back()->with('error', 'Invalid Warehouse ID');
            }

            $path_img = Config::get('app.absolute_path_images').'/'.$store->slug.'/'.Input::get('path_images');
            //cargar imagenes por extension minuscula y mayuscula
            foreach ($extensions_img as $extension) {
                foreach (glob($path_img.'/*.'.$extension) as $file_tmp) {
                    $file_tmp = pathinfo($file_tmp);
                    $files_by_extension[$extension][] = $file_tmp['filename'];
                }
            }

            $absolute_path_images = Config::get('app.absolute_path_images');
            $amazon['bucket_name'] = Config::get('app.aws.bucket_name');
            $amazon['cloudfront_url'] = Config::get('app.aws.cloudfront_url');
            $amazon['distribution_id'] = Config::get('app.aws.distribution_id');
            $amazon['s3'] = AWS::get('s3');
            $amazon['client'] = AWS::get('CloudFront');


            if (Input::get('process') != 'update_products') {
                if (empty($files_by_extension)) {
                    return Redirect::back()->with('error', 'No se encontraron imágenes con las extensiones permitidas.');
                }
            }

            if (Input::get('process') == 'count_images') {
                if (!is_dir($path_img)) {
                    return Redirect::back()->with('error', 'La ruta dada no existe en el servidor.');
                }
                $result = $this->import_count_images($errors, $extensions_img, $files_by_extension, $path_img, $file, $store, $absolute_path_images, $amazon);
            }
            if (Input::get('process') == 'import_products') {
                if (!is_dir($path_img)) {
                    return Redirect::back()->with('error', 'La ruta dada no existe en el servidor.');
                }
                $result = $this->import_products($extensions_img, $files_by_extension, $path_img, $file, $store, $absolute_path_images, $amazon, $warehouse);
            }
            if (Input::get('process') == 'update_products') {
                $result = $this->update_products($extensions_img, false, $path_img, $file, $store, $absolute_path_images, $amazon, $warehouse);
                return Redirect::back()->with('respond', $result);
            }
            if (isset($result['error'])) {
                return Redirect::back()->with('respond', $result);
            } else {
                return Redirect::back()->with('respond', $result);
            }
        } else {
            $url_sample_file = web_url().'/'.Config::get('app.download_directory').'sample_empty_file.xlsx';
            $stores = Store::select('stores.*', 'cities.city', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1)->where('cities.id', Session::get('admin_city_id'))->orderBy('stores.name')->get();

            $data['title'] = 'Catálogo';
            $data['sub_title'] = 'Importar Productos';
            $data['cities'] = $this->get_cities();
            $data['stores'] = $stores;
            $data['warehouses'] = $this->get_warehouses(Session::get('admin_city_id'));
            $data['url_sample_file'] = $url_sample_file;
            return View::make('admin.products.import', $data);
        }
    }

    private function import_count_images($errors, $extensions_img, $files_by_extension, $path_img, $file, $store, $absolute_path_images, $amazon)
    {
        // Borro archivo de imagenes no encontradas de procesos anteriores.
        $path_not_found = public_path(Config::get('app.download_directory')).'images_not_found.txt';
        remove_file($path_not_found);
        $not_found = fopen($path_not_found, 'a');
        $images = array('found' => 0, 'not_found' => 0);

        $cont = 0;
        $row_number = 1;
        $result = Excel::selectSheetsByIndex(0)->load($file->getRealPath(), function($reader){
        })->get();

        foreach ($result as $key => $row) {
            $data = explode(';', $row);
            if (count($data) == 65){
                $error = '';

                $found = false;
                foreach ($extensions_img as $extension){
                    if (file_exists($path_img.'/'.trim($data[1  ]).'.'.$extension)){
                        if (isset($files_by_extension[$extension]) && in_array(trim($data[1  ]), $files_by_extension[$extension])){
                            $images['found']++;
                            $found = true;
                            continue;
                        }
                    }
                }
                if (!$found) {
                    $images['not_found']++;
                    fwrite($not_found, trim($data[1  ])."\r\n");
                }
            } else return ['error' => str_replace(['$1', '$2'], [count($data), $row_number], $errors['incomplete_file'])];
        }

        $text = fopen($file->getRealPath(), 'r');
        fgets($text, 4096);
        while (!feof($text)) {
            $refresh_cache = false;
            $row_number++;

            $row = trim(fgets($text, 4096));
            if ($row != '') {
                $data = explode(';', $row);
                if (count($data) == 65) {
                    $error = '';

                    $found = false;
                    foreach ($extensions_img as $extension) {
                        if (file_exists($path_img.'/'.trim($data[1  ]).'.'.$extension)) {
                            if (isset($files_by_extension[$extension]) && in_array(trim($data[1  ]), $files_by_extension[$extension])) {
                                $images['found']++;
                                $found = true;
                                continue;
                            }
                        }
                    }
                    if (!$found) {
                        $images['not_found']++;
                        fwrite($not_found, trim($data[1  ])."\r\n");
                    }
                } else {
                    return ['error' => str_replace(['$1', '$2'], [count($data), $row_number], $errors['incomplete_file'])];
                }
            }
        }
        fclose($text);
        fclose($not_found);
        if ($images['not_found']) {
            $link = $images['not_found'].'&nbsp; <br> <a href="'.web_url().'/'.Config::get('app.download_directory').'images_not_found.txt" target="_blank">Descargar archivo</a>';
        } else {
            $link = $images['not_found'];
        }

        if ($images['not_found'] > 0) {
            return ['error' => '<br><strong>Imágenes encontradas:</strong> '.$images['found'].'<br><strong>Imágenes no encontradas:</strong> '.$link];
        }
        return ['success' => '<br><strong>Imágenes encontradas:</strong> '.$images['found']];
    }

    private function import_products($extensions_img, $files_by_extension, $path_img, $file, $store, $absolute_path_images, $amazon, $warehouse)
    {
        $image_errors = [];
        $errors = [];

        $results = Excel::load($file->getRealPath(), function ($reader) {
        })->get();

        if ($results->count()) {
            try {
                DB::beginTransaction();
                $errors = [];
                foreach ($results as $key => $result) {
                    $refresh_cache = false;
                    if ($result->count() != 70) {
                        $row_error = $key + 1;
                        array_push($errors, [0 => "El producto de la fila: $row_error no tiene los 70 campos requeridos."]);
                        continue;
                    }
                    $row = $result->toArray();

                    //Validar categoría
                    if (!$category = Category::find($row['id_categoria'])) {
                        if (!empty($row['nombre_categoria']) && !$category = Category::where('name', trim($row['nombre_categoria']))->first()) {
                            $category = new Category;
                            $category->name = trim($row['nombre_categoria']);
                            $category->save();
                        }
                    }
                    $row['id_categoria'] = !empty($category) ? $category->id : null;

                    //Validar subcategoría
                    if (isset($category) && !$subcategory = Subcategory::where('category_id', $category->id)->where('id', $row['id_subcategoria'])->first()) {
                        if (!empty($row['nombre_subcategoria']) && !$subcategory = Subcategory::where('name', trim($row['nombre_subcategoria']))->where('category_id', $category->id)->first()) {
                            $subcategory = new Subcategory;
                            $subcategory->name = trim($row['nombre_subcategoria']);
                            $subcategory->category_id = $category->id;
                            $subcategory->save();
                            $row['id_subcategoria'] = $subcategory->id;
                        }
                    }
                    $row['id_subcategoria'] = !empty($subcategory) ? $subcategory->id : null;

                    //validar marca
                    if (!empty($row['marca']) && !$brand = Brand::where('name', trim($row['marca']))->first()) {
                        $brand = new Brand;
                        $brand->name = trim($row['marca']);
                        $brand->save();
                    }
                    $row['marca'] = !empty($brand) ? $brand->id : null;

                    //validar submarca
                    if (isset($brand) && !empty($row['submarca']) && !$subbrand = Subbrand::where('brand_id', intval($brand->id))->where('name', trim($row['submarca']))->first()) {
                        $subbrand = new Subbrand;
                        $subbrand->brand_id = $brand->id;
                        $subbrand->name = trim($row['submarca']);
                        $subbrand->save();
                    }
                    $row['submarca'] = !empty($subbrand) ? $subbrand->id : null;

                    //validar fabricante
                    if (!empty($row['fabricante']) && !$maker = Maker::where('name', trim($row['fabricante']))->first()) {
                        $maker = new Maker;
                        $maker->name = trim($row['fabricante']);
                        $maker->save();
                    }
                    $row['fabricante'] = !empty($maker) ? $maker->id : null;

                    // Validar departamento
                    if (!$department = Department::where('id', intval($row['id_departamento']))->where('store_id', $store->id)->first()) {
                        if (!$department = Department::where('name', trim($row['departamento']))->first()) {
                            $department = new Department;
                            // $department->id = intval($data[0]);
                            $department->name = trim($row['departamento']);
                            $department->slug = Str::slug(trim($row['departamento']));
                            $department->store_id = $store->id;
                            $department->save();
                        }
                    }
                    $row['id_departamento'] = !empty($department) ? $department->id : null;

                    //validar shelve
                    if (isset($department) && !$shelf = Shelf::where('id', intval($row['id_pasillo']))->where('department_id', $department->id)->first()) {
                        if (!empty($row['pasillo']) && !$shelf = Shelf::where('name', trim($row['pasillo']))->first()) {
                            $shelf = new Shelf;
                            $shelf->name = trim($row['pasillo']);
                            $shelf->slug = Str::slug(trim($row['pasillo']));
                            $shelf->store_id = $store->id;
                            $shelf->department_id = $department->id;
                            $shelf->save();
                        }
                    }
                    $row['id_pasillo'] = !empty($shelf) ? $shelf->id : null;

                    $slug = Product::createSlug($row['referencia'], $row['cantidad'], $row['unidad']);
                    $row['slug'] = $slug;
                    $row['cost_average'] = 0;
                    $row['maneja_fecha_de_vencimiento'] = !is_null($row['maneja_fecha_de_vencimiento']) ? intval($row['maneja_fecha_de_vencimiento']) : null;
                    $row['es_mejor_precio_garantizado'] = !is_null($row['es_mejor_precio_garantizado']) ? intval($row['es_mejor_precio_garantizado']) : null;
                    $row['codigo_barras'] = !is_null($row['codigo_barras']) ? intval($row['codigo_barras']) : null;
                    $row['descontinuado'] = !is_null($row['descontinuado']) ? intval($row['descontinuado']) : null;
                    $row['primera_compra_precio_especial'] = !is_null($row['primera_compra_precio_especial']) ? intval($row['primera_compra_precio_especial']) : null;
                    $row['precio_base'] = !is_null($row['precio_base']) ? intval($row['precio_base']) : 0;
                    $row['descuento_en_domicilio'] = !is_null($row['descuento_en_domicilio']) ? intval($row['descuento_en_domicilio']) : 0;
                    $row['maneja_stock_con_precio_especial'] = !is_null($row['maneja_stock_con_precio_especial']) ? intval($row['maneja_stock_con_precio_especial']) : null;
                    $row['manage_stock'] = is_numeric($row['manage_stock']) ? intval($row['manage_stock']) : $row['manage_stock'];
                    $row['status'] = !is_null($row['status']) ? intval($row['status']) : null;

                    $messages = array(
                        'required' => 'El campo :attribute es obligatorio.',
                        'numeric' => 'El campo :attribute debe ser un campo numérico.',
                        'min' => 'El campo :attribute debe tener una longitud de al menos 5 caracteres.',
                        'unique' => 'El campo :attribute debe ser un valor único dentro del sistema.',
                        'exists' => 'El campo :attribute no existe en el sistema.',
                        'integer' => 'El campo :attribute debe ser un entero.',
                        'boolean' => 'El campo :attribute debe ser un 1 ó 0.',
                        'date' => 'El campo :attribute debe ser una fecha con el formato requerido.',
                    );

                    $validator = Validator::make(
                        $row,
                        [
                            'producto' => 'required|min:5',
                            'cantidad' => 'required|numeric',
                            'unidad' => 'required|min:2',
                            'referencia' => 'required|min:3|unique:products,reference',
                            'slug' => 'required|unique:products,slug',
                            'id_categoria' => 'required|exists:categories,id|integer',
                            'marca' => 'required|numeric',
                            'codigo_barras' => 'required|boolean',
                            'id_departamento' => 'required|exists:departments,id|integer',
                            'id_pasillo' => 'required|exists:shelves,id|integer',
                            'posicion_en_pagina_web' => 'required|integer',
                            'maneja_fecha_de_vencimiento' => 'required|boolean',
                            'es_mejor_precio_garantizado' => 'required|boolean',
                            'descontinuado' => 'required|boolean',
                            'tipo_embalaje_de_proveedor' => 'in:Caja,Estiba,Unidad',
                            'costo_iva' => 'required|numeric',
                            'cost_average' => 'required|numeric',
                            'precio' => 'required|integer',
                            'precio_base' => 'required|integer',
                            'precio_especial' => 'integer',
                            'fecha_expiracion_precio_especial' => 'date',
                            'cantidad_precio_especial' => 'integer',
                            'primera_compra_precio_especial' => 'boolean',
                            'descuento_en_domicilio' => 'integer',
                            'maneja_stock_con_precio_especial' => 'boolean',
                            'stock_con_precio_especial' => 'integer',
                            'almacenamiento' => 'in:Seco,Refrigerado,Congelado',
                            'dias_vida_util' => 'integer',
                            'manage_stock' => 'required|boolean',
                            'status' => 'required|boolean',
                            'producto_visible' => 'required|numeric|digits_between:0,1',
                            'minimum_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'ideal_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'stock_minimo_en_alistamiento' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'stock_maximo_en_alistamiento' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'posicion_en_bodega' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'posicion_en_bodega_altura' => 'required_if:manage_stock,1|string|max:5',
                        ],
                        $messages
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        $errors[$key + 1] = $messages->all();
                        continue;
                    }


                    $slug = Product::createSlug($row['referencia'], $row['cantidad'], $row['unidad']);
                    $cloudfront_url = Config::get('app.aws.cloudfront_url');
                    $product = new Product;
                    $product->category_id = $row['id_categoria'];
                    $product->subcategory_id = $row['id_subcategoria'];
                    $product->brand_id = $row['marca'];
                    $product->subbrand_id = $row['submarca'];
                    $product->maker_id = $row['fabricante'];
                    $product->name = trim($row['producto']);
                    $product->reference = $row['referencia'];
                    $product->slug = $slug;
                    $product->presentation = $row['presentacion'];
                    $product->subpresentation = $row['subpresentacion'];
                    $product->quantity = $row['cantidad'];
                    $product->unit = $row['unidad'];
                    $product->size = $row['dimensiones_tamano'];
                    $product->height = $row['dimensiones_alto'];
                    $product->width = $row['dimensiones_ancho'];
                    $product->length = $row['dimensiones_largo'];
                    $product->has_barcode = $row['codigo_barras'];
                    $product->description = addslashes($row['descripcion']);
                    $product->nutrition_facts = addslashes($row['informacion_nutricional']);
                    $product->accounting_account = $row['cuenta_contable'];
                    $product->accounting_account_type = $row['tipo_cuenta_contable'];
                    $product->accounting_line = $row['linea_contable'];
                    $product->accounting_group = $row['grupo_contable'];
                    $product->accounting_code = $row['codigo_contable'];
                    $product->type = 'Simple';
                    $product->image_large_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->image_medium_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->image_small_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->image_app_url = $cloudfront_url.'/images/no_disponible.jpg';
                    $product->status = 1;
                    $product->save();

                    // Creando store_products
                    if ($product) {
                        $store_product = new StoreProduct;
                        $store_product->store_id = $store->id;
                        $store_product->product_id = $product->id;
                        $store_product->department_id = $row['id_departamento'];
                        $store_product->shelf_id = $row['id_pasillo'];
                        $store_product->provider_id = $row['id_proveedor'];
                        $store_product->allied_store_id = empty($row['id_tienda_aliada']) ? null : $row['id_tienda_aliada'];
                        $store_product->public_price = $row['precio_mostrar_pagina'];
                        $store_product->price = $row['precio'];
                        $store_product->special_price = $row['precio_especial'];
                        $store_product->quantity_special_price = $row['cantidad_precio_especial'];
                        $store_product->has_quantity_special_price_stock = $row['maneja_stock_con_precio_especial'];
                        $store_product->quantity_special_price_stock = $row['stock_con_precio_especial'];
                        $store_product->first_order_special_price = $row['primera_compra_precio_especial'];
                        $store_product->delivery_discount_amount = $row['descuento_en_domicilio'];
                        $store_product->special_price_expiration_date = empty($row['fecha_expiracion_precio_especial']) ? null : $row['fecha_expiracion_precio_especial'];
                        $store_product->provider_discount = $row['precio_descuento_proveedor'];
                        $store_product->merqueo_discount = $row['precio_descuento_merqueo'];
                        $store_product->seller_discount = $row['precio_descuento_vendedor'];
                        $store_product->provider_plu = $row['plu_proveedor'];
                        $store_product->provider_pack_type = $row['tipo_embalaje_de_proveedor'];
                        $store_product->provider_pack_quantity = $row['cantidad_embalaje'];
                        $store_product->provider_pack_quantity_approach = $row['aproximacion_a_embalaje'];
                        $store_product->storage = $row['almacenamiento'];
                        $store_product->useful_life_days = $row['dias_vida_util'];
                        $store_product->iva = $row['iva'];
                        $store_product->consumption_tax = $row['impuesto_al_consumo'];
                        $store_product->base_price = $row['precio_base'];
                        $store_product->base_cost = $row['costo_base'];
                        $store_product->cost = $row['costo_iva'];
                        $store_product->cost_average = 0;
                        $store_product->negociated_price = $row['precio_negociado'];
                        $store_product->is_best_price = $row['es_mejor_precio_garantizado'];
                        $store_product->sort_order = $row['posicion_en_pagina_web'];
                        $store_product->handle_expiration_date = $row['maneja_fecha_de_vencimiento'];
                        $store_product->save();

                        if ($store_product) {
                            $store_product_warehous = new StoreProductWarehouse;
                            $store_product_warehous->warehouse_id = $warehouse->id;
                            $store_product_warehous->store_product_id = $store_product->id;
                            $store_product_warehous->storage_position = $row['posicion_en_bodega'];
                            $store_product_warehous->storage_height_position = $row['posicion_en_bodega_altura'];
                            $store_product_warehous->minimum_picking_stock = $row['stock_minimo_en_alistamiento'];
                            $store_product_warehous->maximum_picking_stock = $row['stock_maximo_en_alistamiento'];
                            $store_product_warehous->manage_stock = $row['manage_stock'];
                            $store_product_warehous->is_visible_stock = $row['is_visible_stock'];
                            $store_product_warehous->ideal_stock = $row['ideal_stock'];
                            $store_product_warehous->minimum_stock = $row['minimum_stock'];
                            $store_product_warehous->reception_stock = 0;
                            $store_product_warehous->picking_stock = 0;
                            $store_product_warehous->picked_stock = 0;
                            $store_product_warehous->current_stock = 0;
                            $store_product_warehous->return_stock = 0;
                            $store_product_warehous->shrinkage_stock = 0;
                            $store_product_warehous->last_updated_stock_date = null;
                            $store_product_warehous->status = $row['status'];
                            $store_product_warehous->discontinued = $row['descontinuado'];
                            $store_product_warehous->is_visible = $row['producto_visible'];
                            $store_product_warehous->save();
                        }
                    }
                    $images_try = 0;
                    foreach ($extensions_img as $extension) {
                        if (file_exists($path_img.'/'.$product->reference.'.'.$extension)) {
                            if (in_array($product->reference, $files_by_extension[$extension])) {
                                //eliminar imagen si existe
                                if (!empty($product->id)
                                    && !empty($product->image_large_url)
                                    && !strstr($product->image_large_url, 'no_disponible')) {
                                    $refresh_cache = true;
                                    if (!empty($product->image_large_url)) {
                                        $path = str_replace($amazon['cloudfront_url'].'/images/', '', $product->image_large_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            $amazon['s3']->deleteObject(array(
                                                'Bucket' => $amazon['bucket_name'],
                                                'Key' => $path,
                                            ));
                                        }
                                    }
                                    if (!empty($product->image_medium_url)) {
                                        $path = str_replace($amazon['cloudfront_url'].'/images/', '', $product->image_medium_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            $amazon['s3']->deleteObject(array(
                                                'Bucket' => $amazon['bucket_name'],
                                                'Key' => $path,
                                            ));
                                        }
                                    }
                                    if (!empty($product->image_app_url)) {
                                        $path = str_replace($amazon['cloudfront_url'].'/images/', '', $product->image_app_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            $amazon['s3']->deleteObject(array(
                                                'Bucket' => $amazon['bucket_name'],
                                                'Key' => $path,
                                            ));
                                        }
                                    }
                                    if (!empty($product->image_small_url)) {
                                        $path = str_replace($amazon['cloudfront_url'].'/images/', '', $product->image_small_url);
                                        if (file_exists($absolute_path_images.'/'.$path)) {
                                            remove_file($absolute_path_images.'/'.$path);
                                            $amazon['s3']->deleteObject(array(
                                                'Bucket' => $amazon['bucket_name'],
                                                'Key' => '/images/'.$path,
                                            ));
                                        }
                                    }
                                }

                                $file_info = pathinfo($path_img.'/'.$product->reference.'.'.$extension);
                                $file_uploaded['real_path'] = $path_img.'/'.$product->reference.'.'.$extension;
                                $file_uploaded['client_original_name'] = $file_info['filename'].'.'.$file_info['extension'];
                                $file_uploaded['client_original_extension'] = $file_info['extension'];
                                $url = upload_image($file_uploaded, true);
                                if ($url) {
                                    $product->image_large_url = $url['large'];
                                    $product->image_medium_url = $url['medium'];
                                    $product->image_app_url = $url['app'];
                                    $product->image_small_url = $url['small'];
                                    $product->save();
                                    $cont++;

                                    //actualizar cache de imagen en AWS
                                    if ($refresh_cache && !Config::get('app.debug')) {
                                        $store = Store::select('stores.*', 'cities.slug AS city_slug')
                                            ->join('cities', 'cities.id', '=', 'stores.city_id')
                                            ->where('stores.id', $product->store_id)->first();
                                        $path_image = '/images/'.$store->city_slug.'/'.$store->slug.'/products';

                                        $result = $amazon['client']->createInvalidation(array(
                                            'DistributionId' => $amazon['distribution_id'],
                                            'Paths' => array(
                                                'Quantity' => 4,
                                                'Items' => array(
                                                    $path_image.'/large/'.$file_uploaded['client_original_name'],
                                                    $path_image.'/medium/'.$file_uploaded['client_original_name'],
                                                    $path_image.'/small/'.$file_uploaded['client_original_name'],
                                                    $path_image.'/app/'.$file_uploaded['client_original_name'],
                                                ),
                                            ),
                                            'CallerReference' => $path_image.'/large/'.$file_uploaded['client_original_name'],
                                        ));
                                    }

                                    break 1;
                                } else {
                                    DB::rollBack();
                                    return ['error' => str_replace(['$1', '$2'], [$product->reference, $extension], $errors['error_upload_reference'])];
                                }
                            }
                        } else {
                            $images_try++;
                        }
                    }
                    if ($images_try == 6) {
                        $errors[$key + 1] = array_push(
                            $errors[$key+1],
                            "la imagen del producto con referencia $product->reference no existe."
                        );
                    }
                    $success[$key+1] = "El producto con referencia: $product->reference, se ha creado exitosamente.";
                }
                if (count($errors)) {
                    DB::rollBack();
                    return ['error' => $errors];
                }
                DB::commit();
                return ['success' => $success];
            } catch (Exception $e) {
                DB::rollBack();
                $errors[0][] = "Se presentó el siguiente error: $e->getMessage() [$e->getFile() - $e->getLine()]";
                return ['error' => $errors];
            }
        }
    }

    private function update_products($extensions_img, $files_by_extension, $path_img, $file, $store, $absolute_path_images, $amazon, $warehouse)
    {
        $image_errors = [];
        $errors = [];

        $results = Excel::load($file->getRealPath(), function ($reader) {
        })->get();

        if ($results->count()) {
            try {
                DB::beginTransaction();
                $errors = [];
                $success = [];
                foreach ($results as $key => $result) {
                    if (empty($result->product_id)){
                        continue;
                    }

                    $row = $result->toArray();

                    //Validar categoría
                    if (!empty($row['id_categoria'])) {
                        if (!$category = Category::find($row['id_categoria'])) {
                            if (!empty($row['nombre_categoria']) && !$category = Category::where('name', trim($row['nombre_categoria']))->first()) {
                                $category = new Category;
                                $category->name = trim($row['nombre_categoria']);
                                $category->save();
                            }
                        }
                        $row['id_categoria'] = !empty($category) ? $category->id : null;
                    }

                    //Validar subcategoría
                    if (!empty($row['id_subcategoria'])) {
                        if (isset($category) && !$subcategory = Subcategory::where('category_id', $category->id)->where('id', $row['id_subcategoria'])->first()) {
                            if (!empty($row['nombre_subcategoria']) && !$subcategory = Subcategory::where('name', trim($row['nombre_subcategoria']))->where('category_id', $category->id)->first()) {
                                $subcategory = new Subcategory;
                                $subcategory->name = trim($row['nombre_subcategoria']);
                                $subcategory->category_id = $category->id;
                                $subcategory->save();
                                $row['id_subcategoria'] = $subcategory->id;
                            }
                        }
                        $row['id_subcategoria'] = !empty($subcategory) ? $subcategory->id : null;
                    }

                    //validar marca
                    if (!empty($row['marca'])) {
                        if (!empty($row['marca']) && !$brand = Brand::where('name', trim($row['marca']))->first()) {
                            $brand = new Brand;
                            $brand->name = trim($row['marca']);
                            $brand->save();
                        }
                        $row['marca'] = !empty($brand) ? $brand->id : null;
                    }

                    //validar submarca
                    if (!empty($row['submarca'])) {
                        if (isset($brand) && !empty($row['submarca']) && !$subbrand = Subbrand::where('brand_id', intval($brand->id))->where('name', trim($row['submarca']))->first()) {
                            $subbrand = new Subbrand;
                            $subbrand->brand_id = $brand->id;
                            $subbrand->name = trim($row['submarca']);
                            $subbrand->save();
                        }
                        $row['submarca'] = !empty($subbrand) ? $subbrand->id : null;
                    }

                    //validar fabricante
                    if (!empty($row['fabricante'])) {
                        if (!empty($row['fabricante']) && !$maker = Maker::where('name', trim($row['fabricante']))->first()) {
                            $maker = new Maker;
                            $maker->name = trim($row['fabricante']);
                            $maker->save();
                        }
                        $row['fabricante'] = !empty($maker) ? $maker->id : null;
                    }

                    // Validar departamento
                    if (!empty($row['id_departamento'])) {
                        if (!$department = Department::where('id', intval($row['id_departamento']))->where('store_id', $store->id)->first()) {
                            if (!$department = Department::where('name', trim($row['departamento']))->first()) {
                                $department = new Department;
                                // $department->id = intval($data[0]);
                                $department->name = trim($row['departamento']);
                                $department->slug = Str::slug(trim($row['departamento']));
                                $department->store_id = $store->id;
                                $department->save();
                            }
                        }
                        $row['id_departamento'] = !empty($department) ? $department->id : null;
                    }

                    //validar shelve
                    if (!empty($row['id_pasillo'])) {
                        if (isset($department) && !$shelf = Shelf::where('id', intval($row['id_pasillo']))->where('department_id', $department->id)->first()) {
                            if (!empty($row['pasillo']) && !$shelf = Shelf::where('name', trim($row['pasillo']))->first()) {
                                $shelf = new Shelf;
                                $shelf->name = trim($row['pasillo']);
                                $shelf->slug = Str::slug(trim($row['pasillo']));
                                $shelf->store_id = $store->id;
                                $shelf->department_id = $department->id;
                                $shelf->save();
                            }
                        }
                        $row['id_pasillo'] = !empty($shelf) ? $shelf->id : null;
                    }
                    if (!empty($row['referencia']) && !empty($row['cantidad']) && !empty($row['unidad'])) {
                        $slug = Product::createSlug($row['referencia'], $row['cantidad'], $row['unidad']);
                        $row['slug'] = $slug;
                    }
                    if (isset($row['maneja_fecha_de_vencimiento'])) {
                        $row['maneja_fecha_de_vencimiento'] = !is_null($row['maneja_fecha_de_vencimiento']) ? intval($row['maneja_fecha_de_vencimiento']) : null;
                    }
                    if (isset($row['es_mejor_precio_garantizado'])) {
                        $row['es_mejor_precio_garantizado'] = !is_null($row['es_mejor_precio_garantizado']) ? intval($row['es_mejor_precio_garantizado']) : null;
                    }
                    if (isset($row['codigo_barras'])) {
                        $row['codigo_barras'] = !is_null($row['codigo_barras']) ? intval($row['codigo_barras']) : null;
                    }
                    if (!empty($row['es_perecedero'])) {
                        $row['es_perecedero'] = !is_null($row['es_perecedero']) ? intval($row['es_perecedero']) : null;
                    }
                    if (isset($row['descontinuado'])) {
                        $row['descontinuado'] = !is_null($row['descontinuado']) ? intval($row['descontinuado']) : null;
                    }
                    if (isset($row['primera_compra_precio_especial'])) {
                        $row['primera_compra_precio_especial'] = !is_null($row['primera_compra_precio_especial']) ? intval($row['primera_compra_precio_especial']) : null;
                    }
                    if (isset($row['precio'])) {
                        $row['precio'] = !is_null($row['precio']) ? intval($row['precio']) : 0;
                    }
                    if (isset($row['precio_base'])) {
                        $row['precio_base'] = !is_null($row['precio_base']) ? intval($row['precio_base']) : 0;
                    }
                    if (isset($row['precio_especial'])) {
                        $row['precio_especial'] = !is_null($row['precio_especial']) ? intval($row['precio_especial']) : 0;
                    }
                    if (isset($row['precio_descuento_merqueo'])) {
                        $row['precio_descuento_merqueo'] = !is_null($row['precio_descuento_merqueo']) ? intval($row['precio_descuento_merqueo']) : 0;
                    }
                    if (isset($row['precio_descuento_proveedor'])) {
                        $row['precio_descuento_proveedor'] = !is_null($row['precio_descuento_proveedor']) ? intval($row['precio_descuento_proveedor']) : 0;
                    }
                    if (isset($row['precio_descuento_vendedor'])) {
                        $row['precio_descuento_vendedor'] = !is_null($row['precio_descuento_vendedor']) ? intval($row['precio_descuento_vendedor']) : 0;
                    }
                    if (isset($row['descuento_en_domicilio'])) {
                        $row['descuento_en_domicilio'] = !is_null($row['descuento_en_domicilio']) ? intval($row['descuento_en_domicilio']) : 0;
                    }
                    if (isset($row['maneja_stock_con_precio_especial'])) {
                        $row['maneja_stock_con_precio_especial'] = !is_null($row['maneja_stock_con_precio_especial']) ? intval($row['maneja_stock_con_precio_especial']) : null;
                    }
                    if (isset($row['manage_stock'])) {
                        $row['manage_stock'] = is_numeric($row['manage_stock']) ? intval($row['manage_stock']) : $row['manage_stock'];
                    }
                    if (isset($row['status'])) {
                        $row['status'] = !is_null($row['status']) ? intval($row['status']) : null;
                    }

                    $messages = array(
                        'required' => 'El campo :attribute es obligatorio.',
                        'numeric' => 'El campo :attribute debe ser un campo numérico.',
                        'min' => 'El campo :attribute debe tener una longitud de al menos 5 caracteres.',
                        'unique' => 'El campo :attribute debe ser un valor único dentro del sistema.',
                        'exists' => 'El campo :attribute no existe en el sistema.',
                        'integer' => 'El campo :attribute debe ser un entero.',
                        'boolean' => 'El campo :attribute debe ser un 1 ó 0.',
                        'date' => 'El campo :attribute debe ser una fecha con el formato requerido.',
                        'between' => 'El campo :attribute debe ser mayor o igual a 0'
                    );

                    $validator = Validator::make(
                        $row,
                        [
                            'product_id' => 'required|integer|exists:products,id',
                            'producto' => 'min:3',
                            'referencia' => 'min:3',
                            'id_categoria' => 'exists:categories,id|integer',
                            'marca' => 'numeric',
                            'cantidad' => 'numeric',
                            'unidad' => 'min:2',
                            'codigo_barras' => 'boolean',
                            'id_departamento' => 'exists:departments,id|integer',
                            'id_pasillo' => 'exists:shelves,id|integer',
                            'posicion_en_pagina_web' => 'integer',
                            'maneja_fecha_de_vencimiento' => 'boolean',
                            'es_mejor_precio_garantizado' => 'boolean',
                            'descontinuado' => 'boolean',
                            'tipo_embalaje_de_proveedor' => 'in:Caja,Estiba,Unidad',
                            'costo_iva' => 'numeric',
                            'cost_average' => 'numeric',
                            'precio' => 'numeric|between:0,999999999',
                            'precio_base' => 'numeric|between:0,999999999',
                            'precio_especial' => 'numeric|between:0,999999999',
                            'precio_descuento_merqueo' => 'numeric|between:0,999999999',
                            'precio_descuento_proveedor' => 'numeric|between:0,999999999',
                            'precio_descuento_vendedor' => 'numeric|between:0,999999999',
                            'fecha_inicio_precio_especial' => 'date',
                            'fecha_expiracion_precio_especial' => 'date',
                            'cantidad_precio_especial' => 'integer',
                            'primera_compra_precio_especial' => 'boolean',
                            'descuento_en_domicilio' => 'integer',
                            'maneja_stock_con_precio_especial' => 'boolean',
                            'stock_con_precio_especial' => 'integer',
                            'almacenamiento' => 'in:Seco,Refrigerado,Congelado',
                            'dias_vida_util' => 'integer',
                            'manage_stock' => 'boolean',
                            'status' => 'boolean',
                            'producto_visible' => 'numeric|digits_between:0,1',
                            'minimum_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'ideal_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'stock_minimo_en_alistamiento' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'stock_maximo_en_alistamiento' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'posicion_en_bodega' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
                            'posicion_en_bodega_altura' => 'required_if:manage_stock,1|string|max:5',
                            'cantidad_neta_pum' => 'numeric',
                            'unidad_pum' => 'in:Unidad,Gramo,Mililitro,Metro',
                        ],
                        $messages
                    );
                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        $errors[$key + 1] = $messages->all();
                        continue;
                    }

                    $product = Product::find($row['product_id']);

                    // NO se pueden modificar combos a través del importador
                    if($product->type == 'Agrupado'){
                        $errors[$key +1] = ["Debido a la distribución de descuentos, no es posible afectar combos con el importador"];
                        continue;
                    }

                    // El precio_especial no puede ser mayor al precio
                    if(isset($row['precio_especial']) && isset($row['precio']) && $row['precio_especial']  > $row['precio']){
                        $errors[$key +1] = ["El precio especial no puede ser mayor al precio del producto"];
                        continue;
                    }

                    // La sumatoria de las distribuciones de los descuentos debe ser igual al 100 %
                    if(isset($row['precio_especial']) &&
                        (
                            !isset($row['precio_descuento_proveedor']) ||
                            !isset($row['precio_descuento_vendedor']) ||
                            !isset($row['precio_descuento_merqueo'])
                        )
                    ){
                        $errors[$key +1] = ["La distribución porcentual del descuento debe ser igual a 100"];
                        continue;

                    }elseif( isset($row['precio_especial']) &&
                        $row['precio_especial']  != 0 && (
                         ($row['precio_descuento_proveedor'] +
                          $row['precio_descuento_vendedor'] +
                          $row['precio_descuento_merqueo']) != 100) ){

                        $errors[$key +1] = ["La distribución porcentual del descuento debe ser igual a 100"];
                        continue;
                    }

                    if (!empty($row['referencia']) && !empty($row['cantidad']) && !empty($row['unidad'])) {
                        $slug = Product::createSlug($row['referencia'], $row['cantidad'], $row['unidad']);
                        $product->slug = $slug;
                    }
                    if (!empty($row['id_categoria'])) {
                        $product->category_id = $row['id_categoria'];
                    }
                    if (!empty($row['id_subcategoria'])) {
                        $product->subcategory_id = $row['id_subcategoria'];
                    }
                    if (!empty($row['marca'])) {
                        $product->brand_id = $row['marca'];
                    }
                    if (!empty($row['submarca'])) {
                        $product->subbrand_id = $row['submarca'];
                    }
                    if (!empty($row['fabricante'])) {
                        $product->maker_id = $row['fabricante'];
                    }
                    if (!empty($row['producto'])) {
                        $product->name = $row['producto'];
                    }
                    if (!empty($row['referencia'])) {
                        $product->reference = $row['referencia'];
                    }
                    if (!empty($row['presentacion'])) {
                        $product->presentation = $row['presentacion'];
                    }
                    if (!empty($row['subpresentacion'])) {
                        $product->subpresentation = $row['subpresentacion'];
                    }
                    if (!empty($row['cantidad'])) {
                        $product->quantity = $row['cantidad'];
                    }
                    if (!empty($row['unidad'])) {
                        $product->unit = $row['unidad'];
                    }
                    if (!empty($row['dimensiones_tamano'])) {
                        $product->size = $row['dimensiones_tamano'];
                    }
                    if (!empty($row['dimensiones_alto'])) {
                        $product->height = $row['dimensiones_alto'];
                    }
                    if (!empty($row['dimensiones_ancho'])) {
                        $product->width = $row['dimensiones_ancho'];
                    }
                    if (!empty($row['dimensiones_largo'])) {
                        $product->length = $row['dimensiones_largo'];
                    }
                    if (isset($row['codigo_barras'])) {
                        $product->has_barcode = $row['codigo_barras'];
                    }
                    if (isset($row['es_perecedero'])) {
                        $product->is_perishable = $row['es_perecedero'];
                    }
                    if (!empty($row['cuenta_contable'])) {
                        $product->accounting_account = $row['cuenta_contable'];
                    }
                    if (!empty($row['tipo_cuenta_contable'])) {
                        $product->accounting_account_type = $row['tipo_cuenta_contable'];
                    }
                    if (!empty($row['linea_contable'])) {
                        $product->accounting_line = $row['linea_contable'];
                    }
                    if (!empty($row['grupo_contable'])) {
                        $product->accounting_group = $row['grupo_contable'];
                    }
                    if (!empty($row['codigo_contable'])) {
                        $product->accounting_code = $row['codigo_contable'];
                    }
                    if (!empty($row['descripcion'])) {
                        $product->description = addslashes($row['descripcion']);
                    }
                    if (!empty($row['informacion_nutricional'])) {
                        $product->nutrition_facts = addslashes($row['informacion_nutricional']);
                    }
                    if (isset($row['cantidad_neta_pum'])) {
                        $product->net_quantity_pum = $row['cantidad_neta_pum'];
                    }
                    if (isset($row['unidad_pum'])) {
                        $product->unit_pum = $row['unidad_pum'];
                    }
                    $product->save();

                    // Creando store_products
                    if ($product) {
                        $store_product = StoreProduct::where('product_id', $product->id)->where('store_id', $store->id)->first();
                        if (!empty($store_product)) {
                            if (!empty($row['id_departamento'])) {
                                $store_product->department_id = $row['id_departamento'];
                            }
                            if (!empty($row['id_pasillo'])) {
                                $store_product->shelf_id = $row['id_pasillo'];
                            }
                            if (!empty($row['id_proveedor'])) {
                                $store_product->provider_id = $row['id_proveedor'];
                            }
                            if (isset($row['precio_mostrar_pagina'])) {
                                $store_product->public_price = $row['precio_mostrar_pagina'];
                            }
                            if (!empty($row['precio'])) {
                                $store_product->price = $row['precio'];
                            }
                            if (isset($row['precio_especial'])) {
                                $store_product->special_price = $row['precio_especial'];
                            }
                            if (isset($row['cantidad_precio_especial'])) {
                                $store_product->quantity_special_price = $row['cantidad_precio_especial'];
                            }
                            if (isset($row['maneja_stock_con_precio_especial'])) {
                                $store_product->has_quantity_special_price_stock = $row['maneja_stock_con_precio_especial'];
                            }
                            if (isset($row['stock_con_precio_especial'])) {
                                $store_product->quantity_special_price_stock = $row['stock_con_precio_especial'];
                            }
                            if (isset($row['primera_compra_precio_especial'])) {
                                $store_product->first_order_special_price = $row['primera_compra_precio_especial'];
                            }
                            if (isset($row['descuento_en_domicilio'])) {
                                $store_product->delivery_discount_amount = $row['descuento_en_domicilio'];
                            }
                            if (isset($row['descuento_total_maximo_domicilio'])) {
                                $store_product->delivery_maximum_amount = $row['descuento_total_maximo_domicilio'];
                            }
                            if (isset($row['fecha_inicio_descuento_domicilio'])) {
                                $store_product->delivery_discount_start_date = $row['fecha_inicio_descuento_domicilio'];
                            }
                            if (isset($row['precio_descuento_proveedor'])) {
                                $store_product->provider_discount = $row['precio_descuento_proveedor'];
                            }
                            if (isset($row['precio_descuento_merqueo'])) {
                                $store_product->merqueo_discount = $row['precio_descuento_merqueo'];
                            }
                            if (isset($row['precio_descuento_vendedor'])) {
                                $store_product->seller_discount = $row['precio_descuento_vendedor'];
                            }
                            if (!empty($row['plu_proveedor'])) {
                                $store_product->provider_plu = $row['plu_proveedor'];
                            }
                            if (!empty($row['tipo_embalaje_de_proveedor'])) {
                                $store_product->provider_pack_type = $row['tipo_embalaje_de_proveedor'];
                            }
                            if (!empty($row['cantidad_embalaje'])) {
                                $store_product->provider_pack_quantity = $row['cantidad_embalaje'];
                            }
                            if (isset($row['aproximacion_a_embalaje'])) {
                                $store_product->provider_pack_quantity_approach = $row['aproximacion_a_embalaje'];
                            }
                            if (!empty($row['almacenamiento'])) {
                                $store_product->storage = $row['almacenamiento'];
                            }
                            if (!empty($row['dias_vida_util'])) {
                                $store_product->useful_life_days = $row['dias_vida_util'];
                            }
                            if (isset($row['iva'])) {
                                $store_product->iva = $row['iva'];
                            }
                            if (isset($row['impuesto_al_consumo'])) {
                                $store_product->consumption_tax = $row['impuesto_al_consumo'];
                            }
                            if (isset($row['precio_base'])) {
                                $store_product->base_price = $row['precio_base'];
                            }
                            if (isset($row['costo_base'])) {
                                $store_product->base_cost = $row['costo_base'];
                            }
                            if (isset($row['costo_iva'])) {
                                $store_product->cost = $row['costo_iva'];
                            }
                            if (isset($row['precio_negociado'])) {
                                $store_product->negociated_price = $row['precio_negociado'];
                            }
                            if (isset($row['es_mejor_precio_garantizado'])) {
                                $store_product->is_best_price = $row['es_mejor_precio_garantizado'];
                            }
                            if (!empty($row['posicion_en_pagina_web'])) {
                                $store_product->sort_order = $row['posicion_en_pagina_web'];
                            }
                            if (isset($row['maneja_fecha_de_vencimiento'])) {
                                $store_product->handle_expiration_date = $row['maneja_fecha_de_vencimiento'];
                            }
                            if (isset($row['id_tienda_aliada'])) {
                                $store_product->allied_store_id = empty($row['id_tienda_aliada']) ? null : $row['id_tienda_aliada'];
                            }
                            if (isset($row['fecha_inicio_precio_especial'])) {
                                $store_product->special_price_starting_date = empty($row['fecha_inicio_precio_especial']) ? null : $row['fecha_inicio_precio_especial'];
                            }
                            if (isset($row['fecha_expiracion_precio_especial'])) {
                                $store_product->special_price_expiration_date = empty($row['fecha_expiracion_precio_especial']) ? null : $row['fecha_expiracion_precio_especial'];
                            }
                            $store_product->base_cost = $store_product->cost / (1 + $store_product->iva / 100);
                            $store_product->save();
                        } else {
                            $row_error = $key+1;
                            array_push($errors, [0 => "El producto de la fila: $row_error no está creado en esa ciudad."]);
                        }

                        if ($store_product) {
                            $store_product_warehouse = StoreProductWarehouse::where('warehouse_id', $warehouse->id)
                                ->where('store_product_id', $store_product->id)->first();
                            if (!empty($store_product_warehouse)) {
                                if (!empty($row['posicion_en_bodega'])) {
                                    $store_product_warehouse->storage_position = $row['posicion_en_bodega'];
                                }
                                if (!empty($row['posicion_en_bodega_altura'])) {
                                    $store_product_warehouse->storage_height_position = $row['posicion_en_bodega_altura'];
                                }
                                if (!empty($row['stock_minimo_en_alistamiento'])) {
                                    $store_product_warehouse->minimum_picking_stock = $row['stock_minimo_en_alistamiento'];
                                }
                                if (!empty($row['stock_maximo_en_alistamiento'])) {
                                    $store_product_warehouse->maximum_picking_stock = $row['stock_maximo_en_alistamiento'];
                                }
                                if (isset($row['manage_stock'])) {
                                    $store_product_warehouse->manage_stock = $row['manage_stock'];
                                }
                                if (isset($row['is_visible_stock'])) {
                                    $store_product_warehouse->is_visible_stock = $row['is_visible_stock'];
                                }
                                if (!empty($row['ideal_stock'])) {
                                    $store_product_warehouse->ideal_stock = $row['ideal_stock'];
                                }
                                if (!empty($row['minimum_stock'])) {
                                    $store_product_warehouse->minimum_stock = $row['minimum_stock'];
                                }
                                if (isset($row['status'])) {
                                    $store_product_warehouse->status = $row['status'];
                                }
                                if (isset($row['descontinuado'])) {
                                    $store_product_warehouse->discontinued = $row['descontinuado'];
                                }
                                if (isset($row['producto_visible'])) {
                                    $store_product_warehouse->is_visible = $row['producto_visible'];
                                }
                                $store_product_warehouse->save();
                            } else {
                                $row_error = $key+1;
                                array_push($errors, [0 => "El producto de la fila: $row_error no está creado en esa bodega."]);
                            }
                        }
                    }
                }
                if (count($errors)) {
                    DB::rollBack();
                    $error_str = "<ul>";
                    foreach ($errors as $index => $error) {
                        $error_str .= "<li> Errores para la fila {$index}:";
                        $error_str .= "<ul>";
                        foreach ($error as $index2 => $item) {
                            $error_str .= "<li>{$item}</li>";
                        }
                        $error_str .= "</ul>";
                        $error_str .= "</li>";
                    }
                    $error_str .= "</ul>";
                    return ['error' => $error_str];
                }
                DB::commit();
                return ['success' => "Todos los productos fueron actualizados correctamente"];

            } catch (\Exception $e) {
                DB::rollBack();
                $errors = "Se presentó el siguiente error: [".$e->getMessage()."]";
                return ['error' => $errors];
            }
        }
    }

    public function get_subcategories_ajax()
    {
        $category_id = Input::get('category_id', null);
        $category = Category::findOrFail($category_id);
        $subcategory = $category->getSubCategories();
        return Response::json($subcategory);
    }

    public function get_subbrands_ajax()
    {
        $brand_id = Input::get('brand_id', null);
        $brand = Brand::findOrFail($brand_id);
        $subbrand = $brand->getSubbrands();
        return Response::json($subbrand);
    }

    /**
     * Funcion para crear categorías desde la página de productos
     */
    public function add_category_ajax()
    {
        $new_category = trim(Input::get('new_category', null));
        $category_obj = Category::where('name', $new_category)->first();
        if (!empty($category_obj)) {
            return Response::json(['status' => false, 'message' => 'La categoría ya existe.']);
        } else {
            $category_obj = new Category;
            $category_obj->name = $new_category;
            $category_obj->save();
            $categories = Category::orderBy('name')->get();
            $data = [
                'categories' => $categories
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['categories'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Funcion para crear marcas desde la página de productos
     */
    public function add_brand_ajax()
    {
        $new_brand = trim(Input::get('new_brand', null));
        $brand_obj = Brand::where('name', $new_brand)->first();
        if (!empty($brand_obj)) {
            return Response::json(['status' => false, 'message' => 'La marca ya existe.']);
        } else {
            $brand_obj = new Brand;
            $brand_obj->name = $new_brand;
            $brand_obj->save();
            $brands = Brand::orderBy('name')->get();
            $data = [
                'brands' => $brands
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['brands'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Funcion para crear fabricantes desde la página de productos
     */
    public function add_maker_ajax()
    {
        $new_maker = trim(Input::get('new_maker', null));
        $maker_obj = Maker::where('name', $new_maker)->first();
        if (!empty($maker_obj)) {
            return Response::json(['status' => false, 'message' => 'El fabricante ya existe.']);
        } else {
            $maker_obj = new Maker;
            $maker_obj->name = $new_maker;
            $maker_obj->save();
            $makers = Maker::orderBy('name')->get();
            $data = [
                'makers' => $makers
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['makers'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Funcion para crear subcategorias desde la página de productos
     */
    public function add_subcategory_ajax()
    {
        $category_id = Input::get('category_id', null);
        $new_subcategory = trim(Input::get('new_subcategory', null));
        $subcategory_obj = Subcategory::where('name', $new_subcategory)->where('category_id', $category_id)->first();
        if (!empty($subcategory_obj)) {
            return Response::json(['status' => false, 'message' => 'La subcategoría ya existe.']);
        } else {
            $subcategory_obj = new Subcategory;
            $subcategory_obj->category_id = $category_id;
            $subcategory_obj->name = $new_subcategory;
            $subcategory_obj->save();
            $subcategories = Subcategory::where('category_id', $category_id)->orderBy('name')->get();
            $data = [
                'subcategories' => $subcategories
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['subcategories'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Funcion para crear submarcas desde la página de productos
     */
    public function add_subbrand_ajax()
    {
        $brand_id = Input::get('brand_id', null);
        $new_subbrand = trim(Input::get('new_subbrand', null));
        $subbrand_obj = Subbrand::where('name', $new_subbrand)->where('brand_id', $brand_id)->first();
        if (!empty($subbrand_obj)) {
            return Response::json(['status' => false, 'message' => 'La subcategoría ya existe.']);
        } else {
            $subbrand_obj = new Subbrand;
            $subbrand_obj->brand_id = $brand_id;
            $subbrand_obj->name = $new_subbrand;
            $subbrand_obj->save();
            $subbrands = Subbrand::where('brand_id', $brand_id)->orderBy('name')->get();
            $data = [
                'subcategories' => $subbrands
            ];
            $html = View::make('admin.products.product_form', $data)->renderSections()['subcategories'];
            return Response::json(['status' => true, 'html' => $html]);
        }
    }

    /**
     * Buscador de productos relacionados
     */
    public function search_product_ajax()
    {
        $term = Input::get('term');
        $product_id = Input::get('product_id', null);
        $products = Product::where(function ($query) use ($term) {
            $query->orWhere('products.reference', 'LIKE', '%'.$term.'%');
            $query->orWhere('products.name', 'LIKE', '%'.$term.'%');
        })->where('id', '<>', $product_id)->whereNotIn('type', ['Agrupado', 'Proveedor']);
        if (!empty($product_id)) {
            $product_group = ProductGroup::where('product_id', $product_id)->lists('product_group_id');
            if (!empty($product_group)) {
                $products = $products->whereNotIn('id', $product_group);
            }
        }

        $products = $products->limit(80)
            ->get();
        foreach ($products as &$product) {
            $prices = StoreProduct::where('product_id', $product->id)->select('store_id', 'price', 'special_price')->get();
            $product->prices = $prices->toArray();
        }

        $data = [
            'products' => $products
        ];

        return View::make('admin.products.product_form', $data)->renderSections()['products'];
    }

    /**
     * Agregar productos a grupo de producto
     */
    public function add_product_group_ajax()
    {
        $product_id = Input::get('product_id');
        $product_group_id = Input::get('product_group_id');
        $quantity = Input::get('quantity');
        $price = Input::get('price');
        $type = Input::get('type');

        $product_obj = Product::find($product_id);
        if ($product_obj->type != $type) {
            $product_obj->type = $type;
            $this->admin_log('products', $product_obj->id, 'update_type');
            $product_obj->save();
            if ($product_obj->type == 'Simple') {
                $product_group = ProductGroup::where('product_id', $product_obj->id)->delete();
            }
        }


        $product_group = ProductGroup::where('product_id', $product_id)->where('product_group_id', $product_group_id)->first();
        if (empty($product_group)) {
            $product_group = new ProductGroup;
            $product_group->product_id = $product_id;
            $product_group->product_group_id = $product_group_id;
            $product_group->quantity = $quantity;
            $product_group->save();
            $this->admin_log('product_group', $product_group->id, 'insert');

            $store_products = StoreProduct::where('product_id', $product_id)->get();
            if (!empty($store_products)) {
                foreach ($store_products as $store_product) {
                    $store_product_gp = StoreProduct::where('product_id', $product_group->product_group_id)->where('store_id', $store_product->store_id)->first();
                    $store_product_groups = StoreProductGroup::where('store_product_id', $store_product->id)->where('store_product_group_id', $store_product_gp->id)->first();
                    if (empty($store_product_groups)) {
                        $store_product_group = new StoreProductGroup;
                        $store_product_group->store_product_id = $store_product->id;
                        $store_product_group->store_product_group_id = $store_product_gp->id;
                        $store_product_group->quantity = $product_group->quantity;
                        $store_product_group->price = $store_product->price;
                        $store_product_group->save();
                        $this->admin_log('store_product_group', $store_product_group->id, 'insert');
                    }
                }
            }

            $data = [
                'status' => true,
                'message' => 'El producto agregado exitosamente.'
            ];
            return Response::json($data);
        } else {
            $data = [
                'status' => false,
                'message' => 'Este producto fue agregado anteriormente.'
            ];
            return Response::json($data);
        }
    }

    /**
     * Eliminar productos del grupo de producto
     */
    public function remove_product_group_ajax()
    {
        $product_id = Input::get('product_id');
        $product_group_id = Input::get('product_group_id');
        $type = Input::get('type');

        try {
            \DB::beginTransaction();
            $product_obj = Product::find($product_id);
            if ($product_obj->type != $type) {
                $product_obj->type = $type;
                $product_obj->save();
                if ($product_obj->type == 'Simple') {
                    $store_products = StoreProduct::where('product_id', $product_id)->get();
                    $product_groups = ProductGroup::where('product_id', $product_obj->id)->get();

                    foreach ($product_groups as $product_group) {
                        foreach ($store_products as $store_product) {
                            $store_product_gp = StoreProduct::where(
                                'product_id',
                                $product_group->product_group_id
                            )->where('store_id', $store_product->store_id)->first();
                            if (!empty($store_product_gp)) {
                                $store_product_groups = StoreProductGroup::where(
                                    'store_product_id',
                                    $store_product->id
                                )->where(
                                    'store_product_group_id',
                                    $store_product_gp->id
                                )->first();
                                if (!empty($store_product_groups)) {
                                    $store_product_groups->delete();
                                }
                            }
                        }
                    }
                }
            }

            $product_group = ProductGroup::where('product_id', $product_id)
                ->where('product_group_id', $product_group_id)
                ->first();
            if (!empty($product_group)) {
                $store_products = StoreProduct::where('product_id', $product_id)->get();
                foreach ($store_products as $store_product) {
                    if (!empty($store_product->special_price)) {
                        throw new \Exception('Debes remover la promoción del producto para actualizar el combo.');
                    }

                    $store_product_gp = StoreProduct::where('product_id', $product_group->product_group_id)
                        ->where('store_id', $store_product->store_id)
                        ->first();

                    if (!empty($store_product_gp)) {
                        $store_product_groups = StoreProductGroup::where('store_product_id', $store_product->id)
                            ->where('store_product_group_id', $store_product_gp->id)
                            ->first();
                        if (!empty($store_product_groups)) {
                            if (!empty($store_product_groups->discount_amount)) {
                                throw new \Exception('Debes remover las promociones del producto para actualizar el combo.');
                            }
                            $this->admin_log('store_product_group', $store_product_gp->id, 'delete');
                            $store_product_groups->delete();
                        }
                    }
                }
                if ($product_group->delete()) {
                    $data = [
                        'status' => true,
                        'message' => 'El producto fue borrado exitosamente'
                    ];
                } else {
                    $data = [
                        'status' => false,
                        'message' => 'Ocurrió un error al borrar el producto'
                    ];
                }
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Ocurrió un error, no se encontró el producto'
                ];
            }

            \DB::commit();
        } catch (\Exception $exception) {
            \DB::rollback();

            $data = [
                'status' => false,
                'message' => $exception->getMessage()
            ];
        }

        return Response::json($data);
    }

    /**
     * Funcion para obtener los productos relacionados
     */
    public function get_product_group_ajax()
    {
        $product_id = Input::get('product_id');
        // $product_group = ProductGroup::where('product_id', $product_id)->get();
        $product_group = Product::join('product_group', 'products.id', '=', 'product_group.product_group_id')
            ->where('product_group.product_id', $product_id)
            ->select(
                'products.id',
                'products.reference',
                'products.name',
                'products.quantity',
                'products.unit',
                'products.image_small_url',
                'product_group.quantity AS product_group_quantity',
                'product_group.price AS product_group_price'
            )
            ->get();
        $data = [
            'product_group' => $product_group
        ];
        return View::make('admin.products.product_form', $data)->renderSections()['product_group'];
    }

    /**
     * Guardad producto sugerido desde admin
     */
    public function save_suggested_product()
    {
        $input = Input::all();
        //Valida campos requeridos
        $validator = Validator::make($input,[
            'promo' => 'required',
            'priority' => 'required',
            'status' => 'required',
            'store_product_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        //redirige si incumple las reglas
        if($validator->fails()) {
            return Redirect::back()->with('type', 'error')->with('message', $validator->errors())->withInput();
        }
        //obtiene los campos
        $promo = $input['promo'];
        $priority = $input['priority'];
        $status = $input['status'];
        $store_product_id = $input['store_product_id'];
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $shelves_suggested_products = Session::get('admin_shelves_suggested_products');

        if ($start_date < date("Y/m/d")) {
            return Redirect::back()->with('type', 'error')->with('message', 'La fecha inicial debe ser mayor o igual a la fecha actual.')->withInput();
        }

        if ($start_date > $end_date) {
            return Redirect::back()->with('type', 'error')->with('message', 'La fecha inicial debe ser mayor a la fecha final.')->withInput();
        }
        if (!isset($input['update'])) {
            $exist_storeProduct_or_promo = ShelvesSuggestedProducts::where('store_product_id', $store_product_id)->orWhere('promo', $promo)->first();
            if(!empty($exist_storeProduct_or_promo)) {
                return Redirect::back()->with('type', 'error')->with('message', 'El producto sugerido ya existe.')->withInput();
            }
        }

        //Actualiza el producto sugerido
        if (isset($input['update'])) {
            $suggested_products = ShelvesSuggestedProducts::where('store_product_id', $store_product_id)->where('promo', $promo)->get();
            foreach ($suggested_products as $suggested_product) {
                $suggested_product->priority = $priority;
                $suggested_product->start_date = $start_date;
                $suggested_product->end_date = $end_date;
                $suggested_product->status = $status;
                $suggested_product->save();
            }
        }
        if ($shelves_suggested_products) {
            if (count($shelves_suggested_products) > 0) {
                foreach ($shelves_suggested_products as $key => $value) {
                    foreach ($value['shelves'] as $item_key => $item_value) {
                        if (isset($input['update'])) {
                            $exist_shelf_id = ShelvesSuggestedProducts::where('store_product_id', $store_product_id)->where('promo', $promo)->where('shelf_id', $item_value)->first();
                        }
                        if(empty($exist_shelf_id)) {
                            $suggested_product = new ShelvesSuggestedProducts();
                            $suggested_product->shelf_id = $item_value;
                            $suggested_product->store_product_id = $store_product_id;
                            $suggested_product->promo = $promo;
                            $suggested_product->priority = $priority;
                            $suggested_product->start_date = $start_date;
                            $suggested_product->end_date = $end_date;
                            $suggested_product->status = $status;
                            $suggested_product->save();
                        }
                    }
                }
            } else {
                if (!isset($input['update'])) {
                    return Redirect::back()->with('type', 'error')->with('message', 'Debe seleccionar más de un pasillo para el producto sugerido.')->withInput();
                } else {
                    return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido actualizado con éxito.');
                }
            }
        } else {
            if (!isset($input['update'])) {
                return Redirect::back()->with('type', 'error')->with('message', 'Debe seleccionar más de un pasillo para el producto sugerido.');
            } else {
                return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido actualizado con éxito.');
            }
        }
        Session::forget('admin_shelves_suggested_products');
        if (!isset($input['update'])) {
            return Redirect::route('adminProducts.editSuggestedProduct', ['id' => $suggested_product->store_product_id, 'promo' => $suggested_product->promo])
                ->with('type', 'success')
                ->with('message', 'Producto sugerido creado con éxito.');
        } else {
            return Redirect::route('adminProducts.editSuggestedProduct', ['id' => $suggested_product->store_product_id, 'promo' => $suggested_product->promo])
                ->with('type', 'success')
                ->with('message', 'Producto sugerido actualizado con éxito.');
        }
        /*return Redirect::route('adminProducts.getSuggestedProducts')
            ->with('type', 'success')
            ->with('message', 'Producto sugerido creado con éxito.');*/
    }

    /**
     * Obtiene los productos por store_id en ajax
     */
    public function get_products_by_store_ajax()
    {
        $products = [];
        if (Input::has('store_id')) {
            $products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', Input::get('store_id'))
                ->select('store_products.*', 'products.name', 'products.unit', 'products.quantity')
                ->orderBy('products.name', 'ASC')
                ->get();
            return Response::json(['result' => ['products' => $products], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['products' => $products], 'message'=> '', 'status' => '400'], 400);
        }
    }

    /**
     * Obtiene los departamentos por store_id en ajax
     */
    public function get_departments_by_store_ajax()
    {
        $departments = [];
        if (Input::has('store_id')) {
            $departments = Department::where('store_id', Input::get('store_id'))
                ->select('id', 'name')
                ->orderBy('name', 'ASC')
                ->get();
            return Response::json(['result' => ['departments' => $departments], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['departments' => $departments], 'message'=> '', 'status' => '400'], 400);
        }
    }

    public function get_shelves_by_department_ajax()
    {
        $shelves = [];
        if (Input::has('department_id')) {
            $shelves = Shelf::where('status', 1)
                ->where('department_id', Input::get('department_id'))
                ->orderBy('name', 'ASC')
                ->get();
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '400'], 400);
        }
    }

    public function add_shelves_suggested_products_ajax()
    {
        $shelves = [];
        Session::forget('admin_shelves_suggested_products');
        if (Input::has('shelves_suggested_products')) {
            $shelves_suggested_products = Input::get('shelves_suggested_products');
            Session::put('admin_shelves_suggested_products', $shelves_suggested_products);
            foreach ($shelves_suggested_products as $key => $item) {
                $shelves_obj = [];
                $dpt = Department::select('name')->where('id', $item['department_id'])->first();
                $shelves[$key]['department'] = $dpt->name;
                foreach ($item['shelves'] as $item_key => $item_shelf) {
                    $shelf = Shelf::find($item_shelf);
                    $shelves_obj[$item_key] = $shelf->name;
                }
                $shelves[$key]['items'] = $shelves_obj;
            }
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '200'], 200);
        } else {
            return Response::json(['result' => ['shelves' => $shelves], 'message'=> '', 'status' => '400'], 400);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete_suggested_products($id)
    {
        if ($id) {
            DB::delete('DELETE FROM shelves_suggested_products WHERE store_product_id = '.$id);
            $this->admin_log('shelves_suggested_products', $id, 'delete');
            return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido eliminado con éxito.');
        } else {
            return Redirect::back()->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar el producto sugerido.');
        }
    }

    /**
     * Listado de productos sugeridos
     */
    public function get_suggested_products()
    {
        $suggested_products = ShelvesSuggestedProducts::join('store_products', 'store_products.id', '=', 'shelves_suggested_products.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->select('shelves_suggested_products.id', 'products.name', 'products.quantity', 'products.unit', DB::raw('count(shelves_suggested_products.shelf_id) AS shelf'), 'promo', 'shelves_suggested_products.start_date', 'shelves_suggested_products.end_date', 'shelves_suggested_products.status', 'store_product_id', 'priority')
            ->groupBy('promo', 'store_product_id')
            ->orderBy('shelves_suggested_products.status', 'DESC')
            ->orderBy('priority', 'ASC')
            /*->limit(80)*/->get();

        return View::make('admin.products.suggested.index')->with('title', 'Productos Sugeridos En Carrito')->with('suggested_products', $suggested_products);
    }

    /**
     * Formulario para crear o editar productos sugeridos
     */
    public function edit_suggested_product()
    {
        Session::forget('admin_shelves_suggested_products');
        $cities = $this->get_cities();
        $suggested_product = [];
        if(!Input::has('id')) {
            $title = 'Agregar Nuevo Producto Sugerido';
            $store = Store::where('status', 1)->where('city_id', Session::get('admin_city_id'))->first();
            $departments = Department::where('status', 1)->where('store_id', $store->id)->get();
            //$shelves = Shelf::where('status', 1)->where('store_id', $store->id)->get();
            $shelves = [];
            return View::make('admin.products.suggested.form')
                ->with('title', $title)
                ->with('suggested_product', $suggested_product)
                ->with('departments', $departments)
                ->with('shelves', $shelves)
                ->with('cities', $cities);
        } else {
            $input =Input::all();
            $suggested_product = ShelvesSuggestedProducts::where('store_product_id', $input['id'])->where('promo', $input['promo'])->first();
            $suggested_products = ShelvesSuggestedProducts::where('store_product_id', $input['id'])->where('promo', $input['promo'])->get();
            $title = 'Editar Producto Sugerido';
            $products = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                ->where('store_products.store_id', $suggested_product->shelf->store->id)
                ->select('store_products.*', 'products.name',  'products.quantity', 'products.unit')
                ->orderBy('products.name', 'ASC')
                ->get();
            $stores = Store::where('status', 1)->where('city_id', $suggested_product->shelf->store->city->id)->get();
            $departments = Department::where('status', 1)->where('store_id', $suggested_product->shelf->store->id)->get();
            $shelves = Shelf::where('status', 1)->where('department_id', $suggested_product->shelf->department->id)->get();
            return View::make('admin.products.suggested.form')
                ->with('title', $title)
                ->with('suggested_product', $suggested_product)
                ->with('suggested_products', $suggested_products)
                ->with('stores', $stores)
                ->with('products', $products)
                ->with('departments', $departments)
                ->with('shelves', $shelves)
                ->with('cities', $cities);
        }
    }

    /**
     * Activar producto sugerido
     * @param $id
     * @return
     */
    public function toggle($id)
    {
        $suggested_products = ShelvesSuggestedProducts::where('store_product_id', $id)->first();
        if ($suggested_products) {
            $suggested_products->status = ($suggested_products->status + 1) % 2;
            if ($suggested_products->status) {
                DB::update('UPDATE shelves_suggested_products SET status = 1 WHERE store_product_id = '.$id);
            } else {
                DB::update('UPDATE shelves_suggested_products SET status = 0 WHERE store_product_id = '.$id);
            }

            return Redirect::back()->with('type', 'success')->with('message', 'Producto sugerido actualizado con éxito.');
        } else {
            return Redirect::back()->with('type', 'failed')->with('message', 'Ocurrió un error al actualizar el producto sugerido.');
        }
    }

    /**
     * Obtener los productos de la tienda
     */
    public function get_store_product_ajax()
    {
        $store_id = Input::get('store_id');
        $product_id = Input::get('product_id');
        $store = Store::with('city.warehouses')->find($store_id);
        $product = Product::find($product_id);
        $store_product = StoreProduct::where('store_id', $store_id)->where('product_id', $product_id)->first();
        $allied_stores = AlliedStore::where('city_id', $store->city_id)->where('status', 1)->get();
        if (!empty($store_product->delivery_discount_start_date)) {
            $store_product->delivery_discount_start_date = Carbon::parse($store_product->delivery_discount_start_date)->format('d/m/Y H:i:s');
        }
        if (!empty($store_product->special_price_expiration_date)) {
            $store_product->org_special_price_expiration_date = $store_product->special_price_expiration_date;
            $store_product->special_price_expiration_date = Carbon::parse($store_product->special_price_expiration_date)->format('d/m/Y H:i:s');
        }
        if (!empty($store_product->special_price_starting_date)) {
            $store_product->org_special_price_starting_date = $store_product->special_price_starting_date;
            $store_product->special_price_starting_date = Carbon::parse($store_product->special_price_starting_date)->format('d/m/Y H:i:s');
        }
        if (!empty($store_product)) {
            $departments = Department::where('status', 1)->where('store_id', $store_id)->get();
            $departments = $departments->each(function (&$department) use ($store_product) {
                if ($department->id == $store_product->department_id) {
                    $department->selected = true;
                    return;
                }
            });

            $shelves = Shelf::where('status', 1)->where('store_id', $store_id)->where('department_id', $store_product->department_id)->get();
            $shelves = $shelves->each(function (&$shelf) use ($store_product) {
                if ($shelf->id == $store_product->shelf_id) {
                    $shelf->selected = true;
                    return;
                }
            });

            $providers = Provider::where('status', 1)->get();
            $providers = $providers->each(function (&$provider) use ($store_product) {
                if ($provider->id == $store_product->provider_id) {
                    $provider->selected = true;
                    return;
                }
            });

            $data = [
                'store_product' => $store_product,
                'departments' => $departments,
                'shelves' => $shelves,
                'providers' => $providers,
                'store_products' => true,
                'store_id' => $store_id,
                'product_id' => $product_id,
                'product' => $product,
                'allied_stores' => $allied_stores,
                'warehouses' => $store->city->warehouses
            ];

            if ($product->type == 'Agrupado' || $product->type == 'Proveedor') {
                $product_groups = ProductGroup::where('product_id', $product_id)->get();
                $product_groups_ids = $product_groups->lists('product_group_id');
                if (count($product_groups)) {
                    $products = Product::whereIn('products.id', $product_groups_ids)
                        ->join('store_products', 'products.id', '=', 'store_products.product_id')
                        ->join('product_group', function ($join) use ($product_id) {
                            $join->on('products.id', '=', 'product_group.product_group_id')
                                ->where('product_group.product_id', '=', $product_id);
                        })
                        ->join('store_product_group', function ($join) use ($store_product) {
                            $join->on('store_products.id', '=', 'store_product_group.store_product_group_id')
                                ->where('store_product_group.store_product_id', '=', $store_product->id);
                        })
                        ->where('store_id', $store_id)
                        ->select(
                            'products.id',
                            'products.image_small_url',
                            'products.reference',
                            'products.name',
                            'products.quantity',
                            'products.unit',
                            'store_products.id AS store_product_id',
                            'store_products.product_id',
                            'product_group.price',
                            'product_group.quantity as group_quantity',
                            'product_group.price as group_price',
                            'store_product_group.id as store_product_group_id',
                            'store_product_group.quantity as store_product_group_quantity',
                            'store_products.price as store_product_group_price'
                        )
                        ->whereNotNull('store_product_group.id')
                        ->get();

                    if (count($product_groups) != count($products)) {
                        $store_product_group_ids = $products->lists('product_id');

                        $products = Product::whereIn('products.id', $product_groups_ids)
                            ->join('store_products', 'products.id', '=', 'store_products.product_id')
                            ->select(
                                'products.id',
                                'products.image_small_url',
                                'products.reference',
                                'products.name',
                                'products.quantity',
                                'products.unit',
                                'store_products.id AS store_product_id',
                                'store_products.product_id',
                                'store_products.price as store_product_group_price'
                            )
                            ->get();

                        $products = $products->filter(function ($product) use ($store_product_group_ids) {
                            return !in_array($product->product_id, $store_product_group_ids);
                        });
                        $data['store_product_group_errors'] = $products;
                    } else {
                        $data['store_product_groups'] = $products;
                    }
                }
            }

            return View::make('admin.products.product_form', $data)->renderSections()['store_products'];
        }

        $departments = Department::where('status', 1)->where('store_id', $store_id)->get();
        $providers = Provider::where('status', 1)->get();

        $data = [
            'store_product' => $store_product,
            'departments' => $departments,
            'providers' => $providers,
            'store_products' => true,
            'store_id' => $store_id,
            'product_id' => $product_id,
            'allied_stores' => $allied_stores,
            'warehouses' => $store->city->warehouses,
            'product' => $product
        ];

        return View::make('admin.products.product_form', $data)->renderSections()['store_products'];
    }

    public function save_store_product()
    {
        \DB::beginTransaction();
        try {
            if (Input::has('id')) {
                $id = Input::get('id');
                $store_product = StoreProduct::with('product')->find($id);
                $action = 'update';
            } else {
                $store_product = new StoreProduct();
                $store_product->store_id = Input::get('store_id');
                $product = Product::find(Input::get('product_id'));
                $store_product->product()->associate($product);
                $action = 'insert';
            }
            if (Input::has('store_product_group_price')) {
                // cuando no hay productos en la table relacionados
                if (Input::has('store_product_id')) {
                    $store_product_ids = Input::get('store_product_id');
                    $store_product_group_prices = Input::get('store_product_group_price');
                    $store_product_group_quantities = 0;
                    if (Input::has('store_product_group_quantity')) {
                        $store_product_group_quantities = Input::get('store_product_group_quantity');
                    }
                    foreach ($store_product_ids as $key => $store_product_id) {
                        $store_product_group = new StoreProductGroup();
                        if (Input::has('store_product_group_quantity')) {
                            $store_product_group->quantity = $store_product_group_quantities[$key];
                        }
                        $store_product_group->storeProductGroup()->associate($store_product);
                        $store_product_group->price = $store_product->price;
                        $store_product_group->store_product_group_id = $store_product_id;
                        $store_product_group->save();
                    }
                }

                if (Input::has('store_product_group_id')) {
                    $total = 0;
                    $store_product_group_ids = Input::get('store_product_group_id');
                    $store_product_group_prices = Input::get('store_product_group_price');
                    foreach ($store_product_group_ids as $key => $store_product_group_id) {
                        $store_product_group = StoreProductGroup::find($store_product_group_id);
                        $store_product_group->price = $store_product_group_prices[$key];
                        $total += $store_product_group->price * $store_product_group->quantity;

                        $store_product_group->save();
                    }

                    if ($total !== intval(Input::get('price'))) {
                        throw new \Exception('El precio total de productos no es igual precio del combo.');
                    }
                }
            }

            $is_simple = $store_product->product->type === 'Simple';

            $store_product->department_id = Input::get('department_id');
            $store_product->shelf_id = Input::get('shelf_id');
            $store_product->sort_order = Input::get('sort_order');
            $store_product->is_best_price = Input::get('is_best_price');
            if($this->admin_permissions['permission1']) {
                $store_product->public_price = Input::get('public_price') ?: null;
            }
            $store_product->price = $is_simple ? Input::get('price') : ($store_product->price ?: 0);

            $store_product->iva = Input::get('iva');
            $store_product->base_price = $store_product->price / (1 + $store_product->iva / 100);
            $store_product->consumption_tax = Input::get('consumption_tax');

            $store_product->delivery_discount_amount = (!is_null(Input::get('delivery_discount_amount'))) ? Input::get('delivery_discount_amount') : 0;
            $store_product->delivery_maximum_amount = Input::get('delivery_maximum_amount');
            $delivery_discount_start_date = Input::get('delivery_discount_start_date');
            if (empty($delivery_discount_start_date)) {
                $store_product->delivery_discount_start_date = null;
            } else {
                $store_product->delivery_discount_start_date = Carbon::createFromFormat(
                    'd/m/Y H:i:s',
                    $delivery_discount_start_date
                )->toDateTimeString();
            }

            if (empty(Input::get('has_special_price'))) {
                $store_product->special_price = null;
                $store_product->quantity_special_price = 0;
            } else {
                $total_discount = 0;
                $store_product->special_price = Input::get('special_price') ?: null;
                $store_product->quantity_special_price = Input::get('quantity_special_price') ?: 0;
                $store_product->quantity_special_price_stock = Input::get('quantity_special_price_stock');
                $store_product->has_quantity_special_price_stock = Input::get('has_quantity_special_price_stock');
                $store_product->first_order_special_price = Input::get('first_order_special_price');
                $special_price_expiration_date = Input::get('special_price_expiration_date');
                $special_price_starting_date = Input::get('special_price_starting_date');
                if (empty($special_price_expiration_date)) {
                    $store_product->special_price_expiration_date = null;
                } else {
                    $store_product->special_price_expiration_date = Carbon::createFromFormat(
                        'd/m/Y H:i:s',
                        $special_price_expiration_date
                    )->toDateTimeString();
                }

                if (empty($special_price_starting_date)) {
                    $store_product->special_price_starting_date = null;
                } else {
                    $store_product->special_price_starting_date = Carbon::createFromFormat(
                        'd/m/Y H:i:s',
                        $special_price_starting_date
                    )->toDateTimeString();
                }

                $product_items = $is_simple
                    ? [$store_product]
                    : StoreProductGroup::where('store_product_id', $store_product->id)->get();

                foreach ($product_items as $index => $product_item) {
                    if (!$is_simple) {
                        $product_item->discount_amount = Input::get('discount_amount')[$index];

                        $unit_price = $store_product->store_products[$index]->price;
                        if( $product_item->discount_amount > ($unit_price * $product_item->quantity) ){
                            throw new \Exception('El descuento de un producto no puede ser mayor a su precio unitario');
                        }

                        $total_discount += Input::get('discount_amount')[$index];
                    }

                    $accept_discount = $is_simple || !empty($product_item->discount_amount);

                    if($this->admin_permissions['permission1']) {
                        $product_item->merqueo_discount = $accept_discount
                            ? Input::get('merqueo_discount')[$index] : 0;
                    }
                    $product_item->provider_discount = $accept_discount
                        ? Input::get('provider_discount')[$index] : 0;
                    $product_item->seller_discount = $accept_discount
                        ? Input::get('seller_discount')[$index] : 0;

                    $total_distribution = $product_item->provider_discount + $product_item->merqueo_discount + $product_item->seller_discount;

                    if (intval($total_distribution) !== 100 && (!empty($product_item->discount_amount) || $is_simple)) {
                        throw new \Exception('La distribución del producto no es correcta.');
                    }

                    $product_item->save();
                }

                if (!$is_simple && $total_discount !== intval($store_product->price - $store_product->special_price)) {
                    throw new \Exception('La distribución del descuento en los productos no es correcta.');
                }
            }

            $store_product->provider_plu = Input::get('provider_plu');
            $store_product->provider_id = Input::get('provider_id');
            $store_product->provider_pack_type = Input::get('provider_pack_type');
            $store_product->provider_pack_quantity = Input::get('provider_pack_quantity');
            $store_product->provider_pack_quantity_approach = Input::get('provider_pack_quantity_approach');
            $store_product->cost = Input::get('cost') ?: 0;
            $store_product->base_cost = $store_product->cost / (1 + $store_product->iva / 100);
            $store_product->storage = Input::get('storage');
            $associate_file = function($input_name, $field, $directory = 'stores/products') use (&$store_product) {
                $cloudfront_url = Config::get('app.aws.cloudfront_url');
                if (Input::hasFile($input_name)) {
                    if (!empty($store_product->{$field}) && !strstr($store_product->{$field}, 'default')){
                        $path = str_replace($cloudfront_url, uploads_path(), $store_product->{$field});
                        remove_file($path);
                        remove_file_s3($store_product->{$field});
                    }
                    $store_product->{$field} = upload_image(Input::file($input_name), FALSE, $directory);
                }
            };
            $associate_file('guarantee_policies', 'guarantee_policies');
            $store_product->useful_life_days = Input::get('useful_life_days');
            $store_product->handle_expiration_date = Input::get('handle_expiration_date');
            $store_product->allied_store_id = empty(Input::get('allied_store_id')) ? null : Input::get('allied_store_id');
            //$store_product->save();

            $url_product = route('frontStoreProducts.single_product', [
                'city_slug'       => $store_product->store->city->slug,
                'store_slug'      => $store_product->store->slug,
                'department_slug' => $store_product->department->slug,
                'shelf_slug'      => $store_product->shelf->slug,
                'product_slug'    => $store_product->product->slug,
            ]);

            $url_product = str_replace(Config::get('app.url'), 'https://merqueo.com', $url_product);

            $deeplink_product_url =  \FirebaseClient::generateDynamicLink([
                'url'           => $url_product,
                'type'          => 'product',
                'store_id'      => $store_product->store_id,
                'department_id' => $store_product->department_id,
                'shelf_id'      => $store_product->shelf_id,
                'product_id'    => $store_product->id,
            ]);
            $store_product->deeplink = $deeplink_product_url->shortLink;
            $store_product->save();

            if ($store_product->product->type == 'Agrupado' || $store_product->product->type == 'Proveedor') {
                $store_product_group = StoreProductGroup::where('store_product_id', $store_product->id)->count();
                if (!$store_product_group) {
                    $product_groups = ProductGroup::where('product_id', $store_product->product_id)->get();
                    foreach ($product_groups as $product_group) {
                        $store_product_gp = StoreProduct::where('product_id', $product_group->product_group_id)
                            ->where('store_id', $store_product->store_id)
                            ->first();
                        $store_product_group = new StoreProductGroup;
                        $store_product_group->store_product_id = $store_product->id;
                        $store_product_group->store_product_group_id = $store_product_gp->id;
                        $store_product_group->quantity = $product_group->quantity;
                        $store_product_group->price = $store_product_gp->price;
                        $store_product_group->save();
                    }
                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            \DB::rollback();
            return Redirect::back()->with('error', $exception->getMessage());
        }


        $this->admin_log('store_products', $store_product->id, $action);
        return Redirect::back()->with('success', 'Producto guardado con éxito.');
    }

    /**
     * Funcion para exportar productos de tiendas con el mismo formato del importador
     */
    public function export_products()
    {
        ini_set('memory_limit', '2024M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $store_id = Input::get('export_store_id');
        $store = Store::find($store_id);
        $city_id = Input::get('export_city_id');
        $city = City::find($city_id);

        $filename = $store->name.' '.$city->city.' '.date('Y-m-d H.i.s', time());
        return Excel::create($filename, function ($excel) {
            $excel->sheet('Productos', function ($sheet) {
                $sheet->setColumnFormat(array(
                    'B' => '@',
                    'C' => '@',
                    'K' => '@',
                    'L' => '@',
                    'V' => '@',
                    'AL' => '@',
                    'AW' => 'yyyy-mm-dd h:mm:ss;@',
                    'AX' => 'yyyy-mm-dd h:mm:ss;@',
                    'BC' => 'yyyy-mm-dd h:mm:ss;@',
                ));

                $titles = [
                    'PRODUCT_ID', // A
                    'PRODUCTO', // B
                    'REFERENCIA', // C
                    'ID_CATEGORIA', // D
                    'NOMBRE_CATEGORÍA', // E
                    'ID_SUBCATEGORIA', // F
                    'NOMBRE_SUBCATEGORIA', // G
                    'MARCA', // H
                    'SUBMARCA', // I
                    'FABRICANTE', // J
                    'DESCRIPCION', // K
                    'INFORMACION_NUTRICIONAL', // L
                    'CODIGO_BARRAS', // M
                    'ES_PERECEDERO', // N
                    'PRESENTACION', // O
                    'SUBPRESENTACION', // P
                    'CANTIDAD', // Q
                    'UNIDAD', // R
                    'DIMENSIONES_ALTO', // S
                    'DIMENSIONES_ANCHO', // T
                    'DIMENSIONES_LARGO', // U
                    'DIMENSIONES_TAMANO', // V
                    'CUENTA_CONTABLE', // W
                    'TIPO_CUENTA_CONTABLE', // X
                    'LINEA_CONTABLE', // Y
                    'GRUPO_CONTABLE', // Z
                    'CODIGO_CONTABLE', // AA
                    'TIPO_PRODUCTO', // AB
                    'ID_DEPARTAMENTO', // AC
                    'DEPARTAMENTO', // AD
                    'ID_PASILLO', // AE
                    'PASILLO', // AF
                    'POSICION_EN_PAGINA_WEB', // AG
                    'MANEJA_FECHA_DE_VENCIMIENTO', // AH
                    'ES_MEJOR_PRECIO_GARANTIZADO', // AI
                    'ID_PROVEEDOR', // AJ
                    'ID_TIENDA_ALIADA', // AK
                    'PLU_PROVEEDOR', // AL
                    'TIPO_EMBALAJE_DE_PROVEEDOR', // AM
                    'CANTIDAD_EMBALAJE', // AN
                    'COSTO_IVA', // AO
                    'COSTO_BASE', // AP
                    'PRECIO', // AQ
                    'IVA', // AR
                    'IMPUESTO_AL_CONSUMO', // AS
                    'PRECIO_BASE', // AT
                    'PRECIO_ESPECIAL', // AU
                    'PRECIO_MOSTRAR_PAGINA', // AV
                    'FECHA_INICIO_PRECIO_ESPECIAL', //AW
                    'FECHA_EXPIRACION_PRECIO_ESPECIAL', // AX
                    'CANTIDAD_PRECIO_ESPECIAL', // AY
                    'PRIMERA_COMPRA_PRECIO_ESPECIAL', // AZ
                    'DESCUENTO_EN_DOMICILIO', // BA
                    'DESCUENTO_TOTAL_MAXIMO_DOMICILIO', // BB
                    'FECHA_INICIO_DESCUENTO_DOMICILIO', // BC
                    'MANEJA_STOCK_CON_PRECIO_ESPECIAL', // BD
                    'STOCK_CON_PRECIO_ESPECIAL', // BE
                    'PRECIO_DESCUENTO_MERQUEO', // BF
                    'PRECIO_DESCUENTO_PROVEEDOR', // BG
                    'PRECIO_DESCUENTO_VENDEDOR', // BH
                    'ALMACENAMIENTO', // BI
                    'DIAS_VIDA_UTIL', // BJ
                    'MANAGE_STOCK', // BK
                    'PRODUCTO_VISIBLE', // BL
                    'STATUS', // BM
                    'DESCONTINUADO', // BN
                    'IS_VISIBLE_STOCK', // BO
                    'MINIMUM_STOCK', // BP
                    'IDEAL_STOCK', // BQ
                    'STOCK_MINIMO_EN_ALISTAMIENTO', // BR
                    'STOCK_MAXIMO_EN_ALISTAMIENTO', // BS
                    'POSICION_EN_BODEGA', // BT
                    'POSICION_EN_BODEGA_ALTURA', // BU
                    'APROXIMACION_A_EMBALAJE', // BV
                    'PRECIO_NEGOCIADO' // BW
                ];

                $sheet->appendRow($titles);

                $city_id = Input::get('export_city_id');
                $city = City::find($city_id);
                $store_id = Input::get('export_store_id');
                $store = Store::find($store_id);
                $warehouse_id = Input::get('export_warehouse_id');


                $products = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
                    ->leftJoin('categories', 'products.category_id', '=', 'categories.id')
                    ->leftJoin('subcategories', 'products.subcategory_id', '=', 'subcategories.id')
                    ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
                    ->leftJoin('subbrands', 'products.subbrand_id', '=', 'subbrands.id')
                    ->leftJoin('makers', 'products.maker_id', '=', 'makers.id')
                    ->leftJoin('departments', 'store_products.department_id', '=', 'departments.id')
                    ->leftJoin('shelves', 'store_products.shelf_id', '=', 'shelves.id')
                    ->leftJoin('providers', 'store_products.provider_id', '=', 'providers.id')
                    // ->leftJoin('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->join('store_product_warehouses', function ($q) use ($warehouse_id) {
                        $q->on('store_products.id', '=', 'store_product_warehouses.store_product_id')
                            ->where('store_product_warehouses.warehouse_id', '=', $warehouse_id);
                    })
                    ->where('store_products.store_id', '=', $store_id)
                    ->where('store_product_warehouses.discontinued', 0)
                    ->where('products.type', 'Simple')
                    ->orderBy('departments.sort_order', 'shelves.sort_order', 'store_products.sort_order')
                    ->select(
                    // Información básica
                        'products.id AS PRODUCT_ID',
                        'products.name AS PRODUCTO',
                        'products.reference AS REFERENCIA',
                        'products.category_id AS ID_CATEGORIA',
                        'categories.name AS NOMBRE_CATEGORÍA',
                        'products.subcategory_id AS ID_SUBCATEGORIA',
                        'subcategories.name AS NOMBRE_SUBCATEGORIA',
                        'brands.name AS MARCA',
                        'subbrands.name AS SUBMARCA',
                        'makers.name AS FABRICANTE',
                        'products.description AS DESCRIPCION',
                        'products.nutrition_facts AS INFORMACION_NUTRICIONAL',
                        'products.has_barcode AS CODIGO_BARRAS',
                        'products.is_perishable AS ES_PERECEDERO',
                        // Presentación
                        'products.presentation AS PRESENTACION',
                        'products.subpresentation AS SUBPRESENTACION',
                        'products.quantity AS CANTIDAD',
                        'products.unit AS UNIDAD',
                        // Dimensiones
                        'products.height AS DIMENSIONES_ALTO',
                        'products.width AS DIMENSIONES_ANCHO',
                        'products.length AS DIMENSIONES_LARGO',
                        'products.size AS DIMENSIONES_TAMANO',
                        // Contabilidad
                        'products.accounting_account AS CUENTA_CONTABLE',
                        'products.accounting_account_type AS TIPO_CUENTA_CONTABLE',
                        'products.accounting_line AS LINEA_CONTABLE',
                        'products.accounting_group AS GRUPO_CONTABLE',
                        'products.accounting_code AS CODIGO_CONTABLE',
                        'products.type AS TIPO_PRODUCTO',
                        // Productoen tiendas
                        'store_products.department_id AS ID_DEPARTAMENTO',
                        'departments.name AS DEPARTAMENTO',
                        'store_products.shelf_id AS ID_PASILLO',
                        'shelves.name AS PASILLO',
                        'store_products.sort_order AS POSICION_EN_PAGINA_WEB',
                        'store_products.handle_expiration_date AS MANEJA_FECHA_DE_VENCIMIENTO',
                        'store_products.is_best_price AS ES_MEJOR_PRECIO_GARANTIZADO',
                        // Proveedor
                        'store_products.provider_id AS ID_PROVEEDOR',
                        'store_products.allied_store_id AS ID_TIENDA_ALIADA',
                        'store_products.provider_plu AS PLU_PROVEEDOR',
                        'store_products.provider_pack_type AS TIPO_EMBALAJE_DE_PROVEEDOR',
                        'store_products.provider_pack_quantity AS CANTIDAD_EMBALAJE',
                        'store_products.cost AS COSTO_IVA',
                        'store_products.base_cost AS COSTO_BASE',
                        // Precio
                        'store_products.price AS PRECIO',
                        'store_products.iva AS IVA',
                        'store_products.consumption_tax AS IMPUESTO_AL_CONSUMO',
                        'store_products.base_price AS PRECIO_BASE',
                        // Promoción
                        'store_products.special_price AS PRECIO_ESPECIAL',
                        'store_products.public_price AS PRECIO_MOSTRAR_PAGINA',
                        'store_products.special_price_starting_date AS FECHA_INICIO_PRECIO_ESPECIAL',
                        'store_products.special_price_expiration_date AS FECHA_EXPIRACION_PRECIO_ESPECIAL',
                        'store_products.quantity_special_price AS CANTIDAD_PRECIO_ESPECIAL',
                        'store_products.first_order_special_price AS PRIMERA_COMPRA_PRECIO_ESPECIAL',
                        'store_products.delivery_discount_amount AS DESCUENTO_EN_DOMICILIO',
                        'store_products.delivery_maximum_amount AS DESCUENTO_TOTAL_MAXIMO_DOMICILIO',
                        'store_products.delivery_discount_start_date AS FECHA_INICIO_DESCUENTO_DOMICILIO',
                        'store_products.has_quantity_special_price_stock AS MANEJA_STOCK_CON_PRECIO_ESPECIAL',
                        'store_products.quantity_special_price_stock AS STOCK_CON_PRECIO_ESPECIAL',
                        // Distribución de promoción
                        'store_products.merqueo_discount AS PRECIO_DESCUENTO_MERQUEO',
                        'store_products.provider_discount AS PRECIO_DESCUENTO_PROVEEDOR',
                        'store_products.seller_discount AS PRECIO_DESCUENTO_VENDEDOR',
                        // Almacenamiento
                        'store_products.storage AS ALMACENAMIENTO',
                        'store_products.useful_life_days AS DIAS_VIDA_UTIL',
                        // Inventario
                        'store_product_warehouses.manage_stock AS MANAGE_STOCK',
                        'store_product_warehouses.is_visible AS PRODUCTO_VISIBLE',
                        'store_product_warehouses.status AS STATUS',
                        'store_product_warehouses.discontinued AS DESCONTINUADO',
                        'store_product_warehouses.is_visible_stock AS IS_VISIBLE_STOCK',
                        'store_product_warehouses.minimum_stock AS MINIMUM_STOCK',
                        'store_product_warehouses.ideal_stock AS IDEAL_STOCK',
                        'store_product_warehouses.minimum_picking_stock AS STOCK_MINIMO_EN_ALISTAMIENTO',
                        'store_product_warehouses.maximum_picking_stock AS STOCK_MAXIMO_EN_ALISTAMIENTO',
                        'store_product_warehouses.storage_position AS POSICION_EN_BODEGA',
                        'store_product_warehouses.storage_height_position AS POSICION_EN_BODEGA_ALTURA',
                        'store_products.provider_pack_quantity_approach AS APROXIMACION_A_EMBALAJE',
                        'store_products.negociated_price AS PRECIO_NEGOCIADO'
                    );

                $products->chunk(50, function ($rows) use ($sheet) {
                    foreach ($rows as $key => $row) {
                        if (!empty($row->DESCRIPCION)) {
                            $row->DESCRIPCION = html_entity_decode($row->DESCRIPCION);
                        }
                        if (!empty($row->INFORMACION_NUTRICIONAL)) {
                            $row->INFORMACION_NUTRICIONAL = html_entity_decode($row->INFORMACION_NUTRICIONAL);
                        }

                        $array_data = $row->toArray();
                        $sheet->appendRow($array_data);
                    }
                });
                // $sheet->fromArray($products->toArray());
            });
        })->export('xlsx');

        return Redirect::back()->with('error', 'No se encontraron productos para exportar.');
    }

    /**
     * Genera un archivo CSV con las cabeceras necesarias
     * para poder un listado de productos al catalogo
     * de productos mostrado en Facebook.
     *
     * @param Store $store
     * @return Illuminate\Http\JsonResponse
     */
    public function download_make_facebook_feed(Store $store)
    {
        $producs_feed_closure = function ($query) {
            $query->select('id', 'slug', 'name');
        };
        $products = $store->products()
            ->active()
            ->with([
                'shelf' => $producs_feed_closure,
                'department' => $producs_feed_closure,
                'brand'
            ])
            ->get();
        if (count($products) === 0) {
            return Response::json(['message' => 'No hay productos disponibles.'], 404);
        }

        $memory = fopen('php://memory', 'w');
        $headers = [
            'id', 'title', 'description', 'link', 'image_link', 'availability',
            'price', 'brand', 'condition',
            // Tienda
            'custom_label_0', 'sale_price',
            // Descuento
            'custom_label_1', 'google_product_category', 'product_type',
            // Tipo de cantidades del producto eg. 1 gr
            'custom_label_2',
            // Unidades en descuento
            'custom_label_3',
        ];
        fputcsv($memory, $headers);
        foreach ($products as $product) {
            fputcsv($memory, [
                $product->id,
                $product->name,
                $product->name,
                action('StoreController@single_product', [
                    'city_slug' => $store->city->slug,
                    'store_slug' => $store->slug,
                    'department_slug' => $product->pivot->department->slug,
                    'shelf_slug' => $product->pivot->shelf->slug,
                    'product_slug' => $product->slug
                ]),
                $product->image_medium_url,
                $product->pivot->current_stock > 0
                    ? 'In stock'
                    : 'Out of stock',
                $product->pivot->price,
                $product->brand->name,
                'New',
                $store->name,
                $product->pivot->special_price
                    ? $product->pivot->special_price
                    : $product->pivot->price,
                // Porcentaje de descuento
                $product->pivot->discount * 100,
                $product->pivot->department->name,
                $product->pivot->shelf->name,
                "{$product->quantity} {$product->unit}",
                $product->pivot->quantity_special_price
            ]);
        }
        $url = upload_image(
            ['client_original_extension' => 'csv'],
            false,
            Config::get('app.download_directory_temp'),
            $memory
        );

        return Response::json(compact('url'));
    }

    /**
     * Genera el archivo con los datos de los productos, este
     * es utilizado por elastic search.
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function download_elastic_search_json_products()
    {
        $json_text = '';
        $products = Product::elasticSearchProducts();
        foreach ($products as $product) {
            $json_text .= json_encode([
                'index' => [
                    '_index' => 'catalog',
                    '_type' => 'product',
                    '_id' => $product->id
                ]
            ]);
            $json_text .= "\n";
            $json_text .= json_encode($product);
            $json_text .= "\n";
        }

        $url = upload_image(
            ['client_original_extension' => 'json'],
            false,
            Config::get('app.download_directory_temp'),
            $json_text
        );

        return Response::json(compact('url'));
    }

    /**
     * Muestra el formulario correspondiente al "store_product_warehouses".
     *
     * @param int $store_product_id
     * @param int $warehouse_id
     * @return mixed
     */
    public function get_store_product_warehouse_form_ajax($store_product_id, $warehouse_id)
    {
        $store_product = \StoreProduct::findOrFail($store_product_id);
        $warehouse = \Warehouse::findOrFail($warehouse_id);
        $boolean_field_values = ['No', 'Sí'];
        $states = ['Inactivo', 'Activo'];
        $store_product_warehouse = \StoreProductWarehouse::firstOrNew([
            'store_product_id' => $store_product_id,
            'warehouse_id' => $warehouse_id
        ]);

        return View::make(
            'admin.products.store_product_warehouse',
            compact('store_product_warehouse', 'store_product', 'warehouse', 'boolean_field_values', 'states')
        );
    }

    /**
     * Almacena un "store_product_warehouses"
     *
     * @return mixed
     */
    public function save_store_product_warehouse_ajax()
    {
        $rules = [
            'id' => 'sometimes|required|numeric|digits_between:1,10|exists:store_product_warehouses',
            'warehouse_id' => 'required|numeric|digits_between:1,10|exists:warehouses,id',
            'store_product_id' => 'required|numeric|digits_between:1,10|exists:store_products,id',
            'manage_stock' => 'required|boolean',
            'minimum_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
            'ideal_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
            'minimum_picking_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
            'maximum_picking_stock' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
            'storage_position' => 'required_if:manage_stock,1|numeric|digits_between:1,10',
            'storage_height_position' => 'required_if:manage_stock,1|string|max:5',
            'discontinued' => 'required|boolean'
        ];

        if ($this->admin_permissions['permission1']) {
            $rules['is_visible_stock'] = 'required|boolean';
            $rules['status'] = 'required|boolean';
        }

        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 422);
        }

        $store_product_warehouse = \StoreProductWarehouse::findOrNew(\Input::get('id') ?: null);
        $store_product_warehouse->warehouse()->associate(\Warehouse::find(\Input::get('warehouse_id')));
        $store_product_warehouse->storeProduct()->associate(\StoreProduct::find(\Input::get('store_product_id')));
        $store_product_warehouse->manage_stock = Input::get('manage_stock');
        $store_product_warehouse->is_visible = Input::get('is_visible');
        $is_marketplace = !empty($store_product_warehouse->storeProduct->allied_store_id);
        if ($this->admin_permissions['permission1']) {
            $store_product_warehouse->is_visible_stock = Input::get('is_visible_stock');
            $store_product_warehouse->status = Input::get('status');
        } elseif (!$store_product_warehouse->exists && $is_marketplace) {
            $store_product_warehouse->is_visible_stock = 1;
        }
        if (!$this->admin_permissions['permission1'] && !$store_product_warehouse->exists){
            $store_product_warehouse->status = 1;
        }
        $store_product_warehouse->storage_position = Input::get('storage_position');
        $store_product_warehouse->storage_height_position = Input::get('storage_height_position');
        $store_product_warehouse->minimum_picking_stock = Input::get('minimum_picking_stock');
        $store_product_warehouse->ideal_stock = Input::get('ideal_stock');
        $store_product_warehouse->maximum_picking_stock = Input::get('maximum_picking_stock');
        $store_product_warehouse->minimum_stock = Input::get('minimum_stock');
        $store_product_warehouse->discontinued = Input::get('discontinued');

        if (!empty($store_product_warehouse->manage_stock)) {
            $result = \WarehouseStorage::validatePosition(
                $store_product_warehouse->warehouse->id,
                'picking',
                $store_product_warehouse->storeProduct->storage === 'Seco' ? 'Seco' : 'Frío',
                $store_product_warehouse->storage_position,
                $store_product_warehouse->storage_height_position
            );
            if (!$result['status']) {
                return Response::json(['Posición' => $result['message']], 422);
            }
        }

        $store_product_warehouse->save();
        $this->admin_log('store_product_warehouses', $store_product_warehouse->id, 'update');

        return Response::json([
            'status' => true,
            'message' => 'Datos guardados correctamente.'
        ]);
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function save_product_detail_ajax($product_id)
    {
        $validator = \Validator::make(\Input::all(), [
            'type' => 'required|string|max:50',
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:1000'
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => false,
                'message' => 'Error en los campos',
                'details' => \Config::get('debug') ? $validator->errors() : new \StdClass()
            ]);
        }

        $product = \Product::findOrFail($product_id);
        $detail = \ProductDetail::findOrNew(\Input::get('id') ?: 0);
        $detail->type = \Input::get('type');
        $detail->name = \Input::get('name');
        $detail->description = \Input::get('description');
        $product->details()->save($detail);

        return \Redirect::back()->with('message', 'Detalle agregado.');
    }

    /**
     * @param $product_detail_id
     * @return mixed
     * @throws \Exception
     */
    public function delete_product_detail_ajax($product_detail_id)
    {
        $detail = \ProductDetail::findOrFail($product_detail_id);
        $detail->delete();

        return Redirect::back()->with('message', 'Detalle eliminado.');
    }

    /**
     * @param $id
     */
    public function delete_suggested_product_applyed_to_shelf($id) {
        $shelf_suggested_product = ShelvesSuggestedProducts::findOrFail($id);
        $shelf_suggested_product->delete();
        return Redirect::back()->with('type', 'success')->with('message', 'Registro eliminado.');
    }

    public function get_store_product_info_copy()
    {
        $tmp_store_id = $store_id = Input::get('store_id');
        if ($store_id == 64) {
            $store_id = 63;
        }else{
            $store_id = 64;
        }
        $product_id = Input::get('product_id');

        $store_product = StoreProduct::where('store_id', $store_id)->where('product_id', $product_id)->first();

        if (!empty($store_product)) {
            $tmp_departments = Department::find($store_product->department_id);
            $departments = Department::where('store_id', $tmp_store_id)->where('name', $tmp_departments->name)->first();

            $tmp_shelves = Shelf::find($store_product->shelf_id);
            $shelves = Shelf::where('store_id', $tmp_store_id)->where('name', $tmp_shelves->name)->first();

            $data['departments'] = $departments;
            $data['shelves'] = $shelves;
            $data['store_product'] = $store_product;

            return Response::json($data);
        }

        return Response::json(['error' => 'No se encontró información para ser duplicada.']);
    }
}
