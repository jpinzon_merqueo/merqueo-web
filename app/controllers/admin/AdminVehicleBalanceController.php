<?php

namespace admin;

use Request, Input, View, Session, Redirect, Response, DB, City, Driver, VehicleMovement, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007, OrderStore, Transporter, Vehicle, Config;

class AdminVehicleBalanceController extends AdminController
{
    /**
     * Grilla de conductores
     */
    public function index()
    {
        $city = City::find(Session::get('admin_city_id'));

        if (!Request::ajax()) {
            return View::make('admin.vehicles.index')
                ->with('title', 'Administrar Saldos de Vehiculos')
                ->with('cities', $this->get_cities());
        } else {

            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $vehicles = Transporter::join('vehicles', 'transporters.id', '=', 'vehicles.transporter_id')
                ->join('cities', 'cities.id', '=', 'transporters.city_id')
                ->leftJoin('vehicle_movements', 'vehicles.id', '=', 'vehicle_movements.vehicle_id')
                ->where(function ($query) use ($search) {
                    $query->where('transporters.fullname', 'LIKE', '%' . $search . '%');
                    $query->orWhere('transporters.document_number', 'LIKE', '%' . $search . '%');
                    $query->orWhere('transporters.phone', 'LIKE', '%' . $search . '%');
                    $query->orWhere('vehicles.plate', 'LIKE', '%' . $search . '%');
                    $query->orWhere('vehicle_movements.reference', $search);

                })
                ->where('transporters.status', 1)
                ->where('transporters.city_id', $city_id == null ? $city->id : $city_id)
                ->select('vehicles.id', 'transporters.fullname', 'transporters.document_number', 'vehicles.plate', 'transporters.phone', 'city', DB::Raw("IFNULL(vehicle_movements.reference, '' ) AS reference"))
                ->groupBy('vehicles.id');

            //debug($vehicles->toSql());
            if (Session::get('admin_designation') != 'Super Admin')
                $vehicles->where('transporters.city_id', Session::get('admin_city_id'));

            $vehicles = $vehicles->orderBy('transporters.fullname', 'asc')->limit(200)->get();

            return View::make('admin.vehicles.index')
                ->with('title', 'vehículos ')
                ->with('vehicles', $vehicles)
                ->with('cities', $this->get_cities())
                ->renderSections()['content'];
        }
    }


    /**
     * Administrar saldos de conductor
     */
    public function edit($id)
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $vehicle = Transporter::join('vehicles', 'transporters.id', '=', 'vehicles.transporter_id')
            ->join('cities', 'cities.id', '=', 'transporters.city_id')
            ->where('transporters.status', 1)
            ->where('vehicles.id', $id)
            ->select('vehicles.id', 'transporters.fullname', 'transporters.document_number', 'vehicles.plate', 'transporters.phone', 'city')->first();


        if (!$vehicle)
            return Redirect::back()->with('type', 'failed')->with('message', 'El vehiculo no existe.');

        $movements = VehicleMovement::select('vehicle_movements.*',
            DB::raw("IF(vehicle_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
            DB::raw("DATE_FORMAT(vehicle_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"),
            'orders.payment_method AS user_payment_method',
            DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"),
            DB::raw("SUM(total_real_amount) AS total_real_amount"), 'shopper_payment_method', 'supermarket')
            ->leftJoin('admin', 'vehicle_movements.admin_id', '=', 'admin.id')
            ->leftJoin('orders', 'vehicle_movements.order_id', '=', 'orders.id')
            ->leftJoin('order_stores', function ($join) {
                $join->on('order_stores.order_id', '=', 'orders.id');
                $join->on('vehicle_movements.status', '=', DB::raw('1'));
            })
            ->join('drivers', 'vehicle_movements.driver_id', '=', 'drivers.id')
            ->where('vehicle_movements.vehicle_id', $vehicle->id)
            ->groupBy('vehicle_movements.id')->groupBy('vehicle_movements.order_id')->groupBy('vehicle_movements.status')
            ->orderBy('vehicle_movements.created_at', 'DESC')
            ->offset(0)
            ->limit(500)
            ->get();

        foreach ($movements as $movement) {
            $order_stores = OrderStore::select('order_stores.shopper_payment_method', 'stores.name AS store_name')
                ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $movement->order_id)->get();
            if (count($order_stores) > 1) {
                $shopper_payment_method = '';
                foreach ($order_stores as $order_store) {
                    $shopper_payment_method .= $order_store->store_name . ': ' . $order_store->shopper_payment_method . ' ';
                }
                $movement->shopper_payment_method = trim($shopper_payment_method);
            } else {
                if (count($order_stores) == 1) {
                    $order_store = $order_stores->toArray();
                    $movement->shopper_payment_method = $order_store[0]['shopper_payment_method'];
                }
            }
        }

        $movements_html = View::make('admin.vehicles.movements')->with('movements', $movements)->render();

        $balance = Vehicle::getBalance($vehicle->id);

        return View::make('admin.vehicles.form')
            ->with('title', 'Administrar Saldo de Vehiculo')
            ->with('vehicle', $vehicle)
            ->with('balance', $balance)
            ->with('movements_html', $movements_html);
    }

    private function validate_unique_reference($reference)
    {
        $reference = VehicleMovement::select(DB::raw('COUNT(*) as cant'))
            ->where('reference', $reference)
            ->where('status', 1)
            ->pluck('cant');
        if ($reference > 0)
            return false;

        return true;

    }

    /**
     * Guardar movimiento de tarjeta a shopper
     */
    public function save_movement_ajax()
    {
        $vehicle = Vehicle::find(Input::get('vehicle_id'));
        if (!$vehicle)
            return Response::json(array('status' => false, 'message' => 'Conductor no existe'), 200);

        if (!$this->validate_unique_reference(Input::get('reference')))
            return Response::json(array('status' => false, 'message' => 'La referencia ' . Input::get('reference') . ' se encuentra asociada a otro movimiento'), 200);

        if (!Input::has('id'))
            $vehicle_movement = new VehicleMovement;

        $vehicle_drivers = \VehicleDrivers::where('vehicle_id',$vehicle->id)->first();

        $vehicle_movement->admin_id = Session::get('admin_id');
        $vehicle_movement->vehicle_id = $vehicle->id;
        $vehicle_movement->driver_id = $vehicle_drivers->driver_id;
        $vehicle_movement->movement = Input::get('movement');
        $vehicle_movement->reference = Input::get('reference');
        $amount = intval(str_replace('.', '', Input::get('amount')));
        if ($vehicle_movement->movement == 'Deducción')
            $vehicle_movement->driver_balance = $amount;
        $vehicle_movement->description = Input::get('description');
        $vehicle_movement->save();
        $this->admin_log('vehicle_movements', $vehicle_movement->id, 'insert');

        $movements = VehicleMovement::select('vehicle_movements.*',
            DB::raw("IF(vehicle_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
            DB::raw("DATE_FORMAT(vehicle_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"),
            'orders.payment_method AS user_payment_method',
            DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"),
            DB::raw("SUM(total_real_amount) AS total_real_amount"), 'shopper_payment_method', 'supermarket')
            ->leftJoin('admin', 'vehicle_movements.admin_id', '=', 'admin.id')
            ->leftJoin('orders', 'vehicle_movements.order_id', '=', 'orders.id')
            ->leftJoin('order_stores', function ($join) {
                $join->on('order_stores.order_id', '=', 'orders.id');
                $join->on('vehicle_movements.status', '=', DB::raw('1'));
            })
            ->join('drivers', 'vehicle_movements.driver_id', '=', 'drivers.id')
            ->where('vehicle_movements.vehicle_id', $vehicle->id)
            ->groupBy('vehicle_movements.id')->groupBy('vehicle_movements.order_id')->groupBy('vehicle_movements.status')->orderBy('vehicle_movements.created_at', 'desc')
            ->get();

        foreach ($movements as $movement) {
            $order_stores = OrderStore::select('order_stores.shopper_payment_method', 'stores.name AS store_name')
                ->join('stores', 'stores.id', '=', 'order_stores.store_id')
                ->where('order_id', $movement->order_id)
                ->get();

            if (count($order_stores) > 1) {
                $driver_payment_method = '';
                foreach ($order_stores as $order_store) {
                    $driver_payment_method .= $order_store->store_name . ': ' . $order_store->shopper_payment_method . ' ';
                }
                $movement->shopper_payment_method = trim($driver_payment_method);
            } else {
                if (count($order_stores) == 1) {
                    $order_store = $order_stores->toArray();
                    $movement->shopper_payment_method = $order_store[0]['shopper_payment_method'];
                }
            }
        }

        $balance = Vehicle::getBalance($vehicle->id);

        $response = array(
            'status' => true,
            'message' => 'Movimiento registrado con éxito.',
            'balance' => $balance,
            'html' => View::make('admin.vehicles.movements')->with('movements', $movements)->render()
        );

        return Response::json($response, 200);
    }

    /**
     * Elimina movimiento de vehiculo
     */
    public function delete_movement_ajax()
    {
        $vehicle = Vehicle::find(Input::get('vehicle_id'));
        if (!$vehicle)
            return Response::json(array('status' => false, 'message' => 'Vehiculo no existe'), 200);

        if (!Input::has('id'))
            return Response::json(array('status' => false, 'message' => 'ID es requerido'), 200);

        if (Input::get('movement') == 'Gestión de pedido') {
            $vehicle_movement = VehicleMovement::where('vehicle_id', $vehicle->id)
                ->where('order_id', Input::get('order_id'))
                ->where('movement', '=', 'Gestión de pedido')
                ->count();

            if ($vehicle_movement && $vehicle_movement > 1) {
                $vehicle_movement = VehicleMovement::find(Input::get('id'));
                $vehicle_movement->status = 0;
                $vehicle_movement->save();
                $this->admin_log('vehicle_movements', Input::get('id'), 'delete');
            }
        } else {
            if (VehicleMovement::where('id', Input::get('id'))->where('vehicle_id', $vehicle->id)->where('movement', '<>', 'Gestión de pedido')->update(array('status' => 0)))
                $this->admin_log('vehicle_movements', Input::get('id'), 'delete');
        }

        $movements = VehicleMovement::select('vehicle_movements.*',
            DB::raw("IF(vehicle_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname) AS user"),
            DB::raw("DATE_FORMAT(vehicle_movements.created_at, '%d/%m/%Y %h:%i %p') AS date"),
            'orders.payment_method AS user_payment_method',
            DB::raw("(orders.total_amount + orders.delivery_amount - orders.discount_amount) AS order_total_amount"),
            DB::raw("SUM(total_real_amount) AS total_real_amount"), 'shopper_payment_method', 'supermarket')
            ->leftJoin('admin', 'vehicle_movements.admin_id', '=', 'admin.id')
            ->leftJoin('orders', 'vehicle_movements.order_id', '=', 'orders.id')
            ->leftJoin('order_stores', function ($join) {
                $join->on('order_stores.order_id', '=', 'orders.id');
                $join->on('vehicle_movements.status', '=', DB::raw('1'));
            })
            ->join('drivers', 'vehicle_movements.driver_id', '=', 'drivers.id')
            ->where('vehicle_movements.vehicle_id', $vehicle->id)
            ->groupBy('vehicle_movements.id')->groupBy('vehicle_movements.order_id')->groupBy('vehicle_movements.status')->orderBy('vehicle_movements.created_at', 'desc')
            ->get();

        foreach ($movements as $movement) {
            $order_stores = OrderStore::select('order_stores.shopper_payment_method', 'stores.name AS store_name')
                ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $movement->order_id)->get();
            if (count($order_stores) > 1) {
                $driver_payment_method = '';
                foreach ($order_stores as $order_store) {
                    $driver_payment_method .= $order_store->store_name . ': ' . $order_store->shopper_payment_method . ' ';
                }
                $movement->shopper_payment_method = trim($driver_payment_method);
            } else {
                if (count($order_stores) == 1) {
                    $order_store = $order_stores->toArray();
                    $movement->shopper_payment_method = $order_store[0]['shopper_payment_method'];
                }
            }
        }

        $balance = Vehicle::getBalance($vehicle->id);

        $response = array(
            'status' => true,
            'message' => 'Movimiento eliminado con éxito.',
            'balance' => $balance,
            'html' => View::make('admin.vehicles.movements')->with('movements', $movements)->render()
        );

        return Response::json($response, 200);
    }

    /**
     * Exportar a excel saldos de vehiculo
     */
    public function export_movement_ajax()
    {
        $vehicle = Vehicle::find(Input::get('vehicle_id'));

        $response = array(
            'status' => false,
            'message' => 'no cuenta con movimientos.',
            'file_url' => ''
        );

        if (!$vehicle)
            return Response::json(array('status' => false, 'message' => 'Vehiculo no existe'), 200);

        $movements = VehicleMovement::select(
            DB::raw("vehicle_movements.movement AS 'Tipo movimiento'"),
            DB::raw(" IF(vehicle_movements.order_id IS NOT NULL, vehicle_movements.order_id,IF(order_stores.order_id IS NOT NULL ,order_stores.order_id, orders.id)) AS Pedido"),
            DB::raw(" order_stores.supermarket AS 'Comprado en'"),
            DB::raw(" total_real_amount AS 'Total factura'"),
            DB::raw(" vehicle_movements.driver_balance AS 'Saldo vehiculo'"),
            DB::raw(" vehicle_movements.description AS 'Observacion'"),
            DB::raw(" vehicle_movements.created_at AS Fecha"),
            DB::raw("IF(order_stores.admin_id IS NULL, IF(vehicle_movements.admin_id IS NULL, CONCAT(first_name, ' ', last_name), admin.fullname), admin.fullname) AS 'Hecho por'"),
            DB::raw(" IF(vehicle_movements.status = 1,'Activo','Inactivo') AS Estado"))
            ->leftJoin('admin', 'admin_id', '=', 'admin.id')
            ->leftjoin('orders', 'order_id', '=', 'orders.id')
            ->leftjoin('order_stores', 'order_stores.order_id', '=', 'orders.id')
            ->join('drivers', 'vehicle_movements.driver_id', '=', 'drivers.id')
            ->where('vehicle_movements.vehicle_id', $vehicle->id)
            ->groupBy('vehicle_movements.id', 'vehicle_movements.order_id', 'vehicle_movements.status')
            ->orderBy('vehicle_movements.created_at', 'desc')
            ->get()->toArray();

        if ($n = count($movements)) {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)) {
                while (false !== ($file = readdir($gestor))) {
                    if (strstr($file, Session::get('admin_username')))
                        remove_file($path . $file);
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Movimientos');
            $objPHPExcel->getProperties()->setSubject('Movimientos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb' => 'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:AR1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($movements[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach ($movements as $row) {
                $i = 0;
                foreach ($row as $key => $value) {
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    $i++;
                }
                $j++;
            }

            //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Movimientos');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username') . ' - ' . date('Y-m-d H.i.s', time()) . '.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
            $url = web_url() . '/' . Config::get('app.download_directory') . $filename;

            $response = array(
                'status' => true,
                'message' => 'Movimientos exportados.',
                'file_url' => $url
            );
        }
        return Response::json($response, 200);
    }

    /**
     * Exportar a excel saldos de shopper
     */
    public function export_balance_ajax()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $balances = VehicleMovement::select(
            DB::raw("transporters.document_number AS 'DOCUMENTO  TRANSPORTADOR' ,
                                     transporters.fullname AS TRANSPORTADOR,
                                     CONCAT(first_name,' ',last_name) AS CONDUCTOR,
                                     drivers.document_number AS 'DOCUMENTO CONDUCTOR',
                                     vehicles.plate AS PLACA,
                                     SUM(vehicle_movements.driver_balance) AS 'SALDO CONDUCTOR'"
            )
        )
            ->join('vehicles', 'vehicle_movements.vehicle_id', '=', 'vehicles.id')
            ->join('vehicle_drivers', 'vehicle_drivers.vehicle_id', '=', 'vehicles.id')
            ->join('drivers', 'drivers.id', '=', 'vehicle_drivers.driver_id')
            ->join('transporters', 'transporters.id', '=', 'vehicles.transporter_id')
            ->where('vehicle_movements.status', 1)
            ->orderBy('CONDUCTOR', 'ASC')
            ->groupBy('vehicles.id','drivers.id')
            ->get()
            ->toArray();

        if ($n = count($balances)) {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)) {
                while (false !== ($file = readdir($gestor))) {
                    if (strstr($file, Session::get('admin_username')))
                        remove_file($path . $file);
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Movimientos');
            $objPHPExcel->getProperties()->setSubject('Movimientos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb' => 'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($balances[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach ($balances as $row) {
                $i = 0;
                foreach ($row as $key => $value) {
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    $i++;
                }
                $j++;
            }

            //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Saldos de Transportadores');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username') . ' - ' . date('Y-m-d H.i.s', time()) . '.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')) . $filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        }

        $response = array(
            'status' => true,
            'message' => 'Saldos exportados.',
            'file_url' => $url
        );

        return Response::json($response, 200);
    }

}
