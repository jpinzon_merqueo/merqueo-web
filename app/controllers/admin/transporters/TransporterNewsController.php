<?php

namespace admin\transporters;

use Auth;
use Config;
use View;
use Input;
use Redirect;
use Request;
use Response;
use Session;
use Validator;
use DB;
use admin\AdminController;
use TransporterNew;
use PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007;


class TransporterNewsController extends AdminController {

    private $autorized_users = [];

    /**
     * TransporterNewsController constructor.
     */
    public function __construct()
    {
        $configuration = \Configuration::where('type', 'transporter_new_users_config')->first();
        if($configuration){
            $this->autorized_users = json_decode($configuration->label, true);
        }
        parent::__construct();
    }

    /**
     * Grilla de novedades por ciudad
     *
     * @return mixed
     */
    public function index() {

        $input = Input::all();
        $city  = isset($input['city_id']) ? $input['city_id'] : Session::get('admin_city_id');
        $plate = isset($input['plate']) ? $input['plate'] : '';
        $cities_object = $this->get_cities();
        $cities = [];
        foreach ($cities_object as $city_object) {
            $cities[$city_object->id] = $city_object;
        }

        if (!Request::ajax()) {
            return View::make('admin.transporters.transporter_news.index')
                ->with('title', 'Novedades de transportador')
                ->with('cities', $cities)
                ->with('city', $city);
        }else{
            //noveades sinb asignar
            $trasnporter_news_unasigned = TransporterNew::select('transporter_news.*' )
                ->join('orders', 'orders.id', '=', 'transporter_news.order_id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('warehouses', 'order_groups.warehouse_id', '=', 'warehouses.id')
                ->join('vehicles', 'vehicles.id', '=', 'transporter_news.vehicle_id')
                ->where('transporter_news.status', 'Sin Asignar')
                ->where('warehouses.city_id', $city);
            if(!empty($plate)){
                if(is_numeric($plate)){
                    $trasnporter_news_unasigned->where('transporter_news.order_id',$plate);
                }else{
                    $trasnporter_news_unasigned->where('vehicles.plate', $plate);
                }
            }
            $trasnporter_news_unasigned = $trasnporter_news_unasigned->get();

            //novedades asignadas
            $trasnporter_news_asigned = TransporterNew::select('transporter_news.*' )
                ->join('orders', 'orders.id', '=', 'transporter_news.order_id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('warehouses', 'order_groups.warehouse_id', '=', 'warehouses.id')
                ->join('vehicles', 'vehicles.id', '=', 'transporter_news.vehicle_id')
                ->where('transporter_news.status', 'Pendiente')
                ->where('transporter_news.admin_id', \Session::get('admin_id'))
                ->where('warehouses.city_id', $city);
            if(!empty($plate)){
                if(is_numeric($plate)){
                    $trasnporter_news_asigned->where('transporter_news.order_id',$plate);
                }else{
                    $trasnporter_news_asigned->where('vehicles.plate', $plate);
                }
            }
            $trasnporter_news_asigned = $trasnporter_news_asigned->get();

            //novedades resueltas
            $trasnporter_news_resolved = TransporterNew::select('transporter_news.*' )
                ->join('orders', 'orders.id', '=', 'transporter_news.order_id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('warehouses', 'order_groups.warehouse_id', '=', 'warehouses.id')
                ->join('vehicles', 'vehicles.id', '=', 'transporter_news.vehicle_id')
                ->whereIn('transporter_news.status', ['Resuelto', 'Notificado'])
                ->where('transporter_news.admin_id', \Session::get('admin_id'))
                ->where('warehouses.city_id', $city);
            if(!empty($plate)){
                if(is_numeric($plate)){
                    $trasnporter_news_resolved->where('transporter_news.order_id',$plate);
                }else{
                    $trasnporter_news_resolved->where('vehicles.plate', $plate);
                }
            }
            $trasnporter_news_resolved = $trasnporter_news_resolved->get();

            return Response::json([
                'status' => true,
                'result' =>[
                    'unasigned' => View::make('admin.transporters.transporter_news.index')
                        ->with('status', 'unasigned')
                        ->with('news', $trasnporter_news_unasigned)
                        ->renderSections()['content'],
                    'asigned' => View::make('admin.transporters.transporter_news.index')
                        ->with('status', 'asigned')
                        ->with('news', $trasnporter_news_asigned)
                        ->renderSections()['content'],
                    'resolved' => View::make('admin.transporters.transporter_news.index')
                        ->with('status', 'resolved')
                        ->with('news', $trasnporter_news_resolved)
                        ->renderSections()['content']
                ]

            ]);
        }



    }

    /**
     * @return mixed
     */
    public function get_users(){
        try{
            if(empty($this->autorized_users)){
                throw new \Exception('Debe primero realizar la configuración de los usuarios.');
            }
            $users = \Admin::whereIn('id', $this->autorized_users )->get();
            return Response::json([
                'status' => true,
                'result' =>[
                    'users' => View::make('admin.transporters.transporter_news.index')
                        ->with('title', 'users ')
                        ->with('users', $users)
                        ->with('id_new', Input::get('id_new'))
                        ->renderSections()['list_user']
                ]

            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => false,
                'message' => 'Ocurrió un error al traer los usuarios. <br>'.$e->getMessage()
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function save_assignment(){
        try{
            DB::beginTransaction();
            $transporter_new = TransporterNew::find(Input::get('id_new'));
            $transporter_new->admin_id = Input::get('admin_id');
            $transporter_new->status = 'Pendiente';
            $transporter_new->save();
            DB::commit();
            return Response::json([
                'status' => true,
                'message' => 'La asignación se guardó con éxito.'
            ]);
        }catch(\Exception $e){
            return Response::json([
                'status' => false,
                'message' => 'Ocurrió un error al guardar la asignación. <br>'.$e->getMessage()
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function config_get_users(){

        try{
            if(!$this->admin_permissions['permission1']){
                throw new \Exception('No tienes los permisos suficientes para configurar los usuarios.');
            }
            switch (Input::get('type')){
                case 'get_users':
                    $users = \Admin::orderBy('fullname', 'ASC')->get();

                    return Response::json([
                        'status' => true,
                        'result' =>[
                            'users' => View::make('admin.transporters.transporter_news.config')
                                ->with('title', 'users ')
                                ->with('users', $users)
                                ->with('autorized_users', $this->autorized_users)
                                ->renderSections()['list_user']
                            ]
                    ]);
                    break;

                case 'save_config':
                    $configuration = \Configuration::where('type', 'transporter_new_users_config')->first();
                    if(!$configuration){
                        $configuration = new \Configuration;
                        $configuration->type = 'transporter_new_users_config';
                        $configuration->warehouse_id = 0;
                        $configuration->city_id = 0;
                        $configuration->value = 0;
                        $configuration->label = json_encode(Input::get('admin_id'));
                        $configuration->save();
                    }else{
                        $configuration->label = json_encode(Input::get('admin_id'));
                        $configuration->save();
                    }
                    return Response::json([
                        'status' => true,
                        'message' => 'Configuración guadada con éxito.'
                    ]);
                    break;
            }
        }catch(\Exception $e){
            return Response::json([
                'status' => false,
                'message' => 'Ocurrió un error al traer los usuarios. <br>'.$e->getMessage()
            ]);
        }
    }


    /**
     * @return mixed
     */
    public function get_form_resolv(){
        if(Request::ajax()){
            $trasnporter_new = TransporterNew::find(Input::get('id_new'));
            return Response::json([
                'status' => true,
                'result' => View::make('admin.transporters.transporter_news.index')
                        ->with('title', 'form')
                        ->with('transporter_new', $trasnporter_new)
                        ->renderSections()['resolv-form']
            ]);
        }
    }


    /**
     * @return mixed
     */
    public function save_form_resolv(){
        if(Request::ajax()){
            try{
                DB::beginTransaction();
                $transporter_new = TransporterNew::find(Input::get('id_new'));
                $transporter_new->status = 'Resuelto';
                $resolution_message['message'] = Input::get('resolution_message');
                if(!empty(Input::get('resolution_url'))){
                    $resolution_message['url']=  Input::get('resolution_url');
                }
                $transporter_new->resolution_message = json_encode($resolution_message);
                $transporter_new->resolve_date = date('Y-m-d H:i:s');
                $transporter_new->save();
                DB::commit();
                return Response::json([
                    'status' => true,
                    'message' => 'Se actualizo la novedad con éxito.'
                ]);
            }catch(\Exception $e){
                DB::rollback();
                return Response::json([
                    'status' => false,
                    'message' => 'Ocurrio un error al actualizar la novedad. <br>'.$e->getMessage()
                ]);
            }
        }
    }

    public function report_ajax() {
        try {
            return $this->generate_report();
        } catch (Exception $e) {
            return Response::json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }

    protected function generate_report() {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        //eliminar archivo
        $path = public_path(Config::get('app.download_directory'));
        if ($gestor = opendir($path)){
            while (false !== ($file = readdir($gestor))){
                if (strstr($file, Session::get('admin_username')))
                    remove_file($path.$file);
            }
        }
        $objPHPExcel = new PHPExcel();
        //propiedades
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setTitle('Novedades Transportador');
        $objPHPExcel->getProperties()->setSubject('Novedades Transportador');

        $objPHPExcel->setActiveSheetIndex(0);
        //estilos
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'5178A5'),
            ),
            'font' => array(
                'color' => array('rgb'=>'FFFFFF'),
                'bold' => true,
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($style_header);
        $headers = ['ID', 'Ciudad', 'Tipo novedad', 'Pedido', 'Producto', 'Cant. productos', 'Transportador', 'Conductor', 'Vehiculo', 'Usuario', 'Estado', 'Fecha de creación', 'Razón', 'Comentarios de la novedad', 'Solución', 'URL solución', 'Fecha de solución', 'Tiempo de respuesta'];
        $i = 0;
        foreach ($headers as $key) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }

        list( $day, $month, $year ) = explode('/',Input::get('report_init_date'));
        $init_date = $year.'-'.$month.'-'.$day. ' 00:00:00';
        list( $day, $month, $year ) = explode('/',Input::get('report_end_date'));
        $end_date = $year.'-'.$month.'-'.$day.' 23:59:59';
        $transporter_news = TransporterNew::select('transporter_news.*' )
            ->whereBetween('created_at', [$init_date, $end_date])
            ->get();

        $j = 2;
        foreach ($transporter_news as $new) {
            $i = 0;
            $resolve_time = '';
            if($new->status == 'Resuelto'){
                $resolve_time = [];
                $resolve_date = new \DateTime($new->resolve_date);
                $created_date = new \DateTime($new->created_at);
                $dif = $resolve_date->diff($created_date);
                if($dif->y > 0){
                    $resolve_time[] = $dif->y.( $dif->y >1 ?' Años':' Año' );
                }
                if($dif->m > 0){
                    $resolve_time[] = $dif->m.( $dif->m >1 ?' Meses':' Mes' );
                }
                if($dif->d > 0){
                    $resolve_time[] = $dif->d.( $dif->d >1 ?' Días':' Día' );
                }
                if($dif->h > 0){
                    $resolve_time[] = $dif->h.( $dif->h >1 ?' Horas':' Hora' );
                }
                if($dif->i > 0){
                    $resolve_time[] = $dif->i.( $dif->i >1 ?' Minutos':' Minuto' );
                }
                if($dif->s > 0){
                    $resolve_time[] = $dif->s.( $dif->s >1 ?' Segundos':' Segundo' );
                }
                $resolve_time = implode(' ', $resolve_time);
            }
            $resolution_message = json_decode($new->resolution_message, true);
            $values = [
                $new->id,
                $new->city()->city,
                $new->type,
                $new->order_id,
                (!is_null($new->order_product_id))?$new->orderProducts()->product_name:'',
                $new->quantity_returned,
                $new->order->driver->transporter->fullname,
                $new->order->driver->first_name.' '.$new->order->driver->last_name,
                $new->vehicle->plate,
                (!is_null($new->admin_id))?$new->admin->fullname:'',
                $new->status,
                $new->created_at,
                $new->reason,
                $new->reason_comments,
                $resolution_message['message'],
                isset($resolution_message['url'])?$resolution_message['url']:'',
                $new->resolve_date,
                $resolve_time
            ];
            foreach ($values as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                $i++;
            }
            $j++;
        }
        //crear archivo
        $objPHPExcel->getActiveSheet()->setTitle('Novedades de vehículos');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
        $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
        $file['real_path'] = public_path(Config::get('app.download_directory')) . $filename;
        $file['client_original_name'] = $filename;
        $file['client_original_extension'] = 'xlsx';
        $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        return Response::json([
            'status' => 1,
            'message' => 'Novedades exportadas.',
            'file_url' => $url
        ]);
    }
}
