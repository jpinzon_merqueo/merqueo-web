<?php

namespace admin\transporters\vehicle_news;

use Auth;
use Config;
use View;
use Input;
use Redirect;
use Request;
use Response;
use Session;
use Validator;
use DB;
use admin\AdminController;
use Carbon\Carbon;
use Vehicle;
use VehicleNew;
use PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Transporter;

class VehicleNewsController extends AdminController {

    /**
     * Grilla de vehículos por ciudad
     */
    public function index() {
        $input = Input::all();
        $city  = isset($input['city_id']) ? $input['city_id'] : Session::get('admin_city_id');
        $plate = isset($input['plate']) ? $input['plate'] : '';
        $cities_object = $this->get_cities();
        $cities = [];
        foreach ($cities_object as $city_object) {
            $cities[$city_object->id] = $city_object;
        }
        $transporters = Transporter::select('id', 'fullname')
            ->whereStatus(1)
            ->where('city_id', $city)
            ->orderBy('fullname')
            ->get();
        if (!Request::ajax()) {
            return View::make('admin.transporters.vehicle_news.index')
                ->with('title', 'Administrar Novedades de los vehículos')
                ->with('cities', $cities)
                ->with('transporters', $transporters)
                ->with('city', $city);
        }
        $vehicles = Vehicle::select('vehicles.*')
            ->join('transporters', 'transporters.id', '=', 'vehicles.transporter_id')
            ->where('transporters.city_id', '=', $city)
            ->where('transporters.status', '=', 1)
            ->where('vehicles.status', '=', 1);
        if (!empty($plate)) {
            $vehicles = $vehicles->whereRaw("vehicles.plate LIKE '%" . $plate . "%'");
        }
        $vehicles = $vehicles->groupBy('vehicles.id')
            ->orderBy('transporters.fullname', 'asc')
            ->get();
        return View::make('admin.transporters.vehicle_news.index')
            ->with('title', 'vehículos ')
            ->with('vehicles', $vehicles)
            ->renderSections()['content'];
    }

    /**
     * Formulario para agregar y/o eliminar novedades asignadas a un vehículo
     *
     * @param  int $id Id del vehículo
     */
    public function view($id) {
        try {
            $types = [
                'Novedad' => 'Novedad',
                'Bonificación' => 'Bonificación',
                'Descuento' => 'Descuento',
            ];
            $vehicle = Vehicle::findOrFail($id);
            $news = $this->news_ajax($id);
            $news = $news->getData();
            return View::make('admin.transporters.vehicle_news.form')
                ->with('title', 'Novedades para el vehículo: ' . $vehicle->plate)
                ->with('news_html', $news->vehicle_news)
                ->with('values', $news->values)
                ->with('types', $types)
                ->with('vehicle', $vehicle);
        } catch (ModelNotFoundException $e) {
            return Redirect::route('adminVehicleNews.index')
                ->with('type', 'error')
                ->with('message', 'Vehículo no encontrado.');
        }
    }

    /**
     * Método para obtener mediante ajax las novedades de un vehículo en específico, mediante queryString se obtiene el rango de fechas.
     *
     * @param  int $id Id del vehículo
     */
    public function news_ajax($id) {
        $created_at = Request::get('created_at', '');
        $vehicle = Vehicle::find($id);
        $vehicle_news = $vehicle->vehicleNews();
        $news = $vehicle->vehicleNews()
            ->where('type', '=', 'Novedad');
        $bonus = $vehicle->vehicleNews()
            ->where('type', '=', 'Bonificación');
        $discount = $vehicle->vehicleNews()
            ->where('type', '=', 'Descuento');
        if (!empty($created_at)) {
            $range = explode(',', $created_at);
            $from = Carbon::createFromFormat('d-m-Y', $range[0]);
            $to = Carbon::createFromFormat('d-m-Y', $range[1]);
            $vehicle_news = $vehicle_news->whereDate('date', '>=', $from->toDateString())
                ->whereDate('date', '<=', $to->toDateString());
            $news = $news->whereDate('date', '>=', $from->toDateString())
                ->whereDate('date', '<=', $to->toDateString());
            $bonus = $bonus->whereDate('date', '>=', $from->toDateString())
                ->whereDate('date', '<=', $to->toDateString());
            $discount = $discount->whereDate('date', '>=', $from->toDateString())
                ->whereDate('date', '<=', $to->toDateString());
        }
        $vehicle_news = $vehicle_news->orderBy('type')
            ->orderBy('value')
            ->get();
        $html = View::make('admin.transporters.vehicle_news.vehicle_news')
            ->with('vehicle_news', $vehicle_news)
            ->render();
        return Response::json([
            'vehicle_news' => $html,
            'values' => [
                'news' => '$' . number_format($news->sum('value'), 0, ',', '.'),
                'bonus' => '$' . number_format($bonus->sum('value'), 0, ',', '.'),
                'discount' => '$' . number_format($discount->sum('value'), 0, ',', '.'),
            ],
        ]);
    }

    /**
     * Método para agregar una novedad a un vehículo en específico.
     *
     * @param  int $id Id del vehículo
     */
    public function add_news_ajax($id) {
        $input = Input::all();
        $date  = empty($input['date']) ? NULL : Carbon::createFromFormat('d-m-Y', $input['date']);
        if (!is_null($date)) {
            $date->hour   = 0;
            $date->minute = 0;
            $date->second = 0;
            $today = Carbon::today()->timestamp;
            $timestamp_input_date = $date->timestamp;
            if ($timestamp_input_date > $today) {
                return Response::json([
                    'status' => 0,
                    'message' => 'No puedes usar fechas futuras.',
                ]);
            }
        }
        try {
            $admin_id = Session::get('admin_id');
            $vehicle_new = new VehicleNew;
            $vehicle_new->admin_id = $admin_id;
            $vehicle_new->vehicle_id = $id;
            $vehicle_new->type = $input['type'];
            $vehicle_new->date = is_null($date) ? NULL : $date->toDateString();
            $vehicle_new->value = $input['amount'];
            $vehicle_new->comments = empty($input['comments']) ? NULL : $input['comments'];
            $vehicle_new->save();
            return Response::json([
                'status' => 1,
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Método para eliminar una novedad a un vehículo en específico.
     *
     * @param  int $id Id del vehículo
     */
    public function delete_news_ajax($id) {
        try {
            $input = Input::all();
            $vehicle_new = VehicleNew::findOrFail($input['id']);
            $vehicle_new->delete();
            return Response::json([
                'status' => 1,
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Método para generar el reportes de novedades asociadas a un vehículo en específico, mediante queryString se obtiene el rango de fechas.
     *
     * @param  int $id Id del vehículo
     */
    public function report_ajax($id) {
        try {
            $input = Input::all();
            $created_at = Request::get('created_at', '');
            $vehicle = Vehicle::findOrFail($id);
            $vehicle_news = $vehicle->vehicleNews();
            if (!empty($created_at)) {
                $range = explode(',', $created_at);
                $from = Carbon::createFromFormat('d-m-Y', $range[0]);
                $to = Carbon::createFromFormat('d-m-Y', $range[1]);
                $vehicle_news = $vehicle_news->whereDate('date', '>=', $from->toDateString())
                    ->whereDate('date', '<=', $to->toDateString());
            }
            $vehicle_news = $vehicle_news->orderBy('type')
                ->orderBy('value')
                ->get();
            if ($vehicle_news->count()) {
                return $this->generate_report($vehicle_news);
            } else {
                return Response::json([
                    'status' => 0,
                    'message' => 'No se encontraron novadeades asociadas al vehículo.'
                ]);
            }
        } catch (Exception $e) {
            return Response::json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Genera el archivo de reporte.
     *
     * @param  Collection $vehicle_news Colección de registros para exportar.
     * @return JSON Objeto json
     */
    protected function generate_report($vehicle_news) {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        //eliminar archivo
        $path = public_path(Config::get('app.download_directory'));
        if ($gestor = opendir($path)){
            while (false !== ($file = readdir($gestor))){
                if (strstr($file, Session::get('admin_username')))
                    remove_file($path.$file);
            }
        }
        $objPHPExcel = new PHPExcel();
        //propiedades
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setTitle('Novedades');
        $objPHPExcel->getProperties()->setSubject('Novedades');
        $objPHPExcel->setActiveSheetIndex(0);
        //estilos
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'5178A5'),
            ),
            'font' => array(
                'color' => array('rgb'=>'FFFFFF'),
                'bold' => true,
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($style_header);
        $headers = ['VEHÍCULO', 'TIPO', 'MONTO', 'COMENTARIOS', 'FECHA', 'FECHA DE REGISTRO'];
        $i = 0;
        foreach ($headers as $key) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
            $i++;
        }
        $j = 2;
        foreach ($vehicle_news as $vehicle_new) {
            $values = [
                $vehicle_new->vehicle->plate,
                $vehicle_new->type,
                $vehicle_new->value,
                $vehicle_new->comments,
                $vehicle_new->date,
                $vehicle_new->created_at,
            ];
            $i = 0;
            foreach ($values as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                $i++;
            }
            $j++;
        }
        //crear archivo
        $objPHPExcel->getActiveSheet()->setTitle('Novedades de vehículos');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
        $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
        $file['real_path'] = public_path(Config::get('app.download_directory')) . $filename;
        $file['client_original_name'] = $filename;
        $file['client_original_extension'] = 'xlsx';
        $url = upload_image($file, false, Config::get('app.download_directory_temp'));
        return Response::json([
            'status' => 1,
            'message' => 'Novedades exportadas.',
            'file_url' => $url
        ]);
    }

    /**
     * Método para generar el reporte de novedades, mediante queryString se obtiene el rango de fechas, la ciudad y el transportador.
     *
     */
    public function report_extend() {
        $city        = Request::get('city_report', Session::get('admin_city_id'));
        $created_at  = Request::get('created_at', FALSE);
        $transporter = Request::get('transporter_id', FALSE);
        $range = explode(',', $created_at);
        $from = Carbon::createFromFormat('d-m-Y', $range[0]);
        $to = Carbon::createFromFormat('d-m-Y', $range[1]);
        $vehicle_news = VehicleNew::select('vehicle_news.*')
            ->join('vehicles', 'vehicles.id', '=', 'vehicle_news.vehicle_id')
            ->join('transporters', 'transporters.id', '=', 'vehicles.transporter_id')
            ->where('transporters.status', '=', 1)
            ->where('vehicles.status', '=', 1)
            ->whereDate('vehicle_news.created_at', '>=', $from->toDateString())
            ->whereDate('vehicle_news.created_at', '<=', $to->toDateString())
            ->where('transporters.city_id', '=', $city)
            ->where('transporters.id', '=', $transporter)
            ->orderBy('vehicles.plate')
            ->orderBy('vehicle_news.type')
            ->orderBy('vehicle_news.value')
            ->get();
        if ($vehicle_news->count()) {
            $response_json = $this->generate_report($vehicle_news);
            $response_json = $response_json->getData();
            return Redirect::action('adminVehicleNews.index')
                ->with('file_url', $response_json->file_url)
                ->with('type', 'success')
                ->with('message', 'Novedades exportadas.');
        }
        return Redirect::action('adminVehicleNews.index')
            ->with('type', 'error')
            ->with('message', 'No se encontraron novedades aplicando los filtros seleccionados.');
    }



}
