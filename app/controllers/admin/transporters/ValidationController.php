<?php

namespace admin\transporters;
use Carbon\Carbon, admin\AdminController, View, Response, Redirect, Order, Routes, Validator, Illuminate\Support\Facades\Input, OrderLog;
use Illuminate\Support\Facades\Session;

/**
 * Clase para validar los presintos de las ordenes por ruta
 */
class ValidationController extends AdminController
{
    /**
     * Atributo de la clase ValidationController que almacena la instancia del modelo Order
     */
    protected $order;

    /**
     * Atributo de la clase ValidationController que almacena la instancia del modelo OrderLog
     */
    protected $orderLog;

    /**
     * Atributo de la clase ValidationController que almacena la instancia del modelo Routes
     */
    protected $route;

    /**
     * Constructor del controlador ValidationController
     * @param $order Order Instancia del modelo Order
     * @param  $routes Routes Intancia del mnodelo Routes
    */
    public function __construct(Order $order, Routes $routes, OrderLog $order_log) {
        parent::__construct();
        $this->order = $order;
        $this->orderLog = $order_log;
        $this->route = $routes;
    }

    /**
     * Metodo que realiza el flujo para validacion de precintos de las ordenes por ruta
     * @return View index validation
     */
    public function index() {
        $title = "Validación";
        $cities = $this->get_cities();
        $data = ['title' => $title,'cities' => $cities];
        return View::make('admin.transporters.validation.index', $data);
    }

    /**
     * Metodo para obtener por petición ajax, las bodegas por ciudad seleccionada
     * @param city_id int id de la ciudad
     * @return Response json
    */
    public function get_warehouses_by_city_ajax() {
        return Response::json($this->get_warehouses_ajax());
    }

    /**
     * Metodo para obtener por petición ajax, las rutas relacionadas a una bodega seleccionada
     * @param warehouse_id int id de la bodega
     * @return Response json
     */
    public function get_routes_by_warehouse_ajax() {
        $routes = $this->route;
        $routes = $routes::where('warehouse_id', Input::get('warehouse_id'))->whereBetween('planning_date', [Carbon::now()->format('Y-m-d'), Carbon::tomorrow()->format('Y-m-d')])->orderBy('picking_priority')->lists('route', 'id');
        return Response::json($routes);
    }

    /**
     * Metodo para obtener por peticón ajax, las ordenes relacionadas a la ruta seleccionada
     * @param route_id int id de la ruta
     * @return Response
     */
    public  function get_orders_by_routes_ajax() {
        $orders = $this->order;
        $orders = $orders::with('orderGroup')->where('route_id', Input::get('route_id'))->whereStatus('Alistado')->get();
        $data = ['orders' => $orders];
        return View::make('admin.transporters.validation.index', $data)->renderSections()['content_section_orders'];
    }

    /**
     * Metodo para actualizar los precintos y contenedores de la orden
     * @param $order_id int id de la orden
     * @return json
    */
    public function add_seals_to_order ($order_id) {
        $order = $this->order;
        $order = $order::find($order_id);
        $seals = Input::get('seals');
        $containers = Input::get('containers');
        $validator = Validator::make(Input::all(), [
            'containers' => 'required',
            'seals' => 'required'
        ]);
        if ($validator->fails()) {
            return Response::json(['message' => 'Debes diligenciar los campos contenedores y precintos para poder guardar.', 'status' => false]);
        }
        $seals = explode(',', $seals);
        if (count($seals) != ($containers*2)) {
            if ($containers == 0) {
                $message = 'Por favor validar, los contenedores estan en 0.';
            }else{
                $message = 'No se pueden registrar los datos debido a que debiste ingresar ' . ($containers * 2) . ' precintos pero ingresaste ' . count($seals) . '.';
            }
            return Response::json(['message' => $message, 'status' => false]);
        }
        $order->picking_baskets = Input::get('containers');
        $order->seals = Input::get('seals');
        $order->save();
        //log validación de prescintos y contenedores
        $log = $this->orderLog;
        $log->type = 'Precintos y contenedores validados. Seals: ' . Input::get('seals') . '. Picking_baskets: ' . Input::get('containers');
        $log->user_id = Session::get('admin_id');
        $log->order_id = $order->id;
        $log->save();
        return Response::json(['message' => 'Los precintos de la orden fueron actualizados correctamente.', 'status' => true]);
    }
}