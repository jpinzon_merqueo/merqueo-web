<?php

namespace admin\transporters;

use Aws\Common\Facade\Ses;
use phpDocumentor\Reflection\Types\Array_;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use admin\AdminController;
use Request, Input, View, Session, Redirect, Response, Validator, DB, Driver, Vehicle,
    Transporter, Order, Store, City, PHPExcel, PHPExcel_Style_Fill,
    PHPExcel_Writer_Excel2007, Hash, Routes, Config, SecuritySocial, DriverAssistant;

class AssistantController extends AdminController
{

    /**
     * Grilla de axiliares
     */
    public function index()
    {
        $city = City::find(Session::get('admin_city_id'));
        $transporters = Transporter::where('city_id', $city->id)->where('status', 1)->select('id', 'fullname')->get();
        $drivers = Driver::whereHas('transporter', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })->where('status', 1)->orderBy('last_name')->get();

        if (!Request::ajax()) {
            $assistants = DriverAssistant::whereHas('driver.transporter.city', function ($q) use ($city) {
                $q->where('id', $city->id);
            })->get();
            return View::make('admin.transporters.assistants')
                ->with('title', 'Auxiliar')
                ->with('transporters', $transporters)
                ->with('drivers', $drivers)
                ->with('assistants', $assistants)
                ->with('cities', $this->get_cities());
        } else {
            $city_id = Input::get('city_id');
            $search = Input::has('s') ? Input::get('s') : '';
            $transporter_id = Input::has('transporter_id') ? Input::get('transporter_id') : 0;
            $driver_id = Input::has('driver_id') ? Input::get('driver_id') : 0;

            $assistants = DriverAssistant::whereHas('driver.transporter', function ($q) use ($city) {
                $q->where('city_id', $city->id);
            });

            if ($driver_id != 0){
                $assistants = $assistants->where('driver_id',$driver_id);
                }
            if ($transporter_id != 0)
                $assistants = $assistants->whereHas('driver', function ($q) use ($transporter_id) {
                    $q->where('transporter_id', $transporter_id);
                });

            if ($search != '') {
                $assistants = $assistants->where(function ($query) use ($search) {
                    $query->where('first_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $search . '%');
                });
            }
            $assistants = $assistants->orderBy('first_name', 'DESC')->limit(200)->get();
            $transporters = Transporter::where('city_id', $city_id == null ? $city->id : $city_id)->where('status', 1)->select('id', 'fullname')->get();
            $city_id == null ? $city->id : $city_id;
            $drivers = Driver::whereHas('transporter', function ($q) use ($city_id) {
                $q->where('city_id', $city_id);
            })->where('status', 1)->get();

            return View::make('admin.transporters.assistants')
                ->with('title', 'Auxiliar')
                ->with('assistants', $drivers)
                ->with('transporters', $transporters)
                ->with('assistants', $assistants)
                ->with('cities', $this->get_cities())
                ->renderSections()['content'];
        }
    }

    /**
     * Editar axiliares
     */
    public function edit_assistant()
    {
        $id = Request::segment(5) ? Request::segment(5) : 0;
        $sub_title = Request::segment(5) ? 'Editar Auxiliar' : 'Nuevo Auxiliar';

        $assistant = (object)Session::get('post');
        $city_id = Session::get('admin_city_id');
        $drivers = Driver::whereHas('transporter', function ($q) use ($city_id) {
            $q->where('city_id', $city_id);
        })->where('status', 1)->orderBy('last_name')->get();

        $seguritySocial = SecuritySocial::all();
        if (!$id) {

            $transports = Transporter::all();
            $vehicle = Vehicle::all();

        } else {
            $assistant = DriverAssistant::find($id);
            $transports = Transporter::all();
            $vehicle = Vehicle::all();
        }
        return View::make('admin.transporters.assistant_form')
            ->with('title', 'Auxiliares')
            ->with('sub_title', $sub_title)
            ->with('transporters', $transports)
            ->with('drivers', $drivers)
            ->with('assistants', $assistant)
            ->with('seguritySocials', $seguritySocial)
            ->with('vehicle', $vehicle);
    }

    /**
     * Guardar axiliares
     */
    public function save_assistant()
    {
        $id = Input::get('id');
        $assistant = DriverAssistant::find($id);
        if (!$assistant) {
            $assistant = new DriverAssistant;
            $assistant->created_at = date("Y-m-d H:i:s");
            if (DriverAssistant::where('document_number', Input::get('document_number'))->first())
                return Redirect::back()->with('error', 'Ya existe un auxiliar con el mismo número de documento.');
            $action = 'insert';
        } else {
            $action = 'update';
        }

        $assistant->driver_id = Input::get('driver_id');
        $assistant->first_name = Input::get('first_name');
        $assistant->last_name = Input::get('last_name');
        $assistant->document_number = Input::get('document_number');
        $assistant->phone = Input::get('phone');
        $assistant->cellphone = Input::get('cellphone');
        $assistant->has_cap = Input::get('has_cap');
        $assistant->has_jacket = Input::get('has_jacket');
        $assistant->has_tshirt = Input::get('has_tshirt');
        $assistant->eps_id = Input::get('eps_id');
        $assistant->afp_id = Input::get('afp_id');
        $assistant->arl_id = Input::get('arl_id');
        $file = Input::file('photo_url');
        if($file) {
            $dir = 'driver_assistant/photos/' . date('Y-m-d');
            $file_data['real_path'] = $file->getRealPath();
            $file_data['client_original_name'] = $file->getClientOriginalName();
            $file_data['client_original_extension'] = $file->getClientOriginalExtension();
            $assistant->photo_url =  upload_image($file_data, false, $dir);
        }
        $assistant->save();

        $this->admin_log('drivers', $assistant->id, $action);

        return Redirect::route('adminAssistant.assistants')->with('type', 'success')->with('message', 'Auxiliar guardado con éxito.');
    }

    /**
     * Eliminar axiliares
     *
     */
    public function delete_assistant($id)
    {
        $driver = DriverAssistant::find($id);
        DriverAssistant::where('id', $id)->update(array('status' => !$driver->status));
        $this->admin_log('assis', $driver->id, 'delete');

        return Redirect::back()->with('type', 'success')->with('message', 'Auxiliar editado con éxito.');
    }

}
