<?php

namespace admin;

use App, Request, View, Input, Route, Redirect, Session, DB, Config, Response, City, Order, Event, OrderEventHandler, Routes, Zone, RouteType, RouteCostDetail, Carbon\Carbon,OrderGroup, UserAddress, OrderLog, Store, PHPExcel, PHPExcel_Style_Fill, PHPExcel_Writer_Excel2007, Exception, PickingGroup, AlliedStore, Warehouse, DeliveryWindow, Excel, Validator;
use App\Models\RoutesLog;
use Configuration;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class AdminRouteController extends AdminController
{
    private $shift_delivery_windows;
    private $seconds_to_add_per_order;
    private $planning_date;
    private $warehouse_id;
    private $routing_service;

    public function __construct()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        parent::__construct();

        $this->seconds_to_add_per_order = Config::get('app.transporters.seconds_to_add_per_order');
        $this->routing_service = Configuration::select('value')->where('key','routing_service')->first();
        Event::subscribe(new OrderEventHandler);
    }

    /**
     * Grilla de planeaciones de rutas
     */
    public function index()
    {
        if (!Request::ajax())
        {
            $data = [
                'cities' => $this->get_cities(),
                'title' => 'Planeación de Rutas'
            ];
            return View::make('admin.planning.routes.index', $data);
        }
        else {
            $city_id = Input::get('city_id');


            $planning_routes = Routes::select('routes.*', 'cities.city', 'admin.fullname AS admin', DB::raw('COUNT(routes.id) AS routes'), 'warehouses.warehouse', 'warehouses.id AS warehouse_id')
                            ->join('cities', 'cities.id', '=', 'routes.city_id')
                            ->join('admin', 'admin.id', '=', 'routes.admin_id')
                            ->join('zones', 'routes.zone_id', '=', 'zones.id')
                            ->join('warehouses', 'warehouses.id', '=', 'routes.warehouse_id')
                            ->where('routes.city_id', $city_id)
                            ->groupBy('routes.warehouse_id')
                            ->groupBy('routes.city_id')
                            ->groupBy('routes.planning_date')
                            ->groupBy('routes.shift')
                            ->orderBy('routes.planning_date', 'desc')
                            ->orderBy('routes.shift', 'desc');

            if (Input::has('date')){
                $date = format_date('mysql', Input::get('date'));
                $planning_routes->where('routes.planning_date', $date);
            }
            $planning_routes = $planning_routes->limit(80)->get();

            $data = [
                'cities' => $this->get_cities(),
                'planning_routes' => $planning_routes,
                'title' => 'Planeación de Rutas'
            ];

            return View::make('admin.planning.routes.index', $data)->renderSections()['content'];
        }
    }

    /**
     * Formulario para nueva planeacion de rutas
     */
    public function add()
    {

        if (!Input::has('planning_date') && !Session::has('post_data'))
            return Redirect::route('adminRoutes.index')->with('type', 'error')->with('message', 'La fecha de planeación es requerida.');

        if (!Input::has('warehouse_id') && !Session::has('post_data'))
            return Redirect::route('adminRoutes.index')->with('type', 'error')->with('message', 'Debe seleccionar la bodega.');

        $this->planning_date = Session::has('post_data') ? Session::get('post_data')['planning_date'] : Input::get('planning_date');
        $this->planning_date = format_date('mysql', trim($this->planning_date));
        $this->warehouse_id = Session::has('post_data') ? Session::get('post_data')['warehouse_id'] : Input::get('warehouse_id');

        if (!Request::isMethod('post'))
        {

            if ($this->planning_date == date('Y-m-d')){
                $shifts = ['AM' =>'Despacho de pedidos de la mañana', 'PM' => 'Despacho de pedidos de la tarde', 'MD' => 'Despacho de pedidos del mismo día'];
            }else{
                $shifts = ['AM' =>'Despacho de pedidos de la mañana', 'PM' => 'Despacho de pedidos de la tarde'];
            }

            $warehouse = Warehouse::find($this->warehouse_id);
            $data = [
                'city' => City::find($warehouse->city_id),
                'shifts' => $shifts,
                'warehouse' =>  Warehouse::find($this->warehouse_id),
                'orders' => Routes::getAllOrders($this->warehouse_id, $this->planning_date),
                'planning_date' => $this->planning_date,
                'title' => 'Nueva Planeación de Rutas ',
                'post_data' => Session::has('post_data') ? Session::get('post_data') : []
            ];

            return View::make('admin.planning.routes.add', $data);
        }else{
            $this->shift_delivery_windows = DeliveryWindow::getDeliveryWindowsGroupByShifts(Input::get('city_id'));

            //validar que no exista la planeacion
            $routes = $this->get_routes_by_date_and_shift(Input::get('city_id'), Input::get('shift'));
            if ($routes)
                return Redirect::route('adminRoutes.add')->with('type', 'error')->with('message', 'Ya existe una planeación con los mismos parametros.')->with('post_data', Input::all());

            //validar que no hayan pedidos con direccion errada

            $orders = Routes::getOrdersWithWrongAddress($this->get_all_city_ids(Input::get('city_id')), $this->planning_date, $this->shift_delivery_windows[Input::get('shift')], $this->warehouse_id);
            if ($orders)
                return Redirect::route('adminRoutes.add')->with('type', 'error')->with('message', 'Hay <b>'.$orders.'</b> pedidos con dirección errada, por favor corrigelos y vuelve a intentarlo.')->with('post_data', Input::all());

            //validar que no hayan pedidos sin zona asignada
            $orders = Routes::getOrdersWithWrongZone($this->get_all_city_ids(Input::get('city_id')), $this->planning_date, $this->shift_delivery_windows[Input::get('shift')], $this->warehouse_id);
            if ($orders)
                return Redirect::route('adminRoutes.add')->with('type', 'error')->with('message', 'Hay <b>'.$orders.'</b> pedidos sin zona asignada, por favor corrigelos y vuelve a intentarlo.')->with('post_data', Input::all());

            $planning_routes = $this->process_planning_routes(Input::get('city_id'), Input::get('shift'));
            if (isset($planning_routes['status']))
                return Redirect::route('adminRoutes.add')->with('type', 'error')->with('message', $planning_routes['message'])->with('post_data', Input::all());
            if (!count($planning_routes))
                return Redirect::route('adminRoutes.add')->with('type', 'error')->with('message', 'No se encontraron pedidos.')->with('post_data', Input::all());

            try {
                DB::beginTransaction();

                //crear rutas y actualizar datos de pedido
                $picking_priority = 1;

                foreach ($planning_routes as $planning_route)
                {
                    foreach ($planning_route as $order_routes)
                    {
                        $create_route = true;
                        foreach($order_routes as $order_route){
                            if ($order = Order::find($order_route['id']))
                            {
                                //crear ruta
                                if ($create_route)
                                {
                                    //validar si ya existe una ruta en la misma zona
                                    $cont = Routes::where('city_id', Input::get('city_id'))->where('shift', Input::get('shift'))->where('planning_date', $this->planning_date)->where('zone_id', $order_route['zone_id'])->max('cont');
                                    $cont++;
                                    $city = City::find(Input::get('city_id'));
                                    $route = new Routes;
                                    $route->status = 'En proceso';
                                    $route->admin_id = Session::get('admin_id');
                                    $route->city_id = Input::get('city_id');
                                    $route->zone_id = $order_route['zone_id'];
                                    $route->cont = $cont;
                                    $route->planning_date = $this->planning_date;
                                    $route->warehouse_id = $this->warehouse_id;

                                    $route->shift = Input::get('shift');
                                    $route->picking_priority = $picking_priority;
                                    $route->route = date('d/m/Y', strtotime($this->planning_date)).' '.strtoupper(substr($city->city, 0, 3)).' '.$route->shift.' '.$route->picking_priority.' '.$order_route['zone'].' '.$cont;
                                    $route->dispatch_time = $route->getDispatchTime();
                                    $route->save();
                                    $create_route = false;
                                    $picking_priority++;
                                }
                                //guardar datos a pedido
                                $order->route_id = $route->id;
                                $order->planning_sequence = $order_route['planning_sequence'];
                                $order->planning_duration = $order_route['planning_duration'];
                                $order->planning_distance = $order_route['planning_distance'];
                                $order->estimated_arrival_date = $order_route['estimated_arrival_date'];
                                $order->save();
                            }
                        }
                    }
                    $cont = 0;
                }
                $OrdersToCount=Routes::getAllOrders($this->warehouse_id, $this->planning_date);
                $fakeError= new \Exception('PLANEACION_CREADA,'.((int) $this->warehouse_id).','.Input::get('shift').','.date('d/m/Y').','.$OrdersToCount->count().',old',204) ;
                ErrorLog::add($fakeError,204);
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                ErrorLog::add($e,204);    
                return Redirect::back()->with('type', 'error')->with('message', $e->getMessage().' '.$e->getLine())->with('post_data', Input::all());
            }

            return Redirect::route('adminRoutes.details', ['city_id' => $route->city_id, 'date' => date('Y-m-d', strtotime($route->planning_date)), 'warehouse_id'=>$this->warehouse_id,  'shift' => $route->shift])
                            ->with('type', 'success')->with('message', 'Se han creado '.count($planning_routes).' rutas con éxito');
        }
    }

    /**
     * Detalle de planeacion de rutas
     */
    public function details()
    {
        $city_id = Route::current()->parameter('city_id');
        $this->planning_date = Route::current()->parameter('date');
        $this->warehouse_id = Route::current()->parameter('warehouse_id');
        $shift = Route::current()->parameter('shift');

        if (!Request::ajax())
        {
            $routes = Routes::select('routes.*', 'cities.city', DB::raw('COUNT(routes.id) AS routes'), 'warehouses.warehouse', 'warehouses.orders_limit', 'warehouses.city_id')
                            ->join('cities', 'cities.id', '=', 'routes.city_id')
                            ->join('zones', 'zones.id','=','routes.zone_id')
                            ->join('warehouses', 'warehouses.id', '=', 'routes.warehouse_id')
                            ->where('routes.city_id', $city_id)
                            ->where('routes.warehouse_id', $this->warehouse_id)
                            ->where('planning_date', $this->planning_date)
                            ->where('shift', $shift)
                            ->groupBy('routes.city_id')
                            ->groupBy('routes.planning_date')
                            ->groupBy('routes.shift')
                            ->orderBy('picking_priority');

            $routes = $routes->first();

            $orders = Warehouse::select( DB::raw('COUNT(orders.id) AS orders'))
                            ->join('routes','routes.warehouse_id', '=', 'warehouses.id')
                            ->join('zones',  'routes.zone_id', '=', 'zones.id')
                            ->join('orders', 'routes.id', '=', 'orders.route_id')
                            ->where('routes.city_id', $city_id)
                            ->where('routes.shift', $shift)
                            ->where('routes.planning_date', $this->planning_date )
                            ->where('routes.warehouse_id', $this->warehouse_id)
                            ->groupBy('routes.warehouse_id')
                            ->get()
                            ->first();
                            //->toSql();
                            
            $data = [
                'cities' => $this->get_cities(),
                'title' => 'Detalle de Planeación de Rutas ',
                'warehouse_id' => $this->warehouse_id,
                'routes' => $routes,
                'orders' => $orders
            ];
            return View::make('admin.planning.routes.details', $data);
        }
        else {
            $routes = Routes::select('routes.*', 'cities.city', 'picking_group', 'color',
                DB::raw('COUNT(orders.id) AS orders'),
                DB::raw('SUM(planning_duration) AS planning_duration'),
                DB::raw('SUM(planning_distance) AS planning_distance'), 'warehouses.warehouse', 'warehouses.id AS warehouse_id')
                ->join('cities', 'cities.id', '=', 'routes.city_id')
                ->join('zones', 'zones.id', '=','routes.zone_id')
                ->join('warehouses', 'warehouses.id','=','routes.warehouse_id')
                ->join('orders', 'routes.id', '=', 'orders.route_id')
                ->join('delivery_windows', 'orders.delivery_window_id','=','delivery_windows.id')
                ->leftJoin('picking_group', 'picking_group.id', '=', 'routes.picking_group_id')
                ->where('routes.city_id', $city_id)
                ->where('routes.warehouse_id', $this->warehouse_id)
                ->where('routes.planning_date', $this->planning_date)
                ->where('routes.shift', $shift)
                ->orderBy('picking_priority');

            if (Input::has('s'))
                $routes->where('routes.route', 'LIKE', '%'.Input::get('s').'%');

            $routes = $routes->groupBy('routes.id')->get();

            if (!$routes)
                return Redirect::route('adminRoutes.index')->with('type', 'error')->with('message', 'No existe la planeación de rutas.');

            $total_orders = 0;
            $total_products = 0;
            $total_containers = 0;
            foreach($routes as $route)
            {
                $orders = Routes::find($route->id)->orders;
                $orders_count = $orders->count();
                $orders_delivered = $orders->filter(function($order){
                    return 'Delivered' == $order->status || 'Cancelled' == $order->status;
                })->count();

                $order = Order::select(DB::raw('SUM(IF(order_products.type <> "Agrupado", order_products.quantity, 0)) AS cant_order_products'),
                    DB::raw('SUM(order_product_group.quantity) AS cant_order_product_group'), 'planning_sequence', 'planning_distance')
                    ->join('order_products', 'orders.id', '=', 'order_id')
                    ->leftJoin('order_product_group', 'order_products.id', '=', 'order_product_id')
                    ->where('orders.route_id', $route->id)
                    ->groupBy('orders.route_id')
                    ->first();
                $route->products = $order->cant_order_products + $order->cant_order_product_group;
                $route->orders_delivered = $orders_delivered;
                $route->orders_count = $orders_count;
                if ($order->planning_sequence == 1)
                    $route->planning_distance = $order->planning_distance;
                $total_products += $route->products;
                $total_orders += $route->orders;

                $delivery_times = Order::getOrdersByRoute($route->id);

                $route->delivery_times = $delivery_times;
                $route->containers = \Volumetry::getTotalRouteContainers($route->id);
                $total_containers += $route->containers;
                $vehicle = \Vehicle::find($route->suggested_vehicle_id);
                $route->plate =isset($vehicle->plate) ? $vehicle->plate : '';

            }
            $transporters = \Transporter::where('city_id', $city_id)
                ->where('status',true)
                ->get();

            $data = [
                'planning_route_status' => $routes[0]->status,
                'routes' => $routes,
                'total_products' => $total_products,
                'total_orders' => $total_orders,
                'total_containers' => $total_containers,
                'transporters' => $transporters
            ];

            return View::make('admin.planning.routes.details', $data)->renderSections()['content'];
        }
    }

    /**
    * Retorna una lista con las rutas 
    */
    public function get_list_routes_ajax(){
        
        $route = Routes::find(Input::get('route_id'));
        $routes =  Routes::select('id', 'route')
                        ->where('warehouse_id', $route->warehouse_id)
                        ->where('planning_date', $route->planning_date)
                        ->where('shift', Input::get('shift'))
                        //->where('status', 'En proceso')
                        ->get();
        if(count($routes)){
            $response = [
                'status' => true,
                'message' => "", 
                'result' => $routes->toArray()
            ];
            return Response::json($response , 200);
        }else{
            $response = [
                'status' => false,
                'message' => "No hay rutas disponibles para el turno ".Input::get('shift') 
            ];
            return Response::json($response , 200);
        }
    }

    /**
    * Cabia de ruta el pedido
    *
    */
    public function change_route_order_ajax(){
        try{
            DB::beginTransaction();

            if (!Input::has('order_sequence') || !Input::has('order_id')){
                $response['message'] = 'Hay datos obligatorios vacios.';
                return Response::json($response , 200);
            }
            $order_sequence = Input::get('order_sequence');

            if (!$order = Order::find(Input::get('order_id'))){
                $response['message'] = 'El pedido no existe.';
                return Response::json($response , 200);
            }
            
            $order = Order::find(Input::get('order_id'));
            $old_route = $order->route_id;
            $order->route_id = Input::get('route_id');
            $order->planning_sequence = $order_sequence;
            $order->save();

            if (!$route_origin = Routes::select('routes.*', 'orders_per_route')->join('zones', 'zone_id', '=', 'zones.id')->where('routes.id', $order->route_id)->first()){
                $response['message'] = 'La ruta no existe.';
                return Response::json($response , 200);
            }

            if ($route_origin->status == 'Terminada' && $order->status != 'Enrutado'){
                $response['message'] = 'La planeación para la ruta ya esta terminada y el pedido no esta en estado Enrutado.';
                return Response::json($response , 200);
            }

            $order_data['id'] = $order->id;
            $order_data['sequence'] = $order_sequence;
            $response = $this->update_route($route_origin, $order_data);

            //$response = json_decode($response->getContent(), true);
            if($response['status']){
                $response['message'] = 'El pedidor fue cambiado de ruta con éxito.';

                $log = new OrderLog();
                $log->type = 'Ruta cambiada: '.$old_route.' a '.$order->route_id;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                DB::commit();
                return Response::json($response, 200);
            }else{
                DB::rollback();
                return Response::json($response, 200);
            }
        }catch(Exception $e){
            DB::rollback();
            return Response::json([
                'status' => false,
                'message' => $e->getMessage()
            ], 200);
        }
    }

    /**
     * Detalle de pedidos en ruta
     */
    public function orders_details($id)
    {
        if ($id)
        {
            //detalle de ruta
            $orders = Order::select('orders.id', 'orders.status', 'delivery_date', 'delivery_time', 'user_address AS address', 'route', 'cities.city', 'expected_containers',
                            'user_address_neighborhood AS neighborhood', 'planning_sequence', DB::raw('SUM(quantity) AS products_quantity'), 'orders.allied_store_id', 'routes.status AS route_status' )
                            ->join('routes', 'route_id', '=', 'routes.id')
                            ->join('order_groups', 'group_id', '=', 'order_groups.id')
                            ->join('cities', 'user_city_id', '=', 'cities.id')
                            ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                            ->where('orders.route_id', $id)
                            ->groupBy('orders.id')
                            ->orderBy('planning_sequence')
                            ->get();

            $data = [
                'title' => count($orders).' Pedidos en Ruta: '.$orders[0]->route,
                'orders' => $orders,
                'route_id' => $id
            ];

        }else{

            //pedidos con direccion errada o sin zona
            $orders = Order::select('orders.id', 'orders.status', 'delivery_date', 'delivery_time', 'user_address AS address', 'cities.city','expected_containers',
                            'user_address_neighborhood AS neighborhood', 'planning_sequence', DB::raw('SUM(quantity) AS products_quantity'), 'orders.allied_store_id' )
                            ->join('order_groups', 'group_id', '=', 'order_groups.id')
                            ->join('cities', 'user_city_id', '=', 'cities.id')
                            ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                            ->whereIn('order_groups.user_city_id', $this->get_all_city_ids(Input::get('city_id')))
                            ->whereRaw("DATE(orders.delivery_date) = '".Input::get('delivery_date')."'")
                            ->where('orders.delivery_time', Input::get('delivery_time'))
                            ->whereIn('orders.status', ['Initiated', 'Alistado', 'In Progress'])
                            ->groupBy('orders.id')
                            ->orderBy('products_quantity', 'desc');
            if (Input::get('type') == 'zone')
                $orders = $orders->whereNull('zone_id');
            else $orders = $orders->where('order_groups.user_address_latitude', 1);

            $orders = $orders->get();

            $data = [
                'title' => count($orders).' Pedidos con dirección errada',
                'orders' => $orders
            ];
        }
        

        return View::make('admin.planning.routes.orders_details', $data);
    }

    /**
     * Mapa con ruta de planeacion
     */
    public function route_map($id)
    {
        $routes = [];
        $delivery_times = Order::select('delivery_window_id')
            ->join('delivery_windows', 'delivery_windows.id','=', 'orders.delivery_window_id')
            ->where('route_id', $id)
            ->groupBy('delivery_window_id')
            ->orderBy('delivery_windows.hour_start')
            ->get();

        foreach($delivery_times as $data)
        {
            $orders = Order::select('orders.id', 'orders.delivery_window_id', 'delivery_windows.delivery_window as delivery_time', 'user_address AS address', 'user_address_neighborhood AS neighborhood',
                'user_address_latitude AS latitude', 'user_address_longitude AS longitude', 'planning_sequence', 'user_city_id',
                DB::raw("ROUND(planning_distance / 1000, 1) AS planning_distance"),
                DB::raw("ROUND(planning_duration / 60) AS planning_duration"),
                'route_id', 'zones.name AS zone', 'route', 'orders.allied_store_id', 'warehouses.latitude AS warehouses_latitude', 'warehouses.longitude AS warehouses_longitude', 'orders.type',
                DB::raw("IFNULL(allied_stores.name, '') AS allied_store_name"))
                ->join('routes', 'route_id', '=', 'routes.id')
                ->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
                ->join('warehouses', 'warehouses.id', '=', 'routes.warehouse_id')
                ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
                ->join('delivery_windows', 'delivery_windows.id','=', 'orders.delivery_window_id')
                ->leftJoin('allied_stores', 'allied_stores.id', '=', 'orders.allied_store_id')
                ->where('routes.id', $id)
                ->where('orders.delivery_window_id', $data->delivery_window_id)
                ->orderBy('planning_sequence')
                ->get()
                ->toArray();
            $routes[] = $orders;
            $merqueo_location = array('lat'=>$orders[0]['warehouses_latitude'], 'lng'=>$orders[0]['warehouses_longitude']);
        }

        $data = [
            'title' => 'Rutas por horario',
            'routes' => $routes,
            'merqueo_location' => $merqueo_location,
        ];

        return View::make('admin.planning.routes.route_map', $data);
    }

    /**
     * Mapa con rutas de planeacion
     */
    public function routes_map()
    {

        $city_id = Route::current()->parameter('city_id');
        $this->planning_date = Route::current()->parameter('date');
        $this->warehouse_id = Route::current()->parameter('warehouse_id');
        $shift = Route::current()->parameter('shift');

        $routes = Routes::select('routes.*')
                        ->where('routes.city_id', $city_id)
                        ->where('routes.planning_date', $this->planning_date)
                        ->where('routes.warehouse_id', $this->warehouse_id)
                        ->where('routes.shift', $shift)
                        ->orderBy('routes.id');

        $warehouse = Warehouse::find($this->warehouse_id);
        $merqueo_location = [ 'lat' => $warehouse->latitude, 'lng' => $warehouse->longitude ];
        $routes = $routes->get();
        foreach($routes as $route){
            $planning_routes[$route->id] = $this->get_route($route->id);
        }

        $data = [
            'title' => 'Rutas',
            'routes' => $routes,
            'cities' => $this->get_cities(true),
            'planning_routes' => $planning_routes,
            'merqueo_location' => $merqueo_location,
            'warehouse_id' => $this->warehouse_id
        ];

        return View::make('admin.planning.routes.routes_map', $data);
    }

    /**
     * Actualizar estado de planeacion de rutas
     */
    public function update_status()
    {
        $city_id = Route::current()->parameter('city_id');
        $this->planning_date = Route::current()->parameter('date');
        $this->warehouse_id = Route::current()->parameter('warehouse_id');
        $shift = Route::current()->parameter('shift');

        $routes = Routes::select('routes.*')
                        ->where('city_id', $city_id)
                        ->where('planning_date', $this->planning_date)
                        ->where('shift', $shift)
                        ->where('routes.warehouse_id', $this->warehouse_id)
                        ->orderBy('picking_priority');

        if (!$routes = $routes->get())
            return Redirect::route('adminRoutes.index')->with('type', 'error')->with('message', 'No se encontraron rutas.');

        try {
            DB::beginTransaction();
            //planeacion terminada
            if (Input::get('status') == 'Terminada')
            {
                $picking_groups = PickingGroup::where('warehouse_id', $this->warehouse_id)->where('status', 1)->get();
                $picking_groups_qty = count($picking_groups) - 1;
                $cont = $index_picking_group = 0;

                foreach($routes as $route)
                {
                    if ($route->status != 'En proceso')
                        throw new Exception('La ruta '.$route->route.' no esta en estado En proceso.');

                    //actualizar pedidos de ruta
                    $orders = Order::where('route_id', $route->id)->whereIn('orders.status', ['Initiated', 'Alistado', 'In Progress'])->get();
                    if (count($orders))
                    {
                        foreach ($orders as $order)
                        {
                            if($order->status == 'Initiated'){
                                $order->status = 'Enrutado';
                            }
                            $order->planning_date = date('Y-m-d H:i:s');
                            $order->save();

                            Event::fire('order.route_log', [[$order, Session::get('admin_id')]]);
                            Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);
                        }

                        Routes::where('id', $route->id)->update(['status' => 'Terminada']);

                    }else Routes::where('id', $route->id)->delete();

                    //asignar grupo de picking a ruta
                    if ($index_picking_group > $picking_groups_qty)
                        $index_picking_group = 0;

                    $route->picking_group_id = $picking_groups[$index_picking_group]->id;
                    $route->status_return = 'Pendiente';
                    $route->save();

                    $volumetry = new \Volumetry();
                    $volumetry->getContainers($route->id);

                    $index_picking_group++;
                    $cont++;
                }
                $this->admin_log('routes', $route->id, 'update');
                $message = 'Se han actualizado '.$cont.' rutas con éxito.';
            }

            //eliminar planeacion
            if (Input::get('status') == 'Eliminar')
            {
                $cont = 0;
                foreach($routes as $route)
                {
                    //validar que los pedidos esten en Initiated o Enrutado
                    $count = Order::where('route_id', $route->id)->whereNotIn('orders.status', ['Initiated', 'Enrutado', 'Cancelled', 'Alistado', 'In Progress'])->count();
                    if ($count)
                        throw new Exception('No se puede eliminar por que hay <b>'.$count.'</b> pedidos que no estan en estado Initiated o Enrutado.');

                    //actualizar pedidos de ruta
                    $orders = Order::where('route_id', $route->id)->whereIn('orders.status', ['Initiated', 'Enrutado', 'Alistado', 'In Progress'])->get();
                    if ($orders)
                    {
                        foreach ($orders as $order)
                        {
                            if($order->status == 'Enrutado'){
                                $order->status = 'Initiated';
                                $order->picker_dry_id = null;
                                $order->picker_cold_id = null;
                            }
                            $order->route_id = null;
                            $order->planning_date = null;
                            $order->planning_sequence = null;
                            $order->planning_duration = null;
                            $order->planning_distance = null;
                            $order->allocated_date = null;
                            $order->save();

                            //Event::fire('order.status_log', [[$order, Session::get('admin_id'), 'admin']]);
                        }
                    }
                    //eliminar ruta
                    Routes::where('id', $route->id)->delete();
                    $cont++;
                }
                if (isset($route))
                    $this->admin_log('routes', $route->id, 'delete');
                $message = 'Se han eliminado '.$cont.' rutas con éxito.';
            }

            DB::commit();
            if (Input::get('status') == 'Terminada')
                return Redirect::route('adminRoutes.details', ['city_id' => $city_id, 'date' => $this->planning_date, 'warehouse_id'=>$this->warehouse_id, 'shift' => $shift])->with('type', 'success')->with('message', $message);
            if (Input::get('status') == 'Eliminar')
                return Redirect::route('adminRoutes.index')->with('type', 'success')->with('message', $message);

        } catch (\Exception $e) {
            DB::rollback();
            \ErrorLog::add($e, 500);
            return Redirect::route('adminRoutes.details', ['city_id' => $city_id, 'date' => $this->planning_date, 'warehouse_id'=>$this->warehouse_id, 'shift' => $shift])
                            ->with('type', 'error')->with('message', $e->getMessage());
        }
    }

    /**
    * Quitar orden de la ruta
    *
    */
    public function remove_order()
    {
        try {
            DB::beginTransaction();
            $current_order = Order::find(Input::get('order_id'));
            $current_route = Routes::find(Input::get('route_id'));
            $routes = Routes::getRoutesByZones($current_route->zone_id, $current_route->planning_date, 'PM', $current_route->city_id, Input::get('warehouse_id'));

            if($current_order->status != 'Alistado' && $current_order->status != 'In Progress'){
                $current_order->status = 'Initiated';
                $current_order->picker_dry_id = null;
                $current_order->picker_cold_id = null;
            }

            $current_order->route_id = null;
            $current_order->planning_date = null;
            $current_order->planning_sequence = null;
            $current_order->planning_duration = null;
            $current_order->planning_distance = null;
            $current_order->allocated_date = null;
            $current_order->save();

            //DB::commit();
            $order_ids = array();
            $shift_delivery_windows = DeliveryWindow::getDeliveryWindowsGroupByShifts($current_route->city_id);

            //obtener pedidos por horario
            foreach($shift_delivery_windows as $delivery_window_id)
            {
                //obtener pedidos por horario y distancia
                $orders = Order::select('orders.id', 'orders.delivery_window_id', 'delivery_windows.delivery_window as delivery_time', 'user_address AS address', 'user_address_neighborhood AS neighborhood', 'user_city_id','warehouses.latitude AS warehouses_latitude', 'warehouses.longitude AS warehouses_longitude', 'user_address_latitude AS latitude', 'user_address_longitude AS longitude', 'planning_sequence', 'zone_id', 'zones.name AS zone', 'management_date',
                    DB::raw("(6371 * ACOS(
                    COS(RADIANS(warehouses.latitude))
                    * COS(RADIANS(user_address_latitude))
                    * COS(RADIANS(user_address_longitude)
                    - RADIANS(warehouses.longitude))
                    + SIN(RADIANS(warehouses.latitude))
                    * SIN(RADIANS(user_address_latitude))
                       )) AS distance"))
                    ->join('order_groups', 'group_id', '=', 'order_groups.id')
                    ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
                    ->join('warehouses', 'warehouses.id', '=', 'order_groups.warehouse_id')
                    ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
                    ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
                    ->where('order_groups.warehouse_id', Input::get('warehouse_id'))
                    ->where('delivery_time', $delivery_window_id)
                    ->where('orders.route_id', $current_route->id)
                    ->orderBy('orders.delivery_date')
                    ->orderBy('distance')
                    ->get()
                    ->toArray();

                //separar pedidos por horario
                if ($orders)
                    foreach($orders as $order){
                        $routes[$order->delivery_window_id][0][] = $order;
                        $order_ids[] = $order['id'];
                    }
            }
            //unir pedidos por horario para armar rutas
            $i = 1;
            foreach($routes as $delivery_time => $route)
            {
                foreach($route as $key => $orders)
                {
                    //completar ruta con pedidos de siguiente horario
                    if (isset($routes[$shift_delivery_windows[$i]]))
                    {
                        foreach ($routes[$shift_delivery_windows[$i]] as $key_next => $route_next)
                        {
                            foreach($route_next as $order_next_key => $order_next)
                            {
                                if (isset($routes[$delivery_time][$key])){
                                    //agregar pedido a ruta por horario
                                    array_push($routes[$delivery_time][$key], $order_next);
                                    //eliminar pedido de horario
                                    unset($routes[$shift_delivery_windows[$i]][$key_next][$order_next_key]);
                                    //eliminar indice de horario si no tiene pedidos
                                    if (!count($routes[$shift_delivery_windows[$i]][$key_next]))
                                        unset($routes[$shift_delivery_windows[$i]][$key_next]);
                                }
                            }
                        }
                    }
                }
            }

            if (isset($routes[$shift_delivery_windows[0]]))
                $total_routes = $routes[$shift_delivery_windows[0]];
            else $total_routes = $routes[$shift_delivery_windows[1]];

            //calcular secuencia de pedidos
            $planning_routes = $this->get_routes_orders_sequence($total_routes, count($order_ids), $current_route->shift);

            foreach ($planning_routes as $planning_route)
            {
                $create_route = true;
                foreach ($planning_route as $order_route)
                {
                    if ($order = Order::find($order_route['id']))
                    {
                        //actualizar id de ruta a pedidos de ruta origen
                        $order->planning_sequence = $order_route['planning_sequence'];
                        $order->planning_duration = $order_route['planning_duration'];
                        $order->planning_distance = $order_route['planning_distance'];
                        $order->estimated_arrival_date = $order_route['estimated_arrival_date'];
                        $order->save();
                    }
                }
            }
            unset($routes);
            unset($planning_routes);

            DB::commit();
            $this->admin_log('routes', $current_route->id, 'Se elimino el pedido '.Input::get('order_id').' de la ruta');
        } catch (\Exception $e) {
            DB::rollback();
            $response['message'] = 'Ocurrio un error al eliminar el pedido de la ruta.'. $e->getMessage();
            return Response::json($response , 200);
            //throw $e;
        }

        unset($routes);
        unset($planning_routes);
        $routes[$current_route->id] = $this->get_route($current_route->id);

        $response = [
            'status' => true,
            'message' => 'Rutas actualizadas con éxito.',
            'result' => $routes
        ];
        return Response::json($response , 200);
    }

    /**
     * Asignar pedidos de una ruta a otra
     */
    public function update_orders_route()
    {
        $response['status'] = false;
        if( Input::has('warehouse_id')){
            $this->warehouse_id = Input::get('warehouse_id');
        }

        if (!Input::has('origin_route_id') || !Input::has('destination_route_id')){
            $response['message'] = 'Hay datos obligatorios vacios.';
            return Response::json($response , 200);
        }

        if (!$route_origin = Routes::find(Input::get('origin_route_id'))){
            $response['message'] = 'La ruta origen no existe.';
            return Response::json($response , 200);
        }

        if ($route_origin->status == 'Terminada'){
            $response['message'] = 'La planeación para la ruta origen ya esta terminada.';
            return Response::json($response , 200);
        }

        $route_destination = Routes::find(Input::get('destination_route_id'));
        if (!$route_destination && Input::get('destination_route_id') != 'create_route'){
            $response['message'] = 'La ruta destino no existe.';
            return Response::json($response , 200);
        }

        if (Input::get('destination_route_id') != 'create_route' && $route_destination->status == 'Terminada'){
            $response['message'] = 'La planeación para la ruta destino ya esta terminada.';
            return Response::json($response , 200);
        }

        //obtener secuencias de pedidos
        if (!Input::get('all_route') || Input::get('destination_route_id') == 'create_route')
        {
            //asignar solo parte de la ruta
            if (Input::has('route_sequences')){
                $route_sequences = explode(',', Input::get('route_sequences'));
                if (!count($route_sequences)){
                    $response['message'] = 'Formato de secuencias de pedidos no valido.';
                    return Response::json($response , 200);
                }
            }

            if (!isset($route_sequences) && Input::get('all_route') && Input::get('destination_route_id') == 'create_route'){
                $response['message'] = 'No se puede crear una ruta nueva con base en otra completa.';
                return Response::json($response , 200);
            }

            $sequences = [];
            foreach($route_sequences as $sequence_value)
            {
                if (is_numeric($sequence_value))
                    $sequences[] = $sequence_value;
                else{
                    $values = explode(',', $sequence_value);
                    foreach($values as $value){
                        $values = explode('-', $value);
                        if (count($values) == 2){
                            for($i = $values[0]; $i <= $values[1]; $i++)
                                $sequences[] = $i;
                        }else{
                            $response['message'] = 'Formato de secuencias por intervalo no valido.';
                            return Response::json($response , 200);
                        }
                    }
                }
            }
        }else{
            //asignar toda la ruta
            if ($orders = Order::select('planning_sequence')->where('route_id', $route_origin->id)->get()){
                foreach($orders as $order)
                    $sequences[] = $order->planning_sequence;
            }
        }

        $planning_routes = [];
        $this->planning_date = $route_origin->planning_date;

        try {
            DB::beginTransaction();
            //recorrer ruta origen y destino
            for ($j = 0; $j <= 1; $j++)
            {
                $routes = [];
                $order_ids = [];

                if ($j == 0) //calcular ruta destino primero
                {
                    //obtener ids de pedidos de ruta origen enviados
                    if ($orders = Order::select('id')->where('route_id', $route_origin->id)->whereIn('planning_sequence', $sequences)->get()){
                        foreach($orders as $order)
                            $order_ids[] = $order->id;
                    }
                    if (Input::get('destination_route_id') != 'create_route'){
                        //obtener ids de pedidos de ruta destino
                        if ($orders = Order::select('id')->where('route_id', $route_destination->id)->get()){
                            foreach($orders as $order)
                                $order_ids[] = $order->id;
                        }
                    }
                }else{ //calcular ruta origen de segundo

                    //obtener ids de pedidos de ruta origen restantes
                    $orders = Order::select('id')->where('route_id', $route_origin->id)->get();
                    if (count($orders)){
                        foreach($orders as $order)
                            $order_ids[] = $order->id;
                    }else{
                        //eliminar ruta si no quedaron pedidos
                        Routes::where('id', $route_origin->id)->delete();
                    }
                }

                $orders = Order::select('orders.id', 'orders.delivery_window_id', 'delivery_windows.delivery_window AS delivery_time', 'user_address AS address', 'user_address_neighborhood AS neighborhood', 'user_city_id','warehouses.latitude AS warehouses_latitude', 'warehouses.longitude AS warehouses_longitude', 'user_address_latitude AS latitude', 'user_address_longitude AS longitude', 'planning_sequence', 'zone_id', 'zones.name AS zone', 'management_date',
                    DB::raw("(6371 * ACOS(
                        COS(RADIANS(warehouses.latitude))
                        * COS(RADIANS(user_address_latitude))
                        * COS(RADIANS(user_address_longitude)
                        - RADIANS(warehouses.longitude))
                        + SIN(RADIANS(warehouses.latitude))
                        * SIN(RADIANS(user_address_latitude))
                           )) AS distance"))
                    ->join('order_groups', 'group_id', '=', 'order_groups.id')
                    ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
                    ->join('warehouses', 'warehouses.id', '=', 'order_groups.warehouse_id')
                    ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
                    ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id' )
                    ->whereIn('orders.id', $order_ids)
                    ->orderBy('delivery_windows.hour_start')
                    ->orderBy('distance')
                    ->get()
                    ->toArray();


                if (count($orders)){
                    foreach( $orders as $order )
                        $routes[0][] = $order;

                    //calcular secuencia de pedidos
                    $planning_routes = $this->get_routes_orders_sequence($routes, count($order_ids), $route_origin->shift);


                    if (isset($planning_routes['status'])){
                        $response = [
                            'status' => false,
                            'message' => $planning_routes['message']
                        ];
                        return Response::json($response , 200);
                    }

                    //debug($planning_routes);

                    //actualizar datos de pedido
                    foreach ($planning_routes as $planning_route)
                    {
                        $create_route = true;
                        foreach ($planning_route as $order_route)
                        {
                            if ($order = Order::find($order_route['id']))
                            {
                                //crear ruta
                                if ($create_route && $j == 0 && Input::get('destination_route_id') == 'create_route')
                                {
                                    $city = City::find($route_origin->city_id);
                                    $this->planning_date = date('Y-m-d', strtotime($order->delivery_date));
                                    //validar si ya existe una ruta en la misma zona
                                    $cont = Routes::where('city_id', $route_origin->city_id)->where('planning_date', $this->planning_date)->where('shift', $route_origin->shift)->where('zone_id', $order_route['zone_id'])->max('cont');
                                    $cont++;
                                    $route_destination = new Routes;
                                    $route_destination->status = 'En proceso';
                                    $route_destination->admin_id = Session::get('admin_id');
                                    $route_destination->city_id = $route_origin->city_id;
                                    $route_destination->zone_id = $order_route['zone_id'];
                                    $route_destination->cont = $cont;
                                    $route_destination->planning_date = $this->planning_date;
                                    $route_destination->warehouse_id = $this->warehouse_id;
                                    $route_destination->shift = $route_origin->shift;
                                    $route_destination->picking_priority = Routes::where('city_id', $route_origin->city_id)->where('planning_date', $this->planning_date)->where('shift', $route_origin->shift)->max('picking_priority');
                                    $route_destination->route = date('d/m/Y', strtotime($this->planning_date)).' '.strtoupper(substr($city->city, 0, 3)).' '.$route_destination->shift.' '.$route_destination->picking_priority.' '.$order_route['zone'].' '.$cont;
                                    $route_destination->save();
                                    $create_route = false;
                                }

                                //actualizar id de ruta a pedidos de ruta origen
                                if ($j == 0)
                                    $order->route_id = $route_destination->id;
                                $order->planning_sequence = $order_route['planning_sequence'];
                                $order->planning_duration = $order_route['planning_duration'];
                                $order->planning_distance = $order_route['planning_distance'];
                                //$order->estimated_arrival_date = $order_route['estimated_arrival_date'];
                                $order->save();

                            }
                        }
                    }
                    unset($routes);
                    unset($planning_routes);
                }
            }


            DB::commit();

        }catch(\Exception $e){
            DB::rollback();
            //throw $e;
            $response = [
                'status' => false,
                'message' => $e->getMessage().' '.$e->getLine()
            ];
            return Response::json($response , 200);
        }

        //obtener rutas
        unset($routes);
        unset($planning_routes);
        $planning_routes[$route_origin->id] = $route_origin->id;
        $planning_routes[$route_destination->id] = $route_destination->id;
        foreach ($planning_routes as $index => $route_id)
            $routes[$index] = $this->get_route($route_id);

        $response = [
            'status' => true,
            'message' => 'Rutas actualizadas con éxito.',
            'result' => $routes
        ];

        return Response::json($response , 200);
    }

    /**
     * Actualizar parte de ruta debido a actualizacion manual de secuencia en pedido
     */
    public function update_order_sequence()
    {
        $response['status'] = false;

        if (!Input::has('order_sequence') || !Input::has('order_id')){
            $response['message'] = 'Hay datos obligatorios vacios.';
            return Response::json($response , 200);
        }

        if (!$order = Order::find(Input::get('order_id'))){
            $response['message'] = 'El pedido no existe.';
            return Response::json($response , 200);
        }

        if (!$route_origin = Routes::select('routes.*', 'orders_per_route')->join('zones', 'zone_id', '=', 'zones.id')->where('routes.id', $order->route_id)->first()){
            $response['message'] = 'La ruta no existe.';
            return Response::json($response , 200);
        }

        if ($route_origin->status == 'Terminada' && $order->status != 'Enrutado'){
            $response['message'] = 'La planeación para la ruta ya esta terminada y el pedido no esta en estado Enrutado.';
            return Response::json($response , 200);
        }

        try {

            DB::beginTransaction();

            $order_sequence = Input::get('order_sequence');

            //quitar secuencia a pedido y colocar nueva
            Order::where('route_id', $route_origin->id)->where('planning_sequence', $order_sequence)->update(['planning_sequence' => $order_sequence]);
            $order->planning_sequence = $order_sequence;
            $order->save();

            $order_data['id'] = $order->id;
            $order_data['sequence'] = $order_sequence;
            $response = $this->update_route($route_origin, $order_data);

            if (!$response['status'])
                return $response;

            DB::commit();

        }catch(\Exception $e){
            DB::rollback();
            //throw $e;
            $response = [
                'status' => false,
                'message' => $e->getMessage().' '.$e->getLine()
            ];
            return Response::json($response , 200);
        }

        $routes[$route_origin->id] = $this->get_route($route_origin->id);

        $response = [
            'status' => true,
            'message' => 'Ruta actualizada con éxito.',
            'result' => $routes
        ];

        return Response::json($response , 200);
    }

    /**
     * Obtener direccion de entrega de pedido
     */
    public function get_order_delivery_address()
    {
        $response['status'] = false;

        if (!Input::has('order_id')){
            $response['message'] = 'Hay datos obligatorios vacios.';
            return Response::json($response , 200);
        }

        $order = Order::select('user_address', 'user_address_further', 'user_address_neighborhood', 'user_city_id')
            ->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->where('orders.id', Input::get('order_id'))
            ->whereIn('orders.status', ['Initiated', 'Alistado', 'In Progress'])
            ->first();

        if (!$order){
            $response['message'] = 'EL pedido no existe o no esta en un estado valido.';
            return Response::json($response , 200);
        }

        $response = [
            'status' => true,
            'message' => 'Direccion obtenida con éxito.',
            'result' => $order
        ];

        return Response::json($response , 200);
    }

    /**
     * Actualizar direccion de entrega de pedido
     */
    public function update_order_delivery_address()
    {
        $response['status'] = false;

        if (!Input::has('order_id')){
            $response['message'] = 'Hay datos obligatorios vacios.';
            return Response::json($response , 200);
        }

        try {

            DB::beginTransaction();

            $order = Order::select('orders.*', 'shift')->join('routes', 'route_id', '=', 'routes.id')//->where('orders.type', 'Merqueo')
                                ->where('orders.id', Input::get('order_id'))->first();
            $order_group = OrderGroup::find($order->group_id);

            $old_address = $order_group->user_address;
            $old_city = $order_group->user_city_id;
            $old_city = City::find($old_city);
            if ($order)
            {
                $address = Input::get('delivery_address');
                $address_further = Input::get('delivery_address_further');
                $address_neighborhood = Input::get('delivery_address_neighborhood');
                $city_id = Input::get('city');
                $city = City::find($city_id);

                $request = Request::create('/api/location', 'GET', array('address' => $address, 'city' => $city->city));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent());

                $order_group->user_address = $address;
                $order_group->user_address_further = $address_further;
                $order_group->user_address_neighborhood = $address_neighborhood;
                $order_group->user_city_id = $city_id;
                if ($result->status == 1) {
                    $order_group->user_address_latitude = $result->result->latitude;
                    $order_group->user_address_longitude = $result->result->longitude;
                    $order->store->validateStoreDeliversToAddress(
                        $order_group->user_address_latitude,
                        $order_group->user_address_longitude,
                        new Carbon($order->delivery_date),
                        $order->delivery_time
                    );

                    //obtener zona
                    $zone_id = null;
                    $zones = Zone::where('city_id', $order_group->user_city_id)->where('status', 1)->get();
                    if ($zones){
                        foreach ($zones as $zone){
                            if (point_in_polygon($zone->delivery_zone, $order_group->user_address_latitude, $order_group->user_address_longitude)){
                                $zone_id = $zone->id;
                                break;
                            }
                        }
                    }
                    $order_group->zone_id = $zone_id;
                }else{
                    $response['message'] = 'No se pudo encontrar la ubicación de la dirección por favor verifica los datos.';
                    return Response::json($response , 200);
                }

                $user_address = UserAddress::find($order_group->address_id);
                if ($user_address) {
                    $user_address->city_id = $city_id;
                    $user_address->save();
                }

                $order_group->save();

                $log = new OrderLog();
                $log->type = 'Dirección de entrega actualizada: '.$old_city->city.' '.$old_address.' a '.$city->city.' '.$order_group->user_address;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                //si esta dentro de una zona
                $old_route_id = $order->route_id;
                //obtener ruta si existe con base a nueva zona del pedido
                $this->planning_date = date('Y-m-d', strtotime($order->delivery_date));

                $route = Routes::select('routes.*', 'orders_per_route')->join('zones', 'zone_id', '=', 'zones.id')
                               ->where('routes.city_id', $order_group->user_city_id)->where('planning_date', $this->planning_date)
                               ->where('shift', $order->shift)
                               ->where('zone_id', $order_group->zone_id)
                               ->where('routes.status', 'En proceso')
                               ->orderBy('created_at', 'desc');

                if (!$route = $rouet->first()){
                    //crear una nueva ruta solo con ese pedido

                    $zone = Zone::find($order_group->zone_id);
                    $cont = Routes::where('city_id', $route->city_id)->where('planning_date', $this->planning_date)->where('shift', $route->shift)->where('zone_id', $route->zone_id)->max('cont');
                    $cont++;
                    $route = new Routes;
                    $route->status = 'En proceso';
                    $route->admin_id = Session::get('admin_id');
                    $route->city_id = $order_group->user_city_id;
                    $route->zone_id = $order_group->zone_id;
                    $route->planning_date = $this->planning_date;
                    $route->warehouse_id = $this->warehouse_id;
                    $route->cont = $cont;
                    $route->shift = $order->shift;
                    $route->route = date('d/m/Y', strtotime($this->planning_date)).' '.strtoupper(substr($city->city, 0, 3)).' '.$route->shift.' '.$zone->name.' 1';
                    $route->picking_priority = Routes::where('city_id', $route->city_id)->where('planning_date', $this->planning_date)->where('warehouse_id', $this->warehouse_id)->where('shift', $route->shift)->max('picking_priority');
                    $route->save();
                    $route->orders_per_route = $zone->orders_per_route;
                }
                $order->route_id = $route->id;
                $order->save();

                //recalcular secuencia de ruta
                $response = $this->update_route($route);
                if (!$response['status'])
                    return $response;

                //eliminar ruta si no quedaron pedidos
                if (!Order::select('id')->where('route_id', $old_route_id)->count()){
                    Routes::where('id', $old_route_id)->delete();
                    $routes[$old_route_id] = [];
                }else $routes[$old_route_id] = $this->get_route($old_route_id);

                $routes[$order->route_id] = $this->get_route($order->route_id);

                $response = [
                    'status' => true,
                    'message' => 'El pedido fue actualizado y asignado a la zona "'.$route->route.'"',
                    'result' => $routes
                ];
            }else $response['message'] = 'Ocurrió un error al actualizar la dirección de entrega del pedido.';

        DB::commit();

        }catch(\Exception $e){
            DB::rollback();
            //throw $e;
            $response = [
                'status' => false,
                'message' => $e->getMessage().' '.$e->getLine()
            ];
        }

        return Response::json($response , 200);
    }

    /**
     * Actualizar prioridad de picking por ruta
     */
    public function update_picking_priority()
    {
        $picking_priority = Input::get('picking_priority');
        $picking_priority = json_decode($picking_priority, true);
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $date = Input::get('date');
        $shift = Input::get('shift');

        $routes = Routes::select('routes.id')
                            ->join('cities', 'cities.id', '=', 'routes.city_id')
                            ->join('zones', 'zones.id', '=', 'routes.zone_id')
                            ->join('warehouses', 'routes.warehouse_id', '=', 'warehouses.id')
                            ->where('routes.warehouse_id', $warehouse_id)
                            ->where('warehouses.city_id', $city_id)
                            ->where('routes.planning_date', $date)
                            ->where('routes.shift', $shift);
        $routes = $routes->get();

        if (count($routes))
        {
            foreach($routes as $route)
            {
                if (isset($picking_priority['route_'.$route->id])){
                    $route->picking_priority = $picking_priority['route_'.$route->id];
                    $route->save();
                }
            }
            $response = [
                'status' => true,
                'message' => 'Rutas actualizadas'
            ];
        }else $response = [
            'status' => false,
            'message' => 'No se actualizo ninguna ruta.'
        ];

        return Response::json($response , 200);
    }

    /**
     * Actualizar hora de despacho
     */
    public function update_dispatch_time()
    {
        $dispatch_time = Input::get('dispatch_time');
        $dispatch_time = json_decode($dispatch_time, true);
        $city_id = Input::get('city_id');
        $warehouse_id = Input::get('warehouse_id');
        $date = Input::get('date');
        $shift = Input::get('shift');

        $routes = Routes::select('routes.id', 'routes.dispatch_time')
                            ->join('cities', 'cities.id', '=', 'routes.city_id')
                            ->join('zones', 'zones.id', '=', 'routes.zone_id')
                            ->join('warehouses', 'warehouses.id', '=', 'routes.warehouse_id')
                            ->where('routes.warehouse_id', $warehouse_id)
                            ->where('warehouses.city_id', $city_id)
                            ->where('routes.planning_date', $date)
                            ->where('routes.shift', $shift);
        $routes = $routes->get();

        if (count($routes))
        {
            foreach($routes as $route)
            {
                if (isset($dispatch_time['time_'.$route->id])){
                    $current_dispatch_date = date('Y-m-d', strtotime($route->dispatch_time));

                    $route->dispatch_time = $current_dispatch_date.' '.$dispatch_time['time_'.$route->id];
                    $route->save();
                }
            }
            $response = [
                'status' => true,
                'message' => 'Rutas actualizadas'
            ];
        }else $response = [
            'status' => false,
            'message' => 'No se actualizo ninguna ruta.'
        ];

        return Response::json($response , 200);
    }

    /**
     * Descargar planeacion
     */
    public function download()
    {
        $city_id = Route::current()->parameter('city_id');
        $date = Route::current()->parameter('date');
        $shift = Route::current()->parameter('shift');
        $warehouse_id = Route::current()->parameter('warehouse_id');

        DB::statement("SET lc_time_names = 'es_ES'");
        $sql = "SELECT
                o.id AS PEDIDO,
                o.type AS TIPO,
                IFNULL(al.name, '') AS 'TIENDA ALIADA',
                o.status AS ESTADO,
                r.route AS RUTA,
                picking_group AS 'GRUPO ALISTAMIENTO',
                o.planning_sequence AS SECUENCIA,
                c.city AS 'CIUDAD',
                w.warehouse AS 'BODEGA',
                DATE_FORMAT(first_delivery_date, '%d/%m/%Y') AS 'FECHA DE ENTREGA',
                o.delivery_time AS 'HORA DE ENTREGA',
                total_products AS 'CANTIDAD DE PRODUCTOS',
                DATE_FORMAT(r.dispatch_time, '%d/%m/%Y %H:%i:%s') AS 'FECHA DE DESPACHO'
            FROM
                orders o
            INNER JOIN order_groups og ON o.group_id = og.id
            INNER JOIN routes r ON o.route_id = r.id LEFT JOIN picking_group ON picking_group_id = picking_group.id
            INNER JOIN warehouses w ON w.id = r.warehouse_id
            INNER JOIN stores st ON o.store_id = st.id
            INNER JOIN cities c ON og.user_city_id = c.id
            LEFT JOIN allied_stores al ON al.id = o.allied_store_id
            WHERE w.city_id = ".$city_id."
            AND og.warehouse_id = '".$warehouse_id."'
            AND r.planning_date = '".$date."'
            AND r.shift = '".$shift."'
            ORDER BY r.id;";

        $rows = DB::select(DB::raw($sql));

        if (!count($rows))
            return Redirect::route('adminRoutes.details', ['city_id' => $city_id, 'date' => $date, 'warehouse_id'=> $warehouse_id, 'shift' => $shift])->with('type', 'error')->with('message', 'No se encontraron pedidos.');

        if ($n = count($rows))
        {
            //eliminar archivo
            $path = public_path(Config::get('app.download_directory'));
            if ($gestor = opendir($path)){
                while (false !== ($file = readdir($gestor))){
                    if (strstr($file, Session::get('admin_username')))
                        remove_file($path.$file);
                }
            }

            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Pedidos');
            $objPHPExcel->getProperties()->setSubject('Pedidos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb'=>'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:AZ1')->applyFromArray($style_header);

            //titulos
            $i = 0;
            foreach ($rows[0] as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 1, $key);
                $i++;
            }

            $j = 2;
            foreach($rows as $row){
                $i = 0;
                foreach ($row as $key => $value){
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                    $i++;
                }
                $j++;
            }

             //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Pedidos');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));

            return Redirect::route('adminRoutes.details', ['city_id' => $city_id, 'date' => $date, 'warehouse_id'=> $warehouse_id, 'shift' => $shift])
                    ->with('type', 'success')->with('message', 'Planeación descargada con éxito.')
                    ->with('file_url', $url);

        }

        return View::make('admin.planning.routes')
                    ->with('title', 'Reportes')
                    ->with('sub_title', 'Pedidos')
                    ->with('cities', $cities)
                    ->with('stores', $stores);
    }

    /**
    * Obtiene ruta para mostrar en mapa
    *
    * @param $route_id int ID de la ruta
    * @return $routes array Datos de ruta
    */
   private function get_route($route_id)
   {
        //obtener ruta
        $orders = Order::select('orders.id', 'delivery_time', 'user_address AS address', 'user_address_neighborhood AS neighborhood',
                        'user_address_latitude AS latitude', 'user_address_longitude AS longitude', 'planning_sequence','warehouses.warehouse','warehouses.id AS warehouse_id',
                        DB::raw("ROUND(planning_distance / 1000, 1) AS planning_distance"),
                        DB::raw("ROUND(planning_duration / 60) AS planning_duration"),
                        DB::raw("IFNULL(allied_stores.name, '') AS allied_store_name"),
                        'orders.type',
                        'route_id', 'zones.name AS zone', 'route', 'routes.shift', 'routes.zone_id')
                        ->join('routes', 'route_id', '=', 'routes.id')
                        ->join('order_groups', 'group_id', '=', 'order_groups.id')
                        ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
                        ->join('warehouses', 'warehouses.id', '=', 'routes.warehouse_id')
                        ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
                        ->leftJoin('allied_stores', 'allied_stores.id', '=', 'orders.allied_store_id')
                        ->where('routes.id', $route_id)
                        ->orderBy('planning_sequence')
                        ->get()
                        ->toArray();

        return $orders;
    }

    /**
    * Actualiza secuencia de ruta
    *
    * @param $route object Objeto ruta
    * @param $order_data array Datos de pedido con secuencia para iniciar desde
    * @return $response array Datos de respuesta
    */
   private function update_route($route, $order_data = null)
   {
        $total_orders = 0;
        $this->planning_date = $route->planning_date;
        $orders = Order::select('orders.id', 'orders.delivery_window_id', 'delivery_windows.delivery_window as delivery_time', 'user_address AS address', 'user_address_neighborhood AS neighborhood', 'user_city_id',
            'user_address_latitude AS latitude', 'user_address_longitude AS longitude', 'planning_sequence', 'zone_id', 'zones.name AS zone',
            'warehouses.latitude AS warehouses_latitude', 'warehouses.longitude AS warehouses_longitude',
            DB::raw("(6371 * ACOS(
            COS(RADIANS(warehouses.latitude))
            * COS(RADIANS(user_address_latitude))
            * COS(RADIANS(user_address_longitude)
            - RADIANS(warehouses.longitude))
            + SIN(RADIANS(warehouses.latitude))
            * SIN(RADIANS(user_address_latitude))
               )) AS distance"))
            ->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->join('warehouses', 'warehouses.id','=', 'order_groups.warehouse_id')
            ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
            ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
            ->where('orders.route_id', $route->id)
            ->orderBy('planning_sequence');
        if ($order_data)
            $orders = $orders->where('planning_sequence', '>=', $order_data['sequence'])->where('orders.id', '<>', $order_data['id']);
        $routes = $orders->get()->toArray();
        if (!count($routes)){
            $response = [
                'status' => false,
                'message' => 'No hay ordenes para esta ruta.'
            ];
            return $response;
        }
        $total_orders = count($routes);
        $total_routes[0] = $routes;

        //calcular secuencia de pedidos
        $planning_routes = $this->get_routes_orders_sequence($total_routes, $total_orders, $route->shift, $order_data['sequence'] ? $order_data['sequence'] : 0);
        if (isset($planning_routes['status'])) {
            $response = [
               'status' => false,
               'message' => $planning_routes['message']
            ];
            return $response;
        }

        //actualizar datos de pedido
        foreach ($planning_routes as $planning_route)
        {
            foreach ($planning_route as $order_route)
            {
                if ($order = Order::find($order_route['id']))
                {
                    $order->planning_sequence = $order_route['planning_sequence'];
                    $order->planning_duration = $order_route['planning_duration'];
                    $order->planning_distance = $order_route['planning_distance'];
                    $order->save();
                }
            }
        }
        $response = [
            'status' => true,
            'message' => 'Ruta actualizada'
        ];
        return $response;
   }



    /**
    *Obtiene las rutas creadas segun la fecha de planeacíón y el horario
    *
    * @param city_id int ID de la ciudad
    * @param $shift tiempo de entrega
    */
   private function get_routes_by_date_and_shift( $city_id, $shift )
   {
       return Routes::where('routes.city_id', $city_id)
           ->where('routes.warehouse_id', $this->warehouse_id)
           ->where('shift', $shift)
           ->where('planning_date', $this->planning_date)
           ->count();
   }


    /**
     * @param $shift_delivery_windows
     * @return array
     */
    private function get_order_per_window_index_array($shift_delivery_windows){
        $orders_per_window = [];
        foreach($shift_delivery_windows as $delivery_time_id)
        {
            $orders_per_window[$delivery_time_id]['qty'] = 0;
            $orders_per_window[$delivery_time_id]['index'] = 0;
        }
        return $orders_per_window;
    }

   /**
    * Procesa la planeacion de rutas
    *
    * @param $city_id int ID de la ciudad
    * @param $shift int ID del turno
    */
    private function process_planning_routes($city_id, $shift)
    {
        $shift_delivery_windows = $this->shift_delivery_windows[$shift];

        //Validar si ya se crearón las rutas para los pedidos de la mañana
        if($shift == 'PM') {
            $morning_routes = $this->get_routes_by_date_and_shift($city_id, 'AM');
            if(!$morning_routes){
                return [
                    'status' => false,
                    'message' => 'Debe crear primero las rutas para los pedidos de la mañana, vuelve a intentarlo.'
                ];
            }
        }

        $zones = Zone::select('zones.id', 'zones.orders_per_route', 'warehouses.latitude', 'warehouses.longitude', 'warehouses.city_id')
            ->join('warehouses', 'warehouses.id', '=', 'zones.warehouse_id')
            ->where('warehouses.city_id', $city_id)
            //->where('zones.id',121)
            ->orderBy('zones.planning_priority')
            ->get();

        $total_routes = [];
        foreach($zones as $zone)
        {
            $routes = [];
            $orders_per_route = $zone->orders_per_route;
            $maximum_orders_per_delivery_window = \ZoneDeliveryWindow::getMaximumOrdersPerRoute($zone->id);
            $orders = Routes::getOrderByZone($zone, $this->warehouse_id, $this->planning_date, $shift);
            $orders_per_window = $this->get_order_per_window_index_array($shift_delivery_windows);

            //$i = 0;
            foreach($shift_delivery_windows as $delivery_window_id){
                foreach ($orders as $order){
                    if($order['delivery_window_id'] == $delivery_window_id ){
                        if(!isset($maximum_orders_per_delivery_window[$delivery_window_id])){
                            $delivery_window = \DeliveryWindow::find($delivery_window_id);
                            return [
                                'status' => false,
                                'message' => 'No se ha establecido limite de pedidos por ruta para la franja horaria '.$delivery_window->shifts.' / '.$delivery_window->delivery_window
                            ];
                        }
                        $orders_per_delivery_window = $maximum_orders_per_delivery_window[$delivery_window_id];
                        $orders_per_window[$delivery_window_id]['qty']++;
                        $i = $orders_per_window[$delivery_window_id]['index'];

                        if(isset($routes[$i]) && count($routes[$i]) == $orders_per_route){
                            $orders_per_window[$delivery_window_id]['index'] ++;
                            $orders_per_window[$delivery_window_id]['qty'] = 0;
                            $i = $orders_per_window[$delivery_window_id]['index'];
                        }
                        if( $orders_per_window[$delivery_window_id]['qty'] >= $orders_per_delivery_window ){
                            $orders_per_window[$delivery_window_id]['index']++;
                            $orders_per_window[$delivery_window_id]['qty'] = 0;
                            $i = $orders_per_window[$delivery_window_id]['index'];

                        }
                        $routes[$i][] = $order;
                    }
                }
            }

            if($routes){
                foreach ($routes as $i => $route){
                    usort($route, function ($a, $b) {
                        if ($a['hour_start'] == $b['hour_start']) {
                            return 0;
                        }
                        return ($a['hour_start'] < $b['hour_start']) ? -1 : 1;
                    });
                    $routes[$i] = $route;
                }
                $total_routes[] = $routes;
            }
        }

        if(empty($total_routes)){
            return [
                    'status' => false,
                    'message' => 'No hay pedidos disponibles para realizar esta planeación. Por favor verifique.'
                ];
        }

        $planning_routes = [];
        foreach ($total_routes as $routes){
            $planning_routes[] = $planning_route = $this->get_routes_orders_sequence($routes, $orders_per_route, $shift);
            if (isset($planning_routes['status'])){
                return $response = [
                    'status' => false,
                    'message' => $planning_route['message']
                ];
            }
        }

        return $planning_routes;
    }

    /**
     * Retorna la hora con la que se calcular el trafico de la planeación
     * según Turno [AM, PM, MD]
     *
     * @param string $shift
     * @return string|null
     */
    private function getHourPlanningByShift( $shift)
    {
        $result = Config::get("app.planning.hours_by_shift.{$shift}");

        return $result === null ? '17:30:00' : $result;
    }

    /**
     * Calcula la secuencia de los pedidos en cada ruta
     *
     * @param array $routes Listado de rutas
     * @param int $orders_per_route Pedidos maximos por ruta
     * @param string $shift Turno a procesar
     * @param int $start_sequence Valor para empezar a asignar la secuencia
     * @return array $planning_routes Listado de rutas ordenadas por secuencia
     */
    private function get_routes_orders_sequence($routes, $orders_per_route, $shift, $start_sequence = 0)
    {
        $maximum_waypoints = 23;
        $planning_routes = [];
        $first_route = reset($routes);
        $first_order = reset($first_route);
        $city_id = $this->getCoverageStoreCityId($first_order['user_city_id']);
        $merqueo_location = $this->config['merqueo_location'][$city_id];

        $departure_hour = $this->getHourPlanningByShift($shift);
        $last_order_estimated_arrival_date = false;
        $last_delivery_time = false;
        $warehouse= !isset($this->warehouse_id) ? (object)['warehouse' => 'notPlanning']: Warehouse::find($this->warehouse_id);
        $logData = [
            'type' => 'following',
            'planning' => $this->planning_date.' '.$warehouse->warehouse.' '.$shift,
            'request' => 'Sequence,'.((int) $this->warehouse_id).'-'.$shift.','.date('d/m/Y'),
            'response' => '',
            'hour_start' => date("Y-m-d H:i:s"),
            'hour_end' => null
        ];
        $logData['response'] = 'Seguimiento (metodo planning proccess)'.json_encode($routes);
        $logData['hour_end'] = date("Y-m-d H:i:s");
        RoutesLog::add($logData);
        //calcular secuencia de pedidos
        foreach($routes as $orders) {
            $first_order = reset($orders);
            $delivery_window_id = $first_order['delivery_window_id'];
            $destinations = '';
            $n = count($orders);
            $cont = 0;
            $cont_destinations = 0;
            $cont_waypoints = 0;
            foreach ($orders as $index => $order) {
                $cont++;
                $cont_destinations++;
                $cont_waypoints++;
                $destinations .=  $this->routing_service->value ?
                    $order['latitude'] . ',' . $order['longitude'] . '|' : 
                    $order['longitude'] . ',' . $order['latitude'] . ';';

                $orders_destinations[] = $order;
                $merqueo_location['lat'] =  $order['warehouses_latitude'];
                $merqueo_location['lng'] =  $order['warehouses_longitude'];

                //optimizar ruta por horario cuando se completa los pedidos, cuando se completa el limite de waypoins o cuando cambia el horario
                if (
                $cont == $n 
                || $delivery_window_id != $orders[$index + 1]['delivery_window_id'] 
                || $cont_waypoints == $maximum_waypoints 
                || (isset($last_order_missing) 
                && count($orders_destinations) == $last_order_missing)) 
                {//punto de inicio de ruta
                    if (!isset($last_order_missing)) {
                        $origins = $this->routing_service->value ?
                         $merqueo_location['lat'] . ', ' . $merqueo_location['lng'] :
                         $merqueo_location['lng'] . ',' . $merqueo_location['lat'];
                    } else {
                        $origins = $last_order_location;
                    } //continuar con el ultimo pedido de la ruta anterior

                    //obtener distancia entre pedidos del horario
                    $logData = [
                        'type' => 'distancematrix',
                        'planning' => $this->planning_date.' '.$warehouse->warehouse.' '.$shift,
                        'request' => 'planeacion'.$shift.' '.$this->planning_date.' '.$origins.''.$destinations,
                        'response' => '',
                        'hour_start' => date("Y-m-d H:i:s"),
                        'hour_end' => null
                    ];
                    $response = Routes::getDistanceMatrix($origins, $destinations, $this->routing_service->value);
                    $logData['response'] = $shift.json_encode($response);
                    $logData['hour_end'] = date("Y-m-d H:i:s");
                    RoutesLog::add($logData);
                    
                    if (isset($response['rows'][0]['elements']))
                    {
                        foreach($response['rows'][0]['elements'] as $key => $data) {
                            if (!isset($data['distance'])) {
                                $order_group = OrderGroup::select('user_address_latitude', 'user_address_longitude')
                                                    ->join('orders', 'order_groups.id', '=', 'group_id')
                                                    ->where('orders.id', $orders_destinations[$key]['id'])
                                                    ->first();
                                if (!validate_latitude(trim($order_group->user_address_latitude))) {
                                    $order_group->user_address_latitude = null;
                                }
                                if (!validate_longitude(trim($order_group->user_address_longitude))) {
                                    $order_group->user_address_longitude = null;
                                }
                                $order_group->save();
                                return [
                                    'status' => false,
                                    'message' => 'El pedido # '.$orders_destinations[$key]['id'].
                                    ' tiene la coordenada incorrecta, el sistema la ha eliminado automaticamente, por favor editarla y asignarle la correcta.'
                                ];
                            }
                            $orders_destinations[$key]['distance'] = $data['distance']['value'];
                            $orders_destinations[$key]['origin'] = $origins;
                            $distances[] = $data['distance']['value'];
                        }
                    } else {
                        if (!$this->routing_service->value && !is_null($response)) {
                            foreach ($response['distances'][0] as $key => $data) {
                                if ($key !== 0) {
                                    $orders_destinations[$key - 1]['distance'] = $data;
                                    $orders_destinations[$key - 1]['origin'] = $origins;
                                    $distances[] = $data;
                                }
                                if (count($response['distances'][0]) == 1) {
                                    $orders_destinations[$key]['distance'] = $data;
                                    $orders_destinations[$key]['origin'] = $origins;
                                    $distances[] = $data;
                                }
                            }
                        } else if (!$this->routing_service->value && is_null($response)) {
                            return ['status' => false, 'message' => 'Ocurrió un error de OSRM : ' . $response['error']];
                        } else {
                            $message = isset($response['error_message']) ? ' / ' . $response['error_message'] : '';
                            return ['status' => false, 'message' => 'Ocurrió un error de Google Maps: ' . $response['status'] . ' / ' . $response['error_message']];
                        }
                    }
                    //ordenar por distancia entre pedidos del horario
                    $waypoints = '';
                    $delivery_routes = $validate_orders = [];
                    if (isset($last_order_missing)) { //si la ruta anterior quedo incompleta se ordena de mayor a menor (el mas lejano a merqueo se entrega primero)
                        arsort($distances, SORT_NUMERIC);
                    } else {
                        sort($distances, SORT_NUMERIC); //se ordena de menor a mayor para nueva ruta (el mas cercano a merqueo se entrega primero)
                    }

                    foreach($distances as $value) {
                        foreach($orders_destinations as $order_data) {
                            if ($order_data['distance'] == $value && !array_key_exists($order_data['id'], $validate_orders)) {
                                $validate_orders[$order_data['id']] = $order_data;
                                $delivery_routes[] = $order_data;
                                $waypoints .= $order_data['latitude'].','.$order_data['longitude'].'|';
                                break;
                            }
                        }
                    }
                    //obtener secuencia de pedidos
                    if (isset($last_order_missing)) { //si la ruta anterior quedo incompleta
                        //el origen es el ultimo pedido de la ruta anterior y el destino merqueo
                        $origins = $last_order_location;
                        $destination = $merqueo_location['lat'].', '.$merqueo_location['lng'];
                    } else {
                        if ($cont_destinations == $orders_per_route) { //si la ruta esta completa el destino es merqueo
                            $destination = $merqueo_location['lat'].', '.$merqueo_location['lng'];
                        } else { //sino el destino es el ultimo pedido del horario
                            $order = end($delivery_routes);
                            $destination = $order['latitude'].','.$order['longitude'];
                        }
                    }
                    $paramsList = [
                        'origin' => $this->routing_service->value ? $origins : explode(',',$origins)[1].','.explode(',',$origins)[0],
                        'destination' => $destination,
                        'waypoints' => 'optimize:true|'.trim($waypoints, '|'),
                        'departure_time' => strtotime($this->planning_date.' '.$departure_hour),
                        //'departure_time' => strtotime( date('Y-m-d 23:00:00') ),
                        'key' => Config::get('app.planning_google_api_key'),
                        'mode' => 'driving',
                        'language' => 'es-Es'
                    ];

                    $params = http_build_query($paramsList);
                    $logData = [
                        'type' => 'adddressService',
                        'planning' => $this->planning_date.' '.$warehouse->warehouse.' '.$shift,
                        'request' => 'planeacion'.$shift.','.$this->planning_date.' '.$params,
                        'response' => '',
                        'hour_start' => date("Y-m-d H:i:s"),
                        'hour_end' => null
                    ];
                    $response = Routes::getRouteGoogleDirection($params);
                    $logData['response'] = 'optimize'.json_encode($response);
                    $logData['hour_end'] = date("Y-m-d H:i:s");
                    RoutesLog::add($logData);

                    if (isset($response['routes'][0]['waypoint_order']) && !array_key_exists($response['routes'][0]['waypoint_order'][0], $delivery_routes)) {
                        $paramsList['waypoints'] = 'optimize:false|'.trim($waypoints, '|');
                        $params = http_build_query($paramsList);
                        $logData = [
                            'type' => 'adddressService',
                            'planning' => $this->planning_date.' '.$warehouse->warehouse.' '.$shift,
                            'request' => 'planeacion'.$shift.','.date('d/m/Y').$params,
                            'response' => '',
                            'hour_start' => date("Y-m-d H:i:s"),
                            'hour_end' => null
                        ];
                        $response = Routes::getRouteGoogleDirection($params);
                        $logData['response']='unoptimal'.json_encode($response);
                        $logData['hour_end']=date("Y-m-d H:i:s");
                        RoutesLog::add($logData);
                    }

                    if (isset($response['routes'][0]['waypoint_order'])) {
                        foreach($response['routes'][0]['waypoint_order'] as $key => $value) {
                            $delivery_routes[$value]['planning_distance'] = $response['routes'][0]['legs'][$key]['distance']['value'];
                            $delivery_routes[$value]['planning_duration'] = $response['routes'][0]['legs'][$key]['duration']['value'] + $this->seconds_to_add_per_order;

                            //calcular fecha de entrega estimada
                            $delivery_time = explode(' - ', $delivery_routes[$value]['delivery_time']);
                            $minutes = round($delivery_routes[$value]['planning_duration'] / 60);
                            if (!$last_order_estimated_arrival_date) {
                                $delivery_departure_hour = date('H:i:s', strtotime($this->planning_date.' '.$delivery_time[0].' -20 minutes'));
                                $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime($this->planning_date.' '.$delivery_departure_hour.' +'.$minutes.' minutes'));
                            }
                            //si la franja cambia y la hora de entrega del ultimo pedido es mayor a la hora de inicio de la franja tomar esa
                            if ($last_delivery_time && $last_delivery_time != $delivery_routes[$value]['delivery_time']) {
                                if ($last_order_estimated_arrival_date < date('Y-m-d H:i:s', strtotime($this->planning_date.' '.$delivery_time[0].' -20 minutes'))) {
                                    $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime($this->planning_date.' '.$delivery_time[0].' -20 minutes'));
                                } else { 
                                    $delivery_departure_hour = date('H:i:s', strtotime($last_order_estimated_arrival_date));
                                }
                            }
                            $delivery_routes[$value]['estimated_arrival_date'] = $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime($last_order_estimated_arrival_date.' +'.$minutes.' minutes'));
                            $last_delivery_time = $delivery_routes[$value]['delivery_time'];

                            //si la ruta anterior quedo incompleta se completa
                            if (isset($last_order_count) && $last_order_count < $orders_per_route) {
                                $last_order_sequence++;
                                $delivery_routes[$value]['planning_sequence'] = $last_order_sequence;
                                $planning_routes[count($planning_routes) - 1][$last_order_count] = $delivery_routes[$value];
                                $last_order_count++;
                                $cont_destinations = $last_order_count;
                                //echo '<pre>'; print_r($delivery_routes[$value]);
                            } else {
                                $delivery_routes[$value]['planning_sequence'] = $key + 1 + $start_sequence;

                                unset($last_order_location);
                                unset($last_order_sequence);
                                unset($last_order_count);
                                unset($last_order_missing);
                                $delivery_routes_new[] = $delivery_routes[$value];
                                //echo $delivery_routes[$value]['planning_sequence'].' - '.$delivery_routes[$value]['id'].'<br>';
                            }
                        }
                    } else {
                        $message = isset($response['error_message']) ? ' / '.$response['error_message'] : '';

                        return ['status' => false, 'message' => 'Ocurrió un error de Google Maps: '.$response['status'].$message];
                    }

                    //echo $cont_destinations.'<br>';
                    //si la ruta quedo incompleta se guarda coordenada del ultimo pedido
                    if ($cont != $n && $cont_destinations != $orders_per_route) {
                        $order = isset($delivery_routes_new) ? end($delivery_routes_new) : end($planning_routes[count($planning_routes) - 1]);
                        $last_order_location = $this->routing_service->value ?
                        $order['latitude'].','.$order['longitude']:
                        $order['longitude'].','.$order['latitude'];
                        $last_order_sequence = $order['planning_sequence'];
                        $last_order_count = $cont_destinations;
                        $last_order_missing = $orders_per_route - $last_order_count;
                    } else {
                        unset($last_order_location);
                        unset($last_order_sequence);
                        unset($last_order_count);
                        unset($last_order_missing);
                    }

                    if (isset($delivery_routes_new)) {
                        $planning_routes[] = $delivery_routes_new;
                    }

                    //limpiar variables para proxima ruta
                    $destinations = $waypoints = '';
                    $cont_destinations = 0;
                    $cont_waypoints = 0;
                    unset($delivery_routes);
                    unset($delivery_routes_new);
                    unset($distances);
                    unset($orders_destinations);
                }
                $delivery_window_id = isset($orders[$index + 1]['delivery_window_id']) ?
                $orders[$index + 1]['delivery_window_id'] :
                '';
            }
            $last_order_estimated_arrival_date = false;
            $last_delivery_time = false;
        }
        return $planning_routes;
    }

    /**
     * Envia mail con pedidos de planeacion de marketplace
     */
    private function send_report_mail($city_id, $planning_date, $allied_store_id, $shift, $order_type)
    {
        $rows = Routes::send_report_mail($city_id, $planning_date, $allied_store_id, $shift, $order_type);
        $allied_store = AlliedStore::find($allied_store_id);

        $path = public_path(Config::get('app.download_directory'));
        if ($gestor = opendir($path)){
            while (false !== ($file = readdir($gestor))){
                if (strstr($file, Session::get('admin_username')))
                    remove_file($path.$file);
            }
        }

        $objPHPExcel = new PHPExcel();
        //propiedades
        $objPHPExcel->getProperties()->setCreator('Merqueo');
        $objPHPExcel->getProperties()->setTitle('Pedidos Marketplace');
        $objPHPExcel->getProperties()->setSubject('Pedidos Marketplace');
        $objPHPExcel->setActiveSheetIndex(0);

        //estilos
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'5178A5'),
            ),
            'font' => array(
                'color' => array('rgb'=>'FFFFFF'),
                'bold' => true,
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A2:AZ2')->applyFromArray($style_header);

        //titulos
        $i = 0;
        foreach ($rows[0] as $key => $value) {
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, 2, $key);
            $i++;
        }

        $j = 3;
        foreach($rows as $row){
            $i = 0;
            foreach ($row as $key => $value){
                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow($i, $j, $value);
                $i++;
            }
            $j++;
        }

             //crear archivo
        $objPHPExcel->getActiveSheet()->setTitle('Pedidos Marketplace');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = Session::get('admin_username').' - '.date('Y-m-d H.i.s',time()).'.xlsx';
        $objWriter->save(public_path(Config::get('app.download_directory')).$filename);
        $file['real_path'] = public_path(Config::get('app.download_directory')).$filename;
        $file['client_original_name'] = $filename;
        $file['client_original_extension'] = 'xlsx';
        $url = upload_image($file, false, Config::get('app.download_directory_temp'));

        $mail['subject'] = 'Ruta de Marketplace para '. $allied_store->name;
        $mail['to'] = [];
        // $mail['to'][0]['email'] = 'arodriguez@merqueo.com';
        // $mail['to'][0]['name'] = 'Alejandro Rodriguez';
        $mail['to'][0]['email'] = 'eheredia@merqueo.com';
        $mail['to'][0]['name'] = 'Elvis Heredia';
        $mail['vars']['title'] = 'Ruta de Marketplace para '. $allied_store->name;
        $mail['vars']['html'] = '
            <h3>Ruta de Marketplace para '. $allied_store->name.'</h3>
            <p>
                Se envía archivo de excel adjunto con el detalle de la ruta.
            </p>
        ';
        $type = pathinfo($file['real_path'], PATHINFO_EXTENSION);
        $data = file_get_contents($file['real_path']);
        $base64 = base64_encode($data);
        $attachments[0]['type'] = 'application/vnd.ms-excel';
        $attachments[0]['name'] = $filename;
        $attachments[0]['content'] = $base64;

        send_mail($mail, $attachments);
    }

    /**
     * Obtiene los costos de la ruta.
     *
     * @param int $route_id Id de la ruta.
     */
    public function route_cost($route_id)
    {
        try {
            $route = Routes::findOrFail($route_id);
            $vehicle = $route->orders()
                ->whereStatus('Delivered')
                ->orderBy('planning_sequence')
                ->first()
                ->vehicle;
            $vehicle_type = $vehicle->class_type;
            $city_id = $vehicle->driver->transporter->city_id;
            $count_orders = $route->orders->count();
            $route_type = RouteType::whereCityId($city_id)
                ->whereRaw("{$count_orders} BETWEEN min AND max")
                ->where('vehicle_type', '=', $vehicle_type)
                ->first();
            if (!$route_type) {
                $route_type = RouteType::whereCityId($city_id)
                    ->where('vehicle_type', '=', $vehicle_type)
                    ->orderBy('max', 'desc')
                    ->first();
            }
        } catch (ModelNotFoundException $e) {
            $route = NULL;
            $vehicle = NULL;
            $route_type = NULL;
        }
        $route_cost_details = $route->routeCostDetails->toArray();
        if ($route_cost_details) {
            $description_keys = array_pluck($route_cost_details, 'description');
            list($keys, $values) = array_divide($route_cost_details);
            $route_cost_details = array_combine($description_keys, $values);
        }
        $route_status = $route->status_cost == 'Calculado';
        $route_cost = ($route_status) ? $route->base_cost : (is_numeric($vehicle->{$route_type->type}) ? $vehicle->{$route_type->type} : $route_type->cost);
        $vars = array(
            'total_orders_count' => $route->orders->count(),
            'route_status' => $route_status,
            'route_cost' => $route_cost,
            'costos' => $route_cost,
            'deducciones' => 0,
        );
        return View::make('admin.planning.routes.route_cost', $vars)
            ->with('route', $route)
            ->with('route_cost_details', $route_cost_details)
            ->with('vehicle', $vehicle)
            ->with('route_type', $route_type)
            ->with('title', 'Costos');
    }

    /**
     * Guarda los costos de la ruta.
     *
     * @param int $route_id Id de la ruta.
     */
    public function route_cost_save($route_id)
    {
        try {
            $route = Routes::findOrFail($route_id);
            $input = Input::all();
            $create_route_cost_detail = function($route_id, $type, $value, $description, $comments) {
                $route_cost_detail = new RouteCostDetail;
                $route_cost_detail->route_id = $route_id;
                $route_cost_detail->type = $type;
                $route_cost_detail->value = $value;
                $route_cost_detail->description = $description;
                $route_cost_detail->comments = $comments;
                $route_cost_detail->save();
                return $route_cost_detail;
            };
            $get_values = function($key, $type, $input, $increments) use ($route, $create_route_cost_detail) {
                $amount   = $input[$key . '_amount'];
                $comments = $input[$key . '_comments'];
                $route_cost_detail = $create_route_cost_detail($route->id, $type, $amount, $key, $comments);
                $increments = $increments + $amount;
                return $increments;
            };
            $route->routeCostDetails()->delete();
            $vehicle = $route->orders()
                ->whereStatus('Delivered')
                ->orderBy('planning_sequence')
                ->first()
                ->vehicle;
            $route_type = RouteType::findOrFail($input['route_type']);
            $count_orders = $route->orders->count();
            $route_cost = $input['base_cost']; //is_numeric($vehicle->{$route_type->type}) ? $vehicle->{$route_type->type} : $route_type->cost;
            $costs = $route_cost;
            $deductions = 0;
            //Costos
            if (isset($input['route_support'])) {
                $costs = $get_values('route_support', 'Costo', $input, $costs);
            }
            if (isset($input['recharge_extra_distance'])) {
                $costs = $get_values('recharge_extra_distance', 'Costo', $input, $costs);
            }
            if (isset($input['recharge_extra'])) {
                $costs = $get_values('recharge_extra', 'Costo', $input, $costs);
            }
            if (isset($input['toll'])) {
                $costs = $get_values('toll', 'Costo', $input, $costs);
            }
            if ($count_orders >= 36) {
                $surcharge_max_orders = ($count_orders - 35) * 5000;
                $costs += $surcharge_max_orders;
                $create_route_cost_detail($route->id, 'Costo', $surcharge_max_orders, 'surcharge_max_orders', NULL);
            }
            //Deducciones
            if (isset($input['penalty'])) {
                $deductions = $get_values('penalty', 'Deduccion', $input, $deductions);
            }
            if (isset($input['kangaroo'])) {
                $deductions = $get_values('kangaroo', 'Deduccion', $input, $deductions);
            }
            if (isset($input['baskets'])) {
                $deductions = $get_values('baskets', 'Deduccion', $input, $deductions);
            }
            if (isset($input['other'])) {
                $deductions = $get_values('other', 'Deduccion', $input, $deductions);
            }
            $total_route = $costs - $deductions;
            $shipping = ($vehicle->driver->transporter->type == 'Tercerizada') ? $route_cost * 0.2 : 0;
            $total = $total_route + $shipping;
            $route->route_type_id = $route_type->id;
            $route->base_cost = $route_cost;
            $route->total_cost = $total;
            $route->status_cost = 'Calculado';
            $route->cost_date = Carbon::now();
            $route->save();
            return Redirect::route('adminRoutes.routesCost', ['route_id' => $route_id])
                ->with('type', 'success')
                ->with('message', 'Costos de la ruta guardados correctamente.');
        } catch (ModelNotFoundException $e) {
            return Redirect::route('adminRoutes.routesCost', ['route_id' => $route_id])
                ->with('type', 'error')
                ->with('message', 'Ocurrió un error cargando los datos para calcular los costos.');
        }
    }

    /**
     * Exporta la plantilla para el importador de costos
    */
    public function download_template_cost()
    {
        $input = Input::all();
        try {
            $objPHPExcel = new PHPExcel();
            //propiedades
            $objPHPExcel->getProperties()->setCreator('Merqueo');
            $objPHPExcel->getProperties()->setTitle('Costos');
            $objPHPExcel->getProperties()->setSubject('Costos');
            $objPHPExcel->setActiveSheetIndex(0);

            //estilos
            $style_header = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '5178A5'),
                ),
                'font' => array(
                    'color' => array('rgb' => 'FFFFFF'),
                    'bold' => true,
                )
            );
            $style_header_cost = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '00CC00'),
                ),
                'font' => array(
                    'color' => array('rgb' => 'FFFFFF'),
                    'bold' => true,
                )
            );

            $style_header_deduction = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'CC0000'),
                ),
                'font' => array(
                    'color' => array('rgb' => 'FFFFFF'),
                    'bold' => true,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($style_header);
            $objPHPExcel->getActiveSheet()->getStyle('D1:K1')->applyFromArray($style_header_cost);
            $objPHPExcel->getActiveSheet()->getStyle('L1:S1')->applyFromArray($style_header_deduction);

            //Header de la hoja de calculo
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, 'Ruta');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 1, 'Placa');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, 1, 'Costo de la ruta');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, 1, '¿Tuvo apoyo a la ruta?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, 1, 'Comentario ¿Tuvo apoyo a la ruta?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, 1, '¿Tiene recargo de distancia extra?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, 1, 'Comentario ¿Tiene recargo de distancia extra?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, 1, '¿Tiene recargo extra?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, 1, 'Comentario ¿Tiene recargo extra?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, 1, 'Peaje');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(10, 1, 'Comentario Peaje');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(11, 1, '¿Tuvo multas?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(12, 1, 'Comentario ¿Tuvo multas?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(13, 1, '¿Tuvo canguro?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(14, 1, 'Comentario ¿Tuvo canguro?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(15, 1, '¿Tuvo productos y/o canastillas no devueltas?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(16, 1, 'Comentario ¿Tuvo productos y/o canastillas no devueltas?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(17, 1, '¿Otro?');
            $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(18, 1, 'Comentario ¿Otro?');

            $start_date = format_date('mysql', $input['start_date']);
            $end_date = format_date('mysql', $input['end_date']);

            $routes = Routes::whereStatus('Terminada')
                ->where('city_id', $input['city_id'])
                ->whereBetween('planning_date', [$start_date, $end_date])
                ->orderBy('routes.planning_date', 'desc')
                ->orderBy('routes.shift', 'desc')
                ->orderBy('picking_priority')->get();
            if ($routes->count()) {
                $i = 2;
                foreach ($routes as $route) {
                    $vehicle = $route->orders()
                        //->whereStatus('Delivered')
                        ->orderBy('planning_sequence')
                        ->first()->vehicle;
                    $vehicle_type = $vehicle->class_type;
                    $city_id = $vehicle->driver->transporter->city_id;
                    $count_orders = $route->orders->count();
                    $route_type = RouteType::whereCityId($city_id)
                        ->whereRaw("{$count_orders} BETWEEN min AND max")
                        ->where('vehicle_type', '=', $vehicle_type)
                        ->first();
                    if (!$route_type) {
                        $route_type = RouteType::whereCityId($city_id)
                            ->where('vehicle_type', '=', $vehicle_type)
                            ->orderBy('max', 'desc')
                            ->first();
                    }
                    $route_status = $route->status_cost == 'Calculado';
                    $route_cost = ($route_status) ? $route->base_cost : (is_numeric($vehicle->{$route_type->type}) ? $vehicle->{$route_type->type} : $route_type->cost);

                    //Body de la hoja de calculo
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, $route->route);
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $vehicle->plate);
                    $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $route_cost);
                    $i++;
                }
            } else {
                return Redirect::route('adminRoutes.index')->with('type', 'error')->with('message', 'La plantilla no se pudo generar ya que en las fechas que elegiste no se encuentran rutas en estado Terminado');
            }
            //crear archivo
            $objPHPExcel->getActiveSheet()->setTitle('Costos');
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $filename = Session::get('admin_username') . ' - ' . $start_date . ' to ' . $end_date . '.xlsx';
            $objWriter->save(public_path(Config::get('app.download_directory')) . $filename);
            $file['real_path'] = public_path(Config::get('app.download_directory')) . $filename;
            $file['client_original_name'] = $filename;
            $file['client_original_extension'] = 'xlsx';
            $url = upload_image($file, false, Config::get('app.download_directory_temp'));


            return Redirect::route('adminRoutes.index')->with('type', 'success')->with('message', 'Plantilla descargada con éxito.')->with('file_url', $url);
        }catch (ModelNotFoundException $e) {
            return Redirect::route('adminRoutes.index')->with('type', 'error')->with('message', $e->getMessage());
        }
    }

    /**
     * Importa costos de rutas
     */
    public function import_costs()
    {
        $csv_file = Input::all()['csv_file'];
        if($csv_file->getClientMimeType() != 'text/csv' && $csv_file->getClientMimeType() != 'application/vnd.ms-excel' && $csv_file->getClientMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            return Redirect::route('adminRoutes.index')
                ->with('type', 'error')
                ->with('message', 'Debes cargar un archivo con extensiones validas. Ej:csv,xls,xlsx');
        }
        if(Input::hasFile('csv_file')) {
            $file = Input::file('csv_file')->getRealPath();
            Excel::load($file, function($reader) {
                $results = $reader->get();
                try {
                    foreach ($results->getIterator() as $row) {
                        $route = Routes::where('routes.route', $row->ruta)->first();
                        if ($route) {
                            $create_route_cost_detail = function ($route_id, $type, $value, $description, $comments) {
                                $route_cost_detail = new RouteCostDetail;
                                $route_cost_detail->route_id = $route_id;
                                $route_cost_detail->type = $type;
                                $route_cost_detail->value = $value;
                                $route_cost_detail->description = $description;
                                $route_cost_detail->comments = $comments;
                                $route_cost_detail->save();
                                return $route_cost_detail;
                            };
                            $get_values = function ($key, $type, $amount, $comments, $increments) use ($route, $create_route_cost_detail) {
                                $route_cost_detail = $create_route_cost_detail($route->id, $type, $amount, $key, $comments);
                                $increments = $increments + $amount;
                                return $increments;
                            };
                            $route->routeCostDetails()->delete();
                            $vehicle = $route->orders()
                                //->whereStatus('Delivered')
                                ->orderBy('planning_sequence')
                                ->first()
                                ->vehicle;
                            $vehicle_type = $vehicle->class_type;
                            $city_id = $vehicle->driver->transporter->city_id;
                            $count_orders = $route->orders->count();
                            $route_type = RouteType::whereCityId($city_id)
                                ->whereRaw("{$count_orders} BETWEEN min AND max")
                                ->where('vehicle_type', '=', $vehicle_type)
                                ->first();
                            if (!$route_type) {
                                $route_type = RouteType::whereCityId($city_id)
                                    ->where('vehicle_type', '=', $vehicle_type)
                                    ->orderBy('max', 'desc')
                                    ->first();
                            }
                            $route_cost = $row->costo_de_la_ruta;
                            $costs = $route_cost;
                            $deductions = 0;
                            //Costos
                            if (!is_null($row->tuvo_apoyo_a_la_ruta)) {
                                $costs = $get_values('route_support', 'Costo', $row->tuvo_apoyo_a_la_ruta, $row->comentario_tuvo_apoyo_a_la_ruta, $costs);
                            }
                            if (!is_null($row->tiene_recargo_de_distancia_extra)) {
                                $costs = $get_values('recharge_extra_distance', 'Costo', $row->tiene_recargo_de_distancia_extra, $row->comentario_tiene_recargo_de_distancia_extra, $costs);
                            }
                            if (!is_null($row->tiene_recargo_extra)) {
                                $costs = $get_values('recharge_extra', 'Costo', $row->tiene_recargo_extra, $row->comentario_tiene_recargo_extra, $costs);
                            }
                            if (!is_null($row->peaje)) {
                                $costs = $get_values('toll', 'Costo', $row->peaje, $row->comentario_peaje, $costs);
                            }
                            if ($count_orders >= 36) {
                                $surcharge_max_orders = ($count_orders - 35) * 5000;
                                $costs += $surcharge_max_orders;
                                $create_route_cost_detail($route->id, 'Costo', $surcharge_max_orders, 'surcharge_max_orders', NULL);
                            }
                            //Deducciones
                            if (!is_null($row->tuvo_multas)) {
                                $deductions = $get_values('penalty', 'Deduccion', $row->tuvo_multas, $row->comentario_tuvo_multas, $deductions);
                            }
                            if (!is_null($row->tuvo_canguro)) {
                                $deductions = $get_values('kangaroo', 'Deduccion', $row->tuvo_canguro, $row->comentario_tuvo_canguro, $deductions);
                            }
                            if (!is_null($row->tuvo_productos_yo_canastillas_no_devueltas)) {
                                $deductions = $get_values('baskets', 'Deduccion', $row->tuvo_productos_yo_canastillas_no_devueltas, $row->comentario_tuvo_productos_yo_canastillas_no_devueltas, $deductions);
                            }
                            if (!is_null($row->otro)) {
                                $deductions = $get_values('other', 'Deduccion', $row->otro, $row->comentario_otro, $deductions);
                            }
                            $total_route = $costs - $deductions;
                            $shipping = ($vehicle->driver->transporter->type == 'Tercerizada') ? $route_cost * 0.2 : 0;
                            $total = $total_route + $shipping;
                            $route->route_type_id = $route_type->id;
                            $route->base_cost = $route_cost;
                            $route->total_cost = $total;
                            $route->status_cost = 'Calculado';
                            $route->cost_date = Carbon::now();
                            $route->save();
                        }
                    }
                }catch (ModelNotFoundException $e) {
                    return Redirect::route('adminRoutes.index')
                        ->with('type', 'error')
                        ->with('message', $e->getMessage());
                }
            });
            return Redirect::route('adminRoutes.index')
                ->with('type', 'success')
                ->with('message', 'Costos importados correctamente.');
        } else {
            return Redirect::route('adminRoutes.index')
                ->with('type', 'error')
                ->with('message', 'No seleccionaste ningun archivo.');
        }
    }

    /**
     * Actualizar el vehiculo sugerido a la ruta
     */
    public function update_vehicle_orders()
    {
        $id = Input::get('route_id');
        $route = Routes::find($id);
        $route->suggested_vehicle_id = Input::get('vehicle_id');
        $route->save();
        \AdminLog::saveLog('routes',$id, 'update_suggested_vehicle');
        return;
    }
}
