<?php


namespace admin;

use Request, View, Input, Redirect, Session, DB, Str, Validator, AlliedStore, City, Response, Config;


class AdminAlliedStoreController extends AdminController
{
    /**
     * Grilla de tiendas aliadas
     */
    public function index()
    {
        $allied_stores = AlliedStore::select('allied_stores.*', 'cities.city', 'cities.city')->join('cities', 'cities.id', '=', 'allied_stores.city_id');

        if (!Request::ajax()) {
            $allied_stores = $allied_stores->where('allied_stores.status', 1)->get();
            return View::make('admin.allied_stores.index')
                ->with('title', 'Tiendas Aliadas')
                ->with('allied_stores', $allied_stores);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            if ($search != '')
                $allied_stores = $allied_stores->where('allied_stores.name', 'LIKE', '%'.$search.'%');

            $allied_stores = $allied_stores->where('allied_stores.status', 1)->get();

            return View::make('admin.allied_stores.index')
                ->with('title', 'Tiendas Aliadas')
                ->with('allied_stores', $allied_stores)
                ->renderSections()['content'];
        }
    }

    /**
     * Editar tienda aliada
     */
    public function edit()
    {
        $id = Request::segment(4) ? Request::segment(4) : 0;
        $sub_title = Request::segment(4) ? 'Editar Tienda Aliada' : 'Nueva Tienda Aliada';
        $providers = \Provider::select('id', 'name')->orderBy('name')->get();

        if (!$id && Session::has('post')){
            $allied_store = (object) Session::get('post');
            $allied_store->id = 0;
        }else $allied_store = AlliedStore::find($id);

        if (Session::get('admin_designation') != 'Super Admin')
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        else $cities = City::all();

        return View::make('admin.allied_stores.allied_store_form')
            ->with('title', 'Tienda Aliada')
            ->with('sub_title', $sub_title)
            ->with('cities', $cities)
            ->with('allied_store', $allied_store)
            ->with(compact('providers'));
    }

    /**
     * Guardar tienda aliada
     */
    public function save()
    {
        $id = Input::get('id');
        $flag = 0;
        $allied_store = AlliedStore::find($id);
        if (!$allied_store) {
            $allied_store = new AlliedStore;
            $flag = 1;
            $action = 'insert';
        }else {
            $action = 'update';
        }
        $allied_store->name = Input::get('name');
        $allied_store->nit = Input::get('nit');
        $allied_store->phone = Input::get('phone');
        $allied_store->city_id = Input::get('city_id');
        $allied_store->schedule = Input::get('schedule');
        $allied_store->delivery_time_hours = Input::get('delivery_time_hours');
        $allied_store->transporter = Input::get('transporter');
        $allied_store->address = Input::get('address');
        $allied_store->status = Input::get('status');
        $allied_store->email = Input::get('email');
        $allied_store->provider_id = Input::get('provider_id');
        $latlng = Input::get('latlng');
        if (!empty($latlng)) {
            $latlng = explode(',', trim($latlng));
            if (count($latlng) == 2 && validate_longitude(trim($latlng[0])) && validate_latitude(trim($latlng[1]))) {
                $allied_store->latitude = trim($latlng[0]);
                $allied_store->longitude = trim($latlng[1]);
            }else return Redirect::route('adminAlliedStores.index')->with('type', 'failed')->with('message', 'La coordenada de la dirección no es valida.');
        }

        $image = Input::file('app_logo_small');
        $validator = Validator::make(
            array('ima' => $image),
            array('ima' => 'required|mimes:jpeg,bmp,png')
        );

        if (Input::hasFile('app_logo_small') && $validator->fails()) {
            $error_messages = $validator->messages();
            foreach ($error_messages->all() as $message)
                return Redirect::route('adminAlliedStores.index')->with('type', 'failed')->with('message', $message);
        }

        $cloudfront_url = Config::get('app.aws.cloudfront_url');

        if (Input::hasFile('app_logo_small')) {
            if (!empty($allied_store->app_logo_small_url) && !strstr($allied_store->app_logo_small_url, 'default')){
                $path = str_replace($cloudfront_url, uploads_path(), $allied_store->app_logo_small_url);
                remove_file($path);
                remove_file_s3($allied_store->app_logo_small_url);
            }
            $allied_store->app_logo_small_url = upload_image(Input::file('app_logo_small'), false, 'allied-stores/logos');
        }else
            $allied_store->app_logo_small_url = $cloudfront_url.'/allied-stores/logos/default_small_allied_store_logo.png';

        $allied_store->save();
        $this->admin_log('allied_stores', $allied_store->id, $action);

        $allied_store->city_id = Input::get('city_id');

        return Redirect::route('adminAlliedStores.index')->with('type', 'success')->with('message', 'Tienda aliada guardada con éxito.');
    }

    /**
     * Eliminar tienda aliada
     */
    public function delete()
    {
        $id = Request::segment(4);
        try {
            DB::beginTransaction();

            AlliedStore::where('id', $id)->update(array('status' => 0));
            $this->admin_log('allied_stores', $id, 'del
            ete');

            DB::commit();
            return Redirect::route('adminAlliedStores.index')->with('type', 'success')->with('message', 'Tienda aliada eliminada con éxito.');

        }catch(\Exception $e){
            DB::rollback();
            return Redirect::route('adminAlliedStores.index')->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar la tienda aliada.');
        }
    }
}