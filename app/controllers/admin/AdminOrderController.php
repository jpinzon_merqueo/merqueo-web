<?php

namespace admin;

use Admin, App, Request, View, Input, Route, Redirect, Session, DB, Config, DateTime, Response, Validator, Order, OrderProduct, Shopper, User, CampaignLog,
UserCredit, UserCreditCard, UserAddress, Coupon, UserCoupon, Store, Product, OrderNoteSms, OrderLog, OrderGroup, RejectReason, City, Payment, OrderStore,
UserBrandCampaign, UserFreeDelivery, StoreBranch, Carbon\Carbon, Zone, Pdf, ShopperMovement, Supermarket,Exception, StoreBranchSlot, Holidays, Siftscience,
AnalyticsClient, Log, Pusherer, Merqueo\Leanplum\LeanplumNotification;
use usecases\contracts\orders\AddCreditWhenDeliveryLateInterface;

/**
 * Class AdminOrderController
 * @package admin
 * @deprecated
 */
class AdminOrderController extends AdminController
{

    /**
     * @var AddCreditWhenDeliveryLateInterface
     */
    private $addCreditWhenDeliveryLate;

    public function __construct(AddCreditWhenDeliveryLateInterface $addCreditWhenDeliveryLate)
    {
        $this->addCreditWhenDeliveryLate = $addCreditWhenDeliveryLate;
    }

    /**
     * Grilla de pedidos
     */
    public function index()
    {
        if (!Request::ajax()) {
            $shoppers = Shopper::where('status', 1);
            $zones = Zone::where('status', 1);
            $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('is_storage', 0)->where('stores.status', 1);

            if (Session::get('admin_designation') == 'Cliente') {
                $cities = City::where('id', Session::get('admin_city_id'))->get();
                $stores = $stores->where('stores.id', Session::get('admin_designation_store_id'))->get();
            } else {
                if (Session::get('admin_designation') == 'Super Admin') {
                    $stores = $stores->get();
                    $cities = City::all();
                } else {
                    $shoppers->where('city_id', Session::get('admin_city_id'));
                    $zones->where('city_id', Session::get('admin_city_id'));
                    $stores = $stores->where('city_id', Session::get('admin_city_id'))->get();
                    $cities = City::where(function($query){
                        $query->where('id', Session::get('admin_city_id'));
                        $query->orWhere('parent_city_id', Session::get('admin_city_id'));
                    })->get();
                }
            }

            $shoppers = $shoppers->orderBy('first_name')->orderBy('last_name')->get();
            $zones = $zones->orderBy('name')->get();

            return View::make('admin.orders.index')->with('title', 'Pedidos de Express')->with('shoppers', $shoppers)
                        ->with('stores', $stores)->with('payment_methods', $this->config['payment_methods'])
                        ->with('cities', $cities)->with('zones', $zones);
        } else {
            $search = Input::has('s') ? Input::get('s') : '';
            $orders = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->leftJoin('shoppers', 'orders.shopper_id', '=', 'shoppers.id')
                ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
                ->leftJoin(DB::raw('(user_credits, coupons)'), function ($join) {
                    $join->on('user_credits.order_id', '=', 'orders.id');
                    $join->on('user_credits.type', '=', DB::raw('0'));
                    $join->on('user_credits.coupon_id', '=', 'coupons.id');
                })
                ->where('stores.is_storage', 0)
                ->where(function ($query) use ($search) {
                    $query->where(DB::raw('CONCAT(order_groups.user_firstname, " ", order_groups.user_lastname)'), 'LIKE', '%'.$search.'%');
                    $query->orWhere('orders.id', 'LIKE', '%'.$search.'%');
                    $query->orWhere('orders.reference', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_groups.user_email', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_groups.user_address', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_groups.user_phone', 'LIKE', '%'.$search.'%');
                })
                ->select('orders.*', 'users.sift_payment_abuse', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email', 'order_groups.user_address',
                'order_groups.user_address_further', 'order_groups.user_address_neighborhood', 'order_groups.user_phone',
                'order_groups.user_comments', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source', 'order_groups.source_os', 'zones.name AS zone',
                'shoppers.first_name', 'shoppers.last_name', 'cities.city', 'stores.name AS store', 'coupons.code AS coupon_code', 'delivery_time_minutes AS store_delivery_time_minutes',
                DB::raw("ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), orders.delivery_date))/3600, 1) AS diff"), 'posible_fraud', DB::raw("SUM(orders.total_amount) AS total_amount_og"));
            //filtros
            if (Input::get('store_id')) {
                $orders->where('stores.id', Input::get('store_id'));
            }
            if (Input::get('revision')) {
                $orders->where('is_checked', Input::get('revision'));
            }
            if (Input::get('shopper_id')) {
                $orders->where('orders.shopper_id', Input::get('shopper_id'));
            }
            if (Input::get('zone_id')) {
                $orders->where('zones.id', Input::get('zone_id'));
            }
            if (Input::get('show_orders')) {
                if (Input::get('show_orders') == 'today') {
                    $orders->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'));
                    $orders->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'));
                }
            }
            if (Input::get('status')) {
                $status = Input::get('status');
                $statuses = explode(',', $status);
                unset($statuses[count($statuses) - 1]);
                $orders->where(function ($query) use ($statuses) {
                    foreach ($statuses as $status) {
                        $query->orWhere('orders.status', $status);
                    }
                });
            }
            if (Input::get('city_id')) {
                $city_id = Input::get('city_id');
                $orders->where('cities.id', $city_id);
            }
            if (Input::get('source')) {
                $orders->where('source', Input::get('source'));
            }
            if (Input::get('source_os')) {
                $orders->where('source_os', Input::get('source_os'));
            }
            if (Input::get('order_by')) {
                if (Input::get('order_by') == 'unfulfilled_orders') {
                    $orders->orderBy(DB::raw("FIELD(orders.status, 'Validation', 'Initiated', 'In Progress', 'Dispatched', 'Delivered', 'Cancelled')"))->orderBy('diff', 'desc');
                } else {
                    $orders->orderBy(Input::get('order_by'), 'desc');
                }
            }
            if (Input::get('payment_method')) {
                $orders->where('orders.payment_method', Input::get('payment_method'));
            }

            /*if (Session::get('admin_designation') != 'Super Admin') {
                $orders->where('order_groups.user_city_id', Session::get('admin_city_id'));
            }*/
            if (Session::get('admin_designation') == 'Cliente') {
                $orders->where('orders.store_id', Session::get('admin_designation_store_id'));
            }
            if (Session::get('admin_designation_store_id') == 26) {
                $orders->where('order_groups.source', 'Callcenter');
            }

            $orders = $orders->groupBy('orders.id')->limit(80)->get();

            if (count($orders)) {
                $suspect_credit_card_order_amount = Config::get('app.suspect_credit_card_order_amount');
                foreach ($orders as $order) {
                    //validar pedidos sospechosos
                    if ($order->status != 'Cancelled' && $order->status != 'Delivered' && $order->payment_method == 'Tarjeta de crédito'
                    && $order->total_amount_og > $suspect_credit_card_order_amount) {
                        $order->suspect = 1;
                    } else {
                        $order->suspect = 0;
                    }
                    //obtener numero de pedidos por cliente
                    $qty = Order::where('user_id', $order->user_id)->where('user_id', '<>', 5169)->where('status', '=', 'Delivered')->count();
                    $order->orders_qty = $qty;
                }
            }

            /*$queries = DB::getQueryLog();
            $last_query = end($queries);
            debug($last_query);*/
        }

        return View::make('admin.orders.index')
                    ->with('title', 'Pedidos')
                    ->with('orders', $orders)
                    ->renderSections()['content'];
    }

    /**
     * Detalle de un pedido
     */
    public function details($id)
    {
        $order = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->join('cities AS cities_user', 'cities_user.id', '=', 'order_groups.user_city_id')
                ->join('stores', 'stores.id', '=', 'orders.store_id')
                ->join('cities AS cities_store', 'cities_store.id', '=', 'stores.city_id')
                ->join('users', 'order_groups.user_id', '=', 'users.id')
                ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                ->leftJoin('user_credit_cards', 'orders.credit_card_id', '=', 'user_credit_cards.id')
                ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
                ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
                ->leftJoin(DB::raw('(user_credits, coupons)'), function ($join) {
                    $join->on('user_credits.order_id', '=', 'orders.id');
                    $join->on('user_credits.type', '=', DB::raw('0'));
                    $join->on('user_credits.coupon_id', '=', 'coupons.id');
                    $join->on('user_credits.status', '=', DB::raw('1'));
                })
                ->leftJoin('shopper_movements', function ($join) {
                    $join->on('shopper_movements.order_id', '=', 'orders.id');
                    $join->on('shopper_movements.status', '=', DB::raw('1'));
                })
                ->where('orders.id', $id)
                ->where('stores.is_storage', 0)
                ->select('orders.*', 'users.sift_payment_abuse', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email', 'coupons.code AS coupon_code',
                'order_groups.user_address', 'order_groups.user_address_further', 'order_groups.user_address_neighborhood', 'order_groups.user_phone', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source',
                'stores.name AS store_name', 'stores.id AS store_id', 'cities_store.city AS store_city', 'zones.id AS zone_id', 'zones.name AS zone', 'orders.invoice_number', 'stores.revision_orders_required',
                'order_groups.source_os', 'order_groups.user_comments', 'cities_user.city', 'cities_user.id AS city_id', DB::raw("SUM(orders.total_amount) AS total_amount_og"),
                'user_brand_campaigns.campaign', 'order_groups.user_address_latitude', 'order_groups.user_address_longitude', 'user_cash_paid', 'user_card_paid');


        if (Session::get('admin_designation') != 'Super Admin') {
            $order->where('order_groups.user_city_id', Session::get('admin_city_id'));
        }
        if (Session::get('admin_designation') == 'Cliente') {
            $order->where('orders.store_id', Session::get('admin_designation_store_id'));
        }

        $order = $order->first();

        if (!$order->id) {
            return App::abort(404);
        }

        //validar pedido sospechoso
        $suspect_credit_card_order_amount = Config::get('app.suspect_credit_card_order_amount');
        if ($order->status != 'Cancelled' && $order->status != 'Delivered' && $order->payment_method == 'Tarjeta de crédito' && $order->total_amount_og > $suspect_credit_card_order_amount) {
            $order->suspect = 'El pedido es con tarjeta de crédito y el total agrupado por tienda es mayor a $'.number_format($suspect_credit_card_order_amount, 0, ',', '.');
        } else {
            $order->suspect = 0;
        }

        $order_products = OrderProduct::where('order_id', $order->id)->leftJoin('products', 'products.id', '=', 'order_products.store_product_id')
                                    ->join('stores', 'order_products.store_id', '=', 'stores.id')
                                    ->select('order_products.*', 'stores.name AS store_name', 'products.reference')
                                    ->orderBy('store_id', 'desc')->get();
        $order_stores = OrderProduct::where('order_products.order_id', $order->id)->join('stores', 'order_products.store_id', '=', 'stores.id')
                                    ->leftJoin('order_stores', function ($join) {
                                        $join->on('order_stores.order_id', '=', 'order_products.order_id');
                                        $join->on('order_stores.store_id', '=', 'order_products.store_id');
                                    })
                                    ->leftJoin('admin', function ($join) {
                                        $join->on('order_stores.admin_id', '=', 'admin.id');
                                    })
                                    ->select('order_stores.*', 'order_products.store_id', 'stores.name', 'fullname AS supermarket_authorizing_admin')
                                    ->groupBy('stores.id')->get();

        //verificar pedidos pendientes por factura y obtener total de factura
        $order->total_real_amount = 0;
        $order_has_pending_total_real_amount = false;
        foreach ($order_stores as $order_store) {
            $order->total_real_amount += $order_store->total_real_amount;
            if (empty($order_store->total_real_amount)) {
                $order_has_pending_total_real_amount = true;
            }
        }
        $order_notes = OrderNoteSms::select('order_notes_sms.created_at', 'order_notes_sms.description', 'admin.fullname')
                                    ->join('admin', 'admin.id', '=', 'admin_id')->where('order_id', $order->id)->where('order_notes_sms.type', 'note')
                                    ->orderBy('created_at', 'desc')->get();
        if ($order->user_id != 5169) { //usuario domicilios
            $customer_orders = Order::select('orders.*', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email', 'order_groups.user_address', 'order_groups.user_phone',
                                 'order_groups.user_address_further', 'order_groups.user_comments', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source', 'order_groups.source_os', 'stores.name AS store')
                                 ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                                 ->join('stores', 'stores.id', '=', 'orders.store_id')
                                 ->where('orders.user_id', $order->user_id)->where('orders.id', '<>', $order->id)->orderBy('orders.created_at', 'desc')->get();
        } else {
            $customer_orders = array();
        }
        $customer_credits = UserCredit::where('user_id', $order->user_id)->where('status', 1)->orderBy('created_at', 'desc')->get();
        $customer_credit_cards = UserCreditCard::where('user_id', $order->user_id)->get();
        $customer_free_deliveries = UserFreeDelivery::where('user_id', $order->user_id)->where('status', 1)->orderBy('created_at', 'desc')->get();

        $shoppers_by_order_zone = $this->get_shoppers_by_order_zone($order);
        $shoppers_to_select = $this->get_shoppers_to_select($order);

        $shopper = array();
        if ($order->shopper_id) {
            $shopper = $shoppers_to_select['shoppers'][$order->shopper_id];
        }

        $order_log = OrderLog::select(DB::raw("CONCAT(shoppers.first_name,' ', shoppers.last_name) AS 'shopper_name'"),
                                 DB::raw("CONCAT(users.first_name,' ', users.last_name) AS 'user_name'"),
                                 DB::raw("admin.fullname AS 'admin_name'"), 'order_logs.type', 'order_logs.created_at')
                                ->leftJoin('shoppers', 'order_logs.shopper_id', '=', 'shoppers.id')
                                ->leftJoin('users', 'order_logs.user_id', '=', 'users.id')
                                ->leftJoin('admin', 'order_logs.admin_id', '=', 'admin.id')
                                ->where('order_logs.order_id', $order->id)
                                ->get();
        $cities = City::all();

        $holidays = new Holidays();
        $day_num = date('N');
        $date_data = explode('-', date('Y-m-d'));
        $holidays->festivos($date_data[0]);
        $is_holiday = $holidays->esFestivo($date_data[2], $date_data[1]);
        if ($is_holiday) {
            $day_num = 0;
        }

        $hour = date('H:i:s');
        $store_branches = StoreBranch::where('store_id', $order->store_id)
                            ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                            ->where('store_branches.status', 1)
                            ->select('store_branches.*', 'stores.name AS store_name')
                            ->get()
                            ->toArray();

        foreach ($store_branches as $key => &$branch) {
            $branch_slot = StoreBranchSlot::where('store_branch_id', DB::raw($branch['id']))
                            ->where('day', DB::raw($day_num))
                            ->whereRaw('start_time <= "'.$hour.'"')
                            ->whereRaw('end_time >= "'.$hour.'"')
                            ->get()->toArray();
            if (empty($branch_slot)) {
                unset($store_branches[$key]);
            } else {
                $branch['schedule'] = $branch_slot;
            }
        }

        $supermarket_ids = array_fetch($store_branches, 'supermarket_id');
        $supermarket_ids = array_unique($supermarket_ids);
        $supermarkets['store'] = Supermarket::whereIn('id', $supermarket_ids)->where('status', 1)->get();
        $supermarkets['others'] = Supermarket::select('supermarkets.name')
                                        ->join('store_branches', 'store_branches.supermarket_id', '=', 'supermarkets.id')
                                        ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                                        ->whereNotIn('supermarkets.id', $supermarket_ids)->where('supermarkets.status', 1)
                                        ->groupBy('supermarkets.id')->orderBy('supermarkets.name')->get();
        $supermarkets['admin_users'] = Admin::select('id', 'fullname')->where('role_id', 3)->where('status', 1)->orderBy('fullname')->get();

        $zones = Zone::where('city_id', $order->city_id)->where('status', 1)->get()->toArray();
        $draw_zones = [];
        foreach ($zones as $key => $zone) {
            $draw_zones[$key] = explode(',', $zone['delivery_zone']);
        }

        $data = [
            'title' => 'Pedido',
            'order' => $order,
            'cities' => $cities,
            'order_log' => $order_log,
            'order_stores' => $order_stores,
            'order_has_pending_total_real_amount' => $order_has_pending_total_real_amount,
            'order_products' => $order_products,
            'order_notes' => $order_notes,
            'customer_orders' => $customer_orders,
            'customer_credits' => $customer_credits,
            'customer_credit_cards' => $customer_credit_cards,
            'customer_free_deliveries' => $customer_free_deliveries,
            'shoppers_active' => $shoppers_to_select['shoppers_active'],
            'shoppers_map' => $shoppers_by_order_zone,
            'current_shopper' => $shopper,
            'store_branches' => $store_branches,
            'supermarkets' => $supermarkets,
            'zones' => $zones,
            'draw_zones' => $draw_zones,
            'reasons' => RejectReason::admin()->orderBy('reason')->get()
        ];

        return View::make('admin.orders.details', $data);
    }

    /**
     * Obtener todos los shoppers para el select
     *
     * @param object $order Pedido
     * @return array Datos de shoppers
     */
    protected function get_shoppers_to_select($order)
    {
        $_shoppers = Shopper::orderBy('first_name')->get()->toArray();

        $shoppers = $shoppers_active = array();
        foreach ($_shoppers as $shopper) {
            $shopper['updated_at_formatted'] = get_time('left', $shopper['last_date_location'], null, false);
            $shoppers[$shopper['id']] = $shopper;
            if ($shopper['status'] && $shopper['is_active']) {
                $shoppers_active[] = $shopper;
            }
        }

        return ['shoppers' => $shoppers, 'shoppers_active' => $shoppers_active];
    }

    /**
     * Obtener los shoppers por zona del pedido
     *
     * @param object $order Pedido
     * @return array $shoppers Shoppers
     */
    protected function get_shoppers_by_order_zone($order)
    {
        $_shoppers = Shopper::orderBy('first_name')
        ->where('shoppers.status', 1)
        ->where('shoppers.is_active', 1)
        ->leftJoin('orders AS o', function ($join) {
            $join->on('shoppers.id', '=', 'o.shopper_id')
            ->where('o.status', '<>', DB::raw('Delivered'))
            ->where('o.status', '<>', DB::raw('Cancelled'));
        });
        if (!empty($order->zone_id)) {
            $_shoppers->join('shopper_zones', function ($join) use ($order) {
                $join->on('shoppers.id', '=', 'shopper_zones.shopper_id')
                ->where('shopper_zones.zone_id', '=', DB::raw($order->zone_id));
            });
        }
        $_shoppers = $_shoppers->groupBy('shoppers.id')
        ->select('shoppers.*', DB::raw('count(o.id) AS num_orders'))
        ->get()->toArray();
        $shoppers = [];
        foreach ($_shoppers as $shopper) {
            if (!empty($shopper['stores'])) {
                $shopper_stores = explode(',', $shopper['stores']);
                if (!in_array($order->store_id, $shopper_stores)) {
                    continue;
                }
            }
            $shopper['updated_at_formatted'] = get_time('left', $shopper['last_date_location'], null, false);
            $shoppers[$shopper['id']] = $shopper;
        }
        return $shoppers;
    }

    /**
     * Realizar cobro del pedido a tarjeta de crédito
     */
    public function charge()
    {
        $message = 'Ocurrió un error al realizar el cobro a la tarjeta de crédito.';
        $type = 'failed';
        if (Route::current()->parameter('id')) {
            $order_id = Route::current()->parameter('id');
            $order = Order::find($order_id);
            if ($order && in_array($order->status, array('In Progress', 'Dispatched', 'Delivered')) && empty($order->cc_charge_id)) {
                $log = new OrderLog();
                $result = Order::chargeCreditCard($order->id, $order->user_id);
                $message = $result['message'];
                if ($result['status']) {
                    $type = 'success';
                    $log->type = 'ORDER_PAYMENT_SUCCESS';
                }else $log->type = 'ORDER_PAYMENT_FAILED: '.$message;

                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();
            }
        } else $order_id = Request::segment(3);

        return Redirect::route('adminOrders.details', ['id' => $order_id])->with('type', $type)->with('message', $message);
    }

    /**
     * Realizar reembolso del pedido a tarjeta de crédito
     */
    public function refund()
    {
        $message = 'Ocurrió un error al hacer el reembolso a la tarjeta de crédito.';
        $type = 'failed';
        if (Route::current()->parameter('id')) {
            $order_id = Route::current()->parameter('id');
            $order = Order::find($order_id);
            if ($order && !empty($order->cc_charge_id)) {
                $log = new OrderLog();
                $payment = new Payment;
                $result = $payment->refundCreditCardCharge($order->user_id, $order->cc_charge_id, $order->id);
                if ($result['status']) {
                    $order->cc_charge_id = null;
                    $order->cc_payment_transaction_id = null;
                    $order->payment_date = null;
                    $order->cc_refund_date = date('Y-m-d H:i:s');
                    $order->save();
                    $message = 'El reembolso a la tarjeta de crédito fue realizado con éxito.';
                    $type = 'success';
                    $log->type = 'ORDER_REFUND_SUCCESS';
                } else $log->type = 'ORDER_REFUND_FAILED';

                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();
            }
        } else $order_id = Request::segment(3);

        return Redirect::route('adminOrders.details', ['id' => $order_id])->with('type', $type)->with('message', $message);
    }

    /**
     * Actualizar estado de un pedido
     */
    public function update_status($id)
    {
        $order = Order::find($id);
        $order_group = OrderGroup::find($order->group_id);

        $order->status = Input::get('status');

        try {
            DB::beginTransaction();

            if ($order->status == 'In Progress') {
                //validar que el pedido tenga shopper asignado
                if (!$order->shopper_id) {
                    throw new Exception('Se debe primero asignar el pedido a un Shopper.');
                }
                $order->received_date = date('Y-m-d H:i:s');
                $order->shopper_delivery_time_minutes = Input::get('shopper_delivery_time_minutes');
            }

            if ($order->status == 'Dispatched')
            {
                //validar que el pedido tenga shopper asignado
                if (!$order->shopper_id) {
                    throw new Exception('Se debe primero asignar el pedido a un Shopper.');
                }

                //validar que los productos no esten pendientes
                $order_products = OrderProduct::where('order_id', $order->id)->where('store_id', Input::get('store_id'))->where('fulfilment_status', 'Pending')->count();
                if ($order_products) {
                    throw new Exception('Hay productos de la tienda pendientes en el pedido.');
                }

                //obtener subpedidos del pedido
                $order_stores = OrderProduct::where('order_products.order_id', $order->id)->join('stores', 'order_products.store_id', '=', 'stores.id')
                                    ->leftJoin('order_stores', function ($join) {
                                        $join->on('order_stores.order_id', '=', 'order_products.order_id');
                                        $join->on('order_stores.store_id', '=', 'order_products.store_id');
                                    })
                                    ->select('order_stores.*', 'order_products.store_id', 'stores.name')->groupBy('stores.id')->orderBy('stores.id', 'desc')->get();

                //verificar pedidos pendientes por factura y validar si la tienda ya tiene factura
                $order_has_pending_total_real_amount = false;
                foreach ($order_stores as $order_store) {
                    if ($order_store->store_id == Input::get('store_id') && !empty($order_store->total_real_amount)) {
                        throw new Exception('La información de la factura de la tienda seleccionada ya fue subida.');
                    }

                    $total_real_amount = $order_store->total_real_amount;

                    if (empty($order_store->total_real_amount) && $order_store->store_id != Input::get('store_id')) {
                        $order_has_pending_total_real_amount = true;
                    }
                }

                if (!$order_has_pending_total_real_amount) {
                    if ($order_group->source == 'Callcenter' && $order->store_id == 26) {
                        $order->total_amount = $total_real_amount + Input::get('total_real_amount');
                    }
                }

                //registrar datos de pago de shopper por tienda
                if (Input::has('store_id')) {
                    if (!$order_store = OrderStore::where('order_id', $order->id)->where('store_id', Input::get('store_id'))->first()) {
                        $order_store = new OrderStore;
                    }
                    $order_store->order_id = $order->id;
                    $order_store->store_id = Input::get('store_id');
                    $order_store->total_real_amount = Input::get('total_real_amount');
                    $order_store->supermarket = Input::get('shopper_supermarket') == 'Otro' ? Input::get('shopper_supermarket_other') : Input::get('shopper_supermarket');
                    $order_store->shopper_payment_method = Input::get('shopper_payment_method');
                    if (Input::get('shopper_payment_method') == 'Efectivo y tarjeta merqueo') {
                        $order_store->shopper_cash_paid = Input::get('shopper_cash_paid');
                        $order_store->shopper_card_paid = Input::get('shopper_card_paid');
                    } else {
                        $order_store->shopper_cash_paid = Input::get('shopper_payment_method') == 'Efectivo' ? $order_store->total_real_amount : 0;
                        $order_store->shopper_card_paid = Input::get('shopper_payment_method') == 'Tarjeta merqueo' ? $order_store->total_real_amount : 0;
                    }
                    if ($order_store->shopper_payment_method == 'Efectivo y tarjeta merqueo' && Input::get('total_real_amount') != ($order_store->shopper_cash_paid + $order_store->shopper_card_paid)) {
                        throw new Exception('El total ingresado en efectivo y con tarjeta merqueo no coincide con el total de la factura.');
                    }
                    $order_store->save();

                    //registrar movimiento de shopper
                    if (!$order_has_pending_total_real_amount && ShopperMovement::where('shopper_id', $order->shopper_id)->where('status', 1)->first()) {
                        if (Shopper::where('id', $order->shopper_id)->where('profile', '<>', 'Shopper Cliente')->first()) {
                            $movement = new ShopperMovement;
                            $movement->order_id = $order->id;
                            $movement->shopper_id = $order->shopper_id;
                            $movement->movement = 'Gestión de pedido';

                            ShopperMovement::saveMovement($movement, $order);
                        }
                    }
                }

                //dejar estado que tenia si tiene subpedido pendiente por subir factura
                if ($order_has_pending_total_real_amount || $order->payment_method == 'Tarjeta de crédito' && empty($order->cc_charge_id)) {
                    $order->status = 'In Progress';
                    $send_notification = false;
                } else {
                    $order->dispatched_date = date('Y-m-d H:i:s');
                }

                /*if ($order->payment_method == 'Tarjeta de crédito' && empty($order->cc_charge_id))
                    throw new Exception('Se debe realizar primero el cobro a la tarjeta de crédito.');*/
            }

            //validar total vs descuento
            $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
            if ($total < 0 && $order->discount_amount) {
                $new_discount_amount = $order->total_amount + $order->delivery_amount;
                $user_credit = UserCredit::where('order_id', $order->id)->where('type', 0)->where('status', 1)->first();
                $user_credit->amount = $new_discount_amount;
                $user_credit->description = 'Deducción de $'.number_format($new_discount_amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
                $user_credit->save();
                $order->discount_amount = $new_discount_amount;
            }

            if ($order->status == 'Delivered')
            {
                $order->management_date = date('Y-m-d H:i:s');
                //actualizar estado de productos
                OrderProduct::where('order_id', $order->id)->where('fulfilment_status', 'Pending')->update(array('fulfilment_status' => 'Fullfilled'));
                //actualizar totales de pedido
                $products = OrderProduct::where('order_id', $order->id)->where('fulfilment_status', 'Fullfilled')->get();
                $total_products = 0;
                $total_amount = 0;
                foreach ($products as $product) {
                    $total_products++;
                    $total_amount = $total_amount + ($product->quantity * $product->price);
                }
                if ($order->payment_method != 'Tarjeta de crédito') {
                    $order->payment_date = date('Y-m-d H:i:s');
                }
                $order->total_products = $total_products;
                $order->total_amount = $total_amount;

                if ($order->payment_method != 'Tarjeta de crédito') {
                    $customer_payment_method = Input::get('customer_payment_method');
                    if (empty($customer_payment_method)) {
                        throw new Exception('El método de pago del cliente es requerido.');
                    }

                    if ($order->payment_method != Input::get('customer_payment_method')) {
                        $order->payment_method = $customer_payment_method;

                        $log = new OrderLog();
                        $log->type = 'ORDER_UPDATE_PAYMENT_METHOD: '.$order->payment_method.' to '.$customer_payment_method;
                        $log->shopper_id = Session::get('admin_id');
                        $log->order_id = $order->id;
                        $log->save();
                    }
                }

                if ($order->payment_method == 'Efectivo y datáfono') {
                    $total_real_amount = OrderStore::where('order_id', $order->id)->sum('total_real_amount');
                    $total_order = $order_group->source == 'Callcenter' && $order->store_id == 26 ? $total_real_amount : $order->total_amount + $order->delivery_amount - $order->discount_amount;
                    if ($total_order != (Input::get('customer_cash_paid') + Input::get('customer_card_paid'))) {
                        throw new Exception('El total pagado en efectivo y con datáfono no coincide con el total del pedido/factura.');
                    }
                }

                if ($movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first()) {
                    if (Shopper::where('id', $order->shopper_id)->where('profile', '<>', 'Shopper Cliente')->first()) {
                        if (Input::get('customer_payment_method') == 'Efectivo y datáfono') {
                            $movement->user_cash_paid = Input::get('customer_cash_paid');
                            $movement->user_card_paid = Input::get('customer_card_paid');
                        } else {
                            $movement->user_cash_paid = 0;
                            $movement->user_card_paid = 0;
                        }
                        ShopperMovement::saveMovement($movement, $order);
                    }
                }

                if (!isset($order->invoice_number))
                    Order::getInvoiceNumber($order, 0);

                //si es primera compra y fue referido
                $count_orders = Order::where('user_id', '=', $order->user_id)->where('status', '=', 'Delivered')->count();

                if (!$count_orders) {
                    $user = User::find($order->user_id);

                    if ($user->referred_by && $referrer = User::find($user->referred_by)) {
                        if ($referrer->total_referrals < Config::get('app.referred.limit')) {
                            //registrar crédito a usuario referido
                            //$referred_by_amount = Config::get('app.referred.referred_by_amount');
                            /*$user_credit = new UserCredit;
                            $user_credit->user_id = $referrer->id;
                            $user_credit->referrer_id = $user->id;
                            $user_credit->amount = $referred_by_amount;
                            $user_credit->type = 1;
                            $user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por registro de amigo '.$user->first_name.' '.$user->last_name.' con ID usuario # '.$user->id.' con sistema de referidos en pedido con ID # '.$order->id;
                            $user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
                            $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                            $user_credit->save();*/

                            //referido con domicilio gratis
                            $referred_by_free_delivery_days = Config::get('app.referred.referred_by_free_delivery_days');
                            $user_free_delivery = new UserFreeDelivery;
                            $user_free_delivery->user_id = $referrer->id;
                            $user_free_delivery->referrer_id = $user->id;
                            $user_free_delivery->amount = $referred_by_free_delivery_days;
                            if (!empty($referrer->free_delivery_expiration_date) && $referrer->free_delivery_expiration_date <= date('Y-m-d')) {
                                $free_delivery_expiration_date = date('Y-m-d', strtotime('+'.$referred_by_free_delivery_days.' day'));
                            } else {
                                $free_delivery_expiration_date = date('Y-m-d', strtotime($referrer->free_delivery_expiration_date.' +'.$referred_by_free_delivery_days.' day'));
                            }
                            $user_free_delivery->expiration_date = $free_delivery_expiration_date;
                            $description = 'Adición de '.$referred_by_free_delivery_days.' días de domicilio gratis por registro de amigo '.$user->first_name.' '.$user->last_name.' con ID usuario # '.$user->id.'
                            con sistema de referidos en pedido con ID # '.$order->id.'. Nueva fecha de vencimiento: '.format_date('normal', $user_free_delivery->expiration_date);
                            $user_free_delivery->description = $description;
                            $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                            $user_free_delivery->save();

                            //aumentar cantidad de referidos y fecha de expiracion de domicilio gratis a usuario
                            $referrer->free_delivery_expiration_date = $user_free_delivery->expiration_date;
                            $referrer->total_referrals += 1;
                            $referrer->save();

                            // Track Referral Code Reedemed event
                            $analytics = new AnalyticsClient;
                            $analytics->track($referrer->id, 'Referral Code Reedemed', array(
                                'code' => $referrer->referral_code,
                                'reedemedBy' => $user->id
                            ));

                            $user_discounts = User::getDiscounts($referrer->id);

                            //enviar mail a usuario referido
                            $referred_mail = array(
                                'template_name' => 'emails.referred',
                                'subject' => 'Tienes '.$referred_by_free_delivery_days.' días de domicilio gratis en cualquier tienda',
                                'to' => array(array('email' => $referrer->email, 'name' => $referrer->first_name.' '.$referrer->last_name)),
                                'vars' => array(
                                    'username' => $referrer->first_name,
                                    'username_referrer' => $user->first_name,
                                    'referred_by_free_delivery_days' => $referred_by_free_delivery_days,
                                    'free_delivery_days' => $user_discounts['free_delivery_days'],
                                    'expiration_date' => format_date('normal_long', $user_free_delivery->expiration_date),
                                    'referal_code' => $referrer->referral_code
                                )
                            );
                        }
                    }
                }

                $this->addCreditWhenDeliveryLate->handle($order, Carbon::now()->addYear());
            }

            if ($order->status == 'Cancelled')
            {
                if (!empty($order->cc_charge_id) && empty($order->cc_refund_date)) {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'El total del pedido ya fue cobrado a la tarjeta.');
                }

                $order->management_date = date('Y-m-d H:i:s');
                $order->reject_reason = Input::get('reject_reason');
                $order->reject_comments = Input::has('reject_comments') ? Input::get('reject_comments') : null;
                //retornar credito a cuenta de usuario
                UserCredit::where('order_id', $order->id)->where('type', 0)->update(array('status' => 0));
                //eliminar registro de campaña de marca
                UserBrandCampaign::where('order_id', $order->id)->delete();
                //actualizar movimiento de shopper
                if (ShopperMovement::where('shopper_id', $order->shopper_id)->where('status', 1)->first()) {
                    if (Shopper::where('id', $order->shopper_id)->where('profile', '<>', 'Shopper Cliente')->first()) {
                        if ($movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first()) {
                            $order_stores = OrderStore::select('order_stores.*', 'stores.name AS store_name')
                                    ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $order->id)->get();
                            foreach ($order_stores as $order_store) {
                                $order_store->product_return = Input::get('product_return_'.$order_store->store_id);
                                $order_store->save();
                            }

                            $shopper_balance = 0;
                            $description = 'Pedido actualizado a Cancelado ';

                            foreach ($order_stores as $order_store) {
                                //con devolucion
                                if ($order_store->product_return) {
                                    //shopper pago en efectivo
                                    if ($order_store->shopper_payment_method == 'Efectivo') {
                                        $shopper_balance += $order_store->total_real_amount;
                                    }
                                    //shopper pago con tarjeta
                                    if ($order_store->shopper_payment_method == 'Tarjeta merqueo' || $order_store->shopper_payment_method == 'No aplica') {
                                        $shopper_balance += 0;
                                    }
                                    //shopper pago con efectivo y tarjeta
                                    if ($order_store->shopper_payment_method == 'Efectivo y tarjeta merqueo') {
                                        $shopper_balance += $order_store->shopper_cash_paid;
                                    }
                                    $description .= 'con devolución en tienda '.$order_store->store_name;
                                }
                                //sin devolucion
                                if (!$order_store->product_return) {
                                    //shopper pago en efectivo
                                    if ($order_store->shopper_payment_method == 'Efectivo') {
                                        $shopper_balance += 0;
                                    }
                                    //shopper pago con tarjeta
                                    if ($movement->shopper_payment_method == 'Tarjeta merqueo' || $movement->shopper_payment_method == 'No aplica') {
                                        $shopper_balance += $order_store->total_real_amount * -1;
                                    }
                                    //shopper pago con efectivo y tarjeta
                                    if ($movement->shopper_payment_method == 'Efectivo y tarjeta merqueo') {
                                        $shopper_balance += $order_store->shopper_card_paid * -1;
                                    }
                                    $description .= 'sin devolución en tienda '.$order_store->store_name;
                                }
                            }

                            $movement->shopper_balance = $shopper_balance;
                            $description .= ' desde el administrador.';
                            $movement->save();
                        }
                    }
                }
            }

            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_STATUS_UPDATE: '.$order->status;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            DB::commit();

            if ($order->status == 'Delivered' || $order->status == 'Cancelled') {
                // Track Order Partial Delivered/Cancelled Event
                $analytics = new AnalyticsClient;

                $analytics->track($order->user_id, "Order Partial $order->status", array(
                    'store' => $order->store->name,
                    'orderId' => $order_group->id,
                    'total' => $order->total_amount,
                    'discount' => $order->discount_amount,
                    'specialPrice' => $order->specialPrice,
                    'delivery' => $order->delivery_amount,
                    'paymentMethod' => $order->payment_method,
                    'partialId' => $order->id
                ));

                if (($order->status == 'Cancelled' &&
                        $order_group->cancelled == $order_group->orderCount) ||
                    ($order->status == 'Delivered' &&
                        $order_group->delivered == 1)) {
                    // Track Order Delivered/Cancelled Event
                    $analytics->track($order->user_id, "Order $order->status", array(
                        'store' => $order_group->storeNames,
                        'orderId' => $order_group->id,
                        'total' => $order_group->total_amount,
                        'discount' => $order_group->discount_amount,
                        'specialPrice' => $order_group->specialPrice,
                        'delivery' => $order_group->delivery_amount,
                        'paymentMethod' => $order->payment_method,
                        'orderCount' => $order_group->orderCount
                    ));
                }

                // Update user attributes
                AnalyticsClient::updateUser($order_group, $order);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', $e->getMessage());
        }

        $message = 'Pedido actualizado con éxito.';

        //enviar mail a usuario referido
        if (isset($referred_mail) && empty($order->parent_order_id)) {
            send_mail($referred_mail);
        }

        //enviar mail de pedido
        $templates = array(
            'In Progress' => 'emails.order_accepted',
            'Cancelled' => 'emails.order_rejected',
            'Delivered' => 'emails.order_delivered',
        );

        $subjects = array(
            'In Progress' => 'ha sido asignado',
            'Cancelled' => 'ha sido cancelado',
            'Delivered' => 'ha sido entregado',
        );

        if ($order->status == 'Cancelled' && !Input::has('reject_notification')) {
            return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'success')->with('message', $message);
        }

        if (isset($templates[$order->status]) && !isset($send_notification) && empty($order->parent_order_id)) {
            $store = Store::find($order->store_id);

            $send_notification = true;
            $shopper = $delivery_time = '';
            if ($order->status == 'In Progress' || $order->status == 'Delivered') {
                if ($order->shopper_id != 280 && $order->shopper_id != 77 && $shopper = Shopper::getShopper($order->shopper_id)) {
                    $user = User::find($order->user_id);
                    if ($order->status == 'In Progress' && strlen(trim($user->phone)) == 10) {
                        $phone_number = trim($user->phone);
                        $msn = 'Merqueo.com: '.$shopper->first_name.' fue asignado para tu pedido de '.$store->name.', te llamara para confirmar el pedido o puedes llamarlo al: '.$shopper->phone;
                        send_sms($phone_number, $msn);
                    }
                } else {
                    $send_notification = false;
                }
            }

            if ($send_notification && $order_group->source != 'Web Service') {
                $products_mail[$order->id] = array();
                $products = OrderProduct::where('order_id', $order->id)->get();

                foreach ($products as $product) {
                    $products_mail[$order->id][] = array(
                         'img' => $product->product_image_url,
                         'name' => $product->product_name,
                         'qty' => $product->quantity,
                         'qty_original' => $product->quantity_original,
                         'quantity' => $product->product_quantity,
                         'unit' => $product->product_unit,
                         'price' => $product->price,
                         'total' => $product->quantity * $product->price,
                         'type' => $product->type,
                         'status' => $product->fulfilment_status != 'Not Available' ? 'available.png' : 'no_available.png'
                     );
                }

                $products_available = [ 'products' => null, 'subtotal' => 0 ];
                $products_not_available = [ 'products' => null, 'subtotal' => 0 ];
                foreach ($products_mail[$order->id] as $product) {
                    if ($product['status'] == 'available.png') {
                        $products_available['products'][] = $product;
                        $products_available['subtotal'] += $product['total'];
                    }else{
                        $products_not_available['products'][] = $product;
                        $products_not_available['subtotal'] += $product['total'];
                    }
                }

                $mail = array(
                    'template_name' => $templates[$order->status],
                    'subject' => 'Tu pedido en '.$store->name.' '.$subjects[$order->status],
                    'to' => array(array('email' => $order_group->user_email, 'name' => $order_group->user_firstname.' '.$order_group->user_lastname)),
                    'vars' => array(
                        'order' => $order,
                        'order_group' => $order_group,
                        'store' => $store,
                        'shopper' => $shopper,
                        'reject_reason' => $order->reject_reason,
                        'products' => $products_mail[$order->id],
                        'products_available' => $products_available,
                        'products_not_available' => $products_not_available
                    )
                );

                //enviar mail
                send_mail($mail);

                //enviar notificacion
                if ($order_group->source == 'Device') {
                    $notifications = array(
                        'In Progress' => 'Tu pedido en '.$store->name.' fue asignado y lo estamos preparando para enviártelo a la dirección que indicaste.',
                        'Delivered' => '¿Cómo te fue con tu pedido de '.$store->name.'?',
                        'Cancelled' => 'Tu pedido en '.$store->name.' fue cancelado.'
                    );
                    $data =  array(
                        'type' => $order->status,
                        'message' => $notifications[$order->status],
                        'order_id' => $order->id
                    );
                    $result = send_notification(
                        $order_group->app_version,
                        $order_group->source_os,
                        [$order_group->user_device_id],
                        $order_group->device_player_id,
                        $data
                    );
                    $message .= $result['status'] ? ' - Notification was sent successfully.' : ' - Error sending notification: '.$result['message'];
                }
            }
        }

        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'success')->with('message', $message);
    }

    /**
     * Actualizar estado de pedido
     */
    public function update_status_admin($id)
    {
        $message = 'Ocurrió un error al actualizar el estado.';
        $type = 'failed';

        if ($id) {
            $order = Order::find($id);
            if ($order) {
                if (!empty($order->cc_charge_id) && empty($order->cc_refund_date)) {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', "El total del pedido ya fue cobrado a la tarjeta.");
                }

                $new_status = Input::get('status');
                $shopper_id = $order->shopper_id;

                $order_stores = OrderStore::select('order_stores.*', 'stores.name AS store_name')
                                ->join('stores', 'stores.id', '=', 'order_stores.store_id')->where('order_id', $order->id)->get();

                if ($new_status == 'Validation' || $new_status == 'Initiated' || $new_status == 'In Progress') {
                    if ($new_status == 'Validation') {
                        if ($order->payment_method != 'Tarjeta de crédito') {
                            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', "El método de pago debe ser Tarjeta de crédito");
                        }
                    }
                    if ($new_status == 'In Progress') {
                        if (empty($order->shopper_id)) {
                            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'Se debe asignar un shopper al pedido primero.');
                        }
                    }
                    if ($new_status == 'Validation' || $new_status == 'Initiated') {
                        $order->auto_allocated = null;
                        $order->dispatched_date = null;
                        $order->shopper_id = null;
                    }
                    if ($new_status == 'In Progress') {
                        $order->allocated_date = date('Y-m-d H:i:s');
                        $order->received_date = date('Y-m-d H:i:s');
                    }
                    $order->payment_date = null;
                    $order->management_date = null;
                    $order->reject_reason = null;
                    $order->reject_comments = null;

                    $cloudfront_url = Config::get('app.aws.cloudfront_url');
                    foreach ($order_stores as $order_store) {
                        if (!empty($order_store->invoice_image_url)) {
                            $path = str_replace($cloudfront_url, uploads_path(), $order_store->invoice_image_url);
                            remove_file($path);
                            remove_file_s3($order_store->invoice_image_url);
                        }
                    }
                    OrderStore::where('order_id', $order->id)->delete();
                    if (!empty($order->voucher_image_url)) {
                        $path = str_replace($cloudfront_url, uploads_path(), $order->voucher_image_url);
                        remove_file($path);
                        remove_file_s3($order->voucher_image_url);
                    }
                    $order->voucher_image_url = null;
                    OrderProduct::where('order_id', $order->id)->update(array('fulfilment_status' => 'Pending'));
                }
                if ($new_status == 'Cancelled') {

                    /*if ($order->status == 'Delivered'){
                        //si es primera compra y fue referido
                        $count_orders = Order::where('user_id', '=', $order->user_id)->where('status', '=', 'Delivered')->count();
                        if ($count_orders == 1){
                            $user = User::find($order->user_id);
                            if ($user->referred_by)
                                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'This was the first order from that user and was referred by another. Please let the system administrator know.');
                        }
                    }*/

                    $order->management_date = date('Y-m-d H:i:s');
                    $order->reject_reason = Input::get('reject_reason');
                    $order->reject_comments = Input::has('reject_comments') ? Input::get('reject_comments') : null;

                    foreach ($order_stores as $order_store) {
                        $order_store->product_return = Input::get('product_return_'.$order_store->store_id);
                        $order_store->save();
                    }

                    //retornar credito a cuenta de usuario
                    UserCredit::where('order_id', $order->id)->where('type', 0)->update(array('status' => 0));
                    //eliminar registro de campaña de marca
                    UserBrandCampaign::where('order_id', $order->id)->where('campaign', 'lavomatic')->delete();
                }

                $order->status = $new_status;
                $order->save();

                //movimiento de shopper
                if (ShopperMovement::where('shopper_id', $shopper_id)->where('status', 1)->first()) {
                    if ($movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first()) {
                        if (Shopper::where('id', $shopper_id)->where('profile', '<>', 'Shopper Cliente')->first()) {
                            if ($order->status == 'Cancelled') {
                                $movement->status = 0;
                                $movement->save();

                                $new_movement = new ShopperMovement;
                                $new_movement->order_id = $order->id;
                                $new_movement->shopper_id = $order->shopper_id;
                                $new_movement->admin_id = Session::get('admin_id');
                                $new_movement->movement = 'Gestión de pedido';
                                $new_movement->user_cash_paid = $movement->user_cash_paid;
                                $new_movement->user_card_paid = $movement->user_card_paid;
                                $new_movement->card_balance = $movement->card_balance;

                                $shopper_balance = 0;
                                $description = 'Pedido actualizado a Cancelado ';

                                foreach ($order_stores as $order_store) {
                                    //con devolucion
                                    if ($order_store->product_return) {
                                        //shopper pago en efectivo
                                        if ($order_store->shopper_payment_method == 'Efectivo') {
                                            $shopper_balance += $order_store->total_real_amount;
                                        }
                                        //shopper pago con tarjeta
                                        if ($order_store->shopper_payment_method == 'Tarjeta merqueo' || $order_store->shopper_payment_method == 'No aplica') {
                                            $shopper_balance += 0;
                                        }
                                        //shopper pago con efectivo y tarjeta
                                        if ($order_store->shopper_payment_method == 'Efectivo y tarjeta merqueo') {
                                            $shopper_balance += $order_store->shopper_cash_paid;
                                        }
                                        $description .= 'con devolución en tienda '.$order_store->store_name;
                                    }
                                    //sin devolucion
                                    if (!$order_store->product_return) {
                                        //shopper pago en efectivo
                                        if ($order_store->shopper_payment_method == 'Efectivo') {
                                            $shopper_balance += 0;
                                        }
                                        //shopper pago con tarjeta
                                        if ($movement->shopper_payment_method == 'Tarjeta merqueo' || $movement->shopper_payment_method == 'No aplica') {
                                            $shopper_balance += $order_store->total_real_amount * -1;
                                        }
                                        //shopper pago con efectivo y tarjeta
                                        if ($movement->shopper_payment_method == 'Efectivo y tarjeta merqueo') {
                                            $shopper_balance += $order_store->shopper_card_paid * -1;
                                        }
                                        $description .= 'sin devolución en tienda '.$order_store->store_name;
                                    }
                                }

                                $new_movement->shopper_balance = $shopper_balance;
                                $description .= ' desde el administrador.';
                                $new_movement->save();
                            } else {
                                ShopperMovement::where('order_id', $order->id)->where('status', 1)->update(array('description' => 'Pedido actualizado a '.$order->status.' desde el Administrador.', 'status' => 0));
                            }
                        }
                    }
                }

                //log de pedido
                $log = new OrderLog();
                $log->type = 'ORDER_STATUS_UPDATE: '.$order->status;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                $message = 'Estado de pedido actualizado con éxito.';
                $type = 'success';
            }
        }

        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', $type)->with('message', $message);
    }

    /**
     * Actualizar shopper en pedido
     */
    public function update_shopper($id)
    {
        $shopper_id = Input::get('shopper-id');
        $store_branch_id = Input::get('store-branch-id', null);
        $ajax = Input::get('ajax', null);
        $user = Shopper::find($shopper_id);

        if (!empty($user)) {
            $order = Order::find($id);

            $order->shopper_id = $shopper_id;
            /*if (!empty($store_branch_id)) {
                $order->store_branch_id = $store_branch_id;
            }*/
            $order->allocated_date = date('Y-m-d H:i:s');
            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_ASSIGNED_TO_SHOPPER: '.$order->shopper_id;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            /*$pusher = new Pusherer(
                Config::get('pusherer.shopper.app_id'),
                Config::get('pusherer.shopper.key'),
                Config::get('pusherer.shopper.secret'),
                ['encrypted' => true]
            );*/
            //$pusher::trigger('private-shopper-' . $shopper_id, 'new-order', array('order_id' => $order->id, 'assign_new_order' => 1));
            Pusherer::send_push( 'private-shopper-' . $shopper_id, 'new-order',  array('order_id' => $order->id, 'assign_new_order' => 1), 'transporter' );

            if ($ajax) {
                return Response::json(['success' => 'Se ha asignado el shopper'], 200);
            }

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'Pedido actualizado con éxito.');
        } else {
            if ($ajax) {
                return Response::json(['error' => 'Shopper no encontrado'], 500);
            }

            return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'error')->with('message', 'No se encontro el shopper.');
        }
    }

    /**
     * Actualizar producto en pedido
     */
    public function update_product()
    {
        if (!Input::has('order_id')) {
            //update price or replace product
            $id = Input::get('id');
            $order_product = OrderProduct::find($id);
            if ($order_product) {
                $order_id = $order_product->order_id;
                $store_id = $order_product->store_id;

                $order = Order::find($order_id);
                if (!empty($order->cc_charge_id) && empty($order->cc_refund_date)) {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', "The order's credit card was already charged with the total amount");
                }

                if (Input::has('quantity') && !Input::has('product_name')) {
                    $order_product->quantity = Input::get('quantity');
                }
                if (Input::has('price') && !Input::has('product_name')) {
                    $order_product->price = Input::get('price');
                }
                if (Input::has('product_name')) {
                    $order_product->fulfilment_status = 'Replacement';
                    $order_product_replace = new OrderProduct;
                    $order_product_replace->order_id = $order_id;
                    $order_product_replace->price = Input::get('price');
                    $order_product_replace->original_price = Input::get('price');
                    $order_product_replace->quantity = Input::get('quantity');
                    $order_product_replace->fulfilment_status = 'Fullfilled';
                    $order_product_replace->type = 'Replacement';
                    $order_product_replace->parent_id = $id;
                    $order_product_replace->product_name = Input::get('product_name');
                    $order_product_replace->product_image_url = image_url().'no_disponible.jpg';
                    $order_product_replace->product_quantity = $order_product->product_quantity;
                    $order_product_replace->product_unit = $order_product->product_unit;
                    $order_product_replace->store_id = $store_id;
                    $order_product_replace->save();
                }
                $order_product->save();

                //log de pedido
                $log = new OrderLog();
                $log->type = 'ORDER_PRODUCT_UPDATE: '.$order_product->store_product_id;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();
            }
        } else {
            //add product
            $order_id = Input::get('order_id');
            $order = Order::find($order_id);

            if (!empty($order->cc_charge_id) && empty($order->cc_refund_date)) {
                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', "The order's credit card was already charged with the total amount");
            }

            $fulfilment_status = 'Fullfilled';
            if ($order->status == 'Initiated' || $order->status == 'In Progress') {
                $fulfilment_status = 'Pending';
            }

            $order_product = new OrderProduct;
            $order_product->order_id = $order_id;
            $order_product->price = Input::get('price');
            $order_product->original_price = Input::get('price');
            $order_product->quantity = Input::get('quantity');
            $order_product->fulfilment_status = $fulfilment_status;
            $order_product->type = 'Product';
            $order_product->parent_id = 0;
            $order_product->product_name = 'Modificación: '.Input::get('product_name');
            $order_product->product_image_url = image_url().'no_disponible.jpg';
            $order_product->store_id = $order->store_id;
            $order_product->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_PRODUCT_INSERT: '.$order_product->id;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();
        }

        //actualizar total de la orden
        $products = OrderProduct::where('order_id', $order_id)
                    ->where(function ($query) {
                        $query->where('fulfilment_status', '=', 'Pending');
                        $query->orWhere('fulfilment_status', '=', 'Fullfilled');
                    })->get();

        $total_products = 0;
        $total_amount = 0;
        foreach ($products as $product) {
            $total_products++;
            $total_amount = $total_amount + ($product->quantity * $product->price);
        }
        if (!isset($order)) {
            $order = Order::find($order_id);
        }
        $order->total_products = $total_products;
        $order->total_amount = $total_amount;

        //recalcular descuento si es por porcentaje
        if ($order->discount_percentage_amount) {
            $order->discount_amount = round($order->total_amount * ($order->discount_percentage_amount / 100), 0);
        }

        //validar total vs descuento
        $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
        if ($total < 0 && $order->discount_amount) {
            $new_discount_amount = $order->total_amount + $order->delivery_amount;
            $user_credit = UserCredit::where('order_id', $order->id)->where('type', 0)->where('status', 1)->first();
            if ($user_credit) {
                $user_credit->amount = $new_discount_amount;
                $user_credit->description = 'Deducción de $'.number_format($new_discount_amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
                $user_credit->save();
            }
            $order->discount_amount = $new_discount_amount;
        }

        $order->save();

        //log de pedido
        $log = new OrderLog();
        $log->type = 'ORDER_TOTAL_UPDATE: '.$order->total_amount;
        $log->admin_id = Session::get('admin_id');
        $log->order_id = $order->id;
        $log->save();

        return Redirect::route('adminOrders.details', ['id' => $order->id]);
    }

    /**
     * Actualizar producto en pedido
     */
    public function update_product_ajax()
    {
        if (Input::has('id')) {
            //update price or replace product
            $id = Input::get('id');
            $order_product = OrderProduct::find($id);
            if ($order_product) {
                $order_id = $order_product->order_id;
                if (Input::has('status') && !Input::has('product_name')) {
                    $order_product->fulfilment_status = Input::get('status');
                }

                $order_product->save();

                //log de pedido
                $log = new OrderLog();
                $log->type = 'ORDER_PRODUCT_UPDATE: '.$order_product->store_product_id;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order_id;
                $log->save();
            }

            //actualizar total de la orden
            $products = OrderProduct::where('order_id', $order_id)
                        ->where(function ($query) {
                            $query->where('fulfilment_status', '=', 'Pending');
                            $query->orWhere('fulfilment_status', '=', 'Fullfilled');
                        })->get();

            $total_amount = $total_products = 0;
            foreach ($products as $product) {
                $total_products++;
                $total_amount = $total_amount + ($product->quantity * $product->price);
            }

            $order = Order::find($order_id);
            $order->total_products = $total_products;
            $order->total_amount = $total_amount;

            //recalcular descuento si es por porcentaje
            if ($order->discount_percentage_amount) {
                $order->discount_amount = round($order->total_amount * ($order->discount_percentage_amount / 100), 0);
            }

            //validar total vs descuento
            $total = $order->total_amount + $order->delivery_amount - $order->discount_amount;
            if ($total < 0 && $order->discount_amount) {
                $new_discount_amount = $order->total_amount + $order->delivery_amount;
                $user_credit = UserCredit::where('order_id', $order->id)->where('type', 0)->where('status', 1)->first();
                if ($user_credit) {
                    $user_credit->amount = $new_discount_amount;
                    $user_credit->description = 'Deducción de $'.number_format($new_discount_amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
                    $user_credit->save();
                }
                $order->discount_amount = $new_discount_amount;
            }

            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_TOTAL_UPDATE: '.$order->total_amount;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            $response['success'] = 'true';
            $response['status'] =  $order_product->fulfilment_status;
            $response['payment_data'] = array(
                                            'total_amount' => '$ '.number_format($order->total_amount, 0, ',', '.'),
                                            'delivery_amount' => '$ '.number_format($order->delivery_amount, 0, ',', '.'),
                                            'discount_amount' => '$ '.number_format($order->discount_amount, 0, ',', '.'),
                                            'total' => '$ '.number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.')
                                        );
        } else {
            $response['success'] = 'false';
        }

        return Response::json($response, 200);
    }

    /**
     * Actualizar precio de producto personalizado
     */
    public function update_product_custom()
    {
        $order_id = Input::get('order_id');
        if (Input::has('product_id')) {
            $id = Input::get('product_id');
            $product = OrderProduct::find($id);
            $product->price = Input::get('price');
            $product->fulfilment_status = Input::get('status');
            $product->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_PRODUCT_UPDATE: '.$product->store_product_id;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order_id;
            $log->save();

            return Redirect::route('adminOrders.details', ['id' => $order_id])->with('type', 'success')->with('message', 'Producto actualizado con éxito.');
        } else {
            return Redirect::route('adminOrders.details', ['id' => $order_id])->with('type', 'failed')->with('message', "The product is not exists to update price.");
        }
    }

    /**
     * Añadir cupon a pedido
     */
    public function save_coupon()
    {
        $message = 'Ocurrió un error al añadir el cupón al pedido.';
        $type = 'failed';

        $id = Input::get('id');

        if (Input::has('id') && Input::has('coupon_code')) {
            $order = Order::find($id);
            if ($order) {
                if ($order->status == 'Delivered' || $order->status == 'Cancelled') {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'El pedido ya fue entregado o cancelado.');
                }

                if (!empty($order->discount_amount)) {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'El pedido ya tiene un descuento aplicado.');
                }

                $coupon_code = Input::get('coupon_code');
                $request = Request::create('/checkout/coupon', 'POST', array('coupon_code' => $coupon_code, 'user_id' => $order->user_id, 'order_id' => $order->id, 'validate' => 1, 'is_new_user' => 1));
                Request::replace($request->input());
                $result = json_decode(Route::dispatch($request)->getContent(), true);
                if ($result['status']) {
                    if (isset($result['coupon'])) {
                        $coupon = (Object)$result['coupon'];
                        $total_cart = $order->total_amount + $order->delivery_amount;
                        //si el cupon cumple con totales requerido
                        if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $total_cart < $coupon->minimum_order_amount) {
                            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'Para redimir el cupón de descuento <b>'.$coupon->code.'</b> el total del pedido sin incluir el domicilio debe ser mayor o igual a <b>$'.number_format($coupon->minimum_order_amount, 0, ',', '.').'</b>');
                        } elseif ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $total_cart > $coupon->maximum_order_amount) {
                            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'Para redimir el cupón de descuento <b>'.$coupon->code.'</b> el total del pedido sin incluir el domicilio debe ser menor o igual a <b>$'.number_format($coupon->maximum_order_amount, 0, ',', '.').'</b>');
                        } elseif ($coupon->minimum_order_amount && $coupon->maximum_order_amount && $total_cart < $coupon->minimum_order_amount && $total_cart > $coupon->maximum_order_amount) {
                            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'Para redimir el cupón de descuento <b>'.$coupon->coupon.'</b> el total del pedido sin incluir el domicilio debe estar entre <b>$'.number_format($coupon->maximum_order_amount, 0, ',', '.').'</b> y <b>$'.number_format($coupon->minimum_order_amount, 0, ',', '.').'</b>');
                        }

                        $amount = 0;
                        if ($coupon->redeem_on == 'Total Cart') {
                            if ($coupon->type == 'Discount Percentage') {
                                $amount = round($order->total_amount * ($coupon->amount /100), 0);
                            } elseif ($coupon->type == 'Credit Amount') {
                                $amount = $order->total_amount >= $coupon->amount ? $coupon->amount : $order->total_amount;
                            }
                        } else {
                            if ($coupon->redeem_on == 'Specific Store') {
                                $amount = $result['discount_amount'];
                            }
                        }
                        try {
                            DB::beginTransaction();

                            //cargar cupon como utilizado a usuario
                            $user_coupon = new UserCoupon;
                            $user_coupon->coupon_id = $coupon->id;
                            $user_coupon->user_id = $order->user_id;
                            $user_coupon->save();
                            //registrar movimiento de crédito a usuario
                            $user_credit = new UserCredit;
                            $user_credit->user_id = $order->user_id;
                            $user_credit->coupon_id = $coupon->id;
                            $user_credit->amount = $amount;
                            $user_credit->type = 1;
                            $user_credit->description = 'Cargo de crédito $'.number_format($user_credit->amount, 0, ',', '.').' por redimir cupón código '.$coupon_code;
                            $user_credit->expiration_date = date('Y-m-d H:i:s', strtotime('+1 year'));
                            $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                            $user_credit->save();
                            //descontar credito utilizado a usuario
                            $user_credit = new UserCredit;
                            $user_credit->user_id = $order->user_id;
                            $user_credit->order_id = $order->id;
                            $user_credit->coupon_id = $coupon->id;
                            $user_credit->amount = $amount;
                            $user_credit->type = 0;
                            $user_credit->description = 'Deducción de $'.number_format($user_credit->amount, 0, ',', '.').' utilizados en pedido con ID # '.$order->id;
                            $user_credit->created_at = $user_credit->updated_at = date('Y-m-d H:i:s');
                            $user_credit->save();

                            $order->discount_amount = $amount;
                            $order->save();

                            //log de pedido
                            $log = new OrderLog();
                            $log->type = 'ORDER_DISCOUNT_UPDATE: '.$order->discount_amount;
                            $log->admin_id = Session::get('admin_id');
                            $log->order_id = $order->id;
                            $log->save();

                            DB::commit();

                            $message = 'Cupón añadido al pedido con éxito.';
                            $type = 'success';
                        } catch (\Exception $e) {
                            DB::rollback();
                            throw $e;
                        }
                    } else {
                        return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'No se puede agregar un codigo referido a un pedido manualmente.');
                    }
                } else {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', $result['message']);
                }
            }
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', $type)->with('message', $message);
    }

    /**
     * Eliminar descuento de pedido
     */
    public function delete_discount_amount()
    {
        $id = Request::segment(3);
        $order = Order::find($id);

        if ($order) {
            try {
                DB::beginTransaction();

                $discount_amount = $order->discount_amount;
                $order->discount_amount = 0;
                $order->save();

                $user_credit = UserCredit::where('order_id', $order->id)->where('type', 0)->where('status', 1)->first();
                if ($user_credit) {
                    UserCredit::where('order_id', $order->id)->where('coupon_id', $user_credit->coupon_id)->update(array('status' => 0));
                    UserCoupon::where('user_id', $order->user_id)->where('coupon_id', $user_credit->coupon_id)->delete();
                }

                $log = new OrderLog();
                $log->type = 'ORDER_DELETE_DISCOUNT_AMOUNT: '.$discount_amount;
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'El descuento fue eliminado del pedido con éxito.');
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al eliminar el descuento al pedido.');
    }

    /**
     * Editar fecha de entrega de pedido a cliente
     */
    public function update_delivery_date()
    {
        $id = Request::segment(3);
        $order = Order::select('orders.*', 'delivery_time_minutes')->join('stores', 'store_id', '=', 'stores.id')->where('orders.id', $id)->first();

        if ($order) {
            //dia y hora de entrega
            $delivery_day = Input::get('delivery_day');
            if (Input::get('delivery_time') == 'immediately') {
                $hour_today = new DateTime();
                $hour_today->modify('+'.$order->delivery_time_minutes.' minutes');
                $delivery_time = $hour_today->format('H:i:s');
                $delivery_time_text = 'Immediately';
                $real_delivery_date = $delivery_day.' '.$delivery_time;
            } else {
                $delivery_time = explode(' - ', Input::get('delivery_time'));
                $delivery_time_begin = explode(':', $delivery_time[0]);
                $hour_begin = new DateTime();
                $hour_begin->setTime($delivery_time_begin[0], $delivery_time_begin[1]);

                $delivery_time_end = explode(':', $delivery_time[1]);
                $hour_end = new DateTime();
                $hour_end->setTime($delivery_time_end[0], $delivery_time_end[1]);

                $delivery_time_text = $hour_begin->format('g:i a').' - '.$hour_end->format('g:i a');
                $hour_begin = $delivery_day.' '.$delivery_time[0];
                if ($hour_end->format('G') < 3) {
                    $post_data['delivery_day'] = date('Y-m-d', strtotime($delivery_day . '+1 day'));
                }

                $hour_end = $real_delivery_date = $delivery_day.' '.$delivery_time[1];
                $delivery_time = get_delivery_time($hour_begin, $hour_end);
            }

            $log_type = 'ORDER_UPDATE_DELIVERY_DATE: '.format_date('normal_with_time', $order->delivery_date).' to '.format_date('normal_with_time', $delivery_day.' '.$delivery_time);
            $order->delivery_date = $delivery_day.' '.$delivery_time;
            $order->real_delivery_date = $real_delivery_date;
            $order->delivery_time = $delivery_time_text;
            $order->scheduled_delivery = 1;
            $order->save();

            $log = new OrderLog();
            $log->type = $log_type;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'La fecha de entrega fue actualizada con éxito.');
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al actualizar la fecha de entrega.');
    }

    /**
     * Editar estado de revision de pedido
     */
    public function update_revision_checked()
    {
        $id = Request::segment(3);

        if ($order = Order::find($id)) {
            $store = Store::find($order->store_id);
            if (!$store->revision_orders_required) {
                return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'El pedido no permite revisión.');
            }

            $order->is_checked = 1;
            $order->revision_user_id = Session::get('admin_id');
            $order->save();

            $log = new OrderLog();
            $log->type = 'ORDER_UPDATE_REVISION_CHECKED';
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'El pedido fue actualizado con éxito.');
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al actualizar el pedido.');
    }

    /*
    *   Funcion para generar numero de factura aleatorio
    */
    private function consecutive($prefix, $min, $max)
    {
        $consecutive = $prefix.'-'.mt_rand($min, $max);
        $invoice_number = Order::where('invoice_number', $consecutive)->first();
        if (!$invoice_number) {
            if ($consecutive < $max) {
                return $consecutive;
            } else {
                return consecutive($prefix, $min, $max);
            }
        } else {
            return consecutive($prefix, $min, $max);
        }
    }

    /**
     * Editar direccion de entrega de pedido
     */
    public function update_delivery_address()
    {
        $id = Request::segment(3);
        $order = Order::find($id);
        $order_group = OrderGroup::find($order->group_id);

        $old_address = $order_group->user_address;
        $old_city = $order_group->user_city_id;
        $old_city = City::find($old_city);
        if ($order) {
            $address = Input::get('delivery_address');
            $address_further = Input::get('delivery_address_further');
            $address_neighborhood = Input::get('delivery_address_neighborhood');
            $city_id = Input::get('city');
            $city = City::find($city_id);

            $request = Request::create('/api/location', 'GET', array('address' => $address, 'city' => $city->city));
            Request::replace($request->input());
            $result = json_decode(Route::dispatch($request)->getContent());

            $order_group->user_address = $address;
            $order_group->user_address_further = $address_further;
            $order_group->user_address_neighborhood = $address_neighborhood;
            $order_group->user_city_id = $city_id;
            if ($result->status == 1) {
                $order_group->user_address_latitude = $result->result->latitude;
                $order_group->user_address_longitude = $result->result->longitude;
                //obtener zona
                $zone_id = null;
                $zones = Zone::where('city_id', $order_group->user_city_id)->where('status', 1)->get();
                if ($zones) {
                    foreach ($zones as $zone) {
                        if (point_in_polygon($zone->delivery_zone, $order_group->user_address_latitude, $order_group->user_address_longitude)) {
                            $zone_id = $zone->id;
                            break;
                        }
                    }
                }
                $order_group->zone_id = $zone_id;
            } else {
                $order_group->zone_id = null;
                $order_group->user_address_latitude = 1;
                $order_group->user_address_longitude = 1;
            }

            $user_address = UserAddress::find($order_group->address_id);
            if ($user_address) {
                $user_address->city_id = $city_id;
                $user_address->save();
            }

            $order_group->save();

            $log = new OrderLog();
            $log->type = 'ORDER_UPDATE_DELIVERY_ADDRESS: '.$old_city->city.' '.$old_address.' to '.$city->city.' '.$order_group->user_address;
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'La dirección de entrega del pedido fue actualizada con éxito.');
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al actualizar la dirección de netrega del pedido.');
    }

    /**
     * Editar metodo de pago del cliente
     */
    public function update_payment_method($id)
    {
        if ($order = Order::find($id)) {
            if ((!$this->admin_permissions['permission2'] && $order->status == 'Delivered') || $order->status == 'Cancelled')
                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'El estado del pedido no es valido.');

            if (!empty($order->cc_charge_id) && empty($order->cc_refund_date)) {
                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'El pedido ya fue cobrado.');
            }

            $order_group = OrderGroup::find($order->group_id);

            if (Input::get('payment_method') != 'Tarjeta de crédito') {
                $order->credit_card_id = null;
                $order->cc_token = null;
                $order->cc_installments = null;
                $order->cc_last_four = null;
                $order->cc_type = null;
                $order->cc_holder_name = null;
                $order->cc_country = null;
            } else {
                $credit_card = UserCreditCard::where('id', Input::get('credit_card_id'))->where('user_id', $order->user_id)->first();
                if (!$credit_card) {
                    return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'Tarjeta de crédito invalida.');
                }

                $order->credit_card_id = $credit_card->id;
                $order->cc_token = $credit_card->card_token;
                $order->cc_installments = 1;
                $order->cc_last_four = $credit_card->last_four;
                $order->cc_type = $credit_card->type;
                $order->cc_holder_name = $credit_card->holder_name;
                $order->cc_country = $credit_card->country;
            }

            $log = new OrderLog();
            $log->type = 'ORDER_UPDATE_PAYMENT_METHOD: '.$order->payment_method.' to '.Input::get('payment_method');
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            //validar datos de fraude
            $city = City::find($order_group->user_city_id);
            $post_data = array(
                'address' => $order_group->user_address,
                'city' => $city->slug,
                'phone' => $order_group->user_phone,
                'email' => $order_group->user_email,
                'payment_method' => Input::get('payment_method')
            );
            if (Input::get('payment_method') == 'Tarjeta de crédito')
                $post_data['credit_card_id'] = $order->credit_card_id;

            $result = $order->validateOrderFraud($post_data);
            $order->posible_fraud = empty($result['posible_fraud']) ? 0 : $result['posible_fraud'];

            $order->payment_method = Input::get('payment_method');
            $order->save();

            //actualizar movimiento de shopper
            if (ShopperMovement::where('shopper_id', $order->shopper_id)->where('status', 1)->first()) {
                if (!empty($order->shopper_id)) {
                    if ($movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first()) {
                        if (Shopper::where('id', $order->shopper_id)->where('profile', '<>', 'Shopper Cliente')->first()) {
                            $movement->description = trim($movement->description.' Método de pago actualizado a '.$order->payment_method.' desde el Administrador.');
                            $movement = ShopperMovement::saveMovement($movement, $order);
                        }
                    }
                }
            }

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'Pedido actualizado con éxito.');
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Error actualizando el método de pago.');
    }

    /**
     * Editar metodo de pago del shopper
     */
    public function update_shopper_payment_method($id)
    {
        if ($order = Order::find($id)) {
            $order_store = OrderStore::where('order_id', $order->id)->where('store_id', Input::get('store_id'))->first();
            $movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first();
            if ($order_store && $movement) {
                $order_store->shopper_payment_method = Input::get('shopper_payment_method');
                if (Input::get('shopper_payment_method') == 'Efectivo y tarjeta merqueo') {
                    $order_store->shopper_cash_paid = Input::get('shopper_cash_paid');
                    $order_store->shopper_card_paid = Input::get('shopper_card_paid');
                } else {
                    $order_store->total_real_amount = Input::get('total_real_amount');
                    $order_store->shopper_cash_paid = Input::get('shopper_payment_method') == 'Efectivo' ? $order_store->total_real_amount : 0;
                    $order_store->shopper_card_paid = Input::get('shopper_payment_method') == 'Tarjeta merqueo' ? $order_store->total_real_amount : 0;
                }

                /*if ($order_store->shopper_payment_method == 'Efectivo y tarjeta merqueo' && $order_store->total_real_amount != ($order_store->shopper_cash_paid + $order_store->shopper_card_paid))
                    return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'El total ingresado en efectivo y con tarjeta merqueo no coincide con el total de la factura.');*/

                $order_store->save();

                $order_group = OrderGroup::find($order->group_id);
                if ($order_group->source == 'Callcenter' && $order->store_id == 26) {
                    $order->total_amount = $order_store->total_real_amount;
                }
                $order->save();

                $movement->description = trim($movement->description.' Método de pago de shopper actualizado a '.Input::get('shopper_payment_method').' desde el Administrador.');
                $movement = ShopperMovement::saveMovement($movement, $order);

                $log = new OrderLog();
                $log->type = 'ORDER_UPDATE_SHOPPER_PAYMENT_METHOD: '.Input::get('shopper_payment_method');
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'Pedido actualizado con éxito.');
            }
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Error actualizando el método de pago del shopper.');
    }

    /**
     * Editar supermercado donde compro el shopper
     */
    public function update_shopper_supermarket($id)
    {
        if ($order = Order::find($id)) {
            $order_store = OrderStore::where('order_id', $order->id)->where('store_id', Input::get('store_id'))->first();
            $movement = ShopperMovement::where('order_id', $order->id)->where('status', 1)->first();
            if ($order_store && $movement) {
                $order_store->supermarket = Input::get('shopper_supermarket') == 'Otro' ? Input::get('shopper_supermarket_other') : Input::get('shopper_supermarket');
                $order_store->save();
                $movement->description =  trim($movement->description.' Supermercado actualizado a '.Input::get('shopper_supermarket').' desde el Administrador.');
                $movement->save();

                $log = new OrderLog();
                $log->type = 'ORDER_UPDATE_SHOPPER_SUPERMARKET: '.Input::get('shopper_supermarket');
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'Pedido actualizado con éxito.');
            }
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Error actualizando el supermercado.');
    }

    /**
     * Editar autorizacion supermercado donde compro el shopper
     */
    public function update_authorize_shopper_supermarket($id)
    {
        if ($order = Order::find($id)) {
            if ($order_store = OrderStore::where('order_id', $order->id)->where('store_id', Input::get('store_id'))->first()) {
                $order_store->admin_id = Input::get('shopper_supermarket_other_admin_id');
                $order_store->save();

                $log = new OrderLog();
                $log->type = 'ORDER_UPDATE_AUTHORIZE_SHOPPER_SUPERMARKET';
                $log->admin_id = Session::get('admin_id');
                $log->order_id = $order->id;
                $log->save();

                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'Pedido actualizado con éxito.');
            }
        }
        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Error actualizando el usuario que autorizó supermercado.');
    }


    /**
     * Guardar nota de cliente en pedido
     */
    public function save_note()
    {
        $id = Request::segment(3);
        $order = Order::find($id);
        if ($order) {
            $order_note = new OrderNoteSms;
            $order_note->order_id = $order->id;
            $order_note->description = Input::get('note');
            $order_note->admin_id = Session::get('admin_id');
            $order_note->save();

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'La nota fue guardada con éxito.');
        }

        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al guardar la nota.');
    }

    /**
     * Subir imagen de factura y voucher
     */
     public function upload_image($id)
     {
         if (Input::has('type') && Input::hasFile('image')) {
             $file = Input::file('image');
             $file_data['real_path'] = $file->getRealPath();
             $file_data['client_original_name'] = $file->getClientOriginalName();
             $file_data['client_original_extension'] = $file->getClientOriginalExtension();

             if (Input::get('type') == 'invoice') {
                 $dir = 'orders/invoices/'.date('Y-m-d');
             }
             if (Input::get('type') == 'voucher') {
                 $dir = 'orders/vouchers/'.date('Y-m-d');
             }

             $url = upload_image($file, false, $dir);

             if ($url) {
                 $cloudfront_url = Config::get('app.aws.cloudfront_url');
                 if (Input::get('type') == 'invoice') {
                     if ($order_store = OrderStore::where('store_id', Input::get('store_id'))->where('order_id', $id)->first()) {
                         if (!empty($order_store->invoice_image_url)) {
                             $path = str_replace($cloudfront_url, uploads_path(), $order_store->invoice_image_url);
                             remove_file($path);
                             remove_file_s3($order_store->invoice_image_url);
                         }
                         $order_store->invoice_image_url = $url;
                         $log_type = 'ORDER_INVOICE_UPLOAD';
                         $order_store->save();
                     }
                 }
                 if (Input::get('type') == 'voucher') {
                     if ($order = Order::find($id)) {
                         if (!empty($order->voucher_image_url)) {
                             $path = str_replace($cloudfront_url, uploads_path(), $order->voucher_image_url);
                             remove_file($path);
                             remove_file_s3($order->voucher_image_url);
                         }
                         $order->voucher_image_url = $url;
                         $log_type = 'ORDER_VOUCHER_UPLOAD';
                         $order->save();
                     }
                 }

                 if (isset($log_type)) {
                     //log de pedido
                     $log = new OrderLog();
                     $log->type = $log_type;
                     $log->admin_id = Session::get('admin_id');
                     $log->order_id = $id;
                     $log->save();

                     return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'success')->with('message', 'Imagen subida con éxito.');
                 }
             }
         }

         return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al subir la imagen.');
     }

    /**
     * Muestra formulario para crear pedido desde administrador
     */
    public function add()
    {
        $days = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
        $now = date('Y-m-d H:i:s');

        if (Session::get('admin_designation') == 'Cliente' && Session::get('admin_designation_store_id') == 26) {
            $cities = City::where('id', Session::get('admin_city_id'))->get();
        } else {
            $cities = City::all();
        }

        if (Session::get('admin_designation') == 'Cliente' && Session::get('admin_designation_store_id') == 26) {
            $stores = Store::where('city_id', $cities[0]->id)->where('id', Session::get('admin_designation_store_id'))->where('is_storage', 0)->where('status', 1)->get();
        } else {
            $stores = Store::where('city_id', $cities[0]->id)->where('status', 1)/*->where('is_storage', 0)*/->get();
        }

        $post = Session::has('post') ? Session::get('post') : array();

        $user = false;
        $address = false;

        if (Input::get('user_id')) {
            $user_id = Input::get('user_id');
            $user = User::find($user_id);
            $address = UserAddress::where('user_id', $user_id)->get();
        }

        return View::make('admin.orders.add')
                    ->with('user', $user)
                    ->with('address', $address)
                    ->with('cities', $cities)
                    ->with('title', 'Crear Pedido')
                    ->with('stores', $stores)
                    ->with('store', $stores[0])
                    ->with('post', $post);
    }

    /**
     * Crea pedido desde administrador
     */
    public function save()
    {
        $post_data = Input::all();
        try {
            DB::beginTransaction();

            //usuario existente
            if (isset($post_data['user_id'])) {
                $user_id = $post_data['user_id'];
                $user = User::find($user_id);
                if (empty($user->phone) && isset($post_data['phone'])) {
                    $phone = $post_data['phone'];
                    //verifica que el usuario no exista
                    if ($result = User::where('phone', $phone)->where('type', 'callcenter')->where('id', '<>', $user->id)->first()) {
                        throw new Exception('El número celular ingresado ya se encuentra en uso.');
                    }
                    $user->phone = $phone;
                    $user->save();
                }
            } else { //nuevo usuario
                $first_name = $post_data['first_name'];
                $last_name  = $post_data['last_name'];
                $email      = $post_data['email'];
                $phone      = $post_data['phone'];

                if (empty($first_name) || empty($last_name) || empty($email) || empty($phone)) {
                    throw new Exception('Debes especificar toda la información para crear el usuario.');
                }

                //verifica que el usuario no exista
                $validator = Validator::make(
                            array( 'email' => $email, 'phone' => $phone ),
                            array( 'email' => 'required|email', 'phone' => 'required|numeric|unique:users,phone,NULL,id,type,callcenter' ),
                            array( 'email.email' => 'El formato del email ingresado no es valido.',
                                   'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                                   'phone.numeric' => 'El número celular debe ser númerico.'
                            )
                );
                if ($validator->fails()) {
                    $messages = $validator->messages();
                    if (!$error = $messages->first('email')) {
                        $error = $messages->first('phone');
                    }
                    throw new Exception($error);
                }

                //validar email
                if ($user = User::where('email', $email)->where('email', '<>', 'domicilios@surtifruver.com')->where('type', 'callcenter')->first()) {
                    throw new Exception('El email ingresado ya se encuentra en uso.');
                }


                $user = User::add(trim($first_name), trim($last_name), $phone, trim($email), '', 'callcenter');
            }

            if (isset($post_data['address_id']) && !empty($post_data['address_id'])) {
                $address = UserAddress::find($post_data['address_id']);
            } else {
                $dir = $post_data['dir'];
                $address = new UserAddress;
                if (!isset($post_data['is_webservice'])) {
                    $address->address = implode('', $dir);
                    $address->address_1 = trim($dir[0]);
                    $address->address_2 = trim($dir[2]);
                    $address->address_3 = trim($dir[6]);
                    $address->address_4 = trim($dir[8]);
                } else {
                    $address->address = $dir;
                }
                $address->user_id = $user->id;
                $address->label = $post_data['label'];
                $address->address_further = $post_data['address_further'];
                $address->city_id = $post_data['city_id'];
                if (!isset($post_data['is_webservice'])) {
                    //validar direccion
                    $city = City::find($post_data['city_id']);
                    $inputs = array(
                        'address' => $address->address,
                        'city' => $city->slug
                    );
                    $request = Request::create('/api/location', 'GET', $inputs);
                    Request::replace($request->input());
                    $result = json_decode(Route::dispatch($request)->getContent(), true);
                    if (!$result['status']) {
                        throw new Exception('No pudimos ubicar la dirección por favor verificala.');
                    }
                    $address->latitude = $result['result']['latitude'];
                    $address->longitude = $result['result']['longitude'];
                } else {
                    $address->latitude = isset($post_data['latitude']) ? $post_data['latitude'] : 1;
                    $address->longitude = isset($post_data['longitude']) ? $post_data['longitude'] : 1;
                }
                if (!isset($post_data['is_webservice'])) {
                    $address->save();
                } else {
                    $address->id = 0;
                }
            }

            $store = Store::find($post_data['store_id']);
            //sin domicilio para super ahorro
            if ($store->id == 63)
                $post_data['delivery_amount'] = 4000;

            $totals['total_amount'] = 0;
            $totals['delivery_amount'] = isset($post_data['delivery_amount']) ? $post_data['delivery_amount'] : $store->delivery_order_amount;
            $totals['discount_amount'] = isset($post_data['discount_amount']) ? $post_data['discount_amount'] : 0;
            //callcenter
            if (isset($post_data['product_quantity'])) {
                $products_tmp = $post_data['product'];
                $product_id = $post_data['product_id'];
                $product_quantity = $post_data['product_quantity'];
                $product_price = $post_data['product_price'];
                $product_image = $post_data['product_image'];
                foreach ($products_tmp as $i => $product) {
                    /*if ($post_data['order_type'] != 'callcenter' && empty($product_price[$i])) {
                        throw new Exception('El precio de los productos no puede ser cero para pedidos de Merqueo.com.');
                    }*/
                    $price = empty($product_price[$i]) ? 0 : $product_price[$i];
                    $products[] = array(
                        'id' => $product_id[$i],
                        'name' => $product,
                        'qty' => $product_quantity[$i],
                        'price' => $price,
                        'image_url' => empty($product_image[$i]) ? '' : $product_image[$i]
                    );
                    $totals['total_amount'] += $price * $product_quantity[$i];
                }
                if ($store->id == 26) {
                    $totals['delivery_amount'] = $totals['delivery_amount'] + 1500;
                }
            }
            //web service (domicilios)
            if (isset($post_data['is_webservice'])) {
                $user->first_name = $post_data['first_name'];
                $user->last_name  = $post_data['last_name'];
                $user->email = $post_data['email'];
                $user->phone = $post_data['phone'];
                $products = $post_data['products'];
                foreach ($products as $i => $product) {
                    $totals['total_amount'] += $product['price'] * $product['qty'];
                }
            }

            //obtener zona
            $zone_id = null;
            $zones = Zone::where('city_id', $address->city_id)->where('status', 1)->get();
            if ($zones) {
                foreach ($zones as $zone) {
                    if (point_in_polygon($zone->delivery_zone, $address->latitude, $address->longitude)) {
                        $zone_id = $zone->id;
                        break;
                    }
                }
            }

            //crear order group
            $order_group = new OrderGroup;

            //validar datos de fraude
            $posible_fraud = Order::validateOrderFraud($post_data);
            $posible_fraud = empty($posible_fraud) ? 0 : $posible_fraud;

            $order_group->source = isset($post_data['source']) ? $post_data['source'] : 'Callcenter';
            $order_group->user_id = $user->id;
            $order_group->user_firstname = $user->first_name;
            $order_group->user_lastname = $user->last_name;
            $order_group->user_email = $user->email;
            $order_group->user_address = $address->address.' '.$address->address_further;
            $order_group->user_address_latitude = $address->latitude;
            $order_group->user_address_longitude = $address->longitude;
            $order_group->user_city_id = $address->city_id;
            $order_group->user_phone = $user->phone;
            $order_group->address_id = $address->id;
            $order_group->zone_id = $zone_id;
            $order_group->discount_amount = $totals['discount_amount'];
            $order_group->delivery_amount = $totals['delivery_amount'];
            $order_group->total_amount = $totals['total_amount'];
            $order_group->products_quantity = count($products);
            $order_group->user_comments = trim($post_data['comments']);
            $order_group->reference = isset($post_data['reference']) ? $post_data['reference'] : null;
            $order_group->ip = get_ip();
            $order_group->save();

            //dia y hora de entrega
            if ($post_data['delivery_time'] == 'immediately') {
                $hour_today = new DateTime();
                $hour_today->modify('+'.$store->delivery_time_minutes.' minutes');
                $delivery_time = $hour_today->format('H:i:s');
                $delivery_time_text = 'Immediately';
                $real_delivery_date = $post_data['delivery_day'].' '.$delivery_time;
            } else {
                $delivery_time = explode(' - ', $post_data['delivery_time']);
                $delivery_time_begin = explode(':', $delivery_time[0]);
                $hour_begin = new DateTime();
                $hour_begin->setTime($delivery_time_begin[0], $delivery_time_begin[1]);

                $delivery_time_end = explode(':', $delivery_time[1]);
                $hour_end = new DateTime();
                $hour_end->setTime($delivery_time_end[0], $delivery_time_end[1]);

                $delivery_time_text = $hour_begin->format('g:i a').' - '.$hour_end->format('g:i a');
                $hour_begin = $post_data['delivery_day'].' '.$delivery_time[0];
                if (!isset($post_data['is_webservice']) && $hour_end->format('G') < 3) {
                    $post_data['delivery_day'] = date('Y-m-d', strtotime($post_data['delivery_day'] . '+1 day'));
                }

                $hour_end = $real_delivery_date = $post_data['delivery_day'].' '.$delivery_time[1];
                $delivery_time = get_delivery_time($hour_begin, $hour_end);
            }

            //crear pedido
            $order = new Order;
            $order->reference = generate_reference();
            $order->user_id = $user->id;
            $order->store_id = $store->id;
            $order->date = date("Y-m-d H:i:s");
            $order->group_id = $order_group->id;
            $order->total_products = count($products);
            $order->delivery_amount = $totals['delivery_amount'];
            $order->discount_amount = $totals['discount_amount'];
            $order->total_amount = $totals['total_amount'];
            $order->status = $post_data['payment_method'] == 'Tarjeta de crédito' || $posible_fraud ? 'Validation' : 'Initiated';
            $order->user_score_token = generate_token();
            $order->coupon = isset($post_data['coupon']) ? $post_data['coupon'] : null;
            $order->first_delivery_date = $post_data['delivery_day'].' '.$delivery_time;
            $order->delivery_date = $post_data['delivery_day'].' '.$delivery_time;
            $order->real_delivery_date = $real_delivery_date;
            $order->delivery_time = $delivery_time_text;
            $order->payment_method = $post_data['payment_method'];
            $order->posible_fraud = $posible_fraud;

            if (isset($store['discount_percentage_amount'])) {
                $order->discount_percentage_amount = $store['discount_percentage_amount'];
            }

            if (isset($post_data['is_webservice']) && isset($post_data['is_paid'])) {
                if ($post_data['is_paid'] == 1) {
                    $order->payment_date = date('Y-m-d H:i:s');
                }
            }

            //si es merqueo de la noche asignar shopper y cambiar estado
            if ($order->store_id == 62 && empty($order->parent_order_id)) {
                $order->shopper_id = 313;
                $order->status = 'In Progress';
                $order->received_date = date('Y-m-d H:i:s');
                $order->shopper_delivery_time_minutes = $store->delivery_time_minutes;
                //enviar mail
                $delivery_date = $order->delivery_time == 'Immediately' ? format_date('normal_with_time', $order->delivery_date) : format_date('normal', substr($order->delivery_date, 0, 10)).' '.$order->delivery_time;
                $html = 'Hay un nuevo pedido para entregar.<br><br>
                <b>Referencia: </b> '.$order->reference.'<br>
                <b>Cliente: </b> '.$order_group->user_firstname.' '.$order_group->user_lastname.'<br>
                <b>Dirección: </b> '.$order_group->user_address.'<br>
                <b>Fecha de entrega: </b> '.$delivery_date.'<br>
                <b>Total a pagar: </b> $'.number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount), 0, ',', '.').'<br><br>
                Por favor ingresa al <a href="' . admin_url(true) . '" target="_blank">administrador</a> para ver detalles.<br><br>';
                $mail = array(
                    'template_name' => 'emails.daily_report',
                    'subject' => 'Nuevo pedido Merqueo de la noche',
                    'to' => array(
                        array('email' => 'gbahamon@teletrade.com.co', 'name' => 'Gilberto Bahhamon'),
                        array('email' => 'domicilios@teletrade.com.co', 'name' => 'Domicilios'),
                        array('email' => 'smontejo@merqueo.com', 'name' => 'Silvia Montejo'),
                    ),
                    'vars' => array(
                        'title' => 'Nuevo pedido en Merqueo de la noche',
                        'html' => $html
                    )
                );
                send_mail($mail);
            }

            $order->save();

            //log de pedido
            $log = new OrderLog();
            $log->type = 'ORDER_CREATED';
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            foreach ($products as $product) {
                $order_product = new OrderProduct;
                $order_product->order_id = $order->id;
                $order_product->store_product_id = isset($product['id']) ? $product['id'] : 0;
                $order_product->price = isset($product['price']) ? $product['price'] : 0;
                if ($order_product->store_product_id) {
                    if ($product_tmp = Product::find($order_product->store_product_id)) {
                        $order_product->original_price = $product_tmp->price;
                        $order_product->merqueo_discount = $product_tmp->merqueo_discount;
                        $order_product->provider_discount = $product_tmp->provider_discount;
                        $order_product->seller_discount = $product_tmp->seller_discount;
                    }
                }
                $order_product->quantity = $product['qty'];
                $order_product->fulfilment_status = 'Pending';
                $order_product->type = 'Product';
                $order_product->parent_id = 0;
                $order_product->product_name = $product['name'];
                $order_product->product_image_url = !empty($product['image_url']) ? $product['image_url'] : image_url().'no_disponible.jpg';
                $order_product->product_quantity = $product['qty'];
                $order_product->product_unit = isset($product['unit']) ? $product['unit'] : '';
                $order_product->store_id = $store->id;
                $order_product->save();
            }

            //guardar datos de campaña
            if (!isset($post_data['is_webservice'])) {
                $campaign_log = new CampaignLog;
                $campaign_log->group_id = $order_group->id;
                $campaign_log->source = 'Alianza';
                $campaign_log->medium = 'call_center';
                $campaign_log->campaign = 'Surtifruver';
                $campaign_log->save();
            }

            DB::commit();

            if (!isset($post_data['is_webservice'])) {
                return Redirect::route('adminOrders.index')->with('success', 'Pedido creado con éxito.');
            } else {
                return json_encode(array('status' => true, 'order_id' => $order->id));
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::route('adminOrders.add')->with('message', $e->getMessage())->with('post', $post_data);
        }
    }

    /**
     * Obtener tiendas por ciudad ajax
     */
    public function get_stores_ajax()
    {
        $stores = array();
        if (Input::has('city_id')) {
            $stores = Store::select('id', 'name')->where('city_id', Input::get('city_id'))->where('status', 1)->where('is_storage', 0)->get()->toArray();
        }
        return Response::json($stores, 200);
    }

    /**
     * Obtener horarios de tienda ajax
     */
    public function get_store_delivery_slot_ajax()
    {
        $delivery_slot = array();
        if (Input::has('store_id')) {
            $zone = Zone::where('status', 1)->first();
            $store = Store::find(Input::get('store_id'));
            $delivery_slot = $store->getDeliverySlotCheckout($zone);
        }
        return Response::json($delivery_slot, 200);
    }

     /**
     * Obtener productos de tienda ajax
     */
    public function get_products_ajax()
    {
        $products = array();
        if (Input::has('store_id') && Input::has('s')) {
            $store = Store::find(Input::get('store_id'));
            $products = Product::where('store_id', $store->id);

            $q = strip_tags(Input::get('s'));
            $keys = explode(' ', $q);
            if (count($keys)) {
                $keywords = '';
                foreach ($keys as $key) {
                    if (($key != '') and ($key != ' ')) {
                        if ($keywords != '') {
                            $keywords .= '-space-';
                        }
                        $keywords .= $key;
                    }
                }
                $keywords = explode('-space-', $keywords);
                foreach ($keywords as $keyword) {
                    $products->whereRaw('products.name LIKE "%'.trim($keyword).'%" COLLATE utf8_spanish_ci');
                }
            }

            $products = $products->select('id', 'name', 'quantity', 'unit', 'price', 'special_price', 'image_small_url', 'image_medium_url')->distinct();
            //daniela y kelly
            if (!in_array(Session::get('admin_id'), [36, 97]))
                $products->where('status', 1);
            $products = $products->orderBy('products.name', 'desc')->limit(6)->get();

            return View::make('admin.autocomplete')
                        ->with('products', $products)
                        ->with('search', $q)
                        ->with('query', urlencode($q))
                        ->with('store', $store);
        }
    }

    /**
     * Filtra las tiendas y los shoppers por ciudad
     */
    public function get_stores_shoppers_by_city_ajax()
    {
        $city_id = Input::get('city_id');

        $shoppers = Shopper::where('status', 1);
        $zones = Zone::where('status', 1);
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('stores.status', 1);

        if (Session::get('admin_designation') == 'Cliente') {
            $city_id = Session::get('admin_city_id');
            $stores->where('stores.id', Session::get('admin_designation_store_id'));
        }

        if ($city_id) {
            $shoppers->where('city_id', $city_id);
            $stores->where('city_id', $city_id);

            if (Session::get('admin_designation') == 'Cliente') {
                $stores->where('orders.store_id', Session::get('admin_designation_store_id'));
            }

            $shoppers = $shoppers->orderBy('first_name')->orderBy('last_name')->get();
            $zones = $zones->orderBy('name')->get();
            $stores = $stores->get();

            $result['shoppers'] = $shoppers;
            $result['stores'] = $stores;
            $result['zones'] = $zones;

            return Response::json($result);
        }

        $shoppers = $shoppers->orderBy('first_name')->orderBy('last_name')->get();
        $zones = $zones->orderBy('name')->get();
        $stores = $stores->get();

        $result['shoppers'] = $shoppers;
        $result['stores'] = $stores;
        $result['zones'] = $zones;

        return Response::json($result);
    }

    /**
     * Función para obtener los shopper por zonas a través de ajax.
     * @return json
     */
    public function get_shoppers_by_zones_ajax()
    {
        $zone_id = Input::get('zone_id', null);
        $store_id = Input::get('store_id', null);

        $_shoppers = Shopper::orderBy('first_name')
                        ->where('shoppers.status', 1)
                        ->where('shoppers.is_active', 1)
                        ->leftJoin('orders AS o', function ($join) {
                            $join->on('shoppers.id', '=', 'o.shopper_id')
                                ->where('o.status', '<>', 'Delivered')
                                ->where('o.status', '<>', 'Cancelled');
                        });
        if (!empty($zone_id)) {
            $_shoppers->join('shopper_zones', function ($join) use ($zone_id) {
                $join->on('shoppers.id', '=', 'shopper_zones.shopper_id')
                                    ->where('shopper_zones.zone_id', '=', $zone_id);
            });
        }
        $_shoppers = $_shoppers->groupBy('shoppers.id')
                        ->select('shoppers.*', DB::raw('count(o.id) AS num_orders'))
                        ->get()->toArray();
        $shoppers = [];
        foreach ($_shoppers as $shopper) {
            if (!empty($shopper['stores'])) {
                $shopper_stores = explode(',', $shopper['stores']);
                if (!in_array($store_id, $shopper_stores)) {
                    continue;
                }
            }
            $shopper['updated_at_formatted'] = get_time('left', $shopper['last_date_location'], null, false);
            $shoppers[$shopper['id']] = $shopper;
        }

        return Response::json($shoppers);
    }

    /**
     *  Genera factura de pedido
     *
     *  @param  $reference Número de refencia de un pedido
     *  @return $file PDF generado
     */
    public function generate_invoice($id)
    {
        $order = Order::select('orders.*', 'stores.name AS store_name')
                       ->join('stores', 'store_id', '=', 'stores.id')
                       ->where('orders.id', $id)->first();
        if ($order) {
            if ($order->status === 'Delivered') {
                Order::generateInvoice($order);
            } else {
                return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'failed')->with('message', 'El pedido no esta entregado.');
            }
        } else {
            return Redirect::route('adminOrders.index')->with('type', 'failed')->with('message', 'No existe el pedido.');
        }
    }

    /**
     *  Guardar datos para facturacion
     */
    public function save_invoice_data()
    {
        $id = Request::segment(3);
        $order = Order::find($id);

        if ($order) {
            if (Input::has('user_identity_type')) {
                $order->user_identity_type   = Input::get('user_identity_type');
            }
            if (Input::has('user_identity_number')) {
                $order->user_identity_number = Input::get('user_identity_number');
            }
            if (Input::has('user_business_name')) {
                $order->user_business_name   = Input::get('user_business_name');
            }

            $order->save();

            $log = new OrderLog();
            $log->type = 'ORDER_UPDATE_USER_DATA_INVOICE: Bussines name:'.Input::get('user_business_name').' , Document:'.Input::get('user_identity_number');
            $log->admin_id = Session::get('admin_id');
            $log->order_id = $order->id;
            $log->save();

            return Redirect::route('adminOrders.details', ['id' => $order->id])->with('type', 'success')->with('message', 'Pedido actualizado, ya puedes descargar la factura.');
        }

        return Redirect::route('adminOrders.details', ['id' => $id])->with('type', 'failed')->with('message', 'Ocurrió un error al actualizarlos datos.');
    }

   /**
     *  Validar razón social y documento paea factura
     */
    public function verify_data_invoice()
    {
        $id = Input::get('id');
        $order = Order::select('orders.*', 'stores.name AS store_name')
                       ->join('stores', 'store_id', '=', 'stores.id')
                       ->where('reference', $id)->first();

        if (!empty($order->user_identity_type) && !empty($order->user_identity_type) && !empty($order->user_business_name)) {
            $response['success'] = 'true';
        } else {
            if ($user = User::find($order->user_id)) {
                if (!empty($user->identity_number) && !empty($user->identity_type) && !empty($user->business_name)) {
                    $response['success'] = 'true';
                } else {
                    $response['success'] = 'false';
                    $response['error'] = 'Ocurrió un error al validar los datos de usuario';
                    $response['error_code'] = 413;
                }
            } else {
                $response['success'] = 'false';
                $response['error'] = 'Ocurrió un error al validar los datos de usuario';
                $response['error_code'] = 413;
            }
        }
        return Response::json($response, 200);
    }

    /**
     * Función para obtener la información de ordenes por ajax.
     */
    public function get_allocate_shopper_info_ajax()
    {
        $order_id = Input::get('order_id', null);

        $order = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
        ->join('cities AS cities_user', 'cities_user.id', '=', 'order_groups.user_city_id')
        ->join('stores', 'stores.id', '=', 'orders.store_id')
        ->join('cities AS cities_store', 'cities_store.id', '=', 'stores.city_id')
        ->leftJoin('user_credit_cards', 'orders.credit_card_id', '=', 'user_credit_cards.id')
        ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
        ->leftJoin(DB::raw('(user_credits, coupons)'), function ($join) {
            $join->on('user_credits.order_id', '=', 'orders.id');
            $join->on('user_credits.type', '=', DB::raw('0'));
            $join->on('user_credits.coupon_id', '=', 'coupons.id');
            $join->on('user_credits.status', '=', DB::raw('1'));
        })
        ->leftJoin('user_brand_campaigns', 'orders.id', '=', 'user_brand_campaigns.order_id')
        ->where('orders.id', $order_id)
        ->select('orders.*', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email', 'coupons.code AS coupon_code',
            'order_groups.user_address', 'order_groups.user_phone', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source',
            'stores.name AS store_name', 'stores.id AS store_id', 'cities_store.city AS store_city', 'zones.id AS zone_id', 'zones.name AS zone', 'orders.invoice_number',
            'order_groups.source_os', 'order_groups.user_comments', 'cities_user.city', 'cities_user.id AS city_id', DB::raw("SUM(orders.total_amount) AS total_amount_og"),
            'user_brand_campaigns.campaign', 'order_groups.user_address_latitude', 'order_groups.user_address_longitude');

        if (Session::get('admin_designation') != 'Super Admin') {
            $order->where('order_groups.user_city_id', Session::get('admin_city_id'));
        }
        if (Session::get('admin_designation') == 'Cliente') {
            $order->where('orders.store_id', Session::get('admin_designation_store_id'));
        }

        $order = $order->first();

        if (!$order->id) {
            return App::abort(404);
        }

        $shoppers = $this->get_shoppers_by_order_zone($order);
        $shopper = Shopper::where('id', $order->shopper_id)->get()->toArray();

        $holidays = new Holidays();
        $day_num = date('N');
        $date_data = explode('-', date('Y-m-d'));
        $holidays->festivos($date_data[0]);
        $is_holiday = $holidays->esFestivo($date_data[2], $date_data[1]);
        if ($is_holiday) {
            $day_num = 0;
        }

        $hour = date('H:i:s');
        $store_branches = StoreBranch::where('store_id', $order->store_id)
                            ->join('stores', 'store_branches.store_id', '=', 'stores.id')
                            ->where('store_branches.status', 1)
                            ->select('store_branches.*', 'stores.name AS store_name')
                            ->get()
                            ->toArray();

        foreach ($store_branches as $key => &$branch) {
            $branch_slot = StoreBranchSlot::where('store_branch_id', DB::raw($branch['id']))
                            ->where('day', DB::raw($day_num))
                            ->whereRaw('start_time <= "'.$hour.'"')
                            ->whereRaw('end_time >= "'.$hour.'"')
                            ->get()->toArray();
            if (empty($branch_slot)) {
                unset($store_branches[$key]);
            } else {
                $branch['schedule'] = $branch_slot;
            }
        }

        $supermarket_ids = array_fetch($store_branches, 'supermarket_id');
        $supermarket_ids = array_unique($supermarket_ids);

        $supermarkets = Supermarket::whereIn('id', $supermarket_ids)->get()->toArray();

        $zones = Zone::where('city_id', $order->city_id)->where('status', 1)->get()->toArray();
        $draw_zones = [];
        foreach ($zones as $key => $zone) {
            $draw_zones[$key] = explode(',', $zone['delivery_zone']);
        }
        $data = [

        'shoppers_active' =>$shoppers,
        'current_shopper' => $shopper,
        'store_branches' => $store_branches,
        'zones' => $zones,
        'supermarkets' => $supermarkets,
        'draw_zones' => $draw_zones,
        'order' => $order
        ];
        return Response::json($data, 200);
    }

    /**
     * Reenviar email de entregado
     */
    public function re_send_delivery_email($id)
    {
        $order = Order::find($id);
        $order_group = OrderGroup::find($order->group_id);
        $store = Store::find($order->store_id);
        $user = User::find($order->user_id);
        $shopper = Shopper::getShopper($order->shopper_id);
        $products = OrderProduct::where('order_id', $order->id)->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available','Fullfilled', 'Replacement')"))->get();
        foreach ($products as $key => $product) {
            $products_mail[$order->id][] = [
                'img' => $product->product_image_url,
                'name' => $product->product_name,
                'quantity' => $product->product_quantity,
                'unit' => $product->product_unit,
                'qty' => $product->quantity,
                'price' => $product->price,
                'total' => $product->quantity * $product->price,
                'type' => $product->type,
                'status' => $product->fulfilment_status != 'Not Available' ? 'available.png' : 'no_available.png'
            ];
        }

        $mail = [
            'template_name' => 'emails.order_delivered',
            'subject' => 'Tu pedido en '.$store->name.' ha sido entregado',
            'to' => [
                [
                    'email' => $order_group->user_email,
                    'name' => $order_group->user_firstname.' '.$order_group->user_lastname
                ]
            ],
            'vars' => [
                'order' => $order,
                'order_group' => $order_group,
                'store' => $store,
                'shopper' => $shopper,
                'reject_reason' => $order->reject_reason,
                'products' => $products_mail[$order->id]
            ]
        ];
        $result = send_mail($mail);
        if ($result) {
            return Redirect::back()->with('message', 'Correo enviado.')->with('type', 'success');
        }

        return Redirect::back()->with('message', 'Ha ocurrido un error al enviar el correo.')->with('type', 'error');
    }

    /**
     * Envía mensajes de texto
     * @param  int $id Identificador de órdenes
     */
    public function send_sms($id)
    {
        $message = trim(Input::get('sms', null));
        $order = Order::where('orders.id', $id)->join('users AS u', 'orders.user_id', '=', 'u.id')->select('u.phone')->first();
        if (!empty($order) && !empty($message)) {
            $result = send_sms($order->phone, $message);
            if ($result) {
                $order_notes_sms = new OrderNoteSms;
                $order_notes_sms->order_id = $id;
                $order_notes_sms->admin_id = Session::get('admin_id');
                $order_notes_sms->type = 'sms';
                $order_notes_sms->description = $order->phone.' || '.$message;
                $order_notes_sms->save();
                return Redirect::back()->with('success', 'El mensaje de texto se ha enviado correctamente.');
            }
            return Redirect::back()->with('error', 'Ha ocurrido un error al enviar el mensaje de texto.');
        }
        return Redirect::back()->with('error', 'No se encontró la orden.');
    }

    /**
     * Obtener supermercados por tienda
     */
    public function get_stores_by_supermarket_ajax()
    {
        $supermarket_id = Input::get('supermarket_id', null);
        $store_id = Input::get('store_id', null);

        $holidays = new Holidays();
        $day_num = date('N');
        $date_data = explode('-', date('Y-m-d'));
        $holidays->festivos($date_data[0]);
        $is_holiday = $holidays->esFestivo($date_data[2], $date_data[1]);
        if ($is_holiday) {
            $day_num = 0;
        }

        $hour = date('H:i:s');

        $store_branches = StoreBranch::where('store_id', $store_id);
        if (!empty($supermarket_id)) {
            $store_branches = $store_branches->where('store_branches.supermarket_id', $supermarket_id);
        }
        $store_branches = $store_branches->join('stores', 'store_branches.store_id', '=', 'stores.id')
                            ->where('store_branches.status', 1)
                            ->select('store_branches.*', 'stores.name AS store_name')
                            ->get()
                            ->toArray();

        foreach ($store_branches as $key => &$branch) {
            $branch_slot = StoreBranchSlot::where('store_branch_id', DB::raw($branch['id']))
                            ->where('day', DB::raw($day_num))
                            ->whereRaw('start_time <= "'.$hour.'"')
                            ->whereRaw('end_time >= "'.$hour.'"')
                            ->get()->toArray();
            if (empty($branch_slot)) {
                unset($store_branches[$key]);
            } else {
                $branch['schedule'] = $branch_slot;
            }
        }

        $data = [
            'store_branches' => $store_branches
        ];
        return Response::json($data, 200);
    }

    /**
     * Obtener ordenes afectadas
     */

    public function order_affected()
    {
        $stores = Store::select('stores.*', 'cities.city AS city_name')->join('cities', 'city_id', '=', 'cities.id')->where('status', 1);

        $cities = City::all();
        $search = Input::has('s') ? Input::get('s') : '';
        $orders = DB::table('provider_order_users')
                    ->join('orders', 'provider_order_users.order_id', '=', 'orders.id')
                    ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                    ->join('stores', 'orders.store_id', '=', 'stores.id')
                    ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->leftJoin('shoppers', 'orders.shopper_id', '=', 'shoppers.id')
                    ->leftJoin('zones', 'order_groups.zone_id', '=', 'zones.id')
                    ->leftJoin(DB::raw('(user_credits, coupons)'), function ($join) {
                        $join->on('user_credits.order_id', '=', 'orders.id');
                        $join->on('user_credits.type', '=', DB::raw('0'));
                        $join->on('user_credits.coupon_id', '=', 'coupons.id');
                    })
                    ->where(function ($query) use ($search) {
                        $query->where(DB::raw('CONCAT(order_groups.user_firstname, " ", order_groups.user_lastname)'), 'LIKE', '%'.$search.'%');
                        $query->orWhere('orders.id', 'LIKE', '%'.$search.'%');
                        $query->orWhere('orders.reference', 'LIKE', '%'.$search.'%');
                        $query->orWhere('order_groups.user_email', 'LIKE', '%'.$search.'%');
                        $query->orWhere('order_groups.user_address', 'LIKE', '%'.$search.'%');
                        $query->orWhere('order_groups.user_phone', 'LIKE', '%'.$search.'%');
                    })
                    ->select('orders.*', 'users.sift_payment_abuse', 'order_groups.user_firstname', 'order_groups.user_lastname', 'order_groups.user_email', 'order_groups.user_address', 'order_groups.user_phone',
                    'order_groups.user_comments', 'orders.delivery_date AS customer_delivery_date', 'order_groups.source', 'order_groups.source_os', 'zones.name AS zone',
                    'shoppers.first_name', 'shoppers.last_name', 'cities.city', 'stores.name AS store', 'coupons.code AS coupon_code', 'delivery_time_minutes AS store_delivery_time_minutes',
                    DB::raw("ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), orders.delivery_date))/3600, 1) AS diff"), 'posible_fraud', DB::raw("SUM(orders.total_amount) AS total_amount_og"));
                //filtros
                if (Input::get('store_id')) {
                    $orders->where('stores.id', Input::get('store_id'));
                }
        if (Input::get('show_orders')) {
            if (Input::get('show_orders') == 'today') {
                $orders->where('orders.delivery_date', '>', date('Y-m-d 00:00:00'));
                $orders->where('orders.delivery_date', '<', date('Y-m-d 23:59:59'));
            }
        }
        if (Input::get('status')) {
            $status = Input::get('status');
            $statuses = explode(',', $status);
            unset($statuses[count($statuses) - 1]);
            $orders->where(function ($query) use ($statuses) {
                foreach ($statuses as $status) {
                    $query->orWhere('orders.status', $status);
                }
            });
        }
        if (Input::get('city_id')) {
            $city_id = Input::get('city_id');
            $orders->where('cities.id', $city_id);
        }
        if (Input::get('order_by')) {
            if (Input::get('order_by') == 'unfulfilled_orders') {
                $orders->orderBy(DB::raw("FIELD(orders.status, 'Validation', 'Initiated', 'In Progress', 'Dispatched', 'Delivered', 'Cancelled')"))->orderBy('diff', 'desc');
            } else {
                $orders->orderBy(Input::get('order_by'), 'ASC');
            }
        }

        $orders->groupBy('orders.id');

        $orders = $orders->paginate(20);

        $data = [
            'title' => 'Ordenes Afectadas',
            'orders' => $orders,
            'cities' => $cities,
            'stores' => $stores
        ];

        return View::make('admin.orders.affected', $data);
    }
}
