<?php

namespace admin\picking;

use Carbon\Carbon;
use Request, Input, View, Session, Order, Response, Validator, DB, Config, Locality, City, admin\AdminController, Routes;

class RouteConstructorController extends AdminController
{
    /**
     * RouteConstructorController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Punto de acceso al modulo
     * @return \Illuminate\Contracts\View\View
     */
    public function index(){
        if(!Request::ajax()) {

            $input = Input::all();
            $city = isset($input['city_id']) ? $input['city_id'] : Session::get('admin_city_id');
            $data = [
                'cities' => $this->get_cities(),
                'title' => 'Armador de rutas',
                'city' => $city,
                'warehouses' => $this->get_warehouses($city),
                'warehouse_id' => null,
                'shifts' => [''=>'Seleccione', 'AM'=>'Rutas de la mañana', 'PM'=> 'Rutas de la tarde', 'MD' => 'Rutas del mismo día'],
                'shift' => 'AM'
            ];

            return View::make('admin.picking.route_constructor.index', $data);
        }
    }


    /**
     * Funcion que retorna las rutas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoutes(){
        if(Request::ajax()){
            $planningDateStart = Carbon::now()->format('Y-m-d');
            $planningDateEnd = Carbon::now()->addDay(1)->format('Y-m-d');
            return Response::json([
                'status' => true,
                'result' => Routes::getRoutes($planningDateStart, $planningDateEnd,Input::get('shift'), Input::get('warehouse_id'))
            ]);
        }
    }

    /**
     * Funcion que retorna una orden de una ruta
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders()
    {
        if(Request::ajax()){
            return Response::json([
                'status' => true,
                'result' => View::make('admin.picking.route_constructor.order', ['order' => Routes::getOrdersToRouteConstructor(Input::get('route_id'))])->renderSections()['content']
            ]);
        }
    }

    /**
     * Función que guarda el movimiento de la canastilla a la ruta
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function save()
    {
        if(Request::ajax()){
            try{
                DB::beginTransaction();
                $order = Order::find(Input::get('order_id'));
                $order->baskets_to_route_date = Carbon::now();
                $order->save();
                DB::commit();
                return Response::json([
                    'status' => true,
                    'message' => 'Pedido movido ha ruta con éxito.'
                ]);
            }catch (Exception $e){
                DB::rollBack();
                \ErrorLog::add($e);
                return Response::json([
                    'status' => false,
                    'message' => 'Ocurrio un error al  mover el pedido a la ruta'
                ]);
            }
        }
    }


}