<?php

class RemindersController extends BaseController {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
	    //metadata
        $city = City::find(Session::get('city_id'));
        $metadata['city_name'] = $city->city;
        $metadata['city_slug'] = $city->slug;
        $metadata['stores'] = '';

	    return View::make('password_remind')->with('metadata', $metadata)->with('footer', $this->getFooter());
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		if ($user = Password::getUser(array('email' => Input::get('email'))))
		{
		    //generar token
		    $token = str_shuffle(sha1($user->email.microtime(true)));
            $token = hash_hmac('sha1', $token, 'ofkshgokshogkis');
            $values = ['email' => $user->email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')];
            DB::table('password_reminders')->insert($values);

		    $vars = array(
				'username' => $user->first_name,
	            'website_name' => Config::get('app.website_name'),
	            'user_url' => url('/mi-cuenta'),
	            'contact_email' => Config::get('app.admin_email'),
				'contact_phone' => Config::get('app.admin_phone'),
				'android_url' => Config::get('app.android_url'),
				'ios_url' => Config::get('app.ios_url'),
				'facebook_url' => Config::get('app.facebook_url'),
				'twitter_url' => Config::get('app.twitter_url'),
                'token' => $token
	        );

            $mail = array(
                'template_name' => 'emails.auth.reminder',
                'subject' => 'Restaurar contraseña',
                'to' => array(
                    array('email' => $user->email, 'name' => $user->first_name)
                ),
                'vars' => $vars
            );
            $result = send_mail($mail);

            if ($result)
                return Redirect::back()->with('status', Lang::get('reminders.sent'));
            else return Redirect::back()->with('error', Lang::get('reminders.error'));

		}else return Redirect::back()->with('error', Lang::get('reminders.user'));
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token))
	      App::abort(404);
		if (Session::has('post'))
			$post = Session::get('post');
		else $post = array();

        //metadata
        $city = City::find(Session::get('city_id'));
        $metadata['city_name'] = $city->city;
        $metadata['city_slug'] = $city->slug;
        $metadata['stores'] = '';

		return View::make('password_reset')
					->with('token', $token)
					->with('post', $post)
                    ->with('metadata', $metadata)
                    ->with('footer', $this->getFooter());
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		if(!input::has('token')){
			return Redirect::back()->with('error', 'Token inválido')->with('post', Input::all());
		}else{
			$user = PasswordReminder::where('token', '=', input::get('token'))->first();
			$credentials = Input::only('password', 'password_confirmation', 'token');
			$user_agent = get_user_agent();

			if(count($user))
				$credentials['email'] = $user->email;

			$response = Password::reset($credentials, function($user, $password)
			{
				$user->password = Hash::make($password);
				$user->save();
			});

			switch ($response)
			{
				case Password::INVALID_PASSWORD:
				case Password::INVALID_TOKEN:
				case Password::INVALID_USER:
					return Redirect::back()->with('error', Lang::get($response))->with('post', Input::all());

				case Password::PASSWORD_RESET:
					return View::make('password_thankyou')->with('user_agent', $user_agent);
			}
		}
	}

}
