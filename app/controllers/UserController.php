<?php

use \tickets\TicketCall;
use \tickets\Ticket;
use \Illuminate\Database\Eloquent\ModelNotFoundException;
use \Symfony\Component\HttpKernel\Exception\HttpException;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \AbTesting\Optimizely\Experiments\Events\ProductAddedLabel;
use \AbTesting\Optimizely\Experiments\Events\ProductAdded;
use \Merqueo\Backend\Auth\AccountKit\AccountKit;
use Carbon\Carbon;
use usecases\Interfaces\Checkout\DeliveryDiscountUseCaseInterface;

class UserController extends BaseController
{
    public $user_credit;
    public $metadata;
    public $mobile_url = null;

    /**
     * @var DeliveryDiscountUseCaseInterface
     */
    private $deliveryDiscountUseCase;

    public function __construct(
        DeliveryDiscountUseCaseInterface $deliveryDiscountUseCase
    ) {
        parent::__construct();

        $this->deliveryDiscountUseCase = $deliveryDiscountUseCase;

        $this->beforeFilter(function () {
            if (Session::has('store_id')) {
                $this->store = Store::find(Session::get('store_id'));
            }
            $this->user_id = Auth::check() ? Auth::user()->id : 0;
            $lat = Session::get('latitude') ? Session::get('latitude') : 1;
            $lng = Session::get('longitude') ? Session::get('longitude') : 1;
            $this->stores = Store::getByLatLng($lat, $lng);
            if ($this->stores instanceof Store) {
                $this->stores = [$this->stores];
            }
            $this->user_discounts = User::getDiscounts();

            //metadata
            $city = City::find(Session::get('city_id'));
            $this->metadata['city_name'] = $city->city;
            $this->metadata['city_slug'] = $city->slug;
            $this->metadata['stores'] = '';
            if ($n = count($this->stores)) {
                $cont = 1;
                foreach ($this->stores as $store) {
                    if ($cont != $n) {
                        $this->metadata['stores'] .= $store->name . ', ';
                    } else {
                        $this->metadata['stores'] = trim($this->metadata['stores'], ', ');
                        $this->metadata['stores'] .= ' y ' . $store->name;
                    }
                    $cont++;
                }
            }
        }, array('except' => array('login', 'verify', 'social', 'coupon', 'logout', 'tester', 'generate_invoice')));
    }

    /**
     * Muestra formulario de login
     */
    public function login()
    {
        if (Auth::check()) {
            return Redirect::to('/');
        }

        $city = City::remember(10)->find(Session::get('city_id'));
        $metadata['city_name'] = $city->city;
        $metadata['city_slug'] = $city->slug;

        $redirect = null;
        if (Input::get('redirect')) {
            $redirect = Input::get('redirect');
        }

        return View::make('login')->with('metadata', $metadata)->with('redirect', $redirect)->with('footer',
            $this->getFooter());
    }

    /**
     * Cerrar sesion
     */
    public function logout()
    {
        $all = Session::all();
        $admin_keys = preg_filter('/^admin_(.*)/', '$1', array_keys($all));
        Session::flush();
        Auth::logout();
        foreach ($admin_keys as $key) {
            Session::put('admin_' . $key, $all['admin_' . $key]);
        }
        return Redirect::to('/');
    }

    /**
     * Realiza login de usuario.
     *
     * @param null|string $redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify($redirect = null)
    {
        $redirect = $redirect ? str_replace(';', '/', $redirect) : $redirect;
        $email = Input::get('email');
        $password = Input::get('password');

        if (empty($email) || empty($password)) {
            return $redirect ? Redirect::to($redirect) : Redirect::back();
        }

        $user = User::where('email', $email)
            ->whereIn('type', array_merge(
                ['merqueo.com'],
                User::DARKSUPERMARKET_TYPE
            ))->first();

        if ($user) {
            if (Hash::check($password, $user->password)) {
                if ($user->status) {
                    Auth::login($user);
                    $warehouse = $this->getCurrentWarehouse();
                    $cart = Cart::with('warehouse')
                        ->where('warehouse_id', $warehouse->id)
                        ->where('user_id', Auth::user()->id)
                        ->orderBy('updated_at', 'desc')
                        ->first();

                    if (in_array(Auth::user()->id, User::DARKSUPERMAKET_MEXICO)){
                        $city_slug = Store::SLUG_DARK_MEXICO;
                        $redirect = $city_slug.'/domicilios-super-ahorro';
                    }

                    if (empty($cart->warehouse) || $cart->warehouse->city_id != Session::get('city_id')) {
                        $cart = null;
                    } elseif (!empty($cart->warehouse->city_id)) {
                        Session::put('city_id', $cart->warehouse->city_id);
                        Cookie::queue('city_id', $cart->warehouse->city_id, 2628000);
                    }

                    if (!$cart) {
                        $cart = new Cart();
                        $cart->warehouse()->associate($warehouse);
                        $cart->user()->associate($user);
                        $cart->save();
                    }

                    // Si el usuario tiene un carrito en sesión, lo pongo en el carrito actual borrando lo que tenga en db
                    if (Session::has('tmp_cart')) {
                        $tmp_cart = json_decode(Session::pull('tmp_cart'));
                        if (!empty($tmp_cart->products) && !empty(get_object_vars($tmp_cart->products))) {
                            CartProduct::where('cart_id', $cart->id)->delete();
                            foreach ($tmp_cart->products as $product) {
                                if ($product->cart_quantity <= 0) {
                                    continue;
                                }

                                $cart_product = new CartProduct();
                                $cart_product->cart_id = $cart->id;
                                $cart_product->store_product_id = $product->store_product_id;
                                $cart_product->quantity = $product->cart_quantity;
                                $cart_product->quantity_full_price = $product->cart_quantity_full_price;
                                $cart_product->added_by = $user->id;
                                $cart_product->save();
                            }
                        }
                    }

                    Session::put('cart_id', $cart->id);
                    Session::save();

                    self::trackEventOnBrowser(['user_signed_in' => [
                        'screen' => StoreController::getReferrer()
                    ]]);

                    return $redirect ? Redirect::intended($redirect) : Redirect::back();
                } else {
                    Session::flash('message', 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros.');
                }
            } else {
                Session::flash('message', 'La contraseña es incorrecta.');
            }
        } else {
            Session::flash('message', 'El email es incorrecto.');
        }
        $redirect = $redirect ? str_replace('/', ';', $redirect) : $redirect;

        return Redirect::route('frontUser.login', ['redirect' => $redirect]);
    }

    /**
     * Registrar nuevo usuario
     */
    public function register()
    {
        $user_agent = get_user_agent();

        if (Session::get('city_id') == 1) {
            $store_product_ids = [695, 731, 635];
        } else {
            $store_product_ids = [3238, 3274, 3178];
        }
        $products = StoreProduct::whereIn('store_products.id', $store_product_ids)
            ->where('c.id', Session::get('city_id'))
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('stores AS s', 'store_products.store_id', '=', 's.id')
            ->join('cities AS c', 's.city_id', '=', 'c.id')
            ->join('departments AS d', 'store_products.department_id', '=', 'd.id')
            ->join('shelves AS sh', 'store_products.shelf_id', '=', 'sh.id')
            ->select(
                'products.id',
                'products.name',
                'products.image_app_url',
                'products.quantity',
                'products.unit',
                'store_products.special_price',
                'store_products.price',
                'products.slug',
                'store_products.is_best_price',
                'c.slug AS city_slug',
                's.slug AS store_slug',
                'd.slug AS department_slug',
                'sh.slug AS shelf_slug',
                'products.type',
                'store_products.special_price AS pum_special_price'
            )
            ->pum()
            ->get();

        //si fue registro con fb
        if ($this->user_id && Session::has('show_register_success')) {
            $promo_code = Session::get('show_register_success');
            $referred['success'] = $referred['limit'] = false;
            $referred['amount'] = Config::get('app.referred.amount');
            $user = Auth::user();

            // Si utilizo codigo de referido
            if ($promo_code) {
                // Si utilizo referido
                if ($user->referred_by) {
                    $referred['success'] = true;
                } else {
                    // Si utilizo referido con mas del limite de pedidos redimidos
                    $referrer = User::where('referral_code', $promo_code)->first();
                    if ($referrer && $referrer->total_referrals >= Config::get('app.referred.limit')) {
                        $referred['limit'] = true;
                    }
                }
            }

            $referred['error'] = !$referred['success'] && !$referred['limit'] && $promo_code ? true : false;
            Session::forget('show_register_success');

            return View::make('register')
                ->with('success', true)
                ->with('user', $user)
                ->with('referred', $referred)
                ->with('stores', $this->stores)
                ->with('metadata', $this->metadata)
                ->with('footer', $this->getFooter())
                ->with('products', $products)
                ->with('user_agent', $user_agent);
        }

        if ($this->user_id) {
            return Redirect::to('/');
        }

        $this->metadata['code_referred_title'] = Config::get('app.referred.share_title') . '.';
        $this->metadata['code_referred_description'] = Config::get('app.referred.share_description');

        if (Request::isMethod('post')) {
            $first_name = Input::get('first_name');
            $last_name = Input::get('last_name');
            $email = Session::get('facebookData.email') ?: Input::get('email');
            $password = Input::get('password');
            $phone = trim(Session::get('phoneValidation.number'));

            if (!Session::get('phoneValidation.validated')) {
                Session::put('post', Input::all());
                return Redirect::back()->with('message', 'Debes validar tu número de celular.');
            }

            if (empty($first_name) || empty($last_name) || empty($email) || empty($password)) {
                return Redirect::back()
                    ->with('message', 'Debes especificar toda la información para crear tu usuario.')
                    ->with('post', Input::all());
            }

            //verifica que el usuario no exista
            $validator = Validator::make(
                compact('email', 'phone'),
                [
                    'email' => 'required|email|unique:users,email,NULL,id,type,merqueo.com',
                    'phone' => 'required|numeric|digits:10|unique:users,phone,NULL,id,type,merqueo.com'
                ],
                [
                    'email.unique' => 'El email ingresado ya se encuentra en uso.',
                    'email.email' => 'El formato del email ingresado no es valido.',
                    'phone.unique' => 'El número celular ya se encuentra en uso. Escríbenos a ' .
                        \Config::get('app.admin_email') . ' si necesitas ayuda para la creación de tu cuenta.',
                    'phone.numeric' => 'El número celular debe ser númerico.',
                    'phone.digits' => 'El número celular debe tener 10 digitos.'
                ]
            );
            if ($validator->fails()) {
                $messages = $validator->messages();
                if ($messages->first('phone')) {
                    RegisterErrorLog::add(new Exception($messages->first('phone')));
                }

                if (!$error = $messages->first('email')) {
                    $error = $messages->first('phone');
                }
                return Redirect::back()
                    ->with('message', $error)
                    ->with('post', Input::all());
            }

            //crear el usuario, realizar login y enviar mail de bienvenida
            $user = User::add(trim($first_name), trim($last_name), $phone, trim($email), $password);
            $user->origin = 'web';
            $user->phone_validated_date = Carbon::create();
            $user->fb_id = Session::get('facebookData.id');
            $user->save();

            Auth::login($user);

            self::trackEventOnBrowser(['user_signed_up' => [
                'screen' => StoreController::getReferrer()
            ]]);

            //validar y cargar codigo de referido
            $referred = User::validateReferred($user);

            $mail = [
                'template_name' => 'emails.welcome',
                'subject' => 'Bienvenido a Merqueo',
                'to' => [['email' => $user->email, 'name' => $user->first_name . ' ' . $user->last_name]],
                'vars' => [
                    'referral_code' => $user->referral_code,
                    'referred' => $referred,
                    'name' => $user->first_name
                ]
            ];
            send_mail($mail);

            $cart = new Cart;
            $cart->user_id = $user->id;
            $cart->save();

            Session::put('cart_id', $cart->id);
            Session::forget('referred_code');
            Session::forget('coupon_code');
            Session::forget('phoneValidation');
            Session::forget('facebookData');
            Session::save();

            return View::make('register')
                ->with('success', true)
                ->with('user', $user)
                ->with('referred', $referred)
                ->with('stores', $this->stores)
                ->with('metadata', $this->metadata)
                ->with('footer', $this->getFooter())
                ->with('products', $products)
                ->with('user_agent', $user_agent);
        }

        for ($i = 0; $i <= 9; $i++) {
            $year = date('Y') + $i;
            for ($j = 1; $j <= 12; $j++) {
                if ($j < 10) {
                    $j = '0' . $j;
                }
                $dates_cc[] = $year . '/' . $j;
            }
        }

        $post = Session::has('post') ? Session::get('post') : array();

        if (Route::current()->parameter('promo_code')) {
            $post['promo_code'] = Route::current()->parameter('promo_code');
            $user_referrer_first_name = User::where('referral_code', '=', $post['promo_code'])->pluck('first_name');
            $this->metadata['code_referred_title'] = Config::get('app.referred.share_title') . ' que te regala ' . $user_referrer_first_name;
            $this->metadata['code_referred_description'] = Config::get('app.referred.share_description') . ' Para aceptar entra aquí y regístrate con el código ' . $post['promo_code'];
        } else $user_referrer_first_name = '';

        $phone_validated = Session::get('phoneValidation.validated');
        $facebook_validated = Session::has('facebookData');
        $post['first_name'] = Session::get('post.first_name') ?: Session::get('facebookData.first_name');
        $post['last_name'] = Session::get('post.last_name') ?: Session::get('facebookData.last_name');
        $post['email'] = Session::get('post.email') ?: Session::get('facebookData.email');
        $post['phone'] = Session::get('phoneValidation.validated')
            ? Session::get('phoneValidation.number')
            : Session::get('post.phone');

        return View::make('register')
            ->with('dates_cc', $dates_cc)
            ->with('post', $post)
            ->with('stores', $this->stores)
            ->with('metadata', $this->metadata)
            ->with('footer', $this->getFooter())
            ->with('products', $products)
            ->with('user_referrer_first_name', $user_referrer_first_name)
            ->with('facebook_validated', $facebook_validated)
            ->with('user_agent', $user_agent)
            ->with('phone_validated', $phone_validated);
    }

    /**
     * Vista que informa al usuario como referir desde la app
     */
    public function referredPage()
    {
        $referralCode = Route::current()->parameter('promo_code');
        $user = User::where('referral_code', $referralCode)->first();

        if (is_null($user)) {
            App::abort(404);
        }

        return View::make('referred')
            ->with('firstName', $user->first_name)
            ->with('referralCode', $referralCode);
    }

    /**
     * Realiza registro y login de usuario con Facebook
     */
    public function social()
    {
        $fb_id = Input::get('fb_id');
        $redirect_url = null;
        $response = [
            'status' => false,
            'message' => 'Ocurrió un error al procesar tus datos de Facebook. Por favor recarga la página e intenta de nuevo.'
        ];

        if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            $user->fb_id = $fb_id;
            $user->save();
            $response = array('status' => true, 'message' => 'Cuenta asociada');
        } else {
            if ($user = User::where('fb_id', $fb_id)->first()) {
                if ($user->status) {
                    Session::forget('referred_code');
                    Session::save();
                    $response = array(
                        'status' => true,
                        'message' => 'Sesión iniciada',
                        'user_phone' => $user->phone,
                        'user_email' => $user->email
                    );

                    self::trackEventOnBrowser(['user_signed_in' => [
                        'screen' => StoreController::getReferrer()
                    ]]);
                } else {
                    $response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros.';
                }
            } else {
                $email = Input::get('email');
                $user = empty($email) ? null : User::where('email', $email)->first();
                if ($user) {
                    try {
                        MerqueoFacebook::setDefaultAccessToken(Input::get('access_token'));
                        MerqueoFacebook::validateFacebookId($fb_id, $email);
                        if ($user->status) {
                            $user->fb_id = $fb_id;
                            $user->save();
                            Session::forget('referred_code');
                            Session::save();
                            $response = [
                                'status' => true,
                                'message' => 'Cuenta asociada',
                                'user_phone' => $user->phone,
                                'user_email' => $user->email
                            ];
                        } else {
                            $response['message'] = 'Tu cuenta esta deshabilitada, por favor comunícate con nosotros.';
                        }
                    } catch (\exceptions\MerqueoException $exception) {
                        $response = [
                            'status' => false,
                            'message' => $exception->getMessage(),
                        ];
                    }
                } else {
                    //registro y login
                    if (!\Session::get('phoneValidation.validated')) {
                        return Response::json([
                            'status' => false,
                            'message' => 'Debes validar tu número telefonico.',
                        ]);
                    }

                    $phone = Session::get('phoneValidation.number');
                    $valid_facebook_id = true;

                    //verifica que el usuario no exista
                    $validator = Validator::make(
                        compact('email', 'phone'),
                        [
                            'email' => 'email|unique:users,email,NULL,id,type,merqueo.com',
                            'phone' => 'numeric|unique:users,phone,NULL,id,type,merqueo.com'
                        ],
                        [
                            'email.unique' => 'El email ingresado ya se encuentra en uso.',
                            'email.email' => 'El formato del email ingresado no es valido.',
                            'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                            'phone.numeric' => 'El número celular debe ser númerico.'
                        ]
                    );

                    try {
                        MerqueoFacebook::setDefaultAccessToken(Input::get('access_token'));
                        MerqueoFacebook::validateFacebookId($fb_id, $email);
                    } catch (\exceptions\MerqueoException $exception) {
                        $valid_facebook_id = false;
                        $response = [
                            'status' => false,
                            'message' => $exception->getMessage(),
                        ];
                    }

                    if ($validator->fails()) {
                        $messages = $validator->messages();
                        if (!$response['message'] = $messages->first('email')) {
                            $response['message'] = $messages->first('phone');
                        }
                    } elseif ($valid_facebook_id) {
                        $first_name = Input::get('first_name');
                        $last_name = Input::get('last_name');
                        $facebook_data = [
                            'id' => $fb_id,
                            'email' => $email,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                        ];

                        Session::put('facebookData', $facebook_data);

                        return Response::json([
                            'status' => true,
                            'message' => 'Usuario validado.',
                            'result' => [
                                'is_new_user' => true,
                                'redirect' => action('frontUser.register'),
                                'phone' => Session::get('phoneValidation'),
                                'facebook' => $facebook_data,
                            ]
                        ]);
                    }
                }
            }

            if ($response['status']) {
                $has_orders = Order::where('user_id', $user->id)
                        ->where('status', '<>', 'Cancelled')
                        ->count() > 0;

                Session::forget('phoneValidation');
                Session::forget('facebookData');
                Session::put('has_orders', $has_orders);
                Auth::login($user);

                //cargar bloque header de sesion
                $response['html']['header'] = '<a href="#" class="dropdown-toggle user-navbar" data-toggle="dropdown" role="button" aria-expanded="false">
                <strong>' . $user->first_name . '</strong><b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menuhome" role="menu">
                  <li><a href="' . web_url() . '/mi-cuenta"><i class="icon-user"></i>Mi Cuenta</a></li>
                  <li class="divider"></li>
                  <li><a href="' . web_url() . '/logout" class="logout"><i class="icon-off"></i>Cerrar sesión</a></li>
                </ul>';
                //cargar direcciones
                $addresses = UserAddress::select('user_address.*', 'cities.city')
                    ->join('cities', 'city_id', '=', 'cities.id')
                    ->where('user_id', $user->id)
                    ->where('coverage_store_id', Session::get('store_id'))
                    ->where('city_id', Session::get('city_id'))
                    ->get();

                if ($addresses->count()) {
                    $addresses = $addresses->toArray();
                    $addresses_html = '<h4>Selecciona una dirección: <div class="address-toggle pull-right btn btn-info">Nueva dirección</div></h4><hr>
                    <input type="hidden" name="address_id" value="">';
                    foreach ($addresses as $addr) {
                        $further = !empty($addr['address_further']) ? '<br>' . $addr['address_further'] . '<br>' : '<br>';
                        $addresses_html .= '<div class="bg-info">
                            <a class="address btn btn-default btn-sm pull-right" data-id="' . $addr['id'] . '" href="javascript:;">+ Enviar a esta dirección</a>
                            <b>' . $addr['label'] . '</b><br>' . $addr['address'] . $further . '</div>
                            <font color="grey">' . $addr['city'] . '</font><br><hr>';
                    }
                    $addresses_html .= '<br/>';
                    $response['html']['address_button'] = '<div class="address-toggle pull-right btn btn-info">Usar una de mis direcciones guardadas</div><br><br>';
                    $response['html']['addresses'] = $addresses_html;
                }
                //cargar tarjetas de credito
                $credit_cards = UserCreditCard::where('user_id', $user->id)->get()->toArray();
                if (count($credit_cards)) {
                    $credit_cards_html = '<h4><div class="credit-card-toggle pull-right btn btn-info">Nueva tarjeta</div></h4><br><br><hr>
                    <input type="hidden" name="credit_card_id" value="">';
                    foreach ($credit_cards as $cc) {
                        $further = !empty($addr['address_further']) ? '<br>' . $addr['address_further'] . '<br>' : '<br>';
                        $credit_cards_html .= '<div class="bg-info"><a class="credit-card btn btn-default btn-sm pull-right" data-id="' . $cc['id'] . '" href="javascript:;">+ Utilizar esta tarjeta</a>
                        <img src="' . web_url() . '/assets/img/credit_card.png">&nbsp;&nbsp;&nbsp;Tarjeta de crédito<br><b>**** ' . $cc['last_four'] . '</b><br><i>' . $cc['type'] . '</i><br></div><hr>';
                    }
                    $response['html']['credit_card_button'] = '<div class="credit-card-toggle pull-right btn btn-info">Usar una de mis tarjetas guardadas</div><br><br><br>';
                    $response['html']['credit_cards'] = $credit_cards_html;
                }
                //cargar bloque de saldo de credito y domicilio gratis
                $user_discounts = User::getDiscounts();
                if ($user_discounts['amount'] && $user_discounts['minimum_discount_amount']) {
                    $credit_amount_format = number_format($user_discounts['amount'], 0, ',', '.');
                    $response['html']['credits'] = '<div class="header-message"><p>Tienes <span>$' . $credit_amount_format . '</span> de crédito</p></div>';
                }
                if ($user_discounts['free_delivery_days'] > 1) {
                    $message = 'Tienes <b>' . $user_discounts['free_delivery_days'] . '</b> días de domicilio gratis';
                } elseif ($user_discounts['free_delivery_days'] == 1) {
                    $message = 'Tienes domicilio gratis hasta el día de hoy';
                }
                if ($user_discounts['free_delivery_days'] > 1) {
                    $message = 'Tienes <b>' . $user_discounts['free_delivery_days'] . '</b> días de domicilio gratis';
                } elseif ($user_discounts['free_delivery_days'] == 1) {
                    $message = 'Tienes domicilio gratis hasta el día de hoy';
                }
                if (isset($message)) {
                    $response['html']['free_delivery_days'] = '<div class="free-delivery-days" style="display: inline-block">
                        <div class="store-message header-message alert">' . $message . '</div>
                    </div>';
                }

                // TODO Refactorizar el código.
                $warehouse = $this->getCurrentWarehouse();
                $cart = Auth::user()
                    ->carts()
                    ->where('warehouse_id', $warehouse->id)
                    ->orderBy('updated_at', 'desc')
                    ->first();

                if (!empty($cart->warehouse->city_id)) {
                    Session::put('city_id', $cart->warehouse->city_id);
                    Cookie::queue('city_id', $cart->warehouse->city_id, 2628000);
                }

                if ($cart) {
                    $cart_id = $cart->id;
                    Session::put('zone_id', $warehouse->zones()->first()->id);
                } else {
                    //si no existe entonces creo un carrito
                    if (!$cart) {
                        $cart = new Cart;
                        $cart->warehouse()->associate($warehouse);
                        $cart->user_id = $user->id;
                        $cart->save();
                        $cart_id = $cart->id;
                    }
                }

                //si el usuario tiene un carrito en sesión, lo pongo en el carrito actual borrando todo lo que tengo en db
                if (Session::has('tmp_cart')) {
                    $cart = json_decode(Session::get('tmp_cart'));
                    $products = (array)$cart->products;
                    if (count($products)) {
                        //elimino los productos del carrito actual
                        CartProduct::where('cart_id', $cart_id)->delete();
                        foreach ($cart->products as $i => $product) {
                            if ($product->cart_quantity <= 0) {
                                continue;
                            }
                            $cart_product = new CartProduct;
                            $cart_product->cart_id = $cart_id;
                            $cart_product->store_product_id = $product->store_product_id;
                            $cart_product->quantity = $product->cart_quantity;
                            $cart_product->quantity_full_price = $product->cart_quantity_full_price;
                            $cart_product->added_by = $user->id;
                            $cart_product->save();
                        }
                        Session::forget('tmp_cart'); //elimina la sesión
                    }
                }

                Session::put('cart_id', $cart_id);
                $response['cart_id'] = $cart_id;
            }
        }

        return Response::json($response, 200);
    }

    /**
     * Agregar codigo de referido
     */
    public function referred()
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        if (Input::has('promo_code')) {
            $order = Order::where('user_id', $this->user_id)->where('status', 'Delivered')->first();
            if (!$order) {
                $user = Auth::user();
                $referred = User::validateReferred($user);
                if ($referred['amount']) {
                    return Redirect::to('/mi-cuenta')->with('type', 'success')->with('message',
                        'Tu código de referido fue procesado con éxito, tienes <b>' . currency_format($referred['amount']) . '</b> en créditos en tu cuenta.');
                }
                if ($referred['limit']) {
                    return Redirect::to('/mi-cuenta')->with('type', 'error')->with('message',
                        'El código referido que ingresaste no es valido por que ha alcanzado el limite permitido de referidos por cuenta.');
                }
                if ($referred['error']) {
                    return Redirect::to('/mi-cuenta')->with('type', 'error')->with('message',
                        'El código de referido que ingresaste no es valido, por favor verificalo y vuelve a intentarlo.');
                }
            } else {
                return Redirect::to('/mi-cuenta')->with('type', 'error')->with('message',
                    'No puedes agregar un código referido por que ya hiciste tu primer pedido.');
            }
        }
    }

    /**
     * Muestra detalle de un pedido
     */
    public function get_order($reference)
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        $order = Order::select('orders.*', 'stores.name AS store_name')
            ->join('stores', 'store_id', '=', 'stores.id')
            ->where('reference', $reference)
            ->where('user_id', $this->user_id)
            ->orderBy('id', 'desc')
            ->first();

        if ($order) {
            if ($order->user_id == $this->user_id) {
                $status = array(
                    'Validation' => 'Iniciado',
                    'Initiated' => 'Iniciado',
                    'Enrutado' => 'En Proceso',
                    'In Progress' => 'En Proceso',
                    'Alistado' => 'En Proceso',
                    'Dispatched' => 'En Camino',
                    'Delivered' => 'Entregado',
                    'Cancelled' => 'Cancelado'
                );
                $order->status = $status[$order->status];
                $user = User::find($order->user_id);
                $products = OrderProduct::where('order_id', $order->id)
                    ->where('parent_id', 0)
                    //->where('type', '<>', 'Muestra')
                    ->where('is_gift', 0)
                    ->get();

                $assigned_gifts = [];
                if ($order->date > '2018-01-01 00:00:00') {
                    foreach ($products as $order_product) {
                        if (empty($assigned_gifts[$order_product->store_product_id])) {
                            $assigned_gifts[$order_product->store_product_id] = 1;

                            $order_product_sampling = OrderProduct::select('order_products.id AS order_product_id', 'store_product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name', 'order_products.fulfilment_status',
                                'product_image_url', 'product_quantity AS quantity', 'product_unit AS unit', 'fulfilment_status', 'reference', 'sampling_id', 'campaign_gift_id', 'is_gift', 'order_products.type')
                                ->where('parent_id', $order_product->store_product_id)
                                ->where('order_id', $order->id)
                                ->whereIn('type', ['Muestra', 'Product'])
                                ->get();

                        } else {
                            $order_product_sampling = [];
                        }

                        if (count($order_product_sampling) > 0) {
                            $order_product_sampling = $order_product_sampling->first();

                            if (!$order_product_sampling->is_gift && $order_product_sampling->type == 'Muestra') {
                                $sampling = Sampling::find($order_product_sampling->sampling_id);
                                $order_product->sampling = $order_product_sampling;
                            }

                            if ($order_product_sampling->is_gift) {
                                $sampling = CampaignGift::find($order_product_sampling->campaign_gift_id);
                                $order_product->gift = $order_product_sampling;
                            }

                            if (isset($sampling) && $sampling)
                                $order_product_sampling->message = 'Merqueo y ' . $sampling->brand . ' te regalan:';

                        }

                        if ($order_product->fulfilment_status == 'Not Available') {
                            $order_product_gift = OrderProduct::select('order_products.id AS order_product_id', 'store_product_id AS id', 'price', 'quantity AS quantity_cart', 'product_name AS name', 'order_products.fulfilment_status',
                                'product_image_url', 'product_quantity AS quantity', 'product_unit AS unit', 'fulfilment_status', 'reference')
                                ->where('parent_id', $order_product->store_product_id)
                                ->where('order_id', $order->id)
                                ->where('is_gift', 1)
                                ->first();
                            if ($order_product_gift) {
                                $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                                $order_product->gift = $order_product_gift;
                            }
                        }
                    }
                }

                return View::make('order_details')->with('user', $user)->with('order', $order)->with('products',
                    $products);
            }
        }
    }

    /**
     * Agrega productos del pedido al carrito
     */
    public function add_order_cart($reference)
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        if (Request::ajax()) {
            $objhtml = array();
        }

        $warehouse = $this->getCurrentWarehouse();
        $products = OrderProduct::select('store_products.id', 'store_products.product_id', 'products.name', 'order_products.quantity',
            'store_products.price', 'store_products.special_price', 'store_products.quantity_special_price',
            'store_products.store_id', 'store_products.merqueo_discount')
            ->join('orders', 'orders.id', '=', 'order_products.order_id')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('stores', 'stores.id', '=', 'store_products.store_id')
            ->where('orders.reference', $reference)
            ->where('orders.user_id', $this->user_id)
            ->where('order_products.type', 'Product')
            ->where('stores.status', 1)
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('warehouse_id', $warehouse->id)
            ->get();

        if (!$products) {
            if (!Request::ajax()) {
                return Redirect::to('user/order/' . $reference)->with('type', 'error')->with('message',
                    'No se encontraron productos para agregar al carrito.');
            } else {
                $objhtml['status'] = 'error';
                $objhtml['msg'] = 'No se encontraron productos para agregar al carrito.';
            }
        }

        $cart = Cart::where('user_id', $this->user_id)->orderBy('updated_at', 'desc')->first();
        if (!$cart) {
            $cart = new Cart;
            $cart->user_id = $this->user_id;
            $cart->save();
            Session::put('cart_id', $cart->id);
        }

        // elimino los productos del carrito actual
        CartProduct::where('cart_id', $cart->id)->delete();

        foreach ($products as $product) {
            if ($product->quantity > 0 && $product->special_price && $product->quantity_special_price) {
                //validar cantidad de pedidos del producto por usuario
                if ($this->user_id) {
                    $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                        ->where('store_product_id', $product->id)
                        ->where('price', $product->special_price)
                        ->where('user_id', $this->user_id)
                        ->where('user_id', '<>', 425200)//usuario trade
                        ->where('status', '<>', 'Cancelled')
                        ->where(DB::raw('DATEDIFF(NOW(), date)'), '<=', Config::get('app.minimum_promo_days'))
                        ->groupBy('order_products.store_product_id')->count();
                    if ($count_products) {
                        $error = 'Ya realizaste un pedido con el limite permitido para el producto en promoción: ' . $product->name;
                        if (!Request::ajax()) {
                            return Redirect::to('user/order/' . $reference)->with('type', 'error')->with('message',
                                $error);
                        } else {
                            $objhtml['status'] = 'error';
                            $objhtml['msg'] = $error;
                        }
                    }
                }

                if ($product->quantity > $product->quantity_special_price) {
                    $error = 'El producto ' . $product->name . ' por estar en promoción puedes agregar máximo ' . $product->quantity_special_price . ' unidades al carrito.';
                    if (!Request::ajax()) {
                        return Redirect::to('user/order/' . $reference)->with('type', 'error')->with('message', $error);
                    } else {
                        $objhtml['status'] = 'error';
                        $objhtml['msg'] = $error;
                    }
                }
            }

            $cart_product = new CartProduct;
            $cart_product->cart_id = $cart->id;
            $cart_product->store_product_id = $product->id;
            $cart_product->quantity = $product->quantity;
            $cart_product->added_by = $this->user_id;
            $cart_product->save();
        }

        if (!Request::ajax()) {
            return Redirect::to('user/order/' . $reference)->with('type', 'success')->with('message',
                'El pedido ha sido agregado a tu carrito actual.');
        } else {
            $objhtml['status'] = 'success';
            $objhtml['msg'] = 'Pedido Actualizado';
        }

        if (Request::ajax()) {
            return $objhtml;
        }
    }

    /**
     * Muestra formulario de restablecer clave
     */
    public function forgot_password()
    {
        return View::make('password_remind')->with('metadata', $this->metadata)->with('footer', $this->getFooter());
    }

    /**
     * Panel de control de la cuenta del usuario
     */
    public function get_user()
    {
        if (!$this->user_id) {
            return Redirect::to('/login');
        }

        if (!$user = User::find($this->user_id)) {
            return Redirect::to('/login');
        }

        $data['user'] = $user->toArray();
        $data['address'] = UserAddress::select('user_address.*', 'cities.city')->join('cities', 'city_id', '=',
            'cities.id')
            ->where('user_id', $this->user_id)->get()->toArray();
        //fecha vencimiento de tarjeta de credito
        for ($i = 0; $i <= 9; $i++) {
            $dates_cc['years'][] = date('Y') + $i;
        }
        for ($i = 1; $i <= 12; $i++) {
            if ($i < 10) {
                $i = '0' . $i;
            }
            $dates_cc['months'][] = $i;
        }
        $data['user_discounts'] = $this->user_discounts;
        $data['credit_available'] = currency_format($this->user_discounts['amount']);
        $data['credit_movements'] = UserCredit::getCreditMovements($user->id, 'user');
        $data['credit_cards'] = UserCreditCard::where('user_id', $this->user_id)->get();
        $has_first_order = false;
        $orders = Order::select('orders.*')->where('orders.user_id', $this->user_id)->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->where('source', '<>', 'Reclamo')->where('orders.is_hidden', 0)->orderBy('orders.id', 'desc')->get();
        foreach ($orders as $order) {
            if ($order->status == 'Delivered') {
                $has_first_order = true;
            }
            $store = Store::find($order->store_id);
            $order->store_name = $store->name;
            $order->total_amount = number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.');
        }
        $data['orders'] = $orders;
        $cities = City::where('status', 1)->where('is_main', 1)->get();

        $status = array(
            'Validation' => 'Iniciado',
            'Initiated' => 'Iniciado',
            'Enrutado' => 'En Proceso',
            'In Progress' => 'En Proceso',
            'Alistado' => 'En Proceso',
            'Dispatched' => 'En Camino',
            'Delivered' => 'Entregado',
            'Cancelled' => 'Cancelado'
        );

        $soat = ExternalService::where('user_id', $this->user_id)->orderBy('id', 'DESC')->get();
        $data['soat'] = $soat;

        return View::make('account')
            ->with('data', $data)
            ->with('status', $status)
            ->with('has_first_order', $has_first_order)
            ->with('dates_cc', $dates_cc)
            ->with('cities', $cities)
            ->with('stores', $this->stores)
            ->with('config', $this->config)
            ->with('metadata', $this->metadata)
            ->with('footer', $this->getFooter());
    }

    /**
     * Obtiene la vista del detalle de un external payments seleccionado.
     */
    public function get_external_service($reference)
    {
        $data = ExternalService::select('external_services.*', 'user_credit_cards.last_four', 'user_credit_cards.type as cc_type')
            ->join('user_credit_cards', 'credit_card_id', '=', 'user_credit_cards.id')
            ->where('external_services.reference', $reference)->first();

        $data['params'] = json_decode($data['params'], true);

        $data['date_start'] = Carbon::createFromFormat("Y-m-d H:i:s", $data['created_at'])->addDay();
        $data['date_end'] = Carbon::createFromFormat("Y-m-d H:i:s", $data['created_at'])->addYear();

        return View::make('external_service_details')
            ->with('data', $data);

    }

    /**
     * Guarda una nueva direccion
     */
    public function save_address()
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        $address = new UserAddress;
        $address->user_id = $this->user_id;
        $address->label = Input::get('label');
        $address->address = Input::get('address');
        $address->address_further = Input::get('address_further');
        $address->neighborhood = Input::get('address_neighborhood');
        $address->is_south_location = Input::get('address_is_south_location');
        $dir = Input::get('dir');
        $address->address_1 = $dir[0];
        $address->address_2 = $dir[2];
        $address->address_3 = $dir[6];
        $address->address_4 = $dir[8];
        $city = City::find(Input::get('city_id'));
        $request = Request::create('/api/location', 'GET',
            array('address' => $address->address, 'city' => $city->slug));
        Request::replace($request->input());
        $result = json_decode(Route::dispatch($request)->getContent());
        if ($result->status) {
            $address->latitude = $result->result->latitude;
            $address->longitude = $result->result->longitude;
            $address->city_id = $result->result->city_id;
            $address->save();
            $flag = 1;
        } else {
            $flag = 0;
        }

        if (Request::format() == 'html') {
            // Redirect to update profile page
            if ($flag == 1) {
                $type = 'success';
                $message = 'Dirección añadida con éxito.';
            } else {
                $type = 'error';
                $message = 'Ocurrió un error al ubicar tu dirección.';
            }
            if (Input::has('is_checkout')) {
                return Redirect::to('/checkout')->with('type', $type)->with('message', $message);
            } else {
                return Redirect::to('/mi-cuenta')->with('type', $type)->with('message', $message);
            }
        } else {
            if ($flag == 1) {
                $response['success'] = 'true';
                $response['user'] = $address->toArray();
            } else {
                $response['success'] = 'false';
                $response['error'] = 'Ocurrió un error al ubicar tu dirección.';
                $response['error_code'] = 413;
            }

            return Response::json($response, 200);
        }
    }

    /**
     * Actualiza una direccion
     */
    public function update_address()
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        $id = Input::get('address_id');
        $address = UserAddress::find($id);
        if ($address) {
            $address->label = Input::get('label');
            $address->address = Input::get('address');
            $address->address_further = Input::get('address_further');
            $address->neighborhood = Input::get('address_neighborhood');
            $address->is_south_location = Input::get('address_is_south_location');
            $dir = Input::get('dir');
            $address->address_1 = $dir[0];
            $address->address_2 = $dir[2];
            $address->address_3 = $dir[6];
            $address->address_4 = $dir[8];
            $city = City::find(Input::get('city_id'));
            $request = Request::create('/api/location', 'GET',
                array('address' => $address->address, 'city' => $city->slug));
            Request::replace($request->input());
            $result = json_decode(Route::dispatch($request)->getContent());
            if ($result->status) {
                $address->latitude = $result->result->latitude;
                $address->longitude = $result->result->longitude;
                $address->city_id = $result->result->city_id;
                $address->save();
                $flag = 1;
            } else {
                $flag = 0;
            }
        } else {
            $flag = 0;
        }

        if ($flag == 1) {
            $type = 'success';
            $message = 'Dirección editada con éxito.';
        } else {
            $type = 'error';
            $message = 'Ocurrió un error al ubicar tu dirección.';
        }
        return Redirect::to('/mi-cuenta')->with('type', $type)->with('message', $message);
    }

    /**
     * Elimina una direccion
     */
    public function delete_address($address_id)
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        UserAddress::where('user_id', $this->user_id)->where('id', $address_id)->delete();
        return Redirect::to('/mi-cuenta')->with('type', 'success')->with('message', 'Dirección eliminada con éxito.');
    }

    /**
     * Actualiza datos de la cuenta
     */
    public function update()
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        $user = User::find($this->user_id);

        if (Input::has('first_name')) {
            $user->first_name = Input::get('first_name');
        }
        if (Input::has('last_name')) {
            $user->last_name = Input::get('last_name');
        }
        if (Input::has('phone')) {
            $user->phone = Input::get('phone');
        }
        if (Input::has('email')) {
            $user->email = Input::get('email');
        }
        if (Input::has('password')) {
            $user->password = Hash::make(Input::get('password'));
        }

        //verifica que el usuario no exista
        $validator = Validator::make(
            array('email' => $user->email, 'phone' => $user->phone),
            array(
                'email' => 'email|unique:users,email,' . $user->id . ',id,type,merqueo.com',
                'phone' => 'numeric|unique:users,phone,' . $user->id . ',id,type,merqueo.com'
            ),
            array(
                'email.unique' => 'El email ingresado ya se encuentra en uso.',
                'email.email' => 'El formato del email ingresado no es valido.',
                'phone.unique' => 'El número celular ingresado ya se encuentra en uso.',
                'phone.numeric' => 'El número celular debe ser númerico.'
            )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            if (!$error = $messages->first('email')) {
                $error = $messages->first('phone');
            }
            return Redirect::to('/mi-cuenta')->with('type', 'failed')->with('message', $error);
        }

        $user->save();

        return Redirect::to('/mi-cuenta')->with('type', 'success')->with('message',
            'Información de cuenta actualizada.');
    }

    /**
     * Actualiza y agrega productos al carrito
     */
    public function cart()
    {
        if (Auth::check() && Input::get('quantity') && Input::get('referer') === 'Product') {
            $added = new ProductAdded(Auth::user());
            $added->track();
        }

        $info = User::getOrdersInfo($this->user_id);
        $store_product_id = Input::get('store_product_id');
        $cart_id = Session::get('cart_id') ?: Input::get('cart_id');
        $delivery_day = Input::get('delivery_day');
        $delivery_window_id = Input::get('delivery_window_id');
        $warehouse = $this->getCurrentWarehouse();

        if ($store_product_id) {
            // Validacion del producto no esta disponible
            $validate_product = StoreProduct::select(
                'store_products.*',
                'products.name'
            )
                ->with('department', 'store', 'shelf')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->where('warehouse_id', $warehouse->id)
                ->where('store_products.id', $store_product_id)
                ->where('store_product_warehouses.is_visible', 1)
                ->where('store_product_warehouses.status', 1)
                ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
                ->first();
        }

        if ($this->user_id) {
            if (!$cart_id) {
                $cart = Cart::where('user_id', $this->user_id)
                    ->where('warehouse_id', $warehouse->id)
                    ->orderBy('updated_at', 'desc')
                    ->first();
            } else {
                $cart = Cart::where('user_id', $this->user_id)
                    ->where('warehouse_id', $warehouse->id)
                    ->where('id', $cart_id)
                    ->orderBy('updated_at', 'desc')
                    ->first();
            }

            if (!$cart) {
                $cart = new Cart;
                $cart->user_id = $this->user_id;
                $cart->warehouse()->associate($warehouse);
                $cart->save();
                $cart_id = $cart->id;
                Session::put('cart_id', $cart_id);
            } else {
                Session::put('cart_id', $cart->id);
            }
        } else {
            if (Input::get('anonymousId')) {
                Session::put('anonymousId', Input::get('anonymousId'));
            }

            if (Session::has('tmp_cart')) {
                $cart = json_decode(Session::get('tmp_cart'));
                $cart->warehouse_id = $warehouse->id;
            } else {
                //si no hay un carrito, crear un esqueleto del nuevo carrito para la sesión
                $cart = new StdClass();
                $cart->products = new \StdClass();
                $cart->warehouse_id = $warehouse->id;
            }
            Session::put('tmp_cart', json_encode($cart));
            //eliminar productos con cantidad negativa
            foreach ($cart->products as $i => $product) {
                if (!property_exists($product, 'cart_quantity_full_price')) {
                    $product->cart_quantity_full_price = 0;
                }
                if (property_exists($product, 'cart_quantity') && $product->cart_quantity <= 0) {
                    unset($cart->products->$i);
                }
            }
        }

        if ($cart) {
            //agregar o editar producto
            if (Input::has('store_product_id')) {
                if ($this->user_id) {
                    $cart_product = CartProduct::where('store_product_id', $store_product_id)
                        ->where('cart_id', $cart_id)
                        ->first();
                } else {
                    $cart_product = false;
                    foreach ($cart->products as $product) {
                        if (property_exists($product, 'store_product_id') && $product->store_product_id == $store_product_id) {
                            $cart_product = $product;
                        }
                    }
                }

                $store_product_tmp = $store_product = StoreProduct::select(
                    'store_products.*',
                    'products.name',
                    StoreProduct::getRawDeliveryDiscountByDate(),
                    StoreProduct::getRawSpecialPriceQuery(
                        $info->qty,
                        $info->last_order_date
                    )
                )
                    ->with('department', 'store', 'shelf')
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->where('store_products.id', $store_product_id)
                    ->first();

                if ($store_product_tmp && !empty($this->user_id) && $store_product_tmp->provider_id == Provider::MERCARDERIA) {
                    $from_label = new ProductAddedLabel(Auth::user());
                    $from_label->track();
                }

                if (!Input::get('add_as_full_price') && Input::get('quantity') > 0 && $store_product->special_price > -1 && $store_product->quantity_special_price) {
                    //validar cantidad de pedidos del producto por usuario
                    if ($this->user_id) {
                        $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                            ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                            ->where('store_product_id', $store_product->id)
                            ->where('order_products.price', $store_product->special_price)
                            ->where('user_id', Auth::user()->id)
                            ->where('status', '<>', 'Cancelled')
                            ->whereNotNull('store_products.special_price')
                            ->whereRaw(DB::raw(StoreProduct::getRawConditionByDateRange()))
                            ->where(DB::raw("DATEDIFF('" . date('Y-m-d') . "', date)"), '<=', Config::get('app.minimum_promo_days'))
                            ->groupBy('order_products.store_product_id')
                            ->count();

                        if ($count_products) {
                            $qty = $cart_product ? $cart_product->quantity + $cart_product->quantity_full_price : $store_product->quantity_special_price;
                            $error = 'Ya realizaste un pedido con el producto ' . $store_product->name .
                                ' en promoción. ¿Deseas agregarlo con precio normal a $' .
                                number_format($store_product->price, 0, ',', '.') . '?' . $store_product->id;

                            return Response::json([
                                'status' => false,
                                'error' => $error,
                                'qty' => $qty,
                                'add_as_full_price' => 1
                            ], 200);
                        }
                    }

                    if (Input::get('quantity') > $store_product->quantity_special_price) {
                        if ($store_product->first_order_special_price && !Session::get('has_orders') || !$store_product->first_order_special_price) {
                            $qty = $this->user_id ? $cart_product->quantity + $cart_product->quantity_full_price : $cart_product->cart_quantity + $cart_product->cart_quantity_full_price;
                            $qty--;
                            $error = 'Por ser un producto en promoción puedes agregar máximo ' .
                                $store_product->quantity_special_price .
                                ' unidad(es) con descuento. ¿Deseas agregarlo con precio normal a $' .
                                number_format($store_product->price, 0, ',', '.') . '?';

                            return Response::json([
                                'status' => false,
                                'error' => $error,
                                'qty' => $qty,
                                'add_as_full_price' => 1
                            ], 200);
                        }
                    }
                }

                if ($cart_product) {
                    //validar si se agrega con precio full o especial
                    if ($this->user_id) {
                        if (!Input::get('add_as_full_price') && (Input::get('quantity') < -1 || !$cart_product->quantity_full_price)) {
                            $quantity = 'quantity';
                            $quantity_full_price = 'quantity_full_price';
                        } else {
                            $quantity = 'quantity_full_price';
                            $quantity_full_price = 'quantity';
                        }
                    } else {
                        if (!Input::get('add_as_full_price') && (Input::get('quantity') < -1 || !$cart_product->cart_quantity_full_price)) {
                            $quantity = 'cart_quantity';
                            $quantity_full_price = 'cart_quantity_full_price';
                        } else {
                            $quantity = 'cart_quantity_full_price';
                            $quantity_full_price = 'cart_quantity';
                        }
                    }

                    if (Input::get('quantity') != 1 && Input::get('quantity') != -1) {
                        $cart_product->{$quantity} = $cart_product->{$quantity_full_price} = 0;
                    } else {
                        $cart_product->{$quantity} += Input::get('quantity');
                    }

                    //validar cantidad del producto
                    if (!Input::get('add_as_full_price') && $store_product_id > 0 && Input::get('quantity') > 0 && $store_product->special_price > -1 && $store_product->quantity_special_price) {
                        if ($cart_product->{$quantity} > $store_product->quantity_special_price) {
                            if ($store_product->first_order_special_price && !Session::get('has_orders') || !$store_product->first_order_special_price) {
                                $qty = $this->user_id ? $cart_product->quantity + $cart_product->quantity_full_price : $cart_product->cart_quantity + $cart_product->cart_quantity_full_price;
                                $qty--;
                                $error = 'Por ser un producto en promoción puedes agregar máximo ' . $store_product->quantity_special_price . ' unidad(es) con descuento. ¿Deseas agregarlo con precio normal a $' . number_format($store_product->price, 0, ',', '.') . '?';
                                return Response::json(array(
                                    'status' => false,
                                    'error' => $error,
                                    'qty' => $qty,
                                    'add_as_full_price' => 1
                                ), 200);
                            }
                        }
                    }

                    if ($cart_product->{$quantity} <= 0 && $cart_product->{$quantity_full_price} <= 0) {
                        if ($this->user_id) {
                            $cart_product->delete();
                        } else {
                            unset($cart->products->{$store_product_id});
                        }
                    } else {
                        if ($this->user_id) {
                            $cart_product->save();
                        } else {
                            $cart->products->{$store_product_id} = $cart_product;
                        }
                    }

                    //guardar los cambios al carrito temporal
                    if (!$this->user_id) {
                        Session::put('tmp_cart', json_encode($cart));
                    }
                } else {
                    if ($this->user_id) {
                        $cart_product = new CartProduct;
                        $cart_product->cart_id = $cart_id;
                        $cart_product->store_product_id = $store_product_id;
                        $cart_product->quantity = !Input::get('add_as_full_price') ? Input::get('quantity') : 0;
                        $cart_product->quantity_full_price = Input::get('add_as_full_price') ? Input::get('quantity') : 0;
                        $cart_product->added_by = $this->user_id;
                        if (Input::get('quantity') > 0) {
                            $cart_product->save();
                        }
                    } else {
                        //crear el objeto del carrito en la sesión y guardar
                        $store_product = (object)[
                            'cart_id' => $cart_id,
                            'store_product_id' => $store_product_id,
                            'cart_quantity' => Input::get('quantity'),
                            'cart_quantity_full_price' => 0,
                            'added_by' => $this->user_id
                        ];
                        $cart->products->$store_product_id = $store_product;

                        Session::put('tmp_cart', json_encode($cart));
                    }
                }

                // Validamos si el producto no esta disponible y enviamos un mensaje de error
                if (!$validate_product) {
                    if ($this->user_id) {
                        $cart_product->delete();
                    } else {
                        unset($cart->products->{$store_product_id});
                    }

                    $response['error'] = "Producto no disponible";
                }

                $properties = Auth::check()
                    ? Auth::user()->getAnalyticsBasicProperties()
                    : [];

                if (!empty($store_product_tmp)) {
                    $properties += [
                        'department' => $store_product_tmp->department->name,
                        'departmentId' => $store_product_tmp->department->id,
                        'shelf' => $store_product_tmp->shelf->name,
                        'shelfId' => $store_product_tmp->shelf->id,
                        'storeProductId' => $store_product_tmp->id,
                        'name' => $store_product_tmp->name,
                        'price' => $store_product_tmp->price,
                        'specialPrice' => floatval($store_product_tmp->special_price) ?: 0,
                        'bestPriceGuaranteed' => $store_product_tmp->is_best_price ? 1 : 0,
                    ];
                }

                try {
                    // Track Product Added/Removed event
                    $quantity = intval(Input::get('quantity'));
                    $event = $quantity > 0 ? 'product_added' : 'product_removed';

                    if (isset($store_product)) {
                        $properties['quantity'] = $quantity;
                        $response['event'] = [
                            'userId' => $this->user_id,
                            'name' => $event,
                            'props' => $properties
                        ];
                    } else {
                        $response['event'] = [
                            'userId' => $this->user_id,
                            'name' => $event,
                            'props' => $properties,
                        ];
                    }
                } catch (Exception $e) {
                    Log::error("Track Product Added event error");
                    Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }
            }

            if ($this->user_id) {
                // Obtiene el carrito por la DB
                $cart_products = StoreProduct::select(
                    'products.*',
                    'store_products.*',
                    'cart_products.quantity AS cart_quantity',
                    'cart_products.quantity_full_price AS cart_quantity_full_price',
                    'cart_products.product_name',
                    'cart_products.comment AS cart_comment',
                    'cart_products.store_id AS cart_store_id',
                    'cart_products.store_product_id',
                    'users.first_name AS added_by',
                    StoreProduct::getRawDeliveryDiscountByDate(),
                    StoreProduct::getRawSpecialPriceQuery(
                        $info->qty,
                        $info->last_order_date
                    )
                )
                    ->join('cart_products', 'cart_products.store_product_id', '=', 'store_products.id')
                    ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                    ->leftJoin('users', 'cart_products.added_by', '=', 'users.id')
                    ->pum()
                    ->where('cart_products.cart_id', $cart_id)
                    ->orderBy('store_id')
                    ->get();
            } else {
                // Se simula obtener el carrito por DB para el usuario guest que sólo maneja sesión
                $store_products_ids = [];
                $cart_products = [];
                foreach ($cart->products as $store_product) {
                    if (property_exists($store_product, 'cart_quantity') && $store_product->cart_quantity <= 0) {
                        continue;
                    }
                    if (property_exists($store_product, 'store_product_id')) {
                        $store_products_ids[] = $store_product->store_product_id;
                    }
                }
                $store_products = StoreProduct::select(
                    'products.*',
                    'store_products.*',
                    StoreProduct::getRawDeliveryDiscountByDate(),
                    StoreProduct::getRawSpecialPriceQuery(
                        $info->qty,
                        $info->last_order_date
                    )
                )
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->pum()
                    ->whereIn('store_products.id', $store_products_ids)
                    ->get();

                // Armo la estructura de los productos:
                foreach ($store_products as $store_product) {
                    $cart_products[$store_product->id] = (object)array_merge(
                        $store_product->toArray(),
                        (array)$cart->products->{$store_product->id}
                    );
                }
            }

            $response['cart']['delivery_amount'] = 0;
            $response['cart']['total_amount'] = 0;
            $response['cart']['discount_amount'] = 0;
            $response['cart']['special_price_amount'] = 0;
            $response['cart']['total_quantity'] = 0;
            $response['cart']['is_minimum_reached'] = 1;
            $response['cart']['stores'] = array();
            $store_product_ids = $shelf_ids = [];

            if ($cart_products) {
                foreach ($cart_products as $cart_product) {
                    $store_product_ids[] = $cart_product->store_product_id;
                    $shelf_ids[] = $cart_product->shelf_id;
                    $store_id = $cart_product->store_id;

                    if (!isset($response['cart']['stores']) || !isset($response['cart']['stores'][$store_id])) {
                        $store = Store::select('id', 'name', 'minimum_order_amount', 'city_id')->where('id', $store_id)->first()->toArray();
                        $store['count'] = 1;
                        $store['sub_total'] = 0;
                        $response['cart']['stores'][$store_id] = $store;
                        $response['cart']['stores'][$store_id]['is_minimum_reached'] = 1;
                        $response['cart']['stores'][$store_id]['products'] = [];
                        $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                        $response['cart']['stores'][$store_id]['delivery_discount_amount'] = 0;
                        $response['cart']['stores'][$store_id]['discount_amount'] = 0;

                        if ($delivery_day) {
                            //valida que todas las tiendas tengan cobertura en la dirección y obtener deliveryWindow
                            if (!$delivery_window = DeliveryWindow::find($delivery_window_id)) {
                                return Response::json([
                                    'status' => false,
                                    'error' => 'No pudimos obtener la hora de entrega.',
                                    'qty' => 0,
                                    'add_as_full_price' => 0
                                ], 200);
                            }
                            $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_window->delivery_amount;
                        }

                    } else {
                        $response['cart']['stores'][$store_id]['count'] += 1;
                    }

                    $store_product = json_decode(json_encode($cart_product), true);
                    $store_product['special_price'] = floatval($store_product['special_price']);
                    if (!isset($store_product['cart_quantity'])) {
                        $store_product['cart_quantity'] = 1;
                    }
                    //obtener precio de producto (full, especial / primera compra)
                    if (! empty($store_product['special_price'])) {
                        if ($store_product['first_order_special_price']) {
                            if (!Session::get('has_orders')) {
                                $price = $store_product['special_price'];
                            } else {
                                $price = $store_product['price'];
                            }
                        } else {
                            $price = $store_product['special_price'];
                        }
                        $response['cart']['special_price_amount'] += ($store_product['price'] - $price) * $store_product['cart_quantity'];
                    } else {
                        $price = $store_product['price'];
                    }

                    $store_product['sub_total'] = $store_product['cart_quantity'] * $price;
                    if ($store_product['cart_quantity_full_price']) {
                        $store_product['sub_total'] += $store_product['cart_quantity_full_price'] * $store_product['price'];
                    }
                    $response['cart']['stores'][$store_id]['sub_total'] += $store_product['sub_total'];
                    $response['cart']['total_amount'] += $store_product['sub_total'];
                    $response['cart']['total_quantity']++;
                    $response['cart']['stores'][$store_id]['delivery_discount_amount'] += $cart_product->delivery_discount_amount;
                    // Domicilio gratis por comprar producto especifico
                    array_push($response['cart']['stores'][$store_id]['products'], $store_product);
                }
            } else {
                $response['cart']['is_minimum_reached'] = 0;
            }

            //aplicar descuento a la compra
            $total_cart = $response['cart']['total_amount'] + $response['cart']['delivery_amount'];

            //si tiene cupon activo en sesion
            if ($delivery_day && isset($this->user_discounts['coupon'])) {
                $coupon = $this->user_discounts['coupon'];
                //si el cupon cumple con los totales requeridos
                if ($coupon->minimum_order_amount && !$coupon->maximum_order_amount && $total_cart < $coupon->minimum_order_amount) {
                    $response['cart']['coupon_message'] = 'Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el total del pedido sin incluir el domicilio debe ser mayor o igual a <b>$' . number_format($coupon->minimum_order_amount, 0, ',', '.') . '</b>. ';
                } elseif ($coupon->maximum_order_amount && !$coupon->minimum_order_amount && $total_cart > $coupon->maximum_order_amount) {
                    $response['cart']['coupon_message'] = 'Para redimir el cupón de descuento <b>' . $coupon->code . '</b> el total del pedido sin incluir el domicilio debe ser menor o igual a <b>$' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '</b>. ';
                } elseif ($coupon->minimum_order_amount && $coupon->maximum_order_amount && ($total_cart < $coupon->minimum_order_amount || $total_cart > $coupon->maximum_order_amount)) {
                    $response['cart']['coupon_message'] = 'Para redimir el cupón de descuento <b>' . $coupon->coupon . '</b> el total del pedido sin incluir el domicilio debe estar entre <b>$' . number_format($coupon->minimum_order_amount, 0, ',', '.') . '</b> y <b>$' . number_format($coupon->maximum_order_amount, 0, ',', '.') . '</b>. ';
                }


                if (!isset($response['cart']['coupon_message'])) {
                    $discount_amount = 0;
                    $response['cart']['coupon_message'] = '';
                    //validar cupon para tienda / categoria / subcategoria / producto especifico
                    if ($coupon->redeem_on == 'Specific Store') {
                        foreach ($cart_products as $store_product) {
                            $is_valid = false;
                            if ($store_product->store_id == $coupon->store_id) {
                                if ($coupon->department_id) {
                                    if ($store_product->department_id == $coupon->department_id) {
                                        if ($coupon->shelf_id) {
                                            if ($store_product->shelf_id == $coupon->shelf_id) {
                                                if ($coupon->store_product_id) {
                                                    if ($store_product->id == $coupon->store_product_id) {
                                                        $is_valid = true;
                                                    }
                                                } else {
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $is_valid = true;
                                        }
                                    }
                                } else {
                                    $is_valid = true;
                                }
                                if ($is_valid) {
                                    if ($coupon->type == 'Discount Percentage') {
                                        $discount_amount += round(
                                            ($store_product->price * $store_product->cart_quantity) * ($coupon->amount / 100)
                                        );
                                    }
                                    if ($coupon->type == 'Credit Amount') {
                                        $discount_amount += round(
                                            $store_product->price * $store_product->cart_quantity
                                        );
                                    }
                                }
                            }
                        }
                        $store = Store::select(
                            'stores.name AS store_name', 'departments.name AS department_name',
                            'shelves.name AS shelf_name',
                            DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"),
                            'cities.city'
                        )
                            ->leftJoin('departments', function ($join) use ($coupon) {
                                $join->on('departments.store_id', '=', 'stores.id');
                                $join->on('departments.id', '=',
                                    DB::raw($coupon->department_id ? $coupon->department_id : 0));
                            })
                            ->leftJoin('shelves', function ($join) use ($coupon) {
                                $join->on('shelves.department_id', '=', 'departments.id');
                                $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                            })
                            ->leftJoin('store_products', function ($join) use ($coupon) {
                                $join->on('store_products.shelf_id', '=', 'shelves.id');
                                $join->on('store_products.product_id', '=',
                                    DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                            })
                            ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                            ->join('cities', 'cities.id', '=', 'stores.city_id')
                            ->where('stores.id', $coupon->store_id)
                            ->first();

                        $discount_message = $store->store_name;
                        if (!empty($store->department_name)) {
                            $discount_message .= ' / ' . $store->department_name;
                        }
                        if (!empty($store->shelf_name)) {
                            $discount_message .= ' / ' . $store->shelf_name;
                        }
                        if (!empty($store->product_name)) {
                            $discount_message .= ' / ' . $store->product_name;
                        }
                        $discount_message .= ' en ' . $store->city;

                        if ($discount_amount) {
                            if ($coupon->type == 'Discount Percentage') {
                                $response['cart']['stores'][$coupon->store_id]['discount_percentage_amount'] = $coupon->amount;
                            } else {
                                if ($discount_amount > $coupon->amount) {
                                    $discount_amount = $coupon->amount;
                                }
                            }
                            $response['cart']['stores'][$coupon->store_id]['discount_amount'] = $discount_amount;
                            $response['cart']['discount_amount'] += $discount_amount;
                        } else {
                            $response['cart']['coupon_message'] .= 'El cupón de descuento <b>' . $coupon->code . '</b> aplica exclusivamente para <b>' . $discount_message . '</b>. ';
                        }
                    }
                    //validar metodo de pago del cupon
                    if ($coupon->payment_method) {
                        $response['cart']['coupon_message'] .= 'El metodo de pago debe ser con <b>' . $this->config['payment_methods_names'][$coupon->payment_method] . trim(' ' . $coupon->cc_bank) . '</b>. ';
                    }
                    //si el cupon no es de productos especificos se aplica el cupon
                    if ($coupon->redeem_on != 'Specific Store') {
                        if ($coupon->type == 'Credit Amount') {
                            if ($coupon->amount > $response['cart']['total_amount']) {
                                $coupon->amount = $response['cart']['total_amount'];
                            }
                            $discount_credit_amount = $coupon->amount;
                        }
                        if ($coupon->type == 'Discount Percentage') {
                            $discount_percentage = $coupon->amount;
                        }
                    }
                }
            }

            //si no tiene tiene cupon activo y si el total es mayor al requerido se aplica descuento con credito
            if (!isset($this->user_discounts['coupon']) && $this->user_discounts['amount'] && $total_cart > $this->user_discounts['minimum_discount_amount']) {
                if ($this->user_discounts['amount'] > $response['cart']['total_amount']) {
                    $this->user_discounts['amount'] = $response['cart']['total_amount'];
                }
                $discount_credit_amount = $this->user_discounts['amount'];
            }

            //obtener descuentos activos en checkout
            if ($delivery_day)
                $checkout_discounts = Order::getCheckoutDiscounts(Session::get('city_id'));

            foreach ($response['cart']['stores'] as $store_id => $store) {
                //domicilio gratis por referido o en proximo pedido
                if ($this->user_discounts['free_delivery_days'] || $this->user_discounts['free_delivery_next_order']) {
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                }

                //validar domicilio gratis global
                if ($delivery_day && $response['cart']['stores'][$store_id]['delivery_amount']) {
                    if ($checkout_discounts['free_delivery']['status']) {
                        if (!empty($checkout_discounts['free_delivery']['store_ids'])) {
                            $store_ids = explode(',', $checkout_discounts['free_delivery']['store_ids']);
                            $store_ids = count($store_ids) > 1 ? $store_ids : array($store_id);
                            if (in_array($store_id,
                                    $store_ids) && $total_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                                $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                            }
                        } else {
                            if (!empty($checkout_discounts['free_delivery']['city_ids'])) {
                                $city_ids = explode(',', $checkout_discounts['free_delivery']['city_ids']);
                                $city_ids = count($city_ids) > 1 ? $city_ids : array($checkout_discounts['free_delivery']['city_ids']);
                                if (in_array($response['cart']['stores'][$store_id]['city_id'],
                                        $city_ids) && $total_cart > $checkout_discounts['free_delivery']['minimum_order_amount']) {
                                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                                }
                            }
                        }
                    }
                }

                //validar descuento en domicilio por productos
                if ($delivery_day && $response['cart']['stores'][$store_id]['delivery_amount'] && $response['cart']['stores'][$store_id]['delivery_discount_amount']) {
                    $delivery_amount = $response['cart']['stores'][$store_id]['delivery_amount'] >= $response['cart']['stores'][$store_id]['delivery_discount_amount'] ? $response['cart']['stores'][$store_id]['delivery_amount'] - $response['cart']['stores'][$store_id]['delivery_discount_amount'] : 0;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = $delivery_amount;
                }

                $response['cart']['delivery_amount'] += $response['cart']['stores'][$store_id]['delivery_amount'];

                $freeDelivery = $this->deliveryDiscountUseCase->handle(
                    $this->user_id,
                    true,
                    false,
                    session::get('city_id'),
                    $store_id,
                    $response['cart']['total_amount']
                );

                if ($freeDelivery) {
                    $response['cart']['delivery_amount'] = 0;
                    $response['cart']['stores'][$store_id]['delivery_amount'] = 0;
                }


                //sino hay cupon activo
                if ($delivery_day && !isset($this->user_discounts['coupon'])) {
                    //validar descuento de credito global
                    if ($checkout_discounts['discount_credit']['status']) {
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_credit']['minimum_order_amount'] && !$checkout_discounts['discount_credit']['maximum_order_amount'] && $total_cart < $checkout_discounts['discount_credit']['minimum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_credit']['maximum_order_amount'] && !$checkout_discounts['discount_credit']['minimum_order_amount'] && $total_cart > $checkout_discounts['discount_credit']['maximum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_credit']['minimum_order_amount'] && $checkout_discounts['discount_credit']['maximum_order_amount'] &&
                            ($total_cart < $checkout_discounts['discount_credit']['minimum_order_amount'] || $total_cart > $checkout_discounts['discount_credit']['maximum_order_amount'])) {
                            $is_valid = false;
                        }

                        if ($is_valid) {
                            $discount_global_credit_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_credit']['store_id']) {
                                foreach ($store['products'] as $store_product) {
                                    $store_product = (object)$store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_credit']['store_id']) {
                                        if ($checkout_discounts['discount_credit']['department_id']) {
                                            if ($store_product->department_id == $checkout_discounts['discount_credit']['department_id']) {
                                                if ($checkout_discounts['discount_credit']['shelve_id']) {
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_credit']['shelve_id']) {
                                                        if ($checkout_discounts['discount_credit']['store_product_id']) {
                                                            if ($store_product->id == $checkout_discounts['discount_credit']['store_product_id']) {
                                                                $is_valid = true;
                                                            }
                                                        } else {
                                                            $is_valid = true;
                                                        }
                                                    }
                                                } else {
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $is_valid = true;
                                        }
                                    }
                                    if ($is_valid) {
                                        $discount_global_credit_amount += round($product_price * $store_product->cart_quantity,
                                            0);
                                    }
                                }
                            } else {
                                $discount_global_credit_amount += $checkout_discounts['discount_credit']['amount'];
                            }

                            if ($discount_global_credit_amount) {
                                if ($discount_global_credit_amount > $checkout_discounts['discount_credit']['amount']) {
                                    $discount_global_credit_amount = $checkout_discounts['discount_credit']['amount'];
                                }
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_credit_amount;
                                $response['cart']['discount_amount'] += $discount_global_credit_amount;
                            }
                        }
                    }

                    //validar descuento por porcentaje global
                    if ($checkout_discounts['discount_percentage']['status']) {
                        $is_valid = true;
                        //si cumple con los totales requeridos
                        if ($checkout_discounts['discount_percentage']['minimum_order_amount'] && !$checkout_discounts['discount_percentage']['maximum_order_amount'] && $total_cart < $checkout_discounts['discount_percentage']['minimum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_percentage']['maximum_order_amount'] && !$checkout_discounts['discount_percentage']['minimum_order_amount'] && $total_cart > $checkout_discounts['discount_percentage']['maximum_order_amount']) {
                            $is_valid = false;
                        } elseif ($checkout_discounts['discount_percentage']['minimum_order_amount'] && $checkout_discounts['discount_percentage']['maximum_order_amount'] &&
                            ($total_cart < $checkout_discounts['discount_percentage']['minimum_order_amount'] || $total_cart > $checkout_discounts['discount_percentage']['maximum_order_amount'])) {
                            $is_valid = false;
                        }

                        if ($is_valid) {
                            $discount_global_percentage_amount = 0;
                            //descuento por tienda /categoria /subcategoria / producto especifico
                            if ($checkout_discounts['discount_percentage']['store_id']) {
                                foreach ($store['products'] as $store_product) {
                                    $store_product = (object)$store_product;
                                    $product_price = empty($store_product->special_price) ? $store_product->price : $store_product->special_price;
                                    $is_valid = false;
                                    if ($store_id == $checkout_discounts['discount_percentage']['store_id']) {
                                        if ($checkout_discounts['discount_percentage']['department_id']) {
                                            if ($store_product->department_id == $checkout_discounts['discount_percentage']['department_id']) {
                                                if ($checkout_discounts['discount_percentage']['shelve_id']) {
                                                    if ($store_product->shelf_id == $checkout_discounts['discount_percentage']['shelve_id']) {
                                                        if ($checkout_discounts['discount_percentage']['store_product_id']) {
                                                            if ($store_product->id == $checkout_discounts['discount_percentage']['store_product_id']) {
                                                                $is_valid = true;
                                                            }
                                                        } else {
                                                            $is_valid = true;
                                                        }
                                                    }
                                                } else {
                                                    $is_valid = true;
                                                }
                                            }
                                        } else {
                                            $is_valid = true;
                                        }
                                    }
                                    if ($is_valid) {
                                        $discount_global_percentage_amount += round(($product_price * $store_product->cart_quantity) * ($checkout_discounts['discount_percentage']['amount'] / 100),
                                            0);
                                    }
                                }
                            } else {
                                $discount_global_percentage_amount += round($response['cart']['total_amount'] * ($checkout_discounts['discount_percentage']['amount'] / 100),
                                    0);
                            }

                            if ($discount_global_percentage_amount) {
                                $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $checkout_discounts['discount_percentage']['amount'];
                                $response['cart']['stores'][$store_id]['discount_amount'] = $discount_global_percentage_amount;
                                $response['cart']['discount_amount'] += $discount_global_percentage_amount;
                            }
                        }
                    }
                }

                if ($store['sub_total'] < $store['minimum_order_amount']) {
                    $response['cart']['stores'][$store_id]['is_minimum_reached'] = 0;
                    $response['cart']['is_minimum_reached'] = 0;
                }
                if (Auth::check() && in_array(Auth::user()->type, User::DARKSUPERMARKET_TYPE)) {
                    if ($store['sub_total'] >= Config::get('app.dark_supermarket.minimum_order_amount')) {
                        $response['cart']['stores'][$store_id]['is_minimum_reached'] = 1;
                        $response['cart']['is_minimum_reached'] = 1;
                    }
                }


                //credito
                if ($delivery_day && (!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                    && isset($discount_credit_amount) && $discount_credit_amount) {
                    $total_order_store = $response['cart']['stores'][$store_id]['sub_total'] + $response['cart']['stores'][$store_id]['delivery_amount'];
                    $discount_amount = $discount_credit_amount > $total_order_store ? $total_order_store : $discount_credit_amount;
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['discount_amount'] += $discount_amount;
                    $discount_credit_amount -= $discount_amount;
                }
                //porcentaje
                if ($delivery_day && (!isset($discount_global_credit_amount) || !$discount_global_credit_amount) && (!isset($discount_global_percentage_amount) || !$discount_global_percentage_amount)
                    && isset($discount_percentage) && $discount_percentage) {
                    $discount_amount = round(($response['cart']['stores'][$store_id]['sub_total'] * $discount_percentage) / 100,
                        2);
                    $response['cart']['stores'][$store_id]['discount_amount'] = $discount_amount;
                    $response['cart']['stores'][$store_id]['discount_percentage_amount'] = $discount_percentage;
                    $response['cart']['discount_amount'] += $discount_amount;
                }

                $response['cart']['stores'][$store_id]['sub_total'] = $store['sub_total'];
                $response['cart']['stores'][$store_id]['is_minimum_reached'] = $store['is_minimum_reached'];
            }

            //producto sugerido
            $suggested_product = null;
            $excluded = SuggestedProduct::promosForProducts($store_product_ids);
            $suggested_products = SuggestedProduct::forShelves($shelf_ids, $excluded, 1, $warehouse, $info);
            if (isset($suggested_products[0])) {
                $suggested_product = $suggested_products[0];
            }


            $response['status'] = true;
            $response['html'] = View::make('cart')
                ->with('cart', $response['cart'])
                ->with('store', $this->store)
                ->with('referer', StoreController::getReferrer() === 'checkout' ? 'checkout' : 'cart')
                ->with('suggested_product', $suggested_product)
                ->render();

            $response['credit_available'] = $this->user_discounts['amount'];
            $response['free_delivery_days'] = $this->user_discounts['free_delivery_days'];
            $response['free_delivery_next_order'] = $this->user_discounts['free_delivery_next_order'];
            if (isset($store_id) && isset($response['cart']['stores'][$store_id]['products']))
                unset($response['cart']['stores'][$store_id]['products']);

            /*if (!json_encode($response)) {
                $error_log = new ErrorLog();
                $error_log->url = \URL::current();
                $error_log->response_http_code = 513;
                $error_log->message = json_last_error();
                $error_log->request = serialize($response);
                $error_log->ip = get_ip();
                $error_log->save();
            }*/

            return Response::json($response, 200);
        }

        $error = 'Ocurrió un problema al obtener la información del carrito.';
        return Response::json(['status' => false, 'error' => $error], 200);
    }

    /**
     * Guarda una tarjeta de credito nueva
     */
    public function save_card()
    {
        try {
            DB::beginTransaction();
            if (!$this->user_id) {
                return Redirect::to('/');
            }

            $user = Auth::user();
            $data = array(
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'user_id' => $user->id
            );

            $address = UserAddress::where('user_id', $user->id)->first();
            if ($address) {
                $data['address'] = $address->address;
                $data['address_further'] = $address->address_further;
            } else {
                $data['address'] = $data['address_further'] = 'No especificada';
            }

            $data = array_merge($data, Input::all());
            $payu = new PayU;
            //validar pais de tarjeta de credito
            $bin = substr($data['number_cc'], 0, 6);
            $credit_card = $payu->getCreditCardInfo($bin, $data);
            if (!$credit_card) {
                return Redirect::back()->with('message',
                    'Ocurrió un problema al validar el origen de tu tarjeta de crédito.')
                    ->with('type', 'failed');
            }
            if (!$credit_card->is_valid)
                return Redirect::to('/mi-cuenta')->with('message', 'Solo se aceptan tarjetas de crédito nacionales.')
                                       ->with('type', 'failed');

            $data['card_type'] = get_card_type($data['number_cc']);

            $result = $payu->associateCreditCard($data);
            if (!$result['status']) {
                return Redirect::back()->with('message',
                    $result['message'])
                    ->with('type', 'failed');
            }

            if (!in_array($result['response']->creditCardToken->paymentMethod, $this->config['credit_cards_types'])) {
                return Redirect::back()->with('message',
                    'Solo se aceptan tarjetas de crédito ' . $this->config['credit_cards_message'])
                    ->with('type', 'failed');
            }

            $user_credit_card = new UserCreditCard;
            $user_credit_card->card_token = $result['response']->creditCardToken->creditCardTokenId;
            $user_credit_card->user_id = $user->id;
            $user_credit_card->bin = substr($data['number_cc'], 0, 6);
            $user_credit_card->type = $data['card_type'];
            $user_credit_card->last_four = substr($data['number_cc'], 12, 4);
            $user_credit_card->country = $credit_card->country_name;
            $user_credit_card->holder_document_type = $data['document_type_cc'];
            $user_credit_card->holder_document_number = $data['document_number_cc'];
            $user_credit_card->holder_name = $data['name_cc'];
            $user_credit_card->holder_email = $data['email_cc'];
            $user_credit_card->holder_phone = $data['phone_cc'];
            $user_credit_card->holder_address = $data['address_cc'];
            $user_credit_card->holder_address_further = $data['address_further_cc'];
            $user_credit_card->holder_city = $data['city_cc'];
            $user_credit_card->save();

            $message = "";
            if ($credit_card && Config::get('app.payu.precharge_enable')) {
                $preCharge = $user->preChargeCreditCard($user->id, $user_credit_card->id, 0);
                if (isset($preCharge['status']) && !$preCharge['status']) {
                    $this->response['message'] = $preCharge['message'];
                    throw new Exception('Ocurrió un error al añadir la tarjeta de crédito.'.$preCharge['message']);
                }elseif (isset($preCharge['status']) && $preCharge['status']){
                    $message = ', '.$preCharge['message'];
                }
            }

            DB::commit();
            return Redirect::to('/mi-cuenta')->with('message', 'Tarjeta de crédito guardada con éxito'.$message)
                ->with('type', 'success');
        } catch (\Exception $exception) {

            DB::rollback();

            //guardar log
            try {
                $response_headers = '';
                $request = json_encode(Input::get());
                $request_headers = '';
                foreach (getallheaders() as $name => $value) {
                    $request_headers .= '
                    ' . $name . ': ' . $value;
                }
            } catch (Exception $e) {
                $request = is_string($request) ? $request : 'Error en json_encode: Invalid UTF-8 sequence in argument';
            }

            //encriptacion de tarjeta de credito
            if (isset($request['number_cc']) && !empty($request['number_cc'])) {
                $request['number_cc'] = substr($request['number_cc'], 0, 6) . '******' . substr($request['number_cc'], 12, 16);
                $request['code_cc'] = '***';
            }

            $log = new CheckoutLog;
            $log->url = URL::current();
            $log->request = "URL: " . $log->url . ' ' . Request::method() . "\r\nREQUEST HEADERS: " . $request_headers . "\r\nREQUEST: " . $request;
            $log->message = "MESSAGE: " . $exception->getMessage() . "\r\nFILE: " . $exception->getFile() . "\r\nLINE: " . $exception->getLine();
            $log->ip = get_ip();
            $log->created_at = $log->updated_at = date('Y-m-d H:i:s');
            $log->save();
            return Redirect::back()->with('message', 'Ocurrió un error al añadir la tarjeta de crédito.')->with('type', 'failed');
        }
    }

    /**
     * Elimina una tarjeta de credito
     */
    public function delete_card($id)
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        //validar pedidos en proceso con la tarjeta a eliminar
        $orders = Order::where('status', '<>', 'Delivered')
            ->where('status', '<>', 'Cancelled')
            ->where('credit_card_id', $id)
            ->count();
        if (!$orders) {
            if (!$user_credit_card = UserCreditCard::find($id)) {
                return Redirect::back()->with('message', 'La tarjeta de crédito no existe.');
            }
            //PayU
            if (strlen($user_credit_card->card_token) == Config::get('app.payu.token_length')) {
                $payu = new PayU;
                $result = $payu->deleteCreditCard($this->user_id, $user_credit_card->card_token);
            } else {
                $payment = new Payment;
                $result = $payment->deleteCreditCard($this->user_id, Auth::user()->customer_token,
                    $user_credit_card->card_token);
            }
            if (!$result['status']) {
                return Redirect::back()->with('message', 'Ocurrió un error al eliminar la tarjeta de crédito.');
            }
        } else {
            return Redirect::back()->with('message',
                'No se puede eliminar la tarjeta de crédito por que esta asociada a un pedido en proceso.');
        }

        if (UserCreditCard::where('user_id', $this->user_id)->where('id', $id)->delete()) {
            return Redirect::to('/mi-cuenta')->with('message',
                'La tarjeta de crédito fue eliminada con éxito')->with('type', 'success');
        }

        return Redirect::back()->with('message', 'Ocurrió un error al eliminar la tarjeta de crédito.');
    }

    /**
     * Validar numero de celular del usuario
     * @return mixed
     */
    public function cellphone_validation()
    {
        $code = \Input::get('code');
        $user = \Auth::user();

        if (!$code) {
            return Response::json([
                'status' => false,
                'message' => 'El código validación es obligatorio.',
            ]);
        }

        try {
            $phone = User::validatePhoneNumber($code, $user);
        } catch (\exceptions\MerqueoException $exception) {
            return Response::json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }

        Session::put([
            'phoneValidation' => [
                'validated' => true,
                'number' => $phone,
            ]
        ]);

        return Response::json([
            'status' => true,
            'message' => 'Tú número de celular ha sido validado con éxito.',
            'result' => $phone,
        ]);
    }

    /**
     * Guarda respuesta de encuesta para primer pedido
     */
    public function save_survey()
    {
        if (!$this->user_id) {
            return Response::json(array('status' => false, 'message' => 'Debes iniciar sesión.'), 200);
        }

        if (!Input::has('survey_id') || !Input::has('answer') || !Input::has('os') || !Input::has('order_id')) {
            return Response::json(array('status' => false, 'message' => 'Hay datos obligatorios vacios.'), 200);
        }

        $user_app_survey = new UserSurvey;
        $user_app_survey->survey_id = Input::get('survey_id');
        $user_app_survey->user_id = $this->user_id;
        $user_app_survey->order_id = Input::get('order_id');
        $user_app_survey->os = Input::get('os');
        $user_app_survey->answer = Input::get('answer');
        $user_app_survey->save();

        return Response::json(array('status' => true, 'message' => 'Tu respuesta fue guardada, muchas gracias.'), 200);
    }

    /**
     * Terminos y condiciones
     */
    public function terms()
    {
        return View::make('terms')
            ->with('admin_email', Config::get('app.admin_email'))
            ->with('stores', $this->stores)
            ->with('metadata', $this->metadata)
            ->with('user_credit_amount', $this->user_discounts['amount'])
            ->with('footer', $this->getFooter());
    }

    /**
     * Terminos y condiciones
     */
    public function privacy_policy()
    {
        return View::make('privacy_policy')
            ->with('admin_email', Config::get('app.admin_email'))
            ->with('stores', $this->stores)
            ->with('metadata', $this->metadata)
            ->with('user_credit_amount', $this->user_discounts['amount'])
            ->with('footer', $this->getFooter());
    }

    /**
     * Preguntas frecuentes
     */
    public function faq()
    {

        $minimum_order_amount = Store::where('id', 63)->pluck('minimum_order_amount');
        return View::make('faq')
            ->with('minimum_order_amount', $minimum_order_amount)
            ->with('stores', $this->stores)
            ->with('metadata', $this->metadata)
            ->with('user_credit_amount', $this->user_discounts['amount'])
            ->with('footer', $this->getFooter());
    }

    /**
     *  Validar razón social y documento
     */
    public function verify_data()
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        if ($user = User::find($this->user_id)) {
            if (!empty($user->identity_number) && !empty($user->identity_type) && !empty($user->business_name)) {
                $response['success'] = 'true';
            } else {

                $current_month = date('m');
                if ($order = Order::where(DB::raw('DATE_FORMAT(date,"%m")'), '=', $current_month)->where('user_id',
                    $this->user_id)->first()) {
                    $delivery_date = date_create($order->delivery_date);
                    $delivery_date = date_format($delivery_date, 'm');

                    if ($delivery_date == $current_month) {
                        $response['success'] = 'false';
                        $response['error'] = 'Por favor completar los datos faltantes.';
                    } else {
                        $response['success'] = 'true';
                    }
                } else {
                    $response['success'] = 'true';
                }
            }
        } else {
            $response['success'] = 'false';
            $response['error'] = 'Ocurrió un error al validar los datos de usuario';
            $response['error_code'] = 413;
        }
        return Response::json($response, 200);
    }

    /**
     *  Guardar datos para facturaación
     */
    public function save_invoice_data()
    {
        if (!$this->user_id) {
            return Redirect::to('/');
        }

        if (Input::has('identity_type') && Input::get('identity_type') == 'NIT' && strlen((string)Input::get('identity_type')) > 9) {
            return Redirect::to('/mi-cuenta')->with('type', 'error')->with('message',
                'Número de NIT supera la longitud permitida.');
        }

        $user = User::find($this->user_id);
        if (Input::has('identity_type')) {
            $user->identity_type = Input::get('identity_type');
        }
        if (Input::has('identity_number')) {
            $user->identity_number = Input::get('identity_number');
        }
        if (Input::has('business_name')) {
            $user->business_name = Input::get('business_name');
        }

        $user->save();

        return Redirect::to('/mi-cuenta')->with('type', 'success')->with('message',
            'Tus datos fueron guardados con éxito, ya puedes descargar tus facturas.');
    }

    /**
     *  Genera factura de pedidos entregados
     *
     * @param  $reference Número de refencia del pedido
     * @return \Illuminate\Http\RedirectResponse $file PDF generado
     */
    public function generate_invoice($reference)
    {
        $userId = $this->user_id ?: Input::get('owner_invoice_id');

        if (!$userId) {
            return Redirect::to('/');
        }

        $order = Order::select('orders.*', 'stores.name AS store_name')
            ->join('stores', 'store_id', '=', 'stores.id')
            ->where('orders.reference', $reference)
            ->where('orders.user_id', $userId)
            ->orderby('orders.id', 'DESC')
            ->first();

        if ($order) {
            if ($order->user_id == $userId) {
                if ($order->status === 'Delivered' && $order->store_id == 63) {
                    Pdf::generateInvoice($order);
                } else {
                    return Redirect::to('/mi-cuenta')->with('message', 'Pedido sin facturar.')->with('type', 'error');
                }
            } else {
                return Redirect::to('/mi-cuenta')->with('message',
                    'No coincide el usuario con el del pedido.')->with('type', 'error');
            }
        } else {
            return Redirect::to('/mi-cuenta')->with('message', 'No existe el pedido.')->with('type', 'error');
        }
    }

    /**
     *  Genera factura de pedidos entregados
     *
     * @param  $order_token Número de refencia del pedido
     * @return $file PDF generado
     */
    public function order_generate_invoice($order_token)
    {
        $order = Order::with('store')
            ->select('orders.*', 'stores.name AS store_name')
            ->join('stores', 'store_id', '=', 'stores.id')
            ->where('user_score_token', $order_token)
            ->where('orders.status', 'Delivered')
            ->orderby('orders.id', 'DESC')->firstOrFail();

        if ($order) {
            if ($order->status === 'Delivered') {
                Pdf::generateInvoice($order);
            } else {
                return [
                    'status' => false,
                    'message' => 'El token ha expirado.'
                ];
            }
        } else {
            return [
                'status' => false,
                'message' => 'No existe el pedido.'
            ];
        }
    }

    /**
     * Registrar calificacion de pedido
     *
     * @param int $order_token
     * @return array $response Respuesta
     */
    public function order_score($order_token)
    {
        $order = Order::with('store')->where('user_score_token', $order_token)->orderBy('orders.id', 'desc')->first();
        $input = Input::all();
        // Se debe cumplir la validación que se realiza en la aplicación del identificador del usuario.
        $input['order_id'] = $order->id;
        $input['user_id'] = $order->user->id;
        DB::beginTransaction();

        try {
            $order = Order::validateScoreData($input);
            $order->user_score_source = 'Web';
            $order->saveOrderScore($input);
            DB::commit();

            $management_date = new DateTime($order->management_date);
            $current_date = new DateTime();
            $diff = $current_date->diff($management_date);
            $hours = $diff->h;
            $hours = $hours + ($diff->days * 24);
            if ($hours <= 12) {
                Order::getStatusFlow($order->id, $order->user_id, true);
            }

        } catch (HttpException $exception) {
            DB::rollback();

            return \Response::json([
                'status' => false,
                'message' => $exception->getMessage()
            ], $exception->getStatusCode());
        }

        if (!$order->hasGoodRate()) {
            Ticket::createTicket($order, \CustomerService::TICKET);
        }

        $message = $order->hasGoodRate()
            ? 'Tu opinión es muy valiosa para nosotros.'
            : 'Recibimos tu mensaje. Pronto te daremos respuesta a tu correo electrónico.';

        $log = new \OrderLog();
        $log->type = 'Calificación: ' . ($order->hasGoodRate() ? 'Positiva' : 'Negativa');
        $log->user_id = $order->user_id;
        $log->order_id = $order->id;
        $log->save();

        return \Response::json([
            'status' => true,
            'message' => $message
        ]);
    }

    /**
     * Muestra el formulario para que el cliente pueda calificar
     * el servicio.
     *
     * @param $token
     * @return mixed
     */
    public function show_order_score_form($token)
    {
        $order = Order::where('user_score_token', $token)
            ->orderBy('id', 'desc')
            ->first();

        // TODO: Refactorizar modulo de posventa
        // Si el usuario ya calificó
        $doneRate = !empty($order->user_score_date);

        if (empty($order)) {
            return Redirect::away(Config::get('app.url_web'));
        }

        return View::make('score_layout', compact('order', 'doneRate'));
    }

    /**
     * Obtiene datos del pedido para que cliente pueda calificar el servicio.
     *
     * @param $token
     * @return mixed
     */
    public function get_score_order_data_ajax($order_token)
    {
        try {
            $order = Order::select('id', 'management_date', 'user_id', 'created_at')
                ->where('user_score_token', $order_token)
                ->orderBy('id', 'desc')->firstOrFail();

            return \Response::json([
                'status' => true,
                'message' => $order->getOrderScoreFormat()
            ]);
        } catch (ModelNotFoundException $e) {
            return \Response::json([
                'status' => false,
                'message' => 'Ocurrió un error obteniendo los datos. Token incorrecto'
            ], 400);
        } catch (\Exception $e) {
            return \Response::json([
                'status' => false,
                'message' => 'Ocurrió un error obteniendo los datos. Por favor, vuelva a intentarlo'
            ], 500);
        }
    }

    /**
     * Determina si el producto esta disponible en la bodaga donde se ubica el usuario.
     *
     * @param $store_product_id
     * @return array
     */
    public function is_product_available_on_warehouse_ajax($store_product_id)
    {
        $warehouse = $this->getCurrentWarehouse();
        $product_exists = StoreProductWarehouse::available()
                ->where('store_product_id', $store_product_id)
                ->where('warehouse_id', $warehouse->id)
                ->with('storeProduct.product')
                ->count() > 0;

        if (!$product_exists) {
            \Session::put('product_not_available_on_change', true);
        }

        return [
            'status' => $product_exists,
            'message' => $product_exists
                ? 'Producto disponible'
                : 'El producto no esta disponible en la tienda'
        ];
    }

    /**
     * Califica la respuesta enviada por un "customer service", en caso
     * de que sea negativa crea un nuevo ticket.
     *
     * @param string $call_token
     * @param int $rate
     * @return mixed
     */
    public function rate_customer_service_response($call_token, $rate)
    {
        DB::beginTransaction();
        try {
            $call = TicketCall::with('ticket.order')
                ->where('token', $call_token)
                ->firstOrFail();

            if (empty($call->user_score)) {
                $call->user_score = $was_good_rate = !empty($rate);
                $call->save();

                if (!$was_good_rate) {
                    $message = 'Recibimos tu calificación. Pronto te daremos respuesta.';
                    Ticket::createTicket($call->ticket->order, CustomerService::PQR);
                } else {
                    $message = 'Tu opinión es muy valiosa para nosotros.';
                }
                DB::commit();
            } else {
                $message = 'Ya hemos registrado tu calificación.';
            }

        } catch (ModelNotFoundException $exception) {
            DB::rollback();
            throw new NotFoundHttpException('No se ha podido encontrar el ticket.');
        }

        return View::make('admin.customer_service.user_rate_response', compact('call', 'message'));
    }

    function soat_faq()
    {
        return View::make('soat_faq');
    }

    /**
     * Agrega productos al carrito actual desde el carrito
     * temporal almacenado en la sesión.
     *
     * @param Cart $current_cart
     * @return Cart
     */
    protected function registerTemporalCart(Cart $current_cart)
    {
        $cart = json_decode(Session::pull('tmp_cart'));
        $products = $cart ? (array)$cart->products : [];
        if (!empty($products)) {
            //elimino los productos del carrito actual
            CartProduct::where('cart_id', $current_cart->id)->delete();
            foreach ($cart->products as $i => $product) {
                if ($product->cart_quantity <= 0) {
                    continue;
                }
                $cart_product = new CartProduct();
                $cart_product->cart_id = $current_cart->id;
                $cart_product->store_product_id = $product->store_product_id;
                $cart_product->quantity = $product->cart_quantity;
                $cart_product->quantity_full_price = $product->cart_quantity_full_price;
                $cart_product->added_by = Auth::user()->id;
                $cart_product->save();
            }
        }

        return $cart;
    }

    /**
     * Determina si un usuario debe validar el estado de
     * su cuenta mediate accountKit.
     *
     * @return array
     */
    public function show_validate_phone_number()
    {
        $email = Input::get('email');
        $facebook_id = Input::get('facebook_id');

        return [
            'status' => true,
            'message' => 'Ok',
            'result' => !(Session::get('phoneValidation.validated') || User::where('email', $email)
                    ->orWhere('fb_id', $facebook_id)
                    ->count() > 0),
        ];
    }
}
