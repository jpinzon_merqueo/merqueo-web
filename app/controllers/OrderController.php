<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use orders\OrderStatus;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Hashids\Hashids;

/**
 * Class OrderController
 */
class OrderController extends BaseController
{
    const STORE_MEXICO = 66;

    /**
     * @var StoreProduct
     */
    private $storeProduct;

    /**
     * OrderController constructor.
     * @param StoreProduct $storeProduct
     */
    public function __construct(StoreProduct $storeProduct)
    {
        $this->storeProduct = $storeProduct;
    }

    /**
     * @param int $user_id
     * @param int $order_reference
     * @return mixed
     */
    public function index($user_id, $order_reference)
    {
        $order = Order::with('orderGroup')
            ->where('user_id', $user_id)
            ->where('reference', $order_reference)
            ->orderBy('id', 'desc')
            ->first();

        if (!$order) {
            throw new NotFoundHttpException('El pedido no existe.');
        }

        $data = [
            "token" => FirebaseClient::makeKey('general'),
            "url" => Config::get('app.firebase.general.url'),
            "coordinates" => [
                "lat" => $order->orderGroup->user_address_latitude,
                "lng" => $order->orderGroup->user_address_longitude,
            ],
            "rangeOrder" => $order->delivery_time,
            "direction" => $order->orderGroup->user_address,
            "vehicle_id" => $order->vehicle_id,
            'order' => $order,
            'driver' => $order->driver
        ];

        return View::make('webviews.follow_order', $data);
    }

    /**
     * @param $orderGroupId
     * @return JsonResponse
     */
    public function postOrder($orderGroupId)
    {
        Event::subscribe(new OrderEventHandler());

        $order = null;
        $orderData = null;
        safe_transaction(5, function () use ($orderGroupId, &$order, &$orderData) {

            \DB::beginTransaction();

            $orderGroup = OrderGroup::with('user', 'orders.storeProducts.product')
                ->findOrFail($orderGroupId);

            $order = $orderGroup->orders->first();
            $quickDelvery = in_array($order->deliveryWindow->shifts,
                [DeliveryWindow::SHIFT_FAST_DELIVERY, DeliveryWindow::SHIFT_EXPRESS]
            );

            $validStatus = [OrderStatus::VALIDATION, OrderStatus::INITIATED];

            //validar estado de pedido
            if (!in_array($order->status, $validStatus) && !($order->status == OrderStatus::ROUTED && $quickDelvery)) {
                return false;
            }

            $products = new Collection();
            foreach ($orderGroup->orders as $order) {
                $products = $order->storeProducts->map(function (StoreProduct $storeProduct) use ($order) {
                    $storeProduct->name = $storeProduct->product->name;
                    $storeProduct->quantity = $storeProduct->product->quantity;
                    $storeProduct->unit = $storeProduct->product->unit;
                    $storeProduct->qty = $storeProduct->pivot->quantity;
                    $storeProduct->price = $storeProduct->pivot->price;
                    $storeProduct->total = $storeProduct->pivot->price * $storeProduct->qty;
                    $storeProduct->store_product_id = $storeProduct->id;
                    $storeProduct->img = $storeProduct->product->image_medium_url;
                    $storeProduct->order_type = $order->type;

                    return $storeProduct;
                });
            }

            $this->storeProduct->storeProductsValidateStockDiscount($order->id);
            Sampling::getSamplingRelations($orderGroup, $products, $order);

            if (empty($order->order_validation_reason_id)) {
                $order->validateOrderBlackListed([
                    'address_id' => $orderGroup->address_id,
                    'user_id' => $orderGroup->user_id,
                    'payment_method' => $order->payment_method,
                    'credit_card_id' => $order->credit_card_id,
                ], $order->status);
            }
            $order->validateCommittedStock();
            $order->save();

            $user = $order->user;
            $hasReferred = !empty($user->referredBy);

            // Log de pedido
            $log = new OrderLog();
            $log->type = 'Pedido creado.';
            $log->user_id = $order->user_id;
            $log->order_id = $order->id;
            $log->save();

            $orderData = [
                'products_mail' => $products->toArray(),
                'product_events' => [],
                'total_products' => '',
                'total_items' => '',
                'is_new_user' => $user->orders()->count() === 1,
                'referred' => [
                    'success' => $hasReferred,
                    'referrer_name' => $hasReferred ? $user->referredBy->first_name : '',
                    'amount' => Config::get('app.referred.amount'),
                    'referred_by_amount' => Config::get('app.referred.referred_by_amount'),
                    'amount_expiration_days' => Config::get('app.referred.amount_expiration_days'),
                ],
                'is_express' => $quickDelvery,
            ];

            \DB::commit();
        },2);

        Event::fire('order.created', [[$order, $orderData]]);

        return Response::json('ok', 200);
    }

    /**
     * @param string $order
     * @return mixed
     */
    public function checkList($order)
    {
        $reference = $this->getReference($order, true);
        $order = Order::with(['orderProducts'])->where('id', $reference)
            ->orderBy('id', 'desc')
            ->first();

        if (!$order) {
            throw new NotFoundHttpException('El pedido no existe.');
        }

        $decimal_amount = $order->store_id == self::STORE_MEXICO ? 2 : 0;

        $data = [
            'order' => $order,
            'groups' => $order->orderProducts()
                            ->orderBy('fulfilment_status', 'desc')
                            ->get()
                            ->groupBy('fulfilment_status'),
            'have_discount' => $order->discount_amount ? true : false,
            'column_width' => $order->discount_amount ? 2 : 3,
            'planing_order'  => $this->getDataWindow($order),
            'decimal_amount' => $decimal_amount,
        ];

        return View::make('webviews.checklist', $data);
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function orderManaged($orderId){
        Event::subscribe(new OrderEventHandler());
        $order = Order::findOrFail($orderId);

        if (!$order) {
            throw new NotFoundHttpException('El pedido no existe.');
        }

        Event::fire('order.managed', [[$order, $order->orderGroup()]]);
        return $order->id;
    }

    /**
     * Method for get id order
     * @param $reference
     * @param bool $crypt
     * @return array|int
     */
    public function getReference($reference, $crypt = false) {
        if($crypt){
            $hash = new Hashids();

            $order = !empty($hash->decode($reference)) ? $hash->decode($reference) : 0;
            return  $order;
        }

        return $reference;
    }

    /**
     * Set message with data turn an count order by windows
     * @param $order
     * @return bool|string
     */
    public function getDataWindow($order)
	{
		$planning = $order->planning_sequence;

        if($planning) {
        	$totalQuantityOrders = 0;

        	if($amount_order_route = $order->getQuantityOrdersByRoute($order->route_id)){
				$totalQuantityOrders = $amount_order_route->orders_quantity;
			}

            return "$planning de $totalQuantityOrders";
        }

        return false;
    }

    /**
     * Get count order by route
     * @param $order
     * @return int
     */
    public function getOrderByWindow($order){
        $routes = $order->getOrdersByRoute($order->route_id);
        foreach ($routes as $route){
            if($route['delivery_time'] == $order->deliveryWindow->delivery_window) {
                return $route['orders_quantity'];
            }
        }

        return 0;
    }
}
