<?php

namespace Merqueo\Backend\Facades;

use Illuminate\Support\Facades\Facade as LaravelFacade;

/**
 * @see \Illuminate\Auth\AuthManager
 * @see \Illuminate\Auth\Guard
 */
class AuthBackend extends LaravelFacade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'backendauth'; }

}
