<?php

namespace Merqueo\Backend;

use Merqueo\Firebase\FirebaseCloudMessageResponse;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;

/**
 * Class MerqueoNotification
 * @package Merqueo\Backend
 */
class FirebaseNotification implements SendNotification
{
    /**
     * @var string|int
     */
    protected $device_id;

    /**
     * MerqueoNotification constructor.
     * @param string|int $device_id
     */
    public function __construct($device_id)
    {
        $this->device_id = $device_id;
    }

    /**
     * @param array $data
     * @param \Closure|null $callback
     * @return FirebaseCloudMessageResponse
     */
    public function send($data = [], \Closure $callback = null)
    {
        $body_notification = empty($data['message']) ? '' : $data['message'];
        $message = new Message();
        $message->setPriority('high')
            ->addRecipient(new Device($this->device_id))
            ->setNotification(new Notification('',  $body_notification))
            ->setData($data);

        $response = \FirebaseCloudMessage::send($message);

        return new FirebaseCloudMessageResponse($response);
    }
}