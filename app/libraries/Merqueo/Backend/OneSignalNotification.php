<?php

namespace Merqueo\Backend;

/**
 * Class MerqueoNotification
 * @package Merqueo\Backend
 */
class OneSignalNotification implements SendNotification
{
    /**
     * @var string|int
     */
    protected $device_id;

    /**
     * MerqueoNotification constructor.
     * @param string|int $device_id
     */
    public function __construct($device_id)
    {
        $this->device_id = $device_id;
    }

    /**
     * @param array $data
     * @param \Closure|null $callback
     * @return array
     */
    public function send($data = [], \Closure $callback = null)
    {
        return send_push_notification($this->device_id, $data);
    }
}