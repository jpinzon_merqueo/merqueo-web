<?php

namespace Merqueo\Backend;

/**
 * Interface SendNotification
 * @package Merqueo\Backend
 */
interface SendNotification
{
    /**
     * SendNotification constructor.
     * @param string $target_id Identificador del usuario a enviar la notificación.
     */
    public function __construct($target_id);

    /**
     * Envia una notificación push.
     *
     * @param array $data
     * @param \Closure|null $callback
     * @return mixed
     */
    public function send($data = [], \Closure $callback = null);
}
