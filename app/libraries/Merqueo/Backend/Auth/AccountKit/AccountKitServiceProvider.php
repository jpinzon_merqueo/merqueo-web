<?php

namespace Merqueo\Backend\Auth\AccountKit;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

/**
 * Class AccountKitServiceProvider
 * @package Merqueo\Backend\Auth\AccountKit
 */
class AccountKitServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AccountKit::class, function () {
            $identifier = Config::get('app.facebook_app_id');
            $secret = Config::get('app.url') === 'https://merqueo.com'
                ? '891c7057bf971db4403d4fd87d962f45'
                : Config::get('app.facebook_ak_secret');

            return new AccountKit($identifier, $secret);
        });
    }
}
