<?php

namespace Merqueo\Backend\Auth\AccountKit;

use exceptions\MerqueoException;
use exceptions\OAuthException;

/**
 * Class AccountKit
 * @package Merqueo\Backend\Auth\AccountKit
 */
class AccountKit
{
    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $applicationId;

    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var bool
     */
    private $debug;

    /**
     * AccountKit constructor.
     * @param $applicationId
     * @param $secret
     * @param string $version
     * @param bool $debug
     */
    public function __construct($applicationId, $secret, $version = 'v1.1', $debug = true)
    {
        $this->applicationId = $applicationId;
        $this->secret = $secret;
        $this->version = 'v1.1';
        $this->debug = $debug;
    }

    /**
     * @param $code
     * @return string
     *
     * @throws MerqueoException
     */
    public function getPhoneNumber($code)
    {
        $this->findAccessToken($code);

        return $this->getAccountKitInformation();
    }

    /**
     * @param $code
     * @return string
     */
    private function getExchangeUrl($code)
    {
        return "https://graph.accountkit.com/{$this->version}/access_token" .
            '?grant_type=authorization_code' .
            "&code={$code}" .
            "&access_token=AA|{$this->applicationId}|{$this->secret}";
    }

    /**
     * @param $code
     * @return string
     *
     * @throws MerqueoException
     */
    private function findAccessToken($code)
    {
        $tokenExchangeUrl = $this->getExchangeUrl($code);
        $data = $this->makeRequest($tokenExchangeUrl);

        if (isset($data['error'])) {
            $message = $this->debug
                ? $data['error']['message']
                : 'Ocurrió un error al validar los datos, por favor refresca la página intentalo de nuevo.';

            throw new MerqueoException($message);
        }

        $this->accessToken = $data['access_token'];

        return $data;
    }

    /**
     * @return string
     *
     * @throws MerqueoException
     */
    private function getAccountKitInformation()
    {
        $appSecretProof = hash_hmac('sha256', $this->accessToken, $this->secret);
        $data = $this->makeRequest(
            "https://graph.accountkit.com/{$this->version}/me" .
            "?access_token={$this->accessToken}".
            "&appsecret_proof={$appSecretProof}"
        );

        if (isset($data['error'])) {
            $message = $this->debug
                ? $data['error']['message']
                : 'Ocurrió un error al validar los datos, por favor refresca la página intentalo de nuevo.';

            throw new MerqueoException($message);
        }

        return empty($data['phone']['national_number']) ? 0 : $data['phone']['national_number'];
    }

    /**
     * @param $url
     * @return mixed
     */
    private function makeRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $data;
    }
}
