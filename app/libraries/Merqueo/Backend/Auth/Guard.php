<?php

namespace Merqueo\Backend\Auth;

use Illuminate\Auth\Guard as LaravelGuard;
use Illuminate\Auth\UserProviderInterface;
use Illuminate\Session\Store as SessionStore;
use Illuminate\Auth\UserInterface;

class Guard extends LaravelGuard {

    /**
     * Create a new authentication guard.
     *
     * @param  \Illuminate\Auth\UserProviderInterface  $provider
     * @param  \Illuminate\Session\Store  $session
     * @param  \Symfony\Component\HttpFoundation\Request  $request
     * @return void
     */
    public function __construct(UserProviderInterface $provider,
                                SessionStore $session,
                                Request $request = null)
    {
        parent::__construct($provider, $session, $request);
        $this->session = $session;
        $this->request = $request;
        $this->provider = $provider;
    }

    /**
     * Log a user into the application.
     *
     * @param  \Illuminate\Auth\UserInterface  $user
     * @param  bool  $remember
     * @return void
     */
    public function login(UserInterface $user, $remember = false)
    {
        $this->updateSession($user->getAuthIdentifier());

        // If the user should be permanently "remembered" by the application we will
        // queue a permanent cookie that contains the encrypted copy of the user
        // identifier. We will then decrypt this later to retrieve the users.
        if ($remember)
        {
            $this->createRememberTokenIfDoesntExist($user);

            $this->queueRecallerCookie($user);
        }

        // If we have an event dispatcher instance set we will fire an event so that
        // any listeners will hook into the authentication events and run actions
        // based on the login and logout events fired from the guard instances.
        if (isset($this->events))
        {
            $this->events->fire('backendauth.login', array($user, $remember));
        }

        $this->setUser($user);
    }

    /**
     * Log the user out of the application.
     *
     * @return void
     */
    public function logout()
    {
        $user = $this->user();

        // If we have an event dispatcher instance, we can fire off the logout event
        // so any further processing can be done. This allows the developer to be
        // listening for anytime a user signs out of this application manually.
        $this->clearUserDataFromStorage();

        if ( ! is_null($this->user))
        {
            $this->refreshRememberToken($user);
        }

        if (isset($this->events))
        {
            $this->events->fire('backendauth.logout', array($user));
        }

        // Once we have fired the logout event we will clear the users out of memory
        // so they are no longer available as the user is no longer considered as
        // being signed into this application and should not be available here.
        $this->user = null;

        $this->loggedOut = true;

    }

    /**
     * Fire the attempt event with the arguments.
     *
     * @param  array  $credentials
     * @param  bool   $remember
     * @param  bool   $login
     * @return void
     */
    protected function fireAttemptEvent(array $credentials, $remember, $login)
    {
        if ($this->events)
        {
            $payload = array($credentials, $remember, $login);
            $this->events->fire('backendauth.attempt', $payload);
        }
        parent::fireAttemptEvent($credentials, $remember, $login);
    }

    /**
     * Register an authentication attempt event listener.
     *
     * @param  mixed  $callback
     * @return void
     */
    public function attempting($callback)
    {
        if ($this->events)
        {
            $this->events->listen('backendauth.attempt', $callback);
        }
        parent::attempting($callback);
    }

    /**
     * Get a unique identifier for the auth session value.
     *
     * @return string
     */
    public function getName()
    {
        return 'backend_login_'.md5(get_class($this));
    }

    /**
     * Get the name of the cookie used to store the "recaller".
     *
     * @return string
     */
    public function getRecallerName()
    {
        return 'backend_remember_'.md5(get_class($this));
    }

}
