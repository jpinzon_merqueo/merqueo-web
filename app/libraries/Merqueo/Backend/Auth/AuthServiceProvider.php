<?php

namespace Merqueo\Backend\Auth;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('backendauth', function($app)
        {
            $app['backend.auth.loaded'] = true;
            return new AuthManager($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('backendauth');
    }

}
