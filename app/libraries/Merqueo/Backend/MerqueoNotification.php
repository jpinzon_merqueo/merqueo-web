<?php

namespace Merqueo\Backend;

use Leanplum\LeanplumResponse;
use Merqueo\Firebase\FirebaseCloudMessageResponse;

/**
 * Class MerqueoNotification
 * @package Merqueo\Backend
 */
class MerqueoNotification
{
    const ANDROID_CONSTRAINT = '2.1.10';

    const IOS_CONSTRAINT = '2.1.10';

    /**
     * @param SendNotification $notification
     * @param array $data
     * @param \Closure|null $closure
     * @return mixed
     */
    public function send(SendNotification $notification, array $data = [], \Closure $closure = null)
    {
        $response = $notification->send($data, $closure);
        if ($response instanceof FirebaseCloudMessageResponse) {
            return [
                'status' => $response->wasValid(),
                'message' => $response->getMessage(),
            ];
        }

        return $response;
    }

    /**
     * @param string $version
     * @param string $os
     * @param string|array $device_id
     * @param string $player_id
     * @return SendNotification
     * @todo Refactorizar
     */
    public static function getSender($version, $os, $device_id, $player_id)
    {
        $identifier = self::isNextVersionAvailable($version, $os) ? $player_id : $device_id;

        return self::isNextVersionAvailable($version, $os)
            ? new FirebaseNotification($identifier)
            : new OneSignalNotification($identifier);
    }

    /**
     * @param string $version Versión del app.
     * @param string $os Sistema operativo.
     * @return bool
     */
    protected static function isNextVersionAvailable($version, $os)
    {
        $os = strtolower($os);

        $valid_android_leanplum = $os === 'android' && version_compare($version, self::ANDROID_CONSTRAINT, '>');
        $valid_ios_leanplum = $os === 'ios' && version_compare($version, self::IOS_CONSTRAINT, '>');

        return $valid_android_leanplum || $valid_ios_leanplum;
    }
}
