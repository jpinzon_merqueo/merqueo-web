<?php

namespace Merqueo\Backend\Paginator;

use Illuminate\Pagination\PaginationServiceProvider as PaginatorProvider;
use Merqueo\Backend\Paginator\Factory;
use Merqueo\Backend\Paginator\Paginator;
use Illuminate\Support\ServiceProvider;

/**
 * Class PaginationServiceProvider
 * @package Merqueo\Backend\Paginator
 */
class PaginationServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('paginator', function($app)
        {
            $paginator = new Factory($app['request'], $app['view'], $app['translator']);

            $paginator->setViewName($app['config']['view.pagination']);

            $app->refresh('request', $paginator, 'setRequest');

            return $paginator;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('paginator');
    }
}