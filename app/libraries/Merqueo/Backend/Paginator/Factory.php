<?php

namespace Merqueo\Backend\Paginator;

use Illuminate\Pagination\Factory AS LaravelFactory;
use Illuminate\Pagination\Paginator AS Paginator;

class Factory extends LaravelFactory
{
    public function getCurrentUrl()
    {
        $url = $this->request->url();
        if (\Config::get('app.force_schema_url')) {
            $url = str_replace( 'http://', 'https://', $url );
        }
        return $this->baseUrl ?: $url;
    }
}