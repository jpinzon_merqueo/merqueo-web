<?php

namespace Merqueo\Facebook;

use exceptions\MerqueoException;
use Facebook\Facebook;

/**
 * Class MerqueoFacebook
 * @package Merqueo\Facebook
 */
class MerqueoFacebook extends Facebook
{
    /**
     * @param $facebookId
     * @param $email
     * @throws \Facebook\Exceptions\FacebookSDKException
     * @throws MerqueoException
     */
    public function validateFacebookId($facebookId, $email)
    {
        $response = $this->get("/{$facebookId}?fields=email");
        $user = $response->getGraphUser();

        if ($user->getEmail() !== $email) {
            throw new MerqueoException('El email del usuario no corresponde al FacebookId suministrado.');
        }
    }
}