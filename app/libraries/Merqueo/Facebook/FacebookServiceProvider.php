<?php

namespace Merqueo\Facebook;

use Illuminate\Support\ServiceProvider;

/**
 * Class FacebookServiceProvider
 * @package Merqueo\Facebook
 */
class FacebookServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MerqueoFacebook::class, function () {
            return new MerqueoFacebook([
                'app_id' => \Config::get('app.facebook_app_id'),
                'app_secret' => \Config::get('app.facebook_api_graph_secret'),
                'default_graph_version' => 'v3.0',
            ]);
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [MerqueoFacebook::class];
    }
}