<?php

namespace Merqueo\Facebook;

use Illuminate\Support\Facades\Facade;

/**
 * Class FacebookFacade
 * @package Merqueo\Facebook
 */
class FacebookFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return MerqueoFacebook::class;
    }
}