<?php

namespace Merqueo\Zendesk;

use GuzzleHttp\HandlerStack;
use Illuminate\Support\ServiceProvider;
use Zendesk\API\HttpClient;
use Zendesk\API\Utilities\Auth;

/**
 * Class ZendeskServiceProvider
 * @package Merqueo\Zendesk
 */
class ZendeskServiceProvider extends ServiceProvider
{
    /**
     * @var int
     */
    const TIMEOUT_CLIENT = 1;

    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var
     */
    private $guzzle;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HttpClient::class, function () {
            $username = \Config::get('zendesk.user');
            $subDomain = \Config::get('zendesk.subdomain');
            $token = \Config::get('zendesk.token');
            $client = new HttpClient($subDomain, '', 'https', 'zendesk.com', 443, $this->getGuzzleClient());
            $client->setAuth(Auth::BASIC, compact('token', 'username'));

            return $client;
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [HttpClient::class];
    }

    /**
     * Get the default guzzle configuration.
     */
    private function getGuzzleClient()
    {
        $handler = HandlerStack::create();
        $timeout = self::TIMEOUT_CLIENT;
        $this->guzzle = new \GuzzleHttp\Client(compact('handler', 'timeout'));

        return $this->guzzle;
    }
}
