<?php

namespace Merqueo\Zendesk;

use Illuminate\Support\Facades\Facade;
use Zendesk\API\HttpClient;

/**
 * Class ZendekFacade
 * @package Merqueo\Zendesk
 */
class ZendekFacade extends Facade
{
    /**
     * @return array|string
     */
    protected static function getFacadeAccessor()
    {
        return HttpClient::class;
    }
}
