<?php

namespace Merqueo\Leanplum;

use Guzzle\Http\Client;

/**
 * Class LeanplumClient
 * @package Merqueo\Leanplum
 */
class LeanplumClient extends \Leanplum\LeanplumClient
{
    /**
     * @return array
     */
    protected function validMethods()
    {
        $methods = parent::validMethods();

        return array_merge($methods, ['setUserAttributes']);
    }

    /**
     * @return Client|null
     */
    protected function getClient()
    {
        $this->client = new Client('', [
            'timeout' => 5,
            'connect_timeout' => 5,
        ]);

        return $this->client;
    }
}
