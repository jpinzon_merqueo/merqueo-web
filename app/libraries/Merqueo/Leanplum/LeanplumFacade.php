<?php

namespace Merqueo\Leanplum;

use \Illuminate\Support\Facades\Facade;

/**
 * Class LeanplumFacade
 */
class LeanplumFacade extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return LeanplumClientServiceProvider::SERVICE_PROVIDER_NAME;
    }
}
