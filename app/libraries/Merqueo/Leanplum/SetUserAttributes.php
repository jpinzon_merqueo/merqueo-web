<?php

namespace Merqueo\Leanplum;

use Leanplum\Message\Request\RequestAbstract;
use User;

/**
 * Class SetUserAttributes
 * @package Merqueo\Leanplum
 */
class SetUserAttributes extends RequestAbstract
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $forceParams;

    /**
     * SetUserAttributes constructor.
     * @param User $user
     * @param array $forceParams
     */
    public function __construct(User $user, array $forceParams = [])
    {
        $this->user = $user;
        $this->forceParams = $forceParams;
    }

    /**
     * @return array
     */
    public function format()
    {
        return [
            'action' => 'setUserAttributes',
            'userId' => $this->user->id,
            'userAttributes' => $this->getDirtyAttributes(),
        ];
    }

    /**
     * @return array
     */
    private function getDirtyAttributes()
    {
        $values = [];
        $dirty = $this->user->getDirty() + array_flip($this->forceParams);
        $hiddenAttributes = [
            'id',
            'identity_type',
            'identity_number',
            'phone_validated_date',
            'password',
            'referred_by',
            'total_referrals',
            'fb_id',
            'free_delivery_expiration_date',
            'free_delivery_next_order',
            'free_delivery_next_order_expiration_date',
            'customer_token',
            'latlng',
            'oauth_verifier',
            'business_name',
            'image_url',
            'first_coupon_used',
            'remember_token',
            'sift_account_abuse',
            'type',
            'created_at',
            'updated_at'
        ];

        foreach ($dirty as $name => $value) {
            if (in_array($name, $hiddenAttributes)) {
                continue;
            }

            $values[camel_case($name)] = $this->user->$name;
        }

        return $values;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return 'setUserAttributes';
    }
}
