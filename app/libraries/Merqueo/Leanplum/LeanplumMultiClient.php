<?php

namespace Merqueo\Leanplum;

use Leanplum\LeanplumResponse;

/**
 * Class LeanplumClient
 * @package Merqueo\Leanplum
 */
class LeanplumMultiClient extends LeanplumClient
{
    /**
     * @var int
     */
    protected $maxQueueSize = 50;

    /**
     * @var array
     */
    protected $content = [];

    /**
     * @var array
     */
    protected $responses = [];

    /**
     * @param $method
     * @param \Leanplum\Message\Request\RequestAbstract|array $arguments
     */
    public function __call($method, array $arguments)
    {
        $message = array_pop($arguments);
        if (!in_array($method, $this->validMethods())) {
            throw new \InvalidArgumentException("Invalid method");
        }

        $this->content[] = $message->format() + ['time' => time()];
    }

    /**
     * @return void
     */
    public function send()
    {
        $chunks = array_chunk($this->content, $this->maxQueueSize);
        foreach ($chunks as $chunk) {
            $this->responses[] = $this->buildHandler($chunk);
        }
    }

    /**
     * Elimina los items que se envian en la petición.
     */
    public function clear()
    {
        $this->content = [];
    }

    /**
     * @return bool
     */
    public function wasValid()
    {
        foreach ($this->responses as $response) {
            if (!$response->wasValid()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        $notValidResults = [];
        foreach ($this->responses as $response) {
            if (!$response->wasValid()) {
                $notValidResults[] = $response->getMessage();
            }
        }

        return $notValidResults;
    }

    /**
     * @param $data
     * @return LeanplumResponse|\Leanplum\Message\Response
     */
    private function buildHandler($data)
    {
        $uriParams = [
            'appId' => $this->appId,
            'action' => 'multi',
            'clientKey' => $this->clientKey,
            'apiVersion' => $this->apiVersion,
            'devMode' => $this->devMode,
            'time' => time(),
        ];

        $url = self::LEANPLUM_URL . http_build_query($uriParams);
        $request = $this->getClient()->post($url, ['Content-Type' => 'application/json']);

        $request->setBody(json_encode(compact('data')));
        return new LeanplumResponse($request->send());
    }
}
