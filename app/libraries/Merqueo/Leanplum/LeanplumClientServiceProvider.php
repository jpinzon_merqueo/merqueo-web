<?php

namespace Merqueo\Leanplum;

use Illuminate\Support\ServiceProvider;

/**
 * Class LeanplumClientServiceProvider
 */
class LeanplumClientServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @const Nombre de la clase principal.
     */
    const SERVICE_PROVIDER_NAME = \Merqueo\Leanplum\LeanplumClient::class;

    /**
     * Register Leanplum client service provider.
     */
    public function register()
    {
        foreach ($this->provides() as $provider) {
            $this->app->singleton($provider, function ($app) use ($provider) {
                return $this->initLeanplumclient($provider);
            });
        }
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            \Merqueo\Leanplum\LeanplumClient::class,
            \Merqueo\Leanplum\LeanplumMultiClient::class,
        ];
    }

    /**
     * @param $classType
     * @return LeanplumClient
     */
    private function initLeanplumclient($classType)
    {
        $leanplumClient = new $classType();
        $leanplumClient->setAppId(\Config::get('leanplum.app_id'))
            ->setClientKey(\Config::get('leanplum.key'))
            ->setDevMode(!empty(\Config::get('leanplum.dev_mode')));

        return $leanplumClient;
    }
}
