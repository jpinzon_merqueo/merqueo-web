<?php

namespace Merqueo\Firebase;

use GuzzleHttp\Psr7\Response;

/**
 * Class FirebaseCloudMessageResponse
 * @package Merqueo\Firebase
 */
class FirebaseCloudMessageResponse
{
    /**
     * @var Response
     */
    protected $response;

    /**
     * @var \StdClass
     */
    protected $jsonResponse;

    /**
     * FirebaseCloudMessageResponse constructor.
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return bool
     */
    public function wasValid()
    {
        $response = $this->getJsonResponse();

        return $response->failure === 0;
    }

    /**
     * @return string|null
     */
    public function getMessage()
    {
        $response = $this->getJsonResponse();
        foreach ($response->results as $result) {
            if (!empty($result->error)) {
                return $result->error;
            }
        }
    }

    /***
     * @return mixed|\StdClass
     */
    protected function getJsonResponse()
    {
        if (!empty($this->jsonResponse)) {
            return $this->jsonResponse;
        }

        $this->jsonResponse = json_decode((string) $this->response->getBody());

        return $this->jsonResponse;
    }
}
