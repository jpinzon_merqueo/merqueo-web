<?php

namespace Merqueo\Firebase;

use \Illuminate\Support\Facades\Facade;

/**
 * Class FirebaseCloudMessageFacade
 */
class FirebaseCloudMessageFacade extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return FirebaseCloudMessageClientServiceProvider::SERVICE_PROVIDER_NAME;
    }
}
