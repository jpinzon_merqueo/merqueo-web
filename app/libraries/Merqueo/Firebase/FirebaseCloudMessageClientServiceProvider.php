<?php

namespace Merqueo\Firebase;

use Illuminate\Support\ServiceProvider;
use sngrl\PhpFirebaseCloudMessaging\Client;

class FirebaseCloudMessageClientServiceProvider extends ServiceProvider
{
    const SERVICE_PROVIDER_NAME = 'firebasecloudmessage';

    /**
     * @var bool
     */
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(self::SERVICE_PROVIDER_NAME, function () {
            $client = new Client();
            $client->setApiKey(\Config::get('app.firebase.general.push_key'));
            $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

            return $client;
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [self::SERVICE_PROVIDER_NAME];
    }
}
