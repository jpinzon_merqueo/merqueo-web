<?php

namespace AbTesting\Merqueo;

/**
 * Interface EventTrack
 * @package AbTesting\Merqueo
 */
interface EventTrack
{
    /**
     * @return mixed
     */
    public function track();
}
