<?php

namespace AbTesting\Merqueo;

abstract class AbTestingProvider
{
    /**
     * @var \User
     */
    protected $user;

    /**
     * @var string
     */
    protected $experiment_name;

    /**
     * @var string
     */
    protected $variant;

    /**
     * @var string
     */
    protected $testing_provider;

    public function __construct(\User $user)
    {
        $this->user = $user;
    }

    /**
     * @param mixed $testing_provider
     */
    public function setTestingProvider($testing_provider)
    {
        $this->testing_provider = $testing_provider;
    }

    /**
     * @return mixed
     */
    abstract public function activate();

    /**
     * @param $conditional
     * @param $valid_variant
     * @return mixed
     */
    abstract public function showVariant($conditional, $valid_variant);

    /**
     * @return string
     */
    public function getExperimentName()
    {
        return $this->experiment_name;
    }

    /**
     * @return string
     */
    public function getVariant()
    {
        return $this->variant;
    }
}
