<?php

namespace AbTesting\Optimizely;

use Illuminate\Support\ServiceProvider;
use Optimizely\Optimizely;

/**
 * Class OptimizelyServiceProvider
 * @package AbTesting\Optimizely
 */
class OptimizelyServiceProvider extends ServiceProvider
{
    /**
     * Register Optimizely client service provider.
     */
    public function register()
    {
        $this->app->singleton(Optimizely::class, function ($app) {
            $json = \Config::get('optimizely.file_content');

            if (empty($json)) {
                $json = file_get_contents(\Config::get('optimizely.file_url'));
            }

            return new Optimizely($json);
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [Optimizely::class];
    }
}
