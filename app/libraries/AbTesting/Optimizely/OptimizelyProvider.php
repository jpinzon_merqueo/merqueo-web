<?php

namespace AbTesting\Merqueo;

/**
 * Class Optimizely
 * @package AbTesting\Merqueo
 */
abstract class OptimizelyProvider extends AbTestingProvider
{
    protected $testing_provider = \Optimizely::class;

    /**
     * @return OptimizelyProvider
     */
    public function activate()
    {
        $this->variant = \Optimizely::activate(
            $this->experiment_name,
            $this->user->id,
            $this->user->getAnalyticsBasicProperties() ?: []
        );

        return $this;
    }
}
