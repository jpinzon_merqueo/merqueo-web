<?php

namespace AbTesting\Optimizely\Experiments\Events;

use AbTesting\Optimizely\OptimizelyEvent;

/**
 * Class ProductAdded
 * @package AbTesting\Optimizely\Experiments\Events
 */
class ProductAdded extends OptimizelyEvent
{
    protected $name = 'product-added';
}
