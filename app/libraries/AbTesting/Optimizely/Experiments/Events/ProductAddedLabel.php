<?php

namespace AbTesting\Optimizely\Experiments\Events;

use AbTesting\Optimizely\OptimizelyEvent;

/**
 * Class ProductAddedLabel
 * @package AbTesting\Optimizely\Experiments\Events
 */
class ProductAddedLabel extends OptimizelyEvent
{
    protected $name = 'product-added-label';
}
