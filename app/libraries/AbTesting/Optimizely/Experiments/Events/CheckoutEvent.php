<?php

namespace AbTesting\Optimizely\Experiments\Events;

use AbTesting\Optimizely\OptimizelyEvent;

/**
 * Class CheckoutEvent
 * @package AbTesting\Optimizely\Experiments\Events
 */
class CheckoutEvent extends OptimizelyEvent
{
    protected $name = 'checkout';
}
