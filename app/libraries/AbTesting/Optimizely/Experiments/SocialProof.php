<?php

namespace AbTesting\Optimizely\Experiments;

use AbTesting\Merqueo\OptimizelyProvider;

/**
 * Class ProductAdded
 * @package AbTesting\Optimizely\Experiments
 */
class SocialProof extends OptimizelyProvider
{
    protected $experiment_name = 'social-proof';

    /**
     * @param $provider_id
     * @param string $variant
     * @return bool
     */
    public function showVariant($provider_id, $variant = 'treatment')
    {
        // Departamentos Ciudado personal, tecnologíam Licores, Mascotas en Bogotá y Medellin
        return $this->getVariant() === $variant && in_array($provider_id, [108, 102, 26, 82, 37, 83, 18, 74]);
    }

    /**
     * Pone dentro de un rango las unidades compradas por un usuario.
     *
     * @param $value
     * @return float|int
     */
    public static function showOnRange($value)
    {
        $factor = 50;

        if ($value < 10000 && $value > 999) {
            $factor = 1000;
        } elseif ($value < 100000 && $value > 999) {
            $factor = 10000;
        }

        // Se agrega "1" para evitar que se muestren ceros.
        return ceil(($value + 1) / $factor) * $factor;
    }
}
