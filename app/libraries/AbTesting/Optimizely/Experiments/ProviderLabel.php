<?php

namespace AbTesting\Optimizely\Experiments;

use AbTesting\Merqueo\OptimizelyProvider;

/**
 * Class ProviderLabel
 * @package AbTesting\Optimizely\Experiments
 */
class ProviderLabel extends OptimizelyProvider
{
    protected $experiment_name = 'provider-labels';

    /**
     * @param $provider_id
     * @param string $variant
     * @return bool
     */
    public function showVariant($product, $variant = 'treatment')
    {
        if ($product instanceof \Product) {
            return $product->provider_labels_additional_params && $this->getVariant() === $variant;
        }

        return !empty($product['provider_labels_additional_params']) && $this->getVariant() === $variant;
    }
}
