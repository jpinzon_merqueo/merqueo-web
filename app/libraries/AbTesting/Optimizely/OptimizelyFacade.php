<?php

namespace AbTesting\Optimizely;

use Illuminate\Support\Facades\Facade;
use Optimizely\Optimizely;

/**
 * Class OptimizelyFacade
 * @package AbTesting\Optimizely
 */
class OptimizelyFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Optimizely::class;
    }
}
