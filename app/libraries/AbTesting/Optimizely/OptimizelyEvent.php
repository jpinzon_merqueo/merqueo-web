<?php

namespace AbTesting\Optimizely;

use AbTesting\Merqueo\EventTrack;

/**
 * Class OptimizelyEvent
 * @package AbTesting\Optimizely
 */
class OptimizelyEvent implements EventTrack
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var \User
     */
    protected $user;

    /**
     * OptimizelyEvent constructor.
     * @param \User $user
     */
    public function __construct(\User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed|void
     */
    public function track()
    {
        \Optimizely::track($this->name, $this->user->id);
    }
}
