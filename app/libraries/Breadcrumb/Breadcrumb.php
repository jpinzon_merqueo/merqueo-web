<?php

	namespace Breadcrumb;

	use Illuminate\Http\Request;

	class Breadcrumb{

		protected $request;

		public function __construct(Request $request){
			$this->request = $request;
		}

		// creacion de miga
		// Recibe $separador => signo para separación, $home => nombre de url inicial, $init => numero de posición inicial

		public function render($separador = '>', $home = 'Home', $init = '0', $city = ''){

			$htmlbrecrumbs = '';
			$contructorUrl = Array();
			
			//Restar una posicion

			if($init != 0){
				$init = $init - 1;
			}
			
			//Indicio de camino
			
			$path = $this->request->path();

			//raiz del sitio
			
			$raiz = $this->request->root();

			//Parse url
			
			$seguidor = explode("\n", $path);

			foreach ($seguidor as $segmento) {
				$segmento_parte = explode("/", $segmento);
			}

			// loop del contructor
			
			for ($i=0; $i < sizeof($segmento_parte) ; $i++) {
				$contructorUrlobj = Array();
			
				if (is_numeric(ucfirst($segmento_parte[$i]))){
					$keyarray_end = array_keys($contructorUrl);
					$keyarray = end($keyarray_end);
					$contructorUrl[$keyarray]['url'] = $raiz.='/'.$segmento_parte[$i];
				}else{
					$contructorUrlobj['url'] = $raiz.='/'.$segmento_parte[$i];
					if($segmento_parte[$i] == 'domicilios-super-merqueo'){
						$segmento_parte[$i] = 'domicilios-merqueo-super';
					}
					if($segmento_parte[$i] != 'embedded'){
						$array_string = array('_', '%20');
						$contructorUrlobj['name'] = str_replace($array_string, ' ', $segmento_parte[$i]);
						//var_dump($contructorUrl);
						$contructorUrlobj['name'] = urldecode($contructorUrlobj['name']); 
						$contructorUrl[] = $contructorUrlobj;
					}
				}

			}

			if (count($contructorUrl) == 0){
					$htmlbrecrumbs = '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="'.$this->request->root().'"><span itemprop="name">'.$home.'</span></a><meta itemprop="position" content="1" /><li>';
			}else{
				$countitem = 1;
				for ($i=$init; $i < sizeof($contructorUrl); $i++) {
					$nameact = str_replace('-', ' ', $contructorUrl[$i]['name']);
					$nameobj = explode(' ', $nameact);
					$namestring = '';
					$contini = 0;
					$contlast = count($nameobj);
					foreach ($nameobj as $valuename) {
						if(strlen($valuename) > 3){
							$valuenametemp = ucwords($valuename);
							if ($contini == 0){
						        $namestring = $valuenametemp;
						    }else{
						    	$namestring = $namestring." ". $valuenametemp;
						    }
							
						}else{
							$namestring = $namestring." ". $valuename;
						}
						$contini ++;
					}
					if ($init == (sizeof($contructorUrl)-1)){
						$htmlbrecrumbs = '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="last_breadcrumb active">'.$namestring.' en '.$city.'</li>';
					}else{
						if ($countitem == 1){
							$htmlbrecrumbs = '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="first_breadcrumb"><a itemprop="item" href="'.$contructorUrl[$i]['url'].'"><span itemprop="name">'.$namestring.'<span></a><meta itemprop="position" content="'.$countitem.'" /></li>';
						}else{
							
							if($i == ( sizeof($contructorUrl) -1 )){
								$htmlbrecrumbs = $htmlbrecrumbs.'<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="last_breadcrumb active"><span itemprop="name">'.$namestring.'</span><meta itemprop="position" content="'.$countitem.'" /></li>';
							}else{
								if(ucwords($contructorUrl[$i]['name']) != 'Marca'){
									$htmlbrecrumbs = $htmlbrecrumbs.'<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="'.$contructorUrl[$i]['url'].'"><span itemprop="name">'.$namestring.'<span></a><meta itemprop="position" content="'.$countitem.'" /></li>';
								}else{
									$htmlbrecrumbs = $htmlbrecrumbs.'<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="'.$contructorUrl[$i]['url'].'"><span itemprop="name">'.$namestring.'<span></a><meta itemprop="position" content="'.$countitem.'" /></li>';
								}
								
							}
							
						}
					}
					$countitem++;
				}
				
			}

			return $htmlbrecrumbs;
			
		}
	}