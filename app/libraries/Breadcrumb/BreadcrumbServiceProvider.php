<?php
	namespace Breadcrumb;

	use Illuminate\Support\ServiceProvider;

	class BreadcrumbServiceProvider extends ServiceProvider{

		public function register(){
			$this->app['Breadcrumb'] = $this->app->share(function($app){
				return new Breadcrumb($app['request']);
			});
		}
	}