<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/404', function () {
    return App::abort(404);
});

$admin_route = admin_url();

//rutas de apps internas (Picking y Transportador)
include 'routes/internal_apps.php';

//rutas de administrador
include 'routes/dashboard.php';

//rutas de producto (APIs y Web)
include 'routes/product.php';
//}
