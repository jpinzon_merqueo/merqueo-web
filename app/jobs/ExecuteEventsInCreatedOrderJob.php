<?php


namespace jobs;

use Event;
use Illuminate\Queue\Jobs\Job;
use OrderEventHandler;

/**
 * Class CreateOrderJob
 * @package jobs
 */
class ExecuteEventsInCreatedOrderJob
{

    /**
     * @param Job $job
     * @param array $data
     */
    public function fire(Job $job, array $data)
    {

        $order = \Order::find($data['order_id']);

        Event::subscribe(new OrderEventHandler);
        Event::fire('order.created', [[
            $order,
            $data['order_data']
        ]]);

        \Log::info("Evendo order created disparado", [
            'class' => __CLASS__,
            'method' => __METHOD__,
            'data' => $data,
        ]);

        $job->delete();
    }
}