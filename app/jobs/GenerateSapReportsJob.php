<?php

namespace jobs;

use actions\QuerysForServicesAndTemplatesSAPAction;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;
use PHPExcel_Writer_Excel2007;

class GenerateSapReportsJob
{
    private $query;

    public function __construct()
    {
        $this->query = new QuerysForServicesAndTemplatesSAPAction();
    }

    public function fire($job, $data)
	{
        $template = $data['template'];
        $start_date = $data['start_date'] . ' 00:00:00';
        $end_date = $data['end_date'] . ' 23:59:59';
        $mail = $data['mail'];

		switch ($template) {
            case 'clients':
                $this->export_clients($mail, $start_date, $end_date);
                break;

            case 'providers':
                $this->export_providers($mail, $start_date, $end_date);
                break;

            case 'retention-providers':
                $this->retention_providers($mail, $start_date, $end_date);
                break;

            case 'products':
                $this->export_products($mail, $start_date, $end_date);
                break;

            case 'purchase-invoices':
                $this->purchase_invoices($mail, $start_date, $end_date);
                break;

            case 'sale-invoices':
                $this->sale_invoices($mail, $start_date, $end_date);
                break;

            case 'credit-note-sale':
                $this->credit_note_sale($mail, $start_date, $end_date);
                break;

            case 'credit-note-purchase':
                $this->credit_note_purchase($mail, $start_date, $end_date);
                break;

            case 'inventory-entry':
                $this->inventory_entry($mail, $start_date, $end_date);
                break;

            case 'inventory-output':
                $this->inventory_output($mail, $start_date, $end_date);
                break;

            case 'payments-received':
                $this->payments_received($mail, $start_date, $end_date);
                break;

            default:
                echo "¡Elegiste una opción no valida, intenta de nuevo!";
                break;
        }

        $job->delete();
	}

    public function saveTemplateAndSendMail($filename, $mail) {
        $url = upload_file_s3("sap-reports/{$filename}.xlsx", '/tmp/' . $filename . '.xlsx');

        $sendMail = [
            'template_name' => 'emails.email_sap_report',
            'to' => [[
                'name' => $mail,
                'email' => $mail
            ]],
            'subject' => 'Link Descarga Plantilla SAP',
            'vars' => [
                'url' => $url
            ]
        ];

        send_mail($sendMail, false, true, $mail);
    }

	/**
     * Obtiene los clientes que piden facturas
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function export_clients($mail, $start_date, $end_date)
    {
        $filename = 'Clientes' . date('Y-m-d H.i.s', time());
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $clients = $this->query->getClients($data['start_date'], $data['end_date']);

            $excel->sheet('Clientes', function ($sheet) use (&$clients) {
                $titles = [
                    'CardCode',
                    'CardName',
                    'CardType',
                    'GroupCode',
                    'DebitorAccount',
                    'Cellular',
                    'CreditLimit',
                    'Currency',
                    'EmailAddress',
                    'FederalTaxID',
                    'PayTermsGrpCode',
                    'Phone1',
                    'Phone2',
                    'Properties1',
                    'Properties2',
                    'Properties3',
                    'WTLiable',
                    'U_BPCO_City',
                    'U_BPCO_CS',
                    'U_BPCO_Nombre',
                    'U_BPCO_1Apellido',
                    'U_BPCO_2Apellido',
                    'U_BPCO_BPExt',
                    'U_BPCO_TP',
                    'U_BPCO_RTC',
                    'U_BPCO_TDC',
                    'U_BPCO_Address',
                    'Attachment',
                    'CodPersonContact',
                    'FirstName',
                    'LastName',
                    'Cellolar'
                ];

                $sheet->appendRow($titles);
                foreach ($clients as $client) {
                    $sheet->appendRow($client);
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene proveedores
     *
     *
     * @param $mail@param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function export_providers($mail, $start_date, $end_date)
    {
        $filename = 'Proveedores' . date('Y-m-d H.i.s', time());
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $providers = $this->query->getProviders($data['start_date'], $data['end_date']);

            $excel->sheet('Proveedores', function ($sheet) use ($providers) {

                $titles = [
                    'CardCode',
                    'CardName',
                    'CardType',
                    'GroupCode',
                    'DebitorAccount',
                    'Cellular',
                    'CreditLimit',
                    'Currency',
                    'EmailAddress',
                    'FederalTaxID',
                    'PayTermsGrpCode',
                    'Phone1',
                    'Phone2',
                    'Properties1',
                    'Properties2',
                    'Properties3',
                    'WTLiable',
                    'U_BPCO_City',
                    'U_BPCO_CS',
                    'U_BPCO_Nombre',
                    'U_BPCO_1Apellido',
                    'U_BPCO_2Apellido',
                    'U_BPCO_BPExt',
                    'U_BPCO_TP',
                    'U_BPCO_RTC',
                    'U_BPCO_TDC',
                    'U_BPCO_Address',
                    'Attachment',
                    'CodPersonContact',
                    'FirstName',
                    'LastName',
                    'Cellolar'
                ];

                $sheet->appendRow($titles);
                foreach ($providers as $provider) {
                    $sheet->appendRow($provider);
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene las retenciones aplicadas a los proveedores
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function retention_providers($mail, $start_date, $end_date)
    {
        $filename = 'Retenciones de Proveedores - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $providers = $this->query->getRetentionProviders($data['start_date'], $data['end_date']);

            $excel->sheet('Retenciones de Proveedores', function ($sheet) use ($providers) {
                $titles = [
                    'CardCode',
                    'WTCode'
                ];

                $sheet->appendRow($titles);
                foreach ($providers as $provider) {
                    foreach ($provider['retentionsList'] as $retention) {
                        $array_data = ['CardCode' => $provider['CardCode'], 'WTCode' => $retention['WTCode']];
                        $sheet->appendRow($array_data);
                    }
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     *  Obtiene productos
     *
     * @param $start_date fecha de inicio creación del producto
     * @param $end_date fecha de fin de creación del producto
     * @return \Illuminate\Http\RedirectResponse
     */
    private function export_products($mail, $start_date, $end_date)
    {
        $filename = 'Productos - ' . date('Y-m-d H.i.s', time());
        $data = ['start_date' => $start_date, 'end_date' => $end_date];
        $file = Excel::create($filename, function ($excel) use ($data) {
            $products = $this->query->getProducts($data['start_date'], $data['end_date']);

            $excel->sheet('Productos', function ($sheet) use (&$products) {
                $titles = [
                    'ItemCode',
                    'ItemName',
                    'ItemsGroupCode',
                    'BarCode',
                    'PurchaseItem',
                    'SalesItem',
                    'InventoryItem',
                    'VatLiable',
                    'IndirectTax',
                    'TaxCodeAP',
                    'TaxCodeAR',
                    'GLMethod',
                    'InventoryUOM',
                    'WhsCode'
                ];

                $sheet->appendRow($titles);
                foreach ($products as $product) {
                    $sheet->appendRow($product);
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene facturas de compra
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function purchase_invoices($mail, $start_date, $end_date)
    {
        $filename = 'Facturas de Compra - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $purchase_invoices = $this->query->getPurchaseInvoices($data['start_date'], $data['end_date']);

            $excel->sheet('Facturas de Compra', function ($sheet) use ($purchase_invoices) {

                $titles = [
                    'DocNum',
                    'CardCode',
                    'DocDate',
                    'TaxDate',
                    'DocDueDate',
                    'NumAtCard',
                    'DocCur',
                    'LineNum',
                    'ItemCode',
                    'Quantity',
                    'WarehouseCode',
                    'Price',
                    'TaxCode',
                    'WTLiable',
                    'CostingCode',
                    'WtCode',
                    'Tarifa',
                    'Valor',
                    'Base'
                ];
                $sheet->appendRow($titles);

                foreach ($purchase_invoices as $purchase_invoice) {
                    $array_data = [
                        'DocNum' => $purchase_invoice['DocNum'],
                        'CardCode' => $purchase_invoice['CardCode'],
                        'DocDate' => $purchase_invoice['DocDate'],
                        'TaxDate' => $purchase_invoice['TaxDate'],
                        'DocDueDate' => $purchase_invoice['DocDueDate'],
                        'NumAtCard' => $purchase_invoice['NumAtCard'],
                        'DocCur' => $purchase_invoice['DocCur'],
                        'LineNum' => $purchase_invoice['LineNum'],
                        'ItemCode' => $purchase_invoice['ItemCode'],
                        'Quantity' => $purchase_invoice['Quantity'],
                        'WarehouseCode' => $purchase_invoice['WarehouseCode'],
                        'Price' => $purchase_invoice['Price'],
                        'TaxCode' => $purchase_invoice['TaxCode'],
                        'WTLiable' => $purchase_invoice['WTLiable'],
                        'CostingCode' => $purchase_invoice['CostingCode']
                    ];

                    if(!empty($purchase_invoice['retentionsList'])) {
                        foreach ($purchase_invoice['retentionsList'] as $retention) {
                            $array_data = array_merge($array_data, [
                                'WtCode' => $retention['WtCode'],
                                'Tarifa' => $retention['Tarifa'],
                                'Valor' => $retention['Valor'],
                                'Base' => $retention['Base']
                            ]);
                            $sheet->appendRow($array_data);
                        }
                    } else {
                        $sheet->appendRow($array_data);
                    }
                }

            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene facturas de venta
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function sale_invoices($mail, $start_date, $end_date)
    {
        $filename = 'Facturas de Venta - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $saleInvoices = $this->query->getSaleInvoices($data['start_date'], $data['end_date']);

            $excel->sheet('Facturas de Venta', function ($sheet) use (&$saleInvoices) {

                $titles = [
                    'DocNum',
                    'CardCode',
                    'DocDate',
                    'TaxDate',
                    'DocDueDate',
                    'NumAtCard',
                    'DocCur',
                    'ExpensesCode',
                    'DistributionRule',
                    'CodeDescuento',
                    'TotalDescuento',
                    'CodeDomicilio',
                    'TotalDomicilio',
                    'LineNum',
                    'ItemCode',
                    'Quantity',
                    'WarehouseCode',
                    'Price',
                    'TaxCode',
                    'WTLiable',
                    'CostingCode',
                    'WtCode',
                    'Tarifa',
                    'Base'
                ];
                $sheet->appendRow($titles);
                foreach ($saleInvoices as $saleInvoice) {
                    $sheet->appendRow($saleInvoice);
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene notas crédito de venta
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function credit_note_sale($mail, $start_date, $end_date)
    {
        $filename = 'Notas Crédito Venta - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $credit_note_sales = $this->query->getCreditNoteSale($data['start_date'], $data['end_date']);

            $excel->sheet('Notas Crédito Venta', function ($sheet) use ($credit_note_sales) {

                $titles = [
                    'DocNum',
                    'NumFact',
                    'DocDate',
                    'TaxDate',
                    'DocDueDate',
                    'DocCur',
                    'ExpensesCode',
                    'DistributionRule',
                    'TipoNota',
                    'LineNum',
                    'ItemCode',
                    'Quantity',
                    'WarehouseCode',
                    'Price',
                    'TaxCode',
                    'WTLiable',
                    'CostingCode',
                    'WtCode',
                    'Tarifa',
                    'Base'
                ];
                $sheet->appendRow($titles);
                foreach ($credit_note_sales as $credit_note_sale) {
                    $sheet->appendRow($credit_note_sale);
                }

            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene notas crédito de compra
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function credit_note_purchase($mail, $start_date, $end_date)
    {
        $filename = 'Notas Crédito Compra - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $credit_note_purchase = $this->query->getCreditNotePurchase($data['start_date'], $data['end_date']);

            $excel->sheet('Notas Crédito Compra', function ($sheet) use ($credit_note_purchase) {

                $titles = [
                    'DocNum',
                    'NumFact',
                    'DocDate',
                    'TaxDate',
                    'DocDueDate',
                    'DocCur',
                    'TipoNota',
                    'LineNum',
                    'ItemCode',
                    'Quantity',
                    'WarehouseCode',
                    'Price',
                    'TaxCode',
                    'WTLiable',
                    'CostingCode',
                    'WtCode',
                    'Tarifa',
                    'Base',
                ];
                $sheet->appendRow($titles);
                foreach ($credit_note_purchase as $credit_note) {
                    $sheet->appendRow($credit_note);
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene entradas de inventario
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function inventory_entry($mail, $start_date, $end_date)
    {
        $filename = 'Entradas de Inventario - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $inventory_entry = $this->query->getInventoryEntry($data['start_date'], $data['end_date']);

            $excel->sheet('Entradas de Inventario', function ($sheet) use (&$inventory_entry) {

                $titles = [
                    'DocNum',
                    'DocDate',
                    'TaxDate',
                    'LineNum',
                    'ItemCode',
                    'Quantity',
                    'WarehouseCode',
                    'Price'
                ];

                $sheet->appendRow($titles);

                foreach ($inventory_entry as $entry) {
                    $sheet->appendRow($entry);
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene salidas de inventario
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function inventory_output($mail, $start_date, $end_date)
    {
        $filename = 'Salidas de inventario - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {

            $inventory_output = $this->query->getInventoryOutput($data['start_date'], $data['end_date']);

            $excel->sheet('Facturas de Venta', function ($sheet) use ($inventory_output) {

                $titles = [
                    'DocNum',
                    'DocDate',
                    'TaxDate',
                    'LineNum',
                    'ItemCode',
                    'Quantity',
                    'WarehouseCode',
                    'AccountCode'
                ];
                $sheet->appendRow($titles);
                foreach ($inventory_output as $output) {
                    $sheet->appendRow($output);
                }

            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }

    /**
     * Obtiene pagos recibidos
     *
     * @param $mail
     * @param $start_date
     * @param $end_date
     * @return \Illuminate\Http\RedirectResponse
     */
    private function payments_received($mail, $start_date, $end_date)
    {
        $filename = 'Pagos Recibidos - ' . date('Y-m-d H:i:s');
        $data = ['start_date' => $start_date, 'end_date' => $end_date];

        $file = Excel::create($filename, function ($excel) use ($data) {
            $payments_received = $this->query->getPaymentsReceived($data['start_date'], $data['end_date']);

            $excel->sheet('Pagos recibidos', function ($sheet) use (&$payments_received) {

                $titles = [
                    'DocNum',
                    'CardCode',
                    'DocDate',
                    'DocDueDate',
                    'TaxDate',
                    'CounterRef',
                    'SumApplied',
                    'MediodePago',
                    'Account',
                    'TransferDate',
                    'Remarks',
                    'Reference1',
                    'Reference2',
                    'CreditCardNumber',
                    'CardValidUntil'
                ];
                $sheet->appendRow($titles);
                foreach ($payments_received as $payment_received) {
                    $array_data = [
                        'DocNum' => $payment_received['DocNum'],
                        'CardCode' => $payment_received['CardCode'],
                        'DocDate' => $payment_received['DocDate'],
                        'DocDueDate' => $payment_received['DocDueDate'],
                        'TaxDate' => $payment_received['TaxDate'],
                        'CounterRef' => $payment_received['CounterRef']
                    ];
                    foreach ($payment_received['PaymentsList'] as $payment) {
                        $array_data = array_merge($array_data, [
                            'SumApplied' => $payment['SumApplied'],
                            'MediodePago' => $payment['MediodePago'],
                            'Account' => $payment['Account'],
                            'TransferDate' => $payment['TransferDate'],
                            'Remarks' => $payment['Remarks'],
                            'Reference1' => $payment['Reference1'],
                            'Reference2' => $payment['Reference2'],
                            'CreditCardNumber' => $payment['CreditCardNumber'],
                            'CardValidUntil' => $payment['CardValidUntil']
                        ]);
                        $sheet->appendRow($array_data);
                    }
                }
            });
        })->store('xlsx','/tmp/');

        $this->saveTemplateAndSendMail($filename, $mail);
    }
}
