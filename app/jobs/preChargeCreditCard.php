<?php

namespace jobs;
use Carbon\Carbon;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use OrderGroup;
use OrderLog;
use OrderPaymentRefund;
use PayU;
use Illuminate\Support\Facades\Event;

/**
 * Class preChargeCreditCard
 * @package jobs
 * @author Leyniker Rivera <rrivera@merqueo.com>
 */
class preChargeCreditCard
{
    /**
     * @param Job $job
     * @param array $data
     */
    public function fire(Job $job, array $data)
    {
        $order = \Order::find($data['orderId']);
        $orderPayments = \OrderPayment::where('order_id', $data['orderId'])->first();

        if ($orderPayments && !empty($orderPayments->cc_charge_id)) {
            $admin_id = Config::get('app.dashboard_admin_id');
            $orderPaymentRefunds = new OrderPaymentRefund;

            //Payu
            $payu = new PayU;
            $result = $payu->refundCreditCardCharge('Reembolso de $' . Config::get('app.payu.minimum_charge') . ' por precobro', $order, $orderPayments, true);

            // Firing the log event
            $admin_id = Config::get('app.dashboard_admin_id');
            $log = new OrderLog();
            $log->type = 'Se envio la solicitud de devolución de pre cobro.';
            $log->order_id = $data['orderId'];
            $log->admin_id = $admin_id;

            if ($result['status']) {
                if (isset($result['response']->result->payload->status) && $result['response']->result->payload->status == 'CANCELLED') {
                    $orderPaymentRefunds->status = 'Aprobada';
                    $log->type = 'Reembolso de tarjeta realizado con éxito.';
                } else {
                    $orderPaymentRefunds->status = 'Pendiente';
                }
            }else{
                $log->type = 'Reembolso a tarjeta rechazado.';
            }
            $log->save();

            $orderPaymentRefunds->order_payment_id = $orderPayments->id;
            $orderPaymentRefunds->date = date('Y-m-d H:i:s');
            $orderPaymentRefunds->admin_id = $admin_id;
            $orderPaymentRefunds->reason = 'Reembolso por precobro.';
            $orderPaymentRefunds->save();

            Log::info('Se realizó el reembolso del pedido '.$order->id.' con estado '.$orderPaymentRefunds->status);

            $job->delete();
        }
    }
}