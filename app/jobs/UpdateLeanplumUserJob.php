<?php

use Illuminate\Queue\Jobs\Job;
use Merqueo\Leanplum\LeanplumClient;

/**
 * Class UpdateLeanplumUser
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class UpdateLeanplumUserJob
{
    /**
     * @var LeanplumClient
     */
    private $client;

    /**
     * UpdateLeanplumUser constructor.
     * @param LeanplumClient $client
     */
    public function __construct(LeanplumClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param  Job     $job
     * @param  $data
     * @return void
     */
    public function fire(Job $job, $data)
    {
        $user = User::find($data['user_id']);

        if (empty($user)) {
            $job->delete();

            return null;
        }

        $action = new UpdateLeanplumUser($user, $this->client);

        try { $action->updateUser();} catch (\Exception $exception) {}

        $job->delete();
    }
}
