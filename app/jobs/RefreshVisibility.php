<?php

namespace jobs;

use Illuminate\Queue\Jobs\Job;
use StoreProductWarehouse;

/**
 * Class RefreshStock
 * @package jobs
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class RefreshVisibility
{
    /**
     * @param Job $job
     * @param array $data
     */
    public function fire(Job $job, array $data)
    {
        $storeProductWarehouse = StoreProductWarehouse::find($data['id']);
        StoreProductWarehouse::validateStockStatus($storeProductWarehouse);

        $job->delete();
    }
}
