<?php

namespace jobs;

use Illuminate\Support\Facades\Log;
use OrderPaymentLog;

/**
 * Class SaveOrderPaymentLogJob
 * @package jobs
 */
class SaveOrderPaymentLogJob
{
    /**
     * @var $data
     */
    protected $data;

    /**
     * @param $job
     * @param $data
     * @return int
     */
    public function fire($job, $data)
    {
        Log::error('paso por el job ', $data);
        try {
            $data  = $data['data'];
            $orderPaymentLogModel = new OrderPaymentLog();

            foreach ($data as $key => $datum) {
                $orderPaymentLogModel->{$key} = $datum;
            }

            $orderPaymentLogModel->save();

            $job->delete();
        } catch (\Exception $e) {
            $job->delete();
        }
    }
}