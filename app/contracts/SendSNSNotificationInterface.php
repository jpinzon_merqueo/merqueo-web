<?php

namespace contracts;

/**
 * SendSNSNotificationInterface
 */
interface SendSNSNotificationInterface
{
    /**
     * Publish a message in Aws SNS 
     * @param array $body
     * @param string $topicArn
     */
    public function sendMessage(array $body, $topicArn);

}