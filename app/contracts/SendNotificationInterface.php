<?php


namespace contracts;


interface SendNotificationInterface
{

    public function sms($message, $to);

    public function push($orderGroup, $data);

    public function mail($mail, $attachments = false, $throw_exception = false, $from_email = null);

}