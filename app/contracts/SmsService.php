<?php
namespace contracts;

/**
 * Interface SmsService.
 *
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
interface SmsService
{

    public function send($to, $text);

}
