<?php
namespace contracts;

/**
 * Interface ApiDashboardInterface.
 *
 * @author Gustavo Luna <gluna@merqueo.com>
 */
interface ApiDashboardInterface
{
    public function currentStockRefresh($warehouseId,$storeProductsIds);
}
