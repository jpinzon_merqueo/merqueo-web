<?php
// Status
Route::get('/internal-status', 'InternalStatusController@checkStatus');


//Route::get('/sitemap.xml', 'SitemapsController@index');
use Carbon\Carbon;

Route::get('/', 'IndexController@index');

//Feed FB
Route::get('/feed/{city_slug}', 'StoreController@feed');
Route::get('/feed/{city_slug}/{store_slug}', 'StoreController@feed');

Route::get('/new-site/migrate', [
    'as' => 'newSite.migrate',
    'uses' => 'NewSiteController@redirectToNewSite',
]);

Route::get('/new-site/get-user-data', [
    'as' => 'newSite.migrate.getUser',
    'uses' => 'NewSiteController@export',
]);

Route::post('/new-site/set-user-data', [
    'as' => 'newSite.migrate.setUser',
    'uses' => 'NewSiteController@import',
]);

//Test
Route::any('/fix-cc-payments/{order_id}', 'TestController@fix_cc_payments');
Route::any('/marketplace-test-date', 'TestController@marketplace_date');
Route::any('/update-bin/{pasarela}', 'TestController@update_bin');
Route::any('/activate/prom/{discount}/{start_date}/{end_date}/{qty}/{token}', 'TestController@activatePromotion');
Route::any('/tester', 'TestController@tester');
Route::any('/tester/import-txt', 'TestController@import_txt');
Route::get('/tester/payu', 'TestController@payu');
Route::match(['POST', 'GET'], '/tester/import-store', 'TestController@import_orders');
Route::get('/tester/coupons', 'TestController@coupons_creator');
Route::any('/tester/update-total/{id}', 'TestController@update_total');
Route::get('/tester/fixcombo', 'TestController@fixCombo');
Route::match(['GET', 'POST'], '/tester/importar-ordenes', ['as' => 'testController.importProviderOrderReception', 'uses' => 'TestController@importProviderOrderReception']);
Route::get('/tester/fixreceptions', 'TestController@copyCostDataReceptions');
Route::get('/tester/fixReceptionFromLog', 'TestController@fixReceptionFromLog');
Route::match(['GET', 'POST'], '/tester/update-ordenes', ['as' => 'testController.updateProviderOrderReception', 'uses' => 'TestController@updateProviderOrderReception']);
Route::match(['GET', 'POST'], '/tester/rollback-reception', ['as' => 'testController.rollbackReceptions', 'uses' => 'TestController@rollbackReceptions']);
Route::match(['GET', 'POST'], '/tester/export-p-files', ['as' => 'testController.exportPFiles', 'uses' => 'TestController@exportPFiles']);
Route::get('/tester/validate-image', 'TestController@validate_large_image');
Route::any('/tester/create-devolutions', 'TestController@create_devolutions');
Route::any('/tester/gen-pdf', 'TestController@pdf_generate_order_out');
Route::any('/tester/gen-pdf-1', 'TestController@pdf_generate_order_out_1');
Route::any('/tester/register-vehicle-movement', 'TestController@register_vehicle_movement');
Route::get('/tester/migrate-banners', 'TestController@migrate_banners');
Route::get('/tester/deeplink_tienda_carrito', 'TestController@deeplink_tienda_carrito');
Route::get('/tester/product-paginate', 'TestController@paginatorTest');
Route::get('/tester/storeProductStockLog/{month}', 'TestController@storeProductStockLog');
Route::get('/tester/check-add-credit-delivery-late/{order}', 'TestController@checkAddCreditDeliveryLate');

//General
Route::group(['prefix' => 'registro'], function () {
    Route::get('/{promo_code}', [
        'as' => 'frontUser.referredPage',
        'uses' => 'UserController@referredPage'
    ]);
    Route::match(['GET', 'POST'], '/', [
        'as' => 'frontUser.register',
        'uses' => 'UserController@register'
    ]);
});
Route::get('/banners/{bannerId}', [
    'as' => 'banner.detail',
    'uses' => 'StoreController@getBannerDetails'
]);
Route::get('/logout', 'UserController@logout');
Route::post('/social', 'UserController@social');
Route::get('/recuperar-clave', 'UserController@forgot_password');
Route::get('/faq', 'UserController@faq');
Route::get('/terminos', [
    'as'=>'terminos',
    'uses'=>'StaticsController@index'
]);
Route::get('/terminos-mx', [
    'as'=>'terminos-mx',
    'uses'=>'StaticsController@index'
]);
Route::get('/user/{user_id}/order/{order_reference}/follow', [
    'as' => 'user.followOrder',
    'uses' => 'OrderController@index'
]);
Route::get('/politicas-de-privacidad', [
    'as'=>'politicas-de-privacidad',
    'uses'=>'StaticsController@index'
]);
Route::get('/politicas-de-privacidad-mx', [
    'as'=>'politicas-de-privacidad-mx',
    'uses'=>'StaticsController@index'
]);
Route::get('/soat-faq', 'UserController@soat_faq');
//User
Route::get('/mi-cuenta', 'UserController@get_user');
Route::get('/{city_slug}/domicilios-{store_slug}/mis-productos', 'StoreController@bought_products');
Route::post('/user/update', 'UserController@update');
Route::get('/login', [
    'as' => 'frontUser.login',
    'uses' => 'UserController@login'
]);
Route::post('/verify', [
    'as' => 'frontUser.verifyUser',
    'uses' => 'UserController@verify'
]);
Route::post('/verify/{redirect}', [
    'as' => 'frontUser.verify',
    'uses' => 'UserController@verify'
]);
Route::get('/user/verify/', [
    'as' => 'frontUser.verifyUserData',
    'uses' => 'UserController@verify_data'
]);
Route::post('/user/update/invoice', [
    'as' => 'frontUser.updateInvoice',
    'uses' => 'UserController@save_invoice_data'
]);
Route::post('/user/need-phone-validation', [
    'as' => 'frontUser.needPhoneValidation',
    'uses' => 'UserController@show_validate_phone_number'
]);
Route::post('/user/address', 'UserController@save_address');
Route::get('/user/address/delete/{id}', 'UserController@delete_address');
Route::post('/user/address/update', 'UserController@update_address');
Route::get('/user/order/{reference}', 'UserController@get_order');
Route::get('/user/external-service/{reference}', 'UserController@get_external_service');
Route::get('/user/order/{reference}/cart', 'UserController@add_order_cart');
Route::post('/user/card', 'UserController@save_card');
Route::get('/user/card/delete/{id}', 'UserController@delete_card');
Route::post('/user/geoaddress', 'StoreController@validate_address_coverage_ajax');
Route::post('/user/referred', 'UserController@referred');
Route::post('/user/survey', 'UserController@save_survey');
Route::get('/mi-cuenta/pedido/{reference}/factura', [
    'as' => 'frontUser.UserInvoice',
    'uses' => 'UserController@generate_invoice'
]);
Route::post('/user/cellphone-validation', [
    'as' => 'frontUser.cellphoneValidation',
    'uses' => 'UserController@cellphone_validation'
]);

//Cart
Route::get('/user/cart', 'UserController@cart');
Route::post('/user/cart', 'UserController@cart');
Route::get('/user/cart/delete', 'UserController@deleteCart');

Route::get('/user/cart/changed-products', 'CheckoutController@get_changed_products');
Route::get('/user/cart/alter-products', 'CheckoutController@alter_cart_content_by_zone');
Route::get('/store/product/product-exists/{store_product_id}', 'UserController@is_product_available_on_warehouse_ajax');
Route::get('/user/cart/remove-limited-products', 'CheckoutController@remove_limited_products');

//Checkout
Route::get('/checkout', [
    'as' => 'frontCheckout.index',
    'uses' => 'CheckoutController@index'
]);
Route::post('/checkout', [
    'as' => 'frontCheckout.checkout',
    'uses' => 'CheckoutController@checkout'
]);
Route::get('/post-checkout/{group_id}', [
    'as' => 'frontCheckout.postCheckout',
    'uses' => 'OrderController@postOrder'
]);
Route::get('/order-managed/{order_id}', [
    'as' => 'frontCheckout.orderManaged',
    'uses' => 'OrderController@orderManaged'
]);
Route::get('/checkout/orden_confirmada/{order_reference}', [
    'as' => 'frontCheckout.checkout_success',
    'uses' => 'CheckoutController@success'
]);
Route::post('/checkout/order-failed', [
    'as' => 'frontCheckout.checkout_failed',
    'uses' => 'CheckoutController@payu_retry_transaction_failed'
]);
Route::post('/checkout/credit', 'CheckoutController@credit');
Route::post('/checkout/coupon', 'CheckoutController@coupon');
Route::get('/checkout/remove-coupon', 'CheckoutController@coupon_remove');
Route::get('/orden/{token}/calificacion/{id?}', 'UserController@show_order_score_form');
Route::get('/orden/{token}/get-score-order-data', 'UserController@get_score_order_data_ajax');
Route::post('/orden/{token}/calificacion', 'UserController@order_score');
Route::post('/payment/confirmation', 'CheckoutController@payment_confirmation');
Route::get('/order/invoice/{order_token}', [
    'as' => 'User.OrderTokenInvoice',
    'uses' => 'UserController@order_generate_invoice'
]);
Route::group(['prefix' => '/payment'], function () {
    Route::get('/response', [
        "as" => 'PayU.resposnseUrl',
        'uses' => 'CheckoutController@payu_response'
    ]);
    Route::get('/confirmation', [
        "as" => 'PayU.confirmation',
        'uses' => 'CheckoutController@payment_confirmation'
    ]);
    Route::get('/retry-transaction/{reference}', [
        "as" => 'PayU.retryTransaction',
        'uses' => 'CheckoutController@payu_retry_transaction'
    ]);
    Route::post('/retry-transaction-save/', [
        "as" => 'PayU.retryTransactionSave',
        'uses' => 'CheckoutController@payu_retry_transaction_save'
    ]);
    Route::post('/notification', [
        "as" => 'PayU.notification',
        'uses' => 'CheckoutController@payu_notification'
    ]);
});

Route::get('/ticket-call/{token}/score/{rate}', [
    'as' => 'customerService.Ticket.solutionRate',
    'uses' => 'UserController@rate_customer_service_response'
]);

Route::controller('password', 'RemindersController');
Route::post('/newsletter', 'StoreController@newsletter');
Route::get('l/{order_ofs}', 'OrderController@checkList');

//API
Route::get('/api/location', 'ApiController@location');
Route::get('/api/location_reverse', 'ApiController@location_reverse');
#Route::post('/api/delivery_zone/{action}/{id}', 'ApiController@delivery_zone');

//API VERSION 1.0
Route::group(['prefix' => 'api/1.0'], function () {
    //Index
    Route::post('/init', 'api\v1\ApiIndexController@init');
    Route::get('/version', 'api\v1\ApiIndexController@version');
    Route::get('/config', 'api\v1\ApiIndexController@config');
    Route::get('/token', 'api\v1\ApiIndexController@request_token');
    Route::get('/cities', 'api\v1\ApiIndexController@get_cities');
    //Store
    Route::get('/search/{id}', 'api\v1\ApiStoreController@search');
    Route::get('/search/{id}/result', 'api\v1\ApiStoreController@search');
    Route::get('/stores', 'api\v1\ApiStoreController@get_stores');
    Route::get('/store/{id}', 'api\v1\ApiStoreController@get_store');
    Route::get('/store/{store_id}/department/{department_id}', 'api\v1\ApiStoreController@get_department');
    Route::get('/store/{store_id}/department/{department_id}/shelf/{shelf_id}', 'api\v1\ApiStoreController@get_shelf');
    Route::get('/store/product/{id}', 'api\v1\ApiStoreController@get_product');
    //User
    Route::post('/login', 'api\v1\ApiUserController@login');
    Route::get('/logout', 'api\v1\ApiUserController@logout');
    Route::post('/password', 'api\v1\ApiUserController@password_remind');
    Route::post('/social', 'api\v1\ApiUserController@social');
    Route::post('/user', 'api\v1\ApiUserController@save_user');
    Route::get('/user/{id}', 'api\v1\ApiUserController@get_user');
    Route::put('/user/{id}', 'api\v1\ApiUserController@update_user');
    Route::get('/user/{id}/credit', 'api\v1\ApiUserController@get_user_credit');
    Route::get('/user/{user_id}/address/{address_id}', 'api\v1\ApiUserController@get_address');
    Route::post('/user/{user_id}/address', 'api\v1\ApiUserController@save_address');
    Route::put('/user/{user_id}/address/{address_id}', 'api\v1\ApiUserController@update_address');
    Route::delete('/user/{user_id}/address/{address_id}', 'api\v1\ApiUserController@delete_address');
    Route::get('/user/{user_id}/creditcard/{credit_card_id}', 'api\v1\ApiUserController@get_credit_card');
    Route::post('/user/{user_id}/creditcard', 'api\v1\ApiUserController@save_credit_card');
    Route::delete('/user/{user_id}/creditcard/{credit_card_id}', 'api\v1\ApiUserController@delete_credit_card');
    Route::get('/user/{user_id}/order/{order_id}/cart', 'api\v1\ApiUserController@add_order_cart');
    //Checkout
    Route::post('/checkout/deliverytime', 'api\v1\ApiCheckoutController@delivery_time');
    Route::post('/checkout', 'api\v1\ApiCheckoutController@checkout');
    Route::post('/coupon', 'api\v1\ApiCheckoutController@coupon');
    Route::post('/order/{id}/score', 'api\v1\ApiCheckoutController@score_order');
    Route::get('/order/{id}/notification', 'api\v1\ApiCheckoutController@notification_order');
    Route::get('/order/pending/{user_id}', 'api\v1\ApiCheckoutController@pending_orders');
});

//API VERSION 1.1 Cambio en sistema de cupones
Route::group(['prefix' => 'api/1.1'], function () {
    //Index
    Route::post('/init', 'api\v11\ApiIndexController@init');
    Route::get('/version', 'api\v11\ApiIndexController@version');
    Route::get('/config', 'api\v11\ApiIndexController@config');
    Route::get('/token', 'api\v11\ApiIndexController@request_token');
    Route::get('/cities', 'api\v11\ApiIndexController@get_cities');
    //Route::get('/oauth/request_token', 'api\v11\ApiOAuthController@request_token');
    //Route::get('/oauth/authorize', 'api\v11\ApiOAuthController@authorize_request_token');
    //Route::post('/oauth/access_token', 'api\v11\ApiOAuthController@request_access_token');
    //Store
    //Route::get('/search/{id}', 'api\v11\ApiStoreController@search');
    Route::get('/search/{id}/result', 'api\v11\ApiStoreController@search');
    Route::get('/search/{id}/products', 'api\v11\ApiStoreController@search');
    Route::get('/stores', 'api\v11\ApiStoreController@get_stores');
    Route::get('/stores/delivery-time', 'api\v11\ApiStoreController@get_stores_delivery_time');
    Route::get('/store/{id}', 'api\v11\ApiStoreController@get_store');
    Route::get('/store/{store_id}/department/{department_id}', 'api\v11\ApiStoreController@get_department');
    Route::get('/store/{store_id}/department/{department_id}/shelf/{shelf_id}', 'api\v11\ApiStoreController@get_shelf');
    Route::get('/store/{store_id}/user/{user_id}/products', 'api\v11\ApiStoreController@get_user_products');
    Route::get('/store/product/{id}', 'api\v11\ApiStoreController@get_product');
    //User
    Route::post('/login', 'api\v11\ApiUserController@login');
    Route::get('/logout', 'api\v11\ApiUserController@logout');
    Route::post('/password', 'api\v11\ApiUserController@password_remind');
    Route::post('/social', 'api\v11\ApiUserController@social');
    Route::post('/user', 'api\v11\ApiUserController@save_user');
    Route::get('/user/{id}', 'api\v11\ApiUserController@get_user');
    Route::put('/user/{id}', 'api\v11\ApiUserController@update_user');
    Route::get('/user/{id}/credit', 'api\v11\ApiUserController@get_user_credit');
    Route::get('/user/{id}/discounts', 'api\v11\ApiUserController@get_user_discounts');
    Route::post('/user/{id}/referred', 'api\v11\ApiUserController@referred');
    Route::post('/user/{id}/app/comment', 'api\v11\ApiUserController@save_comment');
    Route::get('/user/{user_id}/address/{address_id}', 'api\v11\ApiUserController@get_address');
    Route::post('/user/{id}/address', 'api\v11\ApiUserController@save_address');
    Route::put('/user/{user_id}/address/{address_id}', 'api\v11\ApiUserController@update_address');
    Route::delete('/user/{user_id}/address/{address_id}', 'api\v11\ApiUserController@delete_address');
    Route::get('/user/{user_id}/creditcard/{credit_card_id}', 'api\v11\ApiUserController@get_credit_card');
    Route::post('/user/{user_id}/creditcard', 'api\v11\ApiUserController@save_credit_card');
    Route::delete('/user/{user_id}/creditcard/{credit_card_id}', 'api\v11\ApiUserController@delete_credit_card');
    Route::get('/user/{user_id}/order/{order_id}/cart', 'api\v11\ApiUserController@add_order_cart');
    Route::post('/user/{id}/survey', 'api\v11\ApiUserController@save_survey');
    //Checkout
    Route::post('/checkout/deliverytime', 'api\v11\ApiCheckoutController@delivery_time');
    Route::post('/checkout', 'api\v11\ApiCheckoutController@checkout');
    Route::post('/coupon', 'api\v11\ApiCheckoutController@coupon');
    Route::post('/order/{id}/score', 'api\v11\ApiCheckoutController@score_order');
    Route::get('/order/{id}/notification', 'api\v11\ApiCheckoutController@notification_order');
    Route::get('/order/pending/{user_id}', 'api\v11\ApiCheckoutController@pending_orders');
});

//API VERSION 1.2 Cambio tienda en bodega
Route::group(['prefix' => 'api/1.2'], function () {
    //Index
    Route::post('/init', 'api\v12\ApiIndexController@init');
    Route::get('/version', 'api\v12\ApiIndexController@version');
    Route::get('/config', 'api\v12\ApiIndexController@config');
    Route::get('/cities', 'api\v12\ApiIndexController@get_cities');
    //Store
    Route::get('/search/{id}', 'api\v12\ApiStoreController@search');
    Route::get('/search/{id}/result', 'api\v12\ApiStoreController@search');
    Route::get('/stores', 'api\v12\ApiStoreController@get_stores');
    Route::get('/stores/delivery-time', 'api\v12\ApiStoreController@get_stores_delivery_time');
    Route::get('/store/{id}', 'api\v12\ApiStoreController@get_store');
    Route::get('/store/{store_id}/department/{department_id}', 'api\v12\ApiStoreController@get_department');
    Route::get('/store/{store_id}/department/{department_id}/shelf/{shelf_id}', 'api\v12\ApiStoreController@get_shelf');
    Route::get('/store/{store_id}/user/{user_id}/products', 'api\v12\ApiStoreController@get_user_products');
    Route::get('/store/product/{id}', 'api\v12\ApiStoreController@get_product');
    //User
    Route::post('/login', 'api\v12\ApiUserController@login');
    Route::get('/logout', 'api\v12\ApiUserController@logout');
    Route::post('/password', 'api\v12\ApiUserController@password_remind');
    Route::post('/social', 'api\v12\ApiUserController@social');
    Route::post('/user', 'api\v12\ApiUserController@save_user');
    Route::get('/user/{id}', 'api\v12\ApiUserController@get_user');
    Route::put('/user/{id}', 'api\v12\ApiUserController@update_user');
    Route::get('/user/{id}/credit', 'api\v12\ApiUserController@get_user_credit');
    Route::get('/user/{id}/discounts', 'api\v12\ApiUserController@get_user_discounts');
    Route::post('/user/{id}/referred', 'api\v12\ApiUserController@referred');
    Route::post('/user/{id}/app/comment', 'api\v12\ApiUserController@save_comment');
    Route::get('/user/{user_id}/address/{address_id}', 'api\v12\ApiUserController@get_address');
    Route::post('/user/{id}/address', 'api\v12\ApiUserController@save_address');
    Route::put('/user/{user_id}/address/{address_id}', 'api\v12\ApiUserController@update_address');
    Route::delete('/user/{user_id}/address/{address_id}', 'api\v12\ApiUserController@delete_address');
    Route::get('/user/{user_id}/creditcard/{credit_card_id}', 'api\v12\ApiUserController@get_credit_card');
    Route::post('/user/{user_id}/creditcard', 'api\v12\ApiUserController@save_credit_card');
    Route::delete('/user/{user_id}/creditcard/{credit_card_id}', 'api\v12\ApiUserController@delete_credit_card');
    Route::get('/user/{user_id}/order/{order_id}/cart', 'api\v12\ApiUserController@add_order_cart');
    Route::post('/user/{id}/survey', 'api\v12\ApiUserController@save_survey');
    Route::post('/user/{id}/cellphone-validation', 'api\v12\ApiUserController@cellphone_validated');
    //Checkout
    Route::post('/checkout/deliverytime', 'api\v12\ApiCheckoutController@delivery_time');
    Route::post('/checkout', 'api\v12\ApiCheckoutController@checkout');
    Route::post('/checkout/cart', 'api\v12\ApiCheckoutController@get_cart');
    Route::post('/coupon', 'api\v12\ApiCheckoutController@coupon');
    Route::post('/order/{id}/score', 'api\v12\ApiCheckoutController@score_order');
    Route::get('/order/{id}/notification', 'api\v12\ApiCheckoutController@notification_order');
    Route::get('/order/pending/{user_id}', 'api\v12\ApiCheckoutController@pending_orders');
});

//API VERSION 1.3 Cambio diseño de home de tienda y departamentos
Route::group(['prefix' => 'api/1.3'], function () {
    //Index
    Route::get('/cities/main-and-neighborhoods', 'api\v13\ApiCityController@getMainAndNeighborhoodsAction');
    Route::get('/cities/all', 'api\v13\ApiCityController@getAllAction');
    Route::post('/init', 'api\v13\ApiIndexController@init');
    Route::get('/version', 'api\v13\ApiIndexController@version');
    Route::get('/config', 'api\v13\ApiIndexController@config');
    Route::get('/cities', 'api\v13\ApiIndexController@get_cities');
    Route::get('/token', 'api\v13\ApiIndexController@request_token');
    //Store
    Route::get('/store', 'api\v13\ApiStoreController@get_store');
    Route::get('/store/{store_id}/banners', 'api\v13\ApiStoreController@get_banners');
    Route::get('/store/{store_id}/banners/{banner_id}', 'api\v13\ApiStoreController@single_banner');
    Route::get('/city/{city_name}/banners', 'api\v13\ApiStoreController@get_banners_by_city');
    Route::get('/store/{store_id}/department/{department_id}', 'api\v13\ApiStoreController@get_department');
    Route::get('/store/{store_id}/departments-menu', 'api\v13\ApiStoreController@get_departments_menu');
    Route::get('/store/{store_id}/user/{user_id}/products', 'api\v13\ApiStoreController@get_user_products');
    Route::get('/store/product/{id}', 'api\v13\ApiStoreController@get_product');
    Route::get('/search/{store_id}', 'api\v13\ApiStoreController@search');
    Route::get('/search/{store_id}/result', 'api\v13\ApiStoreController@search');
    Route::get('/store/delivery-time', 'api\v13\ApiStoreController@get_store_delivery_time');
    //User
    Route::post('/login', 'api\v13\ApiUserController@login');
    Route::get('/logout', 'api\v13\ApiUserController@logout');
    Route::post('/password', 'api\v13\ApiUserController@password_remind');
    Route::post('/social', 'api\v13\ApiUserController@social');
    Route::post('/user', 'api\v13\ApiUserController@save_user');
    Route::get('/user/{id}', 'api\v13\ApiUserController@get_user');
    Route::put('/user/{id}', 'api\v13\ApiUserController@update_user');
    Route::get('/user/{id}/credit', 'api\v13\ApiUserController@get_user_credit');
    Route::get('/user/{id}/credit/movements', 'api\v13\ApiUserController@get_user_credit_movements');
    Route::get('/user/{id}/discounts', 'api\v13\ApiUserController@get_user_discounts');
    Route::post('/user/update/receives', 'api\v13\ApiUserController@update_user_receives');
    Route::get('/user/{userId}/order/{orderId}/receives', 'api\v13\ApiUserController@get_user_receives');
    Route::post('/user/{id}/referred', 'api\v13\ApiUserController@referred');
    Route::post('/user/{id}/app/comment', 'api\v13\ApiUserController@save_comment');
    Route::get('/user/{user_id}/addresses', 'api\v13\ApiUserController@get_addresses');
    Route::get('/user/{user_id}/address/{address_id}', 'api\v13\ApiUserController@get_address');
    Route::post('/user/{id}/address', 'api\v13\ApiUserController@save_address');
    Route::put('/user/{user_id}/address/{address_id}', 'api\v13\ApiUserController@update_address');
    Route::delete('/user/{user_id}/address/{address_id}', 'api\v13\ApiUserController@delete_address');
    Route::get('/user/{user_id}/creditcards', 'api\v13\ApiUserController@get_credit_cards');
    Route::get('/user/{user_id}/creditcard/{credit_card_id}', 'api\v13\ApiUserController@get_credit_card');
    Route::post('/user/{user_id}/creditcard', 'api\v13\ApiUserController@save_credit_card');
    Route::delete('/user/{user_id}/creditcard/{credit_card_id}', 'api\v13\ApiUserController@delete_credit_card');
    Route::get('/user/{user_id}/order/{order_id}/cart', 'api\v13\ApiUserController@add_order_cart');
    Route::post('/user/{id}/survey', 'api\v13\ApiUserController@save_survey');
    Route::post('/user/{id}/cellphone-validation', 'api\v13\ApiUserController@cellphone_validated');
    Route::post('/user/cellphone-validation', 'api\v13\ApiUserController@cellphone_validation');
    Route::get('/user/{user_id}/order/{order_id}/detail', 'api\v13\ApiUserController@get_order_detail');
    Route::get('/user/{id}/orders', 'api\v13\ApiUserController@get_orders');
    Route::get('/user/{user_id}/order/{order_id}/status', 'api\v13\ApiUserController@get_order_status');
    Route::get('/user/{id}/tags', 'api\v13\ApiUserController@get_user_tags');
    Route::get('/address-validation', 'api\v13\ApiCheckoutController@validate_address');
    Route::any('/coordinates-validation', 'api\v13\ApiCheckoutController@validate_coordinates');
    //Checkout
    Route::post('/checkout/deliverytime', 'api\v13\ApiCheckoutController@delivery_time');
    Route::post('/checkout', 'api\v13\ApiCheckoutController@checkout');
    Route::post('/checkout/cart', 'api\v13\ApiCheckoutController@get_cart');
    Route::post('/coupon', 'api\v13\ApiCheckoutController@coupon');
    Route::post('/store/{store}/delivery-dates', 'api\v13\ApiCheckoutController@get_delivery_dates');
    //Order
    Route::get('/order/pending/{user_id}', 'api\v13\ApiCheckoutController@pending_orders');
    Route::get('/order/{id}/get-score', 'api\v13\ApiUserController@get_score_order');
    Route::post('/order/{id}/save-score', 'api\v13\ApiUserController@save_score_order');
    Route::get('/order/{id}/notification', 'api\v13\ApiCheckoutController@notification_order');
    Route::get('/order/{id}/score', 'api\v13\ApiCheckoutController@get_order_score');
    Route::post('/order/{id}/score', 'api\v13\ApiCheckoutController@save_order_score');

    // Help Center
    Route::group(['prefix' => 'help-center'], function () {
        Route::get('/{order_id}/menu', 'api\v13\help_center\ApiOrderController@get_menu_values');
        Route::get('/order/change-reasons', 'api\v13\help_center\ApiOrderController@get_delivery_time_change_reasons');
        Route::get('/user/{user_id}/order/{order_id}/delivery-time', 'api\v13\help_center\ApiOrderController@get_delivery_time_data');
        Route::match(
            ['GET', 'PUT', 'POST'],
            '/user/{user_id}/order/{order_id}',
            'api\v13\help_center\ApiOrderController@manage_user_orders'
        );
        Route::get('/order/{order_id}', 'api\v13\help_center\ApiOrderController@manage_order');
        Route::get('/order/{id}/notification', 'api\v13\help_center\ApiOrderController@notification_order');
    });
});

//API VERSION 1.4 Optimizacion de servicios
Route::group(array('prefix' => 'api/1.4'), function () {
    //Index
    Route::get('/init', 'api\v14\ApiIndexController@init');
    //Store
    Route::get('/store', 'api\v14\ApiStoreController@get_store');
    Route::get('/store/{store_id}/banners', 'api\v14\ApiStoreController@get_banners');
    Route::get('/store/{store_id}/user/{user_id}/products', 'api\v14\ApiStoreController@get_user_products');
    Route::get('/store/{store_id}/department/{department_id}', 'api\v14\ApiStoreController@get_department');
    Route::get('/store/{store_id}/departments-menu', 'api\v14\ApiStoreController@get_departments_menu');
    Route::get('/store/{store_id}/product/{product_id}', 'api\v14\ApiStoreController@get_product');
    Route::get('/store/{store_id}/search', 'api\v14\ApiStoreController@search');
    //User
    Route::post('/login', 'api\v14\ApiUserController@login');
    Route::get('/logout', 'api\v14\ApiUserController@logout');
    Route::post('/password', 'api\v14\ApiUserController@password_remind');
    Route::post('/social', 'api\v14\ApiUserController@social');
    Route::get('/coordinates-validation', 'api\v14\ApiUserController@validate_coordinates');
    Route::post('/user', 'api\v14\ApiUserController@save_user');
    Route::get('/user/{id}', 'api\v14\ApiUserController@get_user');
    Route::put('/user/{id}', 'api\v14\ApiUserController@update_user');
    Route::get('/user/{id}/discounts', 'api\v14\ApiUserController@get_user_discounts');
    Route::post('/user/{id}/referred', 'api\v14\ApiUserController@referred');
    Route::post('/user/{id}/app/comment', 'api\v14\ApiUserController@save_comment');
    Route::get('/user/{user_id}/addresses', 'api\v14\ApiUserController@get_addresses');
    Route::get('/user/{user_id}/address/{address_id}', 'api\v14\ApiUserController@get_address');
    Route::post('/user/{id}/address', 'api\v14\ApiUserController@save_address');
    Route::put('/user/{user_id}/address/{address_id}', 'api\v14\ApiUserController@update_address');
    Route::delete('/user/{user_id}/address/{address_id}', 'api\v14\ApiUserController@delete_address');
    Route::get('/user/{user_id}/creditcards', 'api\v14\ApiUserController@get_credit_cards');
    Route::get('/user/{user_id}/creditcard/{credit_card_id}', 'api\v14\ApiUserController@get_credit_card');
    Route::post('/user/{user_id}/creditcard', 'api\v14\ApiUserController@save_credit_card');
    Route::delete('/user/{user_id}/creditcard/{credit_card_id}', 'api\v14\ApiUserController@delete_credit_card');
    Route::get('/user/{user_id}/order/{order_id}/cart', 'api\v14\ApiUserController@add_order_cart');
    Route::post('/user/{id}/survey', 'api\v14\ApiUserController@save_survey');
    Route::post('/user/{id}/cellphone-validation', 'api\v14\ApiUserController@cellphone_validated');
    Route::get('/user/{id}/orders', 'api\v14\ApiUserController@get_orders');
    Route::get('/user/{user_id}/order/{order_id}/status', 'api\v14\ApiUserController@get_order_status');
    Route::get('/user/{user_id}/order/{order_id}/detail', 'api\v14\ApiUserController@get_order_detail');
    Route::get('/user/{id}/tags', 'api\v14\ApiUserController@get_user_tags');
    //Checkout
    Route::post('/checkout/cart', 'api\v14\ApiCheckoutController@get_cart');
    Route::post('/checkout/payment-delivery-dates', 'api\v14\ApiCheckoutController@get_payment_delivery_dates');
    Route::post('/coupon', 'api\v14\ApiCheckoutController@coupon');
    Route::post('/checkout', 'api\v14\ApiCheckoutController@checkout');
    //Order
    Route::get('/order/{id}/score', 'api\v14\ApiCheckoutController@get_order_score');
    Route::post('/order/{id}/score', 'api\v14\ApiCheckoutController@save_order_score');
    Route::get('/order/{id}/notification', 'api\v14\ApiCheckoutController@notification_order');
    Route::get('/order/pending/{user_id}', 'api\v14\ApiCheckoutController@pending_orders');
    Route::get('/order/{id}/get-score', 'api\v14\ApiUserController@get_score_order');
    Route::post('/order/{id}/save-score', 'api\v14\ApiUserController@save_score_order');
});

//Store
Route::get('/buscador/{id}', 'StoreController@autocomplete');
Route::get('/{city_slug}', [
    'as' => 'frontStoreCity.index',
    'uses' => 'IndexController@index'
]);
Route::get('/{city_slug}/domicilios-{store_slug}', [
    'as' => 'frontStore.all_departments',
    'uses' => 'StoreController@all_departments'
]);
Route::get('/{city_slug}/domicilios-{store_slug}/buscar', [
    'as' => 'frontStore.search_products',
    'uses' => 'StoreController@search_products'
]);
Route::get('/{city_slug}/domicilios-{store_slug}/{department_slug}', [
    'as' => 'frontStoreDepartment.single_department',
    'uses' => 'StoreController@single_department'
]);
Route::get('/{city_slug}/domicilios-{store_slug}/{department_slug}/{shelf_slug}', [
    'as' => 'frontStoreShelf.single_shelf',
    'uses' => 'StoreController@single_shelf'
]);
Route::get('/{city_slug}/domicilios-{store_slug}/{department_slug}/{shelf_slug}/{product_slug}', [
    'as' => 'frontStoreProducts.single_product',
    'uses' => 'StoreController@single_product'
]);

Route::get('/{city_slug}/{store_slug}/best-price', 'StoreController@best_price');

//API EXTERNAL SERVICES
Route::group(['prefix' => 'api/external-services/1.0'], function () {
    Route::get('/soat', 'api\external_services\soat\v1\ApiSoatController@get_soat_data');
    Route::post('/soat/pay', 'api\external_services\soat\v1\ApiSoatController@pay');
    Route::get('/soat/info-user', 'api\external_services\soat\v1\ApiSoatController@get_info_user');
    Route::post('/soat/reminder', 'api\external_services\soat\v1\ApiSoatController@set_reminder');
    Route::get('/soat/states/{department_id?}', 'api\external_services\soat\v1\ApiSoatController@get_states');
    Route::post('/soat/credit-card', 'api\external_services\soat\v1\ApiSoatController@set_credit_card');

    Route::get('/service/operators', 'api\external_services\services\v1\ApiServiceController@get_operators_mobile');
    Route::post('/service/recharge', 'api\external_services\services\v1\ApiServiceController@recharge');

    Route::get('/service/billers-categories/{parentCategoryId?}', 'api\external_services\services\v1\ApiServiceController@get_billers_categories');
    Route::get('/service/billers/{categoryId}', 'api\external_services\services\v1\ApiServiceController@get_billers_category');
    Route::post('/service/validate-pay', 'api\external_services\services\v1\ApiServiceController@validate_bill_reference');
});

//API TOKEN JWT
Route::group(['prefix' => 'api/token/1.0'], function () {
    Route::get('/generate', 'api\token\v1\ApiTokenController@generate_token');
});

//API MERQUEO SAP VERSION 1.0
Route::group(['prefix' => 'api/sap/1.0'], function () {
    Route::get('/{template}/{start_date}/{end_date}', 'api\sap\v1\ApiSapController@templates');
});

//Redireccionar URLs viejas
/*Route::get('/{city_slug}/{store_slug}', function () {
    return Redirect::to(Route::current()->parameter('city_slug') . '/domicilios-' . Route::current()->parameter('store_slug'));
});
Route::get('/{city_slug}/{store_slug}/buscar', function () {
    return Redirect::to(Route::current()->parameter('city_slug') . '/domicilios-' . Route::current()->parameter('store_slug') . '/buscar');
});
Route::get('/{city_slug}/{store_slug}/{department_slug}', function () {
    if (Route::current()->parameter('city_slug') != 'api') {
        return Redirect::to(Route::current()->parameter('city_slug') . '/domicilios-' . Route::current()->parameter('store_slug') . '/' . Route::current()->parameter('department_slug'));
    }
});
Route::get('/{city_slug}/{store_slug}/{department_slug}/{shelf_slug}', function () {
    if (Route::current()->parameter('city_slug') != 'api') {
        return Redirect::to(Route::current()->parameter('city_slug') . '/domicilios-' . Route::current()->parameter('store_slug') . '/' . Route::current()->parameter('department_slug') . '/' . Route::current()->parameter('shelf_slug'));
    }
});
Route::get('/{city_slug}/{store_slug}/{department_slug}/{shelf_slug}/{product_slug}', function () {
    if (Route::current()->parameter('city_slug') != 'api') {
        return Redirect::to(Route::current()->parameter('city_slug') . '/domicilios-' . Route::current()->parameter('store_slug') . '/' . Route::current()->parameter('department_slug') . '/' . Route::current()->parameter('shelf_slug') . '/' . Route::current()->parameter('product_slug'));
    } else {
        return App::make('ApiController')->internalError(401, 'Not Found');
    }
});*/

//static content
Route::get('/content/{document}', 'StaticsController@index');
