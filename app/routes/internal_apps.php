<?php

//API PICKING VERSION 1.0
Route::group(['prefix' => 'api/picking/1.0'], function () {
    Route::post('/login', 'api\picking\v1\ApiPickingController@login');
    Route::post('/get-picking-group', 'api\picking\v1\ApiPickingController@get_picking_group');
    Route::post('/set-picking-group', 'api\picking\v1\ApiPickingController@set_picking_group');
    Route::post('/logout', 'api\picking\v1\ApiPickingController@logout');
    Route::post('/product-position-storage', 'api\picking\v1\ApiPickingController@product_position_storage');
    Route::post('/pusher-auth', 'api\picking\v1\ApiPickingController@pusher_auth');
    Route::get('/picker/{id}/picking-orders', 'api\picking\v1\ApiPickingController@picking_orders');
    Route::post('/order/{id}/status', 'api\picking\v1\ApiOrderController@update_order_status');
    Route::post('/order/{id}/picking-date', 'api\picking\v1\ApiOrderController@update_order_picking_date'); 
    Route::post('/order/{id}/product-related', 'api\picking\v1\ApiOrderController@get_product_related');

    //Módulo de validación de picking
    Route::group(['prefix' => 'validate'], function () {
        Route::get('/', [
            'as' => 'apiValidation.index',
            'uses' => 'api\picking\v1\ApiValidationController@index'
        ]);
        Route::get('/get-routes', [
            'as' => 'apiValidation.getRoutesAjax',
            'uses' => 'api\picking\v1\ApiValidationController@get_planning_route_ajax'
        ]);
        Route::get('/get-orders', [
            'as' => 'apiValidation.getOrdersAjax',
            'uses' => 'api\picking\v1\ApiValidationController@get_orders_ajax'
        ]);
        Route::post('/get-order-details', [
            'as' => 'apiValidation.getOrderDetailsAjax',
            'uses' => 'api\picking\v1\ApiValidationController@get_order_details_ajax'
        ]);
        Route::post('/update-order-status', [
            'as' => 'apiValidation.updateOrderStatusAjax',
            'uses' => 'api\picking\v1\ApiValidationController@update_order_status_ajax'
        ]);
        Route::post('/update-order-revision-date', [
            'as' => 'apiValidation.updateOrderRevisionDateAjax',
            'uses' => 'api\picking\v1\ApiValidationController@update_order_revision_date_ajax'
        ]);
    });

    Route::group(['prefix' => 'supplier'], function () {
        Route::get('/', [
            'as' => 'apiSupplier.index',
            'uses' => 'api\picking\v1\ApiSupplierController@index'
        ]);
        Route::get('/get-routes', [
            'as' => 'apiSupplier.getRoutesAjax',
            'uses' => 'api\picking\v1\ApiSupplierController@get_planning_route_ajax'
        ]);
        Route::get('/get-orders', [
            'as' => 'apiSupplier.getOrdersAjax',
            'uses' => 'api\picking\v1\ApiSupplierController@get_orders_ajax'
        ]);
        Route::post('/get-order-details', [
            'as' => 'apiSupplier.getOrderDetailsAjax',
            'uses' => 'api\picking\v1\ApiSupplierController@get_order_details_ajax'
        ]);
        Route::post('/update-order-status', [
            'as' => 'apiSupplier.updateOrderStatusAjax',
            'uses' => 'api\picking\v1\ApiSupplierController@update_order_status_ajax'
        ]);
        Route::post('/confirm-product-quantity', [
            'as' => 'apiSupplier.confirmProductQuantityAjax',
            'uses' => 'api\picking\v1\ApiSupplierController@confirmProductQuantityAjax'
        ]);
    });
});

//API PICKING VERSION 1.2
Route::group(['prefix' => 'api/picking/1.2'], function () {
    Route::post('/login', 'api\picking\v12\ApiPickingController@login');
    Route::post('/pusher-auth', 'api\picking\v12\ApiPickingController@pusher_auth');
    Route::post('/assign-cart', 'api\picking\v12\ApiPickingController@assignCard');
    Route::post('/get-picking-group', 'api\picking\v12\ApiPickingController@getPickingGroup');
    Route::post('/set-picking-group', 'api\picking\v12\ApiPickingController@setPickingGroup');
    Route::post('/logout', 'api\picking\v12\ApiPickingController@logout');
    Route::post('/picker/{id}/picking-order', 'api\picking\v12\ApiPickingController@pickingOrderAssign');
    Route::get('/picker/{id}/picking-order/{order_id}/products', 'api\picking\v12\ApiOrderController@pickingProducts');
    Route::post('/order/{id}/status', 'api\picking\v12\ApiOrderController@updateOrderStatus');
    Route::get('/picker/{id}/picker-productivity', 'api\picking\v12\ApiOrderController@getPickerProductivity');
    Route::get('/order/product-cold-status', 'api\picking\v12\ApiOrderController@getOrderProductsColdStatus');
    Route::post('/packing-order-assignment', 'api\picking\v12\ApiPickingController@packingOrderAssignment');
    Route::post('/packing-update-baskets', 'api\picking\v12\ApiPickingController@packingUpdateBaskets');

});

//API PICKING VERSION 1.1
Route::group(['prefix' => 'api/picking/1.1'], function () {
    Route::post('/login', 'api\picking\v11\ApiPickingController@login');
    Route::post('/get-picking-group', 'api\picking\v11\ApiPickingController@get_picking_group');
    Route::post('/set-picking-group', 'api\picking\v11\ApiPickingController@set_picking_group');
    Route::post('/logout', 'api\picking\v11\ApiPickingController@logout');
    Route::post('/picker/{id}/picking-order', 'api\picking\v11\ApiPickingController@picking_order');
    Route::get('/picker/{id}/picking-order/{order_id}/products', 'api\picking\v11\ApiOrderController@picking_products');
    Route::post('/order/{id}/status', 'api\picking\v11\ApiOrderController@update_order_status');
    Route::get('/picker/{id}/picker-productivity', 'api\picking\v11\ApiOrderController@picker_productivity');

    //Route::post('/order/{id}/picking-date', 'api\picking\v1\ApiOrderController@update_order_picking_date');
    //Route::post('/order/{id}/product-related', 'api\picking\v1\ApiOrderController@get_product_related');
});

//API TRANSPORTER VERSION 1.0
Route::group(['prefix' => 'api/transporter/1.0'], function () {
    Route::post('/login', 'api\transporters\v1\ApiTransporterController@login');
    Route::post('/logout', 'api\transporters\v1\ApiTransporterController@logout');
    Route::post('/pusher-auth', 'api\transporters\v1\ApiTransporterController@pusher_auth');
    Route::post('/transporter/{id}/terms', 'api\transporters\v1\ApiTransporterController@terms');
    Route::get('/transporter/{id}/update-status', 'api\transporters\v1\ApiTransporterController@update_agent_status');
    Route::get('/transporter/{id}/enlisted-orders', 'api\transporters\v1\ApiTransporterController@enlisted_orders');
    Route::get('/transporter/{id}/get-movements', 'api\transporters\v1\ApiTransporterController@get_movements');
    Route::get('/transporter/{id}/orders-sort-by', 'api\transporters\v1\ApiTransporterController@orders_sort_by');
    Route::post('/transporter/{id}/add-alert/{alert_num}', 'api\transporters\v1\ApiTransporterController@add_transporter_alert');
    Route::post('/transporter/{id}/validate-zone', 'api\transporters\v1\ApiTransporterController@validate_vehicle_zone');
    Route::post('/order/{id}/status', 'api\transporters\v1\ApiOrderController@update_order_status');
    Route::post('/order/{id}/product-status', 'api\transporters\v1\ApiOrderController@update_product_status');
    Route::post('/order/{id}/product-returned-status', 'api\transporters\v1\ApiOrderController@update_product_status_returns');
    Route::post('/order/{id}/upload-image', 'api\transporters\v1\ApiOrderController@upload_order_image');
    Route::get('/get-firebase-token', 'api\transporters\v1\ApiTransporterController@create_custom_token');
    Route::post('/send-news', 'api\transporters\v1\ApiTransporterController@register_news');
    Route::post('/log-call-user', 'api\transporters\v1\ApiTransporterController@log_call_user');
    Route::post('/get-news-response', 'api\transporters\v1\ApiTransporterController@get_response_news');
    Route::post('/upload-reference', 'api\transporters\v1\ApiTransporterController@upload_reference_image');
    Route::post('/get-order-products', 'api\transporters\v1\ApiOrderController@get_products');
});

//API TRANSPORTER VERSION 1.1
Route::group(['prefix' => 'api/transporter/1.1'], function () {
    Route::post('/login', 'api\transporters\v11\ApiTransporterController@login');
    Route::post('/logout', 'api\transporters\v11\ApiTransporterController@logout');
    Route::post('/validate-version', 'api\transporters\v11\ApiTransporterController@validate_version');
    Route::post('/transporter/{id}/terms', 'api\transporters\v11\ApiTransporterController@terms');
    Route::get('/transporter/{id}/enlisted-orders', 'api\transporters\v11\ApiTransporterController@enlisted_orders');
    Route::post('/transporter/get-movements', 'api\transporters\v11\ApiTransporterController@get_movements');
    Route::post('/transporter/orders-sort-by', 'api\transporters\v11\ApiTransporterController@orders_sort_by');
    Route::post('/transporter/add-alert/', 'api\transporters\v11\ApiTransporterController@add_transporter_alert');
    Route::post('/transporter/validate-zone', 'api\transporters\v11\ApiTransporterController@validate_vehicle_zone');
    Route::post('/order/status', 'api\transporters\v11\ApiOrderController@update_order_status');
    Route::post('/order/upload-image', 'api\transporters\v11\ApiOrderController@upload_order_image');
    Route::post('/order/product-status', 'api\transporters\v11\ApiOrderController@update_product_status');
    Route::post('/order/product-returned-status', 'api\transporters\v11\ApiOrderController@update_product_status_returns');
    Route::get('/get-firebase-token', 'api\transporters\v11\ApiTransporterController@create_custom_token');
    Route::post('/send-news', 'api\transporters\v11\ApiTransporterController@register_news');
    Route::post('/log-call-user', 'api\transporters\v11\ApiTransporterController@log_call_user');
    Route::post('/get-news-response', 'api\transporters\v11\ApiTransporterController@get_response_news');
    Route::post('/upload-reference', 'api\transporters\v11\ApiTransporterController@upload_reference_image');
    Route::post('/pick-up-product', 'api\transporters\v11\ApiTransporterController@save_pickup_products');
    Route::post('/get-order-products', 'api\transporters\v11\ApiOrderController@get_products');
    Route::post('/validate-baskets-transporter', 'api\transporters\v11\ApiTransporterController@validate_baskets_transporter');
    Route::post('/store-report-route-dispatch', 'api\transporters\v11\ApiTransporterController@storeReportRouteDispatch');
    Route::post('/register-status-product-transporter', 'api\transporters\v11\ApiTransporterController@registerStatusProductTransporter');
    Route::get('/status-route-transporter/{vehicle_id}', 'api\transporters\v11\ApiTransporterController@status_route_transporter');
});

//API PICKER STORAGE VERSION 1.0
Route::group(['prefix' => 'api/storage/1.0'], function () {
    Route::get('/{identity_number}', 'api\storage\v1\ApiStorageController@get_picker_storage');
    Route::post('/save', 'api\storage\v1\ApiStorageController@save_picker_storage');
    Route::post('/upload-image/{identity_number}', 'api\storage\v1\ApiStorageController@upload_storage_image');
    Route::post('/halt', 'api\storage\v1\ApiStorageController@halt_picker_storage');
});
