<?php

Route::group(['prefix' => "/{$admin_route}"], function () {
    //Admin
    Route::get('/', [
        'as' => 'admin.dashboard',
        'uses' => 'admin\AdminController@dashboard'
    ]);
    Route::get('/login', [
        'as' => 'admin.login',
        'uses' => 'admin\AdminController@login'
    ]);
    Route::post('/login', [
        'as' => 'admin.login',
        'uses' => 'admin\AdminController@post_login'
    ]);
    Route::get('/password', [
        'as' => 'admin.password',
        'uses' => 'admin\AdminController@password'
    ]);
    Route::post('/password', [
        'as' => 'admin.password',
        'uses' => 'admin\AdminController@save_password'
    ]);
    Route::get('/logout', [
        'as' => 'admin.logout',
        'uses' => 'admin\AdminController@logout'
    ]);
    Route::get('/get-warehouses-ajax', [
        'as' => 'admin.get_warehouses_ajax',
        'uses' => 'admin\AdminController@get_warehouses_ajax'
    ]);
});

//Planning
Route::group(['prefix' => "/{$admin_route}/planning"], function () {

    Route::group(['prefix' => '/route-constructor'], function () {
        Route::get('/', [
            'as' => 'adminPlanningRouteConstructor.index',
            'uses' => 'admin\picking\RouteConstructorController@index'
        ]);
        Route::get('/get-routes', [
            'as' => 'adminPlanningRouteConstructor.getRoutesAjax',
            'uses' => 'admin\picking\RouteConstructorController@getRoutes'
        ]);
        Route::get('/get-orders', [
            'as' => 'adminPlanningRouteConstructor.getOrdersAjax',
            'uses' => 'admin\picking\RouteConstructorController@getOrders'
        ]);
        Route::get('/save-routes', [
            'as' => 'adminPlanningRouteConstructor.saveRoutes',
            'uses' => 'admin\picking\RouteConstructorController@save'
        ]);
    });

    Route::get('/transport', [
        'as' => 'adminPlanning.transport',
        'uses' => 'admin\AdminPlanningController@transport'
    ]);
    Route::get('/transport/{route_id}/reasign-route', [
        'as' => 'adminPlanning.reasign_route',
        'uses' => 'admin\AdminPlanningController@reasign_route'
    ])->where('id', '[0-9]+');
    Route::post('/transport/{route_id}/reasign-route', [
        'as' => 'adminPlanning.reasign_route',
        'uses' => 'admin\AdminPlanningController@reasign_route'
    ])->where('id', '[0-9]+');
    Route::post('/transport/download-route-voucher', [
        'as' => 'adminPlanning.download_route_voucher',
        'uses' => 'admin\AdminPlanningController@download_route_voucher'
    ]);
    Route::get('/get-vehicles-ajax', [
        'as' => 'adminPlanning.getVehiclesAjax',
        'uses' => 'admin\AdminPlanningController@get_vehicles_ajax'
    ]);
    Route::get('/get-drivers-ajax', [
        'as' => 'adminPlanning.getDriversAjax',
        'uses' => 'admin\AdminPlanningController@get_drivers_ajax'
    ]);
    Route::post('/assign-transporters', [
        'as' => 'adminPlanning.assignTransporters',
        'uses' => 'admin\AdminPlanningController@assign_transporters'
    ]);
    Route::post('/reassign-transporters', [
        'as' => 'adminPlanning.reassignTransporters',
        'uses' => 'admin\AdminPlanningController@reassign_transporters'
    ]);
    Route::post('/validate-routes', [
        'as' => 'adminPlanning.validateRoutes',
        'uses' => 'admin\AdminPlanningController@validate_routes'
    ]);
    Route::get('/route-map/{id}', [
        'as' => 'adminPlanning.routeMap',
        'uses' => 'admin\AdminPlanningController@show_route_map'
    ]);
    Route::get('/change-city', [
        'as' => 'adminPlanning.getChangeCity',
        'uses' => 'admin\AdminPlanningController@get_change_city'
    ]);
    Route::get('/change-warehouse', [
        'as' => 'adminPlanning.getChangeWarehouse',
        'uses' => 'admin\AdminPlanningController@get_change_warehouse'
    ]);
    Route::get('/change-route', [
        'as' => 'adminPlanning.getInfoRoute',
        'uses' => 'admin\AdminPlanningController@get_info_route'
    ]);
    Route::get('/routes', [
        'as' => 'adminRoutes.index',
        'uses' => 'admin\AdminRouteController@index'
    ]);
    Route::any('/route/add', [
        'as' => 'adminRoutes.add',
        'uses' => 'admin\AdminRouteController@add'
    ]);
    Route::get('/routes/{city_id}/{date}/{warehouse_id}/{shift}/details', [
        'as' => 'adminRoutes.details',
        'uses' => 'admin\AdminRouteController@details'
    ]);
    Route::get('/route/{id}/map', [
        'as' => 'adminRoutes.routeMap',
        'uses' => 'admin\AdminRouteController@route_map'
    ]);
    Route::get('/routes/{route_id}/cost', [
        'as' => 'adminRoutes.routesCost',
        'uses' => 'admin\AdminRouteController@route_cost'
    ])->where('id', '[0-9]+');
    Route::post('/routes/{route_id}/cost', [
        'as' => 'adminRoutes.routeCostSave',
        'uses' => 'admin\AdminRouteController@route_cost_save'
    ])->where('id', '[0-9]+');
    Route::get('/routes/{city_id}/{date}/{warehouse_id}/{shift}/map', [
        'as' => 'adminRoutes.routesMap',
        'uses' => 'admin\AdminRouteController@routes_map'
    ]);
    Route::get('/routes/{city_id}/{date}/{warehouse_id}/{shift}/update', [
        'as' => 'adminRoutes.updateStatus',
        'uses' => 'admin\AdminRouteController@update_status'
    ]);
    Route::post('/routes/update/orders', [
        'as' => 'adminRoutes.joinOrdersToRoute',
        'uses' => 'admin\AdminRouteController@update_orders_route'
    ]);
    Route::post('/route/update/order-sequence', [
        'as' => 'adminRoutes.updateOrderSequence',
        'uses' => 'admin\AdminRouteController@update_order_sequence'
    ]);
    Route::get('/route/update/get-order-delivery-address', [
        'as' => 'adminRoutes.getOrderDeliveryAddress',
        'uses' => 'admin\AdminRouteController@get_order_delivery_address'
    ]);
    Route::post('/route/update/update-order-delivery-address', [
        'as' => 'adminRoutes.updateOrderDeliveryAddress',
        'uses' => 'admin\AdminRouteController@update_order_delivery_address'
    ]);
    Route::post('/route/update/update-vehicle-route', [
        'as' => 'adminRoutes.updateVehicleOrder',
        'uses' => 'admin\AdminRouteController@update_vehicle_orders'
    ]);
    Route::get('/route/{id}/orders/details', [
        'as' => 'adminRoutes.getOrderDetails',
        'uses' => 'admin\AdminRouteController@orders_details'
    ]);
    Route::get('/routes/{city_id}/{date}/{warehouse_id}/{shift}/download', [
        'as' => 'adminRoutes.download',
        'uses' => 'admin\AdminRouteController@download'
    ]);
    Route::post('/order-departure', [
        'as' => 'adminPlanning.pdfOrderDeparture',
        'uses' => 'admin\AdminPlanningController@pdf_order_departure'
    ]);
    Route::post('/routes/update/picking-priority', [
        'as' => 'adminRoutes.updatePickingPriority',
        'uses' => 'admin\AdminRouteController@update_picking_priority'
    ]);
    Route::post('/routes/update/dispatch-time', [
        'as' => 'adminRoutes.updateDispatchTime',
        'uses' => 'admin\AdminRouteController@update_dispatch_time'
    ]);
    Route::post('/routes/update/remove-order', [
        'as' => 'adminRoutes.updateRemoveOrder',
        'uses' => 'admin\AdminRouteController@remove_order'
    ]);
    Route::post('/routes/get-route-list-ajax', [
        'as' => 'adminRoutes.listRoutesAjax',
        'uses' => 'admin\AdminRouteController@get_list_routes_ajax'
    ]);

    Route::post('/routes/change-route-order_ajax', [
        'as' => 'adminRoutes.changeRouteOrderAjax',
        'uses' => 'admin\AdminRouteController@change_route_order_ajax'
    ]);
    Route::get('/routes/download-template-cost', [
        'as' => 'adminRoutes.downloadTemplateCost',
        'uses' => 'admin\AdminRouteController@download_template_cost'
    ]);
    Route::post('/routes/import-costs', [
        'as' => 'adminRoutes.importCosts',
        'uses' => 'admin\AdminRouteController@import_costs'
    ]);
});
//Picking
Route::group(['prefix' => "/{$admin_route}/picking-quality"], function () {
    Route::get('/', [
        'as' => 'AdminPickingQuality.index',
        'uses' => 'admin\AdminPickingQualityController@index'
    ]);
    Route::post('/add', [
        'as' => 'AdminPickingQuality.add',
        'uses' => 'admin\AdminPickingQualityController@add'
    ]);
    Route::post('/update/{id}', [
        'as' => 'AdminPickingQuality.update',
        'uses' => 'admin\AdminPickingQualityController@update'
    ]);
    Route::get('/edit/{id}/{validation}', [
        'as' => 'AdminPickingQuality.edit',
        'uses' => 'admin\AdminPickingQualityController@edit'
    ]);
    Route::get('/{id}/delete/product', [
        'as' => 'AdminPickingQuality.delete',
        'uses' => 'admin\AdminPickingQualityController@delete'
    ]);
    Route::get('/save', [
        'as' => 'AdminPickingQuality.save',
        'uses' => 'admin\AdminPickingQualityController@save'
    ]);
    Route::any('/{id}/validate-product-ajax', [
        'as' => 'AdminPickingQuality.ValidateProductAjax',
        'uses' => 'admin\AdminPickingQualityController@validateProductAjax'
    ]);
    Route::get('/update-validation/{id}', [
        'as' => 'AdminPickingQuality.updateValidation',
        'uses' => 'admin\AdminPickingQualityController@updateValidation'
    ]);
});
//Transport
Route::group(['prefix' => "/{$admin_route}/transporters"], function () {
    Route::get('/', [
        'as' => 'adminTransporter.index',
        'uses' => 'admin\AdminTransporterController@index'
    ]);

    Route::post('/', [
        'as' => 'adminTransporter.index',
        'uses' => 'admin\AdminTransporterController@index'
    ]);

    Route::get('/add', [
        'as' => 'adminTransporter.add',
        'uses' => 'admin\AdminTransporterController@edit'
    ]);

    Route::get('/edit/{id}', [
        'as' => 'adminTransporter.edit',
        'uses' => 'admin\AdminTransporterController@edit'
    ]);

    Route::get('/delete/{id}', [
        'as' => 'adminTransporter.delete',
        'uses' => 'admin\AdminTransporterController@delete'
    ]);

    Route::post('/save', [
        'as' => 'adminTransporter.save',
        'uses' => 'admin\AdminTransporterController@save'
    ]);

    Route::get('/drivers', [
        'as' => 'adminTransporter.drivers',
        'uses' => 'admin\AdminTransporterController@drivers'
    ]);

    Route::post('/drivers', [
        'as' => 'adminTransporter.drivers',
        'uses' => 'admin\AdminTransporterController@drivers'
    ]);

    Route::get('/drivers/add', [
        'as' => 'adminTransporter.addDriver',
        'uses' => 'admin\AdminTransporterController@edit_driver'
    ]);

    Route::post('/drivers/save', [
        'as' => 'adminTransporter.save_driver',
        'uses' => 'admin\AdminTransporterController@save_driver'
    ]);

    Route::get('/drivers/edit/{id}', [
        'as' => 'adminTransporter.edit_driver',
        'uses' => 'admin\AdminTransporterController@edit_driver'
    ]);

    Route::get('/drivers/delete/{id}', [
        'as' => 'adminTransporter.delete_driver',
        'uses' => 'admin\AdminTransporterController@delete_driver'
    ]);


    Route::get('/assistants', [
        'as' => 'adminAssistant.assistants',
        'uses' => 'admin\transporters\AssistantController@index'
    ]);

    Route::post('/assistants', [
        'as' => 'adminAssistant.assistants',
        'uses' => 'admin\transporters\AssistantController@index'
    ]);


    Route::get('/assistants/add', [
        'as' => 'adminAssistant.addAssistant',
        'uses' => 'admin\transporters\AssistantController@edit_assistant'
    ]);

    Route::post('/assistants/save', [
        'as' => 'adminAssistant.save_assistant',
        'uses' => 'admin\transporters\AssistantController@save_assistant'
    ]);

    Route::get('/assistants/edit/{id}', [
        'as' => 'adminAssistant.edit_assistant',
        'uses' => 'admin\transporters\AssistantController@edit_assistant'
    ]);

    Route::get('/assistants/delete/{id}', [
        'as' => 'adminAssistant.delete_assistant',
        'uses' => 'admin\transporters\AssistantController@delete_assistant'
    ]);

    Route::get('/vehicle/balance', [
        'as' => 'adminVehiclesBalance.index',
        'uses' => 'admin\AdminVehicleBalanceController@index'
    ]);

    Route::get('/vehicle/balance/{id}', [
        'as' => 'adminVehiclesBalance.edit',
        'uses' => 'admin\AdminVehicleBalanceController@edit'
    ]);

    Route::post('/vehicles/balance/export-ajax', [
        'as' => 'adminVehicles.balanceExport',
        'uses' => 'admin\AdminVehicleBalanceController@export_balance_ajax'
    ]);

    Route::post('/vehicle/balance/export-movement-ajax', [
        'as' => 'adminVehiclesBalance.exportMovement',
        'uses' => 'admin\AdminVehicleBalanceController@export_movement_ajax'
    ]);

    Route::post('/vehicle/balance/save-movement-ajax', [
        'as' => 'adminVehiclesBalance.saveMovement',
        'uses' => 'admin\AdminVehicleBalanceController@save_movement_ajax'
    ]);

    Route::post('/vehicle/balance/delete-movement-ajax', [
        'as' => 'adminVehiclesBalance.deleteMovement',
        'uses' => 'admin\AdminVehicleBalanceController@delete_movement_ajax'
    ]);

    Route::get('/vehicles', [
        'as' => 'adminTransporter.vehicles',
        'uses' => 'admin\AdminTransporterController@vehicles'
    ]);

    Route::post('/vehicles', [
        'as' => 'adminTransporter.vehicles',
        'uses' => 'admin\AdminTransporterController@vehicles'
    ]);

    Route::get('/vehicles/report', [
        'as' => 'adminTransporter.report_extend',
        'uses' => 'admin\AdminTransporterController@report_extend'
    ]);

    Route::group(['prefix' => '/vehicles/news'], function () {
        Route::get('/', [
            'as' => 'adminVehicleNews.index',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@index'
        ]);
        Route::get('/report', [
            'as' => 'adminVehicleNews.report_extend',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@report_extend'
        ]);
        Route::get('/{id}', [
            'as' => 'adminVehicleNews.view',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@view'
        ])->where('id', '[0-9]+');
        Route::get('/{id}/vehicle-news-ajax', [
            'as' => 'adminVehicleNews.get_news_ajax',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@news_ajax'
        ])->where('id', '[0-9]+');
        Route::post('/{id}/add-news-ajax', [
            'as' => 'adminVehicleNews.add_news_ajax',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@add_news_ajax'
        ])->where('id', '[0-9]+');
        Route::post('/{id}/delete-news-ajax', [
            'as' => 'adminVehicleNews.delete_news_ajax',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@delete_news_ajax'
        ])->where('id', '[0-9]+');
        Route::get('/{id}/report-ajax', [
            'as' => 'adminVehicleNews.report',
            'uses' => 'admin\transporters\vehicle_news\VehicleNewsController@report_ajax'
        ])->where('id', '[0-9]+');
    });

    Route::group(['prefix' => '/transporter/news'], function () {
        Route::get('/', [
            'as' => 'adminTransporterNews.index',
            'uses' => 'admin\transporters\TransporterNewsController@index'
        ]);
        Route::post('/users', [
            'as' => 'adminTransporterNews.getUsers',
            'uses' => 'admin\transporters\TransporterNewsController@get_users'
        ]);
        Route::post('/config-users', [
            'as' => 'adminTransporterNews.getUsersConfig',
            'uses' => 'admin\transporters\TransporterNewsController@config_get_users'
        ]);
        Route::post('/save-assignment', [
            'as' => 'adminTransporterNews.saveAssignment',
            'uses' => 'admin\transporters\TransporterNewsController@save_assignment'
        ]);
        Route::post('/get-form-resolv-new', [
            'as' => 'adminTransporterNews.getFormResolvNew',
            'uses' => 'admin\transporters\TransporterNewsController@get_form_resolv'
        ]);
        Route::post('/save-form-resolv-new', [
            'as' => 'adminTransporterNews.saveFormResolvNew',
            'uses' => 'admin\transporters\TransporterNewsController@save_form_resolv'
        ]);
        Route::post('/export-report-ajax', [
            'as' => 'adminTransporterNews.exportReportAjax',
            'uses' => 'admin\transporters\TransporterNewsController@report_ajax'
        ]);
    });

    Route::get('/vehicle/add', [
        'as' => 'adminTransporter.add_vehicle',
        'uses' => 'admin\AdminTransporterController@edit_vehicle'
    ]);

    Route::post('/vehicle/save', [
        'as' => 'adminTransporter.save_vehicle',
        'uses' => 'admin\AdminTransporterController@save_vehicle'
    ]);

    Route::get('/vehicle/{id}/cost-report', [
        'as' => 'adminTransporter.cost_report',
        'uses' => 'admin\AdminTransporterController@cost_report'
    ]);

    Route::get('/vehicle/edit/{id}', [
        'as' => 'adminTransporter.edit_vehicle',
        'uses' => 'admin\AdminTransporterController@edit_vehicle'
    ]);

    Route::get('/vehicle/delete/{id}', [
        'as' => 'adminTransporter.delete_vehicle',
        'uses' => 'admin\AdminTransporterController@delete_vehicle'
    ]);

    Route::post('/vehicle/transporter', [
        'as' => 'adminTransporter.transporter',
        'uses' => 'admin\AdminTransporterController@ajax_transporter'
    ]);
    Route::get('/tracing', [
        'as' => 'adminVehicleTracing.vehiclesTracing',
        'uses' => 'admin\AdminVehicleTracingController@index'
    ]);

    Route::post('/get-transporters-ajax', [
        'as' => 'adminVehicleTracing.getTransportersAjax',
        'uses' => 'admin\AdminVehicleTracingController@get_transporters'
    ]);

    Route::post('/get-drivers-ajax', [
        'as' => 'adminVehicleTracing.getDriversAjax',
        'uses' => 'admin\AdminVehicleTracingController@get_drivers'
    ]);

    Route::get('/tracing/map/record/{city_id}/{vehicle_id}', [
        'as' => 'adminVehicleTracing.mapTracingRecord',
        'uses' => 'admin\AdminVehicleTracingController@map_tracing_record'
    ]);

    Route::get('/tracing/map/{city_id}/{vehicle_id}/{route_id}', [
        'as' => 'adminVehicleTracing.mapTracing',
        'uses' => 'admin\AdminVehicleTracingController@map_tracing'
    ]);

    Route::get('/ajax-transporters-city', [
        'as' => 'adminTransporter.ajax_transporters_city',
        'uses' => 'admin\AdminTransporterController@ajax_transporters_per_city'
    ]);

    Route::get('/ajax-drivers-transporter', [
        'as' => 'adminTransporter.ajax-drivers-transporter',
        'uses' => 'admin\AdminTransporterController@ajax_drivers_per_transporter'
    ]);

    Route::get('/ajax-vehicles-transporter', [
        'as' => 'adminTransporter.ajax-vehicles-transporter',
        'uses' => 'admin\AdminTransporterController@ajax_vehicles_per_transporter'
    ]);

    Route::group(['prefix' => '/validation'], function () {
        Route::get('/', [
            'as' => 'adminTransporter.validation.index',
            'uses' => 'admin\transporters\ValidationController@index'
        ]);

        Route::get('/get-warehouses-by-city-ajax', [
            'as' => 'adminTransporter.validation.get_warehouses_by_city_ajax',
            'uses' => 'admin\transporters\ValidationController@get_warehouses_by_city_ajax'
        ]);

        Route::get('/get-routes-by-warehouse-ajax', [
            'as' => 'adminTransporter.validation.get_routes_by_warehouse_ajax',
            'uses' => 'admin\transporters\ValidationController@get_routes_by_warehouse_ajax'
        ]);

        Route::get('/get-orders-by-routes-ajax', [
            'as' => 'adminTransporter.validation.get_orders_by_routes_ajax',
            'uses' => 'admin\transporters\ValidationController@get_orders_by_routes_ajax'
        ]);

        Route::post('/{order_id}/add-seals-to-order', [
            'as' => 'adminTransporter.validation.add_seals_to_order',
            'uses' => 'admin\transporters\ValidationController@add_seals_to_order'
        ]);
    });
});
//Planning
Route::group(['prefix' => "/{$admin_route}/dispatch"], function () {
    Route::get('/dispatch', [
        'as' => 'adminDispatchPlanning.transport',
        'uses' => 'admin\dispatch\DivideRouteController@index'
    ]);
    Route::get('/dispatch/{route_id}/divide-route', [
        'as' => 'adminDispatchPlanning.divide_route',
        'uses' => 'admin\dispatch\DivideRouteController@divideRoute'
    ])->where('id', '[0-9]+');
    Route::post('/dispatch/{route_id}/divide-route', [
        'as' => 'adminDispatchPlanning.divide_route',
        'uses' => 'admin\dispatch\DivideRouteController@divideRoute'
    ])->where('id', '[0-9]+');
    Route::post('/validate-routes', [
        'as' => 'adminDispatchPlanning.validateRoutes',
        'uses' => 'admin\dispatch\DivideRouteController@validateRoutes'
    ]);
    Route::get('/get-drivers-ajax', [
        'as' => 'adminDispatchPlanning.getDriversAjax',
        'uses' => 'admin\dispatch\DivideRouteController@getDriversAjax'
    ]);

    Route::post('/reassign-transporters', [
        'as' => 'adminDispatchPlanning.reassignTransporters',
        'uses' => 'admin\dispatch\DivideRouteController@reassignTransporters'
    ]);

});
//restringir acceso al dashboard por merqueo.com
if (!Config::get('app.debug') && Request::root() == Config::get('app.url')) {
    Route::group(['prefix' => "/{$admin_route}"], function () {
        Route::get('/', function () {
            return App::abort(404);
        });

        Route::get('/login', function () {
            return App::abort(404);
        });
    });
}
