<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
     */
    'name' => getenv('APP_NAME'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
     */

    'debug' => getenv('APP_DEBUG'),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
     */

    'url' => getenv('APP_URL'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
     */

    'timezone' => 'America/Bogota',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
     */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
     */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
     */

    'key' => 'Iw1v932glB7LVLYbCr7KBPQEGkpst117',

    'cipher' => MCRYPT_RIJNDAEL_128,

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
     */

    'providers' => [

        'Illuminate\Foundation\Providers\ArtisanServiceProvider',
        'Illuminate\Auth\AuthServiceProvider',
        'Illuminate\Cache\CacheServiceProvider',
        'Illuminate\Session\CommandsServiceProvider',
        'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
        'Illuminate\Routing\ControllerServiceProvider',
        'Illuminate\Cookie\CookieServiceProvider',
        'Illuminate\Database\DatabaseServiceProvider',
        'Illuminate\Encryption\EncryptionServiceProvider',
        'Illuminate\Filesystem\FilesystemServiceProvider',
        'Illuminate\Hashing\HashServiceProvider',
        'Illuminate\Html\HtmlServiceProvider',
        'Illuminate\Log\LogServiceProvider',
        'Illuminate\Mail\MailServiceProvider',
        'Illuminate\Database\MigrationServiceProvider',
//        'Illuminate\Pagination\PaginationServiceProvider',
        'Illuminate\Queue\QueueServiceProvider',
        'Illuminate\Redis\RedisServiceProvider',
        'Illuminate\Remote\RemoteServiceProvider',
        'Illuminate\Auth\Reminders\ReminderServiceProvider',
        'Illuminate\Database\SeedServiceProvider',
        'Illuminate\Translation\TranslationServiceProvider',
        'Illuminate\Validation\ValidationServiceProvider',
        'Illuminate\View\ViewServiceProvider',
        'Illuminate\Workbench\WorkbenchServiceProvider',
        //'Artdarek\Pusherer\PushererServiceProvider',
        'LaravelCaptcha\Providers\LaravelCaptchaServiceProvider',
        'Aws\Laravel\AwsServiceProvider',
        'Breadcrumb\BreadcrumbServiceProvider',
        'Anouar\Fpdf\FpdfServiceProvider',
        //'Watson\Sitemap\SitemapServiceProvider',
        'Maatwebsite\Excel\ExcelServiceProvider',
        'Merqueo\Backend\Auth\AuthServiceProvider',
        Merqueo\Leanplum\LeanplumClientServiceProvider::class,
        Merqueo\Firebase\FirebaseCloudMessageClientServiceProvider::class,
        \AbTesting\Optimizely\OptimizelyServiceProvider::class,
        \Merqueo\Backend\Auth\AccountKit\AccountKitServiceProvider::class,
        \Merqueo\Facebook\FacebookServiceProvider::class,
        \Merqueo\Zendesk\ZendeskServiceProvider::class,
        \Merqueo\Backend\Paginator\PaginationServiceProvider::class,
        \app\providers\UsecasesServiceProvider::class,
        \app\providers\RepositoriesServiceProvider::class,
        Campaigns\Providers\CampaignServiceProvider::class,
        NewSiteServiceProvider::class,
        ForceArraySessionServiceProvider::class,
        ContractsServiceProvider::class

    ],

    /*
    |--------------------------------------------------------------------------
    | Service Provider Manifest
    |--------------------------------------------------------------------------
    |
    | The service provider manifest is used by Laravel to lazy load service
    | providers which are not needed for each request, as well to keep a
    | list of all of the services. Here, you may set its storage spot.
    |
     */

    'manifest' => storage_path() . '/meta',

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
     */

    'aliases' => [

        'App' => 'Illuminate\Support\Facades\App',
        'Artisan' => 'Illuminate\Support\Facades\Artisan',
        'Auth' => 'Illuminate\Support\Facades\Auth',
        'Blade' => 'Illuminate\Support\Facades\Blade',
        'Cache' => 'Illuminate\Support\Facades\Cache',
        'ClassLoader' => 'Illuminate\Support\ClassLoader',
        'Config' => 'Illuminate\Support\Facades\Config',
        'Controller' => 'Illuminate\Routing\Controller',
        'Cookie' => 'Illuminate\Support\Facades\Cookie',
        'Crypt' => 'Illuminate\Support\Facades\Crypt',
        'DB' => 'Illuminate\Support\Facades\DB',
        'Eloquent' => 'Illuminate\Database\Eloquent\Model',
        'Event' => 'Illuminate\Support\Facades\Event',
        'File' => 'Illuminate\Support\Facades\File',
        'Form' => 'Illuminate\Support\Facades\Form',
        'Hash' => 'Illuminate\Support\Facades\Hash',
        'HTML' => 'Illuminate\Support\Facades\HTML',
        'Input' => 'Illuminate\Support\Facades\Input',
        'Lang' => 'Illuminate\Support\Facades\Lang',
        'Log' => 'Illuminate\Support\Facades\Log',
        'Mail' => 'Illuminate\Support\Facades\Mail',
        'Paginator' => 'Illuminate\Support\Facades\Paginator',
        'Password' => 'Illuminate\Support\Facades\Password',
        'Queue' => 'Illuminate\Support\Facades\Queue',
        'Redirect' => 'Illuminate\Support\Facades\Redirect',
        'Redis' => 'Illuminate\Support\Facades\Redis',
        'Request' => 'Illuminate\Support\Facades\Request',
        'Response' => 'Illuminate\Support\Facades\Response',
        'Route' => 'Illuminate\Support\Facades\Route',
        'Schema' => 'Illuminate\Support\Facades\Schema',
        'Seeder' => 'Illuminate\Database\Seeder',
        'Session' => 'Illuminate\Support\Facades\Session',
        'SoftDeletingTrait' => 'Illuminate\Database\Eloquent\SoftDeletingTrait',
        'SSH' => 'Illuminate\Support\Facades\SSH',
        'Str' => 'Illuminate\Support\Str',
        'URL' => 'Illuminate\Support\Facades\URL',
        'Validator' => 'Illuminate\Support\Facades\Validator',
        'View' => 'Illuminate\Support\Facades\View',
        'AWS' => 'Aws\Laravel\AwsFacade',
        'Breadcrumb' => 'Breadcrumb\Facades\Breadcrumb',
        'Fpdf' => 'Anouar\Fpdf\Facades\Fpdf',
        'Excel' => 'Maatwebsite\Excel\Facades\Excel',
        'BackendAuth' => 'Merqueo\Backend\Facades\AuthBackend',
        'Leanplum' => Merqueo\Leanplum\LeanplumFacade::class,
        'FirebaseCloudMessage' => Merqueo\Firebase\FirebaseCloudMessageFacade::class,
        'Optimizely' => \AbTesting\Optimizely\OptimizelyFacade::class,
        'MerqueoFacebook' => \Merqueo\Facebook\FacebookFacade::class,
        'Zendesk' => \Merqueo\Zendesk\ZendekFacade::class,
    ],

    'force_schema_url' => getenv('APP_FORCE_SCHEMA_URL'),
    'api_log' => getenv('APP_API_LOG'),
    'url_web' => getenv('APP_URL_WEB'),
    'url_osrm' => getenv('OSRM_COLOMBIA_URL'),
    'api' => [
        'key' => getenv('APP_API.KEY'),
        'public_key' => getenv('APP_API.PUBLIC_KEY'),
        'iv' => getenv('APP_API.IV'),
    ],

    'facebook_app_id' => getenv('APP_FACEBOOK_APP_ID'),
    'facebook_ak_secret' => getenv('APP_FACEBOOK_AK_SECRET'),
    'facebook_api_graph_secret' => getenv('APP_FACEBOOK_AG_SECRET'),
    'facebook_ak_access_token' => getenv('APP_FACEBOOK_AK_ACCESS_TOKEN'),

    'google_api_key' => getenv('APP_GOOGLE_API_KEY'),
    'planning_google_api_key' => getenv('PLANNING_GOOGLE_API_KEY'),

    'google_tag_manager' => [
        'container_id' => getenv('APP_GOOGLE_TAG_MANAGER.CONTAINER_ID'),
    ],

    'google_cloud_messaging' => [
        'url' => getenv('APP_GOOGLE_CLOUD_MESSAGING.URL'),
        'key' => getenv('APP_GOOGLE_CLOUD_MESSAGING.KEY'),
    ],

    'apple_push_notification_service' => [
        'url' => getenv('APP_APPLE_PUSH_NOTIFICATION_SERVICE.URL'),
        'certificate' => getenv('APP_APPLE_PUSH_NOTIFICATION_SERVICE.CERTIFICATE'),
    ],

    'geoapps_credentials' => [
        'key' => getenv('APP_GEOAPPS_CREDENTIALS.KEY'),
        'secret' => getenv('APP_GEOAPPS_CREDENTIALS.SECRET'),
    ],

    'sitimapas_token' => getenv('APP_SITIMAPAS_TOKEN'),

    'pusherer' => [
        'picker' => [
            'app_id' => getenv('APP_PUSHERER.PICKER.APP_ID'),
            'key' => getenv('APP_PUSHERER.PICKER.KEY'),
            'secret' => getenv('APP_PUSHERER.PICKER.SECRET'),
            'cluster' => getenv('APP_PUSHERER.PICKER.CLUSTER'),
        ],
        'transporter' => [
            'app_id' => getenv('APP_PUSHERER.TRANSPORTER.APP_ID'),
            'key' => getenv('APP_PUSHERER.TRANSPORTER.KEY'),
            'secret' => getenv('APP_PUSHERER.TRANSPORTER.SECRET'),
            'cluster' => getenv('APP_PUSHERER.TRANSPORTER.CLUSTER'),
        ],
    ],

    'firebase' => [
        'general' =>
        [
            'url' => getenv('APP_FIREBASE.GENERAL.URL'),
            'key' => getenv('APP_FIREBASE.GENERAL.KEY'),
            'push_key' => getenv('APP_FIREBASE.GENERAL.PUSH_KEY'),
            'token' => getenv('APP_FIREBASE.GENERAL.TOKEN'),
            'uid' => getenv('APP_FIREBASE.GENERAL.UID'),
            'email' => getenv('APP_FIREBASE.GENERAL.CLIENT_EMAIL'),
            'private_key' => getenv('APP_FIREBASE.GENERAL.PRIVATE_KEY'),
            'domain' => getenv('APP_FIREBASE.GENERAL.DOMAIN'),
            'storage_bucket' => getenv('APP_FIREBASE.GENERAL.STORAGE_BUCKET'),
            'dinamic_link_domain' => getenv('APP_FIREBASE.GENERAL.DYNAMIC_LINK_DOMAIN'),
        ],
    ],

    'aws' => [
        'key' => getenv('APP_AWS.KEY'),
        'secret' => getenv('APP_AWS.SECRET'),
        'region' => getenv('APP_AWS.REGION'),
        'bucket_name' => getenv('APP_AWS.BUCKET_NAME'),
        'cloudfront_url' => getenv('APP_AWS.CLOUDFRONT_URL'),
        'distribution_id' => getenv('APP_AWS.DISTRIBUTION_ID'),
        'elasticsearch' => [
            'is_enable' => 1,
            'host' => getenv('APP_AWS.ELASTICSEARCH.HOST'),
        ],
        'sns' => [
          'queue_order_confirmed' => getenv('APP_AWS.SNS.TOPIC_ARN_ORDER_CONFIRMED'),
        ],        
    ],

    'public_key' => getenv('APP_PUBLIC_KEY'),
    'private_key' => getenv('APP_PRIVATE_KEY'),

    'debug_payment' => getenv('APP_DEBUG_PAYMENT'),
    'absolute_path_images' => getenv('APP_ABSOLUTE_PATH_IMAGES'),
    'absolute_path_ftp' => getenv('APP_ABSOLUTE_PATH_FTP'),
    'absolute_path_ftp_local' => getenv('APP_ABSOLUTE_PATH_FTP_LOCAL'),

    'tpaga' => [
        'url' => getenv('APP_TPAGA.URL'),
        'api_key' => getenv('APP_TPAGA.API_KEY'),
    ],

    'mandrill' => [
        'key' => getenv('APP_MANDRILL.KEY'),
        'url' => getenv('APP_MANDRILL.URL'),
    ],

    'payu' => [
        'url' => getenv('APP_PAYU.URL'),
        'url_reports' => getenv('APP_PAYU.URL_REPORTS'),
        'api_key' => getenv('APP_PAYU.API_KEY'),
        'api_login' => getenv('APP_PAYU.API_LOGIN'),
        'account_id' => getenv('APP_PAYU.API_ACCOUNTID'),
        'precharge_account_id' => getenv('APP_PAYU.API_ACCOUNTID_PRECHARGE'),
        'merchant_id' => getenv('APP_PAYU.API_MERCHANTID'),
        'test' => getenv('APP_PAYU.TEST'),
        'token_length' => 36,
        'minimum_charge' => 500,
        'precharge_enable' => 1,
    ],

    'aldeamo' => [
        'ip' => getenv('APP_ALDEAMO.IP'),
        'port' => getenv('APP_ALDEAMO.PORT'),
        'username' => getenv('APP_ALDEAMO.USERNAME'),
        'password' => getenv('APP_ALDEAMO.PASSWORD'),
    ],

    'events' => [
        'big_query_service' => [
            'url' => getenv('APP_BIG_QUERY_TRACK'),
        ],
    ],

    'soat' => [
        'url' => getenv('APP_SOAT.URL'),
        'amount_buy' => getenv('APP_SOAT.AMOUNT_BUY'),
        'auth' => getenv('APP_SOAT.AUTH'),
        'test' => getenv('APP_SOAT.TEST'),
    ],

    'jwt' => [
        'key' => getenv('APP_JWT.KEY'),
        'expire' => getenv('APP_JWT.EXPIRE'),
    ],

    'merqueo' => [
        'show_cigarettes' => getenv('APP_MERQUEO.SHOW_CIGARETTES') === null
        ? true : getenv('APP_MERQUEO.SHOW_CIGARETTES'),
        'referred_limit' => getenv('APP_MERQUEO.REFERRED_LIMIT'),
    ],

    /*'movilred' => [
    'customerId'      => getenv('MOVILRED.CUSTOMERID'),
    'userName'        => getenv('MOVILRED.USERNAME'),
    'deviceCode'      => getenv('MOVILRED.DEVICECODE'),
    'passwordHash'    => getenv('MOVILRED.PASSWORDHASH'),
    'originalAddress' => getenv('MOVILRED.ORIGINALADDRESS'),
    'channel'         => getenv('MOVILRED.CHANNEL'),
    'systemId'        => getenv('MOVILRED.SYSTEMID'),
    'secretKey'       => getenv('MOVILRED.SECRETKEY'),
    'requestSource'   => getenv('MOVILRED.REQUESTSOURCE'),
    'host'            => getenv('MOVILRED.HOST'),
    ],*/

    'transporters' => [
        'maximum_orders_pending' => 1,
        'maximum_orders_pending_with_news' => 2,
        'location' => [
            'url' => !getenv('APP_DEBUG') ? 'https://location.merqueo.com' : 'http://location.supermercap.com',
            'hours' => 10,
        ],
        'seconds_to_add_per_order' => 780,
        'app_version' => 103,
    ],

    'dark_supermarket' => [
        'email_accounts' => [
            'warehouse' => [
                'name' => 'Alex Osorio',
                'email' => 'aosorio@merqueo.com',
            ],
            'click_delivery' => [
                'name' => 'saldana@domicilios.com',
                'email' => 'Santiago Aldana',
            ],
        ],
        'vehicle_id' => 574,
        'driver_id' => 920,
        'minimum_order_amount' => 0,
    ],

    'delivery_express' => [
        'base_uri' => getenv('APP_API_MERQUEO_CREATE_SERVICE.BASE_URI'),
        'path' => getenv('APP_API_MERQUEO_CREATE_SERVICE.PATH'),
        'key' => getenv('APP_API_MERQUEO_CREATE_SERVICE.KEY'),
    ],

    'app_messages' => array(
        'score_order_not_login' => 'Para calificar el pedido debes estar autenticado.',
    ),

    'geolocation_method' => 'SitiMapas',
    'website_name' => 'Merqueo',
    'chat_enabled' => 0,
    'admin_email' => 'servicioalcliente@merqueo.com',
    'admin_phone' => '7561938',
    'error_email' => 'jsanchez@merqueo.com',
    'android_url' => 'https://play.google.com/store/apps/details?id=com.merqueo&hl=es',
    'android_current_app_version' => '1.1.4',
    'ios_url' => 'https://itunes.apple.com/us/app/id1080127941',
    'ios_current_app_version' => '1.0.7',
    'facebook_url' => 'https://www.facebook.com/merqueo',
    'twitter_url' => 'https://twitter.com/merqueocol',
    'instagram_url' => 'https://instagram.com/merqueocol',

    'referred' => [
        'limit' => 50,
        'free_delivery_days' => 15,
        'referred_by_free_delivery_days' => 15,
        'amount' => 10000,
        'referred_by_amount' => 5000,
        'amount_expiration_days' => 30,
        'share_image_url' => 'https://merqueo.com/assets/img/regalo-referido.jpg',
        'share_title' => 'Te regalo $10.000 en créditos para que pidas lo que quieras! Descarga la app de Merqueo, haz tu primer mercado e incluye mi código antes de finalizar tu pedido',
        'share_description' => 'Te regalo $10.000 en créditos para que pidas lo que quieras! Descarga la app de Merqueo, haz tu primer mercado e incluye mi código antes de finalizar tu pedido.',
        'remember_days' => 15,
    ],

    'order_score' => [
        'bad' => [
            'title' => '¿Que podriamos mejorar?',
            'options' => [
                'Tiempo de entrega',
                'Atención del repartidor',
                'Presentación del repartidor',
                'Calidad de los productos',
                'Productos faltantes',
                'Productos próximos a expirar',
            ],
        ],
        'good' => [
            'title' => '¿Qué fue lo que más te gustó?',
            'options' => [
                'El servicio del repartidor',
                'Los precios',
                'La calidad de los productos',
            ],
        ],
    ],

    'minimum_discount_amount' => 30000,
    'suspect_credit_card_order_amount' => 500000,
    'suspect_max_order_amount' => 1000000,
    'suspect_max_order_discount_amount' => 200000,
    'minimum_promo_days' => 3,
    'marketplace_delivery_days' => 3,
    'iva' => 19,
    'download_directory' => 'app/storage/downloads/',
    'download_directory_temp' => 'app/storage/downloads/tmp',
    'base_quantity_orders_for_type_route' => 18,
    'bi_query_report_mailing_list' => 'acastro@merqueo.com',
    'dashboard_admin_id' => 64,

    'log' => [
        'store_product_warehouse_log' => true,
    ],

    'slots' => [
        'cache_driver' => getenv('APP_SLOTS_CACHE_DRIVER') ?: 'file',
    ],

    'new_site_url' => getenv('APP_MERQUEO.NEW_SITE_URL') ?: 'https://merqueo.com/v2',
    'new_site_api' => getenv('APP_MERQUEO.NEW_SITE_API') ?: 'https://api-app-product.merqueo.com/api/2.0',
    'leanplum' => [
        'update_delay' => getenv('APP_LEANPLUM_UPDATE_DELAY') ?: 1,
    ],
    'bincodes' => [
        'url' => getenv('APP_BINCODES.URL'),
        'apiKey' => getenv('APP_BINCODES.API_KEY'),
        'allowed_bins' => [ 532004 ]
    ],
    'degub_sms' => getenv('APP_DEBUG_SMS', false),
    'payment' => [
        'url' => getenv('PAYMENTS_API_URL')
    ]
];
