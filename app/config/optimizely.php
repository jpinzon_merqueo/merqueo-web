<?php

// En el caso de que se encuentre configurado el contenido del archivo se
// omite la url; en caso contrario se usara la url para obtener los datos
// en cada petición (No apto para ambientes en producción).
return [
    'file_url' => getenv('APP_OPTIMIZELY.URL_FILE'),
    'file_content' => getenv('APP_OPTIMIZELY.FILE_CONTENT'),
];
