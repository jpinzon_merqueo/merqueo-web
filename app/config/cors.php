<?php

return [
    'allow_origin' => getenv('CORS_ORIGIN') ?: '*',
    'methods' => getenv('CORS_METHODS') ?: 'POST, GET, OPTIONS, PUT, DELETE',
    'credentials' => getenv('CORS_CREDENTIALS') ?: 'true',
    'max_age' => getenv('CORS_MAX_AGE') ?: 86400,
    'headers' => getenv('CORS_HEADERS') ?: 'Content-Type, Authorization, X-Requested-With',
];
