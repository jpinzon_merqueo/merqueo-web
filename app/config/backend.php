<?php

return array(

    'auth' => array(
        'driver' => 'eloquent',
        'model'  => 'Admin',
        'table'  => 'admin',
    ),

    'session' => array(
        'table' => 'admin_sessions',
        'cookie' => 'backend_session',
        'path' => '/admin',
    ),

);
