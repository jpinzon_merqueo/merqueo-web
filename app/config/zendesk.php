<?php

return [
    'user' => getenv('APP_ZENDESK.USERNAME'),
    'token' => getenv('APP_ZENDESK.TOKEN'),
    'subdomain' => getenv('APP_ZENDESK.SUBDOMAIN'),
];
