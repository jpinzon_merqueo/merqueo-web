<?php

// Datos de configuración de Leanplum, se encuentran en
// la sección Dashboard > App settings > Keys & Settings .

return [
    'app_id' => getenv('APP_LEANPLUM.APP_ID'),
    'key' => getenv('APP_LEANPLUM.KEY'),
    'dev_mode' => getenv('APP_LEANPLUM.DEV_MODE'),
];
