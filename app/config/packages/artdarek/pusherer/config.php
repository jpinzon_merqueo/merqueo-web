<?php

return array(

    /**
     * App id
     */
    'app_id' => getenv('APP_PUSHERER.TRANSPORTER.APP_ID'),

    /**
     * App key
     */
    'key' => getenv('APP_PUSHERER.TRANSPORTER.KEY'),

    /**
     * App Secret
     */
    'secret' => getenv('APP_PUSHERER.TRANSPORTER.SECRET')

);