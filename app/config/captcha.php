<?php
// BotDetect PHP Captcha configuration options

$LBD_CaptchaConfig = CaptchaConfiguration::GetSettings();

$LBD_CaptchaConfig->CodeLength = 6;
$LBD_CaptchaConfig->ImageWidth = 250;
$LBD_CaptchaConfig->ImageHeight = 50;
//$LBD_CaptchaConfig->SoundIconUrl = 'https://merqueo.com/assets/img/audio.png';
$LBD_CaptchaConfig->SoundTooltip = 'Escuchar el código CAPTCHA';
//$LBD_CaptchaConfig->ReloadIconUrl = 'https://merqueo.com/assets/img/refresh.png';
$LBD_CaptchaConfig->ReloadTooltip = 'Cambiar el código CAPTCHA';