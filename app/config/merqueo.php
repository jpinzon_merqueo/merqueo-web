<?php

// Datos de configuración de acuerdo al país

return [
    'country' => [
        'co' => [
            'website_name' => 'Merqueo',
            'contact_phone' => '7561938',
            'android_url' => 'https://play.google.com/store/apps/details?id=com.merqueo&hl=es',
            'ios_url' => 'https://itunes.apple.com/us/app/id1080127941',
            'facebook_url' => 'https://www.facebook.com/merqueo',
            'twitter_url' => 'https://twitter.com/merqueocol',
            'instagram_url' => 'https://www.instagram.com/merqueocol',
            'city_referrer' => 'Bogotá, Colombia',
            'web_view' => 'www.merqueo.com',
            'format_value' => ['symbol' => '$', 'decimals' => 0, 'dec_point' => ',', 'thousands_sep' => '.']
        ],
        'mx' => [
            'website_name' => 'Merqueo',
            'contact_phone' => '#',
            'android_url' => 'https://play.google.com/store/apps/details?id=com.merqueo&hl=es',
            'ios_url' => 'https://itunes.apple.com/us/app/id1080127941',
            'facebook_url' => 'https://www.facebook.com/Merqueomx',
            'twitter_url' => '#',
            'instagram_url' => 'https://www.instagram.com/merqueomx/',
            'city_referrer' => 'Ciudad de México, México',
            'web_view' => 'www.merqueo.com.mx',
            'format_value' => ['symbol' => '$', 'decimals' => 2, 'dec_point' => '.', 'thousands_sep' => '.']
        ],
    ],
];
