<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => array(
		'domain' => '',
		'secret' => '',
	),

	'mandrill' => array(
		'secret' => 'U51KV0XBREX44zvIS0Ogqg',
	),

	'stripe' => array(
		'model'  => 'User',
		'secret' => '',
	),

	'infobip' => [
		'url' => getenv('INFOBIP_URL'),
		'key' => getenv('INFOBIP_KEY'),
	],
	
	'dashboard' => [
		'url' => getenv('DASHBOARD_URL'),
	]
);
