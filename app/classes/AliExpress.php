<?php

use AliexApi\Configuration\GenericConfiguration, AliexApi\Operations\GetProductDetail;
use AliexApi\AliexIO;
use AliexApi\Operations\ListProducts;

class AliExpress
{
    private $conf;
    private $username_translator_bi;
    private $password_translator_bi;
    private $resource_translator_bi;

    public function __construct()
    {
        $this->conf = new GenericConfiguration();
        $this->conf
            ->setApiKey('88232')
            ->setTrackingKey('M3rQu30Ap1KeY')
            ->setDigitalSign('zpiFpxdOdv');
        $this->username_translator_bi = 'de1f46bc-cc77-4eee-9bd5-4d37f46d885f';
        $this->password_translator_bi = 'eteShb4C0fKw';
        $this->resource_translator_bi = 'https://gateway.watsonplatform.net/language-translator/api/v2/translate';
    }

    public function getTRM()
    {
        try {
            $soap = new soapclient("https://www.superfinanciera.gov.co/SuperfinancieraWebServiceTRM/TCRMServicesWebService/TCRMServicesWebService?WSDL", array(
                'soap_version'   => SOAP_1_1,
                'trace' => 1,
                "location" => "http://www.superfinanciera.gov.co/SuperfinancieraWebServiceTRM/TCRMServicesWebService/TCRMServicesWebService",
            ));
            $response = $soap->queryTCRM(array('tcrmQueryAssociatedDate' => date("Y-m-d")));
            $response = $response->return;
            if($response->success)
                return $response->value;
            else return 0;
        } catch(Exception $e){
            echo $e->getMessage();
        }
    }

    private function getTranslation($data)
    {
        $fields = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 0);
        curl_setopt($curl, CURLOPT_URL, $this->resource_translator_bi);
        curl_setopt($curl, CURLOPT_USERPWD, urlencode($this->username_translator_bi).':'.urlencode($this->password_translator_bi));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Accept: application/json'));
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        return $response;
    }

    public function listPromotionProduct($lppfields)
    {
        $aliexIO = new AliexIO($this->conf);
        $list_products = new ListProducts();
        $list_products->setFields('productId,productTitle,productUrl,imageUrl,originalPrice,salePrice,discount,evaluateScore,commission,commissionRate,30daysCommission,volume,packageType,lotNum,validTime,storeName,storeUrl,allImageUrls');
        //$listproducts->setKeywords($lppfields['keywords']);
        $list_products->setCategoryId($lppfields['categoryId']);
        $list_products->setHighQualityItems('true');

        $formattedResponse = $aliexIO->runOperation($list_products);
        $array = json_decode($formattedResponse, true);
        $arr = [];
        foreach ($array['result']['products'] as $key => &$item) {
            preg_match('/\$([0-9]+[\.]*[0-9]*)/', $item['salePrice'], $match);
            if ((int)$match[1] != 0) {
                $arr[$key]['price'] = (int)$match[1] * $this->getTRM();
                $data['text'] = strtolower($item['productTitle']);
                $data['source'] = 'en';
                $data['target'] = 'es';
                $resp = $this->getTranslation($data);
                $arr[$key]['name'] = $resp['translations'][0]['translation']; //$item['productTitle'];
                $arr[$key]['description'] = $resp['translations'][0]['translation']; //$item['productTitle'];
                $arr[$key]['id'] = $item['productId'];
                $arr[$key]['image_medium_url'] = $item['imageUrl'];
                $arr[$key]['warning'] = 0;
                $arr[$key]['is_best_price'] = 0;
                $arr[$key]['first_order_special_price'] = 0;
                $arr[$key]['special_price'] = -1;
                $data['text'] = strtolower($item['packageType']);
                $resp = $this->getTranslation($data);
                $arr[$key]['unit'] = $resp['translations'][0]['translation'];
                $arr[$key]['quantity'] = $item['volume'];
                $arr[$key]['department_slug'] = 'aliexpress';
                $arr[$key]['shelf_slug'] = '';
                $arr[$key]['slug'] = $item['productId'];
                $arr[$key]['delivery_discount_amount'] = 0;
            }
        }
        return $arr;
    }

    public function productDetail($lppfields)
    {
        $aliexIO = new AliexIO($this->conf);
        $product_detail = new GetProductDetail();
        $product_detail->setFields('productId,productTitle,productUrl,imageUrl,originalPrice,salePrice,discount,evaluateScore,commission,commissionRate,30daysCommission,volume,packageType,lotNum,validTime,storeName,storeUrl,allImageUrls');
        $product_detail->setProductId($lppfields['productId']);
        $formattedResponse = $aliexIO->runOperation($product_detail);
        $array = json_decode($formattedResponse, true);

        return $array;

    }
}