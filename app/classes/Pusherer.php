<?php

/**
 * Interfaz con la API de Pusher para envìo de mensajes de  notificaciones
 */

class Pusherer extends Eloquent {

	/**
	* Funcion que crea el objeto pusher segun los datos de la api
	*
	* @param string $type tipo de usuario a quien se hara el envio, valores validos 'transporter', 'picker'
	* @return object Pusher
	*/
	private static function create_object( $type ){
		switch ( $type ) {
			case 'picker':
				$app_id = Config::get('app.pusherer.picker.app_id');
				$app_key = Config::get('app.pusherer.picker.key');
				$app_secret  = Config::get('app.pusherer.picker.secret');
				$app_cluster  = Config::get('app.pusherer.picker.cluster');
		    break;

			case 'transporter':
				$app_id = Config::get('app.pusherer.transporter.app_id');
				$app_key = Config::get('app.pusherer.transporter.key');
				$app_secret  = Config::get('app.pusherer.transporter.secret');
				$app_cluster  = Config::get('app.pusherer.transporter.cluster');
		    break;

		}
		return new Pusher\Pusher( $app_key, $app_secret, $app_id, ['cluster' => $app_cluster, 'encrypted' => true ] );
	}

	/**
	* Funcion que envia la notificación
	*
	* @param string $channel canal usado para enviar la notificación
	* @param string $event Evento que se genera
	* @param mixed $data array o string JSON que contiene la información del mensaje
	* @param string $type tipo de usuario a quien se hara el envio, valores validos 'transporter', 'picker'
	*/
	public static function send_push( $channel, $event, $data, $type )
	{
		$data = ( is_array( $data ) )? json_encode($data) : $data;
		$pusher = self::create_object( $type );
		$pusher->trigger( $channel, $event, $data );
	}

	/**
	* Funcion que envia la autentica al transportador en el canal
	*
	* @param string $channel canal usado para enviar la notificación
	* @param string $socket identificador del socket usado
	* @param string $type tipo de usuario a quien se hara el envio, valores validos 'transporter', 'picker'
	* @return String respuesta del servicio Pusher
	*/

	public static function auth_push( $channel, $socket, $type )
	{
		$pusher = self::create_object( $type );
		return $pusher->socket_auth( $channel, $socket );
	}

}