<?php

use exceptions\MigrateException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class NewSiteApi
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class NewSiteApi
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var int
     */
    private $cartId;

    /**
     * @var StdClass
     */
    private $address;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $zoneId;

    /**
     * @var int
     */
    private $cityId;

    /**
     * NewSiteApi constructor.
     * @param Client $client
     * @param $host
     */
    public function __construct(Client $client, $host)
    {
        $this->client = $client;
        $this->host = $host;
    }

    /**
     * @return array
     * @throws MigrateException
     */
    public function getUserSession()
    {
        $userData = null;

        try {
            $userData = $this->client->get("{$this->host}/migration", [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'userId' => $this->user ? $this->user->getKey() : null,
                    'cartId' => $this->cartId,
                ],
            ]);
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() == 400) {
                $content = $exception->getResponse()
                    ->getBody()
                    ->getContents();

                throw new MigrateException($content);
            }

            throw $exception;
        }

        $apiResponse = json_decode(
            $userData->getBody()
                ->getContents()
        );

        $apiResponse->data->address = $this->getAddress();
        $apiResponse->data->zoneId = $this->getZoneId();
        $apiResponse->data->cityId = $this->getCityId();
        return $apiResponse;
    }

    /**
     * @param $token
     * @return string
     */
    public function getUserIdFromToken($token)
    {
        $userData = $this->client->get("{$this->host}/me", [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$token}",
            ]
        ]);

        $apiResponse = json_decode(
            $userData->getBody()
                ->getContents()
        );

        return $apiResponse->data->id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return NewSiteApi
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return int
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * @param string $cartId
     * @return NewSiteApi
     */
    public function setCartId($cartId)
    {
        $this->cartId = $cartId;
        return $this;
    }

    /**
     * @return StdClass
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param StdClass $address
     * @return NewSiteApi
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return NewSiteApi
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * @param string $zoneId
     * @return NewSiteApi
     */
    public function setZoneId($zoneId)
    {
        $this->zoneId = $zoneId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param string $cityId
     * @return NewSiteApi
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }
}
