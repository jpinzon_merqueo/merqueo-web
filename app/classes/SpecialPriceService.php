<?php

/**
 * Class SpecialPriceService
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class SpecialPriceService
{
    /**
     * @var \Carbon\Carbon
     */
    private $promoEndLimit;

    /**
     * @var mixed
     */
    private $userId;

    /**
     * @var \Carbon\Carbon
     */
    private $promoBeginLimit;

    /**
     * SpecialPriceService constructor.
     */
    public function __construct()
    {
        $this->promoBeginLimit = \Carbon\Carbon::createFromDate(2019, 1, 28);
        $this->promoEndLimit = \Carbon\Carbon::createFromDate(2019, 2, 15);
    }

    /**
     * @return bool
     */
    public function shouldApplyFiveSpecialDiscount()
    {
        $userId = $this->getUserId();

        if (empty($userId) || !$this->validPromoDates()) {
            return false;
        }

        list($lastOrderDate, $totalOrder) = User::getLastOrderDateFromDate($userId, $this->promoBeginLimit);

        $validOrders = $totalOrder > Discount::REORDER_ORDERS_DISCOUNT;

        return $validOrders && $this->isValidLastOrderDate($lastOrderDate);
    }

    /**
     * @return bool
     */
    private function validPromoDates()
    {
        return \Carbon\Carbon::now()->lessThanOrEqualTo($this->promoEndLimit);
    }

    /**
     * @param $lastOrderDate
     * @return bool
     */
    protected function isValidLastOrderDate($lastOrderDate)
    {
        $validLastOrderDate = true;

        if (Discount::shouldApplyReorder()) {
            $validLastOrderDate = $lastOrderDate && $lastOrderDate->gte(
                    $this->promoBeginLimit->subDays(Discount::REORDER_DAYS_DISCOUNT)
                );
        }

        return $validLastOrderDate;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId ?: $this->getUserIdFromCurrentRequest();
    }

    /**
     * @return mixed
     */
    private function getUserIdFromCurrentRequest()
    {
        $userFromUrl = Route::current()->parameter('user_id');
        $userFromInput = Input::get('user_id');
        $userFromSession = Auth::check() ? Auth::user()->id : null;

        return $userFromUrl ?: ($userFromSession ?: $userFromInput);
    }
}
