<?php

/**
 * Interfaz con la API de Segment
 *
 */
class AnalyticsClient
{
    /**
     * Inicializa el cliente de Segment con el key
     *
     */
    public function __construct($key = null)
    {
        if (empty($key)) {
          $key = Config::get('app.segment.write_key_php');
        }

        Segment::init($key);

        $this->events = array();
    }

    /**
     * Identifica el usuario y envía los atributos
     *
     * @var string $userId Id del usuario
     * @var array $traits Atributos del usuario
     *
     * @return void
     */
    public function identify($userId, $traits = array())
    {
        try {
            if ($userId) {
                Segment::identify(array(
                    'userId' => $userId,
                    'traits' => $traits
                ));

                Segment::flush();
            }
        } catch (Exception $e) {
            Log::error("AnalyticsClient identify error");
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * Registra un evento y sus propiedades
     *
     * @var string $userId Id del usuario
     * @var string $event Nombre del evento
     * @var array $properties Propiedades del evento
     *
     * @return void
     */
    public function track($userId, $event, $properties = array())
    {
        $properties = array_merge($properties, array('source' => 'server'));
        $data = array(
            'event' => $event,
            'properties' => $properties
        );

        if ($userId) {
            $data['userId'] = $userId;
        } else {
            $data['anonymousId'] = Session::get('anonymousId');
        }

        try {
            Segment::track($data);
            Segment::flush();
        } catch (Exception $e) {
            Log::error("AnalyticsClient track error");
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * Registra un evento y sus propiedades desde la librería JS en el cliente
     *
     * @var string $userId Id del usuario
     * @var string $event Nombre del evento
     * @var array $properties Propiedades del evento
     *
     * @return void
     */
    public function trackClient($userId, $event, $properties = array())
    {
        $this->events[] = array(
            'userId' => $userId,
            'name' => $event,
            'props' => $properties
        );

        Session::flash('events', $this->events);
    }

    /**
     * Actualiza las propiedades de un usuario después de una orden
     *
     * @var OrderGroup $orderGroup Grupo de órdenes
     * @var Order $order Órden
     *
     * @return void
     */
    public static function updateUser($orderGroup, $order) {
        $webAnalytics = AnalyticsClient::webAnalytics();
        $webAnalytics->identify($order->user_id, $order->user->fullTraits);

        if ($orderGroup->source == 'Device') {
            if ($orderGroup->source_os == 'Android') {
                $androidAnalytics = AnalyticsClient::androidAnalytics();
                $androidAnalytics->identify($order->user_id, $order->user->fullTraits);
            } else if ($orderGroup->source_os == 'iOS') {
                $iosAnalytics = AnalyticsClient::iosAnalytics();
                $iosAnalytics->identify($order->user_id, $order->user->fullTraits);
            }
        }
    }

    /**
     * Devuelve un cliente de Analytics para Web
     *
     * @return AnalyticsClient
     */
    public static function webAnalytics() {
        return new AnalyticsClient;
    }

    /**
     * Devuelve un cliente de Analytics para Android
     *
     * @return AnalyticsClient
     */
    public static function androidAnalytics() {
        return new AnalyticsClient(Config::get('app.segment.write_key_android'));
    }

    /**
     * Devuelve un cliente de Analytics para iOS
     *
     * @return AnalyticsClient
     */
    public static function iosAnalytics() {
        return new AnalyticsClient(Config::get('app.segment.write_key_ios'));
    }
}
