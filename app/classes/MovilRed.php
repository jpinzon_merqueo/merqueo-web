<?php

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as GuzzleClient;

/**
* Interfaz con la API de MovilRed
*/
class MovilRed
{

    const TOPUP_SERVICES   = '/sales/';
    const BILL_PAYMENT     = '/billing/';

    public static $topUpOperators = [
        'mobile_telephony' => '1',
        'utilities'        => '4',
        'mobile_packages'  => '7',
        'tv'               => '13',
    ];

    public static $transactionType = [
        'topup_transaction'   => "0",
        'bill_payment'        => "1",
        'banks_obligations'   => "2",
        'deposit'             => "3",
        'withdarwal'          => "4",
        'send_money_transfer' => "5",
        'pay_money_transfer'  => "6",
        'pin_sales'           => "7",
    ];

    private $customerId;
    private $userName;
    private $passwordHash;
    private $channel;
    private $systemId;
    private $originalAddress;
    private $deviceCode;
    private $requestSource;
    private $secretKey;
    private $host;
    private $guzzleClient;

    function __construct()
    {
        $appConfig             = "app.movilred";
        $this->customerId      = Config::get("{$appConfig}.customerId");
        $this->userName        = Config::get("{$appConfig}.userName");
        $this->passwordHash    = Config::get("{$appConfig}.passwordHash");
        $this->channel         = Config::get("{$appConfig}.channel");
        $this->systemId        = Config::get("{$appConfig}.systemId");
        $this->originalAddress = Config::get("{$appConfig}.originalAddress");
        $this->deviceCode      = Config::get("{$appConfig}.deviceCode");
        $this->requestSource   = Config::get("{$appConfig}.requestSource");
        $this->secretKey       = Config::get("{$appConfig}.secretKey");
        $this->host            = Config::get("{$appConfig}.host");
        $this->guzzleClient    = new GuzzleClient();
    }

    /**
     * Método para obtener un listado de los proveedores de servicio activos.
     *
     * @param  string $operator_type
     *
     * 1 = Mobile telephony airtime
     * 7 = Mobile packages
     * 13 = TV
     * 14 = Utilities
     *
     * @return array Response
     */
    public function getTopUpOperators($operator_type = '1')
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'operatorType' => $operator_type,
        ]);
    }

    /**
     * Realiza la recarga de un teléfono móvil o número de cuenta para un proveedor de servicios en particular.
     *
     * @param  integer $productId           Código interno del producto relacionado con el proveedor de servicios.
     * @param  string $EANCode              Código de producto EAN estándar para el proveedor o el operador del servicio. El servicio requiere solo uno de los atributos productCode o EANCode. Si el campo productCode está presente, el método ignorará el valor en el campo EANCode
     * @param  integer $amount              Cantidad de dinero que se recargará al número de cuenta de destino.
     * @param  integer $destinationNumber   Cuenta para ser recargada (número de celular prepago, número de cuenta de TV, número de cliente, etc.).
     * @param  string $customerTxReference  Número de referencia de la transacción para el cliente que consume el servicio.
     */
    public function processTopUpTransaction($productId, $EANCode, $amount, $destinationNumber, $customerTxReference = '')
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'productId'           => $productId,
            'EANCode'             => $EANCode,
            'amount'              => $amount,
            'destinationNumber'   => $destinationNumber,
            'destinationDate'     => date('Ymd'),
            'customerTxReference' => $customerTxReference,
        ]);
    }

    /**
     * Devuelve todas las categorías disponibles para la venta de pin.
     *
     * @param  string $parentCategory Tipo de categoría necesaria, para identificar la categoría de un pin en particular. Enviar 0 (cero) por defecto.
     */
    public function getPinesByCategory($parentCategory = "0")
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'parentCategory' => $parentCategory,
        ]);
    }

    /**
     * Devuelve todos los pines disponibles para el id de la categoría.
     *
     * @param  string $idCategory Id de categoría requerida, para identificar la lista de pin
     */
    public function getReferencePinesByCategory($idCategory)
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'idCategory' => $idCategory,
        ]);
    }

    /**
     * Devuelve todos los productos disponibles para referencia pin
     *
     * @param  string $productPinId Id de producto de pin interno de MovilRed.
     */
    public function getProductoByReference($productPinId)
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'productPinId' => $productPinId,
        ]);
    }

    /**
     * Realiza la recarga de un teléfono móvil o número de cuenta para un proveedor de servicios en particular.
     *
     * @param  string $productId           Id de producto de pin interno de MovilRed.
     * @param  string $EANCode             Código EAN del producto.
     * @param  string $amount              Valor a pagar por el producto.
     * @param  string $phoneNumber         Teléfono para notificar del código del pin. Este campo no es obligatorio, pero debe tener alguna notificación, ya sea por teléfono o correo electrónico.
     * @param  string $email               Correo electrónico para notificar el código del pin. Este campo no es obligatorio, pero debe tener alguna notificación, ya sea por teléfono o correo electrónico.
     * @param  string $customerTxReference Número de referencia de transacción para el cliente que consume el servicio.
     */
    public function processTopUpTransactionPines($productId, $EANCode, $amount, $phoneNumber, $email = '', $customerTxReference = '')
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'productId'           => $productId,
            'EANCode'             => $EANCode,
            'amount'              => $amount,
            'phoneNumber'         => $phoneNumber,
            'customerDate'        => date('Ymd'),
            'email'               => $email,
            'customerTxReference' => $customerTxReference,
        ]);
    }

    /**
     * Realiza la recarga de un teléfono móvil o número de cuenta para un proveedor de servicios en particular.
     *
     * @param  string $productId           Id de producto de pin interno de MovilRed.
     * @param  string $EANCode             Código EAN del producto.
     * @param  string $amount              Valor a pagar por el producto.
     * @param  string $phoneNumber         Teléfono para notificar del código del pin. Este campo no es obligatorio, pero debe tener alguna notificación, ya sea por teléfono o correo electrónico.
     * @param  string $email               Correo electrónico para notificar el código del pin. Este campo no es obligatorio, pero debe tener alguna notificación, ya sea por teléfono o correo electrónico.
     * @param  string $customerTxReference Número de referencia de transacción para el cliente que consume el servicio.
     */
    public function reversePines($productId, $EANCode, $amount, $phoneNumber = '', $email = '', $customerTxReference = '')
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'productId'           => $productId,
            'EANCode'             => $EANCode,
            'amount'              => $amount,
            'customerDate'        => date('Ymd'),
            'phoneNumber'         => $phoneNumber,
            'email'               => $email,
            'customerTxReference' => $customerTxReference,
        ]);
    }

    /**
     * Este servicio permite obtener el estado de la transacción
     *
     * @param  string $thirdPartyReference Id de la transacción de Mobiquity.
     * @param  string $transactionType
     *
     * 0: Topup Transaction
     * 1: Bill Payment
     * 2: Banks Obligations
     * 3: Deposit
     * 4: Withdarwal
     * 5: Send Money Transfer
     * 6: Pay Money Transfer
     * 7: Pin sales
     *
     */
    public function getTransactionState($thirdPartyReference, $transactionType = "0")
    {
        $end_point = self::TOPUP_SERVICES . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'thirdPartyReference' => $thirdPartyReference,
            'transactionType'     => $transactionType,
        ]);
    }

    /**
     * Obtiene las reglas y restricciones de un determinado emisor.
     *
     * @param  string $EAN13BillerCode Número de identificación del emisor de la factura. En un código de barras estándar EAN 128 corresponde al número después del prefijo 415 y antes del código de identificación de campo (8020).
     * @param  string $billerCode      Número de identificación interna MovilRed del emisor de la factura. Si este campo no está vacío, el sistema utilizará este campo para ubicar la información del emisor de la factura, si lo está, el emisor de la factura se ubicará en el campo EAN13BillerCode.
     */
    public function getBillerParameters($EAN13BillerCode, $billerCode)
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'EAN13BillerCode' => $EAN13BillerCode,
            'billerCode'      => $billerCode,
        ]);
    }

    /**
     * Verifica la viabilidad de realizar una transacción de pago de facturas.
     *
     * @param  string $EAN128FullCode      Código de barras completo de la factura a pagar.
     * @param  string $EAN13BillerCode     Número de identificación del emisor de la factura. En un código de barras estándar EAN 128 corresponde al número después del prefijo 415 y antes del código de identificación de campo (8020).
     * @param  array  $billReferenceNumber Matriz con los números de identificación de una factura para un emisor en particular (emisor de la factura)
     * @param  string $billValue           Monto a pagar o valor de la factura. En una codificación de billete EAN 128 será el número después del campo (3900) o identificador de aplicación.
     * @param  string $valueToPay          El valor real que el cliente desea pagar, puede ser diferente si el usuario desea pagar una cantidad diferente solo si el facturador lo permite.
     * @param  string $paymentDate         Fecha de pago máxima especificada en el campo EAN128.
     * @param  string $bankPriority        La prioridad del banco que procesa la transacción.
     */
    public function validateBillPaymentByEANCode($EAN128FullCode, $EAN13BillerCode, $billReferenceNumber = [], $billValue, $valueToPay, $paymentDate, $bankPriority)
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'EAN128FullCode'      => $EAN128FullCode,
            'EAN13BillerCode'     => $EAN13BillerCode,
            'billReferenceNumber' => $billReferenceNumber,
            'billValue'           => $billValue,
            'valueToPay'          => $valueToPay,
            'paymentDate'         => $paymentDate,
            'bankPriority'        => $bankPriority
        ]);
    }

    /**
     * Verifica la viabilidad de realizar una transacción de pago de facturas.
     *
     * @param  string $billerCode           Número de identificación interna del emisor.
     * @param  array  $shortReferenceNumber Matriz con los números de identificación de una factura para un emisor en particular (emisor de la factura)
     * @param  string $valueToPay           El valor real que el cliente desea pagar, puede ser diferente si el usuario desea pagar una cantidad diferente solo si el facturador lo permite.
     * @param  string $bankPriority         La prioridad del banco que procesa la transacción.
     */
    public function validateBillPaymentByReference($billerCode, $shortReferenceNumber = [], $valueToPay, $bankPriority = "1")
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'billerCode'           => $billerCode,
            'shortReferenceNumber' => $shortReferenceNumber,
            'valueToPay'           => $valueToPay,
            'bankPriority'         => $bankPriority,
        ]);
    }

    /**
     * Obtiene una lista de los emisores de facturas de todos los facturadores que en su nombre o palabras clave contienen una cadena en particular.
     *
     * @param  string $keywordSegment Cadena a buscar en la lista de palabras clave de todos los emisores de facturas. La cadena debe tener al menos 3 caracteres.
     */
    public function getBillerByKeyword($keywordSegment)
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'keywordSegment' => $keywordSegment,
        ]);
    }

    /**
     * Solicitud del pago de una factura en particular después de una validación exitosa.
     *
     * @param  string $EAN13BillerCode      Número de identificación del emisor de la factura. En un estándar EAN 128.
     * @param  string $billerCode           Número de identificación interna de MovilRed del emisor de la factura.
     * @param  array  $billReferenceNumber  Matriz con el (los) número (s) de identificación (es) de una factura para un emisor (emisor de factura) particular presente en el Código EAN.
     * @param  array  $shortReferenceNumber Matriz con el (los) número (s) de identificación (es) de una factura para un emisor particular (facturador) presente en la factura.
     * @param  string $valueToPay           El valor real que el cliente desea pagar, puede ser diferente si el usuario desea pagar una cantidad diferente solo si el facturador lo permite.
     * @param  string $echoData             Campo ecológico, de validateBillPaymentByReference y validateBillPaymentByEANCode.
     * @param  string $customerTxReference  Número de referencia de transacción para el cliente que consume el servicio.
     */
    public function payBill($EAN13BillerCode, $billerCode, $billReferenceNumber = [], $shortReferenceNumber = [], $valueToPay, $echoData, $customerTxReference = '')
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'EAN13BillerCode'      => $EAN13BillerCode,
            'billerCode'           => $billerCode,
            'billReferenceNumber'  => $billReferenceNumber,
            'shortReferenceNumber' => $shortReferenceNumber,
            'valueToPay'           => $valueToPay,
            'echoData'             => $echoData,
            'customerTxReference'  => $customerTxReference,
        ]);
    }

    /**
     * Obtiene todas las categorías para los servicios de pago de facturas donde todos los emisores de facturas diferentes se pueden agrupar en una estructura de jerarquía, formar un punto de partida particular o categoría principal.
     *
     * @param  string $parentCategory Punto de partida o categoría principal donde se requieren todos sus descendientes en la jerarquía.
     */
    public function getBillerCategories($parentCategory="")
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'parentCategory' => $parentCategory,
        ]);
    }

    /**
     * Obtenga una lista de los emisores de facturas de todos los emisores de facturas en un grupo de categorías determinado.
     *
     * @param  string $categoryId Identificación de la categoría de búsqueda de emisor
     */
    public function getBillersByCategory($categoryId)
    {
        $end_point = self::BILL_PAYMENT . __FUNCTION__ ;
        return $this->sendRequest($end_point, [
            'categoryId' => $categoryId,
        ]);
    }

    protected function getDateTime($with_point = FALSE)
    {
        $format = 'YmdHis';
        $format .= ($with_point == TRUE) ? '.u' : 'u';
        return date($format);
    }

    protected function getMeta()
    {
        return [
            'requestDate'      => $this->getDateTime(TRUE),
            'customerId'       => $this->customerId,
            'userName'         => $this->userName,
            'passwordHash'     => $this->passwordHash,
            'channel'          => $this->channel,
            'systemId'         => $this->systemId,
            'originAddress'  => $this->originalAddress,
            'deviceCode'       => $this->deviceCode,
            'requestReference' => $this->getDateTime(),
            'requestSource'    => $this->requestSource,
        ];
    }

    protected function generateRequestSignature($data = [])
    {
        return sha1(json_encode($this->getMeta()) . json_encode($data) . $this->secretKey);
    }

    protected function sendRequest($endPoint = '', $data = [])
    {
        try {
            $params = [
                'meta' => $this->getMeta(),
                'data' => $data,
                'requestSignature' => [
                    'systemSignature' => $this->generateRequestSignature($data),
                ],
            ];
            $endPoint = $this->host . $endPoint;
            $response = $this->guzzleClient->post($endPoint, [
                'headers'=>array('Content-Type'=>'application/json'),
                GuzzleHttp\RequestOptions::JSON => $params,
                'timeout' => 5,
            ]);
            
            $result =  json_decode($response->getBody()->getContents());

            if($result->outcome->error->errorCode!="00"){
                throw new \exceptions\ExternalServiceException($result->outcome->error->errorMessage);
            }

            return $result->data;

        } catch (RequestException $e) {
            if($e->hasResponse()){
                $body= ($e->hasResponse()) ? json_decode((string)$e->getResponse()->getBody()) : '';
                $message = $body->outcome->error->errorMessage;
            }else{
                $message = "Error de respuesta";
            }
            
            throw new \exceptions\ExternalServiceException($message);
        }
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     *
     * @return self
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSystemId()
    {
        return $this->systemId;
    }

    /**
     * @param mixed $systemId
     *
     * @return self
     */
    public function setSystemId($systemId)
    {
        $this->systemId = $systemId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalAddress()
    {
        return $this->originalAddress;
    }

    /**
     * @param mixed $originalAddress
     *
     * @return self
     */
    public function setOriginalAddress($originalAddress)
    {
        $this->originalAddress = $originalAddress;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceCode()
    {
        return $this->deviceCode;
    }

    /**
     * @param mixed $deviceCode
     *
     * @return self
     */
    public function setDeviceCode($deviceCode)
    {
        $this->deviceCode = $deviceCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestSource()
    {
        return $this->requestSource;
    }

    /**
     * @param mixed $requestSource
     *
     * @return self
     */
    public function setRequestSource($requestSource)
    {
        $this->requestSource = $requestSource;

        return $this;
    }
}
