<?php

use Aws\Sns\SnsClient;
use Aws\Sns\Exception\SnsException;
use contracts\SendSNSNotificationInterface;

class SendSNSNotification implements SendSNSNotificationInterface
{

    /**
     * AWS Region where SNS is hosted
     * @var AWS_REGION
     */
    const AWS_REGION = 'us-east-1';
    
    /**
     * AWS SNS client.
     * @var string
     */
    protected $client;

    /**
     * Main constructor
     */
    public function __construct()
    {
        // Create a new AWS SNS client
        $this->client = SnsClient::factory(array(
            'region' => self::AWS_REGION,
        ));
    }

    /**
     * Publish a message in Aws SNS 
     * @param array $body
     * @param string $topicArn
     */
    public function sendMessage(array $body, $topicArn = null)
    {
        try {
            $message = json_encode($body); // Encode data
            return $this->client->publish([
                'TopicArn' => \Config::get("app.aws.sns.{$topicArn}"),
                'Message' => $message
            ]);
        } catch (SnsException $exception) {
            ErrorLog::add($exception);
        }
    }
}
