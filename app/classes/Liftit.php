<?php
/**
* Clase para interacciones con api de liftit
*/
class Liftit extends Eloquent
{
	private $url = null;
	private $client = null;
	private $token = null;

	function __construct()
	{
		$this->url = Config::get('app.liftit.url');
		$this->token = Config::get('app.liftit.token');
		$this->client = new GuzzleHttp\Client(['base_uri' => $this->url]);
		$this->authenticate();
	}

	/**
	 * Función para autenticarse en el sistema de liftit
	 * @return boolean retorna true si la autenticació ha sido válida.
	 */
	private function authenticate()
	{
		$uri = '/v1/service';
		$response = $this->client->request('GET', $uri, [
			'headers' => [
				'Content-Type' => 'application/json',
				'authorization' => $this->token
			]
		]);

		$liftit_log = new LiftitLog;
		$liftit_log->url =  $this->url.$uri;
		$liftit_log->http_method = 'GET';
		$liftit_log->response_http_code = $response->getStatusCode();
		$liftit_log->request = json_encode( ['headers' => ['Content-Type' => 'application/json', 'authorization' => $this->token ]] );
		$liftit_log->response = $response->getBody();
		$liftit_log->ip = get_ip();
		$liftit_log->save();

		if ( $response->getStatusCode() == 200 )
			return true;

		throw new Exception("Error al autenticar con liftit: ".$response->getBody(), 1);
	}

	/**
	 * Crea un nuevo servicio en liftit
	 * @param  String $route 	Nombre de la ruta
	 * @param  Int 				$vehicle_id ID del vehiculo
	 * @return boolean 			retorna true si se ha creado con éxito.
	 */
	public function createNewService($route, $vehicle_id)
	{
		$orders = Order::join('order_groups', 'orders.group_id', '=', 'order_groups.id')
		    ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
		    ->join('vehicles', 'vehicles.id','=', 'orders.vehicle_id')
		    ->leftJoin('user_address', 'order_groups.address_id', '=', 'user_address.id')
		    ->join('drivers', 'orders.driver_id', '=', 'drivers.id')
		    ->where('orders.status', 'Dispatched')
		    ->where('vehicles.id', $vehicle_id)
		    ->where('orders.planning_route', $route)
		    ->select('orders.*', 'order_groups.user_phone', 'order_groups.user_address_longitude','order_groups.user_address_latitude', 'order_groups.user_firstname',
		        'cities.city AS user_city', 'order_groups.user_lastname', 'order_groups.user_address', 'order_groups.user_address_further', 'order_groups.user_address_neighborhood',
		        'drivers.first_name AS driver_firstname', 'orders.driver_id', 'drivers.bringg_id AS driver_bringg_id', 'drivers.cellphone AS driver_phone', 'drivers.last_name AS driver_lastname',
		        'orders.real_delivery_date', DB::raw("DATE_FORMAT(DATE_ADD(real_delivery_date, INTERVAL 5 HOUR), '%Y-%m-%dT%H:%i:%s') AS real_delivery_date"))
		    ->groupBy('orders.id')
		    ->orderBy('orders.planning_route', 'ASC')
		    ->get();

		$service_type = Config::get('app.liftit.service_type');
		$body = [
			'customer_id' => (int)Config::get('app.liftit.customer_id'),
			'service_type_id' => (int)$service_type[0],
			'payment_type' => 0,
			'tasks' => []
		];

		foreach ($orders as $key => $order) {
			$task = [
				'task_type' => 'l',
				'description' => 'Referencia Pedido: '.$order->reference,
				'address' => $order->user_address,
				'address_extra' => $order->user_address_further,
				'city' => 'Bogota',
				'dep_state' => 'Cundinamarca',
				'country' => 'Colombia',
				'lat' => $order->user_address_latitude,
				'lon' => $order->user_address_longitude,
				'reference' => 'Pedido #'.$order->reference,
				'payment_type' => 0,
				'payment_amount' => 0,
				/*'recipient' => [
					'name' => $order->user_firstname.' '.$order->user_lastname,
				]*/
			];
			array_push($body['tasks'], $task);
		}
		$uri = '/v1/service';
		$response = $this->client->request('POST', $uri, [
			'headers' => [
				'Content-Type' => 'application/json',
				'authorization' => $this->token
			],
			'body' => json_encode($body)
		]);

		$result = json_decode($response->getBody());

		$liftit_log = new LiftitLog;
		$liftit_log->url =  $this->url.$uri;
		$liftit_log->http_method = 'POST';
		$liftit_log->response_http_code = $response->getStatusCode();
		$liftit_log->request = json_encode($body);
		$liftit_log->response = $response->getBody();
		$liftit_log->ip = get_ip();
		$liftit_log->save();

		if ( $response->getStatusCode() == 201 )
			return $result[0]->id;

		throw new Exception("Error al crear los servicios con liftit: ".$response->getBody(), 1);
	}

	/**
	 * Función para correr un servicio
	 * @param Int $id identificador del servicio
	 */
	public function RunService($id)
	{
		$uri = '/v1/service/'.(int)$id.'/run';
		$response = $this->client->request('PUT', $uri, [
			'headers' => [
				'Content-Type' => 'application/json',
				'authorization' => $this->token
			]
		]);

		$body = json_decode($response->getBody());

		$liftit_log = new LiftitLog;
		$liftit_log->url =  $this->url.$uri;
		$liftit_log->http_method = 'PUT';
		$liftit_log->response_http_code = $response->getStatusCode();
		$liftit_log->request = json_encode(['headers' => ['Content-Type' => 'application/json', 'authorization' => $this->token ]]);
		$liftit_log->response = $response->getBody();
		$liftit_log->ip = get_ip();
		$liftit_log->save();

		if ( $response->getStatusCode() == 200 )
			return $body->estimated_price;

		throw new Exception("Error al crear los servicios con liftit: ".$response->getBody(), 1);
	}
}