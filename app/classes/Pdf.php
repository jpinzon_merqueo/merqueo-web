<?php
use Carbon\Carbon;
/**
 * Clase para generar PDF de factura
 */
class Pdf extends Fpdf
{
    /**
     * Genera el pdf de una factura de bodega
     *
     * @param Order $order Pedido
     * @param string $output Contenido
     *
     * @return file  Respuesta del servicio
     */
    public static function generateInvoice($order, $output = '')
    {
        $user = User::find($order->user_id);
        $products = OrderProduct::where('order_id', $order->id)
            ->where('fulfilment_status', '=', 'Fullfilled')
            ->select('product_quantity', 'product_unit', 'price', 'original_price',
                'product_name', 'quantity', 'iva AS iva', 'base_price')
            ->get();
        $order_group = OrderGroup::where('id', $order->group_id)->first();
        $city = City::where('id', $order_group->user_city_id)->first();
        $business_name = !empty($order->user_business_name) ? $order->user_business_name : $user->business_name;
        $identity_type = !empty($order->user_identity_type) ? $order->user_identity_type : $user->identity_type;
        $identity_number = !empty($order->user_identity_number) ? $order->user_identity_number : $user->identity_number;
        $business_name = !empty($business_name) ? $business_name : 'CLIENTES GENERALES';
        $identity_type = !empty($identity_type) ? $identity_type : 'NIT';
        $identity_number = !empty($identity_number) ? $identity_number : '222.222.222';
        $date = !empty($order->invoice_date) ? Carbon::parse($order->invoice_date)->toDateString() : Carbon::parse($order->management_date)->toDateString();
        $order->delivery_amount = $order->delivery_amount;

        $pdf = new \Anouar\Fpdf\Fpdf('P', 'mm', 'A4');
        $pdf->AddPage();
        $pdf->SetLeftMargin(6);
        // Logo
        $pdf->Image(base_path('admin_asset/img/logo-fucsia.png'),10,10,50);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(75);
        $pdf->Cell(30,5,'MERQUEO S A S',0,0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->SetDrawColor(237,237,237);
        $pdf->Cell(33);
        $pdf->Cell(52,6,'FACTURA DE VENTA '.strtoupper_utf8($city->city),'LTR',0,'C');
        $pdf->Ln(6);

        // Información de contacto
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(76);
        $pdf->Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
        $pdf->Cell(31);
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(52,9,$order->invoice_number,'LBR',0,'C');
        $pdf->Ln(4);

        $pdf->Cell(75);
        $pdf->Cell(40,5,'CLL 97A # 9A - 50 PISO 2',0,0,'C');
        $pdf->Ln(4);

        $pdf->Cell(78);
        $pdf->Cell(30,5,'TEL: 7561938',0,0,'C');
        $pdf->Ln(4);

        $pdf->Cell(78);
        $pdf->Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        $pdf->Ln(15);

        /////////////Cliente//////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        if($identity_type == 'NIT')
            $pdf->Cell(18,5,utf8_decode('Señores'),0,0,'L',true);
        else
            $pdf->Cell(18,5,utf8_decode('Nombre'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(120,5,strtoupper_utf8($business_name),1,0,'L',false);
        $pdf->Ln(5);

        /////////////NIT//////////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(18,5,utf8_decode($identity_type),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(70,5,utf8_decode($identity_number),1,0,'L',false);

        ///////////Telefono///////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(15,5,utf8_decode('Teléfono'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(35,5,utf8_decode($user->phone),1,0,'L',false);
        $pdf->Ln(5);

        /////////////Direccion////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(18,5,utf8_decode('Dirección'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(70,5,mb_substr(strtoupper_utf8($order_group->user_address),0,48),1,0,'L',false);

        /////////////Ciudad///////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(15,5,'Ciudad',0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(35,5,strtoupper_utf8($city->city),1,0,'L',false);

        /////////////Fecha Factura////////
        $pdf->SetXY(148,43);
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(25,5,utf8_decode('Fecha Factura'),'L',0,'C',true);
        $pdf->SetXY(148,48);
        $pdf->Cell(25,10,$date,1,0,'C',false);

        /////////////Fecha Vencimiento////
        $pdf->SetXY(173,43);
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(27,5,utf8_decode('Fecha Vencimiento'),'R',0,'C',true);
        $pdf->SetXY(173,48);
        $pdf->Cell(27,10,$date,1,0,'C',false);
        $pdf->Ln(14);


        /////////////Detalle factura de pedido de bodega////////
        $pdf->SetDrawColor(210,210,210);
        $pdf->SetFillColor(241, 241, 241);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(64, 5, utf8_decode('Producto'), 1, 0, 'C', true);
        $pdf->Cell(20, 5, utf8_decode('Unid. medida'), 1, 0, 'C', true);
        $pdf->Cell(15, 5, utf8_decode('Cantidad'), 1, 0, 'C', true);
        $pdf->Cell(20, 5, utf8_decode('Valor Unitario'), 1, 0, 'C', true);
        $pdf->Cell(15, 5, utf8_decode('Valor'), 1, 0, 'C', true);
        $pdf->Cell(20, 5, utf8_decode('Descuento'), 1, 0, 'C', true);
        $pdf->Cell(15, 5, utf8_decode('Sub total'), 1, 0, 'C', true);
        $pdf->Cell(10, 5, utf8_decode('IVA'), 1, 0, 'C', true);
        $pdf->Cell(15, 5, utf8_decode('Total'), 1, 0, 'C', true);
        $pdf->Ln(5);
        $totalBaseOriginalPriceProducts = $iva = $totalDiscountInProducts = 0;
        foreach ($products as $key => $value) {
            $pdf->SetFont('Arial', '', 7);
            $totalBaseOriginalPriceProducts += $value->total_base_original_price;
            $totalDiscountInProducts += $value->total_product_discount;
            $iva += $value->total_iva_base_original_price;

            if ($value->iva) {
                $iva_arr[$value->iva][] = $value->total_iva_base_original_price;
            }
            /////////////Data///////////////////
            $ellipsis = "";
            if (strlen($value->product_name) > 60) {
                $ellipsis = "...";
            }
            $pdf->Cell(64, 5, substr(utf8_decode($value->product_name), 0, 50) . $ellipsis, 1, 0, 'L', false);
            $pdf->Cell(20, 5, utf8_decode($value->product_quantity . ' ' . $value->product_unit), 1, 0, 'C', false);
            $pdf->Cell(15, 5, utf8_decode($value->quantity), 1, 0, 'C', false);
            $pdf->Cell(20, 5, '$ ' . number_format($value->base_original_price, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(15, 5, '$ ' . number_format($value->total_base_original_price, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(20, 5, '$ ' . number_format($value->total_product_discount, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(15, 5, '$ ' . number_format($value->sub_total_product, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(10, 5, '$ ' . number_format($value->total_iva_base_original_price, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(15, 5, '$ ' . number_format($value->total_product, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Ln(5);
        }
        $pdf->Ln(10);

        //$iva = ($totalitems * $iva) / 100;
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(114);
        $pdf->Cell(40, 5, utf8_decode('Total productos'), 1, 0, 'L', false);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(40, 5, '$ ' . number_format($totalBaseOriginalPriceProducts, 0, ',', '.'), 1, 0, 'R', false);
        $pdf->Ln(5);

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(114);
        $pdf->Cell(40, 5, utf8_decode('Descuento en productos'), 1, 0, 'L', false);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(40, 5, '$ ' . number_format($totalDiscountInProducts, 0, ',', '.'), 1, 0, 'R', false);
        $pdf->Ln(5);

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(114);
        $pdf->Cell(40, 5, utf8_decode('Sub total'), 1, 0, 'L', false);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(40, 5, '$ ' . number_format($totalBaseOriginalPriceProducts - $totalDiscountInProducts, 0, ',', '.'), 1, 0, 'R', false);
        $pdf->Ln(5);

        if (isset($iva_arr)) {
            foreach ($iva_arr as $key => $iva_item) {
                $pdf->SetFont('Arial', 'B', 7);
                $pdf->Cell(114);
                $pdf->Cell(40, 5, utf8_decode('IVA(' . $key . '%)'), 1, 0, 'L', false);
                $pdf->SetFont('Arial', '', 7);
                $pdf->Cell(40, 5, '$ ' . number_format(array_sum($iva_arr[$key]), 0, ',', '.'), 1, 0, 'R', false);
                $pdf->Ln(5);
            }
        }

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(114);
        $pdf->Cell(40, 5, utf8_decode('Otros descuentos'), 1, 0, 'L', false);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(40, 5, '$ ' . number_format($order->discount_amount, 0, ',', '.'), 1, 0, 'R', false);
        $pdf->Ln(5);

        /*$pdf->SetFont('Arial','B',7);
        $pdf->Cell(114);
        $pdf->Cell(40,5,utf8_decode('IVA'),1,0,'L',false);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(40,5,'$ '.number_format($iva, 0, ',', '.'),1,0,'R',false);
        $pdf->Ln(5);*/

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(114);
        $pdf->Cell(40,5,utf8_decode('Domicilio'),1,0,'L',false);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(40,5,'$ '.number_format($order->delivery_amount, 0, ',', '.'),1,0,'R',false);
        $pdf->Ln(5);

        $total = round($totalBaseOriginalPriceProducts - $totalDiscountInProducts + $iva - $order->discount_amount + $order->delivery_amount);
        $pdf->SetFillColor(241, 241, 241);
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(114);
        $pdf->Cell(40,5,utf8_decode('Total a Pagar'),1,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(40,5,'$ '.number_format($total, 0, ',', '.'),1,0,'R',true);
        $pdf->Ln(7);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(40,5,utf8_decode('CONDICION DE PAGO'),0,0,'L',false);
        $pdf->Ln(5);

        $pdf->Cell(3);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(100,5,'Credito Clientes Nacionales Cuota 1 F - 003- 225 Vence el '.$date,0,0,'L',false);
        $pdf->Cell(20,5,'$ '.number_format($total, 0, ',', '.'),0,0,'L',false);
        $pdf->Ln(5);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(40,5,utf8_decode('VALOR EN LETRAS'),0,0,'L',false);
        $pdf->Ln(5);

        $pdf->Cell(3);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(100,5,convert_number_to_word($total).' M/Cte',0,0,'L',false);
        $pdf->Ln(5);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(40,5,utf8_decode('OBSERVACIONES'),0,0,'L',false);
        $pdf->Ln(4);
        $pdf->SetY(-38);
        $pdf->SetAutoPageBreak(true, 1);
        $pdf->Cell(195,5,'FACTURA IMPRESA POR COMPUTADOR GRACIAS POR PREFERIRNOS',0,0,'C');
        $pdf->Ln(7);

        $pdf->SetFont('Arial','',8);
        $pdf->SetFillColor(237, 237, 237);
        $resolution = !is_null($order->invoice_resolution) ? $order->invoice_resolution : '18762006848873' ;
        $pos = strpos($order->invoice_number, '-');
        $prefix = substr($order->invoice_number, $pos + 1, 3);

        $prefix_old = substr($order->invoice_number, 0, 2);
        if($prefix_old == 'F3' || $prefix_old == 'F4'){
            $prefix = substr($order->invoice_number, $pos + 1, 2);
        }


        $pdf->MultiCell(195,5,utf8_decode('A esta factura de venta aplican las normas relativas a la letra de cambio (artículo 5 Ley 1231 de 2008). Con esta el Comprador declara haber
    recibido real y materialmente las mercancías o prestación de servicios descritos en este título - Valor. N° de Resolución '.$resolution.'
    aprobado en 2017-03-08 prefijo '.$prefix.' desde el N° 1 al 400000 y N° Resolución '.$resolution.' 
    aprobado en 2018-02-09 prefijo '.$prefix.' desde el N° 400001 al 600000.
    Regimen Común - Actividad Económica 4631 Tarifa 004.140 y 7310 Tarifa 009.660'),1,'C',true);

        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(175,5,'Original',0,0,'C');
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(20,5,utf8_decode('Página ').$pdf->PageNo(),0,0,'C');


        if($output != '') {
            $pdf  = $pdf->Output(null, $output);
            return $pdf;
        }else{
            $pdf->Output();
        }
        exit;
    }

    /**
     *  Genera el pdf de una orden de compra
     *
     * @param int $provider_order_id Datos de una orden
     * @param Closure|null $file_updater Función que modifica el contenido del archivo final.
     * @param bool $print_file Carga en el buffer el pdf y termina la ejecución.
     * @return string Respuesta del servicio
     */

    public static function generate_provider_order($provider_order_id, Closure $file_updater = null, $print_file = true)
    {
        $provider_order_products = ProviderOrderDetail::where('provider_orders.id', $provider_order_id)
                                                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
                                                        ->leftJoin(DB::raw('(provider_order_receptions, provider_order_reception_details)'), function($join){
                                                            $join->on('provider_order_receptions.provider_order_id', '=','provider_orders.id');
                                                            $join->on('provider_order_receptions.status', '<>', DB::raw('"En proceso"'));
                                                            $join->on('provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id');
                                                            $join->on('provider_order_reception_details.store_product_id', '=', 'provider_order_details.store_product_id');
                                                        })
                                                        ->select('provider_order_details.*')
                                                        ->groupBy('provider_order_details.store_product_id')
                                                        ->get();

        $provider_order = ProviderOrder::join('providers', 'providers.id', '=', 'provider_orders.provider_id')
                                       ->leftjoin('provider_contacts', 'providers.id', '=', 'provider_contacts.provider_id')
                                       ->leftjoin('admin', 'provider_orders.admin_id', '=', 'admin.id')
                                       ->where('provider_orders.id', $provider_order_id)
                                       ->select('provider_orders.*', 'providers.name', 'providers.address','providers.nit','provider_contacts.fullname', 'provider_contacts.phone', 'providers.maximum_order_report_time', DB::raw('admin.fullname AS admin'))
                                       ->with('warehouse')
                                       ->first();

        $warehouse = $provider_order->warehouse ?: Warehouse::whereStatus(1)->has('city')->first();
        $date = !empty($provider_order->created_at) ? Carbon::parse($provider_order->created_at)->toDateString(): '';
        $dateent = !empty($provider_order->delivery_date) ? Carbon::parse($provider_order->delivery_date)->toDateString(): '';
        //$iva = $date > '2016-12-31' ? 19 : 16;
        $city = $warehouse->city;

        $pdf = new \Anouar\Fpdf\Fpdf('P', 'mm', 'A4');
        $pdf->AddPage();
        $pdf->SetLeftMargin(6);
        // Logo
        $pdf->Image(__DIR__.'/../../admin_asset/img/logo_pdf.png',10,18,50);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(75);
        $pdf->Cell(30,5,'MERQUEO S A S',0,0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->SetDrawColor(237,237,237);
        $pdf->Cell(33);
        $pdf->Cell(52,6,'ORDEN DE COMPRA '.strtoupper_utf8($city->city),'LTR',0,'C');
        $pdf->Ln(6);

        // Información de contacto
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(76);
        $pdf->Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
        $pdf->Cell(31);
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(52,9,'# '.$provider_order->id,'LBR',0,'C');
        $pdf->Ln(4);

        $pdf->Cell(75);
        $address = empty($warehouse->address) ? 'CR 69K # 78 - 56' : $warehouse->address;
        $pdf->Cell(40, 5, $address, 0, 0, 'C');
        $pdf->Ln(4);

        $pdf->Cell(78);
        $pdf->Cell(30,5,'TEL: 7561938',0,0,'C');
        $pdf->Ln(4);

        $pdf->Cell(78);
        $pdf->Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        $pdf->Ln(15);

        /////////////Cliente//////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(18,5,utf8_decode('Señores'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(120,5,strtoupper_utf8($provider_order->name),1,0,'L',false);
        $pdf->Ln(5);

        /////////////NIT//////////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(18,5,utf8_decode('NIT'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(70,5,utf8_decode($provider_order->nit),1,0,'L',false);

        ///////////Telefono///////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(15,5,utf8_decode('Teléfono'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(35,5,utf8_decode($provider_order->phone),1,0,'L',false);
        $pdf->Ln(5);

        /////////////Direccion////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(18,5,utf8_decode('Dirección'),0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(70,5,mb_substr(strtoupper_utf8($provider_order->address),0,48),1,0,'L',false);

        /////////////Ciudad///////////////
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(15,5,'Ciudad',0,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(35,5,strtoupper_utf8($city->city),1,0,'L',false);

        /////////////Fecha Factura////////
        $pdf->SetXY(148,43);
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(25,5,utf8_decode('Fecha Orden'),'L',0,'C',true);
        $pdf->SetXY(148,48);
        $pdf->Cell(25,10,$date,1,0,'C',false);

        /////////////Fecha Vencimiento////
        $pdf->SetXY(173,43);
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->Cell(27,5,utf8_decode('Fecha Entrega'),'R',0,'C',true);
        $pdf->SetXY(173,48);
        $pdf->Cell(27,10,$dateent,1,0,'C',false);
        $pdf->Ln(14);

        /////////////Detalle factura////////
        $pdf->SetDrawColor(210,210,210);
        $pdf->SetFillColor(241, 241, 241);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(5,5,utf8_decode('#'),1,0,'C',true);
        $pdf->Cell(14,5,utf8_decode('ID'),1,0,'C',true);
        $pdf->Cell(31,5,utf8_decode('Referencia'),1,0,'C',true);
        $pdf->Cell(78,5,utf8_decode('Producto'),1,0,'C',true);
        $pdf->Cell(10,5,utf8_decode('Unid'),1,0,'C',true);
        $pdf->Cell(21,5,utf8_decode('Valor unitario'),1,0,'C',true);
        $pdf->Cell(14,5,utf8_decode('IVA'),1,0,'C',true);
        $pdf->Cell(21,5,utf8_decode('Valor total'),1,0,'C',true);
        $pdf->Ln(5);

        $total = $iva = 0;
        $i = 1;
        foreach ($provider_order_products as $key => $value) {
            $pdf->SetFont('Arial', '', 7);
            $heightTotal = $pdf->GetY();
            $value->iva_amount = ($value->base_cost * ($value->iva / 100));
            $product_total_iva = ($value->cost * $value->quantity_order);
            $total += $value->cost * $value->quantity_order;
            $productName = wordwrap($value->product_name, 66, "\n", true);
            $pdf->SetX(56);
            /////////////Data///////////////////
            $pdf->MultiCell(78, 5, utf8_decode($productName), 1, 'L', false);
            $pdf->SetX(6);
            if ($pdf->GetY() < $heightTotal) {
                $heightTotal = 10;
            }
            $height = $pdf->GetY() - $heightTotal;
            $pdf->SetY($heightTotal);
            $pdf->Cell(5, $height, $i++, 1, 0, 'L', false);
            $pdf->Cell(14, $height, utf8_decode($value->store_product_id), 1, 0, 'L', false);
            $pdf->Cell(31, $height, str_limit(utf8_decode($value->reference), 28, ''), 1, 0, 'L', false);
            $pdf->SetX(134); /* Set Eje Y */
            $pdf->Cell(10, $height, utf8_decode($value->quantity_order), 1, 0, 'C', false);
            $pdf->Cell(21, $height, '$ ' . number_format($value->base_cost, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(14, $height, '$ ' . number_format($value->iva_amount, 0, ',', '.'), 1, 0, 'R', false);
            $pdf->Cell(21, $height, '$ ' . number_format($value->cost * $value->quantity_order, 0, ',', '.'), 1, 0, 'R',
                false);
            $pdf->Ln($height);
        }

        $pdf->SetFillColor(241, 241, 241);
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(114);
        $pdf->Cell(40,5,utf8_decode('Total a Pagar'),1,0,'L',true);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(40,5,'$ '.number_format($total, 0, ',', '.'),1,0,'R',true);
        $pdf->Ln(5);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(40,5,utf8_decode('VALOR EN LETRAS'),0,0,'L',false);
        $pdf->Ln(5);

        $pdf->Cell(3);
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(100,5,convert_number_to_word($total).' M/Cte',0,0,'L',false);
        $pdf->Ln(5);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(40,5,utf8_decode('OBSERVACIONES'),0,0,'L',false);
        $pdf->Ln(4);

        $pdf->Cell(3);
        $pdf->SetFont('Arial','',7);
        $warehouse_detail = empty($warehouse->warehouse) ? '' : mb_strtoupper("Entregar para bodega en {$warehouse->warehouse}");
        $pdf->Cell(100, 5, $warehouse_detail, 0, 0, 'L');
        $pdf->Ln(5);

        if (is_callable($file_updater)) {
            $file_updater($pdf);
        }

        $pdf->SetY(-34);
        $pdf->SetAutoPageBreak(true, 1);
        $pdf->Ln(7);

        $pdf->SetFont('Arial','',8);
        $pdf->SetFillColor(237, 237, 237);
        $pdf->SetFont('Arial','B',8);
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(20,5,utf8_decode('Página ').$pdf->PageNo(),0,0,'C');

        if ($print_file) {
            $pdf->Output();
            exit;
        }

        return $pdf->Output(null, 'S');
    }

    /**
     *  Genera el pdf de una devolucion de compra
     *
     * @param int $provider_id Datos de una orden
     * @return file  Respuesta del servicio
     */

    public static function order_return_pdf($id)
    {
        $provider_order_products = ProviderOrderDetail::where('provider_orders.id', $provider_id)
                                                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
                                                        ->join('products', 'provider_order_details.store_product_id', '=', 'products.id')
                                                        ->select(DB::raw('LEFT(products.name, 65) AS Nombre'),
                                                            DB::raw('products.id AS product_id'),
                                                            DB::raw('products.reference AS Referencia'),
                                                            DB::raw('products.cost AS Precio'),
                                                            DB::raw('products.provider_plu AS PLU'),
                                                            DB::raw('provider_order_details.quantity_pack AS "Embalaje solicitado"'),
                                                            DB::raw('products.provider_pack_type AS "Tipo de embalaje"'),
                                                            DB::raw('provider_order_details.quantity_order'))
                                                        ->get();

        $provider_order = ProviderOrder::join('providers', 'providers.id', '=', 'provider_orders.provider_id')
                                       ->leftjoin('provider_contacts', 'providers.id', '=', 'provider_contacts.provider_id')
                                       ->leftjoin('admin', 'provider_orders.admin_id', '=', 'admin.id')
                                       ->where('provider_orders.id', $provider_id)
                                       ->select('provider_orders.*', 'providers.name', 'providers.address','providers.nit', 'providers.city_id','provider_contacts.fullname', 'provider_contacts.phone', 'providers.maximum_order_report_time', DB::raw('admin.fullname AS admin'))->first();

        $date = !empty($provider_order->created_at) ? Carbon::parse($provider_order->created_at)->toDateString(): '';
        $dateent = !empty($provider_order->date) ? Carbon::parse($provider_order->date)->toDateString(): '';
        $iva = $date > '2016-12-31' ? 19 : 16;
        $city = City::where('id', $provider_order->city_id)->first();

        Fpdf::AddPage();
        Fpdf::SetLeftMargin(6);
        // Logo
        Fpdf::Image(url('/').'/admin_asset/img/logo_pdf.png',10,18,50);
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(75);
        Fpdf::Cell(30,5,'MERQUEO S A S',0,0,'C');
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetDrawColor(237,237,237);
        Fpdf::Cell(33);
        Fpdf::Cell(52,6,'ORDEN DE COMPRA '.strtoupper_utf8($city->city),'LTR',0,'C');
        Fpdf::Ln(6);

        // Información de contacto
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(76);
        Fpdf::Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
        Fpdf::Cell(31);
        Fpdf::SetFont('Arial','',8);
        Fpdf::Cell(52,9,'# '.$provider_order->id,'LBR',0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'CR 69K # 78 - 56',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,'TEL: 7561938',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        Fpdf::Ln(15);

        /////////////Cliente//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(18,5,utf8_decode('Señores'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(120,5,strtoupper_utf8($provider_order->name),1,0,'L',false);
        Fpdf::Ln(5);

        /////////////NIT//////////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(18,5,utf8_decode('NIT'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(70,5,utf8_decode($provider_order->nit),1,0,'L',false);

        ///////////Telefono///////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,utf8_decode('Teléfono'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(35,5,utf8_decode($provider_order->phone),1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Direccion////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(18,5,utf8_decode('Dirección'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(70,5,mb_substr(strtoupper_utf8($provider_order->address),0,48),1,0,'L',false);

        /////////////Ciudad///////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,'Ciudad',0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(35,5,strtoupper_utf8($city->city),1,0,'L',false);

        /////////////Fecha Factura////////
        Fpdf::SetXY(148,43);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(25,5,utf8_decode('Fecha Orden'),'L',0,'C',true);
        Fpdf::SetXY(148,48);
        Fpdf::Cell(25,10,$date,1,0,'C',false);

        /////////////Fecha Vencimiento////
        Fpdf::SetXY(173,43);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(27,5,utf8_decode('Fecha Entrega'),'R',0,'C',true);
        Fpdf::SetXY(173,48);
        Fpdf::Cell(27,10,$dateent,1,0,'C',false);
        Fpdf::Ln(14);

        /////////////Detalle factura////////
        Fpdf::SetDrawColor(210,210,210);
        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(35,5,utf8_decode('ID'),1,0,'C',true);
        Fpdf::Cell(29,5,utf8_decode('PLU'),1,0,'C',true);
        Fpdf::Cell(78,5,utf8_decode('Producto'),1,0,'C',true);
        Fpdf::Cell(10,5,utf8_decode('Unid'),1,0,'C',true);
        Fpdf::Cell(21,5,utf8_decode('Valor unitario'),1,0,'C',true);
        Fpdf::Cell(21,5,utf8_decode('Valor total'),1,0,'C',true);
        Fpdf::Ln(5);
        $totalitems = 0;
        //debug($provider_order_products);
        foreach ($provider_order_products as $key => $value) {
            Fpdf::SetFont('Arial','',7);
            $item_sum = ($value->quantity_order * $value->Precio);
            $totalitems = $totalitems + $item_sum;
            /////////////Data///////////////////
            Fpdf::Cell(35,5,utf8_decode($value->product_id),1,0,'L',false);
            Fpdf::Cell(29,5,utf8_decode($value->PLU),1,0,'L',false);
            Fpdf::Cell(78,5,utf8_decode($value->Nombre),1,0,'L',false);
            Fpdf::Cell(10,5,utf8_decode($value->quantity_order),1,0,'C',false);
            Fpdf::Cell(21,5,'$ '.number_format($value->Precio, 0, ',', '.'),1,0,'R',false);
            Fpdf::Cell(21,5,'$ '.number_format($item_sum, 0, ',', '.'),1,0,'R',false);
            Fpdf::Ln(5);
        }
        Fpdf::Ln(5);
        $iva = ($totalitems * $iva) / 100;

        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(114);
        Fpdf::Cell(40,5,utf8_decode('Total Bruto'),1,0,'L',false);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(40,5,'$ '.number_format($totalitems, 0, ',', '.'),1,0,'R',false);
        Fpdf::Ln(5);

        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(114);
        Fpdf::Cell(40,5,utf8_decode('IVA'),1,0,'L',false);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(40,5,'$ '.number_format($iva, 0, ',', '.'),1,0,'R',false);
        Fpdf::Ln(5);

        $total = round($totalitems + $iva);

        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(114);
        Fpdf::Cell(40,5,utf8_decode('Total a Pagar'),1,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(40,5,'$ '.number_format($total, 0, ',', '.'),1,0,'R',true);
        Fpdf::Ln(5);

        // Fpdf::SetFont('Arial','B',7);
        // Fpdf::Cell(40,5,utf8_decode('CONDICION DE PAGO'),0,0,'L',false);
        // Fpdf::Ln(5);

        // Fpdf::Cell(3);
        // Fpdf::SetFont('Arial','',7);
        // Fpdf::Cell(100,5,'Credito Clientes Nacionales Cuota 1 F - 003- 225 Vence el '.$date,0,0,'L',false);
        // Fpdf::Cell(20,5,'$ '.number_format($total, 0, ',', '.'),0,0,'L',false);
        // Fpdf::Ln(5);

        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(40,5,utf8_decode('VALOR EN LETRAS'),0,0,'L',false);
        Fpdf::Ln(5);

        Fpdf::Cell(3);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(100,5,convert_number_to_word($total).' M/Cte',0,0,'L',false);
        Fpdf::Ln(5);

        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(40,5,utf8_decode('OBSERVACIONES'),0,0,'L',false);
        Fpdf::Ln(4);
        Fpdf::SetY(-34);
        Fpdf::SetAutoPageBreak(true, 1);
        Fpdf::Cell(195,5,'FACTURA IMPRESA POR COMPUTADOR GRACIAS POR PREFERIRNOS',0,0,'C');
        Fpdf::Ln(7);

        Fpdf::SetFont('Arial','',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::MultiCell(195,5,utf8_decode('A esta factura de venta aplican las normas relativas a la letra de cambio (artículo 5 Ley 1231 de 2008). Con esta el Comprador declara haber
            recibido real y materialmente las mercancías o prestación de servicios descritos en este título - Valor. Número de Resolución 320001363550
            aprobado en 2016-02-11 prefijo FB desde el número 1 al 400000
            Regimen Común - Actividad Económica 7310 Tarifa 009.660'),1,'C',true);

        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(175,5,'Original',0,0,'C');
        Fpdf::SetFont('Arial','I',8);
        Fpdf::Cell(20,5,utf8_decode('Página ').Fpdf::PageNo(),0,0,'C');
        Fpdf::Output();
        exit;
    }

    /**
     *  Genera el archivo P de un recibo de bodega
     *
     * @param int $provider_id Datos de una orden
     * @return file  Respuesta del servicio
     */
    public static function generate_order_reception($reception_id)
    {
        $movement = ProviderOrderReception::select('provider_order_receptions.*', 'admin.fullname AS admin_name', 'provider_orders.provider_name')
                                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
                                        ->join('admin', 'provider_order_receptions.admin_id', '=', 'admin.id')
                                        ->where('provider_order_receptions.id', $reception_id)
                                        ->groupBy('provider_orders.id')
                                        ->first();
        $products = $movement->getProducts();
        $simple_products = $products->filter(function ($product)
        {
            return $product->status == 'Recibido' || $product->status == 'Parcialmente recibido' && $product->type == 'Simple';
        });

        $provider_products = $movement->getProductGroupDetails();
        $grouped_products = $provider_products->filter(function ($product)
        {
            return $product->reception_detail_product_group_status == 'Recibido' || $product->reception_detail_product_group_status == 'Parcialmente recibido';
        });

        $provider_order = ProviderOrder::with('warehouse.city')->find($movement->provider_order_id);
        $provider = Provider::find($provider_order->provider_id);

        // Información de merqueo
        if ( $provider ) {
            if ( $provider_order->warehouse->city_id == 1 ) {
                $e_code = 1;
            }elseif ( $provider_order->warehouse->city_id == 2 ) {
                $e_code = 2;
            }
        }

        Fpdf::AddPage();
        Fpdf::SetDrawColor(237,237,237);

        // Logo
        Fpdf::Image(url('/').'/admin_asset/img/logo_pdf.png',10,18,50);
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'MERQUEO S A S',0,0,'C');
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(28);
        Fpdf::Cell(52,6,'RECIBO DE BODEGA ','LTR',0,'C');
        Fpdf::Ln(6);

        // Información de merqueo
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'NIT : 900.871.444 - 8',0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,'# '.$movement->id,'LR',0,'C');
        Fpdf::Ln(4);

        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'CR 69K # 78 - 56',0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,'E - '.$e_code,'LR',0,'C');
        Fpdf::Ln(4);

        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'TEL: 7561938',0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        // Fpdf::Cell(52,9,6539,'LR',0,'C');
        Fpdf::Cell(52,9,$movement->movement_consecutive_e,'LR',0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,utf8_decode($provider_order->warehouse->city->city) .' - '. utf8_decode($provider_order->warehouse->warehouse) ,'LBR',0,'C');
        Fpdf::Ln(15);

        /////////////Proveedor:////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Proveedor'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(62,5,mb_substr(strtoupper_utf8($movement->provider_name),0,48),1,0,'L',false);

        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Transportador'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(63,5,strtoupper_utf8($movement->transporter),1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Transportador//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Orden de compra'),0,0,'L',true);
        Fpdf::SetFont('Arial','',8);
        Fpdf::Cell(62,5,utf8_decode($movement->provider_order_id),1,0,'L',false);

        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Número de placa'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(63,5,utf8_decode($movement->plate),1,0,'L',false);
        Fpdf::Ln(5);


        /////////////Placa//////////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Número de factura'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(62,5,utf8_decode($movement->invoice_number),1,0,'L',false);

        ///////////Nombre del Conductor///////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Nombre conductor'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(63,5,utf8_decode($movement->driver_name),1,0,'L',false);
        Fpdf::Ln(5);

        $date = !empty($movement->created_at) ? Carbon::parse($movement->created_at)->toDateString(): '';
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Fecha de recibo'),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(62,5,$date,1,0,'L',false);

        /////////////Licencia de Conducción:////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Cédula de transportador'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(63,5,mb_substr(strtoupper_utf8($movement->driver_document_number),0,48),1,0,'L',false);
        Fpdf::Ln(5);

        Fpdf::Ln(14);


        /////////////Detalle recibo de bodega////////
        Fpdf::SetDrawColor(210,210,210);
        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(10,5,utf8_decode('ID'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Referencia'),1,0,'C',true);
        Fpdf::Cell(51,5,utf8_decode('Producto'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Und. medida'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Und. en factura'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Und. recibidas'),1,0,'C',true);
        Fpdf::Cell(16,5,utf8_decode('Almacenaje'),1,0,'C',true);
        Fpdf::Cell(19,5,utf8_decode('Localización'),1,0,'C',true);
        Fpdf::Cell(19,5,utf8_decode('Almacenado'),1,0,'C',true);
        Fpdf::Ln(5);

        foreach ($simple_products as $key => $product) {
            Fpdf::SetFont('Arial','',6);
            /////////////Data///////////////////
            Fpdf::Cell(10,5,utf8_decode($product->product_id),1,0,'C',false);
            Fpdf::Cell(20,5,utf8_decode($product->reference),1,0,'C',false);
            Fpdf::Cell(51,5,substr(utf8_decode($product->product_name),0,48),1,0,'L',false);
            Fpdf::Cell(20,5,utf8_decode($product->quantity.' '.$product->unit),1,0,'C',false);
            Fpdf::Cell(20,5,utf8_decode($product->quantity_expected),1,0,'C',false);
            Fpdf::Cell(20,5,utf8_decode($product->quantity_received),1,0,'C',false);
            Fpdf::Cell(16,5,$product->storage,1,0,'C',false);
            Fpdf::Cell(19,5,$product->storage_position,1,0,'C',false);
            Fpdf::Cell(19,5,' ',1,0,'R',false);
            Fpdf::Ln(5);
        }
        unset($key);
        unset($product);
        if ( !empty( $grouped_products )  ) {
            foreach ($grouped_products as $key => $product) {
                Fpdf::SetFont('Arial','',6);
                /////////////Data///////////////////
                Fpdf::Cell(10,5,utf8_decode($product->product_id),1,0,'C',false);
                Fpdf::Cell(20,5,utf8_decode($product->reference),1,0,'C',false);
                Fpdf::Cell(51,5,substr(utf8_decode($product->product_name),0,48),1,0,'L',false);
                Fpdf::Cell(20,5,utf8_decode($product->quantity.' '.$product->unit),1,0,'C',false);
                Fpdf::Cell(20,5,utf8_decode($product->reception_detail_product_group_quantity_expected),1,0,'C',false);
                Fpdf::Cell(20,5,utf8_decode($product->reception_detail_product_group_quantity_received),1,0,'C',false);
                Fpdf::Cell(16,5,$product->storage,1,0,'C',false);
                Fpdf::Cell(19,5,$product->storage_position,1,0,'C',false);
                Fpdf::Cell(19,5,' ',1,0,'R',false);
                Fpdf::Ln(5);
            }
        }
        Fpdf::Ln(5);

        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(40,5,utf8_decode('OBSERVACIONES'),0,0,'L',false);
        Fpdf::Ln(4);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Ln(5);
        Fpdf::MultiCell(195,5,utf8_decode($movement->observation),1,'L',true);

        Fpdf::Ln(4);

        Fpdf::SetY(-50);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(130,5,utf8_decode('PERSONA QUE ALMACENA'),0,0,'L',false);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(65,5,utf8_decode('COORDINADOR DE BODEGA'),0,0,'L',false);
        Fpdf::Ln(10);

        Fpdf::Cell(130,5,'___________________________________',0,0,'L');
        Fpdf::Cell(65,5,'___________________________________',0,0,'L');
        Fpdf::Ln(5);

        Fpdf::Cell(130,5,'C.C.',0,0,'L');
        Fpdf::Cell(65,5,'C.C.',0,0,'L');
        Fpdf::Ln(7);

        Fpdf::SetFont('Arial','I',8);
        Fpdf::SetY(-8);
        Fpdf::SetX(90);
        Fpdf::SetAutoPageBreak(true, 1);
        Fpdf::Cell(20,5,utf8_decode('Página ').Fpdf::PageNo(),0,0,'C');
        // $this->Footer();
        Fpdf::Output();
        exit;
    }

    /**
     *  Genera el pdf de un recibo de bodega
     *
     * @param int $provider_id Datos de una orden
     * @return file  Respuesta del servicio
     */

    public static function generate_order_reception_p($reception_id)
    {
        $reception = ProviderOrderReception::find($reception_id);
        $provider_order = ProviderOrder::with('warehouse.city')->find($reception->provider_order_id);
        $store = Store::getActiveStores( $provider_order->warehouse->city_id );
        $movement = ProviderOrderReception::select('provider_order_receptions.*', 'admin.fullname AS admin_name', 'provider_orders.provider_name', 'provider_orders.provider_id')
                                        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
                                        ->join('admin', 'provider_order_receptions.admin_id', '=', 'admin.id')
                                        ->where('provider_order_receptions.id', $reception_id)
                                        ->groupBy('provider_orders.id')
                                        ->first();

        $products = $movement->getProducts();
        $simple_products = $products->filter(function ($product)
        {
            return $product->type == 'Simple';
        });
        $grouped_products = $movement->getProductGroupDetails();

        $provider = Provider::find($movement->provider_id);

        Fpdf::AddPage();

        // Logo
        Fpdf::Image(url('/').'/admin_asset/img/logo_pdf.png',10,18,50);
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'MERQUEO S A S',0,0,'C');
        Fpdf::SetFont('Arial','B',10);
        Fpdf::SetDrawColor(237,237,237);
        Fpdf::Cell(28);
        Fpdf::Cell(52,6,'PROVEEDORES BODEGA '.( $provider_order->warehouse->city_id == 1 ? 'BTA' : 'MED' ),0,0,'C');
        Fpdf::Ln(6);

        // Información de merqueo
        if ( $provider->type == 'Para faltantes' ) {
            if ( $provider_order->warehouse->city_id == 1 ) {
                if ( $provider_order->warehouse->id == 1 ) {
                    $p_code = 3;
                }else{
                    $p_code = 8;
                }
            }elseif ( $provider_order->warehouse->city_id == 2 ) {
                $p_code = 5;
            }
        }else{
            if ( $provider_order->warehouse->city_id == 1 ) {
                if ( $provider_order->warehouse->id == 1 ) {
                    $p_code = 2;
                }else{
                    $p_code = 7;
                }
            }elseif ( $provider_order->warehouse->city_id == 2 ) {
                $p_code = 4;
            }
        }
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'NIT : 900.871.444 - 8',0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,'P - '.$p_code,0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'CRA 69K #78-56',0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,$movement->movement_consecutive_p,0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'TEL: 5404058',0,0,'C');
        Fpdf::Cell(28);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,utf8_decode($provider_order->warehouse->city->city).' - '.utf8_decode($provider_order->warehouse->warehouse) ,0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        Fpdf::Ln(15);

        /////////////Proveedor:////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,utf8_decode('Señores'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(49,5,mb_substr(strtoupper_utf8($movement->provider_name),0,48),1,0,'L',false);
        Fpdf::Cell(15);
        Fpdf::Cell(49);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(33, 5, utf8_decode('Fecha Comprobante'), 0, 0, 'C', false);
        Fpdf::Cell(33, 5, utf8_decode('Fecha Vencimiento'), 0, 0, 'C', false);
        Fpdf::Ln(5);

        /////////////Transportador//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,utf8_decode('NIT'),0,0,'L',true);
        Fpdf::SetFont('Arial','',8);
        Fpdf::Cell(49,5,utf8_decode($provider_order->nit),1,0,'L',false);

        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,utf8_decode('Teléfono'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(49,5,utf8_decode($provider_order->phone),1,0,'L',false);
        Fpdf::Cell(33, 5, utf8_decode( Carbon::parse($movement->voucher_date)->toDateString() ), 0, 0, 'C', false);
        Fpdf::Cell(33, 5, utf8_decode( Carbon::parse($movement->expiration_date)->toDateString() ), 0, 0, 'C', false);
        Fpdf::Ln(5);


        /////////////Placa//////////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,utf8_decode('Dirección'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(49,5,utf8_decode($provider_order->address),1,0,'L',false);

        ///////////Nombre del Conductor///////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(15,5,utf8_decode('Ciudad'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        if ( $provider_order->warehouse->city_id == 1 ) {
            Fpdf::Cell(49,5,utf8_decode('BOGOTA D.C.-BOGOTA-COLOMBIA'),1,0,'L',false);
        }else{
            Fpdf::Cell(49,5,utf8_decode('MEDELLIN -MEDELLIN-COLOMBIA'),1,0,'L',false);
        }
        Fpdf::Ln(5);
        Fpdf::Ln(14);

        /////////////Detalle recibo de bodega////////
        Fpdf::SetDrawColor(210,210,210);
        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',8);
        //
        Fpdf::Cell(25,5,utf8_decode('Código'),1,0,'C',true);
        Fpdf::Cell(70,5,utf8_decode('Descripción'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Unidad'),1,0,'C',true);
        Fpdf::Cell(21,5,utf8_decode('Cantidad'),1,0,'C',true);
        Fpdf::Cell(10,5,utf8_decode('IVA'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Valor IVA'),1,0,'C',true);
        Fpdf::Cell(29,5,utf8_decode('Valor Total'),1,0,'C',true);
        Fpdf::Ln(5);

        foreach ($products as $key => $product) {
            Fpdf::SetFont('Arial','',7);

            /////////////Data///////////////////
            Fpdf::Cell(25,5,utf8_decode( str_pad($product->accounting_line, 3, '0', STR_PAD_LEFT) . str_pad($product->accounting_group, 4, '0', STR_PAD_LEFT) . str_pad($product->accounting_code, 6, '0', STR_PAD_LEFT) ),1,0,'C',false);
            // Fpdf::Cell(22,5,utf8_decode($product->reference),1,0,'C',false);
            Fpdf::Cell(70,5,substr(utf8_decode($product->product_name),0,48),1,0,'L',false);
            Fpdf::Cell(20,5,utf8_decode($product->quantity.' '.$product->unit),1,0,'C',false);
            Fpdf::Cell(21,5,utf8_decode( number_format($product->quantity_received, 2) ),1,0,'R',false);
            Fpdf::Cell(10,5,( number_format($product->iva, 2) ),1,0,'R',false);
            Fpdf::Cell(20,5,( number_format($product->iva_amount, 2) ),1,0,'R',false);
            Fpdf::Cell(29,5,( number_format($product->sub_total_cost, 2) ),1,0,'R',false);
            // Fpdf::Cell(19,5,$product->storage_position,1,0,'C',false);
            // Fpdf::Cell(19,5,' ',1,0,'R',false);
            Fpdf::Ln(5);
        }
        unset($key);
        unset($product);
        if ( !empty($grouped_products) ) {
            foreach ($grouped_products as $key => $product) {
                Fpdf::SetFont('Arial','',7);

                /////////////Data///////////////////
                Fpdf::Cell(25,5,utf8_decode( str_pad($product->accounting_line, 3, '0', STR_PAD_LEFT) . str_pad($product->accounting_group, 4, '0', STR_PAD_LEFT) . str_pad($product->accounting_code, 6, '0', STR_PAD_LEFT) ),1,0,'C',false);
                // Fpdf::Cell(22,5,utf8_decode($product->reference),1,0,'C',false);
                Fpdf::Cell(70,5,substr(utf8_decode($product->product_name),0,48),1,0,'L',false);
                Fpdf::Cell(20,5,utf8_decode($product->quantity.' '.$product->unit),1,0,'C',false);
                Fpdf::Cell(21,5,utf8_decode( number_format($product->quantity_received, 2) ),1,0,'R',false);
                Fpdf::Cell(10,5,( number_format($product->iva, 2) ),1,0,'R',false);
                Fpdf::Cell(20,5,( number_format($product->iva_amount, 2) ),1,0,'R',false);
                Fpdf::Cell(29,5,( number_format($product->sub_total_cost, 2) ),1,0,'R',false);
                Fpdf::Ln(5);
            }
        }

        Fpdf::Ln(5);
        Fpdf::Cell(115);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(51,5,'Total Bruto',1,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(29,5, number_format($movement->getSubTotal(), 2),1,0,'R',false);
        Fpdf::Ln(5);
        $iva_details = $movement->getIvaDetails();
        if ( count($iva_details) ) {
            foreach ($iva_details as $key => $iva_detail) {
                Fpdf::Cell(115);
                Fpdf::SetFont('Arial','B',8);
                Fpdf::Cell(51,5,'IVA '.$iva_detail['iva'].'%',1,0,'L',true);
                Fpdf::SetFont('Arial','',7);
                Fpdf::Cell(29,5, number_format($iva_detail['total'], 2) ,1,0,'R',false);
                Fpdf::Ln(5);
            }
        }
        if ( $rete_ica = $movement->rete_ica_amount ) {
            Fpdf::Cell(115);
            Fpdf::SetFont('Arial','B',8);
            Fpdf::Cell(51,5, utf8_decode('Retención ICA'),1,0,'L',true);
            Fpdf::SetFont('Arial','',7);
            Fpdf::Cell(29,5, number_format($rete_ica, 2) ,1,0,'R',false);
            Fpdf::Ln(5);
        }
        if ( $rete_fuente = $movement->rete_fuente_amount ) {
            Fpdf::Cell(115);
            Fpdf::SetFont('Arial','B',8);
            Fpdf::Cell(51,5, utf8_decode('Retención en la Fuente'),1,0,'L',true);
            Fpdf::SetFont('Arial','',7);
            Fpdf::Cell(29,5, number_format($rete_fuente, 2) ,1,0,'R',false);
            Fpdf::Ln(5);
        }
        if ( $consumption_tax = $movement->getConsumptionTax() ) {
            Fpdf::Cell(115);
            Fpdf::SetFont('Arial', 'B', 8);
            Fpdf::Cell(51, 5, 'Impuesto al Consumo', 1, 0, 'L', true);
            Fpdf::SetFont('Arial', '', 7);
            Fpdf::Cell(29, 5, number_format($consumption_tax, 2), 1, 0, 'R', false);
            Fpdf::Ln(5);
        }
        if( $total_credit_note = $movement->getTotal(false)) {
            Fpdf::Cell(115);
            Fpdf::SetFont('Arial','B',8);
            Fpdf::Cell(51,5,utf8_decode('Total Nota Crédito'),1,0,'L',true);
            Fpdf::SetFont('Arial','',7);
            Fpdf::Cell(29,5, number_format($movement->getTotal(false), 2) ,1,0,'R',false);
            Fpdf::Ln(10);
        }
        Fpdf::Cell(115);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(51,5,'Total a Pagar',1,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(29,5, number_format($movement->getTotal() - $movement->getTotal(false), 2) ,1,0,'R',false);
        Fpdf::Ln(10);


        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(40,5,utf8_decode('CONDICION DE PAGO'),0,0,'L',false);
        Fpdf::Ln(4);
        Fpdf::SetFont('Arial','',7);
        Fpdf::MultiCell(195,5,utf8_decode( 'Proveedores                    Cuota 1 '.$provider_order->code.' - '.$movement->movement_consecutive_p.' Vence el '.Carbon::parse($movement->expiration_date)->toDateString() . '         '.number_format($movement->getTotal(), strlen(substr(strrchr($movement->getTotal(), "."), 1))) ),0,'L',false);
        Fpdf::Ln(5);


        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(40,5,utf8_decode('VALOR EN LETRAS'),0,0,'L',false);
        Fpdf::Ln(4);
        Fpdf::SetFont('Arial','',7);
        Fpdf::MultiCell(195,5,utf8_decode( convert_number_to_word($movement->getTotal()).' M/Cte' ),0,'L',false);
        Fpdf::Ln(5);

        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(40,5,utf8_decode('OBSERVACIONES'),0,0,'L',false);
        Fpdf::Ln(4);
        Fpdf::SetFont('Arial','',7);
        Fpdf::MultiCell(195,5,utf8_decode( 'FV '.$movement->invoice_number),0,'L',false);
        Fpdf::Ln(5);

        Fpdf::Ln(4);

        Fpdf::SetY(-50);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(130,5,utf8_decode('ELABORADO POR'),0,0,'L',false);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(65,5,utf8_decode('APROBADO POR'),0,0,'L',false);
        Fpdf::Ln(10);

        Fpdf::Cell(130,5,'___________________________________',0,0,'L');
        Fpdf::Cell(65,5,'___________________________________',0,0,'L');
        Fpdf::Ln(5);

        /*Fpdf::Cell(130,5,'C.C.',0,0,'L');
        Fpdf::Cell(65,5,'C.C.',0,0,'L');*/
        Fpdf::Ln(7);

        Fpdf::SetFont('Arial','I',8);
        Fpdf::SetY(-8);
        Fpdf::SetX(90);
        Fpdf::SetAutoPageBreak(true, 1);
        Fpdf::Cell(20,5,utf8_decode('Página ').Fpdf::PageNo(),0,0,'C');
        Fpdf::Output();
        exit;
    }

    /**
     *  Genera el pdf de una salida de bodega
     *
     * @param int $dispatch_id Datos de una orden de salida de bodega
     * @return file  Respuesta del servicio
     */

    public static function generate_dispatched_orders($dispatch_id)
    {
        $products = DispatchedOrderDetail::join('orders', 'orders.id', '=', 'dispatched_order_details.order_id')
                                          ->join('dispatched_orders', 'dispatched_orders.id', '=', 'dispatched_order_details.dispatch_id')
                                          ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                                          ->where('dispatched_order_details.dispatch_id', $dispatch_id)
                                          ->select('dispatched_order_details.order_id',
                                                   'dispatched_order_details.product_unit',
                                                   DB::raw('CONCAT(order_groups.user_firstname," ",order_groups.user_lastname) AS client'),
                                                   DB::raw('COUNT(dispatched_order_details.product_quantity) AS quantity'),
                                                   DB::raw('SUM(dispatched_order_details.quantity_dispatched) AS dispatched'))
                                          ->groupBy('orders.id')
                                          ->orderBy('orders.planning_route','orders.plannig_sequence')
                                          ->get();

        $movement = DispatchedOrder::where('dispatched_orders.id', '=', $dispatch_id)
                                       ->join('drivers', 'drivers.id', '=', 'dispatched_orders.driver_id')
                                       ->join('admin', 'admin.id', '=', 'dispatched_orders.admin_id')
                                       ->select('dispatched_orders.*', 'drivers.number_license','admin.fullname')
                                       ->first();
        Fpdf::AddPage();
        Fpdf::SetLeftMargin(6);
        // Logo
        Fpdf::Image(url('/').'/admin_asset/img/logo_pdf.png',10,18,50);
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(75);
        Fpdf::Cell(30,5,'MERQUEO S A S',0,0,'C');
        Fpdf::SetFont('Arial','B',10);
        Fpdf::SetDrawColor(237,237,237);
        Fpdf::Cell(33);
        Fpdf::Cell(52,6,'SALIDA DE BODEGA ','LTR',0,'C');
        Fpdf::Ln(6);

        // Información de merqueo
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(76);
        Fpdf::Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
        Fpdf::Cell(31);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,'# '.$movement->id,'LBR',0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'CR 69K # 78 - 56',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,'TEL: 7561938',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        Fpdf::Ln(15);

        /////////////Transportador//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Transportador'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(80,5,utf8_decode($movement->transporter_name),1,0,'L',false);

        $date = !empty($movement->date) ? format_date('normal_with_time', $movement->date): '';
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Fecha de creación'),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(45,5,$date,1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Conductor//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Nombre conductor'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(80,5,utf8_decode($movement->driver_name),1,0,'L',false);

        $dispatch_date = !empty($movement->dispatch_date) ? format_date('normal_with_time', $movement->dispatch_date): '';
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Fecha de despacho'),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(45,5,$dispatch_date,1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Placa//////////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Número de placa'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(80,5,utf8_decode($movement->vehicle_plate),1,0,'L',false);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Usuario'),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(45,5,$movement->fullname,1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Licencia de Conducción:////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Licencia de conducción'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(160,5,mb_substr(strtoupper_utf8($movement->number_license),0,48),1,0,'L',false);
        Fpdf::Ln(14);

        /////////////Detalle recibo de bodega////////
        Fpdf::SetDrawColor(210,210,210);
        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(30,5,utf8_decode('# Pedido'),1,0,'C',true);
        Fpdf::Cell(75,5,utf8_decode('Cliente'),1,0,'C',true);
        Fpdf::Cell(45,5,utf8_decode('Cantidad de productos'),1,0,'C',true);
        Fpdf::Cell(45,5,utf8_decode('Unidades despachadas'),1,0,'C',true);
        Fpdf::Ln(5);

        foreach ($products as $key => $value) {
            Fpdf::SetFont('Arial','',7);

            /////////////Data///////////////////
            Fpdf::Cell(30,5,utf8_decode($value->order_id),1,0,'C',false);
            Fpdf::Cell(75,5,substr(utf8_decode($value->client),0,200),1,0,'L',false);
            Fpdf::Cell(45,5,utf8_decode($value->quantity),1,0,'C',false);
            Fpdf::Cell(45,5,utf8_decode($value->dispatched),1,0,'C',false);
            Fpdf::Ln(5);
        }
        Fpdf::Ln(5);

        Fpdf::SetY(-50);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(195,5,utf8_decode('PERSONA QUE DESPACHA'),0,0,'L',false);
        Fpdf::Ln(10);

        Fpdf::Cell(195,5,'___________________________________',0,0,'L');
        Fpdf::Ln(5);

        Fpdf::Cell(195,5,'C.C.',0,0,'L');
        Fpdf::Ln(7);

        Fpdf::SetFont('Arial','I',8);
        Fpdf::SetY(-8);
        Fpdf::SetX(90);
        Fpdf::SetAutoPageBreak(true, 1);
        Fpdf::Cell(20,5,utf8_decode('Página ').Fpdf::PageNo(),0,0,'C');
        Fpdf::Output();
        exit;
    }

    /**
     *  Genera el pdf de una devolución
     *
     * @param int $return_id Datos de una orden de devolución de bodega
     * @return file  Respuesta del servicio
     */

    public static function order_return_bodega_pdf($return_id)
    {
        $order_return = OrderReturn::select('order_returns.*', 'admin.fullname AS admin_name', DB::raw("count(store_product_id) AS return_stock"),
                                        DB::raw("COUNT(order_return_details.id) AS quantity_products"))
                                        ->join('admin', 'admin_id', '=', 'admin.id')
                                        ->join('order_return_details', 'order_return_details.order_return_id', '=', 'order_returns.id')
                                        ->where('order_returns.id', $return_id)
                                        ->groupBy('order_returns.id')
                                        ->first();

        $returned_products = OrderReturnDetail::select('order_return_details.status', 'products.reference', 'provider_plu', 'order_return_details.store_product_id', 'product_image_url', 'product_name', 'product_quantity', 'product_unit', 'order_id', DB::raw("count(store_product_id) AS return_stock"))
                                              ->join('store_products', 'order_return_details.store_product_id', '=', 'store_products.id')
                                              ->join('products', 'products.id', '=', 'product_id')
                                              ->join('order_returns', 'order_returns.id', '=', 'order_return_id')
                                              ->where('order_return_id', $return_id)
                                              ->groupBy('store_product_id')
                                              ->get();
        Fpdf::AddPage();
        Fpdf::SetLeftMargin(6);
        // Logo
        Fpdf::Image(url('/').'/admin_asset/img/logo_pdf.png',10,18,50);
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(75);
        Fpdf::Cell(30,5,'MERQUEO S A S',0,0,'C');
        Fpdf::SetFont('Arial','B',10);
        Fpdf::SetDrawColor(237,237,237);
        Fpdf::Cell(33);
        Fpdf::Cell(52,6,'DEVOLUCION DE BODEGA ','LTR',0,'C');
        Fpdf::Ln(6);

        // Información de merqueo
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(76);
        Fpdf::Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
        Fpdf::Cell(31);
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(52,9,'# '.$order_return->id,'LBR',0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'CR 69K # 78 - 56',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,'TEL: 7561938',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        Fpdf::Ln(15);

        /////////////Transportador//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Transportador'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(80,5,utf8_decode($order_return->transporter_name),1,0,'L',false);

        $date = !empty($order_return->date) ? format_date('normal_with_time', $order_return->date): '';
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Fecha de creación'),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(45,5,$date,1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Conductor//////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Nombre conductor'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(80,5,utf8_decode($order_return->driver_name),1,0,'L',false);

        $dispatch_date = !empty($order_return->created_at) ? format_date('normal_with_time', $order_return->created_at): '';
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Fecha de devolución'),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(45,5,$dispatch_date,1,0,'L',false);
        Fpdf::Ln(5);

        /////////////Placa//////////////////
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode('Número de placa'),0,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(80,5,utf8_decode($order_return->vehicle_plate),1,0,'L',false);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::Cell(35,5,utf8_decode(' '),'L',0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(45,5,'',1,0,'L',false);
        Fpdf::Ln(5);

        Fpdf::Ln(5);

        /////////////Detalle recibo de bodega////////
        Fpdf::SetDrawColor(210,210,210);
        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(30,5,utf8_decode('# Referencia'),1,0,'C',true);
        Fpdf::Cell(25,5,utf8_decode('PLU'),1,0,'C',true);
        Fpdf::Cell(55,5,utf8_decode('Producto'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Unid. Med.'),1,0,'C',true);
        Fpdf::Cell(20,5,utf8_decode('Pedido #'),1,0,'C',true);
        Fpdf::Cell(30,5,utf8_decode('Unidades a devolver'),1,0,'C',true);
        Fpdf::Cell(15,5,utf8_decode('Estado'),1,0,'C',true);
        Fpdf::Ln(5);

        foreach ($returned_products as $key => $value) {
            Fpdf::SetFont('Arial','',7);

            /////////////Data///////////////////
            Fpdf::Cell(30,5,utf8_decode($value->reference),1,0,'C',false);
            Fpdf::Cell(25,5,substr(utf8_decode($value->provider_plu),0,200),1,0,'C',false);
            Fpdf::Cell(55,5,utf8_decode($value->product_name),1,0,'C',false);
            Fpdf::Cell(20,5,utf8_decode($value->product_quantity.' '.$value->product_unit),1,0,'C',false);
            Fpdf::Cell(20,5,utf8_decode($value->order_id),1,0,'C',false);
            Fpdf::Cell(30,5,utf8_decode($value->return_stock),1,0,'C',false);
            Fpdf::Cell(15,5,utf8_decode($value->status),1,0,'C',false);
            Fpdf::Ln(5);
        }
        Fpdf::Ln(5);

        Fpdf::SetY(-50);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(195,5,utf8_decode('PERSONA QUE RECIBE'),0,0,'L',false);
        Fpdf::Ln(4);

        Fpdf::Cell(195,5,'___________________________________',0,0,'L');
        Fpdf::Ln(5);

        Fpdf::Cell(195,5,'C.C.',0,0,'L');
        Fpdf::Ln(7);

        Fpdf::SetFont('Arial','I',8);
        Fpdf::SetY(-8);
        Fpdf::SetX(90);
        Fpdf::SetAutoPageBreak(true, 1);
        Fpdf::Cell(20,5,utf8_decode('Página ').Fpdf::PageNo(),0,0,'C');
        Fpdf::Output();
        exit;
    }

    public function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print centered page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}
