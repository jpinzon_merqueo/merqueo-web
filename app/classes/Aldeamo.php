<?php

/**
 * Interfaz con la API de Aldeamo para envìo de mensajes de  texto
 */
class Aldeamo extends Eloquent
{
	private $url_rest_service = '';
	private $username;
	private $password;

	/**
	* Constructor de la clase, genera las URL de conexión para los servicios REST y SOAP,
     * apartir de los parametros almacenados en la configuración
	*/
	public function __construct(){

		$ip = Config::get('app.aldeamo.ip');
		$port = Config::get('app.aldeamo.port');
		$this->username = Config::get('app.aldeamo.username');
		$this->password = Config::get('app.aldeamo.password');
		$this->url_rest_service = empty($port)
            ? $ip . '/SmsiWS/'
            : $ip . ':' . $port . '/SmsiWS/';
	}

    /**
     * Funcion para enviar sms rest server usando GET
     *
     * @param string $recipient numero de teléfono de quien recibe el mensaje
     * @param string $message mensaje a ser enviado
     * @param string $country codigo internacional del pais
     * @param string $operator de la red movil valores permitidos ( 57-movistar, 57-tigo, 57-claro, 57-avantel)
     * @return bool $response Respuesta del servicio
     * @throws Exception
     */
	public function sms_send_post($recipient, $message, $country = '57', $operator = '')
	{
		$url = $this->url_rest_service.'smsSendPost/';
		$content = $this->create_xml_rest_post($recipient, $message, $country, $operator);
		$response = $this->send_data($url, 'POST', $content);

		if( !$response ){
			throw new \Exception("Error Processing Request", 1);
		}

        $result = json_decode($response);

        if (empty($result->status) || $result->status < 1) {
            throw new \Exception($this->getErrorMessage(
                empty($result->status) ? '' : $result->status)
            );
        }

        return true;
	}

	/**
	* Funcion que genera el xml para enviar al rest server via POST
	*
	* @param string $recipient numero de teléfono de quien recibe el mensaje
	* @param string $message mensaje a ser enviado
	* @param string $country codigo internacional del pais
    *
	* @return string $xml cadena con el xml que ese envia a l REST server POST
	*/
	private function create_xml_rest_post($recipient, $message, $country = '57')
	{
        return json_encode([
            'country' => $country,
            'message' => $message,
            'addresseeList' => [
                [
                    'mobile' => $recipient,
                ],
            ],
        ]);
	}

	/**
	* Función que hace la petición al servidor REST
	*
	* @param string $url cadena con la url del servicio
	* @param string $method el metodo sobre el cual se va hacer la petición
	* @param mixed $params contiene ya sea un array con el xml o un string con el xml a ser enviado
	* @return string La respuesta del servicio
	*/
	private function send_data($url, $method = 'GET', $params = [])
	{
		/* Abro la conección a la URL de Aldeano */
		$ch = curl_init( $url );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $method );

		/*Valido si se van a enviar parametros y si el metodo es post */
		if( !empty( $params ) && $method == "POST" ){
			$params = ( is_array( $params ) )? http_build_query( $params ) : $params;
			curl_setopt( $ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Basic ' . base64_encode("{$this->username}:{$this->password}"),
            ]);
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
		}

		/* obtengo el resultado de la peticion */
		$response = curl_exec($ch);

		/* cierro la conexón */
		curl_close($ch);

		if(!$response) {
		    return false;
		} else {
			return $response;
		}
	}

    /**
     * @param $code
     * @return string
     */
    private function getErrorMessage($code)
    {
        $errorMessages = [
            '-1' => 'Error de autenticación',
            '-2' => 'Ruta no configurada',
            '-3' => 'Número de celular invalido',
            '-4' => 'Crédito insuficiente',
            '-5' => 'Error interno de la transacción',
            '-6' => 'URL a acortar inválida',
            '-7' => 'Fecha de envío inválida',
            '-8' => 'Petición con formato inválida',
            '-9' => 'Usuario bloqueado por intentos fallidos',
            '-10' => 'Código de país inválido',
            '-11' => 'Mensaje del cuerpo inválido',
            '-12' => 'ID de transacción inválida'
        ];

        return empty($errorMessages[$code])
            ? 'Error while trying to send the message'
            : $errorMessages[$code];
    }
}
