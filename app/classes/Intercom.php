<?php
/**
 * Clase para integracion intercom
 */

use Intercom\IntercomClient;


class Intercom extends Eloquent
{
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        try{
            $this->client = new IntercomClient(Config::get('app.intercom.token'), null);
        }catch(Exception $e){
            return false;
        }
    }

    /**
     * Creacion de usuario
     *
     * @param array $user Datos del usuario
     * @return array $response objeto conexion
     */
    public function user_data_save($user)
    {
        //return false;

        if (!$user)
            return false;

        $data['user_id'] = $user->id;
        $data['email'] = $user->email;
        $data['name'] = $user->first_name.' '.$user->last_name;
        $data['phone'] = $user->phone;
        $data['custom_attributes'] = $user->getTraitsAttribute();

        try{
            $response = $this->client->users->create($data);
        }catch(Exception $e){
            $response = $e->getMessage();
        }

        return $response;
    }

    /**
     * Creacion de usuario
     *
     * @param array $user Datos del usuario
     * @return array $response objeto conexion
     */
    public function user_data_save_complete($user)
    {
        //return false;

        if (!$user)
            return false;

        $data['user_id'] = $user->id;
        $data['email'] = $user->email;
        $data['name'] = $user->first_name.' '.$user->last_name;
        $data['phone'] = $user->phone;
        $data['First name'] = $user->first_name;
        $data['Last name'] = $user->last_name;
        $data['Phone'] = $user->phone;

        $cityobj = UserAddress::select('user_address.*', 'cities.city as city')
                            ->join('cities', 'cities.id', '=','city_id')
                            ->where('user_address.user_id', $user->id)
                            ->orderby('id', 'DESC')
                            ->first();

        $data['City'] = $cityobj->city;
        $data['Signup date'] = $user->created_at;

        $ordercount = Order::select(DB::raw('COUNT(orders.id) as count'))
                            ->where('orders.user_id', $user->id)
                            ->first();
        $data['Completed Purchase'] = $ordercount->count;

        try{
            $response = $this->client->users->create($data);
        }catch(Exception $e){
            return false;
        }
        return $response;
    }

    /**
     * Registrar de evento
     *
     * @param array $event nombre del evento
     * @param array $user Datos del usuario
     * @return array $response objeto conexion
     */
    public function new_event($event, $user)
    {
        //return false;

        if (!$user)
            return false;

        $data['event_name'] = $event;
        $data['created_at'] = time();
        $data['email'] = $user->email;

        try{
          $response = $this->client->events->create($data);
        }catch(Exception $e){
            return false;
        }

        return $response;
    }

    /**
     * Registrar evento con metadata
     *
     * @param array $event String name del evento
     * @param array $user Datos del usuario
     * @param array $product Datos del producto
     * @return array $response objeto conexion
     */
    public function new_event_meta($event, $user, $product)
    {
        //return false;

        if (!$user)
            return false;

        $metadata = array(
          'order_date' => time(),
          'product' => $product->name,
          'store_id' => $product->store_id,
          'comment_product' =>$product->comment
        );

        $data['event_name'] = $event;
        $data['created_at'] = time();
        $data['email'] = $user->email;
        $data['metadata'] = $metadata;

        try{
          $response = $this->client->events->create($data);
        }catch(Exception $e){
            return false;
        }

        return $response;
    }


    /**
     * Listado de eventos por email
     *
     * @param array $email Email de consulta
     * @return array $response objeto conexion
     */
    public function search_event_email($email)
    {
        //return false;

        try{
          $response = $this->client->events->getEvents(['email' => $email]);
        }catch(Exception $e){
            return false;
        }

        return $response;
    }

    /**
     * Creacion de eventos con metadata de usuario no logueado
     *
     * @param array $event String name del evento
     * @param array $user Datos del usuario
     * @param array $product Datos del producto
     * @return array $response objeto conexion
     */
    public function new_event_meta_nologin($event, $user = false, $regid, $product)
    {
        //return false;

        $metadata = array(
          'order_date' => time(),
          'product' => $product->name,
          'store_id' => $product->store_id,
          'comment_product' =>$product->comment
        );

        $data['event_name'] = $event;
        $data['created_at'] = time();
        if($user){
            $data['email'] = $user->email;
            $data['user_id'] = $regid;
        }else $data['user_id'] = $regid;

        $data['metadata'] = $metadata;

        try{
          $response = $this->client->events->create($data);
        }catch(Exception $e){
            return false;
        }

        return $response;
    }


    /**
     * Creacion de usuarios
     *
     * @param array $user Datos del usuario
     * @return array $response objeto conexion
     */
    public function nologin_user_data_save($user = false, $regid)
    {
        //return false;

        if ($user){
            $data['user_id'] = $regid;
            $data['email'] = $user->email;
            $data['name'] = $user->first_name.' '.$user->last_name;
            $data['phone'] = $user->phone;
        }else $data['user_id'] = $regid;

        try{
          $response = $this->client->users->create($data);
        }catch(Exception $e){
            return false;
        }

        return $response;
    }

    /**
     * Registrar evento con metadata
     *
     * @param array $user Datos del usuario
     * @return array $response objeto conexion
     */
    public function new_event_order($user)
    {
        //return false;

        if (!$user)
            return false;

        $orders = Order::where('orders.user_id','=', $user->id)
                       ->where('orders.status', '=', 'Delivered')
                       ->select('orders.date', 'orders.id', 'orders.payment_method', 'orders.total_products', 'group_id')
                       ->get();
        try{

            foreach ($orders as $order)
            {
                $order_group = OrderGroup::find($order->group_id);

                $metadata = [
                    'store' => $order_group->storeNames,
                    'orderId' => $order->id,
                    'total' => $order_group->total_amount,
                    'discount' => $order_group->discount_amount,
                    'specialPrice' => $order_group->specialPrice,
                    'delivery' => $order_group->delivery_amount,
                    'paymentMethod' => $order->payment_method,
                    'orderCount' => $order_group->orderCount,
                    'itemCount' => $order->total_products
                ];

                $data['metadata'] = $metadata;
                $data['event_name'] = 'Order Delivered New';
                $data['user_id'] = $user->id;
                $data['created_at'] = time();

                sleep(1);

                $this->client->events->create($data);
            }

            $response = true;

        }catch(Exception $e){
            $response = $e->getMessage();
        }

        return count($orders);
    }
}