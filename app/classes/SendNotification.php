<?php


use contracts\SendNotificationInterface;
use contracts\SmsService;
use Merqueo\Backend\MerqueoNotification;

class SendNotification implements SendNotificationInterface
{

    public function sms($message, $to)
    {
        if (Config::get('app.debug')) {
            return true;
        }

        try {
            $to = starts_with($to, '+') ? $to : "+57{$to}";
            \App::make(SmsService::class)->send($to, $message);
        } catch (\Exception $e) {
            \Log::error('Could not send SMS notification. Service response: ' . $e->getMessage());
        }
        return false;
    }

    public function push($orderGroup, $data)
    {
        return send_notification($orderGroup->app_version, $orderGroup->source_os, [$orderGroup->user_device_id], $orderGroup->device_player_id, $data);
    }

    /**
     * @param $mail
     * @param bool $attachments
     * @param bool $throw_exception
     * @param null $from_email
     * @return bool|void
     * @throws Exception
     */
    public function mail($mail, $attachments = false, $throw_exception = false, $from_email = null){
        return send_mail($mail, $attachments, $throw_exception, $from_email);
    }
}