<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 13/09/2018
 * Time: 11:50
 */

class Volumetry
{
    private $url;

    private $dry_box = [];
    private $cold_big_box = [];
    private $cold_small_box = [];
    private $batteries = [];
    private $adv = 0;

    /**
     * Volumetry constructor.
     */
    public function __construct(){
        //$this->url = Config::get('app.services_transport.v1.url').'';
        $this->_setConfig();
    }

    /**
     * Funcion publica para calcular y actualizar la cantidad de contenedores de un pedido
     * @param $route_id
     */
    public function getContainers($route_id){
        $orders = Order::select('id')->where('route_id',$route_id)->get();
        if(count($orders)){
            foreach($orders as $order){
                $products = $this->getProducts($order->id);
                if(count($products)){
                    $containers = $this->_getContainers($products);
                    $this->updateOrder($order->id, $containers);
                }
            }
        }
    }

    /**
     * Actualiza las ordenes con el listado de contenedores
     * @param $order_id
     * @param $containers
     */
    private function updateOrder($order_id, $containers){
        try{
            DB::beginTransaction();
            $order = Order::find($order_id);
            $order->expected_containers = json_encode($containers);
            $order->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    /**
     * Obtiene la lista de productos de un pedido
     * @param $order_id
     * @return array
     */
    private function getProducts($order_id){
        $products = OrderProduct::select('order_products.quantity','products.height','products.width','products.length','products.weight',
            DB::raw('((products.height/100)* (products.width/100)*(products.length/100)*order_products.quantity) as volume'),
            DB::raw( "IF(store_products.storage = 'Seco','Seco','Frio') as storage"), 'products.can_mix', 'departments.name as department')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->where('order_products.order_id', $order_id)
            ->whereIn('order_products.type', ['Product','Muestra'])->get()->toArray();

        $products_group = OrderProductGroup::select('order_products.quantity','products.height','products.width', 'products.length','products.weight',
            DB::raw('((products.height/100)* (products.width/100)*(products.length/100)*order_product_group.quantity) as volume'),
            DB::raw( "IF(store_products.storage = 'Seco','Seco','Frio') as storage"), 'products.can_mix', 'departments.name as department')
            ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
            ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->where('order_products.order_id', $order_id)
            ->whereIn('order_products.type', ['Agrupado'])->get()->toArray();

        foreach ($products_group as $product){
            $products[] = $product;
        }
        return $products;
    }

    /**
     * Obtiene el listado de contenedores para unalista de productos
     * @param $products
     * @return array
     */
    private function _getContainers($products){
        $vol_prod_toilet = $vol_prod_dry = $vol_prod_cold = $vol_prod_pet = 0;
        $containers = ['dry'=>0, 'big_cold'=>0, 'small_cold'=>0, 'pets'=>0, 'toilet' => 0];
        foreach($products as $product){
            if($product['can_mix'] == 'Toilet'){
                $vol_prod_toilet += $product['volume'];
            }else if($product['can_mix'] == 'Pets') {
                $vol_prod_pet += $product['volume'];
            }else{
                if($product['storage'] == 'Seco'){
                    $vol_prod_dry += $product['volume'];
                }else{
                    $vol_prod_cold += $product['volume'];
                }
            }
        }
        if($vol_prod_dry > 0){
            $containers['dry'] = $vol_prod_dry/$this->dry_box['volume'];
        }
        if($vol_prod_cold > 0){
            if($vol_prod_cold > $this->cold_small_box['volume']){
                $containers['big_cold'] = $vol_prod_cold/$this->cold_big_box['volume'];
                $containers['dry'] += ($containers['big_cold']*$this->cold_big_box['volume'])/$this->dry_box['volume'];
            }else{
                $containers['small_cold'] = $vol_prod_cold/$this->cold_small_box['volume'];
                $containers['dry'] += ($containers['big_cold']*$this->cold_small_box['volume'])/$this->dry_box['volume'];
            }
        }
        if($vol_prod_pet > 0){
            $containers['pets'] = $vol_prod_pet/$this->dry_box['volume'];
        }
        if($vol_prod_toilet > 0){
            $containers['toilet'] = $vol_prod_toilet/$this->dry_box['volume'];
        }
        $containers['dry'] = ceil($containers['dry'] * $this->adv );
        $containers['toilet'] = ceil($containers['toilet'] * $this->adv);
        $containers['pets'] = ceil($containers['pets'] * $this->adv);
        $containers['small_cold'] = ceil($containers['small_cold'] * $this->adv);
        $containers['big_cold'] = ceil($containers['big_cold'] * $this->adv);
        $containers['total'] = $containers['dry'] + $containers['toilet'] + $containers['pets'] ;
        return $containers;
    }

    /**
     * Actualiza los valores de las variables globales
     */
    private function _setConfig(){
        $conf = $this->_getConfig();
        $this->dry_box=[
            'height'=>$conf['big_container_height']/100, //alto
            'width'=>$conf['big_container_width']/100, //ancho
            'length'=>$conf['big_container_length']/100, //largo
            'volume'=>($conf['big_container_height']/100)*($conf['big_container_width']/100)*($conf['big_container_length']/100) //volumen en m3
        ];
        $this->cold_big_box=[
            'height'=>$conf['big_container_cold_height']/100,
            'width'=>$conf['big_container_cold_width']/100,
            'length'=>$conf['big_container_cold_length']/100,
            'volume'=>($conf['big_container_cold_height']/100)*($conf['big_container_cold_width']/100)*($conf['big_container_cold_length']/100)
        ];
        $this->cold_small_box=[
            'height'=>$conf['small_container_cold_height']/100,
            'width'=>$conf['small_container_cold_width']/100,
            'length'=>$conf['small_container_cold_length']/100,
            'volume'=>($conf['small_container_cold_height']/100)*($conf['small_container_cold_width']/100)*($conf['small_container_cold_length']/100)
        ];
        $this->batteries=[
            'height'=>$conf['batteries_containers_height']/1000,
            'width'=>$conf['batteries_containers_width']/1000,
            'length'=>$conf['batteries_containers_length']/1000,
            'volume'=>($conf['batteries_containers_height']/1000)*($conf['batteries_containers_width']/1000)*($conf['batteries_containers_length']/1000)
        ];
        $this->adv = 1+$conf['margin_error_volume_container']/100;
    }

    /**
     * obtiene los datos de configuracion desde la base de datos
     * @return array
     */
    private function _getConfig(){
        $config = \Configuration::select('key','value')->where('type', 'transport')->get();
        $data_conf = [];
        foreach ($config as $conf){
            $data_conf[$conf->key] = $conf->value;
        }
        return $data_conf;
    }

    public static function getTotalRouteContainers($route_id){
        $orders = Order::select('expected_containers')->where('route_id',$route_id)->get();
        $total_containers = 0;
        if(count($orders)){
            foreach($orders as $order){
                if(!empty($order->expected_containers) ){
                    $containers = json_decode($order->expected_containers);
                    $total_containers += $containers->total;
                }
            }
        }
        return $total_containers;
    }
}