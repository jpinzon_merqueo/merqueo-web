<?php

/**
 * Class Soat
 */
class Soat
{

    private $url = null;
    private $client = null;

    public function __construct()
    {        
        $this->url = Config::get('app.soat.url');
        $auth = Config::get('app.soat.auth');
        $this->client = new GuzzleHttp\Client(['headers' => ['Authorization' => $auth ]]);
    }

    public function check($data)
    {
        try {
            $uri = $this->url . '/soat/' . $data['document'] . '/' . $data['plate'] . '?documentType=' . $data['type'] . '&email='. $data['email'];
            $response = $this->client->request('GET', $uri, [
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]);
            
            if ($response && $response->getStatusCode() == 200){
                $this->setLog($uri, $data, $response);
                return json_decode((string)$response->getBody());
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->setLog($uri, $data, $e->getResponse());
            if($e->getResponse()->getStatusCode() == 400)
               return json_decode((string)$e->getResponse()->getBody());
        }

    }

    public function sendSoat($data,$test=false)
    {
        // limpiar datos que no son necesarios para mimotor
        unset($data['credit_card_id']);
        unset($data['cc_installments']);
        unset($data['user_id']);
        unset($data['user_city_id']);
        unset($data['buyer']);
        
        $data['test'] = $test;

        $data_json = json_encode($data);
        try {
            $uri = $this->url . '/soat/merqueo' ;
            $response = $this->client->request('POST', $uri, [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body'=> $data_json
            ]);

            if ($response && $response->getStatusCode() == 200){
                $this->setLog($uri, $data, $response);
                return json_decode((string)$response->getBody());
            }


        } catch (\GuzzleHttp\Exception\RequestException $exception) {
            $this->setLog($uri, $data, $exception->getResponse());
            if($exception->getResponse()->getStatusCode() == 400 || $exception->getResponse()->getStatusCode() == 500) {
                return json_decode((string)$exception->getResponse()->getBody());
            }
        }
    }

    public function getStates($department_id='')
    {
        try {
            $uri = $this->url . '/co/states/'.$department_id ;
            $response = $this->client->request('GET', $uri, [
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]);

            if ($response && $response->getStatusCode() == 200){
                $this->setLog($uri, ['department_id'=>$department_id], $response);
                return json_decode((string)$response->getBody());
            }


        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->setLog($uri, ['department_id'=>$department_id], $e->getResponse());
            if($e->getResponse()->getStatusCode() == 400)
               return json_decode((string)$e->getResponse()->getBody());
        }
    }

    public function setLog($url,$params,$response)
    {
        $res = $response;
        $external_service_api_log = new \ExternalServiceApiLog();
        //$external_service_api_log->user_id = $data['user_id'];
        $external_service_api_log->url = $url;
        $external_service_api_log->params = json_encode($params);
        $external_service_api_log->response = json_encode((string)$response->getBody());
        $external_service_api_log->status = $response->getStatusCode();

        $external_service_api_log->save();

    }

    public function sendEmail($data)
    {

        $vars = [
            "taker" => $data["taker"],
            "document" => $data["document"],
            "plate" => $data["plate"],
            "runt" => $data["runt"]
        ];


        $mail = [
            'template_name' => 'emails.soat_pay',
            'subject' => 'Compra exitosa de SOAT ',
            'to' => [
                ['email' => $data["email"], 'name' => $data["taker"]["firstName"]." ".$data["taker"]["lastName"] ],
                ['email' => $data["buyer"]["email"], 'name' => $data["buyer"]["name"]  ]
            ],
            'vars' => $vars
        ];
        
        return send_mail($mail);
        
    }


}