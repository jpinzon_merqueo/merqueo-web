<?php
set_time_limit(1200);
/**
* Clase para envio de notificaiones
*/
class OneSignal extends Eloquent
{
	private $url = null;
	private $app_id = null;
    private $template_id = null;
    private $api_key = null;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->url = 'https://onesignal.com/api/v1';
		$this->app_id = Config::get('app.onesignal.app_id');
        $this->api_key = Config::get('app.onesignal.api_key');
        $this->template_id = Config::get('app.onesignal.template_id');
	}

	/**
	 * Envia un mensaje
     *
	 * @param  Array $data Datos a enviar
	 * @return Json array Retorna json array con id y número de envio de mensajes con éxito.
	 */
	public function sendMessage($data)
	{
		$fields = array(
            'app_id' => $this->app_id,
            'template_id' => $this->template_id,
            'data'=> $data['data'],
            'contents' => [
                'es' => $data['message'],
                'en' => $data['message']
            ]
        );

        if (isset($data['subtitle'])){
            $fields['headings'] = [
                'es' => $data['subtitle'],
                'en' => $data['subtitle']
            ];
            $fields['subtitle'] = [
                'es' => $data['subtitle'],
                'en' => $data['subtitle']
            ];
        }

        if(isset($data['send_after'])){
            $fields['send_after'] = $data['send_after'];
            unset($data['send_after']);
        }

        if(isset($data['segment'])){
        	$fields['included_segments'] = $data['segment'];
            unset($data['segment']);
        }

        if(isset($data['include_player_ids'])){
        	$fields['include_player_ids'] = $data['include_player_ids'];
            unset($data['include_player_ids']);
        }

        //debug($fields);

		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url.'/notifications');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic '.$this->api_key));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		$response = json_decode($response);
		if(isset($response->recipients))
			$response = ['status' => true, 'recipients' => $response->recipients];
		else{
			if(isset($response->errors))
				$response = ['status' => false, 'message' => 'Se presentó el siguiente error: '.$response->errors[0]];
			else
				$response = ['status'=> false, 'message' => 'Se presentó error de configuración.'];
		}
		curl_close($ch);

		return $response;
	}

    /**
     * Actualiza tags de usuario
     *
     * @param  Array $data Datos a enviar
     * @return Json array Retorna json array con estado
     */
    public function updateTags($data)
    {
        $fields = array(
            'app_id' => $this->app_id,
            'tags' => $data['tags']
        );

        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url.'/players/'.$data['player_id']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic MTNhOTM0MjMtMTM1NC00ODM4LWIzNzEtNzBmNjc1MTE2MWRj'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        $response = json_decode($response);
        if(isset($response->success) && $response->success)
            $response = ['status' => true, 'message' => 'Datos actualizados.'];
        else{
            if(isset($response->errors))
                $response = ['status' => false, 'message' => 'Se presentó el siguiente error: '.$response->errors[0]];
            else
                $response = ['status'=> false, 'message' => 'Se presentó error de configuración.'];
        }
        curl_close($ch);

        return $response;
    }

    /**
     * Obtiene push enviados
     *
     * @return Json array Retorna json array con estado
     */
    public function get_report($offset = 0)
    { 
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://onesignal.com/api/v1/notifications?app_id=2e27041d-ff5a-42d5-b00f-2306d77bcf28&limit=50&offset=" . $offset,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array('Content-Type: application/json; charset=utf-8',
                                    'Authorization: Basic MTNhOTM0MjMtMTM1NC00ODM4LWIzNzEtNzBmNjc1MTE2MWRj',
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $response;
    }

}