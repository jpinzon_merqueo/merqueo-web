<?php

use Elasticsearch\ClientBuilder;

class AWSElasticsearch
{
    /**
     * The name of the AWS Elasticsearch index.
     * @var string
     */
    protected $index = 'catalog';

    /**
     * The name of the AWS Elasticsearch type.
     * @var string
     */
    protected $type = 'product';

    /**
     * AWS Elasticsearch cliente.
     * @var string
     */
    protected $client;

    /**
     * @var bool
     */
    private $applyFivePercentDiscount = false;

    /**
     * Initialize controller with Elasticsearch client.
     */
    public function __construct()
    {
        //$singleHandler  = ClientBuilder::singleHandler();
        $this->client = ClientBuilder::create()
            ->setHosts([Config::get('app.aws.elasticsearch.host')])
            ->setRetries(2)
            ->build();
    }

    /**
     * Buscar productos en una tienda determinada y todos los demás
     *
     * @param int $warehouseId ID de la bodega
     * @param string $query Texto a buscar
     * @param int $page Numero de la pagina a obtener
     * @param int $rows Numero de registros para la paginacion
     * @param array $exclude Textos a omitir en query
     * @return array
     */
    public function search($warehouseId, $query, $page = 1, $rows = 8, $exclude = [])
    {
        $data = [];
        $warehouseId = intval($warehouseId);
        if ($warehouseId == 0) {
            return [
                'status' => false,
                'message' => "El parámetro de bodega debe ser un número entero.",
                'result' => null
            ];
        }

        // Get $cityId
        $store = $this->client->search([
            'index' => $this->index,
            'type' => $this->type,
            'size' => 1,
            'body' => [
                'query' => [
                    'match' => [
                        'warehouse_id' => $warehouseId
                    ]
                ]
            ]
        ]);

        if (empty($store['hits']['hits'])) {
            return [
                'status' => false,
                'message' => "La bodega $warehouseId no existe",
                'result' => null
            ];
        }

        $search = $this->performSearch($warehouseId, $query, $page, $rows, $exclude);
        $data[$warehouseId] = $search['result'];

        return [
            'status' => true,
            'message' => 'Búsqueda realizada',
            'result' => $data,
            'query' => $search['query']
        ];
    }

    /**
     * Realizar busqueda en el index de AWS Elasticsearch
     *
     * @param string $warehouseId ID de la bodega
     * @param string $query Query a buscar
     * @param int $page Numero de pagina a obtener
     * @param int $rows Numero para la paginacion
     * @param array $exclude Textos a omitir en query
     * @return array
     */
    private function performSearch($warehouseId, $query, $page, $rows, $exclude = [])
    {
        $original = $query;
        $hits = [];

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'size' => $rows,
            'from' => ($page - 1) * $rows,
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'multi_match' => [
                                'query' => $query,
                                'operator' => 'and',
                                'type' => 'most_fields',
                                'fields' =>['name^8', 'name.folded^4', 'name.stemmed^4', 'name.stemmed_folded^4', 'name.autocomplete^4', 'brand^6', 'brand.folded^3', 'brand.stemmed^3', 'brand.stemmed_folded^3', 'brand.autocomplete^3', 'shelf^4', 'shelf.folded^2', 'shelf.stemmed^2', 'shelf.stemmed_folded^2', 'shelf.autocomplete^2', 'department^5', 'department.folded', 'department.autocomplete'],
                            ],
                        ],
                        'should' => [
                            'multi_match' => [
                                'query' => $query,
                                'type' => 'phrase_prefix',
                                'fields' => ['name.keyword^2', 'brand.keyword', 'shelf.keyword', 'department.keyword'],
                                'boost' => 2
                            ]
                        ],
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    ['term' => ['warehouse_id' => $warehouseId]],
                                    ['term' => ['department_status' => 1]],
                                    ['term' => ['shelf_status' => 1]],
                                    ['term' => ['status' => 1]],
                                    ['term' => ['is_visible' => 1]]
                                ]
                            ]
                        ],
                        'must_not' => [
                            'bool' => [
                                'should' => [
                                    ['term'=> ['department_status' => 0]],
                                    ['term'=> ['shelf_status' => 0]],
                                    ['term'=> ['status' => 0]],
                                    [
                                        'bool' => [
                                            'must' => [
                                                [
                                                    'term' => [
                                                        'manage_stock' => 1
                                                    ]
                                                ],
                                                [
                                                    'term' => [
                                                        'is_visible_stock' => 0
                                                    ]
                                                ],
                                                [
                                                    'range' => [
                                                        'current_stock' => [
                                                            'lte' => 0
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ],
                'suggest' => [
                    'text' => $query,
                    'name_suggestion' => [
                        'phrase' => [
                            'size' => 1,
                            'field' => 'name',
                            'direct_generator' => [
                                [
                                    'field' => 'name',
                                    'suggest_mode' => 'popular',
                                    'min_word_length' => 3,
                                    'prefix_length' => 2,
                                ],
                            ],
                        ],
                    ],
                    'brand_suggestion' => [
                        'phrase' => [
                            'size' => 1,
                            'field' => 'brand',
                            'direct_generator' => [
                                [
                                    'field' => 'brand',
                                    'suggest_mode' => 'popular',
                                    'min_word_length' => 3,
                                    'prefix_length' => 2,
                                ],
                            ],
                        ],
                    ],
                    'shelf_suggestion' => [
                        'phrase' => [
                            'size' => 1,
                            'field' => 'shelf',
                            'direct_generator' => [
                                [
                                    'field' => 'shelf',
                                    'suggest_mode' => 'popular',
                                    'min_word_length' => 3,
                                    'prefix_length' => 2,
                                ],
                            ],
                        ],
                    ]
                ],
            ],
        ];

        if (!empty($exclude['store_product_ids'])) {
            $params['body']['query']['bool']['filter']['bool']['must_not'] = [
                'terms' => [
                    'store_product_id' => $exclude['store_product_ids'],
                ]
            ];
        }

	if (!empty($exclude['wildcards'])) {
            foreach($exclude['wildcards'] as $string){
                $params['body']['query']['bool']['must_not']['bool']['should'][] = ['wildcard' => ['name' => '*'.$string.'*']];
            }
        }

        $response = $this->client->search($params);
        $hits = $this->extractProducts($response);

        if (empty($hits)) {
            $suggestion = $this->extractSuggestions($response);

            if ($suggestion) {
                $query = $suggestion;
                $params['body']['query']['bool']['must']['multi_match']['query'] = $query;
                $response = $this->client->search($params);
                $hits = $this->extractProducts($response);
            }
        }

        return [
            'status' => true,
            'message' => 'Búsqueda realizada',
            'result' => $hits,
            'query' => $query,
            'original' => $original,
            'meta' => [
                'page' => $page,
                'size' => $rows,
                'total-pages' => ceil($response['hits']['total']/$rows),
            ]
        ];
    }

    /**
     * Obtener informacion de productos para enviar al indice
     *
     * @param array $args Parametros para actualizar producto
     * @return array
     */
    public function update_products($args)
    {
        $store_products = StoreProduct::select(
            'store_product_warehouses.id AS id',
            'cities.id AS city_id',
            'cities.slug AS city_slug',
            'stores.slug AS store_slug',
            'stores.id AS store_id',
            'departments.id AS department_id',
            'departments.name AS department_name',
            'departments.slug AS department_slug',
            'departments.status AS department_status',
            'shelves.id AS shelf_id',
            'shelves.name AS shelf_name',
            'shelves.slug AS shelf_slug',
            'shelves.has_warning AS shelf_warning',
            'shelves.status AS shelf_status',
            'products.id AS product_id',
            'products.description AS product_description',
            'products.image_app_url AS product_image_app_url',
            'products.image_large_url AS product_image_large_url',
            'products.image_medium_url AS product_image_medium_url',
            'products.image_small_url AS product_image_small_url',
            'products.name AS product_name',
            'products.type AS product_type',
            'products.slug AS product_slug',
            'products.quantity AS product_quantity',
            'products.unit AS product_unity',
            'products.unit_pum AS unit_pum',
            'products.net_quantity_pum AS net_quantity_pum',
            'products.nutrition_facts AS product_nutrition_facts',
            'brands.name AS brand_name',
            DB::raw('IF(store_products.special_price IS NOT NULL AND store_products.public_price > 0, store_products.public_price, store_products.price) AS store_product_price'),
            DB::raw('IF(store_products.allied_store_id IS NULL, 0, '.Config::get('app.marketplace_delivery_days').') AS marketplace_delivery_days'),
            'store_products.id AS store_product_id',
            'store_products.public_price',
            'store_products.special_price AS store_product_special_price',
            'store_products.quantity_special_price AS store_product_quantity_special_price',
            'store_products.delivery_discount_amount AS store_product_special_delivery_discount_amount',
            'store_products.first_order_special_price AS store_product_first_order_special_price',
            'store_products.is_best_price AS store_product_is_best_price',
            'store_products.special_price_starting_date',
            'store_products.special_price_expiration_date',
            'store_products.delivery_discount_start_date',
            'store_product_warehouses.id AS store_product_warehouse_id',
            'store_product_warehouses.status',
            'store_product_warehouses.manage_stock',
            'store_product_warehouses.is_visible_stock',
            'store_product_warehouses.current_stock',
            'store_product_warehouses.warehouse_id',
            'store_product_warehouses.is_visible',
            'store_products.special_price AS pum_special_price'
        )
            ->join('products','products.id', '=', 'store_products.product_id')
            ->join('store_product_warehouses','store_product_warehouses.store_product_id', '=','store_products.id')
            ->join('stores','stores.id', '=', 'store_products.store_id')
            ->join('shelves','shelves.id', '=', 'store_products.shelf_id')
            ->join('departments','departments.id', '=', 'store_products.department_id')
            ->join('cities', 'cities.id', '=', 'stores.city_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->pum()
            ->groupBy('store_product_warehouses.id');

        if (array_key_exists('store_id', $args)) {
            $store_products = $store_products->where('store_products.store_id', $args['store_id'])->get();
        }

        if (array_key_exists('department_id', $args)) {
            $store_products = $store_products->where('store_products.department_id', $args['department_id'])->get();
        }

        if (array_key_exists('shelf_id', $args)) {
            $store_products = $store_products->where('store_products.shelf_id', $args['shelf_id'])->get();
        }

        if (array_key_exists('store_product_id', $args)) {
            $store_products = $store_products->where('store_products.id', $args['store_product_id'])->get();
        }

        if (array_key_exists('product_id', $args)) {
            $store_products = $store_products->where('store_products.product_id', $args['product_id'])->get();
        }

        if (array_key_exists('store_product_warehouse_id', $args)) {
            $store_products = $store_products->where('store_product_warehouses.id', $args['store_product_warehouse_id'])->get();
        }

        if (array_key_exists('all_products', $args)) {
            ini_set('memory_limit', '1024M');
            set_time_limit(0);
            $store_products = $store_products->get();
        }

        if (count($store_products)) {
            $store_products = $store_products->toArray();
            $this->update_documents($store_products);
        }
    }

    /**
     * Creacion o actualizacion de productos en el indice
     *
     * @param $store_products object Productos en tienda
     * @return array
     */
    public function update_documents($store_products)
    {
        $responses = [];
        foreach ($store_products as $i => $store_product) {
            $data = [
                'id' => $store_product['id'],
                'brand' => $store_product['brand_name'],
                'city_id' => $store_product['city_id'],
                'city_slug' => $store_product['city_slug'],
                'store_id' => $store_product['store_id'],
                'warehouse_id' => $store_product['warehouse_id'],
                'store_slug' => $store_product['store_slug'],
                'department' => $store_product['department_name'],
                'department_id' => $store_product['department_id'],
                'department_slug' => $store_product['department_slug'],
                'department_status' => $store_product['department_status'],
                'shelf' => $store_product['shelf_name'],
                'shelf_id' => $store_product['shelf_id'],
                'shelf_slug' => $store_product['shelf_slug'],
                'shelf_status' => $store_product['shelf_status'],
                'has_warning' => $store_product['shelf_warning'],
                'name' => $store_product['product_name'],
                'type' => $store_product['product_type'],
                'slug' => $store_product['product_slug'],
                'unit' => $store_product['product_unity'],
                'quantity' => $store_product['product_quantity'],
                'unit_pum' => $store_product['unit_pum'],
                'net_quantity_pum' => $store_product['net_quantity_pum'],
                'image_app_url' => $store_product['product_image_app_url'],
                'image_large_url' => $store_product['product_image_large_url'],
                'image_medium_url' => $store_product['product_image_medium_url'],
                'image_small_url' => $store_product['product_image_small_url'],
                'description' => $store_product['product_description'],
                'nutrition_facts' => $store_product['product_nutrition_facts'],
                'all_product_slug' => '/domicilios-'.$store_product['store_slug'].'/'.$store_product['department_slug'].'/'.$store_product['shelf_slug'].'/'.$store_product['product_slug'],
                'store_product_id' => $store_product['store_product_id'],
                'price' => $store_product['store_product_price'],
                'special_price' => $store_product['store_product_special_price'],
                'public_price' => $store_product['public_price'],
                'quantity_special_price' => $store_product['store_product_quantity_special_price'],
                'first_order_special_price' => $store_product['store_product_first_order_special_price'],
                'is_best_price' => $store_product['store_product_is_best_price'],
                'delivery_discount_amount' => $store_product['store_product_special_delivery_discount_amount'],
                'marketplace_delivery_days' => $store_product['marketplace_delivery_days'],
                'manage_stock' => $store_product['manage_stock'],
                'is_visible_stock' => $store_product['is_visible_stock'],
                'current_stock' => $store_product['current_stock'],
                'is_visible' => $store_product['is_visible'],
                'status' => $store_product['status'],
                'special_price_starting_date' => $store_product['special_price_starting_date'],
                'special_price_expiration_date' => $store_product['special_price_expiration_date'],
                'delivery_discount_start_date' => $store_product['delivery_discount_start_date'],
                'pum' => $store_product['pum'],
                'volume' => $store_product['volume'],
                'weight' => $store_product['weight'],
            ];

            $params['body'][] = [
                'index' => [
                    '_index' => $this->index,
                    '_type' => $this->type,
                    '_id' => $store_product['store_product_warehouse_id']
                ]
            ];

            $params['body'][] = $data;

            // every 1000 documents stop and send the bulk request
            if ($i % 1000 == 0) {
                $responses = $this->client->bulk($params);

                // erase the old bulk request
                $params = ['body' => []];

                // unset the bulk response when you are done to save memory
                unset($responses);
            }
        }

        // Send the last batch if it exists
        if (!empty($params['body'])) {
            $responses = $this->client->bulk($params);
        }

        //return $responses;
    }

    /**
     * Eliminar producto especifico del indice
     *
     * @param $store_product_warehouse_id
     * @return array
     */
    public function delete_document($store_product_warehouse_id)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $store_product_warehouse_id,
        ];

        return $this->client->delete($params);
    }

    /**
     * Crea nuevo indice
     *
     * @param $params
     * @return array
     */
    public function create_index($params)
    {
        return $this->client->indices()->create($params);
    }

    /**
     * Crea mapeo de indice
     *
     * @param $params
     * @return array
     */
    public function mapping_index($params)
    {
        return $this->client->indices()->putMapping($params);
    }

    /**
     * Elimina indice
     *
     * @return array
     */
    public function delete_index()
    {
        $params['index'] = $this->index;

        return $this->client->indices()->delete($params);
    }

    /**
     * Extraer productos de AWS Elasticsearch response
     *
     * @param array $response
     * @return array
     */
    private function extractProducts($response)
    {
        $products = [];

        foreach ($response['hits']['hits'] as $hit) {
            unset($hit['_source']['@version']);
            unset($hit['_source']['@timestamp']);
            unset($hit['_source']['city_id']);
            unset($hit['_source']['tags']);
            $products[] = $hit['_source'];
        }

        return $products;
    }

    /**
     * Extraer sugerencias de las respuestas de ElasticSearch response
     *
     * @param array $response
     * @return string/null
     */
    private function extractSuggestions($response)
    {
        $suggested = [];

        foreach ($response['suggest'] as $suggestion) {
            if (!empty($suggestion[0]['options'])) {
                $text = $suggestion[0]['options'][0]['text'];
                $score = $suggestion[0]['options'][0]['score'];

                if (empty($suggested[$text]) || $suggested[$text] < $score)
                    $suggested[$text] = $score;
            }
        }

        if (!empty($suggested)) {
            asort($suggested);
            return key(array_slice($suggested, -1, 1, true));
        } else return null;
    }

    /**
     * @param array $product
     * @param \Carbon\Carbon $current_date
     * @param $total_order
     * @param $last_order
     * @return array
     */
    public function clearProductResponse($product, \Carbon\Carbon $current_date, $total_order, $last_order)
    {
        $product['discount_percentage'] = null;
        $product['special_price_starting_date'] = empty($product['special_price_starting_date'])
            ? $current_date
            : new \Carbon\Carbon($product['special_price_starting_date']);
        $product['special_price_expiration_date'] = empty($product['special_price_expiration_date'])
            ? $current_date
            : new \Carbon\Carbon($product['special_price_expiration_date']);

        $valid_special_price_time = $current_date >= $product['special_price_starting_date'] && $current_date <= $product['special_price_expiration_date'];
        if (($total_order && !empty($product['first_order_special_price'])) || !$valid_special_price_time) {
            $product['special_price'] = null;
            $product['discount_percentage'] = null;
        }

        if (!empty($product['special_price'])) {
            $product['discount_percentage'] = round(($product['price'] - $product['special_price']) * 100 / $product['price'], 0);
        }else{
            $product['quantity_special_price'] = 0;
        }

        if (!empty($product['delivery_discount_start_date'])) {
            if (date('Y-m-d H:i:s', strtotime($product['delivery_discount_start_date'])) > date('Y-m-d H:i:s')) {
                $product['delivery_discount_amount'] = 0;
            }
        }

        if (date('Y-m-d H:i:s', strtotime($product['delivery_discount_start_date'])) > date('Y-m-d H:i:s')) {
            $product['delivery_discount_amount'] = 0;
        }

        $showPercentDiscount = empty($product['special_price']) || $product['discount_percentage'] < 5;

        if ($showPercentDiscount && $this->isApplyFivePercentDiscount() &&
            !Discount::shouldSkippedShelve($product['shelf_id'])) {
            $product['discount_percentage'] = 5;
            $product['quantity_special_price'] = 2;
            $product['special_price'] = round($product['price'] * 0.95);
        }

        //Obtener pum del producto
        if ($product['type'] == 'Simple') {
            if ($product['price'] && $product['net_quantity_pum'] != 0) {
                if (!empty($product['special_price'])) {
                    $product['pum'] = $product['unit_pum'] . ' a $' . number_format($product['special_price'] / $product['net_quantity_pum'], 2, ',', '.');
                }else{
                    $product['pum'] = $product['unit_pum'] . ' a $' . number_format($product['price'] / $product['net_quantity_pum'], 2, ',', '.');
                }
            }
        }

        $product['id'] = $product['store_product_id'];

        return $product;
    }

    /**
     * @param bool $applyFivePercentDiscount
     * @return AWSElasticsearch
     */
    public function setApplyFivePercentDiscount($applyFivePercentDiscount)
    {
        $this->applyFivePercentDiscount = $applyFivePercentDiscount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isApplyFivePercentDiscount()
    {
        return $this->applyFivePercentDiscount;
    }
}
