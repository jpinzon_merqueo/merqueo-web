<?php

use app\classes\entities\OrderPaymentLogEntity;
 /**
 * Clase para pagos con tarjetas de crédito
 */
class PayU
{
    private $url;
    private $api_key;
    private $api_login;
    private $account_id;
    private $merchant_id;
    private $test;
    public $orderPaymentLogEntity;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderPaymentLogEntity = new OrderPaymentLogEntity();
        $this->setConfig();
    }

    private function setConfig($preCharge = false)
    {
        $this->url = Config::get('app.payu.url');
        $this->url_reports = Config::get('app.payu.url_reports');
        $this->api_key = Config::get('app.payu.api_key');
        $this->api_login = Config::get('app.payu.api_login');
        if($preCharge){
            $this->account_id = Config::get('app.payu.precharge_account_id');
        }else{
            $this->account_id = Config::get('app.payu.account_id');
        }
        $this->merchant_id = Config::get('app.payu.merchant_id');
        $this->test = (bool) Config::get('app.payu.test');
    }

    /**
     * Asocia una tarjeta de crédito a un cliente
     *
     * @param array $data Datos de la tarjeta de crédito
     * @return array $response Respuesta del servicio
     */
    public function associateCreditCard($data)
    {
        try {
            if (($data['card_type'] == 'AMEX' && strlen($data['code_cc']) != 4) || ($data['card_type'] != 'AMEX' && strlen($data['code_cc']) != 3)) {
                $response = array(
                    'status' => 0,
                    'message' => 'El código de seguridad de la tarjeta no es valido.',
                );
                return $response;
            }
            $year = date('Y');
            $month = date('m');
            if ($year > $data['expiration_year_cc']) {
                $response = array(
                    'status' => 0,
                    'message' => 'La fecha de vencimiento de su tarjeta de crédito no es válida.',
                );
                return $response;
            } else {
                if ($year == $data['expiration_year_cc'] && $month > $data['expiration_month_cc']) {
                    $response = array(
                        'status' => 0,
                        'message' => 'La fecha de vencimiento de su tarjeta de crédito no es válida.',
                    );
                    return $response;
                }
            }

            //debug($data);
            $request_data = array(
                'language' => 'es',
                'command' => 'CREATE_TOKEN',
                'merchant' => array(
                    'apiLogin' => $this->api_login,
                    'apiKey' => $this->api_key
                ),
                'creditCardToken' => array(
                    'payerId' => $data['user_id'],
                    'name' => $data['name_cc'],
                    'identificationNumber' => $data['document_number_cc'],
                    'paymentMethod' => $data['card_type'],
                    'number' => $data['number_cc'],
                    'expirationDate' => $data['expiration_year_cc'] . '/' . $data['expiration_month_cc']
                )
            );

            $request = json_encode($request_data);
            $options = array(
                'http' => array(
                    'header' => array('Content-type: application/json; charset=UTF-8'),
                    'method' => 'POST',
                    'content' => $request,
                    'ignore_errors' => true
                ),
            );

            $context = stream_context_create($options);
            $response_xml = @file_get_contents($this->url, false, $context);

            $valid_xml = $this->isValidXml($response_xml);

            if ($valid_xml)
                $status = simplexml_load_string($response_xml);

            if ($response_xml === false) $response_xml = $http_response_header[0];

            $http_response_code = substr($http_response_header[0], 9, 3);
            $response = $this->parseJson($response_xml);

            $this->orderPaymentLogEntity->setUserId($data['user_id']);
            $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
            $this->orderPaymentLogEntity->setUrl($this->url);
            $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
            $this->orderPaymentLogEntity->setRequest(json_encode($request_data));
            $this->orderPaymentLogEntity->setResponse($response);
            $this->orderPaymentLogEntity->setIp(get_ip());
            $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
            $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
            $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

            if ($valid_xml && isset($status->code) && $status->code == 'ERROR') {
                $response = array(
                    'status' => 0,
                    'message' => 'Ocurrió un error al registrar los datos de tu tarjeta de crédito, por favor verificalos.',
                    'response' => json_decode($response)
                );
            } elseif ($valid_xml && isset($status->code) && $status->code == 'SUCCESS') {
                $response = array(
                    'status' => 1,
                    'message' => 'Tarjeta de crédito asociada con éxito',
                    'response' => json_decode($response)
                );
            } else {
                $response = array(
                    'status' => 0,
                    'message' => 'Ocurrió un error al registrar los datos de tu tarjeta de crédito.',
                    'response' => json_decode($response)
                );
            }
        } catch(Exception $exception) {
            ErrorLog::add($exception);

            $response = array(
                'status' => 0,
                'message' => 'Ocurrió un error al registrar los datos de tu tarjeta de crédito.',
            );
        }

        return $response;
    }

    /**
     * Registra cargo a la tarjeta de crédito
     *
     * @param array $data Datos de la tarjeta de crédito
     * @return array $response Respuesta del servicio
     */
    public function createCreditCardCharge($data, $preCharge = false)
    {
        $this->setConfig($preCharge);
        $data['referenceCode'] = $preCharge ? "pre_".$data['referenceCode'] : $data['referenceCode'];
        $request = array(
            'language' => 'es',
            'command' => 'SUBMIT_TRANSACTION',
            'merchant' => array(
                'apiKey' => $this->api_key,
                'apiLogin' => $this->api_login
            ),
            'transaction' => array(
                'order' => array(
                    'accountId' => $this->account_id,
                    'referenceCode' => $data['referenceCode'],
                    'description' => $data['description'],
                    'language' => 'es',
                    'signature' =>  $this->generateTransactionSignature($data['referenceCode'], $data['total_amount'], 'COP'),
                    'notifyUrl' => 'https://merqueo.com/payment/confirmation',
                    'additionalValues' => array(
                        'TX_VALUE' => array(
                            'value' => $data['total_amount'],
                            'currency' => 'COP'
                        ),
                        'TX_TAX' => array(
                            'value' => $data['iva'],
                            'currency' => 'COP'
                        ),
                        'TX_TAX_RETURN_BASE' => array(
                            'value' => $data['total_base'],
                            'currency' => 'COP'
                        )
                    ),
                    'buyer' => array(
                        'merchantBuyerId' => $data['user_id'],
                        'fullName' => $data['buyer']['fullname'],
                        'emailAddress' => $data['buyer']['email'],
                        'contactPhone' => $data['buyer']['phone'],
                        'dniNumber' => $data['buyer']['document_number'],
                        'shippingAddress' => array(
                            'street1' => $data['buyer']['address'],
                            'street2' => $data['buyer']['address_further'],
                            'city' => $data['buyer']['city'],
                            'state' => $data['buyer']['state'],
                            'country' => 'CO'
                        )
                    ),
                ),
                'payer' => array(
                    'merchantPayerId' => $data['user_id'],
                    'fullName' => $data['payer']['fullname'],
                    'emailAddress' => $data['payer']['email'],
                    'contactPhone' => $data['payer']['phone'],
                    'dniNumber' => $data['payer']['document_number'],
                    'dniType' => empty($data['payer']['document_type']) ? 'CC' : $data['payer']['document_type'] ,
                    'billingAddress' => array(
                        'street1' => $data['payer']['address'],
                        'street2' => $data['payer']['address_further'],
                        'city' => $data['payer']['city'],
                        'country' => 'CO',
                    )
                ),
                'creditCardTokenId' => $data['card_token'],
                'creditCard' => array(
                    'securityCode' => $data['security_code'],
                    'processWithoutCvv2' => true
                ),
                'extraParameters' => array(
                    'INSTALLMENTS_NUMBER' => $data['installments_cc'],
                    'ORDER_ID' => $data['order_id'],
                ),
                'type' => 'AUTHORIZATION_AND_CAPTURE',
                'paymentMethod' => $data['card_type'],
                'paymentCountry' => 'CO',
                'ipAddress' => '127.0.0.1',
                /*'deviceSessionId' => $data['device_session_id'],
                'cookie' => false,//$data['cookie'],
                'userAgent' => $_SERVER['HTTP_USER_AGENT']*/
            ),
            'test' => $this->test
        );

        $request = json_encode($request);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = @file_get_contents($this->url, false, $context);
        $valid_xml = $this->isValidXml($response_xml);

        if($valid_xml)
            $status = simplexml_load_string($response_xml);

        if ($response_xml === false) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);
        $this->orderPaymentLogEntity->setOrderId($data['order_id']);
        $this->orderPaymentLogEntity->setUserId($data['user_id']);
        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);

        if (!empty($status->transactionResponse->transactionId)) {
            $this->orderPaymentLogEntity->setTransactionId((array)$status->transactionResponse->transactionId);
        }

        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if ($valid_xml && isset($status->code) && $status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }elseif($valid_xml && isset($status->code) && $status->code == 'SUCCESS'
            && isset($status->transactionResponse) && $status->transactionResponse->state == 'APPROVED'){
            $response = array(
                'status' => 1,
                'message' => 'Cobro realizado con éxito.',
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }

        return $response;
    }

    /**
     * Obtiene informacion de tarjeta de crédito
     *
     * @param string $card_token Token de la tarjeta de crédito
     * @param string $user_id ID del usuario
     * @return array $response Respuesta del servicio
     */
    public function retriveCreditCard($card_token, $user_id = null)
    {
        $request_data = array(
           'language'  => 'es',
           'command' => 'GET_TOKENS',
           'merchant' => array(
              'apiLogin' => $this->api_login,
              'apiKey' => $this->api_key
           ),
           'creditCardTokenInformation' => array(
              'payerId' => $user_id,
              'creditCardTokenId' => $card_token,
              'startDate' => '2010-01-01T12:00:00',
              'endDate' => '2030-01-01T12:00:00'
           )
        );

        $request = json_encode($request_data);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = @file_get_contents($this->url, false, $context);
        $valid_xml = $this->isValidXml($response_xml);

        if($valid_xml)
            $status = simplexml_load_string($response_xml);

        if ($response_xml === false) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);
        $request = 'No aplica';

        //guardar log
        $this->orderPaymentLogEntity->setUserId($user_id);
        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if ($valid_xml && isset($status->code) && $status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }elseif($valid_xml && isset($status->code) && $status->code == 'SUCCESS'){
            $response = array(
                'status' => 1,
                'message' => 'Tarjeta de crédito obtenida con éxito.',
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }

        return $response;
    }

    /**
     * Elimina una tarjeta de crédito
     *
     * @param array $user_id ID del usuario
     * @param array $card_token Token de la tarjeta de crédito
     * @return array $response Respuesta del servicio
     */
    public function deleteCreditCard($user_id = null, $card_token = null)
    {
        $request_data = array(
           'language' => 'es',
           'command' => 'REMOVE_TOKEN',
           'merchant' => array(
              'apiLogin' => $this->api_login,
              'apiKey' => $this->api_key
           ),
           'removeCreditCardToken' => array(
              'payerId' => $user_id,
              'creditCardTokenId' => $card_token
           )
        );

        $request = json_encode($request_data);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = @file_get_contents($this->url, false, $context);
        $valid_xml = $this->isValidXml($response_xml);

        if($valid_xml)
            $status = simplexml_load_string($response_xml);

        if ($response_xml === false) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        $this->orderPaymentLogEntity->setUserId($user_id);
        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if ($valid_xml && isset($status->code) && $status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }elseif($valid_xml && isset($status->code) && $status->code == 'SUCCESS'){
            $response = array(
                'status' => 1,
                'message' => 'Tarjeta eliminada con éxito.',
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }

        return $response;
    }

    /**
     * Registra reembolso a la tarjeta de crédito
     *
     * @param $reason
     * @param object $order del pedido
     * @param null $order_payments
     * @return array $response Respuesta del servicio
     */
    public function refundCreditCardCharge($reason, $order = null, $order_payments = null, $preCharge = false)
    {
        $this->setConfig($preCharge);
        $request = array(
           'language' => 'es',
           'command' => 'SUBMIT_TRANSACTION',
           'merchant' => array(
              'apiKey' => $this->api_key,
              'apiLogin' => $this->api_login
           ),
           'transaction' => array(
              'order' => array(
                 'id' => $order_payments->cc_charge_id
              ),
              'type' => 'REFUND',
              'reason' => $reason,
              'parentTransactionId' => $order_payments->cc_payment_transaction_id
           ),
           'test' => $this->test
        );

        $request = json_encode($request);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = @file_get_contents($this->url, false, $context);
        $status = simplexml_load_string($response_xml);

        if ($response_xml === false) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        if (!empty($order->id)) {
            $this->orderPaymentLogEntity->setOrderId($order->id);
        }

        $this->orderPaymentLogEntity->setUserId(Session::get('admin_id'));
        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);

        if (!empty($status->transactionResponse->transactionId)) {
            $this->orderPaymentLogEntity->setTransactionId((array)$status->transactionResponse->transactionId);
        }

        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if ($status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 1,
                'message' => 'Reembolso realizado con éxito.',
                'response' => json_decode($response)
            );
        }

        return $response;
    }

    /**
     * Obtiene estado de reembolso a la tarjeta de crédito
     *
     * @param int $charge_id ID del pedido
     * @return array $response Respuesta del servicio
     */
    public function getRefundResponse($charge_id, $order)
    {
        $request = array(
           'test' => $this->test,
           'language' => 'es',
           'command' => 'ORDER_DETAIL',
           'merchant' => array(
              'apiLogin' => $this->api_login,
              'apiKey' => $this->api_key

           ),
           'details' => array(
              'orderId' => (int) $charge_id
           )
        );
        $request = json_encode($request);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = @file_get_contents($this->url_reports, false, $context);
        $status = simplexml_load_string($response_xml);

        if ($response_xml === false) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if ($status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 1,
                'message' => 'Respuesta de reembolso obtenida con éxito.',
                'response' => json_decode($response)
            );
        }

        return $response;
    }

    /**
     * Obtiene el listado de los bancos para pago con PSE
     *
     * @return array $response Respuesta del servicio
     */
    public function getPseBanksList()
    {
        $request = array(
            "language" => "es",
            "command" => "GET_BANKS_LIST",
            "merchant" => array(
                "apiLogin" => $this->api_login,
                "apiKey" => $this->api_key
            ),
            "test" => $this->test,
            "bankListInformation" => array(
                "paymentMethod" => "PSE",
                "paymentCountry" => "CO"
            )

        );

        $request = json_encode($request);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = file_get_contents($this->url, false, $context);
        $status = simplexml_load_string($response_xml);
        if (!$response_xml) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        $response = json_decode($response, true);

        return array_get($response, 'banks.bank', []);
    }

    /**
     * Obtiene la url del banco para poder realizar el pago a traves de PSE
     *
     * @return array $response Respuesta del servicio
     */
    public function pseCharge($data)
    {
        $reference = $data['order_reference'];
        $request = array(
           'test' => $this->test,
           'language' => 'es',
           'command' => 'SUBMIT_TRANSACTION',
           'merchant' => array(
              'apiLogin' => $this->api_login,
              'apiKey' => $this->api_key

           ),
           "transaction" => array(
                "order" => array(
                    "accountId" => $this->account_id,
                    "referenceCode" => $reference,
                    "description" => $data['description'],
                    "language" => "es",
                    "signature" => $this->generateTransactionSignature($reference, $data['total_amount'], 'COP'),
                    "notifyUrl" => route("PayU.notification"),
                    "additionalValues" => array(
                        "TX_VALUE" => array(
                            "value" => $data['total_amount'],
                            "currency" => "COP"
                        ),
                        "TX_TAX" => array(
                            "value" => $data['iva'],
                            "currency" => "COP"
                        ),
                        "TX_TAX_RETURN_BASE" => array(
                            "value" => $data['total_base'],
                            "currency" => "COP"
                        )
                    ),
                    'buyer' => array(
                        'merchantBuyerId' => $data['user_id'],
                        'fullName' => $data['buyer']['fullname'],
                        'emailAddress' => $data['buyer']['email'],
                        'contactPhone' => $data['buyer']['phone'],
                        'dniNumber' => $data['buyer']['document_number'],
                        'shippingAddress' => array(
                           'street1' => $data['buyer']['address'],
                           'street2' => $data['buyer']['address_further'],
                           'city' => $data['buyer']['city'],
                           'state' => $data['buyer']['state'],
                           'country' => 'CO'
                        )
                     ),
                ),
                "payer" => array(
                    "fullName" => $data['payer']['fullname'],
                    "emailAddress" => $data['payer']['email'],
                    "contactPhone" => $data['payer']['phone']
                ),
                "extraParameters" => array(
                    "RESPONSE_URL" => route("PayU.resposnseUrl"),
                    "PSE_REFERENCE1" =>  Request::getClientIp(true),
                    "FINANCIAL_INSTITUTION_CODE" => $data['bank_code'],
                    "USER_TYPE" => $data['person_type'],
                    "PSE_REFERENCE2" => $data['document_type'],
                    "PSE_REFERENCE3" => $data['document_number']
                ),
                "type" => "AUTHORIZATION_AND_CAPTURE",
                "paymentMethod" => "PSE",
                "paymentCountry" => "CO",
                "ipAddress" =>  Request::getClientIp(true),
           )

        );

        $request = json_encode($request);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = file_get_contents($this->url, false, $context);
        $valid_xml = $this->isValidXml($response_xml);

        if($valid_xml)
            $status = simplexml_load_string($response_xml);

        if (!$response_xml) $response_xml = $http_response_header[0];
        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        $this->orderPaymentLogEntity->setOrderId($data['order_id']);
        $this->orderPaymentLogEntity->setUserId($data['user_id']);
        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if (isset($status->code) && $status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }elseif(isset($status->code) && $status->code == 'SUCCESS' && isset($status->transactionResponse) && $status->transactionResponse->state == 'PENDING'){
            $response = array(
                'status' => 1,
                'message' => 'Cobro procesado con éxito.',
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }
        return $response;
    }

    /**
     * Obtiene codigo en texto y mensaje de transaccion en PSE
     *
     * @param $polTransactionState
     * @param $polResponseCode
     * @return array
     */
    public function getPseTransactionStatus($polTransactionState, $polResponseCode, $InputAll)
    {
        $response = array();
        if($polTransactionState == 4 && $polResponseCode == 1){
            $response = [ 'result'=> 'APPROVED', 'message'=>'Transacción aprobada.'];
        }

        if($polTransactionState == 6 && $polResponseCode == 5){
            $response = [ 'result'=> 'FAIL', 'message'=>'Transacción fallida.'];
        }

        if($polTransactionState == 6 && $polResponseCode == 4){
            $response = [ 'result'=> 'REJECT', 'message'=>'Transacción rechazada.'];
        }

        if( ($polTransactionState == 12 || $polTransactionState == 14 ) && ($polResponseCode == 9994 || $polResponseCode == 25) ){
            $response = [ 'result'=> 'PENDING', 'message'=>'Transacción pendiente, por favor revisar si el débito fue realizado en el banco.'];
        }

        $request = array(
            "polTransactionState" => $polTransactionState,
            "polResponseCode" => $polResponseCode,
            "InputAll" => $InputAll
        );
        $request = json_encode($request);

        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode(200);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse(json_encode($response));
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        return $response;
    }

    /**
     * @param $transaction_id
     * @return array|mixed
     */
    public function getPseTransactionResponse($transaction_id)
    {
        $request = array(
            "test" => $this->test,
            "language" => "es",
            "command" => "TRANSACTION_RESPONSE_DETAIL",
            "merchant" => array(
                "apiLogin" => $this->api_login,
                "apiKey" => $this->api_key
            ),
            "details" => array(
                "transactionId" => $transaction_id
            )
        );

        $request = json_encode($request);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = file_get_contents($this->url_reports, false, $context);
        $status = simplexml_load_string($response_xml);
        if (!$response_xml) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        $response = json_decode($response);
        if ($status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => $response
            );
        }else{
            $response = array(
                'status' => 1,
                'message' => 'Resultado de la transacción obtenida con éxito.',
                'response' => $response->result->payload
            );
        }

        return $response;
    }

    /**
     * Obtiene informacion del banco de la tarjeta
     *
     * @param int $bin BIN de la tarjeta
     * @param array $post_data Datos POST del pedido
     * @return array $response Respuesta del servicio
     */
    public function getCreditCardInfo($bin, $postData)
    {
        if (empty($bin))
            return false;

            $binCodesValidator = new BinCodes($bin);
            $response = $binCodesValidator->validate();

            //guardar log
            $log = new BinLog;
            $log->user_id = $postData['user_id'];
            $log->url = $response->httpResponse['url'];
            $log->response_http_code = $response->httpResponse['code'];
            $log->response = $response->httpResponse['data'];
            $log->ip = get_ip();
            $log->created_at = $log->updated_at = date('Y-m-d H:i:s');
            $log->save();
            if(isset($response->error)){
                return false;
            }

            return $response;

    }

    /**
     * Importa tarjetas de crédito
     *
     * @param string $content_file Archivo en base64
     * @return array $response Respuesta del servicio
     */
    public function importCreditCards($content_file)
    {
        $request_data = array(
            'language'  => 'es',
            'command' => 'CREATE_BATCH_TOKENS',
            'merchant' => array(
                'apiLogin' => $this->api_login,
                'apiKey' => $this->api_key
            ),
            'contentFile' => $content_file
        );

        $request = json_encode($request_data);
        $options = array(
            'http' => array(
                'header'  => array('Content-type: application/json; charset=UTF-8'),
                'method'  => 'POST',
                'content' => $request,
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $response_xml = @file_get_contents($this->url, false, $context);
        $valid_xml = $this->isValidXml($response_xml);

        if($valid_xml)
            $status = simplexml_load_string($response_xml);

        if ($response_xml === false) $response_xml = $http_response_header[0];

        $http_response_code = substr($http_response_header[0], 9, 3);
        $response = $this->parseJson($response_xml);

        //guardar log
        $this->orderPaymentLogEntity->setMethod(__FUNCTION__);
        $this->orderPaymentLogEntity->setUrl($this->url);
        $this->orderPaymentLogEntity->setResponseHttpCode($http_response_code);
        $this->orderPaymentLogEntity->setRequest($request);
        $this->orderPaymentLogEntity->setResponse($response);
        $this->orderPaymentLogEntity->setIp(get_ip());
        $this->orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->orderPaymentLogEntity->runCommandSaveOrderPaymentLog();

        if ($valid_xml && isset($status->code) && $status->code == 'ERROR'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }elseif($valid_xml && isset($status->code) && $status->code == 'SUCCESS'){
            $response = array(
                'status' => 1,
                'message' => 'Tarjetas de crédito importadas con éxito.',
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }

        return $response;
    }

    /**
     * Obtiene mensaje de error
     *
     * @param string $code Codigo del error
     * @return array $response Respuesta del servicio
     */
    public function getErrorMessage($code, $paymentNetworkResponseErrorMessage = '')
    {
        $errors = [
            'PAYMENT_NETWORK_REJECTED' => 'Transacción rechazada por entidad financiera.',
            'ENTITY_DECLINED' => 'Transacción rechazada por el banco.',
            'INSUFFICIENT_FUNDS' => 'Fondos insuficientes.',
            'INVALID_CARD' => 'Tarjeta inválida.',
            'CONTACT_THE_ENTITY' => 'Contactar entidad financiera.',
            'BANK_ACCOUNT_ACTIVATION_ERROR' => 'Débito automático no permitido.',
            'BANK_ACCOUNT_NOT_AUTHORIZED_FOR_AUTOMATIC_DEBIT' => 'Débito automático no permitido.',
            'INVALID_AGENCY_BANK_ACCOUNT' => 'Débito automático no permitido.',
            'INVALID_BANK_ACCOUNT' => 'Débito automático no permitido.',
            'INVALID_BANK' => 'Débito automático no permitido.',
            'EXPIRED_CARD' => 'Tarjeta vencida.',
            'RESTRICTED_CARD' => 'Tarjeta restringida.',
            'INVALID_EXPIRATION_DATE_OR_SECURITY_CODE' => 'Fecha de expiración o código de seguridad inválidos.',
            'REPEAT_TRANSACTION' => 'Reintentar pago.',
            'INVALID_TRANSACTION' => 'Transacción inválida.',
            'EXCEEDED_AMOUNT' => 'El valor excede el máximo permitido por la entidad.',
            'CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS' => 'Tarjeta no autorizada para comprar por internet.',
            'ANTIFRAUD_REJECTED' => 'Transacción rechazada por sospecha de fraude.',
            'DIGITAL_CERTIFICATE_NOT_FOUND' => 'Certificado digital no encotnrado.',
            'BANK_UNREACHABLE' => 'Error tratando de cominicarse con el banco.',
            'PAYMENT_NETWORK_NO_CONNECTION' => 'No fue posible establecer comunicación con la entidad financiera.',
            'PAYMENT_NETWORK_NO_RESPONSE' => 'No se recibió respuesta de la entidad financiera.',
            'ENTITY_MESSAGING_ERROR' => 'Error comunicándose con la entidad financiera.',
            'NOT_ACCEPTED_TRANSACTION' => 'Transacción no permitida.',
            'EXPIRED_TRANSACTION' => 'Transacción expirada.'
        ];

        $response = 'Error sin detalle.';

        if (isset($errors[$code])) {
            $response = $errors[$code];
        }

        if (!empty($paymentNetworkResponseErrorMessage)) {
            $response .= ' ' . $paymentNetworkResponseErrorMessage;
        }

        return $response;
    }


    /**
    *  Takes XML string and returns a boolean result where valid XML returns true
    */
    private function isValidXml($xml)
    {
        if (empty($xml))
            return false;
        libxml_use_internal_errors( true );
        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML( $xml );
        $errors = libxml_get_errors();

        return empty( $errors );
    }

    /**
     * Convierte un xml string a json
     *
     * @param array $file_contents Datos xml string
     * @return array $json Retorna json
     */
    private function parseJson($file_contents)
    {
        $file_contents = str_replace(array("\n", "\r", "\t"), '', $file_contents);
        $file_contents = trim(str_replace('"', "'", $file_contents));
        $simpleXml = simplexml_load_string($file_contents);
        $json = json_encode($simpleXml);

        return $json;
    }

    /**
     * Genera la firma de autenticación de la transacción. Leer más aquí:
     * http://developers.payulatam.com/es/api/considerations.html
     *
     * @param string $orderReference
     * @param float $totalAmount
     * @param string $currency
     * @param string $statePol
     * @return string
     */
    public function generateTransactionSignature($orderReference, $totalAmount, $currency, $statePol = null)
    {
        $totalAmount = "$totalAmount.0";
        $chain = $this->api_key.'~'.$this->merchant_id.'~'.$orderReference.'~'.$totalAmount.'~'.$currency;
        $chain = $statePol ? $chain."~$statePol" : $chain;
        return md5($chain);
    }
}
