    <?php

use Firebase\Auth\Token\Generator;
use Firebase\Token\TokenException;
use Firebase\Token\TokenGenerator;
use GuzzleHttp\Client as GuzzleClient;


/**
* Clase para firebase
*/
class FirebaseClient
{

	private $firebase = null;

	/**
	 * Constructor
	 */
	public function __construct($type)
	{
	    $url = Config::get('app.firebase.'.$type.'.url');
		$token = Config::get('app.firebase.'.$type.'.token');

        if ($type == 'push')
        {
            try {
                $generator = new TokenGenerator($token);
                $token = $generator
                    ->setData(array('uid' => Config::get('app.firebase.'.$type.'.uid')))
                    ->create();
            } catch (TokenException $e) {
                //echo "Error: ".$e->getMessage(); exit;
                return false;
            }
        }

        $this->firebase = new \Firebase\FirebaseLib($url, $token);
	}

	/**
     * Guarda datos en bd en firebase
     *
     * @param string $path Ruta a bd
     * @param array $data Datos a guardar
     */
    public function setData($path, $data)
    {
        $this->firebase->set($path, $data);

    }

    /**
     * Eliminar datos en bd en firebase
     *
     * @param string $path Ruta a bd a eliminar
     */
    public function deleteData($path)
    {
        $this->firebase->delete($path);
    }

    /**
     * Obtener datos de bd en firebase
     *
     * @param string $path Ruta a bd a obtener
     */
    public function getData($path)
    {
        return $this->firebase->get($path);
    }

    /**
     * Genera un token valido para manejar datos de firebase.
     *
     * @param  string $type
     * @return string
     */
    public static function makeKey($type = 'gps')
    {
        $client_email = Config::get("app.firebase.{$type}.email");
        $private_key = Config::get("app.firebase.{$type}.private_key");
        $uid = Config::get("app.firebase.{$type}.uid");
        $generator = new Generator($client_email, $private_key);

        return (string) $generator->createCustomToken($uid);
    }

    /**
     * Crea un vínculo dinámico y lo asocia a una url específica.
     *
     * @param array $data Un array asociativo que cuyos valores pueden ser: url, type, store_id, department_id, shelf_id, product_id
     *
     * @param String $suffix_option Un string con dos posibles valores UNGUESSABLE o SHORT
     *
     * @return  stdClass Una clase stdClass con dos atributos: shortLink y previewLink
     */
    public static function generateDynamicLink(array $data = [], $suffix_option = 'SHORT')
    {
        $data_temp = $data;
        list($keys, $values) = array_divide($data);
        $data = $data_temp;
        if (in_array('url', $keys) && in_array('type', $keys) && in_array($suffix_option, ['UNGUESSABLE', 'SHORT'])) {
            $api_version = 'v1';
            $dinamic_links_url = 'https://firebasedynamiclinks.googleapis.com';
            $domain = Config::get('app.firebase.general.dinamic_link_domain');
            $api_key_web = Config::get('app.firebase.general.key');
            $end_point = $api_version . '/shortLinks?key=' . $api_key_web;
            $data = array_only($data, [ 'url', 'type', 'store_id', 'department_id', 'shelf_id', 'product_id', 'banner_id', 'referral_code']);
            $link = $data['url'];
            $data = array_except($data, ['url']);
            $link .= '?' . http_build_query($data);
            $mobile = [
                'apn' => 'com.merqueo', // Android
                'ibi' => 'com.merqueo.user.ios.Merqueo', // IOS
            ];
            $dynamic_link_info = [
                'longDynamicLink' => $domain . '?link=' . urlencode($link) . '&' . http_build_query($mobile),
                'suffix' => [
                    'option' => $suffix_option,
                ],
            ];
            $client = new GuzzleClient([
                'base_uri' => $dinamic_links_url,
            ]);
            $response = $client->post($end_point, [
                'headers'=>array('Content-Type'=>'application/json'),
                GuzzleHttp\RequestOptions::JSON => $dynamic_link_info,
            ]);
            return json_decode($response->getBody()->getContents());
        }
        return array();
    }
}
