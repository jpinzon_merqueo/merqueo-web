<?php


use Illuminate\Support\Facades\Config;


class BinCodes
{
    private $url;
    private $apiKey;
    private $allowed_bins;
    private $bin;
    private $response ;
    /**
     * BinValidation constructor.
     */
    public function __construct($bin)
    {
        $this->apiKey = Config::get('app.bincodes.apiKey');
        $this->url = str_replace('[api_key]', $this->apiKey, Config::get('app.bincodes.url'));
        $this->allowed_bins = Config::get('app.bincodes.allowed_bins');
        $this->bin = $bin;
        $this->response = new stdClass();
    }

    /**
     * @param int $bin
     */
    public function validate()
    {

        $this->response->httpResponse = $this->getApiResponse();

        if ($this->response->httpResponse['code'] == 200) {
            $binData = json_decode($this->response->httpResponse['data']);
            if(isset($binData->error)){
                $this->response->error = true;
                return $this->response;
            }
            if (!$this->isAllowedBin()) {
                $this->response->is_valid = $binData->countrycode == 'CO' ? true : false;
            } else {
                $this->response->is_valid = true;
            }

            $this->response->country_name = $binData->country;
        }else{
            $this->response->is_valid = true;
            $this->response->country_name = 'Validar manualmente';
        }

        return $this->response;

    }

    private function isAllowedBin(){
        $valid = false;
        $this->getAllowedBins();
        if (in_array($this->bin, $this->allowed_bins) || (Config::get('app.debug') && $this->bin == '411111')) {
            $valid = true;
        }
        return $valid;
    }

    private function getAllowedBins()
    {
        $bins = CreditCardBin::query()->lists('bin');
        $this->allowed_bins = array_merge($this->allowed_bins, $bins);
    }

    private function getApiResponse()
    {
        $options = array(
            'http' => array(
                'method'  => 'GET',
                'ignore_errors' => true
            ),
        );

        $context = stream_context_create($options);
        $data = @file_get_contents($this->url.$this->bin, false, $context);
        $code = substr($http_response_header[0], 9, 3);
        return [ 'data'=>$data, 'code'=>$code, 'url' => $this->url.$this->bin ];

    }
}