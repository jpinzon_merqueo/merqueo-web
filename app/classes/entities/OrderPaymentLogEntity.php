<?php

namespace app\classes\entities;

use Illuminate\Support\Facades\Queue;

/**
 * Class OrderPaymentLogEntity
 * @package App\Classes\entities
 */
class OrderPaymentLogEntity
{
    /**
     * @var $orderPaymentLogModel
     */
    public $data = [
        'id' => null,
        'order_id' => null,
        'user_id' => null,
        'method' => null,
        'url' => null,
        'response_http_code' => null,
        'request' => null,
        'response' => null,
        'ip' => null,
        'transaction_id' => null,
    ];

    /**
     * Guarda el log
     *
     * @return bool
     */
    public function saveOrderPaymentLog()
    {
        try {
            $orderPaymentLogModel = new \OrderPaymentLog();

            foreach ($this->data as $key => $datum) {
                $orderPaymentLogModel->{$key} = $datum;
            }

            return $orderPaymentLogModel->save();

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Guarda el log a traves del comando para evitar beginTransaction
     *
     * @return mixed
     * @throws Throwable
     */
    public function runCommandSaveOrderPaymentLog()
    {
        $data = $this->data;
        Queue::push(\jobs\SaveOrderPaymentLogJob::class, ['data'=>$data], 'quick');
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId)
    {
        $this->data['order_id'] = $orderId;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {
        $this->data['user_id'] = $userId;
    }

    /**
     * @param $method
     */
    public function setMethod($method)
    {
        $this->data['method'] = $method;
    }

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->data['url'] = $url;
    }

    /**
     * @param $httpResponseCode
     */
    public function setResponseHttpCode($httpResponseCode)
    {
        $this->data['response_http_code'] = $httpResponseCode;
    }

    /**
     * @param $request
     */
    public function setRequest($request)
    {
        $this->data['request'] = $request;
    }

    /**
     * @param $response
     */
    public function setResponse($response)
    {
        $this->data['response'] = $response;
    }

    /**
     * @param $ip
     */
    public function setIp($ip)
    {
        $this->data['ip'] = $ip;
    }

    /**
     * @param $transactionId
     */
    public function setTransactionId($transactionId)
    {
        if(is_array($transactionId)){
            $this->data['transaction_id'] = $transactionId[0];
        }else{
            $this->data['transaction_id'] = $transactionId;
        }
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->data['created_at'] = $createdAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->data['updated_at'] = $updatedAt;
    }
}