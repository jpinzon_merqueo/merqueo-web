<?php

/**
 *
 */
class AppUserRateBuilder     implements \JsonSerializable
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var array
     */
    private $available_problems;

    /**
     * @var array
     */
    private $unavailable_problems;

    /**
     * @var array
     */
    private $others;

    /**
     * @var array
     */
    private $titles;

    /**
     * @param Order $order
     * @param array $available_problems
     * @param array $unavailable_problems
     * @param array $others
     * @param array $titles
     */
    public function __construct(
        Order $order,
        $available_problems,
        $unavailable_problems,
        $others,
        $titles
    ) {
        $this->order = $order;
        $this->available_problems = $available_problems;
        $this->unavailable_problems = $unavailable_problems;
        $this->others = $others;
        $this->titles = $titles;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'details' => [
                'title' => $this->titles['main'],
                'order_id' => $this->order->id,
                'delivery_date' => $this->order->delivery_date,
                'delivery_time' => $this->order->delivery_time,
            ],
            'rating' => [
                'bad' => [
                    'title' => $this->titles['bad'],
                    'products' => [
                        'title' => $this->titles['products'],
                        'order_products' => [
                            'available_title' => $this->titles['available'],
                            'unavailable_title' => $this->titles['unavailable'],
                            'products_list' => $this->getProducts(),
                            'available_problems' => $this->available_problems,
                            'unavailable_problems' => $this->unavailable_problems,
                        ],
                    ],
                    'other' => $this->others,
                ],
                'success' => [
                    'title' => $this->titles['success'],
                ]
            ]
        ];
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        $order_products = $this->order
                        ->orderProducts()
                        ->orderBy('order_products.fulfilment_status')
                        ->with(['storeProduct' => function($query) {
                            $query->with('product');
                        }])
                        ->get();

        return $order_products->map(function($order_product) {
            $product = $order_product->storeProduct->product;
            $product->available = $order_product->isFulfilled;
            return $product;
        });
    }
}
