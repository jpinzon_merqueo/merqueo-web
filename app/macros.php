<?php

Form::macro('fillSelectWithKeys', function($name, $rows, $default = '', $attr = array(), $key = 'id', $value = 'name', $show_select = FALSE)
{
  if ($show_select)
    $arr = array('' => 'Seleccione');
  else
    $arr = array();
  if (count($rows) > 0){
    foreach($rows as $row){
      $arr = array_add($arr, $row->{$key}, $row->{$value});
    }
  }

  return Form::select($name, $arr, $default, $attr);
});

Form::macro('fillSelectWithoutKeys', function($name, $rows, $default = '', $attr = array(), $show_select = FALSE)
{
  if ($show_select)
    $arr = array('' => 'Seleccione');
  else
    $arr = array();
  foreach ($rows as $key => $value) {
    $arr = array_add($arr, $key, $value);
  }

  return Form::select($name, $arr, $default, $attr);
});
