<?php

namespace exceptions;

use Exception;

/**
 * Class MigrateException
 * @package exceptions
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class MigrateException extends Exception
{

}
