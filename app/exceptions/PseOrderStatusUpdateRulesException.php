<?php
namespace exceptions;

use Exception;

/**
 * Class PseOrderStatusUpdateRulesException.
 *
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
class PseOrderStatusUpdateRulesException extends Exception
{
    /**
     * @var string
     */
    public $message = "Reglas de actualización de estado de orden no cumplidas.";

    /**
     * @param string|null $message
     */
    public function __construct($message = null)
    {
        $message ? $this->message = $message : null;
    }
}
