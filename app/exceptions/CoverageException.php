<?php

namespace exceptions;

/**
 * Class CoverageException
 * @package exceptions
 */
class CoverageException extends MerqueoException
{
    /**
     * CoverageException constructor.
     * @param $latitude
     * @param $longitude
     */
    public function __construct($latitude, $longitude)
    {
        parent::__construct('No hay cobertura en tu dirección en este momento.');
        \UserAddressLog::make($latitude, $longitude, \UserAddressLog::ZONE_ISSUE);
    }
}
