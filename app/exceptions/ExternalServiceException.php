<?php

namespace exceptions;

class ExternalServiceException extends \Exception
{

    public $message = '';
    public $result = '';

    public function __construct($message, $result = '' ){
        $this->message = $message;
        $this->result = $result;
    }


}

