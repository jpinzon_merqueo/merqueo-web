<?php

namespace Campaigns\Repositories;

use Campaigns\Repositories\Contracts\BinRepository;
use CreditCardBin;

/**
 * Class EloquentCreditCardRepository
 * @package Campaigns\Repositories
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class EloquentBinRepository implements BinRepository
{
    /**
     * @param array $reference
     * @param $bin
     * @return bool
     */
    public function exists(array $reference, $bin)
    {
        return CreditCardBin::query()
                ->whereIn('reference', $reference)
                ->where('bin', $bin)
                ->count() > 0;
    }
}
