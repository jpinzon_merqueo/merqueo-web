<?php

namespace Campaigns\Repositories\Contracts;

/**
 * Interface BinRepository
 * @package Campaigns\Repositories\Contracts
 */
interface BinRepository
{
    /**
     * @param array $reference
     * @param $bin
     * @return bool
     */
    public function exists(array $reference, $bin);
}
