<?php

namespace Campaigns\Repositories\Contracts;

/**
 * Interface UserCreditCardRepository
 * @package Campaigns\Repositories\Contracts
 */
interface UserCreditCardRepository
{
    /**
     * @param string $lastFour
     * @param string $type
     * @param string $bin
     * @return int
     */
    public function countUses($lastFour, $type, $bin);

    /**
     * @param $userId
     * @param $lastFour
     * @param $type
     * @param $bin
     * @return int
     */
    public function countUsesByUser($userId, $lastFour, $type, $bin);
}
