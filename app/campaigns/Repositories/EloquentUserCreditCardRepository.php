<?php

namespace Campaigns\Repositories;

use Campaigns\Repositories\Contracts\UserCreditCardRepository;
use Order;

/**
 * Class EloquentUserCreditCardRepository
 * @package Campaigns\Repositories
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class EloquentUserCreditCardRepository implements UserCreditCardRepository
{
    /**
     * @param string $lastFour
     * @param string $type
     * @param string $bin
     * @return int
     */
    public function countUses($lastFour, $type, $bin)
    {
        return Order::where('cc_last_four', $lastFour)
            ->where('cc_bin', $bin)
            ->where('cc_type', $type)
            ->count();
    }

    /**
     * @param $userId
     * @param string $lastFour
     * @param string $type
     * @param string $bin
     * @return int
     */
    public function countUsesByUser($userId, $lastFour, $type, $bin)
    {
        return Order::where('cc_last_four', $lastFour)
            ->where('cc_bin', $bin)
            ->where('cc_type', $type)
            ->where('user_id', $userId)
            ->count();
    }
}
