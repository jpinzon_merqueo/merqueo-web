<?php

namespace Campaigns\Resolvers;

use Auth;
use InvalidArgumentException;
use User;

/**
 * Class UserResolver
 * @package Campaigns\Resolvers
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class UserResolver
{
    /**
     * @param array $input
     * @return User|null
     */
    public function resolve(array $input)
    {
        // TODO decouple of Laravel
        if (Auth::check()) {
            return Auth::user();
        }

        if (isset($input['user_id'])) {
            return $this->getFromRequest($input);
        }

        return null;
    }

    /**
     * @param array $input
     * @return User|null
     */
    private function getFromRequest(array $input)
    {
        return User::find($input['user_id']);
    }
}
