<?php

namespace Campaigns\Resolvers;

use UserCreditCard;

/**
 * Class CreditCardResolver
 * @package Campaigns\Resolvers
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class CreditCardResolver
{
    /**
     * @param array $input
     * @return UserCreditCard|null
     */
    public function resolve(array $input)
    {
        if (isset($input['credit_card_id'])) {
            return $this->getFromRequest($input);
        }

        return null;
    }

    /**
     * @param array $input
     * @return UserCreditCard|null
     */
    private function getFromRequest(array $input)
    {
        return UserCreditCard::find($input['credit_card_id']);
    }
}
