<?php

namespace Campaigns\Rules;

use Campaigns\Rules\Contracts\Validable;
use Carbon\Carbon;
use Closure;

/**
 * Class DateRule
 * @package Campaigns
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class DateRule implements Validable
{
    /**
     * @var Closure
     */
    private $dateResolver;

    /**
     * DateRule constructor.
     * @param Closure $dateResolver
     */
    public function __construct(Closure $dateResolver)
    {
        $this->dateResolver = $dateResolver;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        return call_user_func($this->dateResolver, $input);
    }

    /**
     * @param array $daysNumber
     * @return bool
     */
    public static function daysOfWeek(array $daysNumber)
    {
        return in_array(Carbon::now()->dayOfWeek, $daysNumber);
    }

    /**
     * @param Carbon $begin
     * @param Carbon $end
     * @return bool
     */
    public static function dateRange(Carbon $begin, Carbon $end)
    {
        $today = Carbon::now()
            ->startOfDay();

        return $today->greaterThanOrEqualTo($begin) && $today->lessThanOrEqualTo($end);
    }
}
