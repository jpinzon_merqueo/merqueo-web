<?php

namespace Campaigns\Rules;

use Campaigns\Resolvers\UserResolver;
use Campaigns\Rules\Contracts\Validable;
use InvalidArgumentException;
use User;

/**
 * Class UserOrdersRule
 * @package Campaigns
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class TotalUserOrdersRule implements Validable
{
    /**
     * @var User
     */
    private $userResolver;

    /**
     * @var string
     */
    private $operator;

    /**
     * @var int
     */
    private $amount;

    /**
     * UserOrdersRule constructor.
     * @param UserResolver $userResolver
     * @param $operator
     * @param $amount
     */
    public function __construct(UserResolver $userResolver, $operator, $amount)
    {
        $validOperators = ['<', '=', '>'];
        if (!in_array($operator, $validOperators)) {
            throw new InvalidArgumentException(
                "Operator {$operator} is not valid. Available: " .
                implode(', ', $validOperators)
            );
        }

        $this->userResolver = $userResolver;
        $this->operator = $operator;
        $this->amount = $amount;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        $orders = $this->getTotalOrders($input);

        if ($this->operator === '<') {
            return $orders < $this->amount;
        }

        if ($this->operator === '>') {
            return $orders > $this->amount;
        }

        if ($this->operator === '=') {
            return $orders === $this->amount;
        }

        return false;
    }

    /**
     * @param $input
     * @return int
     */
    private function getTotalOrders($input)
    {
        $user = $this->userResolver->resolve($input);

        if (!$user) {
            return 0;
        }

        return $user->orders()
            ->where('status', '<>', 'Cancelled')
            ->count();
    }
}
