<?php

namespace Campaigns\Rules;

use Campaigns\Resolvers\CreditCardResolver;
use Campaigns\Rules\Contracts\Validable;

/**
 * Class CreditCardRule
 * @package Campaigns\Rules
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class CreditCardRule implements Validable
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var CreditCardResolver
     */
    private $creditCardResolver;

    /**
     * @var array
     */
    private $bins;

    /**
     * CreditCardRule constructor.
     * @param CreditCardResolver $creditCardResolver
     * @param string $type
     * @param array $bins
     */
    public function __construct(CreditCardResolver $creditCardResolver, $type = '', array $bins = [])
    {
        $this->type = $type;
        $this->creditCardResolver = $creditCardResolver;
        $this->bins = $bins;
    }

    /**
     * @param array $input
     * @return bool
     */
    public function validate($input)
    {
        $creditCard = $this->creditCardResolver->resolve($input);

        if (!$creditCard) {
            return false;
        }

        if (!empty($this->bins) && !in_array($creditCard->bin, $this->bins)) {
            return false;
        }

        return $this->type === null || strtolower($creditCard->type) === strtolower($this->type);
    }
}
