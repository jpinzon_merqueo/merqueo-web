<?php

namespace Campaigns\Rules;

use Campaigns\Repositories\Contracts\UserCreditCardRepository;
use Campaigns\Resolvers\CreditCardResolver;
use Campaigns\Rules\Contracts\Validable;

/**
 * Class CreditCardUsesRule
 * @package Campaigns\Rules
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class CreditCardUsesRule implements Validable
{
    /**
     * @var CreditCardResolver
     */
    private $creditCardResolver;

    /**
     * @var int
     */
    private $orderLimit;

    /**
     * @var UserCreditCardRepository
     */
    private $repository;

    /**
     * CreditCardUsesRule constructor.
     * @param CreditCardResolver $creditCardResolver
     * @param UserCreditCardRepository $repository
     * @param int $orderLimit Limite inclusivo
     */
    public function __construct(
        CreditCardResolver $creditCardResolver,
        UserCreditCardRepository $repository,
        $orderLimit = 0
    ) {
        $this->creditCardResolver = $creditCardResolver;
        $this->orderLimit = $orderLimit;
        $this->repository = $repository;
    }

    /**
     * @param array $input
     * @return bool
     */
    public function validate($input)
    {
        $creditCard = $this->creditCardResolver->resolve($input);

        $totalOrders = $this->repository->countUses(
            $creditCard->last_four,
            $creditCard->type,
            $creditCard->bin
        );

        return $totalOrders <= $this->orderLimit;
    }
}
