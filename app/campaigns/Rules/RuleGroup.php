<?php

namespace Campaigns\Rules;

use Campaigns\DataTransferObjects\Contracts\DataTransfer;
use Campaigns\Rules\Contracts\Validable;

/**
 * Class RuleGroup
 * @package Campaigns
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class RuleGroup implements Validable
{
    /**
     * @var Validable[]
     */
    private $rules;

    /**
     * RuleGroup constructor.
     * @param array $rules
     */
    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        foreach ($this->getRules() as $rule) {
            $result = $rule->validate($input);

            if (!$result) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Validable[]
     */
    public function getRules()
    {
        return $this->rules;
    }
}
