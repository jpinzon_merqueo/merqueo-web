<?php

namespace Campaigns\Rules\Contracts;

/**
 * Interface Validable
 */
interface Validable
{
    /**
     * @param array $input
     * @return bool
     */
    public function validate($input);
}
