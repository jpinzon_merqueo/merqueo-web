<?php

namespace Campaigns\Rules;

use Campaigns\Rules\Contracts\Validable;

/**
 * Class PaymentMethodRule
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class PaymentMethodRule implements Validable
{
    /**
     * @var array
     */
    private $paymentMethods;

    /**
     * PaymentMethodRule constructor.
     * @param array $paymentMethods
     */
    public function __construct(array $paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        return in_array($input['payment_method'], $this->paymentMethods);
    }
}
