<?php

namespace Campaigns\Rules;

/**
 * Class InclusiveOrGroup
 * @package Campaigns\Rules
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class InclusiveOrGroup extends RuleGroup
{
    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        foreach ($this->getRules() as $rule) {
            $result = $rule->validate($input);

            if ($result) {
                return true;
            }
        }

        return false;
    }
}
