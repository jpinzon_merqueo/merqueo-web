<?php

namespace Campaigns\Rules;

use Campaigns\Rules\Contracts\Validable;

/**
 * Class OrderAmountRule
 * @package Campaigns
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class OrderAmountRule implements Validable
{
    /**
     * @var int
     */
    private $minimumAmount;

    /**
     * @var int
     */
    private $maximumAmount;

    /**
     * OrderAmountRule constructor.
     * @param $minimumAmount
     * @param $maximumAmount
     */
    public function __construct($minimumAmount, $maximumAmount)
    {
        $this->minimumAmount = $minimumAmount;
        $this->maximumAmount = $maximumAmount;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        if (!isset($input['total_amount'])) {
            throw new \InvalidArgumentException('total_amount values is required');
        }

        return $input['total_amount'] >= $this->minimumAmount &&
            $input['total_amount'] <= $this->maximumAmount;
    }
}
