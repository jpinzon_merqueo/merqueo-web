<?php

namespace Campaigns\Campaigns\Contracts;

/**
 * Interface Campaign
 * @package Campaigns\Campaigns\Contracts
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
interface Campaign
{
    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $input
     * @return bool
     */
    public function isEnabled($input);

    /**
     * @return int|null
     */
    public function getPercentageDiscount();

    /**
     * @param $orderAmount
     * @return int
     */
    public function getDiscountAmount($orderAmount);

    /**
     * @param $input
     * @return bool
     */
    public function validate($input);
}
