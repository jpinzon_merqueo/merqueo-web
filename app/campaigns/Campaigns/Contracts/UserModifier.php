<?php

namespace Campaigns\Campaigns\Contracts;

use User;

/**
 * Interface UserModifier
 * @package Campaigns\Campaigns\Contracts
 */
interface UserModifier
{
    /**
     * @param User $user
     * @return void
     */
    public function updateUser(User $user);
}
