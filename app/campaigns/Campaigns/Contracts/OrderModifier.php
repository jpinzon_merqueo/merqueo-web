<?php

namespace Campaigns\Campaigns\Contracts;

/**
 * Interface OrderModifier
 * @package Campaigns\Campaigns\Contracts
 */
interface OrderModifier
{
    /**
     * @param array $order Valores del carrito actual
     * @return mixed
     */
    public function updateOrder(&$order);
}
