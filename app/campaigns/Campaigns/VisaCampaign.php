<?php

namespace Campaigns\Campaigns;

use Campaigns\Campaigns\Contracts\Campaign;
use Campaigns\Resolvers\CreditCardResolver;
use Campaigns\Resolvers\UserResolver;
use Campaigns\Rules\CreditCardRule;
use Campaigns\Rules\DateRule;
use Campaigns\Rules\OrderAmountRule;
use Campaigns\Rules\PaymentMethodRule;
use Campaigns\Rules\RuleGroup;
use Campaigns\Rules\TotalUserOrdersRule;
use Carbon\Carbon;
use InvalidArgumentException;

/**
 * Class VisaCampaign
 * @package Campaigns\Campaigns
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class VisaCampaign implements Campaign
{
    /**
     * @var int
     */
    private $maximumAmount;

    /**
     * @var float|null
     */
    private $percentageDiscount;

    /**
     * @var UserResolver
     */
    private $userResolver;

    /**
     * @var CreditCardResolver
     */
    private $creditCardResolver;

    /**
     * VisaCampaign constructor.
     * @param UserResolver $userResolver
     * @param CreditCardResolver $creditCardResolver
     * @param $maximumAmount
     * @param $percentageDiscount
     */
    public function __construct(
        UserResolver $userResolver,
        CreditCardResolver $creditCardResolver,
        $maximumAmount,
        $percentageDiscount = null
    ) {
        $this->maximumAmount = $maximumAmount;
        $this->percentageDiscount = $percentageDiscount;
        $this->userResolver = $userResolver;
        $this->creditCardResolver = $creditCardResolver;

        if ($percentageDiscount !== null && ($this->percentageDiscount > 1 || $this->percentageDiscount < 0)) {
            throw new InvalidArgumentException('Percentage discount could not be greater than 1 or less than 0');
        }
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return '30% DTO pagando online con tarjeta de crédito VISA en tu primera compra.';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'visa 30%';
    }

    /**
     * @param $input
     * @return bool
     */
    public function isEnabled($input)
    {
        return self::getDateRules()->validate($input) && $this->getTotalUserOrdersRule()->validate($input);
    }

    /**
     * @return DateRule
     */
    private function getDateRules()
    {
        return new DateRule(function () {
            return DateRule::dateRange(new Carbon('2019-03-01'), new Carbon('2019-04-30')) &&
                DateRule::daysOfWeek([Carbon::WEDNESDAY]);
        });
    }

    /**
     * @return TotalUserOrdersRule
     */
    private function getTotalUserOrdersRule()
    {
        return new TotalUserOrdersRule($this->userResolver, '=', 0);
    }

    /**
     * @return float|null
     */
    public function getPercentageDiscount()
    {
        return $this->percentageDiscount;
    }

    /**
     * @param $orderAmount
     * @return int
     */
    public function getDiscountAmount($orderAmount)
    {
        $discountAmount = round($orderAmount * $this->percentageDiscount);

        if ($discountAmount > $this->maximumAmount) {
            return $this->maximumAmount;
        }

        return $discountAmount;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        $rules = new RuleGroup([
            new PaymentMethodRule(['Tarjeta de crédito']),
            new CreditCardRule($this->creditCardResolver, 'VISA'),
            new OrderAmountRule(30000, INF),
            $this->getDateRules(),
            $this->getTotalUserOrdersRule(),
        ]);

        return $rules->validate($input);
    }
}
