<?php

namespace Campaigns\Campaigns;

use Campaigns\Repositories\Contracts\UserCreditCardRepository;
use User;
use Carbon\Carbon;
use CreditCardBin;
use Campaigns\Rules\DateRule;
use Campaigns\Rules\RuleGroup;
use Campaigns\Rules\CreditCardRule;
use Campaigns\Rules\CreditCardUsesRule;
use Campaigns\Campaigns\Contracts\Campaign;
use Campaigns\Resolvers\CreditCardResolver;
use Campaigns\Campaigns\Contracts\UserModifier;

/**
 * Class BancolombiaCampaign
 * @package Campaigns\Campaigns
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class BancolombiaCampaign implements Campaign, UserModifier
{
    /**
     * @var CreditCardResolver
     */
    private $creditCardResolver;

    /**
     * @var int
     */
    private $daysWithFreeDelivery;

    /**
     * @var UserCreditCardRepository
     */
    private $creditCardRepository;

    /**
     * BancolombiaCampaign constructor.
     * @param CreditCardResolver $creditCardResolver
     * @param UserCreditCardRepository $creditCardRepository
     * @param int $daysWithFreeDelivery
     */
    public function __construct(
        CreditCardResolver $creditCardResolver,
        UserCreditCardRepository $creditCardRepository,
        $daysWithFreeDelivery = 60
    ) {
        $this->creditCardResolver = $creditCardResolver;
        $this->daysWithFreeDelivery = $daysWithFreeDelivery;
        $this->creditCardRepository = $creditCardRepository;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return 'Recibe 2 meses de envío gratis, inscribiendo y pagando con TC Bancolombia. Aplican T&C';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bancolombia';
    }

    /**
     * @param $input
     * @return bool
     */
    public function isEnabled($input)
    {
        return DateRule::dateRange(
            $this->getBeginDate(),
            $this->getEndDate()
        );
    }

    /**
     * @return RuleGroup
     */
    public function getRules()
    {
        return new RuleGroup([
            new DateRule(function ($input) {
                return $this->isEnabled($input);
            }),
            $this->getCreditCardRule(),
            $this->getCreditCardUsesRule(),
        ]);
    }

    /**
     * @return int|null
     */
    public function getPercentageDiscount()
    {
        return null;
    }

    /**
     * @param $orderAmount
     * @return int
     */
    public function getDiscountAmount($orderAmount)
    {
        return 0;
    }

    /**
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        return $this->getRules()
            ->validate($input);
    }

    /**
     * @return CreditCardRule
     */
    private function getCreditCardRule()
    {
        return new CreditCardRule($this->creditCardResolver, null, $this->getBins());
    }

    /**
     * @return CreditCardUsesRule
     */
    private function getCreditCardUsesRule()
    {
        return new CreditCardUsesRule($this->creditCardResolver, $this->creditCardRepository, 0);
    }

    /**
     * @return Carbon
     */
    private function getBeginDate()
    {
        return new Carbon('2019-04-01');
    }

    /**
     * @return Carbon
     */
    private function getEndDate()
    {
        return $this->getBeginDate()
            ->addMonth(3);
    }

    /**
     * @param User $user
     * @return void
     */
    public function updateUser(User $user)
    {
        if (!$this->shouldUpdateUser($user)) {
            return null;
        }

        $user->free_delivery_expiration_date = Carbon::now()->addDays($this->daysWithFreeDelivery);
    }

    /**
     * @param User $user
     * @return bool
     */
    private function shouldUpdateUser(User $user)
    {
        $possibleFreeDeliveryDate = Carbon::now()->addDays($this->daysWithFreeDelivery);

        if (!$user->free_delivery_expiration_date) {
            return true;
        }

        $currentFreeDeliveryDate = new Carbon($user->free_delivery_expiration_date);

        return $currentFreeDeliveryDate->lt($possibleFreeDeliveryDate);
    }

    /**
     * @return array
     */
    private function getBins()
    {
        return CreditCardBin::whereIn('reference', ['bancolombia', 'debito_bancolombia'])
            ->lists('bin');
    }
}
