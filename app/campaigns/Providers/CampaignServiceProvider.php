<?php

namespace Campaigns\Providers;

use Campaigns\Campaigns\BancolombiaCampaign;
use Campaigns\Campaigns\VisaCampaign;
use Campaigns\Repositories\Contracts\BinRepository;
use Campaigns\Repositories\Contracts\UserCreditCardRepository;
use Campaigns\Repositories\EloquentBinRepository;
use Campaigns\Repositories\EloquentUserCreditCardRepository;
use Campaigns\Resolvers\CreditCardResolver;
use Campaigns\Resolvers\UserResolver;
use Illuminate\Support\ServiceProvider;

/**
 * Class CampaignServiceProvider
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class CampaignServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(VisaCampaign::class, function () {
            return new VisaCampaign(app(UserResolver::class), app(CreditCardResolver::class), 75000, 0.3);
        });

        $this->app->singleton(BancolombiaCampaign::class, function () {
            return new BancolombiaCampaign(
                app(CreditCardResolver::class),
                app(UserCreditCardRepository::class)
            );
        });

        $this->app->singleton(UserCreditCardRepository::class, function () {
            return new EloquentUserCreditCardRepository();
        });

        $this->app->bind(BinRepository::class, EloquentBinRepository::class);
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            VisaCampaign::class,
            BancolombiaCampaign::class,
            UserCreditCardRepository::class,
            BinRepository::class,
        ];
    }
}
