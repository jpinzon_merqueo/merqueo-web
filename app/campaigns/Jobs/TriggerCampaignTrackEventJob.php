<?php

namespace Campaigns\Jobs;

use Order;
use ErrorLog;
use Exception;
use Illuminate\Queue\Jobs\Job;
use Guzzle\Http\Exception\RequestException;
use Campaigns\Campaigns\BancolombiaCampaign;
use Campaigns\Repositories\Contracts\BinRepository;
use Campaigns\Repositories\EloquentUserCreditCardRepository;

/**
 * Class TriggerCampaignTrackEventJob
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class TriggerCampaignTrackEventJob
{
    /**
     * @var BancolombiaCampaign
     */
    private $bancolombia;

    /**
     * @var EloquentUserCreditCardRepository
     */
    private $repository;

    /**
     * @var BinRepository
     */
    private $binRepository;

    /**
     * TriggerCampaignTrackEventJob constructor.
     * @param BancolombiaCampaign $bancolombiaCampaign
     * @param EloquentUserCreditCardRepository $repository
     * @param BinRepository $binRepository
     */
    public function __construct(
        BancolombiaCampaign $bancolombiaCampaign,
        EloquentUserCreditCardRepository $repository,
        BinRepository $binRepository
    ) {
        $this->bancolombia = $bancolombiaCampaign;
        $this->repository = $repository;
        $this->binRepository = $binRepository;
    }

    /**
     * @param Job $job
     * @param $data
     * @return bool|void
     * @throws Exception
     */
    public function fire(Job $job, $data)
    {
        sleep(5);

        $order = Order::with('user')
            ->find($data['order_id']);

        if (empty($order)) {
            $job->delete();
            ErrorLog::add(new Exception("Can not trigger campaign event on order {$data['order_id']}"));
            return;
        }

        if (!$this->bancolombia->isEnabled([])) {
            $job->delete();
            return false;
        }

        try {
            if (!$this->trackPositiveEvent($order)) {
                $this->trackNegativeEvent($order);
            }

            $job->delete();
        } catch (RequestException $exception) {
            ErrorLog::add($exception, 418);
            $job->release(20);
            return;
        }
    }

    /**
     * @param $userId
     * @param $apply
     * @throws Exception
     */
    private function trackEvent($userId, $apply)
    {
        track_leanplum_event('alianzas', $userId, [
            'campaign' => "{$this->bancolombia->getName()}-" . ($apply ? '' : 'not-') . "apply",
        ]);
    }

    /**
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function trackPositiveEvent(Order $order)
    {
        if (!$order->hasCampaignDiscount($this->bancolombia->getName())) {
            return false;
        }

        send_sms(
            $order->user->phone,
            'MERQUEO: Felicitaciones! Ya inscribiste y pagaste con tu ' .
            'tarjeta de credito Bancolombia, a partir de ahora disfruta ' .
            '60 dias de Domicilio gratis. Aplican TyC.'
        );
        $this->trackEvent($order->user_id, true);

        return true;
    }

    /**
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function trackNegativeEvent(Order $order)
    {
        $hasBancolombiaBin = $this->binRepository->exists(['bancolombia', 'debito_bancolombia'], $order->cc_bin);

        if (!$hasBancolombiaBin) {
            return false;
        }

        $isFirstOrderByUser = $this->repository->countUsesByUser(
                $order->user_id,
                $order->cc_last_four,
                $order->cc_type,
                $order->cc_bin
            ) <= 1;

        if ($isFirstOrderByUser) {
            send_sms(
                $order->user->phone,
                'MERQUEO: Lo sentimos! Para recibir los 60 dias de envio gratis con Bancolombia, se ' .
                'deben cumplir con los TyC de la campaña. Ver mas https://merqueo.com/terminos'
            );
            $this->trackEvent($order->user_id, false);
            return true;
        }

        return false;
    }
}
