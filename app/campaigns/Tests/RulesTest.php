<?php

namespace Campaigns\Tests;

use Campaigns\Repositories\Contracts\UserCreditCardRepository;
use Campaigns\Resolvers\CreditCardResolver;
use Campaigns\Resolvers\UserResolver;
use Campaigns\Rules\CreditCardUsesRule;
use Campaigns\Rules\DateRule;
use Campaigns\Rules\OrderAmountRule;
use Campaigns\Rules\TotalUserOrdersRule;
use Carbon\Carbon;
use DB;
use Tests\TestCase;
use Tests\Traits\MerqueoFactory;
use User;
use UserCreditCard;

/**
 * Class RulesTest
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class RulesTest extends TestCase
{
    use MerqueoFactory;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
        DB::rollback();
    }

    public function testFirstOrder()
    {
        $rule = new TotalUserOrdersRule($this->userResolver(), '=', 0);
        $result = $rule->validate([]);
        $this->assertTrue($result, 'Users does not have orders');
    }

    public function testValidaGreaterThan()
    {
        $rule = new TotalUserOrdersRule($this->userResolver(), '>', 1);
        $result = $rule->validate([]);
        $this->assertFalse($result, 'Users must to have at least one order');
    }

    public function testUserWithOrder()
    {
        $user = $this->newUser();
        $this->makeFakeOrder(null, $user);

        $rule = new TotalUserOrdersRule($this->userResolver($user), '=', 0);
        $result = $rule->validate([]);
        $this->assertFalse($result, 'User already has a order');
    }

    public function testOrderAmount()
    {
        $rule = new OrderAmountRule(30000, 250000);
        $result = $rule->validate(['total_amount' => 20000]);
        $this->assertFalse($result, 'Order amount is not enough');

        $rule = new OrderAmountRule(30000, 250000);
        $result = $rule->validate(['total_amount' => 30000]);
        $this->assertTrue($result, 'Order amount is not enough');
    }

    public function testProperDateRule()
    {
        Carbon::setTestNow(new Carbon('2019-03-06'));

        $rule = new DateRule(function () {
            return DateRule::dateRange(new Carbon('2019-03-01'), new Carbon('2019-04-30')) &&
                DateRule::daysOfWeek([Carbon::WEDNESDAY]);
        });

        $this->assertTrue($rule->validate([]), 'Date is in the dates range and its wednesday as it requires');
    }

    public function testInvalidDayRule()
    {
        Carbon::setTestNow(new Carbon('2019-03-05'));

        $rule = new DateRule(function () {
            return DateRule::daysOfWeek([Carbon::WEDNESDAY]);
        });

        $this->assertFalse($rule->validate([]), 'Day is not wednesday');
    }

    public function testInvalidDateRule()
    {
        Carbon::setTestNow(new Carbon('2019-05-01'));

        $rule = new DateRule(function () {
            return DateRule::dateRange(new Carbon('2019-03-01'), new Carbon('2019-04-30'));
        });

        $this->assertFalse($rule->validate([]), 'Date is not in range');
    }

    public function testValidateCreditCardUses()
    {
        $repository = $this->userCreditCardMock(0);
        $resolver = $this->mockCreditCardResolver();
        $rule = new CreditCardUsesRule($resolver, $repository, 0);

        $this->assertTrue($rule->validate([]));

        $repository = $this->userCreditCardMock(1);
        $resolver = $this->mockCreditCardResolver();
        $rule = new CreditCardUsesRule($resolver, $repository, 0);
        $this->assertFalse($rule->validate([]));
    }

    /**
     * @param int $numberUses
     * @return UserCreditCardRepository
     */
    private function userCreditCardMock($numberUses = 0)
    {
        $mock = $this->getMock(UserCreditCardRepository::class);
        $mock->expects($this->once())
            ->method('countUses')
            ->willReturn($numberUses);

        return $mock;
    }

    /**
     * @return UserCreditCard
     */
    private function mockCreditCardResolver()
    {
        $mock = $this->getMock(CreditCardResolver::class);
        $mock->expects($this->once())
            ->method('resolve')
            ->willReturn(new UserCreditCard());

        return $mock;
    }

    /**
     * @param null $user
     * @return UserResolver
     */
    private function userResolver($user = null)
    {
        $mock = $this->getMock(UserResolver::class);
        $mock->expects($this->once())
            ->method('resolve')
            ->willReturn($user ?: new User());

        return $mock;
    }
}
