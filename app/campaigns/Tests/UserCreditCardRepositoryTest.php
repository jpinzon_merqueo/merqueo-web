<?php

namespace Campaigns\Tests;

use DB;
use Tests\TestCase;
use Campaigns\Repositories\EloquentUserCreditCardRepository;
use Campaigns\Repositories\Contracts\UserCreditCardRepository;
use Tests\Traits\MerqueoFactory;

/**
 * Class UserCreditCardRepositoryTest
 * @package Campaigns\Tests
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class UserCreditCardRepositoryTest extends TestCase
{
    use MerqueoFactory;

    /**
     * @var UserCreditCardRepository
     */
    private $repository;

    public function setUp()
    {
        parent::setUp();

        DB::beginTransaction();
        $this->repository = new EloquentUserCreditCardRepository();
    }

    public function tearDown()
    {
        parent::tearDown();

        DB::rollBack();
    }

    public function testCountUses()
    {
        $lastFour = '0000';
        $type = '0000';
        $bin = '0000';

        $uses = $this->repository->countUses($lastFour, $type, $bin);
        $this->assertEquals(0, $uses);

        $order = $this->makeFakeOrder();
        $order->cc_last_four = $lastFour;
        $order->cc_type = $type;
        $order->cc_bin = $bin;
        $order->save();

        $uses = $this->repository->countUses($lastFour, $type, $bin);
        $this->assertEquals(1, $uses);
    }
}
