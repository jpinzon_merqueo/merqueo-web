<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

    app_path().'/commands',
    app_path().'/controllers',
    app_path().'/models',
    app_path().'/database/seeds',
    app_path().'/classes',
    app_path().'/events',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useDailyFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function (Exception $exception, $code) {
    //never, ever, use environment variables in responses, not even when debugging
    $_ENV = [];
    foreach ($_SERVER as $key => $value) {
        if (strstr($key, 'APP_') || strstr($key, 'DB_')) {
            unset($_SERVER[$key]);
        }
    }

    if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
        $code = 404;
    }

    $message = ErrorLog::buildMessage($exception);
    if ($code != 404) {
        Log::error($message);

        if (Config::get('app.debug')) {
            Log::error($exception->getTraceAsString());
        }
    }

    if ($code == 500) {
        if (DB::transactionLevel() == 1) {
            DB::rollback();
        }

        //guardar error log
        ErrorLog::add($exception, $code);

        //error en pagina web
        if (!Request::is('api/*')) {
            if (!Config::get('app.debug')) {
                return Response::view('errors.500', array(), 500);
            }
        } else {
            //error en API
            if (!Config::get('app.debug')) {
                $message = 'Algo salió mal intentalo nuevamente.';
            }

            if (Request::is('api/shoppers/*')) {
                return App::make('ApiController')->internalError($code, $exception->getMessage());
            }
            return App::make('ApiController')->internalError($code, $message);
        }
    }

    if (Request::is('api/*') && $code == 404) {
        ErrorLog::add($exception, $code);

        $message = $exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException
            ? 'Elemento no encontrado.'
            : 'Ruta no encontrada.';

        return App::make('ApiController')->internalError($code, $message);
    } else {
        if ($code == 404) {
            $BaseController = App::make('BaseController');
            $footer = $BaseController->getFooter();

            return Response::view('errors.404', ['footer' => $footer], 404);
        }
    }
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function () {

    /*if(Request::is('tester*') || Request::is('admin*') || Request::is('*picking*')) {
        return null;
    }*/

    return Response::view('errors.503', array(), 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';
require app_path().'/macros.php';
require app_path().'/helpers.php';

// Route bind
Route::model('store', 'Store');
Route::model('order', 'Order');
Route::model('external_service', 'ExternalService');
Route::model('external_service_payment', 'ExternalServicePayment');
Route::bind('city_name', function ($slug) {
    $valid_cities = ['bogota', 'medellin'];
    return \City::where('status', 1)
        ->where('slug', in_array($slug, $valid_cities) ? $slug : 'bogota')
        ->firstOrFail();
});

// Model Observers
User::observe(new UserObserver());
StoreProduct::observe(new StoreProductObserver());
StoreProductGroup::observe(new StoreProductGroupObserver());

App::bind(\contracts\SmsService::class, \services\InfoBipSmsService::class);
App::bind(\contracts\ApiDashboardInterface::class, \services\ApiDashboardService::class);
