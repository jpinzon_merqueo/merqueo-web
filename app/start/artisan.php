<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new SyncUserTraits);
Artisan::add(new FirebaseGenerateDynamicLinksCommand);
Artisan::add(new FirebaseGenerateDynamicLinksReferral);
Artisan::add(new CouponsGenerator());
Artisan::add(new UpdateUserStatusLeanplum());
Artisan::add(new refactorDiscontinuedProductsField());
Artisan::add(new UpdateUserFieldsLeanplum());
Artisan::add(new ValidateAddress());
Artisan::add(new SetStoreDiscount());
Artisan::add(new FixDiscountsInStoreProductGroup());
Artisan::add(new SanitizeGroupProductSpecialPrice());
Artisan::add(new UpdateUserFieldsLeanplum());
Artisan::add(new SyncPrices());
Artisan::add(new SendInvoices());
Artisan::add(new BdScripts());
Artisan::add(new UsersNewSiteSegmentation());
Artisan::add(App::make(SyncPseBanksCommand::class));
Artisan::add(new UploadCustomerInvoices());
Artisan::add(new fixConsecutivesInvoice());
Artisan::add(new ChangeStatusCoupons());
Artisan::add(new UpdateSlotsProducts());
Artisan::add(new MerqueoTasksCommand());
Artisan::add(new AddCreditUsers());
Artisan::add(new SaveOrderPaymentLog());
