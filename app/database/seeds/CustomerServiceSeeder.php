<?php

use \Illuminate\Database\Seeder;

class CustomerServiceSeeder extends Seeder
{

    public function run()
    {
        $services = [CustomerService::TICKET, CustomerService::PQR];
        $colors = ['#2982d2', '#ea4335'];

        foreach ($services as $index => $service) {
            $services = new \CustomerService([
                'description' => $service,
                'priority' => $index,
                'color' => $colors[$index]
            ]);
            $services->save();
        }
    }
}
