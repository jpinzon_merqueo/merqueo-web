<?php

use \Illuminate\Database\Seeder;

class ReasignRouteSeeder extends Seeder
{

    public function run()
    {
        MenuItem::insert([
            'title' => 'Dividir y Reasignar Ruta',
            'controller_action' => 'admin\dispatch\DivideRouteController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,reasignRoute,getDriversAjax,divideRoute,reassignTransporters',
            'insert_actions' => 'index,reasignRoute,getDriversAjax,divideRoute,reassignTransporters',
            'update_actions' => 'index,reasignRoute,getDriversAjax,divideRoute,reassignTransporters',
            'delete_actions' => 'index,reasignRoute,getDriversAjax,divideRoute,reassignTransporters',
        ]);
    }
}
