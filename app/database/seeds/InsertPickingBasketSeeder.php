<?php
use Illuminate\Database\Seeder;

class InsertPickingBasketSeeder extends Seeder
{
    public function run()
    {
        for($i = 1000001; $i <= 2005000; $i++ ) {
            $menuItem = new PickingBasket();
            $menuItem->code = $i;
            $menuItem->save();
        }
    }
}