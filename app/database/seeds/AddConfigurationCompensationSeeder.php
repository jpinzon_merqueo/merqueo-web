<?php


class AddConfigurationCompensationSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $configurarions = [
            'distribute_credit_late' => 0,
            'max_time_credit_late' => 10,
            'amount_credit_late' => 10000
        ];

        foreach ($configurarions as $key => $value) {
            Configuration::create([
                'city_id' => 0,
                'warehouse_id' => 0,
                'type' => 'credit_delivery_late',
                'key' =>$key,
                'value'=>$value
            ]);
        }

        $menuItem = MenuItem::where('controller_action', 'admin\AdminConfigController@general_configurations')->first();
        $menuItem->permission1_actions = 'change_compensation';
        $menuItem->permission1_description = 'Compensacion Automatica';
        $menuItem->save();
    }
}