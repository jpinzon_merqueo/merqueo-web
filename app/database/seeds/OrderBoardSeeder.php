<?php

use \Illuminate\Database\Seeder;

class OrderBoardSeeder extends Seeder
{

    public function run()
    {

        MenuItem::insert([

            'title' => 'Tablero de pedidos',
            'controller_action' => 'admin\inventory\picking\OrderBoardController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,getWarehousesAjax,showOrderBoard',
            'insert_actions' => 'index,getWarehousesAjax,showOrderBoard',
            'update_actions' => '',
            'delete_actions' => ''
        ]);

    }
}
