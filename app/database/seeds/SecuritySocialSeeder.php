<?php

use \Illuminate\Database\Seeder;

class SecuritySocialSeeder extends Seeder
{

    public function run()
    {
        SecuritySocial::insert([
            [
                'type' => 'EPS',
                'name' => 'SANITAS'
            ],
            [
                'type' => 'EPS',
                'name' => 'SURA'
            ],
            [
                'type' => 'EPS',
                'name' => 'COMPENSAR'
            ],
            [
                'type' => 'EPS',
                'name' => 'SALUD TOTAL'
            ],
            [
                'type' => 'EPS',
                'name' => 'COOMEVA'
            ],
            [
                'type' => 'EPS',
                'name' => 'NUEVA EPS'
            ],
            [
                'type' => 'EPS',
                'name' => 'FAMISANAR'
            ],
            [
                'type' => 'EPS',
                'name' => 'MUTUAL SER'
            ],
            [
                'type' => 'EPS',
                'name' => 'COOSALUD'
            ],
            [
                'type' => 'EPS',
                'name' => 'EMSSANAR'
            ],
            [
                'type' => 'AFP',
                'name' => 'Colpensiones'
            ],
            [
                'type' => 'AFP',
                'name' => 'Colfondos'
            ],
            [
                'type' => 'AFP',
                'name' => 'Protección'
            ],
            [
                'type' => 'AFP',
                'name' => 'Old Navi'
            ],
            [
                'type' => 'ARL',
                'name' => 'POSITIVA'
            ],
            [
                'type' => 'ARL',
                'name' => 'SURA'
            ],
            [
                'type' => 'ARL',
                'name' => 'COLMENA'
            ],
        ]);
        MenuItem::insert([

            'title' => 'Auxiliares',
            'controller_action' => 'admin\transporters\AssistantController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,ajax_drivers_per_transporter,delete_assistant',
            'insert_actions' => 'index,ajax_drivers_per_transporter,delete_assistant',
            'update_actions' => 'index,assistants,edit_assistant,save_assistant,delete_assistant',
            'delete_actions' => 'index,assistants,edit_assistant,save_assistant,delete_assistant'
        ]);

        $menu_items = MenuItem::where('controller_action', 'admin\AdminTransporterController@vehicles')->first();
        $menu_item = MenuItem::find($menu_items->id);
        $menu_item->access_actions .= ',ajax_drivers_per_transporter,ajax_vehicles_per_transporter';
        $menu_item->save();

    }
}
