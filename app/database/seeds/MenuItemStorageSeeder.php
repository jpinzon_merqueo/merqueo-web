<?php

use \Illuminate\Database\Seeder;

class MenuItemStorageSeeder extends Seeder
{

    public function run()
    {
        $menuItem = MenuItem::where('controller_action', 'admin\inventory\storage\StorageController@index')->first();
        $menuItem->insert_actions = 'add,searchProductsAjax,getProductsPositionAjax,save';
        $menuItem->update_actions = 'add,searchProductsAjax,getProductsPositionAjax,save';
        $menuItem->permission1_actions = 'importRemoveStorage';
        $menuItem->save();
    }
}
