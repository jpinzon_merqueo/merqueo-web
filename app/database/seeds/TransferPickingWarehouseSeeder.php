<?php

use \Illuminate\Database\Seeder;

class TransferPickingWarehouseSeeder extends Seeder
{

    public function run()
    {
        MenuItem::insert([
            'title' => 'Traslado de productos de picking entre bodegas',
            'controller_action' => 'admin\inventory\picking\TransferPickingWarehouseController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,getWarehousesAjax,getProductInfo',
            'insert_actions' => 'index,getWarehousesAjax,getProductInfo',
            'update_actions' => 'index,getWarehousesAjax,getProductInfo,transferProductsPicking',
            'delete_actions' => 'index,getWarehousesAjax,getProductInfo,transferProductsPicking'
        ]);

    }
}
