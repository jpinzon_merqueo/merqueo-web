<?php

use \Illuminate\Database\Seeder;

class TicketSolutionSeeder extends Seeder
{

    public function run()
    {
        \tickets\TicketSolution::insert([
            [
                'id' => \tickets\TicketSolution::COMPLAINT_ORDER,
                'name' => 'Pedido reclamo'
            ],
            [
                'id' => \tickets\TicketSolution::VOUCHER,
                'name' => 'Cupón'
            ],
            [
                'id' => \tickets\TicketSolution::CREDIT,
                'name' => 'Crédito'
            ],
            [
                'id' => \tickets\TicketSolution::FREE_DELIVERY,
                'name' => 'Próximo pedido con domicilio gratis'
            ],
            [
                'id' => \tickets\TicketSolution::OTHER,
                'name' => 'Otro'
            ],
        ]);
    }
}
