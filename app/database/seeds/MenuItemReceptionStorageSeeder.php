<?php

use \Illuminate\Database\Seeder;

class MenuItemReceptionStorageSeeder extends Seeder
{
    public function run()
    {
        $menuItem = MenuItem::where('controller_action', 'admin\inventory\storage\ReceptionStorageController@index')
            ->first();
        $menuItem->access_actions = 'index,getProviderOrderReceptionAjax,getProviderOrderReceptionProductDetailAjax';
        $menuItem->insert_actions = 'processStorageAjax';
        $menuItem->update_actions = 'processStorageAjax';
        $menuItem->save();
    }
}
