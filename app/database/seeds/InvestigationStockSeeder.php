<?php

use \Illuminate\Database\Seeder;

class InvestigationStockSeeder extends Seeder
{

    public function run()
    {
        MenuItem::insert([
            'title' => 'Investigación del inventario',
            'controller_action' => 'admin\inventory\investigation\InvestigationStockController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,getWarehousesAjax,showInvestigationStock',
            'insert_actions' => 'getInvestigationStock,updateInvestigationStock',
            'update_actions' => 'getInvestigationStock,updateInvestigationStock',
            'delete_actions' => 'getInvestigationStock,updateInvestigationStock',
        ]);
    }
}
