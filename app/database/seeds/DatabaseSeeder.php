<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call(CustomerServiceSeeder::class);
        $this->call(TicketSolutionSeeder::class);
        $this->call(TicketLayoutSeeder::class);
	}
}
