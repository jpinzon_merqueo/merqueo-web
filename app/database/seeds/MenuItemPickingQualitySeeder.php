<?php

use \Illuminate\Database\Seeder;

class MenuItemPickingQualitySeeder extends Seeder
{

    public function run()
    {
        $menuItem = MenuItem::where('controller_action', 'admin\AdminPickingQualityController@index')->first();
        $menuItem->access_actions = 'index,edit,getProductsAjax,validateProductAjax,updateValidation';
        $menuItem->save();
    }
}
