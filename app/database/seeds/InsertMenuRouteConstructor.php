<?php
use Illuminate\Database\Seeder;

class InsertMenuRouteConstructor extends Seeder
{
    public function run()
    {
        $menuItem = new MenuItem();
        $menuItem->title = 'Armador de Rutas';
        $menuItem->controller_action = 'admin\picking\RouteConstructorController@index';
        $menuItem->description = 'Módulo de armador de rutas';
        $menuItem->icon_class = 'fa-angle-double-right';
        $menuItem->access_actions = 'index,getDeliveryWindows';
        $menuItem->insert_actions = 'save';
        $menuItem->update_actions = 'save';
        $menuItem->delete_actions = '';
        $menuItem->status = 0;
        $menuItem->save();
    }
}