<?php

use \Illuminate\Database\Seeder;

class InsertTypifications extends Seeder
{
    public function run(){
        $typifications = [
            [ 'type' => 'Proveedor', 'typification' => 'Calidad / Avería'],
            [ 'type' => 'Proveedor', 'typification' => 'No tiene los mínimos de vida útil para recibir'],
            [ 'type' => 'Proveedor', 'typification' => 'Llegan con mas de 1a fecha de vencimiento'],
            [ 'type' => 'Proveedor', 'typification' => 'No llego el producto en físico'],
            [ 'type' => 'Merqueo', 'typification' => 'Capacidad en la bodega'],
            [ 'type' => 'Proveedor', 'typification' => 'Referencia errada'],
            [ 'type' => 'Proveedor', 'typification' => 'Diferencia de costo'],
            [ 'type' => 'Proveedor', 'typification' => 'Temperatura'],
            [ 'type' => 'Proveedor', 'typification' => 'El proveedor llegó fuera de franja'],
            [ 'type' => 'Proveedor', 'typification' => 'Referencias no solicitadas'],
            [ 'type' => 'Proveedor', 'typification' => 'No lee el código de barras']
        ];
        foreach ($typifications as $typification)
        {
            $providerOrderReturnTypification = new ProviderOrderRejectReason();
            $providerOrderReturnTypification->type = $typification['type'];
            $providerOrderReturnTypification->typification = $typification['typification'];
            $providerOrderReturnTypification->save();
        }
    }
}