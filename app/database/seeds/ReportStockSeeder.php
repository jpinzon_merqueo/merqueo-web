<?php

use \Illuminate\Database\Seeder;

class ReportStockSeeder extends Seeder
{
    /**
     * Agrega el menú de reportes para inventario
     */
    public function run()
    {
        MenuItem::insert([
            'title' => 'Reportes del inventario',
            'controller_action' => 'admin\inventory\reports\StockReportController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,reportProductExpired,reportProductExpiredAjax,reportProductDisabled,reportProductDisabledAjax,reportProductEnabled,reportProductEnabledAjax'
        ]);
    }
}
