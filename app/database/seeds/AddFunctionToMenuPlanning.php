<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 14/12/2018
 * Time: 9:52
 */

class AddFunctionToMenuPlanning extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $menuItem = MenuItem::where('controller_action', 'admin\AdminRouteController@index')->first();
        $menuItem->insert_actions = $menuItem->insert_actions.',update_dispatch_time';
        $menuItem->update_actions = $menuItem->update_actions.',update_dispatch_time';
        $menuItem->save();
    }
}