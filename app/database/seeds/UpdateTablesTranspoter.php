<?php

use \Illuminate\Database\Seeder;

class UpdateTablesTranspoter extends Seeder
{
    public function run()
    {

        /**
         * Traer auxiliares de conductores y
         * guardarlos en la tabla de auxiliares
         */

        $drivers = Driver::where('status', '1')->where('profile', 'Auxiliar')->get();
        foreach ($drivers as $driver) {
            $assistant = DriverAssistant::find($driver->id);
            if (is_null($assistant)) {
                $driver_aux = Vehicle::where('driver_aux_id', $driver->id)->orderBy('id')->get();
                $assistant = new DriverAssistant;
                $assistant->created_at = date("Y-m-d H:i:s");
                $assistant->id = $driver->id;
                $assistant->driver_id = count($driver_aux) > 0 ? $driver_aux[count($driver_aux) - 1]->id : null;
                $assistant->first_name = $driver->first_name;
                $assistant->last_name = $driver->last_name;
                $assistant->document_number = $driver->document_number;
                $assistant->phone = $driver->phone;
                $assistant->cellphone = $driver->cellphone;
                $assistant->has_cap = $driver->has_cap;
                $assistant->has_jacket = $driver->has_jacket;
                $assistant->has_tshirt = $driver->has_tshirt;
                $assistant->eps_id = $driver->eps_id;
                $assistant->afp_id = $driver->afp_id;
                $assistant->arl_id = $driver->arl_id;
                $assistant->save();
            }
        }

        /**
         * Actualizar la transportadora en el vehiculo
         * con el conductor asignado
         */

        $vehicles = Vehicle::with('driver')->get();
        foreach ($vehicles as $data) {
            if ($data->driver != '') {
                $vehicle = Vehicle::find($data->id);
                $vehicle->transporter_id = $data->driver->transporter_id;
                $vehicle->save();
            }
        }

        /**
         * Asociar el conductor al vehiculo
         * con el actual registro de la tabla
         * de vehiculos en vehicle_drivers
         */

        $vehicles = Vehicle::get();
        foreach ($vehicles as $data) {
            $vehicleDriver = VehicleDrivers::where('vehicle_id', $data->id)->where('driver_id', $data->driver_id)
                ->get();
            if (count($vehicleDriver) == 0) {
                $vehicleD = new VehicleDrivers;
                $vehicleD->vehicle_id = $data->id;
                $vehicleD->driver_id = $data->driver_id;
                $vehicleD->save();
            }
        }

        $routes = Routes::all();
        foreach ($routes as $data) {
            $order = Order::where('route_id', $data->id)->first();
            if (!is_null($order)) {
                $routeUp = Routes::find($data->id);
                $routeUp->vehicle_id = $order->vehicle_id;
                $routeUp->save();
            }
        }
    }
}