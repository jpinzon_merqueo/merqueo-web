<?php

use Illuminate\Database\Seeder;

/**
 * Class BancolombiaBines
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class BancolombiaBines extends Seeder
{
    public function run()
    {
        foreach ($this->getCreditCardBins() as $bin) {
            CreditCardBin::create([
                'bin' => $bin,
                'reference' => 'bancolombia',
            ]);
        }

        foreach ($this->getDebitCardBins() as $bin) {
            CreditCardBin::create([
                'bin' => $bin,
                'reference' => 'debito_bancolombia',
            ]);
        }
    }

    /**
     * @return array
     */
    private function getDebitCardBins()
    {
        return [
            '530691',
            '601660',
            '530695'
        ];
    }

    /**
     * @return array
     */
    private function getCreditCardBins()
    {
        return [
            '377813',
            '377814',
            '377815',
            '377816',
            '377843',
            '377844',
            '377845',
            '377847',
            '517640',
            '530371',
            '530372',
            '530693',
            '530694',
            '530696',
            '540691',
            '547062',
            '549157',
            '549158',
            '547480',
            '530373',
            '552588',
            '409983',
            '409984',
            '411054',
            '441119',
            '449188',
            '451307',
            '451308',
            '459426',
            '459425',
            '409985',
            '451309',
            '485946'
        ];
    }
}
