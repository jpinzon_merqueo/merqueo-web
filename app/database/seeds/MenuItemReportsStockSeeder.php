<?php

use \Illuminate\Database\Seeder;

class MenuItemReportsStockSeeder extends Seeder
{
    public function run()
    {
        $menuItem = MenuItem::where('controller_action', 'admin\inventory\reports\StockReportController@index')
            ->first();
        $menuItem->access_actions = 'index,reportProductExpired,reportProductExpiredAjax,reportProductDisabled,reportProductDisabledAjax,reportProductEnabled,reportProductEnabledAjax,reportProductEnabledExport,reportProductDisabledExport';
        $menuItem->save();
    }
}
