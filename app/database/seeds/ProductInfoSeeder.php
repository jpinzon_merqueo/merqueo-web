<?php

use \Illuminate\Database\Seeder;

class ProductInfoSeeder extends Seeder
{

    public function run()
    {

        MenuItem::insert([

            'title' => 'Información del producto',
            'controller_action' => 'admin\inventory\product\ProductInfoController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,getWarehousesAjax,showCounting,getProductInfo',
            'insert_actions' => 'index,getWarehousesAjax,showCounting,getProductInfo',
            'update_actions' => '',
            'delete_actions' => ''
        ]);

    }
}
