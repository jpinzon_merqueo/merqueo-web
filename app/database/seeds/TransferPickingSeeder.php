<?php

use \Illuminate\Database\Seeder;

class TransferPickingSeeder extends Seeder
{

    public function run()
    {

        MenuItem::insert([

            'title' => 'Traslado de productos de picking',
            'controller_action' => 'admin\inventory\massive_transfer_picking\MassiveTransferPickingController@index',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index,getWarehousesAjax,getProductsPickingAjax',
            'insert_actions' => 'index,getWarehousesAjax,getProductsPickingAjax',
            'update_actions' => 'index,getWarehousesAjax,getProductsPickingAjax,transferProductsPicking',
            'delete_actions' => 'index,getWarehousesAjax,getProductsPickingAjax,transferProductsPicking'
        ]);

    }
}
