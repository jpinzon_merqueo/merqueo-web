<?php

use \Illuminate\Database\Seeder;

class UpdateCanMixProducts extends Seeder
{
    public function run()
    {
        $products = Product::select('products.id', 'departments.name as department')
            ->join('store_products',  'products.id', '=', 'store_products.product_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->get();

        foreach ($products as $product){
            $p = Product::find($product->id);
            if($product->department == 'Mascotas'){
                $p->can_mix = 'Pets';
            }else if($product->department == 'Aseo del hogar'){
                $p->can_mix = 'Toilet';
            }else{
                $p->can_mix = 'Food';
            }
            $p->save();
        }
    }
}