<?php

use \Illuminate\Database\Seeder;

class DeliveryWindowsSeeder extends Seeder
{

    public function run()
    {
        MenuItem::insert([
            'title' => 'Franjas Horarias',
            'controller_action' => 'admin\config\DeliveryWindowController@index',
            'description' => 'Modulo para la configuración de las franjas horarias para entrega de pedidos',
            'icon_class' => 'fa-angle-double-right',
            'access_actions' => 'index',
            'insert_actions' => 'save,add',
            'update_actions' => 'save,edit',
            'delete_actions' => 'status,delete'
        ]);

        $menu_item = MenuItem::find(53);
        $menu_item->title = 'Slots por Bodega';
        $menu_item->controller_action = 'admin\AdminSlotWarehouseController@index';
        $menu_item->access_actions = 'index,getWarehousesByCityAjax';
        $menu_item->insert_actions = 'save';
        $menu_item->update_actions = 'save';
        $menu_item->delete_actions = null;
        $menu_item->save();

        $menu_item = MenuItem::find(37);
        $menu_item->access_actions = 'index,edit,departments,shelves,edit_department,edit_shelf,slots,form_update_slots';
        $menu_item->insert_actions = 'save,save_department,save_shelf,save_slot';
        $menu_item->update_actions = 'save,save_department,save_shelf,save_slot';
        $menu_item->delete_actions = 'delete,delete_department,delete_shelf,delete_slot';
        $menu_item->save();

        $menu_item = MenuItem::find(37);
        $menu_item->access_actions .= ',get_location_ajax,get_location_slot_ajax,delete_location_slot_ajax,save_location_slot_ajax,delivery_zone';
        $menu_item->insert_actions .= ',save_orders_per_delivery_window';
        $menu_item->save();

        $menu = Menu::find(1);
        $menu->menu_order = str_replace('AdminSlotZoneController', 'AdminSlotWarehouseController', $menu->menu_order);
        $menu->menu_order = str_replace('Slots por Zona', 'Slots por Bodega', $menu->menu_order);
        $menu->save();

        DB::statement("update `stores` set coverage = '5.161874485961748 -74.16038592001496,4.655622133693529 -74.51469500204621,4.329783601852945 -73.94615252157746,4.918374040865416 -73.78685076376496' where id = 63;");
        DB::statement("update `stores` set coverage = '6.398645457154388 -75.62714934226364,6.142011481381849 -75.66560149070114,6.114702622187352 -75.50355315085739,6.378174004867511 -75.47471403952926' where id = 64;");

        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('1','1','07:00:00','11:00:00','AM','7:00 am - 11:00 am','4900','1','2018-07-05 15:36:21','2018-11-23 12:46:05');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('2','1','11:00:00','14:00:00','AM y PM','11:00 am - 2:00 pm','4500','1','2018-07-05 15:50:15','2018-07-05 15:50:15');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('3','1','14:00:00','17:00:00','PM','2:00 pm - 5:00 pm','4900','1','2018-07-05 15:51:25','2018-11-23 12:54:45');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('4','1','18:00:00','22:00:00','PM','6:00 pm - 10:00 pm','3900','1','2018-07-05 15:54:52','2018-11-23 12:55:15');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('5','2','07:00:00','11:00:00','AM','7:00 am - 11:00 am','4900','1','2018-07-08 19:34:50','2018-11-23 14:15:34');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('6','2','11:00:00','14:00:00','AM y PM','11:00 am - 2:00 pm','4900','1','2018-07-08 19:42:39','2018-11-23 14:15:12');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('7','2','20:00:00','22:00:00','MD','8:00 pm - 10:00 pm','5900','1','2018-07-08 19:45:09','2018-11-23 14:13:07');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('8','2','14:00:00','17:00:00','PM','2:00 pm - 5:00 pm','4900','1','2018-07-08 19:48:46','2018-11-23 14:14:04');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('9','2','18:00:00','22:00:00','PM','6:00 pm - 10:00 pm','5500','1','2018-07-08 19:49:35','2018-11-23 14:14:44');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('10','2','18:00:00','20:00:00','MD','6:00 pm - 8:00 pm','5900','1','2018-07-08 19:50:22','2018-11-23 14:12:41');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('11','1','18:00:00','20:00:00','MD','6:00 pm - 8:00 pm','5900','1','2018-09-18 17:50:16','2018-11-23 14:11:18');");
        DB::statement("insert into `delivery_windows` (`id`, `city_id`, `hour_start`, `hour_end`, `shifts`, `delivery_window`, `delivery_amount`, `status`, `created_at`, `updated_at`) values('12','1','20:00:00','22:00:00','MD','8:00 pm - 10:00 pm','5900','1','2018-11-23 14:16:11','2018-11-23 14:16:11');");

        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('1','1','1','0','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('2','1','1','1','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('3','1','1','2','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('4','1','1','3','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('5','1','1','4','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('6','1','1','5','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('7','1','1','6','2500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('8','1','2','0','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('9','1','2','1','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('10','1','2','2','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('11','1','2','3','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('12','1','2','4','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('13','1','2','5','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('14','1','2','6','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('15','1','3','0','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('16','1','3','1','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('17','1','3','2','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('18','1','3','3','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('19','1','3','4','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('20','1','3','5','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('21','1','3','6','2000','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('22','1','4','0','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('23','1','4','1','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('24','1','4','2','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('25','1','4','3','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('26','1','4','4','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('27','1','4','5','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('28','1','4','6','3500','2018-11-27 18:22:02','2018-12-11 18:04:12')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('29','1','11','7','536','2018-11-27 18:22:02','2018-12-12 09:40:08')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('30','1','12','7','536','2018-11-27 18:22:02','2018-12-12 09:40:08')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('31','2','1','0','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('32','2','1','1','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('33','2','1','2','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('34','2','1','3','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('35','2','1','4','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('36','2','1','5','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('37','2','1','6','4250','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('38','2','2','0','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('39','2','2','1','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('40','2','2','2','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('41','2','2','3','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('42','2','2','4','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('43','2','2','5','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('44','2','2','6','3400','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('45','2','3','0','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('46','2','3','1','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('47','2','3','2','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('48','2','3','3','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('49','2','3','4','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('50','2','3','5','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('51','2','3','6','3825','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('52','2','4','0','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('53','2','4','1','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('54','2','4','2','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('55','2','4','3','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('56','2','4','4','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('57','2','4','5','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('58','2','4','6','5950','2018-11-27 18:26:39','2018-12-11 18:07:04')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('59','2','11','7','965','2018-11-27 18:26:39','2018-12-12 09:39:00')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('60','2','12','7','965','2018-11-27 18:26:39','2018-12-12 09:39:00')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('61','3','5','0','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('62','3','5','1','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('63','3','5','2','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('64','3','5','3','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('65','3','5','4','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('66','3','5','5','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('67','3','5','6','1200','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('68','3','6','0','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('69','3','6','1','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('70','3','6','2','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('71','3','6','3','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('72','3','6','4','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('73','3','6','5','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('74','3','6','6','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('75','3','8','0','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('76','3','8','1','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('77','3','8','2','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('78','3','8','3','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('79','3','8','4','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('80','3','8','5','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('81','3','8','6','900','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('82','3','9','0','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('83','3','9','1','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('84','3','9','2','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('85','3','9','3','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('86','3','9','4','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('87','3','9','5','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('88','3','9','6','1000','2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('89','3','10','7',NULL,'2018-12-12 09:43:40','2018-12-12 09:43:40')");
        DB::statement("insert into `slot_warehouses` (`id`, `warehouse_id`, `delivery_window_id`, `day`, `number_products`, `created_at`, `updated_at`) values('90','3','7','7',NULL,'2018-12-12 09:43:40','2018-12-12 09:43:40')");

        //actualizar delivery_window_id
        $orders = Order::whereNull('delivery_window_id')->where('date', '>', \Carbon\Carbon::now()->subDay(3)->toDateTimeString())->get();
        foreach ($orders as $order){
            if ($delivery_window = DeliveryWindow::where('delivery_window', $order->delivery_time)
                ->where('city_id', $order->store->city_id)
                ->first()) {
                $order->delivery_window_id = $delivery_window->id;
                $order->save();
            }
        }
    }
}
