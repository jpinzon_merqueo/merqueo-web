<?php
use Illuminate\Database\Seeder;

class InsertPickingGroup extends Seeder
{
    public function run()
    {
        $pickingGroup = new PickingGroup();
        $pickingGroup->warehouse_id = 4;
        $pickingGroup->picking_group = 'Rojo';
        $pickingGroup->color = '#ff0000';
        $pickingGroup->status = 1;
        $pickingGroup->save();
    }
}