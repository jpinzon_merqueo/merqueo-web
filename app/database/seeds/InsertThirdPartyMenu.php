<?php
use Illuminate\Database\Seeder;

class InsertThirdPartyMenu extends Seeder
{
    public function run()
    {
        $menuItem = new MenuItem();
        $menuItem->title = 'Precios de productos para terceros';
        $menuItem->controller_action = 'admin\catalog\ThirdPartyPriceController@index';
        $menuItem->description = 'Modulo para actualizar los precios de los procuctos en convenio con terceros';
        $menuItem->icon_class = 'fa-angle-double-right';
        $menuItem->access_actions = 'index,getDarkSupermarketWarehouses';
        $menuItem->insert_actions = 'updateThirdPartyPriceForm,updateThirdPartyPrice';
        $menuItem->update_actions = '';
        $menuItem->delete_actions = '';
        $menuItem->permission1_description = 'Añadir nuevos productos a la bodega';
        $menuItem->permission1_actions = 'addThirdPartyProducts,saveThirdPartyProducts';
        $menuItem->status = 0;
        $menuItem->save();
    }
}