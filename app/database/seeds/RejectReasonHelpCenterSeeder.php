<?php

use \Illuminate\Database\Seeder;

class RejectReasonHelpCenterSeeder extends Seeder
{

    public function run()
    {
        RejectReason::insert([
            [
                'status' => 'Cancelado',
                'attendant' => 'Help Center',
                'reason' => 'Modificación de Franja',
                'type' => 'admin',
                'is_storage' => 1,
                'is_active' => 0,
                'old_value' => null
            ],
            [
                'status' => 'Cancelado',
                'attendant' => 'Help Center',
                'reason' => 'No estaré',
                'type' => 'admin',
                'is_storage' => 1,
                'is_active' => 0,
                'old_value' => null
            ],
            [
                'status' => 'Cancelado',
                'attendant' => 'Help Center',
                'reason' => 'Modificación de Productos',
                'type' => 'admin',
                'is_storage' => 1,
                'is_active' => 0,
                'old_value' => null
            ]
        ]);
    }
}
