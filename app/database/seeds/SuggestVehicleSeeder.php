<?php

use \Illuminate\Database\Seeder;

class SuggestVehicleSeeder extends Seeder
{

    public function run()
    {
        $menu_items = MenuItem::where('controller_action', 'admin\AdminPlanningController@transport')->first();
        $menu_item = MenuItem::find($menu_items->id);
        $menu_item->access_actions .= ',get_info_route';
        $menu_item->save();
        $menu_items = MenuItem::where('controller_action', 'admin\AdminRouteController@index')->first();
        $menu_item = MenuItem::find($menu_items->id);
        $menu_item->access_actions .= ',update_vehicle_orders';
        $menu_item->save();

    }
}
