<?php

use \Illuminate\Database\Seeder;

class MenuItemProviderOrderDeliveryWindow extends Seeder
{
    public function run()
    {
        $menuItem = new MenuItem();
        $menuItem->title = 'Franjas horarias para ordenes de compra';
        $menuItem->controller_action = 'admin\provider\DeliveryWindowController@index';
        $menuItem->description = 'Módulo para administrar las franjas horarias para los proveedores';
        $menuItem->icon_class = 'fa-angle-double-right';
        $menuItem->access_actions = 'index,getDeliveryWindows';
        $menuItem->insert_actions = 'save';
        $menuItem->update_actions = 'save,status';
        $menuItem->delete_actions = 'delete';
        $menuItem->status = 0;
        $menuItem->save();
    }
}