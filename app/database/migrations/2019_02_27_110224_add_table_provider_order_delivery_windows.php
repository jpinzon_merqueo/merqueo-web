<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProviderOrderDeliveryWindows extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provider_order_delivery_windows', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('warehouse_id')->nullable(false);
            $table->time('hour_start')->nullable(false);
            $table->time('hour_end')->nullable(false);
            $table->string('delivery_window', 50)->nullable(false);
            $table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provider_order_delivery_windows');
	}

}
