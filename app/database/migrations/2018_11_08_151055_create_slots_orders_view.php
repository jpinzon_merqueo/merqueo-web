<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotsOrdersView extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::statement('DROP VIEW `slots_orders`');
		\DB::statement(
		    'CREATE VIEW `slots_products` AS (
                SELECT sw.warehouse_id                                              AS warehouse_id,
                       dw.id                                                        AS delivery_window_id,
                       sw.day                                                       AS \'day\',
                       SUM(IF(op.type = \'Agrupado\', opg.quantity, op.quantity))	AS current_products,
                       sw.number_products  							                AS limit_products
                FROM orders o
                       JOIN order_groups og ON o.group_id = og.id
                       JOIN slot_warehouses sw ON og.warehouse_id = sw.warehouse_id
                       JOIN delivery_windows dw ON dw.id = o.delivery_window_id AND sw.delivery_window_id = o.delivery_window_id
                       JOIN order_products op ON op.order_id = o.id
                       LEFT JOIN order_product_group opg ON op.id = opg.order_product_id
                WHERE o.status NOT IN (\'Dispatched\', \'Cancelled\', \'Delivered\')
                  AND o.delivery_date >= NOW()
                  AND (WEEKDAY(((o.delivery_date - INTERVAL 1 MINUTE) + INTERVAL 1 DAY)) = sw.day OR (dw.shifts LIKE \'%MD%\' AND sw.day = 7))
                GROUP BY sw.warehouse_id, sw.day, o.delivery_window_id
		    )'
        );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
