<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPickers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pickers', function(Blueprint $table)
		{
            DB::statement("ALTER TABLE pickers MODIFY COLUMN rol ENUM('Extra','De planta','Lider','Validador','Administrativo','Prueba','Empacador','Empacador Extra') CHARSET utf8 COLLATE utf8_general_ci NULL");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pickers', function(Blueprint $table)
		{
			//
		});
	}

}
