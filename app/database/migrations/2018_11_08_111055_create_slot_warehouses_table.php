<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotWarehousesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slot_warehouses', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('warehouse_id')->nullable(false)->index();
		    $table->unsignedInteger('delivery_window_id')->nullable(false)->index();
		    $table->tinyInteger('day')->nullable(false);
		    $table->unsignedInteger('number_products')->nullable();
		    $table->timestamps();
        });

		Schema::table('slot_warehouses', function (Blueprint $table) {
		    $table->index([
		        'warehouse_id',
                'delivery_window_id',
                'day'
            ]);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('slot_warehouses');
	}

}
