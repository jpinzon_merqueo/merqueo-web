<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('invoices', function (Blueprint $table) {
	        $table->increments('id');
	        $table->integer('number')->unique();
	        $table->integer('customer_invoice_id');
	        $table->dateTime('date');
	        $table->dateTime('expiration_date');
	        $table->string('payment_conditions');
	        $table->tinyInteger('status')->default(1);
	        $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('invoices');
	}

}
