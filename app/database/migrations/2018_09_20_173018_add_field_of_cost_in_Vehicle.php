<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOfCostInVehicle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->Float('base_cost')->nullable();
            $table->Float('variable_cost')->nullable();
            $table->Integer('base_order')->nullable();
            $table->Integer('payment_type')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('base_cost');
            $table->dropColumn('variable_cost');
            $table->dropColumn('base_order');
            $table->dropColumn('payment_type');
        });
    }

}
