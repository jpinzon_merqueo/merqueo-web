<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCodeSapDepartments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('departments', function(Blueprint $table) {
            $table->Integer('sap_item_group_code')->default(null)->nullable()->after('sort_order');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('departments', function(Blueprint $table) {
            $table->dropColumn('sap_item_group_code');
        });
	}

}
