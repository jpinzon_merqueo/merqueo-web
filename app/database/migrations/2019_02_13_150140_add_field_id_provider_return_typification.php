<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIdProviderReturnTypification extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_orders', function(Blueprint $table)
		{
			$table->integer('provider_order_reject_reason_id')->unsigned()->nullable()->after('admin_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_orders', function(Blueprint $table)
		{
			$table->dropColumn('provider_order_reject_reason_id');
		});
	}

}
