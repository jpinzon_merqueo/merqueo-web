<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryWindowFieldToOrderProvider extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_orders', function(Blueprint $table)
		{
			$table->unsignedInteger('provider_order_delivery_window_id')->nullable()->after('admin_id');
			$table->string('provider_order_delivery_window_text')->nullable()->after('provider_order_delivery_window_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_orders', function(Blueprint $table)
		{
			$table->dropColumn('provider_order_delivery_window_id');
			$table->dropColumn('provider_order_delivery_window_text');
		});
	}

}
