<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWtcodeFieldToProviderTaxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_taxes', function(Blueprint $table)
		{
		    $table->string('wtcode')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_taxes', function(Blueprint $table)
		{
		    $table->dropColumn('wtcode');
		});
	}

}
