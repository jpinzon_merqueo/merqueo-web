<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypificationToInvestigationStock extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('investigation_stocks', function(Blueprint $table)
		{
			$table->string('typification')->nullable()->default(null)->after('product_group');
			$table->Enum('status', ['Pendiente','Cerrado'])->default('Pendiente')->after('typification');
			$table->Integer('close_admin_id')->nullable()->after('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('investigation_stocks', function(Blueprint $table)
		{
			$table->dropColumn('typification');
			$table->dropColumn('status');
			$table->dropColumn('close_admin_id');
		});
	}

}
