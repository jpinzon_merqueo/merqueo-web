<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePickingGroup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('picking_group', function (Blueprint $table) {
            $table->integer('warehouse_id')->nullable(false)->unsigned()->after('id');
            $table->dropColumn('city_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('picking_group', function (Blueprint $table) {
            $table->dropColumn('warehouse_id');
        });
	}

}
