<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZoneDeliveryWindows extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('zone_delivery_windows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_id')->nullable(false)->unsigned()->index();
            $table->integer('delivery_window_id')->nullable(false)->unsigned()->index();
            $table->integer('maximum_orders_per_route')->nullable(false);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('zone_delivery_windows');
	}

}
