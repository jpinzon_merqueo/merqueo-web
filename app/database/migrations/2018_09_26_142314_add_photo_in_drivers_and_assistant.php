<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoInDriversAndAssistant extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('drivers', function (Blueprint $table) {
            $table->string('photo_url',200)->nullable()->default(null);
        });
        Schema::table('driver_assistants', function (Blueprint $table) {
            $table->string('photo_url',200)->nullable()->default(null);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('drivers', function (Blueprint $table) {
            $table->dropColumn('photo_url');
        });
        Schema::table('driver_assistants', function (Blueprint $table) {
            $table->dropColumn('photo_url');
        });
	}

}
