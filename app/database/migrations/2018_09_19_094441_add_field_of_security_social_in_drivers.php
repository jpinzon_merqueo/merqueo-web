<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOfSecuritySocialInDrivers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->integer('eps_id')->nullable()->index();
            $table->integer('afp_id')->nullable()->index();
            $table->integer('arl_id')->nullable()->index();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->dropColumn('eps_id');
            $table->dropColumn('afp_id');
            $table->dropColumn('arl_id');
        });
    }

}
