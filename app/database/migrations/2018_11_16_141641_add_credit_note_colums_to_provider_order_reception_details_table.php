<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditNoteColumsToProviderOrderReceptionDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_order_reception_details', function(Blueprint $table)
		{
			$table->string('reason_credit_note')->nullable()->after('given_quantity');
			$table->integer('quantity_credit_note')->nullable()->after('reason_credit_note');
			$table->double('consumption_tax_credit_note')->nullable()->after('quantity_credit_note');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_order_reception_details', function(Blueprint $table)
		{
            $table->dropColumn('reason_credit_note');
            $table->dropColumn('quantity_credit_note');
            $table->dropColumn('consumption_tax_credit_note');
		});
	}

}
