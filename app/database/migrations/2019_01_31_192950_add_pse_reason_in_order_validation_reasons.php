<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPseReasonInOrderValidationReasons extends Migration {

	/**
	 * Run the migrations.
	 * Agrega las tres nuevas tipificaciones de PSE
     * para validación de pedidos
     *
	 * @return void
	 */
	public function up()
	{
        $reasons = [
            [
                'reason' => 'Pagos mediante PSE fallidos',
                'status' => 1,
            ],
            [
                'reason' => 'Pagados con PSE y con faltantes',
                'status' => 1,
            ],
            [
                'reason' => 'Pagados con PSE y cancelados por cliente',
                'status' => 1,
            ],
        ];

        OrderValidationReason::insert($reasons);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
