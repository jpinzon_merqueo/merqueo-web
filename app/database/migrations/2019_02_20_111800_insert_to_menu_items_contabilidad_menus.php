<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertToMenuItemsContabilidadMenus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
        $menuItem = new MenuItem();
        $menuItem->title = 'Contabilidad';
        $menuItem->description = 'Menú padre';
        $menuItem->icon_class = 'fa-money';
        $menuItem->save();

        $menuItem = new MenuItem();
        $menuItem->title = 'Facturación Manual';
        $menuItem->controller_action = 'admin\AdminInvoiceController@index';
        $menuItem->description = 'Módulo de facturación manual';
        $menuItem->icon_class = 'fa-angle-double-right';
        $menuItem->access_actions = 'index,create,view,findInvoiceDetails,printPdf';
        $menuItem->insert_actions = 'store';
        $menuItem->update_actions = 'change_status';
        $menuItem->permission1_actions = 'change_status';
        $menuItem->permission1_description = 'Anular Facturas';
        $menuItem->save();

        $menuItem = new MenuItem();
        $menuItem->title = 'Plantillas Sap';
        $menuItem->controller_action = 'admin\AdminSapController@index';
        $menuItem->icon_class = 'fa-angle-double-right';
        $menuItem->access_actions = 'index,templates';
        $menuItem->save();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$menuItem = MenuItem::where('title', 'Contabilidad')->first();
		$menuItem->delete();

		$menuItem = MenuItem::where('title', 'Facturación Manual')->first();
		$menuItem->delete();

		$menuItem = MenuItem::where('title', 'Plantillas Sap')->first();
		$menuItem->delete();
	}

}
