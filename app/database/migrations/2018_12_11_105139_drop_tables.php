<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::drop('store_branch_slots');
        Schema::drop('store_branches');
        Schema::drop('store_location_slots');
        Schema::drop('store_locations');
        Schema::drop('supermarkets');
        Schema::drop('order_forecasts');
        Schema::drop('order_forecast_details');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
