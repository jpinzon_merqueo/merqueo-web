<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSlotWarehouseLog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('slot_warehouse_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->nullable(false)->unsigned()->index();
            $table->integer('slot_warehouse_id')->nullable(false)->unsigned()->index();
            $table->integer('number_products_original')->nullable(false);
            $table->integer('number_products_change')->nullable(false);
            $table->text('log');
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('slot_warehouse_log');
	}

}
