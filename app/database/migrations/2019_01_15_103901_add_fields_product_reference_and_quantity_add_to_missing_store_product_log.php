<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsProductReferenceAndQuantityAddToMissingStoreProductLog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('missing_store_product_log', function (Blueprint $table) {
            $table->string('reference')->nullable(false)->after('order_id');
            $table->string('quantity_add')->nullable(false)->after('reference');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('missing_store_product_log', function (Blueprint $table) {
            $table->dropColumn('reference');
            $table->dropColumn('quantity_add');
        });
	}

}
