<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdDivideTypeInRouteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->Integer('parent_id')->nullable()->index()->after('admin_id');
            $table->enum('route_division',['Apoyo','Canguro'])->nullable()->after('cost_date');
            $table->String('route_division_reason')->nullable()->after('route_division');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('route_division');
            $table->dropColumn('route_division_reason');
        });
    }

}
