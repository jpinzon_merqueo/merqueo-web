<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableToInvestigationStock extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('investigation_stock', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE investigation_stocks MODIFY COLUMN module enum('Pedido','Recibo','Transporte','Alistamiento','Calidad','Conteo por Posición','Devolución') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('investigation_stock', function(Blueprint $table)
		{
			//
		});
	}

}
