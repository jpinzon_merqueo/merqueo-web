<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderGrupsWhoReceives extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_groups', function (Blueprint $table) {
            $table->string('reception_user_name', 150)->nullable()->after('user_comments');
            $table->string('reception_user_phone', 20)->nullable()->after('reception_user_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('reception_user_name');
            $table->dropColumn('reception_user_phone');
        });
    }

}
