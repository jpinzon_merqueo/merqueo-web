<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUserScoreSourceInOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('orders', function (Blueprint $table) {
            $table->Enum('user_score_source', ['App','Web'])->default(null)->nullable()->after('user_score');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('user_score_source');
        });
	}

}
