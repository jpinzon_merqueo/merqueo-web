<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_details', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('invoice_id');
		    $table->integer('product_invoice_id');
		    $table->string('description');
		    $table->integer('quantity');
		    $table->double('price');
		    $table->integer('iva');
		    $table->double('price_iva');
		    $table->double('price_tax_fuente');
		    $table->double('price_tax_ica');
		    $table->double('price_tax_iva');
		    $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('invoice_details');
	}

}
