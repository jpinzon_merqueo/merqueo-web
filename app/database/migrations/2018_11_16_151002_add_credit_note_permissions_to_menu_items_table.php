<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditNotePermissionsToMenuItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menu_items', function(Blueprint $table)
		{
            DB::statement("UPDATE menu_items 
            SET access_actions = CONCAT(access_actions, ',', 'get_products_credit_note'),
            update_actions = CONCAT(update_actions, ',', 'reception_update_credit_note'),
            permission1_actions = CONCAT(permission1_actions, ',', 'reception_update_credit_note'),
            permission3_actions = CONCAT(permission3_actions, ',', 'reception_update_credit_note')
            WHERE controller_action = 'admin\\AdminMovementController@index_reception';");
            DB::statement("UPDATE menu_items
            SET access_actions = CONCAT(access_actions, ',', 'get_products_credit_note'),
            update_actions = CONCAT(update_actions, ',', 'order_update_credit_note'),
            permission2_actions = CONCAT(permission2_actions, ',', 'order_update_credit_note')
            WHERE controller_action = 'admin\\AdminOrderStorageController@index';");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
