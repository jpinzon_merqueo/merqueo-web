<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldModuleInInvetigationStocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('investigation_stocks', function (Blueprint $table) {
            $table->Integer('entity_id')->index()->default(null)->nullable()->after('warehouse_id');
            $table->Integer('admin_id')->index()->default(null)->nullable()->after('entity_id');
            $table->Enum('module',['Pedido','Recibo','Transporte','Actualizacion Stock'])->default(null)->nullable()->after('quantity');
            $table->Boolean('product_group')->default(0)->after('module');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_stocks', function (Blueprint $table) {
            $table->dropColumn('entity_id');
            $table->dropColumn('module');
            $table->dropColumn('product_group');
            $table->dropColumn('admin_id');
        });
    }

}
