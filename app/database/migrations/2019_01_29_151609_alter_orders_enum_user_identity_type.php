<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersEnumUserIdentityType extends Migration {

	/**
	 * Agrega un nuevo ítem (Cédula extranjería)
	 * Al campo enumerado user_identity_type
     *
	 * @return void
	 */
	public function up()
	{
        Schema::table('orders', function (Blueprint $table) {
            DB::statement("ALTER TABLE `orders` CHANGE `user_identity_type` `user_identity_type` ENUM('Cédula','NIT','Cédula extranjería') CHARSET utf8 COLLATE utf8_unicode_ci NULL;");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('orders', function (Blueprint $table) {
            DB::statement("ALTER TABLE `orders` CHANGE `user_identity_type` `user_identity_type` ENUM('Cédula','NIT') CHARSET utf8 COLLATE utf8_unicode_ci NULL;");
        });
	}

}
