<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProviderReturnTypifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provider_order_reject_reason', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('type', ['Merqueo', 'Proveedor']);
			$table->text('typification');
			$table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
		Schema::table('provider_order_reception_validates', function (Blueprint $table)
        {
            DB::statement("ALTER TABLE provider_order_reception_validates CHANGE COLUMN tipification tipification TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER quantity_difference;");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provider_order_reject_reason');
        Schema::table('provider_order_reception_validates', function (Blueprint $table)
        {
            DB::statement("ALTER TABLE provider_order_reception_validates CHANGE COLUMN tipification tipification ENUM('Error de auxiliar','Error de proveedor','Error de recibo virtual') NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER quantity_difference;");
        });
	}

}
