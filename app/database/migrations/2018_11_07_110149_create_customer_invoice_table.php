<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerInvoiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_invoice', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('document_type');
		    $table->string('document_number');
		    $table->string('fullname');
		    $table->string('phone');
		    $table->string('address');
		    $table->string('email')->nullable();
		    $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_invoice');
	}

}
