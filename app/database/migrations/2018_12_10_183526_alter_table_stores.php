<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('delivery_order_amount');
            $table->dropColumn('delivery_order_increase_minimum_amount');
            $table->dropColumn('delivery_order_increase_percentage_amount');
            $table->dropColumn('delivery_order_increase_amount');
            $table->dropColumn('delivery_order_increase_amount_today');
            $table->dropColumn('dispatched_time_minutes');
            $table->dropColumn('before_close_minutes');
            $table->dropColumn('immediate_delivery');
            $table->dropColumn('auto_allocate_orders');
            $table->dropColumn('show_orders_app');
            $table->dropColumn('delivery_order_original_amount');
            $table->text('coverage')->after('longitude');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
