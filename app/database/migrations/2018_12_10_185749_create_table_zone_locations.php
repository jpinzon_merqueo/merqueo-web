<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZoneLocations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zone_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_id')->nullable(false)->unsigned()->index();
            $table->string('name', 60)->nullable(false);
            $table->text('delivery_zone')->nullable(false);
            $table->tinyInteger('status')->default(1);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('zone_locations');
	}

}
