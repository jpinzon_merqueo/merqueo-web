<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccessActionsOrderValidationMenuItems extends Migration {

	/**
	 * Run the migrations.
	 * Agrega el permiso de acceder a la función management_sac
     * del menú items Validaciones de pedidos
	 * @return void
	 */
	 public function up()
	 {
        MenuItem::where('title', 'Validaciones de pedidos')
            ->update(['access_actions' => 'index,search,management_sac']);
	 }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
     */
	 public function down()
     {
        MenuItem::where('title', 'Validaciones de pedidos')
            ->update(['access_actions' => 'index,search']);
	 }

}
