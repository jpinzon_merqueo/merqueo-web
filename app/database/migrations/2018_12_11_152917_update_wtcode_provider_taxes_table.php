<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWtcodeProviderTaxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_taxes', function(Blueprint $table)
		{
		    $fieldsUpdate = [
		        'wtcode' => ['RC25', 'RC35', 'RC15', 'RI9', 'RI69', 'RI4', 'RI11',
                    'RI13', 'RI7M', 'RI6', 'RI10', 'RI3M'],
                'account' => ['2365400100', '2365400200', '2365400300', '2368050100',
                    '2368050200', '2368050300', '2368050400', '2368050500', '2368050600',
                    '2368050700', '2368050800', '2368050900']
            ];
		    $lenghtUpdate = count($fieldsUpdate['wtcode']);
		    for ($i = 0; $i < $lenghtUpdate; $i++) {
                DB::statement("UPDATE provider_taxes 
                                    SET wtcode = '{$fieldsUpdate['wtcode'][$i]}'
                                    WHERE account = '{$fieldsUpdate['account'][$i]}';");
            }
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
