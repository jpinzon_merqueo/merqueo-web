<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditNoteColumsToProviderOrderReceptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_order_receptions', function(Blueprint $table)
		{
			$table->integer('credit_note_number')->nullable()->after('rete_fuente_amount');
			$table->dateTime('credit_note_date')->nullable()->after('credit_note_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_order_receptions', function(Blueprint $table)
		{
			$table->dropColumn('credit_note_number');
            $table->dropColumn('credit_note_date');
		});
	}

}
