<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeUserAndOriginClickDelivery extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `type` `type` ENUM('merqueo.com','callcenter','domicilios.com','uberEats','convenio-domicilios.com') NOT NULL DEFAULT 'merqueo.com' COLLATE 'utf8_unicode_ci';");
        });
        Schema::table('order_groups', function (Blueprint $table) {
            DB::statement("ALTER TABLE `order_groups` CHANGE COLUMN `source` `source` enum('Web','Device','Callcenter','Web Service','Reclamo','domicilios.com','uberEats','convenio-domicilios.com') COLLATE 'utf8_general_ci';");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `type` `type` ENUM('merqueo.com','callcenter','domicilios.com','uberEats') NOT NULL DEFAULT 'merqueo.com' COLLATE 'utf8_unicode_ci';");
        });
        Schema::table('order_groups', function (Blueprint $table) {
            DB::statement("ALTER TABLE `order_groups` CHANGE COLUMN `source` `source` enum('Web','Device','Callcenter','Web Service','Reclamo','domicilios.com','uberEats') COLLATE 'utf8_general_ci';");
        });
	}

}
/*
 * orden producto valor fecha tienda
 *
 */