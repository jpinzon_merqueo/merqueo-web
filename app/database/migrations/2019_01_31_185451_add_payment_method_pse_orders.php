<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentMethodPseOrders extends Migration {

	/**
	 * Run the migrations.
	 * Agrega al enumerado payment_method Débito - PSE
     * Agrega al enumerado cc_payment_status falliida y pendiente
     *
	 * @return void
	 */
	public function up()
	{
        Schema::table('orders', function (Blueprint $table) {
            DB::statement("ALTER TABLE `orders` CHANGE `payment_method` `payment_method` ENUM('Tarjeta de crédito','Efectivo','Datáfono','Efectivo y datáfono','Débito - PSE') CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        });

        Schema::table('order_payments', function (Blueprint $table) {
            DB::statement("ALTER TABLE `order_payments` CHANGE `cc_payment_status` `cc_payment_status` ENUM('Aprobada','Declinada','Expirada','Fallida','Pendiente') CHARSET utf8 COLLATE utf8_general_ci NOT NULL;");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('orders', function (Blueprint $table) {
            DB::statement("ALTER TABLE `orders` CHANGE `payment_method` `payment_method` ENUM('Tarjeta de crédito','Efectivo','Datáfono','Efectivo y datáfono') CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        });

        Schema::table('order_payments', function (Blueprint $table) {
            DB::statement("ALTER TABLE `order_payments` CHANGE `cc_payment_status` `cc_payment_status` ENUM('Aprobada','Declinada','Expirada') CHARSET utf8 COLLATE utf8_general_ci NOT NULL;");
        });
	}

}
