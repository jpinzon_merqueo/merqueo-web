<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddNewSiteBannerImage
 */
class AddNewSiteBannerImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\Schema::table('banners', function (Blueprint $blueprint) {
            $blueprint->string('image_web_url_temp')
                ->after('image_web_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\Schema::table('banners', function (Blueprint $blueprint) {
            $blueprint->dropColumn('image_web_url_temp');
        });
    }
}
