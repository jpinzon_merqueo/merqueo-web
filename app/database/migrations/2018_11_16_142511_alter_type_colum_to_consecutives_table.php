<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypeColumToConsecutivesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('consecutives', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE `consecutives` CHANGE `type` `type` ENUM('invoice','provider_order_reception_e','provider_order_reception_p','provider_order_reception_p_2','provider_order_reception_p_3','provider_order_reception_p_4','provider_order_reception_p_5','credit_note_purchase','credit_note_sale') CHARSET utf8 COLLATE utf8_general_ci NULL;");
			DB::table('consecutives')->insert([
			    'type' => 'credit_note_purchase',
                'city_id' => 1,
                'consecutive' => 0
            ]);
			DB::table('consecutives')->insert([
                'type' => 'credit_note_sale',
                'city_id' => 1,
                'consecutive' => 0
            ]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('consecutives', function(Blueprint $table)
		{
            DB::table('consecutives')->where('type', 'credit_note_purchase')->delete();
            DB::table('consecutives')->where('type', 'credit_note_sale')->delete();
            DB::statement("ALTER TABLE `consecutives` CHANGE `type` `type` ENUM('invoice','provider_order_reception_e','provider_order_reception_p','provider_order_reception_p_2','provider_order_reception_p_3','provider_order_reception_p_4','provider_order_reception_p_5') CHARSET utf8 COLLATE utf8_general_ci NULL;");
		});
	}

}
