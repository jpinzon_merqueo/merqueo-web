<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingProductsFieldToProviderOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_orders', function(Blueprint $table)
		{
			$table->tinyInteger('missing_products')
                ->nullable()->default(0)->after('reject_comments');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_orders', function(Blueprint $table)
		{
			$table->dropColumn('missing_products');
		});
	}

}
