<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationStocksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('investigation_stocks')) {
            Schema::create('investigation_stocks', function ($table) {
                $table->increments('id');
                $table->integer('inventory_counting_position_id')->unsigned();
                $table->integer('store_product_id')->unsigned();
                $table->integer('warehouse_id')->unsigned();
                $table->enum('storage', array('Almacenamiento', 'Alistamiento'));
                $table->integer('position')->unsigned();
                $table->string('height_position', 1);
                $table->integer('quantity');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigation_stocks');
    }
}
