<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWtcodeFieldsToProviderOrderReceptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_order_receptions', function(Blueprint $table)
		{
		    $table->string('wtcode_ica')->nullable()->after('voucher_date');
		    $table->string('wtcode_fuente')->nullable()->after('rete_ica_amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_order_receptions', function(Blueprint $table)
		{
            $table->dropColumn('wtcode_ica');
            $table->dropColumn('wtcode_fuente');
		});
	}

}
