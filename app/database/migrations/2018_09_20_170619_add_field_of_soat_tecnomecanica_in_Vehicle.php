<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOfSoatTecnomecanicaInVehicle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->Date('due_date_soat')->nullable();
            $table->Date('due_date_rtm')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('due_date_soat');
            $table->dropColumn('due_date_rtm');
        });
    }

}
