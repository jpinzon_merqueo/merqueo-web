<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Añadir tipo de usuario uberEats
 * @author Fabián Salinas <esalinas@merqueo.com>
 */
class AddUberEatsSource extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `type` `type` ENUM('merqueo.com','callcenter','domicilios.com','uberEats') NOT NULL DEFAULT 'merqueo.com' COLLATE 'utf8_unicode_ci';");
        });
        Schema::table('order_groups', function (Blueprint $table) {
            DB::statement("ALTER TABLE `order_groups` CHANGE COLUMN `source` `source` enum('Web','Device','Callcenter','Web Service','Reclamo','domicilios.com','uberEats') COLLATE 'utf8_general_ci';");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `type` `type` ENUM('merqueo.com','callcenter','domicilios.com') NOT NULL DEFAULT 'merqueo.com' COLLATE 'utf8_unicode_ci';");
        });
        Schema::table('order_groups', function (Blueprint $table) {
            DB::statement("ALTER TABLE `order_groups` CHANGE COLUMN `source` `source` enum('Web','Device','Callcenter','Web Service','Reclamo') COLLATE 'utf8_general_ci';");
        });
	}

}
