<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttendantHelpCenterRejectReasons extends Migration
{

    public function up()
    {
        DB::statement("ALTER TABLE reject_reasons MODIFY COLUMN attendant ENUM(
            'Datos Errados',
            'Operación Merqueo',
            'Políticas Merqueo',
            'Prueba',
            'Usuario',
            'general',
            'null',
            'payu',
            'Help Center'
        )");
    }
}
