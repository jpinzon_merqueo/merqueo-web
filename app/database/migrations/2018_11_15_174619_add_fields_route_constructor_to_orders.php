<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsRouteConstructorToOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->timestamp('baskets_to_route_date')
                ->nullable()
                ->default(null)
                ->after('planning_date');
			$table->string('picking_sequence_letter', '6')
                ->nullable()
                ->default(null)
                ->after('planning_distance');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropColumn('baskets_to_route_date');
			$table->dropColumn('picking_sequence_letter');
		});
	}

}
