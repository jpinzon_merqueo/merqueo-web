<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrderPayments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_payments', function(Blueprint $table)
		{
			$table->integer('amount')->unsigned()->after('cc_payment_status')->nullable();
			$table->tinyInteger('is_precharge')->unsigned()->after('amount')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_payments', function(Blueprint $table)
		{
			$table->dropColumn('amount');
			$table->dropColumn('is_precharge');
		});
	}

}
