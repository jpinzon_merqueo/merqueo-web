<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZoneLocationSlots extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('zone_location_slots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_location_id')->nullable(false)->unsigned()->index();
            $table->integer('delivery_window_id')->nullable(false)->unsigned()->index();
            $table->integer('day')->nullable(false);
            $table->tinyInteger('status')->default(1);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('zone_location_slots');
	}

}
