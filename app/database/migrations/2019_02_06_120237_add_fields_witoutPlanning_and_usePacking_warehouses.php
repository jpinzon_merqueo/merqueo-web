<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsWitoutPlanningAndUsePackingWarehouses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('warehouses', function (Blueprint $table) {
            $table->tinyInteger('without_planning')->defaut(0)->after('status');
            $table->tinyInteger('use_packing')->defaut(0)->after('without_planning');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouses', function (Blueprint $table) {
            $table->dropColumn('without_planning');
            $table->dropColumn('use_packing');
        });
    }

}
