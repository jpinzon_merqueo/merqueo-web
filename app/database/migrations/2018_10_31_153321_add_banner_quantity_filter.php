<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddBannerQuantityFilter
 */
class AddBannerQuantityFilter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->integer('user_orders_quantity', false, true)
                ->nullable()
                ->after('position');

            $table->string('user_orders_quantity_condition', 5)
                ->nullable()
                ->after('position');
        });

        Banner::where('is_for_first_order', 1)
            ->update([
                'user_orders_quantity_condition' => '=',
                'user_orders_quantity' => 0,
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Banner::where('user_orders_quantity_condition', '=')
            ->where('user_orders_quantity', 0)
            ->update(['is_for_first_order' => 1]);

        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('user_orders_quantity');
            $table->dropColumn('user_orders_quantity_condition');
        });
    }
}
