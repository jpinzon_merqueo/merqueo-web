<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditNoteColumsToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
		    $table->integer('credit_note_number')->nullable()->after('invoice_cancelled');
		    $table->dateTime('credit_note_date')->nullable()->after('credit_note_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropColumn('credit_note_number');
			$table->dropColumn('credit_note_date');
		});
	}

}
