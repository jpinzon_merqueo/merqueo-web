<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsPumInProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function(Blueprint $table) {
            $table->float('net_quantity_pum')->nullable()->after('unit');
            $table->enum('unit_pum', ['Gramo', 'Metro', 'Mililitro', 'Unidad'])
                ->nullable()->after('net_quantity_pum');
        });

        if (Schema::hasColumn('products', 'net_quantity_pum')) {
            DB::statement("UPDATE `products` SET `net_quantity_pum` = IF(`type` = 'Simple', `quantity`, NULL)");
            DB::statement("UPDATE `products` SET `unit_pum` = 'Unidad' WHERE `type` = 'Simple' AND unit = 'Und' OR unit = 'Recetario' OR unit = 'Obsequio' OR unit = 'Und|' OR unit = 'surtido' OR unit = 'Tabletas' OR unit = 'Unidades' OR unit = 'Unidad ' OR unit = 'Pares' OR unit = 'Unid' OR unit = 'Undx' OR unit = 'Pzs' OR unit = 'Tab' OR unit = 'Caja' OR unit = 'Cap' OR unit = 'RPTO'");
            DB::statement("UPDATE `products` SET `unit_pum` = 'Gramo' WHERE `type` = 'Simple' AND unit = 'Gr. ' OR unit = 'Oz' OR unit = 'Lb' OR unit = 'Lbs' OR unit = 'Mg' OR unit = 'G' OR unit = 'Kg' OR unit = 'Gr'");
            DB::statement("UPDATE `products` SET `unit_pum` = 'Mililitro' WHERE `type` = 'Simple' AND unit = 'CC' OR unit = 'L' OR unit = 'Ml' OR unit = 'Lt' ");
            DB::statement("UPDATE `products` SET `unit_pum` = 'Metro' WHERE `type` = 'Simple' AND unit = 'Mts' OR unit = 'Mt' OR unit = 'M' OR unit = 'Cm' ");
            DB::statement("UPDATE `products` SET `net_quantity_pum` = IF(`type` = 'Simple', `quantity`, NULL)");
            DB::statement("UPDATE `products` SET `net_quantity_pum` = `quantity` * 1000 WHERE `type` = 'Simple' AND unit = 'Kg' OR unit = 'L' OR unit = 'Lt' OR unit = 'CC' ");
            DB::statement("UPDATE `products` SET `net_quantity_pum` = `quantity` / 1000 WHERE `type` = 'Simple' AND unit = 'Mg'");
            DB::statement("UPDATE `products` SET `net_quantity_pum` = `quantity` * 453.592 WHERE `type` = 'Simple' AND unit = 'Lb' OR unit = 'Lbs' ");
            DB::statement("UPDATE `products` SET `net_quantity_pum` = `quantity` * 28.35 WHERE `type` = 'Simple' AND unit = 'Oz' ");
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropColumn('net_quantity_pum');
			$table->dropColumn('unit_pum');
		});
	}

}
