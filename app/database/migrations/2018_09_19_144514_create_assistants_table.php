<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('driver_assistants', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('driver_id')->nullable();
            $table->string('document_number',10);
            $table->string('first_name',45);
            $table->string('last_name',45);
            $table->string('phone',20)->nullable();
            $table->string('cellphone',20);
            $table->tinyInteger('has_cap')->nullable()->default(1);
            $table->tinyInteger('has_jacket')->nullable()->default(1);
            $table->tinyInteger('has_tshirt')->nullable()->default(1);
            $table->boolean('status')->default(true);
            $table->integer('eps_id')->nullable();
            $table->integer('afp_id')->nullable();
            $table->integer('arl_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('driver_assistants');
    }

}
