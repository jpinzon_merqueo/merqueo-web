<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionReceptionUpdateRealProviderToMenuItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$menuItem = MenuItem::where('controller_action', 'admin\AdminMovementController@index_reception')->first();
		$menuItem->update_actions = $menuItem->update_actions . ',reception_update_real_provider';
		$menuItem->permission3_actions = $menuItem->permission3_actions . ',reception_update_real_provider';
		$menuItem->save();
	}

}
