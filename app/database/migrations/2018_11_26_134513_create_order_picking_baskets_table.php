<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPickingBasketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('order_picking_baskets', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('order_id')->unsigned()->index();
            $table->string('type');
            $table->string('code_basket');
            $table->tinyInteger('validated')->default(0);
            $table->tinyInteger('missing')->default(0);
            $table->tinyInteger('added')->default(0);
            $table->dateTime('check_transporter_date');
            $table->dateTime('check_arrival_date');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('order_picking_baskets');
	}

}
