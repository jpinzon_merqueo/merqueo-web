<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDriversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('vehicle_drivers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('vehicle_id')->unsigned()->index();
            $table->Integer('driver_id')->unsigned()->index();
            $table->timestamps();
            $table->unique(['vehicle_id','driver_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_drivers');
    }

}
