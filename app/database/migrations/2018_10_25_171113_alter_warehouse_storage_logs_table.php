<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWarehouseStorageLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('warehouse_storage_log')) {
            DB::statement("ALTER TABLE warehouse_storage_log MODIFY COLUMN module enum('Almacenamiento','Almacenamiento de Recibos','Merma','Conteo','Conteo Masivo','Conteo por Posiciones','Traslado de productos', 'Traslado de posiciones') CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
