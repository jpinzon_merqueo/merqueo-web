<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableDeliveryWindows extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_windows', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('city_id')->nullable(false);
		    $table->time('hour_start')->nullable(false);
		    $table->time('hour_end')->nullable(false);
		    $table->enum('shifts', ['AM','PM','AM y PM','MD']);
		    $table->string('delivery_window', 50)->nullable(false);
		    $table->unsignedInteger('delivery_amount')->nullable(false);
		    $table->tinyInteger('status')->default(1);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \DB::statement('DROP TABLE delivery_windows');
	}
}
