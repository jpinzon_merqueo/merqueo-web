<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('warehouse_carts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('warehouse_id')->unsigned()->index();
            $table->Integer('cart_reference')->unsigned()->index();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('warehouse_carts');
	}

}
