<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThirdPartyPriceField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('store_products', function(Blueprint $table)
		{
			$table->integer('third_party_price')->unsigned()->nullable()->after('special_price');
            $table->integer('third_party_cost')->unsigned()->nullable()->after('third_party_price');
		});
        Schema::table('user', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `type` `type` ENUM('merqueo.com','callcenter','domicilios.com') NOT NULL DEFAULT 'merqueo.com' COLLATE 'utf8_unicode_ci';");
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('store_products', function(Blueprint $table)
		{
			$table->dropColumn('third_party_price');
			$table->dropColumn('third_party_cost');
		});
        Schema::table('user', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE COLUMN `type` `type` ENUM('merqueo.com','callcenter') NOT NULL DEFAULT 'merqueo.com' COLLATE 'utf8_unicode_ci';");
        });
	}

}
