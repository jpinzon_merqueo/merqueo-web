<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRealProviderIdFieldToProviderOrderReceptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provider_order_receptions', function(Blueprint $table)
		{
			$table->integer('real_provider_id')
                ->nullable()->after('provider_order_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provider_order_receptions', function(Blueprint $table)
		{
			$table->dropColumn('real_provider_id');
		});
	}

}
