<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderGroupsIdStratum extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_groups', function(Blueprint $table)
		{
			$table->integer('economic_stratum_id')->unsigned()->after('user_city_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_groups', function(Blueprint $table)
		{
			//
		});
	}

}
