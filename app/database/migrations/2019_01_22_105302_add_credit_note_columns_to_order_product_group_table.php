<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditNoteColumnsToOrderProductGroupTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_product_group', function(Blueprint $table)
        {
            $table->string('reason_credit_note')->nullable()->after('reference_returned');
            $table->integer('quantity_credit_note')->nullable()->after('reason_credit_note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product_group', function(Blueprint $table)
        {
            $table->dropColumn('reason_credit_note');
            $table->dropColumn('quantity_credit_note');
        });
    }

}
