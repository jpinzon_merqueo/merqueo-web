<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransportersIdInVehicle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->Integer('transporter_id')->unsigned()->index()->nullable();
            $table->Integer('capacity_containers')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('transporter_id');
            $table->dropColumn('capacity_containers');
        });
    }
}
