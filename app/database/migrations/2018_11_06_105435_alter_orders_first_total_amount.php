<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersFirstTotalAmount extends Migration {

	public function up()
	{
        Schema::table('orders', function(Blueprint $table)
        {
            $table->double('first_total_amount')->nullable()->after('discount_amount');
        });
	}

	public function down()
	{
        Schema::table('orders', function(Blueprint $table)
        {
            $table->dropColumn('first_total_amount');
        });
	}

}
