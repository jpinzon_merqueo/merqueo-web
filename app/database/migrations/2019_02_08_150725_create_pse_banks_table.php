<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePseBanksTable.
 * 
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
class CreatePseBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pse_banks', function ($table) {
            $table->string('id', 50)->unique()->index();
            $table->string('description', 50);
			$table->string('pseCode', 10);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pse_banks');
    }

}
