<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditNoteColumsToOrderProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_products', function(Blueprint $table)
		{
			$table->string('reason_credit_note')->nullable()->after('product_comment');
			$table->integer('quantity_credit_note')->nullable()->after('reason_credit_note');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_products', function(Blueprint $table)
		{
		    $table->dropColumn('reason_credit_note');
		    $table->dropColumn('quantity_credit_note');
		});
	}

}
