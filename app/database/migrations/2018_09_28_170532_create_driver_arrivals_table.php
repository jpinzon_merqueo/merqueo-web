<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverArrivalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('driver_arrivals', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('vehicle_id')->unsigned()->index();
            $table->Integer('driver_id')->unsigned()->index();
            $table->Integer('route_id')->unsigned()->index();
            $table->DateTime('arrival_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('driver_arrivals');
    }

}
