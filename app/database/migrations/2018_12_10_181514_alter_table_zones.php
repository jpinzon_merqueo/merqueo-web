<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableZones extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('zones', function (Blueprint $table) {
            $table->tinyInteger('delivery_on_same_day')->default(0)->unsigned()->after('name');
            $table->dropColumn('maximum_orders_per_delivery_window');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('zones', function (Blueprint $table) {
            $table->dropColumn('delivery_on_same_day');
        });
	}

}
