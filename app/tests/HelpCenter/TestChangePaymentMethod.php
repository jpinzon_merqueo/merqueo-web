<?php

namespace Tests\HelpCenter;

use api\v13\help_center\ApiOrderController;
use Faker\Factory;
use orders\OrderStatus;
use Tests\Traits\MerqueoFactory;

/**
 * Class TestRescheduleOrder
 * @package Tests\HelpCenter
 */
class TestChangePaymentMethod extends \Tests\TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

    public function testGetPaymentMethods()
    {
        $order = $this->makeFakeOrder();

        $action = $this->getUrlAction($order, ApiOrderController::LIST_PAYMENT_METHOD);
        $response = $this->call('GET', $action);
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $paymentMethods = ['Tarjeta de crédito', 'Efectivo', 'Datáfono'];
        foreach ($paymentMethods as $index => $method) {
            $this->assertTrue(in_array($method, $response->result->payment_methods));
            unset($paymentMethods[$index]);
        }
        $this->assertEmpty($paymentMethods);
    }

    public function testGetOnlyCashPaymentMethodsOnDispatchedState()
    {
        $order = $this->makeFakeOrder();
        $order->status = OrderStatus::DISPATCHED;
        $order->save();

        $action = $this->getUrlAction($order, ApiOrderController::LIST_PAYMENT_METHOD);
        $response = $this->call('GET', $action);
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $paymentMethods = ['Efectivo', 'Datáfono'];
        foreach ($paymentMethods as $index => $method) {
            $this->assertTrue(in_array($method, $response->result->payment_methods));
            unset($paymentMethods[$index]);
        }

        $this->assertEmpty($paymentMethods);
    }

    public function testListCreditCards()
    {
        $faker = Factory::create();
        $order = $this->makeFakeOrder();
        $totalCreditCards = $faker->numberBetween(0, 10);
        foreach (range(1, $totalCreditCards) as $item) {
            $this->addCreditCard($order->user);
        }

        $action = $this->getUrlAction($order, ApiOrderController::LIST_PAYMENT_METHOD);
        $response = $this->call('GET', $action);
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $this->assertCount($totalCreditCards, $response->result->credit_cards);
    }

    public function testCantSeeAnythingOnRoutedOrderWithCreditCard()
    {
        $order = $this->makeFakeOrder();
        $order->status = OrderStatus::DISPATCHED;
        $order->payment_method = 'Tarjeta de crédito';
        $order->save();

        $action = $this->getUrlAction($order, ApiOrderController::LIST_PAYMENT_METHOD);
        $response = $this->call('GET', $action);
        $this->assertResponseStatus(400);
        $response = json_decode($response->getContent());
        $this->assertEquals('El pedido ya fue alistado.', $response->message);
    }

    public function testChangePaymentMethod()
    {
        $newPaymentMethod = 'Datáfono';
        $order = $this->makeFakeOrder();

        $action = $this->getUrlAction($order, ApiOrderController::CHANGE_PAYMENT_METHOD);
        $response = $this->call('POST', $action, ['payment_method' => $newPaymentMethod]);
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $order = \Order::findOrFail($order->id);
        $this->assertEquals('¡Cambio de método de pago exitoso!', $response->message);
        $this->assertEquals($newPaymentMethod, $order->payment_method);
    }

    public function testChangePaymentMethodToCreditCard()
    {
        $installments = 10;
        $order = $this->makeFakeOrder();
        $creditCard = $this->addCreditCard($order->user);

        $action = $this->getUrlAction($order, ApiOrderController::CHANGE_PAYMENT_METHOD);
        $response = $this->call(
            'POST', $action,
            ['credit_card_id' => $creditCard->id, 'installments' => $installments]
        );
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $order = \Order::findOrFail($order->id);
        $this->assertEquals('¡Cambio de método de pago exitoso!', $response->message);
        $this->assertNotEmpty($order->cc_token);
        $this->assertNotEmpty($order->credit_card_id);
        $this->assertNotEmpty($order->cc_last_four);
        $this->assertEquals($installments, $order->cc_installments);
    }

    public function testChangeOnlyCashPaymentMethodsOnDispatchedState()
    {
        $payment_method = 'Datáfono';
        $order = $this->makeFakeOrder();
        $order->status = OrderStatus::DISPATCHED;
        $order->save();

        $action = $this->getUrlAction($order, ApiOrderController::CHANGE_PAYMENT_METHOD);
        $response = $this->call('POST', $action, ['payment_method' => $payment_method]);
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $order = \Order::findOrFail($order->id);
        $this->assertEquals('¡Cambio de método de pago exitoso!', $response->message);
        $this->assertEquals($payment_method, $order->payment_method);
    }

    public function testCantChangePaymentMethodOnDisptachedOrderWithCreditCard()
    {
        $order = $this->makeFakeOrder();
        $creditCard = $this->addCreditCard($order->user);
        $order->status = OrderStatus::DISPATCHED;
        $order->payment_method = 'Tarjeta de crédito';
        $order->save();

        $action = $this->getUrlAction($order, ApiOrderController::CHANGE_PAYMENT_METHOD);
        $response = $this->call(
            'POST', $action,
            ['credit_card_id' => $creditCard->id, 'installments' => 1]
        );
        $this->assertResponseStatus(400);
        $response = json_decode($response->getContent());
        $this->assertEquals('El pedido ya fue alistado.', $response->message);
    }

    private function getUrlAction(\Order $order, $action)
    {
        return action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$order->user->id, $order->id, 'action' => $action]
        );
    }
}
