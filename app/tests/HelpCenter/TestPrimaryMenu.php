<?php

namespace Tests\HelpCenter;

use api\v13\help_center\HelpCenterMenu;
use Carbon\Carbon;
use orders\OrderStatus;
use orders\OrderValidator;
use orders\RescheduleOrder;
use Tests\Traits\MerqueoFactory;

/**
 * Class TestPrimaryMenu
 * @package Tests\HelpCenter
 */
class TestPrimaryMenu extends \Tests\TestCase
{
    use MerqueoFactory;

    const RESCHEDULE_ITEM = 1;
    const CHANGE_DELIVERY_TIME = 2;
    const CHANGE_PAYMENT_METHOD = 3;
    const ADD_COUPON = 4;
    const ASSOCIATE_COMPANY = 5;
    const CANCEL_ORDER = 6;
    const ADD_PRODUCT = 8;

    /**
     * @var \Order
     */
    private $order;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

    public function testRescheduleItemOnEarlyState()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create()->subMinutes(30);
        $this->order->status = OrderStatus::DISPATCHED;
        $this->order->save();
        $menu_ids = $this->getMenuIds();

        $this->assertTrue(in_array(self::RESCHEDULE_ITEM, $menu_ids));
    }

    public function testRescheduleItemIsHidden()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->status = OrderStatus::CANCELED;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::RESCHEDULE_ITEM, $menu_ids));
    }

    public function testChangeDeliveryTime()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::CHANGE_DELIVERY_TIME, $menu_ids));
    }

    public function testCantChangeOnInvalidState()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::ROUTED;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::CHANGE_DELIVERY_TIME, $menu_ids));
    }

    public function testCantChangeOnInvalidTries()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::INITIATED;
        $this->order->scheduled_delivery = RescheduleOrder::LIMIT_RESCHEDULE;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::CHANGE_DELIVERY_TIME, $menu_ids));
    }

    public function testChangePaymentMethod()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::INITIATED;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::CHANGE_PAYMENT_METHOD, $menu_ids));
    }

    public function testChangePaymentMethodOnRouterStateIfUserUseCash()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::ROUTED;
        $this->order->payment_method = 'Efectivo';
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::CHANGE_PAYMENT_METHOD, $menu_ids));

        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::ROUTED;
        $this->order->payment_method = 'Datafono';
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::CHANGE_PAYMENT_METHOD, $menu_ids));
    }

    public function testChangePaymentMethodOnEarlyStates()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::INITIATED;
        $this->order->payment_method = 'Tarjeta de crédito';
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::CHANGE_PAYMENT_METHOD, $menu_ids));
    }

    public function testCantChangePaymentMethodOnPreparedIfUsesCreditCard()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::PREPARED;
        $this->order->payment_method = 'Tarjeta de crédito';
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::CHANGE_PAYMENT_METHOD, $menu_ids));
    }

    public function testCanAddCoupons()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::PREPARED;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::ADD_COUPON, $menu_ids));
    }

    public function testCantAddCouponOnAlreadyUsedCoupon()
    {
        $this->order = $this->makeFakeOrder();
        $user = $this->order->user;
        $this->order->delivery_date = Carbon::create();
        $this->order->status = OrderStatus::PREPARED;
        \UserCredit::addCredit($user, 2000, Carbon::create()->addMonth()->format('Y-m-d'), 'order', $this->order);
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::ADD_COUPON, $menu_ids));
    }

    public function testAddBillToCompany()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->invoice_number = null;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::ASSOCIATE_COMPANY, $menu_ids));
    }

    public function testCantAddBillToCompany()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->invoice_number = 100;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::ASSOCIATE_COMPANY, $menu_ids));
    }

    public function testCancelOrder()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(in_array(self::CANCEL_ORDER, $menu_ids));
    }

    public function testCantCancelOrder()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->status = OrderStatus::DELIVERED;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::CANCEL_ORDER, $menu_ids));

        $this->order = $this->makeFakeOrder();
        $this->order->status = OrderStatus::CANCELED;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::CANCEL_ORDER, $menu_ids));
    }

    public function testAddProducts()
    {
        $availableDate = Carbon::create(2018, 05, 18);
        Carbon::setTestNow($availableDate);

        $validStates = [OrderStatus::VALIDATION, OrderStatus::INITIATED, OrderStatus::ROUTED];

        foreach ($validStates as $state) {
            $this->order = $this->makeFakeOrder();
            $this->order->status = $state;
            $this->order->save();

            $menu_ids = $this->getMenuIds();
            $this->assertTrue(in_array(self::ADD_PRODUCT, $menu_ids));
        }
    }

    public function testCantAddProducts()
    {
        $this->order = $this->makeFakeOrder();
        $this->order->status = OrderStatus::IN_PROGRESS;
        $this->order->save();

        $menu_ids = $this->getMenuIds();
        $this->assertTrue(!in_array(self::ADD_PRODUCT, $menu_ids));
    }

    /**
     * @return array
     */
    private function getMenu()
    {
        if (empty($this->order)) {
            $this->order = $this->makeFakeOrder();
        }

        $menu = new HelpCenterMenu(new OrderValidator($this->order));
        return $menu->createMenu();
    }

    /**
     * @return array
     */
    private function getMenuIds()
    {
        $menu = $this->getMenu();
        $menuItems = [];
        foreach ($menu as $items) {
            $menuItems[] = $items['id'];
        }

        return $menuItems;
    }
}