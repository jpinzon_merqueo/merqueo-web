<?php


namespace Tests\HelpCenter;

use Tests\TestCase;
use Illuminate\Support\Facades\Event;

class TestLogPreChargeRequest extends TestCase
{
    public function testCreateLog()
    {        
        $orderId = random_int(1, 100);

        // Firing the log event
        $subscriber = new \LogRequestDevolutionRequestPreChargeEventHandler();
        Event::subscribe($subscriber);
        $event = Event::fire('log.create', array($orderId));        
        $this->assertTrue($event[0]);
    }
}