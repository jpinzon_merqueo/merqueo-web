<?php

namespace Tests\HelpCenter;

use api\v13\help_center\ApiOrderController;
use Carbon\Carbon;
use exceptions\MerqueoException;
use orders\OrderStatus;
use orders\OrderValidator;
use orders\RescheduleOrder;
use Tests\Traits\MerqueoFactory;

/**
 * Class TestRescheduleOrder
 * @package Tests\HelpCenter
 */
class TestRescheduleOrder extends \Tests\TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
        \Event::subscribe(new \OrderEventHandler());
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

    /**
     * Se puede realizar la reprogramación del pedido, lo cual
     * afectara el monto del domicilio a cobrar.
     *
     * @throws MerqueoException
     */
    public function testRescheduleOrder()
    {
        $currentDeliveryAmount = 2500;
        $newDeliveryAmount = 5000;

        $order = $this->makeFakeOrder();
        $order->delivery_date = Carbon::create()->addDay();
        $order->delivery_amount = 0;
        $order->save();

        $status = new OrderValidator($order);
        $status->canChangeTime();

        $newDate = Carbon::create(null, null, null, 12, 0, 0)->addDays(2);
        $schedule = new RescheduleOrder($order);

        $order->orderGroup->delivery_amount = $currentDeliveryAmount;
        $order->orderGroup->user_address_latitude = 1;
        $order->orderGroup->user_address_longitude = 1;
        $order->orderGroup->save();

        $schedule->make($newDate->format('Y-m-d'), '07:00:00 - 12:00:00', 'No reason');
        $order = \Order::with('orderGroup')->findOrFail($order->id);
        $this->assertNotEquals($newDeliveryAmount, $order->delivery_amount);
        $this->assertNotEquals($newDeliveryAmount + $currentDeliveryAmount, $order->orderGroup->delivery_amount);
        $dateFormat = 'Y-m-d H:i:s';
        $deliveryDate = new Carbon($order->delivery_date);
        $this->assertEquals($deliveryDate->format($dateFormat), $newDate->format($dateFormat));
    }

    /**
     * La reprogramación del pedido solo se puede
     * @throws MerqueoException
     */
    public function testCantRescheduleOrderThreeTimes()
    {
        $order = $this->makeFakeOrder();
        $order->delivery_date = Carbon::create()->addDay();
        $order->delivery_amount = 0;
        $order->save();

        $order->orderGroup->user_address_latitude = 1;
        $order->orderGroup->user_address_longitude = 1;
        $order->orderGroup->save();

        $newDate = Carbon::create(null, null, null, 12, 0, 0)->addDays(2);
        // Si se modifica el limite se deben agregar horarios.
        $hours = ['07:00:00 - 12:00:00', '12:00:00 - 17:00:00', ''];
        $this->assertGreaterThan(0, RescheduleOrder::LIMIT_RESCHEDULE);
        foreach (range(0, RescheduleOrder::LIMIT_RESCHEDULE) as $items) {
            $order = \Order::findOrFail($order->id);
            $status = new OrderValidator($order);
            if ($items === RescheduleOrder::LIMIT_RESCHEDULE) {
                $this->setExpectedException(MerqueoException::class);
            }
            $status->canChangeTime();
            $schedule = new RescheduleOrder($order);
            $schedule->make($newDate->format('Y-m-d'), $hours[$items], 'No reason');
        }
    }

    /**
     * La reprogramación del pedido solo se puede
     * @throws MerqueoException
     */
    public function testCantRescheduleOnInvalidState()
    {
        $order = $this->makeFakeOrder();
        $order->delivery_date = Carbon::create()->addDay();
        $order->status = OrderStatus::ROUTED;
        $order->save();
        $status = new OrderValidator($order);
        $this->setExpectedException(MerqueoException::class);
        $status->canChangeTime();
    }

    /**
     * La reprogramación del pedido solo se puede
     * @throws MerqueoException
     */
    public function testRescheduleOnLateOrder()
    {
        $order = $this->makeFakeOrder();
        $order->delivery_date = Carbon::create()->subMinutes(31);
        $order->status = OrderStatus::DISPATCHED;
        $order->orderGroup->user_address_longitude = 1;
        $order->orderGroup->user_address_latitude = 1;
        $order->orderGroup->save();
        $order->save();
        $status = new OrderValidator($order);
        $status->canChangeTime();

        $schedule = new RescheduleOrder($order);
        $newDate = Carbon::create(null, null, null, 12, 0, 0)->addDays(2);
        $schedule->make($newDate->format('Y-m-d'), '07:00:00 - 12:00:00', 'No reason');
    }

    /**
     * La reprogramación del pedido solo se puede
     * @throws MerqueoException
     */
    public function testRescheduleOnNoLateOrder()
    {
        $order = $this->makeFakeOrder();
        $order->delivery_date = Carbon::create()->subMinutes(25);
        $order->status = OrderStatus::DISPATCHED;
        $order->save();
        $status = new OrderValidator($order);
        $this->setExpectedException(MerqueoException::class, 'No puedes reprogramar el pedido.');
        $status->canChangeTime();
    }

    public function testListValidSlots()
    {
        self::markTestIncomplete();
    }

    public function testSeeChangeLog()
    {
        self::markTestSkipped();
    }
}
