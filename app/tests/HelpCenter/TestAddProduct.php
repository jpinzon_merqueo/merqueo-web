<?php

namespace Tests\HelpCenter;

use api\v13\help_center\ApiOrderController;
use Carbon\Carbon;
use orders\OrderStatus;
use Tests\Traits\MerqueoFactory;

/**
 * Class TestRescheduleOrder
 * @package Tests\HelpCenter
 */
class TestAddProduct extends \Tests\TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
        $availableDate = Carbon::create(2018, 05, 18);
        Carbon::setTestNow($availableDate);
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

    /**
     *
     */
    public function testKeepSameProductsShowNoAlterMessage()
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->save();
        $storeProduct = $this->getStoreProduct();
        $storeProduct->price = $order->store->minimum_order_amount;
        $order->orderGroup->total_amount = $storeProduct->price;
        $order->orderGroup->save();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $user = $order->user;
        $cart = [
            [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false,
                    ]
                ]
            ]
        ];
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$user->id, $order->id, 'action' => ApiOrderController::PREVIEW_ORDER]
        );
        $result = $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);
        $response = json_decode($result->getContent());
        $this->assertEquals('No hay cambios en tu pedido', $response->message);
    }

    /**
     *
     */
    public function testAddProductsToOrderWithNormalQuantity()
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->save();
        $storeProduct = $this->getStoreProduct();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $user = $order->user;
        $products = [];
        $cart = [];
        $totalPrice = 0;
        foreach (range(0, 10) as $item) {
            $storeProduct = $this->getStoreProduct();
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $products[] = $storeProduct;
            $totalPrice += $storeProduct->price;
            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false,
                    ],
                ]
            ];
        }
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$user->id, $order->id, 'action' => ApiOrderController::PREVIEW_ORDER]
        );
        $result = $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);
        $response = json_decode($result->getContent());
        $this->assertEquals($totalPrice, $response->result->total);
    }

    /**
     *
     */
    public function testAddProductsToOrderWithManyProducts()
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->save();
        $storeProduct = $this->getStoreProduct();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $user = $order->user;
        $products = [];
        $cart = [];
        $totalPrice = 0;
        foreach (range(0, 10) as $item) {
            $storeProduct = $this->getStoreProduct();
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $products[] = $storeProduct;
            $totalPrice += $storeProduct->price * 2;
            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 2,
                        'has_special_price' => false,
                    ],
                ]
            ];
        }
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$user->id, $order->id, 'action' => ApiOrderController::PREVIEW_ORDER]
        );
        $result = $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);
        $response = json_decode($result->getContent());
        $this->assertEquals($totalPrice, $response->result->total);
    }

    /**
     *
     */
    public function testCantAddProductWithMinimumUnreached()
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->save();
        $storeProduct = $this->getStoreProduct();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $order->store->minimum_order_amount = 50000;
        $order->store->save();

        $user = $order->user;
        $products = [];
        $cart = [];
        $totalPrice = 0;
        foreach (range(0, 1) as $item) {
            $storeProduct = $this->getStoreProduct();
            $storeProduct->price = $order->store->minimum_order_amount / 4;
            $storeProduct->save();
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $products[] = $storeProduct;
            $totalPrice += $storeProduct->price;
            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false,
                    ],
                ]
            ];
        }
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$user->id, $order->id, 'action' => ApiOrderController::PREVIEW_ORDER]
        );
        $result = $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(400);
        $response = json_decode($result->getContent());
        $this->assertEquals('Valor mínimo de pedido no alcanzado.', $response->message);
    }

    /**
     *
     */
    public function testAddProductsWithSpecialPrice()
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->save();
        $storeProduct = $this->getStoreProduct();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $order->store->minimum_order_amount = 50000;
        $order->store->save();

        $user = $order->user;
        $products = [];
        $cart = [];
        $totalPrice = 0;
        foreach (range(0, 1) as $item) {
            $storeProduct = $this->getStoreProduct();
            $storeProduct->price = (int)($order->store->minimum_order_amount / 2);
            $storeProduct->special_price = (int)($order->store->minimum_order_amount / 3);
            $storeProduct->save();
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $products[] = $storeProduct;
            $totalPrice += $storeProduct->special_price * 2;
            $totalPrice += $storeProduct->price * 3;
            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->special_price,
                        'quantity' => 2,
                        'has_special_price' => true,
                    ],
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 3,
                        'has_special_price' => false,
                    ]
                ]
            ];
        }
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$user->id, $order->id, 'action' => ApiOrderController::PREVIEW_ORDER]
        );
        $result = $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);
        $response = json_decode($result->getContent());
        $this->assertEquals($totalPrice, $response->result->subtotal);
    }

    /**
     *
     */
    public function testAddProductsToMarketplaceOrder()
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $storeProduct = $this->getStoreProduct();
        $alliedStore = $this->getAlliedStore($storeProduct->provider);
        $storeProduct->alliedStore()->associate($alliedStore);

        $order = $this->makeFakeOrder($warehouse);
        $order->alliedStore()->associate($alliedStore);
        $order->save();

        $storeProduct->price = $order->store->minimum_order_amount;
        $storeProduct->save();

        $this->stockOnWarehouse($warehouse, $storeProduct);
        $currentPrice = $storeProduct->price;
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $order->store->save();

        $user = $order->user;
        $products = [];
        $cart = [
            [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false
                    ]
                ],
            ]
        ];

        $totalPrice = 0;
        foreach (range(0, 4) as $item) {
            $quantity = 2;
            $storeProduct = $this->getStoreProduct();
            $storeProduct->price = $order->store->minimum_order_amount;
            $storeProduct->save();
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $products[] = $storeProduct;
            $totalPrice += $storeProduct->price * $quantity;
            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => $quantity,
                        'has_special_price' => false,
                    ],
                ]
            ];
        }
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$user->id, $order->id, 'action' => ApiOrderController::ALTER_ORDER]
        );
        $response = $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);
        $response = json_decode($response->getContent());
        $this->assertEquals($totalPrice + $currentPrice, $response->result->total);

        $order = \Order::with('orderGroup.orders')->findOrFail($order->id);
        $orders = $order->orderGroup->orders;
        $this->assertCount(2, $orders);
        foreach ($orders as $order) {
            if ($order->type === 'Merqueo') {
                $this->assertEquals($totalPrice, $order->total_amount);
            } else {
                $this->assertEquals($currentPrice, $order->total_amount);
            }
        }
    }

    /**
     *
     */
    public function testAddProductsWithDeliveryDiscount()
    {
        $delivery_amount = 5000;
        $product_discount = 500;
        $products_limit = 5;
        $discount_products_frequency = 1;

        $city = \City::first();
        $warehouse = $this->newWarehouse($city);

        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->delivery_amount = $delivery_amount;
        $order->save();

        $order->orderGroup->delivery_amount = $delivery_amount;
        $order->orderGroup->save();

        foreach (range(1, $products_limit) as $item) {
            $storeProduct = $this->getStoreProduct();
            $storeProduct->price = $order->store->minimum_order_amount;
            if ($item % $discount_products_frequency === 0) {
                $storeProduct->delivery_discount_amount = $product_discount;
            }
            $storeProduct->save();
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false,
                    ],
                ]
            ];
        }
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$order->user->id, $order->id, 'action' => ApiOrderController::ALTER_ORDER]
        );
        $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);

        $order = \Order::with('orderGroup')->findOrFail($order->id);
        $total_discount_amount = $delivery_amount - ($product_discount * floor($products_limit / $discount_products_frequency));
        $this->assertEquals($total_discount_amount, $order->delivery_amount);
        $ordersTotal = 0;
        foreach ($order->orderGroup->orders as $order) {
            $ordersTotal += $order->delivery_amount;
        }
        $this->assertEquals($total_discount_amount, $ordersTotal);
    }

    /**
     *
     */
    public function testAddProductsWithDeliveryDiscountAffectsPrimaryOrder()
    {
        $order = $this->getMerqueoOrderWithProducts(3);
        $cart = [];
        foreach ($order->orderProducts as $product) {
            $cart[] = [
                'id' => $product->store_product_id,
                'details' => [
                    [
                        'price' => $product->price,
                        'quantity' => $product->quantity,
                        'has_special_price' => false,
                    ]
                ]
            ];
        }

        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$order->user->id, $order->id, 'action' => ApiOrderController::ALTER_ORDER]
        );
        $this->call('POST', $action, compact('cart'));
        $this->assertResponseStatus(200);
    }

    /**
     *
     */
    public function testAddManyProductsWithDiscount()
    {
        $order = $this->makeFakeOrder();
        $storeProduct = $this->getStoreProduct();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);

        $cart = [
            [
                'id' => $storeProduct->id,
                'details' => [],
            ]
        ];

        foreach (range(1, 1) as $item) {
            $cart[0]['details'][] = [
                'price' => $storeProduct->price,
                'has_special_price' => true,
                'quantity' => 1,
            ];
        }

        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$order->user->id, $order->id, 'action' => ApiOrderController::ALTER_ORDER]
        );
        $request = $this->call('POST', $action, compact('cart'));
        $order = \Order::with('orderProducts')->find($order->id);
        $this->assertResponseStatus(400);
    }

    /**
     *
     */
    public function testAddProductWithMarketplace()
    {
        $order = $this->makeFakeOrder();
        $order->type = 'Merqueo';
        $order->save();
        $storeProduct = $this->getStoreProduct();
        $storeProduct->quantity_special_price = 1;
        $storeProduct->special_price = $storeProduct->price * .9;
        $storeProduct->save();
        $this->associateStoreProductsToOrder($order, $storeProduct, 1);
        $this->stockOnWarehouse($order->orderGroup->warehouse, $storeProduct);

        $cart = [
            [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->special_price,
                        'has_special_price' => true,
                        'quantity' => 1,
                    ]
                ],
            ]
        ];

        foreach (range(1, 1) as $item) {
            $cart[0]['details'][] = [
                'price' => $storeProduct->price,
                'has_special_price' => false,
                'quantity' => 1,
            ];
        }

        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$order->user->id, $order->id, 'action' => ApiOrderController::ALTER_ORDER]
        );
        $request = $this->call('POST', $action, compact('cart'));
        $content = json_decode($request->getContent());
        $this->assertInstanceOf(\StdClass::class, $content);
        $order = \Order::with('orderProducts')->find($order->id);
        $this->assertEquals(
            $storeProduct->special_price + $storeProduct->price,
            $content->result->total
        );

        $this->assertEquals(
            $storeProduct->special_price + $storeProduct->price,
            $order->total_amount
        );
    }

    /**
     *
     */
    public function testAddProductsOnOtherOtherDeliveryWithNoMinimumLimit()
    {
        self::markTestSkipped();
    }

    /**
     * @param int $productsQuantity
     * @return \Order
     */
    private function getMerqueoOrderWithProducts($productsQuantity = 1)
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $storeProduct = $this->getStoreProduct();

        $order = $this->makeFakeOrder($warehouse);
        $order->type = 'Merqueo';
        $order->save();

        foreach (range(1, $productsQuantity) as $item) {
            $storeProduct = $this->getStoreProduct($storeProduct->store, $storeProduct->provider);
            $storeProduct->price = $order->store->minimum_order_amount;
            $storeProduct->save();

            $this->stockOnWarehouse($warehouse, $storeProduct);
            $this->associateStoreProductsToOrder($order, $storeProduct, 1);
        }


        return $order;
    }

    /**
     * @param int $productsQuantity
     * @return \Order
     */
    private function getMarketplaceOrderWithProducts($productsQuantity = 1)
    {
        $city = \City::first();
        $warehouse = $this->newWarehouse($city);
        $order = $this->makeFakeOrder($warehouse);
        $storeProduct = $this->getStoreProduct();
        $alliedStore = $this->getAlliedStore($storeProduct->provider);
        $order->alliedStore()->associate($alliedStore);
        $order->save();

        foreach (range(1, $productsQuantity) as $item) {
            $storeProduct = $this->getStoreProduct();
            $storeProduct->alliedStore()->associate($alliedStore);
            $storeProduct->price = $order->store->minimum_order_amount;
            $storeProduct->save();

            $this->stockOnWarehouse($warehouse, $storeProduct);
            $this->associateStoreProductsToOrder($order, $storeProduct, 1);
        }

        return $order;
    }

    /**
     * +
     */
    public function testRemoveProduct()
    {
        $store = $this->newStore();
        $order = $this->makeFakeOrder(null, null, OrderStatus::INITIATED, $store);
        $user = $order->user;

        $order->type = 'Merqueo';
        $order->save();

        $storeProducts = new \Illuminate\Support\Collection();
        $storeProduct = null;
        foreach (range(1, 2) as $item) {
            $storeProduct = $this->getStoreProduct($store);
            $storeProduct->price = $order->store->minimum_order_amount;
            $storeProduct->save();
            $storeProducts->push($storeProduct);
        }
        $this->associateStoreProductsToOrder($order, $storeProducts, 1);
        $this->stockOnWarehouse($order->orderGroup->warehouse, $storeProduct);

        $alliedStore = $this->newAlliedStore();
        $marketplaceProduct = $this->getStoreProduct($store, null, $alliedStore);
        $this->stockOnWarehouse($order->orderGroup->warehouse, $marketplaceProduct, $alliedStore);

        $cart = [
            [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'has_special_price' => false,
                        'quantity' => 1,
                    ]
                ],
            ],[
                'id' => $marketplaceProduct->id,
                'details' => [
                    [
                        'price' => $marketplaceProduct->price,
                        'has_special_price' => false,
                        'quantity' => 1,
                    ]
                ],
            ],
        ];
        $action = action(
            'api\v13\help_center\ApiOrderController@manage_user_orders',
            [$order->user->id, $order->id, 'action' => ApiOrderController::ALTER_ORDER]
        );
        $request = $this->call('POST', $action, compact('cart'));
        $content = json_decode($request->getContent());
        $this->assertResponseOk();
        $this->assertEquals(1, $user->orders()->where('allied_store_id', $alliedStore->id)->count());
    }
}