<?php

namespace Tests;

use Admin;
use DB;
use CustomerService;
use Route;
use UserCredit;
use Tests\Traits\MerqueoFactory;

class TicketsTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
         DB::beginTransaction();
         // Las validaciones tienen en cuenta la cantidad actual de tickets.
         DB::table('tickets')->where('id', '<>', 0)->delete();
         DB::table('ticket_calls')->where('id', '<>', 0)->delete();
         $this->faker = \Faker\Factory::create();
    }

    protected function tearDown()
    {
        parent::tearDown();
        DB::rollback();
    }

    // Se divide en tres secciónes:
    // 1. Creción de los tickets cuando se califica el pedido
    // 2. Gestion de los tickets
    // 3. Gestion de los layouts.

    public function testCreateTicketOnComplaint()
    {
        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $text = [$this->faker->paragraph(), $this->faker->paragraph(), $this->faker->paragraph()];
        $comment = $this->faker->paragraph();
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad',
            'other' => $text,
            'comment' => $comment,
        ];
        $this->call('POST', $action, $body);

        $tickets = \tickets\Ticket::where('order_id', $order->id)->get();
        $this->assertCount(1, $tickets);
        $this->assertNull($tickets[0]->admin);
    }

    public function testPQRCreatedOnComplaint()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);

        $action = route('customerService.Ticket.solutionRate', [$call->token, 0]);
        $this->call('GET', $action);
        $this->assertResponseOk();

        $pqr_service = CustomerService::where('description', CustomerService::PQR)->firstOrFail();
        $pqrs = \tickets\Ticket::where('customer_service_id', $pqr_service->id)->get();
        $this->assertCount(1, $pqrs);
        $pqr = $pqrs[0];
        $this->assertTrue($pqr->activeCall->admin_id == $admin->id);
    }

    public function testAssignOnlyWhenUserIsReady()
    {
        $this->makeTicket();
        $admin = Admin::hasCustomerServiceRol()->first();
        $this->beAdmin($admin);
        $this->setPermissions($admin);

        Route::enableFilters();
        $action = action('admin\callcenter\TicketController@user_receive_tickets');
        $this->initAdminSession($admin, 'user_receive_tickets');
        $this->call('POST', $action);
        $this->assertResponseOk();
        $this->clearAdminSession();
        Route::disableFilters();

        $assignedTickets = \tickets\TicketCall::where('admin_id', $admin->id)
            ->where('status', \tickets\TicketCall::PENDING)
            ->get();
        self::assertCount(1, $assignedTickets);
        $assignedTicket = $assignedTickets[0];
        self::assertEquals($assignedTicket->ticket->activeCall->admin_id, $assignedTicket->admin_id);
        \Session::forget('admin_id');
    }

    public function testTicketAssignedOnlyToOneGuy()
    {
        $this->makeTicket();
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $this->session(['admin_id' => $admin->id]);
        $this->beAdmin($admin);
        Route::enableFilters();
        $action = action('admin\callcenter\TicketController@user_receive_tickets');
        $this->initAdminSession($admin, 'user_receive_tickets');
        $this->call('POST', $action);
        $this->assertResponseOk();
        $this->clearAdminSession();

        $otherAdmin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $this->beAdmin($otherAdmin);
        $this->initAdminSession($otherAdmin, 'user_receive_tickets');
        $action = action('admin\callcenter\TicketController@user_receive_tickets');
        $this->call('POST', $action);
        $this->clearAdminSession();
        $this->assertResponseOk();
        Route::disableFilters();

        $assignedTickets = \tickets\TicketCall::where('admin_id', $otherAdmin->id)
            ->where('status', \tickets\TicketCall::PENDING)
            ->get();

        self::assertCount(0, $assignedTickets);
    }

    public function testAlreadyManagedTokenIsNoAvailable()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $ticket->status = \tickets\Ticket::PENDING;
        $ticket->save();

        $ticket_call = new \tickets\TicketCall();
        $ticket_call->ticket()->associate($ticket);
        $ticket_call->admin()->associate($admin);
        $ticket_call->management_date = \Carbon\Carbon::create();
        $ticket_call->status = \tickets\TicketCall::NOT_ATTENDED;
        $ticket_call->save();

        $otherAdmin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $this->beAdmin($admin);
        $this->initAdminSession($otherAdmin, 'user_receive_tickets');
        Route::enableFilters();
        $action = action('admin\callcenter\TicketController@user_receive_tickets');
        $this->call('POST', $action);
        $this->clearAdminSession();
        Route::disableFilters();
        $assignedTickets = \tickets\TicketCall::where('admin_id', $otherAdmin->id)
            ->where('status', \tickets\TicketCall::PENDING)
            ->get();

        self::assertCount(0, $assignedTickets);
    }

    public function testAssignPendingTicket()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $ticket->status = \tickets\Ticket::PENDING;
        $ticket->save();

        $ticket_call = new \tickets\TicketCall();
        $ticket_call->ticket()->associate($ticket);
        $ticket_call->admin()->associate($admin);
        $ticket_call->management_date = \Carbon\Carbon::create()->subMinute(\tickets\Ticket::TIME_RANGE + 2);
        $ticket_call->status = \tickets\TicketCall::NOT_ATTENDED;
        $ticket_call->save();

        $this->beAdmin($admin);
        $this->initAdminSession($admin, 'user_receive_tickets');
        $this->setPermissions($admin);
        Route::enableFilters();
        $action = action('admin\callcenter\TicketController@user_receive_tickets');
        $this->call('POST', $action);
        Route::disableFilters();
        $this->assertResponseOk();
        $this->clearAdminSession();

        $assignedTickets = \tickets\TicketCall::where('admin_id', $admin->id)
            ->where('status', \tickets\TicketCall::PENDING)
            ->where('ticket_id', $ticket->id)
            ->get();

        self::assertCount(1, $assignedTickets);
    }

    public function testAssignTicketManually()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $action = action('admin\callcenter\TicketController@assign_ticket', $ticket->id);
        $this->call('POST', $action, ['admin_id' => $admin->id]);
        $this->assertResponseOk();

        $assignedTickets = \tickets\TicketCall::where('admin_id', $admin->id)
            ->where('status', \tickets\TicketCall::PENDING)
            ->where('ticket_id', $ticket->id)
            ->get();
        self::assertCount(1, $assignedTickets);
    }

    public function testAssignAlreadyTakenTicketRemoveCurrentAssignment()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $ticket->assignTicket($admin);

        $adminTow = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $action = action('admin\callcenter\TicketController@assign_ticket', $ticket->id);
        $this->call('POST', $action, ['admin_id' => $adminTow->id]);
        self::assertTrue($ticket->activeCall->admin->id === $adminTow->id);
    }

    public function testNotSolvedTicket()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);

        $action = action('admin\callcenter\TicketController@update_status', $call->id);
        $this->call('POST', $action, [
            'status_ticket' => \tickets\Ticket::PENDING,
            'status_call' => \tickets\TicketCall::ATTENDED,
            'comments' => $this->faker->paragraph(),
        ]);
        $this->assertResponseOk();
        $ticket = \tickets\Ticket::find($ticket->id);
        self::assertEquals($ticket->status, \tickets\Ticket::PENDING);
        $this->assertTrue(
            $ticket->lastCall->management_date->lt(\Carbon\Carbon::create()->addMinute(1)) &&
            $ticket->lastCall->management_date->gt(\Carbon\Carbon::create()->subMinute(1))
        );
    }

    public function testTicketSolvedWithComplaintOrder()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);

        $action = action('admin\callcenter\TicketController@update_status', $call->id);
        $current_order = $ticket->order;

        $complaint_order = $this->makeFakeOrder();
        $complaint_order->orderGroup->source = 'Reclamo';
        $complaint_order->orderGroup->save();

        $this->call('POST', $action, [
            'status_ticket' => \tickets\Ticket::SOLVED,
            'status_call' => \tickets\TicketCall::ATTENDED,
            'comments' => $this->faker->paragraph(),
            'solution_id' => \tickets\TicketSolution::COMPLAINT_ORDER,
            'solution_identifier' => $current_order->id,
            'ticket_layout_id' => \tickets\TicketLayout::first()->id,
            'content' => $this->faker->paragraph()
        ]);
        $this->assertResponseOk();
        $ticket = \tickets\Ticket::find($ticket->id);
        self::assertEquals($ticket->status, \tickets\Ticket::SOLVED);
    }

    public function testTicketSolvedWithVoucher()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);

        $action = action('admin\callcenter\TicketController@update_status', $call->id);
        $voucher = $this->getVoucher();
        $this->call('POST', $action, [
            'status_ticket' => \tickets\Ticket::SOLVED,
            'status_call' => \tickets\TicketCall::ATTENDED,
            'comments' => $this->faker->paragraph(),
            'solution_id' => \tickets\TicketSolution::VOUCHER,
            'solution_identifier' => $voucher->code,
            'ticket_layout_id' => \tickets\TicketLayout::first()->id,
            'content' => $this->faker->paragraph()
        ]);
        $this->assertResponseOk();
        $ticket = \tickets\Ticket::find($ticket->id);
        self::assertEquals($ticket->status, \tickets\Ticket::SOLVED);
    }

    public function testTicketSolvedWithCredit()
    {
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);
        $total_credits = UserCredit::where('user_id', $ticket->order->user->id)->count();
        $action = action('admin\callcenter\TicketController@update_status', $call->id);
        $this->call('POST', $action, [
            'status_ticket' => \tickets\Ticket::SOLVED,
            'status_call' => \tickets\TicketCall::ATTENDED,
            'comments' => $this->faker->paragraph(),
            'solution_id' => \tickets\TicketSolution::CREDIT,
            'amount' => $this->faker->numerify('#####'),
            'expiration_date' => \Carbon\Carbon::create()->addYear(1)->format('Y/m/d'),
            'ticket_layout_id' => \tickets\TicketLayout::first()->id,
            'content' => $this->faker->paragraph()
        ]);
        $this->assertResponseOk();
        $ticket = \tickets\Ticket::find($ticket->id);
        self::assertEquals($ticket->status, \tickets\Ticket::SOLVED);
        $new_total_credits = UserCredit::where('user_id', $ticket->order->user->id)->count();
        self::assertEquals($total_credits + 1, $new_total_credits);
    }


    public function testSaveTicketSolution()
    {

        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);
        $body = [
            '_token' => 'Bf37SMiQvamznZtk1ZcsxQiOMG9WNDxAHVBCyzuI',
            'status_ticket' => 'Solucionado',
            'called' => 0,
            'status_call' => '',
            'comments' => 'Calidad de producto, se genera pedido de reclamo',
            'solution_id' => 1,
            'solution_identifier' => '0',
            'amount' => '',
            'expiration_date' => \Carbon\Carbon::create()->addYear(1)->format('Y/m/d'),
            'ticket_layout_id' => 9,
            'content' => ''
        ];
        $action = action('admin\callcenter\TicketController@update_status', $call->id);
        $this->setExpectedException(\Symfony\Component\HttpKernel\Exception\BadRequestHttpException::class);
        $this->call('POST', $action, $body);
    }

    public function testTicketSolvedWithFreeDelivery()
    {
        $end_date = \Carbon\Carbon::create()->addYear(1);
        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $ticket = $this->makeTicket();
        $call = $ticket->assignTicket($admin);
        $action = action('admin\callcenter\TicketController@update_status', $call->id);
        $this->call('POST', $action, [
            'status_ticket' => \tickets\Ticket::SOLVED,
            'status_call' => \tickets\TicketCall::ATTENDED,
            'comments' => $this->faker->paragraph(),
            'solution_id' => \tickets\TicketSolution::FREE_DELIVERY,
            'expiration_date' => $end_date->format('d/m/Y'),
            'ticket_layout_id' => \tickets\TicketLayout::first()->id,
            'content' => $this->faker->paragraph()
        ]);
        $this->assertResponseOk();
        $ticket = \tickets\Ticket::find($ticket->id);
        self::assertEquals($ticket->status, \tickets\Ticket::SOLVED);
        $user = $ticket->order->user()->first();
        self::assertEquals($user->free_delivery_next_order, 1);
        self::assertEquals($user->free_delivery_next_order_expiration_date, $end_date->format('Y-m-d'));
    }

    public function testAssignFullQueue()
    {
        $free_tickets = 2;
        for ($i = 0; $i < \tickets\Ticket::QUEUE_SIZE + $free_tickets; $i++) {
            $this->makeTicket();
        }

        $admin = Admin::orderByRaw('RAND()')->hasCustomerServiceRol()->first();
        $this->beAdmin($admin);
        $this->initAdminSession($admin, 'user_receive_tickets');
        $action = action('admin\callcenter\TicketController@user_receive_tickets');
        Route::enableFilters();
        $this->call('POST', $action);
        $this->clearAdminSession();
        Route::disableFilters();

        $this->assertResponseOk();

        self::assertEquals(\tickets\Ticket::QUEUE_SIZE, $admin->calls()->active()->count());
        self::assertEquals($free_tickets, \tickets\Ticket::justCreated()->count());
    }

    public function testLayoutCreated()
    {
        $this->markTestSkipped('Implementar...');
    }

    public function testLayoutRenderedProperly()
    {
        $this->markTestSkipped('Implementar...');
    }

    /**
     * @param Admin $admin
     * @param array|null $permissions
     */
    private function setPermissions(Admin $admin, array $permissions = null)
    {
        if (count($permissions) === 0) {
            $permissions = CustomerService::lists('id');
        } else {
            $permissions = $permissions->map(function (CustomerService $service) {
                $newService = new CustomerService();
                $newService = $newService->id = $service->id;
                return $newService;
            })->toArray();
        }
        $admin->services_list = implode(',', $permissions);
        $admin->save();
    }

    /**
     * @return \tickets\Ticket
     */
    private function makeTicket()
    {
        $order = $this->makeFakeOrder();
        $ticket = new \tickets\Ticket();
        $ticket->order()->associate($order);
        $service = \CustomerService::where('description', 'Tickets')->first();
        $ticket->customerService()->associate($service);
        $ticket->save();

        return $ticket;
    }
}
