<?php

namespace Tests;


use Tests\Helpers\EmailHelper;
use Tests\Traits\MerqueoFactory;

class SoatTest extends TestCase
{
    use MerqueoFactory;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        require __DIR__ . '/helpers.php';
    }

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }


    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
        EmailHelper::clear();
    }

    public function testAcceptedPayment()
    {
        $mockEmail = EmailHelper::getInstance();
        $order = $this->getOrderWithCreditCard('APPROVED');
        $user = $order->user;
        $service = $this->newExternalService($user, $order->creditCard);
        $result = $service->chargeCreditCard();
        $this->assertTrue($result['status'], 'La transacción no fue acceptada.');
        $this->assertTrue($mockEmail->getResults(), 'El email no fue enviado.');
    }

    public function testRefundPayment()
    {
        $order = $this->getOrderWithCreditCard('APPROVED');
        $user = $order->user;
        $service = $this->newExternalService($user, $order->creditCard);
        $result = $service->chargeCreditCard();
        $this->assertTrue($result['status']);
        $this->assertTrue($service->canMakeRefund());
        $refund = $service->makeRefund(\RejectReason::first(), \Admin::first());
        $changed = $refund->didStateChange(new \PayU());
        $this->assertFalse($changed);
    }

    public function testRejectedPayment()
    {
        $order = $this->getOrderWithCreditCard('REJECTED');
        $user = $order->user;
        $service = $this->newExternalService($user, $order->creditCard);
        $result = $service->chargeCreditCard();
        $this->assertFalse($result['status']);
    }

    /**
     * @param $state
     * @return \Order
     * @throws \exceptions\MerqueoException
     */
    private function getOrderWithCreditCard($state)
    {
        $user = $this->newUser();
        $user->first_name = $state;
        $user->save();

        $creditCard = $this->addCreditCard($user, true);
        $order = $this->makeFakeOrder(null, $user, 'Delivered');
        $order->creditCard()->associate($creditCard);
        $order->save();

        return $order;
    }
}
