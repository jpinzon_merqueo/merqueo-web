<?php

namespace Tests;

use Carbon\Carbon;
use DB;
use Faker\Factory;
use Product;
use ProductDetail;
use Store;
use StoreProduct;
use Department;
use Shelf;
use Tests\Traits\MerqueoFactory;

class ApiStoreTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();
        DB::rollBack();
    }

    public function testProductHasImagesList()
    {
        $faker = Factory::create();
        $guarantee_policies = $faker->url;
        $product = Product::has('storeProduct.store')
            ->where('type', '<>', 'Proveedor')
            ->firstOrFail();

        // TODO usar items de MerqueoFactory
        $storeProduct = $this->newVisibleProduct($product);
        $storeProduct->guarantee_policies = $guarantee_policies;
        $storeProduct->save();
        $this->newImage($product);
        $url = action('api\v13\ApiStoreController@get_product', [
            $storeProduct->id,
            'zone_id' => $storeProduct->storeProductWarehouses[0]->warehouse->zones()->first()->id
        ]);
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $result = json_decode($request->getContent());
        $product_result = $result->result;
        $this->assertEquals($product_result->id, $storeProduct->id);
        $this->assertObjectHasAttribute('images', $product_result);
        $product->load('images');
        $this->assertGreaterThan(0, count($product->images));
        $this->assertCount(count($product->images) + 1, $product_result->images);
        $this->assertObjectHasAttribute('details', $product_result);
        $this->assertObjectHasAttribute('guarantee_policies', $product_result);
        $this->assertEquals($guarantee_policies, $product_result->guarantee_policies);
        $this->assertTrue(is_array($product_result->images));
        foreach ($product_result->images as $image) {
            $this->assertObjectHasAttribute('image_large_url', $image);
            $this->assertObjectHasAttribute('image_small_url', $image);
        }
    }

    public function testProductHasImagesEmptyArray()
    {
        $product = Product::whereDoesntHave('images')->first();
        $detail = new ProductDetail(['type' => ProductDetail::SPECIFICATION, 'name' => 'Test', 'description' => 'XXX']);
        $product->details()->save($detail);
        $storeProduct = $product->storeProducts[0];

        $url = action('api\v13\ApiStoreController@get_product', $storeProduct->id);
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $result = json_decode($request->getContent());
        $product = $result->result;
        $this->assertObjectHasAttribute('images', $product);
        $this->assertTrue(is_array($product->images));
    }

    public function testShowAllProductsWithStock()
    {
        $totalProducts = 10;
        $store = Store::where('name', 'Merqueo Bogotá')->first();
        $warehouse = $this->newWarehouse();
        $shelve = Shelf::first();
        $department = $shelve->department;
        $storeProducts = StoreProduct::with('product')
            ->where('store_id', $store->id)
            ->orderBy('id', 'desc')
            ->limit($totalProducts)
            ->get();

        foreach ($storeProducts as $storeProduct) {
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $storeProduct->shelf_id = $shelve->id;
            $storeProduct->department_id = $department->id;
            $storeProduct->save();
        }

        $url = action('api\v13\ApiStoreController@get_department', [$store->id, $department->id, 'zone_id' => $warehouse->zones()->first()->id]);
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $result = json_decode($request->getContent());
        $shelves = $result->result->department->shelves;
        $products = [];
        foreach ($shelves as $shelf) {
            foreach ($shelf->products as $product) {
                $this->assertObjectHasAttribute('id', $product);
                $this->assertObjectHasAttribute('name', $product);
                $products[] = (array)$product;
            }
        }
        $this->assertGreaterThanOrEqual(1, count($products));
    }

    public function testGetStoreMethod()
    {
        $url_temp = 'https://prueba.com/url_en_image_list_app_url_temp';
        $department = Department::find(20);
        $department->image_list_app_url_temp = $url_temp;
        $department->save();

        $url = action('api\v13\ApiStoreController@get_store', ['lat' => '4.75209473', 'lng' => '-74.08867971']);
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());

        $this->assertObjectHasAttribute('zone_id', $response->result->store);
        $this->assertInternalType('array', $response->result->store->departments);
        $this->assertObjectHasAttribute('image_list_app_url', $response->result->store->departments[0]);
        $this->assertEquals($url_temp, $response->result->store->departments[0]->image_list_app_url);

        $department = Department::find(20);
        $department->image_list_app_url_temp = null;
        $department->save();

        $url = action('api\v13\ApiStoreController@get_store', ['lat' => '4.75209473', 'lng' => '-74.08867971']);
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());

        $this->assertObjectHasAttribute('zone_id', $response->result->store);
        $this->assertInternalType('array', $response->result->store->departments);
        $this->assertObjectHasAttribute('image_list_app_url', $response->result->store->departments[0]);
        $this->assertNotNull($response->result->store->departments[0]->image_list_app_url);
    }

    public function testWithAValidCoordinates()
    {
        $user = \User::orderBy('id', 'desc')->first();
        $url = action(
            'api\v13\ApiStoreController@get_store',
            ['lat' => '4.75209473', 'lng' => '-74.08867971', 'user_id' => $user->id]
        );
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertObjectHasAttribute('departments', $response->result->store);
        $this->assertObjectHasAttribute('banners', $response->result->store);
        $this->assertObjectHasAttribute('zone_id', $response->result->store);
    }

    public function testGetDeliveryTimes()
    {
        \Route::enableFilters();
        $store = $this->newStore();
        $warehouse = $this->newWarehouse($store->city);
        $slot = $this->newSlot($store, Carbon::create());
        $zone = $warehouse->zones()->first();
        $this->newSlotZone($zone, $slot);

        $storeProduct = $this->getStoreProduct($store);
        $storeProduct->save();
        $this->stockOnWarehouse($warehouse, $storeProduct);

        $cart = [
            [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false
                    ]
                ]
            ]
        ];

        $zone_id = $zone->id;
        $url = action('api\v13\ApiCheckoutController@get_delivery_times', $store->id);
        $request = $this->call('POST', $url, compact('cart', 'zone_id'));
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertNotEmpty($response->result);
        $slot = $response->result[0];

        $objectAttributes = ['day', 'date', 'value', 'times'];
        foreach ($objectAttributes as $attribute) {
            $this->assertObjectHasAttribute($attribute, $slot);
        }
        $this->assertNotEmpty($slot->times);
        $times = $slot->times[0];
        $objectAttributes = ['time', 'value', 'delivery_amount'];
        foreach ($objectAttributes as $attribute) {
            $this->assertObjectHasAttribute($attribute, $times);
        }
    }

    public function testGetDeliveryTimesWithDeliveryAmount()
    {
        \Route::enableFilters();
        $totalProducts = 10;
        $store = $this->newStore();
        $warehouse = $this->newWarehouse($store->city);
        $slot = $this->newSlot($store, Carbon::create());
        $zone = $warehouse->zones()->first();
        $this->newSlotZone($zone, $slot);
        $cart = [];

        $discount = $this->setMinimumDeliveryDiscount($warehouse, $store);
        $product_value = $discount->minimum_order_amount / $totalProducts;

        foreach (range(1, $totalProducts) as $item) {
            $storeProduct = $this->productWithStock($store, $warehouse);
            $storeProduct->price = $product_value;
            $storeProduct->save();

            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false
                    ]
                ]
            ];
        }

        $zone_id = $zone->id;
        $url = action('api\v13\ApiCheckoutController@get_delivery_times', $store->id);
        $request = $this->call('POST', $url, compact('cart', 'zone_id'));
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertNotEmpty($response->result[0]->times);
        foreach ($response->result as $date) {
            foreach ($date->times as $time) {
                $this->assertEquals(0, $time->delivery_amount);
            }
        }
    }

    public function testGetDeliveryTimesWithNoDeliveryAmount()
    {
        \Route::enableFilters();
        $totalProducts = 10;
        $store = $this->newStore();
        $warehouse = $this->newWarehouse($store->city);
        $slot = $this->newSlot($store, Carbon::create());
        $zone = $warehouse->zones()->first();
        $this->newSlotZone($zone, $slot);
        $cart = [];

        $order = $this->makeFakeOrder();
        $user = $order->user;

        $discount = $this->setMinimumDeliveryDiscount($warehouse, $store);
        $product_value = $discount->minimum_order_amount / $totalProducts / 2;

        foreach (range(1, $totalProducts) as $item) {
            $storeProduct = $this->productWithStock($store, $warehouse);
            $storeProduct->price = $product_value;
            $storeProduct->save();

            $cart[] = [
                'id' => $storeProduct->id,
                'details' => [
                    [
                        'price' => $storeProduct->price,
                        'quantity' => 1,
                        'has_special_price' => false
                    ]
                ]
            ];
        }

        $zone_id = $zone->id;
        $user_id = $user->id;
        $url = action('api\v13\ApiCheckoutController@get_delivery_times', $store->id);
        $request = $this->call('POST', $url, compact('cart', 'zone_id', 'user_id'));
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertNotEmpty($response->result[0]->times);
        foreach ($response->result as $date) {
            foreach ($date->times as $time) {
                $this->assertNotEquals(0, $time->delivery_amount);
            }
        }
    }
}
