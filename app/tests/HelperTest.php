<?php

namespace Tests;

use Carbon\Carbon;

/**
 * Class HelperTest
 * @package Tests
 */
class HelperTest extends TestCase
{

    public function testSumOnCurrentDate()
    {
        // Lunes 9 de abril
        $carbon = Carbon::create(2018, 4, 9, 7);
        Carbon::setTestNow($carbon);
        $result = get_delivery_date_marketplace($carbon, 48);
        // Miercoles 11 de abril
        $this->assertEquals(
            $this->getFormat(Carbon::create(2018, 4, 11, 7)),
            $this->getFormat($result)
        );
    }

    public function testSumOnValidMargin()
    {
        // Lunes 9 de abril
        $carbon = Carbon::create(2018, 4, 9, 7);
        Carbon::setTestNow($carbon);

        // Jueves 12 de abril
        $result = get_delivery_date_marketplace(Carbon::create(2018, 4, 12, 7), 48);
        $this->assertEquals(
            $this->getFormat(Carbon::create(2018, 4, 12, 7)),
            $this->getFormat($result)
        );
    }

    public function testSumOnWithHolidays()
    {
        // Viernes 13 de abril
        $carbon = Carbon::create(2018, 4, 13, 7);
        Carbon::setTestNow($carbon);

        $result = get_delivery_date_marketplace($carbon, 48);
        // Martes 17 de abril
        $this->assertEquals(
            $this->getFormat(Carbon::create(2018, 4, 17, 7)),
            $this->getFormat($result)
        );
    }

    public function testGetOnWeekend()
    {
        // Lunes 9 de abril
        $carbon = Carbon::create(2018, 4, 9, 7);
        Carbon::setTestNow($carbon);
        // Sabado 14 de abril
        $newDate = Carbon::create(2018, 4, 14, 7);

        $result = get_delivery_date_marketplace($newDate, 48);
        $this->assertEquals(
            $this->getFormat($newDate),
            $this->getFormat($result)
        );
    }

    public function testGetOnNextDay()
    {
        // Martes 2 de enero
        $carbon = Carbon::create(2018, 1, 2, 7);
        Carbon::setTestNow($carbon);
        // Miercoles 3 de enero
        $newDate = Carbon::create(2018, 1, 3, 7);

        // Jueves 4 de enero
        $result = get_delivery_date_marketplace(Carbon::create(2018, 1, 4, 7), 48);
        $this->assertEquals(
            $this->getFormat($newDate->addDay()),
            $this->getFormat($result)
        );
    }

    public function testGetOnWeekendWithMoreHours()
    {
        // Viernes 13 de enero
        $carbon = Carbon::create(2018, 1, 13, 7);
        Carbon::setTestNow($carbon);

        // Sabado 14 de enero
        $result = get_delivery_date_marketplace(Carbon::create(2018, 1, 14, 7), 48);
        $this->assertEquals(
            // Martes 16 de enero
            $this->getFormat(Carbon::create(2018, 1, 16, 7)),
            $this->getFormat($result)
        );
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function getFormat(Carbon $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
