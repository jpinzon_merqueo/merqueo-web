<?php

namespace Tests;

use Carbon\Carbon;
use Config;
use Crypt_AES;
use DB;
use Faker\Factory;
use Hash;
use Order;
use Route;
use Tests\Traits\MerqueoFactory;
use User;

class OrderDeviceTest extends TestCase
{
    use MerqueoFactory;

    protected $ignore = false;
    protected $url;
    protected $method;
    protected $headers;
    protected $data;
    protected $os_elements = ['Android', 'iOS'];

    protected $faker;

    public function setUp()
    {
        parent::setUp();
        Route::enableFilters();
        DB::beginTransaction();
        $this->faker = Factory::create();
        $_SERVER['REMOTE_ADDR'] = '0.0.0.0';
    }

    public function tearDown()
    {
        parent::setUp();
        // Se modifican algunas contraseñas y usuarios, no
        // se debe remover el rollback
        DB::rollback();
        //DB::commit();
    }

    /**
     * Inicializar datos
     */
    private function init()
    {
        $this->url = Config::get('app.url').'/api/1.3/checkout';
        $this->method = 'POST';
        $this->data['email'] = 'test' . time() . '@gmail.com';

        $char = "0123456789";
        $char = str_shuffle($char);
        for ($i = 0, $rand = '', $l = strlen($char) - 1; $i <= 9; $i++) {
            $rand .= $char{mt_rand(0, $l)};
        }
        $this->data['phone'] = $rand;

    }

    /**
     * Hacer request a servicio
     *
     * @param array $data Datos a enviar
     * @return array $response Respuesta
     */
    protected function makeRequest($data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response['content'] = curl_exec($ch);
        $response['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $response;
    }

    /**
     * Test de pedidos
     *
     * @return void
     */
    public function testOrder()
    {
        if ($this->ignore)
            return;

        $this->init();

        $this->headers = [
            'Authorization: ' . $this->setToken(),
        ];

        //USUARIO Y DIRECCION NUEVA CON PAGO EN EFECTIVO Y CUPON

        $response_result = false;
        $warehouse = \Warehouse::first();

        $data = [
            'source_os' => 'iOS',
            'create_user' => '1',
            'first_name' => 'Farid',
            'last_name' => 'Farfan',
            'email' => $this->data['email'],
            'password' => '123456',
            'phone' => $this->data['phone'],
            'address' => 'Calle 127 # 12-7',
            'address_name' => 'Apartamento',
            'address_further' => 'BQ 9 AP 102',
            'other_name' => '',
            'dir' => ['Calle', '', '127', '', '#', '', '12', '-', '7'],
            'lat' => '4.69937494',
            'lng' => '-74.05803687',
            'city' => 'bogota',
            'comments_further' => '',
            'comments' => '',
            'delivery_day' => date('Y-m-d', strtotime('+2 day')),
            'delivery_time' => '2',
            'coupon_code' => 'wikimerqueo',
            'payment_method' => 'Efectivo',
            'app_version' => '2.1.40',
            'cart' => '{"612":{"qty":1,"price":4690,"name":"Chocolate de mesa pastilla Sol"},"1147":{"qty":1,"price":4490,"name":"Chocolate Sol 50% menos azúcar x 16"},"123":{"qty":1,"price":3990,"name":"Chocolate de mesa pastilla"},"1075":{"qty":1,"price":1590,"name":"Leche entera"},"841":{"qty":1,"price":15090,"name":"Leche entera Alquería x 6"},"1324":{"qty":2,"price":14590,"name":"Modificador de leche Milo Nutri Fit Extracontenido"}}',
        ];

        foreach (json_decode($data['cart']) as $product_id => $values) {
            $this->newVisibleProduct(\StoreProduct::find($product_id)->product, $warehouse);
        }

        $response = $this->makeRequest($data);

        echo '
---------------------------------------------------------------------------
Pedido atomico - Pago en efectivo y cupon

REQUEST URL: ' . $this->url . ' ' . $this->method . '
RESPONSE CODE: ' . $response['httpcode'] . '
RESPONSE CONTENT: ' . $response['content'] . '
---------------------------------------------------------------------------';

        if ($response['httpcode'] == 200) {
            $response_result = true;
            $response = json_decode($response['content'], true);
            if (!$response['status']) {
                $this->assertTrue($response['status']);
            }
        }
        $this->assertTrue($response_result);


        //USUARIO Y DIRECCION NUEVA CON PAGO CON TARJETA

        $this->init();

        $response_result = false;

        $data = [
            'source_os' => 'iOS',
            'create_user' => '1',
            'first_name' => 'Maria',
            'last_name' => 'Gomez',
            'email' => $this->data['email'],
            'password' => '123456',
            'phone' => $this->data['phone'],
            'address' => 'Calle 127 # 12-7',
            'address_name' => 'Apartamento',
            'address_further' => 'BQ 9 AP 102',
            'other_name' => '',
            'dir' => ['Calle', '', '127', '', '#', '', '12', '-', '7'],
            'lat' => '4.69937494',
            'lng' => '-74.05803687',
            'city' => 'bogota',
            'comments_further' => '',
            'comments' => '',
            'delivery_day' => date('Y-m-d', strtotime('+2 day')),
            'delivery_time' => '4',
            'coupon_code' => '',
            'payment_method' => 'Tarjeta de crédito',
            'name_cc' => 'APPROVED',
            'number_cc' => '4111111111111111',
            'code_cc' => '123',
            'expiration_year_cc' => '2024',
            'expiration_month_cc' => '4',
            'document_number_cc' => '72584896',
            'address_cc' => 'Cra 23 45 67',
            'phone_cc' => '3025896',
            'installments_cc' => '1',
            'app_version' => '2.1.40',
            'cart' => '{"612":{"qty":1,"price":4690,"name":"Chocolate de mesa pastilla Sol"},"1147":{"qty":1,"price":4490,"name":"Chocolate Sol 50% menos azúcar x 16"},"123":{"qty":1,"price":3990,"name":"Chocolate de mesa pastilla"},"1075":{"qty":1,"price":1590,"name":"Leche entera"},"841":{"qty":1,"price":15090,"name":"Leche entera Alquería x 6"},"1324":{"qty":2,"price":14590,"name":"Modificador de leche Milo Nutri Fit Extracontenido"}}',
        ];

        $response = $this->makeRequest($data);

        echo '
---------------------------------------------------------------------------
Pedido atomico - Pago con tarjeta

REQUEST URL: ' . $this->url . ' ' . $this->method . '
RESPONSE CODE: ' . $response['httpcode'] . '
RESPONSE CONTENT: ' . $response['content'] . '
---------------------------------------------------------------------------';

        if ($response['httpcode'] == 200) {
            $response_result = true;
            $response = json_decode($response['content'], true);
            if (!$response['status']) {
                $this->assertTrue($response['status']);
            }
        }
        $this->assertTrue($response_result);


        //USUARIO LOGUEADO Y DIRECCION NUEVA CON PAGO CON TARJETA NUEVA

        $response_result = false;

        $data = [
            'source_os' => 'Android',
            'user_id' => '15028',
            'address' => 'Calle 127 # 12-7',
            'address_name' => 'Apartamento',
            'address_further' => 'BQ 9 AP 102',
            'other_name' => '',
            'dir' => ['Calle', '', '127', '', '#', '', '12', '-', '7'],
            'lat' => '4.69937494',
            'lng' => '-74.05803687',
            'city' => 'bogota',
            'comments_further' => '',
            'comments' => '',
            'delivery_day' => date('Y-m-d', strtotime('+2 day')),
            'delivery_time' => '1',
            'coupon_code' => '',
            'payment_method' => 'Tarjeta de crédito',
            'document_type_cc' => 'CC',
            'document_number_cc' => '725845844',
            'phone_cc' => '3002658899',
            'address_cc' => 'Calle 127 # 12-7 BQ 9 AP 102',
            'name_cc' => 'APPROVED',
            'number_cc' => '4111111111111111',
            'code_cc' => '123',
            'expiration_year_cc' => '2024',
            'expiration_month_cc' => '4',
            'document_number_cc' => '72584896',
            'address_cc' => 'Cra 23 45 67',
            'phone_cc' => '3025896',
            'installments_cc' => '1',
            'app_version' => '2.1.40',
            'cart' => '{"612":{"qty":1,"price":4690,"name":"Chocolate de mesa pastilla Sol"},"1147":{"qty":1,"price":4490,"name":"Chocolate Sol 50% menos azúcar x 16"},"123":{"qty":1,"price":3990,"name":"Chocolate de mesa pastilla"},"1075":{"qty":1,"price":1590,"name":"Leche entera"},"841":{"qty":1,"price":15090,"name":"Leche entera Alquería x 6"},"1324":{"qty":2,"price":14590,"name":"Modificador de leche Milo Nutri Fit Extracontenido"}}',
        ];

        $response = $this->makeRequest($data);

        echo '
---------------------------------------------------------------------------
Pedido logueado - Direccion nueva con pago con tarjeta nueva

REQUEST URL: ' . $this->url . ' ' . $this->method . '
RESPONSE CODE: ' . $response['httpcode'] . '
RESPONSE CONTENT: ' . $response['content'] . '
---------------------------------------------------------------------------';

        if ($response['httpcode'] == 200) {
            $response_result = true;
            $response = json_decode($response['content'], true);
            if (!$response['status']) {
                $this->assertTrue($response['status']);
            }
        }
        $this->assertTrue($response_result);


        //USUARIO LOGUEADO Y DIRECCION GUARDADA CON PAGO CON TARJETA GUARDADA

        $response_result = false;

        $data = [
            'source_os' => 'Android',
            'user_id' => '153733',
            'address_id' => '121043',
            'comments_further' => '',
            'comments' => '',
            'delivery_day' => date('Y-m-d', strtotime('+2 day')),
            'delivery_time' => '1',
            'coupon_code' => '',
            'payment_method' => 'Tarjeta de crédito',
            'credit_card_id' => '12796',
            'installments_cc' => '1',
            'app_version' => '2.1.40',
            'cart' => '{"612":{"qty":1,"price":4690,"name":"Chocolate de mesa pastilla Sol"},"1147":{"qty":1,"price":4490,"name":"Chocolate Sol 50% menos azúcar x 16"},"123":{"qty":1,"price":3990,"name":"Chocolate de mesa pastilla"},"1075":{"qty":1,"price":1590,"name":"Leche entera"},"841":{"qty":1,"price":15090,"name":"Leche entera Alquería x 6"},"1324":{"qty":2,"price":14590,"name":"Modificador de leche Milo Nutri Fit Extracontenido"}}',
        ];

        $response = $this->makeRequest($data);

        echo '
---------------------------------------------------------------------------
Pedido logueado - Direccion guardada con pago con tarjeta guardada

REQUEST URL: ' . $this->url . ' ' . $this->method . '
RESPONSE CODE: ' . $response['httpcode'] . '
RESPONSE CONTENT: ' . $response['content'] . '
---------------------------------------------------------------------------';

        if ($response['httpcode'] == 200) {
            $response_result = true;
            $response = json_decode($response['content'], true);
            if (!$response['status']) {
                $this->assertTrue($response['status']);
            }
        }
        $this->assertTrue($response_result);

    }

    /**
     * Init en apps
     */
    public function testApiIndexInit()
    {
        if ($this->ignore)
            return;

        $action = action('api\v13\ApiIndexController@init');
        $payload = [
            'lat' => $this->faker->latitude(),
            'lng' => $this->faker->longitude(),
            'brand' => $this->faker->name,
            'model' => $this->faker->name,
            'os' => $this->faker->randomElement($this->os_elements),
            'os_api' => $this->faker->name,
            'regid' => $this->faker->uuid,
            'os_version' => '1.3',
            'app_version' => 214,
            'api_version' => '1.3',
        ];
        $this->setToken($action);
        $request = $this->call('POST', $action, $payload);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Datos guardados y obtenidos.');
    }

    /**
     * Servicio obtener tienda con coordenada
     */
    public function testApiStoreGetStore()
    {
        if ($this->ignore)
            return;

        $lat = '4.714999';
        $lng = '-74.135100';
        $action = action('api\v13\ApiStoreController@get_store', compact('lat', 'lng'));
        $this->setToken($action);
        $request = $this->call('GET', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Se encontró una tienda dentro de la ubicación');
    }

    /**
     * Servicio buscar productos
     */
    public function testApiStoreSearchResultWithQuery()
    {
        if ($this->ignore)
            return;

        $id = \StoreProductWarehouse::whereStatus(1)->first()->storeProduct->store->id;
        $q = 'jugo de';
        $action = action('api\v13\ApiStoreController@search', compact('id', 'q'));
        $this->setToken($action);
        $request = $this->call('GET', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Resultado de busqueda');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio obtener departamento y productos
     */
    public function testApiStoreGetDepartment()
    {
        if ($this->ignore)
            return;

        $result = DB::select(DB::raw(
            'SELECT
                `departments`.`id`,
                `departments`.`store_id`
            FROM
                `departments`
                    INNER JOIN
                `store_products` ON `store_products`.`department_id` = `departments`.`id`
                    INNER JOIN
                `products` ON `store_products`.`product_id` = `products`.`id`
                    INNER JOIN
                `store_product_warehouses` ON `store_products`.`id` = `store_product_warehouses`.`store_product_id`
            WHERE
                `store_product_warehouses`.`status` = 1
                    AND `departments`.`status` = 1
                    AND IF(
                      store_product_warehouses.manage_stock = 1,
                            IF(store_product_warehouses.is_visible_stock = 0,
                              IF( store_product_warehouses.current_stock > 0, TRUE, FALSE
                          ), TRUE
                      ), TRUE)
                    AND `products`.`type` <> \'Proveedor\'
            GROUP BY `departments`.`id`
            LIMIT 1;'
        ))[0];

        $store_id = $result->store_id;
        $department_id = $result->id;
        $action = action(
            'api\v13\ApiStoreController@get_department',
            compact('store_id', 'department_id')
        );
        $this->setToken($action);
        $request = $this->call('GET', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Departamento obtenido');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio obtener producto
     */
    public function testApiStoreGetProduct()
    {
        if ($this->ignore)
            return;

        $storeProduct = $this->productWithStock();
        $id = $storeProduct->id;
        $warehouse = $storeProduct->storeProductWarehouses[0]->warehouse;
        $zone = $this->newZone($warehouse);
        $zone_id = $zone->id;
        $action = action('api\v13\ApiStoreController@get_product', compact('id', 'zone_id'));
        $this->setToken($action);
        $request = $this->call('GET', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Producto obtenido');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio login
     */
    public function testApiUserLogin()
    {
        if ($this->ignore)
            return;

        $password = '123456';
        $user = User::where('status', 1)->has('orders.orderGroup')->with('orders.orderGroup')->first();
        $user->password = Hash::make($password);
        $user->save();

        $email = $user->email;
        $action = action('api\v13\ApiUserController@login', compact('email', 'password'));
        $this->setToken($action);
        $request = $this->call('POST', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Sesion iniciada');
        $this->assertNotEmpty($content->result->user->user->oauth_verifier);
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio login con fb
     */
    public function testApiUserSocial()
    {
        if ($this->ignore)
            return;

        $user = User::whereNotNull('fb_id')->where('fb_id', '<>', '0')->first();
        $fb_id = $user->fb_id;
        $user_id = $user->id;
        $action = action('api\v13\ApiUserController@social', compact('fb_id', 'user_id'));
        $this->setToken($action);
        $request = $this->call('POST', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Cuenta asociada');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio obtener estado de pedido de usuario
     */
    public function testApiUserGetOrderStatus()
    {
        if ($this->ignore)
            return;

        $order = Order::has('user')->with('user')->first();
        $order->delivery_date = Carbon::create()->addDay();
        $order->save();
        $user_id = $order->user->id;
        $order_id = $order->id;
        $action = action('api\v13\ApiUserController@get_order_status', compact('user_id', 'order_id'));
        $this->setToken($action);
        $request = $this->call('GET', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Flujo de pedido obtenido');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio obtener pedidos de usuario
     */
    public function testApiUserGetOrders()
    {
        if ($this->ignore)
            return;

        $order = Order::has('user')->with('user')->first();
        $id = $order->user->id;
        $action = action('api\v13\ApiUserController@get_orders', compact('id'));
        $this->setToken($action);
        $request = $this->call('GET', $action);
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Pedidos obtenidos');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio checkout obtener metodos de pago y horario de entrega por default
     */
    public function testApiCheckoutDeliveryTime()
    {
        if ($this->ignore)
            return;

        $user = User::first();
        $user_id = $user->id;
        $cart = '{"612":{"qty":1,"price":4690,"name":"Chocolate de mesa pastilla Sol"},"1147":{"qty":1,"price":4490,"name":"Chocolate Sol 50% menos azúcar x 16"},"123":{"qty":1,"price":3990,"name":"Chocolate de mesa pastilla"},"4583":{"qty":2,"price":5490,"name":"Chocolate de mesa pastilla Luker Amargo clavos y canela"},"1075":{"qty":1,"price":1590,"name":"Leche entera"},"841":{"qty":1,"price":15090,"name":"Leche entera Alquería x 6"},"1324":{"qty":2,"price":14590,"name":"Modificador de leche Milo Nutri Fit Extracontenido"}}';

        $action = action('api\v13\ApiCheckoutController@delivery_time');
        $this->setToken($action);
        $request = $this->call('POST', $action, compact('cart', 'user_id'));
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Horario de tienda obtenido.');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio checkout obtener horarios de entrega
     */
    public function testApiCheckoutDeliveryDates()
    {
        if ($this->ignore)
            return;

        $user = User::first();
        $user_id = $user->id;
        $cart = json_decode('[{"details":[{"id":4378,"has_special_price":false,"price":10190,"quantity":"1"}],"id":4378},{"details":[{"id":731,"has_special_price":false,"price":8490,"quantity":"1"}],"id":731},{"details":[{"id":9588,"has_special_price":false,"price":14890,"quantity":"1"}],"id":9588},{"details":[{"id":9450,"has_special_price":false,"price":15990,"quantity":"1"}],"id":9450},{"details":[{"id":6994,"has_special_price":false,"price":14290,"quantity":"1"}],"id":6994},{"details":[{"id":9451,"has_special_price":false,"price":10490,"quantity":"1"}]}]', true);
        $store = 63;
        $zone_id = 36;
        $latitude = 19.427508;
        $longitude = -99.151981;
        $action = action('api\v13\ApiCheckoutController@get_delivery_dates', $store);
        $this->setToken($action);
        $request = $this->call('POST', $action, compact('cart', 'user_id', 'zone_id', 'latitude', 'longitude'));
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Ok');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Servicio hacer pedido usuario logueado
     */
    public function testApiCheckout()
    {
        if ($this->ignore)
            return;

        $user = User::has('userAddresses')->with('userAddresses')->where('status', 1)->first();
        $warehouse = \Warehouse::first();
        $user_id = $user->id;
        $address_id = $user->userAddresses[0]->id;
        $delivery_day = Carbon::create()->addDays(2);
        $delivery_window = \SlotWarehouse::where('day', $delivery_day->dayOfWeek)
            ->first()
            ->deliveryWindow;

        $delivery_time = $delivery_window->id;
        $delivery_day = $delivery_day->toDateString();
        $payment_method = 'Other';
        $source_os = 'android';
        $app_version = '2.1.34';
        $product = \Product::first();
        $storeProduct = $this->newVisibleProduct($product, $warehouse);
        $cart = '{"' . $storeProduct->id . '":{"qty":2,"price":46900,"name":"Chocolate de mesa pastilla Sol"}}';

        $action = action('api\v13\ApiCheckoutController@checkout');
        $this->setToken($action);
        $request = $this->call(
            'POST',
            $action,
            compact(
                'cart',
                'user_id',
                'address_id',
                'payment_method',
                'delivery_day',
                'delivery_time',
                'source_os',
                'app_version'
            )
        );
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals('Pedido registrado', $content->message);
        $this->assertNotEmpty($content->result);
        $this->clearToken();
    }

    /**
     * Servicio obtener carrito de usuario logeado
     */
    public function testGetCart()
    {
        if ($this->ignore)
            return;

        $user = User::has('userAddresses')->with('userAddresses')->first();
        $user_id = $user->id;
        $action = action('api\v13\ApiCheckoutController@get_cart');
        $this->setToken($action);
        $cart = '{"612":{"qty":1,"price":4690,"name":"Chocolate de mesa pastilla Sol"},"1147":{"qty":1,"price":4490,"name":"Chocolate Sol 50% menos azúcar x 16"},"123":{"qty":1,"price":3990,"name":"Chocolate de mesa pastilla"},"4583":{"qty":2,"price":5490,"name":"Chocolate de mesa pastilla Luker Amargo clavos y canela"},"1075":{"qty":1,"price":1590,"name":"Leche entera"},"841":{"qty":1,"price":15090,"name":"Leche entera Alquería x 6"},"1324":{"qty":2,"price":14590,"name":"Modificador de leche Milo Nutri Fit Extracontenido"}}';
        $request = $this->call(
            'POST',
            $action,
            compact(
                'cart',
                'user_id',
                'address_id',
                'payment_method',
                'delivery_day',
                'delivery_time'
            )
        );
        $this->assertResponseOk();
        $content = json_decode($request->getContent());
        $this->assertEquals($content->message, 'Información del carrito obtenida.');
        $this->assertNotEmpty($content->result);
    }

    /**
     * Pagina de checkout en página web con usuario no logueado
     */
    public function testCheckoutPage()
    {
        if ($this->ignore)
            return;

        $this->setFakeSessionAndCart();

        $crawler = $this->client->request('GET', '/checkout');

        $this->assertResponseStatus(200);

        $this->assertSessionHas('tmp_cart');
        $this->assertSessionHas('choose_city');
        $this->assertSessionHas('city_id');
        $this->assertSessionHas('cart_id');
        $this->assertSessionHas('zone_id');
        $this->assertSessionHas('warehouse_id');
        $this->assertSessionHas('store_id');
        $this->assertSessionHas('coverage');
        $this->assertSessionHas('address');

        $this->assertCount(1, $crawler->filter('h3:contains("Resumen de tu Pedido")'));
        $this->assertCount(1, $crawler->filter('h3:contains("¿Ya tienes cuenta?")'));
    }

    /**
     * Pedido en pagina web con usuario no logueado
     */
    public function testToPlaceOrderNoAuthenticatedUser()
    {
        if ($this->ignore)
            return;

        $session = $this->setFakeSessionAndCart(null, [
            'phoneValidation.validated' => true,
            'phoneValidation.number'    => '1998458211',
        ]);

        $addressFragments = explode(',', str_replace(' ', ', ,', $session['address']->address_text));

        $this->route('POST', 'frontCheckout.checkout', [
            'create_user'                          => true,
            'first_name'                           => 'John',
            'last_name'                            => 'Doe',
            'email'                                => 'john@doe.com',
            'password'                             => '123456',
            "delivery_day_{$session['store_id']}"  => Carbon::now()->addDay()->toDateString(),
            "delivery_time_{$session['store_id']}" => 4,
            'payment_method'                       => 'Efectivo',
            'dir'                                  => $addressFragments,
            'city_id'                              => $session['city_id'],
            'address_name'                         => 'Casa',
            'address_further'                      => 'Torre 12, apto 445',
            'address_neighborhood'                 => 'Bel Air',
        ]);

        $createdOrder = \Order::orderBy('id', 'desc')->take(1)->get(['reference'])->first();

        // debemos ser redirigidos a página de orden confirmada, la referencia
        // de la orden es fijada en url
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo("checkout/orden_confirmada/$createdOrder->reference?new=");
    }

    /**
     * Pedido en pagina web con usuario logueado
     */
    public function testToPlaceOrderAuthenticatedUser()
    {
        if ($this->ignore)
            return;

        $user = User::first();
        $this->be($user);
        $session = $this->setFakeSessionAndCart($user);

        $this->route('POST', 'frontCheckout.checkout', [
            'delivery_day_'.$session['store_id'] => Carbon::now()->addDay()->toDateString(),
            'delivery_time_'.$session['store_id'] => 4,
            'payment_method' => 'Efectivo',
            'address_id' => $session['address']->id
        ]);

        $order = \Order::orderBy('id', 'desc')->take(1)->get(['reference'])->first();

        // debemos ser redirigidos a página de orden confirmada, la referencia
        // de la orden es fijada en url
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo("checkout/orden_confirmada/$order->reference?old=");
    }

    /**
     * Fija variables de sesión requeridas por la página de checkout
     *
     * @param null $user
     * @return array
     */
    private function setFakeSessionAndCart($user = null, $append = [])
    {
        // Datos del carro de compras en la sesión.
        $cart = [
            "warehouse_id" => 1,
            "products"     => [
                "695" => [
                    "cart_id"                  => "0",
                    "store_product_id"         => "612",
                    "cart_quantity"            => "1",
                    "cart_quantity_full_price" => 0,
                    "added_by"                 => 0,
                ],
                "286" => [
                    "cart_id"                  => "0",
                    "store_product_id"         => "1324",
                    "cart_quantity"            => "2",
                    "cart_quantity_full_price" => 0,
                    "added_by"                 => 0,
                ],
                "1147" => [
                    "cart_id"                  => "0",
                    "store_product_id"         => "1147",
                    "cart_quantity"            => "2",
                    "cart_quantity_full_price" => 0,
                    "added_by"                 => 0,
                ],
            ],
        ];

        $sessionData = [
            'tmp_cart' => json_encode($cart),
            'choose_city' => true,
            'city_id' => 1,
            'cart_id' => 0,
            'zone_id' => 148,
            'warehouse_id' => 1,
            'store_id' => 63,
            'coverage' => 1,
        ];

        if (empty($user)) {
            $tempAddress = new \stdClass();
            $tempAddress->city_id = '1';
            $tempAddress->surrounded_city_id = '0';
            $tempAddress->address = '{"dir1":"Calle","dir2":"157","dir3":"12","dir4":"34"}';
            $tempAddress->address_text = 'Calle 157 # 7 - 54';
            $tempAddress->latitude = '4.73343563';
            $tempAddress->longitude = '-74.0265509';
            $sessionData['address'] = $tempAddress;
        }else{
            $sessionData['address'] = (object) $user->addresses->first()->toArray();
        }

        $sessionData = $sessionData + $append;

        $this->session($sessionData);

        return $sessionData;
    }
}
