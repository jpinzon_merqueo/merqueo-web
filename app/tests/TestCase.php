<?php

namespace Tests;

use Config;
use Crypt_AES;
use Illuminate\Auth\UserInterface;
use \Illuminate\Foundation\Testing\TestCase as LaravelTestCase;

class TestCase extends LaravelTestCase
{

    /**
     * Creates the application.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        date_default_timezone_set('America/Bogota');

        $_SERVER['HTTP_X_FORWARDED_FOR'] = '0.0.0.0';

        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__ . '/../../bootstrap/start.php';
    }

    /**
     * Al realizar las peticiones con el metodo "call" no se asignan las
     * cabeceras o no son accesibles en la variable global $_SERVER.
     *
     * @todo Crear un metodo donde se asignen las cabeceras de forma dinamica.
     * @param $url
     * @return
     */
    protected function setToken($url = '/api/1.3/checkout')
    {
        $url = str_replace(Config::get('app.url'), '', $url);
        if ($temp_url = strstr($url, '?', true))
            $url = $temp_url;

        $config = [
            'KEY' => Config::get('app.api.key'),
            'PUBLIC_KEY' => Config::get('app.api.public_key'),
            'IV' => Config::get('app.api.iv'),
            'URL' => Config::get('app.url'),];

        $timestamp = strtotime(date('Y-m-d H:i:s'));
        $token = $timestamp . '|'.$url.'|' . $config['KEY'];
        $aes = new Crypt_AES(CRYPT_MODE_CBC);
        $aes->setKey($config['PUBLIC_KEY']);
        $aes->setIV($config['IV']);
        $token = $aes->encrypt($token);
        $token = base64_encode($token);

        $_SERVER['HTTP_AUTHORIZATION'] = $token;

        return $token;
    }

    protected function clearToken()
    {
        $_SERVER['HTTP_AUTHORIZATION'] = '';
    }

    /**
     * @param \Cart $cart
     * @param \StoreProduct $product
     * @return \CartProduct
     */
    public function addProductToCart(\Cart $cart, \StoreProduct $product)
    {
        $product->load('product');
        $pCart = new \CartProduct();
        $pCart->cart()->associate($cart);
        $pCart->storeProduct()->associate($product);
        $pCart->quantity = $product->quantity_special_price ?: 5;
        $pCart->quantity_full_price = 0;
        $pCart->price = $product->special_price ?: $product->price;
        $pCart->product_name = !empty($product->product) ? $product->product->name : 'XXXX';
        $pCart->comment = 'XXXXX';
        $pCart->added_by = $cart->user->id;
        $pCart->save();

        return $pCart;
    }

    /**
     * @param \Warehouse $warehouse
     * @param \Zone|null $zone
     * @return \Zone
     */
    public function enableZone(\Warehouse $warehouse, \Zone $zone = null)
    {
        if (empty($zone)) {
            $zone = \Zone::first();
        }

        $zone->warehouse_id = $warehouse->id;
        $zone->save();

        return $zone;
    }

    /**
     * @param UserInterface $user
     * @param null $driver
     */
    public function beAdmin(UserInterface $user, $driver = null)
    {
        $this->app['backendauth']->driver($driver)->setUser($user);
    }
}
