<?php


namespace Tests\UseCases;


use app\objects\CreditDataObject;
use repositories\contracts\CreditRepositoryInterface;
use repositories\contracts\UserCreditRepositoryInterface;
use Tests\TestCase;
use usecases\credits\AddCreditToUserUsecase;
use UserCredit;

class AddCreditToUserCaseTest extends TestCase
{

    /**
     * @var \Mockery\MockInterface|UserCreditRepositoryInterface
     */
    private $userCreditRepository;

    public function setUp()
    {
        parent::setUp();

        $this->userCreditRepository = \Mockery::mock(UserCreditRepositoryInterface::class);
    }

    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown(); // TODO: Change the autogenerated stub
    }


    public function test_add_credit()
    {
        $usecase = new AddCreditToUserUsecase($this->userCreditRepository);
        $obj = new CreditDataObject(['amount'=>10000]);
        $credit = new UserCredit();
        $credit->amount = 10000;

        $this->userCreditRepository->shouldReceive('add')
            ->with($obj)
            ->andReturn($credit);

        $response = $usecase->handle($obj);

        $this->assertEquals($credit, $response);
    }


}