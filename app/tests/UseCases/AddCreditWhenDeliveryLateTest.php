<?php


namespace Tests\UseCases;

use app\objects\contracts\ObjectInterface;
use app\objects\CreditDataObject;
use Carbon\Carbon;
use contracts\SendNotificationInterface;
use Coupon;
use Credit;
use ExternalService;
use Mockery;
use Order;
use OrderGroup;
use repositories\contracts\ConfigurationRepositoryInterface;
use repositories\interfaces\OrderRepositoryInterface;
use repositories\contracts\UserCreditRepositoryInterface;
use Tests\TestCase;
use usecases\contracts\credits\AddCreditToUserUsecaseInterface;
use usecases\orders\AddCreditWhenDeliveryLate;
use User;
use UserCredit;

class AddCreditWhenDeliveryLateTest extends TestCase
{

    /**
     * @var \Mockery\MockInterface|ConfigurationRepositoryInterface
     */
    private $configurationRepository;

    /**
     * @var SendNotificationInterface|\Mockery\MockInterface
     */
    private $sendNotification;
    /**
     * @var \Mockery\MockInterface|UserCreditRepositoryInterface
     */
    private $userCreditRepository;
    /**
     * @var Mockery\MockInterface|OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var Mockery\MockInterface|AddCreditToUserUsecaseInterface
     */
    private $addCreditToUserUsecase;
    /**
     * @var CreditDataObject|Mockery\MockInterface
     */
    private $creditDataObject;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->configurationRepository = Mockery::mock(ConfigurationRepositoryInterface::class);
        $this->sendNotification = Mockery::mock(SendNotificationInterface::class);
        $this->userCreditRepository = Mockery::mock(UserCreditRepositoryInterface::class);
        $this->addCreditToUserUsecase = Mockery::mock(AddCreditToUserUsecaseInterface::class);
        $this->creditDataObject = Mockery::mock(CreditDataObject::class);
    }

    protected function tearDown()
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @dataProvider provider
     * @param $order
     * @param $userCredit
     * @param $enabled_distribute
     * @param $expected
     */
    public function test_order_verify(Order $order, $userCredit, $enabled_distribute, $expected)
    {
        // given
        $data_notification = [
            'message' => $userCredit->description,
            'order_id' => $order->id,
            'user_id' => $order->user->id,
            'type' => 'user_credits'
        ];

        $mail = [
            'template_name' => 'emails.credit_when_delivery_late',
            'subject' => 'Compensación llegada tarde',
            'to' => [['email' => $order->user->email, 'name' => $order->user->first_name . ' ' . $order->user->last_name]],
            'vars' => [
                'amount' => currency_format(10000),
                'name' => $order->user->first_name
            ]
        ];

        $date_expire = Carbon::now()->addYear(1);

        $data_credit = [
            'amount' => 10000,
            'expiration_date' => $date_expire,
            'object' => ['order_id' => $order->id],
            'user_id' => $order->user->id,
            'credit_id' => 1
        ];
        $objCredit = new CreditDataObject();

        // when
        $this->findConfigurations($enabled_distribute);
        $this->sendNotificationsMethods($order, $data_notification, $userCredit, $mail);
        $this->creditOperations($order, $userCredit, $date_expire, $data_credit, $objCredit);
        $usecases = new AddCreditWhenDeliveryLate($this->configurationRepository, $this->sendNotification, $this->creditDataObject, $this->addCreditToUserUsecase);
        $verify = $usecases->handle($order, $date_expire);

        // then
        $this->assertEquals($usecases->type_notification, 'user_credits');
        $this->assertGreaterThanOrEqual($order->diffBetweenDeliveryAndManagementDatesInMinutes(), 10);
        $this->assertEquals($expected, $verify);


    }

    /**
     * @param $order
     * @param array $data_notification
     * @param $credit
     * @param array $mail
     */
    private function sendNotificationsMethods($order, array $data_notification, $credit, array $mail)
    {
        $this->sendNotification->shouldReceive('sms')->with($credit->description, $order->orderGroup->user_phone);
        $this->sendNotification->shouldReceive('push')->with($order->orderGroup, $data_notification);
        $this->sendNotification->shouldReceive('mail')->with($mail);
    }

    /**
     * @param $enabled_distribute
     */
    private function findConfigurations($enabled_distribute)
    {
        $this->configurationRepository->shouldReceive('findByKey')->with('distribute_credit_late')->andReturn($enabled_distribute);
        $this->configurationRepository->shouldReceive('findByKey')->with('amount_credit_late')->andReturn(10000);
        $this->configurationRepository->shouldReceive('findByKey')->with('max_time_credit_late')->andReturn(10);
    }

    /**
     * @param $order
     * @param $userCredit
     * @param static $date_expire
     * @param array $data_credit
     * @param CreditDataObject $objCredit
     */
    private function creditOperations($order, $userCredit,  $date_expire, array $data_credit, CreditDataObject $objCredit)
    {
        $this->userCreditRepository->shouldReceive('create')->with($order->user, 10000, $date_expire, 'compensation', $order)->andReturn($userCredit);
        $this->creditDataObject->shouldReceive('create')->with($data_credit)->andReturn($objCredit);
        $this->addCreditToUserUsecase->shouldReceive('handle')->with($objCredit)->andReturn($userCredit);
    }

    public function provider()
    {
        $user = new User();
        $user->id=1;
        $user->email = 'test@merqueo.com';
        $user->first_name = 'Test';
        $user->last_name = 'Merqueo';
        $order_group = new OrderGroup();
        $user_credit = new UserCredit([
            'amount' => 10000,
        ]);

        $user_credit->credit = new Credit();
        $user_credit->credit->description = 'Descripcion :amount';
        $user_credit->credit->description_admin = 'Descripcion admin';
        $user_credit->user = $user;
        $user_credit->coupon = new Coupon();
        $user_credit->referrer = $user;
        $user_credit->external = new ExternalService();

        //-- GROUP TEST 1
        $orders[0]['order'] = new Order();
        $orders[0]['order']->user = $user;
        $orders[0]['order']->id = 1;
        $orders[0]['order']->status = 'Delivered';
        $orders[0]['order']->delivery_date = Carbon::now();
        $orders[0]['order']->management_date = Carbon::now()->addMinutes(10);
        $orders[0]['order']->orderGroup = $order_group;
        $orders[0]['userCredit'] = $user_credit;
        $orders[0]['enabled_distribute'] = 1;
        $user_credit->order = $orders[0]['order'];
        $orders[0]['result'] = true;

        //-- GROUP TEST 2
        $orders[1]['order'] = new Order();
        $orders[1]['order']->user = $user;
        $orders[1]['order']->id = 2;
        $orders[1]['order']->status = 'Delivered';
        $orders[1]['order']->delivery_date = Carbon::now();
        $orders[1]['order']->management_date = Carbon::now()->addMinutes(5);
        $orders[1]['order']->orderGroup = $order_group;
        $orders[1]['userCredit'] = $user_credit;
        $orders[1]['enabled_distribute'] = 1;
        $user_credit->order = $orders[1]['order'];
        $orders[1]['result'] = false;

        //-- GROUP TEST 3
        $orders[2]['order'] = new Order();
        $orders[2]['order']->user = $user;
        $orders[2]['order']->id = 3;
        $orders[2]['order']->status = 'Other';
        $orders[2]['order']->delivery_date = Carbon::now();
        $orders[2]['order']->management_date = Carbon::now()->addMinutes(10);
        $orders[2]['order']->orderGroup = $order_group;
        $orders[2]['userCredit'] = $user_credit;
        $orders[2]['enabled_distribute'] = 1;
        $user_credit->order = $orders[2]['order'];
        $orders[2]['result'] = false;

        //-- GROUP TEST 4
        $user = new User();
        $order_group = new OrderGroup();
        $user_credit = new UserCredit();
        $orders[3]['order'] = new Order();
        $orders[3]['order']->user = $user;
        $orders[3]['order']->id = 4;
        $orders[3]['order']->status = 'Delivered';
        $orders[3]['order']->delivery_date = Carbon::now();
        $orders[3]['order']->management_date = Carbon::now()->addMinutes(10);
        $orders[3]['order']->orderGroup = $order_group;
        $orders[3]['userCredit'] = $user_credit;
        $orders[3]['enabled_distribute'] = 0;
        $user_credit->order = $orders[3]['order'];
        $orders[3]['result'] = false;

        return $orders;
    }


}