<?php


namespace Tests\UseCases;


use repositories\Eloquent\EloquentDeliveryExpressRepository;
use Zone;
use Warehouse;
use repositories\Eloquent\EloquentZoneRepository;
use StoreProductWarehouse;
use SlotWarehouse;
use stdClass;
use Tests\TestCase;

class ValidateDeliveryExpressTest extends TestCase
{
    /**
     *  setUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->zoneRepository = new EloquentZoneRepository();
    }

    /**
     * @test
     */
    public function testFindZoneExpress()
    {
        $zonExpress = $this->zoneRepository->findZoneExpress();

        $this->assertInstanceOf(Zone::class, $zonExpress);
        $this->assertEquals(Warehouse::WAREHOUSE_DARKSUPERMARKET_ID[0], $zonExpress->warehouse_id);
    }

    /**
     * @test
    */
    public function testValidateDeliveryExpress()
    {
        $address = new stdClass();
        $address->latitude  = '4.68020415';
        $address->longitude = '-74.04341157';

        $zoneDark = $this->zoneRepository->findZoneExpress();

        $productExpressAvailable = StoreProductWarehouse::where('warehouse_id', $zoneDark->warehouse_id)
            ->available(true)
            ->first();

        $cartProductExpress = new stdClass();
        $cartProductExpress->store_product_id = $productExpressAvailable->store_product_id;
        $cartProductExpress->quantity = 1;
        $cartProductExpress->quantity_full_price = 0;

        $darkProducts[] = $cartProductExpress;

        $deliveryExpressRepository = new EloquentDeliveryExpressRepository();
        $validateDeliveryExpress = $deliveryExpressRepository->validateDeliveryExpress($address, $darkProducts, $zoneDark);

        $this->assertInstanceOf(SlotWarehouse::class, $validateDeliveryExpress);
        $this->assertEquals(Warehouse::WAREHOUSE_DARKSUPERMARKET_ID[0], $validateDeliveryExpress->warehouse_id);
        $this->assertEquals('EX', $validateDeliveryExpress->shifts);
    }

    /**
     * @test
     */
    public function testAddressNotCoverageDeliveryExpress()
    {
        $address = new stdClass();
        $address->latitude  = '5.68020415';
        $address->longitude = '5.04341157';

        $zoneDark = $this->zoneRepository->findZoneExpress();

        $productExpressAvailable = StoreProductWarehouse::where('warehouse_id', $zoneDark->warehouse_id)
            ->available(true)
            ->first();

        $cartProductExpress = new stdClass();
        $cartProductExpress->store_product_id = $productExpressAvailable->store_product_id;
        $cartProductExpress->quantity = 1;
        $cartProductExpress->quantity_full_price = 0;

        $darkProducts[] = $cartProductExpress;

        $deliveryExpressRepository = new EloquentDeliveryExpressRepository();
        $validateDeliveryExpress = $deliveryExpressRepository->validateDeliveryExpress($address, $darkProducts, $zoneDark);

        $this->assertEmpty($validateDeliveryExpress);
    }

    /**
     * @test
     */
    public function testProductsNotAvailableDeliveryExpress()
    {
        $address = new stdClass();
        $address->latitude  = '5.68020415';
        $address->longitude = '5.04341157';

        $zoneDark = $this->zoneRepository->findZoneExpress();

        $productExpressAvailable = StoreProductWarehouse::where('warehouse_id', '<>', $zoneDark->warehouse_id)
            ->available(true)
            ->first();

        $cartProductExpress = new stdClass();
        $cartProductExpress->store_product_id = $productExpressAvailable->store_product_id;
        $cartProductExpress->quantity = 1;
        $cartProductExpress->quantity_full_price = 0;

        $darkProducts[] = $cartProductExpress;

        $deliveryExpressRepository = new EloquentDeliveryExpressRepository();
        $validateDeliveryExpress = $deliveryExpressRepository->validateDeliveryExpress($address, $darkProducts, $zoneDark);

        $this->assertEmpty($validateDeliveryExpress);
    }

    /**
     * @test
     */
    public function testInvalidVolumeDeliveryExpress()
    {
        $address = new stdClass();
        $address->latitude  = '5.68020415';
        $address->longitude = '5.04341157';

        $zoneDark = $this->zoneRepository->findZoneExpress();

        $productExpressAvailable = StoreProductWarehouse::where('warehouse_id', $zoneDark->warehouse_id)
            ->available(true)
            ->first();

        $cartProductExpress = new stdClass();
        $cartProductExpress->store_product_id = $productExpressAvailable->store_product_id;
        $cartProductExpress->quantity = 100;
        $cartProductExpress->quantity_full_price = 0;

        $darkProducts[] = $cartProductExpress;

        $deliveryExpressRepository = new EloquentDeliveryExpressRepository();
        $validateDeliveryExpress = $deliveryExpressRepository->validateDeliveryExpress($address, $darkProducts, $zoneDark);

        $this->assertEmpty($validateDeliveryExpress);
    }

    /**
     * @dataProvider emptyDataProvider
     * @param $address
     * @param $darkProducts
     * @param $zoneDark
     */
    public function testInvalidateDeliveryExpress($address, $darkProducts, $zoneDark)
    {
        $deliveryExpressRepository = new EloquentDeliveryExpressRepository();
        $validateDeliveryExpress = $deliveryExpressRepository->validateDeliveryExpress($address, $darkProducts, $zoneDark);

        $this->assertEmpty($validateDeliveryExpress);
    }



    /**
     * test cases invalidate
     *
     * @return array
     */
    public function emptyDataProvider()
    {
        $address = new stdClass();
        $address->latitude  = '5.68020415';
        $address->longitude = '5.04341157';

        $cartProductExpress = new stdClass();
        $cartProductExpress->store_product_id = 1;
        $cartProductExpress->quantity = 1;
        $cartProductExpress->quantity_full_price = 0;

        $darkProducts[] = $cartProductExpress;

        $zoneDark = new stdClass();

        return [
            [[], $darkProducts, $zoneDark],
            [$address, [], $zoneDark],
            [$address, $darkProducts, []]
        ];
    }

}