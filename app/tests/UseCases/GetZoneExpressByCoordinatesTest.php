<?php


namespace Tests\UseCases;

use Zone;
use Warehouse;
use repositories\Eloquent\EloquentZoneRepository;
use Tests\TestCase;

class GetZoneExpressByCoordinatesTest extends TestCase
{
    /**
     *  setUp
     */
    public function setUp()
    {
        parent::setUp();
    }


    /**
     * @param $latitude
     * @param $longitude
     *
     * @dataProvider validCoordinates
     */
    public function testCasesValidCoordinates($latitude, $longitude)
    {
        $zoneRepository = new EloquentZoneRepository();

        $responseZone = $zoneRepository->getZoneExpressByCoordinates($latitude, $longitude);

        $this->assertInstanceOf(Zone::class, $responseZone);
        $this->assertTrue(in_array($responseZone->warehouse->id, Warehouse::WAREHOUSE_DARKSUPERMARKET_ID));
    }

    /**
     * @dataProvider invalidCoordinates
     *
     * @param $latitude
     * @param $longitude
     */
    public function testCasesInvalidCoordinates($latitude, $longitude){
        $zoneRepository = new EloquentZoneRepository();

        $responseZone = $zoneRepository->getZoneExpressByCoordinates($latitude, $longitude);

        $this->assertNull($responseZone);
    }

    /**
     * test cases valid coordinates
     *
     * @return array
     */
    public function validCoordinates(){
        return [
            ['4.68020415', '-74.04341157'],
            ['6.316965', '-75.560491']
        ];
    }

    /**
     * test cases invalid coordinates
     */
    public function invalidCoordinates(){
        return [
            ['', ''],
            ['5.68020415', '5.04341157']
        ];
    }
}