<?php


namespace Tests\UseCases;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery;
use Order;
use OrderGroup;
use orders\OrderStatus;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\Interfaces\RoutesRepositoryInterface;
use repositories\OrderRepository;
use Routes;
use Tests\TestCase;
use Tests\Traits\MerqueoFactory;
use usecases\Checkout\DeliveryQuick\RouteOrderUseCase;
use Warehouse;
use Zone;

class RouteOrderUseCaseTest extends TestCase
{
    use MerqueoFactory;

    /**
     * @var MockObject
     */
    private $orderRepository;

    /**
     * @var MockObject
     */
    private $routesRepository;

    /**
     * @setUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->orderRepository = Mockery::mock(OrderRepositoryInterface::class);
        $this->routesRepository = Mockery::mock(RoutesRepositoryInterface::class);
        DB::beginTransaction();
    }

    /**
     * @test
     */
    public function testRouteOrderUseCase()
    {
        $useCase = new RouteOrderUseCase($this->orderRepository, $this->routesRepository);

        $warehouse = new Warehouse();
        $warehouse->id = 1;
        $warehouse->city_id = 1;

        $orderGroup = new OrderGroup();
        $orderGroup->setRelation('warehouse', $warehouse);

        $order = new Order();
        $order->id = 1;
        $order->setRelation('orderGroup', $orderGroup);

        $zone = new Zone();
        $zone->id = 142;
        $zone->name = 'Country';

        $route = new Routes();
        $route->id = 1;

        $this->routesRepository->shouldReceive('getCurrentDeliveryQuickRoute')
            ->with($warehouse, $zone, 'ER')
            ->once()
            ->andReturn($route);

        $this->orderRepository->shouldReceive('updateStatusOrderExpressToRouted')
            ->with(1, 1)
            ->once()
            ->andReturn(1);

        $useCase->handler($order, $zone);
    }

    /**
     * @test
     */
    public function testErrorDataBaseRouteOrderUseCase()
    {
        $useCase = new RouteOrderUseCase($this->orderRepository, $this->routesRepository);

        $warehouse = new Warehouse();
        $warehouse->id = 1;
        $warehouse->city_id = 1;

        $orderGroup = new OrderGroup();
        $orderGroup->setRelation('warehouse', $warehouse);

        $order = new Order();
        $order->id = 1;
        $order->setRelation('orderGroup', $orderGroup);

        $zone = new Zone();
        $zone->id = 142;
        $zone->name = 'Country';

        $route = new Routes();
        $route->id = 1;

        $this->routesRepository->shouldReceive('getCurrentDeliveryQuickRoute')
            ->with($warehouse, $zone, 'ER')
            ->once()
            ->andReturn($route);

        $this->orderRepository->shouldReceive('updateStatusOrderExpressToRouted')
            ->with(1, 1)
            ->once()
            ->andThrow(new \Exception("no funciona la bd"));

        $this->setExpectedException(\Exception::class, "no funciona la bd");

        $useCase->handler($order, $zone);
    }

    /**
     * @test
     */
    public function testUpdateStatusOrderExpressToRouted()
    {
        $order = $this->makeFakeOrder();

        $dataExpected = [
            'status' => OrderStatus::ROUTED,
            'routeID' => 1,
            'planningDate' => Carbon::now()->format('Y-m-d')
        ];

        $orderRepository = new OrderRepository(new Order());

        $responseUpdate = $orderRepository->updateStatusOrderExpressToRouted($order->id, 1);

        $orderUpdate = $orderRepository->findById($order->id);
        $orderPlanningDate = Carbon::parse($orderUpdate->planning_date)->format('Y-m-d');

        $this->assertEquals(1, $responseUpdate);
        $this->assertEquals($dataExpected['status'], $orderUpdate->status);
        $this->assertEquals($dataExpected['routeID'], $orderUpdate->route_id);
        $this->assertEquals($dataExpected['planningDate'], $orderPlanningDate);

    }

    /**
     * @tearDown
     */
    protected function tearDown()
    {
        Mockery::close();
        parent::tearDown();
        DB::rollback();
    }
}