<?php

namespace Tests;

use Illuminate\Support\Collection;
use Tests\Traits\MerqueoFactory;

/**
 * Class BannerTest
 * @package Tests
 */
class BannerTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

    public function testShowNormalBannerOnProductActivate()
    {
        $banner = $this->getBannerWithProduct();
        $banner->status = 0;
        $banner->save();

        $storeProduct = $banner->storeProduct;
        $storeProductWarehouse = $storeProduct->storeProductWarehouses[0];

        $storeProductWarehouse->picking_stock = 101;
        $storeProductWarehouse->status = 0;
        $storeProductWarehouse->discontinued = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue(!empty($banner->status), 'El banner debe estar activado.');
    }

    public function testShowBannerOnDeepLinkActivate()
    {
        $banner = $this->getBannerWithProduct();
        $storeProduct = $banner->storeProduct;
        $banner->setRelation('storeProduct', null);
        $banner->store_product_id = null;
        $banner->deeplink_store_product_id = $storeProduct->id;
        $banner->category = 'Deeplink';
        $banner->status = 0;
        $banner->save();

        $storeProductWarehouse = $storeProduct->storeProductWarehouses[0];
        $storeProductWarehouse->picking_stock = 101;
        $storeProductWarehouse->status = 0;
        $storeProductWarehouse->discontinued = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue(!empty($banner->status), 'El banner debe estar activado.');
    }

    public function testShowBannerOnProductGroupActivate()
    {
        $banner = $this->getBannerWithProduct();
        $banner->status = 0;
        $banner->save();

        $storeProduct = $banner->storeProduct;
        $storeProductWarehouse = $storeProduct->storeProductWarehouses[0];

        $storeProductWarehouse->picking_stock = 101;
        $storeProductWarehouse->status = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue(!empty($banner->status), 'El banner debe estar activado.');
    }

    public function testHideBannerOnProduct()
    {
        $banner = $this->getBannerWithProduct();
        $banner->status = 1;
        $banner->save();

        $storeProduct = $banner->storeProduct;
        $storeProductWarehouse = $storeProduct->storeProductWarehouses[0];

        $storeProductWarehouse->picking_stock = 0;
        $storeProductWarehouse->status = 1;
        $storeProductWarehouse->reception_stock  = 0;
        $storeProductWarehouse->return_stock = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue(empty($banner->status), 'El banner debe estar desactivado.');
    }

    public function testKeepBannerStateOnProduct()
    {
        $banner = $this->getBannerWithTwoWarehouses();
        $banner->status = 1;
        $banner->save();

        $storeProduct = $banner->storeProduct;
        $storeProductWarehouse = $storeProduct->storeProductWarehouses[0];

        $storeProductWarehouse->picking_stock = 0;
        $storeProductWarehouse->status = 1;
        $storeProductWarehouse->reception_stock  = 0;
        $storeProductWarehouse->return_stock = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue($banner->status === 1, 'El banner debe estar activado.');

        $storeProductWarehouse = $storeProduct->storeProductWarehouses[1];

        $storeProductWarehouse->picking_stock = 0;
        $storeProductWarehouse->status = 1;
        $storeProductWarehouse->reception_stock  = 0;
        $storeProductWarehouse->return_stock = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue($banner->status === 0, 'El banner debe estar desactivado.');
    }

    /**
     *
     */
    public function testHideNormalBannerOnProductGroupDisable()
    {
        $banner = $this->getBannerWithProductGroup();
        $banner->status = 0;
        $banner->save();

        $storeProduct = $banner->storeProduct;
        $storeProductWarehouse = $storeProduct->storeProductGroup[0]
            ->storeProductGroup
            ->storeProductWarehouses[0];

        $storeProductWarehouse->picking_stock = 0;
        $storeProductWarehouse->status = 1;
        $storeProductWarehouse->discontinued = 0;
        $storeProductWarehouse->current_stock = 0;
        $storeProductWarehouse->reception_stock = 0;
        $storeProductWarehouse->return_stock = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue(empty($banner->status), 'El banner debe estar desactivado.');
    }

    /**
     *
     */
    public function testShowNormalBannerOnProductGroupDisable()
    {
        $banner = $this->getBannerWithProductGroup();
        $banner->status = 0;
        $banner->save();

        $storeProduct = $banner->storeProduct;
        $storeProductWarehouse = $storeProduct->storeProductGroup[0]
            ->storeProductGroup
            ->storeProductWarehouses[0];

        $storeProductWarehouse->picking_stock = 1000;
        $storeProductWarehouse->status = 0;
        $storeProductWarehouse->discontinued = 0;
        $storeProductWarehouse->current_stock = 0;
        $storeProductWarehouse->reception_stock = 0;
        $storeProductWarehouse->return_stock = 0;
        $storeProductWarehouse->save();

        $banner = \Banner::findOrFail($banner->id);
        $this->assertTrue(!empty($banner->status), 'El banner debe estar activado.');
    }

    /**
     * @return \Banner
     */
    private function getBannerWithProduct()
    {
        $banner = $this->newBanner();
        $store = $banner->store;
        $warehouse  = $this->newWarehouse($store->city);
        $storeProduct = $this->getStoreProduct($store);
        $banner->storeProduct()->associate($storeProduct);
        $banner->category = 'Producto';
        $banner->save();
        $this->stockOnWarehouse($warehouse, $storeProduct);
        $banner->warehouses()->sync([$warehouse->id]);

        return $banner;
    }

    /**
     * @return \Banner
     */
    private function getBannerWithProductGroup()
    {
        $banner = $this->newBanner();
        $store = $banner->store;
        $warehouse  = $this->newWarehouse($store->city);
        $storeProduct = $this->getStoreProductGroup($store);
        $banner->storeProduct()->associate($storeProduct);
        $banner->category = 'Producto';
        $banner->save();

        $this->stockGroupOnWarehouse($warehouse, $storeProduct);
        $banner->warehouses()->sync([$warehouse->id]);

        return $banner;
    }

    /**
     * @return \Banner
     */
    private function getBannerWithTwoWarehouses()
    {
        $banner = $this->newBanner();
        $store = $banner->store;
        $storeProduct = $this->getStoreProduct($store);
        $banner->storeProduct()->associate($storeProduct);
        $banner->category = 'Producto';
        $banner->save();
        $warehouses = [];
        foreach (range(1, 2) as $item) {
            $warehouse = $this->newWarehouse($store->city);
            $this->stockOnWarehouse($warehouse, $storeProduct);
            $warehouses[] = $warehouse->id;
        }
        $banner->warehouses()->sync($warehouses);

        return $banner;
    }
}
