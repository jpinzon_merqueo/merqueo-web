<?php

namespace Tests;

class AdminProductTest extends TestCase
{

    public function testOrdenCompraMarketplace()
    {
        $response = $this->call('GET', admin_url() . '/products/elastic_search_json_products');
        $this->assertResponseOk();
        $result = json_decode($response->getContent());
        $this->assertObjectHasAttribute('url', $result);
        $content = file_get_contents($result->url);
        $this->assertTrue(is_string($content));
    }
}
