<?php

namespace Tests\Traits;

use RejectReason;

trait RejectReasonFactory
{
    /**
     * Crea un motivo de cancelación a partir de los parámetros
     * @param string | null enum status
     * @param string | null enum attendant
     * @param string | null reason
     * @return RejectReason | null
     */
    protected function makeRejectReason(
        $status = null,
        $reason = null,
        $attendant = null
    ) {
        $rejectReason = new RejectReason();
        $rejectReason->status = $status ?: 'Cancelado';
        $rejectReason->attendant = $attendant;
        $rejectReason->reason = $reason ?: 'Usuario no se encuentra';
        $rejectReason->type = 'admin';
        $rejectReason->is_storage = 1;
        $rejectReason->is_active = 0;
        $rejectReason->old_value = null;
        $rejectReason->save();

        return $rejectReason;
    }
}
