<?php

namespace Tests\Traits;

use Admin;
use Carbon\Carbon;
use exceptions\MerqueoException;
use Faker\Factory;
use Illuminate\Support\Collection;

/**
 * Trait MerqueoFactory
 * @package Tests\Traits
 */
trait MerqueoFactory
{
    /**
     * Ultimo password asignado a un usuario.
     *
     * @var string
     */
    protected $lastPassword;

    /**
     * @param \Warehouse|null $warehouse
     * @param \User|null $user
     * @param string $status
     * @param \Store|null $store
     * @return \Order
     */
    protected function makeFakeOrder(
        \Warehouse $warehouse = null,
        \User $user = null,
        $status = 'Initiated',
        \Store $store = null
    ) {
        $orderGroup = $this->makeFakeGroupOrder($warehouse, $user);
        $user = $user ?: $orderGroup->user;
        $store = $store ?: $this->newStore();
        $order = new \Order();
        $order->user()->associate($user);
        $order->store()->associate($store);
        $order->orderGroup()->associate($orderGroup);
        $order->temporarily_cancelled = 0;
        $order->date = Carbon::create();
        $order->type = 'Marketplace';
        $order->is_hidden = 0;
        $order->reference = generate_reference();
        $order->status = 'Initiated';
        $order->total_products = 0;
        $order->delivery_amount = 0;
        $order->discount_amount = 0;
        $order->total_amount = 0;
        $order->delivery_date = Carbon::create()->addHour(5);
        $order->delivery_time = '5:00 am - 12:00 pm';
        $order->posible_fraud = '';
        $order->payment_method = 'Efectivo';
        $order->product_return_storage = 0;
        $order->save();

        return $order;
    }

    /**
     * @param \Warehouse|null $warehouse
     * @param \User|null $user
     * @return \OrderGroup
     */
    protected function makeFakeGroupOrder(\Warehouse $warehouse = null, \User $user = null)
    {
        if (empty($warehouse)) {
            $warehouse = $this->newWarehouse();
        }

        $user = $user ?: $this->newUser();
        $order_group = new \OrderGroup();
        $order_group->user()->associate($user);
        $order_group->address_id = \UserAddress::first()->id;
        $order_group->source = 'Web';
        $order_group->delivery_amount = 0;
        $order_group->discount_amount = 0;
        $order_group->total_amount = 0;
        $order_group->user_firstname = $user->first_name;
        $order_group->user_lastname = $user->last_name;
        $order_group->user_email = $user->email;
        $order_group->user_address = '!"#';
        $order_group->user_city_id = \City::first()->id;
        $order_group->warehouse_id = $warehouse->id;
        $order_group->user_phone = $user->phone;
        $order_group->products_quantity = 0;
        $order_group->app_version = 111;
        $order_group->reception_user_name = sprintf('%s %s', $user->first_name, $user->last_name);
        $order_group->reception_user_phone = $user->phone;
        $order_group->ip = '127.0.0.1';
        $order_group->save();

        return $order_group;
    }

    /**
     * @param \Order $order
     * @param int $products
     * @return \Order
     */
    public function getOrderWithProducts(\Order $order = null, $products = 5)
    {
        $order = $this->makeFakeOrder();



    }

    /**
     * @param \Order $order
     * @param \StoreProduct|Collection $products
     * @param int $quantity
     */
    protected function associateStoreProductsToOrder(\Order $order, $products, $quantity = 5)
    {
        if (!($products instanceof Collection)) {
            $products = [$products];
        }

        foreach ($products as $product) {
            $order_product = new \OrderProduct();
            $order_product->store_id = $product->store_id;
            $order_product->order_id = $order->id;
            $order_product->store_product_id = $product->id;
            $order_product->price = $product->special_price ?: $product->price;
            $order_product->original_price = $product->price;
            $order_product->quantity_special_price_stock = $product->quantity_special_price ?: 0;
            $order_product->quantity = $quantity;
            $order_product->fulfilment_status = 'Fullfilled';
            $order_product->is_gift = 0;
            $order_product->type = 'Product';
            $order_product->update_stock_back = 0;
            $order_product->reference = 'wxyz';
            $order_product->product_name = 'wxyz';
            $order_product->product_quantity = $quantity;
            $order_product->product_unit = 'lt';
            $order_product->product_image_url = 'lt';
            $order_product->save();
        }
    }

    protected function safeAction(\Closure $closure)
    {
        $hostName = trim(`hostname` ?: '');
        if ($hostName === 'ops010') {
            $closure();
        } else {
            trigger_error('Executing safe method...', E_WARNING);
        }
    }

    /**
     * @return \Order
     */
    protected function getRandomOrder()
    {
        return \Order::orderBy(\DB::raw('RAND()'))
            ->whereNotNull('management_date')
            ->whereNull('user_score_date')
            ->with('orderProducts')
            ->has('orderProducts')
            ->first();
    }

    /**
     * @return \Order
     */
    protected function getRandomOrderWithManyProducts()
    {
        $order = $this->getRandomOrder();
        if (count($order->orderProducts) > 1) {
            return $order;
        }
        return $this->getRandomOrderWithManyProducts();
    }

    /**
     * @param \Closure $extraQueryClosure
     * @return \Order
     */
    protected function getRandomComplaintOrder(\Closure $extraQueryClosure = null)
    {
        return $this->getRandomOrder(function ($query) use ($extraQueryClosure) {
            $query->whereHas('orderGroup', function ($query) {
                $query->where('source', 'Reclamo');
            });
            if (!empty($extraQueryClosure)) {
                $extraQueryClosure($query);
            }
        });
    }

    /**
     * @param Admin $admin
     * @param array|string $action
     * @param array|string $permissions
     */
    protected function initAdminSession(Admin $admin, $action = [], $permissions = [])
    {
        if (is_string($action)) {
            $action = [$action];
        }
        if (is_string($permissions)) {
            $permissions = [$permissions];
        }
        $this->session([
            'admin_id' => $admin->id,
            'admin_menu' => '[]',
            'admin_permissions' => json_encode([
                'callcenter\ticket' => [
                    'actions' => $action,
                    'permissions' => $permissions,
                ],
            ]),
        ]);
    }

    protected function clearAdminSession()
    {
        $this->session([]);
    }

    /**
     * @return \Coupon
     */
    protected function getVoucher()
    {
        return \Coupon::first();
    }

    /**
     * @return Carbon
     */
    protected function getNextFriday()
    {
        $holiday = new \Holidays();
        $holiday->festivos();

        $carbon = Carbon::create();
        while ($carbon->dayOfWeek !== Carbon::FRIDAY || $holiday->esFestivo($carbon->day, $carbon->month)) {
            $carbon->addDay();
        }

        return $carbon;
    }

    /**
     * @param \Zone $zone
     * @return \OrderGroup
     */
    protected function getOrderGroupByZone(\Zone $zone)
    {
        return $zone->orderGroups()
            ->whereNotNull('user_address_latitude')
            ->whereNotNull('user_address_longitude')
            ->first();
    }

    /**
     * @param \Warehouse|null $warehouse
     * @return \StdClass
     */
    protected function getTemporalCart(\Warehouse $warehouse = null)
    {
        $cart = new \StdClass();
        $cart->products = new \StdClass();
        if (!empty($warehouse)) {
            $cart->warehouse_id = $warehouse->id;
        }

        return $cart;
    }

    /**
     * @param \User $user
     * @param \Warehouse|null $warehouse
     * @return \Cart
     */
    protected function getNewCart(\User $user, \Warehouse $warehouse = null)
    {
        $warehouse = $warehouse ?: \Warehouse::first();
        $cart = new \Cart();
        $cart->user()->associate($user);
        $cart->warehouse()->associate($warehouse);
        $cart->save();

        return $cart;
    }

    protected function getCategory()
    {
        return \Category::first();
    }

    protected function getSubCategory()
    {
        return \Subcategory::first();
    }

    protected function getBrand()
    {
        return \Brand::first();
    }

    protected function getSubBrand()
    {
        return \Subbrand::first();
    }

    protected function getMaker()
    {
        return \Maker::first();
    }

    protected function getProduct()
    {
        $faker = Factory::create();
        $product = new \Product();
        $product->category()->associate($this->getCategory());
        $product->subCategory()->associate($this->getSubCategory());
        $product->brand()->associate($this->getSubCategory());
        $product->subBrand()->associate($this->getSubBrand());
        $product->maker()->associate($this->getMaker());
        $product->maker()->associate($this->getMaker());
        $product->name = $faker->name;
        $product->reference = $faker->numerify('#######');
        $product->slug = snake_case($product->name);
        $product->description = '';
        $product->nutrition_facts = '';
        $product->accounting_account = '';
        $product->accounting_account_type = '';
        $product->accounting_line = '';
        $product->accounting_group = '';
        $product->accounting_code = '';
        $product->type = 'Simple';
        $product->image_large_url = '';
        $product->image_medium_url = '';
        $product->image_small_url = '';
        $product->quantity = '';
        $product->unit = '';
        $product->image_app_url = '';
        $product->status = 1;
        $product->save();

        return $product;
    }

    /**
     * @return \Product
     */
    protected function getProductGroup()
    {
        $product = $this->getProduct();
        $product->type = 'Agrupado';
        $product->save();

        return $product;
    }

    protected function getStoreProduct(\Store $store = null, \Provider $provider = null, \AlliedStore $allied = null)
    {
        $faker = Factory::create();
        $product = $this->getProduct();

        if (empty($store)) {
            $store = $this->newStore();
        }

        $storeProduct = new \StoreProduct();
        $storeProduct->first_order_special_price = '';
        $storeProduct->delivery_discount_amount = '';
        $storeProduct->cost_average = '';
        if ($allied) {
            $storeProduct->alliedStore()->associate($allied);
        }
        $storeProduct->negociated_price = '';
        $storeProduct->is_best_price = 0;
        $storeProduct->sort_order = 0;
        $storeProduct->handle_expiration_date = 0;

        $department = $this->getDepartment($store);
        $shelve = $this->getShelf($store, $department);
        $provider = $provider ?: $this->getProvider($store->city);

        $storeProduct->department()->associate($department);
        $storeProduct->shelf()->associate($shelve);
        $storeProduct->provider()->associate($provider);
        $storeProduct->store()->associate($store);
        $storeProduct->product()->associate($product);

        $storeProduct->price = $faker->numberBetween(5000, 20000);
        $storeProduct->iva = 19;
        $storeProduct->cost = $storeProduct->price * .7;
        $storeProduct->base_price = $storeProduct->price / (1 + $storeProduct->iva / 100);
        $storeProduct->base_cost = $storeProduct->cost / (1 + $storeProduct->iva / 100);

        $storeProduct->save();

        return $storeProduct;
    }

    /**
     * @param \Store|null $store
     * @param \Provider|null $provider
     * @param \AlliedStore|null $allied
     * @return \StoreProduct
     */
    protected function getStoreProductGroup(
        \Store $store = null,
        \Provider $provider = null,
        \AlliedStore $allied = null
    ) {
        $storeProduct = $this->getStoreProduct($store, $provider, $allied);
        $product = $storeProduct->product;
        $product->type = 'Agrupado';
        $product->save();

        foreach (range(1, 5) as $item) {
            $childProduct = $this->getStoreProduct();
            $child = new \StoreProductGroup();
            $child->storeProduct()
                ->associate($storeProduct)
                ->storeProductGroup()
                ->associate($childProduct);
            $child->quantity = 1;
            $child->save();
        }

        return $storeProduct;
    }

    /**
     * @param \Store|null $store
     * @param \Warehouse|null $warehouse
     * @return \StoreProduct
     */
    protected function productWithStock(\Store $store = null, \Warehouse $warehouse = null)
    {
        if (empty($store)) {
            $store = $this->newStore();
        }

        $city = $store->city;

        if (empty($warehouse)) {
            $warehouse = $this->newWarehouse($city);
        }

        $storeProduct = $this->getStoreProduct($store);
        $this->stockOnWarehouse($warehouse, $storeProduct);

        return $storeProduct;
    }

    /**
     * @param \Store|null $store
     */
    protected function getMarketplaceStoreProduct(\Store $store = null)
    {
        $storeProduct = $this->getStoreProduct($store);
        $alliedStore = $this->getAlliedStore($storeProduct->provider);
        $storeProduct->alliedStore()->associate($alliedStore);
        $storeProduct->save();

    }

    /**
     * @param \Store $store
     * @return \Department
     */
    protected function getDepartment(\Store $store)
    {
        $faker = Factory::create();
        $department = new \Department();
        $department->store()->associate($store);
        $department->name = $faker->name;
        $department->slug = snake_case($department->name);
        $department->show_more_app = $faker->randomKey([true, false]);
        $department->sort_order = $faker->randomNumber();
        $department->status = 1;
        $department->save();

        return $department;
    }

    /**
     * @param \Store $store
     * @param \Department|null $department
     * @return \Shelf
     */
    protected function getShelf(\Store $store, \Department $department = null)
    {
        $faker = Factory::create();

        if (empty($department)) {
            $department = $this->getDepartment($store);
        }
        $shelf = new \Shelf();
        $shelf->department()->associate($department);
        $shelf->store()->associate($store);
        $shelf->name = $faker->name;
        $shelf->slug = snake_case($shelf->name);
        $shelf->show_more_app = $faker->randomKey([true, false]);
        $shelf->has_warning = $faker->randomKey([true, false]);
        $shelf->sort_order = $faker->randomNumber();
        $shelf->status = 1;
        $shelf->save();

        return $shelf;
    }

    /**
     * @param \City $city
     * @return \Provider
     */
    protected function getProvider(\City $city)
    {
        $faker = Factory::create();
        $provider = new \Provider();
        $provider->city()->associate($city);
        $provider->code = $faker->numerify('#######');
        $provider->nit = $faker->numerify('#######');
        $provider->name = $faker->name;
        $provider->address = $faker->address;
        $provider->schedule = '';
        $provider->maximum_order_report_time = Carbon::create()->addYear();
        $provider->transporter = 'Merqueo';
        $provider->logo_url = '';
        $provider->logo_small_url = '';
        $provider->rut_url = '';
        $provider->bank_certification_url = '';
        $provider->save();

        return $provider;
    }

    /**
     * @param \Provider $provider
     * @return \AlliedStore
     */
    protected function getAlliedStore(\Provider $provider)
    {
        $faker = Factory::create();
        $alliedStore = new \AlliedStore();
        $alliedStore->city()->associate($provider->city);
        $alliedStore->provider()->associate($provider);
        $alliedStore->name = $faker->name;
        $alliedStore->address = $faker->address;
        $alliedStore->nit = $faker->numerify('#######');
        $alliedStore->schedule = '';
        $alliedStore->transporter = 'Merqueo';
        $alliedStore->delivery_time_hours = $faker->numberBetween(0, 72);
        $alliedStore->status = 1;
        $alliedStore->save();

        return $alliedStore;
    }

    /**
     * @param \User $user
     * @param bool $saveAtPayu
     * @return \UserCreditCard
     * @throws MerqueoException
     */
    protected function addCreditCard(\User $user, $saveAtPayu = true)
    {
        $faker = Factory::create();
        $creditCard = new \UserCreditCard();
        $creditCard->user()->associate($user);
        $creditCard->card_token = $faker->uuid;
        $creditCard->bin = $faker->numerify('######');
        $creditCard->last_four = $faker->numerify('####');
        $creditCard->type = 'VISA';
        $creditCard->country = 'Colombia';
        $creditCard->holder_document_type = 'CC';
        $creditCard->holder_document_number = $faker->numerify('#######');
        $creditCard->holder_name = $user->first_name ?: $faker->name;
        $creditCard->holder_email = $faker->email;
        $creditCard->holder_phone = $faker->phoneNumber;
        $creditCard->holder_address = $faker->address;
        $creditCard->holder_address_further = $faker->address;
        $creditCard->holder_city = $faker->city;

        if ($saveAtPayu) {
            $number = $faker->creditCardNumber('Visa');
            $this->saveCreditCardOnPayu($creditCard, $number);
            $this->last_four = substr($number, strlen($number) - 4, 4);
        }

        $creditCard->save();

        return $creditCard;
    }

    /**
     * @param \UserCreditCard $userCreditCard
     * @param null $code
     * @param null $expirationDate
     * @return array
     * @throws MerqueoException
     */
    public function saveCreditCardOnPayu(\UserCreditCard $userCreditCard, $code = null, $expirationDate = null)
    {
        $faker = Factory::create();

        if (empty($code)) {
            $code = $faker->creditCardNumber('Visa');
        }

        if (empty($expirationDate)) {
            $expirationDate = $faker->creditCardExpirationDate;
        }

        list($month, $year) = explode('/', $expirationDate->format('m/Y'));
        $paymentu = new \PayU();
        $result = $paymentu->associateCreditCard([
            'card_type' => 'VISA',
            'code_cc' => '123',
            'expiration_year_cc' => $year,
            'expiration_month_cc' => $month,
            'user_id' => $userCreditCard->user->id,
            'name_cc' => $userCreditCard->holder_name,
            'document_number_cc' => $faker->numerify('######'),
            'number_cc' => $code,
        ]);

        if (!$result['status']) {
            throw new MerqueoException($result['message']);
        }

        $userCreditCard->card_token = $result['response']->creditCardToken->creditCardTokenId;
        $userCreditCard->save();

        return $result;
    }

    /**
     * @param string $password
     * @param string $email
     * @return \User
     */
    public function newUser($password = '12345', $email = '')
    {
        $faker = Factory::create();

        $user = new \User();
        $user->first_name = $faker->firstName;
        $user->last_name = $faker->lastName;
        $user->email = $email ?: $faker->email;
        $user->phone = $faker->tollFreePhoneNumber;
        $this->lastPassword = $faker->numerify('#####');
        $user->password = \Hash::make($password ?: $this->lastPassword);
        $user->referral_code = \User::generateReferralCode($user->first_name, $user->last_name);
        $user->total_referrals = 0;
        $user->type = 'merqueo.com';

        $user->save();

        return $user;
    }

    /**
     * @param \City|null $city
     * @return \Store
     */
    public function newStore(\City $city = null)
    {
        $faker = Factory::create();
        if (empty($city)) {
            $city = \City::first();
        }

        $store = new \Store();
        $store->city()->associate($city);
        $store->is_hidden = false;
        $store->is_storage = 1;
        $store->name = $faker->name;
        $store->slug = snake_case($store->name);
        $store->nit = $faker->numerify('#######');
        $store->phone = $faker->phoneNumber;
        $store->schedule = $faker->text();
        $store->description = $faker->text();
        $store->latitude = $faker->latitude;
        $store->longitude = $faker->longitude;
        $store->delivery_time = '90 m';
        $store->minimum_order_amount = $faker->numerify('1####');
        $store->revision_orders_required = 1;
        $store->delivery_time_minutes = 180;
        $store->logo_url = $faker->imageUrl();
        $store->logo_small_url = $faker->imageUrl();
        $store->sort_order = $faker->numerify('#');
        $store->status = true;

        $store->save();

        return $store;
    }

    /**
     * @param \Warehouse $warehouse
     * @param \StoreProduct $storeProduct
     * @param \AlliedStore|null $alliedStore
     * @return \StoreProductWarehouse
     */
    protected function stockOnWarehouse(
        \Warehouse $warehouse,
        \StoreProduct $storeProduct,
        \AlliedStore $alliedStore = null
    ) {
        $pWarehouse = new \StoreProductWarehouse();
        $pWarehouse->storage_position = 10;
        $pWarehouse->storage_height_position = 'A';
        $pWarehouse->minimum_picking_stock = 100;
        $pWarehouse->maximum_picking_stock = 100;
        $pWarehouse->manage_stock = !empty($alliedStore);
        $pWarehouse->is_visible_stock = 1;
        $pWarehouse->ideal_stock = 100;
        $pWarehouse->reception_stock = 100;
        $pWarehouse->picking_stock = 100;
        $pWarehouse->picked_stock = 100;
        $pWarehouse->return_stock = 100;
        $pWarehouse->shrinkage_stock = 100;
        $pWarehouse->status = 1;
        $pWarehouse->current_stock = 100;
        $pWarehouse->warehouse()->associate($warehouse);
        $pWarehouse->storeProduct()->associate($storeProduct);

        $pWarehouse->save();

        return $pWarehouse;
    }

    /**
     * @param \Warehouse $warehouse
     * @param \StoreProduct $storeProduct
     * @param \AlliedStore|null $alliedStore
     * @return \StoreProductWarehouse
     */
    protected function stockGroupOnWarehouse(
        \Warehouse $warehouse,
        \StoreProduct $storeProduct,
        \AlliedStore $alliedStore = null
    ) {

        foreach ($storeProduct->storeProductGroup as $group) {
            $this->stockOnWarehouse($warehouse, $group->storeProductGroup, $alliedStore);
        }

        $productWarehouse = $this->stockOnWarehouse($warehouse, $storeProduct, $alliedStore);
        $productWarehouse->manage_stock = false;
        $productWarehouse->save();

        return $productWarehouse;
    }

    /**
     * @param \City|null $city
     * @return \Warehouse
     */
    protected function newWarehouse(\City $city = null)
    {
        $city = empty($city) ? \City::first() : $city;

        $warehouse = new \Warehouse();
        $warehouse->city()->associate($city);
        $warehouse->warehouse = 'XX';
        $warehouse->address = 'XX';
        $warehouse->orders_limit = 100;
        $warehouse->status = 1;
        $warehouse->storage_positions = '';
        $warehouse->save();

        $zone = $this->newZone($warehouse);
        $zone->warehouse()->associate($warehouse);
        $zone->save();

        return $warehouse;
    }

    /**
     * @param \Warehouse|null $warehouse
     * @return \Zone
     */
    public function newZone(\Warehouse $warehouse = null)
    {
        $faker = Factory::create();
        if (empty($warehouse)) {
            $warehouse = \Warehouse::first();
        }

        $zone = new \Zone();
        $zone->warehouse()->associate($warehouse);
        $zone->name = $faker->name;
        $zone->delivery_zone = '4.536541212108173 -74.11797523498535,4.533247075724847 -74.12492752075195,4.529696236573084 -74.12750244140625,4.524241600306159 -74.12690162658691,4.510487178293541 -74.12312507629395,4.505053788919458 -74.11291122436523,4.505781095363369 -74.10437107086182,4.51418784936423 -74.10640150308609,4.5162627838475755 -74.1093760728836,4.519899252644823 -74.11300778388977,4.523407358194256 -74.11357641220093,4.52879782872244 -74.1134262084961,4.534530507270696 -74.11437034606934';
        $zone->orders_per_route = 100;
        $zone->planning_priority = 10;
        $zone->status = true;
        $zone->save();

        return $zone;
    }

    /**
     * @param \Store $store
     * @param \Department $department
     * @return \Shelf
     */
    public function newShelf(\Store $store, \Department $department)
    {
        $shelf = new \Shelf();
        $shelf->store_id = $store->id;
        $shelf->department_id = $department->id;
        $shelf->name = 'AAAAA';
        $shelf->slug = 'AAAAA';
        $shelf->show_more_app = 1;
        $shelf->has_warning = 1;
        $shelf->sort_order = 1;
        $shelf->status = 1;

        $shelf->save();

        return $shelf;
    }

    public function newDepartment(\Store $store)
    {
        $deparment = new \Department();
        $deparment->store_id = $store->id;
        $deparment->name = 'AAAAA';
        $deparment->slug = 'AAAAA';
        $deparment->show_more_app = 1;
        $deparment->sort_order = 1;
        $deparment->status = 1;

        $deparment->save();

        return $deparment;
    }

    public function newImage(\Product $product)
    {
        $image = new \ProductImage();
        $image->product_id = $product->id;
        $image->save();

        return $image;
    }

    /**
     * @param \Product $product
     * @param \Warehouse $warehouse
     * @return \StoreProduct
     */
    public function newVisibleProduct(\Product $product, \Warehouse $warehouse = null)
    {
        if (empty($warehouse)) {
            $warehouse = $this->newWarehouse();
        }
        $storeProduct = $product->storeProducts[0];
        $store = $storeProduct->store;
        $this->newShelf($store, $this->newDepartment($store));
        $sProWarehouse = \StoreProductWarehouse::where('warehouse_id', $warehouse->id)
            ->where('store_product_id', $storeProduct->id)
            ->first();
        if (empty($sProWarehouse)) {
            $this->stockOnWarehouse($warehouse, $storeProduct);
        } else {
            $sProWarehouse->status = 1;
            $sProWarehouse->save();
        }

        return $storeProduct;
    }

    /**
     * @param \Warehouse $warehouse
     * @param \User|null $user
     * @return \Cart
     */
    public function newCart(\Warehouse $warehouse, \User $user = null)
    {
        $cart = new \Cart();
        if (!empty($user)) {
            $cart->user()->associate($user);
        }
        $cart->warehouse()->associate($warehouse);
        $cart->save();

        return $cart;
    }

    public function setMinimumDeliveryDiscount(\Warehouse $warehouse, \Store $store)
    {
        $discount = new \Discount();
        $discount->type = 'free_delivery';
        $discount->order_for_tomorrow = 0;
        $discount->city_id = $warehouse->city->id;
        $discount->store_ids = $store->id;
        $discount->minimum_order_amount = 100000;
        $discount->save();

        return $discount;
    }

    /**
     * @param \Store|null $store
     * @param Carbon $dateMock
     * @return \Slot
     */
    public function newSlot(\Store $store = null, Carbon $dateMock = null)
    {
        if (empty($store)) {
            $store = \Store::first();
        }

        if (empty($dateMock)) {
            $dateMock = Carbon::create();
        }

        $range = 4;
        $slot = new \Slot();
        $slot->store()->associate($store);
        $slot->day = $dateMock->addDay();
        $slot->start_time = $dateMock->subHours($range)->format('Y-m-d H:00:00');
        $slot->end_time = $dateMock->addHours($range * 2)->format('Y-m-d H:00:00');
        $slot->is_available = 1;
        $slot->save();

        return $slot;
    }

    /**
     * @param \Warehouse|null $zone
     * @param \Slot $slot
     * @return \SlotWarehouse
     */
    public function newSlotWarehouse(\Warehouse $warehouse = null, \Slot $slot)
    {
        if (empty($warehouse)) {
            $warehouse = $this->newWarehouse();
        }

        if (empty($slot)) {
            $slot = $this->newSlot();
        }

        $slotWarehouse = new \SlotWarehouse();
        $slotWarehouse->zone()->associate($warehouse);
        $date = new Carbon($slot->day);
        $slotWarehouse->day = $date->dayOfWeek;
        $slotWarehouse->number_products = 100;
        $slotWarehouse->save();

        return $slotWarehouse;
    }

    /**
     * @return \City
     */
    public function newCity()
    {
        return \City::firstOrNew([]);
    }

    /**
     * @param \City|null $city
     * @return \DeliveryWindow
     */
    public function newDeliveryWindow(\City $city = null)
    {
        $deliveryWindow = new \DeliveryWindow([
            'hour_start' => '07:00:00',
            'hour_end' => '12:00:00',
            'shifts' => \DeliveryWindow::SHIFTS[3],
            'status' => true,
            'delivery_window' => '7:00 am - 12:00 pm',
        ]);
        $deliveryWindow->city()->associate($city ?: $this->newCity());
        $deliveryWindow->save();

        return $deliveryWindow;
    }

    /**
     * @param \Zone|null $zone
     * @param \DeliveryWindow|null $window
     * @return \SlotZone
     */
    public function newSlotZoneWithWindow(\Zone $zone = null, \DeliveryWindow $window = null)
    {
        $this->initFaker();

        $slot = new \SlotZone();
        $slot->zone()->associate($zone ?: $this->newZone());
        $slot->day = Carbon::create()->dayOfWeek;
        $slot->number_orders = $this->faker->numberBetween(5, 10);
        $slot->deliveryWindow()->associate($window ?: $this->newDeliveryWindow($slot->zone->city));
        $slot->save();

        return $slot;
    }

    protected function initFaker()
    {
        if (empty($this->faker)) {
            $this->faker = Factory::create();
        }
    }

    /**
     * @param \User|null $user
     * @param \UserCreditCard|null $userCreditCard
     * @param \City|null $city
     * @param string $status
     * @return \ExternalService
     * @throws MerqueoException
     */
    public function newExternalService(
        \User $user = null,
        \UserCreditCard $userCreditCard = null,
        \City $city = null,
        $status = 'Validación'
    ) {
        $faker = Factory::create();
        $user = $user ?: $this->newUser();
        $userCreditCard = $userCreditCard ?: $this->addCreditCard($user, true);
        $city = $city ?: \City::first();
        $externalService = \ExternalService::saveExternal([
            'user_id' => $user->id,
            'credit_card_id' => $userCreditCard->id,
            'type' => 'SOAT',
            'amount' => $faker->numerify('######'),
            'cc_installments' => 1,
            'user_city_id' => $city->id,
            'email' => $user->email,
            'plate' => $faker->numerify('###'),
            'taker' => [
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'email' => $user->email,
                'address' => $faker->address,
                'phone' => $user->phone,
            ],
            'buyer' => [
                'name' => $user->first_name,
                'email' => $user->email,
                'address' => $faker->address,
                'phone' => $user->phone,
            ],
        ]);

        return $externalService;
    }

    /**
     * @param \City|null $city
     * @return \AlliedStore
     */
    public function newAlliedStore(\City $city = null)
    {
        $faker = Factory::create();
        $alliedStore = new \AlliedStore();
        $alliedStore->city()->associate($city ?: $this->newCity());
        $alliedStore->name = $faker->name;
        $alliedStore->nit = $faker->uuid;
        $alliedStore->schedule = 'Lunes a Viernes 7am-5pm';
        $alliedStore->transporter = $faker->randomElement(['Merqueo', 'Merqueo']);
        $alliedStore->delivery_time_hours = 48;
        $alliedStore->save();

        return $alliedStore;
    }

    /**
     * @param \Store $store
     * @return \Banner
     */
    public function newBanner(\Store $store = null)
    {
        $faker = Factory::create();
        $banner = new \Banner();
        $banner->is_home = $faker->boolean;
        $banner->platforms = 'web,android,ios';
        $banner->show_start_date = Carbon::yesterday();
        $banner->show_end_date = Carbon::tomorrow();
        $banner->store()->associate($store ?: $this->newStore());
        $banner->save();

        return $banner;
    }
}
