<?php

namespace Tests;

use Illuminate\Support\Facades\DB;
use Tests\Traits\MerqueoFactory;

class CheckoutTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();
        DB::rollBack();
    }

    /**
     * Valida que los productos de otra bodega se muestren como
     * productos a eliminar.
     */
    public function testPossibleChangedProducts()
    {
        $removedProducts = 0;
        $warehouse = $this->newWarehouse();
        $user = $this->newUser();
        $zone = $this->enableZone($warehouse);
        $store = \Store::has('storeProducts')->where('city_id', $warehouse->city_id)->firstOrFail();
        $products = \StoreProduct::where('store_id', $store->id)->limit(10)->get();

        $otherZone = \Zone::where('id', '<>', $zone->id)->first();
        $otherWarehouse = \Warehouse::where('id', '<>', $warehouse->id)->first();
        $cart = $this->newCart($otherWarehouse, $user);

        foreach ($products as $index => $product) {
            if ($index % 2 === 0) {
                $removedProducts++;
                $this->stockOnWarehouse($warehouse, $product);
            }
            $this->addProductToCart($cart, $product);
        }

        $this->session(['cart_id' => $cart->id, 'possible_zone_id' => $zone->id, 'zone_id' => $otherZone->id]);
        $url = action('CheckoutController@get_changed_products');
        $this->call('GET', $url);
        $this->assertSessionHas(\CheckoutController::REMOVED_PRODUCTS_SESSION_KEY);
        $items = \Session::get(\CheckoutController::REMOVED_PRODUCTS_SESSION_KEY);
        $this->assertCount($removedProducts, $items);
    }

    /**
     * Valida que al confirmar la modificación del
     * carrito los productos sean eliminados de la
     * base de datos.
     */
    public function testConfirmChangedProducts()
    {
        $numAddedProducts = 10;
        $removedProducts = 0;
        $warehouse = $this->newWarehouse();
        $user = $this->newUser();
        $zone = $this->enableZone($warehouse);
        $store = \Store::has('storeProducts')->where('city_id', $warehouse->city_id)->firstOrFail();
        $products = \StoreProduct::where('store_id', $store->id)->limit($numAddedProducts)->get();

        $otherZone = \Zone::where('id', '<>', $zone->id)->first();
        $otherWarehouse = \Warehouse::where('id', '<>', $warehouse->id)->first();
        $cart = $this->newCart($otherWarehouse, $user);

        $addedProduct = [];

        foreach ($products as $index => $product) {
            if ($index % 2 === 0) {
                $removedProducts++;
                $this->stockOnWarehouse($warehouse, $product);
            } else {
                $addedProduct[] = $product;
            }

            $this->addProductToCart($cart, $product);
        }

        $this->session(['cart_id' => $cart->id, 'possible_zone_id' => $zone->id, 'zone_id' => $otherZone->id]);

        // Almacena los productos a remover en la sessión.
        $url = action('CheckoutController@get_changed_products');
        $this->call('GET', $url);

        $currentZone = \Session::get('zone_id');
        $this->assertEquals($currentZone, $otherZone->id);

        $url = action('CheckoutController@alter_cart_content_by_zone');
        $this->call('GET', $url);

        $newCart = \Cart::findOrFail($cart->id);
        $this->assertCount($numAddedProducts - $removedProducts, $newCart->products);
        $currentZone = \Session::get('zone_id');
        $this->assertEquals($currentZone, $zone->id);

        $currentZone = \Zone::findOrFail($currentZone);
        $this->assertEquals($newCart->warehouse->id, $currentZone->warehouse->id);

        foreach ($addedProduct as $product) {
            $this->assertNotEmpty($cart->products()->where('store_product_id', $product->id));
        }
    }

    public function testRemovingUnavailableFromTemporalCart()
    {
        $removed_product_id = null;

        $total_products = 10;
        $user = $this->newUser();
        $city = \City::has('stores.storeProducts')->first();
        $store = $city->stores()->has('storeProducts')->first();
        $warehouse = $this->newWarehouse($city);
        $zone = \Zone::has('orderGroups')->first();
        $this->enableZone($warehouse, $zone);
        $products = \StoreProduct::has('product')
            ->with('product')
            ->where('store_id', $store->id)
            ->limit($total_products)
            ->get();

        $cartVar = $this->getTemporalCart($warehouse);
        $orderGroup = $this->getOrderGroupByZone($zone);
        $this->be($user);

        foreach ($products as $index => $product) {
            $productCart = new \StdClass();
            $productCart->name = $product->product->name;
            $productCart->cart_quantity = 1;
            $productCart->cart_quantity_full_price = 0;
            $productCart->price = $product->price;

            $cartVar->products->{$product->id} = $productCart;
            $sProWarehouse = $this->stockOnWarehouse($warehouse, $product);
            if (!$index) {
                $removed_product_id = $product->id;

                $sProWarehouse->status = 0;
                $sProWarehouse->save();
            }
        }

        $this->session(['tmp_cart' => json_encode($cartVar)]);
        \Route::enableFilters();
        $this->call('POST', action('CheckoutController@checkout'), [
            'address_id' => $orderGroup->address_id,
            'payment_method' => 'XXXX'
        ]);
        $this->assertResponseStatus(302);
        $this->assertSessionHas('removed_products');
        $this->assertCount(1, \Session::get('removed_products'));

        $cart = json_decode(\Session::get('tmp_cart'));
        $this->assertNotEmpty($cart->products->{$removed_product_id});
        $this->call('GET', action('CheckoutController@remove_limited_products'));

        $cart = json_decode(\Session::get('tmp_cart'));
        $this->assertTrue(empty($cart->products->{$removed_product_id}));
    }

    public function testRemovingUnavailableFromDbCart()
    {
        $removed_product_id = null;

        $total_products = 10;
        $user = $this->newUser();
        $city = \City::has('stores.storeProducts')->first();
        $store = $city->stores()->has('storeProducts')->first();
        $warehouse = $this->newWarehouse($city);
        $zone = \Zone::has('orderGroups')->first();
        $this->enableZone($warehouse, $zone);
        $products = \StoreProduct::has('product')
            ->with('product')
            ->where('store_id', $store->id)
            ->limit($total_products)
            ->get();

        $cart = $this->getNewCart($user, $warehouse);
        $orderGroup = $this->getOrderGroupByZone($zone);
        $this->be($user);

        foreach ($products as $index => $product) {
            $storProWarehouse = $this->stockOnWarehouse($warehouse, $product);
            $this->addProductToCart($cart, $product);
            if (!$index) {
                $removed_product_id = $product->id;
                $storProWarehouse->status = 0;
                $storProWarehouse->save();
            }
        }

        $this->session(['cart_id' => $cart->id]);
        \Route::enableFilters();
        $this->call('POST', action('CheckoutController@checkout'), [
            'address_id' => $orderGroup->address_id,
            'payment_method' => 'XXXX'
        ]);
        $this->assertResponseStatus(302);
        $this->assertSessionHas('removed_products');
        $this->assertCount(1, \Session::get('removed_products'));

        $cartProduct = \CartProduct::where('cart_id', $cart->id)
            ->where('store_product_id', $removed_product_id)
            ->first();

        $this->assertNotEmpty($cartProduct);
        $this->call('GET', action('CheckoutController@remove_limited_products'));

        $cartProduct = \CartProduct::where('cart_id', $cart->id)
            ->where('store_product_id', $removed_product_id)
            ->first();

        $this->assertEmpty($cartProduct);
    }
}
