<?php

namespace Tests;

use Tests\Traits\MerqueoFactory;

class ApiUserTest extends TestCase
{
    use MerqueoFactory;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create();
        parent::setUp();
        \DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();
        \DB::rollBack();
    }

    /**
     * @var \Product[]
     */
    private $products;

    /**
     * Valida que el json retornado tenga las variables especificadas:
     * - details
     * - rating
     *     - bad
     *         - products
     *             - order_products
     *                 - products_list
     *                     - ...
     *                     - fulfilment_status
     *                     - ...
     *     - success
     *         - message
     */
    public function testRateJsonFormat()
    {
        $order = $this->makeFakeOrder();
        $product = $this->getStoreProduct($order->store);
        $this->associateStoreProductsToOrder($order, $product);
        $order_id = $order->id;
        $user_id = $order->user_id;
        $action = action('api\v13\ApiUserController@get_score_order', $order_id);

        $request = $this->call('GET', $action, compact('user_id'));
        $response = json_decode($request->getContent());
        $this->assertTrue($response->status);
        $result = $response->message;
        $this->assertObjectHasAttribute('details', $result);
        $this->assertObjectHasAttribute('rating', $result);
        $this->assertObjectHasAttribute('title', $result->rating->bad);
        $this->assertObjectHasAttribute('other', $result->rating->bad);
        $this->assertObjectHasAttribute('title', $result->rating->success);
        $products = $result->rating->bad->products->order_products->products_list;
        $this->assertNotEmpty($products);
        $this->assertCount(count($order->orderProducts), $products);
        foreach ($products as $product) {
            $this->assertObjectHasAttribute('fulfilment_status', $product);
        }
    }

    public function testRateJsonFormatWrongUser()
    {
        $order = $this->getRandomOrder();
        $order_id = $order->id;

        $action = action('api\v13\ApiUserController@get_score_order', $order_id);
        $request = $this->call('GET', $action);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);

        $action = action('api\v13\ApiUserController@get_score_order', $order_id);
        $request = $this->call('GET', $action);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);

        $action = action('api\v13\ApiUserController@get_score_order', 123);
        $request = $this->call('GET', $action);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);
    }

    public function testUserSaveGoodRate()
    {
        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'success',
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertTrue($result->status);
        $order = \Order::find($order->id);
        $this->assertTrue($order->user_score == 1);
    }

    public function testUserSaveBadRateOthers()
    {
        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $text = [$this->faker->paragraph(), $this->faker->paragraph(), $this->faker->paragraph(), null];
        $comment = $this->faker->paragraph();
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad',
            'other' => $text,
            'comment' => $comment,
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertTrue($result->status);
        $order = \Order::find($order->id);
        $comments = explode(';', $order->user_score_typification);
        $this->assertCount(count($text) - 1, $comments);
        $this->assertEquals($order->user_score_comments, $comment);
    }

    public function testUserSaveBadRateProducts()
    {
        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $product = $order->orderProducts()->first();
        $product->reason = $this->faker->paragraph();
        $products_list = [$product->toArray()];
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad',
            'order_products' => $products_list,
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertTrue($result->status);
        $order = \Order::find($order->id);
        $product_orders = \Order::find($order->id)->orderProducts()->get();
        $totalRatesSaved = 0;
        foreach ($product_orders as $orderProduct) {
            $totalRatesSaved += !empty($orderProduct->orderProductScore()->first());
        }
        $this->assertEquals(count($products_list), $totalRatesSaved);
        $this->assertEquals($product_orders[0]->orderProductScore->reason, $product->reason);
        $this->assertEquals($order->user_score_typification, 'Con los productos');
    }

    public function testUserSaveBadRateManyProducts()
    {
        $order = $this->makeFakeOrder();
        $this->associateStoreProductsToOrder($order, \StoreProduct::limit(10)->get());
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $products = $order->orderProducts()->get();
        $products_list = [];
        foreach ($products as $product) {
            $product->reason = $this->faker->paragraph();
            $products_list[] = $product->toArray();
        }

        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad',
            'order_products' => $products_list,
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertTrue($result->status);
        $order = \Order::find($order->id);
        $product_orders = $order->orderProducts()->get();
        $totalRatesSaved = 0;
        foreach ($product_orders as $orderProduct) {
            $totalRatesSaved += !empty($orderProduct->orderProductScore()->first());
        }
        $this->assertEquals(count($products_list), $totalRatesSaved);
        foreach ($product_orders as $key => $product) {
            $this->assertEquals($product->orderProductScore->reason, $products[$key]->reason);
        }
    }

    public function testUserSaveEmptyResponse()
    {
        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad'
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);
    }

    public function testUserSaveWrongOrderProductId()
    {
        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad',
            'order_products' => [['id' => 0]]
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);
    }

    public function testUserSaveAlreadyRated()
    {
        $order = $this->getRandomOrder();
        $order->user_score_date = \Carbon\Carbon::now();
        $order->save();

        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $body = [
            'user_id' => $order->user_id,
            'rating' => 'success',
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);
    }

    public function testProductReasonIsRequired()
    {
        $order = $this->getRandomOrderWithManyProducts();
        $action = action('api\v13\ApiUserController@save_score_order', $order->id);
        $products = $order->orderProducts()->get();
        $products_list = [];
        foreach ($products as $product) {
            $products_list[] = $product->toArray();
        }

        $body = [
            'user_id' => $order->user_id,
            'rating' => 'bad',
            'order_products' => $products_list,
        ];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertFalse($result->status);
        $this->assertEquals($result->message, 'Se debe enviar algún comentario de los productos asociados.');
    }

    public function testShowWebForm()
    {
        $order = $this->getRandomOrder();
        $action = action('UserController@show_order_score_form', $order->user_score_token);
        $this->be($order->user);
        $this->call('GET', $action);
        $this->assertResponseOk();
    }

    public function testScoreOrderUsingUserController()
    {
        $order = $this->getRandomOrder();
        $action = action('UserController@order_score', $order->user_score_token);
        $this->be($order->user);
        $body = ['rating' => 'success',];
        $request = $this->call('POST', $action, $body);
        $result = json_decode($request->getContent());
        $this->assertTrue($result->status);
        $order = \Order::find($order->id);
        $this->assertTrue($order->user_score == 1);
    }

    public function testLogin()
    {
        $password = '123456';
        $user = $this->newUser($password);
        $email = $user->email;
        $store_id = \Store::first()->id;

        $order = $this->getRandomOrder();
        $action = action('api\v13\ApiUserController@login');
        $request = $this->call('POST', $action, compact('password', 'email', 'store_id'));
        $result = json_decode($request->getContent());
        $this->assertTrue($result->status);
        $order = \Order::find($order->id);
        $this->assertTrue($order->user_score == 1);
    }
}
