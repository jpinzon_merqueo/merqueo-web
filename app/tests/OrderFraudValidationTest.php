<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17/05/2018
 * Time: 4:28 PM
 */

namespace Tests;

use Aws\Common\Hash\TreeHash;
use Order;
use Faker\Factory;

class OrderFraudValidationTest extends TestCase
{
    public function test()
    {
        $user_1 = $this->createUsers();
        $this->assertInstanceOf(\User::class, $user_1);
        $user_2 = $this->createUsers($user_1->id);
        $this->assertInstanceOf(\User::class, $user_2);



    }

    public function createUsers($referred_by = null)
    {
        $faker= Factory::create('es_ES');
        $user = new \User();
        $user->identity_type = $faker->randomElement(['Cédula', 'NIT']);
        $user->identify_number = $faker->randomNumber(10, true);
        $user->first_name = $faker->firstName;
        $user->last_name = $faker->lastName;
        $user->email = $faker->email;
        $user->phone = $faker->phoneNumber;
        $user->phone_validated_date = $faker->dateTimeThisMonth;
        $user->password = \Hash::make('12345');
        $user->referral_code = $faker->numerify($user->first_name.'###');
        if (!empty($referred_by)) {
            $user->referred_by = $referred_by;
        }
        $user->save();
        return $user;
    }
}
