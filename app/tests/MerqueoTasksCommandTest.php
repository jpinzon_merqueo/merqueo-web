<?php

namespace Tests;

use Carbon\Carbon;
use Tests\Traits\MerqueoFactory;
use Illuminate\Support\Facades\Artisan;

class MerqueoTasksCommandTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create();
        parent::setUp();
        \DB::beginTransaction();
        \ProviderOrder::where('id', '<>', 0)->delete();
    }

    public function tearDown()
    {
        parent::tearDown();
        \DB::rollBack();
    }

    public function testMarketplaceProviderOrderSingleWarehouse()
    {
        $product_limit = 5;
        $orders_quantity = 10;
        $products_quantity = 5;
        $allied_store = \AlliedStore::whereNotNull('provider_id')->first();
        $store_products = \StoreProduct::where('allied_store_id', $allied_store->id)->limit($product_limit)->get();
        $warehouse = \Warehouse::first();

        for ($i = 0; $i <= $orders_quantity; $i++) {
            $order = $this->makeFakeOrder($warehouse);
            $order->allied_store_id = $allied_store->id;
            $order->delivery_date = \Carbon\Carbon::create()->addDays(1 + ($i === $orders_quantity ? 1 : 0));
            $order->save();

            $this->associateStoreProductsToOrder($order, $store_products, $products_quantity);
        }

        $exitCode = Artisan::call('command:merqueo-tasks', ['task'=>'make_marketplace_provider_orders']);
        $this->assertEquals($exitCode, 0);

        $providerOrder = \ProviderOrder::with('providerOrderDetails')
            ->where('provider_id', $allied_store->provider_id)
            ->latest()
            ->first();

        $this->assertCount($product_limit, $providerOrder->providerOrderDetails);
        foreach ($providerOrder->providerOrderDetails as $details) {
            $this->assertGreaterThanOrEqual($product_limit * $orders_quantity, $details->quantity_order);
        }
    }

    public function testMarketplaceProviderOrderMultipleWarehouse()
    {
        $product_limit = 5;
        $orders_quantity = 10;
        $products_quantity = 5;
        $allied_store = \AlliedStore::whereNotNull('provider_id')->first();
        $store_products = \StoreProduct::where('allied_store_id', $allied_store->id)->limit($product_limit)->get();
        $warehouses = \Warehouse::limit(2)->get();

        for ($i = 0; $i < $orders_quantity; $i++) {
            $order = $this->makeFakeOrder($warehouses[$i % 2]);
            $order->allied_store_id = $allied_store->id;
            $order->delivery_date = \Carbon\Carbon::create()->addDays(1 + ($i >= $orders_quantity - 2 ? 1 : 0));
            $order->save();

            $this->associateStoreProductsToOrder($order, $store_products, $products_quantity);
        }

        $exitCode = Artisan::call('command:merqueo-tasks', ['task'=>'make_marketplace_provider_orders']);
        $this->assertEquals($exitCode, 0);

        $providerOrders = \ProviderOrder::with('providerOrderDetails')
            ->where('provider_id', $allied_store->provider_id)
            ->get();

        $this->assertCount(2, $providerOrders);
        foreach ($providerOrders as $providerOrder) {
            $this->assertCount($product_limit, $providerOrder->providerOrderDetails);
            foreach ($providerOrder->providerOrderDetails as $details) {
                $this->assertEquals(($product_limit - 1) * $orders_quantity / 2, $details->quantity_order);
            }
        }
    }

    public function testMarketplaceProviderDoesNotAddScheduledDelivery()
    {
        $product_limit = 5;
        $orders_quantity = 10;
        $products_quantity = 5;
        $allied_store = \AlliedStore::whereNotNull('provider_id')->first();
        $store_products = \StoreProduct::where('allied_store_id', $allied_store->id)->limit($product_limit)->get();
        $warehouses = \Warehouse::get();
        $tomorrow = Carbon::create()->addDay();

        for ($i = 0; $i < $orders_quantity; $i++) {
            $current_warehouse = $warehouses[$i % 2];

            $order = $this->makeFakeOrder($current_warehouse);
            $order->allied_store_id = $allied_store->id;

            $child_order = $this->makeFakeOrder($current_warehouse);
            $child_order->delivery_date = $tomorrow;
            $child_order->parent_order_id = $order->id;
            $child_order->allied_store_id = $allied_store->id;

            $order->child_order_id = $child_order->id;
            $order->status = 'Cancelled';

            if ($i >= $orders_quantity - 2) {
                $order->management_date = Carbon::create()->subHour(23);
            } else {
                $order->management_date = Carbon::create()->subDays(2);
            }

            $order->save();
            $child_order->save();

            $this->associateStoreProductsToOrder($order, $store_products, $products_quantity);
            $this->associateStoreProductsToOrder($child_order, $store_products, $products_quantity);
        }

        $exitCode = Artisan::call('command:merqueo-tasks', ['task'=>'make_marketplace_provider_orders']);
        $this->assertEquals($exitCode, 0);

        $providerOrders = \ProviderOrder::with('providerOrderDetails')
            ->where('provider_id', $allied_store->provider_id)
            ->get();
        $this->assertCount(2, $providerOrders);
        foreach ($providerOrders as $providerOrder) {
            $this->assertCount($product_limit, $providerOrder->providerOrderDetails);
            foreach ($providerOrder->providerOrderDetails as $details) {
                $this->assertEquals(($product_limit - 1) * $orders_quantity / 2, $details->quantity_order);
            }
        }
    }

    public function testMarketplaceOnWeekends()
    {
        $friday = $this->getNextFriday();
        Carbon::setTestNow($friday);

        $product_limit = 5;
        $orders_quantity = 10;
        $products_quantity = 5;
        $allied_store = \AlliedStore::whereNotNull('provider_id')->first();
        $store_products = \StoreProduct::where('allied_store_id', $allied_store->id)->limit($product_limit)->get();
        $warehouses = \Warehouse::limit(2)->get();

        for ($i = 0; $i < $orders_quantity; $i++) {
            $order = $this->makeFakeOrder($warehouses[$i % 2]);
            $order->allied_store_id = $allied_store->id;
            $order->delivery_date = \Carbon\Carbon::create()->addDays(1 + ($i >= $orders_quantity - 2 ? 3 : 0));
            $order->save();

            $this->associateStoreProductsToOrder($order, $store_products, $products_quantity);
        }

        $exitCode = Artisan::call('command:merqueo-tasks', ['task'=>'make_marketplace_provider_orders']);
        $this->assertEquals($exitCode, 0);

        $providerOrders = \ProviderOrder::with('providerOrderDetails')
            ->where('provider_id', $allied_store->provider_id)
            ->get();
        $this->assertCount(2, $providerOrders);
        foreach ($providerOrders as $providerOrder) {
            $this->assertCount($product_limit, $providerOrder->providerOrderDetails);
            foreach ($providerOrder->providerOrderDetails as $details) {
                $this->assertEquals(($product_limit - 1) * $orders_quantity / 2, $details->quantity_order);
            }
        }
    }

    public function testMarketplaceOnHolidays()
    {
        $holiday = Carbon::create(2018, 3, 16);
        Carbon::setTestNow($holiday);

        $product_limit = 5;
        $orders_quantity = 10;
        $products_quantity = 5;
        $allied_store = \AlliedStore::whereNotNull('provider_id')->first();
        $store_products = \StoreProduct::where('allied_store_id', $allied_store->id)->limit($product_limit)->get();
        $warehouses = \Warehouse::get();

        for ($i = 0; $i < $orders_quantity; $i++) {
            $order = $this->makeFakeOrder($warehouses[$i % 2]);
            $order->allied_store_id = $allied_store->id;
            $order->delivery_date = \Carbon\Carbon::create()->addDays(4 + ($i >= $orders_quantity - 2 ? 1 : 0));
            $order->save();

            $this->associateStoreProductsToOrder($order, $store_products, $products_quantity);
        }

        $exitCode = Artisan::call('command:merqueo-tasks', ['task'=>'make_marketplace_provider_orders']);
        $this->assertEquals($exitCode, 0);

        $providerOrders = \ProviderOrder::with('providerOrderDetails')
            ->where('provider_id', $allied_store->provider_id)
            ->get();
        $this->assertCount(2, $providerOrders);
        foreach ($providerOrders as $providerOrder) {
            $this->assertCount($product_limit, $providerOrder->providerOrderDetails);
            foreach ($providerOrder->providerOrderDetails as $details) {
                $this->assertEquals(($product_limit - 1) * $orders_quantity / 2, $details->quantity_order);
            }
        }
    }

    public function testMarketplaceOnTakeNextHolidays()
    {
        $holiday = Carbon::create(2018, 3, 28);
        Carbon::setTestNow($holiday);

        $product_limit = 5;
        $orders_quantity = 10;
        $products_quantity = 5;
        $allied_store = \AlliedStore::whereNotNull('provider_id')->first();
        $store_products = \StoreProduct::where('allied_store_id', $allied_store->id)->limit($product_limit)->get();
        $warehouses = \Warehouse::get();

        for ($i = 0; $i < $orders_quantity; $i++) {
            $order = $this->makeFakeOrder($warehouses[$i % 2]);
            $order->allied_store_id = $allied_store->id;
            $order->delivery_date = \Carbon\Carbon::create()->addDays(5 + ($i >= $orders_quantity - 2 ? 1 : 0));
            $order->save();

            $this->associateStoreProductsToOrder($order, $store_products, $products_quantity);
        }

        $exitCode = Artisan::call('command:merqueo-tasks', ['task'=>'make_marketplace_provider_orders']);
        $this->assertEquals($exitCode, 0);

        $providerOrders = \ProviderOrder::with('providerOrderDetails')
            ->where('provider_id', $allied_store->provider_id)
            ->get();
        $this->assertCount(2, $providerOrders);
        foreach ($providerOrders as $providerOrder) {
            $this->assertCount($product_limit, $providerOrder->providerOrderDetails);
            foreach ($providerOrder->providerOrderDetails as $details) {
                $this->assertEquals(($product_limit - 1) * $orders_quantity / 2, $details->quantity_order);
            }
        }
    }
}
