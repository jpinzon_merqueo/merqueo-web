<?php

namespace Tests;

use api\v13\help_center\HelpCenterMenu;
use Carbon\Carbon;
use orders\OrderStatus;
use Tests\Traits\MerqueoFactory;

/**
 * Class HelpCenterTest
 * @package Tests
 */
class HelpCenterTest extends TestCase
{

    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }


}
