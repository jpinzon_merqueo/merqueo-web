<?php

namespace Tests;

use DB;
use Order;
use OrderProduct;
use StoreProduct;
use Tests\Traits\MerqueoFactory;

/**
 * Class OrderProductTest
 * @package Tests
 * @author Alejandro Rivera <ariveray@merqueo.com>
 */
class OrderProductTest extends TestCase
{
    use MerqueoFactory;

    /**
     * @var Order
     */
    private $fakeOrder;

    /**
     * @var StoreProduct
     */
    private $storeProduct;

    /**
     * @var StoreProduct
     */
    private $groupProduct;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();

        $this->storeProduct = $this->getStoreProduct();
        $this->groupProduct = $this->getStoreProductGroup();
        $this->fakeOrder = $this->makeFakeOrder();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::setUp();
        DB::commit();
    }

    public function testSaveSimpleProduct()
    {
        $orderProduct = OrderProduct::saveProduct($this->storeProduct->price, 1, $this->storeProduct, $this->fakeOrder);
        $this->assertEquals($orderProduct->price, $this->storeProduct->price);
        $this->assertEquals($orderProduct->original_price, $this->storeProduct->price);
        $this->assertEquals($orderProduct->base_price, $this->storeProduct->base_price);
    }

    public function testSaveSimpleProductWithSpecialPrice()
    {
        $orderProduct = OrderProduct::saveProduct($this->storeProduct->price * .95, 1, $this->storeProduct,
            $this->fakeOrder);
        $this->assertEquals($orderProduct->price, $this->storeProduct->price * .95);
        $this->assertEquals($orderProduct->original_price, $this->storeProduct->price);
        $this->assertEquals($orderProduct->base_price, $this->storeProduct->base_price * .95);
    }

    public function testSaveProductGroup()
    {
        $orderProduct = OrderProduct::saveProduct($this->groupProduct->price, 1, $this->groupProduct, $this->fakeOrder);

        $this->assertNotEmpty($orderProduct->orderProductGroup);

        foreach ($orderProduct->orderProductGroup as $orderProductGroup) {
            $this->assertEquals($orderProductGroup->price, $orderProductGroup->storeProduct->price);
            $this->assertEquals($orderProductGroup->original_price, $orderProductGroup->storeProduct->price);
            $this->assertEquals($orderProductGroup->base_price, $orderProductGroup->storeProduct->base_price);
            $this->assertEmpty($orderProductGroup->merqueo_discount);
            $this->assertEquals($orderProductGroup->seller_discount);
            $this->assertEquals($orderProductGroup->provider_discount);
        }
    }

    public function testSaveProductGroupWithSpecialPrice()
    {
        $discount = 0.05;
        $singleProductDiscount = $this->groupProduct->price * $discount;
        $this->groupProduct->special_price = $this->groupProduct->price - $this->groupProduct->price * $discount;

        foreach ($this->groupProduct->storeProductGroup as $storeProduct) {
            $storeProduct->discount_amount = $singleProductDiscount / $this->groupProduct->storeProductGroup->count();
            $storeProduct->merqueo_discount = 100;
            $storeProduct->provider_discount = 0;
            $storeProduct->seller_discount = 0;
            $storeProduct->save();
        }

        $orderProduct = OrderProduct::saveProduct(
            $this->groupProduct->price * (1 - $discount),
            1,
            $this->groupProduct,
            $this->fakeOrder
        );

        $this->assertNotEmpty($orderProduct->orderProductGroup);

        foreach ($orderProduct->orderProductGroup as $index => $orderProductGroup) {
            $singleProduct = $this->groupProduct->storeProductGroup[$index]->storeProductGroup;
            $expectedPrice = $singleProduct->price - $this->groupProduct->storeProductGroup[$index]->discount_amount;

            $this->assertEquals($orderProductGroup->original_price, $singleProduct->price);
            $this->assertEquals(
                $expectedPrice,
                $orderProductGroup->price,
                "The original price is {$singleProduct->price} and the discount is " .
                "{$this->groupProduct->storeProductGroup[$index]->discount_amount}, current " .
                "price should be {$expectedPrice}"
            );
            $this->assertEquals(
                $orderProductGroup->base_price,
                $orderProductGroup->price / (1 + ($singleProduct->iva / 100)),
                'Base price should be equals to the selling price without taxes',
                0.0001
            );
        }
    }
}
