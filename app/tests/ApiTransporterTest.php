<?php

namespace Tests;

class ApiTransporterTest extends TestCase
{
    public function testTokenRequest()
    {
        $response = $this->call('GET', '/api/transporter/1.0/token');
        $this->assertResponseOk();
        $data = json_decode($response->getContent());
        $this->assertTrue(!empty($data->message->token));
    }
}
