<?php

namespace Tests\Feature\app\Http\Controllers\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\Admin\AdminRouteController;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\Routes;

class AdminRouteControllerTest extends TestCase
{
    use DatabaseTransactions, WithoutMiddleware;

    public $controller = null;

    public function setUp()
    {
        parent::setUp();
        $this->controller = \App::make(
            'App\Http\Controllers\Admin\AdminRouteController'
        );
    }

    public function dispatchHourProvider()
    {
        return [
            ['05:00:00', 1, 1, '05:00:00'],
            ['05:00:00', 2, 1, '05:00:00'],
            ['05:00:00', 3, 1, '05:10:00'],
            ['05:00:00', 4, 1, '05:10:00'],
            ['05:00:00', 5, 1, '05:20:00'],
            ['05:00:00', 6, 1, '05:20:00'],
            ['05:00:00', 7, 1, '05:30:00'],
            ['06:00:00', 1, 2, '06:00:00'],
            ['06:00:00', 2, 2, '06:00:00'],
            ['06:00:00', 3, 2, '06:00:00'],
            ['06:00:00', 4, 2, '06:10:00'],
        ];
    }

    /**
     * Prueba método privado Admin\AdminRouteController@calculateDispatchTime.
     *
     * @dataProvider dispatchHourProvider
     *
     * @return void
     */
    public function testCalculateDispatchTime(
        $dispatchTime,
        $pickingPriority,
        $pickingGroups,
        $expected
    ) {
        $this->assertInstanceOf(
            AdminRouteController::class,
            $this->controller,
            'No es una instancia de AdminRouteController'
        );
        $return = $this->invokeMethod(
            $this->controller,
            'calculateDispatchTime',
            [$pickingPriority, $dispatchTime, $pickingGroups]
        );
        $this->assertEquals($expected, $return);
    }

    /**
     * Prueba servicio Admin\AdminRouteController@update_orders_route.
     *
     * @return void
     */
    public function testUpdateOrdersRoute()
    {
        $routeOrigin = factory(Routes::class)->create([
            'picking_priority' => 1,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);
        $routeMiddle = factory(Routes::class)->create([
            'picking_priority' => 2,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);
        $routeDestination = factory(Routes::class)->create([
            'picking_priority' => 3,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);

        factory(Order::class)->create([
            'route_id' => $routeOrigin->id,
            'planning_sequence' => 1,
        ]);
        factory(Order::class)->create([
            'route_id' => $routeMiddle->id,
            'planning_sequence' => 2,
        ]);
        factory(Order::class)->create([
            'route_id' => $routeDestination->id,
            'planning_sequence' => 2,
        ]);

        $response = $this->post(
            route('adminRoutes.joinOrdersToRoute'),
            [
                'origin_route_id' => $routeOrigin->id,
                'destination_route_id' => $routeDestination->id,
                'route_sequences' => 1,
                'warehouse_id' => 2,
                'all_route' => 0,
            ],
            [
                'X-Requested-With' => 'XMLHttpRequest',
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $array = $response->decodeResponseJson();
        $this->assertTrue($array['status']);

        $doesNotExist = Routes::find($routeOrigin->id);
        $this->assertNull($doesNotExist);

        $route = Routes::find($routeMiddle->id);
        $this->assertEquals(1, $route->picking_priority);

        $route = Routes::find($routeDestination->id);
        $this->assertEquals(2, $route->picking_priority);
    }

    /**
     * Prueba servicio Admin\AdminRouteController@update_picking_priority
     *
     * @return void
     */
    public function testUpdatePickingPriority()
    {
        $firstRoute = factory(Routes::class)->create([
            'picking_priority' => 0,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 04:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 140,
        ]);
        $secondRoute = factory(Routes::class)->create([
            'picking_priority' => 2,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 04:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 140,
        ]);
        $thirdRoute = factory(Routes::class)->create([
            'picking_priority' => 3,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 04:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 140,
        ]);
        $fourthRoute = factory(Routes::class)->create([
            'picking_priority' => 40,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 04:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 140,
        ]);

        factory(Order::class)->create([
            'route_id' => $firstRoute->id,
            'planning_sequence' => 1,
        ]);
        factory(Order::class)->create([
            'route_id' => $secondRoute->id,
            'planning_sequence' => 2,
        ]);
        factory(Order::class)->create([
            'route_id' => $thirdRoute->id,
            'planning_sequence' => 3,
        ]);
        factory(Order::class)->create([
            'route_id' => $fourthRoute->id,
            'planning_sequence' => 4,
        ]);

        $json = [
            'route_' . $firstRoute->id => 3,
            'route_' . $secondRoute->id => 1,
            'route_' . $thirdRoute->id => 2,
            'route_' . $fourthRoute->id => 4,
        ];

        $response = $this->post(
            route('adminRoutes.updatePickingPriority'),
            [
                'picking_priority' => json_encode($json),
                'city_id' => 1,
                'warehouse_id' => 2,
                'date' => date('Y-m-d', strtotime('tomorrow')),
                'shift' => 'AM',
            ],
            [
                'X-Requested-With' => 'XMLHttpRequest',
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $array = $response->decodeResponseJson();
        $this->assertTrue($array['status']);

        $route = Routes::find($firstRoute->id);
        $this->assertEquals(3, $route->picking_priority);
        $this->assertEquals(
            date('Y-m-d', strtotime('tomorrow')) . ' 06:00:00',
            $route->dispatch_time
        );

        $route = Routes::find($secondRoute->id);
        $this->assertEquals(1, $route->picking_priority);
        $this->assertEquals(
            date('Y-m-d', strtotime('tomorrow')) . ' 06:00:00',
            $route->dispatch_time
        );

        $route = Routes::find($thirdRoute->id);
        $this->assertEquals(2, $route->picking_priority);
        $this->assertEquals(
            date('Y-m-d', strtotime('tomorrow')) . ' 06:00:00',
            $route->dispatch_time
        );

        $route = Routes::find($fourthRoute->id);
        $this->assertEquals(4, $route->picking_priority);
        $this->assertEquals(
            date('Y-m-d', strtotime('tomorrow')) . ' 06:10:00',
            $route->dispatch_time
        );
    }

    /**
     * Prueba servicio Admin\AdminRouteController@update_status
     *
     * @return void
     */
    public function testCheckFinishPlanning()
    {
        $routeOrigin = factory(Routes::class)->create([
            'picking_priority' => 1,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);
        $routeMiddle = factory(Routes::class)->create([
            'picking_priority' => 2,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);
        $routeDestination = factory(Routes::class)->create([
            'picking_priority' => 3,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);
        $routeForeverAlone = factory(Routes::class)->create([
            'picking_priority' => 4,
            'dispatch_time' =>
                date('Y-m-d', strtotime('tomorrow')) . ' 05:00:00',
            'warehouse_id' => 2,
            'shift' => 'AM',
            'zone_id' => 135,
        ]);

        factory(Order::class)->create([
            'route_id' => $routeOrigin->id,
            'planning_sequence' => 1,
        ]);
        factory(Order::class)->create([
            'route_id' => $routeMiddle->id,
            'planning_sequence' => 2,
        ]);
        factory(Order::class)->create([
            'route_id' => $routeDestination->id,
            'planning_sequence' => 2,
        ]);

        $response = $this->post(
            route('adminRoutes.joinOrdersToRoute'),
            [
                'origin_route_id' => $routeOrigin->id,
                'destination_route_id' => $routeDestination->id,
                'route_sequences' => 1,
                'warehouse_id' => 2,
                'all_route' => 0,
            ],
            [
                'X-Requested-With' => 'XMLHttpRequest',
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $array = $response->decodeResponseJson();
        $this->assertTrue($array['status']);

        $response = $this->get(
            route('adminRoutes.updateStatus', [
                'city_id' => 1,
                'warehouse_id' => 2,
                'date' => date('Y-m-d', strtotime('tomorrow')),
                'shift' => 'AM',
                'status' => 'Terminada'
            ]),
            [
                'X-Requested-With' => 'XMLHttpRequest',
            ]
        );

        $session_store = $this->app['session.store'];
        $token = $session_store->all();
        $this->assertEquals('success', $token['type']);
        $this->assertEquals('Se han actualizado 3 rutas con éxito.', $token['message']);
        $this->assertTrue($array['status']);
    }

    /**
     * Llama métodos protegidos/privados de una clase.
     *
     * @param object &$object    Objeto instanciando al que pertenece el método.
     * @param string $methodName Método que se va a llamar
     * @param array  $parameters Arreglo de parámetros que se pasarán al método.
     *
     * @return mixed Valores retornados por el método.
     */
    public function invokeMethod(
        &$object,
        $methodName,
        array $parameters = array()
    ) {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
