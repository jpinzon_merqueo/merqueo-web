<?php

namespace Tests;

use Carbon\Carbon;
use repositories\EloquentDiscountRepository;

/**
 * Class EloquentDiscountRepositoryTest
 * @package Tests
 * @property-read \Mockery|\Discount
 */
class EloquentDiscountRepositoryTest extends TestCase
{
    /**
     * @var \Discount|\Mockery\MockInterface
     */
    private $discount;

    /**
     * @var string
     */
    private $datetime;

    /**
     * @var int
     */
    private $cityId;

    /**
     * @var int
     */
    private $storeId;

    /**
     * @var int
     */
    private $subtotal;

    public function setUp()
    {
        parent::setUp();
        $this->discount = \Mockery::mock(\Discount::class);
        $this->datetime = Carbon::now()->format('Y-m-d H:i:s');
        $this->cityId = 1;
        $this->storeId = 63;
        $this->subtotal = 300000;
    }

    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    /**
     *  Success find by date city and subtotal
     */
    public function testSuccessFindByDateCitySubtotal()
    {
        $usecase = new EloquentDiscountRepository($this->discount);

        $this->discount->shouldReceive('whereRaw')->andReturn($this->discount);
        $this->discount->shouldReceive('whereNull')->andReturn($this->discount);
        $this->discount->shouldReceive('where')->andReturn($this->discount);
        $this->discount->shouldReceive('get')->andReturn($this->discount);

        $result = $usecase->findByDateCitySubtotal((object) [$this->datetime, $this->cityId, $this->subtotal]);

        $this->assertNull($result);
    }


    /**
     *  Success find by date city and subtotal
     */
    public function testErrorFindByDateCitySubtotal()
    {
        $usecase = new EloquentDiscountRepository($this->discount);

        $this->discount->shouldReceive('whereRaw')->andReturn($this->discount);
        $this->discount->shouldReceive('whereNull')->andReturn($this->discount);
        $this->discount->shouldReceive('where')->andReturn($this->discount);
        $this->discount->shouldReceive('get')->andReturn($this->discount);

        $result = $usecase->findByDateCityStoreSubtotal((object) [
            $this->datetime,
            $this->cityId,
            $this->storeId,
            $this->subtotal
        ]);

        $this->assertNull($result);
    }
}
