<?php

namespace Tests;

use Illuminate\Support\Facades\DB;
use Tests\Traits\MerqueoFactory;
use Warehouse;
use Zone;
use StoreProduct;

class ApiCheckoutTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();
        DB::rollBack();
    }

    public function testGetAddressInfo()
    {
        $city = 'bogota';
        $address = 'Calle 170 # 15 - 15';
        $app_version = 300;
        $url = action('api\v13\ApiStoreController@get_store', compact('city', 'address', 'app_version'));
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertCount(1, $response->result->addresses);

        $address = 'Calle 12 # 12 - 12';
        $url = action('api\v13\ApiStoreController@get_store', compact('city', 'address', 'app_version'));
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertCount(2, $response->result->addresses);
    }

    public function testGetInvalidAddressInfo()
    {
        $city = 'bogota';
        $address = 'Calle 1000 # 1000 - 1000';
        $app_version = 300;
        $url = action('api\v13\ApiStoreController@get_store', compact('city', 'address', 'app_version'));
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $this->assertFalse($response->status);
        $this->assertEmpty(get_object_vars($response->result));
    }

    public function testWithEmptyCartAndWarehouse()
    {
        $url = action('api\v13\ApiCheckoutController@validate_coordinates', [
            'lat' => '4.75209473',
            'lng' => '-74.08867971',
            'cart' => '{}'
        ]);
        $request = $this->call('POST', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
    }

    public function testClearBuyCartByWarehouse()
    {
        $otherWarehouse = Warehouse::first();
        $warehouse = $this->newWarehouse();
        $currenZone = Zone::getByLatLng($warehouse->city_id, '4.75209473', '-74.08867971');
        $currenZone->warehouse_id = $warehouse->id;
        $currenZone->save();

        $storeProducts = StoreProduct::with('product')
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();

        $excluded = [];

        $cart = new \StdClass();
        foreach ($storeProducts as $index => $storeProduct) {
            $sProduct = $this->stockOnWarehouse($warehouse, $storeProduct);
            if ($index % 7 === 0) {
                $sProduct->status = 0;
                $sProduct->save();
                $excluded[] = $storeProduct->id;
            }

            $cart->{$storeProduct->id} = [
                'qty' => 99,
                'price' => 100,
                'name' => $storeProduct->product->name,
            ];
        }

        $cart->XXXX = [
            'qty' => 99,
            'price' => 100,
            'name' => 'XXXX',
        ];

        $url = action('api\v13\ApiCheckoutController@validate_coordinates', [
            'zone_id' => $otherWarehouse->zones()->first()->id,
            'lat' => '4.75209473',
            'lng' => '-74.08867971',
            'cart' => json_encode($cart),
        ]);
        $request = $this->call('GET', $url);
        $this->assertResponseOk();
        $response = json_decode($request->getContent());
        $cart = json_decode($response->result->cart);
        $this->assertNotEmpty($response->result->zone_id, 'Se debe remover el product con un id que no existe.');
        $cart_array = (array)$cart;

        // El producto con el index 0 y 7 se deshabilita.
        $this->assertCount(count($storeProducts) - count($excluded), $cart_array);
        $this->assertTrue(empty($cart_array['XXXX']), 'Se debe remover el product con un id que no existe.');

        foreach ($storeProducts as $storeProduct) {
            if (in_array($storeProduct->id, $excluded)) {
                $this->assertTrue(empty($cart->$storeProduct->id));
            } else {
                $this->assertNotEmpty($cart->{$storeProduct->id}, 'El elemento no debe ser eliminado existe.');
            }
        }
    }
}
