<?php

namespace Tests;

use Tests\TestCase;
use Tests\Traits\MerqueoFactory;
use Illuminate\Support\Facades\DB;

class CheckoutControllerTest extends TestCase
{
    private $dir = ['Carrera', ' ', '54 d', ' ', '#', ' ', '188', '-', '41'];
    private $data = [
        'first_name' => 'Ariel',
        'last_name' => 'Patiño',
        'email' => 'arielyonoexisto@mailinator.com',
        'password' => 'prueba',
        'phone' => '3211234567',
        'user_phone' => '3211234567',
        'create_user' => 1,
        'address_name' => 'Casa',
        'address' => 'Cra 54D #188 41',
        'address_further' => 'int 6 apto 502',
        'address_neighborhood' => 'Mirandela',
        'lat' => 4.76760854,
        'lng' => -74.04696197,
        'city' => 'Bogota',
        'cart' => null,
        'dir' => null,
        'payment_method' => 'Efectivo',
        'delivery_day_63' => null,
        'delivery_time_63' => 2,
        'source_os' => 'iOS',
        'comments' => 'Que llegue completo gracias.',
        'city_id' => 1,
    ];

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();
        DB::rollBack();
    }

    /**
     * Prueba checkout en efectivo con compra mayor a $ 400,000
     *
     * @return void
     */
    public function testCheckoutFailCash()
    {
        $cart = [
            1 => [
                'qty' => 40,
                'price' => 12490,
                'qty_full_price' => 12490,
            ],
        ];
        $tmp_cart = [
            'products' => [
                '1' => [
                    'cart_quantity' => 40,
                    'cart_quantity_full_price' => 0,
                ],
            ],
        ];
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $this->session([
            'phoneValidation.validated' => true,
            'phoneValidation.number' => '3211234567',
            'tmp_cart' => json_encode($tmp_cart),
        ]);

        $headers = ['X-Requested-With' => 'XMLHttpRequest'];
        $data = $this->data;
        $data['cart'] = $cart;
        $data['dir'] = $this->dir;
        $data['delivery_day_63'] = date('Y-m-d', strtotime('tomorrow'));

        $response = $this->call('post', '/checkout', $data);
        $this->assertContains('/orden_confirmada', $response->getContent());
        $this->assertEquals(302, $response->getStatusCode());
        $reference = $this->getReference($response->getTargetUrl());
        $order = DB::table('orders')
            ->where('reference', $reference)
            ->first();

        $this->assertEquals('Validation', $order->status);
        $this->assertEquals(
            'Pedido con posible fraude por que tiene: 6 puntos, excede monto máximo',
            $order->posible_fraud
        );
    }
    /**
     * Prueba checkout
     *
     * @return void
     */
    public function testCheckout()
    {
        $cart = [
            1 => [
                'qty' => 10,
                'price' => 12490,
                'qty_full_price' => 12490,
            ],
        ];
        $tmp_cart = [
            'products' => [
                '1' => [
                    'cart_quantity' => 10,
                    'cart_quantity_full_price' => 0,
                ],
            ],
        ];
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $this->session([
            'phoneValidation.validated' => true,
            'phoneValidation.number' => '3211234567',
            'tmp_cart' => json_encode($tmp_cart),
        ]);

        $headers = ['X-Requested-With' => 'XMLHttpRequest'];
        $data = $this->data;
        $data['cart'] = $cart;
        $data['dir'] = $this->dir;
        $data['delivery_day_63'] = date('Y-m-d', strtotime('tomorrow'));

        $response = $this->call('post', '/checkout', $data);
        $this->assertContains('/orden_confirmada', $response->getContent());
        $this->assertEquals(302, $response->getStatusCode());
        $reference = $this->getReference($response->getTargetUrl());
        $order = DB::table('orders')
            ->where('reference', $reference)
            ->first();

        $this->assertEquals('Initiated', $order->status);

        return $order->user_id;
    }

    /**
     * Prueba checkout con pago online
     *
     * @depends testCheckout
     *
     * @param int $user_id
     *
     * @return void
     */
    public function testCheckoutOnlinePayment($user_id)
    {
        $cart = [
            1 => [
                'qty' => 10,
                'price' => 12490,
                'qty_full_price' => 12490,
            ],
        ];
        $tmp_cart = [
            'products' => [
                '1' => [
                    'cart_quantity' => 10,
                    'cart_quantity_full_price' => 0,
                ],
            ],
        ];

        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $data = $this->data;
        $data['cart'] = $cart;
        $data['dir'] = $this->dir;
        $data['delivery_day_63'] = date('Y-m-d', strtotime('tomorrow'));
        $data['payment_method'] = 'Tarjeta de crédito';
        $data['user_id'] = $user_id;
        $creditCard = [
            'number_cc' => '4111113386903438',
            'code_cc' => '123',
            'expiration_year_cc' => date('Y', strtotime('next year')),
            'expiration_month_cc' => '12',
            'document_type_cc' => 'CC',
            'document_number_cc' => '18004718',
            'name_cc' => 'APPROVED',
            'phone_cc' => '3211234567',
            'address_cc' => 'Cr. 54 188-41',
        ];
        $data = $data + $creditCard;

        $this->session([
            'phoneValidation.validated' => true,
            'phoneValidation.number' => '3211234567',
            'tmp_cart' => json_encode($tmp_cart),
        ]);

        $headers = ['X-Requested-With' => 'XMLHttpRequest'];
        $response = $this->call('post', '/checkout', $data);
        $this->assertContains('/orden_confirmada', $response->getContent());
        $this->assertEquals(302, $response->getStatusCode());

        $reference = $this->getReference($response->getTargetUrl());

        $order = DB::table('orders')
            ->where('reference', $reference)
            ->first();

        DB::table('orders')
            ->where('id', $order->id)
            ->update(['status' => 'Delivered']);

        // Entra a validación por ser el primer pedido con tarjeta
        $this->assertEquals('Validation', $order->status);

        return (int)$order->user_id;
    }

    /**
     * Prueba checkout con pago online con sospecha de fraude por exceder el monto máximo
     *
     * @depends testCheckoutOnlinePayment
     *
     * @param int $user_id
     *
     * @return void
     */
    public function testPossibleFraudCheckoutOnlinePayment($user_id)
    {
        $cart = [
            1 => [
                'qty' => 40,
                'price' => 12490,
                'qty_full_price' => 12490,
            ],
        ];
        $tmp_cart = [
            'products' => [
                '1' => [
                    'cart_quantity' => 40,
                    'cart_quantity_full_price' => 0,
                ],
            ],
        ];

        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $data = $this->data;
        $data['cart'] = $cart;
        $data['dir'] = $this->dir;
        $data['delivery_day_63'] = date('Y-m-d', strtotime('tomorrow'));
        $data['payment_method'] = 'Tarjeta de crédito';
        $data['user_id'] = $user_id;

        $creditCard = [
            'number_cc' => '4111115940854379',
            'code_cc' => '123',
            'expiration_year_cc' => date('Y', strtotime('next year')),
            'expiration_month_cc' => '12',
            'document_type_cc' => 'CC',
            'document_number_cc' => '18004718',
            'name_cc' => 'APPROVED',
            'phone_cc' => '3211234567',
            'address_cc' => 'Cr. 54 188-41',
        ];
        $data = $data + $creditCard;

        $this->session([
            'phoneValidation.validated' => true,
            'phoneValidation.number' => '3211234567',
            'tmp_cart' => json_encode($tmp_cart),
        ]);
        $headers = ['X-Requested-With' => 'XMLHttpRequest'];
        $response = $this->call('post', '/checkout', $data);
        $this->assertContains('/orden_confirmada', $response->getContent());
        $this->assertEquals(302, $response->getStatusCode());
        $reference = $this->getReference($response->getTargetUrl());
        $order = DB::table('orders')
            ->where('reference', $reference)
            ->first();
        $this->assertEquals('Validation', $order->status);
        $this->assertEquals(
            'Pedido con posible fraude por que tiene: 6 puntos, excede monto máximo',
            $order->posible_fraud
        );
    }

    private function getReference($string)
    {
        preg_match('/orden_confirmada\/([^?]*)/', $string, $matches);
        return $matches[1];
    }
}
