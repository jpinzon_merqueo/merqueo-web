<?php


if (!function_exists('send_mail'))
{
    function send_mail($mail, $attachments = false)
    {
        $mailMock = \Tests\Helpers\EmailHelper::getInstance();
        $mailMock->addTry();

        $vars = [
            'website_name' => Config::get('app.website_name'),
            'user_url' => url('/mi-cuenta'),
            'contact_email' => Config::get('app.admin_email'),
            'contact_phone' => Config::get('app.admin_phone'),
            'android_url' => Config::get('app.android_url'),
            'ios_url' => Config::get('app.ios_url'),
            'facebook_url' => Config::get('app.facebook_url'),
            'twitter_url' => Config::get('app.twitter_url')
        ];

        $vars = array_merge($vars, $mail['vars']);

        if (empty($mail['template_name'])) {
            throw new InvalidArgumentException('Parameter "template_name" must be provided.');
        }

        $content = View::make($mail['template_name'], $vars);

        if (empty($content)) {
            throw new InvalidArgumentException('Mail content is empty.');
        }

        if (empty($mail['to'])) {
            throw new InvalidArgumentException('Parameter "to" must be provided.');
        }

        foreach ($mail['to'] as $data) {
            if (empty($data['email']) || empty($data['name'])) {
                throw new InvalidArgumentException('Parameters "email" and "name" must be provided.');
            }
        }

        if (empty($mail['subject'])) {
            throw new InvalidArgumentException('Parameters "subject" must be provided.');
        }

        // TODO validar la variable attachments.

        $mailMock->addHistory(true);
    }
}