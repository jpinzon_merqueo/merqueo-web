<?php

namespace Tests;

use admin\inventory\storage\ProductsTransferController;
use Tests\Traits\MerqueoFactory;

class ProductsTransferControllerTest extends TestCase
{
    use MerqueoFactory;

    public function testStore()
    {
        $products = '[{"product_id":175849,"reference":"25293003552","name":"Leche de almendras vainilla Silk X6","quantity":24,"unit":"Und","store_product_id":6630,"warehouse_storage_id":116859,"storage_type":"Seco","position_height_from":"E","position_from":5,"expiration_date":"2018-09-22","transfer_quantity":"20"},{"product_id":173564,"reference":"7702001029723","name":"Leche Saborizada chocolate Alpin caja x 6","quantity":22,"unit":"Und","store_product_id":1152,"warehouse_storage_id":98134,"storage_type":"Seco","position_height_from":"D","position_from":5,"expiration_date":"2019-03-28","transfer_quantity":"6"},{"product_id":176769,"reference":"7702177019887","name":"Leche más Alquería pack x 6","quantity":10,"unit":"Lt","store_product_id":8305,"warehouse_storage_id":98026,"storage_type":"Seco","position_height_from":"D","position_from":5,"expiration_date":"2018-08-14","transfer_quantity":"5"}]';
        $response = $this->call('POST', route('productTransfer.store'), ['origin_warehouse_id' => 1, 'destiny_warehouse_id' => 2, 'admin_id' => 34, 'products' => $products]);
        $this->assertRedirectedToRoute('productTransfer.show');
    }
}
