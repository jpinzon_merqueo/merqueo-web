<?php

namespace Tests;


use Carbon\Carbon;
use Tests\Traits\MerqueoFactory;

class DeliveryDatesTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::setUp();
        \DB::rollback();
    }

    public function testGetSlotsOnSameDay()
    {
        $window = $this->newDeliveryWindow();
        $store = $this->newStore($window->city);
        $warehouse = $this->newWarehouse($window->city);
        $zone = $this->newZone($warehouse);
        $this->newSlotZoneWithWindow($zone, $window);

        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals(1, $slots->count());
        $this->checkForObjectType($slots);
    }

    public function testGetSlotsOnOtherDays()
    {
        $window = $this->newDeliveryWindow();
        $window->shifts = \DeliveryWindow::SHIFTS[0];
        $window->save();

        $store = $this->newStore($window->city);
        $warehouse = $this->newWarehouse($window->city);
        $zone = $this->newZone($warehouse);
        $this->newSlotZoneWithWindow($zone, $window);

        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals(1, $slots->count());
        $this->checkForObjectType($slots);
    }

    public function testHideSlotWithNoOrderAmount()
    {
        $window = $this->newDeliveryWindow();
        $store = $this->newStore($window->city);
        $warehouse = $this->newWarehouse($window->city);
        $zone = $this->newZone($warehouse);
        $slot = $this->newSlotZoneWithWindow($zone, $window);
        $slot->number_products = 0;
        $slot->save();

        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals(0, $slots->count());
    }

    public function testHideSlotWithOrderLimitReached()
    {
        $window = $this->newDeliveryWindow();
        $window->shifts = \DeliveryWindow::SHIFTS[2];
        $window->save();

        $store = $this->newStore($window->city);
        $warehouse = $this->newWarehouse($window->city);
        $zone = $this->newZone($warehouse);

        $limitHour = explode(':', $window->hour_end)[0];
        $tomorrow = Carbon::create(null, null, null, $limitHour, 0, 0)->addDay();
        $slot = $this->newSlotZoneWithWindow($zone, $window);
        $slot->day = $tomorrow->dayOfWeek;
        $slot->number_orders = 1;
        $slot->save();

        $order = $this->makeFakeOrder($warehouse);
        $order->delivery_date = $tomorrow;
        $order->deliveryWindow()->associate($window);
        $order->save();
        $order->orderGroup->zone()->associate($zone);
        $order->orderGroup->save();

        // Hide slot by order limit.
        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals(0, $slots->count());

        // Enable slot by order limit.
        $slot->number_orders = 2;
        $slot->save();
        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals(1, $slots->count());
    }

    public function testGetManyDays()
    {
        $totalSlots = 5;
        $city = $this->newCity();
        $store = $this->newStore($city);
        $warehouse = $this->newWarehouse($city);
        $zone = $this->newZone($warehouse);
        foreach (range(1, $totalSlots) as $item) {
            $window = $this->newDeliveryWindow();
            $window->shifts = \DeliveryWindow::SHIFTS[0];
            $window->save();
            $this->newSlotZoneWithWindow($zone, $window);
        }

        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals($totalSlots, $slots->count());
        $this->checkForObjectType($slots);
    }

    public function testHideSlotByException()
    {
        $totalSlots = 5;
        $city = $this->newCity();
        $store = $this->newStore($city);
        $warehouse = $this->newWarehouse($city);
        $zone = $this->newZone($warehouse);
        foreach (range(1, $totalSlots) as $item) {
            $window = $this->newDeliveryWindow();
            $window->shifts = \DeliveryWindow::SHIFTS[0];
            $window->status = 0;
            $window->save();
            $this->newSlotZoneWithWindow($zone, $window);
        }

        $slots = $store->getDeliverySlots($zone, false);
        $this->assertEquals(0, $slots->count());
    }

    public function testHideSlotByCoordinates()
    {
        $this->initFaker();
        $city = $this->newCity();
        $store = $this->newStore($city);
        $warehouse = $this->newWarehouse($city);
        $zone = $this->newZone($warehouse);
        $zone->delivery_zone = '0 0,0 5,5 5,5 0';
        $zone->save();
        $window = $this->newDeliveryWindow();

        $zoneLocation = new \ZoneLocation();
        $zoneLocation->name = $this->faker->name;
        $zoneLocation->delivery_zone = '0 1,0 4,4 4,4 0';
        $zoneLocation->status = 1;
        $zoneLocation->zone()->associate($zone);
        $zoneLocation->save();

        $zoneLocationSlot = new \ZoneLocationSlot();
        $zoneLocationSlot->zoneLocation()->associate($zoneLocation);
        $zoneLocationSlot->deliveryWindow()->associate($window);
        $zoneLocationSlot->status = 1;
        $zoneLocationSlot->day = Carbon::create()->dayOfWeek;
        $zoneLocationSlot->save();

        $this->newSlotZoneWithWindow($zone, $window);
        $slots = $store->getClearDeliverySlots($zone, false, 2, 2);
        $this->assertEquals(0, $slots->count());

        $zoneLocation->delivery_zone = '0 0,0 0,0 0,0 0';
        $zoneLocation->save();

        $slots = $store->getClearDeliverySlots($zone, false, 2, 2);
        $this->assertEquals(1, $slots->count());

        $zoneLocation->delivery_zone = '0 1,0 4,4 4,4 0';
        $zoneLocation->save();
        $zoneLocationSlot->day = Carbon::create()->addDay()->dayOfWeek;
        $zoneLocationSlot->save();

        $slots = $store->getClearDeliverySlots($zone, false, 2, 2);
        $this->assertEquals(1, $slots->count());

        $zoneLocationSlot->day = Carbon::create()->dayOfWeek;
        $zoneLocationSlot->save();

        $slots = $store->getClearDeliverySlots($zone, false, 2, 2);
        $this->assertEquals(0, $slots->count());
    }

    public function testGetOldFormat()
    {
        $days = 6;
        // No puede ser mayor al número de días o se repetiran algunos slots.
        $totalSlots = 7;
        $slotsPerDay = 4;
        $city = $this->newCity();
        $store = $this->newStore($city);
        $warehouse = $this->newWarehouse($city);
        $zone = $this->newZone($warehouse);
        $date = Carbon::create();

        $deliveryWindow = [];
        for ($i = 0; $i < $slotsPerDay; $i++) {
            $deliveryWindow[] = ['0' . ($i * 2 + 1) . ':00:00', '0' . ($i * 2 + 2) . ':00:00'];
        }

        for ($j = 0; $j < $totalSlots; $j++) {
            for ($i = 0; $i < $slotsPerDay; $i++) {
                $window = $this->newDeliveryWindow();
                $window->shifts = \DeliveryWindow::SHIFTS[0];
                $window->hour_start = $deliveryWindow[$i][0];
                $window->hour_end = $deliveryWindow[$i][1];
                $window->save();
                $slotZone = $this->newSlotZoneWithWindow($zone, $window);
                $slotZone->day = $date->dayOfWeek;
                $slotZone->save();
            }

            $date->addDay();
        }

        $slots = $store->getDeliverySlotCheckout($zone, true)[$store->id];
        $this->assertCount($days, $slots['days']);
        $this->assertCount($days, $slots['time']);
        foreach ($slots['time'] as $index => $slot) {
            $this->assertCount($slotsPerDay, $slot, "Error at slot '{$index}'");

            foreach ($deliveryWindow as $elementKey => $window) {
                $this->assertEquals("{$window[0]} - {$window[1]}", $slot[$window[0] . ' - ' . $window[1]]['value']);
            }
        }
    }

    public function testValidateDeliveryToAddressFunction()
    {

    }

    /**
     * @param $collection
     */
    private function checkForObjectType($collection)
    {
        $this->greaterThan(0, $collection->count());
        foreach ($collection as $item) {
            $this->assertInstanceOf(\SlotZone::class, $item);
        }
    }
}