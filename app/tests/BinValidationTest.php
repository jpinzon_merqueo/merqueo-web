<?php


namespace Tests;


class BinValidationTest extends TestCase
{
    public function testValidBin() {
        $validBin = 513689;
        $binValidation = new \BinCodes();
        $this->assertTrue($binValidation->validate($validBin));
    }

    public function testInvalidBinExceptions() {
        $invalidBin = 411111;
        $binValidation = new \BinCodes();
        $this->assertTrue($binValidation->validate($invalidBin));
    }

    public function testInvalidBin(){
        $invalidBin = 411111;
        $binValidation = new \BinCodes();
        $this->assertFalse($binValidation->validate($invalidBin));
    }
}