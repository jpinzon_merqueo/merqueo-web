<?php

namespace Tests;

class CustomerServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    protected function tearDown()
    {
        parent::tearDown();
        DB::rollback();
    }

    public function testManagePermissionsViewRenderedProperly()
    {
        // TODO Deberia existir una constante para los identificadores de los roles;
        $users = Admin::hasCustomerServiceRol()->limit(10)->get();
        $action = action('admin\callcenter\CustomerServiceController@assign_permises_view');
        $this->call('GET', $action);
        $this->assertResponseOk();
        foreach ($users as $user) {
            $this->assertViewHas($user->fullname);
        }
    }

    public function testGivePermissions()
    {
        $user = Admin::hasCustomerServiceRol()->first();
        $service = CustomerService::first();
        $services = [$service->id];
        $action = action('admin\callcenter\CustomerServiceController@assign_services_table_ajax', compact('services'));
        $this->call('POST', $action);
        $this->assertResponseOk();
        $user = Admin::find($user->id);
        $this->assertEquals($service->id, $user->services_list);
    }

    public function testShowUserIndex()
    {
        self::markTestSkipped('Se debe implementar.');
    }

    public function testGiveMultiplePermissions()
    {
        $user = Admin::hasCustomerServiceRol()->first();
        $services = CustomerService::get();
        $services = $services->map(function ($service) {
            return $service->id;
        });
        $action = action('admin\callcenter\CustomerServiceController@assign_services_table_ajax', compact('services'));
        $this->call('POST', $action);
        $this->assertResponseOk();
        $user = Admin::find($user->id);
        $this->assertEquals(implode(',', $services), $user->services_list);
    }

    public function testViewRenderedProperly()
    {
        $action = action('admin\callcenter\CustomerServiceController@view');
        $this->call('GET', $action);
        $this->assertResponseOk();
        // TODO Probar el contenido extra: Botón de asignar permisos e inicar a (recibir pedidos)???
    }
}
