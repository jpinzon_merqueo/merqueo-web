<?php

namespace Tests\Repositories;

use DB;
use Tests\TestCase;
use orders\CancelOrder;
use Tests\Traits\MerqueoFactory;
use Tests\Traits\RejectReasonFactory;

class CancelOrderTest extends TestCase
{
    use RejectReasonFactory, MerqueoFactory;

    const STATUS = 'Cancelado';
    const ATTENDANT = 'Help Center';

    protected $order;
    protected $rejectReasonRepo;

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
        
        $this->order = $this->makeFakeOrder();
    }

    protected function tearDown()
    {
        parent::tearDown();
        DB::rollback();
    }

    /**
     * @dataProvider reasonsData
     */
    public function testConvertRejectReason(
        $reasonOld,
        $status,
        $attendant,
        $reasonNew
    ) {
        $this->makeRejectReason(
            self::STATUS,
            'Modificación de Franja',
            self::ATTENDANT
        );
        $this->makeRejectReason(
            self::STATUS,
            'No estaré',
            self::ATTENDANT
        );
        $this->makeRejectReason(
            self::STATUS,
            'Modificación de Productos',
            self::ATTENDANT
        );

        $cancelOrder = new CancelOrder($this->order);
        $rejectReason = $cancelOrder->convertRejectReason($reasonOld);

        $this->assertEquals($rejectReason->status, $status);
        $this->assertEquals($rejectReason->attendant, $attendant);
        $this->assertEquals($rejectReason->reason, $reasonNew);
    }

    public function reasonsData()
    {
        return array(
            array(
                'Elegí un horario incorrecto',
                self::STATUS,
                self::ATTENDANT,
                'Modificación de Franja'
            ),
            array(
                'No voy a estar en la dirección',
                self::STATUS,
                self::ATTENDANT,
                'No estaré'
            ),
            array(
                'Se me olvidaron algunos productos',
                self::STATUS,
                self::ATTENDANT,
                'Modificación de Productos'
            ),
            array(
                'Texto no considerado',
                null,
                null,
                null
            ),
            array(
                null,
                null,
                null,
                null
            )
        );
    }

    public function testManage()
    {
        $oldRejectReason = $this->makeRejectReason(
            'Cancelado definitivamente',
            'No voy a estar en la dirección',
            null
        ); 
        $newRejectReason = $this->makeRejectReason(
            self::STATUS,
            'No estaré',
            self::ATTENDANT
        );
        $cancelOrder = new CancelOrder($this->order);
        $rejectReason = $cancelOrder->convertRejectReason($oldRejectReason->reason);
        $manage = $cancelOrder->manage($oldRejectReason);

        $this->assertEquals($this->order->reject_reason_id, $rejectReason->id);
        $this->assertEquals($this->order->temporarily_cancelled, 0);
        $this->assertEquals($this->order->scheduled_delivery, 0);
        $this->assertEquals($this->order->reject_comments, 'Cancelado desde el Help Center.');
        $this->assertEquals($this->order->status, 'Cancelled');
    }
}
