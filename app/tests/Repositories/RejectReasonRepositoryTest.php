<?php

namespace Tests\Repositories;

use Tests\TestCase;
use DB;
use RejectReason;
use Tests\Traits\RejectReasonFactory;
use repositories\Eloquent\RejectReasonRepository;

class RejectReasonRepositoryTest extends TestCase
{
    use RejectReasonFactory;

    const STATUS = 'Cancelado';
    const ATTENDANT = 'Help Center';

    protected $rejectReason;

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
        $this->rejectReason = $this->makeRejectReason(
            self::STATUS,
            'Prueba',
            self::ATTENDANT
        );
    }

    protected function tearDown()
    {
        parent::tearDown();
        DB::rollback();
    }

    public function testGetRejectReasonIdUser()
    {
        $rejectReasonRepo = new RejectReasonRepository();
        $result = $rejectReasonRepo->getRejectReasonIdUser(
            self::STATUS,
            'Prueba',
            self::ATTENDANT
        );

        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('attendant', $result);
        $this->assertArrayHasKey('reason', $result);

        $this->assertEquals($result->status, self::STATUS);
        $this->assertEquals($result->attendant, self::ATTENDANT);
        $this->assertEquals($result->reason, 'Prueba');
    }
}
