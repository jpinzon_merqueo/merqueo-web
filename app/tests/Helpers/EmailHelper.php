<?php

namespace Tests\Helpers;

/**
 * Class EmailHelper
 * @package Tests\Helpers
 */
class EmailHelper
{
    /**
     * @var array
     */
    private $mailHistory = [];

    /**
     * @var int
     */
    private $sendMailTries = 0;

    /**
     * @var
     */
    private static $instance;

    /**
     * EmailHelper constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return EmailHelper
     */
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new EmailHelper();
        }

        return self::$instance;
    }

    /**
     *
     */
    public static function clear()
    {
        self::$instance = new EmailHelper();
    }

    /**
     * @param $result
     */
    public function addHistory($result)
    {
        $this->mailHistory[] = $result;
    }

    /**
     *
     */
    public function addTry()
    {
        $this->sendMailTries++;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function getResults()
    {
        if ($this->sendMailTries === 0) {
            throw new \Exception('No hay ningun intento de enviar un mail.');
        }

        if ($this->sendMailTries !== count($this->mailHistory)) {
            throw new \Exception("Se intentaron enviar {$this->sendMailTries} mail y solo fueron enviados " . count($this->mailHistory));
        }

        return true;
    }
}