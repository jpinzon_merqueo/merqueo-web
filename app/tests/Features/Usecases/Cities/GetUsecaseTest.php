<?php

namespace Tests\Features\Usecases\Cities;


use Illuminate\Support\Collection;
use Mockery\Mock;
use repositories\contracts\CityRepositoryInterface;
use repositories\contracts\OrderGroupRepositoryInterface;
use repositories\interfaces\OrderRepositoryInterface;
use Tests\TestCase;
use usecases\cities\GetUsecase;

/**
 * Class GetUsecaseTest
 * @package Tests\Features\Usecases\Cities
 */
class GetUsecaseTest extends TestCase
{
    /**
     * @var Mock
     */
    private $cityRepository;


    public function setUp()
    {
        parent::setUp();
        $this->cityRepository = \Mockery::mock(CityRepositoryInterface::class);
    }


    protected function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }


    /**
     * @test
     */
    public function test_success()
    {
        $usecase = new GetUsecase($this->cityRepository);

        $c1 = new \City();
        $c2 = new \City();
        $collectionCities = new Collection([$c1, $c2]);
        $this->cityRepository->shouldReceive('findAllActives')
            ->once()
            ->withNoArgs()
            ->andReturn($collectionCities);

        $response = $usecase->allActives();

        $this->assertEquals([[],[]], $response);
    }

    /**
     * @test
     */
    public function test_error_in_repository(){
        $usecase = new GetUsecase($this->cityRepository);

        $this->cityRepository->shouldReceive('findAllActives')
            ->once()
            ->withNoArgs()
            ->andThrow(new \Exception('Se perdió comunicación con la base de datos'));

        $this->setExpectedException(\Exception::class, 'Se perdió comunicación con la base de datos');

        $usecase->allActives();
    }

}