<?php


namespace Tests\Features\Usecases\Orders;


use Mockery\Mock;
use repositories\contracts\CityRepositoryInterface;
use repositories\contracts\OrderGroupRepositoryInterface;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\OrderGroupRepository;
use repositories\OrderRepository;
use Tests\TestCase;
use Tests\Traits\MerqueoFactory;
use usecases\Exception\OrderNotFoundException;
use usecases\orders\OrderWhoReceives;

class OrderWhoReceivesTest extends TestCase
{
    use MerqueoFactory;

    /**
     * @var Mock
     */
    private $orderRepository;

    /**
     * @var Mock
     */
    private $orderGroupRepository;

    public function setUp()
    {
        parent::setUp();
        $this->orderRepository = \Mockery::mock(OrderRepositoryInterface::class);
        $this->orderGroupRepository = \Mockery::mock(OrderGroupRepositoryInterface::class);
        \DB::beginTransaction();
    }

    public function test_getWhoReceives_order_not_found()
    {
        $useCase = new OrderWhoReceives($this->orderRepository, $this->orderGroupRepository);

        $this->orderRepository->shouldReceive('findByIdUserAndOrder')
            ->with(1, 2)
            ->once()
            ->andReturn(null);

        $this->setExpectedException(OrderNotFoundException::class, 'Datos Inválidos');

        $result = $useCase->getWhoReceives(1, 2);
    }

    /**
     * @test
     */
    public function test_getWhoReceives_order_not_found_info()
    {
        $useCase = new OrderWhoReceives($this->orderRepository, $this->orderGroupRepository);

        $orderGroup = new \OrderGroup();
        $orderGroup->user_firstname = 'camilo';
        $orderGroup->user_lastname = 'rojas';
        $order = \Mockery::mock(\Order::class);
        $order->shouldReceive('getAttribute')
            ->once()
            ->andReturn($orderGroup);

        $this->orderRepository->shouldReceive('findByIdUserAndOrder')
            ->with(1, 2)
            ->once()
            ->andReturn($order);

        $this->setExpectedException(OrderNotFoundException::class, 'No se encontró Información');

        $result = $useCase->getWhoReceives(1, 2);
    }

    /**
     * @test
     */
    public function test_getWhoReceives_success()
    {
        $useCase = new OrderWhoReceives($this->orderRepository, $this->orderGroupRepository);

        $orderGroup = new \OrderGroup();
        $orderGroup->reception_user_name = 'camilo';
        $orderGroup->reception_user_phone = '66666';
        $orderGroup->user_firstname = 'camilo';
        $orderGroup->user_lastname = 'rojas';
        $order = \Mockery::mock(\Order::class);
        $order->shouldReceive('getAttribute')
            ->once()
            ->andReturn($orderGroup);

        $this->orderRepository->shouldReceive('findByIdUserAndOrder')
            ->with(1, 2)
            ->once()
            ->andReturn($order);

        $result = $useCase->getWhoReceives(1, 2);

        $this->assertEquals($result, [
            "order_id" => (string)1,
            'is_user_owner' => ('camilo rojas' == 'camilo') ? true : false,
            'name_user_receives' => 'camilo',
            'phone_user_receives' => '66666'
        ]);
    }

    /**
     * @test
     */
    public function UpdateWhoReceivesNew () {
        $useCase = new OrderWhoReceives(new OrderRepository(), new OrderGroupRepository());
        $order = $this->makeFakeOrder();
        $rep = $useCase->updateWhoReceives($order->id, 'andres rojas', '312121212');
        $this->assertTrue($rep);
    }
    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

}