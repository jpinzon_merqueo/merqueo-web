<?php

namespace Tests;

use repositories\contracts\DiscountRepositoryInterface;
use usecases\Checkout\DeliveryDiscount\GlobalUseCase;

/**
 * Class EloquentDiscountRepositoryTest
 * @package Tests
 * @property-read \Mockery|\Discount
 */
class GlobalDiscountDeliveryUsecaseTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface|DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var int
     */
    private $cityId;

    /**
     * @var int
     */
    private $storeId;

    /**
     * @var int
     */
    private $deliveryAmount;

    /**
     * @var int
     */
    private $lessSubtotal;

    /**
     * @var int
     */
    private $greaterSubtotal;

    public function setUp()
    {
        parent::setUp();
        $this->discountRepository = \Mockery::mock(DiscountRepositoryInterface::class);
        $this->cityId  = 1;
        $this->storeId = 63;
        $this->deliveryAmount = 9500;
        $this->lessSubtotal = 50000;
        $this->greaterSubtotal = 400000;
    }


    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    /**
     * Test when subtotal is less return delivery amount
     */
    public function testSubtotalIsLess()
    {
        $usecase = new GlobalUseCase($this->discountRepository);

        $this->discountRepository->shouldReceive('findByDateCitySubtotal')->andReturn(null);
        $this->discountRepository->shouldReceive('findByDateCityStoreSubtotal')->andReturn(null);

        $usecase->setCityId($this->cityId);
        $usecase->setStoreId($this->storeId);
        $usecase->setCartSubtotal($this->deliveryAmount);

        $this->assertFalse($usecase->handle());
    }

    /**
     * Test when subtotal is greater and contains city return delivery amount
     */
    public function testSubtotalIsGreater()
    {
        $usecase = new GlobalUseCase($this->discountRepository);

        $count = \Mockery::mock(\Discount::class);

        $count->shouldReceive('count')->andReturn(1);

        $this->discountRepository->shouldReceive('findByDateCitySubtotal')->andReturn($count);

        $usecase->setCityId($this->cityId);
        $usecase->setStoreId($this->storeId);
        $usecase->setCartSubtotal($this->deliveryAmount);

        $this->assertTrue($usecase->handle());
    }

    /**
     * Test when contain city and store
     */
    public function testWhenIsStore()
    {
        $usecase = new GlobalUseCase($this->discountRepository);

        $this->discountRepository->shouldReceive('findByDateCitySubtotal')->andReturn(null);

        $count = \Mockery::mock(\Discount::class);

        $count->shouldReceive('count')->andReturn(1);

        $this->discountRepository->shouldReceive('findByDateCityStoreSubtotal')->andReturn($count);

        $usecase->setCityId($this->cityId);
        $usecase->setStoreId($this->storeId);
        $usecase->setCartSubtotal($this->deliveryAmount);

        $this->assertTrue($usecase->handle());
    }
}
