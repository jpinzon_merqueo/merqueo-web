<?php


namespace Tests\Features\app\OrderGroup;

use http\Env;
use Mockery\Mock;
use Tests\TestCase;
use Tests\Traits\MerqueoFactory;

class WhoReceivesTest extends TestCase
{
    use MerqueoFactory;

    public function setUp()
    {
        parent::setUp();
        \DB::beginTransaction();
    }

    /**
     * @test
     */
    public function createOrderWhoRweceives()
    {
        $order = $this->makeFakeOrder();
        $orderGropu = $order->orderGroup;
        $ownerNameOrder = sprintf('%s %s', $orderGropu->user_firstname, $orderGropu->user_lastname);
        $ownerNameUser = sprintf('%s %s', $order->user->first_name, $order->user->last_name);
        $this->assertEquals($ownerNameOrder, $orderGropu->reception_user_name);
        $this->assertEquals($ownerNameUser, $orderGropu->reception_user_name);
        $this->assertEquals($order->user->phone, $orderGropu->reception_user_phone);
        $this->assertEquals($orderGropu->user_phone, $orderGropu->reception_user_phone);
    }

    protected function tearDown()
    {
        parent::tearDown();
        \DB::rollback();
    }

}