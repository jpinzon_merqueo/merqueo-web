<?php

namespace Tests\Feature\app\Classes\Entities;

use DB;
use Tests\TestCase;
use app\classes\entities\OrderPaymentLogEntity;
use Faker\Factory as Faker;

/**
 * Class OrderPaymentLogEntityTest
 *
 * @package Tests\Feature\app\Classes\Entities
 */
class OrderPaymentLogEntityTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown()
    {
        parent::setUp();
        DB::rollback();
    }

    /**
     * @param $getInstanceEmpty
     * @return mixed
     */
    public function setValuesInstance($getInstanceEmpty = false)
    {
        $faker = Faker::create();
        $orderPaymentLogEntity = new OrderPaymentLogEntity();

        if ($getInstanceEmpty) {
            return $orderPaymentLogEntity;
        }

        $orderPaymentLogEntity->setOrderId($faker->randomNumber());
        $orderPaymentLogEntity->setUserId($faker->randomNumber());
        $orderPaymentLogEntity->setMethod(__METHOD__);
        $orderPaymentLogEntity->setUrl($faker->imageUrl());
        $orderPaymentLogEntity->setResponseHttpCode(200);
        $orderPaymentLogEntity->setRequest($faker->word());
        $orderPaymentLogEntity->setResponse($faker->word());
        $orderPaymentLogEntity->setIp($faker->word());
        $orderPaymentLogEntity->setTransactionId($faker->randomNumber());
        $orderPaymentLogEntity->setCreatedAt(date('Y-m-d H:m:i'));
        $orderPaymentLogEntity->setUpdatedAt(date('Y-m-d H:m:i'));

        return $orderPaymentLogEntity;
    }

    /**
     * Casos de prueba
     *
     * @return array
     */
    public function valueCases()
    {
        return [
            [$this->setValuesInstance(true), 0],
            [$this->setValuesInstance(), 1],
        ];
    }

    /**
     * Prueba el guardado de los datos desde el comanado
     *
     * @param OrderPaymentLogEntity $orderPaymentLogEntityInstance
     * @param $output
     * @dataProvider valueCases
     */
    public function testCommandSaveOrderPaymentLog($orderPaymentLogEntityInstance, $output)
    {
        $response = \Artisan::call("command:SaveOrderPaymentLog", ['data' => $orderPaymentLogEntityInstance->data]);

        $this->assertEquals($output, $orderPaymentLogEntityInstance->saveOrderPaymentLog());
        $this->assertEquals($output, $response);
    }
}