<?php
/**
 * Created by PhpStorm.
 * User: merqueo
 * Date: 14/02/19
 * Time: 04:28 PM
 */

namespace Tests;

use Illuminate\Database\QueryException;

class LockRowTest extends TestCase
{
    public function testSelectReception()
    {
        try {
            \DB::beginTransaction();
            $e = new \Exception('test', 40001);
            throw new QueryException("Select * from store_products where id = ?", ['test'], $e);
            $reception = \ProviderOrderReception::lockForUpdate()->find(22);
            $reception->date = now();
            \DB::commit();
        } catch (QueryException $e) {
            if ($e->getCode() == 'HY000' || $e->getCode() == '40001') {
                dd($e);
            }
            $this->assertFalse(true);
        } catch (\Exception $e) {
            \DB::rollBack();
            $this->assertFalse(true);
        }
        $this->assertTrue(!empty($reception));
    }
}
