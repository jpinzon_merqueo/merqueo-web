@extends('layout')

@section('title')Merqueo - El supermercado del ahorro. Entrega a domicilio.@stop
@section('description')El supermercado del ahorro. Entrega a domicilio en {{ $store->city_name }}, puedes pagar con tarjeta o en efectivo contra entrega. Todo sin moverte de casa.@stop

@section('content')
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "{{ $store->name }}",
  "logo": "{{ $store->logo_url }}",
  "url": "{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}",
  "contactPoint" : [{
        "@type" : "ContactPoint",
        "telephone" : "{{ $store->phone }}",
        "contactType" : "Teléfono Domicilios"
      }]
  }
}
</script>

@if(strpos( Route::getCurrentRoute()->getPath(), 'checkout' ) === false)
  <div class="row" style="margin-top: 130px;">
    <div class="col-xs-12">
      <div class="logtexttienda" style="padding: 0 3rem;">
        {{ View::make('common.banners', compact('banners', 'has_orders', 'variation_key') + ['store' => $data['store']]) }}
      </div>
    </div>
  </div>
@endif

<div class="col-md-12 content_list_data">
  <div class="col-md-12">
    <div class="col-md-12">
      <div class="text_hora">
        <h1><strong>{{ $store->name }}</strong> con Entrega Mañana</h1>
      </div>
    </div>
    <div class="mdl-cell mdl-cell--3-col"></div>
  </div>

  <ol class="breadcrumb" data-catrel="{{ $data['store']['id'] }}" itemscope itemtype="http://schema.org/BreadcrumbList">
    {{ Breadcrumb::render('>', $data['store']['name'], 2, $store->city_name) }}
  </ol>

  @if (isset($data['store']['user_products']))
  <div class="row clearfix pro-row">
    <div class="col-md-12 product-page-title">
        <h2>
            <a class="head"
               href="{{ action('StoreController@bought_products', [$data['store']['city_slug'], $data['store']['slug']]) }}">
                Últimos Pedidos
            </a>
        </h2>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 all_departments">
        {{ View::make('elements.products-list', ['products' => $data['store']['user_products'], 'store' => $data['store'], 'referrer' => 'recent', 'variation_key' => $variation_key]) }}
        <div class="product-column-link column text-center">
          <div class="product-list">
              <a href="{{ action('StoreController@bought_products', [$data['store']['city_slug'], $data['store']['slug']]) }}">
                  <div class="col-md-12">
                      <div class="contenedor_items mq-new-contenedor-items">
                          <i class="fa fa-arrow-right" aria-hidden="true"></i>
                          <p>Ver más</p>
                          <br><span>Últimos pedidos</span>
                      </div>
                  </div>
              </a>
          </div>
        </div>
    </div>
  </div>
  @endif

  @if ((isset($data['store']['user_products']) && count($data['store']['user_products']) < 40)  || !isset($data['store']['user_products']))

  @foreach ($data['store']['departments'] as $department)
  @if ($department->products->count())
  <div class="row clearfix pro-row">
    <div class="col-md-12 product-page-title">
        <h2>
        <a class="head" href="{{ route('frontStoreDepartment.single_department', ['city_slug' => $data['store']['city_slug'], 'store_slug' => $data['store']['slug'], 'department_slug' => $department->slug]) }}">
          {{ $department->name }}
        </a>
        <span>
          <a class="mq-see-more-link" href="{{ route('frontStoreDepartment.single_department', ['city_slug' => $data['store']['city_slug'], 'store_slug' => $data['store']['slug'], 'department_slug' => $department['slug']]) }}">
            VER MÁS <i class="icon-chevron-right"></i>
          </a>
        </span>
      </h2>
      @if ($department->description != '')
      <div class="category-msg">
        <p>{{ $department->description }}</p>
      </div>
      @endif
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
      {{ View::make('elements.products-list', ['products' => $department->products, 'store' => $data['store'], 'variation_key' => null]) }}
      <div class="product-column-link column text-center">
        <div class="product-list">
          <a href="{{ web_url() }}/{{ $data['store']['city_slug'] }}/domicilios-{{ $data['store']['slug'] }}/{{ $department->slug }}">
            <div class="col-md-12">
              <div class="contenedor_items mq-new-contenedor-items">
                  <i class="fa fa-arrow-right" aria-hidden="true"></i>
                  <p>Ver más</p>
                  <br><span>{{ ucfirst(strtolower($department->name)) }} </span>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  @endif
  @endforeach
  @endif
  <div class="col-md-12 col-sm-12 col-xs-12 seo-text">
      {{ $store->description }}
  </div>
</div>
@stop
