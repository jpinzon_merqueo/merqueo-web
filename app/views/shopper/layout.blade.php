<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Speed Basket | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{ web_url() }}/admin_asset/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{ web_url() }}/admin_asset/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href="{{ web_url() }}/admin_asset/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        
        <link href="{{ web_url() }}/admin_asset/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="{{ web_url() }}/admin_asset/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <link href="{{ web_url() }}/admin_asset/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>
                <!-- date-range-picker -->
        <script src="{{ web_url() }}/admin_asset/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="{{ web_url() }}/admin_asset/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="{{ web_url() }}/admin_asset/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/orders" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Speed Basket
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ (($store_admin) ? Session::get('store_admin_name') : Session::get('shopper_name')) }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{ web_url() }}/admin_asset/img/avatar2.png" class="img-circle" alt="User Image" />
                                    <p>
                                        {{ Session::get('admin_name') }}  
                                        <small>{{ Session::get('admin_designation') }}</small>
                                    </p>
                                </li>
                                <!-- 
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>
                                -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/profile" class="btn btn-default btn-flat">Change Password</a>
                                    </div> 
                                    <div class="pull-right">
                                        <a href="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/signout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ web_url() }}/admin_asset/img/avatar2.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <span>{{ (($store_admin) ? Session::get('store_admin_name') : Session::get('shopper_name')) }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- 
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/realtime">
                                <i class="fa fa-shopping-cart"></i> 
                                <span>Live Orders</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/orders">
                                <i class="fa fa-shopping-cart"></i> <span>Orders</span>
                            </a>
                        </li>                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                @yield('content')
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <!-- Bootstrap -->
        <script src="{{ web_url() }}/admin_asset/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->

        <script src="{{ web_url() }}/admin_asset/js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- page script -->
        

    </body>
</html>