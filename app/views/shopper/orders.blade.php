@extends('shopper.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
@if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif

    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="coupons-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Total Products</th>
                                <th>Amount</th>
                                <th>Shopper</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->first_name }} {{ $order->last_name }}</td>
                                <td>{{ $order->email }}</td>
                                <td>{{ $order->phone }}</td>
                                <td>{{ $order->total_products }}</td>
                                <td>{{ $order->total_amount }}</td>
                                <td>
                                @if($order->shopper_id != 0)
                                <span class="badge bg-green">Allocated</span>                              
                                @else
                                <span class="badge bg-red">Unallocated</span>
                                @endif
                                </td>
                                <td>
                                @if($order->status == 'Delivered')
                                <span class="badge bg-green">Delivered</span>                              
                                @elseif($order->status == 'Cancelled')
                                <span class="badge bg-red">Cancelled</span>
                                @elseif($order->status == 'Initiated')
                                <span class="badge bg-yellow">Initiated</span>
                                @else
                                <span class="badge bg-green">In Progress</span>
                                @endif
                                </td>
                                <td>{{ date("d M Y",strtotime($order->created_at)) }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/order/{{ $order->id }}/details">View Details</a></li>
                                            @if($order->shopper_id == 0)
                                            <li class="divider"></li>
                                            <li><a href="#" onclick="allocate({{ $order->id }})">Allocate Shopper</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<!-- Allocate Shopper Modal -->
<div class="modal fade" id="shopper-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Allocate Shopper</h4>
      </div>
        <form method="post" action="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/order/updateshopper">
      <div class="modal-body">
          <input type="hidden" id="order-id" name="id" value=""> 
          <div class="form-group">
            <label for="recipient-name" class="control-label">Shopper</label>
            <select class="form-control" name="shopper_id">
                @foreach($shoppers as $shopper)
                <option value="{{ $shopper->id }}">{{ $shopper->first_name }}</option>
                @endforeach
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>

<script type="text/javascript">
    $(function() { 
        // DataTable
        var table = $('#coupons-table').DataTable();
    });

    function allocate(order_id){
        $('#order-id').val(order_id);
        $('#shopper-modal').modal('show');
    }
</script>
@stop