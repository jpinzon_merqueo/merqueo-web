
<h5>
    PARA CONTINUAR LO INVITAMOS A LEER DE MANERA ATENTA Y COMPLETA LOS PRESENTES TÉRMINOS Y CONDICIONES, ANTES DE ACCEDER A NUESTRA PLATAFORMA.
</h5>

<div class="container-scroll">
<p align="center">
    <strong><u>TÉRMINOS Y CONDICIONES</u></strong>
</p>
<p>
    <strong>1. </strong>
    <strong><u>Objeto.</u></strong>
</p>
<p>
Usted (en adelante el "<strong><u>Shopper</u></strong>"), como aceptante de estos términos y condiciones (los "<strong><u>Términos y Condiciones</u></strong>") y al haber completado el proceso operativo de registro como Usuario, en su calidad de comprador personal    <em>(personal shopper)</em>(la "<strong><u>Habilitación</u></strong>"), dentro las plataformas (las "<strong><u>Plataformas</u></strong>") de propiedad, o
    que sean controladas por la sociedad colombiana Merqueo S.A.S. (en adelante "<strong><u>Merqueo</u></strong>") o cualquier sociedad colombiana o extranjera
    afiliada a Merqueo por vínculos de subordinación o control (las "<strong><u>Sociedades Vinculadas</u></strong>"), se encuentra autorizado para el uso de
    las Plataformas, accediendo a las facilidades tecnológicas para que por su propia cuenta y riesgo, y de manera directa, le preste a los Usuarios de las
Plataformas, que tengan la calidad de clientes (el "<strong><u>Cliente</u></strong>"), el servicio de selección, transporte y entrega (los "<strong><u>Servicios</u></strong>") de pedidos de productos adquiridos en los establecimientos de comercio disponibles en las Plataformas (los "    <strong><u>Comercios</u></strong>").
</p>
<p>
    Por los Servicios facilitados a través de las Plataformas, Merqueo garantizará que los Clientes pagarán un valor adicional por los productos adquiridos
    que, incluirá la remuneración por los Servicios prestados por los Shoppers de los Clientes (la "<strong><u>Tarifa</u></strong>") y, que, a través de las
    Plataformas, Merqueo facilitará y entregará a los Shoppers en los términos informados a los mismos. Con la aceptación de estos Términos y Condiciones,
    usted reconoce, otorgando su irrevocable y expresa instrucción y autorización, para que dicha Tarifa sea definida y facilitada exclusivamente por Merqueo,
    pudiendo variar según los horarios de demanda en las Plataformas, en beneficio de los Shopper. La Tarifa será informada al Shopper al haber obtenido la
    Habilitación, indicándose la fecha de facilitación y entrega de las Tarifas generadas en los períodos definidos por Merqueo.
</p>
<p>
    <strong>2. </strong>
    <strong><u>Funcionamiento de las Plataformas.</u></strong>
</p>
<p>
    <strong><u> </u></strong>
</p>
<p>
    Al acceder a las Plataformas, reconoce y acepta que las mismas de manera automática operarán con el propósito de facilitar la conexión y atención de
    pedidos por parte de Clientes de productos de los Comercios (los "<strong><u>Pedidos</u></strong>"), que permitirán al Shopper que se encuentre “Activo”,
    “En Línea” o “Disponible” en las Plataformas, aceptar los Pedidos asignados automáticamente por las Plataformas, para efectos de prestar los Servicios. Es
    importante que recuerde que usted es libre de usar o no las Plataformas con el fin de brindarle Servicios a los Clientes y beneficiarse económicamente,
    siendo de su entera responsabilidad todas las reclamaciones derivadas de la forma de atender y prestar los mismos a los Clientes. Con su ingreso voluntario
    a las Plataformas, usted reconoce que tiene un deber con los clientes para atender los Pedidos aceptados, asumiendo frente a Merqueo y los Usuarios, las
    consecuencias y sanciones a que haya lugar.
</p>
<p>
    <strong><u> </u></strong>
</p>
<p>
    <strong>3. </strong>
    <strong><u>Deberes de los Shoppers en el uso de las Plataformas.</u></strong>
</p>
<p>
    Con el propósito que las Plataformas operen correctamente, y las relaciones entre los Usuarios de la misma se desenvuelvan normalmente, en beneficio de
    todas las partes, usted como Shopper, durante la ejecución y prestación del Servicio al Cliente, se compromete de manera libre, voluntaria y con pleno
    conocimiento de las consecuencias jurídicas, a cumplir los siguientes deberes:
</p>
<p>
    3.1. Utilizar las Plataformas de manera exclusiva para los fines previstos en estos Términos y Condiciones, en especial para la prestación del servicio a
    los Clientes, respetando en todo momento los derechos de propiedad intelectual de Merqueo.
</p>
<p>
    3.2. Custodiar en debida forma y con las medidas de seguridad necesarias sus datos e información que permiten el acceso a las Plataformas, en especial, sus
    datos de usuario y contraseña.
</p>
<p>
    3.3. Garantizar la propiedad o la posesión legítima sobre el dispositivo móvil que le permite usar y acceder a las Plataforma, con los planes de voz y
    datos debidamente activados que permitan el correcto funcionamiento y acceso y descargar o acceder a los servicios y aplicaciones de la misma.
</p>
<p>
    3.4. Dado que el uso de las Plataformas está autorizado de manera personal a cada Shopper, usted no permitirá que ningún tercero o persona diferente a
    usted, acceda a las Plataformas o las utilice para cualquier propósito, siendo de su entera y exclusiva responsabilidad todo efecto o consecuencia
    jurídica, incluyendo de contenido económico, que se presente por permitir un uso no autorizado.
</p>
<p>
    3.5. Notificar de manera inmediata a Merqueo cualquier situación extraña, anormal o de otra naturaleza que pueda afectar el funcionamiento de las
    Plataformas, o que restrinjan o limiten el acceso o uso de la misma por parte de usted, o que puedan afectar el desarrollo y prestación de los servicios al
    Cliente.
</p>
<p>
    3.6. Prestar de manera directa, y por su cuenta y riesgo, el servicio al Cliente facilitado a través de las Plataformas, siendo su obligación y deber, como
    proveedor independiente de servicios al Cliente, el cumplir en todo momento con la legislación colombiana que le sea aplicable, y en especial:
</p>
<p>
    3.6.1. Mantener y contar, durante todo el tiempo de uso de las Plataformas y hasta tanto no sea deshabilitada o cancelada su calidad de Usuario de las
    mismas, con una licencia de conducción de motos y/o vehículos en el territorio de la República de Colombia, encontrándose al día en todas sus obligaciones,
    de cualquier naturaleza o índole, frente a las autoridades de tránsito colombianas.
</p>
<p>
    3.6.2. Encontrarse afiliado de manera voluntaria u obligatoria, según le sea aplicable, a: (a) una ARL debidamente constituida y autorizada para operar en
    Colombia, en su calidad de “Independiente” y en el Nivel 4, o más alto permitido por dicha entidad, (b) una EPS, mediante la cotización y los pagos
    exigidos por la legislación colombiana aplicable, y (c) un fondo de pensiones y cesantías, mediante la cotización y los pagos exigidos por la legislación
    colombiana aplicable.
</p>
<p>
    3.6.3. Mantener al día todos los permisos, registros, seguros y demás garantías o requisitos de los vehículos que destine para los servicios prestados al
    Cliente, declarando que cuenta con el título de la propiedad de los mismos o con la posesión legítima bajo un contrato de leasing, arrendamiento, comodato
    o de otro tipo similar.
</p>
<p>
    3.7. El Contratista declara que al encontrarse en las Plataformas “Activo”, “En Línea” o “Disponible” deberá atender todos los pedidos que le sean
    automáticamente asignados.
</p>
<p>
    3.8. Verificar las características físicas de hermeticidad del empaque de los productos de los Comercios (apariencia, olor, estado, condición y todas las
    que sean necesarias), para determinar las condiciones de los mismos.
</p>
<p>
    3.9. Asistir a las capacitaciones adelantadas por Merqueo respecto del uso de las Plataformas y la prestación de los Servicios.
</p>
<p>
    3.10. Guardar la reserva y confidencialidad de toda la información a la que tenga acceso con ocasión del uso de las Plataformas, y en especial, se
    abstendrá de copiar, reproducir, divulgar o almacenar información de los usuarios de las Plataformas.
</p>
<p>
    3.11. Leer íntegramente y conocer el Manual del uso correcto de la aplicación y servicio al cliente (el "<strong><u>Manual</u></strong>"), el cual contiene
    los protocolos y estándares que facilitarán al Shopper la correcta prestación de los Servicios a los Clientes y que le es puesto a su conocimiento en las
    oficinas de Merqueo al momento de adelantar el proceso de registro en las Plataformas y recibir la capacitación de uso de las mismas.
</p>
<p>
    3.12. Responder por las quejas o reclamos que le lleguen a Merqueo por parte de Clientes derivadas de incumplimiento de estos Términos y Condiciones por
    parte del Shopper, salvo que las mismas estén justificadas en situaciones fuerza mayor o caso fortuito debidamente comprobado.
</p>
<p>
    El incumplimiento por parte del Shopper de cualquiera de los deberes anteriormente descritos, facultará a Merqueo para adoptar las medidas establecidas en
    el Manual.
</p>
<p>
    <strong>4. </strong>
    <strong><u>Facultades de Merqueo en el uso de las Plataformas</u></strong>
</p>
<p>
    Con el fin de garantizar el correcto uso de las Plataformas, establecer reglas de comunicación entre los Usuarios de las mismas y colaborarle a los Shopper
    en la prestación de los Servicios a los Clientes, previniendo inconvenientes entre los mismos, Merqueo estará facultado para:
</p>
<p>
    <strong> </strong>
</p>
<p>
    4.1. Definir y facilitar por cuenta de los Shopper, la Tarifa.
</p>
<p>
    <strong> </strong>
</p>
<p>
    4.2. Aplicar los procedimientos definidos en el Manual y en los presentes Términos y Condiciones.
</p>
<p>
    4.3. Realizar las retenciones a que haya lugar frente a las Tarifas de los Shopper, previstos en el Manual y estos Términos y Condiciones, las cuales son
    expresamente autorizadas por los Shopper con la aceptación de estos Términos y Condiciones.
</p>
<p>
    4.4. Revocar la Habilitación por incumplimiento de los Shopper a sus deberes y obligaciones bajo estos Términos y Condiciones.
</p>
<p>
    4.5. Modificar los Términos y Condiciones de manera unilateral sin previo aviso, salvo por aquellas modificaciones o cambios que puedan afectar la
    prestación del Servicio por parte de los Shopper, los cuales serán informados por Merqueo con diez (10) días calendario de anticipación a la fecha de
    implementación de los cambios en este documento y en las Plataformas.
</p>
<p>
    <strong> </strong>
</p>
<p>
    <strong>5. </strong>
    <strong><u>Confidencialidad.</u></strong>
</p>
<p>
    <strong><u> </u></strong>
</p>
<p>
    5.1. Considerando que Merqueo posee información valiosa y confidencial que debe mantener en secreto, y el Shopper podrá acceder a la misma con el uso de
    las Plataformas, el Shopper declara conocer y aceptar los siguientes términos, condiciones y obligaciones bajo los cuales se deberá manejar la Información
    Confidencial a la que tengan acceso con ocasión de la aceptación de estos Términos y Condiciones, y la prestación de los Servicios. Obligaciones de
    confidencialidad que en todo caso no excluyen ni afectan otras obligaciones especiales de este carácter, contenidas en otros apartes del presente
    documento. "<strong><u>Información Confidencial</u></strong>" incluye sin limitación alguna, todos los datos, datos personales, listas de clientes,
    políticas, contratos, procesos, métodos, fórmulas, know-how, secretos empresariales, ideas, creaciones, diseños, creaciones técnicas o artísticas, y
    cualquier otra información sujeta a reserva, que haya sido accesible al Shopper por cualquier medio. Así las cosas, el Shopper se obliga especialmente a:
    (i) mantener la Información Confidencial en secreto y no la divulgarán a terceros, utilizándola únicamente con el propósito de desempeñar las funciones
    propias del objeto pactado en este documento, (ii) la Información Confidencial no podrá ser copiada, reproducida, distribuida, tratada, transferida o
    transmitida por ningún medio, en todo o en parte, sin la aprobación previa y por escrito manifestada por Merqueo como única propietaria de la información,
    (iii) que una vez haya empleado la Información Confidencial, solamente en el caso de ser estrictamente necesario, dejará en manos de Merqueo todos los
    documentos y medios, ya sean originales o copias que hubieren tomado, en los cuales esté contenida la Información Confidencial, y (iv) se compromete a
    destruir o devolver, a la Fecha de Terminación o en cualquier momento que lo solicite Merqueo, cualquier documento o medio que contenga Información
    Confidencial y a enviar constancia escrita de la destrucción, de ser el caso. El Shopper reconoce que no goza de ningún derecho o licencia sobre el uso de
    la Información Confidencial que siendo exclusiva de Merqueo, haya sido entregada para la materialización de los fines descritos en este documento y que por
    un período de diez (10) años a partir de la Terminación (según se define en la Sección 8), no podrá divulgar la Información Confidencial utilizada en la
    ejecución de los Servicios, salvo las excepciones previstas en la normatividad vigente, y en especial, cuando la información tenga el carácter de
    información pública.
</p>
<p>
    5.2. <u>No transferencia de tecnología.</u> Acuerdan Merqueo y el Shopper que los presentes Términos y Condiciones no tiene como efecto una transferencia
    de tecnología entre ellas ni a terceros, sino solamente una licencia de uso de las Plataformas a favor del Shopper, para que, en su calidad de Usuario,
    utilice las Plataformas para los fines aquí descritos.
</p>
<p>
    <strong>6. </strong>
    <strong><u>Derechos de propiedad intelectual y propiedad industrial. </u></strong>
</p>
<p>
    6.1. Merqueo y el Shopper reconocen que en virtud del presente documento no se transmite ningún derecho y no se otorga ningún tipo de licencia, respecto de
    los derechos de autor de los cuales Merqueo es titular, salvo la licencia de uso de las Plataformas. Así mismo, acuerdan que la titularidad de los derechos
    de autor que se generen en publicidad o en cumplimiento de este documento, corresponderá siempre a Merqueo.
</p>
<p>
    6.2. Como consecuencia del presente documento no se transfiere tampoco ninguna propiedad industrial de Merqueo. El uso de las marcas y otros signos
    distintivos de propiedad de Merqueo se limitará a lo estrictamente necesario para la ejecución del presente documento. El Shopper empleará la mayor
    diligencia y cuidado en el uso o manipulación de las marcas y signos distintivos ajenos, cuando haya lugar a ello; comprometiéndose a utilizar los signos
    distintivos exactamente iguales como le fueren suministrados por sus titulares y a evitar cualquier uso que pueda ocasionar daños en la imagen de dichos
    signos.
</p>
<p>
    <strong>7. </strong>
    <strong><u>Indemnidad.</u></strong>
</p>
<p>
    Usted mantendrá indemne a Merqueo frente a cualquier proceso, acción o reclamación que se intente en su contra derivados o con ocasión de los
    incumplimientos de estos Términos y Condiciones, de la violación de las condiciones de uso de las Plataformas y la prestación de los Servicios.
</p>
<p>
    <strong><u> </u></strong>
</p>
<p>
    <strong>8. </strong>
    <strong><u>Vigencia y finalización de la Habilitación</u></strong>
</p>
<p>
    <strong> </strong>
</p>
<p>
    8.1. La Habilitación se otorga por parte de Merqueo de manera indefinida a favor de los Shopper. No obstante, tanto Merqueo como el Shopper estarán
    facultados para cancelarla en cualquier momento, mediante notificación por escrito, la cual podrá realizarse a través de las Plataformas, implicando esta
    cancelación la revocatoria de la Habilitación otorgada al Shopper desde la fecha de la notificación aquí prevista y constituyendo una terminación del uso
    de las Plataformas y la restricción para acceder a las mismas (la "<strong><u>Terminación</u></strong>"). Dicha cancelación no conllevará el pago o
    reconocimiento de indemnización alguna a favor de Merqueo ni del Shopper, no obstante, con posterioridad a la Terminación, Merqueo procederá a liquidar,
    compensar y facilitar al Shopper las sumas generadas hasta la Terminación.
</p>
<p>
    8.2. En todo caso, Merqueo se encuentra facultado a revocar la Habilitación en cualquier momento, desactivando al Shopper de las Plataformas, si se llegaré
    a presentar algún incumplimiento de los deberes y obligaciones del Shopper en los términos de este documento.
</p>
<p>
    <strong>9. </strong>
    <strong><u>Declaraciones.</u></strong>
</p>
<p>
    <strong> </strong>
</p>
<p>
    Usted declara que el origen de los recursos utilizados para la ejecución y prestación de los Servicios, y el cumplimiento de estos Términos y Condiciones,
    no provienen de ninguna actividad ilícita de las contempladas en el código penal colombiano. Cualquier inexactitud respecto a la información consignada,
    exime a la contraparte de toda responsabilidad derivada por tal situación, facultando a la parte afectada y cumplida a dar por terminada la relación
    comercial y denunciar dichas inexactitudes a la autoridad competente. Con la aceptación de estos Términos y Condiciones, autoriza expresa y voluntariamente
    a Merqueo o a quien represente sus derechos u ostenten en el futuro a cualquier titulo la calidad de acreedor, para consultar, verificar, reportar, y
    transmitir su información comercial personal, económica y financiera y en especial, sobre su comportamiento comercial en las centrales de riesgo, previa
    notificación de tal situación con al menos veinte (20) días de antelación a la fecha del reporte efectivo.
</p>
<p>
    <strong> </strong>
</p>
<p>
    <strong>10. </strong>
    <strong><u>Independencia.</u></strong>
</p>
<p>
    El Shopper reconoce que los presentes términos y condiciones regulan el acceso y uso libre de las Plataformas, y en ningún caso existirá entre Merqueo y el
    Shopper relación laboral alguna, rigiéndose en todos sus aspectos por la ley mercantil aplicable. En ese sentido, el Shopper no tiene autorización expresa
    ni implícita para representar a Merqueo en acuerdos, garantías, o cualquier otra obligación a nombre de Merqueo salvo por el uso autorizado de las
    Plataformas bajo la Habilitación, y se encuentra obligada a sufragar y asumir íntegramente y por su propia cuenta y riesgo todos los gastos y erogaciones
    exigidas bajo la ley colombiana para las personas independientes. Para tales propósitos, y como requisito para la entrega de cualquiera suma de dinero
    derivada de estos Términos y Condiciones, el Shopper deberá remitir los comprobantes válidos y documentación solicitada por Merqueo, donde se acredite el
    cumplimiento por parte del Shopper de los diferentes aspectos y sus obligaciones bajo la normatividad colombiana. La ausencia de estos documentos facultará
    a Merqueo para detener a su entera discreción cualquier entrega bajo este documento.
</p>
<p>
    <strong>11. </strong>
    <strong><u>Ley aplicable.</u></strong>
</p>
<p>
    <strong><u> </u></strong>
</p>
<p>
    Este documento será gobernado e interpretado de acuerdo con las leyes de la República de Colombia, sin dar efecto a cualquier principio de conflictos de
    ley. Si alguna disposición de estos Términos y Condiciones es declarada ilegal, o presenta un vacío, o por cualquier razón resulta inaplicable, la misma
    deberá ser interpretada dentro del marco del mismo y en cualquier caso no afectará la validez y la aplicabilidad de las provisiones restantes.
</p>
<p>
    <strong>12. </strong>
    <strong><u>Comunicaciones.</u></strong>
</p>
<p>
    <strong><u> </u></strong>
</p>
<p>
    Estos Términos y Condiciones son personales, y no se pueden ceder, transferir ni sub-licenciar, excepto con el consentimiento previo por escrito de
    Merqueo. Merqueo podrá ceder, transferir o delegar cualquiera de sus derechos y obligaciones en virtud de estos Términos y Condiciones sin el
    consentimiento del Shopper. A menos que se especifique lo contrario en estos Términos y Condiciones, todos los avisos, modificaciones o notificaciones
    serán considerados debidamente entregados desde el momento de su publicación en las Plataformas, o bien desde el momento en que sea notificado al Shopper a
    su dirección de correo electrónico o teléfono móvil que fue incluida en durante el proceso de registro a las Plataformas, según corresponda.
</p>
<p>
    La aceptación de estos Términos y Condiciones, constituye a su vez la aceptación y conocimiento de la Política de Privacidad de Merqueo, publicada en la
    página web de Merqueo.
</p>

</div>
<br>
<input class="btn btn-success btn-block btn-terms" type="button" value="ACEPTO">
<br>