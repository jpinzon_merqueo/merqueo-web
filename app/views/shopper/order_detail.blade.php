@extends('shopper.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top:0px">
        @if($order->status == 'Initiated')
        <a href="#" class="accept-order" data-id="{{$order->id}}"><button type="button" class="btn btn-success">Accept</button></a>
        @elseif($order->status == 'In Progress')
        <a href="{{ admin_url(true) }}/order/{{ $order->id }}/update?status=Delivered"><button type="button" class="btn btn-primary">Delivered</button></a>
        @endif

        @if($order->status != 'Cancelled' && $order->status != 'Delivered')
        <a href="#" class="reject-order" data-id="{{$order->id}}">
            <button type="button" class="btn btn-danger">Reject Order</button>
        </a>
        @endif
    </span>

</section>

<section class="content">
    @if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif
    <div class="row">
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Order Details</h4>
                <p>Date - {{ date("d M Y",strtotime($order->date)); }}</p>
                <p>Total Amount - ${{ $order->total_amount }}</p>
                <p>Status - <b>{{ $order->status }}</b></p>
                @if($order->status == 'Cancelled')
                <p>Reject reason - {{ $order->reject_reason }}</p>
                @endif
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Customer Details</h4>
                <p>Name - {{ $order->first_name }} {{ $order->last_name }}</p>
                <p>Phone - {{ $order->phone }}</p>
                <p>Email - {{ $order->email }}</p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Delivery Details</h4>
                <p>{{ $order->address }}</p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Shopper Details</h4>
                @if($shopper)
                <p>Name - {{ $shopper->first_name }} {{ $shopper->last_name }}</p>
                <p>Phone - {{ $shopper->phone }}</p>
                <p>Email - {{ $shopper->email }}</p>
                @endif
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="shelves-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Cart Quantity</th>
                                <th>Product Quantity</th>
                                <th>Price</th>
                                <th>Store</th>
                                <th>Fulfilment</th>
                                @if($order->status != 'Cancelled' && $order->status != 'Delivered')
                                <th>Actions</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order_products as $product)
                            <tr>
                                <td><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->store_name }}</td>
                                <td>
                                    @if($product->fulfilment_status == 'Fullfilled')
                                    <span class="badge bg-green">Fulfilled</span>
                                    @elseif($product->fulfilment_status == 'Not Available')
                                    <span class="badge bg-red">Not Available</span>
                                    @elseif($product->fulfilment_status == 'Pending')
                                    <span class="badge bg-orange">Pending</span>
                                    @else
                                    <span class="badge bg-blue">Replaced</span>
                                    @endif
                                </td>
                                @if($order->status != 'Cancelled' && $order->status != 'Delivered')
                                <td>
                                    <div class="btn-group">
                                        @if($product->fulfilment_status == 'Pending')

                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                                                                        <li><a href="#" onclick="edit({{ $product->id }},{{ $product->quantity }},{{ $product->price }});">Change Quantity &amp; Price</a></li>
                                            <li><a href="{{ web_url() }}/shopper/orderproduct/update?status=Fullfilled&id={{ $product->id }}">Fulfilled</a></li>
                                            <li><a href="{{ web_url() }}/shopper/orderproduct/update?status=Not Available&id={{ $product->id }}">Not Available</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onclick="replace({{ $product->id }});">Replace Product</a></li>

                                        </ul>
                                        @endif
                                    </div>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<!-- Edit Modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modify Order</h4>
      </div>
        <form method="post" action="{{ web_url() }}/shopper/orderproduct/update">
      <div class="modal-body">
            <input type="hidden" id="order-product-id" name="id" value="">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Price</label>
            <input type="text" class="form-control" name="price" id="order-product-price">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Quantity</label>
            <input type="text" class="form-control" name="quantity" id="order-product-quantity">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Replace Modal -->
<div class="modal fade" id="replace-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Replacement</h4>
      </div>
        <form method="post" action="{{ web_url() }}/{{$store_admin?"storeadmin":"shopper"}}/orderproduct/update" enctype="multipart/form-data">
      <div class="modal-body">
            <input type="hidden" id="order-product-parent-id" name="id" value="">
            <div class="form-group">
            <label for="recipient-name" class="control-label">Name</label>
            <input type="text" class="form-control" name="product_name" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Image</label>
            <input type="file" class="form-control" name="product_image_url" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Product Quantity</label>
            <input type="text" class="form-control" name="product_quantity" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Product Unit</label>
            <select class="form-control" id="" name="product_unit">
                <option value='No'>No's</option>
                <option value='bunch'>bunch</option>
                <option value='liter' >liter</option>
                <option value='kg'>kg</option>
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Price</label>
            <input type="text" class="form-control" name="price" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Cart Quantity</label>
            <input type="text" class="form-control" name="quantity" >
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>



<!-- Edit Modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modify Order</h4>
      </div>
        <form method="post" action="{{ admin_url(true) }}/orderproduct/update">
      <div class="modal-body">
            <input type="hidden" id="order-product-id" name="id" value="">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Price</label>
            <input type="text" class="form-control" name="price" id="order-product-price">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Quantity</label>
            <input type="text" class="form-control" name="quantity" id="order-product-quantity">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Replace Modal -->
<div class="modal fade" id="replace-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Replacement</h4>
      </div>
        <form method="post" action="{{ admin_url() }}/orderproduct/update" enctype="multipart/form-data">
      <div class="modal-body">
            <input type="hidden" id="order-product-parent-id" name="id" value="">
            <div class="form-group">
            <label for="recipient-name" class="control-label">Name</label>
            <input type="text" class="form-control" name="product_name" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Image</label>
            <input type="file" class="form-control" name="product_image_url" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Product Quantity</label>
            <input type="text" class="form-control" name="product_quantity" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Product Unit</label>
            <select class="form-control" id="" name="product_unit">
                <option value='No'>No's</option>
                <option value='bunch'>bunch</option>
                <option value='liter' >liter</option>
                <option value='kg'>kg</option>
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Price</label>
            <input type="text" class="form-control" name="price" >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Cart Quantity</label>
            <input type="text" class="form-control" name="quantity" >
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>


<div class="modal fade" id="modal-status">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title reject-title">Reject order</h4>
        <h4 class="modal-title accept-title">Accept order</h4>
      </div>
      <div class="modal-body">
        <label class="accept-label">Select the estimated delivery time in minutes for this order</label>
        <select class="form-control accept-options">
            @for($i = 15; $i < 100; $i += 15)
                <option value="{{$i}}">{{$i}}</option>
            @endfor
        </select>
        <label class="reject-label">Select reason why you can't deliver this order</label>
        <select class="form-control reject-options">
            @foreach($reasons as $reason)
                <option value="{{$reason->reason}}">{{$reason->reason}}</option>
            @endforeach
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success save-status accept-btn">Accept order</button>
        <button type="button" class="btn btn-danger save-status reject-btn">Reject order</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    $(function() {
        // DataTable
        var table = $('#shelves-table').DataTable();
        var id = null;
        // Aceptar / Rechazar
        // {{ web_url() }}/admin/order/{{ $order->id }}/update?status=In Progress
        // {{ web_url() }}/admin/order/{{ $order->id }}/update?status=Cancelled

        $('.accept-order, .reject-order').click(function() {
            id = $(this).attr('data-id');
            $('.modal *[class*="' + (($(this).hasClass('accept-order')) ? 'accept-' : 'reject-') + '"]').show();
            $('.modal *[class*="' + (($(this).hasClass('accept-order')) ? 'reject-' : 'accept-') + '"]').hide();
            $('#modal-status').addClass((($(this).hasClass('accept-order')) ? 'accept' : 'reject')).modal('show');
        })

        $('.save-status').click(function() {

            switch($(this).parents('.modal').hasClass('accept')) {
                case true:
                    window.location.href = "{{ admin_url() }}/order/" + id + "/update?status=In Progress&reason=" + $('.accept-options').val();
                break;
                case false:
                    window.location.href = "{{ admin_url() }}/order/" + id + "/update?status=Cancelled&reason=" + $('.reject-options').val();
                break;
            }
        })
    });

    function edit(product_id,quantity,price){
        $('#order-product-id').val(product_id);
        $('#order-product-price').val(price);
        $('#order-product-quantity').val(quantity);
        $('#edit-modal').modal('show');
    }

    function replace(product_id){
        $('#order-product-parent-id').val(product_id);
        $('#replace-modal').modal('show');
    }
</script>

@stop