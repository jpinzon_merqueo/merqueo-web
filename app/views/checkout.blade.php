@extends('layout')

@section('content')
<div class="col-sm-10 col-sm-offset-1">
	<br>
	<h3>Resumen de tu Pedido</h3>
	<hr>
</div>
<div class="cart-data cart-view col-sm-5 col-sm-offset-1 col-xs-12 no-padding">
	<p align="center"><img src="{{ asset_url() }}/img/loading.gif" /></p>
</div>
<div class="checkout-bg col-sm-5 col-xs-12">
	<form class="form-horizontal col-xs-12 order" id="user-data-form" role="form" method="post" action="{{ route('frontCheckout.checkout') }}">
		@if(Session::has('message'))
	  	<div class="row">
			@if(Session::get('type') == 'success')
			<div class="alert alert-success alert-dismissable">
			    {{ Session::get('message') }}
			</div>
			@else
			<div class="alert alert-danger alert-dismissable">
				<script>
					error = true;
				</script>
			    {{ Session::get('message') }}
			</div>
			@endif
		</div>
		@endif

		@if(!Auth::check())
		<div class="account">
			<div class="row"><h3>¿Ya tienes cuenta?</h3><hr></div>
			<div class="form-group login-fb text-center">
			  <div class="unseen alert alert-danger fb-signin-error"></div>
			  <div align="center" class="fb-signin-loading unseen"><img src="{{ asset_url() }}/img/loading.gif" /></div>
		      <button type="button" class="btn btn-primary btn-block fb-signin"><i class="icon-facebook"></i>&nbsp;&nbsp;&nbsp;Registrate o ingresa con Facebook</button>
		      <button type="button" data-toggle="modal" data-target="#login" class="btn-block btn btn-success login-button">Ingresa con tu cuenta de Merqueo</button><br>
		    </div>
			<div class="row"><h3>Crea una nueva Cuenta</h3><hr></div>
		    <input type="hidden" name="create_user" value="1">
			<div class="row">
				<div class="form-group col-xs-6">
					<label>Nombres</label>
				    <input type="text" class="form-control onlyLetters required" placeholder="Nombres" name="first_name" maxlength="80" value="{{ $post['first_name'] or '' }}">
				</div>
				<div class="form-group col-xs-6 field-margin">
					<label>Apellidos</label>
				    <input type="text" class="form-control onlyLetters required" placeholder="Apellidos" name="last_name" maxlength="80" value="{{ $post['last_name'] or '' }}">
				</div>
			</div>
            <div class="row">
                <div class="form-group col-md-12 form-group-validate">
                    <label>Celular</label>
                    <div class="input-group validate-group" style="width: 100%;">
                        <input type="text"
                               class="form-control onlyNumbers required"
                               placeholder="Celular"
                               name="phone"
                               maxlength="10"
							   {{ Session::get('phoneValidation.validated') ? 'disabled' : '' }}
                               value="{{ Session::get('phoneValidation.validated') ? Session::get('phoneValidation.number') : (empty($post['phone']) ? '' : $post['phone']) }}">
						<span id="send-button" class="input-group-addon validate-button"
							  style="width: 50%;">
							Validar número
						</span>
                    </div>
					<span class="help-block"></span>
				</div>
            </div>
            <div class="row">
                <div class="form-group col-xs-6">
                    <label>Correo electronico</label>
                    <input type="email"
						   class="form-control required"
						   placeholder="Email"
						   name="email"
						   maxlength="100"
						   {{ Session::get('facebookData.email') ? 'disabled' : '' }}
						   value="{{ $post['email'] or '' }}">
                </div>
                <div class="form-group col-xs-6 field-margin">
                    <label>Contraseña</label>
                    <input type="password" class="form-control required" placeholder="Contraseña" name="password" maxlength="100">
                </div>
            </div>
			<br>
		</div>
		@endif

		<div class="user-phone @if(Auth::check() && empty(Auth::user()->phone)) nothing @else unseen @endif">
			<div class="row"><h3>Datos Personales</h3><hr></div>
			<div class="form-group">
				<label>Celular</label>
			    <input type="text" class="form-control onlyNumbers required" placeholder="Celular" name="user_phone" maxlength="10" value="{{ $post['user_phone'] or '' }}">
			</div>
			<div class="user-email form-group @if(Auth::check() && empty(Auth::user()->email)) nothing @else unseen @endif">
                <label>Email</label>
                <input type="text" class="form-control required" placeholder="Email" name="user_email" value="{{ $post['user_email'] or '' }}">
            </div><br>
		</div>

	    <!--<div class="row"><h3>Observaciones de tu Pedido</h3><hr></div>
		<div class="form-group">
			<textarea class="form-control" placeholder="Escribe observaciones especificas para tu pedido" name="comments">{{ $post['comments'] or '' }}</textarea>
		</div><br></br>-->

	  	<div class="row"><h3>Dirección de Envío<a href="#coverage" class="fancybox coverage">Ver cobertura</a></h3><hr></div>
		<div class="row">
			<h2 class="checkout-address-text">
				{{ empty($current_address->address_text) ? '' : $current_address->address_text }}
				{{ empty($current_address->address_further) ? '' : $current_address->address_further }}
				{{ empty($current_address->neighborhood) ? '' : $current_address->neighborhood }}
			</h2>
		</div>
		<div class="new-address">
			<input type="hidden" value="{{ $current_address->city_id }}" name="city_id">
			@if (count($surrounded_cities['surrounded_cities']))
				<input type="hidden" name="surrounded_city_id" value="{{ $current_address->surrounded_city_id }}">
			@endif

			<input type="hidden" name="address_id" value="{{ empty($current_address->id) ? 0 : $current_address->id }}">
			<input type="hidden" name="dir[]" value="{{ empty($current_address_parts->dir1) ? 0 : $current_address_parts->dir1 }}">
			<input type="hidden" name="dir[]" value=" ">
			<input type="hidden" name="dir[]" value="{{ empty($current_address_parts->dir2) ? 0 : $current_address_parts->dir2 }}">
			<input type="hidden" name="dir[]" value=" ">
			<input type="hidden" name="dir[]" value="#">
			<input type="hidden" name="dir[]" value=" ">
			<input type="hidden" name="dir[]" value="{{ empty($current_address_parts->dir3) ? 0 : $current_address_parts->dir3 }}">
			<input type="hidden" name="dir[]" value="-">
			<input type="hidden" name="dir[]" value="{{ empty($current_address_parts->dir4) ? 0 : $current_address_parts->dir4 }}">

			@if (empty($current_address->id))
				<div class="form-group">
					<div class="col-md-6 no-padding">
						<input type="text" class="form-control" name="address_further" placeholder="Ej: Apartamento 501, Torre 2" maxlength="100" value="{{ $post['address_further'] or '' }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control required" name="address_neighborhood" placeholder="Barrio" maxlength="100" value="{{ $post['address_neighborhood'] or '' }}">
					</div>
				</div>

				<div class="form-group">
					<label for="lab">Nombre para ésta dirección</label><br>
					<div class="form-group col-md-5">
						<select type="text" class="form-control required" name="address_name">
							<option value="Casa" @if (isset($post['address_name']) && $post['address_name'] == 'Casa') selected="selected" @endif>Casa</option>
							<option value="Apartamento" @if (isset($post['address_name']) && $post['address_name'] == 'Apartamento') selected="selected" @endif>Apartamento</option>
							<option value="Oficina" @if (isset($post['address_name']) && $post['address_name'] == 'Oficina') selected="selected" @endif>Oficina</option>
							<option value="Otro" @if (isset($post['address_name']) && $post['address_name'] == 'Otro') selected="selected" @endif>Otro</option>
						</select>
					</div>
					<label for="dir2" class="col-xs-1">&nbsp;</label>
					<div class="form-group col-md-6 @if (!(isset($post['address_name']) && $post['address_name'] == 'Otro')) unseen @endif">
						<input type="text" class="form-control required" name="other_name" placeholder="Nombre" maxlength="150" value="{{ $post['other_name'] or '' }}">
					</div>
				</div>
			@endif
			<input type="hidden" name="is_checkout" value="1">
		</div>

	    <div class="row">
	        <h3>Redimir Cupón</h3><hr>
	        @if (Session::has('coupon_code'))
	        <div class="checkout-coupon">
	            @if (Session::has('coupon_code'))
    	        <label>Cupón de descuento activo</label>
    	        <div>{{ Session::get('coupon_code') }}&nbsp;<button type="button" class="close btn-remove-coupon" aria-label="Close" title="Desmarcar cupón"><span aria-hidden="true">&times;</span></button></div>
    	        @endif
    	    </div>
	        @endif
		    <div class="btn-show-coupon">
		    	<button type="button" class="btn-block btn btn-success">Redimir</button><br>
		    </div>
		    <div class="unseen coupon">
			    <div class="unseen alert alert-danger coupon-error"></div>
			    <div class="unseen alert alert-success coupon-success"></div>
		        <label>Si tienes un cupón ingresa el código aquí</label>
			    <input type="text" class="form-control" placeholder="Código" name="coupon_code" maxlength="30" value="{{ $post['coupon_code'] or '' }}">
			    <br>
			    <button type="button" class="pull-left btn btn-success btn-coupon col-md-2 col-sm-2 col-xs-2">Redimir</button>
			    <button type="button" class="pull-left btn btn-info btn-hide-coupon col-md-2 col-sm-2 col-xs-2">Ocultar</button>
			    <p align="center" class="loading-coupon unseen"><img src="{{ asset_url() }}/img/loading.gif" /></p>
				<br/><br/>
			</div><br/>
		</div>

		<div class="row"><h3>¿Cuándo quieres recibir tu Pedido?</h3><hr></div>
		@foreach($delivery as $store_id => $delivery_store)
		<div class="form-group">
		    @if (count($delivery) > 1)
		    <h4>Pedido en tienda {{ $delivery_store['name'] }}</h4><br>
		    @endif
		    @if ($checkout_discounts['discount_percentage']['status'] && $checkout_discounts['discount_percentage']['order_for_tomorrow']
		    && $checkout_discounts['discount_percentage']['store_id'] == $store_id)
			<div class="blo_promo">
				<div class="contblo">
					<div class="text-title">
						<p>Gánate un <span>{{ $checkout_discounts['discount_percentage']['amount'] }}% de descuento en tu pedido de Merqueo Super</span> programando tu entrega para mañana</p>
					</div>
					<div class="img_pro">
						<img src="{{ asset_url() }}/img/grocery-bags.png" alt="grocery-bags">
					</div>
				</div>
			</div>
			@endif
			<select class="form-control required delivery_day" name="delivery_day_{{ $store_id }}" id="delivery_day_{{ $store_id }}" data-store-id="{{ $store_id }}">
	       		<option value="">Selecciona el día</option>
	       		@if (count($delivery_store['days']))
	       		@foreach($delivery_store['days'] as $value => $date)
	       		<option value="{{ $value }}" @if (isset($post['delivery_day_'.$store_id]) && $post['delivery_day_'.$store_id] == $value) selected="selected" @endif>{{ $date }}</option>
	       		@endforeach
	       		@endif
	       	</select>
	    </div>
	    <div class="form-group delivery_time_block delivery_time_{{ $store_id }}">
	       	<select class="form-control required delivery_time" name="delivery_time_{{ $store_id }}" id="delivery_time_{{ $store_id }}">
	       		<option value="">Selecciona la hora</option>
	       		@if (isset($post['delivery_time_'.$store_id]) && isset($delivery_store['time'][$post['delivery_day_'.$store_id]]))
				  @foreach($delivery_store['time'][$post['delivery_day_'.$store_id]] as $value => $data)
					<option value="{{ $value }}" {{ $post["delivery_time_{$store_id}"] === $value ? 'selected="selected"' : '' }}>{{ $data['text'] }}</option>
				  @endforeach
	       		@endif
	       	</select>
	    </div><br>
	    @endforeach

		<div class="row payment"><h3>Método de Pago</h3><hr></div>
		@if($campaigns['visa']->isEnabled([]))
			<?php $campaign = $campaigns['visa'] ?>
			<div class="form-group">
				<div class="blo_promo">
					<div class="contblo">
						<div class="img_pro">
							<img src="{{ asset_url() }}/img/visa.png" alt="grocery-bags">
						</div>
						<div class="text-title">
							<p>{{ $campaign->getMessage() }}</p>
						</div>
					</div>
				</div>
			</div>
		@elseif ($campaigns['bancolombia']->isEnabled([]))
			<?php $campaign = $campaigns['bancolombia'] ?>
			<div class="form-group">
				<div class="blo_promo">
					<div class="contblo">
						<div class="img_pro">
							<img src="{{ asset_url() }}/img/bancolombia.png" alt="grocery-bags">
						</div>
						<div class="text-title">
							<p>{{ $campaign->getMessage() }}</p>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="form-group">
	       	<select type="text" class="form-control required" name="payment_method">
	       	    <option value="">Selecciona el método de pago</option>
	       	    @foreach($payment_methods as $value => $name)
	       		<option value="{{ $value }}" @if (isset($post['payment_method']) && $post['payment_method'] == $value) selected="selected" @endif>{{ $name }}</option>
	       		@endforeach
	       	</select>
	    </div>
	    <div class="form_pse unseen">
	    	<div class="row"><br><h4>Débito - PSE</h4><br></div>
			<div class="form-group">
				<div class="box-info-devolucion-dinero">
					<p>
						Hacemos todo lo posible para que el pedido salga completo, en caso de presentar inconvenientes con un producto y el valor es inferior a
						<b>$10.000</b> se reembolsará en créditos <b>Merqueo</b>, de ser superior nuestro servicio al cliente se contactará contigo.
					</p>
				</div>
			</div>
	    	<div class="row">
	    		<div class="form-group col-xs-6">
	    			<label>Banco</label>
	    			<select class="form-control pse_field" id="bank_pse" name="bank_pse">
                        <option value="" selected="selected">Seleccione</option>
                        @foreach($banks as $bank)
                        	<option value="{{!$bank->pseCode ? '' : $bank->pseCode}}" @if (isset($post['bank_pse']) && $post['bank_pse'] == $bank->pseCode) selected="selected" @endif>{{$bank->description}}</option>
                        @endforeach
	    			</select>
	    		</div>
               	<label class="label-complement">&nbsp;</label>
	    		<div class="form-group col-xs-6">
					<label>Nombre</label>
				    <input type="text" autocomplete="off" class="form-control onlyLetters pse_field" placeholder="Nombre" name="name_pse" maxlength="24" value="{{ $post['name_pse'] or '' }}">
				</div>
	    	</div>
	    	<div class="row">
	    		<div class="form-group col-xs-6">
	    			<label>Tipo de persona</label>
	    			<select class="form-control pse_field" id="type_person_pse" name="type_person_pse">
                        <option value="" selected="selected">Seleccione</option>
                        <option value="N" @if (isset($post['type_person_pse']) && $post['type_person_pse'] == 'N') selected="selected" @endif>Persona Natural</option>
                        <option value="J" @if (isset($post['type_person_pse']) && $post['type_person_pse'] == 'J') selected="selected" @endif>Persona Jurídica</option>
	    			</select>
	    		</div>
               	<label class="label-complement">&nbsp;</label>
			    <div class="form-group col-xs-6">
                    <label>Tipo de documento</label>
                    <select type="text" class="form-control pse_field" id="document_type_pse" name="document_type_pse">
                        <option value="" selected="selected">Tipo de documento</option>
                        <option value="CC" @if (isset($post['document_type_pse']) && $post['document_type_pse'] == 'CC') selected="selected" @endif>Cédula de ciudadania</option>
                        <option value="CE" @if (isset($post['document_type_pse']) && $post['document_type_pse'] == 'CE') selected="selected" @endif>Cédula de extranjeria</option>
                        <option value="NIT" @if (isset($post['document_type_pse']) && $post['document_type_pse'] == 'NIT') selected="selected" @endif>Número de identificación tributario</option>
                        <option value="PP" @if (isset($post['document_type_pse']) && $post['document_type_pse'] == 'PP') selected="selected" @endif>Pasaporte</option>
                    </select>
                </div>
	    	</div>
	    	<div class="row">
			    <div class="form-group col-xs-6">
			        <label>Documento de identificación</label>
                    <input type="text" autocomplete="off" class="form-control onlyNumbers pse_field" placeholder="Número de documento" id="document_number_pse" name="document_number_pse" maxlength="16" value="{{ $post['document_number_pse'] or '' }}">
                </div>
                <label class="label-complement">&nbsp;</label>
                <div class="form-group col-xs-6">
                    <label>Télefono</label>
                    <input type="text" autocomplete="off" class="form-control onlyNumbers pse_field" placeholder="Teléfono del titular" id="phone_pse" name="phone_pse" maxlength="10" value="{{ $post['phone_pse'] or '' }}">
                </div>
            </div>
            <script type="text/javascript" src="https://maf.pagosonline.net/ws/fp/tags.js?id={{$device_session_id}}80200"></script>
			<noscript>
				<iframe style="width: 100px; height: 100px; border: 0; position: absolute; top: -5000px;" src="https://maf.pagosonline.net/ws/fp/tags.js?id={{$device_session_id}}80200"></iframe>
			</noscript>
	    	
	    </div>
	    <div class="form_cc unseen">
	    	<div class="row"><br><h4>Tarjeta de Crédito</h4><br></div>
	    	<div class="user-credit-card @if(!$credit_cards) unseen @endif">
    	    @if($credit_cards)
	    		<h4><div class="credit-card-toggle pull-right btn btn-info">Nueva tarjeta</div></h4><br><br><hr>
				<input type="hidden" name="credit_card_id" value="">
				@foreach($credit_cards as $cc)
				<div class="bg-info">
					<a class="credit-card btn btn-default btn-sm pull-right" data-id="{{ $cc['id'] }}" href="javascript:;">+ Utilizar esta tarjeta</a>
					<img src="{{ web_url() }}/assets/img/credit_card.png">&nbsp;&nbsp;&nbsp;Tarjeta de crédito<br>
					<b>**** {{ $cc['last_four'] }}</b><br>
					<i>{{ $cc['type'] }}</i><br>
					@if(empty($cc['holder_document_number']))
					<div class="add-document-number-cc unseen">
					    <br>
				        <div class="row">
                            <div class="form-group col-xs-6">
                                <label>Documento de identificación</label>
                                <select type="text" class="form-control cc" name="add_document_type_cc_{{ $cc['id'] }}">
                                    <option value="" selected="selected">Tipo de documento</option>
                                    <option value="CC" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'CC') selected="selected" @endif>Cédula de ciudadania</option>
                                    <option value="CE" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'CE') selected="selected" @endif>Cédula de extranjeria</option>
                                    <option value="NIT" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'NIT') selected="selected" @endif>Número de identificación tributario</option>
                                    <option value="PP" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'PP') selected="selected" @endif>Pasaporte</option>
                                </select>
                           </div>
                           <label class="label-complement">&nbsp;</label>
                           <div class="form-group col-xs-6">
                                <label>&nbsp;</label>
                                <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Número de documento" name="add_document_number_cc_{{ $cc['id'] }}" maxlength="16" value="{{ $post['add_document_number_cc_'.$cc['id']] or '' }}">
                           </div>
                        </div>
                    </div>
                    @endif
				</div><hr>
				@endforeach
			@endif
			</div>
	    	<div class="new-credit-card @if($credit_cards) unseen @endif">
	    		@if($credit_cards)
					<div class="credit-card-toggle pull-right btn btn-info">Usar una de mis tarjetas guardadas</div>
					<br><br><br>
				@endif
				<div class="form-group">
				    <p>Si es tu primera compra con Tarjeta de Crédito y no eres el dueño, te llamáremos y modificaremos el método de pago a Datáfono. Lo hacemos por tu seguridad. (Solo se hará la primera vez).
					<!--<p>Se realizará un cargo de $1.000 COP en tu tarjeta de crédito para su validación, este monto no se cobrará ni aparecerá en tu extracto bancario. Solo aceptamos tarjetas de crédito tipo VISA, MASTERCARD y AMERICAN EXPRESS.-->
					<br><br><img src="{{ asset_url() }}/img/payment_methods.png" width="200">
					<img src="{{ asset_url() }}/img/payu.png" width="65">
					</p>
				</div>
	        	<div class="form-group">
					<label>Nombre (tal como aparece en tu tarjeta)</label>
				    <input type="text" autocomplete="off" class="form-control onlyLetters cc" placeholder="Nombre" name="name_cc" maxlength="24" value="{{ $post['name_cc'] or '' }}">
				</div>
				<div class="form-group">
					<label>Número de tarjeta</label>
				    <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Número de tarjeta" id="number_cc" name="number_cc" maxlength="16" value="{{ $post['number_cc'] or '' }}">
				</div>
				<div class="form-group">
					<label>Código de seguridad</label>
				    <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Código de seguridad" name="code_cc" maxlength="4" value="{{ $post['code_cc'] or '' }}">
				</div>
				<div class="row">
					<div class="form-group col-xs-6"><label>Fecha de expiración</label>
					    <select type="text" class="form-control cc" id="expiration_year_cc" name="expiration_year_cc">
					    	<option value="" selected="selected">Año</option>
			           		@foreach($dates_cc['years'] as $year)
			           		<option value="{{ $year }}" @if (isset($post['expiration_year_cc']) && $post['expiration_year_cc'] == $year) selected="selected" @endif>{{ $year }}</option>
			           		@endforeach
			           	</select>
		           </div>
		           <label class="label-complement">&nbsp;</label>
		           <div class="form-group col-xs-6">
		           		<label>&nbsp;</label>
			           	<select type="text" class="form-control cc" id="expiration_month_cc" name="expiration_month_cc">
					    	<option value="" selected="selected">Mes</option>
			           		@foreach($dates_cc['months'] as $month)
			           		<option value="{{ $month }}" @if (isset($post['expiration_month_cc']) && $post['expiration_month_cc'] == $month) selected="selected" @endif>{{ $month }}</option>
			           		@endforeach
			           	</select>
		           </div>
				</div>
				<div class="row">
				    <div class="form-group col-xs-6">
                        <label>Documento de identificación del titular</label>
                        <select type="text" class="form-control cc" id="document_type_cc" name="document_type_cc">
                            <option value="" selected="selected">Tipo de documento</option>
                            <option value="CC" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'CC') selected="selected" @endif>Cédula de ciudadania</option>
                            <option value="CE" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'CE') selected="selected" @endif>Cédula de extranjeria</option>
                            <option value="NIT" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'NIT') selected="selected" @endif>Número de identificación tributario</option>
                            <option value="PP" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'PP') selected="selected" @endif>Pasaporte</option>
                        </select>
                   </div>
                   <label class="label-complement">&nbsp;</label>
    			   <div class="form-group col-xs-6">
    			        <label>&nbsp;</label>
                        <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Número de documento" id="document_number_cc" name="document_number_cc" maxlength="16" value="{{ $post['document_number_cc'] or '' }}">
                   </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-6">
                        <label>Teléfono del titular</label>
                        <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Teléfono del titular" id="phone_cc" name="phone_cc" maxlength="10" value="{{ $post['phone_cc'] or '' }}">
                   </div>
                   <label class="label-complement">&nbsp;</label>
                   <div class="form-group col-xs-6">
                        <label>Dirección del titular</label>
                        <input type="text" autocomplete="off" class="form-control cc" placeholder="Dirección del titular" id="address_cc" name="address_cc" maxlength="50" value="{{ $post['address_cc'] or '' }}">
                   </div>
                </div>
	        </div>
	        <div class="form-group">
				<label>Número de cuotas</label>
			    <select type="text" class="form-control cc" id="installments_cc" name="installments_cc">
			    	<option value="" selected="selected"></option>
	           		@for ($i = 1; $i <= 36; $i++)
	           			@if ($i == 1)
	           				<option value="1" selected="selected">1</option>
	           				@else
	           				<option value="{{ $i }}" @if (isset($post['installments_cc']) && $post['installments_cc'] == $i) @endif>{{ $i }}</option>
	           			@endif
	           		@endfor
	           	</select>
			</div>
	    </div><br>
		<div class="form-group">
			<input type="checkbox" name="invoice" id="invoice"/>
			<label style="display: inline;" for="invoice">Necesito factura</label>
		</div>
		<div class="content-invoice unseen">
			<div class="form-group">
				<select class="form-control" name="type_invoice" id="type-invoice">
					<option value="natural">Persona Natural</option>
					<option value="company" {{ $invoice_data && $invoice_data->user_identity_type == 'NIT' ? 'selected' : '' }}>Empresa</option>
				</select>
			</div>
			<div class="row {{ $invoice_data && $invoice_data->user_identity_type == 'NIT' ? 'unseen' : '' }}" id="content-type-natural">
				<div class="form-group col-xs-5">
					<label>Nombre completo</label>
					<input type="text"
						   name="invoice_user_name"
						   class="form-control {{ $invoice_data && $invoice_data->user_identity_type != 'NIT' ? 'required' : '' }} "
						   placeholder="Nombre completo"
						   autocomplete="off"
						   value="{{ $invoice_data && $invoice_data->user_identity_type != 'NIT' ? $invoice_data->user_business_name : '' }}" />
				</div>
				<div class="form-group col-xs-3">
					<label>Tipo de doc</label>
					<select name="invoice_user_identity_type" class="form-control">
						<option value="Cédula" {{ $invoice_data && $invoice_data->user_identity_type == 'Cédula' ? 'selected' : '' }} >C.C</option>
						<option value="Cédula extranjería" {{ $invoice_data && $invoice_data->user_identity_type == 'Cédula extranjería' ? 'selected' : '' }}>C.E</option>
					</select>
				</div>
				<div class="form-group col-xs-5">
					<label>Número de documento</label>
					<input type="text"
						   name="invoice_user_identity_number"
						   class="form-control onlyNumbers {{ $invoice_data && $invoice_data->user_identity_type != 'NIT' ? 'required' : '' }} "
						   placeholder="Número de documento"
						   autocomplete="off"
						   maxlength="15"
						   value="{{ $invoice_data && $invoice_data->user_identity_type != 'NIT' ? $invoice_data->user_identity_number : '' }}" />
				</div>
			</div>
			<div class="row {{ $invoice_data && $invoice_data->user_identity_type != 'NIT' ? 'unseen' : !$invoice_data ? 'unseen' : '' }}" id="content-type-company">
				<div class="form-group col-xs-6">
					<label>Nombre de la empresa</label>
					<input type="text"
						   name="invoice_company_name"
						   class="form-control {{ $invoice_data && $invoice_data->user_identity_type == 'NIT' ? 'required' : '' }} "
						   placeholder="Nombre de la empresa"
						   autocomplete="off"
						   value="{{ $invoice_data && $invoice_data->user_identity_type == 'NIT' ? $invoice_data->user_business_name : '' }}" />
				</div>
				<label class="label-complement">&nbsp;</label>
				<div class="form-group col-xs-6">
					<label>NIT <small>(Sin número de verificación)</small></label>
					<input type="text"
						   name="invoice_company_nit"
						   class="form-control onlyNumbers {{ $invoice_data && $invoice_data->user_identity_type == 'NIT' ? 'required' : '' }} "
						   placeholder="Sin número de verificación"
						   autocomplete="off"
						   maxlength="9"
						   value="{{ $invoice_data && $invoice_data->user_identity_type == 'NIT' ? $invoice_data->user_identity_number : '' }}" />
				</div>
			</div>
			<br>
		</div>
	    <div class="form-group" id="terms-container">
			<input type="checkbox" name="terms" id="terms" class="required"/>
			<label style="display: inline;" for="terms">He leído y estoy de acuerdo con los <a href="{{ web_url() }}/terminos" target="blank_">términos y condiciones</a> y <a class="pinkLink" href="{{ web_url() }}/politicas-de-privacidad" target="_blank">políticas de privacidad.</a></label>
			<br><br><p class="small-font">El total a pagar puede variar dependiendo de la disponibilidad y/o cambios de los productos del pedido.</p>
		</div>
	    <div class="form-group unseen alert alert-danger form-has-errors">
		    <i class="fa fa-check"></i>
		    <b>Hay errores en el formulario,</b> corrígelos para continuar:
		    <ul class="error-list"></ul>
		</div><br>
		<div class="form-group">
			<button type="submit" class="btn btn-success btn-block btn-lg">REALIZAR PEDIDO</button>
			<div align="center" class="loading unseen"><img src="{{ asset_url() }}/img/loading.gif" /></div>
		</div>
		<br><br>
	</form>
</div>

@if (\Session::has('removed_products') && \Session::pull('removed_from_checkout_post'))
	<div class="modal fade next-modal-style"
		 id="removed-products-modal-container"
         tabindex="-1"
         role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Los siguientes productos están agotados.</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
							@include('removed_products_list', ['removed_products' => \Session::get('removed_products')])
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row col-sm-12" style="margin: 0">
						<label for="accept-terms-on-modal" class="text-center" style="display: block; margin-bottom: 20px">
							Si continuas tu pedido quedará sin estos productos. ¿Deseas continuar?
						</label>
					</div>

					<button type="submit"
							class="btn btn-success"
							id="accept-changes-from-checkout">Continuar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function () {
            $('#removed-products-modal-container').modal('show');
        });
	</script>
@endif


<script>
    var delivery_times = JSON.parse('{{ json_encode($delivery) }}');
    var error = false;
    var confirmed = false;
    var phoneValidated = {{ Session::get('phoneValidation.validated') ? 'true' : 'false' }};

    $(document).ready(function($) {
        var selected = $('.delivery_time').find(':selected').val();

        if(!error){
            $.each(delivery_times, function(value, text) {
                $('#delivery_day_' + value).val($('#delivery_day_' + value +' option').eq(1).val());
                $('#delivery_day_' + value).trigger('change');
                $('#delivery_time_' + value).val($('#delivery_time_' + value + ' option').eq(1).val());
            });
            if ( $('input[name=credit_card_id]').length > 0 ) {
                var credit_card_option = $('select[name=payment_method] option').eq(1).val();
                $('select[name=payment_method]').val(credit_card_option).trigger('change');
                $('.credit-card.btn').trigger('click');
                $('select[name=installments_cc]').val(1);
            }else{
                var credit_card_option = $('select[name=payment_method] option').eq(2).val();
                $('select[name=payment_method]').val(credit_card_option).trigger('change');
            }

            if($('body .user-credit-card').is(':visible')){
                $('body .new-credit-card').hide();
                $('body .user-credit-card').show();
                var id =  $('body .user-credit-card').find('.credit-card:first-child').data('id');
                $('body .user-credit-card [name="credit_card_id"]').val(id);
                $('body .user-credit-card .credit-card').removeClass('btn-primary');
                $('body .user-credit-card .credit-card:eq(0)').trigger('click');
                $('body #installments_cc option:selected').removeAttr('selected');
                $('body #installments_cc').val(1);
                if ($('body .user-credit-card .credit-card:eq(0)').parent().find('.add-document-number-cc').length)
                    $('body .user-credit-card .credit-card:eq(0)').parent().find('.add-document-number-cc').show();
            }
        }else{
            $.each(delivery_times, function(value, text) {
                $('#delivery_day_' + store_id).trigger('change');
            });
        }

        $('#accept-changes-from-checkout').on('click', function () {
            confirmed = true;
            var data = getConfirmData();
            data.action = 'continue';
            enableLoader();

            trackEvent('unavailable_products', data, function () {
                $('#removed-products-modal-container').modal('hide');
                $('#terms').prop('checked', true);
                $.ajax({url: '{{ action('CheckoutController@remove_limited_products') }}'})
                    .done(submit)
					.fail(submit);

                function submit() {
                    $('#user-data-form').submit();
                    hideLoader();
                    $('html, body').animate({
                        scrollTop: $('input[name="password"]')
							.offset().top - 100
                    }, 1000);
                }
            });
        });

        $('#removed-products-modal-container').on('hidden.bs.modal', function () {
            if (!confirmed) {
                var data = getConfirmData();
                data.action = 'cancel';

                trackEvent('unavailable_products', data);
            }
        });

        if (!$('#cart-btn').length ) {
            $('.navbar .dropdown-menu').css('margin-top', '0px');
        }

		@if (isset($post) && isset($post['payment_method']) && $post['payment_method'] == 'Tarjeta de crédito')
        	$('*[name="payment_method"]').trigger('change');
		@endif

        $('.delivery_time').val(selected);

        $('#send-button').on('click', function () {
            var phoneNumber = $('input[name="phone"]').val();

            if (phoneValidated) {
                return;
            }

            if (phoneNumber.length !== 10) {
                setValidationError('El número de celular no es valido');
                return;
            }
            setValidationError(false);
            askValidation(phoneNumber, function (error) {
                if (error) {
                    errorValidation(error);
				} else {
                    successValidation();
				}
			});
        });

        toggleRegisterButtons(phoneValidated);
    });

    function successValidation() {
		toggleRegisterButtons(true);
		phoneValidated = true;
	}

    function errorValidation(error) {
        if (error instanceof FormValidationError) {
            setValidationError(error.getErrorDetails());
        } else {
            setValidationError(error.message);
        }
	}

    /**
     * @param {boolean} disabled
     */
    function toggleRegisterButtons(disabled) {
        $('.validate-button')[disabled ? 'addClass' : 'removeClass']('disabled');
        $('.fb-signin, .login-button, input[name="phone"], #send-button')
            .prop('disabled', disabled);
    }

    /**
     * @param {string} message
     */
    function setValidationError(message) {
        var group = $('.form-group-validate');

        if (message) {
            group.addClass('has-error');
            group.find('.help-block').text(message);
        } else {
            group.removeClass('has-error');
            group.find('.help-block').text('');
        }
    }
</script>
@stop
