@extends('layout_page')

@section('extra_assets')
    <link rel="stylesheet" href="{{asset_url()}}css/register.css?v={{Cache::get('js_version_number')}}">
@stop

@section('content')
@if (!isset($success))

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div class="header-content">
                    <div class="header-content-inner">
                        @if (isset($post['promo_code']))
                            <h1>REGÍSTRATE Y GANA CRÉDITOS EN MERQUEO</h1>
                            <p>{{ $user_referrer_first_name }} te regaló {{ currency_format(Config::get('app.referred.amount')) }} pesos en créditos para tu primer pedido en Merqueo. Regístrate para recibirlo y empieza a pedir.</p>
                        @else
                            <h1>INICIA TU EXPERIENCIA MERQUEO</h1>
                            <p>Crea tu cuenta y empieza a ahorrar con Merqueo, el supermercado del ahorro con los precios más bajos del mercado.</p>
                        @endif
                        <img class="ilustracionCiudad" src="{{ asset_url() }}img/img.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="device-container">
                    <form method="POST" action="{{ route('frontUser.register') }}" class="register">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="unseen alert alert-danger fb-signin-error text-center"></div>
                                <div class="@if(!Session::has('message')) unseen @endif alert alert-danger form-has-errors">
                                    <ul class="error-list">
                                        @if(Session::has('message')) {{ Session::get('message') }} @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first_name"><span class="pinkLabel">*</span> Nombre</label>
                                    <input type="text" class="form-control onlyLetters required" id="first_name" name="first_name" value="{{ $post['first_name'] or '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="last_name"><span class="pinkLabel">*</span> Apellido</label>
                                    <input type="text" class="form-control onlyLetters required" id="last_name" name="last_name" value="{{ $post['last_name'] or '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="phone"><span class="pinkLabel">*</span> Celular</label>
                                    <div class="input-group validate-group" style="width: 100%;">
                                        <input type="text"
                                               class="form-control onlyNumbers required"
                                               id="phone"
                                               name="phone"
                                               maxlength="10"
                                               {{ empty($phone_validated) ? '' : 'disabled' }}
                                               value="{{ Session::get('phoneValidation.validated') ? Session::get('phoneValidation.number') : (empty($post['phone']) ? '' : $post['phone']) }}">
                                        <span id="send-button" class="input-group-addon validate-button {{ Session::get('phoneValidation.validated') ? 'disabled' : '' }}" style="width: 50%;">
                                            Validar número
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="email"><span class="pinkLabel">*</span> Correo electrónico</label>
                                    <input type="email"
                                           class="form-control required"
                                           id="email"
                                           name="email"
                                           {{ Session::get('facebookData.email') ? 'disabled' : '' }}
                                           value="{{ $post['email'] or '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="password"><span class="pinkLabel">*</span> Contraseña</label>
                                    <input type="password" class="form-control required" id="password" name="password" maxlength="100">
                                </div>
                            </div>
                        </div>

                        <div class="row form-group recaptcha" align="center">
                            <div class="col-xs-12">
                                <div class="g-recaptcha" data-sitekey="6LeEDyEUAAAAAIAPBxmmeTiXqCF8bEWz6B-Esm6y"></div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <div align="center" class="fb-signin-loading unseen"><img src="{{ asset_url() }}/img/loading.gif" /></div>
                            </div>
                        </div>
                        @if (empty($facebook_validated))
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-default facebookBTN fb-signin">
                                        <i class="fa fa-facebook" aria-hidden="true"> </i>&nbsp;&nbsp;&nbsp;&nbsp; Crear cuenta con Facebook
                                    </button>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-default crearCuentaBTN">
                                    CREAR CUENTA
                                </button>
                            </div>
                        </div>
                    </form>
                    <div>
                        <p class="termsAndConditions">Al crear tu cuenta aceptas nuestros <a class="pinkLink" href="{{ web_url() }}/terminos" target="_blank">términos y condiciones</a> y <a class="pinkLink" href="{{ web_url() }}/politicas-de-privacidad" target="_blank">políticas de privacidad.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="productsHorizontal">
    <div class="container productsNoSlider">
        <h4 class="titleProducts">PRODUCTOS QUE ENCONTRARÁS EN MERQUEO</h4>
        <div class="row">
            @if ( !empty($products) && count($products) )
                @foreach ($products as $product)
                    <div class="col-md-4 paddingProduct">
                        <div class="row productBox">
                            <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $product->city_slug, 'store_slug' => $product->store_slug, 'department_slug' => $product->department_slug, 'shelf_slug' => $product->shelf_slug, 'product_slug' => $product->slug]) }}">
                                <div class="col-md-4">
                                    <img class="productImg" src="{{ $product->image_app_url }}" alt="{{ $product->name }}" />
                                </div>
                                <div class="col-md-8">
                                    <p class="textoProducto">{{ $product->name }} <span class="grayDetail">{{ $product->quantity }} {{ $product->unit }}</span></p>
                                    <p style="margin-bottom: 0px;">
                                        @if ( $product->is_best_price )<img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" />@endif <span class="Price">${{ number_format(( empty($product->special_price) ? $product->price : $product->special_price ), 0, '.', ',') }}</span>
                                    </p>
                                    @if(isset($product->pum))
                                        <p class="product-pum" style="font-size: smaller;">
                                                @if($product->type == 'Simple')
                                                <small>{{ $product->pum }}</small>
                                            @elseif($product->type == 'Agrupado')
                                                @foreach(explode('|', $product->pum, 1) as $pum)
                                                    @if($pum)
                                                        <small>{{ mb_substr($pum, 0, 22)  }} ...</small>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </p>
                                    @endif
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div><p class="centeredText"><img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" /> Cientos de productos con el mejor precio garantizado</p></div>
    </div>
    <div class="container carrouselSlider">
        <h4 class="titleProducts">PRODUCTOS QUE ENCONTRARÁS EN MERQUEO</h4>
        <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                @if ( !empty($products) && count($products) )
                    @foreach ($products as $key => $product)
                        <div class="item @if ( $key == 0 ) active @endif">
                            <a href="javascript:;">
                                <div class="row">
                                    <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $product->city_slug, 'store_slug' => $product->store_slug, 'department_slug' => $product->department_slug, 'shelf_slug' => $product->shelf_slug, 'product_slug' => $product->slug]) }}">
                                        <div class="col-phone-slider4">
                                            <img class="productImg" src="{{ $product->image_app_url }}" alt="{{ $product->name }}" />
                                        </div>
                                        <div class="col-phone-slider8">
                                            <p class="textoProducto">{{ $product->name }} <span class="grayDetail">{{ $product->quantity }} {{ $product->unit }}</span></p>
                                            <p>
                                                @if ( $product->is_best_price )<img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" />@endif <span class="Price">${{ number_format(( empty($product->special_price) ? $product->price : $product->special_price ), 0, '.', ',') }}</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
            <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a href="#carousel-example-captions" class="right carousel-control" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div>
            <p class="centeredText"><img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" /> Cientos de productos con el mejor precio garantizado</p>
        </div>
    </div>
</section>
</body>

</html>

<script>
$(document).ready(function() {
    var phoneValidated = {{ Session::get('phoneValidation.validated') ? 'true' : 'false' }};
    $('form.register').on('submit', function(e) {
        if ($('button[type="submit"]', form).hasClass('disabled')) {
            return false;
        }
        $('button[type="submit"], .facebookBTN', this).addClass('disabled');

        e.preventDefault();
        var errors = [];
        var form = this;

        //validar campos
        $('input[type!="hidden"], select', this).each(function() {
            if($(this).val().length == 0 && $(this).is(':visible') && $(this).hasClass('required')) {
                $(this).parent().addClass('has-error');
                if(errors.indexOf("Debes ingresar todos los campos") < 0) {
                    errors.push("Debes ingresar todos los campos");
                }
            }else $(this).parent().removeClass('has-error');
        });

        if($('input[name="password"]', this).length
        && $('input[name="password"]', this).val() != '' && $('input[name="password"]', this).val().length < 6) {
            errors.push("La contraseña debe ser minimo de 6 caracteres");
            $('input[name="password"]').parent().addClass('has-error');
        }

        if($('input[name="phone"]', this).length
        && $('input[name="phone"]', this).val() != '' && $('input[name="phone"]', this).val().length < 10) {
            errors.push("El número celular debe ser mínimo de 10 digitos");
            $('input[name="phone"]').parent().addClass('has-error');
        }

        if(grecaptcha.getResponse() == '') {
            errors.push("Debes marcar la casilla de verificación");
            $('.recaptcha iframe').addClass('element-has-error');
        }else
            $('.recaptcha iframe').removeClass('element-has-error');

        /*if(!$('input[name="terms"]', this).is(':checked')) {
            errors.push("Debes aceptar los términos y condiciones");
        }*/

        if(errors.length) {
            showErrors(errors);
            return false;
        }
        $('.loading').show();
        form.submit();
        return false;
    });

    function showErrors(errors) {
        var errors_list = '';
        for(var i in errors) {
            errors_list += '<li>' + errors[i] + '</li>';
        }
        $('.form-has-errors .error-list').html(errors_list);
        $('.form-has-errors').fadeIn();
        $('button[type="submit"], .facebookBTN').removeClass('disabled');
        $('body').scrollTop(0);
    }

    $('#send-button').on('click', function () {
        var phoneNumber = $('input[name="phone"]').val();

        if (phoneValidated) {
            return;
        }

        if (phoneNumber.length !== 10) {
            showErrors(['El número de celular no es valido']);
            return;
        }

        askValidation(phoneNumber, function (error) {
            if (error) {
                if (error instanceof FormValidationError) {
                    showErrors([error.getErrorDetails()]);
                } else {
                    showErrors([error.message]);
                }

                return;
            }

            toggleRegisterButtons(true);
            phoneValidated = true;
        });
    });

    function toggleRegisterButtons(disabled) {
        $('.validate-button')[disabled ? 'addClass' : 'removeClass']('disabled');
        $('.login-button, input[name="phone"], #send-button')
            .prop('disabled', disabled);
    }

    $('#captcha_CaptchaImageDiv').find('.LBD_CaptchaImageDiv').next().remove();

    $(".carousel").nivoSlider({
      effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
      slices: 2, // For slice animations
      boxCols: 8, // For box animations
      boxRows: 4, // For box animations
      animSpeed: 500, // Slide transition speed
      pauseTime: 6000, // How long each slide will show
      startSlide: 0, // Set starting Slide (0 index)
      directionNav: false, // Next & Prev navigation
      directionNavHide: true, // Only show on hover
      controlNav: true, // 1,2,3... navigation
      controlNavThumbs: false, // Use thumbnails for Control Nav
      controlNavThumbsFromRel: false, // Use image rel for thumbs
      controlNavThumbsSearch: '', // Replace this with...
      controlNavThumbsReplace: '', // ...this in thumb Image src
      keyboardNav: true, // Use left & right arrows
      pauseOnHover: true, // Stop animation while hovering
      manualAdvance: false, // Force manual transitions
      captionOpacity: 0, // Universal caption opacity
      prevText: 'Prev', // Prev directionNav text
      nextText: 'Next', // Next directionNav text
      beforeChange: function(){}, // Triggers before a slide transition
      afterChange: function(){}, // Triggers after a slide transition
      slideshowEnd: function(){}, // Triggers after all slides have been shown
      lastSlide: function(){}, // Triggers when last slide is shown
      afterLoad: function(){} // Triggers when slider has loaded
    });
});
</script>

@else

    <div class="container thankyoupage-container">
        <div class="row form-group">
            <div class="col-sm-6">
                <h1>
                    TU CUENTA FUE CREADA CON ÉXITO
                </h1>
                <br>
                @if (!$referred['success'] && !$referred['limit'] && !$referred['error'])
                    <p>Hola {{ $user->first_name }}, tu cuenta fue creada con éxito, gracias por registarte en <span style="color: #9a114c">merqueo.com</span>.</p>
                @endif
                @if ($referred['success'])
                    <p>Hola {{ $user->first_name }}, tu código de referido fue procesado con éxito, tienes <b>{{ currency_format($referred['amount']) }}</b> en créditos en tu cuenta.</p>
                @endif
                @if ($referred['limit'])
                    <p>Hola {{ $user->first_name }}, te informamos que el código referido que ingresaste <font color="red">no es válido</font> por que ha alcanzado el limite permitido de referidos por cuenta.</p>
                @endif
                @if ($referred['error'])
                    <p>Hola {{ $user->first_name }}, te informamos que el código de referido que ingresaste <font color="red">no es válido</font>, por favor verifícalo y vuelve a intentarlo desde tu cuenta.</p>
                @endif
                <br>
                @if ( $user_agent['mobile_browser'] || $user_agent['tablet_browser'] )
                    @if ( $user_agent['control_mobile'] )
                        <a href="{{ Config::get('app.ios_url') }}" class="btn btn-default crearCuentaBTN">Descargar aplicación</a>
                    @else
                        <a href="{{ Config::get('app.android_url') }}" class="btn btn-default crearCuentaBTN">Descargar aplicación</a>
                    @endif
                @else
                    <button type="submit" class="btn btn-default crearCuentaBTN" onclick="window.location = '{{ web_url() }}'">IR A COMPRAR AHORA</button>
                @endif
            </div>
            <div class="col-sm-6">
                <img src="{{ asset_url() }}img/mercado.png" alt="merqueo.com" class="img-responsive">
            </div>
        </div>
        <br>
        <br>
    </div>
    <section class="productsHorizontal">
        <div class="container productsNoSlider">
        <h4 class="titleProducts">PRODUCTOS QUE ENCONTRARÁS EN MERQUEO</h4>
            <div class="row">
                @if ( !empty($products) && count($products) )
                    @foreach ($products as $key => $product)
                        <div class="col-md-4 paddingProduct">
                            <div class="row productBox">
                                <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $product->city_slug, 'store_slug' => $product->store_slug, 'department_slug' => $product->department_slug, 'shelf_slug' => $product->shelf_slug, 'product_slug' => $product->slug]) }}">
                                    <div class="col-md-4">
                                        <img class="productImg" src="{{ $product->image_app_url }}" alt="{{ $product->name }}" />
                                    </div>
                                    <div class="col-md-8">
                                        <p class="textoProducto">{{ $product->name }} <span class="grayDetail">{{ $product->quantity }} {{ $product->unit }}</span></p>
                                        <p>
                                            @if ( $product->is_best_price )<img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" />@endif <span class="Price">${{ number_format(( empty($product->special_price) ? $product->price : $product->special_price ), 0, '.', ',') }}</span>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="container carrouselSlider">
        <h4 class="titleProducts">PRODUCTOS QUE ENCONTRARÁS EN MERQUEO</h4>
            <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    @if ( !empty($products) && count($products) )
                        @foreach ($products as $key => $product)
                            <div class="item @if ( $key == 0 ) active @endif">
                                <a href="#">
                                    <div class="row">
                                        <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $product->city_slug, 'store_slug' => $product->store_slug, 'department_slug' => $product->department_slug, 'shelf_slug' => $product->shelf_slug, 'product_slug' => $product->slug]) }}">
                                            <div class="col-phone-slider4">
                                                <img class="productImg" src="{{ $product->image_app_url }}" alt="{{ $product->name }}" />
                                            </div>
                                            <div class="col-phone-slider8">
                                                <p class="textoProducto">{{ $product->name }} <span class="grayDetail">{{ $product->quantity }} {{ $product->unit }}</span></p>
                                                <p>
                                                    @if ( $product->is_best_price )<img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" />@endif <span class="Price">${{ number_format(( empty($product->special_price) ? $product->price : $product->special_price ), 0, '.', ',') }}</span>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
            <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a href="#carousel-example-captions" class="right carousel-control" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div><p class="centeredText"><img src="{{ asset_url() }}img/Icon Estrella.svg" alt="" /> Cientos de productos con el mejor precio garantizado</p></div>
        </div>
    </section>
@endif
@include('common.account_kit_validation')
@stop