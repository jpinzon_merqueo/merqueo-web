@extends('layout_page')

@section('content')
    <style>
        .new-terms-styles {
            font-size: 18px !important;
            font-family: Muli, Helvetica, Arial, sans-serif;
        }

    </style>
    <div class="container">
        <div class="row">
            <div class="page_content page_fondo col-md-12 col-xs-12">
                <div class="content-page new-terms-styles">
                    <br><br>
                    {{ $document['content'] }}
                </div>
            </div>
        </div>
    </div>
@endsection