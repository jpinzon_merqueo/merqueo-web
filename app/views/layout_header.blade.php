<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="robots" @if(Config::get('app.debug'))content="noindex,nofollow" @else content="index,follow" @endif>
    <title>@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')</title>
    <meta name="description" content="@yield('description', 'Mercado a domicilio en Bogotá - Merqueo.com')">
    <meta name="keywords"
          content="@yield('keywords', 'supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express')">
    <meta name="author" content="Merqueo">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, target-densitydpi=medium-dpi, user-scalable=0"/>

    <meta property="fb:app_id" content="{{ Config::get("app.facebook_app_id") }}"/>
    <meta property="og:locale" content="es_ES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')"/>
    <meta property="og:description"
          content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:site_name" content="Merqueo.com"/>
    <meta property="og:image" content="{{ asset_url() }}img/fb_logo.jpg"/>

    <link href='//fonts.googleapis.com/css?family=Crete+Round:400,500,400italic|Open+Sans:300,400,500,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,100,900' rel='stylesheet' type='text/css'>
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Noto+Sans:400,500,700,400italic' rel='stylesheet' type='text/css'>
    <link href="{{ asset_url() }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset_url() }}/css/style.css" rel="stylesheet">
    <link href="{{ asset_url() }}/css/base.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css?v={{Cache::get('js_version_number')}}">
    <link href="{{ asset_url() }}/css/layout-footer.css" rel="stylesheet">

    @include('common/meta_icons')

    <link rel="canonical" href="{{ Request::url() }}">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
    <script>
        // Javascript Vars
        var fb_app_id = '{{ Config::get("app.facebook_app_id") }}';
        var web_url = '{{ web_url() }}';
        var google_api_key = '{{ Config::get("app.google_api_key") }}';
    </script>

    @include('common/analytics')

    <script type="text/javascript" src="{{ asset_url() }}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.lazyload.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/headroom.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/geocode.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/general.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
</head>

<body class="layout_content">
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top"
     style="background-color: white; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2); border: none">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <a href="{{ web_url() }}" title="Merqueo.com">
                    <img src="{{ asset_url() }}img/Logo.svg" style="width: 160px;" alt="Merqueo.com">
                </a>
            </div>
        </div>
    </div>
</nav>
<div class="col-md-12 col-xs-12">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>

@include('login_modal')

<script src="https://use.fontawesome.com/0ced85153d.js"></script>
<script>
    $(document).ready(function () {
        $('.fancybox').fancybox({
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });

        var ancho = $(window).width();
        var anchocont = $('.navbar .col-md-12 .contentmenu_home').width();
        var anchodrop = $('.navbar .col-md-12 .dropdown-menuhome').width();
        var valuecal = (anchocont - anchodrop) + 12;
        $('.navbar .col-md-12 .dropdown-menuhome').css('margin-left', valuecal);

        $(window).resize(function () {
            var ancho = $(window).width();
            var anchocont = $('.navbar .col-md-12 .contentmenu_home').width();
            var anchodrop = $('.navbar .col-md-12 .dropdown-menuhome').width();
            var valuecal = (anchocont - anchodrop) + 12;
            $('.navbar .col-md-12 .dropdown-menuhome').css('margin-left', valuecal);
        });
    });
</script>
</body>
</html>
