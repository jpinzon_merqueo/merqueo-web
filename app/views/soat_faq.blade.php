<!doctype html>
<html lang="es">
<head>
    <base href="/">
    <meta charset="utf-8">
    <title>El supermercado del ahorro. Entrega a domicilio - Merqueo.com</title>
    <meta name="description"
          content="Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.">
    <meta name="keywords"
          content="supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express">
    <meta name="author" content="Merqueo">
    <meta name="viewport" content="width=824, initial-scale=0"/>
    <meta name="apple-itunes-app" content="app-id=1080127941">
    <meta name="google-play-app" content="app-id=com.merqueo">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset_url() }}img/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
    <meta property="og:locale" content="es_ES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="Merqueo.com"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
    @if (Request::segment(1) == 'registro')
        <meta property="og:title"
              content="@if($metadata['code_referred_title']) {{ $metadata['code_referred_title'] }} @else {{ Config::get('app.referred.share_title') }} @endif"/>
        <meta property="og:description"
              content="@if($metadata['code_referred_description']) {{ $metadata['code_referred_description'] }} @else {{ Config::get('app.referred.share_description') }} @endif"/>
        <meta property="og:image" content="{{ Config::get('app.referred.share_image_url') }}"/>
    @else
        <meta property="og:title" content="@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')"/>
        <meta property="og:description"
              content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios, pide a los principales supermercados online de Colombia y uno de nuestros compradores lo seleccionara cuidadosamente por ti. Entregamos a domicilio en menos de 2 horas. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')"/>
        <meta property="og:image" content="{{ asset_url() }}img/fb_logo.jpg"/>
    @endif

    <link rel="shortcut icon" href="{{ web_url() }}/favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css?v={{Cache::get('js_version_number')}}">
    <link rel="stylesheet" href="{{asset_url()}}/css/bootstrap.min.css">

    <link rel="android-touch-icon" href="{{asset_url()}}img/icon/favicon-96x96.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset_url()}}img/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset_url()}}img/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset_url()}}img/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset_url()}}img/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset_url()}}img/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset_url()}}img/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset_url()}}img/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset_url()}}img/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset_url()}}img/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset_url()}}img/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset_url()}}img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset_url()}}img/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset_url()}}img/icon/favicon-16x16.png">
    <link rel="manifest" href="{{asset_url()}}img/icon/manifest.json">

    <link rel="canonical" href="{{ Request::url() }}">

    <link rel="stylesheet" href="{{asset_url()}}components/services.bundle.css">
    <script type="text/javascript" src="{{asset_url()}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{asset_url()}}js/general.js"></script>
</head>

<body>
<header class="hero-header">
    <div class="hero-info">
        <div class="hero-top">
            <div class="hero-logos">
                <img src="/assets/img/superintendencia.png" alt="Superintendencia" class="logo-vertical">
                <img src="/assets/img/logo-seguros.png" alt="Seguros mundial">
                <img src="/assets/img/merqueocom.png" alt="merqueo">
            </div>
            <div class="hero-pdf">
                <a href="https://s3-us-west-1.amazonaws.com/merqueo/downloads/soat_info.pdf" target="_blank" class="btn-hero-pdf">
                    <img src="/assets/img/pdf.png" alt="pdf" style="margin-right: 5px">
                    Descarga tu PDF
                </a>
            </div>
        </div>
    </div>
    <h1 class="hero-title">Encuentra toda la información acerca del SOAT aquí.</h1>
    <div class="container">
        <img class="hero-traffic" src="/assets/img/traffic.svg" alt="Traffic">
    </div>
</header>
<main class="container">
    <div class="row" style="margin-top: 20px; margin-bottom: 60px">
        <div class="col-xs-12">
            <div class="panel-group mq-accordion" id="accordion" role="tablist">
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#One">
                                <span>¿QUÉ ES?</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="One" data-icon="One">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="One" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <p>
                                El Seguro Obligatorio de Accidentes de Tránsito - SOAT es un instrumento de protección
                                para las víctimas de accidentes de tránsito, ya sean peatones, conductores o pasajeros,
                                independiente de quién fue responsable en el evento. El no portar el SOAT vigente
                                ocasiona una multa de 30 SMDLV equivalentes a $781.242 pesos.<br><br>
                                La póliza SOAT es un seguro 100% regulado y uniforme que tiene las mismas condiciones y
                                coberturas sin importar la compañía de seguros que lo oferte ya que es un seguro
                                obligatorio, estas condiciones están definidas por medio de leyes y normas que
                                reglamentan los procesos de reclamo y pago en caso de accidentes de tránsito.<br><br>
                                Para apoyar las gestiones de verificación que realiza la autoridad de transito el
                                formato de papelería igualmente es uniforme, se diferencia de una compañía a otra por el
                                logo de aquella que lo expidió. <br><br>
                                La Superintendencia Financiera es la entidad que define la tarifa para cada una de las
                                categorías de vehículos. En la actualidad las tarifas son diferenciales para 35
                                categorías de vehículos, según uso, cilindraje, capacidad de carga, y/o edad.<br>
                                El SOAT debe ser expedido mínimo con un año de cobertura, a excepción de los vehículos
                                de extranjeros que ingresen al país, vehículos de puerto a concesionario y vehículos
                                clásicos o antiguos. <br><br>
                                Cuando se presente un accidente de tránsito en el que haya heridos o muertos, será
                                necesaria la intervención de la autoridad de tránsito. Por tratarse de un trauma que
                                pueda comprometer la integridad y vida de las personas, las víctimas deben ser trasladas
                                al centro de salud más cercano; este traslado debe efectuarse preferiblemente en
                                ambulancia. Todos los centros de salud del país tienen la obligación de atender a las
                                víctimas de accidentes de tránsito sin autorización previa de una compañía aseguradora.
                                <br><br>
                                Los ocupantes de cada vehículo serán atendidos con cargo a la póliza de vehículo en
                                donde viajaban. En caso de que la víctima no sea un ocupante del vehículo, cualquiera de
                                las pólizas aplica para su atención. Si uno de los vehículos no tiene póliza, es falsa o
                                está vencida, el FOSYGA asume los costos pero luego recobra a quien aparezca como
                                propietario del vehículo. <br><br>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Two">
                                <span>¿QUÉ CUBRE?</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Two">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Two" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <p>
                                El SOAT es un seguro obligatorio para cualquier vehículo que transite por el territorio
                                nacional. Se incluyen los vehículos extranjeros que circulen por las carreteras del país
                                Te garantiza una prestación de servicios para conductores, pasajeros o peatones que se
                                vean afectados en caso de sufrir algún accidente.
                            </p>
                            <ul>
                                <li>
                                    Si tú, tus acompañantes o un peatón mueren como producto de un accidente de
                                    tránsito,
                                    los beneficiarios reciben una indemnización por 750
                                    salarios mínimos diarios legales vigentes. <br><br>
                                </li>
                                <li>
                                    Las personas involucradas en el accidente reciben atención médica por un valor hasta
                                    de 800 salarios mínimos diarios legales vigentes. <br><br>
                                </li>
                                <li>
                                    Si quedas con alguna incapacidad permanente te indemnizamos hasta con 180 salarios
                                    mínimos diarios legales vigentes. <br><br>
                                </li>
                                <li>
                                    Cubrimos los gastos de transporte desde el sitio del accidente hasta un centro
                                    médico, hasta por 10 salarios mínimos diarios legales vigentes. <br><br>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Three">
                                <span>¿CÓMO HACER UNA RECLAMACIÓN?</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Three">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Three" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <p>En las instituciones prestadoras de servicios de salud del país están obligadas a atender
                                a las víctimas de accidentes de tránsito. En SOAT no se autoriza la atención o
                                prestación de servicios por parte de la compañía aseguradora. <br><br>

                                Por disposición <b>legal la víctima debe ser direccionada a la institución más cercana,
                                    considerando su capacidad de resolución.</b> <br><br>

                                A continuación están los formularios que se deben utilizar de acuerdo con el amparo a
                                reclamar, igualmente los requisitos para la presentación de la reclamación ante la
                                Compañía de Seguros:<br>
                                Formulario único de reclamación de indemnizaciones por accidentes de tránsito y eventos
                                catastróficos (eventos terroristas, catástrofes naturales y otros eventos aprobados por
                                el cnsss (FURPEN)<br><br>
                            </p>
                            <ul>
                                <li>
                                    Formulario único de reclamación de gastos de transporte y movilización de víctimas
                                    (FURTRAN)<br><br>
                                </li>
                                <li>
                                    Certificación del censo de víctimas-eventos catastróficos (FURCEN)<br><br>
                                </li>
                                <li>
                                    Formulario único de reclamación de las instituciones prestadoras de servicios de
                                    salud
                                    (FURIPS)<br><br>
                                </li>
                                <li>
                                    Descargar requisitos SOAT normatividad vigente<br><br>
                                </li>
                                <li>
                                    Descargar requisitos SOAT desde 18 de enero de 2007 hasta 10 de enero de
                                    2012<br><br>
                                </li>
                                <li>
                                    Ver las condiciones generales de tu SOAT<br><br>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab" style="border-bottom: solid 1px #ea1a58">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Six">
                                <span>TARIFAS</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Six">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Six" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <img src="{{ asset_url() }}img/tarifas.jpg" alt="tarifas" style="padding-bottom: 50px; padding-top: 50px">
                            <p> El Congreso de la República, la Presidencia de la Republica y la Superintendencia
                                Financiera de Colombia son los entes reguladores. Su tarifa será actualizada anualmente
                                de acuerdo con el aumento del salario mínimo legal vigente.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab" style="border-bottom: solid 1px #ea1a58">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Five">
                                <span>CONSEJOS</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Five">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Five" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <ul>
                                <li>
                                    Guarda una copia impresa de tu SOAT Digital, en caso de no tener un celular o tablet
                                    disponible cuando sea solicitado.
                                    <br><br>
                                </li>
                                <li>
                                    Solicita copia del Informe Policial de Accidente de Tránsito; documento con el que
                                    se acredita la ocurrencia del accidente.
                                    <br><br>
                                </li>
                                <li>
                                    En la cobertura de gastos médicos, la reclamación ante la aseguradora la debe
                                    adelantar el hospital y no el paciente ni sus beneficiarios.
                                    <br><br>
                                </li>
                                <li>
                                    La reclamación de las coberturas de incapacidad permanente o muerte, las puedes
                                    realizar directamente ante la aseguradora o el FOSYGA (Fondo
                                    de Solidaridad y Garantía).
                                    <br><br>
                                </li>
                                <li>
                                    Denuncia ante la Superintendencia Nacional de Salud aquellos hospitales que se
                                    nieguen a prestar servicios de salud a víctimas de accidentes de
                                    tránsito.
                                    <br><br>
                                </li>
                                <li>
                                    Las pólizas del SOAT no son cancelables ni reembolsables.
                                    <br><br>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab" style="border-bottom: solid 1px #ea1a58">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Seven">
                                <span>VIGENCIA</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Seven">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Seven" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <p>La vigencia del SOAT de todo vehículo nacional es de un año. Se contemplan dos únicas
                                excepciones:</p>
                            <ul>
                                <li>
                                    Vehículos que circulen por las zonas fronterizas y a los vehículos importados que se
                                    desplacen del puerto a los concesionarios para su venta al
                                    público, para los cuales su vigencia es por mensualidades
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab" style="border-bottom: solid 1px #ea1a58">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Eight">
                                <span>¿EL SOAT DIGITAL TIENE LA MISMA VALIDEZ ANTE AUTORIDADES DE TRÁNSITO?</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Eight">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Eight" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">

                            <p> Sí, según la Ley 527 de 1999 que regula los mensajes de datos, el comercio electrónico y
                                las firmas digitales, las pólizas de seguro enviadas por correo electrónico tienen los
                                mismos efectos jurídicos que las pólizas expedidas en papel membretado por la compañía
                                de seguros (Concepto 2011089903-001 del 15 de diciembre de 2011 Superintendencia
                                Financiera de Colombia). Por lo tanto, la póliza adquirida en forma digital tiene los
                                mismos efectos jurídicos que la que se adquiere en los formatos tradicionales.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab" style="border-bottom: solid 1px #ea1a58">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Nine">
                                <span>¿QUÉ DEBO HACER SI EL PAGO DE MI SOAT FUE DEBITADO PERO LA PÓLIZA NO HA SIDO ENVIADA A MI CORREO ELECTRÓNICO? </span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Nine">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Nine" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">

                            <p> Para poder ayudarte, envíanos un correo a servicioalcliente@merqueo.com informándonos tu
                                caso.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-default">
                    <div class="panel-heading" role="tab" style="border-bottom: solid 1px #ea1a58">
                        <h4 class="panel-title">
                            <a class="trigger-accordion" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#Four">
                                <span>CRÉDITOS MERQUEO</span>
                                <div class="collapse-icon">
                                    <i class="material-icons" data-icon="Four">add</i>
                                </div>
                            </a>
                        </h4>
                    </div>
                    <div id="Four" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <p>Aplica para pedidos mínimo de $50.000. Válido para los 45 días siguientes a la fecha de
                                redención. No acumulable con otras promociones, créditos o descuentos.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    $(document).ready(function () {
        function triggerAccordion() {
            var $item = $('.panel-collapse');
            $.each($item, function (key, val) {
                var id = $(this).attr('id');
                if ($(this).hasClass('in')) {
                    var $elem = $('.material-icons[data-icon=' + id +']');
                    console.log($elem);
                    $elem.html('remove');
                } else {
                    var $elem = $('.material-icons[data-icon=' + id +']');
                    $elem.html('add');
                }
            });
        }

        var $item = $('#accordion');
        $item.on('shown.bs.collapse', function () {
            triggerAccordion();
        });

        $item.on('hidden.bs.collapse', function () {
            triggerAccordion();
        });
    });
</script>
</body>

</html>
