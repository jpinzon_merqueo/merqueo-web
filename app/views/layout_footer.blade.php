<footer class="col-md-12 android-footer mega-footer mq-new-footer">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="col-md-12 logo_text_footer">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <a href="{{ web_url() }}">
                    <img src="{{ asset_url() }}img/Logo.svg" class="logo-footer col-md-1"
                         alt="Logo Merqueo">
                </a>
                @foreach($footer['cities'] as $city)
                    <h5>
                        <a href="{{ web_url().'/'.$city->slug }}">{{ $city->city }}</a>
                    </h5>
                @endforeach
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="col-md-12 elemts_form">
            <div class="col-md-6">
                <div class="col-md-12 text_desx">
                    <p><strong>REGÍSTRATE A NUESTRO NEWSLETTER</strong></p>
                    <div class="col-md-12 newsletter">
                        <form class="form-nl">
                            <div class="alert alert-success unseen col-md-9"></div>
                            <div class="alert alert-danger unseen col-md-9"></div>
                            <div class="col-md-9">
                                <input type="email" name="email-newsletter" id="email-newsletter"
                                       placeholder="Escribe tu correo electrónico" class="col-md-12">
                            </div>
                            <div class="col-md-3">
                                <button type="button" id="newsletter-btn">Suscribirme</button>
                            </div>
                        </form>
                    </div>
                    <br>
                    <p style="padding-top: 30px; padding-bottom: 10px;">
                        <br>
                        <strong>¿NECESITAS AYUDA?</strong> <br><br>
                        <a href="#" class="channels-contact-btn" id="channels-contact-btn" role="button"
                           style="padding: 7px 20px; display: inline-block; color: white; text-decoration: none"><img
                                    src="{{asset_url()}}img/iconoBotonContacto.png" alt="" width="25px">&nbsp;CONTÁCTANOS</a>
                    </p>
                </div>
            </div>
            <div class="space_footer col-md-1"></div>
            <div class="col-md-5 contpagos">
                <div class="col-md-12 contpagos">
                    <div class="col-md-6 applinks">
                        <h5 class="col-md-12">Descarga la App</h5>
                        <a href="{{ Config::get('app.ios_url') }}" target="_blank"><img
                                    src="{{ asset_url() }}img/appStore.png" alt="apple"></a>
                        <a href="{{ Config::get('app.android_url') }}" target="_blank"><img
                                    src="{{ asset_url() }}img/androidStore.png" alt="android"></a>
                    </div>
                    <div class="col-md-6 forms_pag">
                        <p></p>
                        <h5>Formas de Pago</h5>
                        <p></p>
                        <div><img src="{{ asset_url() }}img/payment_methods_sodexo.png" alt="Metodos de pago"
                                  class="col-md-12"></div>
                        <p></p>
                        <p style="font-weight:bold; color: white">Pago contra entrega</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 inf_padding">
            <div class="col-md-6 categorialist">
                @if (isset($footer['links']['store']))
                    @foreach($footer['links']['store'] as $link)
                        <div class="col-md-3 linkscat">
                            <h5><a href="{{ $link->url }}">{{ $link->name }}</a></h5>
                            <ul>
                                @foreach($link->sublinks as $sublinks)
                                    <li><a href="{{ $sublinks->url }}">- {{ $sublinks->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="space_footer col-md-1"></div>
            <div class="col-md-5 redescont">
                <div class="col-md-12 redescont">
                    <div class="col-md-6 linksfijos">
                        <!--<h5><a href="{{ web_url().'/content/legals' }}">Aviso Legal</a></h5>-->
                        <ul>
                            <li>
                                <a href="{{ web_url().'/'}}faq">- Preguntas Frecuentes</a>
                            </li>
                            <li>
                                <a href="{{ web_url().'/'}}terminos">- Términos y Condiciones</a>
                            </li>
                            <li>
                                <a href="{{ web_url().'/'}}politicas-de-privacidad">- Políticas de Privacidad</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 redes">
                        <h5>Síguenos</h5>
                        <a href="{{Config::get('app.facebook_url')}}" target="_blank"><img
                                    src="{{ asset_url() }}img/facebook-white.png" alt="facebook"></a>
                        <a href="{{Config::get('app.twitter_url')}}" target="_blank"><img
                                    src="{{ asset_url() }}img/twitter-white.png" alt="twitter"></a>
                        <a href="{{Config::get('app.instagram_url')}}" target="_blank" style="display: none"><img
                                    src="{{ asset_url() }}img/instagram.png" alt="instagram"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <!-- Channels contact modal -->
    <div class="modal fade" id="modal-channel-contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="padding-top: 5px !important;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" style="padding-top: 50px">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{asset_url()}}img/iconoMercado.png" alt="">
                        </div>
                        <div class="col-md-6">
                            <p>Si quieres ponerte en contacto con nosotros, escríbenos al correo</p>
                            <div class="channels-contact-email">
                                <a href="mailto:{{Config::get('app.admin_email')}}"><strong>{{ Config::get('app.admin_email') }}</strong></a>
                            </div>
                            <p>O síguenos en nuestras redes sociales</p>
                            <div class="channels-contact-social">
                                <a href="{{Config::get('app.instagram_url')}}" target="_blank"><img
                                            src="{{asset_url()}}/img/email_design/social/insta.png" alt=""></a>
                                <a href="{{Config::get('app.twitter_url')}}" target="_blank"><img
                                            src="{{asset_url()}}/img/email_design/social/twitter.png" alt=""></a>
                                <a href="{{Config::get('app.facebook_url')}}" target="_blank"><img
                                            src="{{asset_url()}}/img/email_design/social/face.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function show_modal(action, reference) {
            var ref = reference || 0;
            $('#modal-' + action).modal('show');
        }

        $(document).ready(function () {
            $('#channels-contact-btn').click(function (e) {
                show_modal('channel-contact');
                trackEvent('support_clicked');
                e.preventDefault();
            });
        });
    </script>
</footer>