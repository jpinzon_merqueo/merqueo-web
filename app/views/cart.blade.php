@if (count($cart['stores']))
	@if (strpos( Route::getCurrentRoute()->getPath(), 'checkout') === false)
	<div class="side-cart-top">
		<div class="side-cart-top-inn">
			<div class="row sd-cart-subtit sd-cart-price" style="background-color: #515151">
			    @if(isset($cart['coupon_message']))
	            <!--<p align="right" style="text-align: center!important;">{{ $cart['coupon_message'] }}</b></p>-->
	            @endif
				<p align="right" style="text-align: center!important; font-size: 17px;">MI PEDIDO</b></p>
				<!--<p align="right" class="total-cart">Total a pagar: <b>${{ number_format($cart['total_amount'] + $cart['delivery_amount'] - $cart['discount_amount'], 2, ',', '.') }}</b></p>-->
			</div>
		</div>
	</div>
	@endif
@endif
<div class="contenscroll">
    @if ($cart['discount_amount'])
	<!--<div class="row sd-cart-pro-li">
	    <div class="col-xs-12">
		    <div class="col-xs-9 discount_block">
		        <p class="col-xs-11 pull-left">Has ahorrado ${{ number_format($cart['discount_amount'], 2, ',', '.') }}</p>
		        <i class="fa fa-info-circle right" aria-hidden="true"></i>
		    </div>
		    <div class="col-xs-3 discount_block_close pull-right">
		        <button type="button" class="col-xs-12 btn btn-default close-discount" >Cerrar</button>
		    </div>
	    </div>
	</div>-->
	@endif
	@if (count($cart['stores']))
	  @foreach ($cart['stores'] as $store)
		<div class="side-cart-top contentstoredata">
			<div class="side-cart-top-inn">
				@if($store['is_minimum_reached'] == 0)
					<div class="row sd-cart-subtit alert_store_min error">
						<div class="col-md-9 cont_error pull-left">
							Tienda con pedido mínimo no alcanzado
						</div>
					</div>
				@endif
			</div>
			<div class="contentproductcart">
			@foreach ($store['products'] as $product)
			<div class="row sd-cart-pro-li" data-id="{{ $product['id'] }}">
				<div class="col-md-2 col-sm-2 col-xs-2 sd-ser-no">
					<div class="items_number_ta" data-store="10">
						<h4 class="btn_n_cart btn_sel"><i class="fa fa-caret-up" aria-hidden="true" onclick="modify_cart({{ $product['id'] }}, 0,({{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }} + 1), {{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }}, '{{ $referer }}', event)"></i></h4>
						{{--TODO Se debe poder agregar una cantidad en el "input" que afecte el total de productos agregados--}}
                        <span>{{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }}</span>
						<input id="product-{{ $product['id'] }}" type="hidden" size="2" value="{{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }}" style="width:30px" onchange="modify_cart({{ $product['id'] }}, 0, this.value, {{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }}, '{{ $referer }}', event)">
						<h4 class="btn_m_cart btn_sel"><i class="fa fa-caret-down" aria-hidden="true" onclick="modify_cart({{ $product['id'] }}, 0, ({{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }} - 1), {{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }}, '{{ $referer }}', event)"></i></h4>
					</div>
				</div>
				<div class="col-md-2 col-sm-1 col-xs-1 sd-pro-img">
					<img src="{{ $product['image_small_url'] }}">
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5 sd-pro-detail">
				    {{ $product['name'] }}
			    @if ($product['cart_quantity'])
				    @if ($product['first_order_special_price'])
                        @if (!Session::get('has_orders'))
                        <div class="qty">Precio: ${{ number_format($product['special_price'], 2, ',', '.') }}
                        @else
                        <div class="qty">Precio: ${{ number_format($product['price'], 2, ',', '.') }}
                        @endif
                        (Cant. {{ $product['cart_quantity'] }})
                    @else
                    <div class="qty">Precio: $@if ($product['special_price']){{ number_format($product['special_price'], 2, ',', '.') }}@else{{ number_format($product['price'], 2, ',', '.') }}@endif
                        (Cant. {{ $product['cart_quantity'] }})
                    @endif
                    </div>
                @endif
                    @if ($product['cart_quantity_full_price'])
                    <div class="qty">Precio: ${{ number_format($product['price'], 2, ',', '.') }} (Cant. {{ $product['cart_quantity_full_price'] }})</div>
                    @endif
				@if(isset($product['pum']))
					<label class="qty product-pum">
						@if($product['type'] == 'Simple')
							<small>{{ $product['pum'] }}</small>
						@elseif($product['type'] == 'Agrupado')
							@foreach(explode('|', $product['pum'], 1) as $pum)
								@if($pum)
									<small>{{ mb_substr($pum, 0, 22)  }} ...</small>
								@endif
							@endforeach
						@endif
					</label>
				@endif
		        </div>
				<div class="col-md-2 col-sm-1 col-xs-1 no-padding price_tag pull-right">
				    @if ($product['first_order_special_price'])
                        @if (!Session::get('has_orders'))
                        <span>${{ number_format(($product['special_price'] * $product['cart_quantity']) + ($product['price'] * $product['cart_quantity_full_price']), 2, ',', '.') }}</span>
                        @else
                        <span>${{ number_format($product['price'] * $product['cart_quantity'] + ($product['price'] * $product['cart_quantity_full_price']), 2, ',', '.') }}</span>
                        @endif
                    @else
                    <span>$@if ($product['special_price']){{ number_format(($product['special_price'] * $product['cart_quantity']) + ($product['price'] * $product['cart_quantity_full_price']), 2, ',', '.') }}@else{{ number_format($product['price'] * $product['cart_quantity'], 2, ',', '.') }}@endif</span>
                    @endif
					<div class="table_ico_cerrar" onclick="modify_cart({{ $product['id']}}, 0, 0, {{ $product['cart_quantity'] + $product['cart_quantity_full_price'] }}, '{{ $referer }}', event)";><i class="fa fa-times" aria-hidden="true"></i></div>
				</div>
			</div>
			@endforeach
			</div>
		</div>
	  @endforeach

		<!-- Producto sugerido -->
		@if($suggested_product)
		<div class="suggested-product" style="display:none" data-name="{{ $suggested_product->name }}" data-has-ordered="{{ Session::get('has_orders') }}" data-current-city="{{ Session::get('city_id') }}" data-id="{{ $suggested_product->id }}">
			<div class="row sd-cart-pro-li" data-id="{{ $suggested_product->id }}">
				<div class="col-md-2 col-sm-1 col-xs-1 sd-pro-img">
					<img src="{{ $suggested_product->image_medium_url }}" />
				</div>
				<div class="col-md-7 col-sm-5 col-xs-5 sd-pro-detail">
					<h4>Sugerencia</h4>
					@if($suggested_product->is_best_price)
          <div class="product-best-detail">
            <div id="ico_best-detail">
              <img src="{{ asset_url() }}img/icon_best.png" alt="best-product">
            </div>
            <div id="text_best-detail">
              <h6>Mejor precio garantizado</h6>
            </div>
          </div>
		      @endif
			    {{ $suggested_product->name }}
			    <div class="qty">Precio unitario: $@if ($suggested_product->special_price){{ number_format($suggested_product->special_price, 2, ',', '.') }}@else{{ number_format($suggested_product->price, 2, ',', '.') }}@endif
		      </div>
					@if(isset($suggested_product->pum))
						<label class="qty product-pum">
							@if($suggested_product->type == 'Simple')
								<small>{{ $suggested_product->pum }}</small>
							@elseif($suggested_product->type == 'Agrupado')
								@foreach(explode('|', $suggested_product->pum, 1) as $pum)
									@if($pum)
										<small>{{ mb_substr($pum, 0, 22)  }} ...</small>
									@endif
								@endforeach
							@endif
						</label>
					@endif
				</div>
				<div class="col-md-2 col-sm-1 col-xs-1 no-padding price_tag pull-right">
					<button type="button" name="button" class="btn btn-sm btn-default" data-suggested onclick="modify_cart({{ $suggested_product->id}}, 0, 1, 0, 'suggestedproducts', event)">Agregar</button>
				</div>
			</div>
		</div>
		@endif

      @if (count($cart['stores']))
    	<div class="selectministore_down">
    		<div class="row">
        		<div class="col-md-6 col-sm-6 col-xs-6 text-left"><br>
        			<p>Subtotal</p>
					@if ($referer == 'checkout')
        			<p>Costo domicilio</p>
        			<p>Descuento</p>
					<!--<p class="item_cart_col">Ahorro</p>-->
        			<p>Total este pedido</p>
					@endif
        		</div>
        		<div class="col-md-6 col-sm-6 col-xs-6 text-right"><br>
        			<div class="storeprod_10">
        				<p class="subtotal" >${{ number_format($cart['total_amount'], 2, ',', '.') }}</p>
						@if ($referer == 'checkout')
        				<p class="delivery-amount" data-amount="{{ $cart['delivery_amount'] }}">
							${{ number_format($cart['delivery_amount'], 2, ',', '.') }}
						</p>
        				<p class="discount-amount">@if ($cart['discount_amount'])-${{ number_format($cart['discount_amount'], 2, ',', '.') }}@else $0 @endif</p>
        				<!--<p class="discount-amount item_cart_col">$ {{ number_format($cart['discount_amount'], 2, ',', '.') }}</p>-->
        				<p class="total_store total_item_store_10 sd-store-price">$<span>{{ number_format($cart['total_amount'] + $cart['delivery_amount'] - $cart['discount_amount'], 2, ',', '.') }}</span></p>
						@endif
        			</div>
        		</div>
    		</div>
    		<div class="len_store"></div>
    	</div>
    	@endif
    @else
    <br><p class="muted text-center">No tienes productos en tu carrito.</p>
    @endif
</div>

@if($cart['is_minimum_reached'] == 1)
	<div class="row ck-now text-center shadow-cart">
		<br>
		<hr>
		<a href="{{ route('frontCheckout.index') }}" id="checkout-button">
			<button class="col-md-10 col-sm-10 col-xs-10 btn btn-block" type="button">
				<p class="col-md-9 text_pedi">HACER PEDIDO </p>
				<p class="col-md-2 number_pedi">$ <span>{{ number_format($cart['total_amount'] + $cart['delivery_amount'] - $cart['discount_amount'], 2, ',', '.') }}</span></p>
			</button>
		</a>
	</div>
@else
	@if (count($cart['stores']))
	<div class="row ck-now text-center shadow-cart ">
		<strong>Pedido mínimo no alcanzado </strong>
		<br>
		<hr>
		<button class="col-md-10 col-sm-10 col-xs-10 btn btn-block disabled" type="button">
			<p class="col-md-9 text_pedi" style="padding-left: 8%">SIGUIENTE </p>
			<p class="col-md-2 number_pedi">$ <span>{{ number_format($cart['total_amount'] + $cart['delivery_amount'] - $cart['discount_amount'], 2, ',', '.') }}</span></p>
		</button>
	</div>
	<br>
	@endif
@endif
