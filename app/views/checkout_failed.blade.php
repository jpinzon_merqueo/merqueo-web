@extends('layout_page')

@section('extra_assets')
    <title>Tu pedido</title>
    <meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,800" rel="stylesheet">
    <link href="{{ asset_url() }}css/order-success.css" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-7">
                    <div class="miPedido_table">
                        <p class="miPedidoTitle">Mi Pedido <br> <span>#{{ $order_data['reference'] }}</span></p>
                        @if(Session::has('message') && !empty(Session::get('message')))
                            <div class="alert alert-info alert-dismissable">
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td scope="row" class="table-left">Estado</td>
                                    <td class="table-right rechazado">Rechazado</td>
                                </tr>
                                <tr>
                                    <td scope="row" class="table-left">Productos pedidos</td>
                                    <td class="table-right">{{ $order_data['products'] }}</td>
                                </tr>
                                <tr>
                                    <td scope="row" class="table-left">Dirección</td>
                                    <td class="table-right">{{ $order_data['address'] }}</td>
                                </tr>
                                <tr>
                                    <td scope="row" class="table-left">Método de pago</td>
                                    <td class="table-right">{{ $order_data['payment_method'] }}</td>
                                </tr>
                                <tr>
                                    <td scope="row" class="table-left totalText">Total</td>
                                    <td class="table-right totalText">$ {{ number_format($order_data['total'], 0, ',', '.') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <p class="text-center"><br><button onclick="window.location = '{{ web_url() }}'" class="btn-tags-change">Ir al inicio</button></p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="imgPedidoRecibido">
                        <img src="{{ asset_url() }}img/pedido-rechazado.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
