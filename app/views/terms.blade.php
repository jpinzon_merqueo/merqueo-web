@extends('layout_page')

@section('content')
    <style>
        .new-terms-styles {
            font-size: 18px !important;
            font-family: Muli, Helvetica, Arial, sans-serif;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="page_content page_fondo col-md-12 col-xs-12">
                <div class="content-page new-terms-styles">
                    <br><br>
                    <h1>T&eacute;rminos y Condiciones</h1><br>
                    <p align="justify">Los presentes Términos y Condiciones rigen el uso que toda persona natural o 
                    jurídica, o representante en cualquier forma de los mismos, hace de la Plataforma 
                    y de cualquier Servicio que sea de propiedad o esté controlado por MERQUEO 
                    S.A.S. (en adelante “MERQUEO”). Este documento contiene información legal que le recomendamos leer 
                    completamente en conjunto con la Política de Privacidad. Por medio de la 
                    aprobación de los presentes Términos y Condiciones, se entiende que el Usuario 
                    los ha leído y aceptado, en todas sus partes, y entiende que estos le son 
                    legalmente vinculantes y obligatorios. Por tanto, acepta las condiciones de 
                    utilización y aprovechamiento de la Plataforma, Contenido y los Servicios. En caso 
                    contrario, el Usuario deberá abstenerse de acceder a la Plataforma y Servicios, ya 
                    sea directa o indirectamente, y de utilizar cualquier información o servicio provisto 
                    por la misma.
                    </p>
                    <br>
                    <ol style="padding-left: 15px">
                        <li><b>Aceptación de Términos y Condiciones</b></li>
                    </ol>
                    <p align="justify">
                        Mediante la creación de un Perfil y/o la utilización en cualquier forma de los 
                        Servicios y Plataforma, el Usuario acepta todos los Términos de Uso aquí 
                        contenidos y la Política de Privacidad. Asimismo, se entiende que acepta todas las 
                        demás reglas de operación, políticas y procedimientos que puedan ser publicados 
                        por MERQUEO en la Plataforma, cada uno de los cuales se incorpora por 
                        referencia. <br><br>
                        Sin perjuicio de lo anterior, algunos servicios que se ofrecen en la Plataforma 
                        pueden estar sujetos a términos y condiciones adicionales. En ese caso, el uso 
                        que el Usuario haga de dichos servicios estará sujeto, además, a los referidos 
                        términos y condiciones adicionales, que se incorporan a los presentes Términos 
                        de Uso por medio de esta referencia. En caso que el Usuario no los acepte, 
                        deberá abstenerse de utilizar en cualquier forma dichos servicios y/o Plataforma.
                    </p>
                    <br>
                    <ol style="padding-left: 15px" start="2">
                        <li><b>Definiciones.</b></li>
                    </ol>
                    <P align="justify">
                        Los términos cuya primera letra figura en mayúscula (salvo cuando se deba exclusivamente a que 
                        inician una oración o se trate de un nombre propio), tienen el significado que se les asigna a 
                        continuación, o en la Política de Privacidad: <br><br>

                        <b>Beneficio(s):</b> significa las promociones y descuentos que se otorgan al Usuario con el 
                        objetivo 
                        de brindar mejores precios y mayores beneficios a estos.
                        Comercio(s): significa comercios minoristas seleccionados a los cuales el Usuario podrá adquirir
                        Productos. <br><br>

                        <b>Contenido:</b> significa todo tipo de información, ya sea gráfica o escrita, imágenes, audio 
                        y 
                        video, incluyendo pero no limitándose a la ubicación, anuncios, comentarios, noticias, datos, 
                        guiones, gráficos, dibujos o imágenes, textos, diseño, esquemas, mapas y características 
                        interactivas presentadas por MERQUEO en la Plataforma y en cualquier canal administrado por 
                        MERQUEO (y cualquier software y códigos informáticos subyacentes), así como también programas o 
                        algoritmos computacionales, módulos de programación, manuales de operación, sea que dicho 
                        Contenido es generado, provisto o de cualquier otra forma producido o suministrado por MERQUEO, 
                        los Usuarios o terceros. <br><br>

                        <b>Equipo:</b> significa los teléfonos móviles y smartphones, tablets, computadores y cualquier
                        otro
                        aparato electrónico por el cual se pueda acceder a la Plataforma. <br><br>

                        <b>Material de Usuarios:</b> significa toda clase de fotografías, videos, comentarios, comunicaciones u
                        otros contenidos generado por Usuarios, que hayan sido subidos o cargados a la Plataforma y/o a
                        cualquier canal administrado por MERQUEO. <br><br>

                        <b>MERQUEO:</b> significa MERQUEO S.A.S. Compañía de comercio electrónico la cual mediante
                        medios 
                        tecnológicos y de servicio facilita a los Usuarios un catálogo digital de Productos ofrecidos 
                        para la respectiva compra y el traslado de estos al destino indicado por el Usuario. <br><br>
                       
                        <b>Orden de Compra:</b> significa el pedido particular de un Usuario para la compra de determinados
                        Productos para el cual se ha terminado el proceso en las Plataformas. <br><br>

                        <b>Perfil:</b> significa la cuenta personal única creada por cada Usuario que acredita el 
                        registro en la Plataforma, en base a la información personal proveída por el mismo a MERQUEO, la cual 
                        incluye su nombre, apellidos, registro único tributario (RUT), fecha de nacimiento, dirección, teléfono, 
                        nombre de usuario, correo electrónico y contraseña. <br><br>

                        <b>Plataforma:</b> significa el sitio web (www.merqueo.com), aplicaciones móviles y plataforma 
                        operada 
                        por MERQUEO, a través de los cuales los Usuarios podrán acceder a los Servicios. <br><br>
                        
                        <b>Política de Privacidad:</b> significa las políticas de privacidad de MERQUEO, las cuales se 
                        encuentran debidamente publicadas en la Plataforma. <br><br>

                        <b>Producto(s):</b> significa los productos ofrecidos en la Plataforma como alimentos, canasta básica 
                        familiar, víveres, bebidas, bebidas alcohólicas, aseo, belleza, consumo masivo, tecnología, 
                        juguetería de mascotas, SOAT Digital, libros, revistas, electrodomésticos y otros artículos de alimentación, 
                        en resumen, todos los artículos y productos que se encuentran disponibles para ser adquiridos por los Usuarios. <br><br>

                        <b>Repartidor(es):</b> significa el personal contratado por MERQUEO o de un tercero externo, 
                        que efectúa la entrega de los Productos. <br><br>

                        <b>Servicios:</b> significa todos los servicios ofrecidos por medio de la Plataforma, 
                        así como los demás servicios provistos por MERQUEO, a los cuales los Usuarios pueden acceder 
                        por medio de la Plataforma y sus Equipos. <br><br>

                        <b>Términos y Condiciones, Términos de Uso o Acuerdo:</b> significa los presentes Términos y Condiciones de MERQUEO.<br><br>

                        <b>Usuario(s):</b> significa toda persona natural o jurídica, o representante en cualquier forma de los 
                        mismos, que utilice o que se encuentra registrado como tal en la Plataforma o haya creado un perfil en la misma. 
                        Un Usuario sólo podrá crear un Perfil, es decir la relación Usuario-Perfil es uno a uno. 
                        Estos podrán clasificarse en Navegadores, Participantes y Colaboradores, de acuerdo a lo señalado en los 
                        presentes Términos y Condiciones. El término “Usuario” abarca todas y cualquiera de las referidas categorías.<br><br>

                    </P>
                    <ol style="padding-left: 15px" start="3">
                        <li><b>Cuenta de Usuario</b></li>
                    </ol>
                    <p align="justify">
                        La persona que corresponde a un Usuario el cual crea un Perfil es la única persona autorizada para el acceso 
                        a la Plataforma por intermedio de dicho Usuario. El Usuario es responsable de mantener la confidencialidad 
                        de cualquier contraseña o número de cuenta proporcionado por el Usuario o MERQUEO para acceder a la Plataforma 
                        a su Perfil. Cada Usuario es el único y absoluto responsable de todas las actividades que ocurran bajo su 
                        contraseña, cuenta o Perfil. MERQUEO no tiene control sobre el uso de la cuenta de un Usuario y 
                        renuncia expresamente a cualquier responsabilidad derivada de la misma. En caso de que un Usuario 
                        sospeche que un tercero pudiera estar accediendo a la plataforma bajo su cuenta de Usuario o utilizando 
                        su contraseña, notificará a MEQUEO inmediatamente. <br><br>

                        Si usted proporciona su número de teléfono celular, por ese hecho entrega expresamente su consentimiento para
                        que MERQUEO haga uso del mismo para llamadas, información de los pedidos, envío de mensajes de textos y notificaciones
                        por whatsapp con el fin de ofrecer los servicios, aplican las Políticas de Privacidad. Si usted proporciona su correo
                        electrónico, por ese hecho entrega expresamente su consentimiento para que MERQUEO haga uso del mismo para envío de
                        correos electrónicos con información de los pedidos y mensajes con el fin de ofrecer los servicios o beneficios
                        disponibles, aplican las Políticas de Privacidad. Usted puede optar por no recibir mensajes de texto,
                        notificaciones por whatsapp y/o correos electrónicos desde MERQUEO, enviando un correo electrónico
                        a  [<a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a>]. <br><br>



                        Toda información suministrada por el Usuario en la creación de su Perfil, la cual hace parte de la 
                        Cuenta de Usuario, debe ser verídica y al entregar dicha información el Usuario autoriza a MERQUEO 
                        para validarla por cualquier medio. <br>

                        Está prohibida la creación de múltiples Perfiles, Cuentas de Usuarios y Usuarios asociados a una misma 
                        persona natural o jurídica. Si un usuario olvida la contraseña podrá solicitar a través de la plataforma 
                        la actualización de la misma. <br>

                        En caso de identificarse la creación de múltiples Usuarios/Perfiles/Cuentas de Usuarios asociados 
                        una misma persona natural o jurídica, MERQUEO podrá bloquear dichos Usuarios y podrá incluirlos en 
                        listas restrictivas de la Plataforma y/o realizar la cancelación de sus pedidos.
                    </p>
                    <ol style="padding-left: 15px" start="4">
                        <li><b>Liberación De Responsabilidad De MERQUEO Por Indebida Proporción De Información Por Parte 
                            Del Usuario</b></li>
                    </ol>
                    <p align="justify">
                        La entrega de información por parte del Usuario para la creación de su Perfil y la información para 
                        la documentación de póliza SOAT Digital será responsabilidad únicamente del Usuario. 
                        Errores en la escritura, diligenciamiento y veracidad de la misma no generarán responsabilidad 
                        alguna a MERQUEO y el Usuario será quien responda por las consecuencias que la indebida 
                        proporción de información pueda generar. <br><br>

                        Para el Caso de SOAT Digital el Usuario será responsable de realizar el pago de los costos 
                        adicionales en los que se incurra para realizar el cambio de información errada. 
                        Para solicitar el cambio deberá escribir un correo electrónico a 
                        [<a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a>].  
                        <br><br>

                        El Usuario declara que conoce que la indebida proporción de la información puede impactar los 
                        tiempos de entrega y Productos incluidos en la Orden de Compra, e inclusive puede generar la 
                        cancelación del mismo sin que pese perjuicio para MERQUEO y no está en la obligación de notificar 
                        previamente al Usuario.
                    </p>
                    <ol style="padding-left: 15px" start="5">
                        <li><b>Proceso de Compra</b></li>
                    </ol>
                    <ul>
                        <li>COBERTURA</li>
                    </ul>
                    <p align="justify">
                        A la fecha MERQUEO cuenta con cobertura en las ciudades de Bogotá y Medellín, la cobertura
                        puntual dentro de las ciudades puede variar según el alcance logístico con el que cuente MERQUEO
                        para realizar las respectivas entregas de las compras realizadas por sus Usuarios;
                        adicionalmente MERQUEO está en la facultad de modificar en cualquier momento estas coberturas y
                        así mismo está en la obligación de ofrecer al cliente el mecanismo para que el Usuario pueda
                        verificar si la dirección donde recibirá su pedido se encuentra dentro de la cobertura, para
                        esto el Usuario deberá ingresar los datos de la dirección donde recibirá el Pedido en la
                        Plataforma verificando que la geográfica sea la correcta.

                        La opción de entrega en franjas “Mismo Día” solo aplicarán para Bogotá en las siguientes zonas
                        demarcadas:
                        <a href="https://www.google.com/maps/d/viewer?mid=1OdheFxDRegX3Y4SPUCeAAf9AFxFUpYfk&ll=4.638651214660725%2C-74.0500118850411&z=12"
                           target="_blank" style="color: dodgerblue">ver mapa</a>
                    </p>
                    <ul>
                        <li>DISPONIBILIDAD DE PRODUCTOS</li>
                    </ul>
                    <p align="justify">
                        Los Productos presentados en la Plataforma son revisados respecto a los inventarios, 
                        en caso de no contar con la existencia de los mismos directamente o en locales aliados, 
                        MERQUEO no realizará el cobro de Producto faltante en la Orden de Compra y no estará en 
                        la obligación de avisar al Usuario previamente. Pero avisará en el momento de la entrega 
                        mediante la Plataforma.
                        <br><br>
                        SOAT Digital: El SOAT es un seguro obligatorio para todos los vehículos automotores que 
                        transitan por el territorio nacional y ampara los daños corporales causados a las personas 
                        en un accidente de tránsito. Conforme la Resolución 4170 de 2016 expedida por el Ministerio 
                        de Transporte, en Colombia existe la posibilidad de contar con el Seguro Obligatorio de 
                        Accidentes de Tránsito SOAT en medios magnéticos. En virtud de lo anterior, 
                        MERQUEO ofrece la posibilidad de obtener el mismo a través de la Plataforma. 
                        La aseguradora que respaldará dicha emisión es Seguros Mundial a través del servicio de MiMotor.com.
                    </p>
                    <ul>
                        <li>HORARIOS DE ENTREGA Y COSTO DEL DOMICILIO</li>
                    </ul>
                    <p align="justify">
                        La hora de entrega de una Orden de Compra dependerá de la franja horaria seleccionada por los
                        clientes en el momento del check out de la misma. Esta corresponde a la hora en que el
                        Repartidor entregará el producto en la dirección indicada.

                        La entrega de los Productos tendrá un costo indicado como costo de domicilio, el cual varía de
                        acuerdo a la franja horaria seleccionada y los Beneficios (Promociones y Descuentos) que
                        apliquen sobre el mismo.

                        La cantidad de entregas por franja horaria posee una capacidad limitada por lo cual MERQUEO se
                        reserva el derecho de quitar la disponibilidad de una franja cuando esta ha llegado a su límite
                        de capacidad, el Usuario verá en la Plataforma las franjas que se encuentran disponibles.

                        En caso de que en la ciudad de entrega ocurran incidentes fuera de control de MERQUEO como
                        marchas, huelgas, condiciones climáticas adversas o accidentes, haremos todo lo posible por
                        entregar la orden con el menor tiempo de retraso posible, teniendo como máximo 1 hora posterior
                        a la hora fin de la franja seleccionada.

                        Las entregas seleccionadas en las franjas denominadas “Mismo Día”, están sujetas a lo indicado
                        anteriormente y aplicarán de acuerdo a las zonas de cobertura indicadas.
                    </p>

                    <ul>
                        <li>HORARIO DE PEDIDO</li>
                    </ul>
                    <p align="justify">
                        Todo pedido realizado posterior a las 11:00pm quedará asignado como si se hubiese 
                        realizado el día posterior y por tanto no estará habilitado para ser entregado en 
                        algunas franjas del día siguiente, es decir, si el pedido se realiza el día 1 
                        a las 11:00 pm, es como si se hubiese realizado en el día 2, por lo cual la 
                        entrega quedará programada para el día 3 en la franja que el cliente prefiera.
                    </p>
                    <ul>
                        <li>PEDIDO MÍNIMO</li>
                    </ul>
                    <p align="justify">
                        MERQUEO en sus Plataformas restringe el valor del pedido mínimo requerido para poder 
                        procesar un Orden de Compra. Este valor será indicado cuando el Usuario incluye ítems 
                        o productos en el carrito de compras. <br><br>

                        El valor para cumplir la restricción del valor del pedido mínimo no incluye el valor del domicilio.
                    </p>
                    <ul>
                        <li>PRECIO DE VENTA</li>
                    </ul>
                    <p align="justify">
                        El precio que se deberá pagar por cada artículo adquirido será el precio vigente en el sitio al 
                        momento de hacerse el pedido, el precio sin tachadura.
                    </p>
                    <ul>
                        <li>PAGO DE LA ORDEN DE COMPRA</li>
                    </ul>
                    <p align="justify">
                        Las Órdenes de Compra podrán pagarse de las siguientes formas:
                    </p>
                    <ul style="margin-left: 20px; list-style-type: circle">
                        <li>Pago contra entrega: Efectivo y Datáfono</li>
                        <li>Pago Online/No Presencial: Tarjeta de Crédito Visa, Mastercard, Diners y American Express.
                        </li>
                        <li>Los productos faltantes o productos sin disponibilidad no se cobrarán o se devolverá el 
                            dinero de acuerdo al medio de pago utilizado por el cliente o según el acuerdo entre el Usuario 
                            y MERQUEO el cual incluye, pero no se limita a, devolución en bonos o créditos en la aplicación, 
                            en algunos casos el dinero se entregará en efectivo con la entrega de los productos, 
                            sin que se requiera aviso al cliente.   
                        </li>
                        <li>
                            Para pago con tarjetas Sodexo únicamente aplican las Premium Pass y Canasta Pass a través de los datáfonos. El usuario debe seleccionar la opción "datáfono" en la plataforma a la hora de realizar el pedido para realizar el pago contra entrega con tarjetas Sodexo.
                        </li>
                        <li>
                            Créditos Merqueo, dependiendo del producto o servicio se podrá habilitar la 
                            forma de pago por medio de créditos Merqueo. Los créditos Merqueo son valores
                            equivalentes a dinero que tienen una fecha de uso determinada.
                        </li>
                    </ul>
                    <ul>
                        <li>ENTREGA DE ORDEN DE COMPRA</li>
                    </ul>
                    <p align="justify">
                        Repartidor realizará la entrega de la Orden de Compra en la dirección indicada por 
                        el Usuario para tal fin, realizará la entrega de los Productos verificando el estado de 
                        la Entrega junto al Usuario, en caso de no encontrarse el Usuario en la dirección dejará 
                        el pedido con un tercero autorizado para tal fin. <br><br>

                        El Repartidor recibirá el valor de la Orden de Compra en caso de pago con efectivo o datáfono.
                        <br><br>
                        La póliza SOAT será enviada únicamente en formato PDF al correo electrónico proporcionado por el 
                        Usuario al momento de la compra. Será responsabilidad del Usuario agregar el correo 
                        [<a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a>] 
                        a su libreta de direcciones para garantizar la entrega 
                        de los documentos a su bandeja de entrada.
                    </p>
                    <ul>
                        <li>EXPEDICIÓN DE SOAT DIGITAL</li>
                    </ul>
                    <p align="justify">
                        Una vez se haya efectuado el pago del SOAT se emitirá el documento con la información 
                        del Runt que se encuentre relacionada con el vehículo (carro o moto).  <br><br>
                        Cuando se expide el SOAT, se enviará al correo electrónico indicado por el Usuario 
                        en formato digital (PDF) para que sea portada en los dispositivos electrónicos o 
                        se realice la impresión de la misma.
                    </p>
                    <ul data-hash="#mejor-precio">
                        <li>MEJOR PRECIO GARANTIZADO</li>
                    </ul>
                    <p align="justify">
                        El Mejor Precio Garantizado aplica a los productos marcados con esta campaña en donde MERQUEO 
                        garantiza que el precio del producto es el mejor precio del mercado. En caso de que algún usuario 
                        de MERQUEO posterior a haber realizado su compra en la Plataforma encuentre el producto adquirido 
                        (que debe ser de las mismas características y marca al ofertado) a menor precio podrá presentar 
                        una solicitud vía e-mail a <a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a> 
                        adjuntando comprobantes necesarios 
                        (factura o imagen que contenga el menor precio del producto en comparación al ofertado por MERQUEO) 
                        para que sea reintegrado en crédito (saldo a favor) al Usuario el valor del producto comprado. 
                        Adicionalmente, se hará este crédito por el valor de solo un ítem de las características
                         mencionadas en este literal. MERQUEO tendrá quince (15) días hábiles para la revisión y 
                         resolución de estas solicitudes.
                        <br><br>
                        Para que un producto cumpla la condición de Mejor Precio Garantizado se deberá cumplir las siguientes características: 
                    </p>
                    <ul style="margin-left: 20px; list-style-type: circle">
                        <li>Si el producto fue comprado en Bogotá deberá compararse con productos vendidos 
                            en la ciudad de Bogotá, de igual forma aplica para los productos comprados en Medellín.
                        </li>
                        <li>
                            Productos comparables: En productos Marcas Propias de Justo y Bueno, serán 
                            comparables con marcas blancas, es decir, de consumo común y que sean marca 
                            propia de otra cadena de distribución o supermercado. En caso de no ser productos 
                            Marcas propias de Justo y Bueno, la comparación debe realizarse con la misma marca 
                            y las mismas características del producto de otros supermercados en la misma fecha de compra. 
                        </li>
                        <li>
                            Las cualidades del producto como tamaño, dimensiones y usos deberán ser idénticos. 
                        </li>
                        <li>
                            La diferencia de precio sólo será válida si el precio de venta del otro supermercado 
                            es menor al precio de venta de MERQUEO y este precio no es afectado por 
                            algún descuento en particular.
                        </li>
                        <li>
                            El cliente debe haber comprado el producto en MERQUEO.
                        </li>
                    </ul>
                    
                    <ul>
                        <li>NORMAS ESPECÍFICAS SOBRE ORDENES DE COMPRA EN LAS QUE SE INCLUYA BEBIDAS ALCOHÓLICAS Y 
                            CIGARRILLOS
                        </li>
                    </ul>
                    <p align="justify">
                        Prohíbase el expendio de bebidas embriagantes a menores de edad. Ley 124 de 1994. El exceso de 
                        alcohol es perjudicial para la salud. Ley 30 de 1986 <br><br>
                        Toda Orden de Compra que incluya bebidas alcohólicas o cigarrillos sólo podrá ser requerida 
                        por Usuarios mayores de 18 años de edad. El Repartidor que efectúe la entrega de Órdenes de 
                        compra que incluyan dichos productos exigirá, para su entrega, la identificación del Usuario 
                        que efectuó la Orden de compra, exigiendo la presentación de una cédula de identidad o 
                        pasaporte vigente, liberando de toda responsabilidad, tanto a MERQUEO, sus directores,
                        empleados, subsidiarias, afiliados, agentes y representantes, como también al Repartidor, 
                        en caso de no producirse la entrega por la falta de acreditación de identidad del Usuario.
                    </p>
                    <ol style="padding-left: 15px" start="6">
                        <li><b>Cancelación y Retracto de Compra</b></li>
                    </ol>
                    <ul>
                        <li>RETRACTO POR PARTE DEL USUARIO</li>
                    </ul>
                    <p align="justify">
                        Cuando un usuario requiera cancelar su pedido por motivos que corresponden a su necesidad, 
                        deberá notificar por los canales de contacto ya enunciados en los presentes términos y 
                        condiciones de uso en un término no mayor a cinco (5) horas posteriores a la realización 
                        del pedido, esto, con el fin de tener un margen de reacción a nivel logístico que no 
                        signifique un perjuicio ni para el Usuario ni para MERQUEO. <br><br>
                        MERQUEO es un supermercado en línea, con su propio centro de distribución, y a cargo 
                        logísticamente de la calidad, estado y condiciones de varios de sus Productos. 
                        En este sentido, MERQUEO comercializa, entre otros, productos que tienen la calidad 
                        de perecederos, o que están destinados al uso personal, razón por la cual a este 
                        tipo de Productos no les será aplicable el derecho de retracto establecido en el 
                        artículo 47 de la Ley 1480 de 2011. <br><br>
                        Para solicitudes de devoluciones por favor referirse a la sección 7 del presente documento, 
                        “Reclamaciones y Servicio al Cliente”. 
                    </p>
                    <ul>
                        <li>RETRACTO POR PARTE DE MERQUEO</li>
                    </ul>
                    <p align="justify">
                        MERQUEO se reserva el derecho a cancelar pedidos y/o anular beneficios que 
                        considere sospechosos de incumplir cualquiera de los términos de este documento 
                        y no está en la obligación de notificar previamente al usuario.
                    </p>
                    <ol style="padding-left: 15px" start="7">
                        <li><b>Reclamaciones y Servicio al Cliente</b></li>
                    </ol>
                    <p align="justify">
                        MERQUEO recibirá y dará trámite a las solicitudes, peticiones, quejas y 
                        reclamos que formule el Usuario en las eventualidades que se desprendan del servicio 
                        que presta la compañía como cumplimiento en los tiempos de entrega y calidad de los 
                        productos entre otros casos. MERQUEO tiene control de los servicios y el personal que 
                        sea contratado de manera directa como ha sido enunciado en el presente numeral. <br><br>
                        Si el Usuario tiene alguna duda respecto de los Términos y Condiciones, Política de Privacidad, 
                        uso de la Plataforma o de su Perfil, podrá ponerse en contacto con MERQUEO escribiendo al 
                        correo electrónico <a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a>. 
                        Los mensajes serán atendidos en un máximo 
                        de 48 horas y resueltos definitivamente en un tiempo de cinco días (5) según lo establecido 
                        por la legislación vigente y por las políticas internas de MERQUEO para la atención de PQR.
                    </p>
                    <ul>
                        <li>Reclamación</li>
                    </ul>
                    <p align="justify">
                        El Usuario podrá realizar reclamaciones cuando el Producto entregado no corresponda al 
                        producto indicado, cuando el Producto no se encuentre en el estado indicado en la Plataforma 
                        o no se haya entregado un Producto que se encontraba en la Orden de Compra, esta podrá 
                        realizarla a través de la Plataforma o del correo electrónico <a href="mailto:{{ $admin_email }}"
                                                                  style="color: dodgerblue">{{ $admin_email }}</a>.
                    </p>
                    <ul>
                        <li>Solicitud de Devoluciones</li>
                    </ul>
                    <p align="justify">
                        El Usuario podrá solicitar devoluciones del dinero o cambios de producto de acuerdo a 
                        la tipología del Producto siempre garantizando que los productos conserven las características 
                        originales. <br><br>
                        <b>Productos no perecederos:</b> Cambios máximo 12 horas después de recibido el producto. 
                        Los productos deben conservar las características originales. 
                        Para todos los cambios se debe realizar la solicitud por escrito a través del correo 
                        electrónico de servicio al cliente <a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}.</a> <br><br>
                        <b>Productos perecederos:</b> Se recibirán solicitudes de cambio máximo 3 horas después de recibido 
                        el Producto, en caso de que la solicitud de cambio se deba a calidad o estado del mismo la solicitud 
                        deberá realizarse de acuerdo al tratamiento indicado como “Reclamación”. En el momento de la recogida 
                        del producto el Repartidor verificará el estado de los Productos, garantizando que los Productos que 
                        requieren cadena de frío no la hayan perdido. Para todos los cambios se debe realizar la solicitud por 
                        escrito a través del correo electrónico de servicio al cliente 
                        <a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}.</a>
                        <br><br>
                        <b>Productos Medicamentos:</b> No se recibirán solicitudes de cambio para medicamentos. 
                        Los reclamos o cambios se atenderán si al recibir los medicamentos el Usuario revisó 
                        junto con el Repartidor los Productos entregados, y se determinó que los mismos no 
                        coinciden con la Orden de Compra, o presentan defectos de calidad. <br><br>
                        Para las reclamaciones y solicitud de devoluciones, después de la revisión y de la verificación 
                        por parte de servicio al cliente se podrá realizar devolución del dinero al cliente abonándolo 
                        como crédito a la cuenta de Usuario de MERQUEO, o realizando la entrega del Producto sujeto del 
                        reclamo, cumpliendo con la calidad y características del mismo. 
                    </p>
                    <ul>
                        <li>Garantías</li>
                    </ul>
                    <p align="justify">
                        <b>Productos perecederos:</b> No tendrán garantía. Aplican los términos y condiciones indicados 
                        para reclamaciones y devoluciones. <br><br>
                        <b>Productos no perecederos:</b> Productos de tecnología y juguetería, la garantía será 
                        otorgada por el fabricante de los productos, razón por la cual la reclamación correspondiente 
                        deberá ser presentada ante el fabricante. Para los demás productos no aplican garantías, 
                        aplican los términos y condiciones indicados para reclamaciones y devoluciones.
                    </p>
                    <ol id="referidos" style="padding-left: 15px" start="8">
                        <li><b>Referidos</b></li>
                    </ol>
                    <p align="justify">
                        A continuación, se detallan los términos y condiciones que se aplicarán al programa de referidos al 
                        cual se puede acceder a través de la Plataforma.
                    </p>
                    <ul>
                        <li>Usuario nuevo se define como una persona natural única que nunca ha usado el servicio o 
                            realizado la compra de Productos de MERQUEO.
                        </li>
                        <li>El beneficio sólo podrá ser usado una vez por dirección de entrega.</li>
                        <li>Los Usuarios existentes podrán referir familiares y/o amigos a través de su código de 
                            referido y recibirán {{ currency_format(Config::get('app.referred.referred_by_amount')) }}
                            pesos en crédito de descuento para su próximo pedido 
                            inmediatamente después de que el Usuario nuevo haya recibido su primer pedido.
                        </li>
                        <li>
                            Solo se podrá referir a un Usuario nuevo desde nuestra app.
                        </li>
                        <li>
                            El código de referido podrá ser compartido por los Usuarios existentes cuando éstos
                            han realizado su primera compra. 
                        </li>
                        <li>Los nuevos Usuarios que hayan usado un código válido de referido tendrán un descuento 
                            exclusivo para su primera compra por un valor de {{ currency_format(Config::get('app.referred.amount')) }} 
                            pesos en crédito de descuento aplicable sólo a su primer pedido.
                        </li>
                        <li>El crédito de descuento aplica sólo para pedidos mayores a 
                            {{ currency_format(Config::get('app.minimum_discount_amount')) }} pesos sin incluir 
                            el costo de domicilio.
                        </li>
                        <!-- <li>
                            Un Usuario podrá utilizar máximo 20.000 pesos en crédito de descuento en un mismo pedido.
                        </li> -->
                        <li>
                            El beneficio aplica exclusivamente para Usuarios de acuerdo a la cobertura y será efectivo 
                            únicamente si el Usuario hace el pedido dentro de la fecha de vigencia del crédito de descuento.
                        </li>
                        <li>
                            Referidos con información o datos como correo electrónico o teléfonos no veraces o no verificables 
                            no serán tenidos en cuenta y no podrán acceder al programa de referidos, pudiendo también aplicar 
                            la cancelación de la Orden de Compra.
                        </li>
                        <li>
                            Un Usuario puede referir máximo a {{ Config::get('app.referred.limit') }} usuarios nuevos.
                        </li>
                        <li>
                            En caso de que MERQUEO pueda tener dudas sobre el origen y la veracidad de los datos entregados 
                            en virtud del programa de referidos, MERQUEO podrá contactar por teléfono al titular de los datos 
                            con el fin de verificar los mismos, antes de dar aplicabilidad al programa. En este sentido, 
                            el programa de referidos podrá quedar suspendido mientras los datos son verificados.
                        </li>
                    </ul>
                    <ol style="padding-left: 15px" start="9">
                        <li><b>Beneficios: Promociones y Descuentos</b></li>
                    </ol>
                    <p align="justify">
                        A continuación, se detallan los términos y condiciones que se aplicarán a todos los beneficios 
                        (descuentos y promociones) que se anuncian o a las que se puede acceder a través de la Plataforma.
                    </p>
                    <ul>
                        <li>Los descuentos corresponden al beneficio u oferta que MERQUEO por sí mismo y/o con aliados 
                            aplica al valor de la Orden de Compra o sobre una parte de ésta, este valor se indica en el 
                            resumen de la orden de pago en la sección “Descuentos” y disminuye el valor a pagar por parte 
                            del Usuario.
                        </li>
                        <li>Las promociones corresponderán al beneficio u oferta que MERQUEO por sí mismo y/o con aliados 
                            aplica al precio de venta de los Productos ofrecidos en la Plataforma.
                        </li>
                        <li>El precio en promoción de los artículos corresponderá al valor que NO se encuentra tachado, 
                            el valor tachado corresponde al valor del Producto antes de la promoción. En caso de que el valor 
                            tachado sea inferior al valor no tachado se entenderá por el valor de promoción el valor menor.
                        </li>
                        <li>Los descuentos aplicarán sobre el valor de la Orden de Compra o sobre los Productos de la 
                            Orden de Compra comprendidos en el descuento. Si en una Orden de Compra a la cual se aplica un 
                            descuento se incluyen Productos en promoción, el descuento aplicará sobre el precio indicado en 
                            promoción a no ser que en el descuento se indique lo contrario.
                        </li>
                        <li>Vigencia de los beneficios (descuentos y promociones): los beneficios presentados dentro de los 
                            banners de la Plataforma o que se envíen en comunicaciones propias o con aliados, tienen la vigencia 
                            indicada en los mismos, en caso de no indicarse estarán vigentes por el día en el que se visualizan 
                            a no ser que se especifique una fecha de fin en el mismo. La vigencia también se encuentra supeditada 
                            a las existencias disponibles para el beneficio. 
                        </li>
                        <li>Los beneficios serán efectivos y están supeditados a las coberturas del mismo, el Usuario hace el 
                            pedido y programa la entrega dentro de las fechas de vigencia de los beneficios o hasta agotar existencias 
                            (teniendo en cuenta las unidades disponibles de Productos destinadas a la promoción).
                        </li>
                        <li>Los beneficios efectuados dentro de “Primera Compra” sólo podrán ser redimidos por Usuarios 
                            nuevos de MERQUEO que realicen su pedido vía la Plataforma.
                        </li>
                        <li>El Usuario podrá hacer compras de uno o varios ítems en promoción teniendo en cuenta para un ítem 
                            la cantidad máxima de unidades que se pueden incluir en una Orden de Compra indicada en la Plataforma. 
                            Para los casos donde el ítem no indique directamente la cantidad máxima, se tendrá como capacidad máxima 
                            de un ítem los que el sistema deje incluir bajo el precio de promoción en la Orden de Compra.
                        </li>
                        <li>MERQUEO podrá finalizar un beneficio antes de tiempo dado el caso en que los artículos de dicha 
                            promoción se encuentren agotados en inventario o sucursales de sus aliados.
                        </li>
                        <li>MERQUEO podrá restringir la compra de un mismo ítem o Producto con promoción a un tiempo determinado 
                            (Ej. Un ítem en promoción cada 5 días) por Usuario o dispositivo. 
                        </li>
                        <li>Si un pedido contiene artículos en promoción, y al momento de realizar todo el proceso logístico 
                            interno MERQUEO evidencia que el artículo está agotado en existencias, MERQUEO podrá cancelar 
                            la Orden de Compra del Usuario u ofrecer una nueva opción en caso de que se quiera dar continuidad 
                            a la relación de servicio.
                        </li>
                        <li>Beneficio domicilio gratis: además de beneficios puntuales, se dará domicilio gratis cuando 
                            se supere el monto indicado en la Plataforma. Este monto es variable de acuerdo a las campañas 
                            de MERQUEO, el monto puede ser modificado a discreción por MERQUEO sin que haya lugar a un aviso 
                            previo al cambio. El monto vigente para el domicilio gratis se indicará en la Plataforma.
                        </li>
                        <li>
                            Beneficio descuento primera compra: Este beneficio se aplica en diferentes modalidades como 
                            lo son domicilio gratis, cupones, productos con precio especial o beneficios de acuerdo al plan 
                            de Referidos. El descuento de primera compra aplicable dependerá de la campaña, la cual estará 
                            indicada en la Plataforma y se dará a conocer al Usuario nuevo.
                        </li>
                    </ul>
                    <p align="justify">
                        Los tipos de descuentos particulares hacen referencia sin limitante a:
                    </p>
                    <ul>
                        <li>Descuento por tipo de medio de pago (es decir: dinero en efectivo, tarjeta débito o crédito 
                            de franquicia alguna en específico o de entidades bancarias en específico; todo lo correspondiente 
                            a este tipo de alianzas).
                        </li>
                        <li>Descuento por amarre de productos (Tómese como ejemplo promociones tipo "Pague 1 y lleve 2 
                            del mismo producto por el precio inicial"). 
                        </li>
                        <li>Descuento por ser cliente frecuente: en relación a Usuarios que cuenten con vinculación 
                            de alguna índole con determinados Productos al acceder a descuentos (entiéndase como tarjeta 
                            cliente frecuente y semejantes).
                        </li>
                        <li>"Ahorra $1.000* en el domicilio" aplicará solo para un ítem de la cantidad final de Productos 
                            de iguales características que se hayan incluido en el carrito de compra. Este descuento será acumulable 
                            teniendo en cuenta que solo aplicará según los Productos que cuenten con la marca distintiva. (*Valor variable) 
                        </li>  
                    </ul>
                    <p align="justify">
                        <b>Campaña 30% primera compra con VISA</b>
                    </p>
                    <ul>
                        <li>
                            Válido únicamente para la primera compra pagando online en Merqueo con tarjetas de crédito VISA
                        </li>
                        <li>
                            Solo aplica para nuevos usuarios Merqueo
                        </li>
                        <li>
                            Monto máximo de descuento $75.000
                        </li>
                        <li>
                            Pedido mínimo $30.000
                        </li>
                        <li>
                            El pedido máximo para aplicar el descuento del 30% es de $250.000. Si el pedido excede ese monto, el descuento máximo será $75.000 en cualquier caso.
                        </li>
                        <li>Oferta válida los miércoles de marzo y abril de 2019.</li>
                        <li>No es acumulable con otras promociones</li>
                        <li>Sujeto a cambios sin previo aviso</li>
                        <li>
                            Válido únicamente para pagos online sujeto a proceso de validación de Merqueo.com,
                            leer políticas de privacidad en
                            <a href="{{ action('UserController@privacy_policy') }}">
                                www.merqueo.com/politicas-de-privacidad
                            </a>.
                        </li>
                        <li>Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella</li>
                    </ul>
                    <p align="justify">
                        SODEXO - Tarjetas Premium y Canasta en datáfonos
                    </p>
                    <ul>
                        <li>
                            Válido exclusivamente para pagos con tarjetas Sodexo Premium y Sodexo Canasta, aplica
                            únicamente para pagos contra entrega a través de datáfonos.
                        </li>
                        <li>No se reciben bonos o cheques Sodexo.</li>
                        <li>No es válido para pagos online.</li>
                        <li>
                            Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, Madrid, Mosquera, Funza,
                            Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella.
                        </li>
                    </ul>
                    <p align="justify">
                        BANCOLOMBIA - Campaña Domicilio gratis
                    </p>
                    <ul>
                        <li>
                            Válido únicamente para la primera compra pagando online en Merqueo, que se realice
                            inscribiendo una tarjeta de crédito Visa, MasterCard o American Express del grupo
                            Bancolombia. Aplica para usuarios nuevos y existentes.</li>
                        <li>
                            El usuario recibirá dos meses (60 días calendario) de domicilio gratis en Merqueo. y
                            podrá hacer uso del beneficio en el momento que desee durante el tiempo de vigencia.
                        </li>
                        <li>Beneficio válido solo una vez por usuario y por tarjeta de crédito.</li>
                        <li>
                            El domicilio gratis se activará automáticamente una vez realice la inscripción
                            de la tarjeta de crédito Bancolombia y se verá reflejado al finalizar el pedido.
                        </li>
                        <li>Oferta válida del 1ro de Abril al 30 de junio de 2019.</li>
                        <li>No es acumulable con otras promociones</li>
                        <li>El beneficio no es canjeable por dinero en efectivo.</li>
                        <li>Sujeto a cambios sin previo aviso</li>
                        <li>Las imágenes utilizadas en la publicidad son de referencia.</li>
                        <li>El descuento aplica únicamente pagando con Tarjeta de crédito Bancolombia.</li>
                        <li>
                            Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, Madrid, Funza, Mosquera
                            Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella
                        </li>
                        <li>
                            Válido únicamente para pagos online sujeto a proceso de validación de Merqueo.com,
                            leer políticas de privacidad en <a href="{{ action('UserController@privacy_policy') }}">
                                www.merqueo.com/politicas-de-privacidad
                            </a>
                        </li>
                        <li>
                            La entrega de los productos estará sujeta a las condiciones establecidas en el proceso
                            de compra a través de la página web <a href="{{ action('UserController@terms') }}">
                                www.merqueo.com/terminos
                            </a>
                        </li>
                        <li>Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella</li>
                    </ul>
                    <p align="justify">
                        <b>30% de devolución en créditos (16 - 19 agosto)</b>
                    </p>
                    <p align="justify">
                        Te devolvemos en créditos el 30% de la compra, el máximo de la devolución es de $150.000, el pedido mínimo debe ser $50.000. No acumulable con otras promociones. Se debe tener en cuenta lo siguiente:
                    </p>
                    <ul>
                        <li>
                            La promoción sólo será válida para un pedido por usuario solicitado por la aplicación móvil y en la página web https://merqueo.com entre el 16 y 19 de agosto del 2019.
                        </li>
                        <li>
                            Tus créditos estarán activos el día 20 de agosto 2019 y serán válidos hasta el 31 de agosto de 2019.
                        </li>
                        <li>
                            El mínimo de compra para redimir el reembolso debe ser de $ 100,000 (cien mil).
                        </li>
                        <li>
                            Si el usuario solicita algún tipo de modificación en el transcurso del pedido, la promoción se aplicará siempre y cuando se mantenga el pedido mínimo establecido.
                        </li>
                        <li>
                            Válido hasta agotar existencias
                        </li>
                        <li>
                            Nos reservamos el derecho de cancelar los pedidos que no cumplan con las condiciones establecidas.
                        </li>
                        <li>
                            Solicitud individual para usuarios que fueron notificados del reembolso por mensaje de texto (SMS).
                        </li>
                    </ul>

                    <p align="justify">
                        <b>Trasnochón Merqueo</b>
                    </p>
                    <p align="justify">
                        Toda nuestra tienda Merqueo tendrá desde el 15% de descuento en todos los productos, para poder obtener este beneficio debe cumplir con las siguientes condiciones:
                    </p>
                    <ul>
                        <li>
                            La promoción de esta campaña aplica solo para usuarios que compren entre el 21 al 26 de agosto de 2019 en el rango horario de 6:00 pm a 9:00 PM, por medio de la aplicación móvil y en la página web https://merqueo.com
                        </li>
                        <li>
                            Aplica para cualquier método de pago.
                        </li>
                        <li>
                            El pedido mínimo debe ser $30.000 y el costo del domicilio está sujeto a la hora de entrega.
                        </li>
                        <li>
                            Promoción válida hasta la hora y fecha estipuladas o hasta agotar existencias, lo que primero suceda.
                        </li>
                        <li>
                            Nos reservamos el derecho de cancelar los pedidos que no cumplan con las condiciones establecidas.
                        </li>
                        <li>
                            Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, San Luis, Madrid, Mosquera, Funza, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella. Sujeto a cambios sin previo aviso.
                        </li>
                        <li>
                            Producto en descuento solo aplica una unidad
                        </li>
                        <li>
                            No aplica para compras de sucedáneos de la leche materna (sustitutivo parcial o total de la leche materna)
                        </li>
                        <li>
                            No aplica para compra de cigarrillos
                        </li>
                    </ul>
                    <p align="justify">
                        50 productos con 50% de descuento
                    </p>
                    <ul>
                        <li>

                            Ofertas válidas del 13 al 17 de Marzo de 2019 ó hasta agotar existencias. Productos en
                            promoción tienen descuento en una unidad. No es acumulable con otras promociones,
                            sujeto a cambios sin previo aviso. Aplica en la zona de cobertura en Bogotá, Chía,
                            Soacha, Medellín, Bello, Sabaneta, Envigado, Itagüí, La Estrella y Caldas. El pago puede
                            ser contra entrega en efectivo o con datáfono, u online con tarjeta de crédito, sujeto a
                            proceso de validación de Merqueo.com, leer políticas de privacidad en
                            <a href="{{ action('UserController@privacy_policy') }}">
                                www.merqueo.com/politicas-de-privacidad
                            </a>.
                            Los términos y condiciones pueden ser consultados en
                            <a href="{{ action('UserController@terms') }}">www.merqueo.com/terminos</a>, ante
                            cualquier inquietud puede escribir a nuestro correo electrónico servicioalcliente@merqueo.com
                        </li>
                    </ul>
                    <p align="justify">
                        Alianza Grupo R5
                    </p>
                    <ul>
                        <li>
                            El Beneficio será válido ÚNICA y EXCLUSIVAMENTE para los Usuarios Nuevos. El Beneficio estará vigente del 15 de julio hasta el 31 de agosto de 2019.
                        </li>
                        <li>
                            El Cupón de descuento será válido del 15 de julio hasta el 31 de agosto de 2019.
                        </li>
                        <li>
                            Sin perjuicio de que LAS Partes, transcurridos 30 días calendario de ejecución del Convenio decidan darlo por terminado, MERQUEO se obliga a seguir entregando los Cupones de descuento hasta cumplirse el período inicialmente pactado, es decir, de 45 días calendario.
                        </li>
                        <li>
                            Válido en la zona geográfica de cobertura de MERQUEO. Ver <a href="{{ action('UserController@terms') }}">www.merqueo.com/terminos</a>. Sujeto a cambios sin previo aviso.
                        </li>
                        <li>
                            El Cupón es un crédito que se le entrega al Usuario Nuevo para que realice su primera compra por la plataforma (página web) o app de MERQUEO. En NINGÚN CASO se entregará dinero en efectivo.
                        </li>
                        <li>
                            Para acceder al Beneficio el Usuario Nuevo debe, (I) previamente, comprar/adquirir el SOAT Digital ofrecido por GR5 durante la vigencia del CONVENIO (II) Realizar un pedido por la plataforma (página web) o app de MERQUEO por un valor de COP $120.000 pesos en adelante durante la vigencia del CONVENIO.
                        </li>
                        <li>
                            GR5 entregará los Cupones de descuento a los Usuarios Nuevos vía correo electrónico única y exclusivamente a los Usuarios Nuevos que hayan comprado una póliza SOAT Digital a GR5 durante el período de vigencia del Beneficio. Se entregará máximo un (1) Cupón de descuento por la compra del SOAT Digital.
                        </li>
                        <li>
                            MERQUEO controlará y validará que cada Usuario sea Nuevo y que se encuentre dentro de la zona geográfica de cobertura del Beneficio. En caso de cualquier error sobre lo anterior, será única y exclusiva responsabilidad de MERQUEO cualquier petición, queja o reclamos que se reciban y/o sobre cualquier perjuicio que sufra GR5.
                        </li>
                        <li>
                            Si un Usuario Nuevo tiene cualquier tipo de PQR sobre el servicio de GR5 en la compra de su SOAT Digital, GR5 está obligado a atender este cliente por los canales de comunicación (teléfono, correo electrónico) dentro de 1 día hábil después de presentado la PQR.
                        </li>
                        <li>
                            Si un Usuario Nuevo tiene cualquier tipo de PQR sobre el servicio de MERQUEO en el uso de su cupón o en su experiencia con el servicio de MERQUEO en general, MERQUEO está obligado a atender a este Usuario Nuevo por los canales de comunicación (teléfono, correo electrónico) dentro de 1 día hábil después de presentada la PQR.
                        </li>
                        <li>
                            Aplica exclusivamente un cupón por dirección física del Usuario Nuevo. No es acumulable con otros beneficios. Productos en promoción tienen descuento en una unidad.
                        </li>
                        <li>
                            Para la redención del Beneficio se debe ingresar a la app o página web de MERQUEO <a href="http://www.merqueo.com">www.merqueo.com</a>, agregar la dirección de entrega deseada, agregar los productos deseados, ingresar el código del Cupón de descuento, escoger el método de pago, confirmar el pedido y crear la cuenta del usuario.
                        </li>
                        <li>
                            La entrega de los productos estará sujeta a las condiciones establecidas en el proceso de compra a través de la página web <a href="{{ action('UserController@terms') }}">www.merqueo.com/terminos</a>.
                        </li>
                        <li>
                            No hay límite en cuanto a cantidades de cupones de descuento entregados por Merqueo a GR5. Siempre y cuando la redención del Cupón de descuento se realice durante la vigencia del CONVENIO.
                        </li>
                        <li>
                            GR5 se encargará de cubrir la totalidad de los costos de la pauta de televisión por cable. Cualquier tipo de comunicación o pauta debe ser aprobada previamente por MERQUEO.
                        </li>
                        <li>
                            Las imágenes utilizadas en la publicidad son de referencia.
                        </li>
                        <li>
                            El beneficio no es canjeable por dinero en efectivo.
                        </li>
                    </ul>
                    <p><b>Categoría Oro</b></p>
                    <p align="justify">
                        Los usuarios que acumulen 3 o más pedidos entregados, tendrán todos los productos de la tienda con el 5% de descuento. La vigencia de la Categoría Oro tendrá un plazo de 35 días, si en este periodo no se realiza la entrega de un pedido, automáticamente la tienda quedará sin descuento del 5%.  Los usuarios que hagan parte de la categoría Oro solo observaran el descuento del 5% en los productos cuando inicien sesión en Merqueo. No aplica para productos sucedáneos de la leche materna y cigarrillos. Máximo 2 unidades por referencia con descuento, descuentos no acumulables con otras promociones. Valido hasta el 31/12/2018.
                    </p>
                    <p><b>¡Te devolvemos el 15% de tu compra en créditos!</b></p>
                    <p align="justify">
                        Por compras superiores a $150.000 entre el 13 y el 16 de enero, recibirás el 15% de descuento de tu compra en créditos para ser redimidos del 21 al 28 de enero de 2019. Los créditos serán cargados el 21 de enero de 2019 a las 4:00 pm, únicamente para los pedidos realizados entre el 13 y el 16 de enero y entregados antes del 21 de enero de 2019. Para la redención de los créditos, el pedido mínimo debe ser de $50.000. Valido en nuestra zona de cobertura en Bogotá, Chía, Soacha, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella. Sujeto a cambios sin previo aviso. No es acumulable con otras promociones.
                    </p>
                    <p><b>¡Haz tu primera compra y recibe el 30% de descuento en créditos!</b></p>
                    <p align="justify">
                        Realiza tu primera compra entre el 17 y el 31 de enero de 2019 y recibe en créditos el 30% de tu compra para redimirlos del 1 al 28 de febrero de 2019. Aplica para los pedidos entregados hasta el 5 de febrero de 2019. Para la redención de los créditos, el pedido mínimo debe ser de $100.000. Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella. Sujeto a cambios sin previo aviso. No es acumulable con otras promociones. La asignación de los créditos se realizará de la siguiente manera:
                        <br><br>
                        -Para los pedidos realizados del 17 al 27 de enero y entregados antes del 1 de febrero de 2019, los créditos serán cargados el 1 de febrero de 2019 a las 4:00 pm
                        <br><br>
                        -Para los pedidos realizados del 28 al 31 de enero y entregados antes del 6 de febrero de 2019, los créditos serán cargados el 6 de febrero de 2019 a las 4:00 pm
                        <br><br>
                        -Ofertas válida en la tienda Online el 29 de mayo de 2019 ó 30 de mayo según  día de envió de mensaje de texto hasta agotar existencias. No es acumulable con otras promociones,Los cupones se pueden redimir únicamente  el 29 de mayo de 2019 o 30 de mayo según fecha de notificación por mensaje de texto y el pedido deberá ser entregado  antes del 31 de mayo de 2019, Sujeto a cambios sin previo aviso. Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, San Luis, Madrid, Mosquera, Funza, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella.El pago puede ser contra entrega en efectivo o con datáfono, u online con tarjeta de crédito, sujeto a proceso de validación de Merqueo.com, leer políticas de privacidad en www.merqueo.com/politicas-de-privacidad.El pago puede ser contra entrega en efectivo o con datáfono, u online con tarjeta de crédito, sujeto a proceso de validación de Merqueo.com, leer políticas de privacidad en www.merqueo.com/politicas-de-privacidad.Los términos y condiciones pueden ser consultados en www.merqueo.com/terminos.Compras no acumulables.Para la redención del beneficio se debe ingresar a la app o página de Merqueo www.merqueo.com, agregar la dirección de entrega deseada, agregar los productos deseados, ingresar el código, escoger el método de pago, confirmar el pedido y crear la cuenta del usuario.Aplica exclusivamente un cupón por dirección.

                    </p>
                    <p><b>¡Te devolvemos Hasta el 40% de tu compra en créditos!</b></p>
                    <p>
                        Por compras entre $100.000 y $150.000 te devolvemos 15% de total de tu compra, entre  $150.001 y $250.000  25%  de total de tu compra  y por compras superiores a $250.001 te devolvemos 40% del total de tu compra, máximo a redimir $120.000.
                        <br><br>
                        Recibirás el porcentaje de descuento de tu compra en créditos para ser redimidos del 18 al 21 de junio de 2019. Los créditos serán cargados el 18 de junio de 2019 a las 2:00 pm, únicamente para los pedidos realizados entre el 12 y el 17 de junio y entregados hasta el 17 de junio de 2019. Para la redención de los créditos, el pedido mínimo debe ser de $80.000.
                        <br><br>
                        Válido en nuestra zona de cobertura en Bogotá, Chía, Soacha, San Luis, Madrid, Mosquera, Funza, Medellín, Envigado, Itagüí, Bello, Sabaneta, Caldas y La Estrella. Sujeto a cambios sin previo aviso. Aplican términos y condiciones
                        <br><br>
                        El descuento se aplicará en la primera orden hecha por el usuario durante la vigencia de la campaña, El monto total del pedido no incluye costo de Domicilio, El límite máximo de órdenes por usuario durante la vigencia de la promoción será de Una (1) orden.
                    </p>
                    <ol style="padding-left: 15px" start="10">
                        <li><b>Uso De Materiales</b></li>
                    </ol>
                    <p align="justify">
                        Salvo que se disponga lo contrario, todos los derechos sobre el Contenido son de propiedad de MERQUEO.
                        Los Usuarios se encuentran autorizados para utilizar la Plataforma, así como para descargar material
                        exhibido en ella, pero únicamente para uso personal, estando expresamente prohibido su copia, reproducción, 
                        publicación, descarga, codificación, modificación, traducción, interpretación, exhibición, distribución, 
                        transmisión, transferencia o difusión en medios de comunicación, sin la autorización previa, expresa y por 
                        escrito de MERQUEO. Los derechos de autor y otros derechos de propiedad intelectual sobre el Contenido, 
                        con independencia de su descarga por parte de los Usuarios, son de propiedad de MERQUEO, haciendo expresa 
                        reserva del ejercicio de todas las acciones tanto civiles como penales destinadas al resguardo de sus 
                        legítimos derechos de propiedad intelectual e industrial.
                    </p>
                    <ol style="padding-left: 15px" start="11">
                        <li><b>Material Generado Por Usuarios</b></li>
                    </ol>
                    <p align="justify">
                        En caso de que la Plataforma permitiera la publicación, subida o carga de Material de Usuarios, 
                        éstos serán considerados en todo momento como no confidenciales. Además, mediante la publicación 
                        o carga de los mismos a MERQUEO, el Usuario otorga a MERQUEO un derecho perpetuo, mundial, 
                        no exclusivo, libre de regalías, sublicenciable y transferible para usar, reproducir, distribuir, 
                        modificar, mostrar y realizar el envío de Material de Usuarios, para la promoción y apoyo de MERQUEO. 
                        El Usuario otorga a cada Usuario una licencia no exclusiva para acceder a su Material de Usuario a 
                        través de la Plataforma y de mostrar y representar públicamente dicho Material de Usuario, en 
                        virtud de estos Términos y Condiciones. 
                        <br><br>
                        En relación con lo anterior, el Usuario declara acerca del Material de Usuario que: (i) es 
                        titular de los derechos de autor sobre los mismos, o que tiene la autorización del propietario 
                        de los mismos para su publicación en MERQUEO; (ii) no es ilegal, obsceno, difamatorio, injurioso, 
                        pornográfico, odioso, racista, o inadecuado por cualquier motivo; (iii) no dañará la reputación 
                        de MERQUEO o cualquier tercero; y (iv) no se hace pasar por otra persona. MERQUEO se reserva el 
                        derecho de eliminar cualquier Material de Usuario a su entera discreción y sin previo aviso ni 
                        responsabilidad hacia el Usuario o cualquier otra persona.
                        <br><br>
                        MERQUEO no avala ni apoya ninguna clase de Material de Usuario, ni realiza recomendaciones o 
                        consejos sobre los mismos, renunciando el Usuario a vincular a MERQUEO con relación a cualquier 
                        Material de Usuario. El Usuario entiende y acepta que el Material de Usuario puede contener 
                        información que es inexacta u ofensiva, renunciando, desde ya, a reclamar o presentar cualquier 
                        clase de recurso en contra de MERQUEO con respecto a la misma.
                        <br><br>
                        MERQUEO puede proporcionar enlaces a sitios web pertenecientes o gestionados por terceros, 
                        sin que por este hecho pueda entenderse, bajo ninguna circunstancia, que MERQUEO respalda el 
                        contenido, productos o servicios disponibles en dichos sitios web, y que no es responsable de 
                        su contenido o su seguridad. El enlace o conexión del Usuario a cualquier otro sitio web es de 
                        su exclusiva responsabilidad.
                    </p>
                    <ol style="padding-left: 15px" start="12">
                        <li><b>Renuncia</b></li>
                    </ol>
                    <p align="justify">
                        El uso de la Plataforma MERQUEO es bajo el propio riesgo del Usuario. Los Servicios se 
                        prestan tal como son ofrecidos, sin garantías de ningún tipo, explícita o implícita, incluyendo, 
                        pero no limitada a, las garantías de comercialización, idoneidad del Producto para un propósito 
                        particular y no infracción. MERQUEO no efectúa ninguna clase de representación o garantía relativa 
                        a la exactitud o totalidad del contenido ofrecido a través del uso de MERQUEO o sobre el contenido 
                        de cualquier sitio web vinculado al uso de MERQUEO. Sin perjuicio de que MERQUEO actúa con la mayor 
                        diligencia en la prestación de sus Servicios, MERQUEO no asume ninguna responsabilidad derivada de (i) 
                        errores o imprecisiones del Contenido; (ii) lesiones personales o daños a la propiedad, de cualquier 
                        naturaleza, como resultado de su acceso y uso de la Plataforma; (iii) acceso no autorizado o uso 
                        de servidores seguros de MERQUEO y/o cualquier información personal y/o financiera almacenada en ellos. 
                        <br><br>
                        MERQUEO no garantiza que la Plataforma funcionará libre de errores o que esté libre de virus u 
                        otros productos peligrosos. Si el uso de la Plataforma deriva en la necesidad de servicio técnico o 
                        reemplazo de equipo o datos, MERQUEO no será responsable de dichos costos. MERQUEO, en la medida máxima 
                        permitida por la ley, renuncia a toda garantía, explícita o implícita, incluyendo sin limitación las 
                        garantías de comercialización, no violación de derechos de terceros y la garantía de idoneidad para un
                        propósito particular. MERQUEO no garantiza, bajo ninguna circunstancia, la exactitud, confiabilidad, 
                        exhaustividad y actualidad de los contenidos, servicios, soporte, software, textos, gráficos o vínculos. 
                        MERQUEO y sus filiales y proveedores de licencias no garantiza, bajo ninguna circunstancia, que la 
                        información personal suministrada por el Usuario pueda ser objeto de apropiación indebida, interceptada, 
                        borrada, destruida o usada por terceros. <br><br>
                        Sin embargo y atendiendo a lo anterior, MERQUEO hará todo lo que esté a su alcance para minimizar 
                        errores tecnológicos o de infraestructura a nivel de servidores, apps móviles y página web que puedan 
                        derivar en perjuicio al Usuario en todo lo referente a la protección, uso correcto y autorizado de sus datos.
                    </p>
                    <ol style="padding-left: 15px" start="13">
                        <li><b>Reportes Y Denuncias</b></li>
                    </ol>
                    <p align="justify">
                            Los Usuarios podrán reportar a MERQUEO cualquier conducta o Contenido que pueda infringir 
                            estos Términos de Uso, la Política de Privacidad o la legislación aplicable. Para tal efecto, 
                            la Plataforma en su Política de Privacidad y en los presentes términos contará con los canales 
                            idóneos para formular la respectiva PQR, de preferencia el buzón 
                            <a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a>
                        <br><br>
                        Recibida la solicitud, MERQUEO, a su entera discreción, determinará si corresponde la 
                        eliminación o modificación del Contenido.
                    </p>
                    <ol style="padding-left: 15px" start="14">
                        <li><b>Limitación De Responsabilidad</b></li>
                    </ol>
                    <p align="justify">
                        El Usuario acepta que MERQUEO no se hace responsable por ningún daño directo, indirecto, 
                        lucro cesante, daño emergente, daño incidental, especial o consecuencial, proveniente de 
                        o en relación con (i) el uso de la Plataforma; (ii) la responsabilidad de cualquier 
                        Repartidor que no preste sus servicios bajo subordinación y dependencia de MERQUEO; o 
                        (iii) en relación con el rendimiento o navegación en la Plataforma o sus enlaces a 
                        otros sitios web, incluso si MERQUEO ha sido informado de la posibilidad de tales daños. 
                        <br><br>
                        El Usuario reconoce y acepta que los Repartidores son prestadores de un servicio creado 
                        por alianzas de tipo logístico con empresas y comercios dedicados a dicha actividad; 
                        la relación entre MERQUEO y el Usuario corresponde a garantizar mediante los medios tecnológicos 
                        y de servicio con los que cuenta, el facilitar al Usuario un catálogo de Productos ofrecidos 
                        para la respectiva compra y el traslado de éstos al destino indicado por el Usuario. 
                        MERQUEO no cuenta con Repartidores vinculados a su razón social como trabajadores; en este 
                        sentido, y sin contrato laboral entre los Repartidores y la compañía, se configura la 
                        tercerización de un servicio de transporte de los productos que solicita el Usuario.  
                        <br><br>
                        Además, el Usuario acepta que MERQUEO no se hace responsable de los daños derivados de 
                        la interrupción, suspensión o terminación de los Servicios, incluyendo sin limitación daño 
                        directo, indirecto, lucro cesante, daño emergente, daño incidental, especial o consecuencial, 
                        aun cuando dicha interrupción, suspensión o terminación estuviera o no justificada. En ningún 
                        caso la responsabilidad total de MERQUEO ante el Usuario por cualquier clase de pérdidas podrá 
                        exceder los montos pagados por éste a MERQUEO.
                    </p>
                    <ol style="padding-left: 15px" start="15">
                        <li><b>Liberación De Responsabilidad De MERQUEO Por Actos De Terceros</b></li>
                    </ol>
                    <p align="justify">
                        La responsabilidad del cumplimiento de las obligaciones de los Repartidores recaerá en MERQUEO 
                        solamente respecto a quienes presten sus servicios bajo subordinación y dependencia de MERQUEO. 
                        En el resto de los casos, dicha responsabilidad recaerá en el Repartidor correspondiente, sin embargo, 
                        MERQUEO se permite recalcar que las solicitudes que se desprendan de estas eventualidades desde los 
                        Usuarios serán recibidas y tramitadas. En caso de ser necesario, MERQUEO cederá al Usuario los derechos 
                        que pudiere tener en contra de los Repartidores, según corresponda, por los perjuicios que éstos 
                        pudieren haber causado. <br><br>
                        Los Repartidores no están autorizados a ingresar a las viviendas o a la propiedad privada donde 
                        debe entregarse un pedido, salvo que el Usuario lo autorice, en caso de daños o perjuicios MERQUEO 
                        no se hará responsable de los mismos. <br><br>
                        Ni MERQUEO, ni sus afiliados o licenciantes serán responsables ante cualquier reclamo, lesión o 
                        daño que surja en relación con los actos u omisiones de cualquier Repartidor, salvo lo indicado en 
                        el párrafo anterior. En caso de presentarse una disputa con uno o más Repartidores, el Usuario libera a 
                        MERQUEO, sus directores, empleados, subsidiarias, afiliados, agentes y representantes de todos los 
                        reclamos, responsabilidades, costos, incluyendo sin limitación honorarios de abogados, pérdidas o daños 
                        de cualquier clase o naturaleza, directos o indirectos, que surjan a consecuencia de tales disputas.
                    </p>
                    <ol style="padding-left: 15px" start="16">
                        <li><b>Indemnidad</b></li>
                    </ol>
                    <p align="justify">
                        El Usuario se obliga a defender, indemnizar y mantener indemne a MERQUEO, sus funcionarios, 
                        directores, empleados, agentes y afiliados, de y contra cualquier pérdida, reclamos, acciones, costos, 
                        daños, sanciones, multas y gastos, incluyendo sin limitación honorarios de abogados, que surjan de, 
                        relacionados con o resultantes del uso no autorizado de la Plataforma por parte del Usuario, o de cualquier 
                        incumplimiento de estos Términos y Condiciones, incluyendo sin limitación, cualquier violación de cualquier 
                        ley, ordenanza, orden administrativa, norma o reglamento. MERQUEO dará aviso, a la brevedad, de cualquier 
                        reclamación, demanda o procedimiento, y tendrá derecho a asumir la defensa respecto de cualquier reclamación, 
                        demanda o procedimiento.
                    </p>
                    <ol style="padding-left: 15px" start="17">
                        <li><b>Terminación</b></li>
                    </ol>
                    <p align="justify">
                        A su sola discreción, MERQUEO puede modificar o interrumpir la Plataforma, o puede modificar, suspender 
                        o interrumpir su acceso o el soporte, por cualquier razón, con o sin previo aviso y sin ninguna responsabilidad 
                        frente a los Usuarios o cualquier tercero. Aun cuando un Usuario pierda el derecho a utilizar la Plataforma, 
                        los presentes Términos y Condiciones serán ejecutables en su contra. El Usuario podrá terminar estos Términos 
                        y Condiciones en cualquier momento, dejando de utilizar la Plataforma, sobreviviendo todas las disposiciones 
                        que por su naturaleza debieran sobrevivir para surtir efecto.
                        <br><br>
                        La terminación de los Servicios y/o el cierre de la cuenta del Usuario, por cualquier causa, no generará 
                        compensación ni indemnización en su favor por parte de MERQUEO.
                    </p>
                    <ol style="padding-left: 15px" start="18">
                        <li><b>Términos De Ley</b></li>
                    </ol>
                    <p align="justify">
                        Este acuerdo será gobernado e interpretado de acuerdo con las leyes de Colombia, sin dar efecto a 
                        cualquier principio de conflictos de ley. Si alguna disposición de estos Términos y Condiciones es 
                        declarada ilegal, o presenta un vacío, o por cualquier razón resulta inaplicable, la misma deberá ser 
                        interpretada dentro del marco del mismo y en cualquier caso no afectará la validez y la aplicabilidad 
                        de las provisiones restantes.
                    </p>
                    <ol style="padding-left: 15px" start="19">
                        <li><b>Ley Aplicable Y Divisibilidad</b></li>
                    </ol>
                    <p align="justify">
                        Estos Términos y Condiciones estarán regidos por las leyes de la República de Colombia. 
                        Los Términos de Uso y la Política de Privacidad constituyen el acuerdo completo entre el Usuario y 
                        MERQUEO con respecto a los Servicios, uso de la Plataforma y del Contenido, y sustituyen todas 
                        las comunicaciones y propuestas previas o contemporáneas (ya sean escritas, orales o electrónicas) 
                        entre el Usuario y MERQUEO con respecto a la Plataforma y a los Servicios.
                    </p>
                    <ol style="padding-left: 15px" start="20">
                        <li><b>No Agencia</b></li>
                    </ol>
                    <p align="justify">
                        Por el presente Acuerdo no se crea ninguna relación contractual de agencia, asociación, 
                        empresa conjunta, empleado-empleador o franquiciador-franquiciado.
                    </p>
                    <ol style="padding-left: 15px" start="21">
                        <li><b>Modificaciones</b></li>
                    </ol>
                    <p align="justify">
                        MERQUEO podrá, a su sola y absoluta discreción, cambiar unilateralmente y sin aviso previo 
                        los presentes Términos y Condiciones. Sin embargo, tales cambios sólo se aplicarán desde el 
                        momento en que sean publicados en la Plataforma y regirán para las transacciones que se celebren 
                        con posterioridad a su entrada en vigor, sin alterar las transacciones celebradas con anterioridad.
                    </p>
                    <ol style="padding-left: 15px" start="22">
                        <li><b>Varios</b></li>
                    </ol>
                    <p align="justify">
                        Estos Términos de Uso son personales, y no se pueden ceder, transferir, ni sublicenciar, 
                        excepto con el consentimiento previo por escrito de MERQUEO. MERQUEO podrá ceder, transferir o 
                        delegar cualquiera de sus derechos y obligaciones en virtud de estos Términos de Uso sin el 
                        consentimiento del Usuario. A menos que se especifique lo contrario en estos Términos de Uso, 
                        todos los avisos o modificaciones serán considerados debidamente entregados desde el momento de su 
                        publicación en la Plataforma, o bien desde el momento en que sea notificado al Usuario, según corresponda.
                    </p>
                    <ol style="padding-left: 15px" start="23">
                        <li><b>Contacto</b></li>
                    </ol>
                    <p align="justify">
                        Si el Usuario tiene alguna duda respecto de los Términos y Condiciones, Política de Privacidad, 
                        uso de la Plataforma o de su Perfil, podrá ponerse en contacto con MERQUEO escribiendo al correo 
                        electrónico <a href="mailto:{{ $admin_email }}" style="color: dodgerblue">{{ $admin_email }}</a>. 
                        Los mensajes serán atendidos en un máximo de 48 horas 
                        y resueltos definitivamente en un tiempo de cinco días (5) según lo establecido por la legislación 
                        vigente y por las políticas internas de MERQUEO para la atención de PQR.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var hash = location.hash;
            if (hash) {
                var $item = $('[data-hash="'+hash+'"]');
                if ($item) {
                    var pos = ($item.offset().top - 100);
                    $('html, body').animate({scrollTop: pos}, 1000);
                }
            }
        });
    </script>
@stop
