<!DOCTYPE html>
<html lang="es" ng-app="merqueoRate">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Merqueo - Calificación de pedido</title>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <link href="{{ asset_url() }}css/user_score.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">

    <script>
        var orderTime = '{{ format_date('normal_long_with_time', $order->management_date) }}';
        var orderToken = '{{ $order->user_score_token }}';
        var orderId = '{{ $order->id }}';
        var doneRate = '{{ $doneRate  }}';
    </script>
    <script type="text/javascript" src="{{ asset_url() }}js/Leanplum_JavaScript-1.2.4/leanplum.min.js"></script>
    @include('common/analytics')
</head>
<body>

    <main class="merqueo-rate">
        <div class="navbar navbar-default">
            <a href="/">
                <img src="{{ asset_url() }}img/Logo.svg">
            </a>
        </div>
        <div ui-view=""></div>
        <p class="merqueo-rate__copyright">Merqueo SAS. Todos los derechos reservados ©</p>
        <resolve-loader></resolve-loader>
    </main>

{{--Js Libraries--}}
<script src="{{ asset_url() }}js/jquery.min.js"></script>
<script src="{{ asset_url() }}js/angular.min.js"></script>
<script src="https://unpkg.com/@uirouter/angularjs/release/angular-ui-router.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>

<!-- Angular Material Library -->
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"></script>
<script src="https://cdnjs.com/libraries/bodymovin" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.1/lottie.js"></script>
{{--End Js Libraries--}}

<script>
    /*
    * Lottie animation
    */
    function loadAnimation(id, fileName, loop, name) {
        bodymovin.loadAnimation({
            container: document.getElementById(id),
            path: '{{ asset_url() }}img/lottie_anim/'+fileName,
            renderer: 'svg',
            loop: loop,
            autoplay: true,
            name: name,
        });
    }

    angular.module('merqueoRate', ['ui.router', 'ngMaterial'])

        .config(function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state({
                    name: 'home',
                    url: '/?done',
                    templateUrl: '/assets/templates/order_score/user-rate-home.html',
                    controller: 'HomeController'
                })
                .state({
                    name: 'thank-you-page',
                    url: '/thank-you?message',
                    templateUrl: '/assets/templates/order_score/thank-you-page.html',
                    controller: function ($scope, $stateParams) {
                        // Lottie animation
                        loadAnimation('lottie-thankyou', 'cierre.json', false, 'Close');
                        $scope.message = $stateParams.message;
                        trackEvent('order_rated', {
                            order_id: this.id,
                            score: 1,
                        });
                    }
                })
                .state({
                    name: 'error-page',
                    url: '/error-page?message',
                    templateUrl: '/assets/templates/order_score/error-page.html',
                    controller: function ($scope, $stateParams) {
                        // Lottie animation
                        loadAnimation('lottie-thankyou', 'cierre.json', false, 'Close');
                        $scope.message = $stateParams.message;
                        trackEvent('previously_order_rated', {});
                    }
                })
                .state({
                    name: 'thank-you-page-bad-rate',
                    url: '/thank-you-bad-rate?message',
                    templateUrl: '/assets/templates/order_score/thank-you-page-bad-rate.html',
                    controller: function ($scope, $stateParams) {
                        // Lottie animation
                        loadAnimation('lottie-thankyou', 'cierre.json', false, 'Close');
                        $scope.message = $stateParams.message
                    }
                })
                .state({
                    name: 'choose-options',
                    url: '/choose-options',
                    templateUrl: '/assets/templates/order_score/problems-home.html',
                    controller: 'ProblemsController',
                    resolve: {
                        problemsFormat: function (Order) {
                            var order = new Order();
                            return order.getFormat()
                        }
                    }
                });

            $urlRouterProvider.otherwise('/');
        })
        .service('Product', function ($http) {
            function Product(model) {
                this.model = model
            }

            Product.prototype.wasDelivered = function () {
                return this.model.fulfilment_status === 'Fullfilled';
            };

            return Product;
        })
        .service('Order', function ($http, OrderFormat) {
            function Order() {
                this.id = orderId;
                this.other = [];
                this.comment = '';
                this.products = [];
                this.token = orderToken;
            }

            Order.prototype.getFormat = function () {
                return $http.get('/orden/' + this.token + '/get-score-order-data', {headers: {Accept: 'application/json'}})
                    .then(function (response) {
                        return new OrderFormat(response.data.message);
                    })
                    .catch(function () {
                        // TODO agregar libreria para las ventanas modales.
                        // TODO Mostrar el error que se reciba.
                    });
            };

            Order.prototype.rateAsOkOrder = function () {
                return $http.post('/orden/' + this.token + '/calificacion', {rating: 'success'})
                    .then(function (response) {
                        if (!response.data.status) {
                            throw new Error(response.data.message);
                        }

                        return response.data.message;
                    });
            };

            Order.prototype.numOtherValidItems = function () {
                var total = 0;
                for (var index in this.other) {
                    if (this.other.hasOwnProperty(index) && this.other[index]) {
                        total++;
                    }
                }

                return total;
            };

            /**
             *
             * @param productProblems
             * @return Promise.<string>
             */
            Order.prototype.save = function (productProblems) {
                var products = [];
                var other = [];

                productProblems.forEach(function (product) {
                    if (product.problem) {
                        products.push({
                            id: product.model.id,
                            reason: product.problem
                        });
                    }
                });
                this.other.forEach(function (item) {
                    if (item) {
                        other.push(item)
                    }
                });

                return $http.post('/orden/' + this.token + '/calificacion', {
                    rating: 'bad',
                    order_products: products,
                    other: other,
                    comment: this.comment,
                    order_id: this.id
                })
                    .then(function (response) {
                        trackEvent('order_rated', {
                            order_id: this.id,
                            score: 0,
                            problem: this.comment
                        });
                        return response.data.message;
                    });
            };

            return Order;
        })

        .service('OrderFormat', function ($http, Product) {
            function OrderFormat(options) {
                this.title = options.rating.bad.title;
                this.productsTitle = options.rating.bad.products.title;
                this.other = options.rating.bad.other;
                this.products = options.rating.bad.products.order_products.products_list.map(function (item) {
                    return new Product(item)
                });
                this.availableProblems = options.rating.bad.products.order_products.available_problems;
                this.unavailableProblems = options.rating.bad.products.order_products.unavailable_problems;
                this.orderId = options.details.order_id;
            }

            OrderFormat.prototype.itemsWithProblems = function () {
                var items = 0;
                for (var index in this.products) {
                    if (!this.products.hasOwnProperty(index)) {
                        continue;
                    }
                    var problem = this.products[index].problem;
                    items += problem ? 1 : 0;
                }
                return items;
            };

            return OrderFormat;
        })


        .controller('HomeController', function ($scope, $state, $stateParams, Order) {
            $scope.isLoading = false;

            // Lottie animation
            loadAnimation('lottie-welcome', 'bienvenida.json', false, 'Welcome');

            $scope.order = new Order({{ $order->id }});
            $scope.order.orderTime = orderTime;
            $scope.rateAsOk = function () {
                $scope.isLoading = true;
                $scope.order.rateAsOkOrder()
                    .then(redirectToThankYouPage)
                    .catch(redirectToErrorPage);
            };

            function redirectToThankYouPage(result) {
                $scope.isLoading = false;
                var message = result instanceof Error ? result.message : result;
                $state.transitionTo('thank-you-page', {message: message});
            }

            function redirectToErrorPage(result) {
                var message = result instanceof Error ? result.message : result;
                $state.transitionTo('error-page', {message: message});
            }

            $scope.rateAsBad = function () {
                $state.go('choose-options', {orderId: $scope.order.id});
            };

            // Validar si el usuario ya calificó el pedido
            if (doneRate) $state.transitionTo('error-page', {message: 'error-page'});

            // Validar flag que indica cuando el usuario calificó positivamente desde el mail
            if ($stateParams.done === "yes" || $stateParams.done === true) $scope.rateAsOk();
        })

        .controller('ProblemsController', function ($scope, $mdDialog, $state, Order, problemsFormat) {
            // Validar si el usuario ya calificó el pedido
            if (doneRate) $state.transitionTo('error-page', {message: 'error-page'});
            
            $scope.order = new Order(problemsFormat.orderId);

            $scope.sendingForm = false;
            $scope.error = '';
            $scope.comments = '';
            $scope.productsProblems = [];
            $scope.list = [];
            $scope.productsTitle = problemsFormat.productsTitle;
            $scope.title = problemsFormat.title;
            $scope.others = problemsFormat.other;
            $scope.products = problemsFormat.products;
            $scope.avProblems = problemsFormat.availableProblems;
            $scope.notavProblems = problemsFormat.unavailableProblems;
            $scope.format = problemsFormat;

            $scope.validResponse = function (form) {
                form.$valid = true;
                form.$pristine = false;

                if (!$scope.order.numOtherValidItems() && !$scope.format.itemsWithProblems()) {
                    form.$valid = false;
                    form.other.$valid = false;
                    form.other.$pristine = false;
                    form.other.$error.required = true;
                } else {
                    form.other.$valid = true;
                }

                return form.$valid;
            };

            $scope.save = function () {
                if (!$scope.validResponse($scope.badRateForm) || $scope.sendingForm) {
                    return;
                }

                $scope.sendingForm = true;
                $scope.order.save($scope.products)
                    .then(function (message) {
                        $state.transitionTo('thank-you-page-bad-rate', {message: message});
                    })
                    .catch(function (message) {
                        $scope.sendingForm = false;
                        // TODO mostrar errores
                    });
            };

            $scope.openModal = function ($event) {
                $mdDialog.show({
                    clickOutsideToClose: true,
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: '/assets/templates/order_score/score-products-list.html',
                    controller: 'DialogProductsController'
                })
            }
        })
        .controller('DialogProductsController', function ($scope, $mdDialog) {
            $scope.anyDirty = function () {
                for (var index in this.products) {
                    if (!this.products.hasOwnProperty(index)) {
                        continue;
                    }
                    var product = this.products[index];
                    if (product.problem) {
                        return true;
                    }
                }

                return false;
            };

            $scope.hide = function () {
                $mdDialog.hide();
            };
        })
        .directive('loading', function () {
            return {
                restrict: 'E',
                replace: true,
                template: '<div class="container-loading"> <div class="lottie-loading" id="lottie-loading"></div> </div>',
                link: function(scope, element) {
                    // Lottie animation
                    loadAnimation('lottie-loading', 'cargando.json', true, 'Loading');
                }
            }
        })
        .directive('resolveLoader', function($transitions) {
            return {
                restrict: 'E',
                replace: true,
                template: '<div class="container-loading ng-hide"> <div class="lottie-loading" id="lottie-loader"></div> </div>',
                link: function(scope, element) {
                    $transitions.onStart({ to: 'choose-options' }, function(transition) {
                        // Lottie animation
                        lottie.destroy();
                        loadAnimation('lottie-loader', 'cargando.json', true, 'Loader');
                        element.removeClass('ng-hide');
                    });

                    $transitions.onSuccess({ to: 'choose-options' }, function(transition) {
                        element.addClass('ng-hide');
                    });
                }
            };
        });
</script>
</body>
</html>
