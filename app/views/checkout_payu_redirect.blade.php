@extends('layout_page')

@section('extra_assets')
    <title>Tu pedido</title>
    <meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,800" rel="stylesheet">
    <link href="{{ asset_url() }}css/order-success.css" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="pse-box">
                        <div class="loading-pse" id="lottie"></div>
                        <p class="redirect-text">En un momento te redirigiremos al banco</p>
                        <button id="btn-pse" class="btn-pse" onclick="window.location = '{{$url_pse_bank}}'">
                            <div class="btn-pse-text">Si en 5 segundos no recibes respuesta, haz clic aquí</div>
                            <img src="{{ asset_url() }}img/order_success/pse.svg" class="pse">
                        </button>
                        <script type="text/javascript">
                            setTimeout(function(){
                                $('#btn-pse').trigger('click');
                            }, 5000);
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($order_data['payment_method'] == 'Débito - PSE')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js" type="text/javascript"></script>
        <script type="text/javascript">

            var animation = bodymovin.loadAnimation({
                container: document.getElementById('lottie'), // Required
                path: '{{ asset_url() }}img/order_success/material_loader.json', // Required
                renderer: 'svg/canvas/html', // Required
                loop: true, // Optional
                autoplay: true, // Optional
                name: "loader", // Name for future reference. Optional.
            })
        </script>
    @endif
@stop
