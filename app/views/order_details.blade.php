<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <title>Pedido</title>
  <link href="{{ asset_url() }}/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css">
  <script type="text/javascript" src="{{ asset_url() }}/js/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/Leanplum_JavaScript-1.2.4/leanplum.min.js"></script>
  @include('common/analytics')
</head>

<body>
<div align="center" style="margin: 20px">
	<h3>Pedido {{ $order->reference }} en {{ $order->store_name }} - <b>{{ $order->status }}</b></h3><br>

	@if(Session::has('message'))
  	<div class="row">
		@if(Session::get('type') == 'success')
		<div class="alert alert-success alert-dismissable added-to-cart">
		    {{ Session::get('message') }}
		</div>
		@else
		<div class="alert alert-danger alert-dismissable">
		    {{ Session::get('message') }}
		</div>
		@endif
	</div>
	@endif

	<div class="form-group text-center">
	   <a href="{{ web_url() }}/user/order/{{ $order->reference }}/cart" class="btn btn-info add-order-to-cart">
		   Agregar este pedido al carrito
	   </a>
 	</div>
	<h3 class="muted"><hr></h3>
	<table class="table table-bordered table-striped dataTable">
	    <thead>
	        <tr>
				<th align="center">Imagen</th>
	            <th align="left">Producto</th>
	            <th align="center">Disponibilidad</th>
	            <th align="center">Cantidad</th>
	            <th align="right">Precio Unitario</th>
	            <th align="right">Total</th>
	        </tr>
	    </thead>
	    <tbody>
		@foreach($products as $product)
	        <tr>
				<td align="left"><img src="{{ $product->product_image_url }}" width="40" height="40"></td>
			    <td align="left">{{ $product->product_name.' '.$product->product_quantity.' '.$product->product_unit }}</td>
				<td align="center">
					@if ($product->fulfilment_status == 'Not Available' || $product->fulfilment_status == 'Replacement')
					<img src="{{ asset_url() }}/img/no_available.png" width="15" height="15">
					@else
						@if ($product->fulfilment_status == 'Fullfilled')
						<img src="{{ asset_url() }}/img/available.png" width="15" height="15">
						@else
						Pendiente
						@endif
					@endif
				</td>
				<td align="center">{{ $product->quantity }}</td>
				<td align="right"><span>$&nbsp;{{ number_format($product->price, 0, ',', '.') }}</span></td>
				<td align="right"><span>$&nbsp;{{ number_format($product->price * $product->quantity, 0, ',', '.') }}</span></td>
			</tr>
			@if(isset($product->gift) )
			<tr>
				<td align="left">
					<img src="{{ $product->gift->product_image_url }}" width="40" height="40">&nbsp;&nbsp;<img src="{{ web_url() }}/admin_asset/img/gift.png" height ="20px" title="Es un regalo">
				</td>
			    <td align="left">
					<small style="color:#D70F65">{{$product->gift->message}}</small>
					{{ $product->gift->name.' '.$product->gift->quantity.' '.$product->gift->unit }}
				</td>
				<td align="center">
					@if ($product->gift->fulfilment_status == 'Not Available' || $product->gift->fulfilment_status == 'Replacement')
					<img src="{{ asset_url() }}/img/no_available.png" width="15" height="15">
					@else
						@if ($product->gift->fulfilment_status == 'Fullfilled')
						<img src="{{ asset_url() }}/img/available.png" width="15" height="15">
						@else
						Pendiente
						@endif
					@endif
				</td>
				<td align="center">{{ $product->gift->quantity_cart }}</td>
				<td align="right"><span>$&nbsp;{{ number_format($product->gift->price, 0, ',', '.') }}</span></td>
				<td align="right"><span>$&nbsp;{{ number_format($product->gift->price * $product->gift->quantity_cart, 0, ',', '.') }}</span></td>
			</tr>
			@endif
			@if(isset($product->sampling) && $product->sampling->fulfilment_status == 'Fullfilled' )
			<tr>
				<td align="left">
					<img src="{{ $product->sampling->product_image_url }}" width="40" height="40">&nbsp;&nbsp;<img src="{{ web_url() }}/admin_asset/img/sample.png" height ="20px" title="Es una muestra">
				</td>
			    <td align="left">
			    	<small style="color:#D70F65">{{$product->sampling->message}}</small>
			    	{{ $product->sampling->name.' '.$product->sampling->quantity.' '.$product->sampling->unit }}<br>
			    </td>
				<td align="center">
					@if ($product->sampling->fulfilment_status == 'Not Available' || $product->sampling->fulfilment_status == 'Replacement')
					<img src="{{ asset_url() }}/img/no_available.png" width="15" height="15">
					@else
						@if ($product->sampling->fulfilment_status == 'Fullfilled')
						<img src="{{ asset_url() }}/img/available.png" width="15" height="15">
						@else
						Pendiente
						@endif
					@endif
				</td>
				<td align="center">{{ $product->sampling->quantity_cart }}</td>
				<td align="right"><span>$&nbsp;{{ number_format($product->sampling->price, 0, ',', '.') }}</span></td>
				<td align="right"><span>$&nbsp;{{ number_format($product->sampling->price * $product->sampling->quantity_cart, 0, ',', '.') }}</span></td>
			</tr>
			@endif
		 @endforeach
	    </tbody>
	    <tbody>
	        <tr>
	        	<td colspan="5" align="right">Total</td>
				<td align="right"><span>$&nbsp;{{ number_format($order->total_amount, 0, ',', '.') }}</span></td>
	    	</tr>
	    	<tr>
                <td colspan="5" align="right">Costo domicilio</td>
                <td align="right"><span>$&nbsp;{{ number_format($order->delivery_amount, 0, ',', '.') }}</span></td>
            </tr>
			<tr>
	        	<td colspan="5" align="right">Descuento</td>
				<td align="right"><span>$&nbsp;{{ number_format($order->discount_amount, 0, ',', '.') }}</span></td>
	    	</tr>
	        <tr>
	        	<td colspan="5" align="right"><strong>Total a pagar</strong></td>
				<td align="right"><strong><span>$&nbsp;{{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}</span></strong></td>
	    	</tr>
	    </tbody>
	</table>
 </div>

<script type="text/javascript">
	$(document).ready(function() {
        var confirmed = false;
        var accepted = false;

        $('body').on('click', '.add-order-to-cart', function (event) {
            if (!confirmed) {
                confirmed = confirm('Se eliminaran los productos que tenga en tu carrito en este momento. ¿Deseas continuar?');
            }

            if (!confirmed) {
                return false;
            }

            if (accepted) {
                return true;
            }

            var anchor = $(this);
            trackEvent('products_added_from_past_order', {}, function () {
                if (!accepted) {
                    accepted = true;
                }

                anchor.trigger('click');
            });

            event.preventDefault();
        });
    });
</script>

</body>

</html>