@extends('layout_page')

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{ asset_url() }}js/new-age.min.js"></script>
<!-- Custom Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
<link href="{{ asset_url() }}css/reset_password.css" rel="stylesheet">
<style type="text/css">
   body { background-color: white; }
   form.home input, form.login input, form.signup input { height: 34px; padding: 5px;}
   form.home { text-align: left !important; }
   label { font-weight: bold; }
</style>
<section>
<div class="container">
	 <div class="row">
        <div class="col-sm-6 image-remind">
	            <div class="header-content">
	                <div class="header-content-inner">
	                    <img class="ilustracionCiudad" src="{{ asset_url() }}img/reset_password/page.svg" alt="">
	                </div>
	            </div>
	        </div>
	    <div class="col-md-6 recuperar-cont">
			<h1>¿Olvidaste tu contraseña?</h1>
	        <p>Ingresa tu correo eléctronico para recuperar tu contraseña</p>
	        <form class="home remind" method="POST" action="{{ action('RemindersController@postRemind') }}">
	        	@if(Session::has('status'))
	              <div class="alert alert-info  col-md-12" role="alert">
	                &nbsp;&nbsp;{{ Session::get('status') }}
	              </div>
	            @endif
				<div class="@if(!Session::has('error')) unseen @endif alert alert-danger form-has-errors">@if(Session::has('error')) {{ Session::get('error') }} @endif
			  	</div>
	            <div class="form-group">
	                <label for="Password1"><span class="pinkLabel">*</span>Correo eléctronico</label>
	                <input type="text" class="form-control" name="email">
	            </div>
				<button type="submit" class="btn btn-default crearCuentaBTN">RECORDAR CONTRASEÑA</button>
	        </form>
	    </div>
	</div>
</div>
</section>
<script>
$('form.remind').on('submit', function(e) {
	e.preventDefault();
	var error = false;
	var form = this;

	$('input[type!="hidden"]', this).each(function() {
		if($(this).val().length == 0) {
			$(this).parent().addClass('has-error');
			error = "Debes ingresar tu correo electrónico";
		}else $(this).parent().removeClass('has-error');
	});

	if(error) {
		$('.form-has-errors').html(error).fadeIn();
		return false;
	}else{
		form.submit();
    	return true;
	}
});
</script>
@stop