<div class="stop-compare" id="stop-compare">
    <div class="header-close">
        <span class="button-close-stop-compare">x</span>
    </div>
    <div class="img-stop-compare">
        <img src="{{ asset_url() }}/img/parecompare.png" alt="Pare y compare" width="100%">
    </div>
</div>
<script>
    $(document).ready(function () {
        var pathname = window.location.pathname.split('/');
        if(pathname.length >= 4 && pathname[2] == 'domicilios-super-ahorro' && pathname[2] != ''){
            if (typeof(Storage) !== "undefined") {
                if (sessionStorage.departmentCount) {
                    sessionStorage.departmentCount = Number(sessionStorage.departmentCount)+1;
                    if(sessionStorage.departmentCount == 5){
                        setTimeout(function(){ $('#stop-compare').fadeIn(); }, 2000);
                        sessionStorage.clear();
                    }
                } else {
                    sessionStorage.departmentCount = 1;
                }
            }
        }

        //Cerrar pare y compare
        $('.button-close-stop-compare').on('click', function () {
            $('#stop-compare').fadeOut();
        });
    });
</script>
<style>
    .stop-compare{
        position: fixed;
        z-index: 999999;
        right: 20px;
        bottom: 20px;
        background: #FFF;
        color: black;
        border-radius: 10px;
        -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.67);
        -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.67);
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.67);
        display: none;
    }
    .header-close{
        text-align: right;
        margin: 5px 0 3px;
    }
    .button-close-stop-compare{
        margin-right: 20px;
        font-size: 14px;
        cursor: pointer;
        padding: 0px 8px 3px;
        background-color: rgba(0, 0, 0, 0.7);
        color: #FFF;
        border-radius: 50%;
    }
    .button-close-stop-compare:hover{
        font-weight: bold;
    }
    .img-stop-compare img{
        border-bottom-right-radius: 10px;
        border-bottom-left-radius: 10px;
    }
</style>
