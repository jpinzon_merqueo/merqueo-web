{{--

    Las variables necesarias para que se renderice son:
        $has_orders - bool
        $banners - Collection
--}}

@if (!$banners->isEmpty())
    <link href="{{ asset_url() }}/lib/slick/slick.css" rel="stylesheet" type="text/css">
    <link href="{{ asset_url() }}/lib/slick/slick-theme.css" rel="stylesheet" type="text/css">

    <style type="text/css">
        .banners{ margin:20px; }
        .slick-dots li.slick-active button::before{ color:#e91a58 !important; }
    </style>

    <div class="banners" style="display: none;">
        @foreach ($banners as $index => $banner)

            <?php $store_product = $banner->storeProduct ?>
            <?php $product = empty($store_product) ? null : $store_product->product ?>

            @if ($banner->isProduct() && empty($product))
                <?php continue ?>
            @endif

            <div class="main-banner-item image-item {{ snake_case(studly_case($banner->category)) }} {{ $banner->isProduct() ? "product-item product-list product-{$store_product->id}" : '' }}"
                 @if ($banner->isProductsGroup() || $banner->isInformative())
                    data-modal-name="{{ $banner->isProductsGroup() ? '#informative-banner-content-with-products' : '#informative-banner-content' }}"
                    data-open-action="{{ $banner->isProductsGroup() ? 'request' : 'open-modal' }}"
                @endif
                 data-type="{{ $banner->type }}"
                 data-banner-id="{{ $banner->id }}"
                 data-has-orders="{{ $has_orders }}"

                @if ($banner->isProduct())

                    <?php $has_special_price_warning = $store_product->has_quantity_special_price_stock ?>
                     data-product-id="{{ $store_product->id }}"
                     data-title="{{ $banner->title }}"
                     data-name="{{ $product->name }}"
                     data-shelf-id="{{ $store_product->shelf_id }}"
                     data-store="{{ $banner->storeProduct->store->name }}"
                     data-position="{{ $index }}"
                     data-type="{{ $banner->type }}"
                     data-has-orders="{{ $has_orders }}"
                     data-banner-id="{{ $banner->id }}">

                        <div style="text-align: center">
                            <div class="product-cart-counter">
                                <i class="icon-shopping-cart"></i>
                                <span class="count">0</span>
                            </div>

                            <div class="product-cart">
                                <span class="count"></span>
                                <span class="sub-text">EN CARRITO</span>
                            </div>

                            <div class="cart-less" data-id="{{ $store_product->id }}" data-warning="0"
                                 data-referrer="banner">
                                <span class="glyphicon glyphicon-minus"></span>
                            </div>

                            <div class="cart-add" data-id="{{ $store_product->id }}" data-warning="0"
                                 data-referrer="banner">
                                <span class="glyphicon glyphicon-plus"></span>
                            </div>
                        </div>
                @else
                    {{--Carácter que cierra el listado de atributos del banner. --}}
                    >
                    {{--Carácter que cierra el listado de atributos del banner. --}}
                @endif

                @if ($banner->isDeeplink())
                    <a href="{{ $banner->url }}"
                       target="{{ $banner->target }}"
                       style="text-decoration: none;">
                @endif

                    <div class="row">
                        <div class="image-cropped-container open-modal banner-image"
                             style="background-image: url('{{ $banner->image_web_url }}')">
                        </div>
                    </div>

                @if ($banner->isDeeplink())
                    </a>
                @endif

                @if ($banner->isProduct())
                    <div class="row">
                        <div class="col-md-12 divider row">
                            <div class="sell-of-info col-md-7 align-vertical-center">
                                @if (!empty($store_product->special_price))
                                    <span>Agrega y ahorra</span>
                                    <span>$ {{ number_format($store_product->price - $store_product->special_price, 0, ',', '.') }}</span>
                                @endif
                            </div>

                            <div class="col-md-5 row align-vertical-center">
                                <div class="cart-add-initial"
                                     data-id="{{ $store_product->id }}"
                                     data-name="{{ $product->name }}"
                                     data-price="{{ !empty($store_product->special_price) ? $store_product->special_price : $store_product->price }}"
                                     data-warning="{{ empty($store_product->shelf->has_warning) ? 0 : $store_product->shelf->has_warning }}"
                                     data-referrer="banners">

                                    <p>Agregar</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-12 divider">
                            <div class="sell-of-info col-md-7 align-vertical-center">
                                @if ($banner->isDeeplink())
                                    <span>{{ $banner->description }}</span>
                                @else
                                    <span>{{ $banner->content_title }}</span>
                                @endif
                            </div>

                            <div class="col-md-5 row align-vertical-center">
                                @if ($banner->isDeeplink())
                                    <a href="{{ $banner->url }}"
                                       target="{{ $banner->target }}"
                                       style="text-decoration: none;">
                                @endif

                                <div class="merqueo-common-button open-modal">
                                    <p>Ver más</p>
                                </div>

                                @if ($banner->isDeeplink())
                                    </a>
                                @endif
                            </div>
                            {{--
                                Contenido del banner informativo. Este contenido se imprime como
                                metadatos de un div para que posteriormente sea tomado
                                por medio de javascript y sea inyectado
                                en el modal principal.
                            --}}
                            <div style="display: none;"
                                 class="modal-content-node"
                                 data-banner-image-src="{{ $banner->image_app_modal_url ?: $banner->image_web_url }}"
                                 data-banner-title="{{ $banner->content_title }}"
                                 data-banner-content="{{ addslashes(htmlentities($banner->content ?: '')) }}">
                            </div>
                            {{--Fin Contenido del banner informativo--}}
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
    </div>

    {{--Ventana modal que muestra los banners informativos--}}
    <div id="informative-banner-content" class="modal fade modal-main-banner" role="dialog" tabindex='-1'>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1></h1>
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="{{ asset_url() }}/img/main_banner/close-button.png" alt="Cerrar">
                    </button>
                </div>
                <div class="modal-body body-banner">
                    <img src="" alt="" class="primary-background">
                </div>
                <div class="modal-body banner-modal-content-wrap" style="margin: 20px 0;">
                    <section class="banner-modal-content"></section>
                </div>
            </div>
        </div>
    </div>
    {{--Fin ventana modal que muestra los banners informativos--}}

    <div id="informative-banner-content-with-products"
         class="modal fade modal-main-banner"
         role="dialog"
         tabindex='-1'>
    </div>

    <script type="text/javascript" src="{{ asset_url() }}/lib/slick/slick.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var active_products = [];
            var tmp_products = [];

            $('.open-modal').on('click', function (event) {
                var container = matchNodeClass('main-banner-item', $(this).parent());

                if (!container.length) {
                    return false;
                }

                var modalName = container.data('modal-name');
                var modalContainer = $(modalName);

                if (container.data('open-action') === 'request') {
                    event.preventDefault();
                    enableLoader();
                    var bannerUrl = '{{ action('banner.detail', ':replace') }}'
                        .replace(':replace', container.data('banner-id'));

                    $.ajax({
                        method: 'GET',
                        url: bannerUrl,
                    })
                        .done(function (response) {
                            hideLoader();
                            modalContainer.html(response);
                            modalContainer.modal('show');

                            var list = $(modalName + ' .banner-informative-list');
                            list.hide();
                            setTimeout(reloadSlick(modalName, list), 300);
                        })
                        .fail(function (response) {
                            hideLoader();
                            alert(
                                'Ocurrió un error al intentar obtener los detalles del banner, ' +
                                'intenta de nuevo o refresca la ventana del navegador'
                            );
                        });

                    return false;
                }

                var contentNode = container.find('.modal-content-node');
                var imgSrc = contentNode.data('banner-image-src');
                var title = contentNode.data('banner-title');
                var content = contentNode.data('banner-content');
                var productList = contentNode.html();

                event.preventDefault();

                modalContainer.find('.primary-background').prop('src', imgSrc);
                modalContainer.find('h1').html(title);
                modalContainer.find('.banner-modal-content').html(content);
                modalContainer.find('.banner-modal-product-list').html(productList);
                modalContainer.modal('show');

                var list = $(modalName + ' .banner-informative-list');

                if (!list.length) {
                    return;
                }

                $('.banners-sell-off-title').text(
                    list.data('is-promo-type')
                        ? 'Productos en promoción'
                        : 'Productos'
                );

                modalContainer.find('.modal-body').append('<img src="/assets/img/rolling.svg" alt="" class="rolling-loader">');
                list.css({display: 'none'});
                setTimeout(reloadSlick(modalName, list), 300);
            });

            function reloadSlick(modalName, list) {
                return function () {
                    list.not('.slick-initialized').slick({
                        infinite: false,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    });

                    list.find('img').each(function (index, element) {
                        var $element = $(element);
                        if ($element.data('original')) {
                            $element.prop('src', $element.data('original'))
                        }
                    });

                    list.show();
                    $('.rolling-loader').hide();
                    list[0].slick.refresh(true);
                    $(window).trigger('resize');

                    var maxHeight = 0;
                    var productColumns = list.find('.product-column');
                    productColumns.each(function (index, element) {
                        var $element = $(element);
                        var elementHeight = parseInt($element.height(), 10);
                        if (elementHeight > maxHeight) {
                            maxHeight = elementHeight;
                        }
                    });
                    list.find('.product-column').height(maxHeight).find('.product-list').height(maxHeight);
                }
            }

            $('.banners').show();

            $('.banners').on('init', function (event, slick) {
                $('.slick-active').each(function (index, el) {
                    let $banner = $(this);
                    trackEvent('banner_viewed', {
                        bannerId: $banner.data('banner-id') || null,
                        title: $banner.data('title') || null,
                        type: $banner.data('type') || null,
                        position: $banner.data('position') || null,
                    });
                    active_products.push($banner.data('product-id'));
                });
            });

            $('.banners').on('afterChange', function (event, currentSlide) {
                tmp_products = [];
                $('.slick-active').each(function (index, el) {
                    var $banner = $(this);
                    var tmp_id = $banner.data('product-id');
                    tmp_products.push($banner.data('product-id'));
                    if (active_products.indexOf(tmp_id) === -1 && $('#cart-desc').is(':hidden')) {
                        trackEvent('banner_viewed', {
                            bannerId: $banner.data('banner-id') || null,
                            title: $banner.data('title') || null,
                            type: $banner.data('type') || null,
                            position: $banner.data('position') || null,
                        });
                    }
                });
                active_products = tmp_products;
            });

            $('.banners').slick({
                dots: false,
                arrows: true,
                infinite: true,
                speed: 500,
                autoplay: false,
                autoplaySpeed: 5500,
                variableWidth: true,
                adaptiveHeight: true,
                slidesToScroll: 1,
                slidesToShow: 3,
                // slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $('.banners').slick('play');

            $('body').on('click', '.slick-slide .name-link, .main-banner-item.image-item', function (event) {
                event.preventDefault();
                var $banner = $(this).parents('.slick-slide');
                var anchor = $(this);
                if (anchor[0].nodeName.toLowerCase() !== 'a') {
                    anchor = anchor.find('a');
                }
                trackEvent('banner_clicked', {
                    title: $banner.data('title') || null,
                    type: $banner.data('type') || null,
                    position: $banner.data('position') || null,
                    name: $banner.data('name') || null,
                    shelfId: $banner.data('shelf-id') || null,
                    storeProductId: $banner.data('product-id') || null,
                    bannerId: $banner.data('banner-id') || null
                }, function () {
                    var headerReference = anchor.attr("href");
                    if (headerReference) {
                        window.location = headerReference;
                    }
                });
            });
        });

        function matchNodeClass(expected, currentNode) {
            if (!currentNode) {
                return null;
            }

            if (currentNode.hasClass(expected)) {
                return currentNode;
            }

            return matchNodeClass(expected, currentNode.parent());
        }
    </script>
@endif
