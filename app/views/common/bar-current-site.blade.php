<!-- Barra (redireccionar a página nueva) -->
<div class="bar-current-to-new">
    <a href="{{ route('newSite.migrate') }}" onclick="barClick()">
        Cambiamos para ti nuestra página web,
        <b>pruébala</b>
    </a>

    <span class="bar-close" onclick="hideBar(false)">
        <img class="bar-btn-close" src="{{ asset_url() }}img/chevron-cerrar.svg" alt="icon-cerrar" title="Cerrar">
    </span>
</div>
<script>
    function hideBar(navBar) {
        $('#main-content-bctn, #navbar-bctn, #aside-departament').css('margin-top', 0);
        $('.bar-current-to-new').hide();
        window.localStorage.setItem('nav-bar', navBar);
    }

    function barClick(){
      var storageNewWeb = JSON.parse(localStorage.getItem("data"));
      if(storageNewWeb && "user" in storageNewWeb){
        storageNewWeb.user.webVersion = "v2";
        localStorage.setItem("data", JSON.stringify(storageNewWeb));
      }
    }

    function redirectToNew(){
      $(".bar-current-to-new").click();
      window.location.href = "{{ route('newSite.migrate') }}";
    }

    function verifyCurrentWeb(){
      var storageNewWeb = JSON.parse(localStorage.getItem("data"));
      if(storageNewWeb && "user" in storageNewWeb && 
        "webVersion" in storageNewWeb.user 
        && storageNewWeb.user.webVersion == "v2"){
        redirectToNew();
      }
    }

    $().ready(function() {
        var navBar = JSON.parse(window.localStorage.getItem('nav-bar'));
        verifyCurrentWeb();

        console.log('NavBar: ', navBar);
        if (!navBar) {
            hideBar(navBar);
        }
    });
</script>

<style>
/* Barra (redireccionar a página nueva) */
.bar-current-to-new {
  cursor: pointer;
  width: 100%;
  height: 50px;  
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100205;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
  background: linear-gradient(268deg, #e400d0, #e4008d);
  text-align: center;
  /*padding-left: 130px;*/
  overflow: hidden;
}

.bar-current-to-new a {
  display: inline-block;
  width: 100%;
  font-size: 17px;
  color: white !important;
  padding: 0;
  padding-top: 12.5px;
  margin: 0 !important;
}

.bar-current-to-new a:hover {
  text-decoration: none;
  color: #f6f6f6;
}

.bar-current-to-new b {
  text-decoration: underline;
}

.bar-current-to-new .bar-close {
  position: absolute;
  right: 0;
    top: 0;
  cursor: pointer;
  float: right;
  width: 130px;
  height: 50px;
  background: #ca027d;
  background: -moz-linear-gradient(-238deg, transparent 50%, #333d53 50%, #ca027d 0%);
  background: -webkit-linear-gradient(-238deg, transparent 50%, #333d53 50%, #ca027d 0%);
  background: linear-gradient(-238deg, transparent 50%, #333d53 50%, #ca027d 0%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
}

.bar-close:after {
  position: absolute;
  content: "";
  width: 0;
  height: 55px;
  transform: rotate(32deg);
  box-shadow: 0 0 6px .5px black;
}

.bar-btn-close {
  position: absolute;
  right: 20px;
  top: 15px;
}

#main-content-bctn, #navbar-bctn, #aside-departament  {
  margin-top: 50px;
}

.contini_home {
    margin-top: 50px !important;
}
</style>
