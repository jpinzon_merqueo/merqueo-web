<script src="https://sdk.accountkit.com/es_ES/sdk.js"></script>

<script>
    function _loginCallback(phoneNumber, callback) {
        return function (response) {
            if (response.status === 'PARTIALLY_AUTHENTICATED') {
                var code = response.code;
                var csrf = response.state;

                $.ajax({
                    url: '{{ route('frontUser.cellphoneValidation') }}',
                    type: 'POST',
                    data: {code: code},
                    dataType: 'json',
                })
                    .done(function (response) {
                        if (response.status) {
                            callback(null, response.result);
                        } else if (response.result) {
                            var error = new FormValidationError(response.message);
                            for (var index in response.result) {
                                if (!response.result.hasOwnProperty(index)) {
                                    continue;
                                }

                                for (var item of response.result[index]) {
                                    error.errors.push(new Error(item));
                                }
                            }
                            callback(error);
                        } else {
                            callback(new Error(response.message));
                        }
                    });
            }
            else if (response.status === 'NOT_AUTHENTICATED') {
                callback(new Error('Error al validar las credenciales.'));
            }
            else if (response.status === 'BAD_PARAMS') {
                callback(new Error('Parámetros invalidos.'));
            }
        }
    }

    AccountKit_OnInteractive = function () {
        AccountKit.init({
            appId: "{{ Config::get("app.facebook_app_id") }}",
            state: "{{ Cache::get('js_version_number') }}",
            version: "v1.0",
            fbAppEventsEnabled: true,
        });
    };

    /**
     * @param phoneNumber
     * @param {Function} callback
     * @returns {Promise}
     */
    function askValidation(phoneNumber, callback) {
        var params = {
            countryCode: '+57',
            phoneNumber: phoneNumber || ''
        };

        AccountKit.login('PHONE', params, _loginCallback(phoneNumber, callback));
    }

    function FormValidationError(message) {
        Error.call(this, message);
        this.errors = [];
    }

    FormValidationError.super_ = Error.constructor;
    FormValidationError.prototype = Object.create(Error.prototype, {
        constructor: {
            value: FormValidationError,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });

    FormValidationError.prototype.push = function (error) {
        this.errors.push(error);
    };

    FormValidationError.prototype.getErrorDetails = function () {
        var message = [];
        for (var i = 0; i < this.errors.length; i++) {
            message.push(this.errors[i].message);
        }
        
        return message;
    };
</script>
