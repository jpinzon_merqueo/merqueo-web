<script type="text/javascript">
    var MERQUEO_VERSION = '0.0.1';
    <?php $whichBrowser = new WhichBrowser\Parser(getallheaders()) ?>

    function getOs() {
        return '{{ $whichBrowser->os->getName() }}';
    }

    function getPlatform() {
        return '{{ $whichBrowser->device->type }}';
    }

    function pageName() {
    if (typeof isMobileSplash !== 'undefined') {
      return 'Mobile Splash';
    }

    var pageMapping = [
      {
        pattern: /\/.+\/domicilios-[^\/]+\/[^\/]+\/[^\/]+\/[^\/]+$/,
        name: 'Product'
      },
      {
        pattern: /\/.+\/domicilios-[^\/]+\/[^\/]+\/[^\/]+$/,
        name: 'Shelf'
      },
      {
        pattern: /\/.+\/domicilios-[^\/]+\/[^\/]+$/,
        name: 'Department'
      },
      {
        pattern: /\/.+\/domicilios-[^\/]+$/,
        name: 'Store'
      },
      {
        pattern: /\/checkout\/orden_confirmada\/.+/,
        name: 'Order Completed'
      },
      {
        pattern: '/checkout',
        name: 'Checkout'
      },
      {
        pattern: /\/mi-cuenta/,
        name: 'Account'
      },
      {
        pattern: /\/registro\/.+/,
        name: 'Referral Sign Up'
      },
      {
        pattern: '/registro',
        name: 'Sign Up'
      },
      {
        pattern: /\/user\/order\/.+$/,
        name: 'Order Details'
      },
      {
        pattern: /\/bogota$/,
        name: 'Store Selection'
      },
      {
        pattern: /\/medellin$/,
        name: 'Store Selection'
      },
      {
        pattern: /\/$/,
        name: 'Store Selection'
      },
      {
        pattern: /orden_confirmada/,
        name: 'Checkout confirmation'
      },
    ];

    var path = window.location.pathname;

    for (var key in pageMapping) {
      var page = pageMapping[key];

      if (path.match(page.pattern)) {
        return page.name;
      }
    }
  }

  function trackEvent(eventName, data, callback) {
      data = Object.assign({}, getCommonProperties(), (data || {}));
      Leanplum.track(eventName, data);
      customTrack(eventName, data, callback);
  }

  function customTrack(eventName, data, callback) {
      $.ajax({
              method: 'POST',
              url: '{{ \Config::get('app.events.big_query_service.url') }}',
              data: Object.assign({}, data, {event: eventName})
          })
          .always(function () {
              if (callback instanceof Function) {
                  callback();
              }
          });
  }

  function getCommonProperties() {
      var now = new Date();

      return {
        date: now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds(),
        deviceId: Leanplum._deviceId,
        os: getOs(),
        platform: getPlatform(),
        appVersion: MERQUEO_VERSION,
        login: {{ Auth::check() ? 1 : 0}},
        warehouseId: {{ Session::get('warehouse_id') ?: 0 }},
        @if (Session::get('zone_id'))
          zoneId: {{ Session::get('zone_id') }},
        @endif
        @if (Auth::check())
            userId: {{ Auth::user()->id }},
        @endif
        hasOrdered: {{ Session::has('last_order_date') ? 1 : 0 }}
      };
  }

  @if (Auth::check())
      var variables = {{ json_encode(Auth::user()->getAnalyticsBasicProperties()) }};
  @else
    var variables = {};
  @endif

  @if (Config::get('app.debug'))
    Leanplum.setAppIdForDevelopmentMode('{{ \Config::get('leanplum.app_id') }}', '{{ \Config::get('leanplum.key') }}');
  @else
    Leanplum.setAppIdForProductionMode('{{ \Config::get('leanplum.app_id') }}', '{{ \Config::get('leanplum.key') }}');
  @endif

  Leanplum.setVariables(variables);
  Leanplum.start({{ Auth::check() ? Auth::user()->id : ''}});

  var isWebPushSupported = Leanplum.isWebPushSupported();
  if(isWebPushSupported){
    Leanplum.isWebPushSubscribed()
      .then(function(subscriptionStatus) {
        if(isWebPushSupported && !subscriptionStatus){
          return Leanplum.registerForWebPush('/leanplum-service-worker.min.js')
        }

        return subscriptionStatus;
      })
      .then(function(subscriptionStatus) {
        console.log('Subscription status: %s', subscriptionStatus);
      });
  }

  @if (Session::has('events'))
    @foreach(Session::pull('events') as $eventName => $props)
      @if ($eventName !== 'multi')
          var data = {{ json_encode($props) }};
          var eventName = '{{ $eventName }}';
          trackEvent(eventName, data);
      @elseif (!empty($props) && is_array($props))
        @foreach($props as $index => $eventData)
            @if (empty(array_values($eventData ?: [])[0]) || empty(array_keys($eventData ?: [])[0]))
                <?php continue ?>
            @endif
            var data = {{ json_encode(array_values($eventData)[0]) }};
            var eventName = '{{ array_keys($eventData)[0] }}';
            trackEvent(eventName, data);
        @endforeach
      @endif
    @endforeach
  @endif
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','{{ Config::get('app.google_tag_manager.container_id') }}');</script>
<!-- End Google Tag Manager -->
