@extends('layout')

@section('title'){{ $product->name }} en {{ $store->store_name }} - Merqueo.com @stop
@section('description'){{ $product->name }} en {{ $store->name }}. Puedes pagar con tarjeta o en efectivo contra entrega. @stop

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="{{ asset_url() }}/lib/slick/slick.css" rel="stylesheet" type="text/css">
    <link href="{{ asset_url() }}/lib/slick/slick-theme.css" rel="stylesheet" type="text/css">

    <div class="col-md-12 content_list_data" style="margin-top: 125px;">
        <script>
            var content_ids = ['{{ $product->id }}'];
            var content_name = '{{ $product->name }} {{ $product->quantity }} {{ $product->unit }}';
            var value = @if(empty($product->special_price)){{$product->price}}@else{{$product->special_price}}@endif;
        </script>
        <script type="application/ld+json">
          {
            "@context": "http://schema.org",
            "@type": "Product",
            "image": "{{ $product->image_large_url }}",
            "name": "{{ $product->name }}",
            @if (!empty($product->description))
                "description": "{{ $product->description }}",
            @endif
            "offers": {
              "@type": "Offer",
              "priceCurrency": "COP",
              "price": "{{ $product->price }}",
              "availability": "http://schema.org/InStock",
              "seller": {
                "@type": "Organization",
                "name": "Merqueo"
              }
            }
          }


        </script>

        <ol class="breadcrumb" data-catrel="{{ $store->id }}">
            {{ Breadcrumb::render('>', $store->store_name, 2, $store->city_name) }}
        </ol>

        <div class="row col-md-12 col-md-offset-1 product-view product-{{ $product->id }} {{ $product->quantity_special_price_stock ? 'last-units' : '' }} {{ $product->quantity_special_price_stock && $product->quantity_special_price_stock < 11 ? 'units-warning' : '' }}">
            <div class="col-md-7 text-center no-padding {{ $product_disable ? 'product-disabled' : '' }}   ">
                <div class="product-cart-counter">
                    <i class="icon-shopping-cart"></i>
                    <span class="count"></span>
                </div>
                <div class="image-container {{ !empty($variation_key['provider-labels']) && $variation_key['provider-labels']->showVariant($product) ? 'show-jb' : '' }}">
                    @if (count($product->product->images))
                        <div class="row">
                            <div class="primary-slider col-sm-12">
                                @foreach($product->product->images as $image)
                                    <div class="modal-product-image-gallery">
                                        <img src="{{ $image->image_large_url }}"
                                             data-zoom-image="{{ $image->image_large_url }}"
                                             class="use-zoom">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="vertical-navigation-slider" style="width: 90%">
                                    @foreach($product->product->images as $image)
                                        <img src="{{ $image->image_large_url }}">
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @else
                        <img class="modal-product-image" src="{{ $product->image_large_url }}"
                             data-zoom-image="{{ $product->image_large_url }}">
                    @endif
                </div>
            </div>
            <div class="col-md-5 {{ $product_disable ? 'product-disabled' : '' }}">
                <h1 class="product-title mq-new-product-title" style="margin-bottom: 30px">{{ $product->name }}</h1>
                <div class="mq-best-detail">
                    <h3 class="product-quantity">{{ $product->quantity }} {{ $product->unit }}</h3>
                    @if($product['is_best_price'])
                        <div class="product-best-detail">
                            <div id="ico_best-detail">
                                <img src="{{ asset_url() }}img/icon_best.png" alt="best-product">
                            </div>
                            <div id="text_best-detail">
                                <h6>Mejor precio garantizado</h6>
                            </div>
                        </div>
                    @endif
                </div>
                @if ($product['delivery_discount_amount'])
                    <div class="delivery-discount">
                        <i class="fa"><img src="{{ asset_url() }}img/truck-icon.png" alt="truck"></i><b>Ahorra
                            ${{ number_format($product['delivery_discount_amount'], 0, ',', '.') }} en el domicilio</b>
                    </div>
                @endif
                <div class="col-md-12" style="padding-left: 0">
                    <div class="price-container mq-price-container">
                        @if ($product->special_price > -1)
                            <span class="product-price text-strike">
                                {{ currency_format($product->price) }}
                            </span>
                            <span class="product-price product-special-price">
                                {{ currency_format($product->special_price) }}
                            </span>
                        @else
                            <span class="product-price">{{ currency_format($product->price) }}</span>
                        @endif
                        @if(isset($product->pum))
                            <p class="product-pum">
                                @if($product->type == 'Simple')
                                    {{ $product->pum }}
                                @elseif($product->type == 'Agrupado')
                                    @foreach(explode('|', $product->pum) as $pum)
                                        <small>{{ $pum }} </small><br/>
                                    @endforeach
                                @endif
                            </p>
                        @endif
                        @if ($product->special_price > -1)
                            <div class="product-total-discount">
                                <span>Ahorro: {{currency_format($product->price - $product->special_price)}}</span>
                                <br>
                            </div>
                        @endif
                        @if($product->quantity_special_price)
                            <div class="product-label-info">
                                <span>Máximo {{$product->quantity_special_price}}
                                    unidad{{$product->quantity_special_price > 1 ? "es" : ""}} en descuento</span>
                                <br>
                            </div>
                        @endif
                        @if(!empty($product->allied_store_id))
                            <div class="product-label-info">
                                <span>Entrega en 3 días hábiles</span>
                            </div>
                        @endif
                        @if (!empty($variation_key['social-proof']) && $variation_key['social-proof']->showVariant($product->provider_id))
                            <div class="social-proof">
                                Más de
                                <span class="product-price">
                                    {{ \AbTesting\Optimizely\Experiments\SocialProof::showOnRange($product->social_proofs_total) }}
                                </span>
                                personas han comprado este producto la última semana.
                            </div>
                        @endif
                    </div>

                    <span id="modal-add-button">
                      <a class="btn btn-success fr cart-add-initial-detail product-add-to-cart-button mq-product-add-to-cart"
                         data-id="{{ $product->id }}"
                         data-name="{{ $product->name }}"
                         data-price="{{ empty($product->special_price) ? $product->price : $product->special_price }}"
                         onclick="modify_cart({{ $product->id }}, {{ $product->warning }}, 1, 0, '{{ \Input::get('referer') === 'banners' ? 'banners' : 'detalle' }}', event)">
                          Agregar al carrito
                      </a>
                    </span>
                    <div class="row">
                        @if(!empty($product->allied_store_id) && !empty($product->guarantee_policies))
                            <div class="col-xs-12 {{$product['is_best_price'] ? 'col-sm-5' : 'col-sm-12'}} mq-message-product">
                                <div class="mq-message-product-data">
                                    <img src="{{ asset_url() }}img/warranty.png" alt="Mejor precio garantizado">
                                    <p>¿Deseas obtener más información sobre las políticas de garantía y devoluciones?</p>
                                </div>
                                <a href="{{$product->guarantee_policies}}" target="_blank">Ver información</a>
                            </div>
                        @endif
                        @if($product['is_best_price'])
                            <div class="col-xs-12 col-sm-7 mq-message-product">
                                <div class="mq-message-product-data">
                                    <img src="{{ asset_url() }}img/star.png" alt="Mejor precio garantizado">
                                    <p>Si encuentras este producto a un precio más bajo en otro supermercado, te
                                        devolvemos el dinero</p>
                                </div>
                                <a href="/terminos#mejor-precio" target="_blank">Ver más...</a>
                            </div>
                        @endif
                    </div>
                    <div class="msn_content_product">
                        <div class="cotenttextsrea">
                            <textarea name="text_area_msn" id="text_area_msn"
                                      placeholder="¿Tienes alguna instrucción especial?"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            @if($product_disable)
            <div class="col-md-7 text-center ">
                <h1>Producto no disponible</h1>
            </div>
            @endif
        </div>

        @if (!empty($product->description) || !empty($product->nutrition_facts))
            <div class="row col-md-12 col-md-offset-1 product-view">
                <div class="col-md-12">
                    @if (!empty($product->test_description))
                        {{ $product->test_description }}
                    @elseif (!empty($product->description))
                        <h4 class="product-content-header">Descripción</h4>
                        <div class="product-description">{{ $product->description }}</div>
                    @endif
                    @if (!empty($product->nutrition_facts))
                        <h4 class="product-content-header">Información Nutricional</h4>
                        <p id="product-nut">{{ $product->nutrition_facts }}</p>
                    @endif
                </div>
            </div>
        @endif

        @if (count($product->product->details))
            <div class="row col-md-12 col-md-offset-1 product-view" style="margin-top: 60px;">
                <div class="col-md-12">
                    <h4 class="product-content-header">Especificaciones</h4>

                    <ul class="product-details">
                        @foreach ($product->product->details as $detail)
                            @if ($detail->type === ProductDetail::SPECIFICATION)
                                <li class="row">
                                    <div class="col-sm-6">{{ $detail->name }}</div>
                                    <div class="col-sm-6">{{ $detail->description }}</div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        @if (count($related_products))
            <div class="mdl-cell mdl-cell--12-col relatedproduct">
                <div class="titlerelated">
                    <h3>Productos Relacionados</h3>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {{ View::make('elements.products-list', ['products' => $related_products, 'store' => $store, 'referrer' => 'detalle']) }}
                    <div class="product-column-link column text-center">
                        <div class="product-list">
                            <a href="{{ route('frontStoreShelf.single_shelf', ['city_slug' => $store['city_slug'], 'store_slug' => $store['slug'], 'department_slug' => $department->slug, 'shelf_slug' => $shelf->slug ]) }}">
                                <div class="col-md-12">
                                    <div class="contenedor_items ">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        <p>Ver más</p>
                                        <br><span>{{ $shelf['name']}} </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <script type="text/javascript" src="{{ asset_url() }}/lib/slick/slick.js"></script>
        <script>
            $(document).ready(function () {
                var slider = $('.primary-slider');
                var descriptionState = true;

                $('.modal-product-image').elevateZoom({
                    borderSize: 2,
                    zoomWindowHeight: 390,
                    zoomWindowWidth: 390,
                    scrollZoom: true
                });

                slider.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.vertical-navigation-slider'
                });

                slider.on('afterChange', function (slider, slick, index) {
                    $('.zoomContainer').remove();
                    $('.primary-slider .slick-active img').elevateZoom({
                        borderSize: 2,
                        zoomWindowHeight: 390,
                        zoomWindowWidth: 390,
                        scrollZoom: true,
                    });
                });

                slider.trigger('afterChange');

                $('.vertical-navigation-slider').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.primary-slider',
                    dots: false,
                    focusOnSelect: true,
                    vertical: false,
                    variableWidth: true,
                    infinite: false,
                    prevArrow: '<i class="fa fa-angle-left slick-prev" style="margin-right:0!important"></i>',
                    nextArrow: '<i class="fa fa-angle-right slick-next" style="margin-right:0!important"></i>'
                });

                $('.more-detail').on('click', function () {
                    var listItem = $(this);
                    $('.product-details li.toggle').slideToggle();
                    listItem.text(listItem.text() === 'Más detalles' ? 'Ocultar detalles' : 'Más detalles');
                });

                $('.more-description').on('click', function () {
                    var listItem = $(this);
                    $('.resume-description')[descriptionState ? 'hide' : 'show']();
                    $('.full-description')[descriptionState ? 'show' : 'hide']();
                    listItem.text(listItem.text() === 'Más detalles' ? 'Ocultar detalles' : 'Más detalles');
                    descriptionState = !descriptionState;
                });

                $('body').on('mouseenter', '.zoomLens', function () {
                    var index = $('.vertical-navigation-slider .slick-current').data('slick-index');
                    var attributes = {
                        imagePosition: (index || 0) + 1,
                        department: '{{ $product->department_name }}',
                        departmentId: {{ $product->department_id }},
                        shelf: '{{ $product->shelf_name }}',
                        shelfId: {{ $product->shelf_id }},
                        storeProductId: {{ $product->id }},
                        name: '{{ $product->name }}',
                        price: {{ $product->price }},
                        specialPrice: {{ $product->special_price ?: 0 }},
                        bestPriceGuaranteed: {{ $product->is_best_price ? 1 : 0 }}
                    };

                    trackEvent('image_viewed', attributes);
                })
            });

            function showProductAdded() {
                if ($('.sd-cart-pro-li #product-{{ $product->id }}').length) {
                    $('.product-view .product-cart-counter').show();
                } else {
                    $('.product-view .product-cart-counter').hide();
                }
            }
        </script>
    </div>
@stop
