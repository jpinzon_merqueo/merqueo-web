@extends('layout_home')

@section('content')

	<div class="content_faq container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 content_items">
				<div class="col-md-12">
					<h1>Preguntas Frecuentes</h1>
				</div>
				<div class="row cont_data">
					<div class="col-md-10 title">Antes de realizar un pedido</div>
					<div class="col-md-5 cont_data_left">
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p1.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Que es Merqueo.com?</h2>
							</div>
							<div class="cont_text_full">
								<p>Somos el supermercado online con los mejores precios, estamos 100% comprometidos con el ahorro. Realizamos la entrega de tu pedido hasta la puesta de tu casa, ayudándote a economizar tiempo y dinero; puedes pedir con nosotros a través de web, dispositivos móviles android y iOS.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p6.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cómo puedo redimir mi cupón?</h2>
							</div>
							<div class="cont_text_full">
								<p>Al momento de diligenciar tus datos personales y hora de entrega en el formulario de registro, vas a encontrar la opción de “Redimir Cupón”, allí podrás ingresar tu Código.</p>
							</div>
						</div>
					<!--div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p3.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Con cuáles tiendas y supermercados tiene alianza Merqueo?</h2>
						</div>
						<div class="cont_text_full">
							<p>En Merqueo.com queremos ofrecerte siempre la mayor cantidad de opciones en tiendas y supermercados para que puedas elegir, en el momento contamos con Surtifruver, Colsubsidio, Locatel, Agrocampo, La 14, Pricesmart, Gastronomy Market.<br />Adem&aacute;s de estas, contamos con un servicio independiente de tiendas de barrio para conseguir los productos que necesites como son Merqueo Express y Merqueo S&uacute;per. &nbsp;&nbsp; &nbsp;&nbsp;</p>
						</div>
					</div-->
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p5.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cómo funcionan las promociones?</h2>
							</div>
							<div class="cont_text_full">
								<p>En Merqueo.com tratamos de ofrecerte siempre las mejores promociones, por eso tenemos gran variedad para ti. Sin embargo, recuerda que sólo puedes pedir de a 1 unidad por cada producto que ofertamos; igualmente te recordamos que debes esperar al menos 5 días calendario para volverlo a solicitar. Te invitamos a tener en cuenta nuestros términos y condiciones de uso</p>
							</div>
						</div>
					</div>

					<div class="col-md-1 cont_divi">
						<div class="cont_div_style"></div>
					</div>
					<div class="col-md-5 cont_divi_right">
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p2.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿En cuáles ciudades tiene presencia Merqueo.com?</h2>
							</div>
							<div class="cont_text_full">
								<p>Merqueo.com tiene presencia en Bogotá y Medellín, actualmente estamos trabajando fuertemente para ampliar nuestra cobertura a toda Colombia.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p4.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cuáles son los tiempos de entrega de Merqueo.com?</h2>
							</div>
							<div class="cont_text_full">
								<p>
									Nuestros horarios de entrega son tres: 07:00am - 12:00m ; 12:00m - 05:00pm ; 05:00pm - 10:00pm; recuerda que no operamos con un modelo de entregas para el mismo día de tu solicitud, es de suma importancia que realices tu orden con al menos 24 horas de anterioridad del horario en el que esperas recibirla. (aplica para Bogotá y Medellín)*
								</p>
								<p>
									Trabajamos de Lunes a Domingo realizando entregas dentro de nuestras zonas de cobertura en los horarios ya indicados en esta subsección. (aplica para Bogotá y Medellín)*
								</p>
							</div>
						</div>

					</div>
				</div>

				<div class="row cont_data">
					<div class="col-md-10 title">Realizando mi pedido</div>
					<div class="col-md-5 cont_data_left">
					<!-- <div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p7.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Cómo realizo un pedido?</h2>
						</div>
						<div class="cont_text_full">
							<iframe  style="width:100%; height:100%; min-height: 265px;" src="https://www.youtube.com/embed/RiEe891rCxU" frameborder="0" allowfullscreen></iframe>
						</div>
					</div> -->
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p9.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cuál es el valor del envío?</h2>
							</div>
							<div class="cont_text_full">
								<p>
									Según nuestras franjas horarias de entrega el recargo de entrega de tu pedido será teniendo en cuenta la siguiente relación: *Entregas de 05:00am - 07:00am: $4,900 *Entregas de 07:00am - 11:00am: $5,300 *Entregas de 11:00am - 02:00pm: $4,100 *Entregas de 02:00pm - 05:00pm: $3,900 *Entregas de 06:00pm - 08:00pm: $5,900 *Entregas de 06:00pm - 10:00pm: $4,300 *Entregas de 08:00pm - 10:00pm: $5,900
								</p>
								<p>
									Por compras iguales o superiores a $150,000 Merqueo no te cobrará el valor del envío.
								</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p17.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cuáles son los métodos de pago?</h2>
							</div>
							<div class="cont_text_full">
								<p>Contra-entrega: Para pago contra entrega puedes elegir datáfono o efectivo en el checkout, al terminar tu proceso de compra.</p>
								<p>Pago en línea: Nuestra pasarela de pagos en línea te permite realizar el pago de tu orden previo a recibirlo, ya sea con tarjeta debito a través de PSE o tarjeta de crédito VISA, Mastercard o American Express (no contamos con sistema de pago en línea para tarjetas débito o tarjetas de crédito expedidas en el exterior, tampoco será recibido pago en línea con tarjeta crédito Codensa); recuerda tener en cuenta las disposiciones y procedimientos establecidos en nuestra política de privacidad para hacer más seguro la experiencia de pagar en línea con nosotros.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p18.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Puedo elegir fecha y hora de recepción de mi pedido?</h2>
							</div>
							<div class="cont_text_full">
								<p>Claro que si! Al momento de llenar el formulario de envío, tienes la opción “¿Cuándo quieres recibir tu pedido?”, donde podrás seleccionar el día y la hora de entrega que prefieras, teniendo en cuenta nuestros días y franjas de entrega disponibles.</p>
							</div>
						</div>

					</div>

					<div class="col-md-1 cont_divi">
						<div class="cont_div_style"></div>
					</div>
					<div class="col-md-5 cont_divi_right">
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p8.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Merqueo.com cubre mi dirección?</h2>
							</div>
							<div class="cont_text_full">
								<p>La cobertura aproximada que tenemos actualmente en Bogotá:</p>
								<p>- Desde la carrera 1, hasta la carrera 129<br>
									- Desde la calle 69 sur, hasta la calle 240</p>
								<p>En este link puedes consultar toda nuestra cobertura:</p>
								<p>https://merqueo.com/assets/img/coverage_map_bogota.jp</p>
								<p>Recuerda que depende 100% de la dirección exacta que registres en la plataforma.</p>
								<p>La cobertura aproximada que tenemos actualmente en Medellín:</p>
								<p>- Desde la calle 78 sur en Sabaneta, hasta la calle 30 entre la autopista sur y la transversal intermedia o carrera 25.<br>
									- Desde la calle 30, hasta la calle 55 entre la avenida regional y carrera 85.<br>
									- Bordeamos toda la zona de la carrera 92 hasta la calle 10 sur.<p>
								<p>En este link puedes consultar toda nuestra cobertura:</p>
								<p>https://merqueo.com/assets/img/coverage_map_medellin.jpg</p>
								<p>Recuerda que depende 100% de la dirección exacta que registres en la plataforma.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p10.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cuál es el valor mínimo para realizar un pedido?</h2>
							</div>
							<div class="cont_text_full">
								<p>El valor mínimo para realizar un pedido es de ${{ number_format($minimum_order_amount, 0, ',', '.') }} tanto en nuestra tienda de Bogotá, como en la de Medellín. </p>
							</div>
						</div>
					<!--div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p12.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Puedo pedir a varias tiendas en un solo pedido?</h2>
						</div>
						<div class="cont_text_full">
							<p>Con Merqueo.com puedes pedir a las tiendas y supermercados que desees en un &uacute;nico pedido, puedes elegir si deseas que un solo shopper haga las compras en todas las tiendas/supermercados, o un shopper por cada tienda (recuerda que el pedido m&iacute;nimo y el costo del pedido aplica por tienda, no por pedido).</p>
						</div>
					</div-->
					</div>
				</div>

			<!--<div class="row cont_data">
				<div class="col-md-10 title">Después de realizar mi pedido</div>
				<div class="col-md-5 cont_data_left">
					<div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p13.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Cómo puedo saber el estado de mi pedido?</h2>
						</div>
						<div class="cont_text_full">
							<p>En Merqueo.com puedes hacer el pedido a tiendas y supermercados, ya sea por la página web, o por tu celular Andriod - IOS y nuestros shoppers comprarán tus pedidos en la tienda que elijas para llevarlo al lugar de entrega que escojas, en menos de 1 hora</p>
						</div>
					</div>
					<div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p13.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Qué hago si mi pedido es rechazado?</h2>
						</div>
						<div class="cont_text_full">
							<p>En Merqueo.com puedes hacer el pedido a tiendas y supermercados, ya sea por la página web, o por tu celular Andriod - IOS y nuestros shoppers comprarán tus pedidos en la tienda que elijas para llevarlo al lugar de entrega que escojas, en menos de 1 hora</p>
						</div>
					</div>
					<div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p13.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Cómo puedo modificar mi pedido ya realizado?</h2>
						</div>
						<div class="cont_text_full">
							<p>En Merqueo.com puedes hacer el pedido a tiendas y supermercados, ya sea por la página web, o por tu celular Andriod - IOS y nuestros shoppers comprarán tus pedidos en la tienda que elijas para llevarlo al lugar de entrega que escojas, en menos de 1 hora</p>
						</div>
					</div>
				</div>

				<div class="col-md-1 cont_divi">
					<div class="cont_div_style"></div>
				</div>
				<div class="col-md-5 cont_divi_right">
					<div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p13.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Cómo puedo comunicarme con Merqueo.com?</h2>
						</div>
						<div class="cont_text_full">
							<p>Merqueo tiene presencia en Bogotá y Medellín, pero estamos trabajando para ampliar nuestra cobertura a toda Colombia.</p>
						</div>
					</div>
					<div class="cont_post_data">
						<div class="cont_ico">
							<img src="{{ asset_url() }}img/faq/p13.png">
						</div>
						<div class="cont_text_lim">
							<h2>¿Cuáles con las políticas de cancelación de pedidos?</h2>
						</div>
						<div class="cont_text_full">
							<p>En Merqueo.com puedes hacer el pedido a tiendas y supermercados, ya sea por la página web, o por tu celular Andriod - IOS y nuestros shoppers comprarán tus pedidos en la tienda que elijas para llevarlo al lugar de entrega que escojas, en menos de 1 hora</p>
						</div>
					</div>
				</div>
			</div>-->

				<div class="row cont_data">
					<div class="col-md-10 title">Mi cuenta</div>
					<div class="col-md-5 cont_data_left">
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p13.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cómo puedo modificar mi información personal?</h2>
							</div>
							<div class="cont_text_full">
								<p>En la parte superior derecha de nuestra página web encontraras la opción “ingresar”, ahí debes escribir tu correo electrónico y contraseña para entrar en tu perfil de Merqueo.com; das click en la opción “Mi Cuenta” y allí tendrás acceso para personalizar tu nombre y/o apellido.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p15.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Dónde puedo añadir o editar mi tarjeta crédito?</h2>
							</div>
							<div class="cont_text_full">
								<p>Dentro de tu perfil de Merqueo.com, en la opción de “Mi Cuenta”, vas a tener acceso a “Tarjeta de Crédito”,  en donde puedes vincular o desvincular tus tarjetas de crédito.</p><p>Recuerda que éste proceso sólo puedes hacerlo por la página web. En nuestra app, no está habilitada la opción.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p14.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Dónde puedo verificar el historial de mis pedidos?</h2>
							</div>
							<div class="cont_text_full">
								<p>Dentro de tu perfil de Merqueo.com, en la opción de “Mi Cuenta”, vas a tener acceso a “Pedidos” en donde puedes ver todo tu historial con todos los datos que necesites.</p>
							</div>
						</div>
					</div>

					<div class="col-md-1 cont_divi">
						<div class="cont_div_style"></div>
					</div>
					<div class="col-md-5 cont_divi_right">
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p16.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Cómo verifico si tengo crédito o domicilio gratis en Merqueo.com?</h2>
							</div>
							<div class="cont_text_full">
								<p>Dentro de tu perfil de Merqueo.com, en la opción de “Mi Cuenta”, vas a tener acceso a “Créditos”, en donde puedes verificar la cantidad de días de domicilios gratis o créditos que tienes. Igualmente recuerda que, si tienes algún saldo a favor, también en ésta opción vas a poderlo ver.</p>
							</div>
						</div>
						<div class="cont_post_data">
							<div class="cont_ico">
								<img src="{{ asset_url() }}img/faq/p19.png">
							</div>
							<div class="cont_text_lim">
								<h2>¿Qué es el código de referido y para qué sirve?</h2>
							</div>
							<div class="cont_text_full">
								<p>Cuando te registras por primera vez en nuestra app o página web. el sistema te asigna un código muy parecido a tu nombre. Con él puedes tener {{ currency_format(Config::get('app.referred.amount')) }} gratis en créditos para usar en Merqueo si se lo compartes a otra persona que aún no esté registrada, para que use nuestro servicio.
								<p>Aplica para pedido mínimo de {{ currency_format(Config::get('app.minimum_discount_amount')) }} y valido para los {{ Config::get('app.referred.amount_expiration_days') }} días siguientes a la fecha de redención. No es acumulable con otras promociones, créditos o descuentos.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
		@include('layout_footer')
	</div>

	<script>
        $(document).on('ready', function(event){
            $('.content_faq .cont_post_data').on('click', function(event){
                event.preventDefault();
                $('.cont_text_full').css('display', 'none');
                $(this).css('height', '100%');
                $(this).children('.cont_text_full').slideToggle('slow');
            })
        });
	</script>

@stop
