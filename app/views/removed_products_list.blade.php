<div class="row" id="removed-products-list">
    <div class="col-sm-10 col-sm-offset-1">
        @foreach ($removed_products as $product)
            <div class="col-sm-12 row item">
                <div class="col-sm-4" style="text-align: center">
                    @if (!empty($product->image_medium_url))
                        <img src="{{ $product->image_medium_url }}">

                    @elseif (!empty($product->image_large_url))
                        <img src="{{ $product->image_large_url }}">

                    @elseif (!empty($product->image_small_url))
                        <img src="{{ $product->image_small_url }}">
                    @endif
                </div>

                <div class="col-sm-8">
                    <p class="product-name">
                        {{
                            empty($product->full_name)
                                ? $product->name
                                : $product->full_name
                        }} x <span class="product-quantity">{{ $product->qty ?: $product->cart_quantity }}</span>
                    </p>
                    <p class="price-style">
                        @if (intval($product->price))
                            ${{ number_format($product->price, 0, ',', '.') }}
                        @else
                            {{ $product->price }}
                        @endif
                    </p>
                </div>
            </div>
        @endforeach
    </div>
</div>
