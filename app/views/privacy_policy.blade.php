@extends('layout_page')

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="page_content page_fondo col-md-12 col-xs-12">
				<div class="content-page">
					<br><br>
					<h1>Pol&iacute;tica de Privacidad y Tratamiento de Datos Personales de MERQUEO S.A.S. (O LA COMPA&Ntilde;&Iacute;A)</h1><br>
					<p align="justify">Protegemos su informaci&oacute;n personal y sus datos.</p>
					<br>
					<p>La presente Política de Privacidad y Tratamiento de Datos Personales de MERQUEO S.A.S. (Merqueo.com) (en adelante la &quot;Política&quot;), establece los mecanismos, garantías e instrumentos que los titulares de la información que recolectamos y tratamos en calidad de Responsables del Tratamiento a la luz del Marco Normativo (según este se define en la Sección 1 de esta Política).</p>
					<p>Debido a la actividad comercial que desarrolla MERQUEO S.A.S. (en adelante “la Compañía” o MERQUEO), ésta recoge y, en algunos casos, comunica información sobre los Usuarios registrados en el Sitio web y/o en las aplicaciones móviles (en adelante, “la Plataforma”). En ese orden de ideas, la siguiente Política de Privacidad tiene como objetivo brindarle a los usuarios un panorama claro respecto de los datos personales que MERQUEO recauda, el tratamiento que se da a los mismos y los procedimientos que los usuarios tienen a su alcance para poder hacer efectivos los derechos derivados de aquellos.</p>
					<br>
					<ul style="text-align: center; list-style: none;">
						<li><p><b>CONSIDERACIONES PREVIAS</b></p></li>
					</ul>
					<br>
					<ol style="padding-left: 15px">
						<li style="font-size: 18px; line-height: 1.5;"><b>Dato Personal.</b></li>
					</ol>
					<p>información vinculada o que pueda asociarse a una o varias personas naturales determinadas o determinables. Para efectos de esta Política, cuando se haga referencia al término &quot;Datos Personales&quot;, se entenderá exclusivamente realizada a los datos personales definidos en la Ley 1581 de 2012, con el alcance otorgada por esta normatividad.</p>
					<br>
					<ol style="padding-left: 15px" start="2">
						<li style="font-size: 18px; line-height: 1.5;"><b>Dato Privado.</b></li>
					</ol>
					<p>Es el dato que por su naturaleza íntima o reservada sólo es relevante para el Titular.</p>
					<br>
					<ol style="padding-left: 15px" start="3">
						<li style="font-size: 18px; line-height: 1.5;"><b>Informaci&oacute;n financiera, crediticia, comercial, de servicios y la proveniente de terceros países.</b></li>
					</ol>
					<p>Para todos los efectos de la Ley 1266 de 2008, y de la Política, se entenderá por información financiera, crediticia, comercial, de servicios y la proveniente de terceros países, aquella referida al nacimiento, ejecución y extinción de obligaciones dinerarias, independientemente de la naturaleza del contrato que les dé origen.</p>
					<br>
					<ol style="padding-left: 15px" start="4">
						<li style="font-size: 18px; line-height: 1.5;"><b>Encargado del Tratamiento.</b></li>
					</ol>
					<p>Persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, realice el Tratamiento de Datos Personales por cuenta del Responsable del Tratamiento.</p>
					<br>
					<ol style="padding-left: 15px" start="5">
						<li style="font-size: 18px; line-height: 1.5;"><b>Responsable del Tratamiento.</b></li>
					</ol>
					<p>Persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, decida sobre la Base de Datos y/o el Tratamiento de los datos.</p>
					<br>
					<ol style="padding-left: 15px" start="6">
						<li style="font-size: 18px; line-height: 1.5;"><b>Tratamiento.</b></li>
					</ol>
					<p>Cualquier operación o conjunto de operaciones sobre Datos Personales (Ley 1581 de 2012), tales como la recolección, almacenamiento, uso, circulación o supresión. Para efectos de esta Política, se entenderá que el Tratamiento abarca también la recolección, captura, almacenamiento, uso, circulación, transmisión, transferencia o supresión de &quot;Información Protegida&quot;, según se define este término en las CONSIDERACIONES PREVIAS del presente documento.</p>			<br>
					<ol style="padding-left: 15px" start="7">
						<li style="font-size: 18px; line-height: 1.5;"><b>Datos Sensibles.</b></li>
					</ol>
					<p>Se entiende por datos sensibles aquellos que afectan la intimidad del Titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición, así como los datos relativos a la salud, a la vida sexual, y los datos biométricos.</p>
					<br>
					<ol style="padding-left: 22px" start="8">
						<li style="font-size: 18px; line-height: 1.5;"><b>Transferencia.</b></li>
					</ol>
					<p>La transferencia de datos tiene lugar cuando el Responsable del Tratamiento y/o el Encargado del Tratamiento de Datos Personales, ubicado en la República de Colombia, envía la información o los Datos Personales a un receptor, que a su vez es Responsable del Tratamiento y se encuentra dentro o fuera de la República de Colombia.</p>
					<br>
					<ol style="padding-left: 22px" start="9">
						<li style="font-size: 18px; line-height: 1.5;"><b>Transmisión.</b></li>
					</ol>
					<p>Tratamiento de Datos Personales que implica la comunicación de los mismos dentro o fuera del territorio de la República de Colombia cuando tenga por objeto la realización de un Tratamiento por el Encargado del Tratamiento por cuenta del Responsable del Tratamiento.</p>
					<br>
					<p style="text-align: justify;">Las anteriores definiciones establecidas por las leyes 1266 de 2008 y 1581 de 2012 y el Decreto 1377 de 2013 serán aplicables y tendrán el alcance que en cada norma se les otorga; en consecuencia, para efectos de su interpretación y aplicación dentro la Política, es necesario tomar en consideración el sentido y ámbito específico de cada concepto dentro de la norma correspondiente. El Marco Normativo forma parte integral de la Política, y en especial, las Leyes 1266 de 2008 y 1581 de 2012, junto con el Decreto 1377 de 2013. Los demás términos definidos en el presente documento, tendrán el significado que aquí se les asigna.</p>
					<br/><br/>
					<ul style="text-align: center; list-style: none;">
						<li><p><b>PRINCIPIOS QUE ORIENTAN LA POL&Iacute;TICA.</b></p></li>
					</ul>
					<br>
					<p style="text-align: justify;">MERQUEO S.A.S. y sus sociedades afiliadas por vínculos de subordinación o control en Colombia o en una jurisdicción o territorio diferente (en adelante las &quot;Sociedades Vinculadas&quot;), en el Tratamiento de la información recolectada y accedida de sus usuarios, empleados, proveedores, subcontratistas y cualquier otro tercero (en adelante los &quot;Titulares&quot;), en el desarrollo y giro de sus negocios y actividades en Colombia, sea que se trate dicha información de Datos Personales y/o Datos Personales Crediticios (en adelante la &quot;Información Protegida&quot;), respeta y respetará los derechos de cada uno de estos sujetos, aplicando y garantizando los siguientes principios orientadores de la Política:</p>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Legalidad:</b>
						</li>
						<p>En el Tratamiento de la Información Protegida, se dará aplicación a las disposiciones vigentes y aplicables, que rigen el Tratamiento de la misma y demás derechos fundamentales conexos, incluyendo las disposiciones contractuales pactadas por la Compañía con los Titulares, según corresponda.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Libertad:</b>
						</li>
						<p>El Tratamiento de Datos Personales y Datos Personales Crediticios, sólo se llevará a cabo con el consentimiento, previo, expreso e informado del Titular. Los Datos Personales y Datos Personales Crediticios, que no tengan el carácter de Datos Públicos, no podrán ser obtenidos o divulgados sin previa autorización, o en ausencia de mandato legal, estatutario, o judicial que releve el consentimiento.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Finalidad:</b>
						</li>
						<p>El Tratamiento de la Información Protegida que no tenga el carácter de Dato Público, a los que tenga acceso y sean acopiados y recogidos por la Compañía, estarán subordinados y atenderán una finalidad legítima, la cual será informada al respectivo Titular de la Información Protegida.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Veracidad o Calidad:</b>
						</li>
						<p>La Información Protegida sujeta a Tratamiento debe ser veraz, completa, exacta, actualizada, comprobable y comprensible. Se prohíbe el Tratamiento de datos parciales, incompletos, fraccionados o que induzcan a error. La Compañía no será responsable frente al Titular cuando sean objeto de Tratamiento, datos o información parcial, incompleta o fraccionada o que induzca a error, entregada por el Titular sin que existiera la forma de ser verificada la veracidad o calidad de la misma por parte de la Compañía o la misma hubiere sido entregada o divulgada por el Titular declarando o garantizando, de cualquier forma, su veracidad o calidad.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Transparencia:</b>
						</li>
						<p>En el Tratamiento de Información Protegida, se garantizará el derecho del Titular a obtener de la Compañía, en cualquier momento y sin restricciones, información acerca de la existencia de cualquier tipo de Información Protegida que sea de su interés (legal, judicial o contractualmente justificado) o titularidad.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Acceso y Circulación Restringida:</b>
						</li>
						<p>La Información Protegida no estará disponible en Internet u otros medios de divulgación o comunicación masiva, salvo que el acceso sea técnicamente controlable para brindar un conocimiento restringido sólo a la Compañía, las Sociedades Vinculadas, los Titulares o terceros debidamente autorizados. Para estos propósitos la obligación de la Compañía, será de medio, según lo establece la normatividad vigente.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Seguridad:</b>
						</li>
						<p>La Información Protegida bajo la Política sujeta a Tratamiento por la Compañía, será objeto de protección en la medida en que los recursos técnicos y estándares mínimos así lo permitan, a través de la adopción de medidas tecnológicas de protección, protocolos, y medidas administrativas que sean necesarias para otorgar seguridad a los registros y repositorios electrónicos evitando su adulteración, modificación, pérdida, consulta, y en general en contra de cualquier uso o acceso no autorizado.</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Confidencialidad:</b>
						</li>
						<p>Todas y cada una de las personas que en la Compañía administran, manejan, actualizan o tienen acceso a Información Protegida que no tenga el carácter de pública, y se encuentre en Sistemas de Información o bases o bancos de datos de terceros debidamente autorizados, se comprometen a conservar y mantener de manera estrictamente confidencial y no revelar a terceros todas o cualesquiera de las informaciones personales, comerciales, contables, técnicas, comerciales o de cualquier otro tipo suministradas en la ejecución y ejercicio de sus funciones. La Compañía y sus Sociedades Vinculadas para el Tratamiento de la Información Protegida podrán utilizar los sistemas de información y bases de datos de propiedad de la Compañía y/o de sus Sociedades Vinculadas (en adelante los &quot;<b>Sistemas de Información</b>&quot;).</p>
					</ul>
					<br>
					<ul>
						<li style="font-size: 18px; line-height: 1.5;">
							<b>Principio de Temporalidad de la Información:</b>
						</li>
						<p>En el caso de los Datos Personales Crediticios, los mismos no serán suministrados a usuarios o terceros cuando dejen de servir para la finalidad del banco de datos correspondiente.</p>
					</ul>
					<ul style="text-align: center; list-style: none;">
						<li><p><b>DERECHOS DE LOS TITULARES</b></p></li>
					</ul>
					<br>
					<p style="text-align: justify;">Los Titulares tendrán los derechos consagrados en el Marco Normativo y en los contratos celebrados con MERQUEO S.A.S, según les sean aplicables teniendo en consideración la Información Protegida objeto de Tratamiento por parte de MERQUEO S.A.S. y/o de las Sociedades Vinculadas.</p>
					<p style="text-align: justify;">Los Titulares de los cuales se realice el Tratamiento de Datos Personales tendrán específicamente los derechos previstos por la Ley 1581 de 2012, concretamente, pero sin limitarse a los descritos en el Artículo 8, y todas aquellas normas que la reglamenten, adicionen o complementen.</p>
					<p style="text-align: justify;">Los Titulares de los cuales se realice el Tratamiento de Datos Personales Crediticios, en caso que MERQUEO S.A.S. recolecte dichos datos, tendrán específicamente los derechos previstos por la Ley 1266 de 2008, concretamente, pero sin limitarse a los descritos en el Artículo 6, y todas aquellas normas que la reglamenten, adicionen o complementen.</p>
					<p style="text-align: justify;">Para efectos de interpretación de las leyes y decretos que se expidan en la materia, se tendrá como parámetro de interpretación legal y constitucional las sentencias de la Corte Constitucional de Colombia. Los derechos de los Titulares se interpretarán en armonía y en un plano de equilibrio con el derecho a la información previsto en el Artículo 20 de la Constitución Política de Colombia y con los demás derechos constitucionales aplicables.</p>
					<p style="text-align: justify;">Para toda aquella Información Protegida, que sea recolectada por MERQUEO S.A.S. y/o las Sociedades Vinculadas, y que no tenga el carácter de: (i) Datos Personales de carácter privado o semiprivado; y (ii) Datos Personales Crediticios de carácter privado o semiprivado, los derechos de los Titulares serán únicamente aquellos establecidos y pactados de manera contractual con MERQUEO S.A.S.. En todo caso frente a esta información, MERQUEO S.A.S. garantizará la seguridad y confidencialidad de la misma cuando esté sujeta a ello y los principios orientadores de las Políticas que les sean aplicables.</p>
					<p style="text-align: justify;">Tratándose de información o datos de carácter público, MERQUEO S.A.S. garantizará la veracidad y calidad de la información que ostenta esta calidad y que repose en los Sistemas de Información.</p>
					<br><br>
					<ul style="text-align: center; list-style: none;">
						<li><p><b>DEBERES DE MERQUEO S.A.S.</b></p></li>
					</ul>
					<br>
					<p style="text-align: justify;"><b>Deber General de MERQUEO S.A.S. en el Tratamiento de Información. </b></p>
					<p style="text-align: justify;">MERQUEO S.A.S. de manera general, y de conformidad con el Marco Normativo, las normas legales que regulan sus relaciones jurídicas con los Titulares, y en especial, las obligaciones específicas que asume frente a los Titulares, tiene como deber general en el Tratamiento de Información Protegida el respetar y garantizar en todo momento los derechos propios de los Titulares, garantizando, cuando sea aplicable y según el carácter de la información utilizada, la confidencialidad, reserva, seguridad e integridad de la misma.</p>
					<br>
					<p><b>Deberes Especiales de MERQUEO S.A.S. en su calidad de Responsable del Tratamiento de Datos Personales y Datos Personales Crediticios.</b></p>
					<p style="text-align: justify;">Serán deberes especiales de MERQUEO S.A.S, cuando actúe como Responsable del Tratamiento de Datos Personales y Datos Personales Crediticios, entre otros, los siguientes:</p>
					<br>
					<ol style="padding-left: 15px; list-style-type:upper-alpha;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Garantizar al Titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Solicitar y conservar, copia de la respectiva autorización otorgada por el Titular, cuando se traten de Datos Personales y Datos Personales Crediticios privados o semiprivados.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Informar debidamente al Titular sobre la finalidad de la recolección y los derechos que le asisten por virtud de la autorización otorgada.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Garantizar que la información que se suministre al Encargado del Tratamiento sea veraz, completa, exacta, actualizada, comprobable y comprensible.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Actualizar la información, comunicando de forma oportuna al Encargado del Tratamiento, todas las novedades respecto de los datos que previamente le haya suministrado y adoptar las demás medidas necesarias para que la información suministrada a este se mantenga actualizada.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Rectificar la información cuando sea incorrecta y comunicar lo pertinente al Encargado del Tratamiento.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Suministrar al Encargado del Tratamiento, según el caso, únicamente datos cuyo Tratamiento esté previamente autorizado.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Exigir al Encargado del Tratamiento en todo momento, el respeto a las condiciones de seguridad y privacidad de la información del Titular.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Tramitar las consultas y reclamos formulados.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Informar al Encargado del Tratamiento cuando determinada información se encuentra en discusión por parte del Titular, una vez se haya presentado la reclamación y no haya finalizado el trámite respectivo.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Informar a solicitud del Titular sobre el uso dado a sus datos.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Informar a la autoridad de protección de datos cuando se presenten violaciones a los códigos de seguridad y existan riesgos en la administración de la información de los Titulares.</li>
					</ol>
					<br><br>
					<ul style="text-align: center; list-style: none;">
						<li><p><b>AUTORIZACIÓN Y CONSENTIMIENTO</b></p></li>
					</ul>
					<br>
					<p><b>Autorización y Consentimiento.</b></p>
					<p style="text-align: justify;">El Tratamiento de Datos Personales y Datos Personales Crediticios privados o semiprivados, por parte de MERQUEO S.A.S. y/o de las Sociedades Vinculadas, requiere del consentimiento libre, previo, expreso e informado del Titular. MERQUEO S.A.S. en todo momento dejará constancia de la autorización entregada por el Titular, a través de los medios idóneos que garanticen que la misma fue otorgada de manera expresa, libre, previa e informada, como lo son las autorizaciones por escrito, en medios electrónicos u otorgadas al aceptar términos y condiciones de servicios y/o productos, el aviso de privacidad correspondiente que soporta el Tratamiento de los Datos Personales o cualquier otro mecanismo que permite acreditar y demostrar un registro, acceso o vinculación a servicios y/o productos de MERQUEO S.A.S.</p>
					<br>
					<p><b>Finalidades.</b></p>
					<p style="text-align: justify;">Los fines y propósitos del Tratamiento de los Datos Personales y Datos Personales Crediticios privados o semiprivados, siempre estarán de manera clara y expresa, establecidos en aquellos medios idóneos, a través de los cuales MERQUEO S.A.S. requiera la autorización de los Titulares. MERQUEO S.A.S. no utilizará los Datos Personales y Datos Personales Crediticios privados o semiprivados para fines distintos de los expresamente autorizados por los Titulares. En el contexto (CONSIDERACIONES PREVIAS) de esta Política, se establecen algunos de los Datos Personales y Datos Personales Crediticios que podrá recolectar MERQUEO S.A.S, estableciendo las finalidades del Tratamiento de los mismos, con el propósito que los Titulares estén informados en todo momento sobre el uso que de los mismos adelantará, o podrá adelantar, MERQUEO S.A.S y las Sociedades Vinculadas.</p>
					<p style="text-align: justify;">Esta Política podrá ser modificada y adicionada de tiempo en tiempo, en cumplimiento de requerimientos establecidos por las normas legales correspondientes. La Política debidamente actualizada, indicando la fecha de actualización, estará publicada oportunamente en el Portal Web de MERQUEO S.A.S. y las Aplicaciones Móviles de MERQUEO S.A.S. o de las Sociedades Vinculadas y podrá ser informada mediante correo electrónico. El Titular reconoce y acepta que con el uso del Portal Web y/o las Aplicaciones Móviles, está de acuerdo con esta Política, implicando todo uso continuo de las mismas la aceptación inequívoca de la misma.</p>
					<p style="text-align: justify;">MERQUEO S.A.S, de conformidad con el Marco Normativo, no estará obligado a suprimir de sus Sistemas de Información la Información Protegida, frente a la cual el Titular tenga un deber legal o contractual con MERQUEO S.A.S. y/o las Sociedades Vinculadas de permanecer en los Sistemas de Información.</p>
					<br>
					<p><b>Seguridad de la Información.</b></p>
					<p style="text-align: justify;">En desarrollo del Principio de Seguridad establecido en la normatividad vigente y de conformidad con las obligaciones legales que MERQUEO S.A.S. posee, la Compañía adoptará las medidas tecnológicas, operativas y administrativas que sean necesarias para otorgar seguridad a los registros e Información Protegida de los Titulares, evitando su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento.</p>
					<br>
					<p><b>Datos e Información Sensible.</b></p>
					<p style="text-align: justify;">Sin perjuicio de las disposiciones específicas que se puedan contemplar en las CONSIDERACIONES PREVIAS de esta Política, MERQUEO S.A.S. manifiesta a todos los Titulares que en el evento que dentro del ejercicio y desarrollo de sus actividades solicite el Tratamiento de Datos Sensibles, o realice efectivamente el Tratamiento de Datos Sensibles para la correcta operación o funcionamiento de los diferentes procesos y actividades de su empresa, dará cumplimiento a las pautas y restricciones que para tales propósitos establece el Marco Normativo, reiterando que en ningún momento los Titulares estarán obligados a autorizar el Tratamiento de Datos Sensibles. Todo Titular deberá consentir expresamente a MERQUEO S.A.S. y las Sociedades Vinculadas el Tratamiento de Datos Sensibles bajo la presente Política.</p>
					<p style="text-align: justify;">En las CONSIDERACIONES PREVIAS se establecen las condiciones específicas relativas a Datos Sensibles de niños, niñas y adolescentes menores de edad (los &quot;<b>Menores de Edad</b>&quot;), y en especial frente a las restricciones y limitaciones establecidas por MERQUEO S.A.S. frente a la contratación y uso de sus servicios y/o productos por parte de Menores de Edad.</p>
					<br>
					<p><b>Utilización y Transferencia Internacional de Información Protegida.</b></p>
					<p style="text-align: justify;">En el evento que MERQUEO S.A.S, en el ejercicio de sus actividades propias, utilice, transmita o transfiera internacionalmente Información Protegida garantizará el cumplimiento de los principios aplicables establecidos en las CONSIDERACIONES PREVIAS de la presente Política. Cuando se trate de la transferencia de Datos Personales, MERQUEO S.A.S. dará cumplimiento a lo dispuesto en la Ley 1581 de 2012, en especial el Artículo 26, y demás normas que lo modifiquen, adicionen o complementen.</p>
					<p style="text-align: justify;">Concretamente MERQUEO S.A.S, en el giro ordinario de sus negocios podrá incorporar la Información Protegida dentro de los Sistemas de Información. MERQUEO S.A.S, como Responsable del Tratamiento garantiza que los Sistemas de Información cumplen en su integridad las Políticas y el Marco Normativo, y en consecuencia, garantiza que cualquier Titular podrá: (i) conocer en cualquier momento la información que reposa en los Sistemas de Información; (ii) solicitar la actualización o rectificación de los datos allí incorporados; y (iii) solicitar, salvo en aquellos eventos previstos en las CONSIDERACIONES PREVIAS de la Política, la supresión de sus datos notificando a MERQUEO S.A.S., para lo cual se seguirá el procedimiento establecido dentro del del presente documento.</p>
					<p style="text-align: justify;">El Titular reconoce con la aceptación de la Política que, de presentarse una venta, fusión, consolidación, cambio de control societario, transferencia sustancial de activos, reorganización o liquidación de la Compañía, la Compañía podrá transferir, enajenar o asignar la Información Protegida a una o más partes relevantes, incluidas Sociedades Vinculadas.</p>
					<br>
					<p><b>Atención de Reclamos y Consultas.</b></p>
					<p style="text-align: justify;">El Titular o sus causahabientes que consideren que la Información Protegida contenida en un Sistema de Información, o en una base de datos, debe ser objeto de corrección, actualización o supresión, o cuando adviertan el presunto incumplimiento de cualquiera de los deberes contenidos en el Marco Normativo, podrán presentar un reclamo o solicitud ante MERQUEO S.A.S., remitiéndola en una primera instancia a través del siguiente correo electrónico habilitado para el efecto por MERQUEO S.A.S: {{ $admin_email }} (en adelante el &quot;<b>Correo Electrónico Autorizado</b>&quot;).</p>
					<p style="text-align: justify;">MERQUEO S.A.S. atenderá y dará respuesta a los reclamos o solicitudes de los Titulares en los plazos y términos establecidos para el efecto por el Marco Normativo.</p>
					<p style="text-align: justify;">El Titular, sin perjuicio de lo anterior, y en el evento que no haya sido atendida su solicitud o reclamo por parte de MERQUEO S.A.S., podrá en todo caso acudir posteriormente en una segunda instancia, ante la Superintendencia de Industria y Comercio (www.sic.gov.co). En dicho evento, para la presentación del reclamo ante la Superintendencia de Industria y Comercio se tomará en consideración la naturaleza de la Información Protegida, siendo procedente cuando dicha información: (i) no tenga el carácter de información pública o sea un Dato Público, y (ii) MERQUEO S.A.S. esté en violación de los principios aplicables para la información pública o de un Dato Público.</p>
					<br/>
					<ul style="text-align: center; list-style: none;">
						<li><p><b>INFORMACI&Oacute;N (INCLUIDA INFORMACIÓN PRETEGIDA) QUE RECOLECTAMOS Y RECABAMOS.</b></p></li>
					</ul>
					<p><b>Frente a Candidatos: </b>MERQUEO S.A.S. podrá recolectar, entre otra, la siguiente información:</p>
					<ul style="padding-left: 50px;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nombres y apellidos.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Tipo y número de identificación.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nacionalidad.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fecha de Nacimiento / Lugar de Nacimiento</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Género.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Estado civil.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Teléfonos fijos y celulares de contacto.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Direcciones postales y electrónicas (personales y/o laborales).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Profesión u oficio.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Perfil Académico.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Perfil Profesional.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Envío hoja de vida actualizada.</li>
					</ul>
					<br>
					<p><b>Frente a Colaboradores: </b>MERQUEO S.A.S. podrá recolectar, entre otra, la siguiente información:</p>
					<ul style="padding-left: 50px;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nombres y apellidos.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Tipo y número de identificación.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fecha de Nacimiento.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Género.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Estado civil.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Teléfono residencia.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Teléfono celular.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Direcciones postales y electrónicas (personales y/o laborales).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Profesión u oficio.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Perfil Académico.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Perfil Profesional.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Hoja de vida actualizada.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fecha Ingreso.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fecha de Nacimiento / Lugar de Nacimiento.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nacionalidad.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Grupo sanguíneo.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nombre de los padres.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Empleo(s) anterior(es).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Deficiente Físico.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Grado de parentesco o afinidad con algún Colaborador.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Tipo Contrato.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Término Contrato.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Cargo.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Salario.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Funciones.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Jornada mensual.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Jornada semanal.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Tipo pago.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Banco pago.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fondo Cesantía.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fondo Salud.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fondo Pensión.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Caja Compensación.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Entidad prestadora de salud – Medicina Prepagada.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Historias clínicas.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Exámenes físicos de ingreso / salida.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">•	Datos biométricos (fotografía, grabaciones de video, grabaciones de voz, huella dactilar).</li>
					</ul>
					<br>
					<p><b>Frente a Beneficiarios Vinculados: </b>MERQUEO S.A.S. podrá recolectar, entre otra, la siguiente información:</p>
					<ul style="padding-left: 50px;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nombre (Beneficiario Vinculado).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nombre Colaborador del que es Beneficiario Vinculado.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fecha inicio vigencia (fecha ingreso a la Compañía).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Fecha nacimiento (de Beneficiario Vinculado).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Nacionalidad.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Departamento.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">País.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Sexo.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Parentesco o afinidad con Colaborador.</li>
					</ul>
					<br>
					<p><b>Finalidades del Tratamiento de Información Protegida de Candidatos, Colaboradores y Beneficiarios Vinculados.</b></p>
					<p style="text-align: justify;">Dentro del giro ordinario de nuestra actividad, y con el fin de desarrollar y ejecutar las relaciones de índole laboral necesarios para cumplir con nuestro objeto social y con la normatividad vigente, MERQUEO S.A.S. realizará el Tratamiento de Información Protegida con base en la autorización recibida de los Candidatos, Colaboradores y Beneficiarios Vinculados, para, entre otras, las siguientes finalidades:</p>
					<br>
					<p><b>Finalidades del Tratamiento de Información Protegida de Candidatos:</b></p>
					<ul style="padding-left: 50px;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Contacto con el Candidato dentro del proceso de selección específico.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Verificar y obtener referencias de personas naturales o jurídicas, indicadas en hojas de vida, formularios o documentos acompañados o entregados a MERQUEO S.A.S.  y/o las Sociedades Vinculadas durante el proceso de selección y/o que hayan sido ingresados y/o incorporados de manera directa por el Candidato en los Sistemas de Información.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Consolidar dicha información en los Sistemas de Información o en una base de datos administrada por terceros debidamente autorizados por MERQUEO S.A.S. o las Sociedades Vinculadas, que le permitan y faciliten tanto a MERQUEO S.A.S.  como a las Sociedades Vinculadas: (a) la administración y control de sus procesos de selección; (b) remitir al Candidato información de oportunidades de empleo y de negocio; (c) el estudio, análisis y evaluación de los Candidatos que participen de procesos de selección abiertos y vigentes; (d) para control interno y de auditoría requerido por MERQUEO S.A.S. o las Sociedades Vinculadas; y (e) para atender oportunamente, solamente entregando la información específicamente solicitada, los requerimientos de orden legal o judicial que realicen órganos de control, autoridades competentes y judiciales, en Colombia o en los países donde se encuentran ubicadas las Sociedades Vinculadas.</li>
					</ul>
					<br>
					<p><b>Finalidades del Tratamiento de Información Protegida de Colaboradores:</b></p>
					<ul style="padding-left: 50px;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Realizar al interior de la Compañía actividades de bienestar para los Colaboradores.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Conservar la información exacta de residencia de los Colaboradores cuando se requiera hacer visitas domiciliarias como procedimiento de ingreso o para el desarrollo de actividades de acompañamiento dentro de los programas de bienestar o se requiera envió de correspondencia.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Hacer actividades de vigilancia epidemiológica enmarcadas en el programa de salud ocupacional.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Contactar a un familiar en caso de una emergencia presentada al interior de la Compañía.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Tener un registro de fechas de ingreso y edad de los Colaboradores cotizantes a la AFP (Administradora de Fondo de Pensiones) y para prestar un apoyo en el proceso de solicitud de pensión.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Ofrecer un apoyo a los Colaboradores en trámites ante la EPS, por inconsistencias en atención personal y de Beneficiarios Vinculados.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Brindar un apoyo a los Colaboradores en trámites ante la Caja de Compensación Familiar, por inconsistencias en atención o afiliación personal y de Beneficiarios Vinculados.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Toma de decisiones en materia laboral con respecto a la ejecución y terminación del contrato de trabajo bien sea por el área jurídica de la Compañía o su asesor externo.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Realizar los pagos, afiliaciones y registro de novedades a las entidades prestadoras de servicios de seguridad social, aportes de ley, pagos parafiscales, cuentas AFC, a las entidades prestadoras de salud, de pensiones y cesantías, incluyendo para el Colaborador y de ser requerido, de sus Beneficiarios Vinculados (cónyuge, hijos y personas a cargo).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Suministrar la información personal y financiera a terceros para el pago de préstamos, viáticos, vuelos, libranzas o diferentes cuentas que informe.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Previa notificación a las centrales de riesgo, realizar el reporte a entidades de control nacional o territorial (DIAN, UIAF, entre otras entidades).</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Contacto para futuras contrataciones u ofertas laborales dentro y fuera del país.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Las demás necesarias y que se presente en el entorno de la ejecución laboral.</li>
					</ul>
					<br>
					<p><b>Finalidades del Tratamiento de Información Protegida de Beneficiarios Vinculados:</b></p>
					<ul style="padding-left: 50px;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Conservar la información exacta de residencia de los Colaboradores y Beneficiarios Vinculados cuando se requiera hacer visitas domiciliarias como procedimiento de ingreso o para el desarrollo de actividades de acompañamiento dentro de los programas de bienestar o se requiera envió de correspondencia.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Contactarlo en caso de una emergencia presentada al interior de la Compañía referente al Colaborador.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Adelantar cuando sea necesario los trámites ante Cajas de Compensación Familiar, entidades prestadoras de servicios de seguridad social y prestadoras de salud, y demás entidades las afiliaciones en calidad de beneficiarios del Colaborador que sean necesarias.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;">Las demás relacionadas directamente con el Tratamiento de Información Protegida de los Colaboradores que requiera en virtud de un mandato legal o contractual, el Tratamiento de Información Protegida de los Beneficiarios Vinculados.</li>
					</ul>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>1. Responsable del tratamiento y/o uso de los datos personales.</b></li>
					</ol>
					<p style="text-align: justify;">La persona jurídica que responde por el tratamiento de los datos personales suministrados a través de la Plataforma, y por tanto, de la base de datos en la cual se encuentren ubicados los mismos, es la sociedad MERQUEO S.A.S., sociedad identificada con N.I.T 900.871.444 – 8 y con domicilio principal en Carrera 69K # 78 – 56 de la ciudad de Bogotá D.C., Colombia; cuyo teléfono y mail de contacto son +571 7561938 Opción 1 y {{ $admin_email }}, respectivamente.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>2. Protegiéndolo.</b></li>
					</ol>
					<p style="text-align: justify;">MERQUEO está comprometida en proteger su privacidad. En esta Política de Privacidad se describe la información que MERQUEO recoge de sus Usuarios, y la finalidad de la recolección de dicha información. Si no está de acuerdo con ésta, por favor absténgase de utilizar la Plataforma.</p>
					<p style="text-align: justify;">Nuestra Política de Privacidad asegura que cualquier información que sea proveída por los Usuarios será mantenida privada y segura. Para dar fe de esto, en este documento establecemos los detalles de la información que recabamos y de qué manera la utilizamos. Nunca recolectaremos información sin su consentimiento explícito.</p>
					<p style="text-align: justify;">Para el efecto, le aclaramos que Usted no podrá registrarse en la Plataforma a través de la creación de una cuenta personal, hasta tanto no otorgue su autorización para el tratamiento de sus datos personales. En este sentido, nuestra Plataforma le exigirá que nos brinde su consentimiento previo, expreso e informado seleccionando la casilla que dispone lo siguiente: “He leído y estoy de acuerdo con los términos y condiciones y con las políticas de privacidad”. En caso de que Usted no seleccione dicha casilla antes de dar “click” en el botón “CREAR CUENTA”, sus datos no serán almacenados en nuestra base de datos.</p>
					<p style="text-align: justify;">Este documento hace parte integral de los Términos y Condiciones de MERQUEO.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>3. La información que recabamos.</b></li>
					</ol>
					<p><b>Recopilación de información provista voluntariamente</b></p>

					<p>Recopilamos información personal que nuestros usuarios nos proporcionan de diversas maneras en nuestro Servicio, incluyendo:</p>

					<p style="text-align: justify;"><b>Boletines informativos por correo electrónico.</b> Podemos ofrecer boletines informativos por correo electrónico con anuncios e información sobre Merqueo de vez en cuando en nuestro Servicio. Si se registra para recibir un boletín de nuestra parte, recopilamos su dirección de correo electrónico.</p>

					<p style="text-align: justify;"><b>Descargas, cuentas de usuario y perfiles.</b> Nuestro Servicio o terceros pueden darle la posibilidad de descargar el software de Merqueo, registrarse para obtener una cuenta de Merqueo o crear y actualizar un perfil de usuario en el Servicio. Si ofrecemos dicha funcionalidad en el Servicio, recopilaremos la Información personal que nos proporcione en el transcurso del uso de esa funcionalidad. Esta información puede incluir, por ejemplo, su nombre, información de contacto e información del sistema, así como información sobre sus intereses, preferencias y necesidades futuras. Podemos indicar que se requiere cierta información personal para que descargue nuestro software, se registre para la cuenta o para crear el perfil, mientras que otra es opcional. En la medida en que su perfil de usuario sea público, compartiremos su información personal en su perfil de usuario con otras personas.</p>

					<p style="text-align: justify;"><b>Servicio Integrado.</b> Se le puede dar la opción de acceder o registrarse en el Servicio mediante el uso de su nombre de usuario y contraseñas para ciertos servicios prestados por terceros (cada uno un "Servicio Integrado"), como mediante el uso de sus credenciales de Facebook a través de Facebook Connect. Al hacer esto, nos autoriza a acceder y almacenar las credenciales que proporciona, su nombre, dirección(es) de correo electrónico, fecha de nacimiento, sexo, ciudad actual, URL de la imagen de perfil y otra información que el Servicio Integrado pone a nuestra disposición, y usarlo y divulgarlo de acuerdo con esta Política. Debe verificar su configuración de privacidad de Facebook u otro servicio integrado para comprender y cambiar la información que nos envía a través de Facebook Connect u otro servicio integrado. Revise los términos de uso y las políticas de privacidad de cada Servicio Integrado cuidadosamente antes de usar sus servicios y conectarse a nuestro Servicio.</p>

					<p style="text-align: justify;"><b>Funciones interactivas y áreas públicas.</b> Nuestro Servicio puede contener una funcionalidad interactiva que le permite interactuar con otros usuarios en el Servicio, publicar comentarios en foros, cargar fotografías y otro contenido (el "Contenido del usuario"), participar en encuestas y de otra manera interactuar con el Servicio y con otros usuarios. Si utiliza alguna funcionalidad interactiva en nuestro Servicio que le solicite o le permita proporcionarnos información personal (incluidos, por ejemplo, cualquier servicio que le permita publicar Contenido de usuario en cualquiera de nuestros Servicios), recopilaremos la Información personal que usted nos proporciona durante el uso de estas funciones interactivas. Debe tener en cuenta que cualquier información que proporcione en sesiones de chat, tableros de mensajes, intercambios de correo electrónico, grupos de noticias o áreas similares no privadas del Servicio puede ser leída, recopilada y utilizada por otras personas que acceden a estas áreas, y esta información no es Personal Información a los fines de esta Política. Para solicitar la eliminación de su información personal de estas áreas, contáctenos. En algunos casos, es posible que no podamos eliminar su información personal, en cuyo caso le informaremos si no podemos hacerlo y por qué.</p>

					<p style="text-align: justify;"><b>Correspondencia.</b> Si se comunica con nosotros por correo electrónico, mediante un formulario de contacto en el Servicio, o por correo, fax u otro medio, recopilamos la Información personal contenida en su correspondencia y asociada a ella. Utilizamos esta información personal para responder a su correspondencia. También podemos usar su correspondencia para mejorar el Servicio.</p>

					<p style="text-align: justify;"><b>Promociones.</b> Nosotros o nuestros anunciantes y otros socios comerciales podemos realizar o patrocinar concursos especiales, sorteos y otras promociones en las que los usuarios pueden participar o participar en nuestro Servicio. Algunas de estas promociones pueden ser de marca compartida con uno de nuestros anunciantes u otros socios comerciales. En estos casos, la recopilación de su información personal puede ser realizada directamente por un socio externo en su sitio web u otro servicio en línea, y puede compartirse con nosotros. La promoción indicará la política o políticas de privacidad que rigen la recopilación de dicha información personal.</p>

					<p><b>Recopilación de información pasiva</b></p>

					<p style="text-align: justify;">Cuando visita nuestro Servicio, parte de la información se recopila automáticamente. Por ejemplo, cuando accede a nuestro Servicio, recopilamos automáticamente cierta información, que puede incluir la dirección de Protocolo de Internet (IP) de su navegador, su tipo de navegador, la naturaleza del dispositivo desde el que está visitando el Servicio (por ejemplo, una computadora personal o un dispositivo móvil), el identificador de cualquier dispositivo portátil o dispositivo móvil que pueda estar utilizando, el sitio web que visitó inmediatamente antes de acceder a cualquier Servicio basado en la web, las acciones que realiza en nuestro Servicio y el contenido, características y actividades a las que accede y participa en nuestro Servicio. También podemos recopilar información sobre su interacción con los mensajes de correo electrónico, como si abrió, hizo clic o reenvió un mensaje.</p>

					<p style="text-align: justify;">Podemos recopilar esta información de manera pasiva utilizando tecnologías como registros de servidor estándar, cookies y GIF transparentes (también conocidos como "web beacons"). Utilizamos información recopilada pasivamente para administrar, operar y mejorar el sitio web y nuestros otros servicios y sistemas; y además para mejorar la efectividad de la publicidad en nuestro Servicio y para proporcionarle anuncios y otro contenido que se adapte a usted. Si vinculamos o asociamos cualquier información recopilada a través de medios pasivos con información personal, o si las leyes aplicables nos obligan a tratar cualquier información recopilada a través de medios pasivos como información personal, tratamos la información combinada como información personal según esta Política. De lo contrario, usamos y divulgamos información recopilada por medios pasivos en forma agregada o de otra manera en una forma de identificación no personal.</p>

					<p style="text-align: justify;">Además, tenga en cuenta que los terceros, como las empresas que muestran anuncios en el Servicio, o los sitios o servicios proporcionados por terceros ("Sitios de terceros") que pueden estar vinculados hacia o desde el Servicio, pueden establecer cookies en su disco duro. conducir o utilizar otros medios de recopilación pasiva de información sobre su uso de sus servicios, sitios o contenido de terceros. No tenemos acceso ni control sobre estos medios de terceros de recopilación pasiva de datos.</p>

					<p><b>Información de otras fuentes</b></p>

					<p style="text-align: justify;">Podemos recibir información sobre usted, incluida información personal, de terceros (por ejemplo, listas de suscripción en las que ha solicitado información sobre Merqueo o el Servicio mientras visitaba un sitio de terceros). Podemos combinar esta información con otra información personal que mantenemos sobre usted. Si lo hacemos, esta Política rige cualquier información combinada que mantenemos en formato de identificación personal.</p>

					<p><b>Eliminación de datos</b></p>

					<p style="text-align: justify;">Se establece que en cualquier momento los Usuarios de MERQUEO podrán solicitar la eliminación de su cuenta e información de la base de datos de la compañía, por medio del procedimiento que se detalla más adelante.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>4. Derechos que le asisten como titular de la información.</b></li>
					</ol>
					<p style="text-align: justify;">Conforme a lo establecido en el artículo 8 de la Ley 1581 de 2012, a usted como titular de todos los datos personales suministrados le asisten los siguientes derechos:</p>
					<ol style="padding-left: 15px; list-style-type:upper-alpha;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Conocer, actualizar y rectificar todos sus datos personales que obren en la base de datos de MERQUEO.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Solicitar prueba de la autorización otorgada a MERQUEO para el tratamiento de sus datos personales.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Ser informado por MERQUEO, previa solicitud escrita, respecto del uso que le ha dado a sus datos personales.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la Ley 1581 de 2012, el Decreto 1377 de 2013 y demás normas que las modifiquen y/o complementen.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Revocar la autorización y/o solicitar la supresión de sus datos, cuando en el tratamiento no se respeten los principios, derechos y garantías constitucionales y legales.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5;margin-bottom: 20px;">Acceder en forma gratuita a sus datos personales que hayan sido objeto de tratamiento.</li>
					</ol>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>5. Tratamiento y/o Uso que hacemos de la información.</b></li>
					</ol>
					<p style="text-align: justify;">En virtud de la calidad de Comercio que MERQUEO ostenta hacia sus consumidores –léase Usuarios-, y principalmente con el fin de suministrar un excelente servicio para que los Usuarios puedan realizar operaciones de manera ágil y segura, MERQUEO solicita y almacena ciertos datos de carácter personal.</p>
					<p style="text-align: justify;">El propósito final y último de la recolección de esta información se funda en la base de que las relaciones entre Usuarios y MERQUEO se puedan concretar exitosamente. Es decir, la información que recolectamos se recaba y guarda con el fin de suministrar esta información a los Repartidores afiliados a la Plataforma, de manera tal que la relación final de consumo, entre consumidor y oferente del producto (MERQUEO), se pueda concretar de manera exitosa. Lo anterior no obsta para que, así mismo, esta información pueda ser utilizada por la compañía para que a través de la Plataforma, ésta pueda ofrecerle a Usted servicios y funcionalidades que se adecúen mejor a sus necesidades dentro de las diversas actualizaciones que se hacen continuamente para mejorar el servicio.</p>
					<p style="text-align: justify;">Teniendo en cuenta lo anterior, los datos personales que recabamos tienen las siguientes finalidades:</p>
					<ul style="padding-left: 50px;list-style: none;">
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">- Suministrar a los Repartidores, la información que sea necesaria para poder concretar la relación de consumo que se desea perfeccionar.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">- Ponernos en contacto directo con el Usuario cada vez que MERQUEO lo considere conveniente, con el fin de ofrecerle por distintos medios y vías (incluyendo mail, SMS, push notification, whatsapp, etc.) ofertas del día de otros productos y/o servicios, además de toda otra información que creamos conveniente. Si Usted lo prefiere, puede solicitar que lo excluyan de las listas para el envío de información promocional o publicitaria acudiendo a nuestro correo electrónico: {{ $admin_email }}.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">- Desarrollar estudios internos sobre los intereses, el comportamiento y la demografía de los Usuarios con el objetivo de comprender mejor sus necesidades e intereses, y así poder brindar un mejor servicio.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">- Mejorar nuestras iniciativas comerciales y promocionales para analizar las páginas más visitadas por los Usuarios, las búsquedas realizadas, y poder perfeccionar nuestra oferta de productos y servicios.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">- Suministrar los datos personales de los Usuarios a las entidades que intervengan en la resolución de disputas entre los mismos, es decir, tribunales o entidades administrativas o judiciales competentes para solucionar tales disputas.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">- Validar con las centrales de riesgo del orden nacional la titularidad del usuario respecto a medios de pago cuando el elegido por el usuario para realizar su compra al usar MERQUEO S.A.S. sea Tarjeta de crédito (Pago en línea).</li>
					</ul>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>6. Confidencialidad de la información</b></li>
					</ol>
					<p style="text-align: justify;">Los datos de los Usuarios serán suministrados únicamente por MERQUEO en las formas establecidas en estas Políticas de Privacidad. MERQUEO hará todo lo que esté a su alcance para proteger la privacidad de la información. Puede suceder que en virtud de órdenes judiciales, o de regulaciones legales, MERQUEO S.A.S. se vea obligado a revelar información a las autoridades o terceras partes bajo ciertas circunstancias.</p>
					<p style="text-align: justify;">En aquellos casos en los cuales terceras partes puedan interceptar o acceder a cierta información o transmisiones de datos, MERQUEO S.A.S. no responderá por la información que sea revelada. Sin embargo, MERQUEO S.A.S. se compromete a implementar todas las medidas de seguridad y confidencialidad pertinentes para la protección de los datos personales de su portafolio de Usuarios.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>7. Menores de Edad</b></li>
					</ol>
					<p style="text-align: justify;">Nuestros servicios sólo están disponibles para aquellas personas que tengan capacidad legal para contratar. Por lo tanto, aquellos que no cumplan con esta condición deberán abstenerse de suministrar información personal para ser incluida en nuestras bases de datos.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>8. Cookies</b></li>
					</ol>
					<p style="text-align: justify;">Los Usuarios de la Plataforma conocen y aceptan que MERQUEO podrá utilizar un sistema de seguimiento mediante la utilización de cookies. Las cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios. También ofrecemos ciertas funcionalidades que sólo están disponibles mediante el empleo de cookies. Estas se utilizan con el fin de conocer los intereses, el comportamiento y la demografía de quienes visitan la Plataforma, y así comprender mejor sus necesidades e intereses, y darles un mejor servicio, o poder proveerles información relacionada. También usamos la información obtenida por intermedio de las cookies para analizar las páginas navegadas por el Usuario y las búsquedas realizadas, con el fin de mejorar nuestras iniciativas comerciales y promocionales, mostrar publicidad o promociones, banners de interés, enviar noticias sobre MERQUEO S.A.S, y perfeccionar nuestra oferta de contenidos.</p>
					<p style="text-align: justify;">Adicionalmente, MERQUEO S.A.S utiliza las cookies para que el Usuario no tenga que introducir su clave de manera frecuente al iniciar sesión en una misma cuenta, y también para contabilizar y corroborar los registros y la actividad del Usuario, siempre teniendo como objetivo el beneficio de éste último. MERQUEO S.A.S. no las usará con otros fines. Se establece que la instalación, permanencia y existencia de las cookies en el computador del Usuario depende de su exclusiva voluntad y puede ser eliminada de su computador cuando así lo desee. Para saber cómo eliminar las cookies del sistema es necesario revisar la sección “Ayuda” (Help) del navegador. También, se pueden encontrar cookies u otros sistemas similares instalados por terceros en ciertas páginas de nuestra Plataforma. MERQUEO S.A.S. no controla el uso de cookies por parte de terceros.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li><b>9. E-mails y notificaciones PUSH</b></li>
					</ol>
					<p style="text-align: justify;">MERQUEO S.A.S. podrá enviarle e-mails como parte de un proceso inherente al desarrollo y prestación de sus servicios, o en las siguientes circunstancias:</p>
					<ul>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">Luego del proceso de registro, notificándole los datos de su cuenta.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">E-mails con noticias y novedades, como parte de un Newsletter.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">E-mails promocionales.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">E-mails para ofrecer servicios relacionados.</li>
						<li style="text-align: justify; color: #333; font-size: 18px; line-height: 1.5; margin-bottom: 20px;">Notificaciones de novedades naturales de la relación de consumo <p style="text-align: justify;">MERQUEO S.A.S. podrá enviarle e-mails como parte de un proceso inherente al desarrollo y prestación de sus servicios, o en las siguientes circunstancias:</li>
					</ul>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li>
							<b>10. Seguridad y almacenamiento</b></li>
					</ol>
					<p style="text-align: justify;">MERQUEO S.A.S. emplea diversas técnicas de seguridad para proteger los datos personales de sus Usuarios. Sin embargo, es necesario tener en cuenta que en Internet no existe una seguridad absoluta y perfecta. Por ello, MERQUEO S.A.S. no se hace responsable por interceptaciones ilegales o violación de sus sistemas o bases de datos por parte de personas no autorizadas. MERQUEO S.A.S. tampoco se hace responsable por la indebida utilización de la información obtenida a través de esos medios, o por la falta de diligencia de sus Usuarios.</p>
					<p>Nuestros servidores y centros de datos están ubicados en los Estados Unidos. Si elige utilizar el Servicio desde Colombia o desde cualquier lugar fuera de los EE. UU., Debe saber que está transfiriendo su Información personal fuera de su región y hacia los EE. UU. para su almacenamiento y procesamiento. Al proporcionarnos su información personal a través del uso del Servicio, usted acepta esa transferencia, almacenamiento y procesamiento en los EE. UU. Además, podemos transferir sus datos de los EE.UU. a otros países o regiones en relación con el almacenamiento y el procesamiento de datos, cumpliendo con sus solicitudes y operando el Servicio. Debe saber que cada región puede tener sus propias leyes de privacidad y seguridad de datos, algunas de las cuales pueden ser menos estrictas en comparación a las de su propia región.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li>
							<b>11. Transferencia en circunstancias especiales.</b></li>
					</ol>
					<p style="text-align: justify;">MERQUEO S.A.S. podrá transferir, vender o asignar la información recabada en la Plataforma a una o más partes relevantes, en caso de que se presente una venta, fusión, consolidación, cambio en el control societario, transferencia de activos sustancial, y/o reorganización o liquidación de la sociedad.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li>
							<b>12. Área responsable de las atenciones: Servicio al Cliente.</b></li>
					</ol>
					<p style="text-align: justify;">En dado caso de querer ejercer los derechos que le asisten como titular de la información, por favor no dude en contactar a nuestro departamento de Servicio al Cliente, quien con mucho gusto escuchará su petición y se encargará de darle el debido trámite a través de: vía telefónica +571 7561938 opción 1 y en nuestro buzón {{ $admin_email }}.</p>
					<br/>
					<ol style="text-align: left; font-size: 18px; line-height: 1.5; list-style: none; padding: 0;">
						<li>
							<b>12.1 Procedimiento para que los usuarios puedan ejercer sus derechos.</b></li>
					</ol>
					<p style="text-align: justify;">MERQUEO S.A.S. dispone de los siguientes mecanismos para dar un efectivo trámite a su petición. En cualquier momento los Usuarios podrán contactar a nuestro servicio al cliente con el fin de elevar las peticiones, quejas y reclamos que tengan. Estas peticiones se podrán realizar bien sea por correo electrónico dirigido a {{ $admin_email }}, o bien por comunicación escrita dirigida al mismo departamento a la dirección Carrera 69K # 78-56 de la ciudad de Bogotá D.C., Colombia.</p>
					<p style="text-align: justify;">En el e-mail y/o carta escrita se deberá explicitar la petición de manera clara y concreta, de tal forma que nos sea posible conocer y entender cuál es la intención final del Usuario (actualización, supresión, rectificación de datos personales y/o revocatoria de la autorización, entre otros). Lo anterior con el fin de que podamos brindarle a su solicitud la respuesta más eficiente y efectiva que nos sea posible. Junto a la petición concreta la comunicación deberá incluir el nombre completo del titular y el número de identificación, de manera tal que podamos determinar quién realiza la petición, y respecto de quién se habrá de realizar el trámite pertinente.</p>
					<p style="text-align: justify;">En todo caso, MERQUEO S.A.S. se compromete a brindarle una respuesta a su solicitud dentro de un término máximo de 10 (diez) días hábiles.</p>
					<br/>
					<ol style="text-align: left; font-size: 18px; line-height: 1.5; list-style: none; padding: 0;">
						<li>
							<b>12.2 Trámite especial para el ejercicio del derecho de acceso.</b></li>
					</ol>
					<p style="text-align: justify;">En cualquier momento, los Usuarios de la Plataforma podrán exigir mediante comunicación escrita, bien sea física o mediante correo electrónico, que la empresa les suministre o les facilite el acceso a los datos personales que ésta tenga en su poder.</p>
					<p style="text-align: justify;">Este derecho podrá ser ejercido gratuitamente una vez al mes y/o cada vez que MERQUEO S.A.S. realice modificaciones sustanciales a esta Política de Privacidad. El término de respuesta establecido para estos trámites se solucionará dentro de un término máximo de 10 (diez) días hábiles.</p>
					<br/>
					<ol style="text-align: left; font-size: 18px; line-height: 1.5; list-style: none; padding: 0;">
						<li>
							<b>12.3 Trámite especial para reclamos.</b></li>
					</ol>
					<p style="text-align: justify;">El titular de los datos, o quien por ley o autorización se encuentre facultado para tal efecto, podrá elevar ante MERQUEO S.A.S. un reclamo escrito en donde se incluya la identificación del titular de la información, la descripción de los hechos que dan lugar al reclamo y la dirección, acompañando los documentos que se quieran hacer valer.</p>
					<p style="text-align: justify;">Para todos los efectos especiales de su trámite, éstos se regirán por lo contemplado en el artículo 15 de la Ley 1581 de 2012.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li>
							<b>13. Modificaciones de las Políticas de Privacidad</b></li>
					</ol>
					<p style="text-align: justify;">MERQUEO S.A.S. podrá modificar en cualquier momento esta Política de Privacidad. Cualquier cambio será efectivo apenas sea publicado en la Plataforma. Dependiendo de la naturaleza del cambio podremos anunciar el mismo a través de: (a) la página de inicio de la Plataforma, o (b) un e-mail. En todo caso, el continuo uso de la Plataforma implica la aceptación por parte del Usuario de estos cambios. Si Usted no está de acuerdo con nuestra Política de Privacidad vigente, por favor absténgase de utilizar la Plataforma.</p>
					<br/>
					<ol style="text-align: center; font-size: 18px; line-height: 1.5; list-style: none;">
						<li>
							<b>14. Disposiciones finales</b></li>
					</ol>
					<p style="text-align: justify;">La presente Política de Privacidad tendrá plena vigencia y efectos a partir del 21 (veintiuno) de Junio de 2017 (dosmil diecisiete).</p>
				</div>
			</div>
		</div>
@stop
