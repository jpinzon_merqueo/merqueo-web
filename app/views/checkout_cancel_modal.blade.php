<div class="modal fade cancel-transaction"
     id="cancel-transaction-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="close" data-dismiss="modal">
                    <img src="/assets/img/gift/cerrar.png">
                </div>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img src="{{ asset_url() }}img/warning-cancel-pse.png" alt="Cancelar transacción">
                </div>
                <p class="txt-cancel">¿Estas seguro que deseas cancelar la transacción?</p>
                <form role="form" method="post" action="{{route('frontCheckout.checkout_failed')}}" onsubmit="loaderSpin()">
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="submit" class="btn-payu btn-acept-cancel" value="Aceptar">
                </form>
                <div class="loading-pse spinner">
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function loaderSpin(){
        $('.btn-acept-cancel').prop('disabled',true);
        $('.loading-pse.spinner').css('visibility','visible');
    }
</script>
