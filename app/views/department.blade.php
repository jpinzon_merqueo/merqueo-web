@extends('layout')

@section('title'){{ $data['department']['name'] }} en {{ $store->name }} - Merqueo.com @stop
@section('description'){{ $data['department']['name'] }} en {{ $store->name }}. Puedes pagar con tarjeta o en efectivo contra entrega.@stop

@section('content')

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "{{ $store->name }}",
  "logo": "{{ $store->logo_url }}",
  "url": "{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}",
  "contactPoint" : [{
        "@type" : "ContactPoint",
        "telephone" : "{{ $store->phone }}",
        "contactType" : "Teléfono Domicilios"
      }]
  }
}
</script>

<div class="row" style="margin-top: 130px;">
    <div class="col-xs-12">
        <div class="logtexttienda" style="padding: 0 3rem;">
            {{ View::make('common.banners', compact('banners', 'has_orders', 'variation_key') + ['store' => $data['store']]) }}
        </div>
    </div>
</div>

<div class="col-md-12 content_list_data">
  <div class="col-md-12">
      <div class="col-md-12">
          <div class="text_hora">
              <h1>{{ $data['department']->name }}</h1>
          </div>
      </div>
  </div>

  <ol class="breadcrumb" data-catrel="{{ $data['store']['id'] }}">
    {{ Breadcrumb::render('>', $data['store']['name'], 2, $store->city_name) }}
  </ol>

  @foreach ($data['department']->shelves as $shelf)
    @if ($shelf->products->count())
    <div class="row col-md-12 col-sm-12 col-xs-12 clearfix pro-row">
    	<div class="col-md-12 product-page-title">
    		<h2>
    		    <a class="head" href="{{ web_url() }}/{{ $data['store']['city_slug'] }}/domicilios-{{ $data['store']['slug'] }}/{{ $data['department']['slug'] }}/{{ $shelf['slug'] }}">{{ $shelf['name'] }}</a>
    		    <span>
                    <a class="mq-see-more-link" href="{{ web_url() }}/{{ $data['store']['city_slug'] }}/domicilios-{{ $data['store']['slug'] }}/{{ $data['department']->slug }}/{{ $shelf->slug }}">
                        VER MÁS <i class="icon-chevron-right"></i>
                    </a>
                </span>
    	    </h2>
    	</div>
    	@if (!empty($shelf->description))
        <div class="category-msg alerts">
          <p>{{ $shelf->description }}</p>
        </div>
      	@endif
    	<div class="col-md-12 col-sm-12 col-xs-12">
    		{{ View::make('elements.products-list', ['products' => $shelf->products, 'store' => $data['store'], 'referrer' => 'department', 'variation_key' => $variation_key]) }}
    	</div>
    </div>
    @endif
  @endforeach
</div>
@stop
