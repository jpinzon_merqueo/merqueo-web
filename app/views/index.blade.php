@extends('layout')

@section('title')Mercado a domicilio en {{ $metadata['city_name'] }} - Merqueo.com @stop
@section('description')Pide ahora todo lo que necesites y recíbelo en la puerta de tu casa. Pide a {{ $metadata['stores'] }}. Pide ahora! Puedes pagar con tarjeta o efectivo contra entrega.@stop

@section('content')

<script>
@if ($stores[0]->city_id == 2)
    window.location = "{{ Config::get('app.url').'/medellin/domicilios-super-ahorro' }}";
@else
    window.location = "{{ Config::get('app.url').'/bogota/domicilios-super-ahorro' }}";
@endif
</script>

<div class="container-fluid clearfix col-md-12 col-xs-12">
    @if (isset($user_discounts))
        <div class="row clearfix">
            <div class="col-md-3 col-xs-3 "></div>
            @if ($user_discounts['free_delivery_days'] > 1)
                <div class="col-md-6 col-xs-6 bonodiv">
                    <div class="col-md-2 col-xs-2">
                        <img src="{{ asset_url() }}img/ico_not.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>Tienes <b>{{ $user_discounts['free_delivery_days'] }}</b> días de domicilio gratis.</p>
                    </div>
                </div>
            @else
              @if ($user_discounts['free_delivery_days'] == 1)
                <div class="col-md-6 col-xs-6 bonodiv">
                    <div class="col-md-2 col-xs-2">
                        <img src="{{ asset_url() }}img/ico_not.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>Tienes domicilio gratis hasta el día de hoy.</p>
                    </div>
                </div>
              @else
                @if ($user_discounts['free_delivery_next_order'])
                <div class="col-md-6 col-xs-6 bonodiv">
                    <div class="col-md-2 col-xs-2">
                        <img src="{{ asset_url() }}img/ico_not.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>Tienes domicilio gratis en tu próximo pedido.</p>
                    </div>
                </div>
                @else
                    @if ($user_discounts['amount'] > 0)
                    <div class="col-md-6 col-xs-6 bonodiv">
                        <div class="col-md-2 col-xs-2">
                            <img src="{{ asset_url() }}img/ico_not.png" alt="">
                        </div>
                        <div class="col-md-10">
                            <p>Tienes <strong>{{ currency_format($user_discounts['amount']) }}</strong> en créditos para tu próximo pedido.</p>
                        </div>
                    </div>
                    @endif
                @endif
              @endif
            @endif
        </div>
    @endif
    <div class="row clearfix">
        <div class="page_content page_fondo col-md-12 col-xs-12 clearfix">
            <div class="home_grind">
                <div class="col-md-12 col-xs-12 clearfix">
                    <br><br><br><br>
                	@if($stores)
                    	@foreach ($stores as $store)
                            <div class="col-md-12 col-xs-12 col-sm-12 clearfix store-home">
                                <div class="col-md-12 item_store clearfix">
                                    <div class="card_supporting-text col-md-12 col-xs-12">
                                        <div class="titleint">
                                            <h2>{{ $store->name }}</h2>
                                        </div>
                                        <div class="content_store">
                                            {{ $store->description_web }}
                                        </div>
                                        <div class="store_slot">
                                            <img src="{{ asset_url() }}img/box-ico.png" alt="">
                                            <h3>Próxima entrega: {{ $store->slot }}</h3>
                                        </div>
                                        <div class="btn_store">
                                            <a href="{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}" class="btn" style="background-color: #E91A58; color: #fff">Ir a comprar</a>
                                        </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                    <br /><h2>No tenemos supermecados disponibles en este momento.</h2><br /><br />
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('layout_footer')

</div>

@stop