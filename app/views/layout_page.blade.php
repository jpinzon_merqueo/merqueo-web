<!doctype html>
<html lang="es">
<head>
    <base href="/">
    <meta name="robots" content="{{ $robots }}">
    <meta charset="utf-8">
    <title>@yield('title', 'El supermercado del ahorro. Entrega a domicilio - Merqueo.com')</title>
    <meta name="description" content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')">
    <meta name="keywords" content="@yield('keywords', 'supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express')">
    <meta name="author" content="Merqueo">
    <meta name="viewport" content="width=device-width">
    <meta name="apple-itunes-app" content="app-id=1080127941">
    <meta name="google-play-app" content="app-id=com.merqueo">

    <meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Merqueo.com" />
    <meta property="og:url" content="{{ Request::url() }}" />
    @if (Request::segment(1) == 'registro' && isset($metadata['code_referred_title']))
    <meta property="og:title" content="@if($metadata['code_referred_title']) {{ $metadata['code_referred_title'] }} @else {{ Config::get('app.referred.share_title') }} @endif" />
    <meta property="og:description" content="@if($metadata['code_referred_description']) {{ $metadata['code_referred_description'] }} @else {{ Config::get('app.referred.share_description') }} @endif" />
    <meta property="og:image" content="{{ Config::get('app.referred.share_image_url') }}" />
    @else
    <meta property="og:title" content="@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')" />
    <meta property="og:description" content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')" />
    <meta property="og:image" content="{{ asset_url() }}img/fb_logo.jpg" />
    @endif

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css?v={{Cache::get('js_version_number')}}">
    <link rel="stylesheet" href="{{asset_url()}}/lib/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="{{asset_url()}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset_url()}}/css/style.css">
    <link rel="stylesheet" href="{{asset_url()}}/css/base.css?v={{Cache::get('js_version_number')}}">
    <link rel="stylesheet" href="{{asset_url()}}/css/cities.css">
    <link rel="stylesheet" href="{{asset_url()}}css/smart-app-banner.css?v={{Cache::get('js_version_number')}}">

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset_url()}}lib/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="{{asset_url()}}css/layout-page.css">

    @include('common/meta_icons')

    <link rel="canonical" href="{{ Request::url() }}">

    <script>
        var fb_app_id = '{{ Config::get("app.facebook_app_id") }}';
        var web_url = '{{ web_url() }}';
        var google_api_key = '{{ Config::get("app.google_api_key") }}';
    </script>

    <script type="text/javascript" src="{{asset_url()}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/Leanplum_JavaScript-1.2.4/leanplum.min.js"></script>
    @include('common/analytics')
    <script type="text/javascript" src="{{asset_url()}}js/material.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.lazyload.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.elevatezoom.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/geocode.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{asset_url()}}js/general.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/checkout.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{asset_url()}}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/smart-app-banner.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/bar-new-web.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/ui.js?v={{ Cache::get('js_version_number') }}"></script>
    @if (Request::segment(1) == 'registro')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    @endif

    @yield('extra_assets')
</head>

<body class="home_init">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="{{ web_url() }}" title="Merqueo.com">
                        <img src="{{ asset_url() }}img/Logo.svg" style="width: 140px" alt="Merqueo.com">
                    </a>
                </div>
            </div>
        </div>
    </nav>

    @yield('content')

</body>

</html>
