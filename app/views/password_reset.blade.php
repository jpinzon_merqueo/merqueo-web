@extends('layout_page')

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{ asset_url() }}js/new-age.min.js"></script>
<!-- Custom Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
<link href="{{ asset_url() }}css/reset_password.css" rel="stylesheet">
<style type="text/css">
   body { background-color: white; }
   form.home input, form.login input, form.signup input { height: 34px;}
   form.home { text-align: left !important; }
   label { font-weight: bold; }
</style>
<!--div class="container-fluid"-->
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 image-remind">
	            <div class="header-content">
	                <div class="header-content-inner">
	                    <img src="{{ asset_url() }}img/reset_password/page.svg" alt="">
	                </div>
	            </div>
	        </div>
	        <div class="col-sm-6 formComplete">
	            <div>
	            	<h1>RESTABLECE TU CONTRASEÑA</h1>
	                <p>Ingresa una nueva contraseña y confírmala</p>
	                <form class="home reset" method="POST" action="{{ action('RemindersController@postReset') }}">
	                	<div class="@if(!Session::has('error')) unseen @endif alert alert-danger form-has-errors">@if(Session::has('error')) {{ Session::get('error') }} @endif</div>
	                    <div class="form-group">
	                        <label for="Password1"><span class="pinkLabel">*</span> Contraseña</label>
	                        <input type="password" class="form-control" id="password" name="password">
	                    </div>
	                    <div class="form-group">
	                        <label for="Password1"><span class="pinkLabel">*</span> Confirmar Contraseña</label>
	                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
	                    </div>
						<input type="hidden" name="token" value="{{ $token }}" />
	                    <button type="submit" class="btn btn-default crearCuentaBTN">RESTABLECER CONTRASEÑA</button>
	                </form>
	            </div>
	        </div>
		</div>
		</div>
		<section>



<script>
$('form.reset').on('submit', function(e) {
	e.preventDefault();
	var error = false;
	var form = this;

	$('input[type!="hidden"]', this).each(function() {
		if($(this).val().length == 0) {
			$(this).parent().addClass('has-error');
			error = "Debes ingresar todos los campos";
		}else $(this).parent().removeClass('has-error');
	});

	if(!error) {
		if($('input[name="password"]', this).length
			&& $('input[name="password"]').val() != $('input[name="password_confirmation"]').val()) {
			error = "La contraseña ingresada no coincide con la confirmación";
			$('input[name="password"]').parent().addClass('has-error');
			$('input[name="password_confirmation"]').parent().addClass('has-error');
		}else{
			$('input[name="password"]').parent().removeClass('has-error');
			$('input[name="password_confirmation"]').parent().removeClass('has-error');
		}
	}

	if(error) {
		$('.form-has-errors').html(error).fadeIn();
		return false;
	}else{
		form.submit();
    	return true;
	}
});
</script>

@stop