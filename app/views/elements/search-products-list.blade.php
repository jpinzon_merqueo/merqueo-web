@if (count($products))
@foreach ($products as $product)
<div class="product-column column text-center">
    <div class="product-list product-{{ $product['id'] }}">
        <a href="javascript:;">
            <div class="product-cart-counter">
                <i class="icon-shopping-cart"></i>
                <span class="count">0</span>
            </div>

            <div class="product-cart">
                <span class="count"></span>
                <span class="sub-text">EN CARRITO</span>
            </div>

            <div class="cart-less" data-id="{{ $product['id'] }}" data-warning="0">
                <span class="glyphicon glyphicon-minus"></span>
            </div>
            <div class="cart-add" data-id="{{ $product['id'] }}" data-warning="0">
                <span class="glyphicon glyphicon-plus"></span>
            </div>
            <div class="cart-add-initial mq-new-button-blue" data-id="{{ $product['id'] }}" data-name="{{ $product['name'] }}"  data-price="@if(!empty($product['special_price'])){{ $product['special_price'] }}@else{{ $product['price'] }}@endif" data-warning="{{ $product['has_warning'] }}">
                <p>AGREGAR</p>
            </div>

            @if($product['is_best_price'])
                <div class="product-best">
                    <div id="ico_best">
                        <img src="{{ asset_url() }}img/pesos.png" alt="best-product">
                    </div>
                    <div id="text_best">
                        <h6>Mejor precio garantizado</h6>
                    </div>
                </div>
            @else
                <div class="product-best" style="opacity: 0;">
                    <div id="ico_best">

                    </div>
                    <div id="text_best">
                    </div>
                </div>
            @endif

            <div class="product-list-img">
                @if (!Session::get('has_orders') && $product['first_order_special_price'])
                <!-- <img class="first_order_promo" src="{{ asset_url() }}img/first_order_promo.png" alt="first_order_promo">-->
                @endif
                <img id="_image2" data-original="{{ $product['image_medium_url'] }}" src="{{ asset_url() }}img/placeholder.jpg" alt="{{ $product['slug'] }}">
            </div>
        </a>

        <div class="product-list-info text-left price_for_product">
            <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $product['city_slug'], 'store_slug' => $product['store_slug'], 'department_slug' => $product['department_slug'], 'shelf_slug' => $product['shelf_slug'], 'product_slug' => $product['slug']]) }}" class="name-link">
                <p class="product-name">{{ $product['name'] }}</p>
            </a>
            @if($product['quantity'] != '' && $product['unit'] !='')
            <p>{{ $product['quantity'] }} {{ $product['unit'] }}</p>
            @endif
            @if ($product['special_price'] > -1)
                <div class="product-list-prize">
                    <strike>${{ number_format($product['price'],0, ',', '.') }}</strike>
                </div>
                <div class="product-list-prize product-special-price">
                    ${{ number_format($product['special_price'],0, ',', '.') }}
                </div>
            @else
                <div class="product-list-prize">
                    ${{ number_format($product['price'],0, ',', '.') }}
                </div>
            @endif
            @if(isset($product['pum']))
                <p class="product-pum">
                    @if($product['type'] == 'Simple')
                        <small>{{ $product['pum'] }}</small>
                    @elseif($product['type'] == 'Agrupado')
                        @foreach(explode('|', $product['pum'], 1) as $pum)
                            @if($pum)
                                <small>{{ mb_substr($pum, 0, 22)  }} ...</small>
                            @endif
                        @endforeach
                    @endif
                </p>
            @endif

            @if ($product['special_price'])
                <div class="contbad">
                    <div class="badgesale">
                        - {{ round((($product['price'] - $product['special_price']) * 100 / $product['price']), 0) }} %
                    </div>
                </div>
            @endif

            @if ($product['delivery_discount_amount'])
                <div class="delivery-discount">
                    <i class="fa"><img src="{{ asset_url() }}img/truck-icon.png" alt="truck"></i>Ahorra ${{ number_format($product['delivery_discount_amount'], 0, ',', '.') }} en el domicilio
                </div>
            @endif
        </div>

    </div>
</div>
@endforeach
@endif