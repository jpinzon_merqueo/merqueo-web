<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h1>{{ $banner->content_title }}</h1>
            <button type="button" class="close" data-dismiss="modal">
                <img src="{{ asset_url() }}/img/main_banner/close-button.png" alt="Cerrar">
            </button>
        </div>
        <div class="modal-body banner-modal-content-wrap">
            <div style="margin-top: 20px; font-size: 18px">
                <b class="banners-sell-off-title">Productos en promoción</b>
                <div class="bar-underlined-title"></div>
            </div>
            <section class="banner-modal-product-list"
                     style="display: block; width: 88%;position: relative;margin-left: 6%;">

                <img src="/assets/img/rolling.svg" alt="" class="rolling-loader">

                <?php

                // Para mostrar el listado de productos es necesario
                // mapear los modelos para que tengan el mismo
                // formato utilizado en toda la página web.

                $isSellOffList = true;

                $products = $banner->bannerStoreProducts->map(function (StoreProduct $storeProduct) use (&$isSellOffList
                ) {
                    $storeProduct->name = $storeProduct->product->name;
                    $storeProduct->warning = empty($storeProduct->shelf->has_warning) ? 0 : $storeProduct->shelf->has_warning;
                    $storeProduct->image_medium_url = $storeProduct->product->image_medium_url;
                    $storeProduct->slug = $storeProduct->product->slug;
                    $storeProduct->department_slug = $storeProduct->department->slug;
                    $storeProduct->store_slug = $storeProduct->store->slug;
                    $storeProduct->city_slug = $storeProduct->store->city->slug;
                    $storeProduct->shelf_slug = empty($storeProduct->shelf->slug) ?: $storeProduct->shelf->slug;
                    $storeProduct->quantity = $storeProduct->product->quantity;
                    $storeProduct->unit = $storeProduct->product->unit;

                    if (empty($storeProduct->special_price)) {
                        $isSellOffList = false;
                    }

                    $storeProduct->type = $storeProduct->product->type;
                    $storeProduct->pum = (isset($storeProduct->pum) ? $storeProduct->pum : null);

                    return $storeProduct;
                });

                ?>

                <div class="banner-informative-list" data-is-promo-type="{{ $isSellOffList }}">
                    <?php $banner->store['city_slug'] = $banner->store->city->slug ?>
                    {{ View::make('elements.products-list', ['products' => $products, 'referrer' => 'banners', 'store' => $banner->store, 'variation_key' => null]) }}
                </div>
            </section>
            <section class="banner-modal-content" style="margin-top: 10px">
                {{ $banner->content }}
            </section>
        </div>
    </div>
</div>
