<div class="modal fade" id="login">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-10 col-sm-10 col-xs-12 col-xs-offset-1">
          	<h4 class="text-center">¿No tienes una Cuenta?</h4><br />
            <div class="login-fb text-center">
              <div class="unseen alert alert-danger fb-login-error"></div>
   		  	  <div align="center" class="fb-login-loading unseen"><img src="{{ asset_url() }}/img/loading.gif" /></div>
              <button type="button" class="btn btn-primary btn-block fb-login"><i class="icon-facebook"></i>&nbsp;&nbsp;&nbsp;Ingresa o registrate con tu cuenta de Facebook</button>
              <button type="button" class="btn-block btn btn-success btn-phone-register">Registrate con tu celular</button>
            </div>
            <hr>
            <h4 class="text-center">Ingresa con tu Cuenta de Merqueo</h4><br />
            <div class="unseen alert alert-danger form-has-errors"></div>
            <form role="form" class="login" method="post" action="{{ route('frontUser.verify', ['redirect' => str_replace('/', ';', Request::path())]) }}">
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Correo eléctronico" name="email">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" placeholder="Contraseña" name="password">
              </div>
              <button type="submit" class="btn btn-lg btn-info col-md-3 col-sm-3 col-xs-3 col-xs-offset-9">INGRESAR</button>
            </form>
            <h5 class="text-center revreload">
              <a href="{{ web_url() }}/recuperar-clave">¿Olvidaste tu contraseña?</a><br />
              <!--Aún no tienes cuenta? <a href="{{ web_url() }}">Regístrate</a>-->
            </h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
      $('.btn-phone-register')
        .off()
        .on('click', function () {
          toggleButtons(true);
          askValidation(null, function (error, phoneNumber) {
              if (error) {
                  var message = '';
                  if (error instanceof FormValidationError) {
                      message = error.getErrorDetails();
                  } else {
                      message = error.message;
                  }

                  toggleButtons(false);
                  if (isModalShown()) {
                      $('.fb-login-error').html(message).fadeIn();
                      return;
                  }

                  if (typeof errorValidation !== 'undefined') {
                      errorValidation(error);
                      return;
                  }

                  var messageContainer = $('.login-fb');
                  if (messageContainer.length) {
                      messageContainer.append('<p class="message" style="color: #b94a48; margin-top: 5px;">' + message + '</p>');
                  } else {
                      $('.login-fb p.message').text(message);
                  }

                  return;
              }

              if (typeof successValidation === 'undefined') {
                  toggleButtons(false);
                  window.location.replace(web_url + '/registro');
              } else {
                  successValidation();
                  if (isModalShown()) {
                      $('#login').modal('toggle');
                      $('input[name="phone"]').val(phoneNumber);
                      $('.validate-button').addClass('disabled');
                  }
              }
          });
        });

      /**
       * @param disable
       */
      function toggleButtons(disable) {
          $('.fb-login, .btn-phone-register').prop('disabled', disable);
      }
  });
</script>
