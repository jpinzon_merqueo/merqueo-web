@extends('layout_page')

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{ asset_url() }}js/new-age.min.js"></script>
<!-- Custom Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
<link href="{{ asset_url() }}css/reset_password.css" rel="stylesheet">
<style type="text/css">
   body { background-color: white; }
   form.home input, form.login input, form.signup input { height: 34px;}
   form.home { text-align: left !important; }
   label { font-weight: bold; }
</style>
<!--div class="container-fluid"-->
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 thankyouPage">
	            <h1>TU CONTRASEÑA HA SIDO RESTABLECIDA</h1>
	            <img class="thankyouImg" src="{{ asset_url() }}img/reset_password/thankyou.svg" alt="">
	            <br><br>
	            @if ( $user_agent['mobile_browser'] || $user_agent['tablet_browser'] )
                    @if ( $user_agent['control_mobile'] )
                        <a href="{{ Config::get('app.ios_url') }}" class="btn btn-default crearCuentaBTN">Descargar aplicación</a>
                    @else
                        <a href="{{ Config::get('app.android_url') }}" class="btn btn-default crearCuentaBTN">Descargar aplicación</a>
                    @endif
                @else
                	<button type="button" class="btn btn-default thankyouBTN" onclick="window.location = '{{ web_url() }}'">IR A COMPRAR AHORA</button>
                @endif
            </div>
		</div>
	</div>
<section>
@stop