@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">

        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alert!</b> {{ Session::get('error') }}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title }}</h3>
                    </div>
                    <form enctype="multipart/form-data" role="form" method="post" id='main-form'
                          action="{{ route('adminTransporter.save') }}">
                        <input type="hidden" name="id" value="{{ $transport ? $transport->id : "" }}">
                        <table width="100%" class="orders-table">
                            <tr>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Tipo de documento</label>
                                        <select name="document_type" id="document_type" required class="form-control">
                                            <option value="Cédula de ciudadania"
                                                    @if($transport->document_type == 'Cédula de ciudadania') selected="selected" @endif>
                                                Cédula de ciudadania
                                            </option>
                                            <option value="NIT"
                                                    @if($transport->document_type == 'NIT') selected="selected" @endif>
                                                NIT
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nombre completo</label>
                                        <input type="text" class="form-control" id="" name="fullname"
                                               placeholder="Nombre completo" required
                                               value="{{ $transport ? $transport->fullname : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" class="form-control" id="" name="address"
                                               placeholder="Dirección" required
                                               value="{{ $transport ? $transport->address : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Celular</label>
                                        <input type="text" class="form-control" id="" name="cellphone" maxlength="10"
                                               placeholder="Celular"
                                               value="{{ $transport ? $transport->cellphone : "" }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="text" class="form-control" id="" name="phone"
                                               placeholder="Telefono" value="{{ $transport ? $transport->phone : "" }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Pensión</label>
                                        <select id="afp" name="afp" class="form-control col-xs-12"
                                                style="margin-bottom: 15px;">
                                            <option></option>
                                            @foreach ($seguritySocials as $seguritySocial)
                                                @if($seguritySocial->type == 'AFP')
                                                    <option value="{{ $seguritySocial->id }}"
                                                           @if(isset($transport->afp_id) && $transport->afp_id == $seguritySocial->id ) selected="selected" @endif  >{{ $seguritySocial->name }}</option>

                                                @endif

                                            @endforeach
                                        </select>
                                        </br>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                @if(!isset($transport->file_rut_url))
                                                    <div class="row">
                                                        <div class="col-xs-12">

                                                            <label title="Archivo RUT" for="file_rut_url" >Archivo
                                                                RUT</label>
                                                            <input type="file" class="form-control"  accept=".pdf" name="file_rut_url" required>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label title="Archivo RUT" for="file_rut_url">Archivo
                                                                RUT</label>
                                                            <input type="file" class="form-control"  accept=".pdf" name="file_rut_url" >

                                                        </div>
                                                    </div>
                                                    @if($transport->file_rut_url != '')
                                                    <a href="{{$transport->file_rut_url}}"  target="_blank">Ver Archivo</a>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Número de documento</label>
                                        <input type="text" class="form-control" id="" name="document_number"
                                               placeholder="Número de documento" required
                                               value="{{ $transport ? $transport->document_number : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" id="" name="email" placeholder="Email"
                                               value="{{ $transport ? $transport->email : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Complemento dirección</label>
                                        <input type="text" class="form-control" id="" name="address_supplement"
                                               placeholder="Oficina "
                                               value="{{ $transport ? $transport->address_supplement : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Ciudad</label>
                                        <select id="city_id" name="city" required class="form-control col-xs-12"
                                                style="margin-bottom: 15px;">
                                            <option></option>
                                            @foreach ($cities as $city)
                                                @if($transport->city_id == '')
                                                    <option value="{{ $city->id }}"
                                                            @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @else
                                                    <option value="{{ $city->id }}"
                                                            @if( $transport->city_id == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        </br>
                                    </div>
                                    <div class="form-group">
                                        <label>Salud</label>
                                        <select id="eps" name="eps" class="form-control col-xs-12"
                                                style="margin-bottom: 15px;">
                                            <option></option>
                                            @foreach ($seguritySocials as $seguritySocial)
                                                @if($seguritySocial->type == 'EPS')
                                                    <option value="{{ $seguritySocial->id }}"
                                                            @if( isset($transport->afp_id) && $transport->eps_id == $seguritySocial->id) selected="selected" @endif  >{{ $seguritySocial->name }}</option>

                                                @endif

                                             @endforeach
                                        </select>
                                        </br>
                                    </div>
                                    <div class="form-group">
                                        <label>ARL</label>
                                        <select id="arl" name="arl" class="form-control col-xs-12"
                                                style="margin-bottom: 15px;">
                                            <option></option>
                                            @foreach ($seguritySocials as $seguritySocial)
                                                @if($seguritySocial->type == 'ARL')
                                                    <option value="{{ $seguritySocial->id }}"
                                                            @if(isset($transport->arl_id) &&$transport->arl_id == $seguritySocial->id ) selected="selected" @endif >{{ $seguritySocial->name }}</option>

                                                @endif

                                             @endforeach
                                        </select>
                                        </br>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo de flota</label>
                                        {{ Form::fillSelectWithoutKeys('type', ['Propia' => 'Propia', 'Tercerizada' => 'Tercerizada'], $transport->type, ['class' => 'form-control col-xs-12', 'id' => 'type'], TRUE) }}
                                    </div>


                                </div>

                                <br>
                                <div class="box-footer pull-right">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $("#main-form").validate({
            rules: {
                name: "required",
                sort_order: "required",
                type: "required",

            }
        });
    </script>

@stop

