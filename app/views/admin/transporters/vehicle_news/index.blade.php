@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <div class="row">
        <div class="col-xs-12">
            <button data-toggle="modal" data-target="#modal-report" type="button" class="btn btn-primary export-balance pull-right">Generar reporte</button>
        </div>
    </div>
</section>
<section class="content">
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Información</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form" method="GET" autocomplete="off">
                                <div class="table-responsive">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                {{ Form::label('city_id', 'Ciudad:') }}
                                            </td>
                                            <td>
                                                {{ Form::fillSelectWithKeys('city_id', $cities, $city, array('class' => 'form-control', 'id' => 'city_id'), 'id', 'city' ) }}
                                            </td>
                                            <td align="right">
                                                {{ Form::label('plate', 'Placa:') }}
                                            </td>
                                            <td>
                                                {{ Form::text('plate', '', ['id' => 'plate', 'class' => 'form-control', 'placeholder' => 'Placa']) }}
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                {{ Form::submit('Buscar', ['class' => 'btn btn-primary', 'id' => 'search']) }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                {{ Form::token() }}
                                {{ Form::hidden('_method', 'GET') }}
                            </form>
                        </div>
                    </div>
                    <br />
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
    <a href="#importing" class="fancybox unseen"></a>
    <div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
</section>
<div class="modal fade" id="modal-report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Formulario para la generación de reporte de novedades</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="GET" id='main-form' action="{{ route('adminVehicleNews.report_extend') }}" autocomplete="off">
                    <div class="form-group">
                        {{ Form::label('created_at', 'Rango de fechas') }}
                        {{ Form::text('created_at', '', ['class' => 'form-control', 'id' => 'created_at', 'placeholder' => 'DD-MM-YYYY,DD-MM-YYYY']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('city_report', 'Ciudad') }}
                        {{ Form::fillSelectWithKeys('city_report', $cities, $city, array('class' => 'form-control', 'id' => 'city_report'), 'id', 'city' ) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('transporter_id', 'Transportador') }}
                        {{ Form::fillSelectWithKeys('transporter_id', $transporters, '', array('class' => 'form-control', 'id' => 'transporter_id'), 'id', 'fullname', TRUE ) }}
                    </div>
                    {{ Form::token() }}
                    {{ Form::hidden('_method', 'GET') }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="send-form" type="button" class="btn btn-primary">Generar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var web_url_ajax = "{{ route('adminVehicleNews.index') }}";

    $(document).ready(function() {
        $('#search').on('click', function(e) {
            e.preventDefault();
            search();
        }).trigger('click');
        function search() {
            var data = $('#search-form').serialize();
            $('.paging').html('');
            $('.paging-loading').show();
            $.ajax({
                url: web_url_ajax,
                data: data,
                type: 'GET',
                dataType: 'html',
                success: function(response) {
                    $('.paging-loading').hide();
                    $('.paging').html(response);
                }, error: function() {
                    $('.paging-loading').hide();
                    $('.paging').html('Ocurrió un error al obtener los datos.');
                }
            });
        }
        $('#created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ',' + picker.endDate.format('DD-MM-YYYY'));
        });
        $('#created_at').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            format: 'DD-MM-YYYY'
        });
        $('#main-form').submit(function () {
            if($(this).valid()) {
               $('.fancybox').trigger('click');
               $('#modal-report').modal('hide')
            }
        });
        $('.fancybox').fancybox({
            autoSize    : true,
            closeClick  : false,
            closeBtn    : false ,
            openEffect  : 'none',
            closeEffect : 'none',
            helpers   : {
               overlay : {closeClick: false}
            }
        });
        $("#main-form").validate({
          rules: {
            created_at: {
                required: true
            },
            city_report: {
                required: true
            },
            transporter_id: {
                required: true
            }
          }
        });
        $('#main-form').submit(function () {
            if($(this).valid()) {
               $('.fancybox').trigger('click');
               $('#modal-report').modal('hide');
            }
        });
        $('#send-form').on('click', function(event) {
            event.preventDefault();
            $('#main-form').submit();
        });
        $('#city_report').change(function(event) {
            var $this = $(this);
            $.ajax({
                url: "{{ route('adminTransporter.ajax_transporters_city') }}",
                data: {
                    'city_id': $this.val()
                },
                type: 'GET',
                dataType: 'JSON',
                success: function(response) {
                    var transporter_id = $('#transporter_id');
                    transporter_id.empty();
                    transporter_id.append('<option value="">Seleccione</option>');
                    $.each(response.result.transporters, function(key, value){
                        transporter_id.append('<option value="' + value.id + '">'+ value.fullname + '</option>');
                    });
                }, error: function() {
                    alert('Ocurrió un error al obtener los datos.');
                }
            });
        });
        @if (Session::has('file_url'))
        setTimeout(function(){
            window.location = '{{ Session::get('file_url') }}';
        }, 1000);
        @endif
    });
</script>

@else

@section('content')
<table id="balance-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Ciudad</th>
            <th>Transportador</th>
            <th>Identificación</th>
            <th>Celular</th>
            <th>Placa de vehiculo</th>
            <th>Novedades</th>
        </tr>
    </thead>
    <tbody>
    @if (count($vehicles))
        @foreach($vehicles as $vehicle)
        <tr>
            <td>{{ $vehicle->transporter->city->city }}</td>
            <td>{{ $vehicle->transporter->fullname }}</td>
            <td>{{ $vehicle->transporter->document_number }}</td>
            <td>{{ $vehicle->transporter->phone }}</td>
            <td>{{ $vehicle->plate }}</td>
            <td class="text-center">
                <a class="btn btn-xs btn-default" href="{{ route('adminVehicleNews.view', ['id' => $vehicle->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="6" align="center">Vehículo no encontrado</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-12">
        <div class="dataTables_info text-center" id="shelves-table_info">Mostrando {{ count($vehicles) }} registros</div>
    </div>
</div>

@endif
@stop
