@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Información del Vehículo</h3>
                </div>
                <div class="box-body">
                    <input type="hidden" name="id" id="id" value="{{ $vehicle->id }}">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-3">
                            <label>Transportador</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->driver->transporter->fullname }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Identificación</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->driver->transporter->document_number }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Placa</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->plate }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Teléfono celular</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->driver->transporter->phone }}">
                        </div>
                    </div>
                </div>
                <div class="box-header">
                    <h3 class="box-title">Resumen</h3>
                </div>
                <div class="box-body">
                     <div class="form-group clearfix">
                        <div class="col-xs-12 col-sm-4">
                            <label>Novedades</label>
                            <br>
                            <span id="novedades" class="badge bg-green" style="font-size: 20px;">{{ $values->news }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <label>Bonificaciónes</label>
                            <br>
                            <span id="bonificaciones" class="badge bg-green" style="font-size: 20px;">{{ $values->bonus }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <label>Descuentos</label>
                            <br>
                            <span id="descuentos" class="badge bg-green" style="font-size: 20px;">{{ $values->discount }}</span>
                        </div>
                     </div>
                </div>
            </div>
            <div class="box box-primary">
                @if ($admin_permissions['insert'])
                <div class="box-header">
                    <h3 class="box-title">Registrar Novedad</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('adminVehicleNews.add_news_ajax', ['id' => $vehicle->id]) }}" id="add-news" method="POST" autocomplete="off">
                        <div class="form-group clearfix">
                            <div class="col-xs-12 col-sm-2">
                                {{ Form::label('type', 'Tipo de novedad') }}
                                {{ Form::fillSelectWithoutKeys('type', $types, '', array('class' => 'form-control', 'id' => 'type'), TRUE ) }}
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                {{ Form::label('amount', 'Monto') }}
                                {{ Form::number('amount', '', ['class' => 'form-control', 'id' => 'amount', 'placeholder' => 'Monto', 'data-show_hidden' => '1']) }}
                            </div>
                            <div class="col-xs-12 col-sm-2">
                                {{ Form::label('date', 'Fecha') }}
                                {{ Form::text('date', '', ['class' => 'form-control', 'id' => 'date', 'placeholder' => 'DD-MM-YYYY']) }}
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <div class="col-xs-12 col-sm-9">
                                    {{ Form::label('comments', 'Comentarios') }}
                                    {{ Form::text('comments', '', ['class' => 'form-control', 'id' => 'comments', 'placeholder' => 'Comentarios']) }}
                                </div>
                                <div class="col-xs-12 col-sm-2">
                                    <br>
                                    <button style="position: relative; top: 5px;" type="button" id="btn-add-news" class="btn btn-primary center-block">Guardar</button>
                                </div>
                            </div>
                        </div>
                        {{ Form::token() }}
                        {{ Form::hidden('_method', 'POST') }}
                    </form>
                </div>
                @endif
                <div class="box-header">
                    <h3 class="box-title">Novedades</h3>
                </div>
                <div class="box-body">
                    <div class="form-group clearfix">
                        <div class="col-xs-12 col-sm-12">
                            <form action="{{ route('adminVehicleNews.get_news_ajax', ['id' => $vehicle->id]) }}" class="form-horizontal" id="filter-news" method="GET" autocomplete="off">
                                <div class="form-group">
                                    {{ Form::label('created_at', 'Filtro por rango de fechas', ['class' => 'col-sm-2 control-label']) }}
                                    <div class="col-sm-4">
                                        {{ Form::text('created_at', '', ['class' => 'form-control', 'id' => 'created_at', 'placeholder' => 'DD-MM-YYYY,DD-MM-YYYY']) }}
                                    </div>
                                    <div class="col-xs-12 col-sm-1">
                                        <button type="button" id="button-filter-news" class="btn btn-primary">Filtrar</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-2">
                                    <button type="button" class="btn btn-primary export-news">Exportar novedades</button>
                                </div>
                                </div>
                                {{ Form::token() }}
                                {{ Form::hidden('_method', 'GET') }}
                            </form>
                        </div>
                        <div class="col-xs-12 col-sm-12"><br></div>
                        <div class="col-xs-12 col-sm-12">
                            <div class="container-overflow" style="min-height: 400px">
                                <div class="movements">{{ $news_html }}</div>
                                <div align="center" class="loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#importing" class="fancybox unseen"></a>
    <div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
</section>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            autoSize    : true,
            closeClick  : false,
            closeBtn    : false ,
            openEffect  : 'none',
            closeEffect : 'none',
            helpers   : {
               overlay : {closeClick: false}
            }
        });
        $('#created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ',' + picker.endDate.format('DD-MM-YYYY'));
        });
        $('#created_at').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            format: 'DD-MM-YYYY'
        });
        $('#date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $("#add-news").validate({
            rules: {
                type : {
                    required: true,
                },
                amount: {
                    required: true,
                    number: true,
                    min: 0
                },
                date: {
                    required: function() {
                        return $('#type').val() == 'Bonificación';
                    }
                }
            }
        });
        $('body').on('click', '#btn-add-news', function(event) {
            event.preventDefault();
            var $form = $('#add-news'),
                data  = $form.serialize(),
                $this = $(this);
            if ($form.valid()) {
                $this.addClass('disabled');
                $('.movements').hide();
                $('.loading').show();
                $.ajax({
                    url: "{{ route('adminVehicleNews.add_news_ajax', ['id' => $vehicle->id]) }}",
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                })
                .done(function(data) {
                    if (data.status) {
                        swal(
                          '¡Bien hecho!',
                          'Se guardó correctamente la novedad.',
                          'success'
                        );
                        $form[0].reset();
                        $this.removeClass('disabled');
                        $('#button-filter-news').trigger('click');
                    } else {
                        $this.removeClass('disabled');
                        $('.movements').show();
                        $('.loading').hide();
                        swal(
                          'Oops...',
                          data.message,
                          'error'
                        );
                    }
                })
                .fail(function() {
                    $this.removeClass('disabled');
                    $('#button-filter-news').trigger('click');
                    swal(
                      'Oops...',
                      '¡Al parecer tenemos problemas tratando de guardar la novedad!',
                      'error'
                    );
                })
                .always(function() {
                    $this.removeClass('disabled');
                    $('#button-filter-news').trigger('click');
                });
            }
        });
        $('#button-filter-news').on('click', function(event) {
            var $form = $('#filter-news'),
                data  = $form.serialize(),
                $this = $(this);
            $this.addClass('disabled');
            $('.movements').hide();
            $('.loading').show();
            $.ajax({
                url: "{{ route('adminVehicleNews.get_news_ajax', ['id' => $vehicle->id]) }}",
                type: 'GET',
                dataType: 'json',
                data: data,
            })
            .done(function(data) {
                $('#novedades').html(data.values.news);
                $('#bonificaciones').html(data.values.bonus);
                $('#descuentos').html(data.values.discount);
                $('.movements').html(data.vehicle_news)
                    .show();
                $('.loading').hide();
                $this.removeClass('disabled');
            })
            .fail(function() {
                $this.removeClass('disabled');
                $('.movements').show();
                $('.loading').hide();
                $this.removeClass('disabled');
            })
            .always(function() {
                $this.removeClass('disabled');
                $('.movements').show();
                $('.loading').hide();
            });
        });
        $('body').on('click', '.news-remove-item', function(event) {
            var $this = $(this),
                $form = $this.parent(),
                data  = $form.serialize();
            $this.addClass('disabled');
            $('.movements').hide();
            $('.loading').show();
            $.ajax({
                url: "{{ route('adminVehicleNews.delete_news_ajax', ['id' => $vehicle->id]) }}",
                type: 'POST',
                dataType: 'json',
                data: data,
            })
            .done(function(data) {
                if (data.status) {
                    swal(
                      '¡Bien hecho!',
                      'Se eliminó correctamente la novedad.',
                      'success'
                    );
                    $('#button-filter-news').trigger('click');
                }
            })
            .fail(function() {
                $this.removeClass('disabled');
                $('.movements').show();
                $('.loading').hide();
                swal(
                  'Oops...',
                  '¡Al parecer tenemos problemas tratando de eliminar la novedad!',
                  'error'
                );
            })
            .always(function() {
                $this.removeClass('disabled');
                $('.movements').show();
                $('.loading').hide();
            });
        });
        $('body').on('click','.export-news', function() {
            var $form = $('#filter-news'),
                data  = $form.serialize(),
                $this = $(this);
            $this.addClass('disabled');
            $('.fancybox').trigger('click');
            $.ajax({
                url: "{{ route('adminVehicleNews.report', ['id' => $vehicle->id]) }}",
                type: 'POST',
                dataType: 'json',
                data: data,
            })
            .done(function(data) {
                $.fancybox.close();
                $this.removeClass('disabled');
                 if(data.status == 1) {
                    window.open(data.file_url,'_blank');
                 } else {
                    swal(
                      'Oops...',
                      data.message,
                      'error'
                    );
                 }
            })
            .fail(function() {
                $.fancybox.close();
                $this.removeClass('disabled');
                swal(
                  'Oops...',
                  '¡Al parecer tenemos problemas tratando de exportar las novedades!',
                  'error'
                );
            })
        });

    });
</script>

@stop
