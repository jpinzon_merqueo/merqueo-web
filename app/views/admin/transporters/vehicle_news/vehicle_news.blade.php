<table id="shoppers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Tipo</th>
            <th>Valor</th>
            <th>Comentarios</th>
            <th>Fecha</th>
            <th>Hecho por</th>
            <th>Fecha de registro</th>
            @if ($admin_permissions['delete'])
            <th>Eliminar</th>
            @endif
        </tr>
    </thead>
    <tbody>
    @if (count($vehicle_news))
        @foreach($vehicle_news as $vehicle_new)
        <tr>
            <td>{{ $vehicle_new->type}}</td>
            <td>${{ number_format($vehicle_new->value, 0, ',', '.') }}</td>
            <td>{{ $vehicle_new->comments}}</td>
            <td>{{ !empty($vehicle_new->date) ? date('d-m-Y', strtotime($vehicle_new->date)) : ''; }}</td>
            <td>{{ $vehicle_new->admin->fullname }}</td>
            <td>{{ date('d-m-Y', strtotime($vehicle_new->created_at)); }}</td>
            @if ($admin_permissions['delete'])
            <td>
                <div class="btn-group">
                    <form action="{{ route('adminVehicleNews.delete_news_ajax', ['id' => $vehicle_new->vehicle->id]) }}" method="POST">
                        <a class="btn btn-xs btn-default news-remove-item" data-vehicle="{{ $vehicle_new->vehicle->id }}" data-id="{{ $vehicle_new->id }}" href="javascript:;"><span class="glyphicon glyphicon-remove"></span></a>
                        {{ Form::token() }}
                        {{ Form::hidden('id', $vehicle_new->id) }}
                        {{ Form::hidden('_method', 'POST') }}
                    </form>
                </div>
            </td>
            @endif
        </tr>
        @endforeach
    @else
        <tr>
            <td colspan="@if($admin_permissions['delete']){{'7'}} @else {{'6'}}@endif" align="center">No éxisten novedades para este vehículo.</td>
        </tr>
    @endif
    </tbody>
</table>
