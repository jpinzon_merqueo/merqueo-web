@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div>
                <form role="form" enctype="multipart/form-data" method="post" id='main-form' action="{{ route('adminAssistant.save_assistant') }}" >
                    <input type="hidden" name="id" value="{{ isset($assistants->id) ? $assistants->id : "" }}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" id="" name="first_name" placeholder="Nombre" required value="{{ isset($assistants->id) ? $assistants->first_name : "" }}">
                            </div>

                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Apellido</label>
                                <input type="text" class="form-control" id="" name="last_name" placeholder="Apellido" required value="{{ isset($assistants->id) ? $assistants->last_name : "" }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Teléfono</label>
                                <input type="text" class="form-control" id="" name="phone" placeholder="Teléfono" required value="{{ isset($assistants->id) ? $assistants->phone : "" }}">
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Conductor</label>
                                <select id="driver_id" name="driver_id" required class="form-control col-xs-12" style="margin-bottom: 15px;">
                                    <option></option>
                                    @foreach ($drivers as $driver)
                                        <option value="{{ $driver->id }}" @if( isset($assistants->driver_id) && $driver->id == $assistants->driver_id ) selected="selected" @endif >{{ $driver->last_name }} {{ $driver->first_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Número de documento</label>
                                <input type="text" class="form-control" id="" name="document_number" placeholder="Número de documento" required value="{{ isset($assistants->id) ? $assistants->document_number : "" }}">
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Celular</label>
                                <input type="text" class="form-control" id="" name="cellphone" placeholder="Celular" maxlength="10" required value="{{ isset($assistants->id) ? $assistants->cellphone : "" }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label for="eps_id">Salud</label>
                                <select id="eps_id" name="eps_id" class="form-control col-xs-12"
                                        style="margin-bottom: 15px;">
                                    <option></option>
                                    @foreach ($seguritySocials as $seguritySocial)
                                        @if($seguritySocial->type == 'EPS')
                                            <option value="{{ $seguritySocial->id }}"
                                                    @if( isset($assistants->eps_id) && $assistants->eps_id == $seguritySocial->id) selected="selected" @endif  >{{ $seguritySocial->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label for="afp_id">Pensión</label>
                                <select id="afp_id" name="afp_id" class="form-control col-xs-12"
                                        style="margin-bottom: 15px;">
                                    <option></option>
                                    @foreach ($seguritySocials as $seguritySocial)
                                        @if($seguritySocial->type == 'AFP')
                                            <option value="{{ $seguritySocial->id }}"
                                                    @if( isset($assistants->afp_id) && $assistants->afp_id == $seguritySocial->id) selected="selected" @endif  >{{ $seguritySocial->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-xs-12 col-sm-6 form-group">
                                <label for="arl_id">ARL</label>
                                <select id="arl_id" name="arl_id" class="form-control col-xs-12"
                                        style="margin-bottom: 15px;">
                                    <option></option>
                                    @foreach ($seguritySocials as $seguritySocial)
                                        @if($seguritySocial->type == 'ARL')
                                            <option value="{{ $seguritySocial->id }}"
                                                    @if( isset($assistants->arl_id) && $assistants->arl_id == $seguritySocial->id) selected="selected" @endif  >{{ $seguritySocial->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label for="status">¿Tiene chaqueta?</label>
                                <select class="form-control required" id="has_jacket" name="has_jacket">
                                    <option value="">-Selecciona-</option>
                                    <option value="1" {{ isset($assistants->id) && ($assistants->has_jacket == 1) ? "selected" : ''}}>Sí</option>
                                    <option value="0" {{ isset($assistants->id) && ($assistants->has_jacket == 0) ? "selected" : ''}}>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-xs-12 col-sm-6 form-group">
                                <label for="status">¿Tiene camiseta?</label>
                                <select class="form-control required" id="has_tshirt" name="has_tshirt">
                                    <option value="">-Selecciona-</option>
                                    <option value="1" {{ isset($assistants->id) && ($assistants->has_tshirt == 1) ? "selected" : ''}}>Sí</option>
                                    <option value="0" {{ isset($assistants->id) && ($assistants->has_tshirt == 0) ? "selected" : ''}}>No</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label for="status">¿Tiene gorra?</label>
                                <select class="form-control required" id="has_cap" name="has_cap">
                                    <option value="">-Selecciona-</option>
                                    <option value="1" {{ isset($assistants->id) && ($assistants->has_cap == 1) ? "selected" : ''}}>Sí</option>
                                    <option value="0" {{ isset($assistants->id) && ($assistants->has_cap == 0) ? "selected" : ''}}>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12  col-sm-6 form-group">
                                    @if(!isset($assistants->photo_url))
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label title="photo_url" for="photo_url">Foto</label>
                                                <input type="file" class="form-control"  accept="image/x-png,image/jpeg" name="photo_url" ></div>
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label title="photo_url" for="photo_url">Foto</label>
                                                <input type="file" class="form-control"  accept="image/x-png,image/jpeg" name="photo_url" >
                                            </div>
                                        </div>
                                        @if($assistants->photo_url != '')
                                            <a href="{{$assistants->photo_url}}"q target="_blank">Ver Archivo</a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 form-group pull-right">
                                <button type="submit" class="btn btn-primary">Guardar </button>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</section>

<script type="text/javascript">
$("#main-form").validate({
  rules: {
      name: "required",
      sort_order: "required",
      has_cap: "required",
      has_jacket: "required",
      has_tshirt: "required"
  }
});

$(document).ready(function(){
    $('#due_date').datetimepicker({
        format: 'DD/MM/YYYY'
    });
});

</script>

@stop

