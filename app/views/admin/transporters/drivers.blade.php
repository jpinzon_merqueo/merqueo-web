@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminTransporter.addDriver') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Conductor</button>
            </a>
        </span>

    </section>
    <section class="content">
    @if(Session::has('message') )
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @endif
    @endif

    <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="driver-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td width="30%">
                                                <select id="city_id" name="city_id" class="form-control">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif>{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Transportador:</label>
                                            </td>
                                            <td width="30%">
                                                <select id="transporter_id" name="transporter_id" class="form-control">
                                                    <option value="">-Selecciona-</option>
                                                    @foreach($transporters as $transporter)
                                                        <option value="{{ $transporter->id }}">{{ $transporter->fullname }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Buscar:</label>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="Nombre conductor" id="input-search" class="form-control">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var web_url_ajax = "{{ route('adminTransporter.drivers') }}";
        var web_url_ajax_transporters = "{{ route('adminTransporter.ajax_transporters_city') }}";

        $(document).ready(function() {
            paging(1, '');

            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });

            $('#city_id').change(function(e) {
                var data = {'city_id': $('#city_id').val()};
                $.ajax({
                    url: web_url_ajax_transporters,
                    data: data,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        var transporter_id = $('#transporter_id');
                        transporter_id.empty();
                        transporter_id.append('<option value="">-Selecciona-</option>');
                        $.each(response.result.transporters, function(key, value){
                            transporter_id.append('<option value="' + value.id + '">'+ value.fullname + '</option>');
                        });
                    }, error: function() {
                        alert('Ocurrió un error al obtener los datos.');
                    }
                });
            });
        });
    </script>

@else

    @section('content')
    <br>
    <table id="driver-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Ciudad</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Teléfono</th>
            <th>Celular</th>
            <th>Tipo de conductor</th>
            <th>Categoria licencia</th>
            <th>Número de licencia</th>
            <th>Fecha de expiración</th>
            <th>Estado</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @if(count( $drivers ))

            @foreach($drivers as $driver)
            <tr>
                <td>{{ $driver->city_name }}</td>
                <td>{{ $driver->first_name }}</td>
                <td>{{ $driver->last_name }}</td>
                <td>{{ $driver->phone }}</td>
                <td>{{ $driver->cellphone }}</td>
                <td>{{ $driver->profile }}</td>
                <td>{{ $driver->category_license }}</td>
                <td>{{ $driver->document_number }}</td>
                <td>{{ date("d M Y",strtotime($driver->due_date))}}</td>
                <td>@if ($driver->status)
                        <span class="badge bg-green">Activo</span>
                    @else
                        <span class="badge bg-red">Inactivo</span>
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminTransporter.edit_driver', ['id' => $driver->id]) }}">Editar</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminTransporter.delete_driver', ['id' => $driver->id]) }}"  onclick="return confirm('¿Estas seguro que deseas cambiar el estado del conductor?')">Cambiar Estado</a></li>
                        </ul>
                    </div>
                </td>
            </tr>

            @endforeach
        @else
            <tr>
                <td align="center">No hay conductores.</td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"></td>
            </tr>

        @endif

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info"></div>
        </div>
    </div>

@endif

    <script>
        $(function() {
            // DataTable
            var table = $('#driver-table').DataTable( {
                "bFilter": false
            } );
        });
    </script>
@stop