@if(!Request::ajax())
@extends('admin.layout')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <div class="alert alert-success alert-dismissable unseen">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> <div class="message"></div>
        </div>
        <div class="alert alert-danger alert-dismissable unseen">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alert!</b> <div class="message"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::open(['url' => '#filter-form-validation', 'class' => 'form-group']) }}
                                <div class="row">
                                    <div class="col-md-4">
                                        {{ Form::label('city_id', 'Ciudad') }}
                                        {{ Form::fillSelectWithKeys('city_id', $cities, '', ['class' => 'form-control'], 'id', 'city', true) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::label('warehouse_id', 'Bodega') }}
                                        {{ Form::select('warehouse_id', ['' => 'Seleccione'], '', ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::label('route_id', 'Ruta') }}
                                        {{ Form::select('route_id', ['' => 'Seleccione'], '', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body" id="section_orders">
                        Utiliza el filtro para traer las ordenes y validar los precintos.
                    </div>
                </div>
            </div>
        </div>
        @include('admin.transporters.validation.js.index-js')
    </section>
@endsection
@else
@section('content_section_orders')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-1 text-center">
                    <h4>ID</h4>
                </div>
                <div class="col-md-2 text-center">
                    <h4>Nombre / Dirección</h4>
                </div>
                <div class="col-md-1 text-center">
                    <h4>Estado</h4>
                </div>
                <div class="col-md-2 text-center">
                    <h4>Contenedores</h4>
                </div>
                <div class="col-md-4 text-center">
                    <h4>Precintos</h4>
                </div>
                <div class="col-md-2 text-center"> </div>
            </div>
            <br>
            @if($orders->count())
                @foreach($orders as $order)
                {{ Form::open(['route' => ['adminTransporter.validation.add_seals_to_order', $order->id], 'class' => 'form-group validation-form']) }}
                    <div class="row">
                        <div class="col-md-1 text-center">
                            {{ $order->id }}
                        </div>
                        <div class="col-md-2 text-center">
                            {{ $order->orderGroup->user_firstname }} {{ $order->orderGroup->user_lastname }} /
                            {{ $order->orderGroup->user_address }} {{ $order->orderGroup->user_address_further }}
                        </div>
                        <div class="col-md-1 text-center">
                            <span class="badge bg-green">{{ $order->status }}</span>
                        </div>
                        <div class="col-md-2 text-center">
                            {{ Form::number('containers', $order->picking_baskets, ['class' => 'form-control contenedores', 'id' => 'container_' . $order->id, 'placeholder' => 'Número de contenedores.', 'min' => '1']) }}
                        </div>
                        <div class="col-md-4">
                            {{ Form::text('seals', $order->seals, ['class' => 'form-control', 'data-role' => 'tagsinput', 'id' => 'seals_' . $order->id]) }}
                            <p class="small">Precintos de la orden separados por "," Ej: 001,002,003</p>
                        </div>
                        <div class="col-md-1 text-center">
                            {{ Form::button('Guardar', ['class' => 'btn btn-primary btn-save-validation']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                @endforeach
            @else
                <div class="row">
                    <div class="col-md-12">
                        <p>Existen ordenes en un estado diferente a <span class="badge bg-green">Alistado</span>.</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/tagsinput/bootstrap-tagsinput.css" />
    <script src="{{ web_url() }}/admin_asset/js/plugins/tagsinput/bootstrap-tagsinput.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/tagsinput/bootstrap-tagsinput-angular.js" type="text/javascript"></script>
@endsection
@endif