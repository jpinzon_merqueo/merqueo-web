<script>
$(document).ready(function () {
    $('#city_id').change(function () {
        loadWarehouses();
    });

    $('#warehouse_id').change(function () {
        loadRoutes();
    });

    $('#route_id').change(function () {
        loadOrders();
    });

    $('body').on('click', '.btn-save-validation', function () {
        let action = $(this).closest('form').attr('action');
        let formSerialize = $(this).closest('form').serialize();
        saveValidation(formSerialize, action);
    });

    $('.validation-form').on('submit', function (e) {
        e.preventDefault();
        return false;
    });

    $('body').on('keypress', '.contenedores', function (tecla) {
        if (tecla.charCode < 48 || tecla.charCode > 57){
            return false;
        }
    });

    function loadWarehouses(city_select = '#city_id', warehoseSelect = '#warehouse_id') {
        $.ajax({
            url: "{{ route('adminTransporter.validation.get_warehouses_by_city_ajax') }}",
            data: {city_id: $(city_select).val()},
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                let warehose = $(warehoseSelect);
                warehose.empty();
                warehose.append('<option value selected>Seleccione</option>');
                $.each(response, function(index, value) {
                    warehose.append('<option value="' + index + '">' + value + '</option>');
                });
                $('.alert-success').find('.message').html('Bodegas listadas correctamente.').parent().slideToggle('slow').delay(3000).slideToggle('slow');
            }, error: function() {
                swal(
                    'Oops...',
                    'Ocurrió un error al guardar los datos.',
                    'error'
                );
            }
        });
    }

    function loadRoutes(warehouse_select = '#warehouse_id', routeSelect = '#route_id') {
        $.ajax({
            url: "{{ route('adminTransporter.validation.get_routes_by_warehouse_ajax') }}",
            data: {warehouse_id: $(warehouse_select).val()},
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                let route = $(routeSelect);
                route.empty();
                route.append('<option value selected>Seleccione</option>');
                $.each(response, function(index, value) {
                    route.append('<option value="' + index + '">' + value + '</option>');
                });
                $('.alert-success').find('.message').html('Rutas listadas correctamente.').parent().slideToggle('slow').delay(3000).slideToggle('slow');
            }, error: function() {
                swal(
                    'Oops...',
                    'Ocurrió un error al guardar los datos.',
                    'error'
                );
            }
        });
    }

    function loadOrders(route_select = '#route_id', orderSection = '#section_orders') {
        $.ajax({
            url: "{{ route('adminTransporter.validation.get_orders_by_routes_ajax') }}",
            data: {route_id: $(route_select).val()},
            type: 'GET',
            dataType: 'html',
            success: function(view) {
                let viewOrders = $(orderSection);
                viewOrders.empty();
                viewOrders.html(view);
                $('.alert-success').find('.message').html('Ordenes listadas correctamente.').parent().slideToggle('slow').delay(3000).slideToggle('slow');
                swal(
                    '¡Ten en cuenta!',
                    'Pueden haber ordenes para esta ruta en un estado diferente a Alistado, por favor validar.',
                    'info'
                );
            }, error: function() {
                swal(
                    'Oops...',
                    'Ocurrió un error al guardar los datos.',
                    'error'
                );
            }
        });
    }

    function saveValidation(formSerialize, action) {
        $.ajax({
            url: action,
            data: formSerialize,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    swal(
                        '¡Bien hecho!',
                        response.message,
                        'success'
                    );
                } else {
                    swal(
                        'Oops...',
                        response.message,
                        'error'
                    );
                }
            }, error: function (error) {
                swal(
                    'Oops...',
                    'Ocurrió un error al guardar los datos.',
                    'error'
                );
            }
        });
    }
});
</script>