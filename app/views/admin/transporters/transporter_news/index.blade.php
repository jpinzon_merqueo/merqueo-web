@if (!Request::ajax())

@extends('admin.layout')

@section('content')
<style>
    .img-loader{
        width: 50px;
        height: 50px;
    }

    .bg-warning{
        background: #fcf8e3;
        border: 1px solid #f5fcad;
        margin-bottom: 5px;
    }
    #modal_resolv_new .modal-body{
        overflow: hidden;
    }

</style>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top: 0px;">
        <div class="btn-group">
            <button data-toggle="modal" data-target="#modal-report" type="button" class="btn btn-primary export-balance pull-right">Generar reporte</button>
        </div>
        <div class="btn-group">
            @if($admin_permissions['permission1'])
            <button data-target="#modal-configuration-users" type="button" class="btn btn-primary config-users pull-right">Configuración Usuarios</button>
            @endif

        </div>
    </span>
</section>
<section class="content">
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="{{admin_asset_url()}}/css/timepicker/bootstrap-timepicker.css" rel="stylesheet">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Información</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form class="form" id="search-form" method="GET" autocomplete="off">
                                <div class="form-group col-md-3">
                                    <div class="col-md-3">
                                        {{ Form::label('city_id', 'Ciudad:') }}
                                    </div>
                                    <div class="col-md-9">
                                        {{ Form::fillSelectWithKeys('city_id', $cities, $city, array('class' => 'form-control', 'id' => 'city_id'), 'id', 'city' ) }}
                                    </div>
                                </div>
                                <div class="form-group col-md-5">
                                    <div class="col-md-3">
                                        {{ Form::label('plate', 'Buscar:') }}
                                    </div>
                                    <div class="col-md-9">
                                        {{ Form::text('plate', '', ['id' => 'plate', 'class' => 'form-control', 'placeholder' => 'Placa, # Orden']) }}
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    {{ Form::submit('Buscar', ['class' => 'btn btn-primary', 'id' => 'search']) }}
                                </div>
                                {{ Form::token() }}
                                {{ Form::hidden('_method', 'GET') }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Mis Novedades Activas</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-default btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive" id="asigned_news">
                                <div align="center" class="paging-loading img-loader center-block" id="img-loader"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Novedades sin asignar</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-default btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive" id="unasigned_news">
                                <div align="center" class="paging-loading img-loader center-block" id="img-loader_1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Novedades resueltas</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-default btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive" id="resolved_news">
                                <div align="center" class="paging-loading img-loader center-block" id="img-loader_2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#importing" class="fancybox unseen"></a>
    <div id="importing" class="unseen"></div>
</section>

<div class="modal fade" id="modal_asign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-save-assignment" data-dismiss="modal">Guardar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_resolv_new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Resolver Novedad</h4>
            </div>
            <div class="modal-body">
                <form id="frm_save_resolv">
                    <input id="id_new" name="id_new" value="">

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-save-resolv-new" >Guardar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_config_users" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-save-config-user" data-dismiss="modal">Guardar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- /.  modals messages-->
<div class="modal modal-info fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Alerta</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-outline pull-center" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal modal-info fade" id="modal-report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Descargar reporte</h4>
            </div>
            <div class="modal-body modal-report-body">
                <form id="frm-report">
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label>Fecha de inicio</label>
                            <input placeholder="DD/MM/YYYY" type="text" class="form-control" name="report_init_date" id="report_init_date">
                        </div>
                        <div class="form-group col-xs-6">
                            <label>Fecha de fin</label>
                            <input placeholder="DD/MM/YYYY" type="text" class="form-control" name="report_end_date" id="report_end_date">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-outline pull-center btn-download-report" data-dismiss="modal">Descargar reporte</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="{{admin_asset_url()}}/js/plugins/datetimepicker/moment.js"></script>
<script src="{{admin_asset_url()}}/js/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js"></script>
<script>

    var anim ;
    var transporter_news = {
        web_url_ajax : "{{ route('adminTransporterNews.index') }}",
        user_url_ajax : "{{ route('adminTransporterNews.getUsers')}}",
        assignment_url_ajax : "{{ route('adminTransporterNews.saveAssignment')}}",
        user_config_url_ajax : "{{ route('adminTransporterNews.getUsersConfig')}}",
        url_get_form_resolv : "{{ route('adminTransporterNews.getFormResolvNew')}}",
        url_save_form_resolv : "{{ route('adminTransporterNews.saveFormResolvNew')}}",
        url_report_ajax : "{{ route('adminTransporterNews.exportReportAjax')}}",
        time_out : null,
        anim : null,
        load_animation : function(element_id){
            anim = bodymovin.loadAnimation({
                wrapper: document.getElementById(element_id),
                animType: 'svg',
                loop: true,
                prerender: true,
                autoplay: true,
                path: "{{ web_url() }}/admin_asset/img/material_loader.json"
            });
        },
        load_tables : function(){
            var self = this;
            $.ajax({
                url : self.web_url_ajax,
                type: 'GET',
                data : $('#search-form').serialize(),
                dataType : 'json',
                cache : false,
                beforeSend : function () {
                    self.add_load_animation();
                },
                success: function(response){
                    if(response.status){
                        $('#unasigned_news').html(response.result.unasigned);
                        $('#asigned_news').html(response.result.asigned);
                        $('#resolved_news').html(response.result.resolved);
                        console.log(self.time_out)
                        if(self.time_out != null ){
                            clearTimeout(self.time_out);
                        }
                        self.time_out = setTimeout(function () {
                            self.load_tables()
                        }, 60000)
                    }
                },
                error : function(xhr){
                    transporter_news.show_modal_messages('Ocurrio un error al cargar las novedades.', 'error');
                }
            })
        },
        export_report : function(){
            var self = this;
            $.ajax({
                url : self.url_report_ajax,
                type: 'POST',
                data : $('#frm-report').serialize(),
                dataType : 'json',
                cache : false,
                beforeSend : function () {
                },
                success: function(response){
                    console.log(response)
                    if(response.status){
                        window.open(response.file_url,'_blank');
                        transporter_news.show_modal_messages(response.message, 'success');
                    }
                },
                error : function(xhr){
                    transporter_news.show_modal_messages('Ocurrio un error al cargar las novedades.', 'error');
                }
            })
        },
        search : function(){
            var self = this;
            $.ajax({
                url : self.web_url_ajax,
                type: 'GET',
                data : $('#search-form').serialize(),
                dataType : 'json',
                cache : false,
                beforeSend : function () {
                    self.add_load_animation();
                },
                success: function(response){
                    if(response.status){
                        $('#unasigned_news').html(response.result.unasigned);
                        $('#asigned_news').html(response.result.asigned);
                        $('#resolved_news').html(response.result.resolved);
                        console.log(self.time_out)
                        if(self.time_out != null ){
                            clearTimeout(self.time_out);
                        }
                        self.time_out = setTimeout(function () {
                            self.load_tables()
                        }, 60000)
                    }
                },
                error : function(xhr){
                    transporter_news.show_modal_messages('Ocurrio un error al cargar las novedades.', 'error');
                }
            })
        },
        add_load_animation : function () {
            var self = this;
            $('#asigned_news').html('<div align="center" class="paging-loading img-loader center-block" id="img-loader"></div>');
            $('#unasigned_news').html('<div align="center" class="paging-loading img-loader center-block" id="img-loader_1"></div>');
            $('#resolved_news').html('<div align="center" class="paging-loading img-loader center-block" id="img-loader_2"></div>');
            $.each($('.img-loader'), function(index, element){
                self.load_animation($(element).attr('id'));
            });
        },
        get_users : function(id_new){
            var self = this;
            $.ajax({
                url : self.user_url_ajax,
                type: 'POST',
                data: {id_new : id_new},
                dataType : 'json',
                cache : false,
                success: function(response){
                    if(response.status){
                        $('#modal_asign').find('.modal-body').html(response.result.users);
                        $('#modal_asign').modal('show');
                    }else{
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                },
                error:function (xhr) {
                    transporter_news.show_modal_messages('Ocurrio un error al cargar los usuarios.', 'error');
                }
            });
        },
        save_assignment : function () {
            var self = this;
            $.ajax({
                url : self.assignment_url_ajax,
                type: 'POST',
                data: $('#asign_users_form').serialize(),
                dataType : 'json',
                cache : false,
                success: function(response){
                    if(response.status){
                        transporter_news.show_modal_messages(response.message, 'success');
                        self.load_tables();
                    }else{
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                },
                error : function (xhr) {
                    transporter_news.show_modal_messages('Ocurrio un error al asignar la novedad.', 'error');
                }
            });
        },
        save_auto_assignment : function (data) {
            var self = this;
            $.ajax({
                url : self.assignment_url_ajax,
                type: 'POST',
                data: data,
                dataType : 'json',
                cache : false,
                success: function(response){
                    if(response.status){
                        transporter_news.show_modal_messages(response.message, 'success');
                        self.load_tables();
                    }else{
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                },
                error : function (xhr) {
                    transporter_news.show_modal_messages('Ocurrio un error al asignar la novedad.', 'error');
                }
            });
        },
        config_users : function () {
            var self = this;
            $.ajax({
                url : self.user_config_url_ajax,
                type: 'POST',
                data : { type : 'get_users'},
                dataType : 'json',
                cache : false,
                success: function(response){
                    if(response.status){
                        $('#modal_config_users').find('.modal-body').html(response.result.users);
                        $('#modal_config_users').modal('show');
                        //$('.table-config-users').DataTable();
                    }else{
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                },
                error : function () {
                    transporter_news.show_modal_messages('Ocurrio un error al cargar los usuarios.', 'error');
                }
            });
        },
        save_config_user : function () {
            var self = this;
            $.ajax({
                url: self.user_config_url_ajax,
                type: 'POST',
                data: $('#config_asign_users_form').serialize(),
                dataType: 'json',
                cache: false,
                success: function (response) {
                    if (response.status) {
                        transporter_news.show_modal_messages(response.message, 'success');
                    } else {
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                },
                error : function (xhr) {
                    transporter_news.show_modal_messages('Ocurrio un error al guardar la configuración.', 'error');
                }
            });
        },
        get_form_resolv : function (id_new) {
            var self = this;
            $.ajax({
                url: self.url_get_form_resolv,
                type: 'POST',
                data: { id_new : id_new },
                dataType: 'json',
                cache: false,
                success: function (response) {
                    if (response.status) {
                        $('#modal_resolv_new').find('.modal-body').html(response.result);
                        $('#modal_resolv_new').modal('show');
                    } else {
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                },
                error : function (xhr) {
                    transporter_news.show_modal_messages('Ocurrio un error al traer la información de la novedad.', 'error');
                }
            });
        },
        save_form_resolv : function (id_new) {
            var self = this;
            $('.btn-save-resolv-new').prop('disable', true);
            $.ajax({
                url: self.url_save_form_resolv,
                type: 'POST',
                data: $('#frm_save_resolv_new').serialize(),
                dataType: 'json',
                cache: false,
                success: function (response) {
                    if (response.status) {
                        transporter_news.show_modal_messages(response.message, 'success');
                        $('#modal_resolv_new').modal('hide');
                        self.load_tables();
                    } else {
                        transporter_news.show_modal_messages(response.message, 'error');
                    }
                    $('.btn-save-resolv-new').prop('disable', false);
                },
                error : function (xhr) {
                    $('.btn-save-resolv-new').prop('disable', false);
                    transporter_news.show_modal_messages('Ocurrio un error al actualizar la novedad.', 'error');
                }
            });
        },
        show_modal_messages : function (content, type) {
            $('#modal-info').removeClass('modal-danger');
            $('#modal-info').removeClass('modal-info');
            $('#modal-info').removeClass('modal-success');
            $('#modal-info').removeClass('modal-warning');
            switch (type){
                case 'info':
                    $('#modal-info').addClass('modal-info');
                    $('#modal-info').find('.modal-body').html(content);
                    break;
                case 'error':
                    $('#modal-info').addClass('modal-danger');
                    break;
                case 'success':
                    $('#modal-info').addClass('modal-success');
                    break;
                case 'warning':
                    $('#modal-info').addClass('modal-warning');
                    break;
            }
            $('#modal-info').find('.modal-body').html(content);
            $('#modal-info').modal('show');
        }
    }

    $(document).ready(function(){
        $.ajaxSetup({ cache: false });
        $.each($('.img-loader'), function(index, element){
            transporter_news.load_animation($(element).attr('id'));
        });
        transporter_news.load_tables();
        $('body').on('click','.btn-asign_modal', function(){
            var id_new = $(this).data('id');
            transporter_news.get_users(id_new);
        });

        $('body').on('click', '.config-users', function () {
            transporter_news.config_users();
        });
        $('body').on('click', '.btn-save-assignment', function () {
            if($('#asign_users_form').valid() ){
                transporter_news.save_assignment();
            }else{
                transporter_news.show_modal_messages("Debe seleccionar al menos un usuario.", 'error');
            }
        });
        $('body').on('click', '.btn-asign', function () {
            var data = $(this).data();
            transporter_news.save_auto_assignment(data);
        });

        $('body').on('click', '.btn-save-config-user', function () {
            if($('#config_asign_users_form').valid() ){
                transporter_news.save_config_user();
            }else{
                transporter_news.show_modal_messages("Debe seleccionar al menos un usuario.", 'error');
            }
        });

        $('body').on('click', '#search', function (event) {
            event.preventDefault();
            transporter_news.search();
        });

        $('body').on('click', '.btn-resolv_modal', function () {
            var id_new = $(this).data('id');
            transporter_news.get_form_resolv(id_new);
        });

        $('body').on('click', '.btn-save-resolv-new', function () {

            if($('#frm_save_resolv_new').valid()){
                transporter_news.save_form_resolv();
            }
        });

        $('body').on('click', '.btn-download-report', function () {
            if($('#frm-report').valid()){
                transporter_news.export_report();
            }
        });
        $('#report_init_date').datetimepicker({
            format: 'DD/MM/YYYY',
            allowInputToggle : true
        });
        $('#report_end_date').datetimepicker({
            format: 'DD/MM/YYYY',
            allowInputToggle : true
        });
    });
</script>
@endsection
@else

    @section('content')
        @if(isset($news) )
        <table class="table table-bordered table-striped" >
            <thead>
            <tr>
                <th>#ID</th>
                <th>Tipo Novedad</th>
                <th>Transportador</th>
                <th>Conductor</th>
                <th>Vehiculo</th>
                <th># Pedido</th>
                <th>
                    @if($status != 'resolved')
                        Tiempo de retraso
                    @else
                        Fecha de respuesta
                    @endif
                </th>
                @if($status != 'resolved')
                <th>Acciones</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @if (count($news))
                @foreach($news as $new)
                    <tr>
                        <td>{{ $new->id }}</td>
                        <td>{{ $new->type }}</td>
                        <td>{{ $new->vehicle->transporter->fullname }}</td>
                        <td>{{ $new->order->driver->first_name }} {{ $new->order->driver->last_name }}</td>
                        <td>{{ $new->vehicle->plate }}</td>
                        <td>{{ $new->order_id }}</td>
                        <td>
                            @if($new->status == 'Sin Asignar' || $new->status == 'Pendiente')
                                {{ get_time('left', $new->created_at) }}
                            @else
                                {{ format_date('normal_long_with_time',$new->resolve_date) }}
                            @endif
                        </td>
                        @if($status != 'resolved')
                        <td class="text-center">
                            @if($new->status == 'Sin Asignar' && $admin_permissions['permission1'])
                                <a class="btn btn-primary btn-asign_modal" data-id="{{$new->id}}"  data-target="#modal_asign"> Asignar </a>
                                <a class="btn btn-primary btn-asign" data-id_new="{{$new->id}}" data-admin_id="{{Session::get('admin_id')}}"  >Tomar novedad</a>
                            @endif
                            @if($new->status == 'Sin Asignar' && !$admin_permissions['permission1'])
                                <a class="btn btn-primary btn-asign" data-id_new="{{$new->id}}" data-admin_id="{{Session::get('admin_id')}}"  >Tomar novedad</a>
                            @endif
                            @if($new->status == 'Pendiente')
                                <a class="btn btn-success btn-resolv_modal" data-id="{{$new->id}}"  data-target="#modal_resolv_new"> Resolver </a>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr><td colspan="8" align="center">No hay novedades disponibles.</td></tr>
            @endif
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="dataTables_info text-center" id="shelves-table_info">Mostrando {{ count($news) }} registros</div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
        @endif
    @endsection

    @section('list_user')
        @if(isset($users))
            <form id="asign_users_form">
                <input type="hidden" name="id_new" value="{{$id_new}}">
                <h3>Usuarios</h3>
                <table class="table table-bordered table-striped table-users">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->fullname}}</td>
                            <td>{{$user->username}}</td>
                            <td><input type="radio" name="admin_id" value="{{$user->id}}" @if($user->id ==  Session::get('admin_id')) checked="checked"@endif required></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </form>
        @endif
    @endsection
    @section('resolv-form')
        @if(isset($transporter_new))

            <form id="frm_save_resolv_new">
                <input value="{{$transporter_new->id}}" type="hidden" name="id_new">
                <div class="col-md-12"><label>Tipo:</label> {{$transporter_new->type}}</div>
                @if($transporter_new->type != 'Productos')
                <div class="col-md-12"><label>Razón:</label> {{$transporter_new->reason}}</div>
                @endif
                <div class="col-md-12"><label>Comentarios:</label> {{$transporter_new->reason_comments}}</div>
                <div class="col-md-12" data-toggle="tooltip" data-placement="top" title="Abrir pedido">
                    <label># Pedido:</label> <a href="{{route('adminOrderStorage.details', ['id'=>$transporter_new->order_id])}}" target="_blank" >{{$transporter_new->order_id}}</a>
                </div>
                @if($transporter_new->type == 'Productos')
                    @foreach($transporter_new->transporterNewDetail as $product )
                        <div class="col-md-12 bg-warning">
                            <label>Producto:</label> {{$product->orderProducts()->product_name}} {{$product->orderProducts()->product_quantity}} {{$product->orderProducts()->product_unit}}<br>
                            <label>Cantidad:</label> {{$product->quantity_returned}}<br>
                            <label>Razón:</label> {{$product->reason}}
                        </div>
                    @endforeach

                @endif
                @if(!empty($transporter_new->url_file))
                    <div class="col-md-12">
                        <label>Imagén dirección:</label>
                        <img src="{{$transporter_new->url_file}}" class="image-responsive" width="250">
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Transportador:</label> {{$transporter_new->vehicle->transporter->fullname}} - <label>Conductor:</label> {{ $transporter_new->order->driver->first_name }} {{ $transporter_new->order->driver->last_name }} - <label>Vehiculo:</label> {{ $transporter_new->vehicle->plate }}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Mensaje de respuesta:</label>
                        <textarea class="form-control" name="resolution_message" id="resolution_message" rows="3" required></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>URL adicional de respuesta:</label>
                        <input type="url" class="form-control" name="resolution_url">
                    </div>
                </div>
            </form>
        @endif
    @endsection

@endif

