@if (Request::ajax())
    @section('list_user')
        @if(isset($users))
            <form id="config_asign_users_form">
                <input type="hidden"  value="save_config" name="type">
                <input type="hidden"  value="" name="user_selected" id="user_selected">
                <h3>Usuarios</h3>
                <div class="table-responsive table-users" style="overflow: scroll; height: 300px;">
                    <table class="table table-bordered table-striped table-config-users">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->fullname}}</td>
                                <td>{{$user->username}}</td>
                                <td><input type="checkbox" class="admin_id" name="admin_id[]" value="{{$user->id}}" @if(in_array($user->id, $autorized_users))checked="checked"@endif required></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        @endif
    @endsection
@endif
