@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminTransporter.add_vehicle') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Vehículo</button>
            </a>
            <button data-toggle="modal" data-target="#modal-report" type="button" class="btn btn-primary">Generar reporte</button>
        </span>

    </section>
    <section class="content">
    @if(Session::has('message') )
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @endif
    @endif

    <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="vehicle-table">
                                    <table width="100%">
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td width="30%">
                                            <select id="city_id" name="city_id" class="form-control">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif>{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right">
                                            <label>Transportador:</label>
                                        </td>
                                        <td width="30%">
                                            <select id="transporter_id" name="transporter_id" class="form-control">
                                                <option value="">-Selecciona-</option>
                                                @foreach ($transporters as $transporter)
                                                    <option value="{{ $transporter->id }}">{{ $transporter->fullname }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right">
                                            <label>Buscar:</label>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Placa" id="input-search" class="form-control">
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>

                                        </td>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#importing" class="fancybox unseen"></a>
        <div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
        <div class="modal fade" id="modal-cost-report">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Formulario para la generación de reporte de costos</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="GET" id='main-form' action="" autocomplete="off">
                            <div class="form-group">
                                <label for="from" class="control-label">Desde:</label>
                                <input id="from" type="text" class="form-control" name="from" >
                            </div>
                            <div class="form-group">
                                <label for="to" class="control-label">Hasta:</label>
                                <input id="to" type="text" class="form-control" name="to" >
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="generate-report" type="button" class="btn btn-primary">Generar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-report">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Formulario para la generación de reporte de costos</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="GET" id='main-form-report' action="{{ route('adminTransporter.report_extend') }}" autocomplete="off">
                        <div class="form-group">
                            {{ Form::label('created_at', 'Rango de fechas') }}
                            {{ Form::text('created_at', '', ['class' => 'form-control', 'id' => 'created_at', 'placeholder' => 'DD-MM-YYYY,DD-MM-YYYY']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('city_report', 'Ciudad') }}
                            {{ Form::fillSelectWithKeys('city_report', $cities, Session::get('admin_city_id'), array('class' => 'form-control', 'id' => 'city_report'), 'id', 'city' ) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('transporter_report', 'Transportador') }}
                            {{ Form::fillSelectWithKeys('transporter_report', $transporters, '', array('class' => 'form-control', 'id' => 'transporter_report'), 'id', 'fullname', TRUE ) }}
                        </div>
                        {{ Form::token() }}
                        {{ Form::hidden('_method', 'GET') }}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button id="send-form" type="button" class="btn btn-primary">Generar</button>
                </div>
            </div>
        </div>
    </div>
    </section>

    <script>
        var web_url_ajax = "{{ route('adminTransporter.vehicles') }}";
        var web_url_ajax_transporters = "{{ route('adminTransporter.ajax_transporters_city') }}";

        $(document).ready(function() {
            paging(1, '');

            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });

            $('#city_id').change(function(e) {
                var data = {'city_id': $('#city_id').val()};
                $.ajax({
                    url: web_url_ajax_transporters,
                    data: data,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        var transporter_id = $('#transporter_id');
                        transporter_id.empty();
                        transporter_id.append('<option value="">-Selecciona-</option>');
                        $.each(response.result.transporters, function(key, value){
                            transporter_id.append('<option value="' + value.id + '">'+ value.fullname + '</option>');
                        });
                    }, error: function() {
                        console.log('Ocurrió un error al obtener los datos.');
                    }
                });
            });

            $('body').on('click', '.report', function(event) {
                event.preventDefault();
                var href = $(this).attr('href');
                $('#main-form').attr('action', href);
                $('#modal-cost-report').modal();
            });

            $('#from, #to').datetimepicker({
                format: 'DD-MM-YYYY'
            });

            $('body').on('click', '#generate-report', function(event) {
                event.preventDefault();
                $('#main-form').submit();
            });

            $('.fancybox').fancybox({
                autoSize    : true,
                closeClick  : false,
                closeBtn    : false ,
                openEffect  : 'none',
                closeEffect : 'none',
                helpers   : {
                   overlay : {closeClick: false}
                }
            });
            $('#created_at').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ',' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#created_at').daterangepicker({
                autoApply: true,
                autoUpdateInput: false,
                format: 'DD-MM-YYYY'
            });
            $('#main-form-report').submit(function () {
                if($(this).valid()) {
                   $('.fancybox').trigger('click');
                   $('#modal-report').modal('hide')
                }
            });
            $("#main-form-report").validate({
              rules: {
                created_at: {
                    required: true
                },
                city_report: {
                    required: true
                },
                transporter_report: {
                    required: true
                }
              }
            });
            $('#send-form').on('click', function(event) {
                event.preventDefault();
                $('#main-form-report').submit();
            });
            $('#city_report').change(function(e) {
                var data = {'city_id': $('#city_report').val()};
                $.ajax({
                    url: web_url_ajax_transporters,
                    data: data,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        var transporter_id = $('#transporter_report');
                        transporter_id.empty();
                        transporter_id.append('<option value="">Selecciona</option>');
                        $.each(response.result.transporters, function(key, value){
                            transporter_id.append('<option value="' + value.id + '">'+ value.fullname + '</option>');
                        });
                    }, error: function() {
                        console.log('Ocurrió un error al obtener los datos.');
                    }
                });
            });

            @if (Session::has('file_url'))
            setTimeout(function(){
                window.location = '{{ Session::get('file_url') }}';
            }, 1000);
            @endif

        });
    </script>

@else

    @section('content')
    <br>
    <table id="vehicle-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Ciudad</th>
            <th>Placa</th>
            <th>Tipo de vehículo</th>
            <th>Modelo</th>
            <th>Capacidad toneladas</th>
            <th>Capacidad volumen</th>
            <th>Transportador</th>
            <th>Estado</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @if(count( $vehicles ))
            @foreach($vehicles as $vehicle)
            <tr>
                <td>{{ $vehicle->city_name }}</td>
                <td>{{ $vehicle->plate }}</td>
                <td>{{ $vehicle->class_type }}</td>
                <td>{{ $vehicle->model }}</td>
                <td>{{ $vehicle->capacity }}</td>
                <td>{{ $vehicle->total_volume }}</td>
                <td>{{ $vehicle->fullname }}</td>
                <td>@if ($vehicle->status)
                        <span class="badge bg-green">Activo</span>
                    @else
                        <span class="badge bg-red">Inactivo</span>
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminTransporter.edit_vehicle', ['id' => $vehicle->id]) }}">Editar</a></li>
                            <li class="divider"></li>
                            <li><a data-vehicle="{{ $vehicle->id }}" class="report" href="{{ route('adminTransporter.cost_report', ['id' => $vehicle->id]) }}">Reporte de costos</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminTransporter.delete_vehicle', ['id' => $vehicle->id]) }}"  onclick="return confirm('¿Estas seguro que deseas  cambiar el estado  del vehículo?')">Cambiar Estado</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach

        @else

            <tr>
                <td align="center">No hay vehículos.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endif

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info"></div>
        </div>
    </div>

@endif
    <script>
        $(function() {
            // DataTable
            var table = $('#vehicle-table').DataTable( {
                "bFilter": false
            } );

        });
    </script>
@stop
