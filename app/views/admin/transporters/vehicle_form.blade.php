@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')

        <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/select2/select2.css"/>
        <script src="{{ web_url() }}/admin_asset/js/select2/select2.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <script>
        var web_url_ajax = "{{ route('adminTransporter.add_vehicle') }}";
    </script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alert!</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title }}</h3>
                    </div>
                    <form role="form" method="post" id='main-form' action="{{ route('adminTransporter.save_vehicle') }}" >
                        <input type="hidden" name="id" value="{{ $vehicles ? $vehicles->id : "" }}">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group item_sel">
                                        <label>Transportador</label>
                                        <select id="transporter_id" name="transporter_id" required class="form-control col-xs-12" style="margin-bottom: 15px;">
                                            <option value="">-Selecciona-</option>
                                            @foreach ($transporters as $transport)
                                                <option value="{{ $transport->id }}" @if( $transport->id == $vehicles->transporter_id ) selected="selected" @endif >{{ $transport->fullname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Placa</label>
                                        <input type="text" class="form-control" id="plate" name="plate" placeholder="Placa" required value="{{ $vehicles ? $vehicles->plate : "" }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Modelo</label>
                                        <input type="text" class="form-control" id="model" name="model" placeholder="Modelo" required value="{{ $vehicles ? $vehicles->model : "" }}">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Fecha de matrícula</label>
                                        <input type="text" class="form-control" id="enrollment_date" name="enrollment_date" placeholder="Fecha de matrícula" required value="{{ $vehicles ? format_date('normal',$vehicles->enrollment_date) : "" }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="status">Estado</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="1" @if ( isset($vehicles) && $vehicles->status === 1 ) selected=""@endif>Activo</option>
                                            <option value="0" @if ( isset($vehicles) && $vehicles->status === 0 ) selected=""@endif>Inactivo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Fecha de SOAT</label>
                                        <input type="text" class="form-control" id="due_date_soat" name="due_date_soat" placeholder="Fecha de SOAT" required value="{{ $vehicles ? format_date('normal',$vehicles->due_date_soat) : "" }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Fecha de revisión técnico mecánica</label>
                                        <input type="text" class="form-control" id="due_date_rtm" name="due_date_rtm" placeholder="Fecha de revisión técnico mecánica" required value="{{ $vehicles ? format_date('normal',$vehicles->due_date_rtm) : "" }}">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Observación carroceria</label>
                                        <textarea class="form-control" id="" name="body_inspection">@if($vehicles->body_inspection){{ $vehicles->body_inspection}}@endif</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group sel_cond">
                                        <label>Conductores</label>
                                        <select id="drivers_id"   multiple="multiple" name="drivers_id[]"  required class="form-control col-xs-12" style="margin-bottom: 15px;">
                                            <option value="">-Selecciona-</option>
                                            @foreach ($drivers as $driver)
                                                @if($driver->profile == 'Conductor')
                                                    <option value="{{ $driver->id }}" @if( in_array($driver->id,$vehicles->drivers_ids ) ) selected="selected" @endif >{{ $driver->first_name }} {{ $driver->last_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Clase de vehículo</label>
                                        <select name="class_type" id="class_type" class="form-control" required>
                                            <option value="">-Selecciona-</option>
                                            <option value="N300" @if($vehicles->class_type == 'N300') selected="selected" @endif>N300</option>
                                            <option value="LUV" @if($vehicles->class_type == 'LUV') selected="selected" @endif>LUV</option>
                                            <option value="NHR" @if($vehicles->class_type == 'NHR') selected="selected" @endif>NHR</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Capacidad toneladas</label>
                                        <input type="text" class="form-control" id="capacity" name="capacity" placeholder="Capacidad toneladas" required value="{{ $vehicles ? $vehicles->capacity: "" }}">
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Capacidad volumen</label>
                                        <select name="total_volume" id="total_volume" required class="form-control">
                                            <option value="">-Selecciona-</option>
                                            <option value="0-25" @if($vehicles->total_volume == '0-25') selected="selected" @endif>0-25</option>
                                            <option value="25-40" @if($vehicles->total_volume == '25-40') selected="selected" @endif>25-40</option>
                                            <option value="Más de 40" @if($vehicles->total_volume == 'Más de 40') selected="selected" @endif>Más de 40</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Capacidad en contenedores</label>
                                        <input type="number" class="form-control" id="capacity_containers" name="capacity_containers" placeholder="Capacidad en contenedores" required value="{{ $vehicles ? $vehicles->capacity_containers: "" }}">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>¿Permitir entrega anticipada?</label>
                                        <select id="allow_early_delivery" name="allow_early_delivery" class="form-control col-xs-12" style="margin-bottom: 15px;" required>
                                            <option value="">-Selecciona-</option>
                                            <option value="1" @if($vehicles->allow_early_delivery == 1) selected="selected" @endif>Si</option>
                                            <option value="0" @if($vehicles->allow_early_delivery == 0) selected="selected" @endif>No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" class="form-control" name="password" placeholder="Ingrese la contraseña" value="" requierd>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>¿Validar secuencia al tomar pedido?</label>
                                        <select id="validate_sequence" name="validate_sequence" class="form-control col-xs-12" style="margin-bottom: 15px;" required>
                                            <option value="">-Selecciona-</option>
                                            <option value="1" @if($vehicles->validate_sequence == 1) selected="selected" @endif>Si</option>
                                            <option value="0" @if($vehicles->validate_sequence == 0) selected="selected" @endif>No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>¿Tiene branding?</label>
                                        <select id="has_branding" name="has_branding" class="form-control col-xs-12" style="margin-bottom: 15px;" required>
                                            <option value="">-Selecciona-</option>
                                            <option value="1" @if($vehicles->has_branding == 1) selected="selected" @endif>Si</option>
                                            <option value="0" @if($vehicles->has_branding == 0) selected="selected" @endif>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>¿Revisión diseño branding?</label>
                                        <select id="has_branding_check_design" name="has_branding_check_design" class="form-control col-xs-12" style="margin-bottom: 15px;" required>
                                            <option value="">-Selecciona-</option>
                                            <option value="1" @if($vehicles->has_branding_check_design == 1) selected="selected" @endif>Si</option>
                                            <option value="0" @if($vehicles->has_branding_check_design == 0) selected="selected" @endif>No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Fecha del branding</label>
                                        <input type="text" class="form-control" id="branding_date" name="branding_date" placeholder="Fecha del branding" value="{{ $vehicles ? format_date('normal',$vehicles->branding_date) : "" }}">
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group item_sel">
                                        <label>Tipo de pago</label>
                                        <select id="payment_type" name="payment_type" required
                                                class="form-control col-xs-12" style="margin-bottom: 15px;">
                                            <option value="">-Selecciona-</option>
                                            <option value="0" @if( 0 ==  $vehicles->payment_type ) selected="selected" @endif >Ruta</option>
                                            <option value="1" @if( 1 ==  $vehicles->payment_type ) selected="selected" @endif >Pedido</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                @if ($admin_permissions['permission1'])
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="old_version">Permite trabajar con la version anterior</label>
                                            <select name="old_version" id="old_version" class="form-control">
                                                <option value="1" @if ( isset($vehicles) && $vehicles->old_version === 1 ) selected=""@endif>Activo</option>
                                                <option value="0" @if ( isset($vehicles) && $vehicles->old_version === 0 ) selected=""@endif>Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif

                            </div>

                            <div class="row">
                                <div class="col-xs-12 ">
                                    <legend>Costos</legend>
                                    <div class="row cost-ruta">
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label" for="full_route_cost">Costo ruta completa</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <input class="form-control input-lg" id="full_route_cost" min="1" required min="1" type="number" name="full_route_cost" value="{{ $vehicles ? $vehicles->full_route_cost : 0 }}">
                                                    <div class="input-group-addon">.00</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label" for="half_route_cost">Costo ruta media</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <input class="form-control input-lg" id="half_route_cost" min="1" required type="number" name="half_route_cost" value="{{ $vehicles ? $vehicles->half_route_cost : 0 }}">
                                                    <div class="input-group-addon">.00</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label" for="partial_route_cost">Costo ruta parcial</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <input class="form-control input-lg" id="partial_route_cost" min="1" required type="number" name="partial_route_cost" value="{{ $vehicles ? $vehicles->partial_route_cost : 0 }}">
                                                    <div class="input-group-addon">.00</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 cost-pedido">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label" for="full_route_cost">Pedido Base</label>
                                                <div class="input-group" style="width: 100%">
                                                    <input class="form-control input-lg" id="base_order" required min="1" type="number" name="base_order" value="{{ $vehicles ? $vehicles->base_order : 0 }}">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label" for="half_route_cost">Costo base</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <input class="form-control input-lg" id="base_cost" required min="1" type="number" name="base_cost" value="{{ $vehicles ? $vehicles->base_cost : 0 }}">
                                                    <div class="input-group-addon">.00</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label" for="partial_route_cost">Costo variable</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <input class="form-control input-lg" id="variable_cost" min="1" required type="number" name="variable_cost" value="{{ $vehicles ? $vehicles->variable_cost : 0 }}">
                                                    <div class="input-group-addon">.00</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary">Guardar </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    $("#main-form").validate({
      rules: {
        name: "required",
        sort_order: "required",
        transporter_id: "required",
        has_branding: "required",
        has_branding_check_design: "required",
        allow_early_delivery: "required",
        full_route_cost: {
            number: true
        },
        half_route_cost: {
            number: true
        },
        partial_route_cost: {
            number: true
        }
      }
    });

    $(document).ready(function(){

        $('#payment_type').on('change', function () {
            var i = $('#payment_type').val();
            $('.cost-ruta').hide();
            $('.cost-pedido').hide();
            if (i == 0) {
                $('.cost-ruta').show();
                $('.cost-pedido').hide();
            }
            if (i == 1) {
                $('.cost-ruta').hide();
                $('.cost-pedido').show();
            }
        });
        $('#payment_type').change();

        $('#drivers_id').select2();

        $('#enrollment_date').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#branding_date').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#due_date_soat').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#due_date_rtm').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        var pathname = window.location.pathname;
        var objpath = pathname.split("/");
        $('.item_sel #transporter_id').on('change', function() {
            var data = {
                trasnport_id: $(this).val()
            };

            $('.sel_cond #drivers_id').html($('<option>', {
                value: '',
                text : '-Selecciona-'
            }));

            $.ajax({
                url: "{{ route('adminTransporter.transporter')}}",
                data: data,
                type: 'POST',
                dataType: 'html',
                success: function(response) {
                    $('.sel_cond').show();
                    var data = JSON.parse(response);
                    $.each(data, function( index, value ) {
                        if(index == 'cond'){
                            $.each(value, function( ind, va ) {
                                $('.sel_cond #drivers_id').append($('<option>', {
                                    value: va.id,
                                    text : va.name
                                }));
                            });
                        }

                    });
                    $('#drivera_id').select2();
                    $('.paging').html(response);
                }, error: function() {
                    console.log('Ocurrió un error al obtener los datos.');
                }
            });

        });

    });

    </script>

@else

    @section('content')
        <table id="transporter-table" class="table table-bordered table-striped">

            @if(count( $drivers ))
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Teléfono</th>
                    <th>Celular</th>
                    <th>Tipo de conductor</th>
                    <th>Categoria Licencia</th>
                    <th>Numero de licencia</th>
                    <th>Fecha de expiración</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($drivers as $driver)
                <tr>
                    <td>{{ $driver->first_name }}</td>
                    <td>{{ $driver->last_name }}</td>
                    <td>{{ $driver->phone }}</td>
                    <td>{{ $driver->cellphone }}</td>
                    <td>{{ $driver->profile }}</td>
                    <td>{{ $driver->category_license }}</td>
                    <td>{{ $driver->document_number }}</td>
                    <td>{{ date("d M Y",strtotime($driver->due_date)) }}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-success asig">
                                <span class="sr-only-asig data-id="{{ $driver->id }}">Asignar</span>
                            </button>
                            <button type="button" class="btn btn-danger rem" style="display:none;">
                                <span class="sr-only-asig data-id="{{ $driver->id }}">Remover</span>
                            </button>
                        </div>
                    </td>
                </tr>
            </tbody>
                @endforeach
            @else
                <thead>
                <tr>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td align="center">Conductores no encontrados para Transportador seleccionado</td>
                </tr>
                </tbody>
            @endif
        </table>

        <div class="row">
            <div class="col-xs-3">
                <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($vehicles) }} registros</div>
            </div>
        </div>

@endif
        <script>
            $(function() {
                // DataTable
                table = $('#transporter-table').DataTable( {
                    "bFilter": false
                } );

            });
        </script>
@stop

