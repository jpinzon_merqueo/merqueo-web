@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminAssistant.addAssistant') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Auxiliar</button>
            </a>
        </span>

    </section>
    <section class="content">
    @if(Session::has('message') )
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @endif
    @endif

    <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="driver-table">

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">

                                        <label>Ciudad:</label>
                                        <select id="city_id" name="city_id" class="form-control">
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif>{{ $city->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">

                                        <label>Transportador:</label>
                                        <select id="transporter_id" name="transporter_id" class="form-control">
                                            <option value="">-Selecciona-</option>
                                            @foreach($transporters as $transporter)
                                                <option value="{{ $transporter->id }}">{{ $transporter->fullname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">

                                        <label>Conductor:</label>
                                        <select id="driver_id" name="driver_id" class="form-control">
                                            <option value="">-Selecciona-</option>
                                            @foreach($drivers as $driver)
                                                <option value="{{ $driver->id }}">{{ $driver->last_name }} {{ $driver->first_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">

                                        <label>Buscar:</label>
                                        <input type="text" placeholder="Nombre auxiliar" id="input-search" class="form-control">

                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">

                                        <br>
                                        <button type="button" id="btn-search-assistants" class="btn btn-primary">Buscar</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var web_url_ajax = "{{ route('adminAssistant.assistants') }}";
        var web_url_ajax_transporters = "{{ route('adminTransporter.ajax_transporters_city') }}";
        var web_url_ajax_drivers = "{{ route('adminTransporter.ajax-drivers-transporter') }}";
        var table = '';

        $(document).ready(function() {
            paging(1, '');

            $('#btn-search-assistants').on('click', function(e) {
                $.ajax({
                    url: web_url_ajax,
                    data: {
                        city_id: $('#city_id').val(),
                        transporter_id: $('#transporter_id').val(),
                        driver_id: $('#driver_id').val(),
                        s: $('#input-search').val(),
                        p: 1
                    },
                    type: 'GET',
                    dataType: 'html',
                    success: function(response) {
                        $('.paging').html(response);
                    }, error: function() {
                        $('.paging-loading').hide();
                        $('.paging').html('Ocurrió un error al obtener los datos.');
                    }
                });
                e.preventDefault();
            });


            $('#city_id').change(function(e) {
                var data = {'city_id': $('#city_id').val()};
                $.ajax({
                    url: web_url_ajax_transporters,
                    data: data,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        var transporter_id = $('#transporter_id');
                        transporter_id.empty();
                        transporter_id.append('<option value="">-Selecciona-</option>');
                        $.each(response.result.transporters, function(key, value){
                            transporter_id.append('<option value="' + value.id + '">'+ value.fullname + '</option>');
                        });
                    }, error: function() {
                        alert('Ocurrió un error al obtener los datos.');
                    }
                });
            });
            $('#transporter_id').change(function(e) {
                var data = {'transporter_id': $('#transporter_id').val()};
                $.ajax({
                    url: web_url_ajax_drivers,
                    data: data,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        var driver_id = $('#driver_id');
                        driver_id.empty();
                        driver_id.append('<option value="">-Selecciona-</option>');
                        $.each(response.result.drivers, function(key, value){
                            driver_id.append('<option value="' + value.id + '">'+ value.last_name + ' '+ value.first_name + '</option>');
                        });
                    }, error: function() {
                        alert('Ocurrió un error al obtener los datos.');
                    }
                });
            });
        });
    </script>

@else

    @section('content')
    <br>
    <table id="driver-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Ciudad</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Teléfono</th>
            <th>Celular</th>
            <th>Documento</th>
            <th>Conductor</th>
            <th>Estado</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @if(count( $assistants )>0)

            @foreach($assistants as $assistant)
            <tr>
                <td>{{ $assistant->driver->transporter->city->city }}</td>
                <td>{{ $assistant->first_name }}</td>
                <td>{{ $assistant->last_name }}</td>
                <td>{{ $assistant->phone }}</td>
                <td>{{ $assistant->cellphone }}</td>
                <td>{{ $assistant->document_number }}</td>
                <td>{{ $assistant->driver->last_name }} {{ $assistant->driver->first_name }}</td>

                <td>@if ($assistant->status)
                        <span class="badge bg-green">Activo</span>
                    @else
                        <span class="badge bg-red">Inactivo</span>
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminAssistant.edit_assistant', ['id' => $assistant->id]) }}">Editar</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminAssistant.delete_assistant', ['id' => $assistant->id]) }}"  onclick="return confirm('¿Estas seguro que deseas cambiar el estado del auxiliar?')">Cambiar Estado</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr>
                <td  align="center">No hay auxiliares.</td>
                <td  align="center"></td>
                <td  align="center"></td>
                <td  align="center"></td>
                <td  align="center"></td>
                <td  align="center"></td>
                <td  align="center"></td>
                <td  align="center"></td>
                <td  align="center"></td>
            </tr>

        @endif

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info"></div>
        </div>
    </div>

@endif

    <script>
        $(function() {
            // DataTable
            table = $('#driver-table').DataTable( {
                "bFilter": false
            } );

        });
    </script>
@stop