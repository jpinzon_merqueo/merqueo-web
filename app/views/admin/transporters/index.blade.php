@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminTransporter.add') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Transportador</button>
            </a>
        </span>

    </section>
    <section class="content">
    @if(Session::has('message') )
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @endif
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="transporter-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td width="33%">
                                                <select id="city_id" name="city_id" class="form-control">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td align="right">
                                                <label>Buscar:</label>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="Nombre transportador" id="input-search" class="form-control">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var web_url_ajax = "{{ route('adminTransporter.index') }}";
        $(document).ready(function() {
            paging(1, '');

            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });
        });
    </script>
@else

    @section('content')
    <br />
    <table id="transporter-table" class="table table-bordered table-striped">

        @if(count($transporters))
            <thead>
            <tr>
                <th>Ciudad</th>
                <th>Nombre completo</th>
                <th>Numero de documento</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Celular</th>
                <th>Dirección</th>
                <th>Estado</th>
                <th>Acción</th>
            </tr>
            </thead>
            <tbody>
            @foreach($transporters as $transporter)
            <tr >
                <td>
                    @foreach ($cities as $city)
                        @if($transporter->city_id == $city->id)
                            {{ $city->city }}
                        @endif
                    @endforeach
                </td>
                <td>{{ $transporter->fullname }}</td>
                <td>{{ $transporter->document_number }}</td>
                <td>{{ $transporter->email }}</td>
                <td>{{ $transporter->phone }}</td>
                <td>{{ $transporter->cellphone }}</td>
                <td>{{ $transporter->address }} {{ $transporter->address_supplement }}</td>
                <td>@if ($transporter->status)
                        <span class="badge bg-green">Activo</span>
                    @else
                        <span class="badge bg-red">Inactivo</span>
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminTransporter.edit', ['id' => $transporter->id]) }}">Editar</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminTransporter.delete', ['id' => $transporter->id]) }}" onclick="return confirm('¿Estas seguro que deseas cambiar el estado del transportador?')">Cambiar Estado</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <thead>
            <tr>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr><td align="center">No hay transportistas</td></tr>
            </tbody>
        @endif
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($transporters) }} registros</div>
        </div>
    </div>

@endif

    <script>
        $(function() {
            // DataTable
            var table = $('#transporter-table').DataTable( {
                "bFilter": false
            } );

        });
    </script>
@stop
