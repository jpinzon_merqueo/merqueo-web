@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<script>
    var web_url_ajax = "{{ route('adminCoupons.index') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminCouponManager.index') }}">
            <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-lock"></i> Administrador</button>
        </a>
        @if($permission_insert)
        <a href="{{ route('adminCoupons.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Cupón</button>
        </a>
        @endif
        
    </span>
</section>

<section class="content">
@if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Hecho!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="search-form">
                                <table width="100%" class="admin-stock-table">
                                    <tr><td colspan="5"><div class="unseen alert alert-danger form-has-errors"></div></td></tr>
                                    <tr>
                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                        <td align="right"><input type="text" placeholder="Código del cupón" class="search form-control"></td>
                                        <td><button type="button" id="btn-search" class="btn btn-primary">Buscar</button></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
    paging(1, $('.search').val());

    $('#btn-search-updates').click(function(){
        paging(1, $('.search').val());
    });
});
</script>

@else

@section('content')

<table id="coupons-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Código del cupón</th>
            <th>Tipo de descuento</th>
            <th>Valor del descuento</th>
            <th>Tipo de uso</th>
            <th>Redimido</th>
            <th>Usuario</th>
            <th>Estado</th>
            <!-- Mostrar sólo tiene algún permiso para realizar acciones sobre los cupones -->
            @if($permission_update || $permission_delete)
                <th>Acciones</th>
            @endif
            <!-- End if de validación de permisos -->

        </tr>
    </thead>
    <tbody>
    @if (count($coupons))
        @foreach($coupons as $coupon)
        <tr>
            <td>{{ $coupon->code }}</td>
            <td>{{ $coupon->type }}</td>
            <td align="right">@if($coupon->type == 'Credit Amount') ${{ number_format($coupon->amount, 0, ',', '.') }} @else {{ $coupon->amount }}% @endif</td>
            <td>{{ $coupon->type_use }}</td>
            <td align="right">{{ $coupon->qty_uses }} @if ($coupon->qty_uses == 1) Vez @else Veces @endif</td>
            <td>{{ $coupon->fullname }}</td>
            <td>{{ $coupon->status == 1 ? '<span class="badge bg-green">Activo</span>' : '<span class="badge bg-red">Inactivo</span>' }}</td>
            
            <!-- Mostrar sólo tiene algún permiso para realizar acciones sobre los cupones -->
            @if($permission_update || $permission_delete)
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-danger">Acción</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        @if($permission_update)<li><a href="{{ route('adminCoupons.edit', ['id' => $coupon->id]) }}">Editar</a></li>@endif
                        @if($permission_update && $permission_delete)<li class="divider"></li>@endif <!-- Mostramos el separador solo cuando estén las dos opciones -->
                        @if($permission_delete)<li><a href="{{ route('adminCoupons.delete', ['id' => $coupon->id]) }}"  onclick="return confirm('¿Estas seguro que deseas eliminar el cupón?')">Eliminar</a></li>@endif
                    </ul>
                </div>
            </td>
            @endif
            <!-- End if de validación de permisos -->

        </tr>
        @endforeach
    @else
        <tr><td colspan="10" align="center">No se encontrarón cupones</td></tr>
    @endif
    </tbody>
</table>

@endif

@stop
