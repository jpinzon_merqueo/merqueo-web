@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Panel de control</small>
    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id="main-form" action="{{ route('adminCoupons.save') }}" >
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{ $coupon ? $coupon->id : '' }}">
                        <div class="form-group clearfix">
                            <div class="col-xs-3">
                                <label>Tipo de descuento</label>
                                <select name="type" class="form-control type">
                                    <option value="Credit Amount" {{ ($coupon && $coupon->type=='Credit Amount') ? 'selected="selected"' : '' }}>Crédito</option>
                                    <option value="Discount Percentage" {{ ($coupon && $coupon->type=='Discount Percentage') ? 'selected="selected"' : '' }}>Porcentaje</option>
                                </select>
                             </div>
                             <div class="col-xs-3">
                                <label>Código del cupón</label>
                                <input type="text" class="form-control" name="code" placeholder="Ingresa el código del cupón" value="{{ $coupon ? $coupon->code : '' }}">
                             </div>
                             <div class="col-xs-3">
                                <label>Valor de descuento</label>
                                <input type="text" class="form-control" name="amount" placeholder="Ingresa el valor de descuento" value="{{ $coupon ? $coupon->amount : '' }}">
                             </div>
                             <div class="col-xs-3">
                                <label>Para pedidos menores a</label>
                                <input type="text" class="form-control" name="maximum_order_amount" placeholder="Ingresa total maxímo del pedido" value="{{ $coupon && $coupon->maximum_order_amount ? $coupon->maximum_order_amount : '' }}">
                             </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-3">
                                <label>Para pedidos mayores a</label>
                                <input type="text" class="form-control" name="minimum_order_amount" placeholder="Ingresa total mínimo del pedido" value="{{ $coupon && $coupon->minimum_order_amount ? $coupon->minimum_order_amount : '' }}">
                            </div>
                            <div class="col-xs-3">
                                <label>Número de usos</label>
                                <input type="text" class="form-control" name="number_uses" placeholder="Ingresa número de usos" value="{{ $coupon ? $coupon->number_uses : '' }}">
                            </div>
                            <div class="col-xs-3">
                                <label>Tipo de uso</label>
                                <select name="type_use" class="form-control">
                                    <option value="Normal" {{ ($coupon && $coupon->type_use == 'Normal') ? 'selected="selected"' : "" }}>Normal</option>
                                    <option value="Only The First Order" {{ ($coupon && $coupon->type_use == 'Only The First Order') ? 'selected="selected"' : '' }}>Solo primera compra</option>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <label>Redimir en</label>
                                <select name="redeem_on" id="redeem_on" class="form-control">
                                    <option value="Total Cart" {{ ($coupon && $coupon->redeem_on == 'Total Cart') ? 'selected="selected"' : '' }}>Total del pedido</option>
                                    <option value="Specific Store" {{ ($coupon && $coupon->redeem_on == 'Specific Store') ? 'selected="selected"' : '' }}>Tienda / Departamento / Pasillo / Producto</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-2 @if($coupon && $coupon->redeem_on=='Specific Store') @else unseen @endif">
                                <label for="city">Ciudad</label>
                                <select class="form-control" name="city_id" id="city_id">
                                    <option value=""></option>
                                    @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" {{ ($coupon && $store && $store->city_id == $city->id) ? 'selected="selected"' : '' }}>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-2 @if($coupon && $coupon->store_id) @else unseen @endif">
                                <label for="store">Tienda</label>
                                <select class="form-control" name="store_id" id="store_id">
                                    <option value=""></option>
                                    @foreach ($stores as $store)
                                    <option value="{{ $store->id }}" {{ ($coupon && $coupon->store_id == $store->id) ? 'selected="selected"' : '' }}>{{ $store->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-2 @if($coupon && ($coupon->store_id || $coupon->department_id)) @else unseen @endif">
                                <label for="department">Departamento</label>
                                <select class="form-control" name="department_id" id="department_id">
                                    <option value=""></option>
                                    @foreach ($departments as $department)
                                    <option value="{{ $department->id }}" {{ ($coupon && $coupon->department_id == $department->id) ? 'selected="selected"' : '' }}>{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-2 @if($coupon && ($coupon->department_id || $coupon->shelf_id)) @else unseen @endif">
                                <label for="shelf">Pasillo</label>
                                <select class="form-control" name="shelf_id" id="shelf_id">
                                    <option value=""></option>
                                    @foreach ($shelves as $shelf)
                                    <option value="{{ $shelf->id }}" {{ ($coupon && $coupon->shelf_id == $shelf->id) ? 'selected="selected"' : '' }}>{{ $shelf->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-4 @if($coupon && ($coupon->shelf_id || $coupon->store_product_id)) @else unseen @endif">
                                <label for="product">Producto</label>
                                <select class="form-control" name="store_product_id" id="store_product_id">
                                    <option value=""></option>
                                    @foreach ($store_products as $store_product)
                                    <option value="{{ $store_product->id }}" {{ ($coupon && $coupon->store_product_id == $store_product->id) ? 'selected="selected"' : '' }}>{{ $store_product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-3">
                                <label>Método de pago</label>
                                <select name="payment_method" id="payment_method" class="form-control">
                                    <option value=""></option>
                                    @foreach($payment_methods as $value => $name)
                                    <option value="{{ $value }}" {{ ($coupon && $coupon->payment_method == $value) ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-3 @if($coupon && !in_array($coupon->payment_method, $config['credit_cards_types'])) unseen @endif">
                                <label>Bines tarjeta de crédito</label>
                                <input type="text" class="form-control" id="cc_bin" name="cc_bin" placeholder="Ingresa bines de tarjeta de crédito" value="{{ $coupon ? $coupon->cc_bin : '' }}">
                            </div>
                            <div class="col-xs-3 @if($coupon && !in_array($coupon->payment_method, $config['credit_cards_types'])) unseen @endif">
                                <label>Banco tarjeta de crédito</label>
                                <input type="text" class="form-control" id="cc_bank" name="cc_bank" placeholder="Ingresa banco tarjeta de crédito" value="{{ $coupon ? $coupon->cc_bank : '' }}">
                            </div>
                            <div class="col-xs-3">
                                <label>Fecha de expiración</label>
                                <input type="text" class="form-control" id="expiration_date" name="expiration_date" placeholder="Ingresa la fecha de expiración (dd/mm/yyyy)" value="{{ $coupon ? date("d/m/Y", strtotime($coupon->expiration_date)) : '' }}">
                            </div>
                            <div class="col-xs-3">
                                <label>Estado</label>
                                <select name="status" class="form-control">
                                    <option value="1" {{ ($coupon && $coupon->status==1) ? 'selected="selected"' : '' }}>Activo</option>
                                    <option value="0" {{ ($coupon && $coupon->status==0) ? 'selected="selected"' : '' }}>Inactivo</option>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <label>Grupo para validación</label>
                                <input type="text" class="form-control" name="campaign_validation" placeholder="Ingresa el nombre del grupo de validación" value="{{ $coupon ? $coupon->campaign_validation : '' }}">
                            </div>
                         </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

$(document).ready(function(){

    $.validator.addMethod("dateFormat", function(value,element) {
            return value.match(/^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/);
        }, "Please enter a date in the format dd/mm/yyyy"
    );
    $.validator.addMethod("percentage", function(value, element) {
        if ($('.type').val() == 'Discount Percentage'){
            if (value > 0 && value < 101)
                return true;
            return false;
        }
        return true;
        }, "Please enter a percentage between 1 and 100"
    );

    $("#main-form").validate({
      rules: {
        coupon_code: "required",
        amount: {
          required: true,
          number: true,
          percentage: true
        },
        minimum_order_amount: {
          number: true,
        },
        maximum_order_amount: {
          number: true,
        },
        number_uses: {
          required: true,
          number: true
        },
        store_id: {
            required: function(){
                if ($('#redeem_on').val() == 'Specific Store'){
                    return true;
                }else return false;
            },
            number: true
        },
        expiration_date: {
          required: true,
          dateFormat: true
        }
      }
    });

    $('#redeem_on').change(function(){
        if ($(this).val() == 'Specific Product'){
            $('#store_product_id').parent().show();
        }else{
            $('#store_product_id').val('').parent().hide();
        }
        if ($(this).val() == 'Specific Store'){
           $('#city_id').parent().show();
        }else{
            $('#city_id').val('').parent().hide();
            $('#store_id').val('').parent().hide();
            $('#department_id').val('').parent().hide();
            $('#shelf_id').val('').parent().hide();
        }
    });

    $('#city_id').change(function () {
        var city_id = $('#city_id').val();
        $('#store_id').empty().parent().show();

        $.ajax({ url: "{{ route('adminOrderStorage.getStoresAjax') }}",
            data: { city_id: city_id },
            type: 'get',
            async: false,
            success:
            function(msg) {
                $('#store_id').append('<option value=""></option>');
                $.each(msg, function(key, value){
                    $('#store_id').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                $('#store_id').slideDown('fast');
            }
        });
    });

    $('#store_id').change(function () {
        var store_id = $('#store_id').val();
        $('#department_id').empty().parent().show();

        $.ajax({ url: "{{ route('adminProducts.getDepartmentsAjax') }}",
            data: { store_id: store_id },
            type: 'get',
            async: false,
            success:
            function(msg) {
                $('#department_id').append('<option value=""></option>');
                $.each(msg, function(key, value){
                    $('#department_id').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                $('#department_id').slideDown('fast');
            }
        });
    });

    $('#department_id').change(function () {
        var department_id = $('#department_id').val();
        $('#shelf_id').empty().parent().show();

        $.ajax({ url: "{{ route('adminProducts.getShelvesAjax') }}",
            data: { department_id: department_id },
            type: 'get',
            async: false,
            success:
            function(msg) {
                $('#shelf_id').append('<option value=""></option>');
                $.each(msg, function(key, value){
                    $('#shelf_id').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                $('#shelf_id').slideDown('fast');
            }
        });
    });

    $('#shelf_id').change(function () {
        var shelf_id = $('#shelf_id').val();
        $('#store_product_id').empty().parent().show();

        $.ajax({ url: "{{ route('adminProducts.getProductsAjax') }}",
            data: { shelf_id: shelf_id },
            type: 'get',
            async: false,
            success:
            function(msg) {
                $('#store_product_id').append('<option value=""></option>');
                $.each(msg, function(key, value){
                    $('#store_product_id').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                $('#store_product_id').slideDown('fast');
            }
        });
    });

    $('#payment_method').change(function(){
        string = $('#payment_method option:selected').text()
        if (string.indexOf('Tarjeta de crédito') > -1){
            $('#cc_bin').parent().show();
            $('#cc_bank').parent().show();
        }else{
            $('#cc_bin').val('').parent().hide();
            $('#cc_bank').val('').parent().hide();
        }
    });
    $('#payment_method').trigger('change');

    $('#expiration_date').datetimepicker({
        format: 'DD/MM/YYYY'
    })

});
</script>

@stop
