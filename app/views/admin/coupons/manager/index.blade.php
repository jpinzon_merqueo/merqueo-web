@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminCouponManager.massiveCreatorIndex') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Creador masivo</button>
            </a>
        </span>
    </section>

    @if(Session::get('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
    @elseif(Session::get('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alert!</b> {{ Session::get('error') }}
        </div>
    @endif

    <section class="content" id="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" @submit.prevent="searchCoupons">
                                    <div class="row form-group">
                                        <div class="col-xs-3">
                                            <label for="codigo">Código:</label>
                                            <input type="text" id="code" name="code" class="form-control" placeholder="Código de cupon" v-model="code" :disabled="paginator.total > 0">
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="campaign_validation">Campaña:</label>
                                            <select name="campaign_validation" id="campaign_validation" class="form-control" v-model="campaign_validation" :disabled="paginator.total > 0">
                                                <option value="">-Selecciona-</option>
                                                @foreach($campaign_validation as $campaign)
                                                    <option value="{{ is_null($campaign->campaign_validation) ? 'NULL' : $campaign->campaign_validation }}">{{ $campaign->campaign_validation }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="status">Estado:</label>
                                            <select name="status" id="status" class="form-control" v-model="status">
                                                <option value="">-Selecciona-</option>
                                                <option value="activo">Activo</option>
                                                <option value="inactivo">Inactivo</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="used">Usados:</label>
                                            <select name="used" id="used" class="form-control" v-model="used">
                                                <option value="">-Selecciona-</option>
                                                <option value="unused">No usados</option>
                                                <option value="used">Usados</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-3">
                                            <button type="submit" class="btn btn-primary" style="margin-top: 25px">Buscar</button>
                                            <button type="reset" class="btn btn-primary" style="margin-top: 25px" @click="resetForm">Reset</button>
                                        </div>
                                    </div>
                                    <div class="row" v-show="error">
                                        <div class="col-xs-12">
                                            <div class="alert alert-warning">
                                                <strong>Alerta!</strong> @{{ error }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" v-show="success">
                                        <div class="col-xs-12">
                                            <div class="alert alert-success">
                                                <strong>Exitoso!</strong> @{{ success }}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading" v-show="isLoading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row form-group" v-show="searched_campaign != '' && used == 'unused' && paginator.total > 0">
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-warning" @click="deleteCoupons">
                                    Borrar no usados
                                </button>
                            </div>
                        </div>
                        <div class="row" v-if="paginator.total > 0">
                            <div class="col-xs-12 text-center">
                                    Mostrando @{{ paginator.per_page }} de @{{ paginator.total }}
                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cupón</th>
                                    <th>Creador</th>
                                    <th>Tipo de descuento</th>
                                    <th>Valor del descuento</th>
                                    <th>Tipo de uso</th>
                                    <th>Redimido</th>
                                    <th>Campaña</th>
                                    <th>Fecha de expiración</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody v-if="paginator.total > 0">
                                <tr v-for="(coupon, index) in paginator.data">
                                    <td>@{{ paginator.from + index }}</td>
                                    <td>@{{ coupon.code }}</td>
                                    <td>@{{ coupon.fullname }}</td>
                                    <td>@{{ coupon.type }}</td>
                                    <td v-show="coupon.type == 'Credit Amount'">$@{{ coupon.amount.toLocaleString() }}</td>
                                    <td v-show="coupon.type == 'Discount Percentage'">%@{{ coupon.amount }}</td>
                                    <td>@{{ coupon.type_use }}</td>
                                    <td>@{{ coupon.uses_number }} veces</td>
                                    <td>@{{ coupon.campaign_validation }}</td>
                                    <td>@{{ coupon.expiration_date }}</td>
                                    <td>
                                        <span class="label label-success" v-if="coupon.status">
                                            Activo
                                        </span>
                                        <span class="label label-danger" v-else>
                                            Inactivo
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Mostrando @{{ paginator.per_page }} de @{{ paginator.total }}
                                    </td>
                                </tr>
                            </tbody>
                            <tbody v-else>
                                <tr>
                                    <td colspan="10" class="text-center">
                                        No se han encontrado cupones
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row" >
                            <div class="col-xs-12 text-center" >
                                <ul class="pagination">
                                    <li :class="paginator.current_page == 1 ? 'disabled' : ''">
                                        <a href="javascript:void(0)" @click.prevent="paginator.current_page != 1 ? changePage(paginator.current_page-1) : ''">&laquo;</a>
                                    </li>
                                    <li v-for="n in pages" :class="paginator.current_page == n ? 'active' : ''">
                                        <a href="javascriot:void(0)" @click.prevent="paginator.current_page != n && n != '...' ? changePage(n) : ''">
                                            @{{ n }}
                                            <span class="sr-only" v-show="paginator.current_page == n">(current)</span>
                                        </a>
                                    </li>
                                    <li :class="paginator.current_page == paginator.last_page ? 'disabled' : ''">
                                        <a href="javascript:void(0)" @click.prevent="paginator.current_page != paginator.last_page ? changePage(paginator.current_page+1) : ''">&raquo;</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.coupons.manager.js.index-js')
@endsection