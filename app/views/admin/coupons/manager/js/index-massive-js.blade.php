<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.min.js"></script>-->
<script>
    $(document).ready(function() {
        $("#coupon-form").validate();
    });
    let vue_obj = new Vue({
        el: '#content',
        data: {
            form_data: {
                type: 'Credit Amount',
                amount: '',
                maximum_order_amount: '',
                minimum_order_amount: '',
                number_uses: '',
                type_use: '',
                expiration_date: '',
                campaign_validation: '',
                payment_method: '',
                cc_bin: '',
                cc_bank: '',
                redeem_on: '',
                city_id: '',
                store_id: '',
                department_id: '',
                shelf_id: '',
                store_product_id: '',
                main_string: '',
                digit_length: '',
                coupon_string_type: '',
                coupon_quantity: ''
            },
            form: {
                cities: {{ $cities->toJson() }},
                stores: [],
                departments: [],
                shelves: [],
                store_products: []
            }
        },
        methods: {
            paymentMethodChanged: function () {
                if (this.form_data.payment_method == 'Efectivo' || this.form_data.payment_method == 'Datáfono') {
                    this.form_data.cc_bin = '';
                    this.form_data.cc_bank = '';
                }
            },
            resetRedeemOn: function() {
                this.form_data.city_id = '';
                this.form_data.store_id = '';
                this.form_data.department_id = '';
                this.form_data.shelf_id = '';
                this.form_data.store_product_id = '';

                this.form.departments = [];
                this.form.shelves = [];
                this.form.store_products = [];
            },
            getStores: function () {
                this.form.departments = [];
                this.form.shelves = [];
                this.form.store_products = [];
                this.form_data.store_id = '';
                this.form_data.department_id = '';
                this.form_data.shelf_id = '';
                this.form_data.store_product_id = '';
                axios.get('{{ route('adminCouponManager.getStoresAjax') }}', {
                    params: {
                        city_id: this.form_data.city_id
                    }
                })
                    .then(response => (this.form.stores = response.data))
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            getDepartments: function () {
                this.form.shelves = [];
                this.form.store_products = [];
                this.form_data.department_id = '';
                this.form_data.shelf_id = '';
                this.form_data.store_product_id = '';
                axios.get('{{ route('adminCouponManager.getDepartmentsAjax') }}', {
                    params: {
                        store_id: this.form_data.store_id
                    }
                })
                    .then(response => (this.form.departments = response.data))
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            getShelves: function () {
                this.form.store_products = [];
                this.form_data.shelf_id = '';
                this.form_data.store_product_id = '';
                axios.get('{{ route('adminCouponManager.getShelvesAjax') }}', {
                    params: {
                        store_id: this.form_data.store_id,
                        department_id: this.form_data.department_id
                    }
                })
                    .then(response => (this.form.shelves = response.data))
                    .catch(function (error) {
                        console.log(error)
                    });
            },
            getProducts: function () {
                this.form_data.store_product_id = '';
                axios.get('{{ route('adminCouponManager.getProductsAjax') }}', {
                    params: {
                        store_id: this.form_data.store_id,
                        department_id: this.form_data.department_id,
                        shelf_id: this.form_data.shelf_id
                    }
                })
                    .then(response => (this.form.store_products = response.data))
                    .catch(function (error) {
                        console.log(error)
                    });
            }
        }
    });
</script>