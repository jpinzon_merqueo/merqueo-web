<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.min.js"></script>-->
<script>
    let vue_obj = new Vue({
        el: '#content',
        data: {
            code: '',
            campaign_validation: '',
            isLoading: false,
            status: '',
            used: '',
            error: '',
            success: '',
            paginator: {
                data: [],
                total: 0,
                per_page: 0,
                current_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
                code: '',
                campaign_validation: '',
            },
            pages: [],
            searched_campaign: ''
        },
        methods: {
            resetForm: function (){
                this.code = '';
                this.campaign_validation = '';
                this.isLoading = false;
                this.status = '';
                this.used = '';
                this.error = '';
                this.paginator = {
                    data: [],
                    total: 0,
                    per_page: 0,
                    current_page: 0,
                    last_page: 0,
                    from: 0,
                    to: 0,
                    code: '',
                    campaign_validation: '',
                };
                this.pages = [];
                this.searched_campaign = '';
            },
            searchCoupons: function () {
                this.error = ''
                if (this.code == '' && this.campaign_validation == '') {
                    this.error = 'Debe buscar por alguno de los campos de código o campaña.'
                    return
                }
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminCouponManager.searchCupons') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        code: this.code,
                        campaign_validation: this.campaign_validation,
                        status: this.status,
                        used: this.used
                    },
                    context: this
                })
                    .done(function(data) {
                        this.paginator = data;
                        this.searched_campaign = data.campaign_validation;
                        this.calculatePages();
                    })
                    .fail(function() {
                        console.log("error al cargar los cupones buscados.");
                    })
                    .always(function() {
                        this.isLoading = false
                    });
            },
            changePage: function (n) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminCouponManager.searchCupons') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        code: this.paginator.code,
                        campaign_validation: this.paginator.campaign_validation,
                        page: n
                    },
                    context: this
                })
                    .done(function(data) {
                        this.paginator = data
                        this.calculatePages();
                    })
                    .fail(function() {
                        console.log("error al cargar los cupones buscados.");
                    })
                    .always(function() {
                        this.isLoading = false
                    });
            },
            calculatePages: function () {
                let last = this.paginator.last_page;
                let delta = 5;
                let left = this.paginator.current_page - delta;
                let right = this.paginator.current_page + delta + 1;
                let range = [];
                let pages = [];
                let l;

                for (let i = 1; i <= last ; i++) {
                    if (i == 1 || i == last || (i >= left && i < right)) {
                        range.push(i);
                    }
                }

                range.forEach(function (i) {
                    if (l) {
                        if (i - l === 2) {
                            pages.push(l + 1);
                        } else if (i - l !== 1) {
                            pages.push('...');
                        }
                    }
                    pages.push(i);
                    l = i;
                });

                this.pages = pages;
            },
            deleteCoupons: function () {
                let answer = confirm(`Desea borrar los cupones no usados de la campaña "${this.searched_campaign}" ?`);
                if (answer) {
                    this.isLoading = true;
                    $.ajax({
                        url: '{{ route('adminCouponManager.deleteCoupons') }}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            campaign_validation: this.searched_campaign
                        },
                        context: this
                    })
                        .done(function(data) {
                            let self = this;
                            if (data.coupons) {
                                this.success = `Se han eliminado ${data.coupons} cupones`;
                                this.searchCoupons();
                                setTimeout(function () {
                                    self.success = '';
                                }, 5000);
                            } else {
                                this.error = data.error;
                                this.searchCoupons();
                                setTimeout(function () {
                                    self.error = '';
                                }, 5000);
                            }
                        })
                        .fail(function() {
                            console.log("error");
                        })
                        .always(function() {
                            console.log("complete");
                            this.isLoading = false;
                        });

                }
            }
        }
    });
</script>