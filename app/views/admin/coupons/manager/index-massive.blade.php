@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form action="{{ route('adminCouponManager.createMassiveCoupons') }}" id="coupon-form" method="POST">
                            <div class="row form-group">
                                <div class="col-xs-3">
                                    <label for="type">Tipo de descuento</label>
                                    <select name="type" id="type" class="form-control required" v-model="form_data.type" required>
                                        <option value="Credit Amount">Crédito</option>
                                        <option value="Discount Percentage">Porcentaje</option>
                                    </select>
                                </div>
                                <div class="col-xs-3">
                                    <label for="amount">Valor del descuento</label>
                                    <input type="number" name="amount" id="amount" placeholder="Ingresa el valor del descuento" class="form-control required" v-model="form_data.amount" required>
                                </div>
                                <div class="col-xs-3">
                                    <label for="maximum_order_amount">Para pedidos menores a</label>
                                    <input type="number" name="maximum_order_amount" id="maximum_order_amount" placeholder="Ingresa total máximo del pedido" class="form-control" v-model="form_data.maximum_order_amount">
                                </div>
                                <div class="col-xs-3">
                                    <label for="minimum_order_amount">Para pedidos mayores a</label>
                                    <input type="number" name="minimum_order_amount" id="minimum_order_amount" placeholder="Ingresa total mínimo del pedido" class="form-control" v-model="form_data.minimum_order_amount">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-3">
                                    <label for="number_uses">Número de usos</label>
                                    <input type="number" name="number_uses" id="number_uses" placeholder="Ingresa número de usos" class="form-control required" v-model="form_data.number_uses" required>
                                </div>
                                <div class="col-xs-3">
                                    <label for="type_use">Tipo de uso</label>
                                    <select name="type_use" id="type_use" class="form-control required" v-model="form_data.type_use" required>
                                        <option value="Normal">Normal</option>
                                        <option value="Only The First Order">Solo primera compra</option>
                                    </select>
                                </div>
                                <div class="col-xs-3">
                                    <label for="expiration_date">Fecha de expiración</label>
                                    <input type="date" name="expiration_date" id="expiration_date" class="form-control required" v-model="form_data.expiration_date" required>
                                </div>
                                <div class="col-xs-3">
                                    <label for="campaign_validation">Grupo para validación</label>
                                    <input type="text" name="campaign_validation" id="campaign_validation" class="form-control required" v-model="form_data.campaign_validation" placeholder="Ingrese el nombre de la campaña" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-3">
                                    <label for="payment_method">Método de pago</label>
                                    <select name="payment_method" id="payment_method" class="form-control" v-model="form_data.payment_method" @change="paymentMethodChanged">
                                        <option value="">-Selecciona-</option>
                                        <option value="Tarjeta de crédito">Tarjeta de crédito</option>
                                        <option value="AMEX">Tarjeta de crédito American Express</option>
                                        <option value="VISA">Tarjeta de crédito Visa</option>
                                        <option value="MASTERCARD">Tarjeta de crédito Mastercard</option>
                                        <option value="Efectivo">Efectivo</option>
                                        <option value="Datáfono">Datáfono</option>
                                    </select>
                                </div>
                                <div class="col-xs-3" v-show="form_data.payment_method == 'Tarjeta de crédito' || form_data.payment_method == 'AMEX' || form_data.payment_method == 'VISA' || form_data.payment_method == 'MASTERCARD'">
                                    <label for="cc_bin">Bines tarjeta de crédito</label>
                                    <input type="text" name="cc_bin" id="cc_bin" class="form-control" v-model="form_data.cc_bin">
                                </div>
                                <div class="col-xs-3" v-show="form_data.payment_method == 'Tarjeta de crédito' || form_data.payment_method == 'AMEX' || form_data.payment_method == 'VISA' || form_data.payment_method == 'MASTERCARD'">
                                    <label for="cc_bank">Banco tarjeta de crédito</label>
                                    <input type="text" name="cc_bank" id="cc_bank" class="form-control" v-model="form_data.cc_bank">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-3">
                                    <label for="redeem_on">Redimir en</label>
                                    <select name="redeem_on" id="redeem_on" class="form-control required" v-model="form_data.redeem_on" @change="resetRedeemOn" required>
                                        <option value="Total Cart">Total del pedido</option>
                                        <option value="Specific Store">Tienda / Departamento / Pasillo / Producto</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2" v-show="form_data.redeem_on == 'Specific Store'">
                                    <label for="city_id">Ciudad</label>
                                    <select name="city_id" id="city_id" class="form-control required" v-model="form_data.city_id" @change="getStores" required>
                                        <option value=""></option>
                                        <option v-for="(city, index) in form.cities" :value="city.id">@{{ city.city }}</option>
                                    </select>
                                </div>
                                <div class="col-xs-2" v-show="form_data.redeem_on == 'Specific Store' && form_data.city_id != '' && form.stores.length > 0">
                                    <label for="store_id">Tienda</label>
                                    <select name="store_id" id="store_id" class="form-control required" v-model="form_data.store_id" @change="getDepartments" required>
                                        <option value=""></option>
                                        <option v-for="(store, index) in form.stores" :value="store.id">@{{ store.name }}</option>
                                    </select>
                                </div>
                                <div class="col-xs-2" v-show="form_data.redeem_on == 'Specific Store' && form_data.city_id != '' && form_data.store_id != '' && form.departments.length > 0">
                                    <label for="department_id">Departamento</label>
                                    <select name="department_id" id="department_id" class="form-control" v-model="form_data.department_id" @change="getShelves">
                                        <option value=""></option>
                                        <option v-for="(department, index) in form.departments" :value="department.id">@{{ department.name }}</option>
                                    </select>
                                </div>
                                <div class="col-xs-2" v-show="form_data.redeem_on == 'Specific Store' && form_data.city_id != '' && form_data.store_id != '' && form_data.department_id != '' && form.shelves.length > 0">
                                    <label for="shelf_id">Pasillo</label>
                                    <select name="shelf_id" id="shelf_id" class="form-control" v-model="form_data.shelf_id" @change="getProducts">
                                        <option value=""></option>
                                        <option v-for="(shelf, index) in form.shelves" :value="shelf.id">@{{ shelf.name }}</option>
                                    </select>
                                </div>
                                <div class="col-xs-4" v-show="form_data.redeem_on == 'Specific Store' && form_data.city_id != '' && form_data.store_id != '' && form_data.department_id != '' && form_data.shelf_id != '' && form.store_products.length > 0">
                                    <label for="store_product_id">Producto</label>
                                    <select name="store_product_id" id="store_product_id" class="form-control" v-model="form_data.store_product_id">
                                        <option value=""></option>
                                        <option v-for="(store_product, index) in form.store_products" :value="store_product.id">@{{ store_product.name }} - @{{ store_product.quantity }} @{{ store_product.unit }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-3">
                                    <label for="main_string">Prefijo de cupones</label>
                                    <input type="text" name="main_string" id="main_string" class="form-control" placeholder="Ingrese el texto que será prefijo de los cupones" v-model="form_data.main_string">
                                </div>
                                <div class="col-xs-3">
                                    <label for="digit_length">Número de digitos del cupón</label>
                                    <input type="number" name="digit_length" id="digit_length" class="form-control required" placeholder="Ingrese el número de digitos del cupón" v-model="form_data.digit_length" required>
                                </div>
                                <div class="col-xs-3">
                                    <label for="coupon_string_type">Tipo de coupón</label>
                                    <select name="coupon_string_type" id="coupon_string_type" class="form-control required" v-model="form_data.coupon_string_type" required>
                                        <option value="text">Texto</option>
                                        <option value="numeric">Numérico</option>
                                    </select>
                                </div>
                                <div class="col-xs-3">
                                    <label for="coupon_quantity">Cantidad de cupones a crear</label>
                                    <input type="number" name="coupon_quantity" id="coupon_quantity" class="form-control required" max="10000" v-model="form_data.coupon_quantity" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 text-right">
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.coupons.manager.js.index-massive-js')
@endsection