@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script>
        var web_url_ajax = "{{ route('adminStockManagement.searchProduct') }}";
    </script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <table width="100%" class="admin-stock-table">
                                         <tr><td colspan="5"><div class="unseen alert alert-danger form-has-errors"></div></td></tr>
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    
                                                </select>
                                            </td>
                                            <td align="right"><label>Buscar:</label>&nbsp;</td>
                                            <td><input type="text" placeholder="Referencia. PLU, Nombre" class="search form-control"></td>
                                            <td><button type="button" id="btn-search-updates" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="paging">
                            <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Referencia</th>
                                        <th>PLU</th>
                                        <th>Nombre</th>
                                        <th>Ciudad</th>
                                        <th>Proveedor</th>
                                        <th>Unidad de medida</th>
                                        <th>Stock actual</th>
                                        <th>Stock mínimo</th>
                                        <th>Stock ideal</th>
                                        <th>Actualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="11" align="center">No hay productos.</td></tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 productos.</div>
                                </div>
                            </div>
                        </div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Edit Product Modal -->
    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <form method="post" id="" action="{{ route('adminStockManagement.updateProductStock') }}" class="form-modal" id="update-product">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="store_product_id" name="store_product_id" value="">
                        <input type="hidden" id="modal_warehouse_id" name="warehouse_id" value="">
                        <div class="form-group">
                            <label class="reject-label">Tipo de movimiento</label>
                            <select class="form-control required" name="type">
                                <option value="">-Selecciona-</option>
                                <option value="Aumento de unidades">Aumento de unidades</option>
                                <option value="Disminución de unidades">Disminución de unidades</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Unidades de producto a actualizar</label>
                            <input type="text" class="form-control required" name="quantity" id="product-quantity">
                        </div>
                        <div class="form-group">
                            <label class="reject-label">Motivo de actualización</label>
                            <textarea class="form-control required" id="comments" name="comments"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
    $(document).ready(function() {
        $('.paging-loading').hide();

        $('#btn-search-updates').click(function(){
            var error = false
            var form = this;
            $('.form-has-errors').html(error).fadeOut();

            if ($('.search').is(':visible') && ($('.search').val().length == 0)) {
                $('.search').parent().addClass('has-error');
                error = 'Completa el campo de búsqueda.';
            }else $('.search').parent().removeClass('has-error');

            if(error) {
                $('.form-has-errors').html(error).fadeIn();
                return false;
            }
            paging(1, $('.search').val());
        });

        $('.form-modal').on('submit', function(e) {
            var error = false
            var form = this;

            $('input[type!="hidden"], select, textarea', this).each(function() {
                if ($(this).is(':visible') && ($(this).val().length == 0)) {
                    $(this).parent().addClass('has-error');
                    error = 'Completa los campos requeridos.';
                }else{
                    if($('#product-quantity').val() > 3000){
                        $('#product-quantity').parent().addClass('has-error');
                        error = 'La cantidad máxima permitida a actualizar es 10.';
                    }else $(this).parent().removeClass('has-error');
                }

            });

            if(error) {
                $('.form-has-errors', form).html(error).fadeIn();
                return false;
            }
            return true;
        });
        $('#city_id').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#city_id').val() },
                success : function( request ){
                    console.log(request);
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse_id').html(options);
                    //paging(1, $('.search').val());
                }
            });
        });
        $('#city_id').trigger('change');
    });

    function edit(store_product_id, product_name, warehouse_id){
        $('#store_product_id').val(store_product_id);
        $('#modal_warehouse_id').val(warehouse_id);
        $('.modal-title').html('Producto - '+product_name);
        $('#edit-modal').modal('show');
    }
    </script>
    @endsection
@else
    @section('content')
    <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Nombre</th>
                <th>Ciudad</th>
                <th>Bodega</th>
                <th>Proveedor</th>
                <th>Unidad de medida</th>
                <th>Stock actual</th>
                <th>Stock mínimo</th>
                <th>Stock ideal</th>
                <th>Actualizar</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products))
            @foreach($products as $product)
            <tr>
                <td>{{ $product->store_product_id }}</td>
                <td>{{ $product->reference }}</td>
                <td>{{ $product->provider_plu }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->city }}</td>
                <td>{{ $product->warehouse }}</td>
                <td>{{ $product->provider_name }}</td>
                <td>{{ $product->quantity }} {{ $product->unit }}</td>
                <td>{{ $product->picking_stock }}</td>
                <td>{{ $product->minimum_stock }}</td>
                <td>{{ $product->ideal_stock }}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn-group btn btn-xs btn-default" href="#" onclick="edit({{ $product->store_product_id }}, '{{ addslashes($product->name) }}', {{ $product->warehouse_id }});"><span class="glyphicon glyphicon-folder-open"></span></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="12" align="center">No hay productos</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($products) }} productos.</div>
        </div>
    </div>
    @endif
@stop