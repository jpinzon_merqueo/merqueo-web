@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <script>
        var web_url_ajax = "{{ route('adminStockManagement.updateProductsStock') }}";
    </script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            @if ( $admin_permissions['permission3'] )
                <a href="javascript:;" onclick="show_modal('stock-update')">
                    <button type="button" class="btn btn-success"><i class="fa fa-file-o"></i>  Importar archivo de actualización</button>
                </a>
            @endif
            <a href="{{ route('adminStockManagement.searchProduct') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Actualización de Stock</button>
            </a>
        </span>
    </section>
    <section class="content">
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <table width="100%" class="admin-stock-table">
                                        <tr><td colspan="7"><div class="unseen alert alert-danger form-has-errors"></div></td></tr>
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    
                                                </select>
                                            </td>
                                            <td align="right"><label>Buscar:</label>&nbsp;</td>
                                            <td><input type="text" placeholder="Referencia. PLU, Nombre" class="search form-control"></td>
                                            <td><button type="button" id="btn-search-updates" class="btn btn-primary">Buscar</button></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="paging">
                            <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Referencia</th>
                                        <th>Nombre</th>
                                        <th>Ciudad</th>
                                        <th>PLU</th>
                                        <th>Proveedor</th>
                                        <th>Unidad de medida</th>
                                        <th>Unidades ingresadas</th>
                                        <th>Stock anterior</th>
                                        <th>Stock nuevo</th>
                                        <th>Usuario</th>
                                        <th>Fecha</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="12" align="center">No hay productos.</td></tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 actualizaciones.</div>
                                </div>
                            </div>
                        </div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Update stock Modal -->
    <div class="modal fade" id="modal-stock-update">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title accept-title">Importar archivo de actualización de stock</h4>
                </div>
                <form id="update-to-enrutado" method="POST" action="{{ route('adminStockManagement.importStockUpdate') }}" class="form-modal" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group accept-options row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <label class="accept-label" for="cities">Ciudad</label>
                                <select name="cities" id="cities" class="form-control required">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group accept-options row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <label class="accept-label" for="warehouse">Bodega</label>
                                <select name="warehouse" id="warehouse" class="form-control required">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <label for="last_updated_stock_date">Tipo de actualización</label>
                                <select name="type_upload" id="type_upload" class="form-control required">
                                    <option value=""></option>
                                    @if ( $admin_permissions['permission1'] )
                                    <option value="inventario">Actualizacion de picking en bodega</option>
                                    @endif
                                    @if ( $admin_permissions['permission2'] )
                                    <option value="compras">Actualización de stock para compras</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group accept-options row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <label class="accept-label" for="file_upload">Archivo</label>
                                <input class="form-control" type="file" name="file" id="file_upload" required="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Importar archivo</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
    var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
    $(document).ready(function() {
        $('#last_updated_stock_date').datetimepicker({
            // format: 'YYYY/MM/DD HH:mm:ss'
            format: 'DD/MM/YYYY HH:mm:ss'
        });

        
        $('#update-to-enrutado').validate();

        $('#btn-search-updates').click(function(){
            var error = false
            var form = this;
            $('.form-has-errors').html(error).fadeOut();

            if ($('.search').is(':visible') && ($('.search').val().length == 0)) {
                $('.search').parent().addClass('has-error');
                error = 'Completa el campo de búsqueda.';
            }else $('.search').parent().removeClass('has-error');

            if(error) {
                $('.form-has-errors').html(error).fadeIn();
                return false;
            }
            paging(1, $('.search').val());
        });

        $('#cities').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#cities').val() },
                success : function( request ){
                    console.log(request);
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse').html(options);
                }
            });
        });

        $('#city_id').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#city_id').val() },
                success : function( request ){
                    console.log(request);
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse_id').html(options);
                    paging(1, $('.search').val());
                }
            });
        });

        $('#city_id').trigger('change');
    });
    

    function show_modal(action) {
        $('#modal-' + action).modal('show');
        $('#cities').trigger('change');
    }
    </script>
    @endsection
@else
    @section('content')
    <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Nombre</th>
                <th>Ciudad</th>
                <th>Bodega</th>
                <th>Proveedor</th>
                <th>Tipo de movimiento</th>
                <th>Unidad de medida</th>
                <th>Unidades ingresadas</th>
                <th>Stock anterior</th>
                <th>Stock nuevo</th>
                <th>Usuario</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products))
            @foreach($products as $product)
            <tr>
                <td>{{ $product->store_product_id }}</td>
                <td>{{ $product->reference }}</td>
                <td>{{ $product->provider_plu }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->city }}</td>
                <td>{{ $product->warehouse }}</td>
                <td>{{ $product->provider_name }}</td>
                <td>{{ $product->movement_type }}</td>
                <td>{{ $product->product_quantity}} {{ $product->unit }}</td>
                <td>{{ $product->quantity }}</td>
                <td>{{ $product->quantity_stock_before }}</td>
                <td>{{ $product->quantity_stock_after }}</td>
                <td>{{ $product->admin_fullname }}</td>
                <td>{{ $product->date }}</td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="14" align="center">No hay productos</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($products) }} actualizaciones.</div>
        </div>
    </div>
    @endif
@stop