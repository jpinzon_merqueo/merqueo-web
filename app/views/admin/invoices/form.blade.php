@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb">
            @if($action == 'update')
            <a href="{{ route('adminInvoice.printPdf', ['id' => $invoice_id]) }}" target="_blank" class="btn btn-primary">Exportar PDF</a>
            @endif
            <a href="{{ route('adminInvoice.index') }}" class="btn btn-primary"><li class="fa fa-chevron-left"></li> Volver a lista de facturas</a>
        </span>
    </section>
    <section id="contenedor" class="content">
        @include('admin.invoices.message')
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <form :action="routeAction" method="POST" @submit="sendInvoice">
                        <div class="box-body">
                            <div class="row form-group">
                                <div class="col-xs-6 col-md-6 col-lg-6">
                                    <label for="number">Num. factura</label>
                                    <input type="text" name="number" v-model="formData.number" class="form-control" placeholder="Numero de factura (Se genera automaticamente)" disabled>
                                </div>
                                <div class="col-xs-6 col-md-6 col-lg-6">
                                    <label for="customer_invoice_id">Cliente</label>
                                    <select name="customer_invoice_id" v-model="formData.customer_invoice_id" v-validate="'required'" data-vv-scope="form" data-vv-as="cliente" class="form-control" :disabled="action == 'update'">
                                        <option value>Seleccione</option>
                                        <option v-for="customer in customerInvoices" :value="customer.id">
                                            @{{ customer.document_number + ' - ' + customer.fullname }}
                                        </option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('form.customer_invoice_id')">@{{ errors.first('form.customer_invoice_id') }}</span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-6 col-md-6 col-lg-6">
                                    <label for="date">Fecha comprobante</label>
                                    <input type="date" name="date" v-model="formData.date" v-validate="'required'" data-vv-scope="form" data-vv-as="fecha facturación" class="form-control" :disabled="action == 'update'">
                                    <span style="color: #d73925" v-show="errors.has('form.date')">@{{ errors.first('form.date') }}</span>
                            </div>
                                <div class="col-xs-6 col-md-6 col-lg-6">
                                    <label for="expiration_date">Fecha vencimiento</label>
                                    <input type="date" name="expiration_date" v-validate="'required'" data-vv-scope="form" data-vv-as="fecha vencimiento" v-model="formData.expiration_date" class="form-control" :disabled="action == 'update'">
                                    <span style="color: #d73925" v-show="errors.has('form.expiration_date')">@{{ errors.first('form.expiration_date') }}</span>
                                </div>
                            </div>
                            <div class="row form-group" v-if="action != 'update'">
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <h4>Agregar producto</h4>
                                    <table class="table table-bordered" style="margin-top: 20px; margin-bottom: 20px;">
                                        <tr>
                                            <th>Producto</th>
                                            <th>Descripción</th>
                                            <th>Cantidad</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select name="product_id" v-model="formData.product.id" v-validate="'required'" data-vv-scope="table" data-vv-as="producto" class="form-control">
                                                    <option value>Seleccione</option>
                                                    <option v-for="product in products" :value="product.id">@{{ product.name }}</option>
                                                </select>
                                                <span style="color: #d73925" v-show="errors.has('table.product_id')">@{{ errors.first('table.product_id') }}</span>
                                            </td>
                                            <td>
                                                <input type="text" name="description" v-model="formData.product.description" v-validate="'required'" data-vv-scope="table" data-vv-as="descripción" placeholder="Descripción" class="form-control">
                                                <span style="color: #d73925" v-show="errors.has('table.description')">@{{ errors.first('table.description') }}</span>
                                            </td>
                                            <td>
                                                <input type="number" name="quantity" @change="taxAmountGrid" v-model="formData.product.quantity" v-validate="'required|numeric|min_value:1'" data-vv-scope="table" data-vv-as="cantidad" placeholder="Cantidad" class="form-control">
                                                <span style="color: #d73925" v-show="errors.has('table.quantity')">@{{ errors.first('table.quantity') }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Valor neto</th>
                                            <th>IVA %</th>
                                            <th>Valor iva</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-dollar"></li></span>
                                                    <input type="number" name="price" @change="taxAmountGrid" v-model="formData.product.price" v-validate="'required|min_value:0'" data-vv-scope="table" data-vv-as="valor neto" placeholder="Valor neto" class="form-control">
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.price')">@{{ errors.first('table.price') }}</span>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-percent"></li></span>
                                                    <select name="iva" @change="taxAmountGrid" v-model="formData.product.iva" v-validate="'required'" data-vv-scope="table" placeholder="Iva" class="form-control">
                                                        <option value="0">0</option>
                                                        <option value="5">5</option>
                                                        <option value="19">19</option>
                                                    </select>
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.iva')">@{{ errors.first('table.iva') }}</span>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-dollar"></li></span>
                                                    <input type="number" name="price_iva" v-model="iva_amount" v-validate="'required|min_value:0'" data-vv-scope="table" data-vv-as="valor iva" placeholder="Valor iva" disabled class="form-control">
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.price_iva')">@{{ errors.first('table.price_iva') }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Rete fuente</th>
                                            <th>Rete ica</th>
                                            <th>Rete iva</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-dollar"></li></span>
                                                    <input type="number" name="price_tax_fuente" v-model="formData.product.price_tax_fuente" v-validate="'required|min_value:0'" data-vv-scope="table" data-vv-as="rete fuente" placeholder="Valor rete fuente" class="form-control">
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.price_tax_fuente')">@{{ errors.first('table.price_tax_fuente') }}</span>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-dollar"></li></span>
                                                    <input type="number" name="price_tax_ica" v-model="formData.product.price_tax_ica" v-validate="'required|min_value:0'" data-vv-scope="table" data-vv-as="rete ica" placeholder="Valor rete ica" class="form-control">
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.price_tax_ica')">@{{ errors.first('table.price_tax_ica') }}</span>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-dollar"></li></span>
                                                    <input type="number" name="price_tax_iva" v-model="formData.product.price_tax_iva" v-validate="'required|min_value:0'" data-vv-scope="table" data-vv-as="rete iva" placeholder="Valor rete iva" class="form-control">
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.price_tax_iva')">@{{ errors.first('table.price_tax_iva') }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><li class="fa fa-dollar"></li></span>
                                                    <input type="number" name="total_price" v-model="total_price" v-validate="'required|min_value:0'" data-vv-scope="table" data-vv-as="total" placeholder="Total" disabled class="form-control">
                                                </div>
                                                <span style="color: #d73925" v-show="errors.has('table.total_price')">@{{ errors.first('table.total_price') }}</span>
                                            </td>
                                            <td colspan="2">
                                                <button type="button" @click="addProductGrid(formData.product)" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Agregar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-group" v-if="productsToInvoice.length > 0 && action != 'update'">
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <h4>Productos para agregar a la factura</h4>
                                    <table class="table table-bordered table-striped" style="margin-top: 20px; margin-bottom: 20px;">
                                        <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Descripción</th>
                                            <th>Cantidad</th>
                                            <th>Valor neto</th>
                                            <th>IVA %</th>
                                            <th>Valor iva</th>
                                            <th>Rete fuente</th>
                                            <th>Rete ica</th>
                                            <th>Rete iva</th>
                                            <th>Total</th>
                                            <th>Eliminar</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(product_to_invoice, index) in productsToInvoice">
                                            <td>
                                                <select class="form-control" disabled>
                                                    <option v-for="product in products" :value="product.id" :selected="product_to_invoice.id == product.id">@{{ product.name }}</option>
                                                </select>
                                            </td>
                                            <td>
                                                @{{ product_to_invoice.description }}
                                            </td>
                                            <td>
                                                @{{ product_to_invoice.quantity }}
                                            </td>
                                            <td align="right">
                                                $@{{ formatPrice(product_to_invoice.price) }}
                                            </td>
                                            <td>
                                                @{{ product_to_invoice.iva }}%
                                            </td>
                                            <td align="right">
                                                $@{{ formatPrice(product_to_invoice.price_iva) }}
                                            </td>
                                            <td align="right">
                                                $@{{ formatPrice(product_to_invoice.price_tax_fuente) }}
                                            </td>
                                            <td align="right">
                                                $@{{ formatPrice(product_to_invoice.price_tax_ica) }}
                                            </td>
                                            <td align="right">
                                                $@{{ formatPrice(product_to_invoice.price_tax_iva) }}
                                            </td>
                                            <td align="right">
                                                $@{{ formatPrice(product_to_invoice.total_price) }}
                                            </td>
                                            <td>
                                                <button type="button" @click="deleteProductGrid(index)" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus"></span></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-3 col-md-3 col-lg-3">
                                    <label for="payment_conditions">Condiciones de pago</label>
                                    <input type="text" name="payment_conditions" v-model="formData.payment_conditions" v-validate="'required'" data-vv-scope="form" data-vv-as="condiciones de pago" class="form-control" :disabled="action == 'update'">
                                    <span style="color: #d73925" v-show="errors.has('form.payment_conditions')">@{{ errors.first('form.payment_conditions') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="hidden" name="products" v-model="JSON.stringify(productsToInvoice)">
                            <button type="submit" class="btn btn-primary" v-if="action != 'update'">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('admin.invoices.js.form-js')
@endsection