<div class="modal fade" id="modal-credit-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div id="content-credit-note">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Nota crédito <span v-if="creditNoteConsecutive">#@{{ creditNoteConsecutive }}</span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form @submit.prevent="searchProducts">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="modal-title">Buscar Productos</h4>
                                        <table width="100%" class="modal-product-request-table">
                                            <tbody>
                                            <tr>
                                                <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                <td>
                                                    <input type="text" placeholder="Descripción" name="boxFilter" v-model="formData.boxFilter" required class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
                                                </td>
                                                <td colspan="2" align="left"><button type="submit" :disabled="disabledSearch" class="btn btn-primary">Buscar</button></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td align="center" colspan="1"><img src="{{ asset_url() }}/img/loading.gif" v-if="dsearch"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row form-group" v-if="productsSearch.length > 0">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <h4>Productos para agregar a la nota crédito</h4>
                            <table class="table table-bordered table-striped" style="margin-top: 20px; margin-bottom: 20px;">
                                <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Valor neto</th>
                                    <th>IVA %</th>
                                    <th>Valor iva</th>
                                    <th>Rete fuente</th>
                                    <th>Rete ica</th>
                                    <th>Rete iva</th>
                                    <th>Total</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(product, index) in productsSearch">
                                    <td>
                                       @{{ product.product_invoice.name }}
                                    </td>
                                    <td>
                                        @{{ product.description }}
                                    </td>
                                    <td>
                                        @{{ product.quantity }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price) }}
                                    </td>
                                    <td>
                                        @{{ product.iva }}%
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_iva) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_fuente) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_ica) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_iva) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.total_price) }}
                                    </td>
                                    <td>
                                        <button type="button" @click="addProductGrid(index)" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus"></span></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row form-group" v-if="productsToCreditNote.length > 0">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <h4>Productos para agregar a la nota crédito</h4>
                            <table class="table table-bordered table-striped" style="margin-top: 20px; margin-bottom: 20px;">
                                <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Valor neto</th>
                                    <th>IVA %</th>
                                    <th>Valor iva</th>
                                    <th>Rete fuente</th>
                                    <th>Rete ica</th>
                                    <th>Rete iva</th>
                                    <th>Total</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(product, index) in productsToCreditNote">
                                    <td>
                                        <select class="form-control" disabled>
                                            <option v-for="product in products" :value="product.id" :selected="product.id == product.id">@{{ product.name }}</option>
                                        </select>
                                    </td>
                                    <td>
                                        @{{ product.description }}
                                    </td>
                                    <td>
                                        @{{ product.quantity }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price) }}
                                    </td>
                                    <td>
                                        @{{ product.iva }}%
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_iva) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_fuente) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_ica) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_iva) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.total_price) }}
                                    </td>
                                    <td>
                                        <button type="button" @click="deleteProductGrid(index)" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus"></span></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row form-group" v-if="productsCreditNote.length > 0">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <h4>Productos en la nota crédito</h4>
                            <table class="table table-bordered table-striped" style="margin-top: 20px; margin-bottom: 20px;">
                                <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Valor neto</th>
                                    <th>IVA %</th>
                                    <th>Valor iva</th>
                                    <th>Rete fuente</th>
                                    <th>Rete ica</th>
                                    <th>Rete iva</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(product, index) in productsCreditNote">
                                    <td>
                                        @{{ product.product_invoice.name }}
                                    </td>
                                    <td>
                                        @{{ product.description }}
                                    </td>
                                    <td>
                                        @{{ product.quantity }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price) }}
                                    </td>
                                    <td>
                                        @{{ product.iva }}%
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_iva) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_fuente) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_ica) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.price_tax_iva) }}
                                    </td>
                                    <td align="right">
                                        $@{{ formatPrice(product.total_price) }}
                                    </td>
                                </tr>
                                <tr v-if="subTotal > 0">
                                    <td colspan="9" align="right" style="font-weight: bold">Subtotal: </td>
                                    <td align="right">$@{{ formatPrice(subTotal) }}</td>
                                </tr>
                                <tr v-if="totalIva > 0">
                                    <td colspan="9" align="right" style="font-weight: bold">Total iva: </td>
                                    <td align="right">$@{{ formatPrice(totalIva) }}</td>
                                </tr>
                                <tr v-if="totalTaxFuente > 0">
                                    <td colspan="9" align="right" style="font-weight: bold">Total rete fuente: </td>
                                    <td align="right">$@{{ formatPrice(totalTaxFuente) }}</td>
                                </tr>
                                <tr v-if="totalTaxica > 0">
                                    <td colspan="9" align="right" style="font-weight: bold">Total rete ica: </td>
                                    <td align="right">$@{{ formatPrice(totalTaxica) }}</td>
                                </tr>
                                <tr v-if="totalTaxIva > 0">
                                    <td colspan="9" align="right" style="font-weight: bold">Total rete iva: </td>
                                    <td align="right">$@{{ formatPrice(totalTaxIva) }}</td>
                                </tr>
                                <tr v-if="totalAmount > 0">
                                    <td colspan="9" align="right" style="font-weight: bold">Total: </td>
                                    <td align="right">$@{{ formatPrice(totalAmount) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <form @submit.prevent="updateProductsCreditNote">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" :disabled="disableSave" class="btn btn-primary">Guardar</button>
                        <img src="{{ asset_url() }}/img/loading.gif" v-if="loader">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('admin.invoices.js.credit-note-js')