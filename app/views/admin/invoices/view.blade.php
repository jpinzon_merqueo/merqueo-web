@extends('admin.layout')

@section('content')
    <style type="text/css">
        #modal-credit-note .modal-dialog{
            width: 70% !important;
        }
    </style>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb">
            <a href="{{ route('adminInvoice.printPdf', ['id' => $invoice_id]) }}" target="_blank" class="btn btn-primary">Exportar PDF</a>
           {{-- <a href="#" onclick="show_modal('credit-note')">
                <button type="button" class="btn btn-primary">Agregar Nota Crédito</button>
            </a>--}}
            <a href="{{ route('adminInvoice.index') }}" class="btn btn-primary"><li class="fa fa-chevron-left"></li> Volver a lista de facturas</a>
        </span>
    </section>
    <section id="contenedor" class="content">
        @include('admin.invoices.message')
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Recibo</h4>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-6 form-group">
                            <p><label>Número de factura</label> - @{{ viewData.number }}</p>
                            <p><label>Fecha de comprobante</label> - @{{ viewData.date }}</p>
                            <p><label>Fecha de vencimiento</label> - @{{ viewData.expiration_date }}</p>
                            <p><label>Total: </label> - $@{{ formatPrice(totalAmount) }}</p>
                            <p><label>Condiciones de pago</label> - @{{ viewData.payment_conditions }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Cliente</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-6 form-group">
                            <p><label>Cliente</label> - @{{ viewData.customer.fullname }}</p>
                            <p><label>@{{ viewData.customer.document_type }}</label> - @{{ viewData.customer.document_number }}</p>
                            <p><label>Teléfono</label> - @{{ viewData.customer.phone }}</p>
                            <p><label>Dirección</label> - @{{ viewData.customer.address }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row form-group">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <h4>Productos en la factura</h4>
                                <table class="table table-bordered table-striped" style="margin-top: 20px; margin-bottom: 20px;">
                                    <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Descripción</th>
                                        <th>Cantidad</th>
                                        <th>Valor neto</th>
                                        <th>IVA %</th>
                                        <th>Valor iva</th>
                                        <th>Rete fuente</th>
                                        <th>Rete ica</th>
                                        <th>Rete iva</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(product_invoice, index) in invoice.invoice_details">
                                        <td>
                                            @{{ product_invoice.product_invoice.name }}
                                        </td>
                                        <td>
                                            @{{ product_invoice.description }}
                                        </td>
                                        <td>
                                            @{{ product_invoice.quantity }}
                                        </td>
                                        <td align="right">
                                            $@{{ formatPrice(product_invoice.price) }}
                                        </td>
                                        <td>
                                            @{{ product_invoice.iva }}%
                                        </td>
                                        <td align="right">
                                            $@{{ formatPrice(product_invoice.price_iva) }}
                                        </td>
                                        <td align="right">
                                            $@{{ formatPrice(product_invoice.price_tax_fuente) }}
                                        </td>
                                        <td align="right">
                                            $@{{ formatPrice(product_invoice.price_tax_ica) }}
                                        </td>
                                        <td align="right">
                                            $@{{ formatPrice(product_invoice.price_tax_iva) }}
                                        </td>
                                        <td align="right">
                                            $@{{ formatPrice(product_invoice.total_price) }}
                                        </td>
                                    </tr>
                                    <tr v-if="subTotal > 0">
                                        <td colspan="9" align="right" style="font-weight: bold">Subtotal: </td>
                                        <td align="right">$@{{ formatPrice(subTotal) }}</td>
                                    </tr>
                                    <tr v-if="totalIva > 0">
                                        <td colspan="9" align="right" style="font-weight: bold">Total iva: </td>
                                        <td align="right">$@{{ formatPrice(totalIva) }}</td>
                                    </tr>
                                    <tr v-if="totalTaxFuente > 0">
                                        <td colspan="9" align="right" style="font-weight: bold">Total rete fuente: </td>
                                        <td align="right">$@{{ formatPrice(totalTaxFuente) }}</td>
                                    </tr>
                                    <tr v-if="totalTaxica > 0">
                                        <td colspan="9" align="right" style="font-weight: bold">Total rete ica: </td>
                                        <td align="right">$@{{ formatPrice(totalTaxica) }}</td>
                                    </tr>
                                    <tr v-if="totalTaxIva > 0">
                                        <td colspan="9" align="right" style="font-weight: bold">Total rete iva: </td>
                                        <td align="right">$@{{ formatPrice(totalTaxIva) }}</td>
                                    </tr>
                                    <tr v-if="totalAmount > 0">
                                        <td colspan="9" align="right" style="font-weight: bold">Total: </td>
                                        <td align="right">$@{{ formatPrice(totalAmount) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.invoices.js.view-js')
    {{--@include('admin.invoices.credit-note')--}}
@endsection