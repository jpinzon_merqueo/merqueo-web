<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vee-validate@latest"></script>
<script type="text/javascript">
    function show_modal(action) {
        $('#modal-' + action).modal('show')
    }
    new Vue({
        el: '#contenedor',
        data: {
            viewData: {},
            invoice: {{ $invoice }},
            customerInvoices: {{ $customerInvoices }},
            products: {{ $products }},
            productsInvoice: [],
            subTotal: 0,
            totalIva: 0,
            totalTaxFuente: 0,
            totalTaxica: 0,
            totalTaxIva: 0,
            totalAmount: 0,
            loading: false,
        },
        created() {
            this.setVars()
        },
        methods: {
            setVars: function () {
                let route = ''
                let viewData = {
                    number: '',
                    customer: {
                        document_type: '',
                        document_number: '',
                        fullname: '',
                        phone: '',
                        address: ''
                    },
                    date: '',
                    expiration_date: '',
                    product: {
                        id: '',
                        description: '',
                        quantity: 1,
                        price: 0,
                        iva: 19,
                        price_iva: 0,
                        price_tax_fuente: 0,
                        price_tax_ica: 0,
                        price_tax_iva: 0,
                        total_price: 0
                    },
                    payment_conditions: '',
                    status: ''
                }
                let subTotal = 0
                let totalIva = 0
                let totalTaxFuente = 0
                let totalTaxica = 0
                let totalTaxIva = 0
                let totalAmount = 0
                viewData.number = this.invoice.number
                viewData.date = this.invoice.date
                viewData.expiration_date = this.invoice.expiration_date
                viewData.payment_conditions = this.invoice.payment_conditions
                viewData.customer.document_type = this.invoice.customer_invoice.document_type
                viewData.customer.document_number = this.invoice.customer_invoice.document_number
                viewData.customer.fullname = this.invoice.customer_invoice.fullname
                viewData.customer.phone = this.invoice.customer_invoice.phone
                viewData.customer.address = this.invoice.customer_invoice.address
                this.invoice.invoice_details.forEach(function (product) {
                    subTotal += (product.quantity * product.price)
                    totalIva += product.price_iva
                    totalTaxFuente += product.price_tax_fuente
                    totalTaxica += product.price_tax_ica
                    totalTaxIva += product.price_tax_iva
                    product.total_price = (product.quantity * product.price) +
                        product.price_iva -
                        (product.price_tax_fuente + product.price_tax_ica + product.price_tax_iva)
                })
                totalAmount = subTotal + totalIva - (totalTaxFuente + totalTaxica + totalTaxIva)
                this.subTotal = subTotal
                this.totalIva = totalIva
                this.totalTaxFuente = totalTaxFuente
                this.totalTaxica = totalTaxica
                this.totalTaxIva = totalTaxIva
                this.totalAmount = totalAmount
                this.viewData = viewData
            },
            formatPrice(value) {
                let val = (value / 1).toFixed(2).replace('.', ',')
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            }
        },
        computed: {
            total_price: function () {
                let subtotal = (this.viewData.product.quantity * this.viewData.product.price)
                let iva = this.viewData.product.price_iva
                let tax = (this.viewData.product.price_tax_fuente + this.viewData.product.price_tax_ica + this.viewData.product.price_tax_iva)
                this.viewData.product.total_price = subtotal + iva - tax
                return this.viewData.product.total_price
            },
            iva_amount: function () {
                let price_iva = this.viewData.product.quantity * this.viewData.product.price * this.viewData.product.iva / 100
                this.viewData.product.price_iva = price_iva
                return this.viewData.product.price_iva
            }
        }
    })
</script>