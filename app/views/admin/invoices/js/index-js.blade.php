<script type="text/javascript">
    new Vue({
        el: '#contenedor',
        data: {
            formData: {
                filter: ''
            },
            invoices: [],
            loading: false
        },
        mounted() {
            this.searchInvoices()
        },
        methods: {
            searchInvoices: function () {
                this.loading = true
                this.invoices = []
                let route = '{{ route('adminInvoice.index') }}'
                let data = {
                    filter: this.formData.filter
                }
                axios({
                    url: route,
                    method: 'POST',
                    headers: {'X-Requested-With': 'XMLHttpRequest'},
                    data: data
                })
                    .then(response => {
                        if (response.status === 200) {
                            this.invoices = response.data.invoices
                            this.addRoutesAction()
                        } else {
                            swal(
                                'Info',
                                'No se encontraron elementos.',
                                'info'
                            )
                        }
                    })
                    .catch(error => {
                        swal(
                            'Oops...',
                            error,
                            'error'
                        )
                    })
                    .then(() => {
                        this.loading = false
                    })
            },
            changeStatus: function (route, number, event) {
                swal({
                    title: "Cuidado!",
                    text: "¿Esta seguro que deseas anular la factura #" + number + "?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'No',
                    confirmButtonText: 'Si'
                })
                    .then((result) => {
                        if (result.value) {
                            window.location = route;
                        }
                    });
            },
            addRoutesAction: function () {
                this.invoices.forEach(function (invoice) {
                    invoice.date = moment(invoice.date.split(' ')[0]).format('DD/MM/YYYY')
                    invoice.expiration_date = moment(invoice.expiration_date.split(' ')[0]).format('DD/MM/YYYY')
                    invoice.route_edit = '{{ route('adminInvoice.index') }}/' + invoice.id + '/view'
                    invoice.route_change_status = '{{ route('adminInvoice.index') }}/' + invoice.id + '/change-status'
                })
            }
        }
    })
</script>