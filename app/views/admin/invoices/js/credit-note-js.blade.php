<script type="text/javascript">
    const messages = {
        _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        excluded: (field) => `El campo ${field} debe ser un valor válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
            if (max) {
                return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
            }

            return `El largo del campo ${field} debe ser ${length}.`;
        },
        max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    }
    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    })
    new Vue({
        el: '#content-credit-note',
        data: {
            formData: {
                boxFilter: ''
            },
            productsSearch: [],
            addProductsToCreditNote: [],
            productsToCreditNote: [],
            creditNoteConsecutive: '{{ $creditNoteConsecutive }}',
            productsCreditNote: {{ $productsCreditNote }},
            subTotal: 0,
            totalIva: 0,
            totalTaxFuente: 0,
            totalTaxica: 0,
            totalTaxIva: 0,
            totalAmount: 0,
            loading: false,
            dsearch: false,
            dsave: false,
            dremove: false,
            load: false,
            routeAction: ''
        },
        created() {
            this.setVars()
        },
        methods: {
            searchProducts: function () {
                let description = this.formData.boxFilter
                let excludeIds = this.productsAddedIds
                this.dsearch = true
                axios.get('{{ route('adminInvoice.findInvoiceDetails', ['invoice_id' => $invoice_id]) }}', {
                    params: {
                        boxFilter: description,
                        excludeIds: JSON.stringify(excludeIds)
                    }
                })
                    .then(response => {
                        debugger
                        this.productsSearch = response.data.productsSearch
                        //this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                        this.dsearch = false
                    }).catch(error => {
                    alert('Error al traer el producto buscado')
                    this.dsearch = true
                })
            },
            addProductGrid: function (product) {
                this.$validator.validate('table.*').then((result) => {
                    if (result) {
                        this.productsToCreditNote.push(Object.assign({}, product))
                        //this.resetTableFormProduct()
                    }
                })
            },
            deleteProductGrid: function (index) {
                this.productsToCreditNote.splice(index, 1)
            },
            updateProductsCreditNote: function () {
                swal({
                    title: '¿Estas seguro?',
                    text: "¿Estas seguro que deseas Agregar los productos a la nota crédito?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Agregar'
                }).then((result) => {
                    if (result.value) {
                        this.dsave = true
                        this.load = true
                        let route = '{{ route('adminInvoice.updateCreditNote', ['id' => $invoice_id]) }}'
                        let data = {
                            products: this.productsToCreditNote
                        }
                        axios({
                            url: route,
                            method: 'POST',
                            data: data
                        })
                            .then(response => {
                                this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                                this.addProductsToCreditNote = response.data.addProductsToCreditNote
                                swal(
                                    '¡Hecho!',
                                    response.data.message,
                                    'success'
                                )
                                this.dsave = false
                                this.load = false
                            })
                            .catch(error => {
                                alert('Error al guardar productos en la nota crédito.')
                                this.dsave = false
                                this.load = false
                            })
                    }
                })
            },
            deleteProductCreditNote: function (id, index) {
                swal({
                    title: '¿Estas seguro?',
                    text: "¿Estas seguro que deseas eliminar el producto de la nota crédito?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Eliminar'
                }).then((result) => {
                    if (result.value) {
                        this.dremove = true
                        let route = '{{ route('adminInvoice.index') }}/' + id + '/delete-product-credit-note'
                        axios({
                            url: route,
                            method: 'POST'
                        }).then(response => {
                            if (response.data.status == 200) {
                                this.productsCreditNote.splice(index, 1)
                                this.setVars()
                                swal(
                                    '¡Hecho!',
                                    response.data.message,
                                    'success'
                                )
                            } else {
                                swal(
                                    '¡Error!',
                                    'No se puede eliminar el producto de la nota crédito, intenta más tarde.',
                                    'error'
                                )
                            }
                        })
                            .catch(error => {
                                swal(
                                    '¡Error!',
                                    'No se puede eliminar el producto de la nota crédito, intenta mas tarde.',
                                    'error'
                                )
                            })
                            .then(() => {
                                this.dremove = false
                            })
                    }
                })
            },
            resetTableFormProduct: function () {
                let resetProduct = {
                    id: '',
                    description: '',
                    quantity: 1,
                    price: 0,
                    iva: 19,
                    price_iva: 0,
                    price_tax_fuente: 0,
                    price_tax_ica: 0,
                    price_tax_iva: 0,
                    total_price: 0
                }
                this.formData.product = resetProduct
                this.$validator.reset('table.*')
            },
            sendInvoice: function (event) {
                this.$validator.validate('form.*').then((result) => {
                    if (!result) {
                        event.preventDefault()
                        return false
                    }
                })
            },
            taxAmountGrid: function () {
                this.formData.product.price_tax_fuente = this.formData.product.quantity * this.formData.product.price * 4 / 100
                this.formData.product.price_tax_ica = this.formData.product.quantity * this.formData.product.price * 9.66 / 1000
                this.formData.product.price_tax_iva = this.formData.product.quantity * this.formData.product.price * 15 / 100
                return true
            },
            setVars: function () {
                let formData = {
                    boxFilter: ''
                }
                let subTotal = 0
                let totalIva = 0
                let totalTaxFuente = 0
                let totalTaxica = 0
                let totalTaxIva = 0
                let totalAmount = 0
                this.productsCreditNote.forEach(function (product) {
                    subTotal += (product.quantity * product.price)
                    totalIva += product.price_iva
                    totalTaxFuente += product.price_tax_fuente
                    totalTaxica += product.price_tax_ica
                    totalTaxIva += product.price_tax_iva
                    product.total_price = (product.quantity * product.price) +
                        product.price_iva -
                        (product.price_tax_fuente + product.price_tax_ica + product.price_tax_iva)
                })
                totalAmount = subTotal + totalIva - (totalTaxFuente + totalTaxica + totalTaxIva)
                this.subTotal = subTotal
                this.totalIva = totalIva
                this.totalTaxFuente = totalTaxFuente
                this.totalTaxica = totalTaxica
                this.totalTaxIva = totalTaxIva
                this.totalAmount = totalAmount
                this.formData = formData
            },
            formatPrice(value) {
                let val = (value / 1).toFixed(2).replace('.', ',')
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            }
        },
        computed: {
            productsAddedIds: function () {
                let addProductsCreditNote = this.addProductsToCreditNote.map(product => {return product.id})
                let productsCredditNote = this.productsCreditNote.map(product => {return product.id})
                return productsCredditNote.concat(addProductsCreditNote)
            },
            total_price: function () {
                let subtotal = (this.formData.product.quantity * this.formData.product.price)
                let iva = this.formData.product.price_iva
                let tax = (this.formData.product.price_tax_fuente + this.formData.product.price_tax_ica + this.formData.product.price_tax_iva)
                this.formData.product.total_price = subtotal + iva - tax
                return this.formData.product.total_price
            },
            iva_amount: function () {
                let price_iva = this.formData.product.quantity * this.formData.product.price * this.formData.product.iva / 100
                this.formData.product.price_iva = price_iva
                return this.formData.product.price_iva
            },
            disableSave: function() {
                return (this.productsToCreditNote.length == 0 || this.dsave) ? true : false
            },
            disableRemove: function() {
                return this.dremove
            },
            disabledSearch: function() {
                return this.dsearch
            },
            loader: function () {
                return this.load
            }
        }
    })
</script>