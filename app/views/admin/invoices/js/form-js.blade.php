<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vee-validate@latest"></script>
<script type="text/javascript">
    const messages = {
        _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        excluded: (field) => `El campo ${field} debe ser un valor válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
            if (max) {
                return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
            }

            return `El largo del campo ${field} debe ser ${length}.`;
        },
        max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    }
    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    })
    new Vue({
        el: '#contenedor',
        data: {
            formData: {},
            invoice: {{ $invoice }},
            customerInvoices: {{ $customerInvoices }},
            products: {{ $products }},
            productsToInvoice: [],
            subTotal: 0,
            totalIva: 0,
            totalTaxFuente: 0,
            totalTaxica: 0,
            totalTaxIva: 0,
            totalAmount: 0,
            loading: false,
            dremove: false,
            routeAction: '',
            action: '{{ $action }}'
        },
        created() {
            this.setVars()
        },
        methods: {
            addProductGrid: function (product) {
                this.$validator.validate('table.*').then((result) => {
                    if (result) {
                        this.productsToInvoice.push(Object.assign({}, product))
                        this.resetTableFormProduct()
                    }
                })
            },
            deleteProductGrid: function (index) {
                this.productsToInvoice.splice(index, 1)
            },
            resetTableFormProduct: function () {
                let resetProduct = {
                    id: '',
                    description: '',
                    quantity: 1,
                    price: 0,
                    iva: 19,
                    price_iva: 0,
                    price_tax_fuente: 0,
                    price_tax_ica: 0,
                    price_tax_iva: 0,
                    total_price: 0
                }
                this.formData.product = resetProduct
                this.$validator.reset('table.*')
            },
            sendInvoice: function (event) {
                this.$validator.validate('form.*').then((result) => {
                    if (!result) {
                        event.preventDefault()
                        return false
                    }
                })
            },
            taxAmountGrid: function () {
                this.formData.product.price_tax_fuente = this.formData.product.quantity * this.formData.product.price * 4 / 100
                this.formData.product.price_tax_ica = this.formData.product.quantity * this.formData.product.price * 9.66 / 1000
                this.formData.product.price_tax_iva = this.formData.product.quantity * this.formData.product.price_iva * 15 / 100
                return true
            },
            setVars: function () {
                let route = '{{ route('adminInvoice.store') }}'
                let formData = {
                    number: '',
                    customer_invoice_id: '',
                    date: '',
                    expiration_date: '',
                    product: {
                        id: '',
                        description: '',
                        quantity: 1,
                        price: 0,
                        iva: 19,
                        price_iva: 0,
                        price_tax_fuente: 0,
                        price_tax_ica: 0,
                        price_tax_iva: 0,
                        total_price: 0
                    },
                    payment_conditions: ''
                }
                route = '{{ route('adminInvoice.store') }}'
                this.routeAction = route
                this.formData = formData
            },
            formatPrice(value) {
                let val = (value / 1).toFixed(2).replace('.', ',')
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            }
        },
        computed: {
            total_price: function () {
                let subtotal = (this.formData.product.quantity * this.formData.product.price)
                let iva = this.formData.product.price_iva
                let tax = (this.formData.product.price_tax_fuente + this.formData.product.price_tax_ica + this.formData.product.price_tax_iva)
                this.formData.product.total_price = subtotal + iva - tax
                return this.formData.product.total_price
            },
            iva_amount: function () {
                let price_iva = this.formData.product.quantity * this.formData.product.price * this.formData.product.iva / 100
                this.formData.product.price_iva = price_iva
                return this.formData.product.price_iva
            }
        }
    })
</script>