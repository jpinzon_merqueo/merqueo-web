@extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb">
            <a href="{{ route('adminInvoice.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva factura</a>
        </span>
    </section>
    <section id="contenedor" class="content">
        @include('admin.invoices.message')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <form @submit.prevent="searchInvoices">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="filter">Buscar</label>
                                    <input type="text" name="filter" v-model="formData.filter" class="form-control" placeholder="# Factura, Nombre Cliente">
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" style="margin-top: 25px;" class="btn btn-primary">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Num. factura</th>
                            <th>Cliente</th>
                            <th>Fecha comprobante</th>
                            <th>Fecha vencimiento</th>
                            <th>Cond. pago</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="invoice in invoices">
                            <td>@{{ invoice.number }}</td>
                            <td>@{{ invoice.fullname }}</td>
                            <td>@{{ invoice.date }}</td>
                            <td>@{{ invoice.expiration_date }}</td>
                            <td>@{{ invoice.payment_conditions }}</td>
                            <td align="center">
                                <span v-if="invoice.status" class="fa fa-check"></span>
                                <span v-if="!invoice.status" class="badge bg-red">Anulada</span>
                            </td>
                            <td>
                                <a :href="invoice.route_edit" class="btn btn-default btn-sm"><li class="fa fa-folder-open"></li> Ver</a>
                                @if ($admin_permissions['update'] && $admin_permissions['permission1'])
                                    <a v-if="invoice.status" @click.prevent="changeStatus(invoice.route_change_status ,invoice.number)" class="btn btn-danger btn-sm"><li class="fa fa-ban"></li> Anular</a>
                                @endif
                            </td>
                        </tr>
                        <tr v-if="invoices.length == 0 || loading">
                            <td colspan="7">
                                <p align="center" v-if="loading"><img src="{{ asset_url() }}/img/loading.gif"></p>
                                <p align="center" v-else>No se encontraron registros.</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    @include('admin.invoices.js.index-js')
@endsection
