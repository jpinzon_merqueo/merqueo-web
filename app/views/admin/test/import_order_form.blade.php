<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Importar pedidos</title>
</head>
<body>
	@if ( Session::has('status') )
		@if ( !Session::get('status') )
			<h4>
				* {{ Session::get('message') }}
			</h4>
		@endif
	@endif
	<div class="error"></div>
	<form action="tester/import-store" method="POST" enctype="multipart/form-data">
		<label for="orders_file">Subir Archivo</label>
		<input type="file" name="orders_file" id="orders_file">
		<button type="submit">Subir</button>
	</form>
</body>
</html>