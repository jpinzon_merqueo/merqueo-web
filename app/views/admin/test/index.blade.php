<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Import shopper branches</title>
</head>
<body>
    @if ( Session::has('error') )
        <h3>{{ Session::get('error') }}</h3>
    @endif
    <form action="" method="POST" enctype="multipart/form-data">
        <label for="file_import">Archivo</label>
        <input type="file" name="file_import">
        <br>
        <br>
        <input type="submit" value="Cargar">
    </form>
    @if ( isset($error_items) )
        <table>
            <thead>
                <tr>
                    <th>id</th>
                    <th>store</th>
                    <th>address</th>
                    <th>city</th>
                    <th>supermarket</th>
                </tr>
            </thead>
            <tbody>
                @if ( !empty($error_items) )
                    @foreach ($error_items as $item)
                <tr>
                    <td>{{ $item['id'] }}</td>
                    <td>{{ $item['store'] }}</td>
                    <td>{{ $item['address'] }}</td>
                    <td>{{ $item['city'] }}</td>
                    <td>{{ $item['supermarket'] }}</td>
                </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    @endif
</body>
</html>