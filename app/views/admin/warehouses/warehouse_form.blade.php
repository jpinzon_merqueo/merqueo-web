@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::get('type') == 'error')
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
            <form id="warehouse-form" action="{{ route('adminWarehouses.save') }}" method="POST">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="city_id">Ciudad</label>
                                                    <select name="city_id" id="city_id" class="form-control">
                                                        <option value="">-Selecciona-</option>
                                                        @foreach($cities as $city)
                                                            <option value="{{ $city->id }}" @if(isset($warehouse['city_id'])
                                                             &&
                                                            $warehouse['city_id'] == $city->id) selected="selected"
                                                                    @endif>{{$city->city }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="warehouse">Nombre</label>
                                                    <input type="text" name="warehouse" id="warehouse"
                                                           placeholder="Nombre de la bodega" class="form-control"
                                                           value="{{ $warehouse['warehouse'] }}">
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="orders_limit">Número máximo de pedidos por día</label>
                                                <input type="number" name="orders_limit" id="orders_limit"
                                                       placeholder="Número máximo de ordenes" class="form-control"
                                                       value="{{
                                                       $warehouse['orders_limit'] }}">
                                            </div>
                                        </div>
                                    </div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="address">Dirección</label>
                                                    <input type="text" name="address" id="address" placeholder="Dirección de bodega" class="form-control" value="{{ $warehouse['address'] }}">
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="warehouse">Coordenada</label>
                                                    <input type="text" name="latlng" id="latlng" placeholder="Ej: 6.122646,-75.634480" class="form-control" @if(!empty($warehouse['latitude']) && !empty($warehouse['longitude'])) value="{{$warehouse['latitude'] }},{{$warehouse['longitude'] }}" @endif>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="status">Estado</label>
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="">-Selecciona-</option>
                                                        <option value="1" @if ( $warehouse && $warehouse->status == 1 ) selected="" @endif>Activo</option>
                                                        <option value="0" @if ( $warehouse && $warehouse->status == 0 )
                                                        selected="" @endif>Inactivo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!empty($warehouse))
                    @if(!empty($warehouse->storage_positions))
                        @foreach($warehouse->storage_positions as $key => $storage_position)
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <div class="box-body table-responsive">
                                            <div class="row">
                                                @if(!empty($storage_position))
                                                    @foreach($storage_position as $index => $item)
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <h4>@if($key == 'storage')  Almacenamiento - {{$index}} @else Alistamiento - {{$index}} @endif</h4>
                                                                <div class="row">
                                                                    @foreach($item as $iter => $option)
                                                                            <div class="col-xs-4">
                                                                                <div class="form-group">
                                                                                    <label for="warehouse">
                                                                                        @if($iter == 'start_position')
                                                                                            Posición inicial
                                                                                        @elseif($iter == 'end_position')
                                                                                            Posición final
                                                                                        @elseif($iter == 'height_positions')
                                                                                            Posiciones de altura
                                                                                        @endif
                                                                                    </label>
                                                                                    <input  @if($iter == 'start_position')
                                                                                                type="number" min="0" placeholder="Número mayor a 0"
                                                                                            @elseif($iter == 'end_position')
                                                                                                type="number" min="0" placeholder="Número mayor a 0"
                                                                                            @elseif($iter == 'height_positions')
                                                                                                type="text" placeholder="Ej: A,B,C,D"
                                                                                            @endif
                                                                                           name="storage_positions[{{$key}}][{{$index}}][{{$iter}}]" id="storage_positions[{{$key}}][{{$index}}][{{$iter}}]" class="form-control" value="{{ $option }}">
                                                                                </div>
                                                                            </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                @endif
                @if(!empty($warehouse))
                    <input type="hidden" name="id" value="{{ $warehouse['id'] }}">
                @endif
                <button class="btn btn-primary" type="submit">Guardar</button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <a class="btn btn-primary" href="{{ route('adminWarehouses.config', ['id' =>
                            $warehouse->id]) }}">Configuración adicional</a>
            </form>
            <div class="row">
                <div class="col-xs-12 elem">

                </div>
            </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#warehouse-form').validate({
                rules: {
                    city_id: 'required',
                    warehouse: 'required',
                    status: 'required',
                    orders_limit: 'required',
                    address: 'required',
                    latlng: 'required'
                }
            });
        });
    </script>
@stop
@endsection