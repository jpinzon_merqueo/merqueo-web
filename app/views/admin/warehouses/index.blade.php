@if(!Request::ajax())
    @extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminWarehouses.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Bodega</button>
            </a>
        </span>
    </section>
    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="warehouses-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Buscar:</label>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="Nombre de bodega" id="input-search" class="form-control">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        var web_url_ajax = "{{ route('adminWarehouses.index') }}";
        $(document).ready(function() {
            paging(1, '');

            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });
        });
    </script>
    @else
@section('content')
    <br />
    <table id="warehouses-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Ciudad</th>
            <th>Nombre</th>
            <th>Número máximo de pedidos por día</th>
            <th>Estado</th>
            <th>Editar</th>
        </tr>
        </thead>
        <tbody>
        @if(count($warehouses))
            @foreach($warehouses as $warehouse)
                <tr>
                    <td>{{ $warehouse->city }}</td>
                    <td>{{ $warehouse->warehouse }}</td>
                    <td align="right">{{ $warehouse->orders_limit }}</td>
                    <td>
                        @if($warehouse['status'] == 1)
                            <span class="label label-success">
                            Activa
                        </span>
                        @else
                            <span class="label label-danger">
                            Inactiva
                        </span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-xs btn-danger" href="{{ route('adminWarehouses.edit', ['id' =>
                            $warehouse->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr><td colspan="9" align="center">No hay bodegas</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($warehouses) }} registros</div>
        </div>
    </div>
    @endif
@stop