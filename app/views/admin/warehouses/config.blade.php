@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Configuración
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::get('type') == 'error')
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
        <form id="warehouse-form" action="{{ route('adminWarehouses.saveConfig') }}" method="POST">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <h4>Bodega: {{$warehouse->warehouse}}</h4>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="col-md-4">
                                                <div class="col-md-6">Alistar pedididos sin planeación</div>
                                                <div class="col-md-6">
                                                    <select id="withoutPlanning" name="withoutPlanning">
                                                        <option value="1"
                                                                @if ($warehouse->without_planning === 1) selected @endif >
                                                            Si
                                                        </option>
                                                        <option value="0"
                                                                @if ($warehouse->without_planning === 0) selected @endif>
                                                            No
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-6">Franja</div>
                                                <div class="col-md-6">
                                                    <select id="shift_planning" name="shift_planning">
                                                        <option value="AM"
                                                                @if ($warehouse->shift_planning === 'AM') selected @endif >
                                                            AM
                                                        </option>
                                                        <option value="MD"
                                                                @if ($warehouse->shift_planning === 'MD') selected @endif>
                                                            MD
                                                        </option>
                                                        <option value="PM"
                                                                @if ($warehouse->shift_planning === 'PM') selected @endif>
                                                            PM
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-6">Utilizar packing</div>
                                                <div class="col-md-6">
                                                    <select id="usePacking" name="usePacking">
                                                        <option value="1"
                                                                @if ($warehouse->use_packing === 1) selected @endif>Si
                                                        </option>
                                                        <option value="0"
                                                                @if ($warehouse->use_packing === 0) selected @endif>No
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="{{ $warehouse->id }}">
            <button class="btn btn-primary" type="submit">Guardar</button>
            <a class="btn btn-primary" href="{{route('adminWarehouses.generatePdfLogout',$warehouse->id )}}" target="_blank">Generar pdf logout picking</a>

        </form>
        <div class="row">
            <div class="col-xs-12 elem">

            </div>
        </div>
    </section>
@stop
@endsection