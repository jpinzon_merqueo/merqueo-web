@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Información del Vehículo</h3>
                </div>
                <div class="box-body">
                    <input type="hidden" name="id" id="id" value="{{ $vehicle->id }}">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-3">
                            <label>Transportador</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->fullname }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Identificación</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->document_number }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Placa</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->plate }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Teléfono celular</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $vehicle->phone }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                @if ($admin_permissions['insert'])
                <div class="box-header">
                    <h3 class="box-title">Registrar Movimiento</h3>
                </div>
                <div class="alert alert-success alert-dismissable unseen"></div>
                <div class="alert alert-danger alert-dismissable unseen"></div>
                <div class="box-body">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-3">
                            <label>Saldo actual del vehiculo</label>
                            <br><span class="badge @if ($balance >= 0) bg-green @else bg-red @endif balance-shopper" style="font-size: 20px;">${{ number_format($balance, 0, ',', '.') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <button type="button" class="btn btn-primary export-movement">Exportar movimientos</button>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label>Tipo movimiento</label>
                            <select class="form-control" name="movement" id="movement">
                                <option value="Deducción">Deducción</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <label>Valor</label>
                            <input type="text" class="form-control" name="amount" id="amount">
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <label>Referencia</label>
                            <input type="text"  class="form-control" name="reference" id="reference" required="">
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <label>Observación</label>
                            <input type="text" class="form-control" name="description" id="description">
                        </div>
                        <div class="col-xs-12 col-sm-2"><br>
                            <button type="button" id="btn-save-recharge" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
                @endif
                <div class="box-header">
                    <h3 class="box-title">Movimientos</h3>
                </div>
                <div class="box-body">
                    <div class="container-overflow" style="min-height: 400px">
                        <div class="movements">{{ $movements_html }}</div>
                        <div align="center" class="loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
var hide_alerts = function(){
    $('.alert-danger').html('').hide();
    $('.alert-success').html('').hide();
}

$(document).ready(function(){

    $('#btn-save-recharge').click(function () {

        var vehicle_id = $('#id').val();
        var amount = $('#amount').val();
        var movement = $('#movement').val();
        var description = $('#description').val();
        var reference = $('#reference').val();

        if (amount === ''){
            alert('El valor es requerido.');
            return false;
        }

        /*if( isNaN(reference) ){
            alert('El valor debe ser númerico, diferente de cero y sin puntos');
            return false;
        }*/
        if (isNaN(amount)){
            alert('El valor debe ser númerico, diferente de cero y sin puntos');
            return false;
        }

        $('.movements').hide();
        $('.loading').show();
        $(this).addClass('disabled');
        $.ajax({ url: "{{ route('adminVehiclesBalance.saveMovement') }}",
            data: { amount: amount, vehicle_id: vehicle_id, movement: movement, description: description, reference: reference },
            type: 'post',
            dataType: 'json',
            success:
                function(response) {
                    $('.loading').hide();
                    $('#btn-save-recharge').removeClass('disabled');
                    $('#amount').val('');
                    $('#description').val('');
                    $('#reference').val('');
                    if (response.balance >= 0)
                        $('.balance-shopper').removeClass('bg-red').addClass('bg-green').html(numberWithCommas(response.balance));
                    else if (response.balance < 0)
                        $('.balance-shopper').removeClass('bg-green').addClass('bg-red').html(numberWithCommas(response.balance));

                    $('.movements').html(response.html).show();
                    if (!response.status)
                        $('.alert-danger').html(response.message).show();
                    else $('.alert-success').html(response.message).show();

                    setTimeout( hide_alerts, 10000 );
                }
        });
    });

    $('body').on('click', '.movement-remove-item', function() {

        if (confirm('¿Estas seguro que deseas eliminar este movimiento?'))
        {
            var id = $(this).attr('data-id');
            var order_id = $(this).data('order_id');
            var vehicle_id = $('#id').val();
            var movement = $(this).data('movement');

            $('.movements').hide();
            $('.loading').show();
            $(this).addClass('disabled');
            $.ajax({ url: "{{ route('adminVehiclesBalance.deleteMovement') }}",
                data: { id: id, vehicle_id: vehicle_id, order_id: order_id, movement: movement },
                type: 'post',
                dataType: 'json',
                success:
                    function(response) {
                        $('.loading').hide();
                        if (response.balance >= 0)
                            $('.balance-shopper').removeClass('bg-red').addClass('bg-green').html(numberWithCommas(response.balance));
                        else if(response.balance < 0)
                            $('.balance-shopper').removeClass('bg-green').addClass('bg-red').html(numberWithCommas(response.balance));

                        $('.movements').html(response.html).show();
                        if (!response.status)
                            $('.alert-danger').html(response.message).show();
                        else $('.alert-success').html(response.message).show();

                        setTimeout( hide_alerts, 10000 );
                    }
            });
        }
    });

    $('body').on('click','.export-movement', function(){
        var vehicle_id = $('#id').val();
        $(this).addClass('disabled');
        $.ajax({ url: "{{ route('adminVehiclesBalance.exportMovement') }}",
            data: { vehicle_id: vehicle_id},
            type: 'post',
            dataType: 'json',
            success:
                function(response) {
                    if(response.status) {
                        $('.loading').hide();
                        $('.export-movement').removeClass('disabled');

                        if(response.status){
                            window.open(response.file_url,'_blank');
                        }else{
                            console.error('error al consultar los movimientos del conductor.');
                        }
                    } else {
                        $('.export-movement').removeClass('disabled');
                        alert('Este vehículo no cuenta con movimientos');
                    }

                }
        });
    });

});
</script>

@stop