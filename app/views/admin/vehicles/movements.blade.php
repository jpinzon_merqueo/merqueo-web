<table id="shoppers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Tipo movimiento</th>
            <th>Pedido</th>
            <th>Método de pago Cliente</th>
            <th>Total pedido</th>
            <th>Total pagado tarjeta de crédito</th>
            <th>Total pagado datáfono</th>
            <th>Saldo vehículo</th>
            <th>Referencia</th>
            <th>Observación</th>
            <th>Fecha</th>
            <th>Hecho por</th>
            <th>Estado</th>
            @if ($admin_permissions['delete'])
            <th>Eliminar</th>
            @endif
        </tr>
    </thead>
    <tbody>
    @if (count($movements))
        @foreach($movements as $movement)
        <tr>
            <td>{{ $movement->movement }}</td>
            <td>
                @if (!empty($movement->order_id))
                <a href="{{ route('adminOrderStorage.details', ['id' => $movement->order_id]) }}" target="_blank">{{ $movement->order_id }}</a>
                @endif
            </td>
            <td>{{ $movement->user_payment_method }}</td>
            <td align="right">${{ number_format($movement->order_total_amount, 0, ',', '.') }}</td>
            <td align="right">@if($movement->user_payment_method == 'Tarjeta de crédito') ${{ number_format($movement->user_card_paid, 0, ',', '.') }} @else $0 @endif</td>
            <td align="right">@if($movement->user_payment_method == 'Datáfono' || $movement->user_payment_method == 'Efectivo y datáfono') ${{ number_format($movement->user_card_paid, 0, ',', '.') }} @else $0 @endif</td>
            <td align="right" @if ($movement->driver_balance > 0) style="color: green" @else @if ($movement->driver_balance < 0) style="color: red" @endif @endif><b>
                ${{ number_format($movement->driver_balance, 0, ',', '.') }}</b></td>
            <td>{{ $movement->reference }}</td>
            <td>{{ $movement->description }}</td>
            <td>{{ $movement->date }}</td>
            <td>{{ $movement->user }}</td>
            <td>@if ($movement->status)
                <span class="badge bg-green">Activo</span>
                @else
                <span class="badge bg-red">Inactivo</span>
                @endif
            </td>
            @if ($admin_permissions['delete'])
            <td>
                @if ($movement->status)
                <div class="btn-group">
                   <a class="btn btn-xs btn-default movement-remove-item" data-movement="{{ $movement->movement }}" data-id="{{ $movement->id }}" data-order_id="{{ $movement->order_id }}" href="javascript:;"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                @endif
            </td>
            @endif
        </tr>
        @endforeach
    @else
        <tr><td colspan="15" align="center">No hay movimientos.</td></tr>
    @endif
    </tbody>
</table>