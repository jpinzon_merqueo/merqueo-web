@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-primary export-balance pull-right">Exportar Saldos</button>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form" class="balance-table">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td width="33%">
                                            <select id="city_id" name="city_id" class="form-control">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td align="right">
                                            <label>Buscar:</label>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Nombre conductor, referencia movimiento" id="input-search" class="form-control">
                                        </td>
                                        
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>                                    </tr>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <br />
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var web_url_ajax = "{{ route('adminVehiclesBalance.index') }}";

    $(document).ready(function() {
        paging(1, '');

        $('#btn-search').on('click', function(e) {
            paging(1, '');
            e.preventDefault();
        });

        $('.export-balance').on('click', function(e) {
            e.preventDefault();
            //$(this).addClass('disabled');
            $.ajax({ url: "{{ route('adminVehicles.balanceExport') }}",
                type: 'POST',
                dataType: 'json',
                success:
                    function(response) {
                        $('.loading').hide();
                        //$('.export-balance').removeClass('disabled');

                        if(response.status){
                            window.open(response.file_url,'_blank');
                        }else{
                            console.log("error al consultar los movimientos del shopper.");
                        }
                    }
            });
        });
    });
</script>

@else

@section('content')
<table id="balance-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Ciudad</th>
            <th>Transportador</th>
            <th>Identificación</th>
            <th>Celular</th>
            <th>Placa de vehiculo</th>
            <th>Ver saldos</th>
        </tr>
    </thead>
    <tbody>
    @if (count($vehicles))
        @foreach($vehicles as $vehicle)
        <tr>
            <td>{{ $vehicle->city }}</td>
            <td>{{ $vehicle->fullname }}</td>
            <td>{{ $vehicle->document_number }}</td>
            <td>{{ $vehicle->phone }}</td>
            <td>{{ $vehicle->plate }}</td>
            <td>
                <div class="btn-group">
                    <a class="btn btn-xs btn-default" href="{{ route('adminVehiclesBalance.edit', ['id' => $vehicle->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="6" align="center">Conductor no encontrado</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($vehicles) }} registros</div>
    </div>
</div>

@endif
@stop