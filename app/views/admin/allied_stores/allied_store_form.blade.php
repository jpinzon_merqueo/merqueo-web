@extends('admin.layout')
@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Información</b> {{ Session::get('error') }}
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST" id='main-form' action="{{ route('adminAlliedStores.save') }}" enctype="multipart/form-data">
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{ $allied_store ? $allied_store->id : '' }}">
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="name" placeholder="Ingresa nombre" value="{{ $allied_store ? $allied_store->name : '' }}">
                            </div>
                            <div class="col-xs-6">
                                <label>NIT</label>
                                <input type="number" class="form-control" name="nit" placeholder="Ingresa NIT"  value="{{ $allied_store ? $allied_store->nit : '' }}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Teléfono</label>
                                <input type="number" class="form-control" name="phone" placeholder="Ingresa teléfono"  value="{{ $allied_store ? $allied_store->phone : '' }}">
                            </div>
                            <div class="col-xs-6">
                                <label>Ciudad</label>
                                <select class="form-control" id="city_id" name="city_id">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" @if ($allied_store && $allied_store->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Tiempo de entrega en horas</label>
                                <input type="number" class="form-control" name="delivery_time_hours" placeholder="Ingresa tiempo de entrega en horas"  value="{{ $allied_store ? $allied_store->delivery_time_hours : '' }}">
                            </div>
                            <div class="col-xs-6">
                                <label>Transportador que recoge y entrega productos en bodega</label>
                                <select class="form-control" id="transporter" name="transporter">
                                    <option value="Merqueo" @if ($allied_store && $allied_store->transporter == 'Merqueo') selected="selected" @endif>Merqueo</option>
                                    <option value="Proveedor" @if ($allied_store && $allied_store->transporter == 'Proveedor') selected="selected" @endif>Proveedor</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Texto de horario</label>
                                <input type="text" class="form-control" name="schedule" placeholder="Ingresa texto de horario"  value="{{ $allied_store ? $allied_store->schedule : '' }}">
                            </div>
                            <div class="col-xs-6">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Ingresa el correo electrónico" value="{{ $allied_store ? $allied_store->email : '' }}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Dirección</label>
                                <input type="text" class="form-control" name="address" placeholder="Ingresa dirección de la tienda aliada"  value="{{ $allied_store ? $allied_store->address : '' }}">
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Coordenada</label>
                                <input type="text" class="form-control" name="latlng" id="latlng" placeholder="Ej: 6.122646,-75.634480">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Estado</label>
                                <select class="form-control" name="status">
                                    <option value="1" {{ $allied_store && $allied_store->status ? "selected" : ''}}>Activo</option>
                                    <option value="0" {{ $allied_store && !$allied_store->status ? "selected" : ''}}>Inactivo</option>
                                </select>
                            </div>
                            <div class="@if ($allied_store && $allied_store->app_logo_small_url) col-xs-4 @else col-xs-6 @endif">
                                <label>Logo pequeño para app</label>
                                <input type="file" class="form-control" name="app_logo_small" >
                            </div>
                            @if ($allied_store && $allied_store->app_logo_small_url)
                                <div class="col-xs-2">
                                    <img src="{{ $allied_store->app_logo_small_url }}" class="img-responsive" />
                                </div>
                            @endif
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label class="control-label">Proveedor</label>
                                <select name="provider_id" class="form-control">
                                    <option value="">Selecciona</option>
                                    @foreach($providers as $provider)
                                        <option {{ $allied_store && $provider->id === $allied_store->provider_id ? 'selected' : '' }}
                                                value="{{ $provider->id }}">
                                            {{ $provider->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <br />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<script type="text/javascript">
    $("#main-form").validate({
      rules: {
          name: {
              required: true
          },
          nit: {
              required: true
          },
          phone: {
              required: true
          },
          address: {
              required: true
          },
          city_id: {
              required: true
          },
          schedule: {
              required: true
          },
          status: {
              required: true
          },
          delivery_time_hours: {
              required: true,
              number: true
          },
          email: {
              required: true
          },
          provider_id: {
              required: true
          }
      }
    });
</script>
@endsection
