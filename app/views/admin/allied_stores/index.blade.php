@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminAlliedStores.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Tienda Aliada</button>
            </a>
        </span>
    </section>
    <section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Información</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="allied-store-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Buscar:</label>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="Nombre de tienda aliada" id="input-search" class="form-control">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        var web_url_ajax = "{{ route('adminAlliedStores.index') }}";
        $(document).ready(function() {
            paging(1, '');

            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });
        });
    </script>
@else
        @section('content')
            <br />
            <table id="allied-stores-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Ciudad</th>
                        <th>Nombre</th>
                        <th>NIT</th>
                        <th>Teléfono</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($allied_stores))
                    @foreach($allied_stores as $allied_store)
                    <tr>
                        <td>{{ $allied_store->city }}</td>
                        <td>{{ $allied_store->name }}</td>
                        <td>{{ $allied_store->nit }}</td>
                        <td>{{ $allied_store->phone }}</td>
                        <td>@if ($allied_store->status) Activa @else Inactiva @endif</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('adminAlliedStores.edit', ['id' => $allied_store->id]) }}">Editar</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ route('adminAlliedStores.delete', ['id' => $allied_store->id]) }}"  onclick="return confirm('¿Estas seguro que deseas eliminar la tienda aliada?')">Eliminar</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr><td colspan="9" align="center">No hay tiendas aliadas</td></tr>
                @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-xs-3">
                    <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($allied_stores) }} registros</div>
                </div>
            </div>
@endif
@stop