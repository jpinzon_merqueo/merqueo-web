@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <script>
        var web_url_ajax = "{{ route('adminNotifications.index') }}";
    </script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    	<span class="breadcrumb" style="top:0px">
    		<a href="{{ route('adminNotifications.add') }}">
    			<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Notificación</button>
    		</a>
    	</span>
    </section>
    <section class="content">
         @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
         @if(Session::has('success') )
        <div class="alert alert-success">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('success') }}
        </div>
        @endif
         <div class="row">
             <div class="col-xs-12">
                 <div class="box box-primary">
                     <div class="box-body table-responsive">
                         <div class="row">
                             <div class="col-xs-12">
                                 <form id="search-form" class="notifications-table">
                                     <div class="form-group col-xs-3">
                                         <label for="city_id">Ciudad</label>
                                         <select name="city_id" id="city_id" class="form-control">
                                            <option value="">-Selecciona-</option>
                                            @foreach($cities as $city )
                                            <option value="{{ $city->id }}">{{  $city->city }}</option>
                                            @endforeach
                                         </select>
                                     </div>
                                     <div class="form-group col-xs-3">
                                         <label>Buscar</label>
                                         <input type="text" placeholder="Subtitulo, mensaje, campaña" id="s" name="s" class="search form-control">
                                     </div>
                                     <div class="form-group col-xs-3">
                                         <button type="button" class="btn btn-primary search" id="btn-search-notifications" style="margin-top: 24px;">Buscar</button>
                                     </div>
                                 </form>
                             </div>
                         </div>
                         <div class="paging"></div>
                         <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                     </div>
                 </div>
             </div>
         </div>
    </section>

    @include('admin.notifications.preview_modal')

    <script type="text/javascript">
        function paging_notifications(page, search)
        {
            $('.paging').html('');
            $('.paging-loading').show();

            data = {
                city_id: $('#city_id').val(),
                s: $('.search').val(),
                p: 1
            };

            $.ajax({
                url: web_url_ajax,
                data: data,
                type: 'GET',
                dataType: 'html',
                success: function(response) {
                    $('.paging-loading').hide();
                    $('.paging').html(response);
                }, error: function() {
                    $('.paging-loading').hide();
                    $('.paging').html('Ocurrió un error al obtener los datos.');
                }
            });
        }

        $(document).ready(function() {
            paging_notifications(1, $(this).val());

            $('#btn-search-notifications').click(function(){
                paging_notifications(1, '');
            });
        });
    </script>
    @endsection
@else
    @section('content')
    <table id="notifications-table" class="notifications-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Ciudad</th>
                <th>Tipo</th>
                <th>Subtitulo push</th>
                <th>Mensaje push</th>
                <th>Campaña</th>
                <th>Fecha creación</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
        @if (count($notifications))
            @foreach($notifications as $notification)
            <tr>
                <td>{{ $notification->id }}</td>
                <td>{{ $notification->city }}</td>
                <td>{{ $notification->type }}</td>
                <td>{{ $notification->push_subtitle }}</td>
                <td>{{ $notification->push_message }}</td>
                <td>{{ $notification->campaign }}</td>
                <td>{{ date('d M Y h:i a',strtotime($notification->created_at)) }}</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminNotifications.edit', ['id' => $notification->id]) }}">Editar</a></li>
                            <li><a href="javascript:;" onclick="show_modal({{  $notification->id }});">Vista previa</a></li>
                            <li><a href="{{ route('adminNotifications.sendFormNotification', ['id' => $notification->id]) }}">Enviar notificación</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminNotifications.delete', ['id' => $notification->id]) }}"  onclick="return confirm('Are you sure you want to delete it?')">Borrar</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="10" align="center">No se encontraron notificaciones.</td></tr>
        @endif
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($notifications) }} ítems</div>
        </div>
    </div>
    @endsection
@endif