@extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

	<section class="content-header">
		<h1>{{ $title }}<small>Control panel</small></h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b>Hecho!</b> {{ Session::get('success') }}
				</div>
				@endif
				@if(Session::has('error'))
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Alerta!</b> {{ Session::get('error') }}
				</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
					    <div class="row">
                            <div class="col-xs-12">
                                <table class="notifications-table table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <td colspan="6" align="center"><b>Información de Push</b></td>
                                        </tr>
                                        <tr>
                                            <th>Ciudad</th>
                                            <th>Subtitulo</th>
                                            <th>Mensaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $notification->city }}</td>
                                            <td>{{ $notification->push_subtitle }}</td>
                                            <td>{{ $notification->push_message }}</td>
                                        </tr>
                                    </tbody>
                                </table><br>
                            </div>
                        </div>
						@if (!empty($notification->title) && isset($notification->title))
						<div class="row">
							<div class="col-xs-12">
								<table class="notifications-table table table-bordered table-striped">
									<thead>
									    <tr>
                                            <td colspan="6" align="center"><b>Información de InApp</b></td>
                                        </tr>
									    <tr>
									        <th>Título</th>
									        <th>Descripción</th>
									        <th>Términos y condiciones</th>
									        <th>Texto en botón</th>
									        <th>Imagen</th>
									    </tr>
									</thead>
									<tbody>
    								   <tr>
    								        <td>{{ $notification->title }}</td>
    								        <td>{{ $notification->description }}</td>
    								        <td>{{ $notification->terms_conditions }}</td>
    								        <td>{{ $notification->text_button }}</td>
    								        <td><img src="{{ $notification->image_url }}" class="img-responsive" width="70"></td>
    								    </tr>
									</tbody>
								</table><br>
							</div>
						</div>
						@endif
						<div class="row">
							<div class="col-xs-12">
								<form id="send-notification-form" action="{{ route('adminNotifications.sendNotification') }}" method="POST" enctype="multipart/form-data">
								    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="status">¿Cuando quieres enviar la notificación?</label>
                                                <select name="send" id="send" class="form-control">
                                                    <option value="now" selected="selected">Ahora mismo</option>
                                                    <option value="after">Otra fecha y hora</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 unseen">
                                            <div class="form-group">
                                                <label for="status">Fecha de envio de notificación</label>
                                                <input type="text" id="show_after_date" name="show_after_date" class="form-control" placeholder="Ingresa fecha (dd/mm/yyyy hh:mm am)">
                                            </div>
                                        </div>
                                    </div>
									<div class="row">
										<div class="col-xs-6">
											<div class="form-group">
												<label for="status">Enviar notificación a</label>
												<select name="type" id="type" class="form-control">
													<option value="">-Selecciona-</option>
													<option value="segments">Segmento</option>
													<option value="player_ids">Dispositivos</option>
												</select>
											</div>
										</div>
										@if ($notification->type == 'inapp')
										<div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="status">¿Enviar InApp con push incluido?</label>
                                                <select name="send_push" id="send_push" class="form-control">
                                                    <option value="1" selected="selected">Sí</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        @endif
									</div>
									<div class="row onesignal-segment unseen">
										<div class="col-xs-6">
											<div class="form-group">
												<label for="segment">Segmento OneSignal</label>
												<input type="text" name="segment" id="segment" class="form-control" required="required"/>
												<p class="small">Debes escribir el nombre exacto del Segmento como fue creado en One Signal.</p>
											</div>
										</div>
									</div>
									<div class="row onesignal-device unseen">
										<div class="col-xs-6 onesignal-device unseen">
											<div class="form-group">
												<label for="device">Importar dispositivos</label>
												<input type="file" name="device" id="device" class="form-control" required="required"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<div class="form-group">
												<input type="hidden" name="id" value="{{ $notification->id }}">
												<a href="javascript:;">
													<button class="btn btn-success send-notification" type="button">Enviar notificación</button>
												</a>
												@if ($notification->type == 'inapp')
												<a href="javascript:;" onclick="show_modal({{  $notification->id }});">
									            	<button type="button" class="btn btn-primary">Vista previa</button>
									        	</a>
									        	@endif
									        	<a href="{{ route('adminNotifications.edit', ['id' => $notification->id]) }}"
									        		<button type="button" class="btn btn-primary">Volver</button>
									        	</a>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    @if ($notification->type == 'inapp')
	@include('admin.notifications.preview_modal')
	@endif

	<script type="text/javascript">

		$(document).ready(function(){

            $('#show_after_date').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY hh:mm a',
            });

            $('#send').on('change', function(){
                if ($(this).val() == 'now') {
                    $(this).parent().parent().next().slideUp('fast');
                }else{
                    $(this).parent().parent().next().slideDown('fast');
                    $('#date').val('');
                }
            });
            $('#send').trigger('change');

			$('#type').on('change', function(){
				var type_notification = $(this).val();
				if (type_notification == 'player_ids') {
					$('.onesignal-segment').slideUp('fast');
					$('.onesignal-device').slideDown('fast');
				}else{
					if (type_notification == 'segments') {
						$('.onesignal-device').slideUp('fast');
						$('.onesignal-segment').slideDown('fast');
					}else{
						$('.onesignal-device').slideUp('fast');
						$('.onesignal-segment').slideUp('fast');
					}
				}
			});
			$('#type').trigger('change');

			$('.send-notification').on('click', function(){
				var validator = $('#send-notification-form').validate({
									rules: {
										type: 'required',
										show_after_date:{
                                                    required: function (element) {
                                                        if ($("#send").val() == 'after')
                                                            return true;
                                                        else
                                                            return false;
                                                    }
                                                },
										segment:{
													required: function (element) {
														if ($("#type").val() == 'segments')
															return true;
														else
															return false;
													}
												},
										player_ids:{
													required: function (element) {
													    if ($("#type").val() == 'player_ids')
													        return true;
													    else
													        return false;
													}
										}
									}
								});
				if ( validator.form() ) {
					var response = confirm('¿Estas seguro que deseas enviar la notificación?');
					if (response == true)
					    $('#send-notification-form').submit();
					else
					    return false;
				}
			});

		});
	</script>
@endsection