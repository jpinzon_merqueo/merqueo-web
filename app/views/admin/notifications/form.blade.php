@extends('admin.layout')

@section('content')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
	<script src="{{ web_url() }}/admin_asset/js/clipboard.min.js"></script>
	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
		@if(isset($notification))
		<span class="breadcrumb" style="top:0px">
			<a href="{{ route('adminNotifications.sendFormNotification', ['id' => $notification->id]) }}">
            	<button type="button" class="btn btn-primary">Enviar notificación</button>
        	</a>
        	@if ($notification->type == 'inapp')
            <a href="javascript:;" onclick="show_modal({{  $notification->id }});">
                <button type="button" class="btn btn-primary">Vista previa</button>
            </a>
            @endif
		</span>
		@endif
	</section>
	<section class="content">
		<div class="row">
			@if(Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Hecho!</b> {{ Session::get('success') }}
			</div>
			@endif
			@if(Session::has('error'))
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Alerta!</b> {{ Session::get('error') }}
			</div>
			@endif
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<form id="add-notification-form" action="{{ route('adminNotifications.save') }}" method="POST" enctype="multipart/form-data">
									<fieldset>
										<legend>Configuración Push</legend>
										<div class="row">
											<div class="col-xs-3">
												<div class="form-group">
													<label for="city_id">Ciudad</label>
													<select name="city_id" id="city_id" class="form-control">
														@foreach($cities as $city )
														<option value="{{ $city->id }}" @if(isset($notification) && $notification->city_id == $city->id) selected="selected" @endif>{{  $city->city }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-xs-3">
												<div class="form-group">
													<label for="type_push">Tipo</label>
													<select name="type_push" id="type_push" class="form-control">
														<option value="">-Selecciona-</option>
														<option value="inapp" @if(isset($notification) && $notification->type == 'inapp') selected="selected" @endif>InApp</option>
														<option value="push" @if(isset($notification) && $notification->type == 'push') selected="selected" @endif>Push</option>
													</select>
												</div>
											</div>
											<div class="col-xs-3">
												<div class="form-group">
													<label for="title">Campaña</label>
													<input type="text" name="campaign" id="campaign" placeholder="Nombre de la campaña" class="form-control" @if(isset($notification)) value="{{ $notification->campaign }}" @endif>
												</div>
											</div>
										</div>
										<div class="row">
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label>Variables de reemplazo</label>
                                                    <p>Nombre de usuario: @{{ first_name| default: "Ahorrador"}} <a href="javascript:;" data-clipboard-text='@{{ first_name| default: "Ahorrador"}}' class="copy-firstname">Copiar</a></p>
                                                </div>
                                            </div>
                                        </div>
										<div class="row">
										    <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="description">Subtitulo de la notificación</label>
                                                    <textarea name="push_subtitle" id="push_subtitle" class="form-control" @if(!isset($notification) && empty($notification->push_subtitle)) required="required" @endif>@if(isset($notification)){{ $notification->push_subtitle }} @endif</textarea>
                                                </div>
                                            </div>
											<div class="col-xs-8">
												<div class="form-group">
													<label for="description">Mensaje de la notificación</label>
													<textarea name="push_message" id="push_message" class="form-control" @if(!isset($notification) && empty($notification->push_message)) required="required" @endif>@if(isset($notification)){{ $notification->push_message }} @endif</textarea>
												</div>
											</div>
										</div>
									</fieldset>
									<div class="row">&nbsp;</div>
									<fieldset class="inapp unseen">
										<legend>Configuración In App</legend>
										<div class="row">
											<div class="col-xs-4">
												<div class="form-group">
													<label for="title">Título</label>
													<textarea name="title" id="title" class="form-control" required="required">@if(isset($notification)){{ $notification->title }} @endif</textarea>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="form-group">
													<label for="url">Texto en botón</label>
													<input type="text" name="text_button" id="text_button" class="form-control" placeholder="Ingrese el texto del botón" @if(isset($notification)) value="{{ $notification->text_button }}" @endif>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="form-group">
													<label for="url">URL</label>
													<input type="text" name="url" id="url" class="form-control" placeholder="Ingrese la URL" @if(isset($notification)) value="{{ $notification->url }}" @endif>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group">
													<label for="description">Descripción</label>
													<textarea name="description" id="description" class="form-control ckeditor" @if(!isset($notification) && empty($notification->description)) required="required" @endif>@if(isset($notification)){{ $notification->description }} @endif</textarea>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group">
													<label for="terms_conditions">Términos y condiciones</label>
													<textarea name="terms_conditions" id="terms_conditions" class="form-control" @if(!isset($notification) && empty($notification->terms_conditions)) @endif>@if(isset($notification)){{ $notification->terms_conditions }} @endif</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group">
													<label for="image_url">Imagen</label>
													<input type="file" name="image_url" id="image_url" class="form-control" @if(!isset($notification) && empty($notification->image_url)) required="required" @endif>
													<p class="small">La imagen debe ser de 673 x 520 y no debe pesar mas de 1MB</p>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group">
													@if(isset($notification) && !empty($notification->image_url))
														<img src="{{ $notification->image_url }}" style="width: 100px;" class="img-responsive">
													@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group">
													<label for="last_order_day">Mostrar a usuarios con fecha de último pedido mayor a</label>
													<input type="text" id="last_order_date" name="last_order_date" class="form-control notification-date" placeholder="Ingresa fecha (dd/mm/yyyy)" @if(isset($notification) && !empty($notification->last_order_date)) value="{{ date('d/m/Y', strtotime($notification->last_order_date)) }}" @endif>
												</div>
											</div>
											<div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="last_order_day">Mostrar hasta</label>
                                                    <input type="text" id="show_until_date" name="show_until_date" class="form-control notification-date" placeholder="Ingresa fecha (dd/mm/yyyy)" @if(isset($notification) && !empty($notification->show_until_date)) value="{{ date('d/m/Y', strtotime($notification->show_until_date)) }}" @endif>
                                                </div>
                                            </div>
										</div>
										<div class="row">&nbsp;
											@if(!empty($notification))
												<input type="hidden" name="id" value="{{ $notification->id }}">
											@endif
										</div>
									</fieldset>
									<!-- Deeplinks -->
									<fieldset>
											<legend>Configuración de Deeplinks</legend>
											<div class="row">
												<div class="form-group col-xs-4 deeplink">
													<label for="deeplink">Deeplink?</label>
													<select class="form-control" name="add_deeplink" id="add_deeplink">
														<option value="">-Selecciona-</option>
														<option value="1" @if(isset($notification) && !empty($notification->deeplink)) selected="selected" @endif>Sí</option>
														<option value="0" @if(isset($notification) && empty($notification->deeplink)) selected="selected" @endif>No</option>
													</select>

												</div>
											</div>
											<div class="is_deeplink @if(isset($notification) && empty($notification->deeplink)) unseen @endif">
												<div class="row">
													<div class="form-group col-xs-4 deeplink_type">
														<label for="deeplink_type">Tipo de deeplink</label>
														<select class="form-control" name="deeplink_type" id="deeplink_type" @if(!empty($notification->deeplink_type)) @endif required="required">
															<option value="">-Selecciona-</option>
															<option value="Store"   @if(isset($notification) && isset($notification->deeplink_type) && $notification->deeplink_type == 'Store' ) selected="selected" @endif>Tienda</option>
															<option value="Department" @if(isset($notification) && isset($notification->deeplink_type) && $notification->deeplink_type == 'Department' ) selected="selected" @endif>Departamento</option>
															<option value="Shelf"   @if(isset($notification) && isset($notification->deeplink_type) && $notification->deeplink_type == 'Shelf' ) selected="selected" @endif>Pasillo</option>
															<option value="Product" @if(isset($notification) && isset($notification->deeplink_type) && $notification->deeplink_type == 'Product' ) selected="selected" @endif>Producto</option>
														</select>
													</div>
													<div class="form-group col-xs-4 deeplink_cities @if(isset($notification) && empty($notification->deeplink_city_id)) unseen @endif">
														<label for="deeplink_cities">Ciudad</label>
														<select class="form-control" name="deeplink_city_id" id="deeplink_city_id" required="required">
															<option value="">-Selecciona-</option>
															@foreach($cities as $city)
																<option value="{{ $city->id }}" @if(isset($notification) && isset($notification->deeplink_city_id) && $notification->deeplink_city_id == $city->id ) selected="selected" @endif>{{ $city->city }}</option>
															@endforeach
														</select>
													</div>
													<div class="form-group col-xs-4 deeplink_store @if(isset($notification) && empty($notification->store_id)) unseen @endif">
														<label for="deeplink_store">Tienda</label>
														<select class="form-control" name="store_id" id="store_id" required="required">
															@if(!empty($stores))
																<option value="">-Selecciona-</option>
																@foreach($stores as $strs)
																	<option value="{{ $strs->id }}" @if(isset($notification) && isset($notification->store_id) && $notification->store_id == $strs->id ) selected="selected" @endif>{{ $strs->name }}</option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
												<div class="row">
													<div class="form-group col-xs-4 deeplink_departments @if(isset($notification) && empty($notification->department_id)) unseen @endif">
														<label for="deeplink_departments">Departamento</label>
														<select class="form-control" name="department_id" id="department_id" required="required">
															@if(!empty($departments))
																<option value="">-Selecciona-</option>
																@foreach($departments as $department)
																	<option value="{{ $department->id }}" @if(isset($notification) && isset($notification->department_id) && $notification->department_id == $department->id ) selected="selected" @endif>{{ $department->name }}</option>
																@endforeach
															@endif
														</select>
													</div>
													<div class="form-group col-xs-4 deeplink_shelves @if(isset($notification) && empty($notification->shelf_id)) unseen @endif">
														<label for="deeplink_shelves">Pasillo</label>
														<select class="form-control" name="shelf_id" id="shelf_id" required="required">
															@if(!empty($shelves))
																<option value="">-Selecciona-</option>
																@foreach($shelves as $shelve)
																	<option value="{{ $shelve->id }}" @if(isset($notification) && isset($notification->shelf_id) && $notification->shelf_id == $shelve->id ) selected="selected" @endif>{{ $shelve->name }}</option>
																@endforeach
															@endif
														</select>
													</div>
													<div class="form-group col-xs-4 deeplink_products @if(isset($notification) && empty($notification->store_product_id)) unseen @endif">
														<label for="deeplink_products">Producto</label>
														<select class="form-control" name="store_product_id" id="store_product_id" required="required">
															@if(!empty($store_products))
																<option value="">-Selecciona-</option>
																@foreach($store_products as $store_product)
																	<option value="{{ $store_product->id }}" @if(isset($notification) && isset($notification->store_product_id) && $notification->store_product_id == $store_product->id ) selected="selected" @endif>{{ $store_product->name }}</option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
											</div>
									</fieldset>
								</form>
							</div>
						</div>
						<div class="row">&nbsp;</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<button class="btn btn-primary save" type="button">Guardar</button>
									<a href="{{ route('adminNotifications.index') }}" class="btn btn-primary cancel">Cancelar</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('admin.notifications.preview_modal')

<script type="text/javascript">
	function ajaxCall(city_id) {
		$.ajax({
			url: '{{ route('adminNotifications.getStoresAjax') }}',
			type: 'GET',
			dataType: 'json',
			data: {city_id: city_id}
		})
		.done(function(data) {
		    var store_id = $('#store_id');
		    store_id.empty();
			store_id.append('<option value="" selected="selected">-Selecciona-</option>');
			$.each(data.result.stores, function(key,value) {
				if(store_id.attr('data-selected') && store_id.data('selected') === value.id) {
					store_id.append('<option value="'+value.id+'" selected="selected">'+value.name+'</option>');
				}else{
					store_id.append('<option value="'+value.id+'">'+value.name+'</option>');
				}
			});
		})
		.fail(function(err) {
		    alert('Ocurrio un problema al cargar la tiendas por la ciudad seleccionada');
		});
	}

	$(document).ready(function() {
		$('.notification-date').datetimepicker({
		    locale: 'es',
            format: 'DD/MM/YYYY',
        });

        var clipboard = new Clipboard('.copy-firstname');
        clipboard.on('success', function(e) {
            alert('Variable de reemplazo copiada');
            e.clearSelection();
        });

		$('.save').on('click', function(e){
			var validator = $('#add-notification-form').validate({
								rules: {
									city_id: 'required',
									campaign: 'required',
									type_push: 'required',
									push_subtitle: 'required',
									push_message: 'required',
    								title: {
    								    required: function (element) {
											if($('#type_push').val() == 'inapp') return true;
											else return false;
										}
									},
									text_button: {
                                        required: function (element) {
                                            if($('#type_push').val() == 'inapp') return true;
                                            else return false;
                                        }
                                    },
                                    description: {
                                        required: function (element) {
                                            if($('#type_push').val() == 'inapp') return true;
                                            else return false;
                                        }
    				                },
    				                show_until_date: {
                                        required: function (element) {
                                            if($('#type_push').val() == 'inapp') return true;
                                            else return false;
                                        }
                                    }
                                }
							});
			if(validator.form()){
				localStorage.removeItem('deeplink');
				$('#add-notification-form').submit();
			}
		});

		$('#add_deeplink').change(function() {
			var is_deeplink = $(this).val();
			if (is_deeplink == 1) {
				$('.is_deeplink').slideDown('fast');
				$('.deeplink_type').slideDown('fast');
			}else{
				$('.is_deeplink').slideUp('fast');
				$('.deeplink_type, .deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
				if ( $(this).data('default') === 'undefined' ) {
					$('#deeplink_type, #deeplink_city_id, #store_id, #department_id, #shelf_id, #product_id').prop('selectedIndex',0);
				}
			}
		});
		$('#add_deeplink').trigger('change');

		$('#deeplink_city_id').change(function(e) {
			var city_id = $(this).val();
			if(city_id !== '') ajaxCall(city_id);
		});

		$('#type_push').change(function(e) {
			var type_push = $(this).val();
			if(type_push === 'inapp'){
			  $('.inapp').slideDown('fast');
			  $('.image_url').attr('required', 'required');
			}
			else{
			  $('.inapp').slideUp('fast');
			  $('.image_url').removeAttr('required');
			  $('#title').val('');
			  $('#text_button').val('');
			  $('#description').val('');
			  $('#terms_conditions').val('');
			  $('#url').val('');
			  $('#last_order_date').val('');
			  $('#show_until_date').val('');
			}

		});
		$('#type_push').trigger('change');

		$('#deeplink_type').change(function() {
			var type = $(this).val();
			$('.deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
			$('#deeplink_city_id, #store_id, #department_id, #shelf_id, #product_id').prop('selectedIndex', 0);


			if (type !== '') {
				$('.deeplink_cities').slideDown('fast');
			}else{
				$('.deeplink_cities').slideUp('fast');
			}
		});

		$('#deeplink_city_id').change(function() {
			var city_id = $('#deeplink_city_id').val();
			var type = $('#deeplink_type').val();
			$('.deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
			$('#store_id, #department_id, #shelf_id, #product_id').prop('selectedIndex',0);

		    $.ajax({
		    	url: '{{ route('adminNotifications.getStoresAjax') }}',
		    	type: 'GET',
		    	dataType: 'json',
		    	data: {city_id: city_id},
		    	context: this
		    })
		    .done(function(data) {
		        var store_id = $('#store_id');
		    	store_id.empty();
		    	store_id.append('<option value="">-Selecciona-</option>');
		    	$.each(data.result.stores, function(key, value) {
					store_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
		    	});
		    	$('.deeplink_store').slideDown('fast');
		    })
		    .fail(function() {
		    	alert('Falló la función para buscar ciudades.');
		    });
		});

		$('#store_id').change(function() {
			var type = $('#deeplink_type').val();
			var store_id = $('#store_id').val();

			$('.deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
			$('#department_id, #shelf_id, #product_id').prop('selectedIndex',0);

			if (type === 'Department' || type === 'Shelf' || type === 'Product') {
				$.ajax({
					url: '{{ route('adminProducts.getDepartmentsAjax') }}',
					type: 'GET',
					dataType: 'json',
					data: {store_id: store_id},
					context: this
				})
				.done(function(msg) {
				    var department_id = $('#department_id');
					department_id.empty();
					department_id.append('<option value="">-Selecciona-</option>');
					$.each(msg, function(key, value){
						if(value.status === 1) department_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
					});
					$('.deeplink_departments').slideDown('fast');
				})
				.fail(function(error) {
					alert('ocurrió un error en la consulta de departmens');
				});
			}
		});

		$('#department_id').change(function() {
			var type = $('#deeplink_type').val();
			var department_id = $(this).val();

			$('.deeplink_products').slideUp('fast');
			$('#product_id').prop('selectedIndex',0);

			if (type === 'Shelf' || type === 'Product') {
				$.ajax({
					url: '{{ route('adminProducts.getShelvesAjax') }}',
					type: 'GET',
					dataType: 'json',
					data: {department_id: department_id}
				})
				.done(function(msg) {
				    var shelf_id = $('#shelf_id');
					shelf_id.empty();
					shelf_id.append('<option value="">-Selecciona-</option>');
					$.each(msg,function(key,value){
						if (value.status === 1) shelf_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
					});
					$('.deeplink_shelves').slideDown('fast');
				})
				.fail(function() {
					alert('error al consultar las subcategorias.');
				});
			}
		});

		$('#shelf_id').change(function() {
			var type = $('#deeplink_type').val();
			var shelf_id = $(this).val();
			$('.deeplink_products').slideUp('fast');
			$('#product_id').prop('selectedIndex',0);

			if ( type === 'Product' ) {
				$.ajax({
					url: "{{ route('adminProducts.getProductsAjax') }}",
					type: 'GET',
					dataType: 'json',
					data: {shelf_id: shelf_id}
				})
				.done(function(data) {
				    var store_product_id = $('#store_product_id');
					store_product_id.empty();
					store_product_id.append('<option value="">-Selecciona-</option>');
					$.each(data,function(key,value){
						if (value.status === 1) store_product_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
					});
					$('.deeplink_products').slideDown('fast');
				})
				.fail(function() {
					alert('Falló la función para mostrar los productos.');
				});
			}
		});
	});
</script>
@endsection