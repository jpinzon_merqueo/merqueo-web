    <div class="modal fade" id="preview_notification">
        <div class="modal-dialog" style="width: 330px;">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 15px;"><span aria-hidden="true">&times;</span></button>
                <div class="modal-header text-center" style="border-bottom:none !important; padding: 15px 50px">
                    <h3 class="preview-title"></h3>
                </div>
                <div class="modal-body" style="padding-top: 0;">
                    <p class="text-center preview-image"></p>
                    <p class="text-center preview-description" style="font-size: 16px;"></p>
                    <p class="text-center preview-terms" style="color:#ccc;font-size: 14px;margin:0;"></p>
                </div>
                <div class="modal-footer" style="background: #e91a58; color: #fff; text-align: center; margin-top: 0;padding: 10px 20px 10px;">
                    <span class="preview-url" style="font-size: 20px;"></span>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function show_modal(id)
        {
            $.ajax({
                    url: '{{ route('adminNotifications.previewAjax') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function(data){
                    if(data.status == 'OK'){
                        $('.preview-title').html(data.result.notification.title);
                        if (data.result.notification.image_url)
                           $('.preview-image').html('<img src="'+data.result.notification.image_url+'" class="img-responsive center-block" width="260" style="border:solid 1px #ccc;" />');
                        $('.preview-description').html(data.result.notification.description);
                        $('.preview-terms').html(data.result.notification.terms_conditions);
                        $('.preview-url').html(data.result.notification.text_button);
                        $('#preview_notification').modal('show');
                    }
                })
                .fail(function(data) {
                    console.log("error");
                })
                .always(function(data) {
                    console.log("complete");
                });
        }
    </script>