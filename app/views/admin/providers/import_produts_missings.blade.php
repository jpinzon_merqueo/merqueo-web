@extends('admin.layout')

@section('content')

<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
</script>
<a href="#importing" class="fancybox unseen"></a>
<div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Exitoso!</b> {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alerta!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id='main-form' action="{{ route('adminProviderOrders.importMissing') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group col-xs-12">
                            <label for="city_id">Ciudades</label>
                            <select id="city_id" name="city_id" class="form-control get-warehouses">
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="warehouse_id">Bodegas</label>
                            <select id="warehouse_id" name="warehouse_id" class="form-control">
                                @foreach ($warehouses as $warehouse)
                                    <option value="{{ $warehouse->id }}" >{{ $warehouse->warehouse }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="provider_id">Proveedor</label>
                            <select id="provider_id" name="provider_id" class="form-control">
                                @foreach ($providers as $provider)
                                    <option value="{{ $provider->id }}" @if($provider->id == 17) selected="selected" @endif >{{ $provider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Archivo Excel</label>
                            <input type="file" class="form-control" name="file_missing_products" placeholder="File">
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="missing_products">Productos faltantes</label>
                            <div class="radio">
                                <label>Si <input type="radio" name="missing_products" value="1"></label>
                                <label>No <input type="radio" name="missing_products" value="0" checked></label>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" onclick="">Importar productos</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>



<script type="text/javascript">
$('#main-form').validate({
  rules: {
    file_missing_products: 'required',
  }
});

$('#main-form').submit(function () {
    if($(this).valid()) {
        if(confirm('Está seguro que quiere importar este archivo?')){
            $('.fancybox').trigger('click');
        }
    }
});
</script>

@stop