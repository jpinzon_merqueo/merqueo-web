@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminProviders.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Proveedor</button>
            </a>
        </span>
    </section>
    <section class="content">
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
            @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
            @endif
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="providers-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td width="33%">
                                                <select id="city_id" name="city_id" class="form-control">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td align="right">
                                                <label>Buscar:</label>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="input-search" placeholder="Nombre proveedor" width="200px">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>

                                </form>

                            </div>
                        </div>
                        <br />
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <script>
        var web_url_ajax = "{{ route('adminProviders.index') }}";

        $(document).ready(function() {
            paging(1, '');

            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });
        });
    </script>
    @endsection
@else
    @section('content')
    <table id="providers-table" class="admin-providers-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Ciudad</th>
                <th>Código</th>
                <th>NIT</th>
                <th>Logo pequeño</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @if(count($providers))
            	@foreach($providers as $provider)
                <tr>
                    <td>{{ $provider->name }}</td>
                    <td>{{ $provider->city }}</td>
                    <td>{{ $provider->code }}</td>
                    <td>{{ $provider->nit }}</td>
                    <td align="center"><img src="{{ $provider->logo_small_url }}" width="50"></td>
                    <td>@if ($provider->status) Activa @else Inactiva @endif</td>
                    <td>
                    	<div class="btn-group">
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('adminProviders.edit', ['id' => $provider->id]) }}">Editar</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ route('adminProviders.delete', ['id' => $provider->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar el proveedor?')">Eliminar</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="7" align="center">No hay datos.</td></tr>
            @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="providers-table_info">Mostrando {{ count($providers) }} ítems</div>
        </div>
    </div>
    @endsection
@endif