<script>
    new Vue({
        el: '#content-credit-note',
        data: {
            formData: {
                productName: '',
                creditNoteReasons: [
                    {'reason': 'Devolución por avería'},
                    {'reason': 'Devolución por mal estado'},
                    {'reason': 'Devolución por productos no recibidos'},
                    {'reason': 'Devolución por fecha de vencimiento'},
                    {'reason': 'Devolución por diferencia en precio'}
                ],
                errors: {
                    quantity_calculate: {
                        error: false,
                        menssage: '',
                        index: 0
                    },
                    consumption_tax_credit_note: {
                        error: false,
                        menssage: '',
                        index: 0
                    }
                }
            },
            dsearch: false,
            dsave: false,
            dremove: false,
            load: false,
            productSearch: [],
            addProductsCreditNote: [],
            productsCreditNote: [],
            total_cost: 0,
            total_iva: 0,
            credit_note_number: '{{ $reception->credit_note_number }}',
            rete_fuente: {{-- ($reception->rete_fuente_amount) ? $reception->rete_fuente_amount : 0 --}}0,
            rete_ica: {{-- ($reception->rete_ica_amount) ? $reception->rete_ica_amount : 0 --}}0,
            total_consumption_tax_credit_note: 0,
            total: 0
        },
        mounted() {
            this.getProductsCreditNote()
        },
        methods: {
            formatPrice(value) {
                let val = (value / 1).toFixed(0)
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            calculateAmountProductsCreditNote: function (products) {
                let total_cost = 0
                let total_iva = 0
                let total_consumption_tax_credit_note = 0
                let productsInCreditNote = []
                products.forEach(function (product) {
                    if(product.type == 'Proveedor'){
                        product.grouped_products.forEach(function (productGroup) {
                            if(productGroup.reception_detail_product_group_id) {
                                productGroup.id = productGroup.reception_detail_product_group_id;
                                productGroup.quantity_expected = productGroup.reception_detail_product_group_quantity_expected
                                productGroup.quantity_received = productGroup.reception_detail_product_group_quantity_received
                                productGroup.sub_total_cost = productGroup.base_cost * productGroup.quantity_credit_note
                                productGroup.iva_amount = (productGroup.sub_total_cost * productGroup.iva) / 100
                                productGroup.total_cost = productGroup.cost * productGroup.quantity_credit_note
                                total_cost += productGroup.sub_total_cost
                                total_consumption_tax_credit_note += productGroup.consumption_tax_credit_note
                                total_iva += productGroup.iva_amount
                                productGroup.type = 'Proveedor'
                                productsInCreditNote.push(productGroup)
                            }
                        })
                    } else {
                        product.sub_total_cost = product.base_cost * product.quantity_credit_note
                        product.iva_amount = (product.sub_total_cost * product.iva) / 100
                        product.total_cost = product.cost * product.quantity_credit_note
                        total_cost += product.sub_total_cost
                        total_consumption_tax_credit_note += product.consumption_tax_credit_note
                        total_iva += product.iva_amount
                        productsInCreditNote.push(product)
                    }
                })
                this.total_cost = total_cost
                this.total_iva = total_iva
                this.total_consumption_tax_credit_note = total_consumption_tax_credit_note
                this.total = total_iva + total_consumption_tax_credit_note - this.rete_fuente - this.rete_ica + total_cost
                this.productsCreditNote = productsInCreditNote
            },
            calculateAmountPreviewCreditNote: function (product) {
                product.sub_total_cost = product.base_cost * product.quantity_calculate
                product.iva_amount = (product.sub_total_cost * product.iva) / 100
                product.total_cost = product.cost * product.quantity_calculate
                return product
            },
            getProductsCreditNote: function () {
                axios.get('{{ route('adminMovements.getProductsCreditNote', ['id' => $reception->id]) }}')
                    .then(response => {
                        this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                    }).catch(error => {
                    alert('Error al traer los productos para la nota crédito')
                })

            },
            searchProducts: function () {
                let name = this.formData.productName
                let excludeIds = this.productsAddedIds
                this.dsearch = true
                axios.get('{{ route('adminMovements.getProductsCreditNote', ['id' => $reception->id]) }}', {
                    params: {
                        productName: name,
                        excludeIds: JSON.stringify(excludeIds)
                    }
                })
                    .then(response => {
                        let products = []
                        response.data.productSearch.forEach(function (product) {
                            if(product.type == 'Proveedor'){
                                product.grouped_products.forEach(function (productGroup) {
                                    if(productGroup.reception_detail_product_group_id) {
                                        productGroup.id = productGroup.reception_detail_product_group_id;
                                        productGroup.quantity_expected = productGroup.reception_detail_product_group_quantity_expected
                                        productGroup.quantity_received = productGroup.reception_detail_product_group_quantity_received
                                        productGroup.quantity_calculate = productGroup.quantity_expected - productGroup.quantity_received
                                        productGroup.reason_credit_note = 'Devolución por avería'
                                        productGroup.type = 'Proveedor'
                                        products.push(productGroup)
                                    }
                                })
                            } else {
                                product.quantity_calculate = product.quantity_expected - product.quantity_received
                                product.reason_credit_note = 'Devolución por avería'
                                products.push(product)
                            }
                        })
                        this.productSearch = products
                        this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                        this.dsearch = false
                    }).catch(error => {
                    alert('Error al traer el producto buscado')
                    this.dsearch = true
                })
            },
            validateForm: function (product, index) {
                let validate = {
                    errors: {
                        quantity_calculate: {
                            error: (product.quantity_calculate <= 0 || product.quantity_calculate == null),
                            message: (product.quantity_calculate === '') ? 'Campo númerico requerido' : 'El campo debe ser mayor a cero',
                            index: index
                        },
                        consumption_tax_credit_note: {
                            error: (product.consumption_tax_credit_note < 0 || product.consumption_tax_credit_note == null || product.consumption_tax_credit_note === ''),
                            message: (product.consumption_tax_credit_note === '') ? 'Campo númerico requerido' : 'El campo debe ser cero o mayor a cero',
                            index: index
                        }
                    }
                }
                if (validate.errors.quantity_calculate.error || validate.errors.consumption_tax_credit_note.error) {
                    this.formData.errors = validate.errors
                    return false
                }
                this.resetErrorsForm(product, index)
                return true
            },
            resetErrorsForm: function (product, index) {
                let reset = {
                    errors: {
                        quantity_calculate: {
                            error: false,
                            menssage: '',
                            index: index,
                        },
                        consumption_tax_credit_note: {
                            error: false,
                            menssage: '',
                            index: index,
                        }
                    }
                }
                this.formData.errors = reset.errors
            },
            addCreditNote: function (product, index) {
                if (this.validateForm(product, index)) {
                    this.addProductsCreditNote.push(this.calculateAmountPreviewCreditNote(product))
                    this.productSearch.splice(index, 1)
                    this.resetErrorsForm(product, index)
                }
            },
            removeAddCreditNote: function (product, index) {
                this.productSearch.push(product)
                this.addProductsCreditNote.splice(index, 1)
            },
            removeCreditNote: function (product) {
                let confirmation = confirm('¿Estas seguro que deseas eliminar el producto de la nota crédito?')
                if (confirmation) {
                    this.dremove = true
                    let route = '{{ route('adminMovements.receptionUpdateCreditNote', ['id' => $reception->id]) }}'
                    let data = {
                        products: product,
                        action: 'delete'
                    }
                    axios({
                        url: route,
                        method: 'POST',
                        data: data
                    })
                        .then(response => {
                            this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                            this.addProductsCreditNote = response.data.addProductsCreditNote
                            alert('Producto eliminado de la nota crédito con éxito.');
                            this.dremove = false
                            search_products()
                        })
                        .catch(error => {
                            alert('Error al guardar las notas crédito.')
                            this.dremove = false
                        })
                }
            },
            updateProductsCreditNote: function () {
                let confirmation = confirm('¿Estas seguro que deseas agregar los productos a la nota crédito?')
                if (confirmation) {
                    this.dsave = true
                    this.load = true
                    let route = '{{ route('adminMovements.receptionUpdateCreditNote', ['id' => $reception->id]) }}'
                    let data = {
                        products: this.productsAddedSubmit,
                        action: 'edit'
                    }
                    axios({
                        url: route,
                        method: 'POST',
                        data: data
                    })
                        .then(response => {
                            this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                            this.addProductsCreditNote = response.data.addProductsCreditNote
                            alert('Productos agregados de la nota crédito con éxito.');
                            this.dsave = false
                            this.load = false
                            search_products()
                        })
                        .catch(error => {
                            alert('Error al guardar las notas crédito.')
                            this.dsave = false
                            this.load = false
                        })
                }
            }
        },
        computed: {
            productsAddedIds: function () {
                let addProductsCreditNote = this.addProductsCreditNote.map(product => {
                    return product.id
                })
                let productsCredditNote = this.productsCreditNote.map(product => {
                    return product.id
                })
                return productsCredditNote.concat(addProductsCreditNote)
            },
            productsAddedSubmit: function () {
                return this.addProductsCreditNote.map(product => {
                    return {
                        id: product.id,
                        quantity_calculate: product.quantity_calculate,
                        reason_credit_note: product.reason_credit_note,
                        consumption_tax_credit_note: product.consumption_tax_credit_note,
                        type: product.type
                    }
                })
            },
            disableDivAdd: function () {
                return (this.addProductsCreditNote.length == 0) ? false : true
            },
            disableDivProductsCN: function () {
                return (this.productsCreditNote.length == 0) ? false : true
            },
            disableSave: function () {
                return (this.addProductsCreditNote.length == 0 || this.dsave) ? true : false
            },
            disableRemove: function () {
                return this.dremove
            },
            disabledSearch: function () {
                return this.dsearch
            },
            loader: function () {
                return this.load
            }
        }
    })
</script>