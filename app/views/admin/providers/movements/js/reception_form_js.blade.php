<!--coverage.js-->
<script type="text/javascript">
    @if ($reception)
    function search_products(){
        $.ajax({
            url: '{{ route('adminMovements.getProductsAjax', ['id' => $reception->id]) }}',
            data: { s: $('#search-products').val() },
            type: 'GET',
            dataType: 'json'
        })
            .done(function(data) {
                $('#products_table_container').empty();
                $('#products_table_container').html(data.html);
                if ( data.success ) {
                    $('#reception_products_error .message').empty().html(data.message);
                    $('#reception_products_error').slideDown('fast').delay(10000).slideUp('fast');
                }
                $("#quantity_received_input").focus();
            })
            .fail(function() {
                console.error("error al cargar los contactos");
            })
            .always(function() {
                $('#search-products').val('');
            });
    }
    @endif
    $(document).ready(function() {
        @if ($reception)
        $('#reception_expiration_date, #reception_voucher_date, #expiration_date, #set_expiration_date').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $('.set_expiration_date_form').validate();
        $('#credit-note').validate();

        search_products();

        $('#search-products').keyup(function(event){
            code = event.which;
            if (code == 32 || code == 13 || code == 188 || code == 186){
                search_products();
            }
        });

        $('body').on('click', '.open-update-product', function(event) {
            event.preventDefault();
            $('#edit-expiration-date .update-product').data('name', $(this).data('name'));
            $('#edit-expiration-date .update-product').data('product', $(this).data('product'));
            $('#edit-expiration-date .update-product').data('handle_expiration_date', $(this).data('handle_expiration_date'));
            $('#edit-expiration-date').modal('show');
        });

        $('body').on('click', '.update-product', function(event) {
            event.preventDefault();
            var status = $(this).data('status');
            var data = {
                status: status
            };

            if ( status == 'Recibido' && $(this).data('handle_expiration_date') == 1 ) {
                var expiration_date = $('#edit-expiration-date #expiration_date').val();
                data.expiration_date = expiration_date;
            }

            if (status == 'Parcialmente recibido')
            {
                $('.modal-title').html($(this).data('name'));
                $('#modal-product').modal('show');
                $('#modal_store_product_id').val($(this).data('product'));
            }
            else{
                var store_product_id = $(this).data('product');
                data.store_product_id = store_product_id;
                $('.btn-group button').addClass('disabled');
                $.ajax({
                    url: '{{ route('adminMovements.updateProductReceptionAjax', ['id' => $reception->id ]) }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                })
                    .done(function(data) {
                        if(data.success){
                            search_products();
                        }else{
                            alert(data.message);
                        }
                        $('.btn-group button').removeClass('disabled');
                    })
                    .fail(function(data) {
                        $('.btn-group button').removeClass('disabled');
                    })
                    .always(function(data) {
                        $('#edit-expiration-date').modal('hide');
                        $('.btn-group button').removeClass('disabled');
                    });
            }
        });

        $('.form-modal').on('submit', function(e) {
            var error = false
            var form = this;

            $('input[type!="hidden"], select', this).each(function() {
                if ($(this).is(':visible') && ($(this).val().length == 0)) {
                    $(this).parent().addClass('has-error');
                    error = 'Completa los campos requeridos.';
                }else $(this).parent().removeClass('has-error');
            });

            if(error) {
                $('.form-has-errors', form).html(error).fadeIn();
                return false;
            }
            return true;
        });

        $('#modal-order-product-group').on('hidden.bs.modal', function () {
            $('#update-group-quantity').hide();
        })
        @endif
        $("#main-form").validate({
            rules: {
                provider_order_id: {
                    required: true
                },
                invoice_number: {
                    required: true
                },
                transporter: {
                    required: true
                },
                plate: {
                    required: true
                },
                driver_name: {
                    required: true
                },
                driver_license: {
                    required: true
                },
                observations: {
                    required: true
                }
            }
        });
    });

    @if ($reception)
    var addProduct = (function() {
            'use strict';

            function addProduct(args) {
                // enforces new
                if (!(this instanceof addProduct)) {
                    return new addProduct(args);
                }
                this.url_search_product = '{{ route('adminMovements.receptionSearchProductAjax', ['id' => $reception->id, 'provider_id' => $reception->provider_id]) }}';
                this.url_add_product = '{{ route('adminMovements.receptionAddProductAjax', ['id' => $reception->id, 'provider_id' => $reception->provider_id]) }}';
                this.add_product = {
                    provider_id: null,
                    reception_id: null,
                    store_product_id: null,
                    current_stock: null,
                    cant: null
                };
                this.bindActions();
            }

            addProduct.prototype.bindActions = function() {
                var self = this;
                $('body').on('click', '#btn-modal-search', function(){
                    if ($('.modal-search').val() != ''){
                        // alert('search');
                        // search_modal($('.modal-search').val());
                        $('.modal-products-request-loading').show();
                        var search = $('.modal-search').val();
                        $.ajax({
                            url: self.url_search_product,
                            data: {
                                s: search
                            },
                            type: 'GET',
                            dataType: 'html',
                            success: function(data) {
                                $('.modal-products-request-loading').hide();
                                $('.modal-products-request .tbody').html(data);
                            }, error: function() {
                                $('.modal-products-request-loading').hide();
                                $('.modal-products-request .tbody').html('Ocurrió un error al obtener los datos.');
                            }
                        });
                    }else{
                        $('.modal-search').css({ border:'solid 1px #ff0000'});
                        $('.error').show();
                    }
                });
                $('body').on('click', '.btn-add-product', function(event) {
                    event.preventDefault();
                    self.add_product.provider_id = $(this).data('provider_id');
                    self.add_product.reception_id = $(this).data('reception_id');
                    self.add_product.store_product_id = $(this).data('store_product_id');
                    self.add_product.current_stock = $(this).data('current_stock');
                    self.add_product.cant = $(this).parents('tr').find('.product_cant').val();

                    if (self.add_product.cant > 0) {
                        self.add_product_ajax();
                    }else{
                        $('.error-search').show().find('.alert-sucess').hide();
                        $('.error-search').show().find('.alert-danger').html('La cantidad de producto debe ser mayor a cero.').show();
                    }
                });
            };

            addProduct.prototype.add_product_ajax = function() {
                $.ajax({
                    url: this.url_add_product,
                    type: 'POST',
                    dataType: 'json',
                    data: this.add_product
                })
                    .done(function(response) {
                        // console.debug(response);
                        if (response.status == true) {
                            $('.error-search').show().find('.alert-danger').hide();
                            $('.error-search').show().find('.alert-success').html(response.message).show();
                            location.reload();
                        }else{
                            alert(response.message);
                            /*if ( res ) {
                                data.update_quantity = true;
                                add_product_ajax(data);
                                return;
                            }else{*/
                            $('.error-search').show().find('.alert-success').hide();
                            $('.error-search').show().find('.alert-danger').html('No se ha agregado el producto.').show();
                            // }
                        }
                        return;
                    })
                    .fail(function() {
                        console.log("error al agregar el producto");
                    });
            };

            return addProduct;
        }());
    var add_product = new addProduct;

    var UpdateProductStatus = (function() {
        'use strict';

        function UpdateProductStatus() {
            // enforces new
            if (!(this instanceof UpdateProductStatus)) {
                return new UpdateProductStatus();
            }
            // constructor body
            this.store_product_id = null;
            this.accounting_status = null;
            this.url_update_product_status_ajax = '{{ route('adminMovements.receptionUpdateProductAccountingStatus', ['id' => $reception->id]) }}';
            this.bindActions();
        }

        UpdateProductStatus.prototype.bindActions = function() {
            var self = this;
            $('body').on('click', '.update-accounting-status', function(event) {
                event.preventDefault();
                self.store_product_id = $(this).data('product');
                self.accounting_status = $(this).data('accounting_status');
                self.update_product_status_ajax();
            });
        };

        UpdateProductStatus.prototype.update_product_status_ajax = function() {
            $.ajax({
                url: this.url_update_product_status_ajax,
                type: 'POST',
                dataType: 'json',
                data: {
                    store_product_id: this.store_product_id,
                    accounting_status: this.accounting_status
                },
            })
                .done(function(data) {
                    if (data.status) {
                        search_products();
                    }else{
                        alert(data.message);
                    }
                })
                .fail(function(data) {
                    alert('Ocurrió un error al cambiar de estado el producto.');
                });

        };

        return UpdateProductStatus;
    }());
    var update_product_status = new UpdateProductStatus;

    var editCostModal = (function() {
        'use strict';

        function editCostModal() {
            // enforces new
            if (!(this instanceof editCostModal)) {
                return new editCostModal();
            }
            this.edit_cost_old = {
                product_name: null,
                store_product_id: null,
                product_cost: null,
                product_iva: null,
                product_consumption_tax: null,
                product_quantity_expected: null,
                product_quantity_received: null,
                is_given: 0,
                given_quantity: 0
            };
            this.edit_cost_old_grouped = {
                product_name: null,
                provider_order_details_id: null,
                provider_order_detail_product_group_id: null,
                provider_order_reception_detail_id: null,
                provider_order_reception_detail_product_group_id: null,
                product_cost: null,
                product_iva: null,
                product_consumption_tax: null,
                product_quantity_expected: null,
                product_quantity_received: null
            };
            this.bindActions();
        }

        editCostModal.prototype.bindActions = function() {
            var self = this;
            $('body').on('click', '.show_products_btn', function(event) {
                event.preventDefault();
                $('.show_products').find('#provider_product').removeAttr('disabled');
                $('.hide_products_btn, .show_products_btn, .show_products').toggle('fast');
            });
            $('body').on('click', '.hide_products_btn', function(event) {
                event.preventDefault();
                $('#reception-product-quantity-received').val(self.edit_cost_old.product_quantity_received);
                $('#reception-product-cost').val(self.edit_cost_old.product_cost);
                $('#reception-product-iva').val(self.edit_cost_old.product_iva);
                $('#reception-product-consumption-tax').val(self.edit_cost_old.product_consumption_tax);

                $('.show_products').find('#provider_product').attr('disabled', true);
                $('.show_products_btn, .hide_products_btn, .show_products').toggle('fast');
            });
            $('#edit-cost-modal').on('hidden.bs.modal', function () {
                $('.hide_products_btn, .show_products').hide();
                $('.show_products_btn').show();
                $('.show_products').find('#provider_product').attr('disabled', true);
            })
            $('#edit-cost-modal').on('shown.bs.modal', function () {
                $('#provider_product option:selected').each(function(index, el) {
                    $(el).prop('selected', false)
                });
                $('#provider_product option').each(function(index, el) {
                    if ( $(this).val() == $('#reception-store-product-id').val() ){
                        $(this).prop('selected', true);
                    }
                });
            });
            $('body').on('change', '#provider_product', function(event) {
                event.preventDefault();
                let iva = $(this).find(':selected').data('iva');
                let cost = $(this).find(':selected').data('cost');
                let iva_amount = cost * ( iva / 100 );
                $('#reception-product-quantity-received').val(self.edit_cost_old.product_quantity_received);
                $('#reception-product-cost').val( $(this).find(':selected').data('cost') );
                $('#reception-product-iva').val( $(this).find(':selected').data('iva') );
                $('#reception-product-iva-amount').val( iva_amount );
                $('#reception-product-consumption-tax').val( $(this).find(':selected').data('consumption_tax') ? $(this).find(':selected').data('consumption_tax') : 0 );
            });
            $('#reception-product-cost').keyup(function () {
                let cost = $('#reception-product-cost').val();
                let iva = $('#reception-product-iva').val();
                let iva_amount = cost * ( iva / 100 );
                $('#reception-product-iva-amount').val( iva_amount );
            });
            $('#reception-product-grouped-cost').keyup(function () {
                let cost = $('#reception-product-grouped-cost').val();
                let iva = $('#reception-product-grouped-iva').val();
                let iva_amount = cost * ( iva / 100 );
                $('#reception-product-grouped-iva-amount').val( iva_amount );
            });
            $('#reception-product-iva').keyup(function () {
                let cost = $('#reception-product-cost').val();
                let iva = $('#reception-product-iva').val();
                let iva_amount = cost * ( iva / 100 );
                $('#reception-product-iva-amount').val( iva_amount );
            });
            $('#reception-product-grouped-iva').keyup(function () {
                let cost = $('#reception-product-grouped-cost').val();
                let iva = $('#reception-product-grouped-iva').val();
                let iva_amount = cost * ( iva / 100 );
                $('#reception-product-grouped-iva-amount').val( iva_amount );
            });
            $('body').on('change', '#is_given', function () {
                let value = $(this).val();
                if (value == 0) {
                    $('#given_quantity').val(0);
                    $('#given_quantity').prop('min', 0);
                    $('#given_quantity').hide();
                    $('.given_quantity').hide();
                } else {
                    $('#given_quantity').prop('min', 1);
                    $('#given_quantity').show();
                    $('.given_quantity').show();
                }
            });
            $('#is_given').trigger('change');
        };

        editCostModal.prototype.set_edit_form_data = function(store_product_id, product_name, base_cost, iva, consumption_tax, quantity_expected, quantity_received, is_given, given_quantity) {
            this.edit_cost_old.product_name = product_name;
            this.edit_cost_old.store_product_id = store_product_id;
            this.edit_cost_old.product_base_cost = base_cost;
            this.edit_cost_old.product_iva = iva;
            this.edit_cost_old.product_consumption_tax = consumption_tax;
            this.edit_cost_old.product_quantity_expected = quantity_expected;
            this.edit_cost_old.product_quantity_received = quantity_received;
            this.edit_cost_old.is_given = is_given;
            this.edit_cost_old.given_quantity= given_quantity;

            let iva_amount = this.edit_cost_old.product_base_cost * ( iva / 100 );
            $('#reception-product-name').html(product_name);
            $('#reception-store-product-id').val(store_product_id);
            $('#reception-product-cost').val(base_cost);
            $('#reception-product-iva').val(iva);
            $('#reception-product-iva-amount').val(iva_amount);
            $('#reception-product-consumption-tax').val(consumption_tax);
            $('#reception-product-quantity-expected').val(quantity_expected);
            $('#reception-product-quantity-received').val(quantity_received);
            $('#is_given').val(is_given);
            $('#given_quantity').prop('max', quantity_received);
            $('#given_quantity').prop('min', 1);
            $('#given_quantity').val(given_quantity);
            $('#edit-cost-modal').modal('show');
            $('#is_given').trigger('change');
        };

        editCostModal.prototype.set_edit_form_grouped_data = function(product_name, provider_order_reception_detail_id, provider_order_details_id, provider_order_detail_product_group_id, provider_order_reception_detail_product_group_id, base_cost, iva, consumption_tax, quantity_expected, quantity_received) {
            this.edit_cost_old_grouped.product_name = product_name;
            this.edit_cost_old_grouped.provider_order_details_id = provider_order_details_id;
            this.edit_cost_old_grouped.provider_order_detail_product_group_id = provider_order_detail_product_group_id;
            this.edit_cost_old_grouped.provider_order_reception_detail_id = provider_order_reception_detail_id;
            this.edit_cost_old_grouped.provider_order_reception_detail_product_group_id = provider_order_reception_detail_product_group_id;
            this.edit_cost_old_grouped.product_base_cost =  base_cost;
            this.edit_cost_old_grouped.product_iva = iva;
            this.edit_cost_old_grouped.product_consumption_tax = consumption_tax;
            this.edit_cost_old_grouped.product_quantity_expected = quantity_expected;
            this.edit_cost_old_grouped.product_quantity_received = quantity_received;
            let iva_amount = this.edit_cost_old_grouped.product_base_cost * ( iva / 100 );
            $('#reception-product-grouped-name').html(product_name);
            $('#provider_order_details_id').val(provider_order_details_id);
            $('#provider_order_detail_product_group_id').val(provider_order_detail_product_group_id);
            $('#provider_order_reception_detail_group_id').val(provider_order_reception_detail_id);
            $('#provider_order_reception_detail_product_group_id').val(provider_order_reception_detail_product_group_id);
            $('#reception-product-grouped-cost').val(base_cost);
            $('#reception-product-grouped-iva').val(iva);
            $('#reception-product-grouped-iva-amount').val(iva_amount);
            $('#reception-product-grouped-consumption-tax').val(consumption_tax);
            $('#reception-product-grouped-quantity-expected').val(quantity_expected);
            $('#reception-product-grouped-quantity-received').val(quantity_received);
            $('#edit-cost-modal-grouped').modal('show');
        };

        return editCostModal;
    }());
    var edit_cost_modal = new editCostModal;

    var editExpectedQuantityModal = (function () {
        'use strict';

        function editExpectedQuantityModal() {
            if (!(this instanceof editExpectedQuantityModal)) {
                return new editExpectedQuantityModal();
            }
            this.edit_expected_quantity = {
                product_name: null,
                store_product_id: null,
                product_quantity_expected: null
            };
            this.edit_expected_quantity_grouped = {
                product_name: null,
                provider_order_details_id: null,
                provider_order_detail_product_group_id: null,
                provider_order_reception_detail_id: null,
                provider_order_reception_detail_product_group_id: null,
                product_quantity_expected: null
            };
        }

        editExpectedQuantityModal.prototype.set_edit_form_data = function(product_name, store_product_id, product_quantity_expected, product_quantity_order, reception_detail_id) {
            this.edit_expected_quantity.product_name = product_name;
            this.edit_expected_quantity.store_product_id = store_product_id;
            this.edit_expected_quantity.product_quantity_expected = product_quantity_expected;

            $('#expectedQuantity-product-name').html(product_name);
            $('#expectedQuantity-store-product-id').val(store_product_id);
            $('#expectedQuantity-product-quantity-expected').val(product_quantity_expected);
            $('#expectedQuantity-reception-detail-id').val(reception_detail_id);
            $('#expectedQuantity-product-quantity-order').val(product_quantity_order);
            $('#edit-expectedQuantity-modal').modal('show');
        }

        editExpectedQuantityModal.prototype.set_edit_form_data_grouped = function(product_name, store_product_id, product_quantity_expected, product_quantity_order, reception_detail_product_group_id) {
            this.edit_expected_quantity_grouped.product_name = product_name;
            this.edit_expected_quantity_grouped.store_product_id = store_product_id;
            this.edit_expected_quantity_grouped.product_quantity_expected = product_quantity_expected;

            $('#expectedQuantityGrouped-product-name').html(product_name);
            $('#expectedQuantityGrouped-store-product-id').val(store_product_id);
            $('#expectedQuantityGrouped-product-quantity-expected').val(product_quantity_expected);
            $('#expectedQuantityGrouped-reception-detail-id').val(reception_detail_product_group_id);
            $('#expectedQuantityGrouped-product-quantity-order').val(product_quantity_order);
            $('#edit-expectedQuantityGrouped-modal').modal('show');
        }

        return editExpectedQuantityModal;
    }());
    var expected_quantity_modal = new editExpectedQuantityModal;

    var accountingData = (function() {
        'use strict';

        function accountingData(args) {
            // enforces new
            if (!(this instanceof accountingData)) {
                return new accountingData(args);
            }
            this.rete_ica = null;
            this.rete_iva = null;
            this.bindActions();
            // constructor body
        }

        accountingData.prototype.bindActions = function() {
            $('#taxes-form').validate();
            $('body').on('click', '#update-status-reception', function(event) {
                $('#modal-taxes').modal('show');
                // event.preventDefault();
                /* Act on the event */
            });
        };

        return accountingData;
    }());
    var accounting_data = new accountingData;

    var updateReceivedQuantity = (function() {
        'use strict';

        function updateReceivedQuantity(args) {
            // enforces new
            if (!(this instanceof updateReceivedQuantity)) {
                return new updateReceivedQuantity(args);
            }
            this.handle_expiration_date = 0;
            this.expiration_date = null;
            this.quantity_received = null;
            this.store_product_id = 0;
            this.bindActions();
        }

        updateReceivedQuantity.prototype.resetAttributes = function(first_argument) {
            this.handle_expiration_date = 0;
            this.expiration_date = null;
            this.quantity_received = null;
            this.store_product_id = 0;
        };

        updateReceivedQuantity.prototype.bindActions = function(args) {
            let self = this;
            $('body').on('keypress','#quantity_received_input', function(e){
                var key = e.which;
                if(key == 13){
                    $('#search-products').val('');
                    if ( $(this).val() != '' ) {
                        if ( $(this).val() >= 2000 ) {
                            let conf = confirm('Se ha ingresado un valor mayor a dos mil unidades. ¿Desea seguir con esta cantidad?');
                            if ( !conf ) {
                                return;
                            }
                        }
                        self.quantity_received = $(this).val();
                        self.store_product_id = $(this).data('store_product_id')
                        if ( $(this).data('handle_expiration_date') ) {
                            self.handle_expiration_date = 1;
                            $('#set-expiration-date').modal('show');
                        }else{
                            self.handle_expiration_date = 0;
                            self.expiration_date = null;
                            self.updateProductStatusAjax();
                        }
                    }else{
                        alert('el campo no debe estar vacío')
                    }
                }
            });
            $('body').on('click', '.set_expiration_date_update', function(event) {
                if ( $('.set_expiration_date_form').valid() ) {
                    self.handle_expiration_date = $('#quantity_received_input').data('handle_expiration_date')
                    self.expiration_date = $('#set_expiration_date').val();
                    self.updateProductStatusAjax();
                    $('#set-expiration-date').modal('hide');
                }
            });
            $('body').on('click', '#update-group-quantity', function(event) {
                if ( $('#product-group-info').valid() ) {
                    $('#product-group-info').submit();
                    // let form_data = $('#product-group-info').serialize();
                }
                event.preventDefault();
            });
            $('body').on('click', '.update-product-group', function(event) {
                let status = {
                    status: 'Dañado',
                    product: $(this).data('product')
                };
                self.updateProductGroupStatusAjax(status);
                event.preventDefault();
            });
        };

        updateReceivedQuantity.prototype.updateProductStatusAjax = function() {
            let data = {};
            if ( this.handle_expiration_date != 0 ) {
                data.handle_expiration_date = this.handle_expiration_date;
            }
            if ( this.expiration_date != null ) {
                data.expiration_date = this.expiration_date;
            }
            if ( this.quantity_received != '' ) {
                data.quantity_received = this.quantity_received;
            }
            if ( this.store_product_id != 0 ) {
                data.store_product_id  = this.store_product_id;
            }
            $.ajax({
                url: '{{ route('adminMovements.updateProductReceptionAjax', ['id' => $reception->id]) }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function(data) {
                    if ( data.success ) {
                        $('#reception_products_success .message').empty().html(data.message);
                        $('#reception_products_success').slideDown('fast').delay(4000).slideUp('fast');
                        search_products();
                    }else{
                        if ( data.status ) {
                            $('#reception_products_success .message').empty().html(data.message);
                            $('#reception_products_success').slideDown('fast').delay(4000).slideUp('fast');
                            search_products();
                        }else{
                            $('#reception_products_error .message').empty().html(data.message);
                            $('#reception_products_error').slideDown('fast').delay(4000).slideUp('fast');
                        }
                    }
                    $("#search-products").focus();
                })
                .fail(function() {
                    console.log("error al actualizar los productos.");
                });
        };

        updateReceivedQuantity.prototype.updateProductGroupStatusAjax = function(status) {
            $.ajax({
                url: '{{ route('adminMovements.receptionUpdateProductGroup', ['id' => $reception->id]) }}',
                type: 'POST',
                dataType: 'json',
                data: status,
            })
                .done(function(data) {
                    if ( data.status ) {
                        $('#modal-order-product-group').modal('hide');
                        $('#reception_products_success .message').empty().html(data.message);
                        $('#reception_products_success').slideDown('fast').delay(4000).slideUp('fast');
                        search_products();
                    }
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        };

        return updateReceivedQuantity;
    }());
    var update_received_quantity = new updateReceivedQuantity;

    var Taxes = (function() {
        'use strict';

        function Taxes() {
            // enforces new
            if (!(this instanceof Taxes)) {
                return new Taxes();
            }
            this.wtcode_ica = null;
            this.wtcode_fuente = null;
            this.ica_account = null;
            this.ica_percentage = null;
            this.fuente_account = null;
            this.fuente_percentage = null;
            this.bindActions();
        }

        Taxes.prototype.bindActions = function() {
            let self = this;
            $('body').on('change', '#rete_ica_options', function(event) {
                if ( $(this).val() != 'No Aplica' ) {
                    self.wtcode_ica = $(this).find(':selected').data('wtcodeica');
                    self.ica_account = $(this).find(':selected').data('account');
                    self.ica_percentage = $(this).find(':selected').data('percentage');
                    let total = $('#rete_ica_amount').data('total') * self.ica_percentage;
                    $('#wtcode_ica').val(self.wtcode_ica);
                    $('#rete_ica_percentage').val(self.ica_percentage);
                    $('#rete_ica_account').val(self.ica_account);
                    $('#rete_ica_amount').val(Math.round(total));
                }else{
                    self.wtcode_ica = '';
                    self.ica_account = '';
                    self.ica_percentage = 0;
                    $('#wtcode_ica').val(self.wtcode_ica);
                    $('#rete_ica_percentage').val(self.ica_percentage);
                    $('#rete_ica_account').val(self.ica_account);
                    $('#rete_ica_amount').val(0);
                }
            });
            $('body').on('change', '#rete_fuente_options', function(event) {
                if ( $(this).val() != 'No Aplica' ) {
                    self.wtcode_fuente = $(this).find(':selected').data('wtcodefuente')
                    self.fuente_account = $(this).find(':selected').data('account');
                    self.fuente_percentage = $(this).find(':selected').data('percentage');
                    let total = ($('#rete_fuente_amount').data('total') * self.fuente_percentage) / 100;
                    $('#wtcode_fuente').val(self.wtcode_fuente);
                    $('#rete_fuente_percentage').val(self.fuente_percentage);
                    $('#rete_fuente_account').val(self.fuente_account);
                    $('#rete_fuente_amount').val(Math.round(total));
                }else{
                    self.wtcode_fuente = '';
                    self.fuente_account = '';
                    self.fuente_percentage = 0;
                    $('#wtcode_fuente').val(self.wtcode_fuente);
                    $('#rete_fuente_percentage').val(self.fuente_percentage);
                    $('#rete_fuente_account').val(self.fuente_account);
                    $('#rete_fuente_amount').val(0);
                }
            });
        };
        return Taxes;
    }());
    let tax_obj = new Taxes;

    var receptionValidate = (function() {
        'use strict';

        function receptionValidate(args) {
            // enforces new
            if (!(this instanceof receptionValidate)) {
                return new receptionValidate(args);
            }
            this.bindActions();
            // constructor body
        }

        receptionValidate.prototype.bindActions = function(args) {
            $('body').on('click', '#get-reception-validate', function(event) {
                event.preventDefault();
                $('#reception-validate-modal').modal('show');
            });
            // method body
        };

        return receptionValidate;
    }());
    let reception_validate = new receptionValidate;
    @endif
</script>