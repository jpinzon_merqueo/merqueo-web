@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }} @if ($provider_order) de Orden de Compra # {{ $provider_order->id }} @endif
            <small>Control panel</small>
        </h1>
        @if (!$provider_order || ($provider_order && $provider_order->status != 'Cerrada'))
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminMovements.addReception', ['id' => $provider_order ? $provider_order->id : 0]) }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Recibo de Bodega</button>
            </a>
        </span>
        @endif
    </section>
    <section class="content">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <div class="admin-providers-table">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <div class="row form-group">
                                                <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                    <label for="city_id">Ciudad:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                    <label for="warehouse_id">Bodega:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    <select name="warehouse_id" id="warehouse_id" class="form-control">
                                                        <option value="">-Selecciona-</option>
                                                        @foreach ($warehouses as $warehouse)
                                                            <option value="{{ $warehouse->id }}">{{ $warehouse->warehouse }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                    <label for="status">Estado:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    <select id="status" name="status[]" multiple="multiple" class="form-control">
                                                        <option value="En proceso">En proceso</option>
                                                        <option value="Recibido">Recibido</option>
                                                        <option value="Almacenado">Almacenado</option>
                                                        <option value="Cancelado">Cancelado</option>
                                                        <option value="Revisado">Revisado</option>
                                                        <option value="Contabilizado">Contabilizado</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="row form-group">
                                                <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                    <label for="provider_id">Proveedor:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    <select id="provider_id" name="provider_id" class="form-control">
                                                        <option value="">Selecciona</option>
                                                        @if (count($providers))
                                                            @foreach ($providers as $provider)
                                                                <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                    <label for="provider_type">Tipo de proveedor:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    <select id="provider_type" name="provider_type" class="form-control">
                                                        <option value="">Selecciona</option>
                                                        <option value="Merqueo">Merqueo</option>
                                                        <option value="Marketplace">Marketplace</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                    <label for="search_term">Buscar:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    <input id="search_term" name="search_term" type="text" placeholder="Nro. Recibo de bodega" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-9 col-md-9 col-xs-9 col-lg-offset-3 col-md-offset-3">
                                                    <input type="hidden" name="provider_order_id" value="{{ $provider_order_id }}">
                                                    <button type="submit" id="btn-reception-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                                    <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($admin_permissions['permission2'] || $admin_permissions['permission3'])
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="row form-group">
                                                    <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                                        <label for="movement_consecutive_p">Consecutivo P:</label>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-xs-9">
                                                        <input type="text" name="movement_consecutive_p" id="movement_consecutive_p" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <table id="reception-table" class="admin-reception-table table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Recibo de bodega #</th>
                                    <th>Orden de compra #</th>
                                    <th>Ciudad</th>
                                    <th>Bodega</th>
                                    <th>Proveedor</th>
                                    <th>Tipo proveedor</th>
                                    <th>Estado</th>
                                    <th>Número de factura</th>
                                    <th>Fecha de recibo</th>
                                    <th>Unidades recibidas</th>
                                    <th>Transportador</th>
                                    <th>Usuario</th>
                                    <th>Editar</th>
                                </tr>
                            </thead>
                            <tbody class="tbody">
                                @if(count($receptions))
                                    @foreach($receptions as $reception)
                                    <tr>
                                        <td>{{ $reception->id }}</td>
                                        <td><a href="{{ route('adminProviderOrders.details', ['id' => $reception->provider_order_id]) }}" target="_blank">{{ $reception->provider_order_id }}</a></td>
                                        <td>{{ $reception->city }}</td>
                                        <td>{{ $reception->providerOrder->warehouse->warehouse }}</td>
                                        @if(!$reception->real_provider_id)
                                            <td>{{ $reception->provider_name }}</td>
                                            <td>{{ $reception->providerOrder->provider->provider_type }}</td>
                                        @else
                                            <td>{{ $reception->provider->name }}</td>
                                            <td>{{ $reception->provider->provider_type }}</td>
                                        @endif
                                        <td align="center">
                                            @if($reception->status == 'En proceso')
                                                <span class="badge bg-orange">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Recibido' || $reception->status == 'Almacenado')
                                                <span class="badge bg-green">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Cancelado')
                                                <span class="badge bg-red">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Revisado')
                                                <span class="badge bg-yellow">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Contabilizado')
                                                <span class="badge bg-green">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Iniciado')
                                                <span class="badge bg-blue">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Recibido con factura')
                                                <span class="badge bg-grey">{{ $reception->status }}</span>
                                            @elseif($reception->status == 'Validación')
                                                <span class="badge bg-yellow">{{ $reception->status }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $reception->invoice_number }}</td>
                                        <td>{{ date('d M Y g:i a', strtotime($reception->date)) }}</td>
                                        <td>{{ $reception->quantity_received }}</td>
                                        <td>{{ $reception->transporter }}</td>
                                        <td>{{ $reception->fullname }}</td>
                                        <td>
                                            <div class="btn-group">
                                               <a class="btn btn-xs btn-default" href="{{ route('adminMovements.editReception', ['id' => $reception->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="10" align="center">No hay datos.</td></tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="dataTables_info" id="reception-table_info">Mostrando {{ count($receptions) }} ítems</div>
                            </div>
                           </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    $(document).ready(function() {

        $('body').on('submit', '#search-form', function(event) {
            event.preventDefault();
            var data = $('#search-form').serialize();
            $('.paging-loading').show();
            $.ajax({
                url: '{{ route('adminMovements.reception') }}',
                type: 'GET',
                dataType: 'html',
                data: data,
            })
            .done(function(data) {
                $('tbody.tbody').html(data);
            })
            .fail(function(data) {
                console.debug(data);
                console.log("error al buscar.");
            })
            .always(function() {
                $('.paging-loading').hide();
            });
        });

    });

    </script>
    @endsection
@else
    @section('grid')
        @if(count($receptions))
            @foreach($receptions as $reception)
            <tr>
                <td>{{ $reception->id }}</td>
                <td><a href="{{ route('adminProviderOrders.details', ['id' => $reception->provider_order_id]) }}" target="_blank">{{ $reception->provider_order_id }}</a></td>
                <td>{{ $reception->city }}</td>
                <td>{{ $reception->providerOrder->warehouse->warehouse }}</td>
                @if(!$reception->real_provider_id)
                    <td>{{ $reception->provider_name }}</td>
                    <td>{{ $reception->providerOrder->provider->provider_type }}</td>
                @else
                    <td>{{ $reception->provider->name }}</td>
                    <td>{{ $reception->provider->provider_type }}</td>
                @endif
                <td align="center">
                    @if($reception->status == 'En proceso')
                        <span class="badge bg-orange">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Recibido' || $reception->status == 'Almacenado')
                        <span class="badge bg-green">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Cancelado')
                        <span class="badge bg-red">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Revisado')
                        <span class="badge bg-yellow">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Contabilizado')
                        <span class="badge bg-green">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Iniciado')
                        <span class="badge bg-blue">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Recibido con factura')
                        <span class="badge bg-grey">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Validación')
                        <span class="badge bg-yellow">{{ $reception->status }}</span>
                    @endif
                </td>
                <td>{{ $reception->invoice_number }}</td>
                <td>{{ date('d M Y g:i a', strtotime($reception->date)) }}</td>
                <td>{{ $reception->quantity_received }}</td>
                <td>{{ $reception->transporter }}</td>
                <td>{{ $reception->fullname }}</td>
                <td>
                    <div class="btn-group">
                       <a class="btn btn-xs btn-default" href="{{ route('adminMovements.editReception', ['id' => $reception->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="13" align="center">No hay datos.</td></tr>
        @endif
    @stop
@endif
