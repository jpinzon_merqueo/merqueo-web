@if (!Request::ajax())
    @extends('admin.layout')
    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <style type="text/css">
        #modal-credit-note .modal-dialog, #modal-products .modal-dialog, #modal-order-product-group .modal-dialog, #reception-validate-modal .modal-dialog{
            width: 70%!important;
        }
        #modal-order-product-group .modal-dialog{
            width: 90%!important;
        }
        .provider-product td{
            background: #fdfbb4 !important;
        }
        td.und-received, th.und-received{
            background: #fff3ba !important;
        }
        td.und-expected, th.und-expected{
            background: #baf2ff !important;
        }
        .right-side > .content-header h1 {
            font-size: 24px;
            margin: 0;
        }
        .buttons-container .btn {
            margin-bottom: 1rem;
        }
    </style>

    <section class="content-header">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-4">
                <h1>
                    {{ $title }}  @if ($reception) # {{ $reception->id }} @endif
                    <small>Control panel</small>
                </h1>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-8 buttons-container">
                @if ($reception && $admin_permissions['update'])
                    @if($reception->status == 'En proceso' || $reception->status == 'Validación')
                        <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Recibido']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Recibido?')">
                            <button type="button" class="btn btn-success">Actualizar estado a Recibido</button>
                        </a>
                    @endif

                    @if($reception->status == 'Recibido' && $reception->providerOrder->provider_id == 17)
                        <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Almacenado']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Almacenado?')">
                            <button type="button" class="btn btn-success">Actualizar estado a Almacenado</button>
                        </a>
                    @endif

                    @if (in_array($reception->status, ['En proceso', 'Iniciado']))
                        <a href="{{ route('adminMovements.deleteReception', ['id' => $reception->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar el recibo de bodega?')">
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </a>
                    @endif

                    @if ($reception->status == 'Almacenado' && ($admin_permissions['permission1'] || $admin_permissions['permission3']))
                        <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Revisado']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a Revisado?')">
                            <button type="button" class="btn btn-primary">Actualizar estado a Revisado</button>
                        </a>
                    @endif

                    @if ($reception->status == 'Revisado' && ($admin_permissions['permission1'] || $admin_permissions['permission3']))
                        <button type="button" class="btn btn-primary" onclick="$('#modal-credit-note').modal('show');">Agregar nota crédito</button>
                        <!-- <button type="button" class="btn btn-primary" onclick="$('#upload-credit-note').modal('show');">Agregar nota crédito</button> -->
                        <a href="{{ route('adminMovements.updateProductsStatus', ['id' => $reception->id, 'status' => 'Contabilizado']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a Contabilizado?')">
                            <button type="button" class="btn btn-danger">Actualizar productos a Contabilizado</button>
                        </a>
                    @endif

                    @if ($reception->status == 'Almacenado' && ($admin_permissions['permission1'] || $admin_permissions['permission3']))
                        <a href="{{ route('adminMovements.updateProductsStatus', ['id' => $reception->id, 'status' => 'Revisado']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a Revisado?')">
                            <button type="button" class="btn btn-danger">Actualizar productos a Revisado</button>
                        </a>
                    @endif

                    @if ($reception->status == 'Iniciado' && $reception->providerOrder->provider->provider_type != 'Marketplace')
                        <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Recibido con factura']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a Recibido con factura?')">
                            <button type="button" class="btn btn-primary">Actualizar estado a Recibido con factura</button>
                        </a>
                        <button type="button" class="btn btn-primary import-expected-quantities" onclick="$('#import-expectedQuantityGrouped-modal').modal('show');">Importar cantidades</button>
                    @endif

                    @if ($reception->status == 'Recibido con factura')
                        <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'En proceso']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a En proceso?')">
                            <button type="button" class="btn btn-primary">Actualizar estado a En proceso</button>
                        </a>
                    @elseif($reception->status == 'Iniciado' && $reception->providerOrder->provider->provider_type == 'Marketplace')
                        <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'En proceso']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a En proceso?')">
                            <button type="button" class="btn btn-primary">Actualizar estado a En proceso</button>
                        </a>
                    @endif

                    @if ($reception->status == 'Revisado' && ($admin_permissions['permission2'] || $admin_permissions['permission3']))
                    <!-- <a id="" href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Contabilizado']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a Contabilizado?')"> -->
                        <button id="update-status-reception" type="button" class="btn btn-primary">Actualizar estado a Contabilizado</button>
                        <!-- </a> -->
                        <button type="button" class="btn btn-primary upload-invoice" onclick="$('#upload-invoice').modal('show');">Cargar factura</button>
                    @endif
                    <a href="javascript:;" onclick="show_modal('products')">
                        <button type="button" class="btn btn-primary">Agregar Producto</button>
                    </a>

                    @if ($reception->status == 'Revisado' && ($admin_permissions['permission1'] || $admin_permissions['permission3']))
                        <a href="{{ route('adminMovements.receptionUploadedEPlane', ['id' => $reception->id]) }}">
                            <button type="button" class="btn btn-success">Plano E cargado</button>
                        </a>
                        <a href="{{ route('adminMovements.receptionExportEPlane', ['id' => $reception->id]) }}" target="_blank">
                            <button type="button" class="btn btn-primary">Exportar plano E</button>
                        </a>
                    @endif

                    @if (!in_array($reception->status, ['En proceso', 'Iniciado', 'Recibido con factura']))
                        <a href="{{ route('adminMovements.receptionPrintPdf', ['id' => $reception->id]) }}" target="_blank">
                            <button type="button" class="btn btn-primary">Imprimir</button>
                        </a>
                    @endif

                    @if ($reception->status == 'Contabilizado' && ( $admin_permissions['permission1'] || $admin_permissions['permission3'] ))
                        @if ( !empty($reception->movement_consecutive_p) && !empty($reception->expiration_date) )
                            <a href="{{ route('adminMovements.receptionPrintPdf', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Imprimir plano E</button>
                            </a>
                            <a href="{{ route('adminMovements.receptionExportEPlane', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Exportar plano E</button>
                            </a>
                            <a href="{{ route('adminMovements.receptionPrintPPdf', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Imprimir archivo P</button>
                            </a>
                            <a href="{{ route('adminMovements.receptionExportPPlane', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Exportar archivo P</button>
                            </a>
                        @else
                            <a href="{{ route('adminMovements.receptionPrintPdf', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Imprimir plano E</button>
                            </a>
                            <a href="{{ route('adminMovements.receptionExportEPlane', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Exportar plano E</button>
                            </a>
                            <a href="javascript:;" onclick="show_modal('p_file')">
                                <button type="button" class="btn btn-primary">Imprimir archivo P</button>
                            </a>
                            <a href="{{ route('adminMovements.receptionExportPPlane', ['id' => $reception->id]) }}" target="_blank">
                                <button type="button" class="btn btn-primary">Exportar archivo P</button>
                            </a>
                        @endif
                            <a href="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Revisado']) }}" onclick="return confirm('¿Estás seguro que deseas actualizar el estado a Revisado?')">
                                <button type="button" class="btn btn-warning">Abrir recibo</button>
                            </a>
                    @endif

                    @if ( $simple_validates->count() )
                        <button id="get-reception-validate" type="button" class="btn btn-primary btn-group">Ver incidencias del recibo</button>
                    @endif

                @endif
            </div>
        </div>
    </section>
    <section class="content">
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('errors'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b>
            <ul>
            @foreach (Session::get('errors') as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        <div class="row">
            @if ($reception)
            <div class="col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Recibo</h4>
                    <p><label>Estado</label> -
                    @if($reception->status == 'En proceso')
                        <span class="badge bg-orange">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Recibido' || $reception->status == 'Almacenado')
                        <span class="badge bg-green">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Revisado')
                        <span class="badge bg-yellow">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Contabilizado')
                        <span class="badge bg-green">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Cancelado')
                        <span class="badge bg-red">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Iniciado')
                        <span class="badge bg-blue">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Recibido con factura')
                        <span class="badge bg-grey">{{ $reception->status }}</span>
                    @elseif($reception->status == 'Validación')
                        <span class="badge bg-yellow">{{ $reception->status }}</span>
                    @endif
                    </p>
                    <p><label>Ciudad</label> - {{ $reception->city }}</p>
                    <p><label>Bodega</label> - {{ $reception->providerOrder->warehouse->warehouse }}</p>
                    <p><label>Orden de compra</label> - # <a href="{{ route('adminProviderOrders.details', ['id' => $reception->provider_order_id]) }}" target="_blank">{{ $reception->provider_order_id }}</a></p>
                    <p><label>Proveedor</label> -
                        @if(!$reception->real_provider_id)
                            {{ $reception->provider_name }}
                        @else
                            {{ $reception->provider->name }}
                        @endif

                        @if($reception->status == 'Revisado' && $admin_permissions['update'] && $admin_permissions['permission3'])
                            <a href="javascript:;" title="Editar Proveedor" onclick="show_modal('edit-provider')">Editar</a>
                        @endif
                    </p>
                    <p><label>Tipo proveedor</label> - {{ $reception->providerOrder->provider->provider_type }}</p>
                    <p><label>Fecha de creación</label> - {{ date('d M Y g:i a', strtotime($reception->date)) }}</p>
                    @if (!empty($reception->received_date))
                    <p><label>Fecha de recepción</label> - {{ date('d M Y g:i a', strtotime($reception->received_date)) }}</p>
                    @endif
                    @if (!empty($reception->storage_date))
                    <p><label>Fecha de almacenamiento</label> - {{ date('d M Y g:i a', strtotime($reception->storage_date)) }}</p>
                    @endif
                    <p><label>Creado por</label> - {{ $reception->admin_name }}</p>
                    @if ($reception->status == 'Recibido')
                    <p><label>Observaciones</label> - {{ $reception->observation }}</p>
                    @endif
                    <p><a href="javascript:;" title="Log de recibo" onclick="show_modal('reception_log')">Log de recibo</a></p>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Contabilidad</h4>
                    <p><label>Archivo plano E cargado</label> - {{ $reception->plane_e_upload_date ? 'Si' : 'No' }}</p>
                    <p><label>Archivo plano F cargado</label> - {{ $reception->plane_f_upload_date ? 'Si' : 'No' }}</p>
                </div>
            </div>
            @endif

            @if ($reception && $reception->status != 'En proceso')

            <div class="col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Transportador</h4>
                    <p><label>Número de factura</label> - {{ $reception->invoice_number }} @if($reception->invoice_url && ($admin_permissions['permission2'] || $admin_permissions['permission3']))<a href="{{ $reception->invoice_url }}" target="_blank">Ver factura</a>@endif</p>
                    <p><label>Transportador</label> - {{ $reception->transporter }}</p>
                    <p><label>Placa vehículo</label> - {{ $reception->plate }}</p>
                    <p><label>Nombre conductor</label> - {{ $reception->driver_name }}</p>
                    <p><label>Cédula del conductor</label> - {{ $reception->driver_document_number }}</p>
                 </div>
             </div>

            @else

            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" method="post" id='main-form' action="@if ($reception) {{ route('adminMovements.updateReception', ['id' => $reception->id]) }} @else {{ route('adminMovements.saveReception') }} @endif" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row form-group">
                                @if (!$reception)
                                <div class="col-xs-4">
                                    <label>Número orden de compra</label>
                                    <input type="text" class="form-control" name="provider_order_id" placeholder="Ingresa orden de compra" value="@if($reception && $reception->provider_order_id) {{ $reception->provider_order_id }} @elseif($provider_order_id != 0) {{ $provider_order_id }} @endif"
                                        @if($provider_order_id != 0 || $reception)
                                            readonly="readonly"
                                        @endif
                                    >
                                </div>
                                @else
                                <input type="hidden" value="{{ $reception->provider_order_id }}" name="provider_order_id">
                                @endif
                                <div class="col-xs-4">
                                    <label>Número de factura</label>
                                    <input type="text" class="form-control required" name="invoice_number" placeholder="Ingresa número de factura"  value="@if ($reception){{ $reception->invoice_number }}@else{{ $post['invoice_number'] or '' }}@endif">
                                </div>
                                <div class="col-xs-4">
                                    <label>Transportador</label>
                                    <input type="text" class="form-control" name="transporter" placeholder="Ingresa transportador" value="@if ($reception){{ $reception->transporter }}@else{{ $post['transporter'] or '' }}@endif">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-xs-4">
                                    <label>Número de placa</label>
                                    <input type="text" class="form-control" name="plate" placeholder="Ingresa número de placa"  value="@if ($reception){{ $reception->plate }}@else{{ $post['plate'] or '' }}@endif">
                                </div>
                                <div class="col-xs-4">
                                    <label>Nombre del conductor</label>
                                    <input type="text" class="form-control" name="driver_name" placeholder="Ingresa nombre del conductor"  value="@if ($reception){{ $reception->driver_name }}@else{{ $post['driver_name'] or '' }}@endif">
                                </div>
                                <div class="col-xs-4">
                                    <label>Cédula del conductor</label>
                                    <input type="text" class="form-control" name="driver_document_number" placeholder="Ingresa cédula del conductor"  value="@if ($reception){{ $reception->driver_document_number }}@else{{ $post['driver_document_number'] or '' }}@endif">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <label>Observaciones</label>
                                    <textarea class="form-control" name="observation" placeholder="Ingresa observación">@if ($reception){{ $reception->observation }}@else{{ $post['observation'] or '' }}@endif</textarea>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Guardar </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @endif

        </div>

         @if ($reception)
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <legend>Productos recibidos</legend>
                        <label>Buscar</label>
                        <input type="text" placeholder="Referencia, PLU, nombre" id="search-products" class="form-control"><br>
                        <div id="reception_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b>Alerta!</b> <div class="message"></div>
                        </div>
                        <div id="reception_products_success" class="alert alert-success alert-dismissable" style="display: none;">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b>Alerta!</b> <div class="message"></div>
                        </div>
                        <div id="products_table_container">
                            <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </section>
    @if($reception)
    <!-- Update Quantity Product Modal -->
    <!-- Candidata a ser borrada -->
    <!-- <div class="modal fade" id="modal-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Producto</h4>
                </div>
                <form method="POST" id="form-contact" class="form-modal" action="{{-- route('adminMovements.updateProductPartialQuantity', ['id' => $reception->id]) --}}">
                    <div class="modal-body">
                        <input type="hidden" id="modal_store_product_id" name="store_product_id">
                        <div class="form-group">
                            <label for="">Unidades recibidas</label>
                            <input class="form-control new-email" type="text" placeholder="Ingresa las unidades disponibles" id="modal_quantity_received" name="quantity_received" required="required">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="modal-update-product">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div> -->

    <!-- Show Product Group Modal -->
    <div class="modal fade" id="modal-order-product-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Productos del Producto Agrupado</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button id="update-group-quantity" type="button" class="btn btn-default" style="display: none;">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Cost Modal -->
    <div class="modal fade" id="edit-cost-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar Valores - <span id="reception-product-name"></span></h4>
                </div>
                <form method="post" action="{{ route('adminMovements.updateProductCost', ['id' => $reception->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="reception-store-product-id" name="store_product_id">
                        @if ( $admin_permissions['permission1'] || $admin_permissions['permission3'] )
                            @if ( $reception->status == 'Almacenado' || $admin_permissions['permission3'] )
                                <!-- <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="show_products" style="display: none;">
                                                <label class="control-label">
                                                    Productos del proveedor
                                                </label>
                                                <select class="form-control" name="provider_product" id="provider_product" disabled="disabled">
                                                    @foreach ($products as $product)
                                                        <option
                                                            value="{{ $product->id }}"
                                                            data-cost="{{ $product->cost }}"
                                                            data-iva="{{ $product->iva }}"
                                                            data-consumption_tax="{{ $product->consumption_tax }}">
                                                                {{ $product->name }} {{ $product->quantity }} {{ $product->unit }} @if ( $product->type == 'Proveedor' )- ({{ $product->type }})  @endif
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="control-label" style="color: #FFF;">Botones de acción</label>
                                            <button class="btn btn-success show_products_btn" type="button">Cambiar producto</button>
                                            <button class="btn btn-danger hide_products_btn" style="display: none;" type="button">Cancelar</button>
                                        </div>
                                    </div>
                                </div> -->
                            @endif

                        <div class="form-group">
                            <label class="control-label">Unidades en factura</label>
                            <input type="number" class="form-control required" name="quantity_expected" id="reception-product-quantity-expected" placeholder="Ingresa unidades en factura">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Unidades recibidas</label>
                            <input type="number" class="form-control required" name="quantity_received" id="reception-product-quantity-received" placeholder="Ingresa unidades recibidas">
                        </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label">Costo base unitario</label>
                            <input type="text" class="form-control required" name="cost" id="reception-product-cost" placeholder="Ingresa el costo base unitario">
                        </div>
                        @if ($admin_permissions['permission2'] || $admin_permissions['permission3'])
                        <div class="form-group">
                            <label class="control-label">IVA %</label>
                            <input type="text" class="form-control required" name="iva" id="reception-product-iva" placeholder="Ingresa el porcentaje de IVA">
                        </div>
                        <div class="form-group">
                            <label class="control-label">IVA</label>
                            <input type="text" class="form-control required" name="iva_amount" id="reception-product-iva-amount" placeholder="Ingresa el valor del IVA" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Impuesto al consumo</label>
                            <input type="text" class="form-control required" name="consumption_tax" id="reception-product-consumption-tax" placeholder="Ingresa el impuesto al consumo">
                        </div>
                            @if ($reception->status == 'Almacenado' || $reception->status == 'Revisado')
                            <div class="form-group">
                                <label for="is_given">Es bonificado</label>
                                <select name="is_given" id="is_given" class="form-control">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                            <div class="form-group given_quantity">
                                <label for="given_quantity">Cantidad bonificada</label>
                                <input type="number" min="1" id="given_quantity" name="given_quantity" class="form-control">
                            </div>
                            @endif
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Edit Cost Modal -->
    <div class="modal fade" id="edit-cost-modal-grouped" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar Valores - <span id="reception-product-grouped-name"></span></h4>
                </div>
                <form method="post" action="{{ route('adminMovements.updateProductCost', ['id' => $reception->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="provider_order_details_id" name="provider_order_details_id">
                        <input type="hidden" id="provider_order_detail_product_group_id" name="provider_order_detail_product_group_id">
                        <input type="hidden" id="provider_order_reception_detail_group_id" name="provider_order_reception_detail_group_id">
                        <input type="hidden" id="provider_order_reception_detail_product_group_id" name="provider_order_reception_detail_product_group_id">
                        @if ( $admin_permissions['permission1'] || $admin_permissions['permission3'] )
                        <div class="form-group">
                            <label class="control-label">Unidades en factura</label>
                            <input type="number" class="form-control required" name="quantity_expected" id="reception-product-grouped-quantity-expected" placeholder="Ingresa unidades en factura">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Unidades recibidas</label>
                            <input type="number" class="form-control required" name="quantity_received" id="reception-product-grouped-quantity-received" placeholder="Ingresa unidades recibidas">
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="control-label">Costo base unitario</label>
                            <input type="text" class="form-control required" name="cost" id="reception-product-grouped-cost" placeholder="Ingresa el costo base unitario">
                        </div>
                        @if ($admin_permissions['permission2'] || $admin_permissions['permission3'])
                        <div class="form-group">
                            <label class="control-label">IVA %</label>
                            <input type="text" class="form-control required" name="iva" id="reception-product-grouped-iva" placeholder="Ingresa el porcentaje de IVA">
                        </div>
                        <div class="form-group">
                            <label class="control-label">IVA</label>
                            <input type="text" class="form-control required" name="iva_amount" id="reception-product-grouped-iva-amount" placeholder="Ingresa el valor del IVA" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Impuesto al consumo</label>
                            <input type="text" class="form-control required" name="consumption_tax" id="reception-product-grouped-consumption-tax" placeholder="Ingresa el impuesto al consumo">
                        </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Cost Modal -->
    <div class="modal fade" id="edit-expiration-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Fecha de vencimiento - <span class="reception-product-name"></span></h4>
                </div>
                <form method="post" action="" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" class="reception-store-product-id" name="store_product_id">
                        <div class="form-group">
                            <label class="control-label">Fecha de vencimiento</label>
                            <input type="text" class="form-control required" name="expiration_date" id="expiration_date" placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary update-product" data-status="Recibido">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Add products Modal -->
    <div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Producto a Pedido</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="" class="modal-search-form">
                                <div class="row error-search unseen">
                                    <div class="col-xs-12">
                                        <div class="alert alert-danger unseen"></div>
                                        <div class="alert alert-success unseen"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table width="100%" class="modal-product-request-table">
                                            <tbody>
                                                <tr>
                                                    <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                    <td>
                                                        <input type="text" placeholder="Nombre" name="s" id="s" class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
                                                    </td>
                                                    <td colspan="2" align="left"><button type="button" id="btn-modal-search" class="btn btn-primary" data-order_id=""  data-store_id="">Buscar</button>&nbsp;&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="2"><span class="error" style="display: none;">Campo requerido</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="modal-products-request pre-scrollable">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Referencia</th>
                                                    <th>Nombre</th>
                                                    <th>Unidad de medida</th>
                                                    <th>Precio</th>
                                                    <th>Stock actual</th>
                                                    <th>Cantidad</th>
                                                    <th>Agregar</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody">
                                                <tr><td colspan="14" align="center">&nbsp;</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div align="center" class="modal-products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Log Order Modal -->
    <div class="modal fade" id="modal-reception_log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 900px; width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Log de Recibo</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Usuario</th>
                                                <th>Log</th>
                                                <th>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($logs))
                                            @foreach($logs as $log)
                                            <tr>
                                                <td>{{ $log->fullname }}</td>
                                                <td>{{ $log->type }}</td>
                                                <td>{{ date_format($log->created_at, 'd/m/Y h:i:s A') }}</td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr><td colspan="6" align="center">No hay datos.</td></tr>
                                            @endif
                                        </tbody>
                                    </table><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- ICA Modal -->
    <div class="modal fade" id="modal-taxes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Impuestos</h4>
                </div>
                <form id="taxes-form" action="{{ route('adminMovements.updateStatusReception', ['id' => $reception->id, 'status' => 'Contabilizado']) }}" method="POST" class="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4>Rete ICA</h4>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Porcentajes rete ICA</label>
                                        <select name="rete_ica_options" id="rete_ica_options" class="form-control">
                                            <option value="No Aplica">No Aplica</option>
                                            @foreach ($ica_taxes as $key => $ica_tax)
                                                <option value="{{ $key }}" data-wtcodeica="{{ $ica_tax->wtcode }}" data-account="{{ $ica_tax->account }}" data-percentage="{{ $ica_tax->percentage }}"><b>cuenta:</b> {{ $ica_tax->account }} - {{ $ica_tax->percentage }}%</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Código cuenta</label>
                                        <input type="text" class="form-control" name="wtcode_ica" id="wtcode_ica" value="" readonly="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Cuenta rete ICA</label>
                                        <input type="text" class="form-control" name="rete_ica_account" id="rete_ica_account" value="" readonly="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Porcentaje rete ICA</label>
                                        <input type="text" class="form-control" name="rete_ica_percentage" id="rete_ica_percentage" value="" readonly="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Valor rete ICA</label>
                                        <input type="text" class="form-control" name="rete_ica_amount" id="rete_ica_amount" data-total="{{ round($reception->getSubTotal()) - round($reception->getSubTotal(false)) }}" >
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4>Rete fuente</h4>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Porcentajes rete Fuente</label>
                                        <select name="rete_fuente_options" id="rete_fuente_options" class="form-control">
                                            <option value="No Aplica">No Aplica</option>
                                            @foreach ($fuente_taxes as $key => $fuente_tax)
                                                <option value="{{ $key }}" data-wtcodefuente="{{ $fuente_tax->wtcode }}" data-account="{{ $fuente_tax->account }}" data-percentage="{{ $fuente_tax->percentage }}"><b>cuenta:</b> {{ $fuente_tax->account }} - {{ $fuente_tax->percentage }}%</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Código cuenta</label>
                                        <input type="text" class="form-control" name="wtcode_fuente" id="wtcode_fuente" value="" readonly="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Cuenta rete fuente</label>
                                        <input type="text" class="form-control" name="rete_fuente_account" id="rete_fuente_account" value="" readonly="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Porcentaje rete fuente</label>
                                        <input type="text" class="form-control" name="rete_fuente_percentage" id="rete_fuente_percentage" value="" readonly="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Valor rete fuente</label>
                                        <input type="text" class="form-control" name="rete_fuente_amount" id="rete_fuente_amount" data-total="{{ $reception->getSubTotal() - $reception->getSubTotal(false) }}" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- archivo P Modal -->
    <div class="modal fade" id="modal-p_file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Datos de archivo P</h4>
                </div>
                <form id="taxes-form" action="{{ route('adminMovements.updateReceptionPData', ['id' => $reception->id]) }}" method="POST" class="form-modal">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Fecha de vencimiento</label>
                                        <input type="text" class="form-control" id="reception_expiration_date" name="reception_expiration_date" required="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Fecha de comprobante</label>
                                        <input type="text" class="form-control" id="reception_voucher_date" name="reception_voucher_date" required="">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>Número de factura</label>
                                        <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="{{ $reception->invoice_number }}" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Cost Modal -->
    <div class="modal fade" id="set-expiration-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Fecha de vencimiento - <span class="reception-product-name"></span></h4>
                </div>
                <form method="post" action="" class="form-modal set_expiration_date_form">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Fecha de vencimiento</label>
                            <input type="text" class="form-control required" name="set_expiration_date" id="set_expiration_date" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary set_expiration_date_update" data-status="Recibido">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Expected Quantity Modal -->
    <div class="modal fade" id="edit-expectedQuantity-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar unidades en factura - <span id="expectedQuantity-product-name"></span></h4>
                </div>
                <form method="post" action="{{ route('adminMovements.updateProductExpectedQuantity', ['id' => $reception->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="expectedQuantity-store-product-id" name="store_product_id">
                        <input type="hidden" id="expectedQuantity-product-quantity-order" name="quantity_order">
                        <input type="hidden" id="expectedQuantity-reception-detail-id" name="reception_detail_id">
                        @if ( $admin_permissions['permission1'] || $admin_permissions['permission3'] )
                        <div class="form-group">
                            <label class="control-label">Unidades en factura</label>
                            <input type="number" class="form-control required" name="quantity_expected" id="expectedQuantity-product-quantity-expected" placeholder="Ingresa unidades esperadas">
                        </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Expected Quantity Grouped Modal -->
    <div class="modal fade" id="edit-expectedQuantityGrouped-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar unidades en factura - <span id="expectedQuantityGrouped-product-name"></span></h4>
                </div>
                <form method="post" action="{{ route('adminMovements.updateProductExpectedQuantity', ['id' => $reception->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="expectedQuantityGrouped-store-product-id" name="store_product_id">
                        <input type="hidden" id="expectedQuantityGrouped-product-quantity-order" name="quantity_order">
                        <input type="hidden" id="expectedQuantityGrouped-reception-detail-id" name="reception_detail_product_group_id">
                        @if ( $admin_permissions['permission1'] || $admin_permissions['permission3'] )
                        <div class="form-group">
                            <label class="control-label">Unidades en factura</label>
                            <input type="number" class="form-control required" name="quantity_expected" id="expectedQuantityGrouped-product-quantity-expected" placeholder="Ingresa unidades esperadas">
                        </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- import Expected Quantity Modal -->
    <div class="modal fade" id="import-expectedQuantityGrouped-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Importar unidades en factura</h4>
                </div>
                <form method="post" action="{{ route('adminMovements.importProductExpectedQuantity', ['id' => $reception->id]) }}" class="form-modal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        @if ( $admin_permissions['permission1'] || $admin_permissions['permission3'] )
                        <div class="form-group">
                            <label class="control-label">Importar archivo</label>
                            <input type="file" class="form-control required" name="file" id="expectedQuantity-file" placeholder="Archivo de unidades esperadas">
                        </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="upload-invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cargar Factura</h4>
                </div>
                <form method="post" action="{{ route('adminMovements.uploadInvoice', ['id' => $reception->id]) }}" class="form-modal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label for="invoice_url">Importar factura</label>
                            <input type="file" class="form-control required" name="invoice_url" id="invoice_url" placeholder="Imagen de la factura">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal para notas crédito -->
    @if($reception)
       <!-- <div class="modal fade" id="upload-credit-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Nota crédito</h4>
                    </div>
                    <form method="post" id="credit-note" action="{{ route('adminMovements.receptionUpdateCreditNote', ['id' => $reception->id]) }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="unseen alert alert-danger form-has-errors"></div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <label for="credit_note_reason">Razón nota crédito</label>
                                    <input type="text" maxlength="255" id="credit_note_reason" name="credit_note_reason" class="form-control" value="{{ $reception->credit_note_reason }}">
                                </div>
                                <div class="col-xs-12 form-group">
                                    <label for="credit_note_amount">Valor</label>
                                    <input type="number" min="0" id="credit_note_amount" name="credit_note_amount" class="form-control" value="{{ $reception->credit_note_amount }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    @endif

    <!-- Modal para notas crédito -->
    @if($reception)
    <div class="modal fade" id="modal-credit-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div id="content-credit-note">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Nota crédito <span v-if="credit_note_number">#@{{ credit_note_number }}</span></h4>
                    </div>
                    <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form @submit.prevent="searchProducts">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="modal-title">Buscar Productos</h4>
                                                <table width="100%" class="modal-product-request-table">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                        <td>
                                                            <input type="text" placeholder="Referencia, PLU, nombre" name="productName" v-model="formData.productName" required class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
                                                        </td>
                                                        <td colspan="2" align="left"><button type="submit" :disabled="disabledSearch" class="btn btn-primary">Buscar</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td align="center" colspan="1"><img src="{{ asset_url() }}/img/loading.gif" v-if="disabledSearch"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-default">
                                        <div class="box-body pre-scrollable">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Producto</th>
                                                    <th>Unid. solicitadas</th>
                                                    <th class="und-expected">Unid. en factura</th>
                                                    <th class="und-received">Unid. recibidas</th>
                                                    <th>Unid. nota crédito</th>
                                                    <th>Razón nota crédito</th>
                                                    <th>Imp. consumo</th>
                                                    <th>Costo base</th>
                                                    <th>Costo</th>
                                                    <th>IVA %</th>
                                                    <th>Agregar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(product, index) in productSearch" :key="product.id">
                                                    <td><img :src="product.image_url" width="58" max-height="58" class="img-responsive"/></td>
                                                    <td>@{{ product.product_name }}</td>
                                                    <td align="center">@{{ product.quantity_order }}</td>
                                                    <td align="center" class="und-expected">@{{ product.quantity_expected }}</td>
                                                    <td align="center" class="und-received">@{{ product.quantity_received }}</td>
                                                    <td>
                                                        <input type="number" name="credit_note_reason" v-model="product.quantity_calculate" class="form-control">
                                                        <small class="error" v-if="formData.errors.quantity_calculate.error && formData.errors.quantity_calculate.index == index">@{{ formData.errors.quantity_calculate.message }}</small>
                                                    </td>
                                                    <td>
                                                        <select name="creditNoteReasons" v-model="product.reason_credit_note" class="form-control">
                                                            <option v-for="(reason, index) in formData.creditNoteReasons" :value="reason.reason">@{{ reason.reason }}</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="number" name="consumption_tax_credit_note" v-model="product.consumption_tax_credit_note" class="form-control">
                                                        <small class="error" v-if="formData.errors.consumption_tax_credit_note.error && formData.errors.consumption_tax_credit_note.index == index">@{{ formData.errors.consumption_tax_credit_note.message }}</small>
                                                    </td>
                                                    <td align="right">$@{{ formatPrice(product.base_cost) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.cost) }}</td>
                                                    <td align="right">@{{ product.iva }}%</td>
                                                    <td><button @click="addCreditNote(product, index)" class="btn btn-xs btn-success btn-add-product-to-credit"><span class="glyphicon glyphicon-plus"></span></button></td>
                                                </tr>
                                                <tr v-if="productSearch.length == 0">
                                                    <td colspan="15" align="center">No se encontraron productos</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" v-if="disableDivAdd">
                                <div class="col-xs-12">
                                    <h4 class="modal-title">Productos a agregar</h4>
                                    <div class="box box-default">
                                        <div class="box-body pre-scrollable">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Producto</th>
                                                    <th>Unid. solicitadas</th>
                                                    <th class="und-expected">Unid. en factura</th>
                                                    <th class="und-received">Unid. recibidas</th>
                                                    <th>Unid. nota crédito</th>
                                                    <th>Razón nota crédito</th>
                                                    <th>Costo base</th>
                                                    <th>Costo</th>
                                                    <th>IVA %</th>
                                                    <th>IVA</th>
                                                    <th>Imp. consumo</th>
                                                    <th>Sub total</th>
                                                    <th>Costo total</th>
                                                    <th>Eliminar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(product, index) in addProductsCreditNote" :key="product.id">
                                                    <td><img :src="product.image_url" width="58" max-height="58" class="img-responsive"/></td>
                                                    <td>@{{ product.product_name }}</td>
                                                    <td align="center">@{{ product.quantity_order }}</td>
                                                    <td align="center" class="und-expected">@{{ product.quantity_expected }}</td>
                                                    <td align="center" class="und-received">@{{ product.quantity_received }}</td>
                                                    <td align="center">@{{ product.quantity_calculate }}</td>
                                                    <td>@{{ product.reason_credit_note }}</td>
                                                    <td align="right">$@{{ formatPrice(product.base_cost) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.cost) }}</td>
                                                    <td align="right">@{{ product.iva }}%</td>
                                                    <td align="right">$@{{ formatPrice(product.iva_amount) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.consumption_tax_credit_note) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.sub_total_cost) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.total_cost) }}</td>
                                                    <td><button @click="removeAddCreditNote(product, index)" class="btn btn-xs btn-danger btn-add-product-to-credit"><span class="glyphicon glyphicon-minus"></span></button></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" v-if="disableDivProductsCN">
                                <div class="col-xs-12">
                                    <h4 class="modal-title">Productos en la nota crédito</h4>
                                    <div class="box box-default">
                                        <div class="box-body pre-scrollable">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Producto</th>
                                                    <th>Unid. solicitadas</th>
                                                    <th class="und-expected">Unid. en factura</th>
                                                    <th class="und-received">Unid. recibidas</th>
                                                    <th>Unid. nota crédito</th>
                                                    <th>Razón nota crédito</th>
                                                    <th>Costo base</th>
                                                    <th>Costo</th>
                                                    <th>IVA %</th>
                                                    <th>IVA</th>
                                                    <th>Imp. consumo</th>
                                                    <th>Sub total</th>
                                                    <th>Costo total</th>
                                                    <th>Eliminar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="product in productsCreditNote" :key="product.id">
                                                    <td><img :src="product.image_url" width="58" max-height="58" class="img-responsive"/></td>
                                                    <td>@{{ product.product_name }}</td>
                                                    <td align="center">@{{ product.quantity_order }}</td>
                                                    <td align="center" class="und-expected">@{{ product.quantity_expected }}</td>
                                                    <td align="center" class="und-received">@{{ product.quantity_received }}</td>
                                                    <td align="center">@{{ product.quantity_credit_note }}</td>
                                                    <td>@{{ product.reason_credit_note }}</td>
                                                    <td align="right">$@{{ formatPrice(product.base_cost) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.cost) }}</td>
                                                    <td align="right">@{{ product.iva }}%</td>
                                                    <td align="right">$@{{ formatPrice(product.iva_amount) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.consumption_tax_credit_note) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.sub_total_cost) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.total_cost) }}</td>
                                                    <td>
                                                        <button @click="removeCreditNote(product)" :disabled="disableRemove" class="btn btn-xs btn-danger btn-add-product-to-credit"><span class="glyphicon glyphicon-minus"></span></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="13"><b>Sub Total: </b></td>
                                                    <td>$@{{ formatPrice(total_cost) }}</td>
                                                </tr>
                                                <tr v-if="total_iva != 0">
                                                    <td align="right" colspan="13"><b>IVA: </b></td>
                                                    <td align="right">$@{{ formatPrice(total_iva) }}</td>
                                                </tr>
                                                <tr v-if="rete_ica != 0">
                                                    <td align="right" colspan="13"><b>Rete ICA:</b></td>
                                                    <td align="right" v-if="total_cost != 0">$@{{ formatPrice(rete_ica) }}</td>
                                                    <td align="right" v-else>$@{{ formatPrice(0) }}</td>
                                                </tr>
                                                <tr v-if="rete_fuente != 0">
                                                    <td align="right" colspan="13"><b>Rete fuente:</b></td>
                                                    <td align="right" v-if="total_cost != 0">$@{{ formatPrice(rete_ica) }}</td>
                                                    <td align="right" v-else>$@{{ formatPrice(0) }}</td>
                                                </tr>
                                                <tr v-if="total_consumption_tax_credit_note != 0">
                                                    <td align="right" colspan="13"><b>Impuesto al consumo:</b></td>
                                                    <td align="right">$@{{ formatPrice(total_consumption_tax_credit_note) }}</td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="13"><b>Total: </b></td><td>$@{{ formatPrice(total) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <form @submit.prevent="updateProductsCreditNote">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" :disabled="disableSave" class="btn btn-primary">Guardar</button>
                            <img src="{{ asset_url() }}/img/loading.gif" v-if="loader">
                        </div>
                    </form>
            </div>
            </div>
        </div>
    </div>

    <!-- Update Real Provider Modal -->
    <div class="modal fade" id="modal-edit-provider" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Proveedor</h4>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                    <form action="{{ route('adminMovements.receptionUpdateRealProvider', ['id' => $reception->id]) }}" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="real_provider_id">Proveedor</label>
                                    <select name="real_provider_id" class="form-control">
                                        <option value=""> Selecciona </option>
                                        @foreach($providers as $id => $value)
                                            <option value="{{ $id }}" @if($provider->id == $id) selected @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- upload invoice -->
    <div class="modal fade" id="reception-validate-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Incidencias</h4>
                </div>
                <form method="POST" action="{{ route('adminMovements.updateReceptionValidate', ['id' => $reception->id]) }}" class="">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        @if ( $admin_permissions['permission1'] || $admin_permissions['permission3'] )
                            <table id="validates-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Referencia sugerida</th>
                                        <th>Unidades</th>
                                        <th>Tipo incidencia</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if ( $simple_validates->count() )
                                    @foreach ($simple_validates as $validate)
                                        @if ($validate->type != 'Actualización de referencia')
                                            <tr>
                                                @if ( !is_null($validate->providerOrderReceptionDetail) )
                                                <td>{{ $validate->providerOrderReceptionDetail->storeProduct->product->name }}</td>
                                                @elseif ( !is_null($validate->providerOrderReceptionDetailProductGroup) )
                                                <td>{{ $validate->providerOrderReceptionDetailProductGroup->storeProduct->product->name }}</td>
                                                @else
                                                <td></td>
                                                @endif
                                                <td>{{ $validate->product_reference }}</td>
                                                <td>{{ $validate->quantity_difference }}</td>
                                                <td>{{ $validate->type }}</td>
                                                <td>
                                                    <select name="validate[{{ $validate->id }}]" id="tipification" class="form-control" @if ( !is_null($validate->tipification) ) disabled="disabled" @endif>
                                                        <option value="">-Seleccione una opción-</option>
                                                        @foreach($provider_order_reject_reasons as $provider_order_reject_reason)
                                                            <option value="{{$provider_order_reject_reason->typification}}" @if ( $validate->tipification == $provider_order_reject_reason->typification ) selected="selected" @endif>{{$provider_order_reject_reason->typification}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    @include('admin.providers.movements.js.reception_form_js')
    @if ($reception && $reception->status == 'Revisado' && ($admin_permissions['permission1'] || $admin_permissions['permission3']))
        @include('admin.providers.movements.js.reception_form_vue')
    @endif
    @stop
@endif
@if (Request::ajax() && $section == 'products_table_container')
    @section('products_table_container')
    <script type="text/javascript">
        function show_modal(action, reference)
        {
            reference = reference || 0;

            if (action == 'order-product-group')
            {
                var provider_order_reception_detail_id = reference;
                $.ajax({
                 url: "{{ route('adminMovements.getProductGroupAjax', ['id' => $reception->id]) }}",
                 data: { provider_order_reception_detail_id: provider_order_reception_detail_id, action: action },
                 type: 'get',
                 dataType: 'html',
                 success:
                    function(response) {
                        $('#modal-order-product-group .modal-body').html(response);
                    }
                });
            }

            $('#modal-' + action).modal('show');
        }
        function show_modal_edit_qty(action, reference)
        {
            reference = reference || 0;

            if (action == 'order-product-group-qty')
            {
                var provider_order_reception_detail_id = reference;
                $.ajax({
                 url: "{{ route('adminMovements.getProductGroupAjax', ['id' => $reception->id]) }}",
                 data: { provider_order_reception_detail_id: provider_order_reception_detail_id, action: action },
                 type: 'get',
                 dataType: 'html',
                 success:
                    function(response) {
                        $('#modal-order-product-group .modal-body').html(response);
                        $('.expiration_date').datetimepicker({
                            format: 'DD/MM/YYYY',
                        });
                        $('#update-group-quantity').show();
                    }
                });
            }

            $('#modal-order-product-group').modal('show');
        }
    </script>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Imagen</th>
                <th>ID</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Producto</th>
                <th>Unid. solicitadas</th>
                <th>Embalaje</th>
                <th>Embalaje a solicitar</th>
                <th class="und-expected">Unid. en factura</th>
                <th class="und-received">Unid. recibidas</th>
                @if ( $reception->status != 'En proceso' )
                    <th>Costo base unitario</th>
                    <th>Costo unitario</th>
                    <th>IVA %</th>
                    <th>IVA</th>
                    <th>Imp. consumo</th>
                    <th>Sub total</th>
                    <th>Costo total</th>
                @endif
                    <th>Estado</th>
                @if ( ($admin_permissions['permission1'] || $admin_permissions['permission2'] || $admin_permissions['permission3']) )
                    <th>Estado contable</th>
                @endif
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            @if (count($products))
              @foreach($products as $product)
                <tr @if ($product->type == 'Proveedor') class="provider-product" @endif>
                    <td align="center"><img src="{{ $product->image_url }}" height ="50px"></td>
                    <td>{{ $product->product_id }}</td>
                    <td>{{ $product->reference }}</td>
                    <td>{{ $product->plu }}</td>
                    <td>
                        {{ $product->product_name }}
                            @if ($product->type == 'Proveedor')
                                <p>
                                    <a href="javascript:;" title="Productos" onclick="show_modal('order-product-group', {{ $product->id }})">Ver productos</a>
                                </p>
                            @endif
                    </td>
                    <td align="center">{{ $product->quantity_order }}</td>
                    <td align="center">{{ $product->pack_description }}</td>
                    <td align="center">
                        {{ $product->quantity_pack }}
                    </td>
                    <td class="und-expected" align="center">{{ $product->quantity_expected }}</td>
                    <td align="center" id="quantity_received-{{ $product->store_product_id }}" class="und-received">
                    @if ( $reception->status == 'En proceso' )
                        @if ( $is_search )
                            @if ( $product->type == 'Simple' )
                                <input type="number" data-store_product_id="{{ $product->store_product_id }}" data-handle_expiration_date="{{ $product->handle_expiration_date }}" id="quantity_received_input" name="quantity_received_input" class="form-control text-center" style="font-size: 2rem; font-weight: bold;">
                            @elseif($product->type == 'Proveedor')
                                <button class="btn btn-danger" onclick="show_modal_edit_qty('order-product-group-qty', {{ $product->id }})">Asignar cantidades</button>
                            @endif
                        @else
                            {{ $product->quantity_received }}
                        @endif
                    @else
                        {{ $product->quantity_received }}
                    @endif
                    </td>
                    @if ( $reception->status != 'En proceso' )
                        <td align="right">${{ number_format($product->base_cost, 0, ',', '.') }}</td>
                        <td align="right">${{ number_format($product->cost, 0, ',', '.') }}</td>
                        <td align="right">{{ $product->iva }}%</td>
                        <td align="right">${{ number_format($product->iva_amount, 0, ',', '.') }}</td>
                        <td align="right">${{ number_format($product->consumption_tax, 0, ',', '.') }}</td>
                        <td align="right">${{ number_format( $product->sub_total_cost , 0, ',', '.') }}</td>
                        <td align="right">${{ number_format( $product->total_cost , 0, ',', '.') }}</td>
                    @endif
                    <td align="center" id="status-{{ $product->store_product_id }}">
                        @if($product->status == 'Recibido')
                            <span class="badge bg-green">Recibido</span>
                        @elseif($product->status == 'No recibido')
                            <span class="badge bg-red">No recibido</span>
                        @elseif($product->status == 'Parcialmente recibido')
                            <span class="badge bg-orange">Parcialmente recibido</span>
                        @elseif($product->status == 'Dañado')
                            <span class="badge bg-red">Dañado</span>
                        @else
                            <span class="badge bg-orange">Pendiente</span>
                        @endif
                    </td>
                    @if ( ($admin_permissions['permission1'] || $admin_permissions['permission2'] || $admin_permissions['permission3']) )
                    <td align="center" id="accounting-status-{{ $product->store_product_id }}">
                        @if($product->accounting_status == 'Revisado')
                            <span class="badge bg-yellow">Revisado</span>
                        @elseif($product->accounting_status == 'Contabilizado')
                            <span class="badge bg-green">Contabilizado</span>
                        @endif
                    </td>
                    @endif
                    <td>
                        @if ( $product->accounting_status != 'Contabilizado' || $admin_permissions['permission3'] )
                            @if( ( ($provider_order->status == 'Pendiente' || $provider_order->status == 'Enviada') && $reception->status == 'En proceso' )
                            || ($admin_permissions['permission1'] || $admin_permissions['permission2'] || $admin_permissions['permission3']) )
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    @if ($product->reception_status == 'En proceso' && $product->type != 'Proveedor')
                                        <li>
                                            <a class="update-product" href="javascript:;" data-product="{{ $product->store_product_id }}" data-status="{{ 'No recibido' }}">No recibido</a>
                                        </li>
                                        <li>
                                            <a class="update-product" href="javascript:;" data-product="{{ $product->store_product_id }}" data-status="{{ 'Dañado' }}">Dañado</a>
                                        </li>
                                    @endif
                                    @if ( $product->reception_status == 'Almacenado' && ($admin_permissions['permission1'] || $admin_permissions['permission3']) )
                                        @if ( $product->type != 'Proveedor' )
                                            <li>
                                                <a class="update-product" href="javascript:;" data-product="{{ $product->store_product_id }}" data-status="{{ 'No recibido' }}">No recibido</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a class="update-accounting-status" href="javascript:;" data-product="{{ $product->store_product_id }}" data-accounting_status="{{ 'Revisado' }}">Revisado</a>
                                        </li>
                                    @endif
                                    @if ( $product->reception_status == 'Revisado' && ($admin_permissions['permission2'] || $admin_permissions['permission3']) )
                                        <li>
                                            <a class="update-accounting-status" href="javascript:;" data-product="{{ $product->store_product_id }}" data-accounting_status="{{ 'Contabilizado' }}">Contabilizado</a>
                                        </li>
                                        <li class="divider"></li>
                                    @endif
                                    @if ( ($admin_permissions['permission1'] || $admin_permissions['permission2'] || $admin_permissions['permission3']) && $product->type != 'Proveedor' && $reception->status != 'Recibido con factura')
                                    <li>
                                        @if ($reception->status == 'Iniciado')
                                            <a href="javascript:;" onclick="expected_quantity_modal.set_edit_form_data('{{ addslashes($product->product_name) }}', {{$product->store_product_id}}, {{$product->quantity_expected}}, {{ $product->quantity_order }},{{$product->provider_order_reception_details_id}});">
                                                Editar cantidades
                                            </a>
                                        @else
                                            <a href="javascript:;" onclick="edit_cost_modal.set_edit_form_data({{ $product->store_product_id }}, '{{ addslashes($product->product_name) }}', {{ $product->base_cost }}, {{ $product->iva }}, {{ $product->consumption_tax }}, {{$product->quantity_expected}}, {{ $product->quantity_received }}, {{ $product->is_given }}, {{ $product->given_quantity }});">
                                                Editar valores
                                            </a>
                                        @endif
                                    </li>
                                    @endif
                                </ul>
                            </div>
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
                @if ( $reception->status != 'En proceso' )
                <tr>
                    <td align="right" colspan="16"><b>Sub Total:</b></td>
                    <td align="right"><b>${{ number_format($total_cost, 0, ',', '.') }}</b></td>
                </tr>
                @foreach ($total_ivas as $total_iva)
                    <tr>
                        <td align="right" colspan="16"><b>IVA {{ $total_iva['iva'] }}%:</b></td>
                        <td align="right"><b>${{ number_format($total_iva['total'], 0, ',', '.') }}</b></td>
                    </tr>
                @endforeach
                @if ( $reception->rete_ica_amount )
                <tr>
                    <td align="right" colspan="16"><b>Rete ICA:</b></td>
                    @if ( $total_cost != 0 )
                        <td align="right"><b>${{ number_format($reception->rete_ica_amount, 0, ',', '.') }}</b></td>
                    @else
                        <td align="right"><b>${{ number_format(0, 0, ',', '.') }}</b></td>
                    @endif
                </tr>
                @endif
                @if ( $reception->rete_fuente_amount )
                <tr>
                    <td align="right" colspan="16"><b>Rete fuente:</b></td>
                    @if ( $total_cost != 0 )
                        <td align="right"><b>${{ number_format($reception->rete_fuente_amount, 0, ',', '.') }}</b></td>
                    @else
                        <td align="right"><b>${{ number_format(0, 0, ',', '.') }}</b></td>
                    @endif
                </tr>
                @endif
                @if ( $total_consumption_tax )
                <tr>
                    <td align="right" colspan="16"><b>Impuesto al consumo:</b></td>
                    <td align="right"><b>${{ number_format($total_consumption_tax, 0, ',', '.') }}</b></td>
                </tr>
                @endif
                @if($reception->credit_note_amount)
                    <tr>
                        <td align="right" colspan="16"><b>Nota crédito @if(!empty($reception->credit_note_reason)) ({{ $reception->credit_note_reason }}) @endif:</b></td>
                        <td align="right"><b>${{ number_format($reception->credit_note_amount, 0, ',', '.') }}</b></td>
                    </tr>
                @endif
                @if( $total_credit_note )
                    <tr>
                        <td align="right" colspan="16"><b>Total nota crédito:</b></td>
                        <td align="right"><b>${{ number_format($total_credit_note, 0, ',', '.') }}</b></td>
                    </tr>
                @endif
                <tr>
                    <td align="right" colspan="16"><b>Total:</b></td>
                    <!-- <td align="right"><b>${{ number_format($total, strlen(substr(strrchr($total, "."), 1)), ',', '.') }}</b></td> -->
                    <td align="right"><b>${{ number_format($total - $total_credit_note, 0, ',', '.') }}</b></td>
                </tr>
                @endif
            @else
            <tr>
                <td colspan="20" align="center">
                    No se encontraron productos.
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    @stop
@endif
@if (Request::ajax() && $section == 'product-table')
    @section('product-table')
        @if (isset($products))
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td><img src="{{$product->image_small_url}}" class="img-responsive"></td>
                        <td>{{$product->reference}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->quantity}} {{$product->unit}}</td>
                        <td>
                            @if ($product->special_price) <p style="text-decoration: line-through;">${{number_format($product->price)}}</p> <p>${{number_format($product->special_price)}}</p> @else ${{number_format($product->price)}} @endif
                        </td>
                        <td>{{$product->current_stock}}</td>
                        <td>
                            <input type="number" name="product_quantity" class="product_cant form-control" value="0">
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-xs btn-default btn-add-product" href="javascript:;" data-current_stock="{{$product->current_stock}}" data-store_product_id="{{$product->id}}" data-reception_id="{{ $reception_id }}" data-provider_id="{{ $provider_id }}">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
            @endif
        @else
            <tr><td colspan="14" align="center">&nbsp;</td></tr>
        @endif
    @stop
@endif
@if (Request::ajax() && $section == 'provider_order_detail_product_group')
    @section('provider_order_detail_product_group')
        @if (isset($provider_order_detail_product_group))
            @if ( $action == 'order-product-group-qty' )
                <form id="product-group-info" action="{{ route('adminMovements.receptionUpdateProductGroup', ['id' => $id]) }}" method="POST">
            @endif
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>ID</th>
                        <th>Referencia</th>
                        <th>PLU</th>
                        <th>Producto</th>
                        <th>Unid. solicitadas</th>
                        <th>Embalaje</th>
                        <th style="background: #baf2ff">Unid. en factura</th>
                        <th style="background: #fff3ba">Unid. recibidas</th>
                        <th>Costo base</th>
                        <th>Costo</th>
                        <th>IVA %</th>
                        <th>IVA</th>
                        <th>Imp. consumo</th>
                        <th>Costo Total</th>
                        @if ( $action == 'order-product-group-qty' )
                        <th>Fecha de expiración</th>
                        @endif
                        <th>Estado</th>
                        <th>Estado contable</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($provider_order_detail_product_group->grouped_products as $product)
                        <tr>
                            <td align="center"><img src="{{ $product->image_url }}" height ="50px"></td>
                            <td>{{ $product->product_id }}</td>
                            <td>{{ $product->reference }}</td>
                            <td>{{ $product->plu }}</td>
                            <td>{{ $product->product_name }}</td>
                            <td>{{ $product->quantity_order }}</td>
                            <td>{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                            <td style="background: #baf2ff" align="center">{{ $product->reception_detail_product_group_quantity_expected }}</td>
                            <td style="background: #fff3ba" align="center">
                                @if ( $action == 'order-product-group' )
                                    {{ $product->reception_detail_product_group_quantity_received }}
                                @else
                                    <input type="hidden" name="provider_ordered_input[{{ $product->reception_detail_product_group_id }}]" value="{{ $product->id }}">
                                    <input type="hidden" name="quantity_ordered_input[{{ $product->reception_detail_product_group_id }}]" value="{{ $product->quantity_order }}">
                                    <input type="number" data-handle_expiration_date="{{ $product->handle_expiration_date }}" name="quantity_received_input[{{ $product->reception_detail_product_group_id }}]" class="form-control text-center quantity_received_input required" style="font-size: 2rem; font-weight: bold;" value="{{ $product->reception_detail_product_group_quantity_received }}">
                                @endif
                            </td>
                            <td align="right">${{ number_format($product->base_cost, 0, ',', '.') }}</td>
                            <td align="right">${{ number_format($product->cost, 0, ',', '.') }}</td>
                            <td align="right">{{ $product->iva }}%</td>
                            <td align="right">${{ number_format($product->iva_amount, 0, ',', '.') }}</td>
                            <td align="right">${{ number_format($product->consumption_tax, 0, ',', '.') }}</td>
                            <td align="right">${{ number_format( $product->total_cost , 0, ',', '.') }}</td>
                            @if ( $action == 'order-product-group-qty' )
                            <td>
                                @if ( $product->handle_expiration_date )
                                    <input type="text" name="expiration_date_input[{{ $product->reception_detail_product_group_id }}]" class="form-control text-center quantity_received_input required expiration_date" value="{{ date_format( date_create($product->reception_detail_product_group_expiration_date), "d/m/Y") }}">
                                @endif
                            </td>
                            @endif
                            <td>
                                @if($product->reception_detail_product_group_status == 'Recibido')
                                    <span class="badge bg-green">Recibido</span>
                                @elseif($product->reception_detail_product_group_status == 'No recibido')
                                    <span class="badge bg-red">No recibido</span>
                                @elseif($product->reception_detail_product_group_status == 'Parcialmente recibido')
                                    <span class="badge bg-orange">Parcialmente recibido</span>
                                @elseif($product->reception_detail_product_group_status == 'Dañado')
                                    <span class="badge bg-red">Dañado</span>
                                @else
                                    <span class="badge bg-orange">Pendiente</span>
                                @endif
                            </td>
                            <td>
                                {{ $product->accounting_status }}
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        @if ( $reception->status == 'En proceso' )
                                            <li>
                                                <a class="update-product-group" href="javascript:;" data-product="{{ $product->reception_detail_product_group_id }}" data-status="{{ 'Dañado' }}">Dañado</a>
                                            </li>
                                        @endif
                                        @if ($reception->status != 'Recibido con factura')
                                            <li>
                                                @if ( $reception->status == 'Iniciado' )
                                                    <a href="javascript:;" onclick="expected_quantity_modal.set_edit_form_data_grouped( '{{ addslashes($product->product_name) }}', {{ $product->store_product_id }}, {{ $product->reception_detail_product_group_quantity_expected }}, {{ $product->quantity_order}}, {{ $product->reception_detail_product_group_id }});">
                                                        Editar cantidades
                                                    </a>
                                                @else

                                                    <a href="javascript:;" onclick="edit_cost_modal.set_edit_form_grouped_data( '{{ addslashes($product->product_name) }}', {{ $provider_order_detail_product_group->id }}, {{ $product->provider_order_detail_id }}, {{ $product->id }}, {{ $product->reception_detail_product_group_id }}, {{ $product->base_cost }}, {{ $product->iva }}, {{ $product->consumption_tax }}, {{ $product->reception_detail_product_group_quantity_expected }}, {{ $product->reception_detail_product_group_quantity_received }} );">
                                                        Editar valores
                                                    </a>
                                                @endif
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            </div>
            @if ( $action == 'order-product-group-qty' )
                </form>
            @endif
        @endif
    @stop
@endif
