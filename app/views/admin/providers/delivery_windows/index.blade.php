@if (!Request::ajax())
    @extends('admin.layout')
    @section('content')
        <section class="content-header">
            <h1>
                {{ $title }}
                <small>Control panel</small>
            </h1>
            <span class="breadcrumb" style="top:0px">
                <a type="button" class="btn btn-primary btn-add-window" data-toggle="modal" data-target="#frm-modal"><i class="fa fa-plus"></i> Nueva franja horaria</a>
            </span>
        </section>
        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('success') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alert!</b> {{ Session::get('error') }}
                </div>
            @endif

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Buscar</h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="box-body" id="form_search">
                            <div class="row error-search">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger unseen"></div>
                                    <div class="alert alert-success unseen"></div>
                                </div>
                            </div>
                            <div class="row">
                                <form id="search">
                                    <div class="col-md-2 from-group">
                                        <label>Ciudad:</label>
                                        <select class="form-control city"  data-warehouse="warehouse" name="city_id">
                                            <option value="">Seleccione</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}">{{$city->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="">Bodega</label>
                                        <select class="form-control " name="warehouse_id" id="warehouse" required>
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2 from-group">
                                        <label>Hora inicio:</label>
                                        <div class='input-group date' id='s_hour_start'>
                                            <input class="form-control" name="hour_start" value="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 from-group">
                                        <label>Hora final:</label>
                                        <div class='input-group date' id='s_hour_end'>
                                            <input class="form-control" name="hour_end" value="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 from-group">
                                        <label>Estado:</label>
                                        <select name="status" class="form-control">
                                            <option value="">Seleccione</option>
                                            <option value="1">Activo</option>
                                            <option value="0">Inactivo</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="box-footer clearfix" id="form_search">
                            <input type="button" id="search-btn" class="btn btn-primary pull-right" value="Buscar">
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Franjas horarias</h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="box-body" id="form_search_start_supplier">
                            <div class="row error-search">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger unseen"></div>
                                    <div class="alert alert-success unseen"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="delivery-windows-list">
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="frm-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Franja horaria</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frm-add-delivery-window">
                                <input type="hidden" id="id-window" name="id">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Ciudad</label>
                                            <select class="form-control city" data-warehouse="warehouse_id" name="city_id" id="city_id" required>
                                                <option value="">Seleccione</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->id}}">{{$city->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Bodega</label>
                                            <select class="form-control" name="warehouse_id" id="warehouse_id" required>
                                                <option value="">Seleccione</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Hora inicial</label>
                                            <div class='input-group date' id='start_date'>
                                                <input type="text" name="hour_start" id="hour_start" class="form-control" required/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Hora final</label>
                                            <div class='input-group date' id='end_date'>
                                                <input type="text" name="hour_end" id="hour_end" class="form-control" required/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary btn-save-delivery-window">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js"></script>
        <script>
            var web_url_ajax = "{{route('adminProvider.deliveryWindows')}}";
            var deliveryWindow = (function () {
                'use strict';
                function deliveryWindow( args ){
                    if( !( this instanceof deliveryWindow ) ){
                        return new deliveryWindow( args );
                    }
                    this._urls = {
                        ini: "{{route('adminProvider.deliveryWindows')}}",
                        save: "{{route('adminProvider.deliveryWindowsSave')}}",
                        table: "{{route('adminProvider.deliveryWindowsAjax')}}",
                        status: "{{route('adminProvider.deliveryWindowsStatus')}}",
                        @if($admin_permissions['delete'])
                        delete: "{{route('adminProvider.deliveryWindowsDelete')}}"
                        @endif
                    };

                    this.bindActions();
                }

                deliveryWindow.prototype.bindActions = function(){
                    var self = this;
                    $('#hour_end').datetimepicker({
                        format: 'HH:mm',
                        allowInputToggle : true
                    });
                    $('#hour_start').datetimepicker({
                        format: 'HH:mm',
                        allowInputToggle : true
                    });
                    $('#s_hour_start').datetimepicker({
                        format: 'HH:mm',
                        allowInputToggle : true
                    });
                    $('#s_hour_end').datetimepicker({
                        format: 'HH:mm',
                        allowInputToggle : true
                    });
                    $('.btn-save-delivery-window').on('click', function () {
                        $('.btn-save-delivery-window').prop('disabled', true);
                        if($('#frm-add-delivery-window').valid()){
                            self._save_data();
                        }else{
                            $('.btn-save-delivery-window').prop('disabled', false);
                        }
                    });
                    $('#search-btn').on('click', function (event) {
                        event.preventDefault();
                        self._load_table();
                    });

                    $('body').on('click', '.btn-edit', function () {
                        self._edit($(this).data());
                    });
                    $('body').on('click', '.btn-status', function () {
                        self._change_status($(this).data());
                    });
                    $('body').on('click', '.btn-delete', function () {
                        if(confirm('¿Esta seguro de eliminar la franja horaria?')){
                            self._delete_delivery_window($(this).data());
                        }
                    });
                    $('body').on('change','.city', function(){
                        self._getWarehouses(this, $(this).data('warehouse'))
                    });
                    $('#frm-modal').on('hidden.bs.modal', function () {
                        $('#hour_start').data('DateTimePicker').destroy()
                        $('#hour_end').data('DateTimePicker').destroy()
                        $('#frm-add-delivery-window')[0].reset();
                        setTimeout(function () {
                            $('#hour_end').datetimepicker({
                                format: 'HH:mm',
                                allowInputToggle : true
                            });
                            $('#hour_start').datetimepicker({
                                format: 'HH:mm',
                                allowInputToggle : true
                            });
                        }, 1000);

                    });
                    self._load_table();
                };
                deliveryWindow.prototype._getWarehouses = function(cityField,warehouseFieldId) {
                    $.ajax({
                        url: "{{ route('admin.get_warehouses_ajax') }}",
                        data: { city_id: $(cityField).val() },
                        type: 'GET',
                        dataType: 'json',
                        before : function(data){
                            $('#'+warehouseFieldId).addClass('disabled');
                        } ,
                        success: function(response) {

                            $('#'+warehouseFieldId).empty();
                            $('#'+warehouseFieldId).append('<option value="">Seleccione</option>');
                            $.each(response, function(key, value){
                                $('#'+warehouseFieldId).append('<option value="' + key + '">' + value + '</option>');
                            });
                            $('#'+warehouseFieldId).removeClass('disabled').trigger('change');
                        }
                    });
                };

                deliveryWindow.prototype._save_data = function () {
                    var self = this;
                    $.ajax({
                        url : self._urls.save,
                        type : 'POST',
                        data : $('#frm-add-delivery-window').serialize(),
                        dataType : 'json',
                        beforeSend : function ( xhr ) {
                            $('.btn-save-delivery-window').prop('disabled', true);
                            load_animation._add_element('#delivery-windows-list', 'list-loader');
                        },
                        success : function ( xhr ) {
                            $('#frm-modal').modal('hide');
                            if(xhr.status == true){
                                alert(xhr.message);
                            }else{
                                alert(xhr.message)
                            }
                            self._load_table();
                            $('.btn-save-delivery-window').prop('disabled', false);
                            $('#frm-add-delivery-window')[0].reset();
                            $('#id-window').val('');
                        },
                        error : function ( xhr ) {
                            alert(xhr.message)
                            $('.btn-save-delivery-window').prop('disabled', false);
                        }
                    })
                };

                deliveryWindow.prototype._load_table = function () {
                    var self = this;
                    $.ajax({
                        url : self._urls.table,
                        type : 'GET',
                        data : $('#search').serialize(),
                        dataType : 'json',
                        beforeSend : function ( xhr ) {
                            load_animation._add_element('#delivery-windows-list', 'list-loader');
                        },
                        success : function ( xhr ) {
                            if(xhr.status == true){
                                $('#delivery-windows-list').html(xhr.result);
                            }else{
                                alert(xhr.message);
                            }
                        },
                        error : function ( xhr ) {
                            //
                        }
                    })
                };

                deliveryWindow.prototype._edit = function ( data ) {
                    $('#city_id').val(data.city_id);
                    $('#hour_start').val(data.hstart);
                    $('#hour_end').val(data.hend);
                    $('#shifts').val(data.shifts);
                    $('#id-window').val(data.id);
                    $('#delivery_amount').val(data.amount)
                    $('#frm-modal').modal('show');
                };

                deliveryWindow.prototype._change_status = function ( data ) {
                    var self = this;
                    $.ajax({
                        url : self._urls.status,
                        type : 'POST',
                        data : { id : data.id, status : data.status},
                        dataType : 'json',
                        beforeSend : function ( xhr ) {
                            load_animation._add_element('#delivery-windows-list', 'list-loader');
                        },
                        success : function ( xhr ) {
                            if(xhr.status == true){
                                alert(xhr.message);
                            }else{
                                alert(xhr.message)
                            }
                            self._load_table();
                        },
                        error : function ( xhr ) {
                            alert(xhr.message)
                        }
                    })
                };

                @if($admin_permissions['delete'])
                deliveryWindow.prototype._delete_delivery_window = function ( data ) {
                    var self = this;
                    $.ajax({
                        url : self._urls.delete,
                        type : 'POST',
                        data : { id : data.id, status : data.status},
                        dataType : 'json',
                        beforeSend : function ( xhr ) {
                            load_animation._add_element('#delivery-windows-list', 'list-loader');
                        },
                        success : function ( xhr ) {
                            if(xhr.status == true){
                                alert(xhr.message);
                            }else{
                                alert(xhr.message)
                            }
                            self._load_table();
                        },
                        error : function ( xhr ) {
                            alert(xhr.message)
                        }
                    });
                };
                @endif
                return deliveryWindow;
            }());
            $(document).ready(function () {
                var delivery_window = new deliveryWindow;
            })


        </script>
    @endsection
@else
    @section('list')
        @if(isset($deliveryWindows))
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Ciudad</th>
                            <th>Bodega</th>
                            <th>Franja horaria</th>
                            <th>Hora inicio</th>
                            <th>Hora final</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($deliveryWindows))
                            @foreach($deliveryWindows as $deliveryWindow)
                                <tr>
                                    <td>{{$deliveryWindow->warehouse->city->city}}</td>
                                    <td>{{$deliveryWindow->warehouse->warehouse}}</td>
                                    <td>{{$deliveryWindow->delivery_window}}</td>
                                    <td>{{$deliveryWindow->hour_start}}</td>
                                    <td>{{$deliveryWindow->hour_end}}</td>
                                    <td align="center">
                                        @if($deliveryWindow->status)
                                            <span class="badge bg-green">Activo</span>
                                        @else
                                            <span class="badge bg-red">Desactivado</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a class="btn-edit" data-id="{{$deliveryWindow->id}}" data-city_id="{{$deliveryWindow->city_id}}" data-hstart="{{$deliveryWindow->hour_start}}" data-hend="{{$deliveryWindow->hour_end}}" data-shifts="{{$deliveryWindow->shifts}}" data-amount="{{$deliveryWindow->delivery_amount}}">Editar</a>
                                                </li>
                                                <li>
                                                    @if($deliveryWindow->status == 1)
                                                        <a class="btn-status" data-id="{{$deliveryWindow->id}}" data-status="0">Desactivar</a>
                                                    @else
                                                        <a class="btn-status" data-id="{{$deliveryWindow->id}}" data-status="1">Activar</a>
                                                    @endif
                                                </li>
                                                @if($admin_permissions['delete'])
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a class="btn-delete" data-id="{{$deliveryWindow->id}}" data-status="1">Eliminar</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" align="center">No hay franjas horarias.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        @endif
    @endsection
@endif