@if (!Request::ajax())

@extends('admin.layout')
@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

    <style type="text/css">
        th{ text-align: center!important; }
        #modal-order-product-group .modal-dialog{
            width: 70%!important;
        }
        #modal-products .modal-dialog{
            width: 70%!important;
        }
    </style>
    <section class="content-header">
        <h1>
            {{ $title }} # {{ $provider_order->id }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
        @if ($admin_permissions['update'])
            @if($provider_order->status == 'Iniciada')
                <a href="{{ route('adminProviderOrders.updateStatus', ['id' => $provider_order->id, 'status' => 'Enviada']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Enviada?')">
                    <button type="button" class="btn btn-success">Actualizar estado a Enviada</button>
                </a>
                <a href="javascript:;" onclick="show_modal('send-provider-order')">
                    <button type="button" class="btn btn-success">Enviar correo y actualizar estado a Enviada</button>
                </a>
            @endif
            <a href="javascript:;" onclick="show_modal('products')">
                <button type="button" class="btn btn-primary">Agregar Producto</button>
            </a>
            @if($provider_order->status == 'Enviada')
                <a href="{{ route('adminProviderOrders.updateStatus', ['id' => $provider_order->id, 'status' => 'Pendiente']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Pendiente?')">
                    <button type="button" class="btn btn-success">Actualizar estado a Pendiente</button>
                </a>
            @endif
            @if($provider_order->status == 'Iniciada' || $provider_order->status == 'Enviada')
                <a href="#" data-id="{{ $provider_order->id }}">
                    <button type="button" class="btn btn-danger" onclick="show_modal('reject-order')">Cancelar Orden</button>
                </a>
            @endif
            @if($provider_order->status == 'Pendiente')
                <a href="{{ route('adminProviderOrders.updateStatus', ['id' => $provider_order->id, 'status' => 'Cerrada']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Cerrada?')">
                    <button type="button" class="btn btn-success">Actualizar estado a Cerrada</button>
                </a>
            @endif
            @if($provider_order->status != 'Iniciada')
                <a href="javascript" id="export-order" data-id="{{ $provider_order->id }}">
                    <button type="button" class="btn btn-primary">Exportar Excel</button>
                </a>
                <a href="{{ route('adminMovements.receptionProviderOrder', ['id' => $provider_order->id]) }}">
                    <button type="button" class="btn btn-primary">Recibos de Bodega</button>
                </a>
                <a href="{{ route('adminProviderOrders.printPdf', ['id' => $provider_order->id]) }}" target="_blank">
                    <button type="button" class="btn btn-primary">Imprimir</button>
                </a>
            @endif
            @if($provider_order->status != 'Cancelada')
                <a href="javascript:;" id="upload-invoice" onclick="show_modal('invoice')">
                    <button type="button" class="btn btn-primary">Facturas</button>
                </a>
            @endif
            <a href="#" onclick="show_modal('note')">
                <button type="button" class="btn btn-primary">Agregar Nota</button>
            </a>
            @if($provider_order->status != 'Iniciada' || $provider_order->status != 'Enviada')
                @if ($admin_permissions['permission2'])
                <div class="btn btn-primary" onclick="show_modal('change-status')">Cambiar Estado</div>
                @endif
            @endif
       @endif
       </span>
    </section>

    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
        @if(Session::has('success') )
        <div class="alert alert-success">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif
        @if( $errors->has() )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b>
            <ul>
                @if($errors->has())
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                @endif

            </ul>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Orden</h4>
                    <p><label>Estado</label> -
                    @if($provider_order->status == 'Iniciada')
                        <span class="badge bg-orange">{{ $provider_order->status }}</span>
                    @elseif($provider_order->status == 'Enviada')
                        <span class="badge bg-yellow">{{ $provider_order->status }}</span>
                    @elseif($provider_order->status == 'Pendiente')
                        <span class="badge bg-blue">{{ $provider_order->status }}</span>
                    @elseif($provider_order->status == 'Cerrada')
                        <span class="badge bg-green">{{ $provider_order->status }}</span>
                    @elseif($provider_order->status == 'Cancelada')
                        <span class="badge bg-red">{{ $provider_order->status }}</span>
                    @endif
                    </p>
                    <p><label>Ciudad</label> - {{ $provider_order->warehouse->city->city }}</p>
                    <p><label>Bodega</label> - {{ $provider_order->warehouse->warehouse }}</p>
                    <p><label>Recibos de bodega</label> - <b>{{ count($provider_order->reception) }}</b></p>
                    <p><label>Fecha de creación</label> - {{ date("d M Y g:i a", strtotime($provider_order->date)) }}</p>
                    <p><label>Fecha de entrega</label> - {{ date("d M Y", strtotime($provider_order->delivery_date)) }}
                    @if($admin_permissions['update'] && in_array($provider_order->status, ['Iniciada', 'Enviada']))
                        <a href="#" onclick="show_modal('delivery-date')">Editar</a>
                    @endif
                    </p>
                    @if(!empty($provider_order->management_date))
                        <p><label>Fecha de gestión</label> - {{ date("d M Y g:i a", strtotime($provider_order->management_date)) }}</p>
                    @endif
                    <p><label>Productos Faltantes</label> - @if($provider_order->missing_products) Si @else No @endif</p>
                    <p><label>Creada por </label> - {{ $provider_order->admin->fullname }}</p>
                    @if(!empty($provider_order->comments))
                        <p><label>Comentarios</label> - {{ $provider_order->comments }}</p>
                    @endif
                    @if($provider_order->status == 'Cancelada') <p><label>Comentarios de cancelación</label> - {{ $provider_order->reject_comments }} @endif
                    <p><a href="#" title="Log de pedido" onclick="show_modal('orders_log')">Log de orden</a></p>
                    <p><a href="#" title="Notas de pedido" onclick="show_modal('notes')">Notas de orden</a></p>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Proveedor</h4>
                    <p><label>Tipo de proveedor</label> - {{ $provider_order->provider->provider_type }}</p>
                    <p><label>Proveedor</label> - {{ $provider_order->provider->name }}</p>
                    <p><label>NIT</label> - {{ $provider_order->provider->nit }}</p>
                    <p><label>Contacto</label> - {{ !empty($provider_order->provider->contacts->first()) ? $provider_order->provider->contacts->first()->fullname : '' }}</p>
                    <p><label>Teléfono</label> - {{ !empty($provider_order->provider->contacts->first()) ? $provider_order->provider->contacts->first()->phone : '' }}</p>
                    <p><label>Hora máxima para recibir ordenes</label> - {{ $provider_order->provider->maximum_order_report_time }}</p>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <legend>Productos solicitados</legend>
                        <label>Buscar</label>
                        <input type="text" placeholder="Referencia, PLU, nombre" id="search-products" class="form-control"><br>
                        <div id="products_table_container">
                            <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Order Notes Modal -->
    <div class="modal fade" id="modal-notes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Notas de Pedido</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body table-responsive container-overflow">
                                <table id="shelves-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Usuario admin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($provider_order_notes))
                                        @foreach($provider_order_notes as $note)
                                        <tr>
                                            <td>{{ date("d M Y g:i a",strtotime($note->created_at)) }}</td>
                                            <td>{{ $note->description }}</td>
                                            <td>{{ $note->fullname }}</td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="3" align="center">No hay datos.</td></tr>
                                    @endif
                                    </tbody>
                                </table><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Note Modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Nota a Pedido</h4>
                </div>
                <form method="post" action="{{ route('adminProviderOrders.saveNote', ['id' => $provider_order->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $provider_order->id }}">
                        <div class="form-group reject-comments">
                            <label class="reject-label">Nota</label>
                            <textarea class="form-control required" required="required" name="note"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Status Order Modal -->
    <div class="modal fade" id="modal-change-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cambiar Estado de Orden de Compra</h4>
                </div>
                <form method="post" action="{{ route('adminProviderOrders.updateStatus', ['id' => $provider_order->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" class="order-id" name="id" value="{{ $provider_order->id }}">
                        <div class="form-group">
                            <label class="control-label">Nuevo estado de orden de compra</label>
                            <select class="form-control required" name="status" id="modal-new-status">
                                <option value="">-Selecciona-</option>
                                <option value="Iniciada">Iniciada</option>
                                <option value="Enviada">Enviada</option>
                                <option value="Pendiente">Pendiente</option>
                                <option value="Cancelada">Cancelada</option>
                            </select>
                        </div>
                        <div class="reject-block" style="display: none">
                            <div class="form-group">
                                <label class="reject-label">Razón por la cual se cancela la orden de compra</label>
                                <select class="form-control required" name="reject_reason">
                                    <option value="">-Selecciona-</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="reject-label">Comentarios de cancelación</label>
                                <textarea class="form-control" name="reject_comments"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Reject Order Modal -->
    <div class="modal fade" id="modal-reject-order">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cancelar Orden de Compra</h4>
                </div>
                <form method="post" action="{{ route('adminProviderOrders.updateStatus', ['id' => $provider_order->id, 'status' => 'Cancelada']) }}" enctype="multipart/form-data" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        {{--<div class="form-group">
                            <label class="reject-label">Selecciona la razón por la cual cancelas esta orden de compra</label>
                            <select class="form-control required" name="reject_typification">
                                <option value="">-Selecciona-</option>
                                @foreach($provider_order_reject_reasons as $provider_order_reject_reason)
                                    <option value="{{$provider_order_reject_reason->id}}">{{$provider_order_reject_reason->typification}}</option>
                                @endforeach
                            </select>
                        </div>--}}
                        <div class="form-group">
                            <label class="reject-label">Comentarios de cancelación</label>
                            <textarea class="form-control required" id="reject_comments" name="reject_comments"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger save-status reject-btn">Cancelar Orden</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Log Order Modal -->
    <div class="modal fade" id="modal-orders_log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 900px; width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Log de Orden de Compra</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Proveedor</th>
                                                <th>Usuario</th>
                                                <th>Log</th>
                                                <th>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($provider_order_log))
                                            @foreach($provider_order_log as $log)
                                            <tr>
                                                <td>{{ $log->user_name }}</td>
                                                <td>{{ $log->admin_name }}</td>
                                                <td>{{ $log->type }}</td>
                                                <td>{{ date_format($log->created_at, 'd/m/Y h:i:s A') }}</td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr><td colspan="5" align="center">No hay datos.</td></tr>
                                            @endif
                                        </tbody>
                                    </table><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Product Modal -->
    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modificar Unidades de Solicitadas de Producto</h4>
                </div>
                <form method="post" id="" action="{{ route('adminProviderOrders.updateProduct', ['id' => $provider_order->id]) }}" class="form-modal" id="update-product">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="provider-order-store-product-id" name="store_product_id" value="">
                        <div class="form-group">
                            <label class="control-label">Cantidad de unidades a solicitar</label>
                            <input type="text" class="form-control required" name="quantity" id="provider-order-product-quantity">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delivery Date Modal -->
    <div class="modal fade" id="modal-delivery-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar Fecha de Entrega</h4>
                </div>
                <form method="post" action="{{ route('adminProviderOrders.updateDeliveryDate', ['id' => $provider_order->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Nueva fecha de entrega</label>
                            <input type="text" name="delivery_date" class="form-control delivery_date" value="{{ $provider_order->delivery_date }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Show Product Group Modal -->
    <div class="modal fade" id="modal-order-product-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Productos del Producto Agrupado</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Invoice Image Order Modal -->
    <div class="modal fade" id="modal-invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Facturas de orden de compra #{{ $provider_order->id }}</h4>
                </div>
                <form method="post" action="{{ route('adminProviderOrders.saveUploadImage', ['id' => $provider_order->id]) }}" enctype="multipart/form-data" class="form-modal">
                    <input type="hidden" name="type" value="invoice">
                    <input type="hidden" name="provider_order_id" value="{{ $provider_order->id }}">

                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Imagen</label>
                            <input type="file" class="form-control" name="image" required="required">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Subir</button>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-body table-responsive container-overflow">
                                        <table id="shelves-table" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Fecha</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($provider_order_invoices))
                                                @foreach($provider_order_invoices as $invoice_image)
                                                <tr>
                                                    <td align="center"><img src="{{ $invoice_image->invoice_image_url }}" height="50px"></td>
                                                    <td>{{ date_format($invoice_image->created_at, 'd/m/Y h:i:s A') }}</td>
                                                    <td align="center"headers="">
                                                        <a href="{{ $invoice_image->invoice_image_url }}" target="_blank">Ver</a> /
                                                        <a href="{{ route('adminProviderOrders.deleteImage', ['id' => $provider_order->id, 'image' => $invoice_image->id]) }}">Eliminar</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr><td colspan="3" align="center">No hay imagenes de factura.</td></tr>
                                                @endif
                                            </tbody>
                                        </table><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Import Remission Modal -->
    <div class="modal fade" id="modal-import-remission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Importar archivo de remisión</h4>
                </div>
                <form method="POST" action="{{ route('adminProviderOrders.importRemission', ['id' => $provider_order->id]) }}" enctype="multipart/form-data" class="form-modal">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $provider_order->id }}">
                        <div class="form-group reject-comments">
                            <label class="remission-file">Archivo de remisión</label>
                            <input type="file" name="remission_file" id="remission_file" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Send excel to providers and change status -->
    <div class="modal fade" id="modal-send-provider-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Enviar órden de compra</h4>
                </div>
                <form id="send-message" method="POST" action="{{ route('adminProviderOrders.sendProviderEmail', ['id' => $provider_order->id]) }}" enctype="multipart/form-data" class="">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $provider_order->id }}">
                        <fieldset>
                            <legend>Información por parte del proveedor</legend>
                            <div class="row form-group">
                                <div class="col-lg-12">
                                    <label for="contact">Por favor seleccione el contacto para el envío del correo</label>
                                    <select name="contact[]" id="contact" class="form-control required" multiple required>
                                        @if (!empty($provider_order->provider->contacts))
                                            @foreach ($provider_order->provider->contacts as $contact)
                                                <option value="{{ $contact->id }}">{{ $contact->fullname }} - {{ $contact->email }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-12">
                                    <label for="add_contact">¿Desea agregar otros correos para el envío?</label>
                                    <input type="text" name="add_contact" id="add_contact" placeholder="Agregue los correos separándolos por comas" class="form-control">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Mensaje</legend>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="message">Por favor verifique el mensaje a enviar:</label>
                                    <textarea name="message" id="message" rows="10" style="width: 100%" class="form-control required" required>Buen día

Adjunto envió OC {{ $provider_order->id }} Merqueo XXX, por favor confirmar recibido y entregar en el día y horario establecido:

Fecha de entrega: {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $provider_order->delivery_date)->format('d/m/Y') }}

Hora de entrega:

Nota: Por favor tener en cuenta que para las entregas debe anexar la OC impresa a la factura y que los precios de la factura deben coincidir con los de la OC; En caso de encontrar alguna diferencia por favor informarnos.

Quedo atenta.

Gracias

Cordialmente.</textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Add products Modal -->
    <div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Producto a Pedido</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="" class="modal-search-form">
                                <input type="hidden" name="warehouse_id" value="{{ $provider_order->warehouse_id }}">
                                <input type="hidden" name="provider_id" value="{{ $provider_order->provider_id }}">
                                <input type="hidden" name="show_all_products" value="1">
                                <input type="hidden" name="ignore_on_route_stock" value="0">
                                <input type="hidden" name="store_id" value="{{ $store_id }}">
                                <div class="row error-search unseen">
                                    <div class="col-xs-12">
                                        <div class="alert alert-danger unseen"></div>
                                        <div class="alert alert-success unseen"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table width="100%" class="modal-product-request-table">
                                            <tbody>
                                                <tr>
                                                    <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                    <td>
                                                        <input type="text" placeholder="Nombre" name="search" id="search" class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
                                                    </td>
                                                    <td colspan="2" align="left"><button type="submit" id="btn-modal-search" class="btn btn-primary" data-order_id=""  data-store_id="">Buscar</button>&nbsp;&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="2"><span class="error" style="display: none;">Campo requerido</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="modal-products-request pre-scrollable">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Producto</th>
                                                    <th>PLU</th>
                                                    <th>Proveedor</th>
                                                    <th>Stock actual</th>
                                                    <th>Stock minimo</th>
                                                    <th>Stock ideal</th>
                                                    <th>Stock comprometido</th>
                                                    <th>Embalaje</th>
                                                    <th>Cantidad embalaje a solicitar</th>
                                                    <th>Unidades a solicitar</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody">
                                                <tr><td colspan="11" align="center">&nbsp;</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div align="center" class="modal-products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    @include('admin.providers.orders.js.order_details-js')
@stop
@endif

@if (Request::ajax() && $section == 'products_table_container')
    @section('products_table_container')
    <table class="table table-bordered table-striped" border="1">
        <thead>
            <tr>
                <th>Imagen</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Producto</th>
                <th>Unidades solicitadas</th>
                <th>Embalaje</th>
                <th>Cantidad embalaje a solicitar</th>
                <th>Unidades recibidas</th>
                <th>Costo base unitario</th>
                <th>Costo unitario</th>
                <th>Costo total</th>
                <th>Estado</th>
                @if($admin_permissions['update'] && $provider_order->status == 'Iniciada')
                <th>Editar</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($provider_order_products as $provider_order_product)
            @if($provider_order_product->provider_order_id == $provider_order->id)
            <tr @if ($provider_order_product->type == 'Proveedor') class="orders-scheduled_delivery" @endif>
                <td align="center"><img src="{{ $provider_order_product->image_url }}" height ="50px"></td>
                <td>{{ $provider_order_product->reference }}</td>
                <td>{{ $provider_order_product->plu }}</td>
                <td>{{ $provider_order_product->product_name }} @if ($provider_order_product->type == 'Proveedor')<p><a href="javascript:;" title="Productos" onclick="show_modal('order-product-group', {{ $provider_order_product->id }})">Ver productos</a></p>@endif</td>
                <td align="center">
                    {{ $provider_order_product->quantity_order }}
                    @if ($provider_order_product->type == 'Proveedor')
                        <br>Cantidad de embalaje
                    @endif
                </td>
                <td align="center">{{ $provider_order_product->pack_description }}</td>
                <td align="center">
                    {{ $provider_order_product->quantity_pack }} {{ $provider_order_product->provider_pack_type }}
                </td>
                <td align="center">@if (empty($provider_order_product->quantity_received)) 0  @else {{ $provider_order_product->quantity_received }} @endif</td>
                <td align="right">${{ number_format($provider_order_product->base_cost, 0, ',', '.') }}</td>
                <td align="right">${{ number_format($provider_order_product->cost, 0, ',', '.') }}</td>
                @if ( $provider_order_product->quantity_received )
                    <td align="right">${{ number_format($provider_order_product->cost * $provider_order_product->quantity_received, 0, ',', '.') }}</td>
                @else
                    <td align="right">${{ number_format($provider_order_product->cost * $provider_order_product->quantity_order, 0, ',', '.') }}</td>
                @endif
                <td align="center">
                    @if (empty($provider_order_product->status))
                        <span class="badge bg-orange">Pendiente</span>
                    @else
                        @if($provider_order_product->quantity_received == $provider_order_product->quantity_order || $provider_order_product->quantity_received >= $provider_order_product->quantity_order)
                            <span class="badge bg-green">Recibido</span>
                        @elseif($provider_order_product->quantity_received == 0)
                            <span class="badge bg-red">No recibido</span>
                        @elseif($provider_order_product->quantity_received < $provider_order_product->quantity_order)
                            <span class="badge bg-orange">Parcialmente recibido</span>
                        @endif
                    @endif
                </td>
                @if($admin_permissions['update'] && $provider_order->status == 'Iniciada')
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="javascript:;" onclick="edit({{ $provider_order_product->store_product_id }},{{ $provider_order_product->quantity_order }});">
                                    Cambiar unidades solicitadas
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('adminProviderOrders.deleteProduct', ['id' => $provider_order->id, 'store_product_id'=>$provider_order_product->store_product_id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar el producto?')">
                                    Eliminar
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
                @endif
            </tr>
            @endif
            @endforeach
            <tr>
                <td align="right" colspan="10"><b>Total:</b></td>
                <td align="right"><b>${{ number_format($total_cost, 0, ',', '.') }}</b></td>
            </tr>
        </tbody>
    </table>
    @stop
@endif

@if (Request::ajax() && $section == 'provider_order_detail_product_group')
    @section('provider_order_detail_product_group')
        @if (isset($provider_order_detail_product_group))
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Referencia</th>
                    <th>Nombre</th>
                    <th>Unidad de medida</th>
                    <th>Cantidad</th>
                    <th>Costo base unitario</th>
                    <th>Costo unitario</th>
                    <th>Costo total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($provider_order_detail_product_group as $product)
                <tr>
                    <td align="center"><img src="{{ $product->image_url }}" height ="50px"></td>
                    <td>{{ $product->reference }}</td>
                    <td>{{ $product->product_name }}</td>
                    <td>{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                    <td>{{ $product->quantity_order }}</td>
                    <td align="right">${{ number_format($product->base_cost, 0, ',', '.') }}</td>
                    <td align="right">${{ number_format($product->cost, 0, ',', '.') }}</td>
                    <td align="right">${{ number_format($product->cost * $product->quantity_order, 0, ',', '.') }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    @stop
@endif
