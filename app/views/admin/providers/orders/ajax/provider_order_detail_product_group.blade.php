<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Imagen</th>
			<th>Referencia</th>
			<th>Nombre</th>
			<th>Unidad de medida</th>
			<th>Cantidad</th>
			<th>Costo base unitario</th>
			<th>Costo unitario</th>
			<th>Costo total</th>
		</tr>
	</thead>
	<tbody>
		@foreach($provider_order_detail->providerOrderDetailProductGroup as $productGroup)
		<tr>
			<td align="center"><img src="{{ $productGroup->image_url }}" height ="50px"></td>
			<td>{{ $productGroup->storeProduct->product->reference }}</td>
			<td>{{ $productGroup->product_name }}</td>
			<td>{{ $productGroup->product_quantity }} {{ $productGroup->product_unit }}</td>
			<td>{{ $productGroup->quantity_order }}</td>
			<td align="right">${{ number_format($productGroup->base_cost, 0, ',', '.') }}</td>
			<td align="right">${{ number_format($productGroup->cost, 0, ',', '.') }}</td>
			<td align="right">${{ number_format($productGroup->cost * $productGroup->quantity_order, 0, ',', '.') }}</td>
		</tr>
		@endforeach
	</tbody>
</table>