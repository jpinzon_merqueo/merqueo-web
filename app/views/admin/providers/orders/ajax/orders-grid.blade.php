<table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Ciudad</th>
			<th>Bodega</th>
			<th>Tipo proveedor</th>
			<th>Proveedor</th>
			<th>Estado</th>
			<th>Fecha de creación</th>
			<th>Fecha de entrega</th>
			<th>Unidades a solicitar</th>
			<th>Dirección</th>
			<th>Creada por</th>
			<th>Ver</th>
		</tr>
	</thead>
	<tbody>
	@if (count($provider_orders))
		@foreach($provider_orders as $provider_order)
		<tr @if ($provider_order->status == 'Recibida') class="orders-scheduled_delivery" @endif>
			<td>{{ $provider_order->id }}</td>
			<td>{{ $provider_order->city }}</td>
			<td>{{ $provider_order->warehouse }}</td>
			<td>{{ $provider_order->provider_type }}</td>
			<td>{{ $provider_order->name }}</td>
			<td>
				@if($provider_order->status == 'Iniciada')
				<span class="badge bg-orange">Iniciada</span>
				@elseif($provider_order->status == 'Enviada')
				<span class="badge bg-yellow">Enviada</span>
				@elseif($provider_order->status == 'Pendiente')
				<span class="badge bg-blue">Pendiente</span>
				@elseif($provider_order->status == 'Cerrada')
				<span class="badge bg-green">Cerrada</span>
				@elseif($provider_order->status == 'Cancelada')
				<span class="badge bg-red">Cancelada</span>
				@endif
			</td>
			<td>{{ date('d M Y g:i a', strtotime($provider_order->date)) }}</td>
			<td>{{ date('d M Y g:i a', strtotime($provider_order->delivery_date)) }}</td>
			<td>{{ $provider_order->quantity }}</td>
			<td>{{ $provider_order->address }}</td>
			<td>{{ $provider_order->admin }}</td>
			<td>
				<div class="btn-group">
				   <a class="btn btn-xs btn-default" href="{{ route('adminProviderOrders.details', ['id' => $provider_order->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
				</div>
			</td>
		</tr>
		@endforeach
	@else
		<tr><td colspan="14" align="center">No hay ordenes de compra.</td></tr>
	@endif
	</tbody>
</table>
<div align="center">{{$provider_orders->links()}}</div>
<div class="row">
	<div class="col-xs-3">
		<div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($provider_orders) }} ordenes de compra.</div>
	</div>
</div>