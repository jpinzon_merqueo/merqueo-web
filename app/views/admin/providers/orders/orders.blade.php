@extends('admin.layout')

@section('content')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<!--<link rel="stylesheet" type="text/css" href="{{asset_url()}}css/style.css">-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script>
		var web_url_ajax = "{{ route('adminProviderOrders.index') }}";
	</script>

	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
		@if ($admin_permissions['insert'])
		<span class="breadcrumb" style="top:0px">
			<a href="{{ route('adminProviderOrders.createOrder') }}">
			<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Orden de Compra</button>
			</a>
			<a href="{{ route('adminProviderOrders.importMissing') }}">
			<button type="button" class="btn btn-primary"><i class="fa fa-upload"></i> Importar Orden</button>
			</a>
		</span>
		@endif
	</section>

	<section class="content">
		@if(Session::has('success') )
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Hecho!</b> {{ Session::get('success') }}
			</div>
		@endif

		@if(Session::has('error') )
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Alerta!</b> {{ Session::get('error') }}
			</div>
		@endif

		@if(isset($providerOrders))
			<script>
				$(document).ready(function() {
					$('.delivery_date').datetimepicker({
						format: 'DD/MM/YYYY',
					});
				});
			</script>

			<div class="col-xs-12">
				<div class="box box-default">
					<div class="box-header">
						<div class="col-xs-12">
							<h3>Actualizar fechas de entrega</h3>
						</div>
					</div>
					<div class="box-body table-responsive">
						<form action="{{ route('adminProviderOrders.updateDeliveryDates') }}" method="post">
							<table id="provider-orders-table" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Orden de Compra #</th>
										<th>Estado</th>
										<th>Proveedor</th>
										<th>Hora máxima para entregar al proveedor</th>
										<th>Promesa de servicio en horas</th>
										<th>Hora despacho a bodega de merqueo</th>
										<th>Unidades a solicitar</th>
										<th>Fecha de entrega</th>
										<th>Franja de entrega</th>
									</tr>
								</thead>
								<tbody>
								@if (!empty($providerOrders))
									<tr>
										<td>{{ $providerOrders->id }}</td>
										<td>
											<span class="badge bg-yellow">{{ $providerOrders->status }}</span>
										</td>
										<td>{{ $providerOrders->name }}</td>
										<td>{{ $providerOrders->maximum_order_report_time }}</td>
										<td>{{ $providerOrders->delivery_time_hours }}</td>
										<td>{{ $providerOrders->provider_delivery_time }}</td>
										<td>{{ $providerOrders->quantity }}</td>
										<td style="background-color: yellow">
											<div class="btn-group">
											   <input type="text" name="delivery_date[{{ $providerOrders->id }}]" class="delivery_date" value="{{ $providerOrders->delivery_date }}">
											</div>
										</td>
										<td>
											<div class="btn-group">
												<select name="delivery_window[{{ $providerOrders->id }}]" class="form-field">
													<option value="">Seleccione</option>
													@foreach($deliveryWindows as $deliveryWindow)
														<option value="{{$deliveryWindow->id}}">{{$deliveryWindow->delivery_window}}</option>
													@endforeach
												</select>
											</div>
										</td>
									</tr>
								@else
									<tr><td colspan="10" align="center">No hay ordenes de compra.</td></tr>
								@endif
								</tbody>
							</table>
							<br><div class="form-group"><button type="submit" class="btn btn-primary" onclick="return confirm('¿Estas seguro que deseas actualizar las fechas de entrega?')">Guardar</button></div>
						</form>
					</div>
				</div>
			</div>
		@else
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<div class="row">
								<div class="col-xs-12">
									<form id="search-form" class="admin-order-providers-table">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-xs-12">
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="city_id">Ciudad:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<select id="city_id" name="city_id" class="form-control get-warehouses">
															@foreach ($cities as $city)
																<option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
															@endforeach
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="warehouse_id">Bodega:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<select id="warehouse_id" name="warehouse_id" class="form-control">
															<option value="">-Selecciona-</option>
															@foreach ($warehouses as $warehouse)
																<option value="{{ $warehouse->id }}">{{ $warehouse->warehouse }}</option>
															@endforeach
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="search">Buscar:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<input id="input-search" type="text" placeholder="Nro. Orden de compra, Nombre producto" class="search form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-xs-12">
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="provider_type">Tipo proveedor:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<select id="provider_type" name="provider_type" class="form-control">
															<option value="">-Selecciona-</option>
															<option value="Merqueo">Merqueo</option>
															<option value="Marketplace">Marketplace</option>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="provider_id">Proveedor:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<select id="provider_id" name="provider_id" class="form-control">
															<option value="">-Selecciona-</option>
															@if (count($providers))
																@foreach ($providers as $provider)
																	<option value="{{ $provider->id }}">{{ $provider->name }}</option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="start_delivery_date">Fecha inicio:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<input type="text" placeholder="DD/MM/YYYY" id="start_delivery_date" name="start_delivery_date" class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="end_delivery_date">Fecha final:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<input type="text" placeholder="DD/MM/YYYY" id="end_delivery_date" name="end_delivery_date" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-xs-12">
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="status">Estado:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<select id="status" name="status" multiple="multiple" class="form-control">
															<option value="Iniciada">Iniciada</option>
															<option value="Enviada">Enviada</option>
															<option value="Pendiente">Pendiente</option>
															<option value="Cerrada">Cerrada</option>
															<option value="Cancelada">Cancelada</option>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-lg-4 col-md-4 col-xs-4 text-right">
														<label for="status">Creada por:</label>
													</div>
													<div class="col-lg-8 col-md-8 col-xs-8">
														<select name="user_id" id="user_id" class="form-control">
															<option value="">- Selecciona -</option>
															@if (count($users))
																@foreach ($users as $user)
																	<option value="{{ $user->id }}">{{ $user->fullname }}</option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4 col-md-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-xs-12">
												<div class="row form-group">
													<div class="col-lg-8 col-md-8 col-xs-8 col-lg-offset-4 col-md-offset-4 col-xs-offset-4">
														<button type="submit" id="btn-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
														<button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
							<br>
							<div class="paging"></div>
							<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
						</div>
					</div>
				</div>
			</div>
			@include('admin.providers.orders.js.orders-js')
		@endif
	</section>
@stop