<script>
    $(document).ready(function() {
        $('#end_delivery_date, #start_delivery_date').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });
        paging(1, $('.search').val());

        $('body').on('submit', '#search-form', function(event) {
            event.preventDefault();
        });

        $('#provider_type').change(function () {
            let provider_type = $(this).val();
            $('.paging-loading').show();
            if ( provider_type ) {
                $.ajax({
                    url: '{{ route('adminProviderOrders.getProvidersAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        provider_type: provider_type
                    },
                })
                .done(function(data) { 
                    console.log(data);
                    $('#provider_id').empty();
                    $('#provider_id').append('<option value="">-Selecciona-</option>');
                    if (data) {
                        $.each(data, function(index, item) {
                            $('#provider_id').append('<option value="'+ item.id +'">'+ item.name +'</option>');
                        });
                    }
                })
                .fail(function(data) {
                })
                .always(function(data) {
                    $('.paging-loading').hide();
                });
            }
        });

         $('body').on('click', '.pagination a', function(event) {
            event.preventDefault();

            var url = new URL($(this).prop('href'));
            var page = url.searchParams.get("page");
            console.log(page);

            $.ajax({
                    url: '{{ route('adminProviderOrders.index') }}',
                    type: 'GET',
                    dataType: 'html',
                    data: {
                        page: page,
                        
                        s: $('#input-search').val(),
                        p: 1,
                        provider: $('#provider_id').val(),
                        warehouse_id: $('#warehouse_id').val(),
                        provider_type: $('#provider_type').val(),
                        status: status,
                        city_id: $('#city_id').val(),
                        user_id: $('#user_id').val(),
                        start_delivery_date: $('#start_delivery_date').val(),
                        end_delivery_date: $('#end_delivery_date').val()
                                
                    },
                })
                .done(function(data) {
                    $('.paging').html(data);
                })
                
                .always(function(data) {
                    $('.paging-loading').hide();
                });
        });
    });
</script>