<script type="text/javascript">
    //$('.delivery_date').val({{$provider_order->delivery_date}});
    function show_modal(action, reference)
    {
        reference = reference || 0;

        if (action == 'order-product-group')
        {
            var provider_order_detail_id = reference;
            $.ajax({
             url: "{{ route('adminProviderOrders.getProductGroupAjax', ['id' => $provider_order->id]) }}",
             data: { provider_order_detail_id: provider_order_detail_id },
             type: 'get',
             dataType: 'html',
             success:
                function(response) {
                    $('#modal-order-product-group .modal-body').html(response);
                }
            });
        }

        $('#modal-' + action).modal('show');
    }

    function edit(store_product_id,quantity){
        $('#provider-order-store-product-id').val(store_product_id);
        $('#provider-order-product-quantity').val(quantity);
        $('#edit-modal').modal('show');
    }

    $(function() {

        function search_products(){
            $.ajax({
                url: '{{ route('adminProviderOrders.getProductsAjax', ['id' => $provider_order->id]) }}',
                data: { s: $('#search-products').val() },
                type: 'GET',
                dataType: 'json'
            })
            .done(function(data) {
                $('#products_table_container').empty();
                $('#products_table_container').html(data.html);
            })
            .fail(function() {
                console.error("error al cargar los contactos");
            });
        }

        search_products();

        $('#search-products').keyup(function(event){
            code = event.which;
            if (code == 32 || code == 13 || code == 188 || code == 186){
                search_products();
            }
        });

        $('.delivery_date').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
        });

        $('.form-modal').on('submit', function(e) {
            var error = false
            var form = this;

            $('input[type!="hidden"], select, textarea', this).each(function() {
                if ($(this).is(':visible') && ($(this).val().length == 0)) {
                    $(this).parent().addClass('has-error');
                    error = 'Completa los campos requeridos.';
                }else $(this).parent().removeClass('has-error');
            });

            if(error) {
                $('.form-has-errors', form).html(error).fadeIn();
                return false;
            }
            return true;
        });

        $('#update-product').validate();
        $('#send-message').validate();

        $('body').on('click','#export-order', function(e){
            e.preventDefault();
            $(this).find('button').addClass('disabled');
            $.ajax({ url: "{{ route('adminProviderOrders.export') }}",
                data: { id: $(this).data('id') },
                type: 'post',
                dataType: 'json',
                success:
                    function(response) {
                        $('#export-order').find('button').removeClass('disabled');
                        if(response.status){
                            window.open(response.file_url,'_blank');
                        }else alert("Error al descargar la orden de compra.");
                    }
            });
        });

        $('body').on('submit', '.modal-search-form', function(event) {
            event.preventDefault();
            $('.modal-products-request-loading').show();
            $.ajax({
                url: '{{ route('adminProviderOrders.getFormProductsAjax') }}',
                type: 'POST',
                dataType: 'json',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.length){
                    $('.tbody').html('');
                    $.each(data, function(index, val) {
                        let html = '';
                        html += '<tr>';
                        html += `   <td>${val.name} ${val.quantity} ${val.unit}</td>`;
                        html += `   <td>${val.store_product[0].provider_plu}</td>`;
                        html += `   <td>${val.store_product[0].provider.name}</td>`;
                        html += `   <td>${val.current_stock}</td>`;
                        html += `   <td>${val.store_product[0].store_product_warehouses[0].minimum_stock}</td>`;
                        html += `   <td>${val.store_product[0].store_product_warehouses[0].ideal_stock}</td>`;
                        html += `   <td>${val.committed_stock}</td>`;
                        html += `   <td>${val.store_product[0].provider_pack_quantity} x ${val.store_product[0].provider_pack_type}</td>`;
                        html += `   <td>${val.quantity_to_package_request} x ${val.store_product[0].provider_pack_type}</td>`;
                        html += `   <td><input type="number" value="${val.quantity_to_request}" class="form-control add_new_${val.store_product[0].id}"/></td>`;
                        html += `   <td><button class="btn btn-success btn-xs btn-add" data-id="${val.store_product[0].id}"><span class="glyphicon glyphicon-plus"></span></button></td>`;
                        html += '</tr>';
                        $('.tbody').append(html);
                    });
                }else{
                    $('.tbody').html('<tr><td colspan="11" align="center">No se encontraron productos.</td></tr>');
                }
            })
            .fail(function() {
                console.log("Ocurrió un error al cargar los productos");
            })
            .always(function() {
                $('.modal-products-request-loading').hide();
            });
        });

        $('#modal-products').on('hidden.bs.modal', function (e) {
          $('.tbody').html('<tr><td colspan="11" align="center">No se encontraron productos.</td></tr>');
        });


        $('body').on('click', '.btn-add', function(event) {
            let store_product_id = $(this).data('id');
            let quantity_unit_to_request = $('.add_new_'+store_product_id).val();
            if (quantity_unit_to_request < 1){
                alert('No se pueden agregar cantidades en cero.');
                return;
            }
            $.ajax({
                url: '{{ route('adminProviderOrders.addProductToOrder', ['id' => $provider_order->id]) }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    store_product_id: store_product_id,
                    quantity_unit_to_request: quantity_unit_to_request
                },
            })
            .done(function(data) {
                if (typeof data.success !== 'undefined') {
                    alert('Producto Agregado.');
                    location.reload();
                }else if (typeof data.error !== 'undefined'){
                    alert('El producto no se ha agregado: '+ data.error);
                }
            })
            .fail(function(data) {
                alert(data.responseJSON.error.message);
            })
            .always(function() {
            });
        });
    });
</script>