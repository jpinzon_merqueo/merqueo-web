<script>
    let vue_obj = new Vue({
        el: '#content',
        data: {
            isDisableForm: false,
            isProviderOrder: false,
            isMissingOrder: false,
            isLoading: false,
            products_searched: [],
            form_selects:{
                stores: {{ $stores->toJson() }},
                warehouses: {{ $warehouses->toJson() }},
                providers: {{ $providers->toJson() }},
                departments: {{ $departments->toJson() }},
                shelves: []
            },
            form_data: {
                order_type: 'Proveedor',
                city_id: {{ $first_city }},
                store_id: {{ $stores->first()->id }},
                warehouse_id: {{ $warehouses->first()->id }},
                department_id: '',
                shelf_id: '',
                provider_id: '',
                show_all_products: 0,
                start_date: '',
                end_date: '',
                search: '',
                ignore_on_route_stock: 0,
                show_commited_products_only: 0,
                products_added: []
            }
        },
        methods: {
            addAllProducts: function (e) {
                if (this.products_searched.length) {
                    for (var i = 0; i < this.products_searched.length; i++) {
                        this.onClicked(this.products_searched[i], i);
                    }
                }else{
                    alert('No hay productos para agregar.')
                }
            },
            onOrderTypeChange: function (e) {
                if (this.form_data.order_type == 'Proveedor') {
                    this.isMissingOrder = false
                    this.isProviderOrder = true
                } else if (this.form_data.order_type == 'Faltantes') {
                    this.isMissingOrder = true
                    this.isProviderOrder = false
                }
            },
            onClicked: function (product, e) {
                if (product.quantity_to_request < 1) {
                    alert('La cantidad a solicitar no puede ser cero o menor.');
                    return;
                }
                product.is_added = true
                if ( ! this.findProduct(product, this.form_data.products_added) ) {
                    var copy = Object.assign({}, product)
                    this.form_data.products_added.push(copy)
                }
                /*else{
                    index = this.products_added.findIndex(x => x.id == product.id);
                    this.products_added.splice(index,1)
                    this.products_added.push(product)
                }*/
            },
            onRemoved: function (product, index, e) {
                product.is_added = false
                if (this.findProduct(product, this.form_data.products_added) ) {
                    this.form_data.products_added.splice(index, 1)
                    var el = []
                    if (el = this.findProduct(product, this.products_searched)) {
                        el[0].is_added = false
                    }
                }
            },
            findProduct: function (neddle, haystack) {
                var result = haystack.filter(function (element, index) {
                    if (element.id == neddle.id) {
                        return element;
                    }
                }, neddle)

                if ( result.length > 0 ) {
                    return result;
                }
                return false;
            },
            onCityChange: function (e) {
                this.store_id = ''
                this.department_id = ''
                this.shelf_id = ''
                this.isLoading = true
                this.getStores()
                this.getWarehouses()
            },
            getStores: function () {
                this.isDisableForm = true
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProviderOrders.getStoresAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.form_data.city_id
                    },
                    context: this
                })
                .done(function(data) {
                    this.form_data.store_id = data[0].id
                    this.form_selects.stores = data
                    this.getDepartments()
                })
                .fail(function() {
                    alert('Ocurrió un error al obtener las tiendas de la ciudad.')
                })
                .always(function() {
                    this.isDisableForm = false
                    this.isLoading = false
                });
            },
            getWarehouses: function () {
                this.isDisableForm = true
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProviderOrders.getWarehousesAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.form_data.city_id
                    },
                    context: this
                })
                .done(function(data) {
                    this.form_data.warehouse_id = data[0].id
                    this.form_selects.warehouses = data
                    this.getProviders()
                })
                .fail(function() {
                    alert('Ocurrió un error al obtener las bodegas de la ciudad.')
                })
                .always(function() {
                    this.isDisableForm = false
                    this.isLoading = false
                });
            },
            getProviders: function () {
                this.isDisableForm = true
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProviderOrders.getProvidersAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        warehouse_id: this.form_data.warehouse_id
                    },
                    context: this
                })
                .done(function(data) {
                    this.form_data.provider_id = ''
                    this.form_selects.providers = data
                })
                .fail(function() {
                    alert('Ocurrió un error al obtener los proveedores de la bodega.')
                })
                .always(function() {
                    this.isDisableForm = false
                    this.isLoading = false
                });
            },
            getDepartments: function () {
                this.isDisableForm = true
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProviderOrders.getDepartmentsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        store_id: this.form_data.store_id
                    },
                    context: this
                })
                .done(function(data) {
                    this.form_data.department_id = ''
                    this.form_selects.departments = data
                    this.getShelves()
                })
                .fail(function() {
                    alert('Ocurrió un error al obtener los departamentos de las tiendas.')
                })
                .always(function() {
                    this.isDisableForm = false
                    this.isLoading = false
                });
            },
            getShelves: function () {
                this.isDisableForm = true
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProviderOrders.getShelvesAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        department_id: this.form_data.department_id,
                        store_id: this.form_data.store_id
                    },
                    context: this
                })
                .done(function(data) {
                    this.form_data.shelf_id = ''
                    this.form_selects.shelves = data
                })
                .fail(function() {
                    alert('Ocurrió un erro al obtener los pasillos de la tienda y departamento.')
                })
                .always(function() {
                    this.isDisableForm = false
                    this.isLoading = false
                });
            },
            resetForm: function () {
                this.form_data.city_id = {{ Session::get('admin_city_id') }}
                this.isProviderOrder = false
                this.isLoading = true
                this.products_searched = []
                this.form_data.products_added = []
                this.getStores()
                this.getWarehouses()
                this.getProviders()
                this.getDepartments()
                this.getShelves()
            },
            searchProduct: function (e) {
                if (this.form_data.provider_id == '') {
                    alert('Debes seleccionar un proveedor para poder realizar la búsqueda.')
                    return
                }
                this.isLoading = true
                this.onOrderTypeChange();
                this.products_searched = [];
                let self = this;
                $.ajax({
                    url: '{{ route('adminProviderOrders.getFormProductsAjax') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        city_id: this.form_data.city_id,
                        store_id: this.form_data.store_id,
                        warehouse_id: this.form_data.warehouse_id,
                        department_id: this.form_data.department_id,
                        shelf_id: this.form_data.shelf_id,
                        provider_id: this.form_data.provider_id,
                        show_all_products: this.form_data.show_all_products,
                        start_date: this.form_data.start_date,
                        end_date: this.form_data.end_date,
                        search: this.form_data.search,
                        ignore_on_route_stock: this.form_data.ignore_on_route_stock,
                        show_commited_products_only: this.form_data.show_commited_products_only,
                        products_added: JSON.stringify(this.productsAdded)
                    },
                    context: this
                })
                .done(function(data) {
                    self.products_searched = data
                })
                .fail(function(data) {
                    alert('Ocurrió un error al realizar la búsqueda.')
                })
                .always(function() {
                    self.isLoading = false
                });
            }
        },
        computed:{
            productsFound() {
                return this.products_searched.filter(product => !product.is_added);
            },
            productsAdded() {
                return this.form_data.products_added.map(product => ({store_product_id: product.store_product[0].id, quantity_unit_to_request: product.quantity_to_request, warehouse_id: product.store_product[0].store_product_warehouses[0].warehouse_id}))
            }
        }
    });
    $(document).ready(function() {
        $('#start_date, #end_date').datetimepicker({
            format: 'DD/MM/YYYY HH:mm:ss',
        });
        $('#start_date').on('dp.change', function(e){
            vue_obj.form_data.start_date = e.date.format("DD/MM/YYYY HH:mm:ss");
        })
        $('#end_date').on('dp.change', function(e){
            vue_obj.form_data.end_date = e.date.format("DD/MM/YYYY HH:mm:ss");
        })
        $('.products-order-loading').hide();
    });
</script>
