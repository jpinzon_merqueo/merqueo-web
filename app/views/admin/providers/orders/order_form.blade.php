@extends('admin.layout')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <script>
        var url_ajax = "{{ route('adminProviderOrders.add') }}";
    </script>
    <style type="text/css">
        #purchase-order-table th, #product-purchase-order-table th{ text-align: center!important; }
        .modal-dialog { width: 70%!important; }
    </style>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>

    <section id="content" class="content">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif

        @if ( $errors->count() > 0 )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b>
                <ul>
                    @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" @submit.prevent="searchProduct">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="order_type">Tipo de orden:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="order_type" name="order_type" class="form-control" v-model="form_data.order_type" :disabled="isDisableForm || isProviderOrder || isMissingOrder" @cahnge="onOrderTypeChange">
                                                        <option value="Proveedor">Proveedor</option>
                                                        <option value="Faltantes">Faltantes</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="city_id">Ciudad:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="city_id" name="city_id" class="form-control" v-model="form_data.city_id" @change="onCityChange" :disabled="isDisableForm || isProviderOrder || isMissingOrder">
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="warehouse_id">Bodega:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="warehouse_id" name="warehouse_id" class="form-control" v-model="form_data.warehouse_id" @change="getProviders" :disabled="isDisableForm || isProviderOrder || isMissingOrder">
                                                        <option v-for="warehouse in form_selects.warehouses" :value="warehouse.id">@{{warehouse.warehouse}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="provider_id">Proveedor:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="provider_id" name="provider_id" class="form-control" v-model="form_data.provider_id" :disabled="isDisableForm || isProviderOrder">
                                                        <option value="">-Selecciona-</option>
                                                        <option v-for="provider in form_selects.providers" :value="provider.id" :style="provider.count_today > 0 ? 'background-color: #cceeff; color: #000;' : ''">@{{provider.name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="show_all_products">Ver todos los productos:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="show_all_products" name="show_all_products" class="form-control" v-model="form_data.show_all_products" :disabled="isDisableForm">
                                                        <option value="0" selected="selected">No</option>
                                                        <option value="1">Sí</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="s">Buscar:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <input type="text" placeholder="Referencia, PLU, nombre" name="search" id="search"  class="search form-control" v-model="form_data.search" :disabled="isDisableForm">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="store_id">Tienda:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="store_id" name="store_id" class="form-control" v-model="form_data.store_id" @change="getDepartments" :disabled="isDisableForm">
                                                        <option v-for="store in form_selects.stores" :value="store.id">@{{store.name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="department_id">Departamento:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="department_id" name="department_id" class="form-control" v-model="form_data.department_id" @change="getShelves" :disabled="isDisableForm">
                                                        <option value="">-Selecciona-</option>
                                                        <option v-for="department in form_selects.departments" :value="department.id">@{{department.name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="shelf_id">Pasillo:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="shelf_id" name="shelf_id" class="form-control" v-model="form_data.shelf_id" :disabled="isDisableForm">
                                                        <option value="">-Selecciona-</option>
                                                        <option v-for="shelf in form_selects.shelves" :value="shelf.id">@{{shelf.name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <div class="col-xs-offset-2 col-xs-2 orders-scheduled_delivery" style="float: right">&nbsp;</div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    Producto tipo proveedor
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="ignore_on_route_stock">Ignorar stock solicitado:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="ignore_on_route_stock" name="ignore_on_route_stock" class="form-control" v-model="form_data.ignore_on_route_stock" :disabled="isDisableForm">
                                                        <option value="0" selected="selected">No</option>
                                                        <option value="1">Sí</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-4 col-md-4 col-xs-4 text-right">
                                                    <label for="show_commited_products_only">Ver solo stock comprometido:</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-8">
                                                    <select id="show_commited_products_only" name="show_commited_products_only" class="form-control" v-model="form_data.show_commited_products_only" :disabled="isDisableForm">
                                                        <option value="0" selected="selected">No</option>
                                                        <option value="1">Sí</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-xs-12">
                                            <div class="row form-group">
                                                <div class="col-lg-8 col-md-8 col-xs-8 col-lg-offset-4 col-md-offset-4 col-xs-offset-4">
                                                    <button type="submit" id="btn-search-products" class="btn btn-primary" :disabled="isDisableForm">Buscar</button>&nbsp;&nbsp;
                                                    <button type="reset" id="btn-reset" class="btn btn-primary" @click="resetForm">Reset</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div align="center" class="paging-loading" v-show="isLoading">
                                                    <img src="{{ asset_url() }}/img/loading.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header">
                        <div class="col-xs-12">
                            <h3>Consulta de productos</h3>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="paging products-query pre-scrollable">
                            <table id="purchase-order-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            <button class="btn btn-primary btn-xs" @click="addAllProducts">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </th>
                                        <th>Producto</th>
                                        <th>PLU</th>
                                        <th>Proveedor</th>
                                        <th>Stock actual</th>
                                        <th>Stock solicitado</th>
                                        <th>Stock minimo</th>
                                        <th>Stock ideal</th>
                                        <th>Stock comprometido</th>
                                        <th>Embalaje</th>
                                        <th>Cantidad embalaje a solicitar</th>
                                        <th>Unidades a solicitar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-show="productsFound.length" v-for="(productFound, index) in productsFound" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                        <td :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            <button class="btn btn-success btn-xs" @click="onClicked(productFound, index)">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </td>
                                        <td :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{productFound.name}} @{{productFound.quantity}} @{{productFound.unit}}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.store_product[0].provider_plu }}
                                        </td>
                                        <td :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.store_product[0].provider.name }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.current_stock }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.on_route_stock }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.store_product[0].store_product_warehouses[0].minimum_stock }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.store_product[0].store_product_warehouses[0].ideal_stock }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.committed_stock }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            1 @{{  productFound.store_product[0].provider_pack_type }} X @{{  productFound.store_product[0].provider_pack_quantity }}
                                            <span v-show="productFound.store_product[0].provider_pack_quantity > 1">Unidades</span>
                                            <span v-show="productFound.store_product[0].provider_pack_quantity == 1">Unidad</span>
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            @{{  productFound.quantity_to_package_request }} @{{  productFound.store_product[0].provider_pack_type }}
                                        </td>
                                        <td align="center" :style="productFound.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                            <input type="number" v-model="productFound.quantity_to_request" class="form-control text-align" style="width:60px;text-align:center" :step="productFound.store_product[0].provider_pack_quantity" min="0">
                                        </td>
                                    </tr>
                                    <tr v-show="!productsFound.length">
                                        <td colspan="14" align="center">Productos no encontrados.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <form method="post" action="{{ route('adminProviderOrders.save') }}" class="" id="update-price-custom-product">
                    <div class="box box-default col-xs-12">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h3>Productos a solicitar en Orden de Compra</h3><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <label for="order_city_id">Ciudad</label>
                                    <select id="order_city_id" name="order_city_id" class="form-control" v-model="form_data.city_id" :disabled="isProviderOrder || isMissingOrder">
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="order_city_id" v-model="form_data.city_id">
                                </div>
                                <div class="col-xs-4">
                                    <label for="order_warehouse_id">Bodega</label>
                                    <select id="order_warehouse_id" name="order_warehouse_id" class="form-control" v-model="form_data.warehouse_id" :disabled="isProviderOrder || isMissingOrder">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="warehouse in form_selects.warehouses" :value="warehouse.id">@{{warehouse.warehouse}}</option>
                                    </select>
                                    <input type="hidden" name="order_warehouse_id" v-model="form_data.warehouse_id">
                                </div>
                                <div class="col-xs-4">
                                    <label>Proveedor a asignar</label>
                                    <select id="provider_id" name="provider_id" class="form-control" :disabled="isProviderOrder">
                                        <option value="0">Asociado al producto</option>
                                        @foreach ($providers_missing as $provider_missing)
                                            <option value="{{ $provider_missing->id }}">{{ $provider_missing->name }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="order_provider_id" id="order_provider_id" v-model="form_data.provider_id">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body table-responsive">
                                <div class="products-order pre-scrollable">
                                    <table id="product-purchase-order-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Producto</th>
                                                <th>PLU</th>
                                                <th>Proveedor</th>
                                                <th>Stock actual</th>
                                                <th>Stock solicitado</th>
                                                <th>Stock minimo</th>
                                                <th>Stock ideal</th>
                                                <th>Stock comprometido</th>
                                                <th>Embalaje</th>
                                                <th>Cantidad embalaje a solicitar</th>
                                                <th>Unidades a solicitar</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-show="form_data.products_added.length" v-for="(product, index) in form_data.products_added" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                <td :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{product.name}} @{{product.quantity}} @{{product.unit}}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.store_product[0].provider_plu }}
                                                </td>
                                                <td :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.store_product[0].provider.name }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.current_stock }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.on_route_stock }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.store_product[0].store_product_warehouses[0].minimum_stock }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.store_product[0].store_product_warehouses[0].ideal_stock }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.committed_stock }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    1 @{{  product.store_product[0].provider_pack_type }} X @{{  product.store_product[0].provider_pack_quantity }}
                                                    <span v-show="product.store_product[0].provider_pack_quantity > 1">Unidades</span>
                                                    <span v-show="product.store_product[0].provider_pack_quantity == 1">Unidad</span>
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    @{{  product.quantity_to_package_request }} @{{  product.store_product[0].provider_pack_type }}
                                                </td>
                                                <td align="center" :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    <input type="number" v-model="product.quantity_to_request" class="form-control text-align" style="width:60px;text-align:center" :step="product.store_product[0].provider_pack_quantity" min="0">
                                                </td>
                                                <td :style="product.type == 'Proveedor' ? 'background-color: #FFFB89' : ''">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger btn-xs" @click="onRemoved(product, index)">
                                                            <span class="glyphicon glyphicon-minus"></span>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr v-show="!form_data.products_added.length"><td colspan="14" align="center">Productos no encontrados.</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer">
                                <input type="hidden" :value="JSON.stringify(productsAdded)" name="products_added">
                                <button type="submit" id="btn-generate-order-request" class="btn btn-primary" onclick="return confirm('¿Estas seguro que deseas generar las ordenes de compra de los productos?')">Generar Ordenes de Compra</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <style>
        .error{
            color:#ff0000;
            font-size: 13px;
            display: none;
        }
    </style>
    <!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
    <script src="https://unpkg.com/vue"></script>
    @include('admin.providers.orders.js.order_form-js')
@stop


@if (Request::ajax() && isset($products))
    @section('content_products_query')
    <table id="purchase-order-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th><input type="checkbox" name="all-product" class="all-product"  checked="checked" /></th>
                <th>Producto</th>
                <th>PLU</th>
                <th>Proveedor</th>
                <th>Stock actual</th>
                <th>Stock solicitado</th>
                <th>Stock minimo</th>
                <th>Stock ideal</th>
                <th>Stock comprometido</th>
                <th>Embalaje</th>
                <th>Cantidad embalaje a solicitar</th>
                <th>Unidades a solicitar</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products))
            @foreach($products as $product)
            <tr @if ($product['type'] == 'Proveedor') class="orders-scheduled_delivery" @endif>
                <td align="center"><input type="checkbox" name="product_id-{{ $product['id'] }}" class="product" value="{{ $product['id'] }}" checked="checked" /></td>
                <td align="left">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}</td>
                <td align="center">{{ $product['provider_plu'] }}</td>
                <td align="left">{{ $product['provider'] }}</td>
                <td align="center">{{ $product['current_stock'] }}</td>
                <td align="center">{{ $product['on_route_stock'] }}</td>
                <td align="center">{{ $product['minimum_stock'] }}</td>
                <td align="center">{{ $product['ideal_stock'] }}</td>
                <td align="center">{{ $product['committed_stock'] }}</td>
                <td align="center">
                @if (!$product['provider_pack_quantity_approach'])
                    1 {{ $product['provider_pack_type'] }} x {{ $product['provider_pack_quantity'] }} Unidades
                @else
                    1 {{ $product['provider_pack_type'] }} x {{ $product['provider_pack_quantity'] }} Unidades <br> (Acepta aproximación)
                @endif
                </td>
                <td align="center">
                @if (!$product['provider_pack_quantity_approach'])
                    {{ $product['quantity_pack_to_request'] }} @if ($product['quantity_pack_to_request'] > 1) {{ $product['provider_pack_type'] }}s @else {{ $product['provider_pack_type'] }} @endif
                @else
                    @if ($product['quantity_pack_to_request'])
                    {{ $product['quantity_pack_to_request'] }} @if ($product['quantity_pack_to_request'] > 1) {{ $product['provider_pack_type'] }}s @else {{ $product['provider_pack_type'] }} @endif
                        @if ($product['product_quantity_unit'])
                        <br> y {{ $product['product_quantity_unit'] }} @if ($product['product_quantity_unit'] > 1) Unidades @else Unidad @endif
                        @endif
                    @else
                    {{ $product['product_quantity_unit'] }} @if ($product['product_quantity_unit'] > 1) Unidades @else Unidad @endif
                    @endif
                @endif
                </td>
                <td align="center">
                   <input type="text" name="product_cant-{{ $product['id'] }}" id="product_cant-{{ $product['id'] }}" data-warehouse_id="{{ $product['warehouse_id'] }}" class="product_cant form-control" style="width:50px;text-align:center" value="{{ $product['quantity_unit_to_request'] }}" />
                   @if ($product['type'] == 'Proveedor') Se solicita por embalaje @endif
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($products) }} productos</div>
        </div>
    </div><br>
    @stop
@endif

@if (Request::ajax() && isset($products_added))
    @section('content_products_order')
    <table id="product-purchase-order-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Producto</th>
                <th>PLU</th>
                <th>Proveedor</th>
                <th>Stock actual</th>
                <th>Stock solicitado</th>
                <th>Stock minimo</th>
                <th>Stock ideal</th>
                <th>Stock comprometido</th>
                <th>Embalaje</th>
                <th>Cantidad embalaje a solicitar</th>
                <th>Unidades a solicitar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products_added))
            @foreach($products_added as $product)
            <tr @if ($product['type'] == 'Proveedor') class="orders-scheduled_delivery" @endif>
                <td align="left">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}</td>
                <td align="center">{{ $product['provider_plu'] }}</td>
                <td align="left">{{ $product['provider'] }}</td>
                <td align="center">{{ $product['current_stock'] }}</td>
                <td align="center">{{ $product['on_route_stock'] }}</td>
                <td align="center">{{ $product['minimum_stock'] }}</td>
                <td align="center">{{ $product['ideal_stock'] }}</td>
                <td align="center">{{ $product['committed_stock'] }}</td>
                <td align="center">
                @if (!$product['provider_pack_quantity_approach'])
                    1 {{ $product['provider_pack_type'] }} x {{ $product['provider_pack_quantity'] }} Unidades
                @else
                    1 {{ $product['provider_pack_type'] }} x {{ $product['provider_pack_quantity'] }} Unidades <br> (Acepta aproximación)
                @endif
                </td>
                <td align="center">
                @if (!$product['provider_pack_quantity_approach'])
                    {{ $product['quantity_pack_to_request'] }}
                    @if ($product['quantity_pack_to_request'] > 1)
                        {{ $product['provider_pack_type'] }}s
                    @else
                        {{ $product['provider_pack_type'] }}
                    @endif
                @else
                    @if ($product['quantity_pack_to_request'])
                        {{ $product['quantity_pack_to_request'] }}
                        @if ($product['quantity_pack_to_request'] > 1)
                            {{ $product['provider_pack_type'] }}s
                        @else
                            {{ $product['provider_pack_type'] }}
                        @endif

                        @if ($product['product_quantity_unit'])
                            <br> y {{ $product['product_quantity_unit'] }}
                            @if ($product['product_quantity_unit'] > 1)
                                Unidades
                            @else
                                Unidad
                            @endif
                        @endif
                    @else
                    {{ $product['product_quantity_unit'] }} @if ($product['product_quantity_unit'] > 1) Unidades @else Unidad @endif
                    @endif
                @endif
                </td>
                <td align="center">{{ $product['quantity_unit_to_request'] }}</td>
                <td>
                    <div class="btn-group">
                     <a class="btn btn-xs btn-default" id="btn-remove-product" data-id="{{ $product['id']}}">
                        <span class="glyphicon glyphicon-trash"></span>
                     </a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($products_added) }} productos</div>
        </div>
    </div><br>
    @stop
@endif

@section('content_products_query_modal')
<table id="modal-product-request-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Producto</th>
            <th>PLU</th>
            <th>Proveedor</th>
            <th>Stock actual</th>
            <th>Stock solicitado</th>
            <th>Stock minimo</th>
            <th>Stock ideal</th>
            <th>Stock comprometido</th>
            <th>Embalaje</th>
            <th>Cantidad embalaje a solicitar</th>
            <th>Unidades a solicitar</th>
            <th>Agregar</th>
        </tr>
    </thead>
    <tbody>
     @if (isset($products))
         @if (count($products))
            @foreach($products as $product)
            <tr @if ($product['type'] == 'Proveedor') class="orders-scheduled_delivery" @endif>
                <td align="left">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}</td>
                <td align="center">{{ $product['provider_plu'] }}</td>
                <td align="center">{{ $product['provider'] }}</td>
                <td align="center">{{ $product['current_stock'] }}</td>
                <td align="center">{{ $product['on_route_stock'] }}</td>
                <td align="center">{{ $product['minimum_stock'] }}</td>
                <td align="center">{{ $product['ideal_stock'] }}</td>
                <td align="center">{{ $product['committed_stock'] }}</td>
               <td align="center">
                @if (!$product['provider_pack_quantity_approach'])
                    1 {{ $product['provider_pack_type'] }} x {{ $product['provider_pack_quantity'] }} Unidades
                @else
                    1 {{ $product['provider_pack_type'] }} x {{ $product['provider_pack_quantity'] }} Unidades <br> (Acepta aproximación)
                @endif
                </td>
                <td align="center">
                @if (!$product['provider_pack_quantity_approach'])
                    {{ $product['quantity_pack_to_request'] }} @if ($product['quantity_pack_to_request'] > 1) {{ $product['provider_pack_type'] }}s @else {{ $product['provider_pack_type'] }} @endif
                @else
                    @if ($product['quantity_pack_to_request'])
                    {{ $product['quantity_pack_to_request'] }} @if ($product['quantity_pack_to_request'] > 1) {{ $product['provider_pack_type'] }}s @else {{ $product['provider_pack_type'] }} @endif
                        @if ($product['product_quantity_unit'])
                        <br> y {{ $product['product_quantity_unit'] }} @if ($product['product_quantity_unit'] > 1) Unidades @else Unidad @endif
                        @endif
                    @else
                    {{ $product['product_quantity_unit'] }} @if ($product['product_quantity_unit'] > 1) Unidades @else Unidad @endif
                    @endif
                @endif
                </td>
                <td align="center">
                   <input type="text" name="product_{{ $product['id'] }}" id="modal_product_{{ $product['id'] }}" class="product_cant form-control" style="width:50px;text-align:center" value="{{ $product['quantity_unit_to_request'] }}" />
                </td>
                <td align="center">
                    <div class="btn-group">
                     <a class="btn btn-xs btn-default btn-add-product" href="javascript:;" data-id="{{ $product['id'] }}">
                        <span class="glyphicon glyphicon-plus"></span>
                     </a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="15" align="center">Productos no encontrados.</td></tr>
        @endif
    @else
        <tr><td colspan="15" align="center">&nbsp;</td></tr>
    @endif
    </tbody>
</table>
@stop
