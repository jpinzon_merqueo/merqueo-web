@if ( !Request::ajax() )
    @extends('admin.layout')
    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title }}</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" id='main-form' action="{{ route('adminProviders.save') }}" enctype="multipart/form-data">
                        <div class="box-body">
                            <input type="hidden" name="id" value="{{ $provider ? $provider->id : '' }}">
                            <div class="row form-group">
                                <div class="col-xs-4">
                                    <label>Tipo</label>
                                    <select class="form-control" id="type" name="type">
                                        <option value="Para bodega" @if ($provider && $provider->type == 'Para bodega') selected="selected" @endif>Para bodega</option>
                                        <option value="Para faltantes" @if ($provider && $provider->type == 'Para faltantes') selected="selected" @endif>Para faltantes</option>
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="name" placeholder="Ingresa nombre" value="{{ $provider ? $provider->name : '' }}">
                                </div>
                                <div class="col-xs-4">
                                    <label>NIT</label>
                                    <input type="text" class="form-control" name="nit" placeholder="Ingresa NIT"  value="{{ $provider ? $provider->nit : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-4">
                                    <label>Código</label>
                                    <input type="text" class="form-control" name="code" placeholder="Ingresa Código"  value="{{ $provider ? $provider->code : '' }}">
                                </div>
                                <div class="col-xs-4">
                                    <label>Ciudad</label>
                                    <select class="form-control" id="city_id" name="city_id">
                                        @foreach($cities as $city)
                                        <option value="{{ $city->id }}" @if ($provider && $provider->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    <label>Dirección</label>
                                    <input type="text" class="form-control" name="address" placeholder="Ingresa dirección"  value="{{ $provider ? $provider->address : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                 <div class="col-xs-4">
                                    <label>Texto de horario de atención del proveedor</label>
                                    <input type="text" class="form-control" name="schedule" placeholder="Ingresa texto de horario"  value="{{ $provider ? $provider->schedule : '' }}">
                                </div>
                                <div class="col-xs-4">
                                    <label>Hora máxima para entrega de orden de compra al proveedor</label>
                                    <input type="text" class="form-control time" id="maximum_order_report_time" name="maximum_order_report_time" placeholder="Ingresa tiempo en horas"  value="{{ $provider ? $provider->maximum_order_report_time : '' }}">
                                </div>
                                <div class="col-xs-4">
                                    <label>Promesa de servicio en horas</label>
                                    <input type="text" class="form-control" id="delivery_time_hours" name="delivery_time_hours" placeholder="Ingresa promesa de servicio en horas"  value="{{ $provider ? $provider->delivery_time_hours : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-4">
                                    <label>Transportador que recoge y entrega productos en bodega</label>
                                    <select class="form-control" id="transporter" name="transporter">
                                        <option value="Merqueo" @if ($provider && $provider->transporter == 'Merqueo') selected="selected" @endif>Merqueo</option>
                                        <option value="Proveedor" @if ($provider && $provider->transporter == 'Proveedor') selected="selected" @endif>Proveedor</option>
                                    </select>
                                </div>
                                <div class="col-xs-4 @if ($provider && $provider->transporter == 'Proveedor') hide @endif collection_time">
                                    <label>Hora de recogida de productos en proveedor</label>
                                    <input type="text" class="form-control time" id="collection_time" name="collection_time" placeholder="Ingresa tiempo de entrega en horas"  value="{{ $provider ? $provider->collection_time : '' }}">
                                </div>
                                <div class="col-xs-4 @if ($provider && $provider->transporter == 'Proveedor') hide @endif delivery_time">
                                    <label>Hora estimada de despacho de productos a bodega</label>
                                    <input type="text" class="form-control" name="delivery_time" placeholder="Ingresa tiempo de entrega en minutos"  value="{{ $provider ? $provider->delivery_time : '' }}">
                                </div>
                                <div class="col-xs-4  @if ($provider && $provider->transporter == 'Merqueo') hide @endif provider_delivery_time">
                                    <label>Hora despacho de bodega proveedor a bodega de merqueo</label>
                                    <input type="text" class="form-control time" id="provider_delivery_time" name="provider_delivery_time" placeholder="Ingresa tiempo de despacho en horas"  value="{{ $provider ? $provider->provider_delivery_time : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-4">
                                    <label for="provider_type">Tipo proveedor</label>
                                    <select class="form-control" name="provider_type">
                                        <option value="Merqueo" {{ $provider && $provider->provider_type == 'Merqueo' ? "selected" : ''}}>Merqueo</option>
                                        <option value="Marketplace" {{ $provider && $provider->provider_type == 'Marketplace' ? "selected" : ''}}>Marketplace</option>
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    {{ Form::label('financial_condition', 'Condición financiera') }}
                                    {{ Form::fillSelectWithoutKeys('financial_condition', $financial_condition, isset($provider) ? $provider->financial_condition : 'Crédito', array('class' => 'form-control', 'id' => 'financial_condition'), FALSE ) }}
                                </div>
                                <div class="col-xs-4">
                                    {{ Form::label('payment_days', 'Días de pago negociado') }}
                                    {{ Form::fillSelectWithoutKeys('payment_days', $payment_days, ($provider) ? $provider->payment_days : '', ['class' => 'form-control', 'id' => 'payment_days']) }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-4">
                                    {{ Form::label('due_quota', 'Cupo de vencimiento') }}
                                    {{ Form::fillSelectWithoutKeys('due_quota', ['No', 'Si'], ($provider && $provider->financial_condition == 'Crédito' && $provider->due_quota == 1) ? 1 : 0, ['class' => 'form-control', 'id' => 'due_quota']) }}
                                </div>
                                <div class="col-xs-4">
                                    {{ Form::label('credit_limit', 'Cupo de crédito') }}
                                    {{ Form::number('credit_limit', ($provider) ? $provider->credit_limit : '', array('class' => 'form-control', 'id' => 'credit_limit')) }}
                                </div>
                            </div>
                             <div class="row form-group">
                                <div class="col-xs-4">
                                    {{ Form::label('order_days', 'Dias de pedido') }}
                                    {{ Form::text('order_days', ($provider) ? $provider->order_days : '', array('class' => 'form-control', 'id' => 'order_days', 'placeholder' => 'Ingresa los días de pedido')) }}
                                </div>
                                <div class="col-xs-4">
                                    {{ Form::label('delivery_days', 'Dias de entrega') }}
                                    {{ Form::text('delivery_days', ($provider) ? $provider->delivery_days : '', array('class' => 'form-control', 'id' => 'delivery_days', 'placeholder' => 'Ingresa los días de entrega')) }}
                                </div>
                                <div class="col-xs-4">
                                    {{ Form::label('lead_time', 'Lead time') }}
                                    {{ Form::text('lead_time', ($provider) ? $provider->lead_time : '', array('class' => 'form-control', 'id' => 'lead_time', 'placeholder' => 'Ingresa el lead time')) }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-6">
                                    <label>Observaciones</label>
                                    <textarea class="form-control" name="observations" placeholder="Ingresa observación">{{ $provider ? $provider->observations : '' }}</textarea>
                                </div>
                                <div class="col-xs-6">
                                    <label>Estado</label>
                                    <select class="form-control" name="status">
                                        <option value="1" {{ $provider && $provider->status ? "selected" : ''}}>Activo</option>
                                        <option value="0" {{ $provider && !$provider->status ? "selected" : ''}}>Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="@if ($provider && $provider->logo_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Logo</label>
                                    <input type="file" class="form-control" name="logo" >
                                </div>
                                @if ($provider && $provider->logo_url)
                                <div class="col-xs-2">
                                    <img src="{{ $provider->logo_url }}" class="img-responsive" />
                                </div>
                                @endif
                                <div class="@if ($provider && $provider->logo_small_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Logo pequeño</label>
                                    <input type="file" class="form-control" name="logo_small" >
                                </div>
                                 @if ($provider && $provider->logo_small_url)
                                <div class="col-xs-2">
                                    <img src="{{ $provider->logo_small_url }}" class="img-responsive" />
                                </div>
                                @endif
                            </div>
                            <div class="row form-group">
                                <div class="@if ($provider && $provider->rut_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>RUT</label>
                                    <input type="file" class="form-control" name="rut">
                                </div>
                                @if ($provider && $provider->rut_url)
                                <div class="col-xs-2">
                                    <br><br>
                                    <a href="{{ $provider->rut_url }}" target="_blank">Ver archivo</a>
                                </div>
                                @endif
                                <div class="@if ($provider && $provider->bank_certification_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Certificación bancaria</label>
                                    <input type="file" class="form-control" name="bank_certification_url">
                                </div>
                                 @if ($provider && $provider->bank_certification_url)
                                <div class="col-xs-2">
                                    <br><br>
                                    <a href="{{ $provider->bank_certification_url }}" target="_blank">Ver archivo</a>
                                </div>
                                @endif
                            </div>
                            @if ($provider)
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <fieldset>
                                        <legend>Contactos</legend>
                                        <button id="add-contact" type="button" class="btn btn-success">Nuevo Contacto</button>
                                        <div id="contacts_table_container" style="max-height: 400px; overflow-y: auto;">
                                            <!-- ajax section contacts_table_container -->
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Teléfono</th>
                                                        <th>Cargo</th>
                                                        <th>Estado</th>
                                                        <th>Acción</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="7" align="center">
                                                            <img class="u-loader" src="{{ asset('assets/img/loading.gif') }}" alt="">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        @endif
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar </button>
                        </div>
                        {{ Form::token() }}
                        {{ Form::hidden('_method', 'POST') }}
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-contact">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title add-title">Contacto</h4>
                </div>
                <form id="form-contact">
                    <div class="modal-body">
                        <input type="hidden" id="contact_id" name="contact_id">
                        <div class="form-group">
                            <label for="">Nombre completo</label>
                            <input class="form-control new-name" type="text" placeholder="Nombre completo" id="contact_name" name="contact_name" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">Teléfono</label>
                            <input class="form-control new-phone" type="text" placeholder="Teléfono" id="contact_phone" name="contact_phone" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">Correo electrónico</label>
                            <input class="form-control new-email" type="email" placeholder="Correo electrónico" id="contact_email" name="contact_email" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">Cargo</label>
                            <input class="form-control new-position" type="text" placeholder="Cargo" id="contact_position" name="contact_position" required="required">
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <select class="form-control" name="contact_status" id="contact_status">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="save-contact">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--coverage.js-->
    <script type="text/javascript">
    $(document).ready(function() {
    @if ($provider)
        $.ajax({
            url: '{{ route('adminProviders.getContactAjax', ['id' => $provider->id]) }}',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            $('#contacts_table_container').empty();
            $('#contacts_table_container').html(data.html);
        })
        .fail(function() {
            console.error("error al cargar los contactos");
        });


        $('#add-contact').click(function() {
            $('.edit-title').hide();
            $('.add-title, .new-name').show();
            $('#contact_id').val('');
            $('#modal-contact').modal('show');
        });

        $('body').on('click', '.edit-contact', function(event) {
            event.preventDefault();
            var status = $(this).data('status');

            $('#contact_id').val($(this).data('id'));
            $('#contact_name').val($(this).data('name'));
            $('#contact_phone').val($(this).data('phone'));
            $('#contact_email').val($(this).data('email'));
            $('#contact_position').val($(this).data('position'));


            if (status == 1)
               $('#contact_status > option:eq(0)').attr('selected', true);
            else
               $('#contact_status > option:eq(1)').attr('selected', true);

            $('#modal-contact .edit-title').hide();
            $('#modal-contact .add-title, .new-name').show();
            $('#modal-contact').modal('show');
        });

        $('#save-contact').click(function () {
            var validator = $('#form-contact').validate({
                rules: {
                    contact_name: {
                        required: true
                    },
                    contact_phone: {
                        required: true
                    },
                    contact_email: {
                        required: true,
                        email: true
                    }
                }
            });
            if ( validator.form() ) {
                var data = {
                    contact_id: $('#contact_id').val(),
                    name: $('#contact_name').val(),
                    phone: $('#contact_phone').val(),
                    email: $('#contact_email').val(),
                    position: $('#contact_position').val(),
                    status: $('#contact_status').val()
                };
                $(this).addClass('disabled');
                $.ajax({
                    url: '{{ route('adminProviders.saveContactAjax', ['id' => $provider->id]) }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    context: this
                })
                .done(function(data) {
                    if (data.success) {
                        $('#contacts_table_container').empty();
                        $('#contacts_table_container').html(data.html);
                        $('#modal-contact').modal('hide');
                        $('#contact_name').val();
                        $('#contact_phone').val();
                        $('#contact_email').val();
                    }
                })
                .fail(function(data) {
                    console.log("Ocurrio un error al guardar la información del contacto.");
                })
                .always(function () {
                    $(this).removeClass('disabled');
                });
            }
        });

        $('body').on('click', '.delete-contact', function(event) {

            var id = $(this).attr('data-id');
            var url = "{{ admin_url(true) }}/providers/delete/"+id+"/contact";
            var btn = $(this);
            if(confirm('¿Deseas eliminar el contacto?')) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data:{ provider_id: $(this).attr('data-provider-id') },
                    success: function(data) {
                        $('#contacts_table_container').empty();
                        $('#contacts_table_container').html(data.html);
                    }
                });
            }
        });

    @endif
        $("#main-form").validate({
          rules: {
            name: {
                required: true
            },
            nit: {
              required: true
            },
            code: {
              required: true
            },
            phone: {
              required: true
            },
            city_id: {
              required: true
            },
            address: {
              required: true
            },
            schedule: {
              required: true
            },
            maximum_order_report_time: {
              required: true
            },
            collection_time: {
              required: function (element) {
                if ($("#transporter").val() == 'Merqueo')
                    return true;
                else
                    return false;
              }
            },
            delivery_time: {
              required: function (element) {
                if ($("#transporter").val() == 'Merqueo')
                    return true;
                else
                    return false;
              },
              //number: true
            },
            provider_delivery_time: {
              required: function (element) {
                if ($("#transporter").val() == 'Proveedor')
                    return true;
                else
                    return false;
              }
            },
            observations: {
              required: true
            },
            financial_condition: {
                required: true
            },
            payment_days: {
                required: function() {
                    return $('#financial_condition').val() == 'Crédito';
                }
            },
            credit_limit: {
                required: function() {
                    return $('#financial_condition').val() == 'Crédito' && $('#due_date').val() == 0;
                },
                min: 1000000
            }/*,
            rut: {
                required: function() {
                    return $('img.rut-image').length == 0;
                }
            },*/
            /*bank_certification_url: {
                required: function() {
                    return $('img.bank-certification-image').length == 0;
                }
            }*/
          }
        });

        $(".time").each(function(index) {
            var time = $(this).attr('id');
            $('#'+time).datetimepicker({
                format: 'HH:mm'
            });
        });

        if($("#transporter").val() == 'Merqueo')
            $('.provider_delivery_time').addClass('hide');


        if($("#transporter").val() == 'Proveedor'){
            $('.collection_time').addClass('hide');
            $('.delivery_time').addClass('hide');
        }

        $('#financial_condition').change(function(event) {
            var value = $(this).val();
            if ('Crédito' == value) {
                $('#credit_limit, #payment_days, #due_quota').parent()
                    .slideDown();
            } else {
                $('#credit_limit, #payment_days, #due_quota').parent()
                    .slideUp();
            }
        }).trigger('change');

        $('#due_quota').change(function(event) {
            var value = $(this).val();
            if (value == 0) {
                $('#credit_limit').attr('disabled', 'disabled');
            } else {
                $('#credit_limit').removeAttr('disabled', 'disabled');
            }
        }).trigger('change');

        $("#transporter").change(function(event) {
            if($("#transporter").val() == 'Merqueo'){
                $('.collection_time').removeClass('hide');
                $('.delivery_time').removeClass('hide');
                $('.provider_delivery_time').addClass('hide');
            }else{
                $('.collection_time').addClass('hide');
                $('.delivery_time').addClass('hide');
                $('.provider_delivery_time').removeClass('hide');
            }
        });
        var taxesController = (function() {
            'use strict';

            function taxesController(args) {
                // enforces new
                if (!(this instanceof taxesController)) {
                    return new taxesController(args);
                }
                this.bindActions();
            }

            taxesController.prototype.bindActions = function(args) {
                $('body').on('change', '#ica_tax_id', function(){
                    $('#ica_account').val( $(this).find(':selected').data('account') );
                });
                $('#ica_tax_id').trigger('change');
                $('body').on('change', '#fuente_tax_id', function(){
                    $('#fuente_account').val( $(this).find(':selected').data('account') );
                });
                $('#fuente_tax_id').trigger('change');
            };

            return taxesController;
        }());
        var taxes_controller = new taxesController;
    });
    </script>
    @stop
@endif

@if ( Request::ajax() && $section == 'contacts_table_container' )
    @section('contacts_table_container')
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Cargo</th>
                <th>Estado</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @if (count($provider_contacts))
            @foreach ($provider_contacts as $provider_contact)
                <tr>
                   <td>{{ $provider_contact->fullname }}</td>
                   <td>{{ $provider_contact->phone }}</td>
                   <td>{{ $provider_contact->position }}</td>
                   <td>
                       @if ($provider_contact->status)
                       <span class="label label-success">Activo</span>
                       @else
                       <span class="label label-danger">Inactivo</span>
                       @endif
                   </td>
                   <td>
                       <button type="button" class="btn btn-default edit-contact"
                               data-id="{{ $provider_contact->id }}"
                               data-provider-id = "{{ $provider_contact->providerId }}"
                               data-name="{{ $provider_contact->fullname }}"
                               data-phone="{{ $provider_contact->phone }}"
                               data-email="{{ $provider_contact->email }}"
                               data-position="{{ $provider_contact->position }}"
                               data-status="{{ $provider_contact->status }}"
                               >Editar
                       </button>
                       <button type="button" class="btn btn-danger delete-contact"
                               data-id="{{ $provider_contact->id }}"
                               data-provider-id = "{{ $provider_contact->providerId }}"
                               >Eliminar
                       </button>
                   </td>
               </tr>
            @endforeach
            @else
            <tr>
                <td colspan="7" align="center">
                    No hay contactos
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    @stop
@endif
