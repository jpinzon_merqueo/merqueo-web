@extends('admin.layout')

@section('content')

<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
</script>
<a href="#importing" class="fancybox unseen"></a>
<div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminStoreProductRelated.index') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Regresar a lista</button>
        </a>
    </span>
</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Exitoso!</b> {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alerta!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <form role="form" method="post" id='main-form' action="{{ route('adminStoreProductRelated.import') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group col-xs-12">
                            <label>Ciudad</label>
                            <select type="text" class="form-control" id="city_id" name="city_id">
                                <!-- <option value="" selected="selected"></option> -->
                                @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->city}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Bodega</label>
                            <select type="text" class="form-control" id="warehouse_id" name="warehouse_id">
                                <option value="" selected="selected"></option>
                                
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Proceso a ejecutar</label>
                            <select type="text" class="form-control" name="process" id="process">
                                <!-- <option value="" selected="selected"></option> -->
                                <option value="import_products">Importar productos</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Archivo Excel</label>
                            <input type="file" class="form-control" name="file" placeholder="File">
                        </div>
                        <div class="form-group col-xs-12">
                            
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" onclick="">Importar productos</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Update stock Modal -->
<div class="modal fade" id="modal-export-products">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title accept-title">Exportar productos</h4>
            </div>
            <form id="update-to-enrutado" method="POST" action="{{ route('adminProducts.exportProducts') }}" class="form-modal" accept-charset="UTF-8">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group accept-options row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <label class="accept-label" for="store_id">Tienda</label>
                            <select type="text" class="form-control" id="city_id_1" name="city_id">
                                <!-- <option value="" selected="selected"></option> -->
                                @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->city}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
$('#main-form').validate({
  rules: {
    store_id: 'required',
    process: 'required',
    file: 'required',
  }
});

$('#city_id').on('change', function(){
    $.ajax({
        url : warehouse_url_ajax,
        method : 'GET',
        data : { city_id : $('#city_id').val() },
        success : function( request ){
            var options = '';
            $.each(request, function( index, name){
                options += '<option value="'+index+'">'+name+'</option>';
            })
            $('#warehouse_id').html(options);
        }
    });
});
$('#city_id').trigger('change');

$('#main-form').submit(function () {
    if($(this).valid()) {
        if( confirm('Está seguro que quiere importar este archivo?') ){
           $('.fancybox').trigger('click');
        }
    }
});

jQuery(document).ready(function($) {
    $('body').on('click', '.export-products', function(event) {
        event.preventDefault();
        $('#modal-export-products').modal('show');
    });
    $('body').on('click', '.export-products-file', function(event) {
        $('#modal-export-products').modal('hide');
    });
});
</script>

@stop