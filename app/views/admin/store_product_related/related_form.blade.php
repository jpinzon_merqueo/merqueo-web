@if ( !Request::ajax() )
	@extends('admin.layout')

	@section('content')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
	<style type="text/css">
		.search-result{
			max-height: 300px;
			overflow: auto;
		}
	</style>
	<section class="content-header">
		<h1>
			{{ $title }} - {{ $sub_title }}
			<small>Control panel</small>
		</h1>
		<span class="breadcrumb" style="top:0px">
		    <a href="{{ route('adminStoreProductRelated.index') }}">
		    	<button type="button" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Regresar a lista</button>
		    </a>
		    <a id="delete-relation" href="{{ route('adminStoreProductRelated.deleteRelation', ['id' => $id ]) }}">
		    	<button type="button" class="btn btn-danger"><i class="fa fa-trash"></i>Eliminar producto</button>
		    </a>
		</span>
	</section>
	<section class="content">

		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alerta!</b> {{ Session::get('error') }}
		</div>
		@endif

		@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif


		<input type="hidden" name="id" value="{{ $product ? $product->store_product_id : '' }}">
		@if ( $product )
			<div class="tab-content">
				<div class="col-md-12 col-sm-12 col-xs-12" id="home">
					@include('admin.store_product_related.related_form_single', ['product' => $product])
				</div>
				<hr>
				<div class="col-md-12 col-sm-12 col-xs-12" id="group">
					<div class="row product-group @if ( $product->type == 'Simple' ) unseen @endif">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-header">
									<h4 class="box-title">
										Buscar productos para relacionar
									</h4>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-xs-12">
											<div class="row error-search">
												<div class="col-xs-12">
													<div class="alert alert-danger unseen"></div>
													<div class="alert alert-success unseen"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12">
													<table width="100%" class="modal-product-request-table">
														<tbody>
															<tr>
																<td align="right"><label>Buscar:</label>&nbsp;</td>
																<td>
																	<input type="text" placeholder="Nombre, referencia, PLU" name="s" id="s" class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
																</td>
																<td colspan="2" align="left">
																	<button type="button" id="btn-search-form" class="btn btn-primary" data-warehouse_id="{{ $product->warehouse_id }}">Buscar</button>
																</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td colspan="2"><span class="error" style="display: none;">Campo requerido</span></td>
															</tr>
														</tbody>
													</table>
													<div align="center" class="modal-products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 search-result unseen">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row product-group list-related-products">
					</div>
					
				</div>
			</div>
		@else
			@include('admin.products.product_form_only', ['product' => $product])
			@if ( $product )
			<div class="row">
				<div class="col-xs-3 col-xs-offset-9 text-right">
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</div>
			@endif
		@endif
	</section>

	<script type="text/javascript">

		var relatedProducts = ( function(){
			'use strict';

			function relatedProducts( args ){
				if( !( this instanceof relatedProducts ) ){
					return new relatedProducts( args );
				}
				this.url = '{{ route('adminStoreProductRelated.searchProductsAjax') }}';
				this.url_add = '{{ route('adminStoreProductRelated.addProductsAjax') }}';
				this.url_update_priority = '{{ route('adminStoreProductRelated.updatePriority') }}';
				this.url_delete = '{{ route('adminStoreProductRelated.delete') }}';
				this.store_product_id = {{ ( $product ? $product->store_product_id : 'null' ) }};
				this.warehouse_id = {{ ( $product ? $product->warehouse_id : 'null' ) }};
				this.name_product = '{{ ( $product ? $product->name : 'null' ) }}';
				this.term = null;
				this.bindActions();
			}

			relatedProducts.prototype.bindActions = function(){
				var self = this;
				self.ajax_load();

				$('body').on('click', '#btn-search-form', function(event) {
					event.preventDefault();
					var term = $('#s').val();
					self.term = term;
					self.ajax();
				});

				$('#btn-search-form').trigger('click');

				$('body').on( 'click', '.delete_related_product', function(){
					if ( confirm('¿Seguro que quieres eliminar este producto?') ) {
						var id = $(this).data('id');
						self.ajax_delete( id );
					}
				} );

				$('body').on('click', '.btn-add-product', function(event) {
					event.preventDefault();
					var priority = $(this).parents('tr').find('.product_related_priority').val();
					var store_product_related_id = $(this).data('store_product_related_id');
					var store_product_related_name = $(this).data('store_product_related_name');

					if ( priority == '' || priority == 0 || priority > 3 ) {
						$('.error-search .alert-danger').html('Debes ingresar un valor entre 1 y 3 en el campo "prioridad" .').fadeIn().delay(2500).fadeOut();
						return;
					}

					if( $('#cant_related_products').val() == 3 ){
						$('.error-search .alert-danger').html('No es posible relacionar mas de 3 productos, para poder continuar debe eliminar un producto de la lista').fadeIn().delay(2500).fadeOut();
						return;
					}

					var data = {
						priority: priority,
						store_product_related_id: store_product_related_id,
						store_product_id : self.store_product_id,
						name_product_related : store_product_related_name,
						name_product : self.name_product
					}

					self.ajax_add( data );
					
				});
				$('body').on('click', '.relate_product_up_priority', function(event) {
					event.preventDefault();
					var id = $(this).data( 'id' );
					var store_product_related_id = $(this).data( 'store_product_related_id' );
					
					var data = {
						action: 'up',
						store_product_related_id: store_product_related_id,
						store_product_id : self.store_product_id,
						id : id,
					}
					self.ajax_update_priority( data );
					
				} );
				$('body').on('click', '.relate_product_down_priority', function(event) {
					event.preventDefault();
					var id = $(this).data( 'id' );
					var store_product_related_id = $(this).data( 'store_product_related_id' );
					
					var data = {
						action: 'down',
						store_product_related_id: store_product_related_id,
						store_product_id : self.store_product_id,
						id : id,
					}

					self.ajax_update_priority( data );
					
				} );

				$('#delete-relation').on('click', function(event){
					if(confirm('Esta seguro de eliminar este producto')){
						document.location.href = $(this).attr('href')
					}
				});
				

			}

			relatedProducts.prototype.ajax = function () {
				$('.modal-products-request-loading').show();
				$.ajax({
					url: this.url,
					type: 'POST',
					dataType: 'html',
					data: {
							term: this.term,
							warehouse_id: this.warehouse_id,
							search : 1
						},
				})
				.done(function(data) {
					$('.search-result').html(data).removeClass('unseen');
				})
				.fail(function(data) {
					console.log("error al cargar los productos.");
				})
				.always(function(data) {
					$('.modal-products-request-loading').hide();
				});

			}

			relatedProducts.prototype.ajax_load = function(){

				$.ajax({
					url: this.url,
					type: 'POST',
					dataType: 'html',
					data: {
							store_product_id: this.store_product_id,
							related : 1
						},
				})
				.done(function(data) {
					$('.list-related-products').html(data).removeClass('unseen');
				})
				.fail(function(data) {
					console.log("error al cargar los productos.");
				})
				.always(function(data) {
					$('.modal-products-request-loading').hide();
				});
			}

			relatedProducts.prototype.ajax_add = function ( data ) {
				var self = this;
				$('.modal-products-request-loading').show();
				$.ajax({
					url: self.url_add,
					type: 'POST',
					dataType: 'json',
					data: data,
				})
				.done(function(data) {
					if( data.result ){
						$('.error-table .alert-success').html( data.msg ).fadeIn().delay(2500).fadeOut();
						$('.error-search .alert-success').html( data.msg ).fadeIn().delay(2500).fadeOut();
					}else{
						$('.error-table .alert-danger').html( data.msg ).fadeIn().delay(2500).fadeOut();
						$('.error-search .alert-danger').html( data.msg ).fadeIn().delay(2500).fadeOut();
					}
					self.ajax_load();
				})
				.fail(function(data) {
					console.log("error al cargar los productos.");
				})
				.always(function(data) {
					$('.modal-products-request-loading').hide();
				});
			}

			relatedProducts.prototype.ajax_delete = function( id ){
				var self = this;
				$.ajax({
					url: self.url_delete,
					type: 'POST',
					dataType: 'json',
					data: {
							id: id,
						},
				})
				.done(function(data) {
					if( data.result ){
						$('.error-table .alert-success').html( data.msg ).fadeIn().delay(2500).fadeOut();
						$('.error-search .alert-success').html( data.msg ).fadeIn().delay(2500).fadeOut();
					}else{
						$('.error-table .alert-danger').html( data.msg ).fadeIn().delay(2500).fadeOut();
						$('.error-search .alert-danger').html( data.msg ).fadeIn().delay(2500).fadeOut();
					}
					self.ajax_load();
				})
				.fail(function(data) {
					console.log("error al cargar los productos.");
				})
				.always(function(data) {
					$('.modal-products-request-loading').hide();
				});
			}

			relatedProducts.prototype.ajax_update_priority = function( data ){
				var self = this;
				$.ajax({
					url: self.url_update_priority,
					type: 'POST',
					dataType: 'json',
					data: data,
				})
				.done(function(data) {
					if( data.result ){
						$('.error-table .alert-success').html( data.msg ).fadeIn().delay(2500).fadeOut();
						$('.error-search .alert-success').html( data.msg ).fadeIn().delay(2500).fadeOut();
					}else{
						$('.error-table .alert-danger').html( data.msg ).fadeIn().delay(2500).fadeOut();
						$('.error-search .alert-danger').html( data.msg ).fadeIn().delay(2500).fadeOut();
					}
					self.ajax_load();
				})
				.fail(function(data) {
					console.log("error al actualizar los productos.");
				})
				.always(function(data) {
					$('.modal-products-request-loading').hide();
				});
			}

			return relatedProducts;

		}() );
		

		$(document).ready(function() {

			var related_products = new relatedProducts;
			
		});
	</script>

	<script type="text/javascript">
		
	</script>

	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>

	@stop
@else

	@if ( isset($related_products) )
		@section('products_related')
			<input type="hidden" id="cant_related_products" value="{{count($related_products)}}">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header">
						<h4 class="box-title">
							Productos relacionados
						</h4>
						<div class="row error-table">
							<div class="col-xs-12">
								<div class="alert alert-danger unseen"></div>
								<div class="alert alert-success unseen"></div>
							</div>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12 related-products">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Imagen</th>
											<th>Referencia</th>
											<th>Nombre</th>
											<th>Prioridad</th>
											<th>Accion</th>
										</tr>
									</thead>
									<tbody class="tbody">
										@if (isset($related_products))
											@if (count($related_products))
												@foreach ($related_products as $related_product)

													<tr>
														<td><img src="{{$related_product->image_small_url}}" class="img-responsive"></td>
														<td>{{$related_product->reference}}</td>
														<td>{{$related_product->name}}</td>
														<td>{{$related_product->priority}}</td>
														<td>
															
															<div class="btn-group">
											                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
											                        <span class="caret"></span>
											                        <span class="sr-only">Toggle Dropdown</span>
											                    </button>
											                    <ul class="dropdown-menu" role="menu">
											                        <li><a class="delete_related_product" href="javascript:;" data-id="{{$related_product->id}}" data-store_product_related_id="{{$related_product->store_product_related_id}}" data-store_product_id="{{$related_product->store_product_id}}">Eliminar</a></li>
											                    @if ( $related_product->priority > 1 )
											                        <li><a class="relate_product_up_priority" href="javascript:;" data-id="{{$related_product->id}}" data-store_product_related_id="{{$related_product->store_product_related_id}}" data-store_product_id="{{$related_product->store_product_id}}">Subir</a></li>
											                    @endif
											                    @if ( $related_product->priority < 3 )
											                        <li><a class="relate_product_down_priority" href="javascript:;" data-id="{{$related_product->id}}" data-store_product_related_id="{{$related_product->store_product_related_id}}" data-store_product_id="{{$related_product->store_product_id}}">Bajar</a></li>
											                    @endif
											                    </ul>
											                </div>
														</td>
													</tr>
												@endforeach
											@else
												<tr><td colspan="14" align="center">No hay productos asignados.</td></tr>
											@endif
										@else
											<tr><td colspan="14" align="center">No hay productos asignados.</td></tr>
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		@stop
	@endif
	
	@if ( isset($products) )

		@section('products')
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Imagen</th>
						<th>Referencia</th>
						<th>Nombre</th>
						<th>Unidad de medida</th>
						<th>Ciudad</th>
						<th>Prioridad</th>
						<th>Agregar</th>
					</tr>
				</thead>
				<tbody class="tbody">
					@if (count($products))
						@foreach ($products as $product)
							<tr>
								<td><img src="{{$product->image_small_url}}" class="img-responsive" width="60"></td>
								<td>{{$product->reference}}</td>
								<td>{{$product->name}}</td>
								<td>{{$product->quantity}} {{$product->unit}}</td>
								<td>{{$product->city}}</td>
								<td>
									<input type="number" min="1" max="3" name="product_related_priority" class="product_related_priority form-control" value="1">
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-xs btn-default btn-add-product" href="javascript:;" data-store_product_related_name="{{$product->name}}" data-store_product_related_id="{{$product->id}}">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
								</td>
							</tr>
						@endforeach
					@else
						<tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
					@endif
				</tbody>
			</table>
		@stop
	@endif
@endif