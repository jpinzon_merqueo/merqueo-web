@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top:0px;">
        
        <a type="button" href="{{ route('adminStoreProductRelated.import') }}" class="btn btn-primary ">Importar productos </a>
        @if( $status == 1 )
        <a type="button" href="{{ route('adminStoreProductRelated.changeStatus', ['status' => $status]) }}" id="change-status" class="btn btn-warning ">Desactivar productos</a>
        @else
        <a type="button" href="{{ route('adminStoreProductRelated.changeStatus', ['status' => $status]) }}" id="change-status" class="btn btn-success ">Activar productos</a>
        @endif
        
    </span>
</section>
<section class="content">
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form" class="balance-table">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td width="33%">
                                            <select id="city_id" name="city_id" class="form-control" required>
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right">
                                            <label>Bodega:</label>
                                        </td>
                                        <td width="33%">
                                            <select type="text" class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value="" selected="selected"></option>
                                            </select>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td align="right">
                                            <label>Buscar:</label>
                                        </td>
                                        <td>
                                            <input type="text" name="s" placeholder="Nombre producto, referencia" id="input-search" class="form-control">
                                        </td>
                                        
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>                                    </tr>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <br />
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var web_url_ajax = "{{ route('adminStoreProductRelated.index') }}";
    var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
    var delete_product = function(el){
        event.preventDefault();
        if(confirm('Esta seguro de eliminar este producto')){
            document.location.href = $(el).attr('href')
        }

    }
    var get_items = function( search, warehouse_id ){
        $('.paging-loading').show();
        $.ajax( {
            url : web_url_ajax,
            type : 'POST',
            data : { s : search, warehouse_id : warehouse_id },
            dataType : 'json',
            success : function( hxr ){
               $('.paging').html( hxr );
               $('.paging-loading').hide();
            }
        } );
    }

    $(document).ready(function() {
        
        
        $('#btn-search').on('click', function(e) {
            e.preventDefault();
        });

        $('.export-balance').on('click', function(e) {
            e.preventDefault();
            //$(this).addClass('disabled');
            $.ajax({ url: "{{ route('adminVehicles.balanceExport') }}",
                type: 'POST',
                dataType: 'json',
                success:
                    function(response) {
                        $('.loading').hide();
                        //$('.export-balance').removeClass('disabled');

                        if(response.status){
                            window.open(response.file_url,'_blank');
                        }else{
                            console.log("error al consultar los movimientos del shopper.");
                        }
                    }
            });
        });

        $('#city_id').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#city_id').val() },
                success : function( request ){
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse_id').html(options);
                    paging(1, '');
                }
            });
        });
        $('#city_id').trigger('change');
    });
</script>

@else

@section('content')
<table id="balance-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Bodega</th>
            <th>Producto</th>
            <th>Productos relacionados</th>
            <th>Estado</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
    @if (count($list))
        @foreach($list as $product)
        <tr>
            <td>{{ $product->warehouse }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->products_related }}</td>
            <td>
                @if($product->status == 1)
                    <span class="badge bg-green">Activo</span>
                @else
                    <span class="badge bg-orange">Inactivo</span>
                @endif
            </td>
            <td>
                
                <div class="btn-group">
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ route('adminStoreProductRelated.edit', ['id' => $product->store_product_id]) }}">Editar</a></li>
                        <li><a onclick="change_status(this, event)" href="{{ route('adminStoreProductRelated.changeStatusSingle', ['status'=>$product->status, 'id' => $product->store_product_id]) }}">Cambiar estado</a></li>
                        <li><a onclick="delete_product(this, event)" href="{{ route('adminStoreProductRelated.deleteRelation', ['id' => $product->store_product_id]) }}">Eliminar</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="6" align="center">Producto no encontrado</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($list) }} registros</div>
    </div>
</div>

@endif
@stop