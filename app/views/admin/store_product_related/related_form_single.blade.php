<input type="hidden" name="id" id="" value="{{ $product ? $product->id : '' }}">
<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h4 class="box-title">
				Información básica
				</h4>
			</div>
			<div class="box-body">
				<div class="form-group row">
					<div class="col-xs-3">
						<label title="Nombre del producto que le será mostrado al cliente." for="name">Nombre</label>
						<p>{{$product->name}}</p>
					</div>
					<div class="col-xs-2">
						<label title="Formato URL del producto" for="slug">Unidad de medida</label>
						<p>{{$product->quantity}} {{$product->unit}}</p>
					</div>
					<div class="col-xs-2">
						<label title="Referencia del producto" for="reference">Referencia</label>
						<p>{{$product->reference}}</p>
					</div>
					<div class="col-xs-3">
						<label for="">Tienda</label>
						<p>{{ $product->store_name }}</p>
					</div>
					@if ($product && $product->image_medium_url)
					<div class="col-xs-2">
						<img src="{{ $product->image_medium_url }}" width="100" height="100" />
					</div>
					@endif
				</div>
				<div class="form-group row">
				</div>
			</div>
		</div>
	</div>
</div>

