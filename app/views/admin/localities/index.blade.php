@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminLocalities.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Localidad</button>
            </a>
        </span>
    </section>
    <section class="content">
    @if(Session::has('message') )
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @endif
    @endif
        <div class="row">
            <form id="search-form" class="zone-options">
                <div class="form-group col-xs-3">
                    <label>Ciudad</label>
                    <select id="city_id" name="city_id" class="form-control">
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        </div>
        <div class="row">
            <div id="map" class="col-xs-12" style="height: 400px;"></div>
            <div id="market_stores"></div>
        </div>
        <hr>
        <div class="paging"></div>
        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
    </section>

    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key={{ Config::get('app.google_api_key') }}"></script>
    <script type="text/javascript">
        var map = null;
        var localities = [];
        var hexVal = '0123456789ABCDEF'.split('');
        var poly;
        var latitude_map = 0;
        var longitude_map = 0;
        var store_draw = [];
        var store_html = '';
        var localities_draw;
        var web_url_ajax = "{{ route('adminLocalities.index') }}";

        function makeColor() {
            return '#' + hexVal.sort(function(){
                return (Math.round(Math.random())-0.5);
            }).slice(0,6).join('');
        }

        function change_localities() {
            poly = new google.maps.Polyline({
                strokeColor: '#000000',
                strokeOpacity: 1.0,
                strokeWeight: 3
            });

            poly.setMap(map);

            for(var i in localities) {
                var localitiles_array = [];
                var locality_store = [];
                var zone = localities[i];
                locality_store.name = zone.name !== null ? zone.name : '';
                locality_store.delivery_zone = zone.delivery_zone;
                locality_store.active = zone.status;

                if (zone.polygon !== null) {
                    var coverage = zone.polygon.split(',');

                    for(var i = 0; i < coverage.length; i++) {
                        var c = coverage[i].split(' ');
                        localitiles_array.push(new google.maps.LatLng(c[0], c[1]));
                    }
                }

                var coloract = makeColor();
                locality_store.color = coloract;
                var validate = $.inArray(locality_store);
                if (validate) {
                    if (locality_store.active !== 0){
                        store_draw.push(locality_store);
                    }
                }
                localities_draw = new google.maps.Polygon({
                    path: localitiles_array,
                    strokeColor: coloract,
                    strokeOpacity: 1.0,
                    strokeWeight: 3,
                    fillColor: coloract,
                    fillOpacity: 0.35
                });
                localities_draw.setMap(map);
            }

            $.each(store_draw, function(key, value) {
                store_html = store_html + '<li class="'+key+'" style="width:13%; display: inline-block; height:auto; text-align:left;">' +
                    '<a href="#" class="item-filter-locality" idzone="'+key+'" colorzone="'+value.color+'" title="'+value.name+'">' +
                    '<div style="background-color: '+value.color+'; width: 15px; height:15px; display:inline-block !important; vertical-align: top; margin: 4%; border:2px solid black;"></div>' +
                    '<div style="display:inline-block !important; vertical-align: top; margin-top:2%; width: 70%;">'+value.name+'</div></a></li>';
            });

            $('#market_stores').html('<ul id="localities-list" style="margin:5px; display: inline-block; list-style: none; width: 100%;">'+store_html+'</ul>');

            $('.item-filter-locality').on('click', function(e) {
                e.preventDefault();
                var idstore = $(this).attr('idzone');
                var colorzone = $(this).attr('colorzone');
                var objstore = store_draw[idstore];
                var localitiles_array = [];
                var lat = 0;
                var lng = 0;
                if(objstore.polygon !== null) {
                    var coverage = objstore.polygon.split(',');

                    for(var i = 0; i < coverage.length; i++) {
                        var c = coverage[i].split(' ');
                        if (i === 0){
                            lat = parseFloat(c[0]);
                            lng = parseFloat(c[1]);
                        }
                        localitiles_array.push(new google.maps.LatLng(c[0], c[1]));
                    }
                }

                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: { lat: lat, lng: lng }
                });

                var localities_draw = new google.maps.Polygon({
                    path: localitiles_array,
                    strokeColor: colorzone,
                    strokeOpacity: 1.0,
                    strokeWeight: 3,
                    fillColor: colorzone,
                    fillOpacity: 0.35
                });
                localities_draw.setMap(map);
            });
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: { lat: parseFloat(latitude_map), lng: parseFloat(longitude_map) }
            });
            change_localities();
        }

        function load_data() {
            $('.paging').html('');
            $('.paging-loading').show();

            data = {
                city_id: $('#city_id').val()
            };

            $.ajax({
                url: web_url_ajax,
                data: data,
                type: 'GET',
                dataType: 'html',
                success: function(response) {
                    store_draw = [];
                    $('.paging-loading').hide();
                    $('.paging').html(response);
                    $(function() {
                        store_html = '';
                        localities = JSON.parse($('#ls').val());
                        latitude_map = $('#lat').attr('value');
                        longitude_map = $('#lng').attr('value');
                        initMap();
                    });
                }, error: function() {
                    $('.paging-loading').hide();
                    $('.paging').html('Ocurrió un error al obtener los datos.');
                }
            });
        }

        load_data();

        $('#city_id').change(function(e) {
            load_data();
            e.preventDefault();
        });
    </script>
@else
    @section('content')
        <input type="hidden" id="lat" value="{{$lat}}" />
        <input type="hidden" id="lng" value="{{$lng}}" />
        <textarea hidden id="ls">{{ $localities_map }}</textarea>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <table id="zone-table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Ciudad</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($localities as $locality)
                                <tr>
                                    <td>{{ $locality->name }}</td>
                                    <td>
                                        {{$locality->city['city']}}
                                    </td>
                                    <td>
                                        @if($locality->status == 1)
                                            <span class="badge bg-green">Activo</span>
                                        @else
                                            <span class="badge bg-red">Inactivo</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="{{ route('adminLocalities.edit', ['id' => $locality->id]) }}">Editar</a></li>
                                                @if($locality->status == 1)
                                                    <li><a href="{{ route('adminLocalities.delete', ['id' => $locality->id]) }}">Desactivar Localidad</a></li>
                                                @else
                                                    <li><a href="{{ route('adminLocalities.toggle', ['id' => $locality->id]) }}">Activar Localidad</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    @endif
@stop
