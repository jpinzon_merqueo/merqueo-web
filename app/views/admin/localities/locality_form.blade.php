@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div>
                <div class="box-body">
                    <form role="form" method="post" id="main-form" action="{{ route('adminLocalities.save') }}" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Nombre</label>
                                <input type="hidden" name="id" value="{{ $locality->id }}">
                                <input name="name_locality" class="form-control new-name" value="{{ $locality->name }}" type="text" placeholder="Nombre">
                            </div>
                            <div class="col-sm-6">
                                <label>Ciudad</label>
                                <select class="form-control" name="city_id" id="city_id">
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" @if ($locality && $locality->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label title="Estado" for="status">Estado</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{ $locality && (1 === $locality->status) || Input::old('status') === 1 ? "selected" : ''}}>Activo</option>
                                    <option value="0" {{ $locality && (0 === $locality->status) || Input::old('status') === 0 ? "selected" : ''}}>Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="alert alert-danger" style="display:none;" role="alert">
                            <p>Área vacía en zona de delimitación</p>
                        </div>
                        <div id="map_canvas"></div>
                        <div class="row form-group">
                            <div class="col-md-11"></div>
                            <div class="col-md-1" style="text-align: right; margin-top: 25px;">
                                <div class="btn btn-primary undo-poly" style="right: 0px; margin: 5px;">Deshacer</div>
                            </div>
                            <input type="hidden" id="data_locality" name="polygon" value="{{ $locality->polygon }}">
                        </div>

                        <div class="row box-footer">
                            <div class="col-md-11"></div>
                            <div class="col-md-1" style="text-align: right;">
                                <button class="btn btn-primary save-zone">Guardar </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key={{ Config::get('app.google_api_key') }}"></script>
<script src="{{ web_url() }}/admin_asset/js/coverage.js" type="text/javascript"></script>

<style>
    #map_canvas {
        width: 100%;
        height: 400px;
    }
    .new-name {
        margin-bottom: 10px;
    }
</style>

<script type="text/javascript">
var locality = {{ json_encode($locality_map) }};
var hexVal = '0123456789ABCDEF'.split("");
var lat;
var lng;

$(document).on('ready',function() {
    mapPosition(4.687376662169584, -74.0848549141556); // Inicia el mapa

    var editing = -1;
    $('.add-new').click(function() {
        clearMap();
        editing = -1;
        $('.edit-title').hide();
        $('.add-title, .new-name').show();
    });

    $('#modal-zone').on('shown.bs.modal', function() {
        google.maps.event.trigger(map, 'resize');
    });

    $('.save-zone').click(function(e) {
        e.preventDefault();
        var validator = $('#main-form').validate({
                            rules: {
                                name_locality: {
                                    required: true
                                },
                                city_id: {
                                  required: true
                                }
                              }
                        });

        if (validator.form()) {
            var dataact = $('#data_locality').val();
            if (dataact === '' && getCoverage() === '') {
                $('#map_canvas').css('border', '1px solid red');
                $('.alert').css('display', 'block');
            }else{
                if (getCoverage() !== '') $('#data_locality').val(getCoverage());
                $('#map_canvas').css('border', 'none');
                $('.alert').css('display', 'none');
                $('#main-form').submit();
            }
        }
    });

    bind(locality);

    $('#city_id').change(function() {
        $.ajax({
            url: '{{ route('adminLocalities.getCoordinatesByCityAjax') }}',
            type: 'GET',
            dataType: 'JSON',
            data: {city_id: $(this).val()}
        })
        .done(function(data) {
            if (data) mapPosition(data.latitude, data.longitude);
        })
        .fail(function() {
            console.log("falló la consulta para obtener.");
        });
    });
});

function makeColor() {
    return '#' + hexVal.sort(function(){
        return (Math.round(Math.random())-0.5);
    }).slice(0, 6).join('');
}

function clearMap() {
    var path = poly.getPath();
    while(path.length > 0) path.removeAt(0);
}

function bind(locality) {
    var locality_array = [];
    var loc = locality;
    if(loc.polygon !== null && loc.polygon.length) {
        clearMap();
        var coverage = loc.polygon.split(',');
        for(var i = 0; i < coverage.length; i++) {
            var c = coverage[i].split(' ');
            if (i === 0){
                lat = parseFloat(c[0]);
                lng = parseFloat(c[1]);
            }
            addLatLng(new google.maps.LatLng(c[0], c[1]));
        }
        var coloract = makeColor();
        locality_draw = new google.maps.Polygon({
            path: locality_array,
            strokeColor: coloract,
            strokeOpacity: 1.0,
            strokeWeight: 3,
            fillColor: coloract,
            fillOpacity: 0.35
        });
        locality_draw.setMap(map);
        map.setCenter(new google.maps.LatLng(lat, lng));
    } else {
        clearMap();
    }
    map.setZoom(13);
}
</script>
@stop