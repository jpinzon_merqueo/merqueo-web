<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Imagen</th>
			<th>Nombre</th>
			<th>Referencia</th>
			<th>PLU</th>
			<th>Embalaje</th>
			<th>Cantidades recibidas</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		@if (count($products))
			@foreach ($products as $product)
				<tr>
					<td>
						<img src="{{ $product->storeProduct->product->image_small_url }}" class="img-responsive">
					</td>
					<td>{{ $product->product_name }}</td>
					<td>{{ $product->storeProduct->product->reference }}</td>
					<td>{{ $product->storeProduct->provider_plu }}</td>
					<td>{{ $product->pack_description }}</td>
					<td>
						<input type="number" name="grouped_quantity_received" class="form-control text-center grouped_quantity_received" data-handle_expiration_date="{{ $product->handle_expiration_date }}" data-quantity_received="{{ $product->quantity_received }}" data-quantity_expected="{{ $product->quantity_expected }}" data-id="{{ $product->id }}" value="{{ $product->quantity_received }}">
					</td>
					<td>
						@if ( $product->status == 'Recibido' )
							<span class="badge bg-green">
						@elseif ( $product->status == 'No recibido' )
							<span class="badge bg-red">
						@elseif ( $product->status == 'Parcialmente recibido' )
							<span class="badge bg-orange">
						@elseif ( $product->status == 'Dañado' )
							<span class="badge bg-red">
						@elseif ( $product->status == 'Pendiente' )
							<span class="badge bg-orange">
						@else
							<span class="badge">
						@endif
								{{ $product->status }}
							</span>
					</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="9" align="center">
					Productos no encontrados
				</td>
			</tr>
		@endif
	</tbody>
</table>