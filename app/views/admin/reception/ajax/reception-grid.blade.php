@if(count($receptions))
	@foreach($receptions as $reception)
	<tr>
		<td>{{ $reception->id }}</td>
		<td><a href="{{ route('adminProviderOrders.details', ['id' => $reception->provider_order_id]) }}" target="_blank">{{ $reception->provider_order_id }}</a></td>
		<td>
			@if ( isset($reception->providerOrder->warehouse->city->city) )
				{{ $reception->providerOrder->warehouse->city->city }}
			@endif
		</td>
		<td>
			@if ( isset($reception->providerOrder->warehouse->warehouse) )
				{{ $reception->providerOrder->warehouse->warehouse }}
			@endif
		</td>
		<td>
			@if ( isset($reception->providerOrder->provider->name) )
				{{ $reception->providerOrder->provider->name }}
			@endif
		</td>
		<td align="center">
			@if($reception->status == 'En proceso')
				<span class="badge bg-orange">{{ $reception->status }}</span>
			@elseif($reception->status == 'Validación')
				<span class="badge bg-yellow">{{ $reception->status }}</span>
			@else
				<span class="badge bg-green">{{ $reception->status }}</span>
			@endif
		</td>
		<td>{{ $reception->invoice_number }}</td>
		<td>{{ date('d M Y g:i a', strtotime($reception->date)) }}</td>
		<td>{{ $reception->transporter }}</td>
		<td>
			@if(!empty($reception->admin->fullname))
				{{ $reception->admin->fullname }}
			@endif
		</td>
		<td>
			<div class="btn-group">
			   <a class="btn btn-xs btn-default" href="{{ route('adminReception.editReceptions', ['reception_id' => $reception->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
			</div>
		</td>
	</tr>
	@endforeach
@else
	<tr><td colspan="10" align="center">No hay datos.</td></tr>
@endif