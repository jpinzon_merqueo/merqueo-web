<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Imagen</th>
			<th>Nombre</th>
			<th>Referencia</th>
			<th>PLU</th>
			<th>Embalaje</th>
			<th>Cantidad de embalaje</th>
			<th>Cantidades recibidas</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		@if (count($products))
			@foreach ($products as $product)
				<tr>
					<td>
						<img src="{{ $product->storeProduct->product->image_small_url }}" class="img-responsive">
					</td>
					<td>
						{{ $product->product_name }}
						<br>
						@if ( $product->type == 'Proveedor' )
							<a href="javascript:;" class="provider-product" data-id="{{ $product->id }}">Ver productos</a>
						@endif
					</td>
					<td class="text-center">
						{{ $product->storeProduct->product->reference }}
						<br>
						@if ( $product->storeProduct->product->has_barcode )
							<button type="button" class="btn btn-info suggest-reference" data-product_name="{{ $product->product_name }}" data-product_id="{{ $product->id }}">
								Sugerir código
							</button>
						@endif
					</td>
					<td>{{ $product->storeProduct->provider_plu }}</td>
					<td>{{ $product->pack_description }}</td>
					<td>{{ $product->quantity_pack }}</td>
					<td class="text-center">
						@if ( $product->type == 'Proveedor' )
							<span class="badge bg-red">
								Es surtido
							</span>
						@else
							<input type="number" name="quantity_received" class="form-control text-center quantity_received" data-handle_expiration_date="{{ $product->handle_expiration_date }}" data-quantity_expected="{{ $product->quantity_expected }}" data-quantity_received="{{ $product->quantity_received }}" data-id="{{ $product->id }}" value="{{ $product->quantity_received }}">
						@endif
					</td>
					<td>
						@if ( $product->status == 'Recibido' )
							<span class="badge bg-green">
						@elseif ( $product->status == 'No recibido' )
							<span class="badge bg-red">
						@elseif ( $product->status == 'Parcialmente recibido' )
							<span class="badge bg-orange">
						@elseif ( $product->status == 'Dañado' )
							<span class="badge bg-red">
						@elseif ( $product->status == 'Pendiente' )
							<span class="badge bg-orange">
						@else
							<span class="badge">
						@endif
								{{ $product->status }}
							</span>
					</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="10" align="center">
					Productos no encontrados
				</td>
			</tr>
		@endif
	</tbody>
</table>