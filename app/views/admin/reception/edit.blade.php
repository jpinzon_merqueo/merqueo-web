@if (!Request::ajax())
	@extends('admin.layout')
	@section('content')
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
		<style type="text/css">
			#modal-reception-product-group .modal-dialog{
				width: 90%!important;
			}
			@media only screen and (max-width: 600px) {
				.form-group-expiration-date{
					padding: 10px;
				}
			}

		</style>

		<section class="content-header">
			<h1>
				{{ $title }} # {{ $reception->id }} <small>Control panel</small>
			</h1>
			<span class="breadcrumb" style="top:0px">
				@if ( $reception->status == 'Recibido con factura' )
					<a href="{{ route('adminReception.updateReceptionStatus', ['reception_id' => $reception->id, 'status' => 'En proceso']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a En proceso?')">
						<button type="button" class="btn btn-success">Actualizar estado a En proceso</button>
					</a>
				@endif
				@if ( $reception->status == 'En proceso' )
					<a href="{{ route('adminReception.updateReceptionStatus', ['reception_id' => $reception->id, 'status' => 'Recibido']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Recibido?')">
						<button type="button" class="btn btn-success">Actualizar estado a Recibido</button>
					</a>
				@endif
			</span>
		</section>

		<section class="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12">
					@if(Session::has('success'))
					<div class="alert alert-success alert-dismissable">
					    <i class="fa fa-check"></i>
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					    <b>Hecho!</b> {{ Session::get('success') }}
					</div>
					@endif

					@if(Session::has('error'))
					<div class="alert alert-danger alert-dismissable">
					    <i class="fa fa-ban"></i>
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					    <b>Alerta!</b> {{ Session::get('error') }}
					</div>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12">
					<div class="callout callout-info">
						<h4>Datos de Recibo</h4>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-xs-12 form-group">
								<p>
									<label>Estado</label> -
									@if($reception->status == 'En proceso')
										<span class="badge bg-orange">{{ $reception->status }}</span>
									@elseif($reception->status == 'Recibido' || $reception->status == 'Almacenado')
										<span class="badge bg-green">{{ $reception->status }}</span>
									@elseif($reception->status == 'Revisado')
										<span class="badge bg-yellow">{{ $reception->status }}</span>
									@elseif($reception->status == 'Contabilizado')
										<span class="badge bg-green">{{ $reception->status }}</span>
									@elseif($reception->status == 'Cancelado')
										<span class="badge bg-red">{{ $reception->status }}</span>
									@elseif($reception->status == 'Iniciado')
										<span class="badge bg-blue">{{ $reception->status }}</span>
									@elseif($reception->status == 'Recibido con factura')
										<span class="badge bg-grey">{{ $reception->status }}</span>
									@elseif($reception->status == 'Validación')
										<span class="badge bg-yellow">{{ $reception->status }}</span>
									@endif
								</p>
								<p><label>Ciudad</label> - {{ $reception->providerOrder->warehouse->city->city }}</p>
								<p><label>Orden de compra</label> - # <a href="{{ route('adminProviderOrders.details', ['id' => $reception->providerOrder->id]) }}" target="_blank">{{ $reception->providerOrder->id }}</a></p>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<p><label>Proveedor</label> - {{ $reception->providerOrder->provider->name }}</p>
								<p><label>Fecha de creación</label> - {{ date('d M Y g:i a', strtotime($reception->date)) }}</p>
								<p><label>Creado por</label> - {{ $reception->admin->fullname }}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			@if ( Session::has('simple_products_received') && count(Session::get('simple_products_received')) )
				<div class="row">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Productos simples recibidos</h3>
							</div>
							<div class="panel-body">
								<ul class="list-group">
								@foreach ( Session::get('simple_products_received')  as $product)
									<li class="list-group-item"><span class="badge bg-light-blue">{{ $product->quantity_received }}</span><span class="badge bg-aqua">{{ $product->quantity_expected }}</span> {{ $product->storeProduct->product->name }}</li>
								@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
			@endif
			@if ( Session::has('simple_products_partially_received') && count(Session::get('simple_products_partially_received')) )
				<div class="row">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h3 class="panel-title">Productos simples parcialmente recibidos</h3>
							</div>
							<div class="panel-body">
								<ul class="list-group">
								@foreach ( Session::get('simple_products_partially_received')  as $product)
									<li class="list-group-item"><span class="badge bg-light-blue">{{ $product->quantity_received }}</span><span class="badge bg-aqua">{{ $product->quantity_expected }}</span> {{ $product->storeProduct->product->name }}</li>
								@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
			@endif

			@if ( Session::has('provider_products_received') && count(Session::get('provider_products_received')) )
				<div class="row">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Productos surtidos recibidos</h3>
							</div>
							<div class="panel-body">
								@foreach ( Session::get('provider_products_received')  as $product)
									<div class="list-group">
										<h4 class="list-group-item-heading">{{ $product->storeProduct->product->name }}</h4>
										<p class="list-group-item-text">
											<ul class="list-group">
												@foreach ($product->providerOrderReceptionDetailProductGroup as $prod)
													<li class="list-group-item"><span class="badge bg-light-blue">{{ $prod->quantity_received }}</span><span class="badge bg-aqua">{{ $prod->quantity_expected }}</span> {{ $prod->storeProduct->product->name }}</li>
												@endforeach
											</ul>
										</p>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endif
		@if ( $reception->status == 'En proceso' )
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<legend>Productos recibidos</legend>
							<div class="row form-group">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<label>Buscar por:</label>
									<div class="pull-right">
										<button id="unknow-product" class="btn btn-primary">Producto no encontrado</button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<input type="text" placeholder="Referencia, PLU, nombre" id="search-products" class="form-control">
								</div>
							</div>
							<hr>
							<div id="reception_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Alerta!</b> <div class="message"></div>
							</div>
							<div id="reception_products_success" class="alert alert-success alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="message"></div>
							</div>
							<div align="center" class="loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
							<div id="products_table_container"></div>
						</div>
					</div>
				</div>
			</div>
		@endif
		</section>

		<!--=============================
		=            Modales            =
		==============================-->
		<div class="modal fade" id="modal-reception-product-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Productos del Producto Agrupado</h4>
						<br>
						<div id="reception_grouped_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
							<i class="fa fa-ban"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<b>Alerta!</b> <div class="message"></div>
						</div>
						<div id="reception_grouped_products_success" class="alert alert-success alert-dismissable" style="display: none;">
							<i class="fa fa-ban"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<div class="message"></div>
						</div>
					</div>
					<div class="modal-body">
						<div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
					</div>
					<div class="modal-footer">
						<button id="update-group-quantity" type="button" class="btn btn-default" style="display: none;">Guardar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<!--==============================================
		=            Sugerencia de referencia            =
		===============================================-->
		<div class="modal fade" id="modal-reception-product-reference" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form id="suggest-reference-form" action="{{ route('adminReception.addSuggestedReferenceAjax', ['reception_id' => $reception->id ]) }}" method="POST">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Actualizar referencia para: <span class="product-name"></span></h4>
							<br>
							<div id="reception_grouped_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Alerta!</b> <div class="message"></div>
							</div>
							<div id="reception_grouped_products_success" class="alert alert-success alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="message"></div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<label for="reference" >Ingrese referencia</label>
									<input type="text" name="reference" id="reference" class="form-control required" required="required">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" class="product_id" name="product_id" value="">
							<button id="suggest-reference" type="submit" class="btn btn-primary">Guardar</button>
							{{-- <input type="submit" name="suggest-reference" id="suggest-reference" class="btn btn-warning"> --}}
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!--============================================
		=            Producto no encontrado            =
		=============================================-->
		<div class="modal fade" id="modal-reception-product-unknow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form id="unknow-reference-form" action="{{ route('adminReception.addUnknowReferenceAjax', ['reception_id' => $reception->id ]) }}" method="POST">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Producto no encontrado</h4>
							<br>
							<div id="reception_grouped_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Alerta!</b> <div class="message"></div>
							</div>
							<div id="reception_grouped_products_success" class="alert alert-success alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="message"></div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<label for="reference" >Ingrese referencia</label>
									<input type="text" name="reference" id="unknow-reference" class="form-control required" required="required">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button id="unknow-reference-btn" type="submit" class="btn btn-primary">Guardar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!--=====================================
		=            Producto simple            =
		======================================-->
		<div class="modal fade" id="modal-reception-product-expiration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form id="expiration-form" action="{{ route('adminReception.updateProductExpirationDateAjax', ['reception_id' => $reception->id ]) }}" method="POST" class="">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">Fecha de expiración</h4>
							<br>
							<div id="reception_grouped_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Alerta!</b> <div class="message"></div>
							</div>
							<div id="reception_grouped_products_success" class="alert alert-success alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="message"></div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<label for="expiration_date" >Ingrese fecha de vencimiento</label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-5 form-group form-group-expiration-date">
									<label for="expiration_date" >Año</label>
									<select class="form-control required" name="expiration_date_year" required="required">
										@for($i=0; $i<3; $i++)
											<option value="{{ date('Y')+$i }}">{{ date('Y')+$i }}</option>
										@endfor
									</select>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-3 form-group form-group-expiration-date">
									<label for="expiration_date" >Mes</label>
									<select class="form-control required" name="expiration_date_month" required="required">
										@for($i=1; $i<=12; $i++)
											<option value="{{ $i }}">{{ $i }}</option>
										@endfor
									</select>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-3 form-group form-group-expiration-date">
									<label for="expiration_date" >Día</label>
									<select class="form-control required" name="expiration_date_day" required="required">
										@for($i=1; $i<=31; $i++)
											<option value="{{ $i }}">{{ $i }}</option>
										@endfor
									</select>
								</div>
								<input type="hidden" name="detail_id" id="detail_id">
								<input type="hidden" name="quantity_received" id="quantity_received">

							</div>
						</div>
						<div class="modal-footer">
							<button id="expiration-btn" type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!--=======================================
		=            producto agrupado            =
		========================================-->
		<div class="modal fade" id="modal-reception-product-grouped-expiration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form id="expiration-grouped-form" action="{{ route('adminReception.updateProductGroupedExpirationDateAjax', ['reception_id' => $reception->id ]) }}" method="POST">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">Fecha de expiración</h4>
							<br>
							<div id="reception_grouped_products_error" class="alert alert-danger alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Alerta!</b> <div class="message"></div>
							</div>
							<div id="reception_grouped_products_success" class="alert alert-success alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="message"></div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<label for="expiration_date" >Ingrese fecha de vencimiento</label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-5 form-group form-group-expiration-date">
									<label for="expiration_date" >Año</label>
									<select class="form-control required" name="expiration_date_year" required="required">
										@for($i=0; $i<3; $i++)
											<option value="{{ date('Y')+$i }}">{{ date('Y')+$i }}</option>
										@endfor
									</select>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-3 form-group form-group-expiration-date">
									<label for="expiration_date" >Mes</label>
									<select class="form-control required" name="expiration_date_month" required="required">
										@for($i=1; $i<=12; $i++)
											<option value="{{ $i }}">{{ $i }}</option>
										@endfor
									</select>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-3 form-group form-group-expiration-date">
									<label for="expiration_date" >Día</label>
									<select class="form-control required" name="expiration_date_day" required="required">
										@for($i=1; $i<=31; $i++)
											<option value="{{ $i }}">{{ $i }}</option>
										@endfor
									</select>
								</div>
								<div class="col-lg-12 col-md-12 col-xs-12">
									<input type="hidden" name="grouped_detail_id" id="grouped_detail_id">
									<input type="hidden" name="quantity_received" id="quantity_received">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button id="expiration-btn" type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		@include('admin.reception.js.edit-js')
	@endsection
@endif