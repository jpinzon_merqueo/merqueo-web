<script type="text/javascript">
    var productReception = (function () {
        'use strict';

        function productReception() {
            // enforces new
            if (!(this instanceof productReception)) {
                return new productReception();
            }
            this.bindActions();
            // constructor body
        }

        productReception.prototype.bindActions = function () {
            let self = this;

            $('#expiration_date').datetimepicker({
                format: 'DD/MM/YYYY',
            });
            $('#expiration_grouped_date').datetimepicker({
                format: 'DD/MM/YYYY',
            });

            //Busca los productos al presionar la tecla enter
            $('#search-products').keyup(function (event) {
                var code = event.which;
                if (code == 32 || code == 13 || code == 188 || code == 186) {
                    if ($(this).val() == '') {
                        $('#reception_products_error .message').empty().html('El campo de búsqueda no puede estar vacío.');
                        $('#reception_products_error').slideDown('fast');
                        return;
                    }
                    self.search_products();
                }
            });

            // Muestra los productos que son tipo proveedor
            $('body').on('click', '.provider-product', function (event) {
                event.preventDefault();
                let product_id = $(this).data('id');
                self.get_grouped_products(product_id);
            });

            // Al ocultar el modal de productos proveedor, se elimina el html de ese modal
            // dejando el modal listo para cargar nueva información.
            $('#modal-reception-product-group').on('hidden.bs.modal', function () {
                $(this).find('.modal-body').empty();
            })

            // Al mostrarse el modal y tener cargada la información del producto
            // deja el focus en el campo de cantidades recibidas del producto.
            $('#modal-reception-product-group').on('shown.bs.modal', function (event) {
                $(".grouped_quantity_received:eq(0)").focus().select();
            });

            // Al presionar enter en el campo de cantidades recibidas, se ejecuta la función
            // para guardarse las cantidades
            $('body').on('keyup', '.quantity_received', function (event) {

                if ($(this).val().length > 3) {
                    var txt = $(this).val();
                    $(this).val(txt.substring(0, 3));
                }
                var code = event.which;
                if (code == 32 || code == 13 || code == 188 || code == 186) {
                    if ($(this).val() == '') {
                        $('#reception_products_error .message').empty().html('El campo de cantidades no puede estar vacío.');
                        $('#reception_products_error').slideDown('fast');
                        return;
                    }
                    if ( $(this).val() < 0 ) {
                        $('#reception_products_error .message').empty().html('El campo de cantidades no debe ser menor a 0.');
                        $('#reception_products_error').slideDown('fast');
                        return;
                    }
                    let id = $(this).data('id');
                    let quantity_received = $(this).val();
                    let quantity_expected = $(this).data('quantity_expected');
                    let data_quantity_received = $(this).data('quantity_received');
                    let handle_expiration_date = $(this).data('handle_expiration_date');

                    self.validate_products(id, data_quantity_received, quantity_received, quantity_expected, handle_expiration_date);


                }
            });

            // Al presionarse enter en el campo de cantidades en los modales de productos proveedor
            // se ejecuta la función para guardar las cantidades
            $('body').on('keyup', '.grouped_quantity_received', function (event) {
                var code = event.which;
                if (code == 32 || code == 13 || code == 188 || code == 186) {
                    if ($(this).val() == '') {
                        $('#reception_grouped_products_error .message').empty().html('El campo de cantidades no puede estar vacío.');
                        $('#reception_grouped_products_error').slideDown('fast');
                        return;
                    }
                    if ($(this).val().length > 3) {
                        var txt = $(this).val();
                        $(this).val(txt.substring(0, 3));
                    }
                    let id = $(this).data('id');
                    let quantity_expected = $(this).data('quantity_expected');
                    let data_quantity_received = $(this).data('quantity_received');
                    let quantity_received = $(this).val();
                    let handle_expiration_date = $(this).val('handle_expiration_date');

                    self.validate_grouped_products(id, data_quantity_received, quantity_received, quantity_expected, handle_expiration_date);
                }
            });

            // Función para ocultar las alertas generadas cuando se detecta un intención de modificar cantidades
            $(document).on('click', 'section.content, .modal', function (event) {
                $('#reception_grouped_products_error, #reception_grouped_products_success').slideUp('fast');
                $('#reception_products_error, #reception_products_success').slideUp('fast');
            });

            // Función para ocultar las alertas generadas cuando se detecta un intención de modificar cantidades
            $(document).on('keydown', 'section.content, .modal', function (e) {
                var keyCode = e.keyCode || e.which;

                $('#reception_grouped_products_error, #reception_grouped_products_success').slideUp('fast');
                $('#reception_products_error, #reception_products_success').slideUp('fast');
            });

            // Función para mostrar modal de referencia sugerida
            $('body').on('click', '.suggest-reference', function (event) {
                let product_name = $(this).data('product_name');
                let product_id = $(this).data('product_id');
                $('#modal-reception-product-reference .product-name').empty().html(product_name);
                $('#modal-reception-product-reference .product_id').val(product_id);
                $('#modal-reception-product-reference').modal('show');
            });
            $('#modal-reception-product-reference').on('shown.bs.modal', function (event) {
                $("#modal-reception-product-reference #reference").focus();
            });
            $('#modal-reception-product-reference').on('hide.bs.modal', function (event) {
                $('#modal-reception-product-reference .product-name').empty();
                $('#modal-reception-product-reference .product_id').empty();
                $("#modal-reception-product-reference #reference").val('');
            });

            $('#suggest-reference-form').submit(function (event) {
                event.preventDefault();
                $('#suggest-reference').prop('readonly', true);
                self.add_suggested_reference();
            });

            $('#unknow-reference-form').submit(function (event) {
                event.preventDefault();
                $('#unknow-reference').prop('readonly', true);
                self.add_unknown_reference();
            });

            $('body').on('click', '#unknow-product', function (event) {
                event.preventDefault();
                $('#modal-reception-product-unknow').modal('show');
            });
            $('#modal-reception-product-unknow').on('shown.bs.modal', function (event) {
                $("#unknow-reference-form #unknow-reference").focus();
            });
            $('#modal-reception-product-unknow').on('hidden.bs.modal', function (event) {
                $("#unknow-reference-form #unknow-reference").val('');
            });

            $('#modal-reception-product-expiration').on('hidden.bs.modal', function (event) {
                $('#detail_id').val('');
            });
            $('#modal-reception-product-grouped-expiration').on('hidden.bs.modal', function (event) {
                $('#grouped_detail_id').val('');
            });
            $('#expiration-form').submit(function (event) {
                event.preventDefault();
                self.update_simple_expiration_date_ajax();
            });
            $('#expiration-grouped-form').submit(function (event) {
                event.preventDefault();
                self.update_grouped_expiration_date_ajax();
            });
        };

        // actualizar fecha de vencimiento de producto simple
        productReception.prototype.update_simple_expiration_date_ajax = function () {
            let self = this;
            $('.loading').show('fast');
            $.ajax({
                url: '{{ route('adminReception.updateProductExpirationDateAjax', ['reception_id' => $reception->id]) }}',
                data: $('#expiration-form').serialize(),
                type: 'POST',
                dataType: 'json'
            })
                .done(function (data) {
                    $('#products_table_container').empty();
                    if (data.success) {
                        $('#reception_products_success .message').empty().html(data.success);
                        $('#reception_products_success').slideDown('fast');
                        let id = $('#detail_id').val();
                        let quantity_received = $('#quantity_received').val();
                        self.update_products(id, quantity_received);
                    } else {
                        $('#reception_products_error .message').empty().html(data.error);
                        $('#reception_products_error').slideDown('fast');
                    }
                    $("#search-products").focus();
                    $('#modal-reception-product-expiration').modal('hide');
                })
                .fail(function () {
                    console.error("error al guardar los productos.");
                })
                .always(function () {
                    $('.loading').hide('fast');
                });
        };

        // actualizar fecha de vencimiento de producto agrupado
        productReception.prototype.update_grouped_expiration_date_ajax = function () {
            let self = this;
            $('.loading').show('fast');
            $.ajax({
                url: '{{ route('adminReception.updateProductGroupedExpirationDateAjax', ['reception_id' => $reception->id]) }}',
                data: $('#expiration-grouped-form').serialize(),
                type: 'POST',
                dataType: 'json'
            })
                .done(function (data) {
                    $('#products_table_container').empty();
                    if (data.success) {
                        $('#reception_products_success .message').empty().html(data.success);
                        $('#reception_products_success').slideDown('fast');
                        let id = $('#grouped_detail_id').val();
                        let quantity_received = $('#quantity_received').val();
                        self.update_grouped_products(id, quantity_received);
                    } else {
                        $('#reception_products_error .message').empty().html(data.error);
                        $('#reception_products_error').slideDown('fast');
                        $('#reception_grouped_products_error .message').empty().html(data.error);
                        $('#reception_grouped_products_error').slideDown('fast');
                        $('.grouped_quantity_received[data-id="' + data.grouped_detail_id + '"]').parents('tr').find('.badge').toggleClass('bg-green').toggleClass('bg-red').html('No recibido');
                    }
                    $("#search-products").focus();
                    $('#modal-reception-product-grouped-expiration').modal('hide');
                })
                .fail(function () {
                    console.error("error al guardar los productos.");
                })
                .always(function () {
                    $('.loading').hide('fast');
                });
        };

        // función para buscar los productos
        productReception.prototype.search_products = function () {
            $('.loading').show('fast');
            $.ajax({
                url: '{{ route('adminReception.getProductsAjax', ['reception_id' => $reception->id]) }}',
                data: {s: $('#search-products').val()},
                type: 'GET',
                dataType: 'json'
            })
                .done(function (data) {
                    $('#products_table_container').empty();
                    $('#products_table_container').html(data.html);
                    if (data.error) {
                        $('#reception_products_error .message').empty().html(data.message);
                        $('#reception_products_error').slideDown('fast');
                    }
                    $(".quantity_received:eq(0)").focus().select();
                })
                .fail(function () {
                    console.error("error al cargar los productos.");
                })
                .always(function () {
                    $('.loading').hide('fast');
                    $('#search-products').val('');
                });
        };

        // función para obtener los productos proveedor
        productReception.prototype.get_grouped_products = function (id) {
            $('.loading').show('fast');
            $.ajax({
                url: '{{ route('adminReception.getProductGroupAjax', ['reception_id' => $reception->id]) }}',
                data: {product_id: id},
                type: 'GET',
                dataType: 'json'
            })
                .done(function (data) {
                    $('#modal-reception-product-group .modal-body').html(data.html);
                    $('#modal-reception-product-group').modal('show');
                })
                .fail(function () {
                    console.error("error al cargar los productos.");
                })
                .always(function () {
                    $('.loading').hide('fast');
                });
        };

        productReception.prototype.update_products = function (id, quantity_received) {
            $('.loading').show('fast');
            $.ajax({
                url: '{{ route('adminReception.updateProductAjax', ['reception_id' => $reception->id]) }}',
                data: {product_id: id, quantity_received: quantity_received},
                type: 'POST',
                dataType: 'json'
            })
                .done(function (data) {
                    $('#products_table_container').empty();
                    if (data.success) {
                        $('#products_table_container').html(data.html);
                        $('#reception_products_success .message').empty().html(data.message);
                        $('#reception_products_success').slideDown('fast');
                    } else {
                        $('#products_table_container').html(data.html);
                        $('#reception_products_error .message').empty().html(data.message);
                        $('#reception_products_error').slideDown('fast');
                    }
//				if ( data.handle_expiration_date ) {
//					$('#modal-reception-product-expiration').modal({
//						backdrop: 'static',
//    					keyboard: false,
//    					show: true
//					});
//					$('#detail_id').val(id);
//				}
                    $("#search-products").focus();
                })
                .fail(function () {
                    console.error("error al guardar los productos.");
                })
                .always(function () {
                    $('.loading').hide('fast');
                });
        };

        productReception.prototype.update_grouped_products = function (id, quantity_received, quantity_expected) {
            $('.loading').show('fast');
            $.ajax({
                url: '{{ route('adminReception.updateGroupedProductAjax', ['reception_id' => $reception->id]) }}',
                data: {product_id: id, quantity_received: quantity_received, quantity_expected: quantity_expected},
                type: 'POST',
                dataType: 'json'
            })
                .done(function (data) {
                    $('#modal-reception-product-group .modal-body').empty();
                    $('#products_table_container').empty();
                    if (data.success) {
                        $('#modal-reception-product-group .modal-body').html(data.html);
                        $('#products_table_container').html(data.html_single);
                        $('#reception_grouped_products_success .message').empty().html(data.message);
                        $('#reception_grouped_products_success').slideDown('fast');
                        $(".grouped_quantity_received:eq(0)").focus().select();
                    } else {
                        $('#modal-reception-product-group .modal-body').html(data.html);
                        $('#products_table_container').html(data.html_single);
                        $('#reception_grouped_products_error .message').empty().html(data.message);
                        $('#reception_grouped_products_error').slideDown('fast');
                        $(".grouped_quantity_received:eq(0)").focus().select();
                    }
                })
                .fail(function () {
                    console.error("error al guardar los productos.");
                })
                .always(function () {
                    $('.loading').hide('fast');
                });
        };
        productReception.prototype.validate_products = function (id, data_quantity_received, quantity_received, quantity_expected, handle_expiration_date) {
            let self = this;
            if (data_quantity_received > 0) {
                let valid = confirm('Ya se recibieron ' + data_quantity_received + ' unidades de este producto, ¿Deseas sumarle ' + quantity_received + '?');
                if (valid) {
                    if (handle_expiration_date) {
                        $('#modal-reception-product-expiration').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        });
                        $('#detail_id').val(id);
                        $('#quantity_received').val(quantity_received);
                    } else {
                        self.update_products(id, quantity_received);
                    }
                }
                return
            }
            if (quantity_received > quantity_expected) {
                let valid = confirm('La cantidad recibida es superior a la cantidad ordenada, ¿deseas guardar esta cantidad?');
                if (valid) {
                    if (handle_expiration_date) {
                        $('#modal-reception-product-expiration').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        });
                        $('#detail_id').val(id);
                        $('#quantity_received').val(quantity_received);
                    } else {
                        self.update_products(id, quantity_received);
                    }
                }
            } else {
                if (handle_expiration_date) {
                    $('#modal-reception-product-expiration').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                    $('#detail_id').val(id);
                    $('#quantity_received').val(quantity_received);
                } else {
                    self.update_products(id, quantity_received);
                }
            }
        };

        productReception.prototype.validate_grouped_products = function (id, data_quantity_received, quantity_received, quantity_expected, handle_expiration_date) {

            let self = this;

            if (data_quantity_received > 0) {
                let valid = confirm('Ya se recibieron ' + data_quantity_received + ' unidades de este producto, ¿Deseas sumarle ' + quantity_received + '?');
                if (valid) {
                    if (handle_expiration_date) {
                        $('#modal-reception-product-grouped-expiration').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        });
                        $('#grouped_detail_id').val(id);
                        $('#quantity_received').val(quantity_received);
                    } else {

                        self.update_grouped_products(id, quantity_received);
                    }
                }
                return
            }
            if (quantity_received > quantity_expected) {
                let valid = confirm('La cantidad recibida es superior a la cantidad ordenada, ¿deseas guardar esta cantidad?');
                if (valid) {
                    if (handle_expiration_date) {
                        $('#modal-reception-product-grouped-expiration').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        });
                        $('#grouped_detail_id').val(id);
                        $('#quantity_received').val(quantity_received);
                    } else {

                        self.update_grouped_products(id, quantity_received);
                    }
                }
            } else {
                if (handle_expiration_date) {
                    $('#modal-reception-product-grouped-expiration').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                    $('#grouped_detail_id').val(id);
                    $('#quantity_received').val(quantity_received);
                } else {
                    self.update_grouped_products(id, quantity_received);
                }
            }
        };

        productReception.prototype.add_suggested_reference = function () {
            $.ajax({
                url: '{{ route('adminReception.addSuggestedReferenceAjax', ['reception_id' => $reception->id ]) }}',
                type: 'POST',
                dataType: 'json',
                data: $('#suggest-reference-form').serialize(),
            })
                .done(function (data) {
                    if (data.success) {
                        $('#reception_products_success .message').html(data.success);
                        $('#reception_products_success').slideDown();
                    } else {
                        $('#reception_products_error .message').html(data.error);
                        $('#reception_products_error').slideDown();
                    }
                    $('#modal-reception-product-reference').modal('hide');
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    $('#suggest-reference').prop('disabled', false);
                    console.log("complete");
                });
        };

        productReception.prototype.add_unknown_reference = function () {
            $.ajax({
                url: '{{ route('adminReception.addUnknowReferenceAjax', ['reception_id' => $reception->id ]) }}',
                type: 'POST',
                dataType: 'json',
                data: $('#unknow-reference-form').serialize()
            })
                .done(function (data) {
                    if (data.success) {
                        $('#reception_products_success .message').html(data.success);
                        $('#reception_products_success').slideDown();
                    } else {
                        $('#reception_products_error .message').html(data.error);
                        $('#reception_products_error').slideDown();
                    }
                    $('#modal-reception-product-unknow').modal('hide');
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    $('#unknow-reference').prop('readonly', false);
                    console.log("complete");
                });

        };

        return productReception;
    }());
    $(document).ready(function () {
        var product_reception = new productReception;
        $("#suggest-reference-form").validate();
        $("#unknow-reference-form").validate();
        $("#expiration-form").validate();
    });
</script>