@extends('admin.layout')

@section('content')
	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
	</section>
	<section class="content">
		@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alerta!</b> {{ Session::get('error') }}
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<form id="search-form">
									<table width="100%" class="admin-providers-table">
										<tr>
											<td align="right">
											<label>Ciudad:</label>
											</td>
											<td>
												<select id="city_id" name="city_id" class="form-control">
													@foreach ($cities as $city)
														<option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
													@endforeach
												</select>
											</td>
											<td align="right"><label>Proveedor:</label>&nbsp;</td>
											<td>
												<select id="provider_id" name="provider_id" class="form-control">
													<option value="">Selecciona</option>
													@if (count($providers))
														@foreach ($providers as $provider)
															@if ($provider->city_id == Session::get('admin_city_id'))
																<option value="{{ $provider->id }}">{{ $provider->name }}</option>
															@endif
														@endforeach
													@endif
												</select>
											</td>
											<td align="right"><label>Buscar:</label>&nbsp;</td>
											<td>
												<input name="search_term" type="text" placeholder="Nro. Recibo de bodega" class="form-control">
											</td>
										</tr>
										<tr>
											<td align="right"><label>Estado:</label>&nbsp;</td>
											<td>
												<select id="status" name="status[]" multiple="multiple" class="form-control">
													<option value="Recibido con factura">Recibido con factura</option>
													<option value="En proceso" selected="selected">En proceso</option>
													<option value="Recibido">Recibido</option>
													<option value="Almacenado">Almacenado</option>
													<option value="Cancelado">Cancelado</option>
													<option value="Revisado">Revisado</option>
													<option value="Contabilizado">Contabilizado</option>
												</select>
											</td>
											<td align="right"><label>Tipo de proveedor:</label>&nbsp;</td>
											<td>
												<select id="status" name="provider_type" class="form-control">
													<option value="">Selecciona</option>
													<option value="Merqueo">Merqueo</option>
													<option value="Marketplace">Marketplace</option>
												</select>
											</td>
											<td>&nbsp;</td>
											<td>
												<button type="submit" id="btn-reception-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
												<button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
						<br>
						<div class="paging"></div>
						<div align="center" class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<table id="reception-table" class="admin-reception-table table table-bordered table-striped">
							<thead>
								<tr>
									<th>Recibo de bodega #</th>
									<th>Orden de compra #</th>
									<th>Ciudad</th>
									<th>Bodega</th>
									<th>Proveedor</th>
									<th>Estado</th>
									<th>Número de factura</th>
									<th>Fecha de recibo</th>
									<th>Transportador</th>
									<th>Usuario</th>
									<th>Editar</th>
								</tr>
							</thead>
							<tbody class="tbody">
							</tbody>
						</table>
						<div class="row">
							<div class="col-xs-3">
								<div class="dataTables_info" id="reception-table_info">
									Mostrando <span id="count-receptions"></span> ítems de <span id="total-receptions"></span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div align="center" class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
			<div class="col-xs-12 text-center" id="paginate-links">
			</div>
		</div>
	</section>

	@include('admin.reception.js.index-js')

@endsection