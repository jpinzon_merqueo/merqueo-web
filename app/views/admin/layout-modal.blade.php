<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Merqueo | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="{{ web_url() }}/admin_asset/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/base.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />
        <script src="{{ web_url() }}/assets/js/jquery.min.js"></script>
        <script src="{{ web_url() }}/assets/js/jquery.validate.min.js"></script>
        <script src="{{ web_url() }}/assets/js/moment.js" type="text/javascript"></script>
        <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="{{ web_url() }}/admin_asset/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <link href="{{ web_url() }}/admin_asset/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>
        <script src="{{ web_url() }}/admin_asset/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

        <script src="{{ web_url() }}/admin_asset/js/realtime/rsvp.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/realtime/geofire.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/realtime/lodash.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="https://cdn.jsdelivr.net/lodash/4.6.1/lodash.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/angular.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/angular-simple-logger.min.js" type="text/javascript"></script>
        <!--<script src="{{ web_url() }}/admin_asset/js/angular-google-maps.min.js" type="text/javascript"></script>-->
        <script src="{{ web_url() }}/admin_asset/js/angularfire.min.js" type="text/javascript"></script>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/jquery.nestable.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/general.css">
        <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/validation-picking.css">
        <style type="text/css">
        .error{
            color:red;
            border-color:red;
        }
        </style>
    </head>
    <body>

        @yield('content')

        <script src="{{ web_url() }}/admin_asset/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/AdminLTE/app.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/base.js" type="text/javascript"></script>
        <script>
        	var action = '{{ Request::segment(2) }}';
        	var web_url = '{{ web_url() }}';
        </script>
    </body>
</html>