@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top:0px">
    	<a href="{{ route('adminUsers.addRole') }}">
    		<button type="button" class="btn btn-primary">
    			<i class="fa fa-plus"></i> Add Role
			</button>
    	</a>
    </span>
</section>
<section class="content">
     @if(Session::has('error') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif
     @if(Session::has('success') )
    <div class="alert alert-success">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
                <div class="box-body">
                    <table id="orders-table" class="table table-bordered table-striped">
                		<thead>
                			<tr>
                				<th>ID</th>
                				<th>Name</th>
                				<th>Description</th>
                				<th>Action</th>
                			</tr>
                		</thead>
                		<tbody>
            		    @if ( count($roles) )
                            @foreach ($roles as $role)
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->description }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ route('adminUsers.editRole', [ 'id' => $role->id ]) }}">Edit</a></li>
                                            <li class="divider"></li>
                                            <li><a href="{{ route('adminUsers.deleteRole', [ 'id' => $role->id ]) }}"  onclick="return confirm('Are you sure you want to delete it?')">Delete</a></li>
                                        </ul>
                                    </div>                                    
                                </td>
                            </tr>
                            @endforeach
            			@else
                            <tr><td colspan="4" align="center">There are no roles.</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection