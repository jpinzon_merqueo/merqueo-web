@extends('admin.layout')

@section('content')
<section class="content-header">
	<h1>
		{{ $title }}
		<small>Panel de Control</small>
	</h1>
</section>
<section class="content">
	@if(Session::has('error') )
	<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Alert!</b> {{ Session::get('error') }}
	</div>
	@endif
	@if(Session::has('success') )
	<div class="alert alert-success">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Hecho!</b> {{ Session::get('success') }}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					{{ Form::open(['route' => ['adminUsers.saveRules', $user_admin->id]]) }}
					<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" name="name" class="form-control" readonly="readonly" value="{{ $user_admin->fullname }}">
					</div>
					<div class="form-group">
						<label for="">Reglas</label>
						@if ( !empty($rules) )
							@foreach ($rules as $rule)
							<div class="checkbox">
								<label>
									<input name="rules[]" type="checkbox" value="{{ $rule->id }}" @if ( !empty($rules) && !empty($user_rules) && in_array($rule->id, $user_rules['rules']) ) checked @endif> {{ $rule->description }}
								</label>
							</div>
							@endforeach
						@endif
					</div>
					<hr>
					<div class="form-group">
						<label for="orders_limit">
							Limite de pedidos
						</label>
						<input type="number" name="orders_limit" id="orders_limit" class="form-control" @if (!empty($user_config['orders_limit'])) value="{{ $user_config['orders_limit'] }}" @endif>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection