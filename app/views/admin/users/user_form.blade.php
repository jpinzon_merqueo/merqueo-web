@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Info!</b> {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::has('error') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id='main-form' action="{{ route('adminUsers.save') }}" >
                    <div class="box-body">
                        <input type="hidden" name="id" id="id" value="{{ $user_admin ? $user_admin->id : '' }}">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Nombre Completo</label>
                                <input type="text" class="form-control" name="fullname" placeholder="Ingrese el Nombre Completo" value="{{ $user_admin ? $user_admin->fullname : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Teléfono</label>
                                <input type="text" class="form-control" name="phone" placeholder="Ingrese el Teléfono" value="{{ $user_admin ? $user_admin->phone : '' }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Nombre de Usuario</label>
                                <input type="text" class="form-control" name="username" placeholder="Ingrese el Correo" value="{{ $user_admin ? $user_admin->username : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Contraseña</label>
                                <input type="password" class="form-control" name="password" placeholder="Ingrese la Contraseña" value="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Ciudad</label>
                                <select class="form-control" id="city_id" name="city_id">
                                	@foreach($cities as $city)
    				       			<option value="{{ $city->id }}" @if ($user_admin && $user_admin->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
    				       			@endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Bodega</label>
                                <select class="form-control" id="warehouse_id" name="warehouse_id">
                                    <option value="" selected="selected">-Selecciona-</option>
                                @foreach($warehouses as $warehouse)
                                        <option value="{{ $warehouse->id }}" @if ($user_admin &&
                                        $user_admin->warehouse_id
                                         ==
                                        $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Rol</label>
                                <select class="form-control" id="role_id" name="role_id">
                                    @foreach($roles as $role)
                                    <option value="{{ $role->id }}" @if ($user_admin && $user_admin->role_id == $role->id) selected="selected" @endif>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                <label>Designación</label>
                                <select class="form-control" name="designation" id="designation">
                                    <option value="Super Admin" {{ $user_admin && ($user_admin->designation == 'Super Admin') ? "selected" : ''}}>Super Admin</option>
                                    <option value="Admin" {{ $user_admin && ($user_admin->designation == 'Admin') ? "selected" : ''}}>Admin</option>
                                    <!--<option value="Call Center" {{-- $user_admin && ($user_admin->designation == 'Call Center') ? "selected" : '' --}}>Call Center</option>
                                    <option value="Cliente" {{-- $user_admin && ($user_admin->designation == 'Cliente') ? "selected" : '' --}}>Cliente</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 form-group">
                                    <label for="status">Estado</label>
                                    <select class="form-control required" id="status" name="status">
                                        <option value="">-Selecciona-</option>
                                        <option value="1" {{ $user_admin && ($user_admin->status == 1) ? "selected" : ''}}>Activo</option>
                                        <option value="0" {{ $user_admin && ($user_admin->status == 0) ? "selected" : ''}}>Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group @if (empty($user_admin->designation_store_id)) unseen @endif" id="store">
                                <label>Tienda</label>
                                <select class="form-control" id="store_id" name="store_id">
                                    <option value="">Selecciona</option>
                                    @foreach($stores as $st)
                                        <option value="{{$st->id}}" @if (isset($user_admin->designation_store_id) && $user_admin->designation_store_id == $st->id) selected="selected" @endif>{{$st->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar </button>
                        @if ($user_admin)
                        <a href="{{ route('adminUsers.editPermissions', ['id' => $user_admin->id]) }}" class="btn btn-primary">Editar Permisos </a>
                        @endif
                        @if ($user_admin && $user_admin->role_id == $role->id)
                        <a href="{{ route('adminUsers.editRules', ['id' => $user_admin->id]) }}" class="btn btn-primary">Editar Reglas </a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
    setTimeout(function() { $('.alert.alert-success, .alert.alert-danger').slideToggle('slow'); }, 5000);
    $("#main-form").validate({
        rules: {
            status: "required",
            fullname: "required",
            username: "required",
            phone: "required",
            password: {
                required: function (element) {
                     if ($("#id").val() != ''){
                         return false;
                     }
                     else{
                         return true;
                     }
              }
            },
            city_id: "required",
            role_id: "required",
            store_id: {
                required: function (element) {
                     if ($("#designation").val() == 'Cliente'){
                         return true;
                     }
                     else{
                         return false;
                     }
                  }
             },
            warehouse_id: "required"
        }
    });

     $('#city_id').change(function() {
        warehouses($(this).val());
    });

    $("#designation").on('change',function(){
        var designation = $(this).val();
        var city_id = $("#city_id").val();
        if(designation == 'Cliente'){
            $("#store").removeClass('unseen');
            stores(city_id);
        }else{
            $("#store").addClass('unseen');
            $('#store_id option').remove();
            $('#store_id').append('<option value="" selected="selected"></option>');
        }
    });
});

function warehouses(city_id) {
    var warehouse = $('#warehouse_id');
    warehouse.attr('disabled', 'disabled');
    $.ajax({
        url: '{{ route('adminUsers.getWarehousesAjax') }}',
        type: 'GET',
        data: { city_id: city_id },
    })
    .done(function(response) {
        var data = JSON.parse(response);
        warehouse.empty();
        warehouse.append('<option value="" selected="selected">-Selecciona-</option>');
        if (data.result.warehouses.length > 0) {
            $.each(data.result.warehouses, function(key, value) {
                warehouse.append('<option value="' + value.id + '">' + value.warehouse + '</option>');
            });
        }
        warehouse.removeAttr('disabled');
    })
    .fail(function() {
        warehouse.removeAttr('disabled');
        alert("error al cargar el listado de bodegas.");
    });
}
</script>

@stop