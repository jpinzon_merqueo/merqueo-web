@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
     @if(Session::has('error') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif
     @if(Session::has('success') )
    <div class="alert alert-success">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
                <div class="box-body">
                    {{ Form::open(['route' => 'adminUsers.savePermissions']) }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" readonly="readonly" value="{{ $user_admin->fullname }}">
                        </div>
                        <div class="form-group">
                            <label for="permissions">Permissions</label>
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Module</th>
                                        @foreach ($permissions as $permission)
                                        <th class="text-center">{{ ucfirst($permission->name) }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                               @foreach ($menu as $item)
                                   <tr>
                                       <td @if (empty($item->action)) colspan="{{ count($permissions) + 1 }}" @endif >
                                           @if (!isset($item->children)) {{ $item->title }} @else <b>{{ $item->title }}</b> @endif
                                       </td>
                                       @if (!empty($item->action))
                                         @foreach ($permissions as $permission)
                                         <td>
                                             <div class="checkbox role-checkbox text-center">
                                                 <label>
                                                     @if (isset($permission_descriptions[$item->id][$permission->name.'_description'])) {{ $permission_descriptions[$item->id][$permission->name.'_description'].'<br>' }} @endif
                                                     <input  name="permissions_{{ $item->id }}[]" type="checkbox" value="{{ $permission->id }}" @if (isset($user_permissions[$item->id]) && $user_permissions[$item->id][$permission->name])checked="checked"@endif>
                                                 </label>
                                             </div>
                                         </td>
                                         @endforeach
                                       @endif
                                   </tr>
                                   @if (isset($item->children))
                                   @foreach ($item->children as $item)
                                   <tr>
                                       <td @if (empty($item->action)) colspan="{{ count($permissions) + 1 }}" @endif >
                                          - {{ $item->title }}
                                       </td>
                                       @foreach ($permissions as $permission)
                                       <td>
                                           <div class="checkbox role-checkbox text-center">
                                               <label>
                                                   @if (isset($permission_descriptions[$item->id][$permission->name.'_description'])) {{ $permission_descriptions[$item->id][$permission->name.'_description'].'<br>' }} @endif
                                                   <input  name="permissions_{{ $item->id }}[]" type="checkbox" value="{{ $permission->id }}" @if (isset($user_permissions[$item->id]) && $user_permissions[$item->id][$permission->name])checked="checked"@endif>
                                               </label>
                                           </div>
                                       </td>
                                       @endforeach
                                   </tr>
                                   @endforeach
                                   @endif
                               @endforeach
                               </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="id" value="{{ $user_admin->id }}">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection