@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <script>
        var web_url_ajax = "{{ route('adminUsers.index') }}";
    </script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminUsers.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Usuario Administrador</button>
            </a>
        </span>

    </section>
    <section class="content">
    @if(Session::has('message') )
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('message') }}
    </div>
    @endif
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="admin-user-search-form">
                                    <table width="100%" class="admin-user-table">
                                        <tr>
                                            <td align="right"><label>Estado:</label>&nbsp;</td>
                                            <td>
                                                <select id="status" name="status" class="form-control">
                                                    <option value="1" selected="selected">Activo</option>
                                                    <option value="0">Inactivo</option>
                                                </select>
                                            </td>
                                            <td align="right"><label>Buscar:</label>&nbsp;</td>
                                            <td align="left"><input type="text" placeholder="Nombre Completo, Nombre de Usuario" class="search form-control"></td>
                                            <td align="left"><button type="button" id="btn-search" class="btn btn-primary">Buscar</button></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
        </section>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                paging(1, '');
            });
        </script>
    @endsection
@else
    @section('content')
        <br />
        <table id="admin-user-table" class="table table-bordered table-striped admin-user-table">
        <thead>
            <tr>
                <th>Nombre Completo</th>
                <th>Nombre de Usuario</th>
                <th>Teléfono</th>
                <th>Ciudad</th>
                <th>Designación</th>
                <th>Último login</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($user_admins as $user_admin)
            <tr>
                <td>{{ $user_admin->fullname }}</td>
                <td>{{ $user_admin->username }}</td>
                <td>{{ $user_admin->phone }}</td>
                <td>{{ $user_admin->city }}</td>
                <td>{{ $user_admin->designation }}</td>
                <td>@if (!empty($user_admin->last_logged_in) && $user_admin->last_logged_in != '0000-00-00 00:00:00') {{ date("d M Y g:i A",strtotime($user_admin->last_logged_in)) }} @endif</td>
                <td>
                    @if($user_admin->status == 1)
                        <span class="badge bg-green">Activo</span>
                    @else
                        <span class="badge bg-red">Inactivo</span>
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminUsers.edit', ['id' => $user_admin->id]) }}">Editar</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminUsers.delete', ['id' => $user_admin->id]) }}"  onclick="return confirm('Are you sure you want to delete it?')">Borrar   </a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach

        </tbody>

    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($user_admins) }} ítems</div>
        </div>
    </div>
    @endsection
@endif