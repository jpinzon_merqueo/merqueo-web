<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ingresar</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="form-box" id="login-box">
            <div class="header">Merqueo</div>
            <form action="{{ route('admin.login') }}" method="POST" autocomplete="off">
                <div class="body bg-gray">
                	@if(Session::has('error') )
					<div class="alert alert-danger alert-dismissable">
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					    <b>Alerta!</b> {{ Session::get('error') }}
					</div>
					@endif
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Email"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Clave"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Recordarme
                    </div>
                </div>
                <div class="footer">
                    <button type="submit" class="btn bg-olive btn-block">Iniciar Sesión</button>
                    <!--<p><a href="#">Olvidé mi contraseña</a></p>-->
                </div>
                {{ Form::token() }}
                {{ Form::hidden('_method', 'POST') }}
            </form>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
