@if (!Request::ajax())

	@extends('admin.layout')

	@section('content')

	<script>
		var web_url_ajax = "{{ route('adminBlacklist.index') }}";
	</script>

	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
		<span class="breadcrumb" style="top:0px">
			<a href="{{ route('adminBlacklist.add') }}">
				<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Add Item</button>
			</a>
		</span>
	</section>
	<section class="content">
		@if(Session::has('success') )
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif

		@if(Session::has('error') )
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('error') }}
		</div>
		@endif

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<form id="search-blacklist-form" class="blacklist-table">
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<label for="type">Campo de bloqueo</label>
												<select name="type" id="type" class="form-control">
													<option value=""></option>
													<option value="Dirección">Dirección</option>
													<option value="Celular">Celular</option>
													<option value="Email">E-mail</option>
													<option value="Tarjeta de crédito">Tarjeta de crédito</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row search_container">
										<div class="col-xs-4">
											<div class="form-group">
												<label for="search">Búsqueda</label>
												<input type="text" class="form-control" name="search" id="search">
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group city_container" style="display: none;">
												<label for="city">Ciudad </label>
												<select name="city" id="city" class="form-control">
													<option value="">-- Ciudad --</option>
													@foreach ($cities as $city)
													<option value="{{ $city->id }}">{{ $city->city }}</option>	
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="row card_container" style="display: none;">
										<div class="col-xs-2">
											<div class="form-group">
												<label for="bin">BIN</label>
												<input type="text" class="form-control" name="bin" id="bin">
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
												<label for="four">Últimos cuatro digitos</label>
												<input type="text" class="form-control" name="four" id="four">
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
												<label for="year">Año de expiración</label>
												<input type="text" class="form-control" name="year" id="year">
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
												<label for="month">Mes de expiración</label>
												<input type="text" class="form-control" name="month" id="month">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<div class="form-group">
												<button id="btn-search-blacklist" class="btn btn-primary" type="button">Buscar</button>
												<button class="btn btn-primary" type="reset">Reset</button>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<br>
						<div class="paging"></div>
						<div align="center" class="paging-loading">
							<br>
							<img src="{{ asset_url() }}/img/loading.gif" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<script>
		$(document).ready(function() {
			$('body').on('change', '#type', function(event) {
				event.preventDefault();
				if ( $(this).val() == 'Dirección' ) {
					$('.city_container').show();
				}else{
					$('.city_container').hide();
					$('#city').prop('selectedIndex',0);
				}
				if ( $(this).val() == 'Tarjeta de crédito' ) {
					$('.search_container').hide();
					$('.card_container').show();
				}else{
					$('.search_container').show();
					$('.card_container').hide();
				}
			});
			$('#btn-search-blacklist').click(function(event) {
				paging(1, $('#search').val());
			});

			$('body').on('click', '.delete_item', function(event) {
				var conm = confirm('¿Está seguro que desea eliminar el Item?.');
				if (!conm) {
					event.preventDefault();
				}
			});
		});
		$(window).load(function() {
			paging(1, $('#search').val());
		});
	</script>

@else

@section('content')
	<div class="table-responsive">
		<table class="table table-bordered table-striped blacklist-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tipo</th>
					<th>Valor</th>
					<th>Ciudad</th>
					<th>Número de pedido</th>
					<th>BIN</th>
					<th>Últimos cuatro digitos</th>
					<th>Año de expiración</th>
					<th>Mes de expiración</th>
					<th>Usuario</th>
					<th>Comentario</th>
					<th>Fecha de registro</th>
					<th>Acción</th>
				</tr>
			</thead>
			<tbody>
				@if (count($blacklists))
					@foreach ($blacklists as $blacklist)
						<tr>
							<td>
								{{ $blacklist->id }}
							</td>
							<td>
								{{ $blacklist->type }}
							</td>
							<td>
								@if ( $blacklist->value != null )
									{{ $blacklist->value }}
								@endif
							</td>
							<td>
								@if ( $blacklist->city != null )
									{{ $blacklist->city }}
								@endif
							</td>
							<td>
								@if ( $blacklist->order_id != null )
								<a href="{{ route('adminOrders.details', ['id' => $blacklist->order_id]) }}" target="_blank">
									{{ $blacklist->order_id }}
								</a>
								@endif
							</td>
							<td>
								@if ( $blacklist->cc_bin != null )
									{{ $blacklist->cc_bin }}
								@endif
							</td>
							<td>
								@if ( $blacklist->cc_last_four != null )
									{{ $blacklist->cc_last_four }}
								@endif
							</td>
							<td>
								@if ( $blacklist->cc_expiration_year != null )
									{{ $blacklist->cc_expiration_year }}
								@endif
							</td>
							<td>
								@if ( $blacklist->cc_expiration_month != null )
									{{ $blacklist->cc_expiration_month }}
								@endif
							</td>
							<td>
								@if ( $blacklist->fullname != null )
									{{ $blacklist->fullname }}
								@endif
							</td>
							<td>
								@if ( $blacklist->comment != null )
									{{ $blacklist->comment }}
								@endif
							</td>
							<td>
								{{ date("d M Y", strtotime($blacklist->created_at)) }}
							</td>
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<!-- <li>
											<a href="{{ route('adminBlacklist.edit', ['id' => $blacklist->id]) }}">
												Edit
											</a>
										</li> -->
										<li>
											<a class="delete_item" href="{{ route('adminBlacklist.delete', ['id' => $blacklist->id]) }}">
												Delete
											</a>
										</li>
									</ul>                                        
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr><td colspan="15" align="center">Orders not found</td></tr>
				@endif
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-xs-3">
			<div class="dataTables_info" id="shelves-table_info">Showing {{ count($blacklists) }} entries</div>
		</div>
	</div>

@endif

@stop