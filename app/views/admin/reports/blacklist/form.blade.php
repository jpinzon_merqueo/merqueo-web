@extends('admin.layout')

@section('content')
	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			@if(Session::has('success') )
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Hecho!</b> {{ Session::get('success') }}
			</div>
			@endif
			@if(Session::has('error') )
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Alert!</b> {{ Session::get('error') }}
			</div>
			@endif
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<form id="add-blacklist-form" action="{{ route('adminBlacklist.save') }}" method="POST">
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<label for="type">Campo de bloqueo</label>
												<select name="type" id="type" class="form-control" required="required">
													<option value=""></option>
													<option @if ( !empty($blacklist) && $blacklist->type == 'Dirección' ) selected="selected" @endif value="Dirección">Dirección</option>
													<option @if ( !empty($blacklist) && $blacklist->type == 'Celular' ) selected="selected" @endif value="Celular">Celular</option>
													<option @if ( !empty($blacklist) && $blacklist->type == 'Email' ) selected="selected" @endif value="Email">E-mail</option>
													<option @if ( !empty($blacklist) && $blacklist->type == 'Tarjeta de crédito' ) selected="selected" @endif value="Tarjeta de crédito">Tarjeta de crédito</option>
												</select>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
												<label for="order">Número de pedido</label>
												<input type="text" name="order" id="order" class="form-control" @if ( !empty($blacklist) ) value="{{ $blacklist->order_id }}" @endif>
											</div>
										</div>
									</div>
									<div class="row direccion" @if ( empty($blacklist) || $blacklist->type != 'Dirección' ) style="display: none;" @endif>
										<div class="col-xs-4">
											<div class="form-group">
												<label for="direccion">Dirección</label>
												<input type="text" class="form-control" name="direccion" required="required" @if ( !empty($blacklist) && $blacklist->type == 'Dirección' ) value="{{ $blacklist->value }}" @endif>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
												<label for="city">Ciudad </label>
												<select name="city" id="city" class="form-control" required="required">
													<option value="">-- Ciudad --</option>
													@foreach ($cities as $city)
													<option value="{{ $city->id }}" @if ( !empty($blacklist) && $blacklist->type == 'Dirección' && $blacklist->city_id == $city->id ) selected="selected" @endif>{{ $city->city }}</option>	
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="row celular" @if ( empty($blacklist) || $blacklist->type != 'Celular' ) style="display: none;" @endif>
										<div class="col-xs-4">
											<div class="form-group">
												<label for="celular">Celular</label>
												<input type="text" class="form-control" name="celular" @if ( !empty($blacklist) && $blacklist->type == 'Celular' ) value="{{ $blacklist->value }}" @endif>
											</div>
										</div>
									</div>
									<div class="row email" @if ( empty($blacklist) || $blacklist->type != 'Email' ) style="display: none;" @endif>
										<div class="col-xs-4">
											<div class="form-group">
												<label for="email">E-mail</label>
												<input type="email" class="form-control" name="email" required="required" @if ( !empty($blacklist) && $blacklist->type == 'Email' ) value="{{ $blacklist->value }}" @endif>
											</div>
										</div>
									</div>
									<div class="row tarjeta" @if ( empty($blacklist) || $blacklist->type != 'Tarjeta de crédito' ) style="display: none;" @endif>
										<div class="col-xs-12">
											<div class="row">
												<div class="col-xs-2">
													<div class="form-group">
														<label for="bin">Bin</label>
														<input type="text" class="form-control" name="bin" required="required" @if ( !empty($blacklist) && $blacklist->type == 'Tarjeta de crédito' ) value="{{ $blacklist->cc_bin }}" @endif>
													</div>
												</div>
												<div class="col-xs-2">
													<div class="form-group">
														<label for="last">Últimos cuatro digitos</label>
														<input type="text" class="form-control" name="last" required="required" @if ( !empty($blacklist) && $blacklist->type == 'Tarjeta de crédito' ) value="{{ $blacklist->cc_last_four }}" @endif>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-2">
													<div class="form-group">
														<label for="year">Año de expiración</label>
														<input type="text" class="form-control" name="year" required="required" @if ( !empty($blacklist) && $blacklist->type == 'Tarjeta de crédito' ) value="{{ $blacklist->cc_expiration_year }}" @endif>
													</div>
												</div>
												<div class="col-xs-2">
													<div class="form-group">
														<label for="month">Mes de expiración</label>
														<input type="text" class="form-control" name="month" required="required" @if ( !empty($blacklist) && $blacklist->type == 'Tarjeta de crédito' ) value="{{ $blacklist->cc_expiration_month }}" @endif>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<label for="order">Comentario</label>
												<textarea name="comment" class="form-control"></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<div class="form-group">
												@if ( !empty($blacklist) )
													<input type="hidden" name="id" value="{{ $blacklist->id }}">
												@endif
												<button class="btn btn-primary" type="submit">Guardar</button>
												<button class="btn btn-primary" type="reset">Reset</button>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="form-group">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#add-blacklist-form").validate({
				rules: {
					celular: {
				        minlength: 10,
				        maxlength: 10,
				        required: true,
				        number: true
				    },
				    bin: {
				    	minlength: 6,
				    	maxlength: 6,
				    	required: true,
				    	number: true
				    },
				    last: {
				    	minlength: 4,
				    	maxlength: 4,
				    	required: true,
				    	number: true
				    },
				    year: {
				    	minlength: 4,
				    	maxlength: 4,
				    	required: true,
				    	number: true
				    },
				    month: {
				    	minlength: 2,
				    	maxlength: 2,
				    	required: true,
				    	number: true
				    },
				    order: {
				    	number: true
				    }
				}
			});

			$('body').on('change', '#type', function(event) {
				event.preventDefault();
				$('.direccion, .celular, .email, .tarjeta').hide();
				if ( $(this).val() == 'Dirección' ) {
					$('.direccion').show();
				}
				if( $(this).val() == 'Celular' ) {
					$('.celular').show();
				}
				if( $(this).val() == 'Email' ) {
					$('.email').show();
				}
				if( $(this).val() == 'Tarjeta de crédito' ) {
					$('.tarjeta').show();
				}
			});
		});
	</script>
@endsection