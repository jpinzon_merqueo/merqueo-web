@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Hecho!</b> {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alert!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div>
                <form role="form" method="post" id='main-form' action="{{ route('adminReports.orders') }}" accept-charset="UTF-8">
                    <div class="box-body">
                        <div class="row form-group">
                        	<div class=" col-xs-6">
	                        	<label>Ciudad</label>
	                            <select id="city_id" name="city_id" class="form-control">
	                                <option value=""selected="selected">-Selecciona-</option>
	                                @foreach ($cities as $city)
	                                    <option value="{{ $city->id }}">{{ $city->city }}</option>
	                                @endforeach
	                            </select>
                            </div>
							<div class=" col-xs-6">
								<label>Bodega</label>
								<select id="warehouse_id" name="warehouse_id" class="form-control">
									<option value="" selected="selected">-Selecciona-</option>
									@if (count($warehouses))
										@foreach ($warehouses as $warehouse)
											<option value="{{ $warehouse->id }}">{{ $warehouse->warehouse }}</option>
										@endforeach
									@endif
								</select>
							</div>
                        </div>

                        <div class="row form-group">
                            <div class=" col-xs-6">
	                            <label>Tienda</label>
	                            <select id="store_id" name="store_id" class="form-control">
	                                <option value="" selected="selected">-Selecciona-</option>
	                                @if (count($stores))
	                                @foreach ($stores as $store)
	                                    @if ( $store->city_id == Session::get('admin_city_id') )
	                                        <option value="{{ $store->id }}">{{ $store->name }}</option>
	                                    @endif
	                                @endforeach
	                                @endif
	                            </select>
	                        </div>
						</div>
						<div class="row form-group">
							<div class=" col-xs-6">
									<label>Fecha inicial de pedido</label>
									<input type="text" class="form-control" id="start_date" name="start_date" placeholder="Ingresa fecha inicial (dd/mm/yyyy)">
							</div>
							<div class="form-group col-xs-6">
								<label>Fecha final de pedido</label>
								<input type="text" class="form-control" id="end_date" name="end_date" placeholder="Ingresa fecha final (dd/mm/yyyy)">
							</div>
						</div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Generar Reporte</button>&nbsp;&nbsp;&nbsp;<img src="{{ asset_url() }}/img/loading.gif" class="unseen loading" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    var orders = (function() {
        'use strict';

        function orders() {
            // enforces new
            if (!(this instanceof orders)) {
                return new orders();
            }
            this.url_get_warehouses = '{{ route('admin.get_warehouses_ajax') }}';
            this.url_get_stores = '{{ route('adminReports.getStoresAjax') }}';
            this.bindActions();
        }

        orders.prototype.bindActions = function() {
            var self = this;
            $('body').on('change', '#city_id', function(event) {
                event.preventDefault();
                self.getWarehouses();
            });
            $('body').on('change', '#warehouse_id', function(event) {
                event.preventDefault();
                self.getStores();
            });
            $('#start_date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#end_date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        };
        orders.prototype.getWarehouses = function() {
            var warehouse_id = $('#warehouse_id');
            warehouse_id.empty().append($('<option value="">-Selecciona-</option>'));
            $.ajax({
                url: this.url_get_warehouses,
                type: 'GET',
                dataType: 'JSON',
                data: { city_id: $('#city_id').val() }
            })
            .done(function(data) {
                console.log(data);
                $.each(data, function(index, val) {
                    warehouse_id.append($('<option></option>').attr('value', index).text(val));
                });
            })
            .fail(function() {
                console.log("Ocurrio un error al cargar las bodegas.");
            });
        };
        orders.prototype.getStores = function() {
            var store_id = $('#store_id');
            store_id.empty().append($('<option value="">-Selecciona-</option>'));
            $.ajax({
                url: this.url_get_stores,
                type: 'GET',
                dataType: 'JSON',
                data: { city_id: $('#city_id').val() }
            })
            .done(function(data) {
                $.each(data.result.stores, function(index, val) {
                    store_id.append($('<option></option>').attr('value', val.id).text(val.name));
                });
            })
            .fail(function() {
                console.log("Ocurrio un error al cargar las bodegas.");
            });
        };
        return orders;
    }());


    $(document).ready(function() {
        var orders_report = new orders;
        $.validator.addMethod("dateFormat", function(value, element) {
                return value.match(/^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/);
            }, "Please enter a date using format dd/mm/yyyy"
        );
        $('#main-form').validate({
          rules: {
            start_date: {
              required: true,
              dateFormat: true
            },
            end_date: {
              required: true,
              dateFormat: true
            }
          },
          submitHandler: function(form){
            $('.loading').show();
            $(form).submit();
          }
        });
    });

@if (Session::has('file_url')) window.location = '{{ Session::get('file_url') }}'; @endif

</script>

@stop