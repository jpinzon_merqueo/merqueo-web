@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
	<div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group col-xs-4">
                        <select name="type_returns" id="type_returns" class="form-control">
                        	<option value="all_returns">Todas las devoluciones</option>
                        	<option value="route_returns" selected="selected">Devoluciones por ruta</option>
                        </select>
                    </div>

                	<button type="button" class="btn btn-primary select-type-returns">Cargar</button>&nbsp;&nbsp;&nbsp;<img src="{{ asset_url() }}/img/loading.gif" class="unseen loading" />
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
	$('body').on('click', '.select-type-returns', function(){
		if($('#type_returns').val() == 'route_returns')
			window.location.href = '{{ route('adminOrderReturn.orderReturnsByRoute') }}';
		else
			window.location.href = '{{ route('adminOrderReturn.orderReturns') }}';
	});
});
</script>
@stop