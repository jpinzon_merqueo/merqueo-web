@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td>
                                            <select id="city_id" name="city_id" class="form-control">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Fecha:</label>&nbsp;</td>
                                        <td style="vertical-align: top;">
                                            <input type="text" class="form-control" id="date" name="date" placeholder="Ingresa fecha (dd/mm/yyyy)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label>Bodega:</label>
                                        </td>
                                        <td>
                                            <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                @foreach ($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right" style="vertical-align: top;"><label>Estado:</label>&nbsp;</td>
                                        <td rowspan="5" style="vertical-align: top;">
                                            <select id="status" name="status" multiple="multiple" class="form-control">
                                                <option value="Pendiente">Pendiente</option>
                                                <option value="Validada">Validada</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label>Ruta:</label>
                                        </td>
                                        <td>
                                            <select id="route_id" name="route_id" class="form-control">
                                                <option value="">Selecciona</option>
                                                @foreach ($routes as $route)
                                                    <option value="{{ $route->id }}">{{ $route->route }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                        <td><input type="text" placeholder="Placa" name="s" id="s"  class="form-control"></td>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <button type="button" id="search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                            <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div><br><br>
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){

    $('#search').click(function(){
        $('.paging').html('');
        $('.paging-loading').show();

        $('#date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        var status = '';
        $('#status :selected').each(function(i, selected){
           status += $(selected).val() + ',';
        });

        data = {
            city_id: $('#city_id').val(),
            route_id: $('#route_id').val(),
            date: $('#date').val(),
            warehouse_id: $('#warehouse_id').val(),
            status: status,
            s: $('#s').val(),
        };
        $.ajax({
            url: "{{ route('adminOrderReturn.orderReturnsByRoute') }}",
            data: data,
            type: 'GET',
            dataType: 'html',
            success: function(response) {
                $('.paging-loading').hide();
                $('.paging').html(response);
            }, error: function() {
                $('.paging-loading').hide();
                $('.paging').html('Ocurrió un error al obtener los datos.');
            }
        });
    });

    $('#search').trigger('click');

    $('body').on('change', '#city_id', function() {
        var city_id = $(this).val();
        $('#route_id').html('').append($("<option value=''>Selecciona</option>"));
        $('#warehouse_id').html('').append($("<option value=''>Selecciona</option>"));
        $.ajax({
            url: '{{ route('adminOrderReturn.getRoutesByCityAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: { city_id: city_id },
        })
        .done(function(data) {
            $.each(data.warehouses, function(index, val) {
                 $('#warehouse_id').append($('<option></option>').attr('value', val.id).text(val.warehouse));
            });

            $.each(data.routes, function(index, val) {
                $('#route_id').append($('<option></option>').attr('value', val.id).text(val.route));
            });
        });
    });
});
</script>

@else

@section('content')

<table id="table" class="admin-table table table-bordered table-striped">
    <thead>
        <tr>
            <th>Ruta</th>
            <th>Placa</th>
            <th># Pedidos</th>
            <th># Devoluciones pendientes</th>
            <th># Devoluciones gestionadas</th>
            <th>Unidades a devolver</th>
            <th>Estado</th>
            <th>Fecha de creación</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        @if(count($order_returns))
            @foreach($order_returns as $order_return)
            <tr>
                <td>{{ $order_return->route }}</td>
                <td>{{ $order_return->plate }}</td>
                <td align="center">{{ $order_return->orders }}</td>
                <td align="center">{{ $order_return->quantity_orders_pending }}</td>
                <td align="center">{{ $order_return->quantity_orders_stored }}</td>
                <td align="center">{{ $order_return->quantity_returned }}</td>
                <td>
                    @if($order_return->status_return == 'Pendiente')
                    <span class="badge bg-orange">Pendiente</span>
                    @elseif($order_return->status_return == 'Validada')
                    <span class="badge bg-green">Validada</span>
                    @endif
                </td>
                <td>{{ date('d M Y g:i a', strtotime($order_return->created_at)) }}</td>
                <td>
                    <div class="btn-group">
                       <a class="btn btn-xs btn-default" href="{{ route('adminOrderReturn.editRouteReturn', ['id' => $order_return->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="10" align="center">No hay datos.</td></tr>
        @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="table_info">Mostrando {{ count($order_returns) }} ítems</div>
    </div>
</div>

@endif

@stop