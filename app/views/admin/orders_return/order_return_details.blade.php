@if (!Request::ajax())
    @extends('admin.layout')
    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }}  # {{ $order_return->id }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
        @if ($order_return && $admin_permissions['update'])
            @if($order_return->status == 'Pendiente' || $order_return->status == 'Parcialmente devuelto')
                <a href="{{ route('adminOrderReturn.updateStatusOrderReturn', ['id' => $order_return->id]) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el stock de los productos devueltos?')">
                    <button type="button" class="btn btn-success">Actualizar estado a Devuelto</button>
                </a>
                <a href="{{ route('adminOrderReturn.deleteOrderReturn', ['id' => $order_return->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar la devolución a bodega?')">
                    <button type="button" class="btn btn-danger">Eliminar</button>
                </a>
            @endif
            @if($order_return->status == 'Devuelto')
                <a href="{{ route('adminOrderReturn.updateStatusOrderReturn', ['id' => $order_return->id, 'status' => 'Almacenado']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Almacenado?')">
                    <button type="button" class="btn btn-success">Actualizar estado a Almacenado</button>
                </a>
            @endif
            @if($order_return->status != 'Pendiente')
                <a href="{{ route('adminOrderReturn.orderReturnPrintPdf', ['id' => $order_return->id]) }}" target="_blank">
                    <button type="button" class="btn btn-primary">Imprimir</button>
                </a>
            @endif
       @endif
       </span>
    </section>

    <script type="text/javascript">
    function search_products(){
        $.ajax({
            url: '{{ route('adminOrderReturn.getProductsAjax', ['id' => $order_return->id]) }}',
            data: { s: $('#search-products').val(), status: $('#status').val()},
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            $('#products_table_container').empty();
            $('#products_table_container').html(data.html);
            if ( data.success ) {
                $('#products_error .message').empty().html(data.message);
                $('#products_error').slideDown('fast').delay(10000).slideUp('fast');
            }
        })
        .fail(function() {
            console.error("error al cargar los productos");
        })
        .always(function() {
            $('#search-products').val('');
        });
    }

    $(document).ready(function() {
        search_products();
        $('#search-products').keyup(function(event){
            code = event.which;
            if (code == 32 || code == 13 || code == 188 || code == 186){
                search_products();
            }
        });

        $('body').on('click', '.update-product', function() {
            var status = $(this).data('status');
            var order_return_product_id = $(this).data('order-return-product-id');
            $('.btn-group button').addClass('disabled');
             $.ajax({
                url: '{{ route('adminOrderReturn.updateProductReturnAjax', ['id' => $order_return->id ]) }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    order_return_product_id: order_return_product_id,
                    status: status
                },
            })
            .done(function(data) {
                if(data.status){
                    if(data.product_status  == 'Devuelto'){
                        $('.badge').parents().find('#status-'+order_return_product_id+' .badge').removeClass('bg-orange');
                        $('.badge').parents().find('#status-'+order_return_product_id+' .badge').addClass('bg-green');
                    }else{
                        if(data.product_status  == 'Perdida' || data.product_status  == 'Dañado' || data.product_status  == 'No devuelto'){
                            $('.badge').parents().find('#status-'+order_return_product_id+' .badge').removeClass('bg-green');
                            $('.badge').parents().find('#status-'+order_return_product_id+' .badge').addClass('bg-red');
                        }else{
                            $('.badge').parents().find('#status-'+order_return_product_id+' .badge').removeClass('bg-green');
                            $('.badge').parents().find('#status-'+order_return_product_id+' .badge').addClass('bg-orange');
                        }
                    }
                    console.log(order_return_product_id);
                    $('.badge').parents().find('#status-'+order_return_product_id+' .badge').html(data.product_status);
                }
               $('.btn-group button').removeClass('disabled');
            })
            .fail(function(data) {
                $('.btn-group button').removeClass('disabled');
            })
            .always(function(data) {
                $('.btn-group button').removeClass('disabled');
            });
        });

    });
    </script>

    <section class="content">

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    <form role="form" method="post" id='main-form' action="{{ route('adminOrderReturn.updateOrderReturn', ['id' => $order_return->id]) }}">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="callout callout-info">
                                    <h4>Datos de Devolución</h4>
                                    <p><label>Estado</label> -
                                    @if($order_return->status == 'Pendiente' || $order_return->status == 'Parcialmente devuelto')
                                        <span class="badge bg-orange">{{ $order_return->status }}</span>
                                    @else <span class="badge bg-green">{{ $order_return->status }}</span> @endif
                                    </p>
                                    <p><label>Ciudad</label> - {{ $order_return->city }}</p>
                                    <p><label>Motivo de devolución</label> -
                                        <span>{{ $order_return->reject_reason }}</span>
                                    </p>
                                    <p><label>Pedido #</label> - <a href="{{ route('adminOrderStorage.details', ['id' => $order_return->order_id]) }}" target="_blank">{{ $order_return->order_id }}</a></p>
                                    <p><label>Unidades a devolver</label> - {{ $order_return->quantity_to_return }}</p>
                                    <p><label>Unidades devueltas</label> - {{ $order_return->quantity_returned }}</p>
                                    <p><label>Fecha de creación</label> - {{ date('d M Y g:i a', strtotime($order_return->date)) }}</p>
                                    @if (!empty($order_return->return_date))
                                    <p><label>Fecha de devolución</label> - {{ date('d M Y g:i a', strtotime($order_return->return_date)) }}</p>
                                    @endif
                                    @if (!empty($order_return->storage_date))
                                    <p><label>Fecha de almacenamiento</label> - {{ date('d M Y g:i a', strtotime($order_return->storage_date)) }}</p>
                                    @endif
                                    <p><label>Creado por</label> - @if (!empty($order_return->admin_name)) {{ $order_return->admin_name }} @else {{ $order_return->driver_name }} @endif</p>
                                    @if (!empty($order_return->observation))
                                    <p><label>Observaciones</label> - {{ $order_return->observation }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="callout callout-info">
                                    <h4>Datos de Transportador</h4>
                                    <p><label>Transportador</label> - {{ $order_return->transporter_name }}</p>
                                    <p><label>Placa vehículo</label> - {{ $order_return->vehicle_plate }}</p>
                                    <p><label>Nombre conductor</label> - {{ $order_return->driver_name }}</p>
                                 </div>
                            </div>
                        </div>

                        @if ($order_return->status == 'Pendiente' || $order_return->status == 'Parcialmente devuelto')
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Observaciones</label>
                                <textarea class="form-control required" name="observation" placeholder="Ingresa observación"></textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar </button>
                        </div><br>
                        @endif
                    </form>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <legend>Productos devueltos</legend>
                                    <div class="col-xs-4">
                                        <label>Estado</label>
                                        <select name="status" class="form-control" id="status">
                                            <option value="Dañado">Dañado</option>
                                            <option value="Devuelto" selected="selected">Devuelto</option>
                                            <option value="No devuelto">No devuelto</option>
                                            <option value="Pendiente">Pendiente</option>
                                            <option value="Perdida">Perdida</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Gestionar producto</label>
                                        <input type="text" placeholder="Referencia" id="search-products" class="form-control"><br>
                                    </div>
                                    <div class="col-xs-2">
                                        <button style="margin-top: 24px;" onclick="search_products();" class="btn btn-primary">Ver productos</button>
                                    </div>
                                    <div id="products_error" class="alert alert-danger alert-dismissable" style="display: none;">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Alerta!</b> <div class="message"></div>
                                    </div>
                                    <div id="products_success" class="alert alert-success alert-dismissable" style="display: none;">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Alerta!</b> <div class="message"></div>
                                    </div>
                                    <div id="products_table_container">
                                        <div class="col-xs-12 text-center"><img src="{{ asset_url() }}/img/loading.gif" align="center"></div>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
@section('content')
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Referencia</th>
            <th>PLU</th>
            <th>Producto</th>
            <th>Unidad de medida</th>
            <th>Stock de devolución actualizado</th>
            <th>Stock de bodega actualizado</th>
            <th>Estado</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        @if (count($returned_products))
          @foreach($returned_products as $product)
            <tr>
                <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                <td>{{ $product->reference }}</td>
                <td>{{ $product->provider_plu }}</td>
                <td>{{ $product->product_name }}</td>
                <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                <td align="center">@if (!$product->return_stock_updated) No @else Sí @endif</td>
                <td align="center">@if (!$product->stock_updated) No @else Sí @endif</td>
                <td align="center" id="status-{{ $product->id }}">
                    @if($product->status == 'Devuelto')
                        <span class="badge bg-green">Devuelto</span>
                    @elseif($product->status == 'Perdida' || $product->status == 'Dañado' || $product->status == 'No devuelto')
                        <span class="badge bg-red">{{ $product->status }}</span>
                    @elseif($product->status == 'Pendiente')
                        <span class="badge bg-orange">{{ $product->status }}</span>
                    @endif
                </td>
                <td>
                    @if (!$product->return_stock_updated && ($product->order_return_status == 'Pendiente' || $product->order_return_status == 'Parcialmente devuelto'))
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="update-product" href="javascript:;" data-order-return-product-id="{{ $product->id }}" data-status="{{ 'Pendiente' }}">Pendiente</a></li>
                            <li><a class="update-product" href="javascript:;" data-order-return-product-id="{{ $product->id }}" data-status="{{ 'Devuelto' }}">Devuelto</a></li>
                            <li><a class="update-product" href="javascript:;" data-order-return-product-id="{{ $product->id }}" data-status="{{ 'No devuelto' }}">No Devuelto</a></li>
                            <li><a class="update-product" href="javascript:;" data-order-return-product-id="{{ $product->id }}" data-status="{{ 'Perdida' }}">Perdida</a></li>
                            <li><a class="update-product" href="javascript:;" data-order-return-product-id="{{ $product->id }}" data-status="{{ 'Dañado' }}">Dañado</a></li>
                        </ul>
                    </div>
                    @endif
                </td>
            </tr>
            @endforeach
        @else
        <tr>
            <td colspan="10" align="center">
                No hay productos devueltos.
            </td>
        </tr>
        @endif
    </tbody>
</table>
@endif
@stop