@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
        @if ($route_returns && $admin_permissions['update'])
            @if($route_returns->status_return == 'Pendiente')
                <a href="{{ route('adminOrderReturn.updateStatusRouteReturn', ['id' => $route_returns->id]) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado de la ruta?')">
                    <button type="button" class="btn btn-success">Actualizar ruta a estado Validada</button>
                </a>
            @endif
       @endif
       </span>
    </section>
    <section class="content">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif

    <div class="row">
            <div class="col-xs-12">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="callout callout-info">
                                    <h4>Datos de Ruta</h4>
                                    <p>Ruta - {{ $route_returns->route }}</p>
                                    <p><label>Estado de la ruta</label> -
                                    @if($route_returns->status_return == 'Pendiente')
                                        <span class="badge bg-orange">{{ $route_returns->status_return }}</span>
                                    @else <span class="badge bg-green">{{ $route_returns->status_return }}</span> @endif
                                    </p>
                                    <p><label>Ciudad</label> - {{ $route_returns->city }}</p>
                                    <p><label># Pedidos </label> - {{ $route_returns->quantity_orders}}</p>
                                    <p><label># Devoluciones pendientes</label> - {{ $route_returns->quantity_orders_pending}}</p>
                                    <p><label># Devoluciones gestionadas</label> - {{ $route_returns->quantity_orders_stored}}</p>
                                    @if (!empty($route_returns->admin_name))
                                        <p><label>Validada por</label> -  {{ $route_returns->admin_name }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="callout callout-info">
                                    <h4>Datos de Transportador</h4>
                                    <p><label>Transportador</label> - {{ $route_returns->transporter_name }}</p>
                                    <p><label>Placa vehículo</label> - {{ $route_returns->vehicle_plate }}</p>
                                    <p><label>Nombre conductor</label> - {{ $route_returns->driver_name }}</p>
                                 </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-body table-responsive">
                                        <div class="row">
                                            <form id="search-form">
                                            <table width="100%">
                                                <tr>
                                                    <td align="right" style="vertical-align: top;"><label>Estado:</label>&nbsp;</td>
                                                    <td style="vertical-align: top;">
                                                        <select id="status" name="status" multiple="multiple" class="form-control">
                                                            <option value="Pendiente">Pendiente</option>
                                                            <option value="Parcialmente devuelto">Parcialmente devuelto</option>
                                                            <option value="Devuelto">Devuelto</option>
                                                            <option value="Almacenado">Almacenado</option>
                                                        </select>
                                                    </td>
                                                    <td align="right" style="vertical-align: top;"><label>Buscar:</label>&nbsp;</td>
                                                    <td style="vertical-align: top;"><input type="text" placeholder="ID pedido" name="s" id="s"  class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td></tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <button type="button" id="search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                                        <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                                                    </td>
                                                </tr>
                                            </table>
                                            </form>
                                        </div>
                                        <br><br>
                                        <div class="paging"></div>
                                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
</section>

<script>
$(document).ready(function(){

    $('#search').click(function(){
        $('.paging').html('');
        $('.paging-loading').show();

        $('#date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        var status = '';
        $('#status :selected').each(function(i, selected){
           status += $(selected).val() + ',';
        });

        data = {
            status: status,
            s: $('#s').val()
        };
        $.ajax({
            url: "{{ route('adminOrderReturn.editRouteReturn', ['id' => $route_returns->id]) }}",
            data: data,
            type: 'GET',
            dataType: 'html',
            success: function(response) {
                $('.paging-loading').hide();
                $('.paging').html(response);
            }, error: function() {
                $('.paging-loading').hide();
                $('.paging').html('Ocurrió un error al obtener los datos.');
            }
        });
    });

    $('#search').trigger('click');
});
</script>

@else

@section('content')

<table id="table" class="admin-table table table-bordered table-striped">
    <thead>
        <tr>
            <th>Devolución a bodega #</th>
            <th>Estado</th>
            <th>Ciudad</th>
            <th>Pedido #</th>
            <th>Estado pedido</th>
            <th>Fecha de creación</th>
            <th>Fecha de recibo</th>
            <th>Unidades a devolver</th>
            <th>Transportador</th>
            <th>Conductor</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        @if(count($order_returns))
            @foreach($order_returns as $order_return)
            <tr>
                <td>{{ $order_return->id }}</td>
                <td align="center">
                    @if($order_return->status == 'Pendiente' || $order_return->status == 'Parcialmente devuelto')
                        <span class="badge bg-orange">{{ $order_return->status }}</span>
                    @else
                        <span class="badge bg-green">{{ $order_return->status }}</span>
                    @endif
                </td>
                <td>{{ $order_return->city }}</td>
                <td><a href="{{ route('adminOrderStorage.details', ['id' => $order_return->order_id]) }}" target="_blank">{{ $order_return->order_id }}</a></td>
                <td>
                @if($order_return->order_status == 'Validation')
                <span class="badge bg-maroon">Validation</span>
                @elseif($order_return->order_status == 'Delivered')
                <span class="badge bg-green">Delivered</span>
                @elseif($order_return->order_status == 'Cancelled')
                <span class="badge bg-red">Cancelled</span>
                @elseif($order_return->order_status == 'Initiated')
                <span class="badge bg-yellow">Initiated</span>
                @elseif($order_return->order_status == 'Dispatched')
                <span class="badge bg-green">Dispatched</span>
                @elseif($order_return->order_status == 'In Progress')
                <span class="badge bg-green">In Progress</span>
                @elseif($order_return->order_status == 'Alistado')
                <span class="badge bg-green">Alistado</span>
                @elseif($order_return->order_status == 'Enrutado')
                <span class="badge bg-green">Enrutado</span>
                @endif
                </td>
                <td>{{ date('d M Y g:i a', strtotime($order_return->date)) }}</td>
                <td>@if (!empty($order_return->return_date)) {{ date('d M Y g:i a', strtotime($order_return->return_date)) }} @endif </td>
                <td>{{ $order_return->quantity_returned }}</td>
                <td>{{ $order_return->transporter_name }}</td>
                <td>{{ $order_return->driver_name }}</td>
                <td>
                    <div class="btn-group">
                       <a class="btn btn-xs btn-default" href="{{ route('adminOrderReturn.editOrderReturn', ['id' => $order_return->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="11" align="center">No hay datos.</td></tr>
        @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="table_info">Mostrando {{ count($order_returns) }} ítems</div>
    </div>
</div>

@endif

@stop