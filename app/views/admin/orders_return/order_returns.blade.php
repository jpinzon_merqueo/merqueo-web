@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td>
                                            <select id="city_id" name="city_id" class="form-control">
                                                <option value="">Selecciona</option>
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Transportador:</label>&nbsp;</td>
                                        <td>
                                            <select id="transporter_id" name="transporter_id" class="form-control">
                                                <option value="">Selecciona</option>
                                                @if (count($transporters))
                                                    @foreach ($transporters as $transporter)
                                                        <option value="{{ $transporter->id }}">{{ $transporter->fullname }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label>Bodega:</label>
                                        </td>
                                        <td>
                                            <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Conductor:</label>&nbsp;</td>
                                        <td>
                                            <select id="driver_id" name="driver_id" class="form-control">
                                                <option value="">Selecciona</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                        <td><input type="text" placeholder="ID pedido, ID devolución, nombre, placa" name="s" id="s"  class="form-control"></td>
                                        <td align="right"><label>Vehículo:</label>&nbsp;</td>
                                        <td style="vertical-align: top;">
                                            <select id="vehicle_id" name="vehicle_id" class="form-control">
                                                <option value="">Selecciona</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td align="right" rowspan="3" style="vertical-align: top;"><label>Estado:</label>&nbsp;</td>
                                      <td style="vertical-align: top;">
                                            <select id="status" name="status" multiple="multiple" class="form-control">
                                                <option value="Pendiente">Pendiente</option>
                                                <option value="Parcialmente devuelto">Parcialmente devuelto</option>
                                                <option value="Devuelto">Devuelto</option>
                                                <option value="Almacenado">Almacenado</option>
                                            </select>
                                      </td>
                                      <td align="right" style="vertical-align: top;"><label>Fecha:</label>&nbsp;</td>
                                      <td style="vertical-align: top;">
                                        <input type="text" class="form-control" id="date" name="date" placeholder="Ingresa fecha (dd/mm/yyyy)">
                                      </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center">
                                            <button type="button" id="search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                            <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div><br><br>
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){

    $('#search').click(function(){
        $('.paging').html('');
        $('.paging-loading').show();

        $('#date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        var status = '';
        $('#status :selected').each(function(i, selected){
           status += $(selected).val() + ',';
        });

        data = {
            city_id: $('#city_id').val(),
            warehouse_id: $('#warehouse_id').val(),
            transporter_id: $('#transporter_id').val(),
            driver_id: $('#driver_id').val(),
            vehicle_id: $('#vehicle_id').val(),
            date: $('#date').val(),
            status: status,
            s: $('#s').val(),
        };
        $.ajax({
            url: "{{ route('adminOrderReturn.orderReturns') }}",
            data: data,
            type: 'GET',
            dataType: 'html',
            success: function(response) {
                $('.paging-loading').hide();
                $('.paging').html(response);
            }, error: function() {
                $('.paging-loading').hide();
                $('.paging').html('Ocurrió un error al obtener los datos.');
            }
        });
    });

    $('#search').trigger('click');

    $('body').on('change', '#city_id', function() {
        var city_id = $(this).val();
        $('#warehouse_id').html('').append($("<option value=''>Selecciona</option>"));
        $('#transporter_id').html('').append($("<option value=''>Selecciona</option>"));
        $('#driver_id').html('').append($("<option value=''>Selecciona</option>"));
        $('#vehicle_id').html('').append($("<option value=''>Selecciona</option>"));
        $.ajax({
            url: '{{ route('adminOrderReturn.getWarehousesByCityAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: { city_id: city_id },
        })
        .done(function(data) {
            $.each(data.warehouses, function(index, val) {
                 $('#warehouse_id').append($('<option></option>').attr('value', val.id).text(val.warehouse));
            });

            $.each(data.transporters, function(index, val) {
                 $('#transporter_id').append($('<option></option>').attr('value', val.id).text(val.fullname));
            });
        });
    });

    $('body').on('change', '#transporter_id', function() {
        var transporter_id = $(this).val();
        $('#driver_id').html('').append($("<option value=''>Selecciona</option>"));
        $('#vehicle_id').html('').append($("<option value=''>Selecciona</option>"));
        $.ajax({
            url: '{{ route('adminOrderReturn.getDriversVehiclesByTransporterAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: { transporter_id: transporter_id },
        })
        .done(function(data) {
            $.each(data.drivers, function(index, val) {
                 $('#driver_id').append($('<option></option>').attr('value', val.id).text(val.name));
            });
            $.each(data.vehicles, function(index, val) {
                 $('#vehicle_id').append($('<option></option>').attr('value', val.id).text(val.plate));
            });
        });
    });

});
</script>

@else

@section('content')

<table id="table" class="admin-table table table-bordered table-striped">
    <thead>
        <tr>
            <th>Devolución a bodega #</th>
            <th>Estado</th>
            <th>Ciudad</th>
            <th>Pedido #</th>
            <th>Estado pedido</th>
            <th>Fecha de creación</th>
            <th>Fecha de recibo</th>
            <th>Unidades a devolver</th>
            <th>Transportador</th>
            <th>Conductor</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        @if(count($order_returns))
            @foreach($order_returns as $order_return)
            <tr>
                <td>{{ $order_return->id }}</td>
                <td align="center">
                    @if($order_return->status == 'Pendiente' || $order_return->status == 'Parcialmente devuelto')
                        <span class="badge bg-orange">{{ $order_return->status }}</span>
                    @else
                        <span class="badge bg-green">{{ $order_return->status }}</span>
                    @endif
                </td>
                <td>{{ $order_return->city }}</td>
                <td><a href="{{ route('adminOrderStorage.details', ['id' => $order_return->order_id]) }}" target="_blank">{{ $order_return->order_id }}</a></td>
                <td>
                @if($order_return->order_status == 'Validation')
                <span class="badge bg-maroon">Validation</span>
                @elseif($order_return->order_status == 'Delivered')
                <span class="badge bg-green">Delivered</span>
                @elseif($order_return->order_status == 'Cancelled')
                <span class="badge bg-red">Cancelled</span>
                @elseif($order_return->order_status == 'Initiated')
                <span class="badge bg-yellow">Initiated</span>
                @elseif($order_return->order_status == 'Dispatched')
                <span class="badge bg-green">Dispatched</span>
                @elseif($order_return->order_status == 'In Progress')
                <span class="badge bg-green">In Progress</span>
                @elseif($order_return->order_status == 'Alistado')
                <span class="badge bg-green">Alistado</span>
                @elseif($order_return->order_status == 'Enrutado')
                <span class="badge bg-green">Enrutado</span>
                @endif
                </td>
                <td>{{ date('d M Y g:i a', strtotime($order_return->date)) }}</td>
                <td>@if (!empty($order_return->return_date)) {{ date('d M Y g:i a', strtotime($order_return->return_date)) }} @endif </td>
                <td>{{ $order_return->quantity_returned }}</td>
                <td>{{ $order_return->transporter_name }}</td>
                <td>{{ $order_return->driver_name }}</td>
                <td>
                    <div class="btn-group">
                       <a class="btn btn-xs btn-default" href="{{ route('adminOrderReturn.editOrderReturn', ['id' => $order_return->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="11" align="center">No hay datos.</td></tr>
        @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="table_info">Mostrando {{ count($order_returns) }} ítems</div>
    </div>
</div>

@endif

@stop