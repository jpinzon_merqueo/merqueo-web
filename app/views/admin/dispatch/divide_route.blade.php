@extends('admin.layout')

@section('content')

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif
        @if(Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::get('list_error'))
            <div class="alert alert-danger">
                <h4>Errores</h4>
                {{ Session::get('list_error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form action="{{ route('adminDispatchPlanning.validateRoutes') }}" role="form" method="POST"
                              id="validate-form" accept-charset="UTF-8">
                            <div class="row">
                                <div class="form-group col-xs-6 col-md-3">
                                    <label>Ciudad</label>
                                    <select id="city_id" name="city_id" class="form-control requerid">
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}"
                                                    @if( Session::get('admin_city_id') == $city->id || Session::get('city_id') == $city->id) selected="selected" @endif >{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-6 col-md-3">
                                    <label>Bodega</label>
                                    <select id="warehouse_id" name="warehouse_id" class="form-control required">
                                        @foreach ($warehouses as $warehouse)
                                            <option value="{{ $warehouse->id }}"
                                                    @if(Session::get('warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-md-6">
                                    <label>Ruta</label>
                                    @if(!empty($routes))
                                        <select class="form-control required" name="route_id" id="validate_route">
                                            <option value="">-Selecciona-</option>
                                            @foreach ($routes as $route)
                                                <option value="{{ $route->id }}"
                                                        @if(Session::get('route_id') == $route->id) selected="selected" @endif>{{ $route->route }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                &nbsp;&nbsp;&nbsp;<img src="{{ asset_url() }}/img/loading.gif"
                                                       class="unseen loading-city"/>
                            </div>
                            <div class="row ">
                                <div class="form-group col-xs-12">

                                    <button type="button" id="btn-route-divide" class="btn btn-warning">Dividir ruta
                                    </button>
                                    <button type="button" id="btn-reasign-divide" class="btn btn-primary">Reasignar
                                        ruta
                                    </button>
                                </div>
                            </div>
                        </form>
                        @if(Session::get('assign_exception'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('assign_exception') }}
                            </div>
                        @endif
                        @if(Session::get('validate_success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b>Hecho!</b> Hay {{ Session::get('validate_success') }} pedidos válidos.
                            </div>
                        @endif
                    </div>
                    <br>
                </div>

                <div class="box box-primary divide-route" style="display:none">
                    <div class="modal-header">
                        <h4 class="modal-title">Formulario para la división de ruta</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="POST" id='form-route-divide' autocomplete="off">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group" id="route-divide-type">
                                        {{ Form::label('divide_route_type', 'Tipo', ['class' => 'control-label']) }}
                                        {{ Form::fillSelectWithoutKeys('divide_route_type', ['Apoyo' => 'Apoyo', 'Canguro' => 'Canguro'], '', array('class' => 'form-control required', 'id' => 'divide_route_type'), true) }}
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('motive', 'Motivo', ['class' => 'control-label']) }}

                                        <select class="form-control required" name="route_division_reason"
                                                id="route_division_reason">
                                            <option value="">Seleccione</option>
                                            @if (!empty($tipification_list))
                                                @foreach ($tipification_list as $data)
                                                    <option value="{{ $data }}">{{ $data }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group" id="route-divide-vehicle">
                                        {{ Form::label('divide_route_vehicle_id', 'Vehículo', ['class' => 'control-label']) }}
                                        {{ Form::fillSelectWithKeys('divide_route_vehicle_id', !empty($vehicles) ? $vehicles : [], '', array('class' => 'form-control required', 'id' => 'divide_route_vehicle_id'), 'id', 'plate', true) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group" id="route-divide-driver">
                                        {{ Form::label('divide_route_driver_id', 'Conductor', ['class' => 'control-label']) }}
                                        <select class="form-control required" name="divide_route_driver_id"
                                                id="divide_route_driver_id">
                                            <option value="">Seleccione</option>
                                            @if (!empty($drivers))
                                                @foreach ($drivers as $driver)
                                                    <option value="{{ $driver->id }}">{{ $driver->first_name.' '.$driver->last_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table id="table-orders" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Secuencia</th>
                                                <th># pedido</th>
                                                <th>Dirección</th>
                                                <th># productos</th>
                                                <th>Pago</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {{ Form::token() }}
                            {{ Form::hidden('route_id', '', ['id' => 'divide_route_id']) }}
                            {{ Form::hidden('_method', 'POST') }}
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="form-route-submit" type="button" class="btn btn-primary">Guardar</button>
                    </div>
                </div>


                <div class="box box-primary reasign-route" style="display:none">

                    <form action="{{ route('adminPlanning.assignTransporters') }}" role="form" method="post"
                          id="assign-form" accept-charset="UTF-8">
                        <div class="modal-header">
                            <h4 class="modal-title">Formulario para la reasignación completa de la ruta</h4>
                        </div>
                        <div class="modal-body row">

                            <div class="row">
                                @if(Session::get('assign_success'))
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                        </button>
                                        <b>Hecho!</b> Se han actualizado {{ Session::get('assign_success') }} pedidos.
                                    </div>
                                @endif
                            </div>
                            <input id="reasign_route_id" name="reasign_route_id" hidden>
                            <input id="reasign_city_id" name="reasign_city_id" hidden>
                            <input id="reasign_warehouse_id" name="reasign_warehouse_id" hidden>
                            <div class="form-group col-md-4">
                                {{ Form::label('transporter_id', 'Transportrador', ['class' => 'control-label']) }}
                                {{ Form::fillSelectWithKeys('reasign_transporter_id', !empty($transporters) ? $transporters : [], '', array('class' => 'form-control required', 'id' => 'reasign_transporter_id'), 'id', 'fullname', true) }}
                            </div>
                            <div class="form-group col-md-4">
                                <label>Vehículo</label>
                                <select class="form-control required" name="reasign_vehicle_id" id="reasign_vehicle_id">
                                    <option value="">-Selecciona-</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Conductor</label>
                                <select class="form-control required" name="reasign_driver_id" id="reasign_driver_id">
                                    <option value="">-Selecciona-</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btn-reassign" type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#validate-form').validate();
            $('#form-route-divide').validate();

            $('body').on('click', '#validate-form button[type="submit"]', function (event) {
                if ($('#validate-form').valid()) {
                    $('.city_id').val($('#city_id').val());
                    $('.warehouse_id').val($('#warehouse_id').val());
                    $(this).addClass('disabled');
                    $('#validate-form .loading-vehicle').removeClass('unseen');
                }
            });

            $('body').on('click', '#btn-reasign-divide', function (event) {
                event.preventDefault();
                if ($('#validate-form').valid()) {
                    $(".divide-route").hide('low')
                    $(".reasign-route").show('low')
                }
            });

            $('body').on('click', '#btn-route-divide', function (event) {
                event.preventDefault();
                $(".reasign-route").hide('low')
                if ($('#validate-form').valid()) {
                    $('.city_id').val($('#city_id').val());
                    $('.warehouse_id').val($('#warehouse_id').val());
                    $('#validate-form .loading-vehicle').removeClass('unseen');
                    var route_id = $('#validate_route').val(),
                        $form = $('#form-route-divide'),
                        $this = $(this),
                        url = "{{ route('adminDispatchPlanning.transport') }}/" + route_id + '/divide-route';
                    $this.addClass('disabled');
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                    })
                        .done(function (data) {
                            $('#validate-form .loading-vehicle').addClass('unseen');
                            $this.removeClass('disabled');
                            if (data.status) {
                                $(".divide-route").show('low')
                                $form[0].reset();
                                $form.find('#divide_route_id').val(route_id);
                                $form.find('tbody').html('');
                                $.each(data.orders, function (index, order) {
                                    var item = '<tr class="' + order.status + '" id="order-' + order.id + '">';
                                    item = item + '<td><input name="order[' + order.id + ']" value="' + order.id + '" type="checkbox"></td>';
                                    item = item + '<td>' + order.planning_sequence + '</td>';
                                    item = item + '<td>' + order.id + '</td>';
                                    item = item + '<td>' + order.order_group.user_address + '</td>';
                                    item = item + '<td>' + order.order_group.products_quantity + '</td>';
                                    item = item + '<td>' + order.payment_method + '</td>';
                                    $form.find('tbody').append(item);
                                });
                            } else {
                                swal(
                                    'Oops...',
                                    data.message,
                                    'error'
                                );
                            }
                        })
                        .fail(function () {
                            $('#validate-form .loading-vehicle').addClass('unseen');
                            $this.removeClass('disabled');
                            swal(
                                'Oops...',
                                '¡Al parecer tenemos problemas tratando de obtener la información!',
                                'error'
                            );
                        });
                }
            });

            $('#divide_route_type').on('change', function () {
                var $this = $(this),
                    $form = $('#form-route-divide');
                $trs = $form.find('tbody tr');
                switch ($this.val()) {
                    case 'Apoyo':
                        $trs.show();
                        break;
                    case 'Canguro':
                        $trs.each(function (index, element) {
                            if ($(element).hasClass('Dispatched')) {
                                $(element).show();
                            } else {
                                $(element).hide()
                                    .find('input[type=checkbox]')
                                    .prop('checked', false);
                            }
                        });
                        break;
                    default:
                        $trs.show();
                        break;
                }
            });

            $('#reasign_transporter_id').on('change', function () {
                $('#reasign_vehicle_id').empty();

                var html = '<option value="">-Selecciona-</option>';
                if ($("#reasign_transporter_id").val() != '') {
                    $.ajax({
                        url: '{{ route('adminPlanning.getVehiclesAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            transporter_id: $("#reasign_transporter_id").val()
                        }
                    })
                        .done(function (data) {
                            if (data.length > 0) {
                                $.each(data, function (index, val) {
                                    html += '<option value="' + val.id + '" >' + val.plate + '</option>';
                                });
                            }
                            $('#reasign_vehicle_id').html(html);
                            $('#reasign_vehicle_id').change();
                        })
                        .fail(function (data) {
                            console.error("error al obtener los vehiculos.");
                        })
                } else {
                    $('#reasign_vehicle_id').html(html);
                    $('#reasign_vehicle_id').change();
                }
            });

            $('#reasign_vehicle_id').on('change', function () {

                $('#reasign_driver_id').empty();
                var html = '<option value="">-Selecciona-</option>';
                if ($("#reasign_vehicle_id").val() != '') {
                    $.ajax({
                        url: '{{ route('adminPlanning.getDriversAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            vehicle_id: $("#reasign_vehicle_id").val()
                        }
                    })
                        .done(function (data) {
                            if (data.length > 0) {
                                $.each(data, function (index, val) {
                                    html += '<option value="' + val.id + '">' + val.first_name + ' ' + val.last_name + '</option>';
                                });
                            }
                            $('#reasign_driver_id').html(html);
                        })
                        .fail(function (data) {
                            console.error("error al obtener los conductores.");
                        })
                } else {
                    $('#reasign_driver_id').html(html);
                }
            });

            $('#form-route-submit').on('click', function (event) {
                event.preventDefault();
                var $marks = $('#form-route-divide input[type=checkbox]').filter(':checked');
                if ($marks.length) {
                    if ($('#form-route-divide').valid()) {
                        var route_id = $('#divide_route_id').val(),
                            $form = $('#form-route-divide'),
                            $this = $(this),
                            data = $form.serialize(),
                            url = "{{ route('adminDispatchPlanning.transport') }}/" + route_id + '/divide-route';
                        $this.addClass('disabled');
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            data: data,
                        })
                            .done(function (data) {
                                $this.removeClass('disabled');
                                if (data.status) {
                                    $('#validate_route').val("")
                                    $('#warehouse_id').change();
                                    $(".divide-route").hide('low')
                                    swal(
                                        '¡Bien hecho!',
                                        data.message,
                                        'success'
                                    );
                                } else {
                                    swal(
                                        'Oops...',
                                        data.message,
                                        'error'
                                    );
                                }
                            })
                            .fail(function () {
                                $this.removeClass('disabled');
                                swal(
                                    'Oops...',
                                    '¡Al parecer tenemos problemas tratando de obtener la información!',
                                    'error'
                                );
                            });
                    } else {
                        swal(
                            'Oops...',
                            'Debes diligenciar correctamente todos los datos necesarios.',
                            'error'
                        );
                    }
                } else {
                    swal(
                        'Oops...',
                        'Debes seleccionar al menos una Orden para ser reasignada.',
                        'error'
                    );
                }
            });

            $('#validate_route, #warehouse_id, #city_id').change(function (e) {
                $(".divide-route").hide('low')
                $(".reasign-route").hide('low')
                $("#reasign_transporter_id").val('')
                $("#reasign_transporter_id").change()
                $("#reasign_vehicle_id").val('')
                $("#reasign_driver_id").val('')
            })
            $('#city_id').change(function (e) {
                $('#order_departure_city_id').val($(this).val());
                $('#validate_route, #route, #divide_route_vehicle_id, #divide_route_vehicle_id').addClass('disabled');
                $('#warehouse_id').html('').append($("<option value=''>Selecciona</option>"));
                $('.loading-city').removeClass('unseen');
                $.ajax({
                    url: '{{ route('adminPlanning.getChangeCity') }}',
                    data: {id: $(this).val()},
                    type: 'GET',
                    dataType: 'json'
                })
                    .done(function (data) {
                        $('#validate_route, #route, #divide_route_vehicle_id, #divide_route_driver_id').empty().append('<option value="">-Selecciona-</option>');
                        if ($('#warehouse_id').val() != '') {
                            $.each(data.routes, function (key, value) {
                                $('#validate_route, #route').append('<option value="' + value.id + '">' + value.route + '</option>');
                            });
                        }
                        $.each(data.warehouses, function (index, val) {
                            $('#warehouse_id').append($('<option></option>').attr('value', val.id).text(val.warehouse));
                        });
                    })
                    .complete(function () {
                        $('.loading-city').addClass('unseen');
                        $('#validate_route, #route, #divide_route_vehicle_id, #divide_route_driver_id').removeClass('disabled');
                    })
                    .fail(function (data) {
                        console.error("error al obtener los datos.");
                        e.preventDefault();
                    });
            });

            $('body').on('change', '#warehouse_id', function (e) {
                var warehouse_id = $(this).val();
                var city_id = $('#city_id option:selected').val();

                $('#validate_route, #route, #vehicle_id, #driver_id, #transporter_id').addClass('disabled');
                $('#validate_route').html('').append($("<option value=''>Selecciona</option>"));
                $('#route').html('').append($("<option value=''>Selecciona</option>"));
                $('.loading-city').removeClass('unseen');
                $.ajax({
                    url: '{{ route('adminPlanning.getChangeWarehouse') }}',
                    data: {id: city_id, warehouse_id: warehouse_id},
                    type: 'GET',
                    dataType: 'json'
                })
                    .done(function (data) {
                        $.each(data.routes, function (index, val) {
                            $('#validate_route').append($('<option></option>').attr('value', val.id).text(val.route));
                            $('#route').append($('<option></option>').attr('value', val.id).text(val.route));
                        });
                    })
                    .complete(function () {
                        $('.loading-city').addClass('unseen');
                        $('#validate_route, #route, #vehicle_id, #driver_id, #transporter_id').removeClass('disabled');
                    })
                    .fail(function (data) {
                        console.error("error al obtener los datos.");
                        e.preventDefault();
                    });
            });

            $('#warehouse_id').trigger('change');

            $('#route').change(function (e) {
                $('#order_departure_route_id').val($(this).val());
            });
            setTimeout(function () {
                $('#check-all').iCheck('destroy');
            }, 1000);

            $('body').on('change', '#divide_route_vehicle_id', function (e) {
                $.ajax({
                    url: '{{ route('adminDispatchPlanning.getDriversAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        vehicle_id: $("#divide_route_vehicle_id").val()
                    }
                })
                    .done(function (data) {
                        if (data.length > 0) {
                            $('#divide_route_driver_id').empty();
                            var html = '<option value="">-Selecciona-</option>';
                            $.each(data, function (index, val) {
                                html += '<option value="' + val.id + '">' + val.first_name + ' ' + val.last_name + '</option>';
                            });
                            $('#divide_route_driver_id').html(html);
                        }
                    })
                    .fail(function (data) {
                        console.error("error al obtener los conductores.");
                    })
            });
        });

        {{--$('body').on('click', '#assign-form button[type="submit"]', function (event) {--}}
        {{--event.preventDefault();--}}
        {{--if ($('#assign-form').valid()) {--}}
        {{--if (confirm('¿Esta seguro que desea realizar la reasignación?')) {--}}
        {{--$('#reasign_route_id').val($('#city_id').val());--}}
        {{--$('#reasign_city_id').val($('#city_id').val());--}}
        {{--$('#reasign_warehouse_id').val($('#warehouse_id').val());--}}
        {{--$(this).addClass('disabled');--}}
        {{--$('#assign-form .loading-vehicle').removeClass('unseen');--}}
        {{--$('#assign-form').attr('action', '{{ route('adminDispatchPlanning.reassignTransporters') }}');--}}
        {{--$('#assign-form').submit();--}}
        {{--}--}}
        {{--}--}}
        {{--})--}}

        $('#btn-reassign').on('click', function (event) {
            event.preventDefault();
            if ($('#assign-form').valid()) {
                $this = $(this);
                $this.addClass('disabled');

                $.ajax({
                    url: '{{ route('adminDispatchPlanning.reassignTransporters') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        reasign_route_id: $("#validate_route").val(),
                        reasign_city_id: $("#city_id").val(),
                        reasign_warehouse_id: $("#warehouse_id").val(),
                        reasign_driver_id: $("#reasign_driver_id").val(),
                        reasign_vehicle_id: $("#reasign_vehicle_id").val(),
                    },
                })
                    .done(function (data) {
                        $this.removeClass('disabled');
                        if (data.status) {
                            $('#validate_route').val("")
                            $('#warehouse_id').change();
                            $(".reasing-route").hide('low')
                            swal(
                                '¡Bien hecho!',
                                data.message,
                                'success'
                            );
                        } else {
                            swal(
                                'Oops...',
                                data.message,
                                'error'
                            );
                        }
                    })
                    .fail(function () {
                        $this.removeClass('disabled');
                        swal(
                            'Oops...',
                            '¡Al parecer tenemos problemas tratando de obtener la información!',
                            'error'
                        );
                    });
            } else {
                swal(
                    'Oops...',
                    'Debes diligenciar correctamente todos los datos necesarios.',
                    'error'
                );
            }

        });


    </script>

@stop
