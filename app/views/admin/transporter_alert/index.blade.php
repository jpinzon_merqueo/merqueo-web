@extends('admin.layout')
@if (!Request::ajax())
	@section('content')
		<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
		<section class="content-header">
			<h1>
				{{ $title }}
				<small>Control panel</small>
			</h1>
			<span class="breadcrumb" style="top:0px">
				<button type="button" class="btn btn-success" id="massive_resolver">Resolver masivo</button>
				<a href="{{ route('adminTransporterAlert.resolved') }}">
					<button type="button" class="btn btn-primary">Ver alertas resueltas</button>
				</a>
			</span>
		</section>

		<section class="content">

			@if ( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Hecho!</b> {{ Session::get('success') }}
				</div>
			@endif
			@if ( Session::has('error') )
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Alert!</b> {{ Session::get('error') }}
				</div>
			@endif

			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<div class="row form-group">
								<div class="col-xs-12">
									<form id="search-alert-form">
										<div class="col-lg-4">
											<label class="control-label">Ciudad:</label>
											<select id="city_id" name="city_id" class="form-control">
												@foreach ($cities as $city)
													<option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-lg-4">
											<label class="control-label">Placa:</label>
											<input type="text" name="plate" id="plate" class="form-control">
										</div>
										<div class="col-lg-4">
											<input type="hidden" name="status" value="Pendiente">
											<button type="submit" id="btn-search-alert" class="btn btn-primary">Buscar</button>
										</div>
									</form>
								</div>
							</div>
							<div class="paging" style="display: none;"></div>
							<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Resolve Alerts Modal -->
		<div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form action="{{ route('adminTransporterAlert.saveObservation') }}" method="POST">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Observaciones de la alerta</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<label for="observation">
										Observación
									</label>
									<textarea class="form-control" id="observation" name="observation" rows="3"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="modal-alert-id" name="alert_id">
							<button type="submit" class="btn btn-success">Guardar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">

			var TransporterAlerts = (function() {
				'use strict';

				function TransporterAlerts() {
					// enforces new
					if (!(this instanceof TransporterAlerts)) {
						return new TransporterAlerts();
					}
					this.url_get_alert = '{{ route('adminTransporterAlert.getAlertsAjax') }}';
					this.pusher();
					this.bindAction();
					this.count = 1;
					this.city_id = $('#city_id').val();
					// constructor body
				}

				TransporterAlerts.prototype.pusher = function() {
					Pusher.logToConsole = false;
					var pusher = new Pusher('{{ $app_key }}', {
						cluster: '{{ $app_cluster }}'
					});

					var channel = pusher.subscribe('transporter-alert-channel');
					// Formato para el api
					/*{
					  "id": "4",
					  "city": "Bogotá",
					  "city_id": 1,
					  "order_id": "123122",
					  "plate": "Hello123",
					  "cellphone": "Hello123",
					  "driver": "Luis Palomá",
					  "reason": "prueba",
					  "status": "Pendiente",
					  "created_at": "17 Oct 2017 11:26 am"
					}*/

					channel.bind('transporter-alert-event', function(data) {
						if (data.city_id == this.city_id) {
							var html = '';
							var background_color = '#FFFB89';
							html += '<tr style="display: none;">';
							html += '<td style="background: '+ background_color +'">';
							html += '<input type="checkbox" name="to_solve[]" class="form-control check" value="' + data.id + '" />';
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += (++this.count);
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += data.city;
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							if ( typeof(data.order_id) != 'undefined' && data.order_id != null ) {
								html += '<a href="' + data.order_url + '" target="_blank">';
								html += data.order_id;
								html += '</a>';
							}
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += data.plate;
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += data.cellphone;
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += data.driver;
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += data.reason;
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += '<label class="label label-warning">'+data.status+'</label>';
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += data.created_at;
							html += '</td>';
							html += '<td style="background: '+ background_color +'">';
							html += '<button type="button" class="btn btn-success dropdown-toggle resolve-modal" data-toggle="dropdown" data-alert_id="'+ data.id +'">Resolver</button>';
							html += '</td>';
							html += '</tr>';
							var tr = $(html);
							$('#table-body').prepend(tr);
							if ( $('#data-not-found').length ) {
								$('#data-not-found').remove();
							}
							tr.fadeIn('slow', function () {
								setTimeout(function () {
									tr.find('td').removeAttr('style');
								}, 50000);
							});
						}
					}, this);
				};

				TransporterAlerts.prototype.bindAction = function() {
					var self = this;
					this.getAlerts(null);
					$('body').on('click', '.resolve-modal', function(event) {
						event.preventDefault();
						var alert_id = $(this).data('alert_id');
						$('#modal-alert-id').val(alert_id);
						$('#modal-alert').modal('show');
					});
					$('body').on('click', '#massive_resolver', function(event) {
						event.preventDefault();
						var alert_ids = $("input[name='to_solve[]']:checked")
          									.map(function(){return $(this).val();}).get();
						alert_ids = JSON.stringify(alert_ids);
						$('#modal-alert-id').val( alert_ids );
						$('#modal-alert').modal('show');
					});
					$('#modal-alert').on('hidden.bs.modal', function () {
						$('#modal-alert-id').val(null);
					});
					$('body').on('click', '#check_all', function(event) {
						$('.check').not(this).prop('checked', this.checked);
					});
					$('body').on('submit', '#search-alert-form', function(event) {
						event.preventDefault();
						self.getAlerts(null);
					});
                    $('body').on('click', '.pagination a', function(event) {
                        event.preventDefault();
                        var paginate_url = $(this).prop('href');
                        self.getAlerts(paginate_url);
                    });
				};

				TransporterAlerts.prototype.getAlerts = function(url) {
                    var form_data = $('#search-alert-form').serialize();
                    var url_get = this.url_get_alert;
                    if (url != null) {
                        url_get = url;
					}
					$.ajax({
						url: url_get,
						type: 'GET',
						dataType: 'json',
						data: form_data,
						context: this
					})
					.done(function(data) {
						var self = this;
						$('.paging-loading').slideUp('fast', function () {
							$('.paging').html(data.html).slideDown('fast');
							self.count = data.count;
						});
					})
					.fail(function() {
						alert('Ocurrió un error al consultar las alertas.')
					})
					.always(function() {
						$('.paging-loading').slideUp('fast');
					});
				};

				return TransporterAlerts;
			}());

			$(document).ready(function() {
				var transporter_alert = new TransporterAlerts;
			});
		</script>
	@endsection
@else
	@section('alerts')
		<table id="orders-table" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th align="center" class="text-center"><input type="checkbox" name="" class="" id="check_all"></th>
					<th>ID</th>
					<th>Ciudad</th>
					<th>ID Pedido</th>
					<th>Placa</th>
					<th>Celular</th>
					<th>Conductor</th>
					<th>Motivo</th>
					<th>Estado</th>
					<th>Fecha</th>
					<th>Acción</th>
				</tr>
			</thead>
			<tbody id="table-body">
				@if ( count($alerts) )
					<?php $i = count($alerts); ?>
					@foreach ($alerts as $alert)
					<tr>
						<td>
							<input type="checkbox" name="to_solve[]" class="form-control check" value="{{ $alert->alert_id }}">
						</td>
						<td>
							{{ $alert->alert_id }}
						</td>
						<td>
							{{ $alert->city }}
						</td>
						<td>
							@if ( $alert->order_id )
								<a href="{{ route('adminOrderStorage.details', ['id' => $alert->order_id]) }}" target="_blank">
									{{ $alert->order_id }}
								</a>
							@endif
						</td>
						<td>
							{{ $alert->plate }}
						</td>
						<td>
							{{ $alert->cellphone }}
						</td>
						<td>
							{{ $alert->fullname }}
						</td>
						<td>
							{{ $alert->reason }}
						</td>
						<td>
							<label class="label
							@if ( $alert->status == 'Pendiente' )
								label-warning
							@endif
							@if ( $alert->status == 'Resuelto' )
								label-success
							@endif
							">
								{{ $alert->status }}
							</label>
						</td>
						<td>
							{{ date("d M Y g:i a",strtotime($alert->created_at)) }}
						</td>
						<td>
							@if ( $alert->status == 'Pendiente' )
								<button type="button" class="btn btn-success dropdown-toggle resolve-modal" data-toggle="dropdown" data-alert_id="{{ $alert->alert_id }}">
									Resolver
								</button>
							@endif
						</td>
					</tr>
					@endforeach
				@else
					<tr id="data-not-found">
						<td colspan="11" class="text-center">
							No se encontraron alertas
						</td>
					</tr>
				@endif
			</tbody>
		</table>
		<div class="row">
			<div class="col-lg-12 text-center">
				{{ $links_html }}
			</div>
		</div>
	@endsection
@endif