@extends('admin.layout')
@if (!Request::ajax())
	@section('content')
		<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
		<section class="content-header">
			<h1>
				{{ $title }}
				<small>Control panel</small>
			</h1>
			<span class="breadcrumb" style="top:0px">
				<a href="{{ route('adminTransporterAlert.index') }}">
					<button type="button" class="btn btn-primary">Ver alertas pendientes</button>
				</a>
			</span>
		</section>

		<section class="content">

			@if ( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Alert!</b> {{ Session::get('success') }}
				</div>
			@endif
			@if ( Session::has('error') )
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Alert!</b> {{ Session::get('error') }}
				</div>
			@endif

			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<div class="row form-group">
								<div class="col-xs-12">
									<form id="search-alert-form" class="form-horizontal">
										<div class="form-group">
											<label class="col-xs-1 control-label">Ciudad:</label>
											<div class="col-xs-4">
												<select id="city_id" name="city_id" class="form-control">
													@foreach ($cities as $city)
														<option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
													@endforeach
												</select>
											</div>
											<label class="col-xs-1 control-label">Buscar:</label>
											<div class="col-xs-4">
												<input type="text" placeholder="Placa" id="input-search" class="form-control">
											</div>
											<div class="col-xs-2">
												<button type="submit" id="btn-search-alert" class="btn btn-primary">Buscar</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="paging" style="display: none;"></div>
							<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Resolve Alerts Modal -->
		<div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form action="{{ route('adminTransporterAlert.saveObservation') }}" method="POST">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Observaciones de la alerta</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<label for="observation">
										Observación
									</label>
									<textarea class="form-control" id="observation" name="observation" rows="3"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="modal-alert-id" name="alert_id">
							<button type="submit" class="btn btn-success">Guardar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">

			var TransporterAlerts = (function() {
				'use strict';

				function TransporterAlerts() {
					// enforces new
					if (!(this instanceof TransporterAlerts)) {
						return new TransporterAlerts();
					}
					this.url_get_alert = '{{ route('adminTransporterAlert.getAlertsAjax') }}';
					this.bindAction();
					// constructor body
				}

				TransporterAlerts.prototype.bindAction = function() {
					var self = this;
					this.getAlerts(null, 'Resuelto');
					$('body').on('click', '.resolve-modal', function(event) {
						event.preventDefault();
						var alert_id = $(this).data('alert_id');
						$('#modal-alert-id').val(alert_id);
						$('#modal-alert').modal('show');
					});
					$('#modal-alert').on('hidden.bs.modal', function () {
						$('#modal-alert-id').val(null);
					});
					$('body').on('submit', '#search-alert-form', function(event) {
						event.preventDefault();
						var plate = $(this).find('#input-search').val();
						var city_id = $(this).find('#city_id').val();
						self.getAlerts(null, 'Resuelto', plate, city_id);
					});
					$('body').on('click', '#btn-reset-alert', function(event) {
						self.getAlerts(null, 'Resuelto');
					});
				};

				TransporterAlerts.prototype.getAlerts = function(reason, status, plate, city_id) {
					$.ajax({
						url: this.url_get_alert,
						type: 'GET',
						dataType: 'html',
						data: {
							reason: reason,
							status: status,
							plate: plate,
							city_id: city_id
						},
					})
					.done(function(data) {
						$('.paging-loading').slideUp('fast', function () {
							$('.paging').html(data).slideDown('fast');
						});
					})
					.fail(function() {
						alert('Ocurrió un error al consultar las alertas.')
					})
					.always(function() {
						$('.paging-loading').slideUp('fast');
					});
				};

				return TransporterAlerts;
			}());

			$(document).ready(function() {
				var transporter_alert = new TransporterAlerts;
			});
		</script>
	@endsection
@else
	@section('alerts')
		<table id="orders-table" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Ciudad</th>
					<th>ID Pedido</th>
					@if ( isset($status) && $status == 'Resuelto' )
					<th>Admin</th>
					@endif
					<th>Placa</th>
					<th>Celular</th>
					<th>Conductor</th>
					<th>Razón</th>
					@if ( isset($status) && $status == 'Resuelto' )
					<th width="300">Observaciones</th>
					@endif
					<th>Estado</th>
					<th>Fecha</th>
				</tr>
			</thead>
			<tbody>
				@if ( count($alerts) )
					@foreach ($alerts as $alert)
					<tr>
						<td>
							{{ $alert->alert_id }}
						</td>
						<td>
							{{ $alert->city }}
						</td>
						<td>
							@if ( $alert->order_id )
								<a href="{{ route('adminOrders.details', ['id' => $alert->order_id]) }}" target="_blank">
									{{ $alert->order_id }}
								</a>
							@endif
						</td>
						@if ( isset($status) && $status == 'Resuelto' )
							<td>
								{{ $alert->admin_fullname }}
							</td>
						@endif
						<td>
							{{ $alert->plate }}
						</td>
						<td>
							{{ $alert->cellphone }}
						</td>
						<td>
							{{ $alert->fullname }}
						</td>
						<td>
							{{ $alert->reason }}
						</td>
						@if ( isset($status) && $status == 'Resuelto' )
							<td>
								{{ $alert->observation }}
							</td>
						@endif
						<td>
							<label class="label
							@if ( $alert->status == 'Pendiente' )
								label-warning
							@endif
							@if ( $alert->status == 'Resuelto' )
								label-success
							@endif
							">
								{{ $alert->status }}
							</label>
						</td>
						<td>
							{{ date("d M Y g:i a",strtotime($alert->created_at)) }}
						</td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="11" class="text-center">
							No se encontraron alertas
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	@endsection
@endif