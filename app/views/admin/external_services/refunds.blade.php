<div class="row">
	<div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body table-responsive container-overflow">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th>Charge ID</th>
                            <th>Usuario</th>
                            <th>Razón</th>
                        </tr>
                    </thead>
                    <tbody>
                	@if ($payment->refunds->count())
                        @foreach ($payment->refunds as $payment_refund)
                        <tr>
                            <td>{{ date("d M Y h:i a", strtotime($payment_refund->date)) }}</td>
                            <td>
                                @if ($payment_refund->status == 'Aprobada')
                                    <span class="badge bg-green">{{ $payment_refund->status }}</span>
                                @elseif ($payment_refund->status == 'Declinada')
                                    <span class="badge bg-red">{{ $payment_refund->status }}</span>
                                @elseif ($payment_refund->status == 'Pendiente')
                                    <span class="badge bg-orange">{{ $payment_refund->status }}</span>
                                @endif
                            </td>
                            <td>{{ $payment->cc_charge_id }}</td>
                            <td>{{ $payment_refund->admin->fullname }}</td>
                            <td>{{ $payment_refund->reason }}</td>
                        </tr>
                        @endforeach
                    @else
                        <tr><td colspan="5">No hay reembolsos.</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>