@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<script>
    var web_url_ajax = "{{ route('adminExternalServices.index') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    @if (Session::get('admin_designation') == 'Admin')
    <span class="breadcrumb" style="top:0px">
       
    </span>
    @endif
</section>


<section class="content">
    @if(Session::has('message'))
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive form-horizontal">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="search" class="col-sm-1 control-label">Buscar:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nombre, email, teléfono">
                                </div>
                                <div class="col-sm-1">
                                    <button id="search-btn" class="btn btn-success">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
					<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    <div class="paging"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
	var Buscador = (function() {
        'use strict';

        function Buscador() {
            // enforces new
            if (!(this instanceof Buscador)) {
                return new Buscador();
            }
            // constructor body
            this.search = null;
            this.bindActions();
            this.search_ajax();
        }

        Buscador.prototype.bindActions = function() {
            let self = this;
            $('body').on('click', '#search-btn', function(event) {
                self.search = $('#search').val();
                self.search_ajax();
            });
            $('#search').keyup(function(event) {
                if ( event.which == 13 ) {
                    self.search = $('#search').val();
                    self.search_ajax();
                }
            });
        };

        Buscador.prototype.search_ajax = function() {
            $('.paging-loading').show();
            $.ajax({
                url: '{{ route('adminExternalServices.index') }}',
                type: 'POST',
                dataType: 'html',
                data: {
                    s: this.search
                },
            })
            .done(function(data) {
                $('.paging').html(data);
            })
            .fail(function() {
                console.log("error al buscar, respuesta de ajax.");
            })
            .always(function() {
                $('.paging-loading').hide();
            });
        };
        return Buscador;
    }());
    var buscar = new Buscador;
});
</script>

@else

@section('content')

<table id="customers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Creado</th>
            <th>Tipo</th>
            <th>Estado</th>
            <th>Ver</th>
        </tr>
    </thead>
    <tbody>
    @if (count($externals))
        @foreach($externals as $external)
        <tr>
            <td>{{ $external->id }}</td>
            <td>{{ $external->user_name }}</td>
            <td>{{ $external->user_phone }}</td>
            <td>{{ $external->user_email }}</td>
            <td>{{ date("d M Y",strtotime($external->created_at)) }}</td>
            <td>{{ $external->type }}</td>
            <td>
                @if($external->status == 'Validación')
                    <span class="badge bg-maroon">{{ $external->status }}</span>
                @elseif($external->status == 'Aprobada')
                    <span class="badge bg-green">{{ $external->status }}</span>
                @elseif($external->status == 'Cancelado')
                    <span class="badge bg-red">{{ $external->status }}</span>
                @endif
            </td>
            <td>
                <a class="btn btn-xs btn-default"
                   href="{{ route('adminExternalServices.detail', $external->id) }}">
                    <span class="glyphicon glyphicon-folder-open"></span>
                </a>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="10" align="center">Servicios externos no encontrados</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($externals) }} Servicios</div>
    </div>
</div>


@endif

@stop