@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>
            {{ $title }} # {{ $external_service->id }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            @if($external_service->status == 'Validación' && $admin_permissions['permission2'])
                @if($have_payments_approved)
                    <a href="{{ route('adminExternalServices.soat_emit', $external_service->id) }}"
                    class="charge-amount"
                    onclick="return confirm('¿Estas seguro que deseas Emitir el SOAT manualmente?')">
                        <button type="button" class="btn btn-success">Emitir</button>
                    </a>
                @else
                    <a href="{{ route('adminExternalServices.validate_pay', $external_service->id) }}"
                    class="charge-amount"
                    onclick="return confirm('¿Estas seguro que deseas cobrar el total a la tarjeta de crédito?')">
                        <button type="button" class="btn btn-warning">Realizar Cobro a Tarjeta</button>
                    </a>
                @endif
            @endif
            @if($external_service->status == 'Validación' && $admin_permissions['permission1'])
                <a href="#" data-id="{{ $external_service->id }}">
                    <button type="button" class="btn btn-danger" onclick="show_modal('reject-order')">
                        Cancelar Servicio
                    </button>
                </a>
            @endif
            @if($admin_permissions['permission1'] && $external_service->canMakeRefund())
                <a href="#" data-id="{{ $external_service->id }}">
                <button type="button" class="btn btn-danger" onclick="show_modal('refund-order')">
                    Realizar un reembolso
                </button>
            </a>
            @endif
        </span>
    </section>

    <section class="content">

        {{--Mensajes de Alerta--}}
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif

        {{--Fin Mensajes de Alerta--}}

        <div class="row">
            <div class="col-xs-3">
                <div class="callout callout-info">
                    <h4>Datos de Pedido</h4>
                    <p><label>Estado</label> - <b>{{ $external_service->status }}</b></p>
                    <p><label>Referencia</label> - {{ $external_service->reference }}</p>
                    @if (!empty($external_service->invoice_number))
                        <p><label>Número factura</label> - {{ $external_service->invoice_number }}
                            <a href="javascript:;" data-reference="{{ $external_service->id }}"
                               data-href="{{ route('adminOrders.detailsViewInvoice', ['id' => $external_service->id]) }}"
                               class="view-invoice">Ver factura</a>
                            - <a href="javascript:;" class="modal-invoice">Actualizar factura</a>
                        </p>
                    @endif
                    <p><label>Ciudad</label> - {{ $external_service->city->city }}</p>
                    <p>
                        <label>Fecha de creación</label> -
                        {{ date("d M Y g:i a",strtotime($external_service->created_at)) }}
                    </p>
                    @if($external_service->management_date)
                        <p><label>Fecha gestión</label>
                            - {{ date("d M Y g:i a",strtotime($external_service->management_date)) }}</p>
                    @endif
                    <p><label>Comentarios</label> - {{ $external_service->user_comments }}</p>
                    @if ($external_service->status == 'Cancelled')
                        <p><label>Motivo de cancelación</label> - {{ $external_service->reject_reason }}</p>
                        @if(!empty($external_service->reject_comments))
                            <p><label>Comentarios de cancelación</label> - {{ $external_service->reject_comments }}</p>
                        @endif
                    @endif
                    <p><a href="#" title="Log del servicio" onclick="show_modal('log')">Log del servicio</a></p>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="callout callout-info">
                    <h4>Datos de Pago</h4>
                    <p><label>Tarjeta de crédito</label> -
                        **** {{ $external_service->cc_last_four }} {{ $external_service->cc_type }}
                        ({{ $external_service->cc_country }})
                    </p>
                    <p><a href="#" title="Ver transacciones" onclick="show_modal('movements')">Ver transacciones</a></p>
                    @if (!empty($external_service->cc_holder_name))
                        <p><label>Propietario</label> - {{ $external_service->cc_holder_name }}</p>
                    @endif
                    <p><label>Token</label> - {{ $external_service->cc_token }}</p>
                    <p>
                        <label>Estado de pago</label> -
                        {{ !$external_service->payment_date ? 'Pendiente' : 'Pagado' }}
                    </p>
                    @if($external_service->payment_date != '')
                        <p><label>Fecha de pago</label>
                            - {{ format_date('normal_long_with_time', $external_service->payment_date) }}</p>
                    @endif
                    <p>
                        <label>Total</label> -<b>
                            <span id="total">{{ currency_format($external_service->total) }}</span>
                        </b>
                    </p>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="callout callout-info">
                    <h4>Datos de Pagador</h4>
                    <p>
                        <label>Nombre</label> -
                        @if (empty($external_service->user))
                            {{ $external_service->user_name }}
                        @else
                            {{ $external_service->user->first_name }} {{ $external_service->user->last_name }}
                        @endif
                    </p>
                    <p>
                        <label>Celular</label> -
                        @if (empty($external_service->user))
                            {{ $external_service->user_phone }}
                        @else
                            {{ $external_service->user->phone }}
                        @endif
                    </p>
                    <p>
                        <label>Email</label> -
                        @if (empty($external_service->user))
                            {{ $external_service->user_email }}
                        @else
                            {{ $external_service->user->email }}
                        @endif
                    </p>
                    <p><label>Dirección</label> -
                        <b>{{ $external_service->user_address }} {{ $external_service->user_address_further }}</b>
                    </p>
                    <p><label>Ciudad</label> - {{ $external_service->city->city }}</p>
                    @if ($admin_permissions['update'])
                        <p><a href="#" onclick="show_modal('history')">Historial de pedidos</a></p>
                        @if($external_service->status == 'Dispatched' || $external_service->status == 'In Progress' && $admin_permissions['permission3'])
                            <p>
                                <button type="button" class="btn btn-success" id="send-sms-action">
                                    Enviar mensaje de texto
                                </button>
                            </p>
                        @endif
                        @if($external_service->status == 'Delivered')
                            <p>
                            <form method="POST"
                                  action="{{ route('adminOrders.reSendDeliveryEmail', ['id' => $external_service->id]) }}"
                                  class="form-horizontal" id="re-send-email">
                                <button type="submit" class="btn btn-success">
                                    Reenviar correo de pedido entregado
                                </button>
                            </form>
                            </p>
                        @endif
                    @endif
                </div>
            </div>

            <div class="col-xs-3">
                <div class="callout callout-info">
                    <h4>Datos de Tomador</h4>
                    <p>
                        <label>Nombre</label> -
                        {{ $external_service->taker->firstName }} {{ $external_service->taker->lastName }}
                    </p>
                    <p>
                        <label>Celular</label> -
                        {{ $external_service->taker->phone }}
                    </p>
                    <p>
                        <label>Email</label> -
                        {{ $external_service->taker->email }}
                    </p>
                    <p>
                        <label>Dirección</label> -
                        <b>{{ $external_service->taker->address }}</b>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Service Payments Modal -->
    <div class="modal fade" id="modal-movements" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Listado de Transacciones</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="table-responsive container-overflow box-body">
                                <table id="shelves-table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Estado</th>
                                        <th>Charge ID</th>
                                        <th>Id Transacción</th>
                                        <th>Usuario</th>
                                        <th>Reembolsos</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($external_service->payments->count())
                                        @foreach($external_service->payments as $payment)
                                            <tr>
                                                <td>{{ format_date('normal_long_with_time', $payment->created_at) }}</td>
                                                <td>
                                                    @if ($payment->cc_payment_status == 'Aprobada')
                                                        <span class="badge bg-green">Aprobada</span>
                                                    @elseif ($payment->cc_payment_status == 'Rechazada')
                                                        <span class="badge bg-red">Rechazada</span>
                                                    @elseif ($payment->cc_payment_status == 'Declinada')
                                                        <span class="badge bg-maroon">Declinada</span>
                                                    @endif
                                                </td>
                                                <td>{{ $payment->cc_charge_id }}</td>
                                                <td>{{ $payment->cc_payment_transaction_id }}</td>
                                                <td>{{ $payment->admin ? $payment->admin->fullname : '-' }}</td>
                                                <td>
                                                    @if ($payment->refunds->count())
                                                        <a href="#" onclick="show_modal('service-payment-refunds', {{ $payment->id }})">
                                                            <span class="glyphicon glyphicon-folder-open"></span>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" align="center">No hay datos.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Service Payments Refunds -->
    <div class="modal fade" id="modal-service-payment-refunds" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Listado de Reembolsos</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Log Modal -->
    <div class="modal fade" id="modal-log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Log del Servicio</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="table-responsive container-overflow box-body">
                                <table id="shelves-table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Descripción</th>
                                        <th>Usuario admin</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($external_service->logs->count())
                                        @foreach($external_service->logs as $log)
                                            <tr>
                                                <td>{{ format_date('normal_long_with_time', $log->created_at) }}</td>
                                                <td>{{ $log->type }}</td>
                                                <td>{{ $log->admin ? $log->admin->fullname : '-' }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" align="center">No hay datos.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Reject Order Modal -->
    <div class="modal fade" id="modal-reject-order">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cancelar Servicio</h4>
                </div>
                <form method="post"
                      action="{{ route('adminExternalServices.cancel', $external_service->id) }}"
                      enctype="multipart/form-data" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="reject-label">Selecciona la razón por la cual cancelas este pedido</label>
                            <select class="form-control required" name="reject_reason">
                                <option value="">-Selecciona-</option>
                                @foreach($reasons as $reason)
                                    <option value="{{$reason->id}}">
                                        {{$reason->reason}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="notification" class="reject-label">
                                ¿Deseas enviar un mail al cliente?
                            </label>
                            <div class="checkbox">
                                <label for="notification">
                                    <input type="checkbox" name="notify_user" id="notification"
                                           class="form-control reject-notification" value="true">
                                    Enviar notificación de cancelación.
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="reject-label">Comentarios de cancelación</label>
                            <textarea class="form-control" id="reject_comments" name="reject_comments"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger save-status reject-btn">Cancelar Servicio</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Refund Payment Modal -->
    <div class="modal fade" id="modal-refund-order">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Realizar el reembolso</h4>
                </div>
                <form method="post"
                      action="{{ route('adminExternalServices.refund', $external_service->id) }}"
                      enctype="multipart/form-data" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="reject-label">Selecciona la razón por la cual realiza el reembolso</label>
                            <select class="form-control required" name="reject_reason">
                                <option value="">-Selecciona-</option>
                                @foreach($refund_reasons as $reason)
                                    <option value="{{ $reason->id }}">
                                        {{ $reason->reason }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="notification" class="reject-label">
                                ¿Deseas enviar un mail al cliente?
                            </label>
                            <div class="checkbox">
                                <label for="notification">
                                    <input type="checkbox" name="notify_user" id="notification"
                                           class="form-control reject-notification" value="true">
                                    Enviar notificación de cancelación.
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="reject-label">Comentarios de cancelación</label>
                            <textarea class="form-control" id="reject_comments" name="reject_comments"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger save-status reject-btn">Cancelar Servicio</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Service History Log Modal -->
    <div class="modal fade" id="modal-history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Historial de Servicios</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="table-responsive container-overflow box-body">
                                <table id="shelves-table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Total</th>
                                        <th>Placa</th>
                                        <th>Estado</th>
                                        <th>Ver</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($external_service->user->external_services->count())
                                        @foreach($external_service->user->external_services as $service)
                                            <tr>
                                                <td>{{ format_date('normal_long_with_time', $service->created_at) }}</td>
                                                <td>{{ currency_format($service->total) }}</td>
                                                <td>{{ $service->plate }}</td>
                                                <td>{{ $service->status }}</td>
                                                <td><a href="{{ route('adminExternalServices.detail', $service->id) }}">Ir</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" align="center">No hay datos.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function show_modal(action, reference) {
            if (action === 'service-payment-refunds') {
                var url = '{{ route('adminExternalServicePayment.detail', 'TEMPORAL') }}';
                $('#modal-service-payment-refunds .modal-body').html('<div style="text-align:center;">Cargando...</div>');
                url = url.replace('TEMPORAL', reference);
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    success: function (response) {
                        $('#modal-service-payment-refunds').find('.modal-body').html(response);
                    }
                });
            }

            $('#modal-' + action).modal('show');
        }
    </script>
@stop
