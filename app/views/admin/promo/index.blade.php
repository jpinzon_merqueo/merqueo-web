@extends('admin.layout')
@section('content')
	<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
	<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js"></script>
	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
	</section>
	<section class="content">

		@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('error') }}
		</div>
		@endif

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-4 form-group">
										<label for="type">Tipo de promoción</label>
										<select name="type" id="type" class="form-control" required="required">
											<option value=""></option>
											<optgroup label="Promoción en productos">
												<option value="1">Porcentaje de descuento</option>
												<option value="2">Precio fijo</option>
											</optgroup>
											@if ($admin_permissions['permission1'])
											<optgroup label="Promoción en pedidos">
												<option value="3">Descuento global en domicílio</option>
												<option value="4">Descuento global en crédito</option>
												<option value="5">Descuento global en porcentaje</option>
											</optgroup>
											@endif
										</select>
									</div>
								</div>
								<form id="add-promo-form" action="{{ route('adminPromo.save') }}" method="POST" enctype="multipart/form-data" style="display: none">
									<div class="row percentage unseen option-1">
										<div class="col-xs-12">
											<div class="row">
												<div class="col-xs-12 form-group">
													<label for="action">Que desea hacer?</label>
													<div class="row">
														<div class="col-xs-3">
															<select name="action" id="action" class="form-control">
																<option value="1" selected="selected">Colocar precio especial</option>
																<option value="0">Quitar precio especial</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-xs-3">
													<label for="city_id">Ciudad </label>
													<select name="city_id" id="city_id" class="form-control">
														<option value="">-- Ciudad --</option>
														@foreach ($cities as $city)
														<option value="{{ $city->id }}">{{ $city->city }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group col-xs-3 store_id unseen">
													<label for="store_id">Tienda</label>
													<select class="form-control" name="store_id" id="store_id">
													</select>
												</div>
												<div class="form-group col-xs-3 department_id unseen">
													<label for="department_id">Departamento</label>
													<select class="form-control" name="department_id" id="department_id">
													</select>
												</div>
												<div class="form-group col-xs-3 shelf_id unseen">
													<label for="shelf_id">Pasillo</label>
													<select class="form-control" name="shelf_id" id="shelf_id">
													</select>
												</div>
											</div>
											<div class="row toHide">
												<div class="form-group col-xs-3 inputPercentage hide">
													<label for="percentage">
														Porcentaje
													</label>
													<div class="input-group">
														<input type="number" name="percentage" class="form-control" id="percentage">
														<div class="input-group-addon">%</div>
													</div>
												</div>
												<div class="form-group col-xs-3 inputFixedPrice hide">
													<label for="fixed_price">
														Precio
													</label>
													<input type="text" name="fixed_price" class="form-control" placeholder="Ingresa el precio especial">

												</div>
												<div class="form-group col-xs-3">
													<label for="quantity_special_price">
														Cantidad por pedido
													</label>
													<div class="input-group">
														<input type="number" name="quantity_special_price" id="quantity_special_price" class="form-control">
														<div class="input-group-addon">UND</div>
													</div>
												</div>
												<div class="form-group col-xs-3">
													<label for="first_order_special_price">
														Tipo de uso para cliente
													</label>
													<select id="first_order_special_price" name="first_order_special_price" class="form-control">
														<option></option>
														<option value="1">Solo primera compra</option>
														<option value="0">Normal</option>
													</select>
												</div>
												<div class="form-group col-xs-3">
													<label for="special_price_starting_date">Fecha de inicio</label>
													<input type='text' class="form-control" id='special_price_starting_date'  name="special_price_starting_date">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-xs-3">
													<label for="special_price_expiration_date">Fecha de expiración</label>
													<input type='text' class="form-control" id='special_price_expiration_date'  name="special_price_expiration_date">
												</div>
												<div class="form-group col-xs-4">
													<label for="csv_file">
														Archivo CSV
													</label>
													<input type="file" name="csv_file" id="csv_file">
													<input type="hidden" name="typeImportation" id="typeImportation">
												</div>
											</div>
											<div class="row show-on-special-price">
												<div class="form-group col-xs-5">
													@include('admin.products.store_product_discount_distro', ['store_product' => new StoreProduct(), 'permission' => $permission])
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<div class="form-group">
												<button id="save" class="btn btn-primary" type="submit">Guardar</button>
												<button class="btn btn-primary" type="reset">Reset</button>
											</div>
										</div>
									</div>
								</form>
								@if ($admin_permissions['permission1'])
								<form id="add-free-delivery" action="{{ route('adminPromo.saveDiscount') }}" method="POST" style="display: none;">
									<input type="hidden" name="type" value="free_delivery">
									<div class="row form-group">
										<div class="col-xs-3">
											<label for="">Tipo de descuento</label>
											<div class="row">
												<div class="col-xs-12">
													<select name="discount_type" id="discount_type" class="form-control">
														<option></option>
														<option value="1">Descuento por ciudades especificas</option>
														<option value="2">Descuento por tiendas especificas</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="discount_type_option_1" style="display: none">
										<div class="row">
											<div class="col-xs-3 form-group">
												<label for="city_id">Ciudad</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="city_id[]" id="city_id" class="form-control" data-selector="discount_type_option_1" multiple="multiple">
															@foreach ($cities as $city)
																<option value="{{ $city->id }}">{{ $city->city }}</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group">
												<label for="city_id_free_delivery_opt_1">Para pedidos mayores a</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="minimum_order_amount" id="minimum_order_amount" class="form-control" placeholder="Ingresa Total máximo del pedido">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="discount_type_option_2" style="display: none">
										<div class="row">
											<div class="col-xs-3 form-group">
												<label for="city_id">Ciudad</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="city_id" id="city_id" class="form-control" data-selector="store_free_delivery">
															<option value=""></option>
															@foreach ($cities as $city)
																<option value="{{ $city->id }}">{{ $city->city }}</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group store_free_delivery" style="display: none;">
												<label for="store_id">Tienda</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="store_id[]" id="store_id" class="form-control" multiple="multiple">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group minimum_order_amount" style="display: none">
												<label for="minimum_order_amount">Para pedidos mayores a</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="minimum_order_amount" id="minimum_order_amount" class="form-control" placeholder="Ingresa total máximo del pedido">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<div class="form-group">
												<button id="save" class="btn btn-primary" type="submit">Guardar</button>
												<button class="btn btn-primary" type="reset">Reset</button>
											</div>
										</div>
									</div>
								</form>
								<form id="add-discount-credit" action="{{ route('adminPromo.saveDiscount') }}" method="POST" style="display: none;">
									<input type="hidden" name="type" value="discount_credit">
									<div class="discount_credit_type">
										<div class="row">
											<div class="col-xs-3 form-group">
												<label for="order_for_tomorrow">¿Solo pedidos para mañana?</label>
												<select name="order_for_tomorrow" id="order_for_tomorrow" class="form-control" required="">
													<option value="0">No</option>
													<option value="1">Sí</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3 form-group">
												<label for="city_id_discount_credit">Ciudad</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="city_id" id="city_id" class="form-control" data-selector="store_discount_credit">
															<option value=""></option>
															@foreach ($cities as $city)
																<option value="{{ $city->id }}">{{ $city->city }}</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group store_discount_credit" style="display: none;" required="">
												<label for="store_id">Tienda</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="store_id" id="store_id" class="form-control" data-selector="deparment_id_discount_credit">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group deparment_id_discount_credit" style="display: none;">
												<label for="department_id">Categoría</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="department_id" id="department_id" class="form-control" data-selector="shelf_id_discount_credit">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group shelf_id_discount_credit" style="display: none;">
												<label for="shelf_id">Subcategoría</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="shelf_id" id="shelf_id" class="form-control" data-selector="product_id_discount_credit">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3 form-group product_id_discount_credit" style="display: none">
												<label for="order_amount_discount">Producto</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="store_product_id" id="store_product_id" class="form-control">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group order_amount_discount" style="display: none">
												<label for="order_amount_discount">Valor de descuento</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="order_amount_discount" id="order_amount_discount" class="form-control" placeholder="Ingresa el valor del descuento">
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group minimum_order_amount" style="display: none">
												<label for="minimum_order_amount">Para pedidos mayores a</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="minimum_order_amount" id="minimum_order_amount" class="form-control" placeholder="Ingresa total mínimo del pedido">
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group maximum_order_amount" style="display: none">
												<label for="maximum_order_amount">Para pedidos menores a</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="maximum_order_amount" id="maximum_order_amount" class="form-control" placeholder="Ingresa total máximo del pedido">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<div class="form-group">
												<button id="save" class="btn btn-primary" type="submit">Guardar</button>
												<button class="btn btn-primary" type="reset">Reset</button>
											</div>
										</div>
									</div>
								</form>
								<form id="add-discount-percentage" action="{{ route('adminPromo.saveDiscount') }}" method="POST" style="display: none;">
									<input type="hidden" name="type" value="discount_percentage">
									<div class="discount_percentage_type">
										<div class="row">
											<div class="col-xs-3 form-group">
												<label for="order_for_tomorrow">¿Solo pedidos para mañana?</label>
												<select name="order_for_tomorrow" id="order_for_tomorrow" class="form-control" required="">
													<option value="0">No</option>
													<option value="1">Sí</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3 form-group">
												<label for="city_id_discount_percentage">Ciudad</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="city_id" id="city_id" class="form-control" data-selector="store_discount_percentage" required="">
															<option value=""></option>
															@foreach ($cities as $city)
																<option value="{{ $city->id }}">{{ $city->city }}</option>
															@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group store_discount_percentage" style="display: none;">
												<label for="store_id">Tienda</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="store_id" id="store_id" class="form-control" data-selector="deparment_id_discount_percentage">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group deparment_id_discount_percentage" style="display: none;">
												<label for="department_id">Categoría</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="department_id" id="department_id" class="form-control" data-selector="shelf_id_discount_percentage">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group shelf_id_discount_percentage" style="display: none;">
												<label for="shelf_id">Subcategoría</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="shelf_id" id="shelf_id" class="form-control" data-selector="product_id_discount_percentage">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3 form-group product_id_discount_percentage" style="display: none">
												<label for="order_amount_discount">Producto</label>
												<div class="row">
													<div class="col-xs-12">
														<select name="store_product_id" id="store_product_id" class="form-control">
															<option value=""></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group order_amount_discount" style="display: none">
												<label for="order_amount_discount">Porcentaje de descuento</label>
												<div class="row">
													<div class="col-xs-12">
														<div class="input-group">
															<input type="number" name="order_amount_discount" id="order_amount_discount" class="form-control" placeholder="ingresa el porcentaje de descuento">
															<div class="input-group-addon">%</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group minimum_order_amount" style="display: none">
												<label for="minimum_order_amount">Para pedidos mayores a</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="minimum_order_amount" id="minimum_order_amount" class="form-control" placeholder="Ingresa total mínimo del pedido">
													</div>
												</div>
											</div>
											<div class="col-xs-3 form-group maximum_order_amount" style="display: none">
												<label for="maximum_order_amount">Para pedido menores a</label>
												<div class="row">
													<div class="col-xs-12">
														<input type="number" name="maximum_order_amount" id="maximum_order_amount" class="form-control" placeholder="Ingresa total máximo del pedido">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-2">
											<div class="form-group">
												<button id="save" class="btn btn-primary" type="submit">Guardar</button>
												<button class="btn btn-primary" type="reset">Reset</button>
											</div>
										</div>
									</div>
								</form>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@if ( !empty($discounts) && $admin_permissions['permission1'] )
		<div class="row">
			<div class="col-xs-12">
				<section class="content-header">
					<h4>
						Descuentos activos
					</h4>
				</section>
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<table class="table table-stripped">
							<thead>
								<tr>
									<th>
										Tipo
									</th>
									<th>
										Solo pedidos para mañana
									</th>
									<th>
										Ciudades
									</th>
									<th>
										Tiendas
									</th>
									<th>
										Categorías
									</th>
									<th>
										Subcategorías
									</th>
									<th>
										Producto
									</th>
									<th>
										Descuento
									</th>
									<th>
										Para pedidos mayores a
									</th>
									<th>
										Para pedidos menores a
									</th>
									<th>
										Acción
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($discounts as $discount)
								<tr>
									<td>
										@if ( $discount['type'] == 'free_delivery' )
											Descuento global en domicilio
										@elseif ( $discount['type'] == 'discount_credit' )
											Descuento global en crédito
										@elseif ( $discount['type'] == 'discount_percentage' )
											Descuento global en porcentaje
										@endif
									</td>
									<td>
										{{ $discount['order_for_tomorrow'] ? 'Si' : 'No' }}
									</td>
									@if (!empty($discount['city']))
									<td>
										@foreach ($discount['city'] as $city)
											<h4><span class="label label-primary">{{ $city['city'] }}</span></h4>
										@endforeach
									</td>
									@else
									<td></td>
									@endif

									@if (!empty($discount['store']))
									<td>
										@foreach ($discount['store'] as $store)
											<h4><span class="label label-primary">{{ $store['name'] }}</span></h4>
										@endforeach
									</td>
									@else
									<td></td>
									@endif

									@if (!empty($discount['department']))
									<td>
										{{ $discount['department'] }}
									</td>
									@else
									<td></td>
									@endif

									@if (!empty($discount['shelf']))
									<td>
										{{ $discount['shelf'] }}
									</td>
									@else
									<td></td>
									@endif

									@if (!empty($discount['product']))
									<td>
										{{ $discount['product'] }}
									</td>
									@else
									<td></td>
									@endif

									<td>
										@if ( $discount['type'] == 'discount_percentage' )
											{{ $discount['amount'] }}%
										@else
											${{ number_format($discount['amount'], 0, ',', '.') }}
										@endif
									</td>
									<td>
										${{ number_format($discount['minimum_order_amount'], 0, ',', '.') }}
									</td>
									<td>
										${{ number_format($discount['maximum_order_amount'], 0, ',', '.') }}
									</td>
									<td>
										<button type="button" class="btn btn-danger btn-delete-promo" data-id="{{ $discount['id'] }}">Borrar</button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@endif
		<script>
		</script>
		<div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
	</section>
	<script type="text/javascript">
		var option = 0;

		$('#special_price_expiration_date, #special_price_starting_date').datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
			minDate: moment().format('YYYY-MM-DD HH:mm:ss')
        });

        $("#special_price_starting_date").on("dp.change", function (e) {
            $('#special_price_expiration_date').data("DateTimePicker").minDate(e.date);
        });
        $("#special_price_expiration_date").on("dp.change", function (e) {
            $('#special_price_starting_date').data("DateTimePicker").maxDate(e.date);
        });

		$('#importing').fancybox({
			autoSize    : true,
			closeClick  : false,
			closeBtn    : false,
			openEffect  : 'none',
			closeEffect : 'none',
			helpers   : {
				overlay : {closeClick: false}
			}
		});
		$('#action').change(function () {
			if ( $(this).val() == 0 ) {
				$('.toHide').hide();
			}else{
				$('.toHide').show();
			}
		});
		$('#type').change(function () {
			option = $(this).val();
			$('.percentage, .option-2').hide();
			$('#add-promo-form')[0].reset();
			$('#city_id, #store_id, #department_id, #shelf_id').prop('selectedIndex',0);
			$('.store_id, .department_id, .shelf_id').hide();
			$('#add-free-delivery, #add-promo-form, #add-discount-credit, #add-discount-percentage').slideUp('fast');
            $('.show-on-special-price')[parseInt(option) === 1 ? 'show' : 'hide']();
			if (option == 1) {
				$('#add-promo-form').slideDown('fast');
				$('.percentage').show();
				$('.inputPercentage').removeClass('hide');
				$('.inputFixedPrice').addClass('hide');
				$('#typeImportation').val(1);
			}else if (option == 2){
				$('#add-promo-form').slideDown('fast');
				$('.percentage').show();
				$('.inputFixedPrice').removeClass('hide');
				$('.inputPercentage').addClass('hide');
				$('#typeImportation').val(2);
			}else if (option == 3){
				$('#add-free-delivery').slideDown('fast');
			}else if (option == 4){
				$('#add-discount-credit').slideDown('fast');
			}else if (option == 5){
				$('#add-discount-percentage').slideDown('fast');
			}else{
				$('.percentage, .option-2').hide();
				$('.inputPercentage , .inputFixedPrice').addClass('hide');
				$('#typeImportation').val('');
			}
		});
		$('select[name="city_id"]').change(function () {
			var city_id = $(this).val();
			$('.store_id, .department_id, .shelf_id, .store_free_delivery, .minimum_order_amount').hide();
			$.ajax({
				url: '{{ route('adminPromo.getStoresAjax') }}',
				type: 'GET',
				dataType: 'json',
				data: {city_id: city_id},
				context: this
			})
			.done(function(data) {
				var selector = $(this).data('selector');
				if (selector != undefined) {
					$('.'+selector+' #store_id').empty();
					if (!$('.'+selector+' #store_id').prop('multiple'))
						$('.'+selector+' #store_id').append('<option value=""></option>');
					$('.minimum_order_amount, .maximum_order_amount, .order_amount_discount').show();
					$.each(data, function(index, val) {
						$('.'+selector+' #store_id').append('<option value="'+val.id+'">'+val.name+'</option>')
					});
					$('.'+selector).show();
				}else{
					$('#store_id').empty();
					$('#store_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						$('#store_id').append('<option value="'+value.id+'">'+value.name+'</option>');
					});
					$('.store_id').show();
				}
			})
			.fail(function() {
				console.log("Error no se pueden obtener las tiendas");
			});
		});

		$('select[name="store_id"]').change(function () {
			var store_id = $(this).val();
			$('.department_id, .shelf_id').hide();
			$.ajax({
				url: '{{ route('adminProducts.getDepartmentsAjax') }}',
				type: 'GET',
				dataType: 'json',
				data: {store_id: store_id},
				context: this
			})
			.done(function(data) {
				var selector = $(this).data('selector');
				if ( selector != undefined ) {
					$('.'+selector+' #department_id').empty();
					$('.'+selector+' #department_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						if (value.status == 1) {
							$('.'+selector+' #department_id').append('<option value="'+value.id+'">'+value.name+'</option>');
						}
					});
					$('.'+selector).show();
				}else{
					$('#department_id').empty();
					$('#department_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						if (value.status == 1) {
							$('#department_id').append('<option value="'+value.id+'">'+value.name+'</option>');
						}
					});
					$('.department_id').show();
				}
			})
			.fail(function() {
				console.log("Error al cargar los departamentos.");
			});
		});

		$('select[name="department_id"]').change(function(event) {
			var department_id = $(this).val();
			$('.shelf_id').hide();
			$.ajax({
				url: '{{ route('adminProducts.getShelvesAjax') }}',
				type: 'GET',
				dataType: 'json',
				data: {department_id: department_id},
				context: this
			})
			.done(function(data) {
				var selector = $(this).data('selector');
				if ( selector != undefined ) {
					$('.'+selector+' #shelf_id').empty();
					$('.'+selector+' #shelf_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						if (value.status == 1) {
							$('.'+selector+' #shelf_id').append('<option value="'+value.id+'">'+value.name+'</option>');
						}
					});
					$('.'+selector).show();
				}else{
					$('#shelf_id').empty();
					$('#shelf_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						if (value.status == 1) {
							$('#shelf_id').append('<option value="'+value.id+'">'+value.name+'</option>');
						}
					});
					$('.shelf_id').show();
				}
			})
			.fail(function() {
				console.log("Error al cargar las subcategorías");
			});
		});

		$('select[name="shelf_id"]').change(function(event) {
			var shelf_id = $(this).val();
			$.ajax({
				url: '{{ route('adminProducts.getProductsAjax') }}',
				type: 'GET',
				dataType: 'json',
				data: {shelf_id: shelf_id},
				context: this
			})
			.done(function(data) {
				var selector = $(this).data('selector');
				if ( selector != undefined ) {
					$('.'+selector+' #store_product_id').empty();
					$('.'+selector+' #store_product_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						if (value.status == 1) {
							$('.'+selector+' #store_product_id').append('<option value="'+value.id+'">'+value.name+'</option>');
						}
					});
					$('.'+selector).show();
				}else{
					$('#store_product_id').empty();
					$('#store_product_id').append('<option value=""></option>');
					$.each(data,function(key,value){
						if (value.status == 1) {
							$('#store_product_id').append('<option value="'+value.id+'">'+value.name+'</option>');
						}
					});
					$('.store_product_id').show();
				}
			})
			.fail(function() {
				console.log("Error al cargar las subcategorías");
			});
		});

		var form = $("#add-promo-form").validate({
			rules: {
				city_id:{
					required: function (argument) {
						return $('#csv_file').val() == '';
					}
				},
				store_id:{
					required: function (argument) {
						return $('#csv_file').val() == '';
					}
				},
				quantity_special_price:{
					required: function (argument) {
						return $('#csv_file').val() == '';
					},
					digits: true,
					min: 0
				},
				first_order_special_price:{
					required: function (argument) {
						return $('#csv_file').val() == '';
					}
				},
				percentage:{
					required: function (argument) {
						if(($('#csv_file').val() == '') && ($('#type').val() == 1)){
							return true;
						}else{
							return false;
						}
					},
					number: true,
					min: 0
				},
				fixed_price:{
					required: function (argument) {
						if(($('#csv_file').val() == '') && ($('#type').val() == 2)){
							return true;
						}else{
							return false;
						}
					},
					number: true,
					min: 0
				},
                special_price_starting_date:{
					required: function (argument) {
						return $('#csv_file').val() == '';
					}
				},
                special_price_expiration_date:{
					required: function (argument) {
						return $('#csv_file').val() == '';
					}
				}
			}
		});

		var form_2 = $('#add-free-delivery').validate({
			rules: {
				discount_type: {
					required: true
				},
				city_id: {
					required: true
				},
				'city_id[]': {
					required: true
				},
				store_id: {
					required: true,
				},
				"store_id[]": {
					required: true,
				},
				minimum_order_amount: {
					required: true
				}
			}
		});

		var form_3 = $('#add-discount-credit').validate({
			rules: {
				order_for_tomorrow: {
					required: true
				},
				city_id: {
					required: true
				},
				discount_type: {
					required: true
				},
				minimum_order_amount: {
					required: true
				},
				maximum_order_amount: {
					required: true
				},
				order_amount_discount: {
					required: true
				}
			}
		});

		var form_4 = $('#add-discount-percentage').validate({
			rules: {
				order_for_tomorrow: {
					required: true
				},
				city_id: {
					required: true
				},
				discount_type: {
					required: true
				},
				minimum_order_amount: {
					required: true
				},
				maximum_order_amount: {
					required: true
				},
				order_amount_discount: {
					required: true
				}
			}
		});

	    $('#save').on('click',function(e){
	    	if (confirm('¿Estas seguro que deseas aplicar los cambios?'))
		    {
		    	$("#add-promo-form").submit(function () {
				if (form.valid()) {
						$.fancybox($('#importing'));
					}
				});

		    }else{
		    	return false;
		    }
	    });

	    $('#discount_type').change(function(event) {
	    	var type = $(this).val();
    		$('#city_id_free_delivery_opt_1, #city_id_free_delivery_opt_2, #minimum_order_amount').val('');
    		$('.discount_type_option_1 select, .discount_type_option_1 input').prop('disabled', true);
    		$('.discount_type_option_2 select, .discount_type_option_2 input').prop('disabled', true);
    		$("#store_id_free_delivery option:selected").prop("selected", false);
    		$('.store_free_delivery, .minimum_order_amount').hide();
	    	if (type == 1) {
	    		$('.discount_type_option_1 select, .discount_type_option_1 input').prop('disabled', false);
    			$('.discount_type_option_1').slideDown();
    			$('.discount_type_option_2').slideUp();
	    	}else if(type == 2){
	    		$('.discount_type_option_2 select, .discount_type_option_2 input').prop('disabled', false);
	    		$('.discount_type_option_1').slideUp();
	    		$('.discount_type_option_2').slideDown();
	    	}else{
    			$('.discount_type_option_1').slideUp();
    			$('.discount_type_option_2').slideUp();
	    	}
	    });

	    $('#discount_credit_type').change(function (event) {
	    	var type = $(this).val();
	    	$('.discount_credit_type, .store_discount_credit, .minimum_order_amount, .maximum_order_amount').slideUp();
	    	if (type == 1) {
	    		$('.discount_credit_type').slideDown();
	    	}
	    });

	    $('body').on('click', '.btn-delete-promo', function(event) {
	    	var confirm = window.confirm('¿Confirmas que deseas borrar el descuento?');
	    	if (confirm == false) {
	    		return;
	    	}
	    	var id = $(this).data('id');
	    	$.ajax({
	    		url: '{{ route('adminPromo.deleteDiscount') }}',
	    		type: 'POST',
	    		dataType: 'json',
	    		data: {id: id},
	    		context: this
	    	})
	    	.done(function(data) {
	    		if (data.success) {
	    			$(this).parent().parent().remove();
	    		}
	    	})
	    	.fail(function(data) {
	    		console.log('Ocurrió un error al borrar los datos.');
	    	});

	    });
	</script>
	<style type="text/css">
		.fancybox-close { display: none; }
	</style>
@endsection
