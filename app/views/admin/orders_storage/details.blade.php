@if (!Request::ajax())
    @extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <!--<link rel="stylesheet" type="text/css" href="{{asset_url()}}css/style.css">-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        var delivery_times;
    </script>
    <style type="text/css">
        #modal-credit-note .modal-dialog, #modal-products .modal-dialog, #modal-order-product-group .modal-dialog {
            width: 70% !important;
        }

        #modal-products .error {
            color: #ff0000;
            font-size: 13px;
            display: none;
        }

        #map-point {
            width: 100%;
            height: 400px;
            background-color: grey;
        }
        .right-side > .Header h1 {
            font-size: 24px;
            margin: 0;
        }
        .buttons-container .btn {
            margin-bottom: 1rem;
        }
    </style>
    <section class="content-header Header">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-4">
                <h1>
                    {{ $title }} # {{ $order->id }}
                    <small>Control panel</small>
                </h1>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-8 buttons-container">
                @if (($admin_permissions['update'] && is_null($order->revision_user_id)) || $admin_permissions['permission2'])
                    @if ($order->status == 'Enrutado')
                    <a href="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'In Progress']) }}">
                        <button type="button" class="btn btn-success" onclick="return confirm('¿Estas seguro que deseas cambiar el estado a En proceso de alistamiento?')">
                            Comenzar alistamiento
                        </button>
                    </a>
                    @endif
                    @if($order->status == 'Alistado')
                    <a href="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Dispatched']) }}" onclick="return confirm('¿Estas seguro que deseas cambiar el estado a Despachado?')">
                        <button type="button" class="btn btn-success">Pedido Despachado</button>
                    </a>
                    @endif
                    @if($order->status == 'Validation')
                    <a href="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Initiated']) }}"
                               onclick="return confirm('¿Estas seguro que deseas cambiar el estado a Iniciado?')">
                        <button type="button" class="btn btn-success">Pedido Iniciado</button>
                    </a>
                    @endif
                    @if($order->status == 'Initiated')
                    <a href="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Enrutado']) }}"
                               onclick="return confirm('¿Estas seguro que deseas cambiar el estado a Enrutado?')">
                        <button type="button" class="btn btn-success">Pedido Enrutado</button>
                    </a>
                    @endif

                    @if(in_array($order->status, ['Alistado', 'Dispatched', 'Delivered']) && $order->payment_method == 'Tarjeta de crédito' && empty($last_order_payment) && ($order->total_amount + $order->delivery_amount - $order->discount_amount) > 0 && (empty($last_refund) OR $last_refund->cc_refund_status == 'Aprobada') )
                    <a href="{{ route('adminOrderStorage.saveCharge', ['id' => $order->id]) }}"
                               onclick="return confirm('¿Estas seguro que deseas cobrar el total a la tarjeta de crédito?')">
                        <button type="button" class="btn btn-success">Realizar Cobro a Tarjeta</button>
                    </a>
                    @endif
                    @if($order->status == 'In Progress')
                    <a href="javascript:;" onclick="show_modal('order-baskets-bags')">
                        <button type="button" class="btn btn-success">Pedido Alistado</button>
                    </a>
                    @endif
                    @if($order->status == 'Dispatched')
                        @if($order->payment_method == 'Tarjeta de crédito' || $order->payment_method == 'Débito - PSE')
                            <a href="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Delivered']) }}"
                               onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Entregado?')">
                                <button type="button" class="btn btn-success">Pedido Entregado</button>
                            </a>
                        @else
                        <button type="button" class="btn btn-success"
                            onclick="show_modal('delivered')">Pedido Entregado</button>
                        @endif
                    @endif
                    @if((!$order->invoice_number && $order->status != 'Cancelled' && $admin_permissions['permission3']) || ($order->status == 'Cancelled' && $order->temporarily_cancelled))
                    <a href="#" data-id="{{ $order->id }}">
                        <button type="button" class="btn btn-danger" onclick="show_modal('reject-order')">Pedido Cancelado</button>
                    </a>
                    @endif
                    @if($order->status == 'Enrutado' || $order->status == 'Initiated' || $order->status == 'Validation')
                    <a href="#" onclick="show_modal('products')">
                        <button type="button" class="btn btn-primary">Agregar Producto</button>
                    </a>
                    @endif
                    <a href="#" onclick="show_modal('user-credits')">
                        <button type="button" class="btn btn-primary">Agregar Crédito</button>
                    </a>
                    @if(!in_array($order->status, ['Dispatched', 'Delivered', 'Cancelled']) || ($order->status == 'Dispatched' && $order->payment_method != 'Tarjeta de crédito') || ($order->status == 'Dispatched' && $order->payment_method == 'Tarjeta de crédito' && empty($last_order_payment->cc_charge_id)))
                    <div class="btn btn-primary" onclick="show_modal('add-coupon')">Agregar Cupón</div>
                    @endif
                    @if($order->status != 'Initiated')
                        @if ($order->revision_orders_required && $order->status == 'Delivered' && !$order->is_checked && $admin_permissions['delete'])
                            <a href="{{ route('adminOrderStorage.updateRevisionChecked', ['id' => $order->id]) }}"
                               onclick="return confirm('¿Estas seguro que deseas marcar como revisado el pedido?')">
                                <button type="button" class="btn btn-primary">Pedido Revisado</button>
                            </a>
                        @endif
                        @if($order->status == 'Cancelled' && $order->temporarily_cancelled == 1)
                            <div class="btn btn-primary" onclick="show_modal('reschedule-order')">Reprogramar Pedido</div>
                        @else
                            @if ((!in_array($order->status, ['Alistado', 'Dispatched', 'Delivered']) && $order->num_child_orders == 0))
                                <div class="btn btn-primary" onclick="show_modal('change-status')">Cambiar Estado</div>
                            @endif
                        @endif
                    @endif
                    @if ($admin_permissions['update'] && ($order->payment_method == 'Tarjeta de crédito' || $order->payment_method == 'Datáfono') && !empty($last_order_payment) && empty($last_order_payment->cc_refund_date))
                    <div class="btn btn-warning" onclick="show_modal('refund', {{ $last_order_payment->id }})">Reembolsar Cobro a Tarjeta</div>
                    @endif
                    @if($order->status == 'Cancelled' && $admin_permissions['permission3'] && !$order->temporarily_cancelled && $order->order_return_id && !$order->invoice_number)
                    <a href="{{ route('adminOrderStorage.reverseStatus', ['id' => $order->id]) }}"
                           onclick="return confirm('¿Estas seguro que deseas reversar el estado del pedido?')">
                        <button type="button" class="btn btn-danger">Reversar Estado</button>
                    </a>
                    @endif
                @endif
                @if ( $admin_permissions['update'] && $order->status == 'Delivered' && $order->credit_note_number)
                <a href="{{ route('adminOrderComplaint.orderComplaint', ['id' => $order->id]) }}">
                    <button type="button" class="btn btn-primary">Generar Pedido de Reclamo</button>
                </a>
                @endif
                @if($order->status == 'Delivered' && $admin_permissions['update'] && $admin_permissions['permission2'] && $order->invoice_number)
                    <a href="#" onclick="show_modal('credit-note')">
                        <button type="button" class="btn btn-success">Agregar Nota Crédito</button>
                    </a>
                @endif
                <a href="#" onclick="show_modal('note')">
                    <button type="button" class="btn btn-primary">Agregar Nota</button>
                </a>
            </div>
        </div>
    </section>

    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif
        @if(Session::has('warning') )
            <div class="alert alert-warning">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('warning') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12 col-md-3 col-lg-3">
                <div class="callout callout-info">
                    <h4>Datos de Pedido</h4>
                    @if ($order->child_order_id || $order->parent_order_id)
                        <p>
                            <label>Pedido - </label>
                            @if ($order->child_order_id)
                                (hijo - <a target="_blank"
                                           href="{{ route('adminOrderStorage.details', ['id' => $order->child_order_id]) }}">{{ $order->child_order_id }}</a>
                                )
                            @endif
                            @if ($order->parent_order_id)
                                (padre - <a target="_blank"
                                            href="{{ route('adminOrderStorage.details', ['id' => $order->parent_order_id]) }}">{{ $order->parent_order_id }}</a>
                                )
                            @endif
                        </p>
                    @endif
                    <p><label>Estado</label> -
                        <b>{{ $order->status }} @if($order->status == 'Cancelled' && $order->temporarily_cancelled)
                                (Temporalmente) @endif</b></p>
                    <p><label>Referencia</label> - {{ $order->reference }}</p>
                    @if (!empty($order->invoice_number))
                        <p><label>Número factura</label> - {{ $order->invoice_number }}
                            <a href="javascript:;" data-reference="{{ $order->id }}"
                               data-href="{{ route('adminOrderStorage.detailsViewInvoice', ['id' => $order->id]) }}"
                               class="view-invoice">Ver factura</a>
                            - <a href="javascript:;" class="modal-invoice">Actualizar factura</a>
                        </p>
                    @endif
                    <p><label>Origen</label> - {{ $order->type }}
                        / {{ $order->source }} {{ $order->source_os }} {{ $order->app_version }} {{ $order->user_device }}
                        / {{ $order->ip }}</p>
                    <p><label>Tienda</label> - {{ $order->store_name }}</p>
                    @if (!empty($order->orderGroup->warehouse->warehouse))
                        <p><label>Bodega</label> - {{ $order->orderGroup->warehouse->warehouse }}</p>
                    @endif
                    @if ($order->allied_store_name != '')
                        <p><label>Tienda aliada</label> - {{ $order->allied_store_name }}</p>
                    @endif
                    <p><label>Fecha de creación</label> - {{ date("d M Y g:i a",strtotime($order->date)) }}</p>
                    @if ($order->posible_fraud)
                        <p class="orders-posible_fraud">{{ $order->posible_fraud }}</p>
                    @elseif ($order->suspect)
                        <p class="orders-suspect"><label>Posible fraude</label> - {{ $order->suspect }}</p>
                    @endif
                    <p>
                        <label>Fecha de entrega</label>
                        - {{ date("d M Y g:i a", strtotime($order->customer_delivery_date)) }}
                        ({{ $order->delivery_time }})
                        @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <a href="#" onclick="show_modal('delivery-date')">Editar</a>
                        @endif
                    </p>
                    @if(!empty($order->management_date))
                        <p><label>Fecha gestión</label> - {{ date("d M Y g:i a",strtotime($order->management_date)) }}
                        </p>
                    @endif
                    @if(!empty($order->complaint_reason))
                        <p><label>Motivo devolución</label> - {{ $order->complaint_reason }}</p>
                    @endif

                    <p><label>Revisado</label> - @if($order->is_checked) Sí @else No @endif</p>
                    @if(!empty($order->campaign))
                        <p><label>Campaña</label> - {{ ucfirst($order->campaign) }}</p>
                    @endif

                    @if($order->status == 'Cancelled')
                        <p><label>Motivo de cancelación</label> - {{ $order->reject_reason }}
                            - @if($order->product_return_storage) con devolucíon @else sin devolucíon @endif</p>
                        @if($order->arrival_image_url && $order->reject_reason == 'Cancelado temporalmente - Cliente - El cliente no se encuentra en la dirección indicada en horario indicado')
                            <p><a href="#" onclick="show_modal('arrival')">Ver imagen del motivo de cancelación</a>
                            </p> @endif

                        @if(!empty($order->reject_comments))
                            <p style="white-space: pre-line;"><label>Comentarios de cancelación</label>
                                - {{ $order->reject_comments }}</p>
                        @endif
                    @endif

                    @if(!empty($order->driver_cancel_date))
                        <div style="background-color: #ffdbdb">
                            <p><label>Fecha de cancelación conductor</label>
                                - {{ date("d M Y g:i a",strtotime($order->driver_cancel_date)) }}</p>
                            <p><label>Motivo de cancelación conductor</label> - {{ $order->reject_reason }}</p>
                            <p><label>Comentarios de cancelación conductor</label> - {{ $order->reject_comments }}</p>
                        </div>
                    @endif

                    <p><a href="#" title="Log de pedido" onclick="show_modal('orders_log')">Log de pedido</a></p>
                    <p><a href="#" title="Notas de pedido" onclick="show_modal('notes')">Notas de pedido</a></p>
                </div>
                <div class="callout callout-info">
                    <h4>Datos de Promotor de Referido</h4>
                    @if(!is_null($user_promoter))
                        <p><label>Id</label> - {{ $user_promoter->id }}</p>
                        <p><label>Nombre</label> - {{ $user_promoter->first_name.' '.$user_promoter->last_name }}</p>
                        <p><label>Teléfono</label> - {{ $user_promoter->phone }}</p>
                        <p><label>Correo</label> - {{ $user_promoter->email }}</p>
                        <p><label>Dirección</label>
                            - {{ $user_promoter->user_address.' '.$user_promoter->user_address_further }}</p>
                        <p><a href="#" onclick="show_modal('promoter-orders', {{ $user_promoter->id }})">Historial de
                                pedidos del promotor</a></p>
                        <p><a href="#" onclick="show_modal('referrals-of-promoter', {{ $user_promoter->id }})">Información
                                de referidos del promotor</a></p>
                    @else
                        <p><label>El usuario no tiene promotor</label></p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3">
                <div class="callout callout-info">
                    <h4>Datos de Pago</h4>
                    <p><label>Método de pago</label> - {{ $order->payment_method }}
                        @if ($order->status == 'Delivered' && $order->payment_method == 'Efectivo y datáfono')(Efectivo:
                        ${{ number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount) - $order->user_card_paid, 0, ',', '.') }}
                        Datáfono: ${{ number_format($order->user_card_paid, 0, ',', '.') }}) @endif
                        @if($order->payment_method == 'Tarjeta de crédito')
                            @if(strlen($order->cc_token) == Config::get('app.payu.token_length'))
                                (Online PayU)
                            @else
                                (Online TPAGA)
                            @endif
                        @endif
                        @if(($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled') || ($admin_permissions['permission2'] && $order->status == 'Delivered' && !$order->is_checked))
                            <a href="#" onclick="show_modal('payment-method')">Editar</a>
                        @endif
                        <br><a href="#" onclick="show_modal('order-payments')">Ver transacciones</a>
                    </p>
                    @if ($order->payment_method == 'Tarjeta de crédito' && $order->cc_token != '')
                        <p><label>Tarjeta de crédito</label> - {{ $order->cc_bin }}
                            **** {{ $order->cc_last_four.' '.$order->cc_type.' ('.$order->cc_country.')' }}</p>
                        @if (!empty($order->cc_expiration_month))
                            <p><label>Vencimiento</label> - {{ $order->cc_expiration_month }}
                                /{{ $order->cc_expiration_year }}</p>
                        @endif
                        @if (!empty($order->cc_holder_name))
                            <p><label>Propietario</label> - {{ $order->cc_holder_name }}</p>
                        @endif
                        @if (!empty($order->cc_holder_document_type))
                            <p><label>Tipo documento</label> - {{ $order->cc_holder_document_type }}</p>
                        @endif
                        @if (!empty($order->cc_holder_document_number))
                            <p><label>Nro. documento</label> - {{ $order->cc_holder_document_number }}</p>
                        @endif
                        <p><label>Token</label> - {{ $order->cc_token }}</p>
                    @endif
                    <p><label>Estado de pago</label> -
                        @if(empty($order->payment_date))
                            @if($order->payment_method != 'Débito - PSE')
                                @if($order->status != 'Delivered') Pendiente
                                @else <font color="red"><b>No pagado</b></font>
                                @if ($order->payment_method != 'Tarjeta de crédito' && $admin_permissions['permission1'])
                                    <a href="{{ route('adminOrderStorage.updateOrderPaymentStatus', ['id' => $order->id, 'status' => 1]) }}"
                                       onclick="return confirm('¿Estas seguro que deseas actualizar el pedido?')">Actualizar
                                        a pagado</a>
                                @endif
                                @endif
                            @else
                                @if(isset($order_payments[0]) && $order_payments[0]->cc_payment_status == 'Pendiente') Pendiente
                                @else No pagado
                                @endif
                            @endif
                        @else Pagado @if (!empty($last_order_payment->cc_refund_date)) (Reembolso enviado
                        - {{ $last_order_payment->cc_refund_status }}) @endif
                        @if ($order->payment_method != 'Tarjeta de crédito' && $admin_permissions['permission1'] && !$order->is_checked)
                            <a href="{{ route('adminOrderStorage.updateOrderPaymentStatus', ['id' => $order->id, 'status' => 0]) }}"
                               onclick="return confirm('¿Estas seguro que deseas actualizar el pedido?')">Actualizar a
                                no pagado</a>
                        @endif
                        @endif
                    </p>
                    @if($order->payment_date != '')
                        <p><label>Fecha de pago</label> - {{ date("d M Y h:i a", strtotime($order->payment_date)) }}
                        </p>
                    @endif
                    <p><label>Subtotal</label> - <span
                                id="total_amount">${{ number_format($order->total_amount, 0, ',', '.') }}</span></p>
                    <p><label>Domicilio</label> - <span
                                id="delivery_amount">${{ number_format($order->delivery_amount, 0, ',', '.') }}</span>
                        @if($admin_permissions['update'] && $order->delivery_amount && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <a href="{{ route('adminOrderStorage.deleteDeliveryAmount', ['id' => $order->id]) }}"
                               onclick="return confirm('¿Estas seguro que deseas eliminar el valor del domicilio del pedido?')">Eliminar</a>
                        @endif
                    </p>
                    <p><label>Descuento</label> - <span
                                id="discount_amount">${{ number_format($order->discount_amount, 0, ',', '.') }}</span> @if($order->discount_percentage_amount)
                            ({{ $order->discount_percentage_amount }}%) @endif
                        @if($admin_permissions['update'] && $order->discount_amount && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <a href="{{ route('adminOrderStorage.deleteDiscount', ['id' => $order->id]) }}"
                               onclick="return confirm('¿Estas seguro que deseas eliminar el descuento del pedido?')">Eliminar</a>
                        @endif
                    </p>

                    @if(($notAvailableProductsTotalSum = $order->orderProductsNotAvailable->sum('total')) > 0 && ! $order->hasMissingProductsRefundLog() && $order->payment_method === 'Débito - PSE')
                    <p style="color: red; font-size: 120%; margin-right:10px;">
                    <strong>Total faltantes: ${{ number_format($notAvailableProductsTotalSum, 0, ',', '.') }}</strong>
                    </p>
                    @endif
                    
                    {{-- área que notifica si se debe reembolsar dinero por faltantes a pedido --}}
                    @if($order->payment_method === 'Débito - PSE' && $notAvailableProductsTotalSum > 0 && $order->order_validation_reason_id === 6)
                    <p style="display:inline;">
                        <form action="{{ route('adminOrderStorage.deleteNotAvailableProducts', ['id' => $order->id] ) }}" method="POST" style="display:inline;">
                            <button onClick="confirm('¿Estás seguro de REGISTRAR COMO REEMBOLSADO EL DINERO DE LOS PRODUCTOS FALTANTES? Esta acción no se puede deshacer.') ? event.target.closest('form').submit() : null" type="button" class="btn btn-primary" aria-label="Left Align" title="Marcar dinero como reembolsado">
                                <span>Registrar reembolso de faltantes</span>
                            </button>
                        </form>
                    </p>
                    @endif

                    @if (!empty($order->coupon_code))
                        <p><label>Cupón</label> - {{ $order->coupon_code }}</p>
                    @endif
                    <p><label>Total</label> - <b><span
                                    id="total">${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}</span></b>
                    </p>
                    <div id="total_credit_note" style="display: none;">
                    </div>
                    @if($admin_permissions['update'] && ($order->payment_method == 'Datáfono' || $order->payment_method == 'Efectivo y datáfono') && $order->status != 'Validation' && $order->status != 'Initiated')
                        <p><a href="#" onclick="show_modal('voucher')">Ver/Subir voucher</a></p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3">
                <div class="callout callout-info">
                    <h4>Datos de Cliente</h4>
                    <p><label>Nombre</label> - {{ $order->user_firstname.' '.$order->user_lastname }}</p>
                    <p><label>User id</label> - {{ $order->user_id }}</p>
                    <p><label>Celular</label> - {{ $order->user_phone }}</p>
                    <p><label>Email</label> - {{ $order->user_email }}</p>
                    <p><label>Dirección</label> - <b>{{ $order->user_address.' '.$order->user_address_further }}</b>
                        @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <a href="#" onclick="show_modal('delivery-address')">Editar</a> |
                        @endif
                        <a href="#" onclick="show_modal('map-coords-user')">Ver mapa</a> |
                        <a href="#" onclick="show_modal('orders-per-address', '{{ $order->id }}')">Pedidos en esta
                            dirección</a>
                    </p>
                    @if (!empty($order->user_address_neighborhood))
                        <p><label>Barrio</label> - {{ $order->user_address_neighborhood }}</p>
                    @endif
                    @if(!empty($order->zone))
                        <p><label>Zona</label> - {{ $order->zone }}</p>
                    @endif
                    @if(!empty($order->locality))
                        <p><label>Localidad</label> - {{ $order->locality }}</p>
                    @endif
                    <p><label>Ciudad</label> - {{ $order->city }}</p>

                    @if (!empty($order->user_score_date))
                        <p><label>Calificación de pedido</label> -
                            @if(!$order->user_score)
                                <a href="#" title="Calificación de pedido" onclick="show_modal('score')">Negativa</a>
                            @else
                                Positiva
                    @endif
                    @endif
                    @if ($admin_permissions['update'])
                        <p><a href="#" onclick="show_modal('credits')">Log de movimientos de crédito</a></p>
                        <p><a href="#" onclick="show_modal('free-delivery-log')">Log de domicilio gratis</a></p>
                        <p><a href="#" onclick="show_modal('orders')">Historial de pedidos</a></p>
                        <p><a href="#" onclick="show_modal('referrals-this-client', {{ $order->user_id }})">Información
                                de referidos del cliente</a></p>
                        @if( ($order->order_validation_reason_id == 3 && ($order->status != 'Cancelled' && $order->status != 'Delivered') )
                                || $order->order_validation_reason_id == 5 || $order->order_validation_reason_id == 7 )
                            <p>
                                <a href="javascript:void(0)" onclick="show_modal('management-sac', {{ $order->id }})">
                                    SAC: Gestionar pedido
                                    <span class="label label-success"><i class="glyphicon glyphicon-earphone"></i></span>
                                </a>
                            </p>
                        @endif
                        @if($order->status == 'Dispatched' || $order->status == 'In Progress')
                            <p>
                                <button type="button" class="btn btn-primary" id="send-sms-action">Enviar mensaje de
                                    texto
                                </button>
                            </p>
                        @endif
                        @if($order->status == 'Delivered')
                            <p>
                            <form method="POST"
                                  action="{{ route('adminOrderStorage.reSendDeliveryEmail', ['id' => $order->id]) }}"
                                  class="form-horizontal" id="re-send-email">
                                <button type="submit" class="btn btn-success">Reenviar correo de pedido entregado
                                </button>
                            </form>
                            </p>
                        @endif
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3">
                @if ($order->status != 'Validation')
                    <div class="callout callout-info">
                        <h4>Datos de Transportador</h4>
                        @if (!empty($order->route) || !empty($order->planning_route))
                            <p><label>Ruta
                                    - </label>@if (!empty($order->route)) {{ $order->route }} @else {{ $order->planning_route }} @endif
                            </p>
                            <p><label>Secuencia - </label> {{ $order->planning_sequence }}</p>
                        @endif
                        @if ($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <p>
                            <div class="btn btn-primary" onclick="show_modal('ruta')">Asignar ruta</div></p>
                        @endif
                        @if($order_transporter)
                            <p><label>Transportador -</label> {{ $order->vehicle->transporter->fullname }}</p>
                            <p><label>Conductor
                                    -</label> {{ $order_transporter->driver_first_name }} {{ $order_transporter->driver_last_name }}
                            </p>
                            <p><label>Conductor celular - </label> {{ $order_transporter->driver_phone }}</p>
                            <p><label>Placa del vehículo - </label> {{ $order->vehicle->plate }}</p>
                            <p><label>Fecha de asignación
                                    - </label> {{ date("d M Y h:i a", strtotime($order->planning_date)) }}</p>
                            @if(!empty($order->dispatched_date))
                                <p><label>Fecha de despacho</label>
                                    - {{ date("d M Y h:i a", strtotime($order->dispatched_date)) }}</p>
                            @endif
                            @if(!empty($order->ontheway_notification_date))
                                <p><label>Fecha notificación en camino</label>
                                    - {{ date("d M Y h:i a", strtotime($order->ontheway_notification_date)) }}</p>
                            @endif
                            @if(!empty($order->onmyway_date))
                                <p><label>Fecha en camino</label>
                                    - {{ date("d M Y h:i a", strtotime($order->onmyway_date)) }}</p>
                            @endif
                            @if(!empty($order->arrived_date))
                                <p><label>Fecha de llegada</label>
                                    - {{ date("d M Y h:i a", strtotime($order->arrived_date)) }}</p>
                            @endif
                        @endif
                        @if ($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <p>
                            <div class="btn btn-primary" onclick="show_modal('transporter')">Asignar transportador
                            </div></p>
                        @endif
                        <p><label>Entrega anticipada - </label> @if(!$order->allow_early_delivery) No permitida @else
                                Permitida @endif</p>
                        @if ($order->status == 'Dispatched' && empty($order->onmyway_date) && !$order->allow_early_delivery)
                            <p><a href="{{ route('adminOrderStorage.allowEarlyDelivery', ['id' => $order->id]) }}"
                                  class="btn btn-primary"
                                  onclick="return confirm('¿Estas seguro que deseas permitir la entrega del pedido antes de tiempo?')">Permitir
                                    entrega anticipada</a></p>
                        @endif
                    </div>
                @endif

                <div class="callout callout-info">
                    <h4>Datos de Alistador</h4>
                    @if (!empty($order->picking_sequence_letter))
                        <p><label>Letra para alistamiento</label> - {{ $order->picking_sequence_letter }}</p>
                    @endif
                    @if (!empty($order->picking_group))
                        <p><label>Grupo de alistamiento</label> - {{ $order->picking_group }}</p>
                    @endif
                    @if (!empty($order->picker_dry_name) || !empty($order->picker_cold_name))
                        @if (!empty($order->picker_dry_name))
                            <p><label>Nombre alistador seco</label> - {{ $order->picker_dry_name }}</p>
                        @endif
                        @if(!empty($order->picker_cold_name))
                            <p><label>Nombre alistador frío</label> - {{ $order->picker_cold_name }}</p>
                        @endif
                        <p><label>Fecha de asignación</label>
                            - {{ date("d M Y h:i a", strtotime($order->allocated_date)) }}</p>
                        @if(!empty($order->picking_dry_start_date))
                            <p><label>Fecha de alistamiento seco</label> -
                                Comenzó: {{ date("d M Y h:i a", strtotime($order->picking_dry_start_date)) }}
                                @if(!empty($order->picking_dry_end_date))
                                    Terminó: {{ date("d M Y h:i a", strtotime($order->picking_dry_end_date)) }} @endif
                            </p>
                        @endif
                        @if(!empty($order->picking_cold_start_date))
                            <p><label>Fecha de alistamiento frío</label> -
                                Comenzó: {{ date("d M Y h:i a", strtotime($order->picking_cold_start_date)) }}
                                @if(!empty($order->picking_cold_end_date))
                                    Terminó: {{ date("d M Y h:i a", strtotime($order->picking_cold_end_date)) }} @endif
                            </p>
                        @endif
                        @if(!empty($order->picking_date))
                            <p><label>Fecha de alistamiento</label>
                                - {{ date("d M Y h:i a", strtotime($order->picking_date)) }}</p>
                        @endif
                        @if(!empty($order->order_picking_quality_id))
                            <p><label>Fecha de validación</label>
                                - {{ date("d M Y h:i a", strtotime($order->validation_date)) }}@if($order->status == 'In Progress')
                                    (<font color="red"><b>Pendiente surtido</b></font>)@endif</p>
                        @endif
                        @if(!empty($order->picking_bags))
                            <p><label>Bolsas frío</label> - {{ $order->picking_bags }}</p>
                        @endif
                        @if(!empty($order->picking_baskets))
                            <p><label>Canastillas</label> - {{ $order->picking_baskets }}</p>
                        @endif
                        @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <div class="btn btn-primary" onclick="show_modal('picker')">Reasignar</div>
                        @endif
                    @else
                        @if ($admin_permissions['update'])
                            <div class="btn btn-primary" onclick="show_modal('picker')">Asignar</div>
                        @endif
                    @endif
                </div>
            </div>

            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <table id="shelves-table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Referencia</th>
                                <th>Nombre</th>
                                <th>Unidad de medida</th>
                                <th>Cant. Solicitada</th>
                                <th>Cant. Alistada</th>
                                <th>Precio unitario</th>
                                <th>Precio original</th>
                                <th>Total</th>
                                <th>Estado</th>
                                @if($admin_permissions['update'] && !$order->invoice_number)
                                    <th>Acción</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            <?php $assigned_gifts = []; ?>
                            @foreach($order_products as $product)
                                <tr>
                                    <td align="center">
                                        <img src="{{ $product->product_image_url }}" height="50px">
                                    </td>
                                    <td>{{ $product->reference }}</td>
                                    <td>{{ $product->product_name }}
                                        @if ($product->type == 'Agrupado')<p><a href="javascript:;" title="Productos"
                                                                                onclick="show_modal('order-product-group', {{ $product->id }})">Ver
                                                productos</a></p>@endif
                                        @if($product->type == 'Recogida') <p style="color: #de3131;">Producto para
                                            recoger</p> @endif
                                    </td>
                                    <td>{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                                    <td align="right">{{ $product->quantity_original }}</td>
                                    <td align="right">{{ $product->quantity }}@if(!empty($product->quantity_returned))
                                            <br>(Devolución: {{ $product->quantity_returned }}<br>
                                            Motivo: {{ $product->reason_returned }})@endif</td>
                                    <td align="right">${{ number_format($product->price, 0, ',', '.') }}</td>
                                    <td align="right">@if ($product->price != $product->original_price)
                                            <strike>${{ number_format($product->original_price, 0, ',', '.') }}</strike>@else
                                            ${{ number_format($product->original_price, 0, ',', '.') }} @endif</td>
                                    <td align="right">
                                        ${{ number_format($product->quantity * $product->price, 0, ',', '.') }}</td>
                                    <td align="center" id="status-{{ $product->id }}">
                                        @if($product->fulfilment_status == 'Fullfilled')
                                            <span class="badge bg-green">Disponible</span>
                                        @elseif($product->fulfilment_status == 'Not Available')
                                            <span class="badge bg-red">No disponible</span>
                                        @elseif($product->fulfilment_status == 'Pending')
                                            <span class="badge bg-orange">Pendiente</span>
                                        @elseif($product->fulfilment_status == 'Missing')
                                            <span class="badge bg-maroon">Faltante</span>
                                        @elseif($product->fulfilment_status == 'Returned')
                                            <span class="badge bg-red">Devolución</span>
                                        @else
                                            <span class="badge bg-blue">Reemplazado</span>
                                        @endif
                                    </td>
                                    @if($admin_permissions['update'] && !$order->invoice_number)
                                        <td>
                                            @if ($product->type == 'Agrupado')
                                                @if ( in_array($order->status, ['Initiated', 'Validation', 'Enrutado']) && $admin_permissions['update'] )
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a class="remove-product"
                                                                   href="{{ route('adminOrderStorage.removeProduct', ['order_id' => $order->id, 'product_id' => $product->id]) }}">
                                                                    Eliminar
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @else
                                                    <div class="btn-group">
                                                        <a href="#" title="Productos"
                                                           onclick="show_modal('order-product-group', {{ $product->id }})">
                                                            <button type="button" class="btn btn-danger dropdown-toggle"
                                                                    data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            @else
                                                @if(in_array($order->status, ['In Progress', 'Dispatched']) || ($order->status == 'Delivered' && $admin_permissions['permission2']))
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            @if ($product->type != 'Recogida')
                                                                <li><a class="update-product" href="javascript:;"
                                                                       data-product="{{ $product->id }}"
                                                                       data-status="{{ 'Fullfilled' }}"
                                                                       data-type="{{ $product->type }}">Disponible</a>
                                                                </li>
                                                                <li><a class="update-product" href="javascript:;"
                                                                       data-product="{{ $product->id }}"
                                                                       data-status="{{ 'Not Available' }}"
                                                                       data-type="{{ $product->type }}">No
                                                                        disponible</a></li>
                                                            @endif
                                                            @if ($order->status == 'In Progress')
                                                                <li><a class="update-product" href="javascript:;"
                                                                       data-product="{{ $product->id }}"
                                                                       data-status="{{ 'Missing' }}"
                                                                       data-type="{{ $product->type }}">Faltante</a>
                                                                </li>
                                                            @endif
                                                            @if ($order->status == 'Dispatched' || ($order->status == 'Delivered' && $admin_permissions['permission2']))
                                                                <li>
                                                                    <a onclick="return_product({{ $order->id }} ,{{ $product->id }}, '{{ $product->type }}')"
                                                                       href="#">Devolución</a></li>
                                                            @endif
                                                            @if ($order->status != 'Alistado' )
                                                                <div class="divider"></div>
                                                                <li><a href="#"
                                                                       onclick="edit({{ $product->id }}, {{ $product->quantity }});">Editar
                                                                        cantidad</a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                @elseif( in_array($order->status, ['Initiated', 'Validation', 'Enrutado']) &&  $admin_permissions['update'] )
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a class="remove-product"
                                                                   href="{{ route('adminOrderStorage.removeProduct', ['order_id' => $order->id, 'order_product_id' => $product->id]) }}">
                                                                    Eliminar
                                                                </a>
                                                            </li>
                                                            <div class="divider"></div>
                                                            <li><a href="#"
                                                                   onclick="edit({{ $product->id }}, {{ $product->quantity }});">Editar
                                                                    cantidad</a></li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                                @if(isset($product->gift))
                                    @if(empty($assigned_gifts[$product->store_product_id]))
                                        <?php $assigned_gifts[$product->store_product_id] = 1; ?>
                                        <tr>
                                            <td align="center">
                                                <img src="{{ $product->gift->image_url }}" height="50px">&nbsp;&nbsp;<img
                                                        src="{{ web_url() }}/admin_asset/img/gift.png" height="20px"
                                                        title="Es un regalo">
                                            </td>
                                            <td>{{ $product->gift->reference }}</td>
                                            <td>{{ $product->gift->name }}
                                            <td>{{ $product->gift->quantity }} {{ $product->gift->unit }}</td>
                                            <td align="right">{{ $product->gift->quantity_cart }}</td>
                                            <td align="right">${{ number_format($product->gift->price, 0, ',', '.') }}</td>
                                            <td align="right">${{ number_format($product->gift->price, 0, ',', '.') }}</td>
                                            <td align="right">
                                                ${{ number_format($product->gift->quantity_cart * $product->gift->price, 0, ',', '.') }}</td>
                                            <td align="center" id="status-{{ $product->gift->id }}">
                                                @if($product->gift->fulfilment_status == 'Fullfilled')
                                                    <span class="badge bg-green">Disponible</span>
                                                @elseif($product->gift->fulfilment_status == 'Not Available')
                                                    <span class="badge bg-red">No disponible</span>
                                                @elseif($product->gift->fulfilment_status == 'Pending')
                                                    <span class="badge bg-orange">Pendiente</span>
                                                @elseif($product->gift->fulfilment_status == 'Missing')
                                                    <span class="badge bg-maroon">Faltante</span>
                                                @elseif($product->gift->fulfilment_status == 'Returned')
                                                    <span class="badge bg-red">Devolución</span>
                                                @else
                                                    <span class="badge bg-blue">Reemplazado</span>
                                                @endif
                                            </td>
                                            @if($admin_permissions['update'] && !$order->invoice_number)
                                            <td>
                                                @if(in_array($order->status, ['In Progress', 'Dispatched']) || ($order->status == 'Delivered' && $admin_permissions['permission2']))
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            @if($product->gift->type!= 'Recogida')
                                                                <li><a class="update-product" href="javascript:;"
                                                                       data-product="{{ $product->gift->order_product_id }}"
                                                                       data-status="{{ 'Fullfilled' }}"
                                                                       data-type="{{ $product->gift->type }}">Disponible</a>
                                                                </li>
                                                                <li><a class="update-product" href="javascript:;"
                                                                       data-product="{{ $product->gift->order_product_id }}"
                                                                       data-status="{{ 'Not Available' }}"
                                                                       data-type="{{ $product->gift->type }}">No
                                                                        disponible</a></li>
                                                            @endif
                                                            @if ($order->status == 'In Progress')
                                                                <li><a class="update-product" href="javascript:;"
                                                                       data-product="{{ $product->gift->order_product_id }}"
                                                                       data-status="{{ 'Missing' }}"
                                                                       data-type="{{ $product->gift->type }}">Faltante</a>
                                                                </li>
                                                            @endif
                                                            <div class="divider"></div>

                                                        </ul>
                                                    </div>
                                                @elseif ( in_array($order->status, ['Initiated', 'Validation', 'Enrutado', 'In Progress']) && $admin_permissions['update'] )
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a class="remove-product"
                                                                   href="{{ route('adminOrderStorage.removeProductGift', ['id' => $order->id, 'product_id' => $product->gift->order_product_id]) }}">
                                                                    Eliminar
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @else
                                                    <img src="{{ web_url() }}/admin_asset/img/gift.png" height="20px"
                                                         title="Es un regalo">
                                                @endif
                                            </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endif
                                @if(isset($product->samplings))
                                    @foreach($product->samplings as $sampling)
                                        <tr>
                                            <td align="center">
                                                <img src="{{ $sampling->image_url }}"
                                                     height="50px">&nbsp;&nbsp;<img
                                                        src="{{ web_url() }}/admin_asset/img/sample.png" height="20px"
                                                        title="Es una muestra">
                                            </td>
                                            <td>{{ $sampling->reference }}</td>
                                            <td>{{ $sampling->name }}
                                            <td>{{ $sampling->quantity }} {{ $sampling->unit }}</td>
                                            <td align="right">{{ $sampling->quantity_cart_original }}</td>
                                            <td align="right">{{ $sampling->quantity_cart }}</td>
                                            <td align="right">
                                                ${{ number_format($sampling->price, 0, ',', '.') }}</td>
                                            <td align="right">
                                                ${{ number_format($sampling->price, 0, ',', '.') }}</td>
                                            <td align="right">
                                                ${{ number_format($sampling->quantity_cart * $sampling->price, 0, ',', '.') }}</td>
                                            <td align="center" id="status-{{ $sampling->order_product_id }}">
                                                @if($sampling->fulfilment_status == 'Fullfilled')
                                                    <span class="badge bg-green">Disponible</span>
                                                @elseif($sampling->fulfilment_status == 'Not Available')
                                                    <span class="badge bg-red">No disponible</span>
                                                @elseif($sampling->fulfilment_status == 'Pending')
                                                    <span class="badge bg-orange">Pendiente</span>
                                                @elseif($sampling->fulfilment_status == 'Missing')
                                                    <span class="badge bg-maroon">Faltante</span>
                                                @elseif($sampling->fulfilment_status == 'Returned')
                                                    <span class="badge bg-red">Devolución</span>
                                                @else
                                                    <span class="badge bg-blue">Reemplazado</span>
                                                @endif
                                            </td>
                                            @if($admin_permissions['update'] && !$order->invoice_number)
                                                <td>

                                                    @if(in_array($order->status, ['In Progress', 'Dispatched']) || ($order->status == 'Delivered' && $admin_permissions['permission2']))
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger dropdown-toggle"
                                                                    data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                @if($sampling->type!= 'Recogida')
                                                                    <li><a class="update-product" href="javascript:;"
                                                                           data-product="{{ $sampling->order_product_id }}"
                                                                           data-status="{{ 'Fullfilled' }}"
                                                                           data-type="{{ $sampling->type }}">Disponible</a>
                                                                    </li>
                                                                    <li><a class="update-product" href="javascript:;"
                                                                           data-product="{{ $sampling->order_product_id }}"
                                                                           data-status="{{ 'Not Available' }}"
                                                                           data-type="{{ $sampling->type }}">No
                                                                            disponible</a></li>
                                                                @endif
                                                                @if ($order->status == 'In Progress')
                                                                    <li><a class="update-product" href="javascript:;"
                                                                           data-product="{{ $sampling->order_product_id }}"
                                                                           data-status="{{ 'Missing' }}"
                                                                           data-type="{{ $sampling->type }}">Faltante</a>
                                                                    </li>
                                                                @endif
                                                                <div class="divider"></div>

                                                            </ul>
                                                        </div>
                                                    @elseif( in_array($order->status, ['Initiated', 'Validation', 'Enrutado']) &&  $admin_permissions['update'] )
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger dropdown-toggle"
                                                                    data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li>
                                                                    <a class="remove-product"
                                                                       href="{{ route('adminOrderStorage.removeProduct', ['order_id' => $order->id, 'order_product_id' => $sampling->order_product_id]) }}">
                                                                        Eliminar
                                                                    </a>
                                                                </li>
                                                                <div class="divider"></div>
                                                            </ul>
                                                        </div>
                                                    @else
                                                        <img src="{{ web_url() }}/admin_asset/img/sample.png" height="20px"
                                                             title="Es una muestra">
                                                    @endif

                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div>
    </section>

    <!-- Promoter orders Modal -->
    <div class="modal fade" id="modal-promoter-orders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Historial de pedidos del promotor</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="promoter-orders-table" class="table table-bordered table-striped">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Referrals of Promoter Modal -->
    <div class="modal fade" id="modal-referrals-of-promoter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Información de referidos del promotor</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="referrals-of-promoter-table" class="table table-bordered table-striped">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Orders per Address Modal -->
    <div class="modal fade" id="modal-orders-per-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Pedidos en esta dirección: {{ $order->user_address }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="orders-per-address-table" class="table table-bordered table-striped">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Refrrals this client Modal -->
    <div class="modal fade" id="modal-referrals-this-client" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Información de referidos del cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="referrals-this-client-table" class="table table-bordered table-striped">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal para notas crédito -->
    @if($order->status == 'Delivered' && $admin_permissions['update'] && $admin_permissions['permission2'])
        <div class="modal fade" id="modal-credit-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div id="content-credit-note">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Nota crédito <span v-if="credit_note_number">#@{{ credit_note_number }}</span></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form @submit.prevent="searchProducts">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="modal-title">Buscar Productos</h4>
                                                <table width="100%" class="modal-product-request-table">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                        <td>
                                                            <input type="text" placeholder="Referencia, nombre" name="productName" v-model="formData.productName" required class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
                                                        </td>
                                                        <td colspan="2" align="left"><button type="submit" :disabled="disabledSearch" class="btn btn-primary">Buscar</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td align="center" colspan="1"><img src="{{ asset_url() }}/img/loading.gif" v-if="disabledSearch"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-default">
                                        <div class="box-body pre-scrollable">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Producto</th>
                                                    <th>Cant. pedida</th>
                                                    <th>Cant. entregada</th>
                                                    <th>Cant. nota crédito</th>
                                                    <th>Razón nota crédito</th>
                                                    <th>Precio unitario</th>
                                                    <th>IVA %</th>
                                                    <th>Agregar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(product, index) in productSearch" :key="product.id">
                                                    <td><img :src="product.product_image_url" width="58" max-height="58" class="img-responsive"/></td>
                                                    <td>@{{ product.product_name + ' ' + product.product_quantity + ' ' + product.product_unit}}</td>
                                                    <td align="center">@{{ product.quantity_original }}</td>
                                                    <td align="center">@{{ product.quantity }}</td>
                                                    <td>
                                                        <input type="number" name="credit_note_reason" v-model="product.quantity_calculate" class="form-control">
                                                        <small class="error" v-if="formData.errors.quantity_calculate.error && formData.errors.quantity_calculate.index == index">@{{ formData.errors.quantity_calculate.message }}</small>
                                                    </td>
                                                    <td>
                                                        <select name="creditNoteReasons" v-model="product.reason_credit_note" class="form-control">
                                                            <option v-for="(reason, index) in formData.creditNoteReasons" :value="reason.reason">@{{ reason.reason }}</option>
                                                        </select>
                                                    </td>
                                                    <td align="right">$@{{ formatPrice(product.base_price) }}</td>
                                                    <td align="right">@{{ product.iva }}%</td>
                                                    <td><button @click="addCreditNote(product, index)" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span></button></td>
                                                </tr>
                                                <tr v-if="productSearch.length == 0">
                                                    <td colspan="15" align="center">No se encontraron productos</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" v-if="disableDivAdd">
                                <div class="col-xs-12">
                                    <h4 class="modal-title">Productos a agregar</h4>
                                    <div class="box box-default">
                                        <div class="box-body pre-scrollable">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Producto</th>
                                                    <th>cant. pedida</th>
                                                    <th>cant. entregada</th>
                                                    <th>cant. nota crédito</th>
                                                    <th>Razón nota crédito</th>
                                                    <th>Precio unitario</th>
                                                    <th>IVA %</th>
                                                    <th>IVA</th>
                                                    <th>Precio</th>
                                                    <th>Eliminar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(product, index) in addProductsCreditNote" :key="product.id">
                                                    <td><img :src="product.product_image_url" width="58" max-height="58" class="img-responsive"/></td>
                                                    <td>@{{ product.product_name + ' ' + product.product_quantity + ' ' + product.product_unit}}</td>
                                                    <td align="center">@{{ product.quantity_original }}</td>
                                                    <td align="center">@{{ product.quantity }}</td>
                                                    <td align="center">@{{ product.quantity_calculate }}</td>
                                                    <td>@{{ product.reason_credit_note }}</td>
                                                    <td align="right">$@{{ formatPrice(product.base_price) }}</td>
                                                    <td align="right">@{{ product.iva }}%</td>
                                                    <td align="right">$@{{ formatPrice(product.iva_amount) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.total_price) }}</td>
                                                    <td><button @click="removeAddCreditNote(product, index)" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus"></span></button></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" v-if="disableDivProductsCN">
                                <div class="col-xs-12">
                                    <h4 class="modal-title">Productos en la nota crédito</h4>
                                    <div class="box box-default">
                                        <div class="box-body pre-scrollable">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Producto</th>
                                                    <th>Cant. pedida</th>
                                                    <th>Cant. entregada</th>
                                                    <th>Cant. nota crédito</th>
                                                    <th>Razón nota crédito</th>
                                                    <th>Precio unitario</th>
                                                    <th>IVA %</th>
                                                    <th>IVA</th>
                                                    <th>Precio</th>
                                                    <th>Eliminar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="product in productsCreditNote" :key="product.id">
                                                    <td><img :src="product.product_image_url" width="58" max-height="58" class="img-responsive"/></td>
                                                    <td>@{{ product.product_name + ' ' + product.product_quantity + ' ' + product.product_unit}}</td>
                                                    <td align="center">@{{ product.quantity_original }}</td>
                                                    <td align="center">@{{ product.quantity }}</td>
                                                    <td align="center">@{{ product.quantity_credit_note }}</td>
                                                    <td>@{{ product.reason_credit_note }}</td>
                                                    <td align="right">$@{{ formatPrice(product.base_price) }}</td>
                                                    <td align="right">@{{ product.iva }}%</td>
                                                    <td align="right">$@{{ formatPrice(product.iva_amount) }}</td>
                                                    <td align="right">$@{{ formatPrice(product.total_price) }}</td>
                                                    <td>
                                                        <button @click="removeCreditNote(product)" :disabled="disableRemove" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus"></span></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="9"><b>Sub Total: </b></td>
                                                    <td>$@{{ formatPrice(total_price) }}</td>
                                                </tr>
                                                <tr v-if="total_iva != 0">
                                                    <td align="right" colspan="9"><b>IVA: </b></td>
                                                    <td>$@{{ formatPrice(total_iva) }}</td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="9"><b>Total: </b></td><td>$@{{ formatPrice(total) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form @submit.prevent="updateProductsCreditNote">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" :disabled="disableSave" class="btn btn-primary">Guardar</button>
                                <img src="{{ asset_url() }}/img/loading.gif" v-if="loader">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- Customer Credits Modal -->
    <div class="modal fade" id="modal-credits" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Créditos de Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Crédito</th>
                                            <th>Descripción usuario</th>
                                            <th>Descripción admin</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (count($customer_credits))
                                            @foreach($customer_credits as $credit)
                                                <tr>
                                                    <td>{{ date("d M Y g:i a",strtotime($credit->created_at)) }}</td>
                                                    <td>{{ currency_format($credit->amount) }}</td>
                                                    <td>{{ $credit->description }}</td>
                                                    <td>{{ $credit->description_admin }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3" align="center">No hay datos.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Customer Free Delivery Modal -->
    <div class="modal fade" id="modal-free-delivery-log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Domicilio Gratis Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (count($customer_free_deliveries))
                                            @foreach($customer_free_deliveries as $free_delivery)
                                                <tr>
                                                    <td>{{ date("d M Y g:i a",strtotime($free_delivery->created_at)) }}</td>
                                                    <td>{{ $free_delivery->description }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="2" align="center">No hay datos.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Customer Orders Modal -->
    <div class="modal fade" id="modal-orders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width:900px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">El cliente tiene {{ count($customer_orders) }}pedidos</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Fecha</th>
                                            <th>Nombre / Dirección</th>
                                            <th>Tienda</th>
                                            <th>Items</th>
                                            <th>Total</th>
                                            <th>Estado</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (count($customer_orders))
                                            @foreach($customer_orders as $customer_order)
                                                <tr>
                                                    <td>
                                                        <a href="{{ route('adminOrderStorage.details', ['id' => $customer_order->id]) }}"
                                                           target="_blank">{{ $customer_order->id }}</a></td>
                                                    <td>{{ date("d M Y g:i a",strtotime($customer_order->date)) }}</td>
                                                    <td>{{ $customer_order->user_firstname.' '.$customer_order->user_lastname }}
                                                        <br>{{ $customer_order->user_address.' '.$customer_order->user_address_further }}
                                                    </td>
                                                    <td>{{ $customer_order->store }}</td>
                                                    <td>{{ $customer_order->total_products }}</td>
                                                    <td>
                                                        ${{ number_format($customer_order->total_amount + $customer_order->delivery_amount - $customer_order->discount_amount, 0, ',', '.') }}</td>
                                                    <td>
                                                        @if($customer_order->status == 'Delivered')
                                                            <span class="badge bg-green">Delivered</span>
                                                        @elseif($customer_order->status == 'Cancelled')
                                                            <span class="badge bg-red">Cancelled</span>
                                                        @elseif($customer_order->status == 'Initiated')
                                                            <span class="badge bg-yellow">Initiated</span>
                                                        @elseif($customer_order->status == 'Dispatched')
                                                            <span class="badge bg-green">Dispatched</span>
                                                        @else
                                                            <span class="badge bg-green">In Progress</span>
                                                        @endif
                                                        <p>
                                                            <b>Origen:</b> {{ $customer_order->source }} {{ $customer_order->source_os }}
                                                            @if (!empty($customer_order->user_score_date))
                                                                <br><b>Calificación:</b>
                                                                @if(!$customer_order->user_score)
                                                                    Negativa
                                                                @else
                                                                    Positiva
                                                                @endif
                                                            @endif
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7" align="center">No hay datos.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Order Notes Modal -->
    <div class="modal fade" id="modal-notes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Notas de Pedido</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Usuario admin</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (count($order_notes))
                                            @foreach($order_notes as $note)
                                                <tr>
                                                    <td>{{ date("d M Y g:i a",strtotime($note->created_at)) }}</td>
                                                    <td>{{ $note->description }}</td>
                                                    <td>{{ $note->fullname }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3" align="center">No hay datos.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Customer Note Modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Nota a Pedido</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.saveNote', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                        <div class="form-group reject-comments">
                            <label class="reject-label">Nota</label>
                            <textarea class="form-control required" required="required" name="note"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Add Credit to user -->
    <div class="modal fade" id="modal-user-credits" tabindex="-1" role="dialog" aria-labelledby="User Credits"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Crédito a usuario</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success alert-dismissable unseen">
                        <i class="fa fa-check"></i>
                        {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
                        <b>Hecho!</b>
                        <div class="message" style="display: inline-block;"></div>
                    </div>

                    <div class="alert alert-danger alert-dismissable unseen">
                        <i class="fa fa-ban"></i>
                        {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
                        <b>Alerta!</b>
                        <div class="message" style="display: inline-block;"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <form role="form" method="post" id="main-form-user-credits">
                                <div class="form-group row">
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <label>Ciente</label>
                                        <input type="text" class="form-control" disabled="disabled"
                                               value="{{ $order->user_firstname.' '.$order->user_lastname }}"/>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <label>Saldo actual</label>
                                        <input type="text" class="form-control current-credit" disabled="disabled"
                                               value="{{ currency_format($user_discounts['amount']) }}"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <label>Tipo</label>
                                        <select type="text" class="form-control" name="type" id="type">
                                            <option value="">-Seleciona-</option>
                                            <option value="1">Cargo</option>
                                            <option value="0">Deducción</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <label>Valor</label>
                                        <input type="text" class="form-control" name="amount" id="amount"
                                               placeholder="Ingresa el valor del credito">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <label>Motivo reclamo</label>
                                        <select type="text" class="form-control" name="claim_motive" id="claim_motive">
                                            <option value="">-Selecciona-</option>
                                            <option value="Producto faltante cobrado">Producto faltante cobrado</option>
                                            <option value="Producto faltante no cobrado">Producto faltante no cobrado
                                            </option>
                                            <option value="Producto diferente al solicitado">Producto diferente al
                                                solicitado
                                            </option>
                                            <option value="Producto vencido">Producto vencido</option>
                                            <option value="Producto próximo a vencer">Producto próximo a vencer</option>
                                            <option value="Producto en mal estado">Producto en mal estado</option>
                                            <option value="Queja por transportador">Queja por transportador</option>
                                            <option value="Llegada tarde">Llegada tarde</option>
                                            <option value="Estrategia fidelización">Estrategia fidelización</option>
                                            <option value="Resarcimiento por servicio">Resarcimiento por servicio</option>
                                            <option value="Retorno referido">Retorno referido</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <label>Fecha expiración </label>
                                        <input type="text" class="form-control" name="expiration_date"
                                               id="expiration_date" placeholder="DD/MM/YYYY">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 text-right">
                                        <a href="javascript:show_modal('products-to-credit');" class="btn btn-primary">
                                            <li class="fa fa-plus"></li>
                                            Agregar productos</a>
                                    </div>
                                </div>
                                <input type="hidden" name="products_related" id="products_related">
                            </form>
                        </div>
                    </div>
                    <div class="div-table-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-default">
                                    <div class="box-body">
                                        <div class="modal-products-request pre-scrollable">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Referencia</th>
                                                    <th>Nombre</th>
                                                    <th>Quitar</th>
                                                </tr>
                                                </thead>
                                                <tbody class="tbody content-table-data">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary btn-user-credits">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Add products credit Modal -->
    <div class="modal fade" id="modal-products-to-credit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Productos a Credito</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row error-search">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger unseen"></div>
                                    <div class="alert alert-success unseen"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="modal-products-request pre-scrollable">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Imagen</th>
                                                <th>Referencia</th>
                                                <th>Nombre</th>
                                                <th>Unidad de medida</th>
                                                <th>Agregar</th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody">
                                            @if ( !empty($order_products) )
                                                @foreach ($order_products as $product)
                                                    <tr>
                                                        <td>
                                                            <img src="{{ $product->product_image_url }}" height="50px">
                                                        </td>
                                                        <td>
                                                            {{ $product->reference }}
                                                        </td>
                                                        <td>
                                                            {{ $product->product_name }}
                                                        </td>
                                                        <td>
                                                            {{ $product->product_quantity }} {{ $product->product_unit }}
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-xs btn-success btn-add-product-to-credit"
                                                                   href="javascript:;"
                                                                   data-store-product-id="{{$product->store_product_id}}"
                                                                   data-url-image="{{ $product->product_image_url }}"
                                                                   data-product-reference="{{ $product->reference }}"
                                                                   data-product-name="{{ $product->product_name }} {{ $product->product_quantity }} {{ $product->product_unit }}">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="14" align="center">&nbsp;</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div align="center" class="modal-products-request-loading" style="display: none;">
                                        <br><img src="{{ asset_url() }}/img/loading.gif"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Status Order Modal -->
    <div class="modal fade" id="modal-change-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cambiar Estado de Pedido</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updateStatusAdmin', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                        <div class="form-group">
                            <label class="control-label">Nuevo estado de pedido</label>
                            <select class="form-control required" name="status" id="modal-new-status">
                                <option value="">-Selecciona-</option>
                                @if (!in_array($order->status, ['Validation']))
                                    <option value="Validation">Validation</option>
                                @endif
                                @if (!in_array($order->status, ['Validation', 'Initiated']))
                                    <option value="Initiated">Initiated</option>
                                @endif
                                @if (!in_array($order->status, ['Validation', 'Initiated', 'Enrutado']))
                                    <option value="Enrutado">Enrutado</option>
                                @endif
                                @if (!in_array($order->status, ['Cancelled']))
                                    <option value="Cancelled">Cancelled</option>
                                @endif
                            </select>
                        </div>
                        <div class="reject-block" style="display: none">
                            <div class="form-group">
                                <label class="reject-label">Razón por la cual se cancela el pedido</label>
                                <select class="form-control required reject_reason" name="reject_reason"
                                        id="reject_reason">
                                    <option value="">-Selecciona-</option>
                                    @foreach($reasons as $reason)
                                        @if($order->status == 'Cancelled' && $order->temporarily_cancelled && $reason->status != 'Cancelado temporalmente')
                                            <option value="{{ $reason->status }}">{{ $reason->status }}</option>
                                        @else
                                            @if($order->status != 'Cancelled' && !$order->temporarily_cancelled)
                                                <option value="{{ $reason->status }}">{{ $reason->status }}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group reject_reason_futher unseen">
                                <label class="reject-label">Motivo</label>
                                <select class="form-control required reject_reason_futher_data"
                                        name="reject_reason_futher" id="reject_reason_futher">
                                    <option value="">Cargando...</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="reject-label">Comentarios de cancelación</label>
                                <textarea class="form-control" name="reject_comments"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Coupon Order Modal -->
    <div class="modal fade" id="modal-add-coupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Cupón</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.saveCoupon', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                        <div class="form-group">
                            <label class="control-label">Código de cupón</label>
                            <input type="text" class="form-control" name="coupon_code" id="coupon_code">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Allocate Picker Modal -->
    <div class="modal fade" id="modal-picker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Asignar Alistador</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updatePicker', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Alistador seco</label>
                            <select class="form-control required" name="picker-dry-id" id="modal-pickers">
                                <option value="" selected="selected">Selecciona</option>
                                @foreach($pickers['dry'] as $picker)
                                    <option value="{{$picker->id}}">{{$picker->first_name}} {{$picker->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Alistador frío</label>
                            <select class="form-control required" name="picker-cold-id" id="modal-pickers">
                                <option value="" selected="selected">Selecciona</option>
                                @foreach($pickers['cold'] as $picker)
                                    <option value="{{$picker->id}}">{{$picker->first_name}} {{$picker->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Allocate Transporter Modal -->
    <div class="modal fade" id="modal-transporter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Asignar Transportador</h4>
                </div>
                <form id="update-transporter" method="post"
                      action="{{ route('adminOrderStorage.updateTransporter', ['id' => $order->id]) }}">
                    <div class="modal-body">
                        <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Transportador</label>
                            <select class="form-control required" name="transporter_id" id="transporter_id">
                                <option value="">Selecciona</option>
                                @foreach($transporters as $transporter)
                                    <option value="{{$transporter->id}}">{{ $transporter->fullname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Vehículo</label>
                            <select class="form-control required" name="vehicle_id" id="vehicle_id">
                                <option value="">Selecciona</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar cantidad de Producto</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updateProduct') }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="order-product-id" name="id" value="">
                        <div class="form-group">
                            <label class="control-label">Cantidad</label>
                            <input type="text" class="form-control" name="quantity" id="order-product-quantity">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Reject Order Modal -->
    <div class="modal fade" id="modal-reject-order">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cancelar Pedido</h4>
                </div>
                <form method="post"
                      action="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Cancelled']) }}"
                      class="form-modal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="reject-label">Selecciona la razón por la cual cancelas este pedido</label>
                            <select class="form-control required reject_reason" name="reject_reason" id="reject_reason">
                                <option value="">-Selecciona-</option>
                                @foreach($reasons as $reason)
                                    <option value="{{ $reason->status }}" data-id="{{$reason->id}}">{{ $reason->status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="loading unseen"></div>
                        <div class="form-group reject_reason_futher unseen">
                            <label class="reject-label">Motivo</label>
                            <select class="form-control required reject_reason_futher_data" name="reject_reason_futher"
                                    id="reject_reason_futher">
                                <option value="">Cargando...</option>
                            </select>
                            <input name="reject_reason_futher_id" id="reject_reason_futher_id" value="" type="hidden">
                        </div>
                        @if($order->status == 'Dispatched')
                            <div class="form-group arrival_image_url unseen">
                                <label class="control-label" for="arrival_image_url">Imagen del lugar de entrega</label>
                                <input type="file" class="form-control" name="arrival_image_url" id="arrival_image_url" required="required">
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="reject-label">Comentarios de cancelación</label>
                            <textarea class="form-control" id="reject_comments" name="reject_comments"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger save-status reject-btn">Cancelar Pedido</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Accept Order Modal -->
    <div class="modal fade" id="modal-ruta">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title accept-title">Asignar Ruta</h4>
                </div>
                <form id="update-to-enrutado" method="post"
                      action="{{ route('adminOrderStorage.updateRoute', ['id' => $order->id]) }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group accept-options">
                            <label class="accept-label">Ruta</label>
                            <select class="form-control required" name="route_id">
                                <option value="">Selecciona</option>
                                @foreach($routes as $route)
                                    <option value="{{ $route->id }}">{{ $route->route }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group accept-options">
                            <label class="accept-label">Sequencia</label>
                            <input type="number" name="planning_sequence" id="planning_sequence"
                                   class="form-control required">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Piking Modal -->
    <div class="modal fade" id="modal-order-baskets-bags">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title accept-title">Alistar Pedido</h4>
                </div>
                <form id="update-to-enrutado" method="post"
                      action="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Alistado']) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group accept-options ">
                            <label class="accept-label">Número de canastillas</label>
                            <input type="number" name="picking_baskets" id="picking_baskets"
                                   class="form-control required">
                        </div>
                        <div class="form-group accept-options ">
                            <label class="accept-label">Número de bolsas</label>
                            <input type="number" name="picking_bags" id="picking_bags" class="form-control required">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Track Shopper Modal -->
    <div class="modal fade" id="modal-track">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Localizar Transportador</h4>
                </div>
                <div class="modal-body">
                    <div id="map" class="col-xs-12" style="height: 300px;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Log Order Modal -->
    <div class="modal fade" id="modal-orders_log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 900px; width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Log de Pedido</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Cliente</th>
                                            <th>Alistador</th>
                                            <th>Conductor</th>
                                            <th>Usuario</th>
                                            <th>Log</th>
                                            <th>Fecha</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (count($order_log))
                                            @foreach($order_log as $log)
                                                <tr>
                                                    <td>{{ $log->user_name }}</td>
                                                    <td>{{ $log->picker_name }}</td>
                                                    <td>{{ $log->driver_name }}</td>
                                                    <td>{{ $log->admin_name }}</td>
                                                    <td>{{ $log->type }}</td>
                                                    <td>{{ date_format($log->created_at, 'd/m/Y h:i:s A') }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" align="center">No hay datos.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Voucher Image Order Modal -->
    <div class="modal fade" id="modal-voucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Imagen de Voucher</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.saveUploadImage', ['id' => $order->id]) }}"
                      enctype="multipart/form-data" class="form-modal">
                    <input type="hidden" name="type" value="voucher">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Imagen</label>
                            <input type="file" class="form-control" name="image" required="required">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Subir</button>
                        </div>
                        @if ($order->voucher_image_url)
                            <div class="form-group">
                                <label class="control-label">Imagen actual</label>
                                <img src="{{ $order->voucher_image_url }}" width="100%">
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Customer Delivery Date Modal -->
    <div class="modal fade" id="modal-delivery-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar Fecha de Entrega</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updateDeliveryDate') }}" class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Nueva fecha de entrega</label>
                            <select class="form-control required delivery_day" name="delivery_day" id="delivery_day">
                                <option value="">Cargando...</option>
                            </select>
                            <div id="loading unseen"></div>
                            <div class="delivery_time unseen"><br/>
                                <select class="form-control required" name="delivery_time" id="delivery_time">
                                    <option value="">Cargando...</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                    <input type="hidden" id="order_id" name="order_id" value="{{ $order->id }}"/>
                    <input type="hidden" id="store_id" name="store_id" value="{{ $order->store_id }}"/>
                    <input type="hidden" id="zone_id" name="zone_id" value="{{ $order->zone_id }}"/>
                    <input type="hidden" id="order_detail" name="order_detail" value="1"/>
                </form>
            </div>
        </div>
    </div>

    <!-- Customer Delivery Date Modal -->
    <div class="modal fade" id="modal-reschedule-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reprogramar Pedido</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.reverseStatus', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Reprogramar pedido para entrega</label>
                            <select class="form-control required reschedule_delivery_day" name="reschedule_delivery_day"
                                    id="reschedule_delivery_day">
                                <option value="tomorrow" selected="selected">Otro día</option>
                                <option value="today">Hoy</option>
                            </select>
                            <div class="reschedule_delivery_window_id unseen"><br/>
                                <select class="form-control required" name="reschedule_delivery_window_id" id="reschedule_delivery_window_id">
                                    <option value="">-Selecciona-</option>
                                    <optgroup label="Franjas normales">
                                        @foreach($delivery_windows as $delivery_window)
                                            <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Mismo día">
                                        @foreach($delivery_windows_same_day as $delivery_window)
                                            <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Reprogramar Pedido</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Customer Delivery Address Modal -->
    <div class="modal fade" id="modal-delivery-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar Dirección de Entrega</h4>
                </div>
                <form method="post"
                      action="{{ route('adminOrderStorage.updateDeliveryAddress', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Ciudad</label>
                            <select name="city" id="city" class="form-control required">
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}"
                                            @if ( $order->city_id == $city->id )selected="selected"@endif>{{ $city->city }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group clearfix">
                            <label for="add" style="display: block">Dirección:</label>
                            <div class="col-xs-3" style="padding-left: 0">
                                <select name="address_1" class="form-control">
                                    <option value="Calle" @if($order->address_1 == 'Calle') selected="selected" @endif>Calle</option>
                                    <option value="Carrera" @if($order->address_1 == 'Carrera') selected="selected" @endif>Carrera</option>
                                    <option value="Avenida" @if($order->address_1 == 'Avenida Carrera') selected="selected" @endif>Avenida</option>
                                    <option value="Avenida Carrera" @if($order->address_1 == 'Avenida Carrera') selected="selected" @endif>Avenida Carrera</option>
                                    <option value="Avenida Calle" @if($order->address_1 == 'Avenida Calle') selected="selected" @endif>Avenida Calle</option>
                                    <option value="Circular" @if($order->address_1 == 'Circular') selected="selected" @endif>Circular</option>
                                    <option value="Circunvalar" @if($order->address_1 == 'Circunvalar') selected="selected" @endif>Circunvalar</option>
                                    <option value="Diagonal" @if($order->address_1 == 'Diagonal') selected="selected" @endif>Diagonal</option>
                                    <option value="Manzana" @if($order->address_1 == 'Manzana') selected="selected" @endif>Manzana</option>
                                    <option value="Transversal" @if($order->address_1 == 'Transversal') selected="selected" @endif>Transversal</option>
                                    <option value="Vía" @if($order->address_1 == 'Vía') selected="selected" @endif>Vía</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <input type="text" name="address_2" class="form-control" placeholder="127" value="{{ $order->address_2 }}">
                            </div>
                            <label for="dir3" class="col-xs-1">#</label>
                            <div class="col-xs-2">
                                <input type="text" name="address_3" class="form-control" placeholder="7" value="{{ $order->address_3 }}">
                            </div>
                            <label for="dir4" class="col-xs-1">-</label>
                            <div class="col-xs-2" style="width: 20%;">
                                <input type="text" name="address_4" class="form-control" placeholder="12" value="{{ $order->address_4 }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Complemento de dirección</label>
                            <input type="text" class="form-control" name="delivery_address_further"
                                   id="delivery_address_further" placeholder="Ej: Apartamento 501, Torre 2"
                                   value="{{ $order->user_address_further }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Barrio</label>
                            <input type="text" class="form-control" name="delivery_address_neighborhood"
                                   id="delivery_address_neighborhood" value="{{ $order->user_address_neighborhood }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Coordenada</label>
                            <input type="text" class="form-control" name="latlng" id="latlng"
                                   placeholder="Ej: 6.122646,-75.634480">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input class="form-control" name="update_user_address" id="update_user_address"
                                           type="checkbox">&nbsp;&nbsp;&nbsp;Actualizar coordenada en la dirección del
                                    cliente en la cuenta
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Payment Method Modal -->
    <div class="modal fade" id="modal-payment-method" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar Método de Pago</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updatePaymentMethod', ['id' => $order->id]) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Método de pago</label>
                            <select class="form-control required payment_method" name="payment_method">
                                <option value="Efectivo"
                                        @if ($order['payment_method'] == 'Efectivo') selected="selected" @endif>Efectivo
                                </option>
                                <option value="Tarjeta de crédito"
                                        @if ($order['payment_method'] == 'Tarjeta de crédito') selected="selected" @endif>
                                    Tarjeta de crédito
                                </option>
                                <option value="Datáfono"
                                        @if ($order['payment_method'] == 'Datáfono') selected="selected" @endif>Datáfono
                                </option>
                                <option value="Débito - PSE" 
                                        @if ($order['payment_method'] == 'Débito - PSE') selected="selected" @endif>Débito - PSE
                                </option>
                                @if ( $order->status == 'Delivered' || $order->status == 'Dispatched' )
                                    <option value="Efectivo y datáfono"
                                            @if ($order['payment_method'] == 'Efectivo y datáfono') selected="selected" @endif>
                                        Efectivo y datáfono
                                    </option>
                                @endif
                            </select>
                        </div>
                        <div class="credit_cards unseen"><br/>
                            <div class="form-group">
                                <label class="control-label">Tarjeta de crédito</label>
                                <select class="form-control required" name="credit_card_id">
                                    @foreach($customer_credit_cards as $credit_card)
                                        <option value="{{ $credit_card->id }}"
                                                @if (isset($post['credit_card_id']) && $post['credit_card_id'] == $credit_card->id) selected="selected" @endif>
                                            **** {{ $credit_card->last_four }} {{ $credit_card->type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card_cash unseen">
                            <hr>
                            <div class="form-group">
                                <h4 class="control-label">Efectivo y datáfono</h4>
                            </div>
                            <div class="form-group">
                                <label for="user_cash_paid" class="control-label">Total pagado con efectivo</label>
                                <input type="text" id="user_cash_paid" name="user_cash_paid"
                                       class="form-control required" placeholder="Valor pagado con efectivo">
                            </div>
                            <div class="form-group">
                                <label for="user_card_paid" class="control-label">Total pagado con datáfono</label>
                                <input type="text" id="user_card_paid" name="user_card_paid"
                                       class="form-control required" placeholder="Valor pagado con datáfono">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Order Payments List Modal -->
    <div class="modal fade" id="modal-order-payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" style="width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Listado de Transacciones</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive container-overflow">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Estado</th>
                                            <th>Charge ID</th>
                                            <th>Id Transacción</th>
                                            <th>Usuario</th>
                                            <th>Reembolsos</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($order_payments))
                                            @foreach($order_payments as $order_payment)
                                                <tr>
                                                    <td>{{date("d M Y h:i a", strtotime($order_payment->cc_payment_date))}}</td>
                                                    <td>
                                                        @if(!empty($order_payment->cc_refund_status))
                                                            @if($order_payment->cc_refund_status == 'Aprobada')
                                                                <span class="badge bg-red">Reembolsada</span>
                                                            @else
                                                                <span class="badge bg-red">Reembolso pendiente</span>
                                                            @endif
                                                        @else
                                                            @if( $order_payment->cc_payment_status == 'Aprobada' )
                                                                <span class="badge bg-green">{{$order_payment->cc_payment_status}}</span>
                                                            @elseif($order_payment->cc_payment_status == 'Pendiente')
                                                                <span class="badge bg-orange">{{$order_payment->cc_payment_status}}</span>
                                                            @else
                                                                <span class="badge bg-red">{{$order_payment->cc_payment_status}}</span>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>{{$order_payment->cc_charge_id}}</td>
                                                    <td>{{$order_payment->cc_payment_transaction_id}}</td>
                                                    <td>{{$order_payment->fullname}}</td>
                                                    <td align="center">
                                                        @if(!empty($order_payment->cc_refund_status))
                                                            <a href="#"
                                                               onclick="show_modal('order-payment-refunds', {{$order_payment->id}} )"><span
                                                                        class="glyphicon glyphicon-folder-open"></span></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" align="center">No hay transacciones.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Order Payments List Modal -->
    <div class="modal fade" id="modal-order-payment-refunds" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Listado de Reembolsos</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Payment Method Modal -->
    <div class="modal fade" id="modal-delivered" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Actualizar estado a Entregado</h4>
                </div>
                <form method="post"
                      action="{{ route('adminOrderStorage.updateStatus', ['id' => $order->id, 'status' => 'Delivered']) }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label">Método de pago del cliente</label>
                            <select class="form-control required payment_method_delivered"
                                    name="customer_payment_method">
                                <option value="Efectivo"
                                        @if (isset($post['payment_method']) && $post['payment_method'] == 'Efectivo') selected="selected" @endif>
                                    Efectivo
                                </option>
                                <option value="Datáfono"
                                        @if (isset($post['payment_method']) && $post['payment_method'] == 'Datáfono') selected="selected" @endif>
                                    Datáfono
                                </option>
                                <option value="Efectivo y datáfono"
                                        @if (isset($post['payment_method']) && $post['payment_method'] == 'Efectivo y datáfono') selected="selected" @endif>
                                    Efectivo y datáfono
                                </option>
                            </select>
                        </div>
                        <div class="customer-block-cash-card unseen">
                            <hr>
                            <div class="form-group">
                                <h4 class="control-label">Efectivo y datáfono</h4>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Total pagado con efectivo</label>
                                <input type="text" class="form-control required customer-cash-paid"
                                       name="customer_cash_paid" id="customer_cash_paid" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Total pagado con datáfono</label>
                                <input type="text" class="form-control required customer-card-paid"
                                       name="customer_card_paid" id="customer_card_paid" value="">
                            </div>
                        </div>
                        @if ($admin_permissions['permission1'])
                            <div class="form-group">
                                <label class="control-label">¿El cliente pagó el pedido?</label>
                                <select class="form-control required" name="order_was_paid">
                                    <option value="1" selected="selected">Sí</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Update user data to show invoice -->
    <div class="modal fade" id="modal-update-user-invoice-data">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3>Actualizar Datos de Facturación</h3>
                </div>
                <div class="modal-body">
                    <form id="invoice-data-form" method="post"
                          action="{{ route('adminOrderStorage.updateInvoice', ['id' => $order->id]) }}">
                        <div class="form-group">
                            <label for="user_identity_type">Tipo de Documento</label>
                            <select id="user_identity_type" name="user_identity_type" class="form-control">
                                <option value="Cédula">Cédula</option>
                                <option value="NIT">NIT</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="user_identity_number">Número de Identificación</label>
                            <input type="text" id="user_identity_number" class="form-control"
                                   placeholder="Número de identificación" name="user_identity_number">
                        </div>
                        <div class="form-group">
                            <label for="identity_number">Razón Social o Nombre Completo</label>
                            <input type="text" id="user_business_name" class="form-control"
                                   placeholder="Razón Social o Nombre Completo" name="user_business_name">
                        </div>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                        <br><br>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <!-- Customer Delivery Address Modal -->
    <div class="modal fade" id="modal-send-sms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Envío de mensaje de texto</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.sendSms', ['id' => $order->id]) }}"
                      class="form-modal" id="send-sms">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label" for="sms">Mensaje</label>
                            <textarea class="form-control" name="sms" required=""></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="send-sms-btn">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Price custom product Modal -->
    <div class="modal fade" id="modal-price-custom-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Precio para producto personalizado</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updateProductCustom') }}" class="form-modal"
                      id="update-price-custom-product">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label for="user_identity_number">Precio</label>
                            <input type="text" id="price" class="form-control" placeholder="Precio" name="price">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="save-product-price-btn">Guardar</button>
                    </div>
                    <input type="hidden" id="product_id" class="form-control" name="product_id">
                    <input type="hidden" id="status" class="form-control" name="status" value="Fullfilled">
                    <input type="hidden" id="order_id" name="order_id" value="{{ $order->id }}"/>
                </form>
            </div>
        </div>
    </div>

    <!-- Add products Modal -->
    <div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Producto a Pedido</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="" class="modal-search-form">
                                <div class="row error-search unseen">
                                    <div class="col-xs-12">
                                        <div class="alert alert-danger unseen"></div>
                                        <div class="alert alert-success unseen"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table width="100%" class="modal-product-request-table">
                                            <tbody>
                                            <tr>
                                                <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                <td>
                                                    <input type="text" placeholder="Nombre" name="s" id="s"
                                                           class="modal-search form-control"
                                                           style="border: 1px solid rgb(204, 204, 204);">
                                                </td>
                                                <td colspan="2" align="left">
                                                    <button type="button" id="btn-modal-search" class="btn btn-primary"
                                                            data-order_id="{{ $order->id }}"
                                                            data-store_id="{{ $order->store_id }}">Buscar
                                                    </button>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2"><span class="error" style="display: none;">Campo requerido</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="modal-products-request pre-scrollable">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Imagen</th>
                                                <th>Referencia</th>
                                                <th>Nombre</th>
                                                <th>Unidad de medida</th>
                                                <th>Precio</th>
                                                <th>Stock actual</th>
                                                <th>Cantidad</th>
                                                <th>Agregar</th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody">
                                            <tr>
                                                <td colspan="14" align="center">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div align="center" class="modal-products-request-loading" style="display: none;">
                                        <br><img src="{{ asset_url() }}/img/loading.gif"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Show Product Group Modal -->
    <div class="modal fade" id="modal-order-product-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Productos del Producto Agrupado</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Map with points user -->
    <div class="modal fade" id="modal-map-coords-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3>Dirección de entrega</h3>
                </div>
                <div class="modal-body">
                    <div id="map-point"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="returns-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Devolución de producto</h4>
                </div>
                <form method="post" action="{{ route('adminOrderStorage.updateProductStatusReturned') }}"
                      class="form-modal">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <input type="hidden" id="order-id" name="id" value="">
                        <input type="hidden" id="product-id" name="product_id" value="">
                        <input type="hidden" id="product-type" name="type" value="">
                        <div class="form-group">
                            <label class="control-label">Motivo de devolución</label>
                            <select class="form-control" name="reason_returned" id="reason-returned">
                                <option value="Errado">Errado</option>
                                <option value="Mal estado">Mal estado</option>
                                <option value="Rechazado cliente">Rechazado por cliente</option>
                            </select>
                        </div>
                        <div class="form-group reference-returned">
                            <label class="control-label">Referencia del producto a devolver</label>
                            <input type="text" class="form-control" name="reference_returned" id="reference-returned">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Cantidad de unidades a devolver</label>
                            <input type="text" class="form-control" name="quantity" id="order-product-quantity">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Arrivals Image Order Modal -->
    <div class="modal fade" id="modal-arrival" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Imagen de cancelación</h4>
                </div>
                <div class="modal-body">
                    @if ($order->arrival_image_url)
                        <div class="form-group">
                            <label class="control-label">Imagen</label>
                            <img src="{{ $order->arrival_image_url }}" width="100%">
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Refund Modal -->
    <div class="modal fade" id="modal-refund" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reembolsar Cobro a Tarjeta de Crédito</h4>
                </div>
                <form method="post"
                      action="{{ route('adminOrderStorage.saveRefund', ['id' => ($last_order_payment ? $last_order_payment->id: null) ]) }}"
                      class="form-modal" id="send-sms">
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors"></div>
                        <div class="form-group">
                            <label class="control-label" for="sms">Motivo de reembolso</label>
                            <textarea class="form-control" name="reason" required=""></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Reembolsar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Score Modal -->
    <div class="modal fade" id="modal-score" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Calificación de pedido</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Guardar log de la llamada gestionada por SAC cuando se cambia el método de pago-->
    <div class="modal fade" id="modal-management-sac" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">SAC: Gestionar pedido</h4>
                </div>
                <form method="post" action="{{ route('AdminOrderValidation.managementSac') }}" class="form-modal">
                    <div class="modal-body">
                        <input type="hidden" id="orderId" name="id" value="{{ ( $order->id ? $order->id : null) }}">
                        <input type="hidden" id="reasonId" name="reasonId" value="{{ ( $order->order_validation_reason_id ? $order->order_validation_reason_id : null) }}">
                        <div class="form-group reject-comments">
                            <label class="reject-label">¿Pedido gestionado?</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">Si</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('admin.orders_storage.js.details-js')
    @if($order->status == 'Delivered' && $admin_permissions['permission2'])
        @include('admin.orders_storage.js.details-vue')
    @endif
@stop

@else
@section('product-table')
    @if (isset($store_products))
        @if (count($store_products))
            @foreach ($store_products as $store_product)
                <tr>
                    <td><img src="{{$store_product->image_small_url}}" class="img-responsive"></td>
                    <td>{{$store_product->reference}}</td>
                    <td>{{$store_product->name}}</td>
                    <td>{{$store_product->quantity}} {{$store_product->unit}}</td>
                    <td>
                        @if ($store_product->special_price) <p style="text-decoration: line-through;">
                            ${{number_format($store_product->price)}}</p> <p>
                            ${{number_format($store_product->special_price)}}</p> @else
                            ${{number_format($store_product->price)}} @endif
                    </td>
                    <td>{{$store_product->current_stock}}</td>
                    <td>
                        <input type="number" name="product_quantity" class="product_cant form-control" value="0">
                    </td>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-xs btn-default btn-add-product" href="javascript:;"
                               data-current_stock="{{$store_product->current_stock}}"
                               data-store_product_id="{{$store_product->id}}" data-order_id="{{$order_id}}"
                               data-store_id="{{$store_id}}">
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="14" align="center">Productos no encontrados.</td>
            </tr>
        @endif
    @else
        <tr>
            <td colspan="14" align="center">&nbsp;</td>
        </tr>
    @endif
@endsection

@section('order_product_group')
    @if (isset($order_product_group))
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Imagen</th>
                <th>Referencia</th>
                <th>Nombre</th>
                <th>Unidad de medida</th>
                <th>Cant. Solicitada</th>
                <th>Cant. Alistada</th>
                <th>Precio</th>
                <th>Estado</th>
                @if($admin_permissions['update'] && !$order->invoice_number)
                    <th>Acción</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($order_product_group as $product)
                <tr>
                    <td align="center"><img src="{{ $product->product_image_url }}" height="50px"></td>
                    <td>{{ $product->reference }}</td>
                    <td>{{ $product->product_name }}</td>
                    <td>{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                    <td align="right">{{ $product->quantity_original }}</td>
                    <td align="right">{{ $product->quantity }}@if(!empty($product->quantity_returned))<br>
                        (Devolución: {{ $product->quantity_returned }}<br>Motivo: {{ $product->reason_returned }})@endif
                    </td>
                    <td align="right">${{ number_format($product->price, 0, ',', '.') }}</td>
                    <td align="center" id="status-{{ $product->id }}">
                        @if($product->fulfilment_status == 'Fullfilled')
                            <span class="badge bg-green">Disponible</span>
                        @elseif($product->fulfilment_status == 'Not Available')
                            <span class="badge bg-red">No disponible</span>
                        @elseif($product->fulfilment_status == 'Pending')
                            <span class="badge bg-orange">Pendiente</span>
                        @elseif($product->fulfilment_status == 'Missing')
                            <span class="badge bg-maroon">Faltante</span>
                        @elseif($product->fulfilment_status == 'Returned')
                            <span class="badge bg-red">Devolución</span>
                        @else
                            <span class="badge bg-blue">Reemplazado</span>
                        @endif
                    </td>
                    @if($admin_permissions['update'] && !$order->invoice_number)
                        <td>
                            @if(in_array($order->status, ['In Progress', 'Alistado', 'Dispatched']) || ($order->status == 'Delivered' && $admin_permissions['permission2']))
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a class="update-product" href="javascript:;"
                                               data-product="{{ $product->id }}" data-status="{{ 'Fullfilled' }}"
                                               data-type="{{ $product->type }}">Disponible</a>
                                        </li>
                                        <li>
                                            <a class="update-product" href="javascript:;"
                                               data-product="{{ $product->id }}" data-status="{{ 'Not Available' }}"
                                               data-type="{{ $product->type }}">No disponible</a>
                                        </li>
                                        <li>
                                            <a class="update-product" href="javascript:;"
                                               data-product="{{ $product->id }}" data-status="{{ 'Missing' }}"
                                               data-type="{{ $product->type }}">Faltante</a>
                                        </li>
                                        @if ($order->status == 'Dispatched' || ($order->status == 'Delivered' && $admin_permissions['permission2']))
                                            <li>
                                                <a onclick="return_product({{ $order->id }} ,{{ $product->id }}, '{{ $product->type }}')"
                                                   href="#">Devolución</a></li>
                                        @endif
                                    </ul>
                                </div>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('comments')
    @if (isset($render_order_product_comments))
        <p><b>Fecha</b>: {{ format_date('normal_long_with_time', $order->user_score_date) }}</p>
        @if(!empty($order->user_score_comments))
            <p><b>Comentario</b>: {{ $order->user_score_comments }}</p>
        @endif
        @if(!empty($order->user_score_typification))
            <p><b>Tipificación</b>:</p>
            <ul>
                @foreach(explode(';', $order->user_score_typification) as $reason)
                    <li>{{ $reason }}</li>
                @endforeach
            </ul>
        @endif
        <div class="box box-primary">
            <div class="box-body table-responsive container-overflow">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Producto</th>
                        <th>Comentario</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($order->orderProducts))

                        @foreach($order->orderProducts as $product)
                            @if(!empty($product->orderProductScore))
                                <tr>
                                    <td>
                                        <img height="50px" src="{{ $product->product_image_url }}">
                                    </td>
                                    <td>
                                        {{ $product->product_name }}
                                        {{ $product->product_quantity }}
                                        {{ $product->product_unit }}
                                    </td>
                                    <td>{{ $product->orderProductScore->reason }}</td>
                                </tr>
                            @endif
                        @endforeach

                    @else
                        <tr>
                            <td colspan="3" align="center">
                                No hay productos disponibles.
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection
@endif
