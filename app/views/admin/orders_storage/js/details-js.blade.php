<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&key={{ Config::get('app.google_api_key') }}"></script>
<script>
    var city_id = {{ $order->city_id }};
    var store_id = @if($order->store_id) {{$order->store_id}} @else 0 @endif;
    var bogota_lat = 4.686806;
    var bogota_lng = -74.074663;
    var medellin_lat = 6.251366;
    var medellin_lng = -75.567139;
    var status_reject_reason = null;
    var data_table = [];

    function update_total_credit_note(total)
    {
        let html = '<p><label>Total nota crédito</label> - $' + total + '</p>'
        $('#total_credit_note').html(html)
        if(parseInt(total) > 0){
            $('#total_credit_note').show()
        } else {
            $('#total_credit_note').hide()
        }
    }

    function show_modal(action, reference)
    {
        reference = reference || 0;

        if (action == 'delivery-date' && $('#delivery_day option').length == 1)
        {
            var store_id = $('#store_id').val();
            var zone_id = $('#zone_id').val();
            $.ajax({
                url: "{{ route('adminOrderStorage.getStoreDeliverySlotAjax') }}",
                data: { store_id: store_id, zone_id: zone_id, validate_slots: 0, show_today: 1 },
                type: 'get',
                dataType: 'json',
                success:
                    function(response) {
                        $('#delivery_day').empty().append('<option value="">Selecciona el día</option>');
                        $('#delivery_time').empty().append('<option value="">Selecciona la hora</option>');
                        $.each(response[store_id].days, function(key, value){
                            $('#delivery_day').append('<option value="' + key + '">' + value + '</option>');
                        });
                        delivery_times = response[store_id].time;
                        $('*[name="delivery_day"]').trigger('change');
                    }
            });
        }

        if (action == 'order-product-group')
        {
            var order_product_id = reference;
            $.ajax({
                url: "{{ route('adminOrderStorage.getProductGroupAjax', ['id' => $order->id]) }}",
                data: { order_product_id: order_product_id },
                type: 'get',
                dataType: 'html',
                success:
                    function(response) {
                        $('#modal-order-product-group .modal-body').html(response);
                    }
            });
        }

        if(action == 'map-coords-user')
        {
            $('#modal-map-coords-user').on('shown.bs.modal', function() {
                var point = {lat: {{ $order->user_address_latitude }}, lng: {{ $order->user_address_longitude }}};
                var map = new google.maps.Map(document.getElementById('map-point'), {
                    zoom: 15,
                    center: point
                });
                var marker = new google.maps.Marker({
                    position: point,
                    map: map,
                    zoom: 15
                });
            });
        }

        if(action == 'order-payment-refunds')
        {
            $('#modal-order-payment-refunds').on('shown.bs.modal', function(){
                $.ajax({
                    url: "{{ route('adminOrderStorage.getOrdersPaymentRefunds') }}",
                    data: { order_payment_id : reference },
                    type: 'POST',
                    dataType: 'html',
                    success:
                        function(response){
                            $('#modal-order-payment-refunds').find('.modal-body').html(response);
                        }
                });
            });
        }

        if(action === 'score') {

            $.ajax({
                url: "{{ route('adminOrderStorage.getOrderProductsAjax', ['id' => $order->id]) }}",
                type: 'get',
                dataType: 'html',
                success: function(response) {
                    $('#modal-score .modal-body').html(response)
                }

            });
        }

        if(action == 'user-credits') {
            localStorage.removeItem('data_store_product');
            data_table = [];
            $('#products_related').val('');
            $('.div-table-data').hide();
        }

        if(action == 'promoter-orders' || action == 'referrals-of-promoter' || action == 'orders-per-address' || action == 'referrals-this-client') {
            $('#' + action + '-table').html('');
            $('#' + action + '-table').html('<center><img src="{{ asset_url() }}/img/loading.gif" /></center>');
            let url = "{{ route('adminOrderStorage.getDataPromoterAjax', ['PARAM_1', 'PARAM_2']) }}";
            url = url.replace('PARAM_1', reference).replace('PARAM_2', action);
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    if (response.status) {
                        let tr = $('<tr></tr>');
                        let thead = $('<thead></thead>');
                        let tbody = $('<tbody></tbody>');
                        if (response.action == 'promoter-orders') {
                            $('#promoter-orders-table').html('');
                            tr.append('<th># Pedido</th>');
                            tr.append('<th>Fecha pedido</th>');
                            tr.append('<th>Nombre</th>');
                            tr.append('<th>Dirección</th>');
                            tr.append('<th>Tienda</th>');
                            tr.append('<th>Total</th>');
                            tr.append('<th>Descuento</th>');
                            tr.append('<th>Estado</th>');
                            tr.append('<th>Origen</th>');
                            thead.append(tr);
                            $('#promoter-orders-table').append(thead);
                            $.each(response.result, function (key, value) {
                                let user_firstname = value.user_firstname || '';
                                let user_lastname = value.user_lastname || '';
                                let user_address = value.user_address || '';
                                let user_address_further = value.user_address_further || '';
                                let type = value.type || '';
                                let source = value.source || '';
                                let source_os = value.source_os || '';
                                let app_version = value.app_version || '';
                                let ip = value.ip || '';
                                let regid = value.regid || '';
                                let linkOrder = "{{ route('adminOrderStorage.details', ['PARAM_1']) }}";
                                linkOrder = linkOrder.replace('PARAM_1', value.id);
                                tr = $('<tr></tr>');
                                tr.append('<td><a href="' + linkOrder + '" target="_blank">' + value.id + '</a></td>');
                                tr.append('<td>' + moment(value.date).format('DD MMM Y h:mm a') + '</td>');
                                tr.append('<td>' + user_firstname + ' ' + user_lastname + '</td>');
                                tr.append('<td>' + user_address + ' ' + user_address_further + '</td>');
                                tr.append('<td>' + value.name + '</td>');
                                let ticket = parseInt(value.total_amount) + parseInt(value.delivery_amount) - parseInt(value.discount_amount);
                                tr.append('<td class="text-right">$' + Intl.NumberFormat().format(ticket) + '</td>');
                                tr.append('<td class="text-right">$' + Intl.NumberFormat().format(value.discount_amount) + '</td>');
                                switch (value.status) {
                                    case 'Validation':
                                        tr.append('<td><span class="badge bg-maroon">' + value.status + '</span></td>');
                                        break;
                                    case 'Delivered':
                                        tr.append('<td><span class="badge bg-green">' + value.status + '</span></td>');
                                        break;
                                    case 'Cancelled':
                                        tr.append('<td><span class="badge bg-red">' + value.status + '</span></td>');
                                        break;
                                    case 'Initiated':
                                        tr.append('<td><span class="badge bg-yellow">' + value.status + '</span></td>');
                                        break;
                                    case 'Dispatched':
                                        tr.append('<td><span class="badge bg-yellow">' + value.status + '</span></td>');
                                        break;
                                    case 'In Progress':
                                        tr.append('<td><span class="badge bg-orange">' + value.status + '</span></td>');
                                        break;
                                    case 'Alistado':
                                        tr.append('<td><span class="badge bg-green">' + value.status + '</span></td>');
                                        break;
                                    case 'Enrutado':
                                        tr.append('<td><span class="badge bg-orange">' + value.status + '</span></td>');
                                        break;
                                }
                                if(type == '' && source == '') {
                                    tr.append('<td></td>');
                                } else {
                                    tr.append('<td>' + type + ' / ' + source + ' ' + source_os + ' ' + app_version + ' / ' + ip + ' / ' + regid + '</td>');
                                }
                                tbody.append(tr);
                            });
                            $('#promoter-orders-table').append(tbody);
                        } else if(response.action == 'referrals-of-promoter') {
                            $('#referrals-of-promoter-table').html('');
                            tr.append('<th>User ID</th>');
                            tr.append('<th>Nombre</th>');
                            tr.append('<th>Correo</th>');
                            tr.append('<th>Celular</th>');
                            tr.append('<th>Fecha validación</th>');
                            tr.append('<th>Dirección</th>');
                            tr.append('<th>Origen</th>');
                            thead.append(tr);
                            $('#referrals-of-promoter-table').append(thead);
                            $.each(response.result, function (key, value) {
                                let user_address = value.user_address || '';
                                let user_address_further = value.user_address_further || '';
                                let type = value.type || '';
                                let source = value.source || '';
                                let source_os = value.source_os || '';
                                let app_version = value.app_version || '';
                                let ip = value.ip || '';
                                let regid = value.regid || '';
                                tr = $('<tr></tr>');
                                tr.append('<td>' + value.id + '</td>');
                                tr.append('<td>' + value.first_name + ' ' + value.last_name + '</td>');
                                tr.append('<td>' + value.email + '</td>');
                                tr.append('<td>' + value.phone + '</td>');
                                tr.append('<td>' + value.phone_validated_date + '</td>');
                                tr.append('<td>' + user_address + ' ' + user_address_further + '</td>');
                                if(type == '' && source == '') {
                                    tr.append('<td></td>');
                                } else {
                                    tr.append('<td>' + type + ' / ' + source + ' ' + source_os + ' ' + app_version + ' / ' + ip + ' / ' + regid + '</td>');
                                }
                                tbody.append(tr);
                            });
                            tr = $('<tr></tr>');
                            tr.append('<th colspan="6">Total referidos</th><td>' + response.result.length + '</td>');
                            tbody.append(tr);
                            $('#referrals-of-promoter-table').append(tbody);
                        } else if (response.action == 'orders-per-address') {
                            $('#orders-per-address-table').html('');
                            tr.append('<th># Pedido</th>');
                            tr.append('<th>User ID</th>');
                            tr.append('<th>Fecha del pedido</th>')
                            tr.append('<th>Nombre</th>');
                            tr.append('<th>Tienda</th>');
                            tr.append('<th>Total</th>');
                            tr.append('<th>Estado</th>');
                            tr.append('<th>Origen</th>');
                            thead.append(tr);
                            $('#orders-per-address-table').append(thead);
                            $.each(response.result, function (key, value) {
                                let user_firstname = value.user_firstname || '';
                                let user_lastname = value.user_lastname || '';
                                let type = value.type || '';
                                let source = value.source || '';
                                let source_os = value.source_os || '';
                                let app_version = value.app_version || '';
                                let ip = value.ip || '';
                                let regid = value.regid || '';
                                let linkOrder = "{{ route('adminOrderStorage.details', ['PARAM_1']) }}";
                                linkOrder = linkOrder.replace('PARAM_1', value.id);
                                tr = $('<tr></tr>');
                                tr.append('<td><a href="' + linkOrder + '" target="_blank">' + value.id + '</a></td>');
                                tr.append('<td>' + value.user_id + '</td>');
                                tr.append('<td>' + moment(value.date).format('DD MMM Y h:mm a') + '</td>');
                                tr.append('<td>' + user_firstname + ' ' + user_lastname + '</td>');
                                tr.append('<td>' + value.name + '</td>');
                                let ticket = parseInt(value.total_amount) + parseInt(value.delivery_amount) - parseInt(value.discount_amount);
                                tr.append('<td class="text-right">$' + Intl.NumberFormat().format(ticket) + '</td>');
                                switch (value.status) {
                                    case 'Validation':
                                        tr.append('<td><span class="badge bg-maroon">' + value.status + '</span></td>');
                                        break;
                                    case 'Delivered':
                                        tr.append('<td><span class="badge bg-green">' + value.status + '</span></td>');
                                        break;
                                    case 'Cancelled':
                                        tr.append('<td><span class="badge bg-red">' + value.status + '</span></td>');
                                        break;
                                    case 'Initiated':
                                        tr.append('<td><span class="badge bg-yellow">' + value.status + '</span></td>');
                                        break;
                                    case 'Dispatched':
                                        tr.append('<td><span class="badge bg-yellow">' + value.status + '</span></td>');
                                        break;
                                    case 'In Progress':
                                        tr.append('<td><span class="badge bg-orange">' + value.status + '</span></td>');
                                        break;
                                    case 'Alistado':
                                        tr.append('<td><span class="badge bg-green">' + value.status + '</span></td>');
                                        break;
                                    case 'Enrutado':
                                        tr.append('<td><span class="badge bg-orange">' + value.status + '</span></td>');
                                        break;
                                }
                                if(type == '' && source == '') {
                                    tr.append('<td></td>');
                                } else {
                                    tr.append('<td>' + type + ' / ' + source + ' ' + source_os + ' ' + app_version + ' / ' + ip + ' / ' + regid + '</td>');
                                }
                                tbody.append(tr);
                            });
                            $('#orders-per-address-table').append(tbody);
                        } else if (response.action == 'referrals-this-client') {
                            $('#referrals-this-client-table').html('');
                            tr.append('<th>User ID</th>');
                            tr.append('<th>Nombre</th>');
                            tr.append('<th>Correo</th>');
                            tr.append('<th>Celular</th>');
                            tr.append('<th>Fecha validación</th>');
                            tr.append('<th>Dirección</th>');
                            tr.append('<th>Origen</th>');
                            thead.append(tr);
                            $('#referrals-this-client-table').append(thead);
                            $.each(response.result, function (key, value) {
                                let type = value.type || '';
                                let user_address = value.user_address || '';
                                let user_address_further = value.user_address_further || '';
                                let source = value.source || '';
                                let source_os = value.source_os || '';
                                let app_version = value.app_version || '';
                                let ip = value.ip || '';
                                let regid = value.regid || '';
                                tr = $('<tr></tr>');
                                tr.append('<td>' + value.id + '</td>');
                                tr.append('<td>' + value.first_name + ' ' + value.last_name + '</td>');
                                tr.append('<td>' + value.email + '</td>');
                                tr.append('<td>' + value.phone + '</td>');
                                tr.append('<td>' + value.phone_validated_date + '</td>');
                                tr.append('<td>' + user_address + ' ' + user_address_further + '</td>');
                                if(type == '' && source == '') {
                                    tr.append('<td></td>');
                                } else {
                                    tr.append('<td>' + type + ' / ' + source + ' ' + source_os + ' ' + app_version + ' / ' + ip + ' / ' + regid + '</td>');
                                }
                                tbody.append(tr);
                            });
                            tr = $('<tr></tr>');
                            tr.append('<th colspan="6">Total referidos</th><td>' + response.result.length + '</td>');
                            tbody.append(tr);
                            $('#referrals-this-client-table').append(tbody);
                        }
                    }
                }

            });
        }

        $('#modal-' + action).modal('show');
    }

    $(function()
    {
        $('#modal-track').on('shown.bs.modal', function() {
            if (shopper_obj && shopper_obj.location) {
                var lat_lng = shopper_obj.location.split(' ');
                var lat_lng2 = order_address.split(' ');
                mapInit(lat_lng[0], lat_lng[1], 'map', false, map_track);
                setMarker(map_track, lat_lng[0], lat_lng[1], 'shopper', shopper_obj.first_name+' '+shopper_obj.last_name);
                setMarker(map_track, lat_lng2[0], lat_lng2[1], 'default', 'Sitio de entrega');
            }else{
                if ( city_id == 1 ) {
                    mapInit(bogota_lat, bogota_lng, 'map', false, map_track);
                }else{
                    mapInit(medellin_lat, medellin_lng, 'map', false, map_track);
                }
            }
        });

        $('.modal-invoice').click(function(){
            show_modal('update-user-invoice-data');
        });

        $('#invoice-data-form').validate({
            rules: {
                user_identity_number: "required",
                user_business_name: "required"
            }
        });
        $('#update-transporter').validate();

        $('.view-invoice').on('click',function(){
            var url = $(this).data('href');
            window.open(url,'_blank');
        });

        $('body').on('change', '#reason-returned', function(){
            if($(this).val() != 'Errado')
                $('body .reference-returned').addClass('unseen');
            else
                $('body .reference-returned').removeClass('unseen');
        });
    });

    function return_product(order_id, product_id, product_type){
        $('#order-id').val(order_id);
        $('#product-id').val(product_id);
        $('#product-type').val(product_type);
        $('#returns-modal').modal('show');
    }

    function edit(product_id, quantity){
        $('#order-product-id').val(product_id);
        $('#order-product-quantity').val(quantity);
        $('#edit-modal').modal('show');
    }

    function replace(product_id){
        $('#order-product-parent-id').val(product_id);
        $('#replace-modal').modal('show');
    }

    $(function() {

        $('#modal-new-status').change(function(){
            if ($(this).val() == 'Cancelled'){
                $('.reject-block').show();
            }else{
                $('.reject-block').hide();
            }
        });
        $('#modal-new.status').trigger('change');

        $('.delivery_day').on('change', function() {
            $('#delivery_time option').remove();
            $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
            if ($(this).val() != '') {
                times = delivery_times[$(this).val()];
                $.each(times, function(value, data) {
                    $('#delivery_time').append($("<option></option>").attr('value', value).text(data.text));
                });
                $('.delivery_time').show();
            }else{
                $('.delivery_time').hide();
            }
        });
        $('.delivery_day').trigger('change');

        $('.reschedule_delivery_day').on('change', function() {
            if ($(this).val() == 'today') {
                $('.reschedule_delivery_window_id').show();
            }else{
                $('.reschedule_delivery_window_id').hide();
            }
        });
        $('.delivery_day').trigger('change');

        $('body').on('change', '#reject_reason', function() {
            $('.reject_reason_futher_data option').remove();
            status_reject_reason = $(this).val();
            $.ajax({
                url: "{{ route('adminOrderStorage.getRejectReasonAjax') }}",
                data: { status: status_reject_reason},
                type: 'get',
                dataType: 'json',
                success:
                    function(response) {
                        if(response){
                            $('body').find('.reject_reason_futher_data').empty().append('<option value="" selected>-Selecciona-</option>');
                            $.each(response, function(key, value){
                                $('.reject_reason_futher_data').append('<option value="' + value.reason + '" data-id="'+value.id+'">' + value.reason + '</option>');
                            });
                        }
                    }
            });
            if ($(this).val() != '') {
                $('.reject_reason_futher').show();
            }else{
                $('.reject_reason_futher').hide();
            }
        });
        $('body').on('change', '#reject_reason_futher', function () {
            var reject_reason_id = $(this).find('option:selected').data('id');
            $('#reject_reason_futher_id').val(reject_reason_id);
        })

        $('body').on('change', '#reject_reason_futher', function() {
            console.log('1890: ',status_reject_reason);
            if (status_reject_reason !== 'Cancelar' && status_reject_reason !== 'Reprogramar') {
                if ($(this).val() === 'Fuera de franja programada' || $(this).val() === 'No se encuentra usuario en franja horaria') {
                    $('.arrival_image_url').removeClass('unseen').attr('required', 'required');
                    $('#arrival_image_url').attr('required', 'required');
                    $('.arrival_image_url').validate({rules: {arrival_image_url: 'required'}});
                } else {
                    $('.arrival_image_url').removeClass('unseen').addClass('unseen');
                    $('#arrival_image_url').removeAttrs('required');
                }
            } else {
                $('#arrival_image_url').removeAttrs('required');
            }
        });

        $('.payment_method_delivered').on('change', function() {
            if ($(this).val() != 'Efectivo y datáfono') {
                $('.customer-block-cash-card').hide();
                $('.customer-cash-paid').val('');
                $('.customer-card-paid').val('');
            } else{
                $('.customer-block-cash-card').show();
            }
        });
        $('.payment_method_delivered').trigger('change');

        $('.form-modal').on('submit', function(e) {
            var error = false;
            var form = this;

            $('input[type!="hidden"], select', this).each(function() {
                if ($(this).is(':visible') && $(this).val().length == 0 && $(this).hasClass('required')) {
                    $(this).parent().addClass('has-error');
                    error = 'Completa los campos requeridos.';
                }else $(this).parent().removeClass('has-error');
            });

            if(error) {
                $('.form-has-errors', form).html(error).fadeIn();
                return false;
            }
            return true;
        });

        $('body').on('click', '#allocate_form .btn-primary', function(event) {
            event.preventDefault();
            $('#allocate-shopper-name').removeAttr('readonly');
            $('#allocate-store-branch-name').removeAttr('readonly');
            if($('#allocate_form').valid()){
                $('#allocate_form').submit();
            }
            $('#allocate-shopper-name').attr('readonly', true);
            $('#allocate-store-branch-name').attr('readonly', true);
        });

        $('#re-send-email').submit(function (argument) {
            if (confirm('¿Estas seguro que deseas enviar el correo nuevamente al cliente?')) {
                return true;
            }
            return false;
        });

        $('body').on('click', '#send-sms-action', function(event) {
            $('#modal-send-sms').modal('show');
        });

        $('#send-sms').validate();
        $('#send-sms').submit(function(event) {
            if ( confirm('¿Desea enviar el mensaje de texto al cliente?') ) {
                return true;
            }
            return false;
        });

        $('#update-price-custom-product').validate();

        $('body').on('click', '.update-product', function(){
            var product_id = $(this).data('product');
            var status = $(this).data('status');
            var type = $(this).data('type');
            $('.btn-group button').addClass('disabled');
            $.ajax({
                url: '{{ route('adminOrderStorage.updateProductAjax') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    id: product_id,
                    status: status,
                    type: type
                },
            })
                .done(function(data) {
                    if(data.status){
                        if(data.product_status  == 'Fullfilled'){
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-orange');
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-maroon');
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-red');
                            $('.badge').parents().find('#status-'+product_id+' .badge').addClass('bg-green');
                            $('.badge').parents().find('#status-'+product_id+' .badge').html('Disponible');
                        }else{
                            if(data.product_status  == 'Not Available'){
                                $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-orange');
                                $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-maroon');
                                $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-green');
                                $('.badge').parents().find('#status-'+product_id+' .badge').addClass('bg-red');
                                $('.badge').parents().find('#status-'+product_id+' .badge').html('No disponible');
                            }else{
                                if(data.product_status  == 'Missing'){
                                    $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-orange');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-red');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-green');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').addClass('bg-maroon');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').html('Faltante');
                                }else{
                                    $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-orange');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-maroon');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-green');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').addClass('bg-red');
                                    $('.badge').parents().find('#status-'+product_id+' .badge').html('Devolución');
                                }
                            }
                        }

                        $('#total_amount').html(data.payment_data.total_amount);
                        $('#delivery_amount').html(data.payment_data.delivery_amount);
                        $('#discount_amount').html(data.payment_data.discount_amount);
                        $('#total').html(data.payment_data.total);
                    }else{
                        alert(data.message);
                    }
                })
                .fail(function(data) {
                    $('.btn-group button').removeClass('disabled');
                    console.log("error");
                })
                .always(function(data) {
                    $('.btn-group button').removeClass('disabled');
                    //console.log("complete");
                });
        });

        $('body').on('change', '#transporter_id', function() {
            $('#vehicle_id').empty().append('<option value="">Selecciona</option>');
            var transporter_id = $(this).val();
            var form_id = $(this).parent().parent().parent().attr('id');
            $.ajax({
                url: '{{ route('adminOrderStorage.getDriversVehiclesByTransportersAjax') }}',
                type: 'GET',
                dataType: 'json',
                data: {
                    transporter_id: transporter_id
                },
            })
                .done(function(data) {
                    var html = '';
                    if (data.vehicles) {
                        $.each(data.vehicles, function(index, val) {
                            html += '<option value="'+ val.id +'">'+ val.plate +'</option>';
                        });
                        $('#' + form_id + ' #vehicle_id').append(html);
                    }
                })
                .fail(function() {
                    console.log("Error al obtener los vehículos");
                });
        });

        $('body').on('click', '#btn-modal-search', function(){
            if ($('.modal-search').val() != ''){
                // alert('search');
                // search_modal($('.modal-search').val());
                $('.modal-products-request-loading').show();
                var search = $('.modal-search').val();
                var store_id = $(this).data('store_id');
                var order_id = $(this).data('order_id');
                $.ajax({
                    url: '{{ route('adminOrderStorage.searchProductAjax') }}',
                    data: {
                        s: search,
                        store_id: store_id,
                        order_id: order_id
                    },
                    type: 'GET',
                    dataType: 'html',
                    success: function(data) {
                        $('.modal-products-request-loading').hide();
                        $('.modal-products-request .tbody').html(data);
                    }, error: function() {
                        $('.modal-products-request-loading').hide();
                        $('.modal-products-request .tbody').html('Ocurrió un error al obtener los datos.');
                    }
                });
            }else{
                $('.modal-search').css({ border:'solid 1px #ff0000'});
                $('.error').show();
            }
        });

        function add_product_ajax(data) {
            $.ajax({
                url: '{{ route('adminOrderStorage.addProductAjax') }}',
                type: 'POST',
                dataType: 'json',
                data: data
            })
                .done(function(response) {
                    console.debug(response);
                    if (response.status == true) {
                        $('.error-search').show().find('.alert-danger').hide();
                        $('.error-search').show().find('.alert-success').html(response.message).show();
                        location.reload();
                    }else{
                        var res = confirm(response.message);
                        if ( res ) {
                            data.update_quantity = true;
                            add_product_ajax(data);
                            return;
                        }else{
                            $('.error-search').show().find('.alert-success').hide();
                            $('.error-search').show().find('.alert-danger').html('No se ha agregado el producto.').show();
                        }
                    }
                    return;
                })
                .fail(function() {
                    console.log("error al agregar el producto");
                });
        }

        $('body').on('click', '.btn-add-product', function(event) {
            event.preventDefault();
            var store_id = $(this).data('store_id');
            var order_id = $(this).data('order_id');
            var store_product_id = $(this).data('store_product_id');
            var current_stock = $(this).data('current_stock');
            var cant = $(this).parents('tr').find('.product_cant').val();
            /*if (cant > current_stock) {
                $('.error-search').show().find('.alert-success').hide();
                $('.error-search').show().find('.alert-danger').html('El valor ingresado es mayor al stock del producto').show();
                return;
            }*/
            if (cant > 0) {
                var data = {
                    store_product_id: store_product_id,
                    order_id: order_id,
                    store_id: store_id,
                    cant: cant
                };
                add_product_ajax(data);
            }else{
                $('.error-search').show().find('.alert-sucess').hide();
                $('.error-search').show().find('.alert-danger').html('La cantidad de producto debe ser mayor a cero.').show();
            }
        });

        localStorage.removeItem('data_store_product');
        $('.div-table-data').hide();
        $('body').on('click', '.btn-add-product-to-credit', function (e){
            e.preventDefault();
            $('.div-table-data').hide();
            $('.content-table-data').html('');
            let store_product_id = $(this).data('store-product-id');
            let url_image = $(this).data('url-image');
            let product_reference = $(this).data('product-reference');
            let product_name = $(this).data('product-name');
            let index = _.findIndex(data_table, function(data_store_product) {return data_store_product.store_product_id === store_product_id;});
            if (index === -1) {
                data_table.push({store_product_id: store_product_id, url_image: url_image, reference: product_reference, name: product_name});
            }
            localStorage.setItem('data_store_product', JSON.stringify(data_table));
            let table_data = localStorage.getItem('data_store_product');
            let text_product_related = '';
            $('#products_related').val('');
            $.each(JSON.parse(table_data), function (index, value) {
                let tr = $('<tr></tr>');
                tr.append('<td><img src="' + value.url_image + '" height="50px"/></td>');
                tr.append('<td>' + value.reference + '</td>');
                tr.append('<td>' + value.name + '</td>');
                tr.append('<td><a class="btn btn-xs btn-danger btn-remove-product-to-credit" href="javascript:;" data-store-product-id="' + value.store_product_id + '"><span class="glyphicon glyphicon-remove"></span></a></td>');
                $('.content-table-data').append(tr);
                text_product_related += value.store_product_id + ' - ' + value.name + ', ';
            });
            $('#products_related').val(text_product_related);
            $('.error-search').show().find('.alert-danger').hide();
            $('.error-search').show().find('.alert-success').html('El producto se agregó correctamente').show().delay(3000).hide('slow');
            $('.div-table-data').show();
        });

        $('body').on('click', '.btn-remove-product-to-credit', function(e) {
            e.preventDefault();
            let store_product_id = $(this).data('store-product-id');
            let index = _.findIndex(data_table, function(data_store_product) {return data_store_product.store_product_id === store_product_id;});
            if (index !== -1) {
                data_table.splice(index, 1);
            }
            localStorage.setItem('data_store_product', JSON.stringify(data_table));
            let table_data = localStorage.getItem('data_store_product');
            let text_product_related = '';
            $('.content-table-data').html('');
            $('#products_related').val('');
            table_data = JSON.parse(table_data);
            if (table_data.length) {
                $.each(table_data, function (index, value) {
                    let tr = $('<tr></tr>');
                    tr.append('<td><img src="' + value.url_image + '" height="50px"/></td>');
                    tr.append('<td>' + value.reference + '</td>');
                    tr.append('<td>' + value.name + '</td>');
                    tr.append('<td><a class="btn btn-xs btn-danger btn-remove-product-to-credit" href="javascript:;" data-store-product-id="' + value.store_product_id + '"><span class="glyphicon glyphicon-remove"></span></a></td>');
                    $('.content-table-data').append(tr);
                    text_product_related += value.store_product_id + ' - ' + value.name + ', ';
                });
                $('#products_related').val(text_product_related);
                $('.div-table-data').show();
            } else {
                $('.div-table-data').hide();
            }
            $('.alert-success').show().find('.message').html('El producto se removió correctamente').parent().delay(3000).hide('slow');
        });

        var productActions = (function() {
            'use strict';

            function productActions() {
                // enforces new
                if (!(this instanceof productActions)) {
                    return new productActions();
                }
                // constructor body
                this.bindActions();
            }
            productActions.prototype.bindActions = function(first_argument) {
                var self = this;
                $('body').on('click', '.remove-product', function(event) {
                    event.preventDefault();
                    var is_delete = confirm('¿Deseas eliminar este producto del pedido?');
                    if ( is_delete ) {
                        window.location.href = $(this).attr('href');
                    }
                });
            };

            return productActions;
        }());
        var product_actions = new productActions;

        var paymentMethod = (function() {
            'use strict';

            function paymentMethod(args) {
                // enforces new
                if (!(this instanceof paymentMethod)) {
                    return new paymentMethod(args);
                }
                // constructor body
                this.bindActions();
            }

            paymentMethod.prototype.bindActions = function(args) {
                $('.payment_method').on('change', function() {
                    $('.credit_cards, .card_cash').hide();
                    if ( $(this).val() == 'Tarjeta de crédito' ) {
                        $('.credit_cards').show();
                    }
                    if ( $(this).val() == 'Efectivo y datáfono' ){
                        $('.card_cash').show();
                    }
                });
                $('.payment_method').trigger('change');
            };

            return paymentMethod;
        }());
        var payment_method = new paymentMethod;
    });

    var display_message = function( $class, $message ){
        $( $class ).find('div.message').html( $message );
        $( $class ).show();
        $( $class ).delay(3200).fadeOut()
    }

    $(document).ready( function(){

        $('#expiration_date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        $('body').on('submit', '#update-to-enrutado', function(){
            $('.save-status').prop('disabled',true)
        });
    });
    $(".btn-user-credits").on('click', function(){
        $("#main-form-user-credits").submit();
    })
    $("#main-form-user-credits").validate({
        rules : {
            amount :{
                required : true,
                number : true,
                max : 100000,
                min : 0
            },
            expiration_date : {
                required: function (element) {
                    if ($("#type").val() == 1)
                        return true;
                    else
                        return false;
                }
            },
            type : "required",
            claim_motive: "required",
        },
        messages : {
            amount : {
                required : "Este campo es obligatorio",
                number : "Se requiere un valor númerico",
                max : "Debe ingresar un valor igual o menor a $100.000",
                min : "Debe ingresar un valor igual mayor a $0."
            },
            expiration_date : {
                required : "Este campo es obligatorio",
                //date : "Debe ingresar una fecha valida en formato DD/MM/YYYY"
            },
            type : {
                required : "Este campo es obligatorio"
            },
            claim_motive: {
                required : "Este campo es obligatorio"
            },
        },
        submitHandler : function( form ) {
            $('.alert.alert-success').hide();
            $('.alert.alert-danger').hide();
            if (confirm('¿Esta seguro de asignar este cupo de crédito?')){
                $.ajax({
                    url : '{{ route('adminCustomers.addCredit', ['id' => $order->user_id]) }}',
                    type : 'POST',
                    data : $( form ).serialize(),
                    dataType : 'json',
                    success : function( response ){
                        if( response.status == true ){
                            display_message( '.alert.alert-success', response.message );
                            $('.current-credit').val(response.result);
                            $('#type').val('');
                            $('#amount').val('');
                            $('#expiration_date').val('');
                            $('#claim_motive').val('');
                            $('#products_related').val('');
                            localStorage.removeItem('data_store_product');
                            $('.content-table-data').html('');
                            $('.div-table-data').hide();
                        }else{
                            display_message( '.alert.alert-danger', 'Ocurrió un error.');
                        }
                    },
                    error : function( response ){
                        display_message( '.alert.alert-danger', 'Ocurrió un error.');
                    },
                });
            }
            //form.submit();
        }
    });
</script>