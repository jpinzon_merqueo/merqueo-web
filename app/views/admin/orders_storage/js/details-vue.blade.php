<script>
    new Vue({
        el: '#content-credit-note',
        data: {
            formData:{
                productName: '',
                creditNoteReasons: [
                    {'reason': 'Devolución por avería'},
                    {'reason': 'Devolución por mal estado'},
                    {'reason': 'Devolución por productos no recibidos'},
                    {'reason': 'Devolución por fecha de vencimiento'},
                    {'reason': 'Devolución por diferencia en precio'}
                ],
                errors: {
                    quantity_calculate: {
                        error: false,
                        menssage:  '',
                        index: 0,
                    }
                }
            },
            dsearch: false,
            dsave: false,
            dremove: false,
            load: false,
            productSearch: [],
            addProductsCreditNote: [],
            productsCreditNote: [],
            total_price: 0,
            total_iva: 0,
            credit_note_number: {{ ($order->credit_note_number) ? $order->credit_note_number : 0 }},
            total: 0
        },
        mounted(){
            this.getProductsCreditNote()
        },
        methods:{
            formatPrice(value) {
                let val = (value/1).toFixed(2).replace('.', ',')
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            calculateAmountProductsCreditNote: function(products) {
                let total_price = 0
                let total_iva = 0
                products.forEach(function (product) {
                    product.sub_total_price = product.base_price * product.quantity_credit_note
                    product.iva_amount = (product.sub_total_price * product.iva) / 100
                    product.total_price = product.price * product.quantity_credit_note
                    total_price += product.sub_total_price
                    total_iva += product.base_price * (product.iva / 100)
                })
                this.total_price = total_price
                this.total_iva = total_iva
                this.total = total_iva + total_price
                this.productsCreditNote = products
                update_total_credit_note(this.formatPrice(this.total))
            },
            calculateAmountPreviewCreditNote: function (product) {
                product.sub_total_price = product.base_price * product.quantity_calculate
                product.iva_amount = (product.sub_total_price * product.iva) / 100
                product.total_price = product.price * product.quantity_calculate
                return product
            },
            getProductsCreditNote: function () {
                axios.get('{{ route('adminOrderStorage.getProductsCreditNote', ['id' => $order->id]) }}')
                    .then(response => {
                        this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                    }).catch(error => {
                    alert('Error al traer los productos para la nota crédito')
                })

            },
            searchProducts: function () {
                let name = this.formData.productName
                let excludeIds = this.productsAddedIds
                this.dsearch = true
                axios.get('{{ route('adminOrderStorage.getProductsCreditNote', ['id' => $order->id]) }}', {
                    params: {
                        productName: name,
                        excludeIds: JSON.stringify(excludeIds)
                    }
                })
                    .then(response => {
                        response.data.productSearch.forEach(function (product) {
                            product.quantity_calculate = product.quantity_original - product.quantity
                            product.reason_credit_note = 'Devolución por avería'
                        })
                        this.productSearch = response.data.productSearch
                        this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                        this.dsearch = false
                    }).catch(error => {
                        alert('Error al traer el producto buscado')
                        this.dsearch = true
                    })
            },
            validateForm: function(product, index) {
                let validate = {
                    errors:{
                        quantity_calculate: {
                            error: (product.quantity_calculate <= 0 || product.quantity_calculate == null || product.quantity_calculate > product.quantity_original),
                            message: (product.quantity_calculate === '') ? 'Campo númerico requerido' :
                                (product.quantity_calculate > product.quantity_original) ? 'La cantidad en la nota crédito no puede ser mayor a la cantidad pedida' :
                                    'El campo debe ser mayor a cero',
                            index: index
                        }
                    }
                }
                if(validate.errors.quantity_calculate.error){
                    this.formData.errors = validate.errors
                    return false
                }
                this.resetErrorsForm(product,index)
                return true
            },
            resetErrorsForm: function(product, index) {
                let reset = {
                    errors: {
                        quantity_calculate: {
                            error: false,
                            menssage: '',
                            index: index,
                        }
                    }
                }
                this.formData.errors = reset.errors
            },
            addCreditNote: function(product, index) {
                if(this.validateForm(product,index)) {
                    this.addProductsCreditNote.push(this.calculateAmountPreviewCreditNote(product))
                    this.productSearch.splice(index, 1)
                    this.resetErrorsForm(product, index)
                }
            },
            removeAddCreditNote: function(product, index){
                this.productSearch.push(product)
                this.addProductsCreditNote.splice(index, 1)
            },
            removeCreditNote: function(product){
                let confirmation = confirm('¿Estas seguro que deseas eliminar el producto de la nota crédito?')
                if(confirmation) {
                    this.dremove = true
                    let route = '{{ route('adminOrderStorage.orderUpdateCreditNote', ['id' => $order->id]) }}'
                    let data = {
                        products: product,
                        action: 'delete'
                    }
                    axios({
                        url: route,
                        method: 'POST',
                        data: data
                    })
                        .then(response => {
                            this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                            this.addProductsCreditNote = response.data.addProductsCreditNote
                            alert('Producto eliminado de la nota crédito con éxito.')
                            this.dremove = false
                        })
                        .catch(error => {
                            alert('Error al quitar productos de la nota crédito.')
                            this.dremove = false
                        })
                }
            },
            updateProductsCreditNote: function () {
                let confirmation = confirm('¿Estas seguro que deseas agregar los productos a la nota crédito?')
                if(confirmation) {
                    this.dsave = true
                    this.load = true
                    let route = '{{ route('adminOrderStorage.orderUpdateCreditNote', ['id' => $order->id]) }}'
                    let data = {
                        products: this.productsAddedSubmit,
                        action: 'edit'
                    }
                    axios({
                        url: route,
                        method: 'POST',
                        data: data
                    })
                        .then(response => {
                            this.calculateAmountProductsCreditNote(response.data.productsCreditNote)
                            this.addProductsCreditNote = response.data.addProductsCreditNote
                            alert('Productos agregados a la nota crédito con éxito.')
                            this.dsave = false
                            this.load = false
                        })
                        .catch(error => {
                            alert('Error al guardar productos en la nota crédito.')
                            this.dsave = false
                            this.load = false
                        })
                }
            }
        },
        computed: {
            productsAddedIds: function () {
                let addProductsCreditNote = this.addProductsCreditNote.map(product => {return product.id})
                let productsCredditNote = this.productsCreditNote.map(product => {return product.id})
                return productsCredditNote.concat(addProductsCreditNote)
            },
            productsAddedSubmit: function () {
                return this.addProductsCreditNote.map(product => {
                    return {
                        id: product.id,
                        quantity_calculate: product.quantity_calculate,
                        reason_credit_note: product.reason_credit_note,
                        type: product.type
                    }
                })
            },
            disableDivAdd: function () {
                return (this.addProductsCreditNote.length == 0) ? false : true
            },
            disableDivProductsCN: function() {
                return (this.productsCreditNote.length == 0) ? false : true
            },
            disableSave: function() {
                return (this.addProductsCreditNote.length == 0 || this.dsave) ? true : false
            },
            disableRemove: function() {
                return this.dremove
            },
            disabledSearch: function() {
                return this.dsearch
            },
            loader: function () {
                return this.load
            }
        }
    })
</script>