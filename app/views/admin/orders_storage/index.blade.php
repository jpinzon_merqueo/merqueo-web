@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<!--<link rel="stylesheet" type="text/css" href="{{asset_url()}}css/style.css">-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script>
    var web_url_ajax = "{{ route('adminOrderStorage.index') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
@if(Session::has('success') )
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Hecho!</b> {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error') )
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alerta!</b> {{ Session::get('error') }}
</div>
@endif
    <style>
        @media (min-width: 768px) {
            .form-group{
                min-height: 67px;
                /*border: 1px solid #cccc77;*/
            }
            .form-group label{
                display: block;
            }
        }
    </style>
	<div class="row">
	    <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                	<div class="row">
                	    <div class="col-xs-12">
                	        <form id="search-form form-horizontal">
                                <div class="form-group col-md-4">
                                    <label>Ciudad:</label>
                                    <select id="city_id" name="city_id" class="form-control get-warehouses">
                                        <option value="">Selecciona</option>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Transportador:</label>
                                    <select id="transporter_id" name="transporter_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Alistador seco:</label>
                                    <select id="picker_dry_id" name="picker_dry_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Tienda:</label>
                                    <select id="store_id" name="store_id" class="form-control" style="width: auto; display: inline-block">
                                        @if (count($stores))
                                            @foreach ($stores as $store)
                                                @if ( $store->city_id == Session::get('admin_city_id') )
                                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <select id="warehouse_id" name="warehouse_id" class="form-control" style="width: auto; display: inline-block">
                                        <option value="">Selecciona</option>
                                        @if (count($warehouses))
                                            @foreach ($warehouses as $index => $value)
                                                <option value="{{ $value->id }}">{{ $value->warehouse }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                                <div class="form-group col-md-4">
                                    <label>Vehículo:</label>
                                    <select id="vehicle_id" name="vehicle_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Método de pago:</label>
                                    <select id="payment_method" name="payment_method" class="form-control">
                                        <option value="">Selecciona</option>
                                        @if (count($payment_methods))
                                            @foreach ($payment_methods as $index => $value)
                                                <option value="{{ $index }}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Origen:</label>
                                    <select id="type" name="type" class="form-control" style="width: 49%; display: inline-block">
                                        <option value="">Selecciona</option>
                                        <option value="Merqueo" selected="selected">Merqueo</option>
                                        <option value="Marketplace">Marketplace</option>
                                    </select>
                                    <select id="source" name="source" class="form-control" style="width: 49%; display: inline-block">
                                        <option value="">Selecciona</option>
                                        @foreach ($sources as $source)
                                            <option value="{{ $source }}">{{ $source }}</option>
                                        @endforeach
                                    </select>
                                    <select id="source_os" name="source_os" class="unseen form-control" style="width: auto;">
                                        <option value="">Selecciona</option>
                                        <option value="iOS">iOS</option>
                                        <option value="Android">Android</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Conductor:</label>
                                    <select id="driver_id" name="driver_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Ruta:</label>
                                    <select id="route_id" name="route_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Buscar:</label>
                                    <input type="text" placeholder="ID, dirección, nombre, celular, email, referencia" class="search form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Hora de entrega:</label>
                                    <select id="delivery_window_id" class="form-control">
                                        <option value="">Selecciona</option>
                                        <optgroup label="Franjas normales">
                                            @foreach($delivery_windows as $delivery_window)
                                                <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Mismo día">
                                            @foreach($delivery_windows_same_day as $delivery_window)
                                                <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Zona:</label>
                                    <select id="zone_id" name="zone_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Fecha de entrega:</label>
                                    <input style="display: inline-block; width: 49%;" type="text" placeholder="DD/MM/YYYY" id="delivery_date" class="form-control" value="{{date('d/m/Y')}}">
                                    <input style="display: inline-block; width: 49%;" type="text" placeholder="DD/MM/YYYY" id="delivery_date_end" class="form-control" value="{{date('d/m/Y')}}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label style="">Motivo de cancelación:</label>
                                    <select id="reject_reason" name="reject_reason" class="form-control">
                                        <option value="">Selecciona</option>
                                        <option value="Cancelado definitivamente">Cancelado definitivamente</option>
                                        <option value="Cancelado temporalmente">Cancelado temporalmente</option>
                                        <option value="Reprogramación">Reprogramación</option>
                                        <option value="Cancelado por conductor">Cancelado por conductor</option>
                                        <option value="Reprogramar">Reprogramar</option>
                                        <option value="Cancelar">Cancelar</option>
                                        <option value="Pausar">Pausar</option>
                                        <option value="Rescue">Rescue</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Ordenar por:</label>
                                    <select id="order_by" name="order_by" class="form-control">
                                        <!--<option value="unfulfilled_orders" selected="selected">Pedidos pendientes</option>-->
                                        <option value="customer_delivery_date">Fecha de entrega</option>
                                        <option value="orders.id" selected="selected">ID de pedido</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Estado:</label>
                                    <select id="status" name="status" multiple="multiple" class="form-control">
                                        <option value="Validation">Validation</option>
                                        <option value="Initiated">Initiated</option>
                                        <option value="Enrutado">Enrutado</option>
                                        <option value="In Progress">In Progress</option>
                                        <option value="Alistado">Alistado</option>
                                        <option value="Dispatched">Dispatched</option>
                                        <option value="Delivered">Delivered</option>
                                        <option value="Cancelled">Cancelled</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Mostrar pedidos:</label>
                                    <select id="show_orders" name="show_orders" class="form-control">
                                        <option value="">Selecciona</option>
                                        <option value="update_stock_back">Productos no disponibles despúes de despacho</option>
                                        <option value="no_paid">Pendiente de pago</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label style="">Tienda aliada:</label>
                                    <select id="allied_stores" name="allied_stores" class="form-control" style="">
                                        <option value="">Selecciona</option>
                                        @if (count($allied_stores))

                                            @foreach ($allied_stores as $allied_store)
                                                @if ( $allied_store->city_id == Session::get('admin_city_id') )
                                                    <option value="{{ $allied_store->id }}">{{ $allied_store->name }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-12" style="text-align: center; text-align-all: center;">
                                    <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                    <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>&nbsp;&nbsp;
                                    @if ($admin_permissions['delete'])
                                        <div id="export_btn" class="excel-icon"><img class="excel-icon-image" src="{{asset_url()}}img/excel.jpg" alt="" width="30px" height="30px"></div>
                                    @endif
                                    <button type="button" id="btn-change-delivery-date" class="btn unseen btn-primary" onclick="show_modal('delivery-date')">Cambiar fecha de entrega</button>
                                </div>

                    	        <table width="100%" class="orders-storage-table">

                                </table>
                            </form>
                        </div>
    				</div>
    				<br>
                	<div class="paging"></div>
					<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Customer Delivery Date Modal -->
<div class="modal fade" id="modal-delivery-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Fecha de Entrega</h4>
            </div>
            <form method="post" action="{{ route('adminOrderStorage.updateDeliveryDate') }}" class="form-modal form-delivery-date">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nueva fecha de entrega</label>
                        <select class="form-control required delivery_day" name="delivery_day" id="delivery_day">
                            <option value="">Cargando...</option>
                        </select>
                        <div id="loading unseen"></div>
                        <div class="delivery_time unseen"><br/>
                            <select class="form-control required" name="delivery_time" id="delivery_time">
                                <option value="">Cargando...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                <input type="hidden" name="order_id" id="order_id"/>
                <input type="hidden" name="store_id" id="store_id"/>
                <input type="hidden" name="zone_id" id="zone_id"/>
            </form>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&key={{ Config::get('app.google_api_key') }}&sensor=true"></script>
<script>
    function calculate_days(date_object) {
        var two_weeks = 1209600000;
        var total = date_object.getTime() - two_weeks;
        var date  = new Date(total);
        return date;
    }
$(document).ready(function() {
	paging(1, $('.search').val());

    $('#delivery_date, #delivery_date_end').datetimepicker({
        format: 'DD/MM/YYYY',
    })/*.on('dp.change', function (e) {
        var start_date = $('#delivery_date').val().split('/');
        var end_date = $('#delivery_date_end').val().split('/');
        start_date = new Date(start_date[2], start_date[1] -1, start_date[0]);
        end_date = new Date(end_date[2], end_date[1] -1, end_date[0]);
        var days = parseInt((end_date - start_date)/(24*3600*1000));
        if(days > 3){
            $('#delivery_date').val('')
            $('#delivery_date_end').val('')
            alert('Debe seleccionar máximo 3 días.')
        }
    });*/


    $('#export_btn').on('click', function(e) {

        if( $('#delivery_date').val() == '' && $('#delivery_date_end').val() == ''){
            alert('Los campos de fechas no pueden estar vacios');
            $('.paging-loading').hide();
            return;
        }
        if( $('#delivery_date').val() != '' && $('#delivery_date_end').val() == '') {
            alert('Debe seleccionar la fecha de finalización en el rango de fechas');
            $('.paging-loading').hide();
            return;
        } else if ($('#delivery_date').val() == '' && $('#delivery_date_end').val() != '') {
            alert('Debe seleccionar la fecha de inicio en el rango de fechas');
            $('.paging-loading').hide();
            return;
        }

        $('#export_btn').addClass('disabled');
        $('.excel-icon-image').hide();
        $('#export_btn').append('<img class="excel-icon-loading" src="{{ asset_url() }}/img/loading.gif" />');
        $('.excel-icon-image').show();
        var status_ids = '';
        var l = $('#status :selected').length;
        $('#status :selected').each(function(i, selected){
            status_ids += '"' + $(selected).text() + '"';
            if (i !== (l - 1)) status_ids +=','
        });
        var data = {
            city_id: $('#city_id').val(),
            store_id: $('#store_id').val(),
            revision: $('#revision').val(),
            show_orders: $('#show_orders').val(),
            transporter_id: $('#transporter_id').val(),
            vehicle_id: $('#vehicle_id').val(),
            driver_id: $('#driver_id').val(),
            picker_dry_id: $('#picker_dry_id').val(),
            order_delivery_date: $('#order_delivery_date').val(),
            status: status_ids,
            type: $('#type').val(),
            source: $('#source').val(),
            source_os: $('#source_os').val(),
            order_by: $('#order_by').val(),
            delivery_date: $('#delivery_date').val(),
            delivery_date_end: $('#delivery_date_end').val(),
            delivery_window_id: $('#delivery_window_id').val(),
            payment_method: $('#payment_method').val(),
            route_id: $('#route_id').val(),
            zone_id: $('#zone_id').val(),
            reject_reason: $('#reject_reason').val(),
            allied_stores: $('#allied_stores').val(),
            s: $('.search').val()
        };
        $.ajax({
            url: "{{ route('adminOrderStorage.getOrdersReportAjax') }}",
            data: data,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.status)
                    window.location = response.url;
                else alert('No se encontraron pedidos.');
                $('.excel-icon-image').show();
                $('.excel-icon-loading').hide();
                $('#export_btn').removeClass('disabled');
            }, error: function() {
                $('.excel-icon-image').show();
                $('.excel-icon-loading').hide();
                $('#export_btn').removeClass('disabled');
            }
        });
    });

    /*$('#type').on('change', function(){
        if( $(this).val() == 'Marketplace' ){
            var city = $('#city_id').val();
        }
    } )*/

    $('#city_id').change(function () {
        var city_id = $(this).val();
        $('#transporter_id, #vehicle_id, #driver_id, #picker_dry_id, #route_id, #zone_id').empty().append('<option value="">Selecciona</option>');

        if(city_id != '')
            $('#store_id').empty();
        else
            $('#store_id').empty().append('<option value="">Selecciona</option>');

        $.ajax({
            url: '{{ route('adminOrderStorage.getStoresTransportersByCityAjax') }}',
            type: 'get',
            dataType: 'json',
            data: { city_id: city_id },
        })
        .done(function(data) {
            var html = '';
            html += '';
            $.each(data.stores, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.name +'</option>';
            });
            $('#store_id').append(html);
            html = '';
            $.each(data.transporters, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.fullname +'</option>';
            });
            $('#transporter_id').append(html);
            html = '';
            $.each(data.pickers, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.fullname +'</option>';
            });
            $('#picker_dry_id').append(html);
            html = '<option value="NULL">Sin ruta</option>';
            $.each(data.routes, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.route +'</option>';
            });
            $('#route_id').append(html);
            html = '<option value="NULL">Sin zona</option>';
            $.each(data.zones, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.name +'</option>';
            });
            $('#zone_id').append(html);

             html = '<option value="">Selecciona</option>';
            $.each(data.allied_stores, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.name +'</option>';
            });
            $('#allied_stores').html(html);
        })
        .fail(function() {
            console.log("Error en la consulta para obtener tiendas, transportadores y alistadores por ciudad");
        });
    });
    $('#city_id').trigger('change');

    $('body').on('change', '#transporter_id', function() {
        $('#vehicle_id, #driver_id').empty().append('<option value="">Selecciona</option>');
        var transporter_id = $(this).val();
        $.ajax({
            url: '{{ route('adminOrderStorage.getDriversVehiclesByTransportersAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: {
                transporter_id: transporter_id
            },
        })
        .done(function(data) {
            var html = '';
            if (data.vehicles) {
                $.each(data.vehicles, function(index, val) {
                    html += '<option value="'+ val.id +'">'+ val.plate +'</option>';
                });
                $('#vehicle_id').append(html);
            }
            html = '';
            if (data.drivers) {
                $.each(data.drivers, function(index, val) {
                    html += '<option value="'+ val.id +'">'+ val.first_name + ' ' +val.last_name +'</option>';
                });
                $('#driver_id').append(html);
            }

        })
        .fail(function() {
            console.log("Error al obtener los vehículos");
        });
    });

    $('body').on('change', '#vehicle_id', function() {
        $('#driver_id').empty().append('<option value="">Selecciona</option>');
        var vehicle_id = $(this).val();
        $.ajax({
            url: '{{ route('adminOrderStorage.getDriverByVehicleAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: {
                vehicle_id: vehicle_id
            },
        })
        .done(function(data) {
            var html = '';
            if (data.drivers) {
                $.each(data.drivers, function(index, val) {
                    html += '<option value="'+ val.id +'">'+ val.first_name + ' ' +val.last_name +'</option>';
                });
                $('#driver_id').append(html);
            }

        })
        .fail(function() {
            console.log("Error al obtener el conductor");
        });
    });

    $('body').on('change', '.all-order', function() {
        if($('body .order').size() == $('body .order:checked').size() && $(this).is(':checked')){
            $('#btn-change-delivery-date').removeClass('unseen');
            $('body .order').prop('checked',true);
        }else{
            if($(this).is(':checked')){
                $('#btn-change-delivery-date').removeClass('unseen');
                $('body .order').prop('checked',true);
            }else{
                $('body .order').prop('checked',false);
                $('#btn-change-delivery-date').addClass('unseen');
            }
        }
    });

    $('body').on('click', '.order', function() {
        var selected = $('.order').length;
        var count = 0;

        $(".order").each(function(){
            if($(this).is(':checked'))
                count = count + 1;
        });

        if(selected == count){
            $('#btn-change-delivery-date').removeClass('unseen');
            $('body .all-order').prop('checked',true);
        }else{
            $('#btn-change-delivery-date').removeClass('unseen');
            $('body .all-order').prop('checked',false);
        }

        if(count == 0)
            $('#btn-change-delivery-date').addClass('unseen');
    });

    $('.delivery_day').on('change', function() {
            $('#delivery_time option').remove();
            $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
            if ($(this).val() != '') {
                times = delivery_times[$(this).val()];
                $.each(times, function(value, data) {
                    $('#delivery_time').append($("<option></option>").attr('value', value).text(data.text));
                });
                $('.delivery_time').show();
            }else{
                $('.delivery_time').hide();
            }
        });
        $('.delivery_day').trigger('change');

});

function show_modal(action, reference)
{
    reference = reference || 0;

    if (action == 'delivery-date' && $('#delivery_day option').length == 1)
    {
        var store_id = $('#store_id').val();
        var zone_id = $('#zone_id').val();
        if (!$('.order:checked').length){
            alert('Debes seleccionar al menos un pedido de la grilla.');
            return false;
        }else{
            var orders_selected = {};
            $('.order:checked').each(function(index) {
                orders_selected[index] = $(this).val();
            });

            var orders = JSON.stringify(orders_selected);
            $('.form-delivery-date #store_id').val(store_id);
            $('.form-delivery-date #zone_id').val(zone_id);
            $('.form-delivery-date #order_id').val(orders);

            $.ajax({
             url: "{{ route('adminOrderStorage.getStoreDeliverySlotAjax') }}",
             data: { store_id: store_id, zone_id: zone_id, validate_slots: 0, show_today: 1 },
             type: 'get',
             dataType: 'json',
             success:
                function(response) {
                    $('#delivery_day').empty().append('<option value="">Selecciona el día</option>');
                    $('#delivery_time').empty().append('<option value="">Selecciona la hora</option>');
                    $.each(response[store_id].days, function(key, value){
                        $('#delivery_day').append('<option value="' + key + '">' + value + '</option>');
                    });
                    delivery_times = response[store_id].time;
                    $('*[name="delivery_day"]').trigger('change');
                }
            });
        }

    }

    $('#modal-' + action).modal('show');
}
</script>

@else

@section('content')
<div class="table-responsive">
    <table id="orders-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th><input type="checkbox" name="all-order" class="all-order"/></th>
                <th>ID</th>
                <th>Nombre / Dirección</th>
                <th>Cant. Pedidos</th>
                <th>Ciudad / Zona / Bodega</th>
                <th>Celular</th>
                <th>Alistamiento</th>
                <th>Total</th>
                <th>Método de pago</th>
                <th>Estado</th>
                <th>Conductor</th>
                <th>Vehículo</th>
                <th>Alistador seco</th>
                <th>Ruta</th>
                <th>Secuencia</th>
                <th>Fecha entrega</th>
                @if($admin_permissions['delete']) <th>Revisado</th> @endif
                <th>Ver</th>
            </tr>
        </thead>
        <tbody>
        @if (count($orders))
            @foreach($orders as $order)
            <tr @if ($order->posible_fraud) class="orders-posible_fraud" @elseif ($order->suspect) class="orders-suspect" @elseif ($order->scheduled_delivery) class="orders-scheduled_delivery"@endif>
                <td align="center"><input type="checkbox" name="order" class="order" value="{{ $order->id }}"/></td>
                <td>{{ $order->id }}
                    @if($order->child_order_id)<p>(hijo - <a href="{{ route('adminOrderStorage.details', ['id' => $order->child_order_id]) }}">{{ $order->child_order_id }}</a>)</p>@endif
                    @if($order->parent_order_id)<p>(padre - <a href="{{ route('adminOrderStorage.details', ['id' => $order->parent_order_id]) }}">{{ $order->parent_order_id }}</a>)</p>@endif
                    <p>{{ $order->type }}</p>
                </td>
                <td>
                    {{ $order->user_firstname.' '.$order->user_lastname }}
                    <br>
                    {{ $order->user_address.' '.$order->user_address_further }}
                    @if (!empty($order->user_address_neighborhood))
                    <br>
                    <b>Barrio: </b>{{ $order->user_address_neighborhood }}
                    @endif
                </td>
                <td>{{ $order->orders_qty }}</td>
                <td>{{ $order->city }} / <br> {{ $order->zone }} / <br /> {{ $order->warehouse }}</td>
                <td>{{ $order->user_phone }}</td>
                <td>
                    <div style="text-align:center;">
                    @if($order->products_storage_dry > 0)<img src="{{ asset_url() }}img/dry.png" width="20" title="Seco">@endif
                    @if($order->products_storage_cold > 0)<img src="{{ asset_url() }}img/cold.png" width="20" title="Frío">@endif
                    </div>
                    <div style="text-align:center;">
                    ({{ $order->total_products }} @if($order->total_products > 1) items @else item @endif)
                    </div>
                </td>
                <td>
                    ${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}
                    @if (!empty($order->coupon_code)) <br><b>Cupón:</b> {{ $order->coupon_code }} @endif
                </td>
                <td>{{ $order->payment_method }}</td>
                <td>
                @if($order->status == 'Validation')
                <span class="badge bg-maroon">Validation</span>
                @elseif($order->status == 'Delivered')
                <span class="badge bg-green">Delivered</span>
                @elseif($order->status == 'Cancelled')
                <span class="badge bg-red">Cancelled</span><br><strong>@if($order->temporarily_cancelled) (Temporalmente) @endif</strong>
                @elseif($order->status == 'Initiated')
                <span class="badge bg-yellow">Initiated</span>
                @elseif($order->status == 'Dispatched')
                <span class="badge bg-green">Dispatched</span>
                @elseif($order->status == 'In Progress')
                <span class="badge bg-orange">In Progress</span>
                @elseif($order->status == 'Alistado')
                <span class="badge bg-green">Alistado</span>
                @elseif($order->status == 'Enrutado')
                <span class="badge bg-orange">Enrutado</span>
                @endif
                <p><b>Origen:</b> {{ $order->source }} {{ $order->source_os }}
                @if(!empty($order->user_score_date))
                <br><b>Calificación:</b> @if(!$order->user_score) <span class="badge bg-red">Negativa</span> @else <span class="badge bg-green">Positiva</span> @endif
                @endif
                </p>
                </td>
                <td>{{ $order->driver_first_name.' '.$order->driver_last_name }}</td>
                <td>{{ $order->plate }}</td>
                <td>{{ $order->shopper_first_name.' '.$order->shopper_last_name }}</td>
                <td>@if (!empty($order->route)) {{ $order->route }} @else {{ $order->planning_route }} @endif</td>
                <td>{{ $order->planning_sequence }}</td>
                <td>
                    @if ($order->status != 'Delivered' && $order->status != 'Cancelled')
                    {{ get_time('left', $order->customer_delivery_date); }}
                    @else
                    {{ date("d M Y",strtotime($order->customer_delivery_date)); }} @endif
                </td>
                @if($admin_permissions['delete'])
                    <td align="center">@if ($order->is_checked) <img src="{{ asset_url() }}img/available.png" width="10"> @else <img src="{{ asset_url() }}img/no_available.png" width="10"> @endif</td>
                @endif
                <td>
                    <div class="btn-group">
                       <a class="btn btn-xs btn-default" href="{{ route('adminOrderStorage.details', ['id' => $order->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="19" align="center">Pedidos no encontrados.</td></tr>
        @endif
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($orders) }} pedidos</div>
    </div>
</div>

@endif

@stop
