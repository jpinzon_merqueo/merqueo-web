@if (Request::ajax())
@extends('admin.layout')

@section('table_refunds')
<div class="row">
	<div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body table-responsive container-overflow">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th>Charge ID</th>
                            <th>Usuario</th>
                            <th>Razón</th>
                        </tr>
                    </thead>
                    <tbody>
                	@if(count($order_payment_refunds))
                        @foreach($order_payment_refunds as $order_payment_refund)
                        <tr>
                            <td>{{date("d M Y h:i a", strtotime($order_payment_refund->date))}}</td>
                            <td>
                                @if($order_payment_refund->status == 'Aprobada')
                                <span class="badge bg-green">{{$order_payment_refund->status}}</span>
                                @elseif($order_payment_refund->status == 'Declinada')
                                <span class="badge bg-red">{{$order_payment_refund->status}}</span>
                                @elseif($order_payment_refund->status == 'Pendiente')
                                <span class="badge bg-orange">{{$order_payment_refund->status}}</span>
                                @endif
                            </td>
                            <td>{{$order_payment_refund->cc_charge_id}}</td>
                            <td>{{$order_payment_refund->fullname}}</td>
                            <td>{{$order_payment_refund->reason}}</td>
                        </tr>
                        @endforeach
                    @else
                        <tr><td colspan="5">No hay reembolsos.</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@endif