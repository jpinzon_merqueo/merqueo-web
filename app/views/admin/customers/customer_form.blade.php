@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<script>
    var last_numbers = 0;
    function show_modal(action, reference) {
        var ref = reference || 0;
        $('#identity_number').val(ref);
        $('#modal-' + action).modal('show');
    }

    $('.identity-number-change-form').validate({
        rules: {identity_number: "required"}
    });

    $('body').on('click', '.identity-number-change-form .btn-identity-number-change[type="submit"]', function(e) {
        $('.loading div').removeClass('unseen');
        $('.btn-identity-number-change').addClass('unseen');
        if ($('.identity-number-change-form').valid()) {
            var data = {user_id: $('#user_id').val(), identity_number: $('#identity_number').val(), last_numbers: last_numbers};
            $.post('{{ route('adminCustomers.changeIdentityNumberAjax') }}', data)
                .done(function(data) {
                    $('#user_identity_number').text(data.result.user.identity_number);
                    $('#modal-identity-number-change').modal('hide');
                    $('.loading div').addClass('unseen');
                    $('.btn-identity-number-change').removeClass('unseen');
                    window.location.reload(true);
                })
                .fail(function(data) {
                    $('.loading div').addClass('unseen');
                    $('.btn-identity-number-change').removeClass('unseen');
                });
        } else {
            $('.loading div').addClass('unseen');
            $('.btn-identity-number-change').removeClass('unseen');
        }
        e.preventDefault();
    });

    var open_modal_identity_number_change = function(ln, identity_number) {
        last_numbers = ln;
        show_modal('identity-number-change', identity_number);
    };
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id="main-form" action="{{ route('adminCustomers.save') }}" >
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{ $customer ? $customer->id : '' }}">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" id="" name="first_name" placeholder="Enter First Name" value="{{ $customer ? $customer->first_name : '' }}"
                                        @if ( !$admin_permissions['permission2'] )
                                            readonly="readonly"
                                        @endif
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" class="form-control" id="" name="last_name" placeholder="Enter Last Name" value="{{ $customer ? $customer->last_name : '' }}"
                                        @if ( !$admin_permissions['permission2'] )
                                            readonly="readonly"
                                        @endif
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="" name="email" placeholder="Enter Email" value="{{ $customer ? $customer->email : '' }}"
                                        @if ( !$admin_permissions['permission2'] )
                                            readonly="readonly"
                                        @endif
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" id="" name="phone" placeholder="Enter Phone" value="{{ $customer ? $customer->phone : '' }}"
                                        @if ( !$admin_permissions['permission2'] )
                                            readonly="readonly"
                                        @endif
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="free_delivery_expiration_date">Fecha de expiración de domicilio gratis</label>
                                    <input type="text" name="free_delivery_expiration_date" id="free_delivery_expiration_date" class="form-control" value="{{ $customer && !empty($customer->free_delivery_expiration_date) ? date("d/m/Y", strtotime($customer->free_delivery_expiration_date)) : '' }}"
                                        @if ( !$admin_permissions['permission2'] )
                                            readonly="readonly"
                                        @endif
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="">Crédito actual</label>
                                    <input type="text" class="form-control" value="{{ $customer && !empty($customer->current_credits) ? $customer->current_credits : 0 }}" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="send_advertising">¿Enviar publicidad?</label>
                                    <select name="send_advertising" id="send_advertising" class="form-control">
                                        <option value="1" {{ $customer && $customer->send_advertising == 1 ? 'selected="selected"' : '' }}>Sí</option>
                                        <option value="0" {{ $customer && $customer->send_advertising == 0 ? 'selected="selected"' : '' }}>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Estado</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="1" {{ $customer && $customer->status == 1 ? 'selected="selected"' : '' }}>Activo</option>
                                        <option value="0" {{ $customer && $customer->status == 0 ? 'selected="selected"' : '' }}>Inactivo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="">¿Domicilio gratis en el próximo pedido?</label>
                                    <select name="free_delivery_next_order" id="free_delivery_next_order" class="form-control"
                                        @if ( !$admin_permissions['permission2'] )
                                            readonly="readonly"
                                            disabled="disabled"
                                        @endif
                                    >
                                        <option value="0" @if($customer && !$customer->free_delivery_next_order) selected="selected" @endif>No</option>
                                        <option value="1" @if($customer && $customer->free_delivery_next_order) selected="selected" @endif>Sí</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="free_delivery_next_order_expiration_date">Fecha de expiración de proximo domicilio gratis</label>
                                    <input type="text" name="free_delivery_next_order_expiration_date" id="free_delivery_next_order_expiration_date" class="form-control" value="{{ $customer && !empty($customer->free_delivery_next_order_expiration_date) ? date("d/m/Y", strtotime($customer->free_delivery_next_order_expiration_date)) : '' }}"
                                           @if ( !$admin_permissions['permission2'] )
                                           readonly="readonly"
                                            @endif
                                    >
                                </div>
                            </div>
                        </div>
                        @if(!$customer)
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" id="" name="password" placeholder="Enter Password" value=""
                                @if ( !$admin_permissions['permission2'] )
                                    readonly="readonly"
                                @endif
                            >
                        </div>
                        @endif
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Resumen general del usuario</h3>
                </div>
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Total de pedidos</th>
                                    <th>Fecha de creación del cliente</th>
                                    <th>Total de crédito asignado</th>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <th>Realizados: </th><td>{{ $customer->orders_count }} </td>
                                                <th> Entregados: </th><td>{{ $customer->orders_delivered_count }} </td>
                                                <th> Cancelados: </th><td>{{ $customer->orders_cancelled_count }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>{{ format_date('normal_with_time', $customer->created_at) }}</td>
                                    <td>${{ number_format($customer->current_credits, 0, ',', '.') }}</td>
                                </tr>
                                <tr>
                                    <th>Número de referidos</th>
                                    <th>Fecha de ultimo pedido</th>
                                    <th>Ciudad de ultimo pedido</th>
                                </tr>
                                <tr>
                                    <td>{{ $customer->referrals->count() }}</td>
                                    @if(!is_null($customer->last_order))
                                    <td>{{ format_date('normal_with_time', $customer->last_order->created_at) }}</td>
                                    <td>{{ $customer->last_order->store->city->city }}</td>
                                    @else
                                    <td>N/A</td>
                                    <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Valor promedio de pedidos</th>
                                    <th>Código de referido</th>
                                </tr>
                                <tr>
                                    <td>${{ number_format($customer->prom_orders, 0, ',', '.') }}</td>
                                    <td>{{ $customer->referral_code }}@if($customer->referral_code_blocked) (Bloqueado) @else() (Desbloqueado) @endif</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Direcciones registradas por el usuario</h3>
                </div>
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                @if($customer->addresses->count())
                                    @foreach($customer->addresses as $address)
                                        <tr>
                                            <td>{{ $address->address }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>El cliente no tiene direcciones registradas</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Tarjetas de Crédito</h3>
                </div>
                <div class="box-body table-responsive">
                    <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Tipo de tarjeta</th>
                                <th>Últimos cuatro digitos</th>
                                <th>Cédula de ciudadania</th>
                                <th>Fecha de creación</th>
                                @if($admin_permissions['update'])
                                    <th>Editar cédula</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($user_credit_cars))
                                @foreach($user_credit_cars as $card)
                                    <tr>
                                        <td>{{ $card->type }}</td>
                                        <td>{{ $card->last_four }}</td>
                                        <td><span class="user_identity_number">{{ $card->holder_document_number }}</span></td>
                                        <td>{{  date('d M Y g:i a', strtotime( $card->created_at)) }}</td>
                                        @if($admin_permissions['update'])
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-xs btn-default open-modal-identity-number-change" href="#" onclick="open_modal_identity_number_change({{$card->id}},{{$card->holder_document_number}})"><span class="glyphicon glyphicon-pencil"></span></a>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="19" align="center">El cliente no cuenta con tarjetas registradas.</td></tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Order identity change modal -->
    <div class="modal fade" id="modal-identity-number-change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form accept-charset="UTF-8" class="identity-number-change-form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cambiar número de cédula en tarjeta de crédito</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box-body table-responsive">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <table width="100%">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <tr>
                                                            <td align="left"><label>No. de cédula:</label>&nbsp;</td>
                                                            <td><input type="number" placeholder="Número de identificación" id="identity_number" name="identity_number" class="form-control required"></td>
                                                        </tr>
                                                    </div>
                                                </div>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="user_id" id="user_id" value="{{$customer->id}}">
                        <button type="submit" class="btn btn-primary btn-identity-number-change">&nbsp;Guardar&nbsp;</button>
                        <div class="loading">
                            <div class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    if (parseInt($('#free_delivery_next_order').val()) === 0) $('#free_delivery_next_order_expiration_date').attr('readonly', 'readonly');

    $('#free_delivery_expiration_date, #free_delivery_next_order_expiration_date').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#free_delivery_next_order').change(function(e) {
        if (parseInt($('#free_delivery_next_order').val()) !== 1) {
            $('#free_delivery_next_order_expiration_date').attr('readonly', 'readonly').val('');
        }
        else {
            $('#free_delivery_next_order_expiration_date').removeAttr('readonly');
        }
    });
    $("#main-form").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            phone: "required",
            password: "required",
            email: {
                required: true,
                email: true
            }
        }
    });
});
</script>


@stop