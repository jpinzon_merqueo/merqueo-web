@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<!--<link rel="stylesheet" type="text/css" href="{{asset_url()}}css/style.css">-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>



<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
</script>
<a href="#importing" class="fancybox unseen"></a>
<div id="importing" class="unseen"><p>Importando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>




<section class="content-header">
    <h1>
        {{$title}}
        <small>Control panel</small>
    </h1>
           
</section>

<section class="content">
	<div class="alert alert-success alert-dismissable unseen">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Hecho!</b>
        <div class="message" style="display: inline-block;"></div>
	</div>
	
	<div class="alert alert-danger alert-dismissable unseen">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alerta!</b> 
        <div class="message" style="display: inline-block;"></div>
	</div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Agregar Crédito </h3>
                </div>

                <form role="form" method="post" id="main-form" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group row">
                            <div class="col-xs-12 col-md-6 col-sm-6">
                                <label>Clientes CSV</label>
                                <input id="file" type="file" class="form-control" name="file" placeholder="File">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12 col-md-4 col-sm-4">
                                <label>Tipo</label>
                                <select type="text" class="form-control" name="type" id="type">
                                    <option value="">-Seleciona-</option>
                                    <option value="1">Cargo</option>
                                    <option value="0">Deducción</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-md-4 col-sm-4">
                                <label>Valor</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="Ingresa el valor del credito">
                            </div>
                            <div class="col-xs-12 col-md-4 col-sm-4">
                                <label>Fecha expiración </label>
                                <input type="text" class="form-control" name="expiration_date" id="expiration_date" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar </button>                        
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>


<script type="text/javascript">

    var display_message = function( $class, $message ){
        $( $class ).find('div.message').html( $message );
        $( $class ).show();
    }

    $(document).ready( function(){
        $('#expiration_date').datetimepicker({
            format: 'DD/MM/YYYY',
            minDate : new Date(),
            useCurrent: false,
        });
    })
    $("#main-form").validate({
    rules : {
        amount :{
            required : true,
            number : true,
            max : 100000,
            min : 100
        },
        expiration_date : {
            required: function (element) {
                if ($("#type").val() == 1)
                    return true;
                else
                    return false;
            }
        },
        type : "required",
        file : "required"
        
    },
    messages : {
        amount : {
            required : "Este campo es obligatorio",
            number : "Se requiere un valor númerico",
            max : "Debe ingresar un valor igual o menor a $100.000",
            min : "Debe ingresar un valor igual o mayor a $100"
        },
        expiration_date : {
            required : "Este campo es obligatorio",
            //date : "Debe ingresar una fecha valida en formato DD/MM/YYYY"
        },
        type : {
            required : "Este campo es obligatorio"
        },
        file : {
            required : "Archivo Obligatorio"
        }

    },
    submitHandler : function( form ) {
        form = new FormData(form);
        $('.alert.alert-success').hide();
        $('.alert.alert-danger').hide();
    	if (confirm('¿Esta seguro de asignar este cupo de crédito?')){
            $('.fancybox').trigger('click');
            $.ajax({
                url : '{{ route('adminCustomers.addCreditMultiple') }}',
                type : 'POST',
                data : form,
                dataType : 'json',
                contentType: false,
                processData: false,
                success : function( response ){
                    if( response.status == true ){
                        display_message( '.alert.alert-success', response.message );
                        //$('.current-credit').val(response.result);
                        $('#type').val('');
                        $('#amount').val('');
                        $('#expiration_date').val('');
                        $('#file').val('');
                    }else{
                        display_message( '.alert.alert-danger', 'Ocurrió un error. '+response.message);
                    }
                    $.fancybox.close();
                },
                error : function( response ){
                    display_message( '.alert.alert-danger', 'Ocurrió un error.');
                    $.fancybox.close();
                },
            });
        }
    	//form.submit(); 	
    }
});
</script>


@stop