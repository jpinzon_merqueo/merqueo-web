@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<script>
    var web_url_ajax = "{{ route('adminCustomers.index') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    
    <span class="breadcrumb" style="top:0px">
        @if (Session::get('admin_designation') == 'Admin')
            <a href="{{ route('adminCustomers.add') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Cliente</button>
            </a>
        @endif
        @if( $admin_permissions['permission3'] )
            <a href="{{ route('adminCustomers.addCreditMultiple') }}">
                <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Crédito Masivo</button>
            </a>
        @endif
    </span>
    
</section>

<section class="content">
@if(Session::has('message'))
    @if(Session::get('type') == 'success')
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('message') }}
    </div>
    @else
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('message') }}
    </div>
    @endif
@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive form-horizontal">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="search" class="col-sm-1 control-label">Buscar:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="User id, nombre, email, teléfono">
                                </div>
                                <div class="col-sm-1">
                                    <button id="search-btn" class="btn btn-success">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
					<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    <div class="paging"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
	var Buscador = (function() {
        'use strict';

        function Buscador() {
            // enforces new
            if (!(this instanceof Buscador)) {
                return new Buscador();
            }
            // constructor body
            this.search = null;
            this.bindActions();
            this.search_ajax();
        }

        Buscador.prototype.bindActions = function() {
            let self = this;
            $('body').on('click', '#search-btn', function(event) {
                self.search = $('#search').val();
                self.search_ajax();
            });
            $('#search').keyup(function(event) {
                if ( event.which == 13 ) {
                    self.search = $('#search').val();
                    self.search_ajax();
                }
            });
        };

        Buscador.prototype.search_ajax = function() {
            $('.paging-loading').show();
            $.ajax({
                url: '{{ route('adminCustomers.index') }}',
                type: 'POST',
                dataType: 'html',
                data: {
                    s: this.search
                },
            })
            .done(function(data) {
                $('.paging').html(data);
            })
            .fail(function() {
                console.log("error al buscar, respuesta de ajax.");
            })
            .always(function() {
                $('.paging-loading').hide();
            });
        };
        return Buscador;
    }());
    var buscar = new Buscador;
});
</script>

@else

@section('content')

<table id="customers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>User ID</th>
            <th>Código Referido</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Creado</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @if (count($customers))
        @foreach($customers as $customer)
        <tr>
            <td>{{ $customer->id }}</td>
            <td>{{ $customer->referral_code }}</td>
            <td>{{ $customer->first_name }}</td>
            <td>{{ $customer->last_name }}</td>
            <td>{{ $customer->phone }}</td>
            <td>{{ $customer->email }}</td>
            <td>{{ date("d M Y",strtotime($customer->created_at)) }}</td>
            <td>
                @if($customer->status == 1)
                    <span class="label label-success">
                        Activo
                    </span>
                @else
                    <span class="label label-danger">
                        Inactivo
                    </span>
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                    	<li><a href="{{ route('adminCustomers.edit', ['id' => $customer->id]) }}">Editar</a></li>
                        <!-- <li><a href="{{ route('adminOrders.add') }}?user_id={{ $customer->id }}">Create Order</a></li> -->
                        
                        @if( $admin_permissions['permission3'] )
                        <li class="divider"></li>
                        <li><a href="{{ route('adminCustomers.addCredit', ['user_id' => $customer->id]) }}">Cargar crédito</a></li>
                        @endif

                        @if (Session::get('admin_designation') == 'Super Admin' && (Session::get('admin_id') == 1 || Session::get('admin_id') == 17))
                            <li><a href="{{ route('adminCustomers.charge', ['id' => $customer->id]) }}">Cargar cantidad a tarjeta de crédito</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminCustomers.delete', ['id' => $customer->id]) }}" onclick="return confirm('Are you sure you want to delete it?')">Borrar</a></li>
                        @endif

                        @if( $admin_permissions['update'] )
                            <li class="divider"></li>
                            <li><a href="{{ route('adminCustomers.blockReferralCode', ['user_id' => $customer->id]) }}">@if($customer->referral_code_blocked) Desbloquear @else Bloquear @endif código de referido</a></li>
                        @endif
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="10" align="center">Clientes no encontrados</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($customers) }} clientes</div>
    </div>
</div>

@endif

@stop