@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        Realizar cobro manual a tarjeta de crédito de cliente
        <small>Control panel</small>
    </h1>
           
</section>
<section class="content">
	
	@if(Session::has('message') && Session::get('type') == 'success')
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Hecho!</b> {{ Session::get('message') }}
	</div>
	@endif
	
	@if(Session::has('message') && Session::get('type') == 'error')
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alerta!</b> {{ Session::get('message') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Cargar información</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id="main-form" action="{{ route('adminCustomers.chargeSave') }}" >
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{ $user ? $user->id: '' }}">
                        <div class="form-group">
                            <label>Cliente</label>
                            <input type="text" class="form-control" disabled="disabled" value="{{ $user->first_name.' '.$user->last_name }}" />
                        </div> 
                        <div class="form-group">
                            <label>Tarjeta de crédito</label>
                            <select name="credit_card_id" class="form-control type">
                            	<option value=""></option>
                            	@foreach($credit_cards as $cc)
                                <option value="{{ $cc->id }}" @if ($post && $post['credit_card_id'] == $cc->id) selected="selected" @endif>**** {{ $cc->last_four }} {{ $cc->type }}</option>
                                @endforeach                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Valor total</label>
                            <input type="text" class="form-control" name="total_amount" placeholder="Ingrese el valor total" value="{{ $post['total_amount'] or '' }}">
                        </div>
                        <div class="form-group">
                            <label>Cuotas</label>
	                        <select type="text" class="form-control" name="installments">
						    	<option value="" selected="selected"></option>
				           		@for ($i = 1; $i <= 36; $i++)
				           		<option value="{{ $i }}" @if ($post && $post['installments'] == $i) selected="selected" @endif>{{ $i }}</option>
				           		@endfor
				           	</select>
					    </div>
					    <div class="form-group">
                            <label>ID del pedido</label>
                            <input type="text" class="form-control" name="order_id" placeholder="Ingrese el ID de la orden" value="{{ $post['order_id'] or '' }}">
                        </div>
                        <div class="form-group">
                            <label>Descripción</label>
                            <input type="text" class="form-control" name="description" placeholder="Ingrese la descripción" value="{{ $post['description'] or '' }}">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Cargar valor </button>                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$("#main-form").validate({
  rules: {
    credit_card_id:  {
      required: true,
      number: true
    },
    total_amount:  {
      required: true,
      number: true
    },
    installments:  {
      required: true,
      number: true
    },
    order_id:  {
      required: true,
      number: true
    },
    description: "required",
  },
  submitHandler: function(form) {
  	if (confirm('¿Are you sure you want to charge the amount to the credit card?'))
    	form.submit(); 	
  }
});
</script>


@stop