@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alert!</b> {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alert!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <form role="form" method="post" id='main-form' action="{{ route('adminAccounting.invoices') }}" accept-charset="UTF-8">
                    <div class="box-header">
		                <h3 class="box-title">{{ $sub_title }}</h3>
		            </div>
                    <div class="box-body">
                        <div class="form-group col-xs-3">
                            <label>Tipo de reporte</label>
                            <select name="type_report" id="type_report" class="form-control">
                            	<option value="invoice_client">Archivo plano facturación clientes</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-3">
                            <label>Tienda</label>
                            <select id="store_id" name="store_id" class="form-control">
                                @if (count($stores))
                                @foreach ($stores as $store)
                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-xs-3">
                            <label>Fecha Inicio</label>
                            <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Ingresa fecha inicio (dd/mm/yyyy)">
                        </div>
                        <div class="form-group col-xs-3">
                            <label>Fecha Final</label>
                            <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Ingresa fecha final (dd/mm/yyyy)">
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary generate-report">Generar Reporte</button>&nbsp;&nbsp;&nbsp;<img src="{{ asset_url() }}/img/loading.gif" class="unseen loading" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){

	$.validator.addMethod("dateFormat", function(value,element) {
	        return value.match(/^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/);
		}, "Please enter a date using format dd/mm/yyyy"
	);

	$("#main-form").validate({
	  rules: {
	    start_date: {
	      required: true,
	      dateFormat: true
	    },
	    end_date: {
	      required: true,
	      dateFormat: true
	    },
	  },
	  submitHandler: function(form){
	  	$('.loading').show();
	  	$('body .generate-report').addClass('disabled');
	  	$(form).submit();
	  }
	});

	$('#start_date').datetimepicker({
        format: 'DD/MM/YYYY',
    });

    $('#end_date').datetimepicker({
        format: 'DD/MM/YYYY',
    });

});

@if (Session::has('file_url')) window.location = '{{ Session::get('file_url') }}'; @endif

</script>

@stop