@extends('admin.layout')

@section('content')
<script src="https://maps.googleapis.com/maps/api/js?key={{ Config::get('app.google_api_key') }}"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>


    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            position: absolute;
        }
    </style>

    <section class="content-header">
        <div class="row">
            <div class="col-xs-11">
                <h1>
                    {{ $title }} - {{ $plate }}
                    <small>Control panel</small>
                </h1>
            </div>
            @if ($admin_permissions['permission1'])
                <div class="col-xs-1">
                    <a href="{{ route('adminVehicleTracing.mapTracingRecord', ['city_id' => 1, 'vehicle_id' => $vehicle_id]) }}">
                        <button type="button" class="btn btn-primary">Historial de rutas</button>
                    </a>
                </div>
            @endif
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div id="map" class="col-xs-12"></div>
        </div>
    </section>
    <script>
        $(document).ready(function(e) {
            var config = {
                apiKey: '{{ Config::get('app.firebase.general.key') }}',
                authDomain: '{{ Config::get('app.firebase.general.domain') }}',
                databaseURL: '{{ Config::get('app.firebase.general.url') }}',
                storageBucket: '{{ Config::get('app.firebase.general.storage_bucket') }}'
            };
            firebase.initializeApp(config);
            firebase.database().ref('vehicles/{{ $vehicle_id }}').on('value', function(snapshot) {
                console.log(snapshot.val());
                addOrdersMarker(new google.maps.LatLng(snapshot.val().lat, snapshot.val().lng), snapshot.val().last_update, '', '', 999);
            });

            var vehicle_id = {{ $vehicle_id }};
            var replace_string = '{{ $orders }}';
            replace_string = replace_string.replace("'", '');
            replace_string = replace_string.replace("\n", '');
            var orders = JSON.parse(replace_string.replace('\t', ''));
            var map;
            var coords = [];
            var position = JSON.parse('{{ $position }}');
            var orders_markers = [];
            var transporter_marker_aux = false;
            var latlng = new google.maps.LatLng(parseFloat(position.latitude), parseFloat(position.longitude));
            var colors = {delivered: '#00ff00', dispatched: '#ffa500', otherwise: '#f61e12'};
            var font_colors = {
                blank: '#ffffff',
                black: '#000000',
                red: '#ff0000'
            };
            var myOptions = {
                zoom: 12,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map'), myOptions);

            //agregar marcadores al mapa
            if (orders.length > 0)
            {
                setMapOrdersOnAll(null);
                for (var i = 0; i < orders.length; i++) {
                    var content = '<b>Pedido: </b>' + orders[i].id + '<br />' +
                        '<b>Estado: </b>' + orders[i].status + '<br />' +
                        '<b>Dirección: </b>' + orders[i].user_address + '<br />' +
                        '<b>Barrio: </b>' + orders[i].user_address_neighborhood + '<br />' +
                        '<b>Horario: </b>' + orders[i].delivery_time + '<br />' +
                        '<b>Secuencia: </b>' + orders[i].planning_sequence + '<br />';

                    addOrdersMarker(new google.maps.LatLng(orders[i].user_address_latitude, orders[i].user_address_longitude), content, orders[i].status, orders[i].planning_sequence, i);
                }
                setMapOrdersOnAll(map);

            }else alert('La ruta no cuenta con pedidos para mostrar.');

            //cargar ruta recorrida del transportador
            $.ajax({
                url: '{{ Config::get('app.transporters.location.url') }}/api/tracking/v1/traveled/' + vehicle_id + '/{{ Config::get('app.transporters.location.hours') }}',
                method: 'GET',
                type: 'json',
                async: true,
                success: function(response) {
                    if (response.status){
                        coords = [];
                        $.each(response.result, function(value, data) {
                            coords.push({lat: data.latitude, lng: data.longitude})
                        });
                        drawPolyLine(coords, map);
                    }
                },
                error: function(error) {
                    console.error('Ocurrió un error al cargar la ruta recorrida: ', error);
                }
            });

            //eliminar o insertar marcadores al mapa
            function setMapOrdersOnAll(m) {
                if (orders_markers.length) {
                    for (i = 0; i < orders_markers.length; i++) {
                        orders_markers[i].setMap(m);
                    }
                }
            }

            //agregar marcador al mapa
            function addOrdersMarker(location, content, status, marker_label, index)
            {
                //marcador para pedido
                if (index != 999)
                {
                    if (status === 'Delivered') {
                        marker_color = colors.delivered;
                        font_color = font_colors.black;
                    } else if (status === 'Dispatched') {
                        marker_color = colors.dispatched;
                        font_color = font_colors.black;
                    } else {
                        marker_color = colors.otherwise;
                        font_color = font_colors.blank;
                    }

                    var marker = new google.maps.Marker({
                        position: location,
                        map: map,
                        icon: {
                            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                            fillColor: marker_color,
                            fillOpacity: 1,
                            strokeColor: 'black',
                            strokeWeight: 1,
                            scale: 7,
                            labelOrigin: new google.maps.Point(0,-2.4)
                        },
                        //icon: marker_image,
                        label: {
                            color: font_color,
                            fontSize: '12px',
                            text: marker_label.toString()
                        },
                        zIndex: parseInt(index)
                    });
                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    marker.addListener('click', function() {
                        infoWindow.open(map, marker);
                    });
                    orders_markers.push(marker);
                } else {
                    //marcador para transportador
                    if (transporter_marker_aux)
                        transporter_marker_aux.setMap(null);

                    var transporter_marker = new google.maps.Marker({
                        position: location,
                        map: map,
                        zIndex: parseInt(index)
                    });
                    var infoWindow = new google.maps.InfoWindow({
                        content: '<p>Ultima actualización: ' + content + '</p>'
                    });
                    transporter_marker.addListener('click', function() {
                        infoWindow.open(map, transporter_marker);
                    });
                    transporter_marker.setIcon('{{ asset_url() }}img/markermerqueo.png');
                    transporter_marker.setMap(map);
                    transporter_marker_aux = transporter_marker;
                }
            }

            //pinta ruta recorrida del trasnportador
            function drawPolyLine(poly, m) {
                var poly_lines = new google.maps.Polyline({
                    path: poly,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 4
                });

                poly_lines.setMap(m);
            }

        });
    </script>
@stop
