@extends('admin.layout')

@section('content')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ Config::get('app.google_api_key') }}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            position: absolute;
        }
    </style>

    <section class="content-header">
        <h1>
            {{ $title }} - {{ $plate }}
            <small>Control panel</small>
        </h1>
    </section>
    <div class="content">
        <div class="box-body table-responsive">
            <div class="row">
                <div class="col-xs-6">
                    <form id="search-form">
                        <table width="100%" class="orders-storage-table">
                            <tr>
                                <td align="right"><label>Fecha de inicio:</label>&nbsp;</td>
                                <td><input type="text" placeholder="YYYY-MM-DD HH:mm:SS" id="start_date" class="form-control"></td>
                                <td align="right"><label>Fecha de finalización:</label>&nbsp;</td>
                                <td><input type="text" placeholder="YYYY-MM-DD HH:mm:SS" id="end_date" class="form-control"></td>
                                <td colspan="7" align="center">
                                    <button type="button" id="btn-traveled" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="row">
            <div id="map" class="col-xs-12"></div>
        </div>
    </section>
    <script>
        $(document).ready(function(e) {
            var vehicle_id = {{ $vehicle_id }};
            var map;
            var coords = [];
            var position = JSON.parse('{{ $position }}');
            var latlng = new google.maps.LatLng(parseFloat(position.latitude), parseFloat(position.longitude));
            var myOptions = {
                zoom: 12,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map'), myOptions);

            var poly_lines = new google.maps.Polyline();

            $('#start_date, #end_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:SS'
            });

            function clearPolyLine() {
                poly_lines.setMap(null);
            }

            //pinta ruta recorrida del trasnportador
            function drawPolyLine(poly, m) {
                poly_lines = new google.maps.Polyline({
                    path: poly,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 4
                });
                poly_lines.setMap(m);
            }

            $('#btn-traveled').click(function(e) {
                //cargar ruta recorrida del transportador
                clearPolyLine();
                var sd = $('#start_date').val();
                var ed = $('#end_date').val();
                sd = sd.replace(/\s/g, "T");
                ed = ed.replace(/\s/g, "T");
                console.log(sd, ed);
                $.ajax({
                    url: '{{ Config::get('app.transporters.location.url') }}/api/tracking/v1/traveled/' + vehicle_id + '/' + sd +'/' + ed,
                    method: 'GET',
                    type: 'json',
                    async: true,
                    success: function(response) {
                        if (response.status) {
                            coords = [];
                            console.log(response);
                            $.each(response.result, function(value, data) {
                                coords.push({lat: data.latitude, lng: data.longitude})
                            });
                            drawPolyLine(coords, map);
                        }
                    },
                    error: function(error) {
                        console.error('Ocurrió un error al cargar la ruta recorrida: ', error);
                    }
                });
            });
        });
    </script>
@stop
