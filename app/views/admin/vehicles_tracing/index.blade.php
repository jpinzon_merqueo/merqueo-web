@if (!Request::ajax())

    @extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>
                    <b>Alert!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>
                    <b>Alert!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form class="tracing-table">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group col-md-3">
                                    <label>Ciudad</label>
                                    <select id="city_id" name="city_id" class="form-control">
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Ruta</label>
                                    <select id="route_id" name="route_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Transportador</label>
                                    <select id="transporter_id" name="transporter_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group  col-md-3">
                                    <label>Conductor</label>
                                    <select id="driver_id" name="driver_id" class="form-control">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Rutas finalizadas</label>
                                    <select id="show_orders" name="show_orders" class="form-control">
                                        <option value="">Selecciona</option>
                                        <option value="1">Mostrar</option>
                                        <option value="0">Ocultar</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Ordenar por</label>
                                    <select id="order_by" name="order_by" class="form-control">
                                        <option value="dispatched_date" selected="selected">Hora de despacho</option>
                                        <option value="status">Pedidos pendientes</option>
                                        <option value="plate">Placa</option>
                                        <option value="last_time">Tiempo desde última entrega</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Buscar</label>
                                    <input type="text" placeholder="Placa" id="input-search" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                        <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                    </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        var web_url_ajax = "{{ route('adminVehicleTracing.vehiclesTracing') }}";
        $(document).ready(function() {
            paging(1, '');

            $('#city_id').change(function () {
                let city_id = $(this).val();
                $('#route_id').empty().append('<option value="">Selecciona</option>');
                $('#transporter_id').empty().append('<option value="">Selecciona</option>');
                $('#driver_id').empty().append('<option value="">selecciona</option>');
                $('#route_id').prop('disabled', true);
                $('#transporter_id').prop('disabled', true);
                $.ajax({
                    url: '{{ route('adminOrderStorage.getStoresTransportersByCityAjax') }}',
                    type: 'get',
                    dataType: 'json',
                    data: { city_id: city_id },
                })
                .done(function(data) {
                    let html = '';
                    html = '<option value="NULL">Sin ruta</option>';
                    $.each(data.routes, function(index, val) {
                        html += '<option value="'+ val.id +'">'+ val.route +'</option>';
                    });
                    $('#route_id').append(html);
                    $('#route_id').prop('disabled', false);
                })
                .fail(function() {
                    console.log("Error en la consulta para obtener tiendas, transportadores y alistadores por ciudad");
                    $('#route_id').prop('disabled', false);
                });

                $.ajax({
                    url: '{{ route('adminVehicleTracing.getTransportersAjax') }}',
                    type: 'post',
                    dataType: 'json',
                    data: { city_id: city_id},
                }).done(function(data) {
                    let html = '';
                    html = '';
                    $.each(data.transporters, function(index, val) {
                        html += '<option value="'+ val.id +'">'+ val.fullname +'</option>';
                    });
                    $('#transporter_id').append(html);
                    $('#transporter_id').prop('disabled', false);
                }).fail(function() {
                    console.log("Error en la consulta para obtener tiendas, transportadores y alistadores por ciudad");
                    $('#transporter_id').prop('disabled', false);
                });
            });
            $('#city_id').trigger('change');
            /*$('#route_id').change(function () {
                let city_id = $('#city_id').val();
                let route_id  = $(this).val();
                $('#transporter_id').empty().append('<option value="">Selecciona</option>');
                $('#transporter_id').prop('disabled', true);
                $.ajax({
                    url: '{{-- route('adminVehicleTracing.getTransportersAjax') --}}',
                    type: 'post',
                    dataType: 'json',
                    data: { city_id: city_id, route_id: route_id },
                }).done(function(data) {
                    let html = '';
                    html = '';
                    $.each(data.transporters, function(index, val) {
                        html += '<option value="'+ val.id +'">'+ val.fullname +'</option>';
                    });
                    $('#transporter_id').append(html);
                    $('#transporter_id').prop('disabled', false);
                }).fail(function() {
                    console.log("Error en la consulta para obtener tiendas, transportadores y alistadores por ciudad");
                    $('#transporter_id').prop('disabled', false);
                });
            });*/
            $('#route_id').trigger('change');
            $('#transporter_id').change(function () {
                let transporter_id  = $(this).val();
                $('#driver_id').empty().append('<option value="">Selecciona</option>');
                $('#driver_id').prop('disabled', true);
                $.ajax({
                    url: '{{ route('adminVehicleTracing.getDriversAjax') }}',
                    type: 'post',
                    dataType: 'json',
                    data: { transporter_id: transporter_id },
                }).done(function(data) {
                    let html = '';
                    html = '';
                    $.each(data.drivers, function(index, val) {
                        html += '<option value="'+ val.id +'">'+ val.fullname +'</option>';
                    });
                    $('#driver_id').append(html);
                    $('#driver_id').prop('disabled', false);
                }).fail(function() {
                    console.log("Error en la consulta para obtener tiendas, transportadores y alistadores por ciudad");
                    $('#driver_id').prop('disabled', false);
                });
            })
        });
    </script>
@else

@section('content')
    <br />
    <table id="tracing-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Placa</th>
            <th>Ruta</th>
            <th>Transportador</th>
            <th>Hora de despacho</th>
            <th># Pedidos asignados</th>
            <th># Pedidos despachados</th>
            <th># Total pedidos entregados</th>
            <th># Pedidos cancelados</th>
            <th># Pedidos pendientes</th>
            <th>Tiempo desde la última entrega</th>
            <th>Tiempo promedio</th>
            <th>Valor total de ruta</th>
            <th>Valor total recaudado</th>
            <th>Mapa</th>
        </tr>
        </thead>
        <tbody>
        @if(count($vehicles))
            <input type="hidden" name="count" class="count" value="{{count($vehicles)}}" />
            <?php $average_time = $total_orders_pending = $total_orders_delivered = $total_orders_cancelled = $total_orders = 0; ?>
            <?php $total_hour_5_7 = $total_hour_7_9 = $total_hour_9_11 = $total_hour_11_14 = $total_hour_14_17 = $total_hour_18_22 = $total_hour_18_20 = $total_hour_20_22 = 0?>
            @foreach($vehicles as $key => $vehicle)
                <tr>
                    <td>{{ $vehicle->plate }}</td>
                    <td>{{ $vehicle->route }}</td>
                    <td>{{ $vehicle->fullname }} / {{ $vehicle->driver }}</td>
                    <td>{{ $vehicle->hour_dispatched }}</td>
                    <td align="center">{{ $vehicle->orders_assigned }}</td>
                    <td>
                        <table border="1" align="center" class="table table-bordered table-striped">
                            <tr>
                                @foreach($delivery_windows AS $delivery_window)
                                <th align="center">{{$delivery_window}}</th>
                                @endforeach

                            </tr>
                            <tr>
                                <td align="center">{{ $vehicle->hour_5_7 }}</td>
                                <td align="center">{{ $vehicle->hour_7_9 }}</td>
                                <td align="center">{{ $vehicle->hour_9_11 }}</td>
                                <td align="center">{{ $vehicle->hour_11_14 }}</td>
                                <td align="center">{{ $vehicle->hour_14_17 }}</td>
                                <td align="center">{{ $vehicle->hour_18_22 }}</td>
                                <td align="center">{{ $vehicle->hour_18_20 }}</td>
                                <td align="center">{{ $vehicle->hour_20_22 }}</td>
                            </tr>
                        </table>
                    </td>
                    <td align="center">{{ $vehicle->total_orders_delivered }}</td>
                    <td align="center">{{ $vehicle->total_orders_cancelled }}</td>
                    <td align="center">{{ $vehicle->total_orders_pending }}</td>
                    <td>
                        @if( ($vehicle->total_orders_delivered + $vehicle->total_orders_cancelled) == 0 )
                        @elseif( $vehicle->orders_assigned == ($vehicle->total_orders_delivered + $vehicle->total_orders_cancelled) )
                            Ruta finalizada
                        @else
                            <?php echo get_time('', $vehicle->last_time_delivered); ?>
                        @endif
                    </td>
                    <td>@if( $vehicle->orders_dispatched != $vehicle->total_orders_pending)
                        <?php
                            $timestamp = strtotime(date('Y-m-d H:i:s')) - $vehicle->average_delivered_orders;
                            $average_time = strtotime(date('Y-m-d H:i:s')) - $vehicle->average_delivered_time;
                            echo get_time('', date('Y-m-d H:i:s', $timestamp));
                        ?>
                        @endif
                    </td>
                    <td align="right">${{ number_format($vehicle->total_amount_route, 0, ',', '.') }}</td>
                    <td align="right">${{ number_format($vehicle->total_amount_current_delivered, 0, ',', '.') }}</td>
                    <td align="center">
                        <div class="btn-group">
                           <a class="btn btn-xs btn-default" href="{{ route('adminVehicleTracing.mapTracing', ['vehicle_id' => $vehicle->id, 'city_id' => $vehicle->user_city_id, 'route_id' => $vehicle->route_id]) }}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span></a>
                        </div>
                    </td>
                </tr>
                <?php
                    $total_hour_5_7 = $total_hour_5_7 + $vehicle->hour_5_7;
                    $total_hour_7_9 = $total_hour_7_9 + $vehicle->hour_7_9;
                    $total_hour_9_11 = $total_hour_9_11 + $vehicle->hour_9_11;
                    $total_hour_11_14 = $total_hour_11_14 + $vehicle->hour_11_14;
                    $total_hour_14_17 = $total_hour_14_17 + $vehicle->hour_14_17;
                    $total_hour_18_22 = $total_hour_18_22 + $vehicle->hour_18_22;
                    $total_hour_18_20 = $total_hour_18_20 + $vehicle->hour_18_20;
                    $total_hour_20_22 = $total_hour_20_22 + $vehicle->hour_20_22;
                    $total_orders = $total_orders + $vehicle->orders_dispatched;
                    $total_orders_pending = $total_orders_pending + $vehicle->total_orders_pending;
                    $total_orders_delivered = $total_orders_delivered + $vehicle->total_orders_delivered;
                    $total_orders_cancelled = $total_orders_cancelled + $vehicle->total_orders_cancelled;
                ?>
            @endforeach
                <tr>
                    <td colspan="4">&nbsp;</td>
                    <td align="center"><b><?php echo $total_orders; ?></b></td>
                    <td>
                        <table border="1" align="center" class="table table-bordered table-striped">
                            <tr>
                                @foreach($delivery_windows AS $delivery_window)
                                    <th align="center">{{$delivery_window}}</th>
                                @endforeach

                            </tr>
                            <tr>
                                <td align="center">{{ $total_hour_5_7 }}</td>
                                <td align="center">{{ $total_hour_7_9 }}</td>
                                <td align="center">{{ $total_hour_9_11 }}</td>
                                <td align="center">{{ $total_hour_11_14 }}</td>
                                <td align="center">{{ $total_hour_14_17 }}</td>
                                <td align="center">{{ $total_hour_18_22 }}</td>
                                <td align="center">{{ $total_hour_18_20 }}</td>
                                <td align="center">{{ $total_hour_20_22 }}</td>
                            </tr>
                        </table>
                    </td>
                    <td align="center"><b><?php echo $total_orders_delivered; ?></b></td>
                    <td align="center"><b><?php echo $total_orders_cancelled; ?></b></td>
                    <td align="center"><b><?php echo $total_orders_pending; ?></b></td>
                    <td>&nbsp;</td>
                    <td><?php echo get_time('', date('Y-m-d H:i:s', $average_time)); ?></td>
                    <td colspan="3">&nbsp;</td>
                </tr>
        @else
            <tr><td colspan="15" align="center">No hay vehículos con pedidos pendientes.</td></tr>
        @endif
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($vehicles) }} registros</div>
        </div>
    </div>
    @endif

@stop