@extends('admin.layout')

@if (!Request::ajax())
    @section('content')
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <section class="content-header">
            <h1>
                {{ $title }}
                <small>Control panel</small>
            </h1>
            <span class="breadcrumb" style="top:0px">
				@if (!empty($banner))
                    <a href="{{ route('adminBanners.banners') }}">
                        <button type="button" class="btn btn-primary">Regresar</button>
                    </a>
                @endif
			</span>
        </section>
        <section class="content">
            <div class="row">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alerta!</b> {{ Session::get('success') }}
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('error') }}
                </div>
                @endif
            </div>
            <form id="add-banner-form" action="{{ route('adminBanners.save') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="city_id" class="control-label">Ciudad</label>
                                            <select name="city_id" id="city_id" class="form-control" v-model="city_id" :disabled="isLoading" @change="getStores(); getWarehouses();" required="required">
                                                <option value="">-Selecciona-</option>
                                            @if(!empty($cities))
                                                @foreach($cities as $city)
                                                    <option value="{{ $city->id }}" {{ isset($banner) && $banner->city_id == $city->id ? 'selected' : '' }}>{{ $city->city }}</option>
                                                @endforeach
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group store_container" v-show="city_id != ''" :disabled="isLoading">
                                            <label for="store_id" class="control-label">Tienda</label>
                                            <select name="store_id" id="store_id" class="form-control" v-model="store_id" :disabled="isLoading" required="required">
                                                <option value="">-Selecciona-</option>
                                                <option v-for="(store, index) in stores" :value="store.id">@{{ store.name }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group" v-show="store_id != ''" :disabled="isLoading">
                                            <label for="warehouse_id">Bodegas:</label>
                                            <multiselect v-model="warehouses_selected" :options="warehouses" :multiple="true" :close-on-select="false" :clear-on-select="false" :hide-selected="true" :preserve-search="true" placeholder="Selecciona uno o varios" label="label" track-by="value" :preselect-first="true">
                                            </multiselect>
                                            <input type="hidden" v-for="(warehouse, index) in warehouses_selected" :name="'warehouses['+index+']'" :value="warehouse.value" class="required">
                                        </div>
                                    </div>
                                </div>
                                <div :class="'row ' + (city_id != '' && store_id != '' ? '' : 'hide' )">
                                    <div class="col-xs-4">
                                        <div class="form-group category_container">
                                            <label for="category" class="control-label">Categoría</label>
                                            <select name="category" id="category" v-model="category" class="form-control"  required="required" @change="getProducts">
                                                <option value="">-Selecciona-</option>
                                                <option value="Producto">Producto</option>
                                                <option value="Informativo">Informativo</option>
                                                <option value="Conjunto de productos">Conjunto de productos</option>
                                                <option value="Deeplink">Deeplink</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group status_container">
                                            <label for="status">Estado</label>
                                            <select name="status" id="status" v-model="status" class="form-control" required>
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="platforms">Plataformas</label>
                                            <select name="platforms[]" id="platforms" class="form-control required" multiple v-model="selected_platform" required>
                                                <option v-for="(platform, index) in platforms" :value="platform">@{{ platform | capitalize }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div :class="'row ' + (city_id != '' && store_id != '' ? '' : 'hide' )">
                                    <div class="col-xs-4 target_container">
                                        <div class="form-group">
                                            <label for="target">Tipo de ventana</label>
                                            <select name="target" id="target" class="form-control">
                                                <option value="_self">Misma ventana</option>
                                                <option value="_blank">Nueva ventana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 url_container">
                                        <div class="form-group">
                                            <label for="url">Url</label>
                                            <input type="text" name="url" id="url" v-model="url" class="form-control" placeholder="Ingrese la URL">
                                        </div>
                                    </div>
                                    <div class="col-xs-4 form-group">
                                        <label for="deeplink_url">Deeplink dinamico</label>
                                        <input type="text" name="deeplink_url" id="deeplink_url" class="form-control" readonly v-model="deeplink_url">
                                    </div>
                                    <div class="col-xs-4 form-group">
                                        <label for="deeplink_url">Deeplink (Leanplum, Appflyer OneLink)</label>
                                        <input type="text" name="leanplum_url" id="leanplum_url" class="form-control" readonly v-model="leanplum_url">
                                    </div>
                                </div>
                                <div :class="'row ' + (city_id != '' && store_id != '' ? '' : 'hide' )">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="show_start_date">Fecha de activación</label>
                                            <input type="text" name="show_start_date" id="show_start_date" placeholder="Fecha de activación" value="{{ !empty($banner->show_start_date) ? Carbon\Carbon::parse($banner->show_start_date)->format('d/m/Y h:i A') : '' }}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="show_end_date">Fecha de desactivación</label>
                                            <input type="text" id="show_end_date" name="show_end_date" placeholder="Fecha de desactivación" value="{{ !empty($banner->show_end_date) ? Carbon\Carbon::parse($banner->show_end_date)->format('d/m/Y h:i A') : '' }}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div :class="'row ' + (city_id != '' && store_id != '' ? '' : 'hide' )">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="title">Nombre administrativo</label>
                                            <input type="text" id="title" name="title" class="form-control" v-model="title" placeholder="Título administrativo del banner" required>
                                        </div>
                                    </div>
                                    <div class="col-xs-12" v-show="category == 'Informativo' || category == 'Conjunto de productos' || category == 'Deeplink'">
                                        <div class="form-group">
                                            <label for="description">Descripción</label>
                                            <input type="text" id="description" name="description" v-model="description" class="form-control" placeholder="Descripción del banner" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header">
                        <div class="box-title">
                            Ubicación del banner
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-3 form-group">
                                <label for="is_home">¿Se muestra en el home?</label>
                                <select name="is_home" id="is_home" v-model="isHome" class="form-control">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="col-xs-3 form-group" v-show="isHome == 1">
                                <label for="position">Posición en Home</label>
                                <input type="number" min="0" id="position" name="position" v-model="position" class="form-control">
                            </div>
                            <div class="col-xs-3 form-group">
                                <label for="is_department">¿Se muestra en departamentos?</label>
                                <select name="is_department" id="is_department" v-model="is_department" class="form-control" @change="getDepartmentsLocation">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" v-show="is_department == 1">
                            <div class="col-xs-12 form-group" style="overflow: auto; max-height: 250px;">
                                <h4>Departamentos encontrados</h4>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Departamento</th>
                                            <th>Posición</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(department, index) in departments_location">
                                            <td>@{{ index + 1 }}</td>
                                            <td>@{{ department.id }}</td>
                                            <td>@{{ department.name }}</td>
                                            <td>
                                                <input type="number" min="0" v-model="department.position" class="form-control">
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary" @click="addDepartment(department, index)">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" v-show="is_department == 1">
                            <div class="col-xs-12" style="overflow: auto; max-height: 250px;">
                                <h4>Departamentos agregados</h4>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Departamento</th>
                                            <th>Posición</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(department, index) in departments_added">
                                            <td>@{{ index + 1 }}</td>
                                            <td>@{{ department.id }}</td>
                                            <td>@{{ department.name }}</td>
                                            <td>
                                                <input type="number" min="1" v-model="department.position" :name="'departments['+department.id+']'" class="form-control">
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary" @click="removeDepartment(department, index)">
                                                    <i class="glyphicon glyphicon-minus"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header">
                        <div class="box-title">
                            Condiciones del banner
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-4 form-group">
                                <label for="is_home">¿Se muestra con base a pedidos del usuario?</label>
                                <select name="user_orders_quantity_condition"
                                        id="user_orders_quantity_condition"
                                        v-model="hasConditions"
                                        class="form-control">
                                    <option value="">No</option>
                                    <option value="true">Si</option>
                                </select>
                            </div>
                            <div v-if="hasConditions">
                                <div class="col-xs-3 form-group">
                                    <label for="user_orders_quantity_condition">Condición</label>
                                    <select name="user_orders_quantity_condition"
                                            id="user_orders_quantity_condition"
                                            v-model="user_orders_quantity_condition"
                                            class="form-control">
                                        <option value="">Operadores</option>
                                        <option value=">">Mayor que</option>
                                        <option value="<">Menor que</option>
                                        <option value="=">Igual</option>
                                    </select>
                                </div>
                                <div class="col-xs-3 form-group">
                                    <label for="position">Número de pedidos entregados</label>
                                    <input type="number"
                                           min="0"
                                           id="user_orders_quantity"
                                           name="user_orders_quantity"
                                           v-model="user_orders_quantity"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary" v-show="category">
                    <div class="box-header">
                        <h3 class="box-title">Ventana emergente</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-6" >
                                <div class="form-group">
                                    <label for="content_title">Título</label>
                                    <input type="text" id="content_title" name="content_title" class="form-control" placeholder="Título" v-model="content_title">
                                </div>
                            </div>
                            <div class="col-xs-6" v-show="category == 'Conjunto de productos' || category == 'Informativo'">
                                <div class="form-group">
                                    <label for="content_subtitle">Subtítulo</label>
                                    <input type="text" id="content_subtitle" name="content_subtitle" class="form-control" placeholder="Subtítulo" v-model="content_subtitle">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12" v-show="category == 'Producto' || category == 'Deeplink'">
                                <div class="form-group">
                                    <label for="store_product_id">Producto</label>
                                    <multiselect v-model="store_product_id" :options="store_products" :searchable="true" :close-on-select="false" :show-labels="false" placeholder="Pick a value" label="label" track-by="value"></multiselect>
                                    <input type="hidden" id="store_product_id" name="store_product_id" :value="(store_product_id == null ? '' : store_product_id.value)" class="required">
                                </div>
                            </div>
                            @if(isset($banner) && !empty($image_product))
                                <div class="col-xs-12">
                                    <img src="{{ $image_product->image_medium_url }}" style="width: 170px;" class="img-responsive">
                                </div>
                            @endif
                            <div class="col-xs-12" v-show="category == 'Conjunto de productos'">
                                <div class="form-group">
                                    <label for="store_product_ids">Productos</label>
                                    <multiselect v-model="store_products_id" :options="store_products" :multiple="true" :close-on-select="false" :clear-on-select="false" :hide-selected="true" :preserve-search="true" placeholder="Selecciona uno o varios productos" label="label" track-by="value" :preselect-first="true">
                                    </multiselect>
                                    <input type="hidden" v-for="(product, index) in store_products_id" :name="'store_product_ids['+index+']'" :value="product.value" class="required store_products_id">
                                </div>
                            </div>
                        </div>
                        <div class="row" v-show="category == 'Informativo' || category == 'Conjunto de productos'">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="content">Cuerpo</label>
                                    <textarea name="content" id="content" rows="6" placeholder="Ingresa cuerpo" class="ckeditor" v-model="content"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Imágenes</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group image_app_url">
                                    <label for="image_app_url">Imagen para App</label>
                                    <input type="file" name="image_app_url" id="image_app_url" class="form-control" @if(!isset($banner) && empty($banner->image_app_url)) required="required" @endif>
                                    @if(isset($banner) && !empty($banner->image_app_url))
                                        <img src="{{ $banner->image_app_url }}" style="width: 170px;" class="img-responsive">
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group image_web_url">
                                    <label for="image_web_url">Imagen para Web</label>
                                    <input type="file" name="image_web_url" id="image_web_url" class="form-control" @if(!isset($banner) && empty($banner->image_web_url)) required="required" @endif>
                                    @if(isset($banner) && !empty( $banner->image_web_url))
                                        <img src="{{ $banner->image_web_url }}" style="width: 170px;" class="img-responsive">
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group image_web_url_temp">
                                    <label for="image_web_url_temp">Imagen para Nueva Web</label>
                                    <input type="file" name="image_web_url_temp" id="image_web_url_temp" class="form-control" @if(empty($banner->image_web_url_temp)) required="required" @endif>
                                    @if(!empty($banner->image_web_url_temp))
                                        <img src="{{ $banner->image_web_url_temp }}" style="width: 170px;" class="img-responsive">
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3" v-show="category == 'Conjunto de productos' || category == 'Informativo'">
                                <div class="form-group image_app_modal_url">
                                    <label for="image_app_modal_url">Imagen interna</label>
                                    <input type="file" name="image_app_modal_url" id="image_app_modal_url" class="form-control" @if(!isset($banner) && empty($banner->image_app_modal_url)) required="required" @endif>
                                    @if(isset($banner) && !empty( $banner->image_app_modal_url))
                                        <img src="{{ $banner->image_app_modal_url }}" style="width: 170px;" class="img-responsive">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($banner))
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a href="javascript:;" id="banner_log" data-banner_id="{{ isset($banner) ? $banner->id : '' }}">Log del banner</a>
                    </div>
                </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Configuración de deeplinks para Apps</h3>
                    </div>
                    <div class="box-body">
                        <!-- Deeplinks -->
                        <fieldset id="deeplinks_container">
                            <div class="row">
                                <div class="form-group col-xs-4 deeplink">
                                    <label for="deeplink">Deeplink?</label>
                                    <select class="form-control" name="deeplink" id="deeplink" v-model="deeplinks" :disabled="isLoading">
                                        <option value="">-Selecciona-</option>
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="is_deeplink" v-show="deeplinks == 1">
                                <div class="row">
                                    <div class="form-group col-xs-4 deeplink_type">
                                        <label for="deeplink_type">Tipo de deeplink</label>
                                        <select class="form-control" name="deeplink_type" id="deeplink_type"  required="required" v-model="deeplink_type" :disabled="isLoading">
                                            <option value="">-Selecciona-</option>
                                            <option value="Store">Tienda</option>
                                            <option value="Department">Departamento</option>
                                            <option value="Shelf">Pasillo</option>
                                            <option value="Product">Producto</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4 deeplink_cities" v-show="deeplinks && deeplink_type">
                                        <label for="deeplink_cities">Ciudad</label>
                                        <select class="form-control" name="deeplink_city_id" id="deeplink_city_id" v-model="deeplink_city_id" @change="getDeeplinkStores" required="required" :disabled="isLoading">
                                            <option value="">-Selecciona-</option>
                                            @foreach($cities as $city)
                                                <option value="{{ $city->id }}">
                                                    {{ $city->city }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4 deeplink_store" v-show="deeplinks && (deeplink_type == 'Store' || deeplink_type == 'Department' || deeplink_type == 'Shelf' || deeplink_type == 'Product')">
                                        <label for="deeplink_store">Tienda</label>
                                        <select class="form-control" name="deeplink_store_id" id="deeplink_store_id" v-model="deeplink_store_id" @change="getDeeplinkDepartments" required="required" :disabled="isLoading">
                                            <option value="">-Selecciona-</option>
                                            <option v-for="(store, index) in deeplink_stores" :value="store.id">@{{ store.name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-4 deeplink_departments" v-show="deeplinks && (deeplink_type == 'Department' || deeplink_type == 'Shelf' || deeplink_type == 'Product')">
                                        <label for="deeplink_departments">Departamento</label>
                                        <select class="form-control" name="deeplink_department_id" id="deeplink_department_id" v-model="deeplink_department_id" @change="getDeeplinkShelves" required="required" :disabled="isLoading">
                                            <option value="">-Selecciona-</option>
                                            <option v-for="(department, index) in deeplink_departments" :value="department.id">@{{ department.name }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4 deeplink_shelves" v-show="deeplinks && (deeplink_type == 'Shelf' || deeplink_type == 'Product')">
                                        <label for="deeplink_shelves">Pasillo</label>
                                        <select class="form-control" name="deeplink_shelf_id" id="deeplink_shelf_id" v-model="deeplink_shelf_id" @change="getDeeplinkStoreProduct" required="required" :disabled="isLoading">
                                            <option value="">-Selecciona-</option>
                                            <option v-for="(shelf, index) in deeplink_shelves" :value="shelf.id">@{{ shelf.name }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4 deeplink_products" v-show="deeplinks && deeplink_type == 'Product'">
                                        <label for="deeplink_products">Producto</label>
                                        <select class="form-control" name="deeplink_store_product_id" id="deeplink_store_product_id" v-model="deeplink_store_product_id" required="required" :disabled="isLoading">
                                            <option value="">-Selecciona-</option>
                                            <option v-for="(deeplink_store_product, index) in deeplink_store_products" :value="deeplink_store_product.id">@{{ deeplink_store_product.name }} @{{ deeplink_store_product.quantity }} @{{ deeplink_store_product.unit }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    @if(!empty($banner))
                                        <input type="hidden" name="id" value="{{ $banner->id }}">
                                    @endif
                                    <button class="btn btn-primary" type="submit" :disabled="isLoading">Guardar</button>
                                    <button class="btn btn-primary" type="reset" :disabled="isLoading">Reset</button>
                                    @if(!empty($banner))
                                    <a class="btn btn-success" href="{{ route('adminBanners.duplicate', ['id' => $banner->id]) }}" onclick="return confirm('Are you sure you want to duplicate it?')">Duplicar</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{ Form::token() }}
                    </div>
                </div>
            </form>
        </section>

        <!-- Log Banner Modal -->
        <div class="modal fade" id="modal-banner_log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="max-width: 1000px; width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Log de Banner</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-body table-responsive container-overflow">
                                        <table id="banner-logs-table" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Admin</th>
                                                    <th>Cambio</th>
                                                    <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody id="banner-logs-table-tbody">
                                            </tbody>
                                        </table><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
        @include('admin.banners.js.banner-js')
    @endsection
@else
    @section('logs')
        @if (count($banner_logs))
            @foreach($banner_logs as $key => $log)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $log['admin'] }}</td>
                    <td>
                        @if ( isset($log['changes']) )
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Atributo</th>
                                        <th>Original</th>
                                        <th>Cambio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($log['changes'] as $key => $log_item)
                                            <tr>
                                                <td>
                                                    {{ $key }}
                                                </td>
                                                <td>
                                                    {{ $log_item['original'] }}
                                                </td>
                                                <td>
                                                    {{ $log_item['change'] }}
                                                </td>
                                            </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </td>
                    <td>{{ $log['created_at'] }}</td>
                </tr>
            @endforeach
        @else
        <tr><td colspan="6" align="center">No hay datos.</td></tr>
        @endif
    @endsection
@endif
