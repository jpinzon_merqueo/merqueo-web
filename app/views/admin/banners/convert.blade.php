<style type="text/css">
    p {
        font-family: 'Open Sans', sans-serif;
    }
    .content{
        background-color: black;
        height: 390px;
        width: 945px;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f2e51e+0,05974a+100 */
        background: #f2e51e; /* Old browsers */
        background: -webkit-linear-gradient(bottom left, #f2e51e 0%, #05974a 100%);
        background: -o-linear-gradient(bottom left, #f2e51e 0%, #05974a 100%);
        background: linear-gradient(to top right, #f2e51e 0%, #05974a 100%); /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */ /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2e51e', endColorstr='#05974a',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
        border-radius: 10px;
    }

    .imgProducto {
        width: 40%;
        float: left;
    }
    .fotoProducto {
        width: 100%;
    }
    .Texto {
        padding: 25px 25px;
    }
    .productName {
        font-size: 38px;
        color: #fff;
        text-align: right;
        font-weight: 600;
        line-height: 30px;
        margin: 0;
    }
    .productName span{
        font-size: 30px;
        color: #fff;
        text-align: right;
        font-weight: 300;
    }
    .productPrice {
        text-align: right;
        font-size: 84px;
        line-height: 0px;
        margin-top: 7%;
        color: #fff;
        font-weight: 700;
    }
    .productOtherSups {
        text-align: right;
        color: #fff;
        font-size: 34px;
        font-weight: 300;
        margin-top: -3%;
    }
    .productOtherSups span {
        font-size: 47px;
        font-weight: 400;
        text-decoration:line-through;
    }
    button.pedirBTN {
        float: right;
        width: 268px;
        height: 74px;
        font-size: 34px;
        border-radius: 13px;
        background-color: white;
        border: none;
        color: #d60d64;
        margin-top: -2%;
    }
</style>
 <section class="content-header">
        <h1>
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
    		<a href="{{ route('adminBanners.add') }}">
    			<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Banner</button>
    		</a>
    	</span>
    </section>
    <section class="content">
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="content" id="no">
                <div class="imgProducto">
                    <img class="fotoProducto" src="{{ asset_url() }}img/producto.png" alt="">
                </div>

                <div class="Texto">
                    <p class="productName">Combo Bon yurt vaso x 4<br><span>170Gr</span></p>
                    <p class="productPrice">$7.500</p>
                    <p class="productOtherSups">otros supermercados: <span>$9.500</span></p>
                    <button class="pedirBTN">Pedir ahora</button>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset_url() }}js/jquery.min.js"></script>
    <script src="{{asset_url()}}js/dom-to-image.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var node = document.getElementById('no');

            domtoimage.toPng(node)
            .then(function (dataUrl) {
                var img = new Image();
                img.src = dataUrl;
                document.body.appendChild(img);
            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
        });
    </script>
