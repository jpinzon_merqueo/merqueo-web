@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <script>
        var web_url_ajax = "{{ route('adminBanners.banners') }}";
    </script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminBanners.add') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Banner</button>
            </a>
        </span>
    </section>
    <section class="content" id="content">
         @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
         @if(Session::has('success') )
        <div class="alert alert-success">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('success') }}
        </div>
        @endif
         <div class="row">
             <div class="col-xs-12">
                 <div class="box box-primary">
                     <div class="box-body table-responsive">
                         <div class="row">
                             <div class="col-xs-12">
                                 <form id="search-form" class="banners-table" @submit.prevent="searchBanners">
                                     <div class="row">
                                         <div class="form-group col-xs-3">
                                             <label for="city_id">Ciudad</label>
                                             <select id="city_id" name="city_id" v-model="city_id" class="form-control" :disabled="isLoading" @change="getStores">
                                                 <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                                             </select>
                                         </div>
                                         <div class="form-group col-xs-3">
                                             <label for="store_id">Tienda</label>
                                             <select id="store_id" name="store_id" v-model="store_id" class="form-control" :disabled="isLoading">
                                                 <option value="">-Selecciona-</option>
                                                 <option v-for="(store, index) in stores" :value="store.id">@{{ store.name }}</option>
                                             </select>
                                         </div>
                                         <div class="form-group col-xs-3">
                                             <label for="search_name">Nombre administrativo</label>
                                             <input type="text" placeholder="Nombre administrativo" id="search_name" name="search_name" v-model="search_name" class="search form-control" :disabled="isLoading">
                                         </div>
                                         <div class="form-group col-xs-3">
                                             <label for="">Estado</label>
                                             <select id="status" name="status" v-model="status" class="form-control" :disabled="isLoading">
                                                 <option value="1">Activo</option>
                                                 <option value="0">Inactivo</option>
                                             </select>
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="form-group col-xs-3">
                                             <label for="location">Ubicación</label>
                                             <select name="location" id="location" v-model="location" class="form-control" @change="getDepartments" :disabled="isLoading">
                                                 <option value="">-Selecciona-</option>
                                                 <option value="home">Home</option>
                                                 <option value="departments">Departamentos</option>
                                             </select>
                                         </div>
                                         <div class="form-group col-xs-3" v-show="departments.length > 0">
                                             <label for="department_id">Departamento</label>
                                             <select name="department_id" id="department_id" v-model="department_id" class="form-control" :disabled="isLoading">
                                                 <option value="">-Selecciona-</option>
                                                 <option v-for="(department, index) in departments" :value="department.id">@{{ department.name }}</option>
                                             </select>
                                         </div>
                                         <div class="form-group col-xs-3">
                                             <label style="color: #fff; display: block">|</label>
                                             <button type="submit" class="btn btn-primary search" :disabled="isLoading">Buscar</button>&nbsp;&nbsp;<button type="reset" class="btn btn-primary">Resetear</button>
                                         </div>
                                     </div>
                                 </form>
                             </div>
                         </div>
                         <div align="center" class="paging-loading" v-show="isLoading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-xs-12">
                 <div class="box box-primary">
                     <div class="box-body table-responsive">
                         <div class="row">
                             <div class="col-xs-12">
                                 <table id="banners-table" class="banners-table table table-bordered table-striped">
                                     <thead>
                                     <tr>
                                         <th>#</th>
                                         <th>ID</th>
                                         <th>Categoría</th>
                                         <th>Nombre administrativo</th>
                                         <th>Imagen Web</th>
                                         <th>Imagen App</th>
                                         <th>Tienda</th>
                                         <th>Estado</th>
                                         <th>Posición</th>
                                         <th>Acción</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                        <tr v-for="(banner, index) in banners">
                                            <td>@{{ index + 1 }}</td>
                                            <td>@{{ banner.id }}</td>
                                            <td>@{{ banner.category }}</td>
                                            <td>@{{ banner.title }}</td>
                                            <td>
                                                <img :src="banner.image_web_url" alt="" width="80">
                                            </td>
                                            <td>
                                                <img :src="banner.image_app_url" alt="" width="80">
                                            </td>
                                            <td>@{{ banner.store_name }}</td>
                                            <td>
                                                <span class="label label-success" v-show="banner.status === 1">Activo</span>
                                                <span class="label label-danger" v-show="banner.status === 0">Inactivo</span>
                                            </td>
                                            <td>
                                                <span class="label label-default" v-show="banner.is_home ===1">Home: @{{ banner.position }}</span>
                                                <br>
                                                <span style="display: inline-block;" class="label label-default" v-for="(department, index) in banner.departments" style="margin-right: .5rem">
                                                    @{{ department.name }}: @{{ department.pivot.position }}
                                                </span>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a :href="'{{ route('adminBanners.edit') }}/'+banner.id">Editar</a></li>
                                                        <li class="divider"></li>
                                                        <!--<li><a :href="'{{ route('adminBanners.delete') }}/'+banner.id" onclick="return confirm('Are you sure you want to delete it?')">Borrar</a></li>
                                                        <li class="divider"></li>-->
                                                        <li><a :href="'{{ route('adminBanners.duplicate') }}/'+banner.id" onclick="return confirm('Are you sure you want to duplicate it?')">Duplicar</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr v-show="banners.length == 0">
                                            <td colspan="10" class="text-center">No se encontraron productos</td>
                                        </tr>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
    </section>
    @include('admin.banners.js.index-js')
    <script type="text/javascript">
        /*function paging_banners(page, search)
        {
            $('.paging').html('');
            $('.paging-loading').show();

            if (!$('.banners-table').length) {
                data = { p: page, s: search };
            } else {
                if ($('.banners-table').length) {
                    data = {
                        status: $('#status').val(),
                        store_id: $('#store_id').val(),
                        city_id: $('#city_id').val(),
                        s: $('.search').val(),
                        p: 1
                    };
                }
            }
            $.ajax({
                url: web_url_ajax,
                data: data,
                type: 'GET',
                dataType: 'html',
                success: function(response) {
                    $('.paging-loading').hide();
                    $('.paging').html(response);
                }, error: function() {
                    $('.paging-loading').hide();
                    $('.paging').html('Ocurrió un error al obtener los datos.');
                }
            });
        }

        $(document).ready(function() {
            $('#city_id').change(function(e) {
                var data = {
                    city_id: $('#city_id').val()
                };

                $.ajax({
                    url: "{{ route('adminBanners.getStoresAjax') }}",
                    data: data,
                    type: 'GET',
                    dataType: 'json',
                    success: function(response) {
                        $('#store_id').empty();
                        $('#store_id').append('<option value="">-Selecciona-</option>');
                        $.each(response.result.stores, function(key, value) {
                            $('#store_id').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }, error: function(err) {
                        console.error(err);
                    }
                });
                e.preventDefault();
            });

            paging_banners(1, $(this).val());

            $('#btn-search').click(function(){
                paging_banners(1, '');
            });
        });*/
    </script>
    @endsection
@else
    @section('content')
    <table id="banners-table" class="banners-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Categoría</th>
                <th>Producto</th>
                <th>Nombre administrativo</th>
                <th>Imagen Web</th>
                <th>Imagen App</th>
                <th>Tienda</th>
                <th>Estado</th>
                <th>Posición</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
        @if (count($banners))
            @foreach($banners as $banner)
            <tr>
                <td>
                    {{ $banner['id'] }}
                </td>
                <td>
                    {{ $banner['category'] }}
                </td>
                <td>
                    @if($banner['category'] == 'Producto' && !empty($banner['image_small_url']))
                    <img style="height: 60px;" class="img-responsive" src="{{ $banner['image_small_url'] }}">
                    @endif
                </td>
                <td>
                    {{ $banner['title'] }}
                </td>
                <td>
                    <img src="{{ $banner['image_web_url'] }}" class="img-responsive" width="170">
                </td>
                <td>
                    <img src="{{ $banner['image_app_url'] }}" class="img-responsive" width="170">
                </td>
                <td>
                    <span class="label label-default">
                        {{ $banner['name'] }}
                    </span><br>
                </td>
                <td>
                    @if($banner['status'] == 1)
                        <span class="label label-success">
                            Activo
                        </span>
                    @else
                        <span class="label label-danger">
                            Inactivo
                        </span>
                    @endif
                </td>
                <td>
                    @if ( !empty($banner['position']) )
                        {{ $banner['position'] }}
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('adminBanners.edit', ['id' => $banner['id']]) }}">Editar</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminBanners.delete', ['id' => $banner['id']]) }}"  onclick="return confirm('Are you sure you want to delete it?')">Borrar</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('adminBanners.duplicate', ['id' => $banner['id']]) }}" onclick="return confirm('Are you sure you want to duplicate it?')">Duplicar</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="10" align="center">No se encontraron banners.</td></tr>
        @endif
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($banners) }} ítems</div>
        </div>
    </div>
    @endsection
@endif
