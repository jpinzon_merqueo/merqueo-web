<!--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-multiselect@2.0.6/dist/vue-multiselect.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vue-multiselect@2.0.6/dist/vue-multiselect.min.css">
<script type="text/javascript">

    Vue.component('vue-multiselect', window.VueMultiselect.default);
    var vue_obj = new Vue({
        el: '#add-banner-form',
        components: {
            Multiselect: window.VueMultiselect.default
        },
        data: {
            city_id: {{ !empty($banner->city_id) ? $banner->city_id : "''" }},
            store_id: {{ !empty($banner->store_id) ? $banner->store_id : "''" }},
            stores: {{ !empty($stores) ? $stores : "''" }},
            warehouses: {{ !empty($warehouses) ? $warehouses->toJson() : '[]' }},
            warehouses_selected: {{ !empty($selected_warehouses) ? $selected_warehouses : '[]' }},
            status: {{ !empty($banner->status) ? $banner->status : 0 }},
            isLoading: false,
            isHome: {{ !empty($banner->is_home) ? $banner->is_home : '0' }},
            category: '{{ !empty($banner->category) ? $banner->category : '' }}',
            url: '{{ !empty($banner->url) ? $banner->url : '' }}',
            position: '{{ !empty($banner->position) ? $banner->position : '' }}',
            show_start_date: '{{ !empty($banner->show_start_date) ? $banner->show_start_date : '' }}',
            show_end_date: '{{ !empty($banner->show_end_date) ? $banner->show_end_date : '' }}',
            title: "{{ !empty($banner->title) ? $banner->title : '' }}",
            description: "{{ !empty($banner->description) ? $banner->description : '' }}",
            content_title: "{{ !empty($banner->content_title) ? $banner->content_title : '' }}",
            content_subtitle: "{{ !empty($banner->content_subtitle) ? $banner->content_subtitle : '' }}",
            content: {{ !empty($banner->content) ? json_encode($banner->content) : '""' }},
            store_products: {{ !empty($store_products) ? $store_products->toJson() : '[]' }},
            store_product_id: {{ !empty($store_product_id) ? $store_product_id->toJson() : 'null' }},
            store_products_id: {{ !empty($related_products) ? $related_products->toJson() : 'null' }},
            platforms: ['web', 'ios', 'android'],
            selected_platform: {{ !empty($banner->platforms) ? json_encode(explode(',', $banner->platforms)) : '[]' }},
            deeplinks: '{{ !empty($banner->deeplink_type) ? 1 : "" }}',
            deeplink_type: '{{ !empty($banner->deeplink_type) ? $banner->deeplink_type : '' }}',
            deeplink_city_id: '{{ !empty($banner->deeplink_city_id) ? $banner->deeplink_city_id : '' }}',
            deeplink_stores: {{ !empty($stores_deeplink) ? $stores_deeplink->toJson() : '""' }},
            deeplink_store_id: '{{ !empty($banner->deeplink_store_id) ? $banner->deeplink_store_id : '' }}',
            deeplink_department_id: '{{ !empty($banner->deeplink_department_id) ? $banner->deeplink_department_id : '' }}',
            deeplink_departments: {{ !empty($departments_deeplink) ? $departments_deeplink->toJson() : '""' }},
            deeplink_shelf_id: '{{ !empty($banner->deeplink_shelf_id) ? $banner->deeplink_shelf_id : '' }}',
            deeplink_shelves: {{ !empty($shelves_deeplinks) ? $shelves_deeplinks->toJson() : '""' }},
            deeplink_store_product_id: '{{ !empty($banner->deeplink_store_product_id) ? $banner->deeplink_store_product_id : '' }}',
            deeplink_store_products: {{ !empty($deeplink_store_products) ? $deeplink_store_products->toJson() : '""' }},
            is_department: {{ !empty($banner) && $banner->departments()->get()->count() ? 1 : 0 }},
            departments_location: {{ !empty($departments_location) ? $departments_location->toJson() : '[]' }},
            departments_added: {{ !empty($departments_added) ? json_encode($departments_added) : '[]' }},
            deeplink_url: "{{ !empty($banner->deeplink_url) ? $banner->deeplink_url : '' }}",
            leanplum_url: "{{ !empty($banner->leanplum_url) ? $banner->leanplum_url : '' }}",
            hasConditions: "{{ !empty($banner->user_orders_quantity_condition) ? 'true' : '' }}",
            user_orders_quantity_condition: "{{ empty($banner->user_orders_quantity_condition) ? '' : $banner->user_orders_quantity_condition  }}",
            user_orders_quantity: {{ empty($banner->user_orders_quantity) ? 0 : $banner->user_orders_quantity }},
        },
        methods: {
            getWarehouses: function (e) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminBanners.getWarehousesAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.city_id
                    },
                    context: this
                })
                    .done(function(data) {
                        if (data.length != 0) {
                            this.warehouses = data
                        }
                        this.warehouses_selected = null
                    })
                    .fail(function () {
                        alert('Ocurrió un error al obtener las tiendas.')
                    })
                    .always(function () {
                        this.isLoading = false
                    });
            },
            getStores: function (e) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminBanners.getStoresAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.city_id
                    },
                    context: this
                })
                    .done(function(data) {
                        if (data.length != 0) {
                            this.stores = data
                        }
                        this.store_id = ''
                        this.store_product_id = null
                        this.store_products_id = null
                    })
                    .fail(function () {
                        alert('Ocurrió un error al obtener las tiendas.')
                    })
                    .always(function () {
                        this.isLoading = false
                    });
            },
            getDepartments: function (e) {
                this.isLoading = true
                let location = e.target.value;
                if (location == 'Departamento') {
                    $.ajax({
                        url: '{{ route('adminProducts.getDepartmentsAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            store_id: this.store_id
                        },
                        context: this
                    })
                        .done(function (data) {
                            if (data.length != 0) {
                                this.departments = data
                            }
                            this.store_product_id = null
                            this.store_products_id = null
                            this.department_id = ''

                        })
                        .fail(function () {
                            alert('Ocurrió un error al obtener los departamentos')
                        })
                        .always(function () {
                            this.isLoading = false
                        });
                } else {
                    this.departments = ''
                    this.department_id = ''
                    this.isLoading = false
                }
            },
            getDepartmentsLocation: function (e) {
                if (this.is_department === '1') {
                    this.isLoading = true
                    $.ajax({
                        url: '{{ route('adminBanners.getDepartmentsAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            store_id: this.store_id
                        },
                        context: this
                    })
                        .done(function (data) {
                            if (data.length !== 0) {
                                this.departments_location = data
                            }
                        })
                        .fail(function () {
                            alert('Ocurrió un error al obtener los departamentos')
                        })
                        .always(function () {
                            this.isLoading = false
                        });
                } else {
                    this.departments_location = [];
                    this.departments_added = [];
                }
            },
            getProducts: function (e) {
                if (this.category == 'Producto' || this.category == 'Conjunto de productos' || this.category == 'Deeplink') {
                    this.isLoading = true
                    $.ajax({
                        url: '{{ route('adminBanners.getProductsAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            store_id: this.store_id,
                        },
                        context: this
                    })
                        .done(function (data) {
                            this.store_products = ''
                            if (data.result.products.length) {
                                this.store_products = data.result.products
                            }
                            this.store_product_id = null
                            this.store_products_id = null
                        })
                        .fail(function () {
                            alert('Ocurrió un error al obtener los productos')
                        })
                        .always(function () {
                            this.isLoading = false
                        })
                }
            },
            getDeeplinkStores: function (e) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminBanners.getStoresAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.deeplink_city_id
                    },
                    context: this
                })
                    .done(function(data) {
                        this.deeplink_stores = ''
                        if (data.length != 0) {
                            this.deeplink_stores = data
                        }
                        this.deeplink_department_id = ''
                        this.deeplink_store_id = ''
                        this.deeplink_shelf_id = ''
                        this.deeplink_store_product_id = ''
                    })
                    .fail(function () {
                        alert('Ocurrió un error al obtener las tiendas.')
                    })
                    .always(function () {
                        this.isLoading = false
                    });
            },
            getDeeplinkDepartments: function (e) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProducts.getDepartmentsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        store_id: this.deeplink_store_id
                    },
                    context: this
                })
                    .done(function (data) {
                        this.deeplink_departments = ''
                        if (data.length != 0) {
                            this.deeplink_departments = data
                        }
                        this.deeplink_shelf_id = ''
                        this.deeplink_department_id = ''
                        this.deeplink_store_product_id = ''

                    })
                    .fail(function () {
                        alert('Ocurrió un error al obtener los departamentos')
                    })
                    .always(function () {
                        this.isLoading = false
                    });
            },
            getDeeplinkShelves: function (e) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProducts.getShelvesAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        department_id: this.deeplink_department_id
                    },
                    context: this
                })
                    .done(function (data) {
                        this.deeplink_shelves = ''
                        if (data.length != 0) {
                            this.deeplink_shelves = data
                        }
                        this.deeplink_shelf_id = ''
                        this.deeplink_store_product_id = ''
                    })
                    .fail(function () {
                        alert('Ocurrió un error al obtener los pasillos')
                    })
                    .always(function () {
                        this.isLoading = false
                    });
            },
            getDeeplinkStoreProduct: function (e) {
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminProducts.getProductsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        shelf_id: this.deeplink_shelf_id
                    },
                    context: this
                })
                    .done(function (data) {
                        if (data.length != 0) {
                            this.deeplink_store_products = data
                            this.deeplink_store_product_id = ''
                        } else {
                            this.deeplink_store_products = ''
                            this.deeplink_store_product_id = ''
                        }
                    })
                    .fail(function () {
                        alert('Ocurrió un error al obtener los productos')
                    })
                    .always(function () {
                        this.isLoading = false
                    });
            },
            onChange: function (obj) {
                this.store_product_id = obj.value;
            },
            addDepartment: function (department, index) {
                if (department.position !== 0) {
                    this.departments_location.splice(index, 1);
                    this.departments_added.push(department);
                } else {
                    alert('Debe ingresar una posición superior a 0 para poder agregar el departamento.');
                }
            },
            removeDepartment: function (department, index) {
                this.departments_added.splice(index, 1);
                this.departments_location.push(department);
            }
        },
        watch: {
            city_id: function (val) {
                if (val == '') {
                    this.store_id = ''
                    this.location = 'Home'
                    this.department_id = ''
                    this.category = ''
                }
            },
            store_id: function (val) {
                if (val == '') {
                    this.location = 'Home'
                    this.department_id = ''
                    this.category = ''
                }
            },
            category: function (val) {
                if (val == 'Producto') {
                    this.content_subtitle = ''
                    this.content = ''
                    this.store_products_id = null
                } else if (val == 'Informativo') {
                    this.store_products_id = null
                    this.store_product_id = null
                } else if (val == 'Conjunto de productos') {
                    this.content_subtitle = ''
                    this.store_product_id = null
                }
            },
            hasConditions(newValue) {
                if (!newValue) {
                    this.user_orders_quantity_condition = '';
                    this.user_orders_quantity = 0;
                }
            }
        },
        filters: {
            capitalize: function (value) {
                if (!value) return ''
                value = value.toString()
                return value.charAt(0).toUpperCase() + value.slice(1)
            }
        }
    });

    // $(document).ready(function() {
    $(window).load(function() {
        // $('#store_product_id').select2();
        $('#store_product_ids').select2();
        $('#show_start_date').datetimepicker({
            format: 'DD/MM/YYYY HH:mm A',
            sideBySide: true,
            useCurrent: false,
            defaultDate: false
        });
        $('#show_end_date').datetimepicker({
            format: 'DD/MM/YYYY HH:mm A',
            sideBySide: true
        });
        $("#show_start_date").on("dp.change", function (e) {
            $('#show_end_date').data("DateTimePicker").minDate(e.date);
            vue_obj.show_start_date = e.date.format('DD/MM/YYYY HH:mm A');
        });
        $("#show_end_date").on("dp.change", function (e) {
            $('#show_start_date').data("DateTimePicker").maxDate(e.date);
            vue_obj.show_end_date = e.date.format('DD/MM/YYYY HH:mm A');
        });

        $('#add-banner-form').validate({
            submitHandler: function(form) {
                var category = $('#category').val();
                if (category == 'Conjunto de productos') {
                    var il = $('.store_products_id').length;
                    if (il == 0) {
                        alert('Debe seleccionar productos');
                        return false;
                    }
                } else if (category == 'Deeplink') {
                    if ($('#store_product_id').val() == '') {
                        alert('Debe seleccionar un producto');
                        return false;
                    }
                }
                form.submit();
            },
            ignore: ":hidden:not(.required)",
            rules: {
                title: 'required',
                position: 'required',
                status: 'required',
                store_id: 'required',
                city_id: 'required',
                category: 'required',
                show_start_date: 'required',
                show_end_date: 'required',
                url: {
                    required: function(element) {
                        var category = $('#category').val();
                        return category == 'Deeplink';
                    }
                },
                content_title: {
                    required: function(element) {
                        var category = $('#category').val();
                        return category == 'Informativo' || category == 'Conjunto de productos';
                    }
                },
                content_subtitle: {
                    required: function(element) {
                        var category = $('#category').val();
                        return category == 'Conjunto de productos';
                    }
                },
                description: {
                    required: function(element) {
                        // var type = $('#type').val();
                        // return type == 'Imagen';
                    }
                },
                content: {
                    required: function(element) {
                        var category = $('#category').val();
                        return category == 'Informativo' || category == 'Conjunto de productos';
                    }
                },
                store_product_id: {
                    required: function(element) {
                        var type = $('#category').val();
                        return type == 'Producto';
                    }
                },
                'store_product_ids[]': {
                    required: function(element) {
                        console.log(element);
                        var category = $('#category').val();
                        return category == 'Conjunto de productos';
                    }
                },
                deeplink: {
                    required: function(element) {
                        var category = $('#category').val();
                        return category == 'Deeplink';
                    }
                },
                department_id: {
                    required: function (elemnt) {
                        var location = $('#location').val();
                        return location == 'Departamento';
                    }
                }
            }
        });
    });
</script>
