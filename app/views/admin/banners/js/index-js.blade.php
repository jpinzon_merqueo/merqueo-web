<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://npmcdn.com/vue-select@latest"></script>
<script type="text/javascript">
    var vue_obj = new Vue({
        el: '#content',
        data: {
            cities: {{ $cities->toJson() }},
            city_id: {{ Session::get('admin_city_id') }},
            stores: {{ $stores->toJson() }},
            store_id: '',
            search_name: '',
            status: 1,
            location: '',
            departments: [],
            department_id: '',
            isLoading: false,
            banners: []
        },
        methods: {
            searchBanners: function (e) {
                if (this.location === 'departments' && this.store_id === ''){
                    alert('Debe seleccionar una tienda para realizar esta búsqueda');
                    return;
                }
                if (this.location === 'departments' && this.department_id === ''){
                    alert('Debe seleccionar un departamento para realizar esta búsqueda');
                    return;
                }
                $.ajax({
                    url: '{{ route('adminBanners.banners') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        department_id: this.department_id,
                        city_id: this.city_id,
                        store_id: this.store_id,
                        search: this.search_name,
                        status: this.status,
                        location: this.location
                    },
                    context: this
                })
                    .done(function(data) {
                        this.banners = data;
                    })
                    .fail(function() {
                        console.log("error al caragar los banners");
                    })
                    .always(function() {
                        console.log("complete");
                    });
            },
            getDepartments: function (e) {
                if (this.location === 'departments') {
                    if (this.store_id === '') {
                        alert('Debe seleccionar una tienda para cargar los departamentos.');
                        this.location = '';
                        return;
                    }
                    $.ajax({
                        url: '{{ route('adminBanners.getDepartmentsAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            store_id: this.store_id
                        },
                        context: this
                    })
                        .done(function(data) {
                            this.departments = data;
                        })
                        .fail(function() {
                            console.log("error al caragar los departamentos");
                        })
                        .always(function() {
                            console.log("complete");
                        });
                } else {
                    this.departments = [];
                    this.department_id = '';
                }
            },
            getStores: function (e) {
                this.store_id = '';
                $.ajax({
                    url: '{{ route('adminBanners.getStoresAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.city_id
                    },
                    context: this
                })
                    .done(function(data) {
                        this.stores = data;
                    })
                    .fail(function() {
                        console.log("error al caragar las tiendas");
                    })
                    .always(function() {
                        console.log("complete");
                    });
            }
        },
        mounted() {
            this.searchBanners();
        }
    });
</script>