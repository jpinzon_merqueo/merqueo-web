<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Merqueo | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="{{ web_url() }}/admin_asset/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/base.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />
        <script src="{{ web_url() }}/assets/js/jquery.min.js"></script>
        <script src="{{ web_url() }}/assets/js/jquery.validate.min.js"></script>
        <script src="{{ web_url() }}/assets/js/moment.js" type="text/javascript"></script>
        <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="{{ web_url() }}/admin_asset/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <link href="{{ web_url() }}/admin_asset/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="{{ web_url() }}/admin_asset/css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>
        <script src="{{ web_url() }}/admin_asset/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

        <script src="{{ web_url() }}/admin_asset/js/realtime/rsvp.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/realtime/geofire.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/realtime/lodash.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="https://cdn.jsdelivr.net/lodash/4.6.1/lodash.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/angular.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/angular-simple-logger.min.js" type="text/javascript"></script>
        <!--<script src="{{ web_url() }}/admin_asset/js/angular-google-maps.min.js" type="text/javascript"></script>-->
        <script src="{{ web_url() }}/admin_asset/js/angularfire.min.js" type="text/javascript"></script>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        <script src="{{ web_url() }}/admin_asset/js/jquery.nestable.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/general.css">
        <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/validation-picking.css">

        <style type="text/css">
        .error{
            color:red;
            border-color:red;
        }
        </style>
    </head>
    <body class="skin-black">
        <header class="header">
            <a href="{{ route('admin.dashboard') }}" class="logo">
                <img src="{{ web_url() }}/admin_asset/img/logo-horizontal-blanco.svg" width="130"/>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ Session::get('admin_name') }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-light-blue">
                                    <img src="{{ Session::get('admin_image_url') }}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{ Session::get('admin_name') }}<br>
                                        <small>{{ Session::get('admin_username') }}</small>
                                        <small>{{ Session::get('admin_designation') }}</small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ route('admin.password') }}" class="btn btn-default btn-flat">Cambiar clave</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('admin.logout') }}" class="btn btn-default btn-flat">Cerrar sesión</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left info">
                            <p>{{ Session::get('admin_name') }}</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        @foreach ($menu as $menu_item)
                            @if(!empty($menu_item) && $menu_item['id'] === 62)
                                <li @if ( !empty($menu_item['children']))class="treeview"@endif>
                                    <a href="@if ( !empty($menu_item['action']) ){{ action($menu_item['action']) }}@else#@endif">
                                        <i class="fa @if ( !empty($menu_item['iconclass']) ){{ $menu_item['iconclass'] }}@endif"></i> <span>{{ $menu_item['title'] }}</span> @if( !empty($menu_item['children']) )<i class="fa fa-angle-left pull-right"></i>@endif
                                    </a>
                                    @if ( !empty($menu_item['children']) )
                                        <ul class="treeview-menu">
                                            @foreach ($menu_item['children'] as $submenu)
                                                <li>
                                                    <a href="@if ( !empty($submenu['action']) ){{ action($submenu['action']) }}@else#@endif">
                                                        <i class="fa @if($submenu['iconclass']){{ $submenu['iconclass'] }}@endif"></i> <span>{{ $submenu['title'] }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </section>
            </aside>

            <aside class="right-side">
                <div class="callout callout-warning lead">
                    <h3>Alerta</h3>
                    <p style="font-size: 30px;">Esta versión del dashboard pronto quedará obsoleta, por favor <a href="https://n-dashboard.merqueo.com/admin-m3rqu30/">USA LA NUEVA VERSIÓN.</a></p>
                </div>
                @yield('content')
            </aside>
        </div>

        <script src="{{ web_url() }}/admin_asset/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/AdminLTE/app.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/base.js" type="text/javascript"></script>
        <script>
        	var action = '{{ Request::segment(2) }}';
        	var web_url = '{{ web_url() }}';
        	var web_url_warehouses_ajax = "{{ route('admin.get_warehouses_ajax') }}";
        </script>
    </body>
</html>