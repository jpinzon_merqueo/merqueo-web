
    @extends('admin.layout')

    @section('content')
    @include('admin.inventory.counting.css.counting-css')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('AdminInventoryCounting.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver a la lista de conteos</a>
            @if (($inventory_counting->status == 'En validación primer conteo' || $inventory_counting->status == 'En validación segundo conteo') && $admin_permissions['permission2'])
                <a href="{{ route('AdminInventoryCounting.approveCounting', ['id' => $inventory_counting->id]) }}">
                <button type="button" class="btn btn-primary">Aprobar conteo</button>
            </a>
            @endif
            @if (($admin_permissions['access'] && $inventory_counting->status != 'En validación primer conteo' && $inventory_counting->status != 'En validación segundo conteo') || $admin_permissions['permission2'])
            <a href="{{ route('AdminInventoryCounting.count') }}">
                <button type="button" class="btn btn-primary">Ir al conteo</button>
            </a>
            @endif
            <a href="{{ route('AdminInventoryCounting.report', ['id' => $inventory_counting->id]) }}">
                <button id="btn-generate-report-count" type="button" class="btn btn-primary">Generar reporte</button>
            </a>
            @if(($inventory_counting->status != 'Cerrado' && $inventory_counting->status != 'En validación primer conteo' && $inventory_counting->status != 'En validación segundo conteo') || ($admin_permissions['permission2'] && $inventory_counting->status != 'Cerrado'))
                <button id="btn-close-count" type="button" class="btn btn-success">Cerrar conteo</button>
            @endif
            @if ($admin_permissions['delete'] && $inventory_counting->status == 'Pendiente')
            <a href="{{ route('AdminInventoryCounting.delete', ['id' => $inventory_counting->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar este conteo?')">
                <button type="button" class="btn btn-danger">Eliminar</button>
            </a>
            @endif
       </span>
    </section>
    <section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Información</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @foreach($errors->all() as $error)
            <p class=""><strong>{{ $error }}</strong></p>
            @endforeach
        </div>
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="callout callout-info">
                                <?php $status = ($inventory_counting->status == 'Pendiente' && $inventory_counting->first_count_date) ? 'En primer conteo' : $inventory_counting->status; ?>
                                <?php $status = ($inventory_counting->status == 'Primer conteo realizado' && $inventory_counting->second_count_date) ? 'En segundo conteo' : $status; ?>
                                <h4>Datos del Conteo</h4>
                                <p><label>Estado</label> - <span class="badge bg-@if($inventory_counting->status == 'Cerrado')green @elseif($inventory_counting->status == 'En validación primer conteo' || $inventory_counting->status == 'En validación segundo conteo')blue @else()orange @endif">{{ $status }}</span>
                                </p>
                                <p><label>Ciudad</label> - {{ $inventory_counting->warehouse->city->city }}</p>
                                <p><label>Bodega</label> - {{ $inventory_counting->warehouse->warehouse }}</p>
                                <p><label># Productos</label> - {{ $inventory_counting->inventoryCountingDetails->count() }}</p>
                                <p><label>Creado por</label> - {{ $inventory_counting->admin->fullname }}</p>
                                <p><label>Fecha de creación</label> - {{ date("d M Y g:i a", strtotime($inventory_counting->created_at)) }}</p>
                            </div>
                        </div>
                        @if ($inventory_counting->status !== 'Cerrado')
                        <div class="col-xs-6">
                            <div class="callout callout-info">
                                <h4>Acciones sobre el conteo</h4>
                                <form action="{{ route('AdminInventoryCounting.update', ['id' => $inventory_counting->id]) }}" method="POST" id="update-inventory-counting" autocomplete="off">
                                    <div class="form-group">
                                        <label>
                                            <input @if($inventory_counting->show_stock) {{ 'checked' }} @endif type="checkbox" id="show_stock" name="show_stock">
                                            <span style="margin-left: 5px;">Ocultar stock actual</span>
                                        </label>
                                    </div>
                                    {{ Form::token() }}
                                </form>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="row form-group">
                        <form action="{{ route('AdminInventoryCounting.close', ['id' => $inventory_counting->id]) }}" method="POST" id="mark-for-second-count" autocomplete="off">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <legend>Productos en el conteo</legend>
                                        <div class="paging">
                                            <div class="table-responsive">
                                                <?php $total_first_quantity_counted = 0; ?>
                                                <?php $total_second_quantity_counted = 0; ?>
                                                <table id="table" class="admin-table table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            @if ($inventory_counting->status != 'Cerrado')
                                                            <td>
                                                                <label for="close-check-all">Marcar para el cierre</label>
                                                                <input type="checkbox" id="close-check-all" name="close-check-all">
                                                            </td>
                                                            @endif
                                                            <th>Imagen</th>
                                                            <th>Referencia</th>
                                                            <th>Producto</th>
                                                            <th>Unidad de medida</th>
                                                            <th>Unidades iniciales en stock para primer conteo</th>
                                                            <th>Unidades en primer conteo</th>
                                                            <th>Diferencia unidades primer conteo</th>
                                                            <th>Unidades iniciales en stock para segundo conteo</th>
                                                            <th>Unidades en segundo conteo</th>
                                                            <th>Diferencia unidades segundo conteo</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($inventory_counting_details->count())
                                                            @foreach($inventory_counting_details as $detail)
                                                            <tr>
                                                                @if ($inventory_counting->status != 'Cerrado')
                                                                <td>
                                                                    @if (!$detail->stock_updated)
                                                                    <input  class="is-close-count" type="checkbox" name="is_marked_close_count[]" value="{{ $detail->id }}">
                                                                    @endif
                                                                </td>
                                                                @endif
                                                                <td>
                                                                    <a class="load-detail-modal" data-href="{{ route('AdminInventoryCounting.release_detail', ['id' => $detail->id]) }}" href="javascript:;">
                                                                        <img src="{{ $detail->storeProduct->product->image_small_url }}" height ="50px">
                                                                    </a>
                                                                </td>
                                                                <td>{{ $detail->storeProduct->product->reference }}</td>
                                                                <td>{{ $detail->storeProduct->product->name }}</td>
                                                                <td>{{ $detail->storeProduct->product->quantity . ' ' . $detail->storeProduct->product->unit }}</td>
                                                                <td>
                                                                    @if($inventory_counting->status == 'Cerrado')
                                                                    {{ $detail->first_snapshot_stock }}
                                                                    @else
                                                                    {{ $detail->current_stock }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{ $detail->first_total_count }}
                                                                </td>
                                                                <td>{{ is_numeric($detail->first_quantity_counted) ? abs($detail->first_total_count) - abs(($inventory_counting->status == 'Cerrado') ? $detail->first_snapshot_stock : $detail->current_stock) : '' }}</td>
                                                                <td>
                                                                    @if($inventory_counting->status == 'Cerrado')
                                                                    {{ $detail->second_snapshot_stock }}
                                                                    @else
                                                                    {{ $detail->current_stock }}
                                                                    @endif
                                                                </td>
                                                                <td @if ($detail->is_marked_second_count) class="bg-yellow" @endif>
                                                                    {{ $detail->second_total_count }}
                                                                </td>
                                                                <td>
                                                                    {{ is_numeric($detail->second_quantity_counted) ? abs($detail->second_total_count) - abs(($inventory_counting->status == 'Cerrado') ? $detail->second_snapshot_stock : $detail->current_stock) : '' }}
                                                                </td>
                                                                <td>
                                                                    {{ (is_numeric($detail->second_quantity_counted)) ? $detail->second_total_count : ($detail->first_total_count) }}
                                                                </td>
                                                            </tr>
                                                            <?php $total_first_quantity_counted += $detail->first_total_count; ?>
                                                            <?php $total_second_quantity_counted += $detail->second_total_count; ?>
                                                            @endforeach
                                                        @else
                                                            <tr><td colspan="10" align="center">No hay datos.</td></tr>
                                                        @endif
                                                    </tbody>
                                                </table>
                                                <hr>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <legend>Totales</legend>
                                                    <div class="table-responsive">
                                                        <table id="table-report" class="admin-table table table-bordered table-condensed">
                                                            <tr>
                                                                <th>Total referencias contadas</th>
                                                                <td>{{ $inventory_counting_details->count() }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Total primer conteo</th>
                                                                <td>{{ $total_first_quantity_counted }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Total segundo conteo</th>
                                                                <td>{{ $total_second_quantity_counted }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                            </div>
                            {{ Form::token() }}
                            {{ Form::hidden('id', $inventory_counting->id) }}
                            {{ Form::hidden('_method', 'POST') }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-release-inventory-counting-detail" tabindex="-1" role="dialog" aria-labelledby="Detalles de la referencia">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Detalles de la referencia</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-inventory-counting-detail-count" tabindex="-1" role="dialog" aria-labelledby="Detalles las ubicaciones">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Detalles de las ubicaciones</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <a href="#importing" class="fancybox unseen"></a>
        <div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
    </section>

<script>
$(document).ready(function(){
    @if (($inventory_counting->status == 'En validación primer conteo' || $inventory_counting->status == 'En validación segundo conteo') && !$admin_permissions['permission2'])
    swal(
        'Info',
        '¡Este conteo necesita aprobación por parte de los líderes, por lo tanto no podrás editarlo!',
        'info'
    );
    @endif
    var url_base_detail = '{{ route('AdminInventoryCounting.index') }}' + '/detail/',
        url_base = '{{ route('AdminInventoryCounting.update', ['id' => $inventory_counting->id]) }}';

    $('body').on('click', '#close-check-all', function(event) {
        var $this = $(this),
            $marks = $('input[name="is_marked_close_count[]"]');
        if ($this.is(':checked')) {
            $marks.prop('checked', true);
        } else {
            $marks.prop('checked', false);
        }
    });
    $('body').on('click', '#btn-close-count', function(event) {
        event.preventDefault();
        $('.fancybox').trigger('click');
        $('#mark-for-second-count').submit();
    });
    $('body').on('click', '.load-detail-modal', function(event) {
        event.preventDefault();
        $('.fancybox').trigger('click');
        var url = $(this).data('href');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html'
        })
        .done(function(data) {
            $('#modal-release-inventory-counting-detail').find('div.modal-body')
                .empty()
                .html(data);
            $('#modal-release-inventory-counting-detail').modal('show');
        })
        .fail(function() {
            $.fancybox.close();
            swal(
              'Oops...',
              '¡Al parecer tenemos problemas obteniendo la información!',
              'error'
            );
        })
        .always(function() {
            $.fancybox.close();
        });
    });
    $('body').on('change', '#show_stock', function(event) {
        var $check = $(this);
        var data = $('#update-inventory-counting').serialize();
        $('.fancybox').trigger('click');
        $.ajax({
            url: url_base,
            type: 'POST',
            dataType: 'json',
            data: data,
        })
        .done(function(data) {
            if (!data.status) {
                swal(
                  'Oops...',
                  data.message,
                  'error'
                );
            } else {
                swal(
                  'Correcto',
                  'Hemos actualizado el conteo!',
                  'success'
                );
            }
        })
        .fail(function() {
            $.fancybox.close();
            swal(
              'Oops...',
              '¡Al parecer tenemos problemas actualizando el conteo!',
              'error'
            );
        })
        .always(function() {
            $.fancybox.close();
        });
    });
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
    setTimeout(function(){
        $('#check-all, #close-check-all, .is-marked-second-count, .is-close-count, #show_stock').iCheck('destroy');
    }, 1000);

    @if (Session::has('file_url'))
    window.location = '{{ Session::get('file_url') }}';
    @endif
});
</script>
@stop
