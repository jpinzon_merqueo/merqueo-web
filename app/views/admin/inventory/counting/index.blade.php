@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        @if ($admin_permissions['permission1'])
        <span class="breadcrumb" style="top:0px">
            @if ($admin_permissions['access'])
            <a href="{{ route('AdminInventoryCounting.count') }}">
                <button type="button" class="btn btn-primary">Ir al conteo</button>
            </a>
            @endif
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-store-product-counting"><i class="fa fa-plus"></i> Cargar archivo</button>
        </span>
        @endif
    </section>
    <section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Información</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @foreach($errors->all() as $error)
            <p class=""><strong>{{ $error }}</strong></p>
            @endforeach
        </div>
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form action="{{ route('AdminInventoryCounting.index') }}" id="search-form" method="GET" autocomplete="off">
                            <div class="table-responsive">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <label for="city_id">Ciudad:</label>
                                        </td>
                                        <td>
                                            {{ Form::fillSelectWithKeys('city_id', $cities, $city, array('class' => 'form-control', 'id' => 'city_id'), 'id', 'city' ) }}
                                        </td>
                                        <td align="right">
                                            <label for="warehouse">Bodega:</label>
                                        </td>
                                        <td>
                                            {{ Form::fillSelectWithKeys('warehouse', ((count($cities)) ? $cities[$city]->warehouses : ''), '', array('class' => 'form-control', 'id' => 'warehouse'), 'id', 'warehouse' ) }}
                                        </td>
                                        <td align="right">
                                            <label class="control-label" for="status">Estado:</label>
                                        </td>
                                        <td>
                                            {{ Form::fillSelectWithoutKeys('status', $statuses, $status, array('class' => 'form-control', 'id' => 'status'), TRUE ) }}
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <button type="submit" id="search" class="btn btn-primary center-block">Buscar</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                        <div class="paging"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-store-product-counting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Nuevo conteo de productos</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="POST" id='main-form' action="{{ route('AdminInventoryCounting.save') }}" enctype="multipart/form-data" autocomplete="off">
                            <div class="form-group">
                                <label for="city_id_form" class="control-label">Ciudad:</label>
                                {{ Form::fillSelectWithKeys('city_id_form', $cities, $city, array('class' => 'form-control', 'id' => 'city_id_form'), 'id', 'city' ) }}
                            </div>
                            <div class="form-group">
                                <label for="warehouse_id_form" class="control-label">Bodega:</label>
                                {{ Form::fillSelectWithKeys('warehouse_id_form', ((count($cities)) ? $cities[$city]->warehouses : ''), '', array('class' => 'form-control', 'id' => 'warehouse_id_form'), 'id', 'warehouse' ) }}
                            </div>
                            <div class="form-group">
                                <label for="csv_file" class="control-label">Archivo de Productos:</label>
                                <input type="file" class="form-control" name="csv_file" >
                            </div>
                            <div class="form-group">
                                <label for="first_count_date" class="control-label">Fecha de inicio del conteo:</label>
                                <input id="first_count_date" type="text" class="form-control" name="first_count_date" >
                            </div>
                            <div class="form-group">
                                <label for="first_end_count_date" class="control-label">Fecha limite del conteo:</label>
                                <input id="first_end_count_date" type="text" class="form-control" name="first_end_count_date" >
                            </div>
                            <div class="form-group">
                                <label for="threshold" class="control-label">Umbral del conteo:</label>
                                <input id="threshold" type="text" class="form-control" name="threshold" >
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="send-form">Envíar</button>
                    </div>
                </div>
            </div>
        </div>
        <a href="#importing" class="fancybox unseen"></a>
        <div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
    </section>
    <script>
        var web_url_ajax = "{{ route('AdminInventoryCounting.index') }}";
        $(document).ready(function() {
            $('#search').on('click', function(e) {
                e.preventDefault();
                search();
            }).trigger('click');
            $('#send-form').on('click', function(event) {
                event.preventDefault();
                $('#main-form').submit();
            });
            $("#main-form").validate({
              rules: {
                city_id_form: {
                    required: true
                },
                csv_file: {
                    required: true
                },
                first_count_date: {
                    required: true,
                    date: true
                },
                first_end_count_date: {
                    required: true,
                    date: true
                },
                threshold: {
                    required: true,
                    number: true
                }
              }
            });
            $('#first_count_date, #first_end_count_date').datetimepicker({
                format: 'YYYY/MM/DD'
            });
            $('#main-form').submit(function () {
                if($(this).valid()) {
                   $('.fancybox').trigger('click');
                   $('#modal-store-product-counting').modal('hide')
                }
            });
            $('.fancybox').fancybox({
                autoSize    : true,
                closeClick  : false,
                closeBtn    : false ,
                openEffect  : 'none',
                closeEffect : 'none',
                helpers   : {
                   overlay : {closeClick: false}
                }
            });
            $('#city_id').change(function(event) {
                loadWarehouses();
            });
            $('#city_id_form').change(function(event) {
                loadWarehouses('#city_id_form', '#warehouse_id_form');
            });
            function search() {
                var data = $('#search-form').serialize();
                $('.paging').html('');
                $('.paging-loading').show();
                $.ajax({
                    url: web_url_ajax,
                    data: data,
                    type: 'GET',
                    dataType: 'html',
                    success: function(response) {
                        $('.paging-loading').hide();
                        $('.paging').html(response);
                    }, error: function() {
                        $('.paging-loading').hide();
                        $('.paging').html('Ocurrió un error al obtener los datos.');
                    }
                });
            }
            function loadWarehouses(city_select = '#city_id', warehoseSelect = '#warehouse', trigger = false) {
                $.ajax({
                    url: "{{ route('AdminInventoryCounting.get_warehouse_ajax') }}",
                    data: {city_id: $(city_select).val()},
                    type: 'GET',
                    dataType: 'json',
                    success: function(response) {
                        $('.paging-loading').hide();
                        var $warehose = $(warehoseSelect);
                        $warehose.empty();
                        $.each(response, function(index, value) {
                            $warehose.append('<option value="' + index + '">' + value + '</option>');
                        });
                        if (trigger)
                            $('#search').trigger('click');
                    }, error: function() {
                        $('.paging-loading').hide();
                        $('.paging').html('Ocurrió un error al obtener los datos.');
                    }
                });
            }
        });
    </script>
@else
    @section('content')
    <br>
    <div class="table-responsive">
        <table id="allied-stores-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Ciudad</th>
                    <th>Bodega</th>
                    <th>Fecha de creación</th>
                    <th>Estado</th>
                    <th>Cantidad de productos</th>
                    <th>Creado por</th>
                    <th>Detalle</th>
                </tr>
            </thead>
            <tbody>
            @if(count($inventory_counting))
                @foreach($inventory_counting as $count)
                <tr>
                    <td>{{ $count->warehouse->city->city }}</td>
                    <td>{{ $count->warehouse->warehouse }}</td>
                    <td>{{ date("d M Y g:i a", strtotime($count->created_at)) }}</td>
                    <td>
                        <span class="badge bg-@if($count->status == 'Cerrado')green @elseif($count->status == 'En validación primer conteo' || $count->status == 'En validación segundo conteo')blue @else()orange @endif">{{ $count->status }}</span>
                    </td>
                    <td>{{ $count->inventoryCountingDetails->count() }}</td>
                    <td>{{ $count->admin->fullname }}</td>
                    <td align="center">
                        <div class="btn-group">
                           <a class="btn btn-xs btn-default" href="{{ route('AdminInventoryCounting.view', ['id' => $count->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="7" align="center">No éxisten registros de conteo</td></tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dataTables_info" id="shelves-table_info">
                <div class="text-center">Mostrando {{ count($inventory_counting) }} registros</div>
            </div>
        </div>
    </div>
@endif
@stop
