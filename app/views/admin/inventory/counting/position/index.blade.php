@extends('admin.layout')

@section('content')
    <div class="contenido">
        <section class="content-header">
            <div class="col-xs-6">
                <h1>
                    {{ $title }}
                    <small>Control panel</small>
                </h1>
            </div>
            <div class="col-xs-6">
                <span class="breadcrumb" style="float: right;margin-top: 20px;">
                    <a href="{{ route('adminPositionCounting.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Crear Conteo</a>
                </span>
            </div>
        </section>
        <section id="content" class="content">
            <div class="col-xs-12">
                @include('admin.inventory.counting.position.message')
            </div>
            @include('admin.inventory.counting.position.filter')
            <div class="table-responsive">
                <table id="allied-stores-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Posición</th>
                            <th>Posición altura</th>
                            <th>Ciudad</th>
                            <th>Bodega</th>
                            <th>Fecha de creación</th>
                            <th>Estado</th>
                            <th>Cantidad de productos</th>
                            @if(Session::get('admin_designation') === 'Super Admin')
                            <th>Unidades</th>
                            <th>Unidades contadas</th>
                            @endif
                            <th>Creado por</th>
                            <th>Detalle</th>
                            @if($admin_permissions['delete'])
                            <th>Eliminar</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="inventory_counting_position in paginator.data">
                            <td>@{{ inventory_counting_position.id }}</td>
                            <td>@{{ inventory_counting_position.positions }}</td>
                            <th>@{{ inventory_counting_position.height_positions }}</th>
                            <td>@{{ inventory_counting_position.warehouse.city.city }}</td>
                            <td>@{{ inventory_counting_position.warehouse.warehouse }}</td>
                            <td>@{{ inventory_counting_position.created_at }}</td>
                            <td>
                                <span class="badge bg-green" v-if="inventory_counting_position.status === 'Cerrado'">@{{ inventory_counting_position.status }}</span>
                                <span class="badge bg-orange" v-if="inventory_counting_position.status !== 'Cerrado'">@{{ inventory_counting_position.status }}</span>
                            </td>
                            <td>@{{ inventory_counting_position.inventory_counting_position_details.length }}</td>
                            @if(Session::get('admin_designation') === 'Super Admin')
                            <td>@{{ inventory_counting_position.total_quantity }}</td>
                            <td>@{{ inventory_counting_position.total_counted }}</td>
                            @endif
                            <td>@{{ inventory_counting_position.admin.fullname }}</td>
                            <td align="center">
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-default" :href="inventory_counting_position.url_open">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                    </a>
                                </div>
                            </td>
                            @if($admin_permissions['delete'])
                                <td align="center">
                                    <div class="btn-group">
                                        <a :href="inventory_counting_position.url_delete" @click="confirmDelete(inventory_counting_position.id, $event)" class="btn btn-xs btn-danger">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </td>
                            @endif
                        </tr>
                        <tr v-show="paginator.data == 0" class="text-center">
                            <td @if($admin_permissions['delete']) colspan="12" @else colspan="11" @endif>No se encontraron conteos</td>
                        </tr>
                    </tbody>
                </table>
                <div class="row" >
                    <div class="col-xs-12 text-center" >
                        <ul class="pagination">
                            <li :class="paginator.current_page == 1 ? 'disabled' : ''">
                                <a href="javascript:void(0)" @click.prevent="paginator.current_page != 1 ? getCountings(paginator.current_page-1) : ''">&laquo;</a>
                            </li>
                            <li v-for="n in pages" :class="paginator.current_page == n ? 'active' : ''">
                                <a href="javascriot:void(0)" @click.prevent="paginator.current_page != n && n != '...' ? getCountings(n) : ''">
                                    @{{ n }}
                                    <span class="sr-only" v-show="paginator.current_page == n">(current)</span>
                                </a>
                            </li>
                            <li :class="paginator.current_page == paginator.last_page ? 'disabled' : ''">
                                <a href="javascript:void(0)" @click.prevent="paginator.current_page != paginator.last_page ? getCountings(paginator.current_page+1) : ''">&raquo;</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        @include('admin.inventory.counting.position.js.index-js')
    </div>
@endsection