
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <form autocomplete="off" @submit.prevent="getCountings(0)">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <label for="city_id">Ciudad:</label>
                            <select
                                    id="city_id"
                                    v-model="city_id"
                                    class="form-control"
                                    @change="getWarehouses"
                                    :disabled="isLoading"
                                    {{ Session::get('admin_designation') !== 'Super Admin' ? 'disabled="disabled"' : '' }}
                            >
                                <option value="">-Selecciona-</option>
                                <option v-for="city in cities" :value="city.id">@{{ city.city }}</option>
                            </select>
                            <input type="hidden" name="city_id" v-model="city_id">
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <label for="warehouse_id">Bodega:</label>
                            <select
                                    id="warehouse_id"
                                    v-model="warehouse_id"
                                    class="form-control"
                                    :disabled="isLoading"
                                    {{ Session::get('admin_designation') !== 'Super Admin' ? 'disabled="disabled"' : '' }}
                            >
                                <option value="">-Selecciona-</option>
                                <option v-for="warehouse in warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                            </select>
                            <input type="hidden" name="warehouse_id" v-model="warehouse_id">
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <label for="status">Estado:</label>
                            <select name="status" id="status" v-model="status" class="form-control" :disabled="isLoading">
                                <option value="">-Selecciona-</option>
                                <option v-for="status in statuses" :value="status">@{{ status }}</option>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <label for="" style="display: block; color: #FFF;">|</label>
                            <button class="btn btn-primary center-block" type="submit" :disabled="isLoading || (city_id === '' || warehouse_id === '' || status === '')">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>