<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.1.1/dist/vee-validate.js"></script>
<script src="{{ admin_asset_url() }}/js/vee-validate-messages.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment@2.18.1/min/moment-with-locales.min.js"></script>
<script>
    let vue = new Vue({
        el: '#content',
        data: {
            counting: {{ $inventoryCountingPosition->toJson() }},
            isLoading: false,
            requestErrors: [],
            requestSuccess: []
        },
        methods: {
            updateQuantity: function (detail, quantity) {
                this.requestErrors = [];
                this.requestSuccess = [];
                this.$validator.validate(`product-${detail.id}`).then(result => {
                    if (!result) {
                        return false;
                    } else {
                        this.isLoading = true;
                        axios.post("{{ route('adminPositionCounting.update_detail') }}", {
                            counting_id: this.counting.id,
                            id: detail.id,
                            contado: quantity
                        })
                            .then(response => {
                                this.requestSuccess = response.data;
                                detail.quantity_updated = 1;
                                detail.picking_updated = 1;
                            })
                            .catch(error => {
                                console.log(error);
                                this.requestErrors = error.response.data;
                            })
                            .then(() => {
                                this.isLoading = false;
                            });
                    }
                });
            },
            validateCounting: function (e) {
                this.$validator.validate()
                    .then(result => {
                        let fails = this.counting.inventory_counting_position_details.filter(function (detail) {
                            return !(detail.quantity_updated === 1 || detail.picking_updated === 1);
                        });

                        if (fails.length > 0) {
                            result = false;
                        }

                        if (!result) {
                            e.preventDefault();
                            alert('Debes resolver los errores antes de continuar.');
                            return false;
                        } else {
                            let confirmation = confirm('¿Estas seguro de querer cerrar el conteo?');
                            if (!confirmation) {
                                e.preventDefault();
                                return false;
                            }
                        }
                    })
            },
            moment: function (data) {
                return moment(data);
            }
        }
    });
</script>