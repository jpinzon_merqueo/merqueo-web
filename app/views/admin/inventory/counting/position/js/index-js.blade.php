<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script type="text/javascript">
    let vue = new Vue({
        el: '#content',
        data: {
            city_id: {{ Session::get('admin_city_id') }},
            cities: {{ $cities->toJson() }},
            warehouse_id: {{ Session::get('admin_warehouse_id') }},
            warehouses: {{ $warehouses->toJson() }},
            statuses: {{ json_encode($statuses) }},
            status: 'Pendiente',
            isLoading: false,
            paginator: {
                data: [],
                total: 0,
                per_page: 0,
                current_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
                code: '',
                campaign_validation: '',
            },
            pages: []
        },
        methods: {
            getWarehouses: function (e) {
                this.isLoading = true;
                this.warehouse_id = '';
                axios.get('{{ route('adminPositionCounting.getWarehousesByCityId') }}', {
                    params: {
                        city_id: this.city_id
                    }
                })
                    .then(response => {
                        this.warehouses = response.data;
                    })
                    .catch(error => {
                        alert(`Ocurrio el siguiente error ${error.message}`);
                        console.log(error);
                    })
                    .then(() => {
                        this.isLoading = false;
                    });
            },
            getCountings: function (n) {
                this.isLoading = true;
                axios.get('{{ route('adminPositionCounting.getCountingsAjax') }}', {
                    params: {
                        warehouse_id: this.warehouse_id,
                        status: this.status,
                        page: n
                    }
                })
                    .then(response => {
                        this.paginator = response.data;
                        this.calculatePages();
                    })
                    .catch(error => {
                        alert(`Ocurrio el siguiente error ${error.message}`);
                        console.log(error);
                    })
                    .then(() => {
                        this.isLoading = false;
                    });
            },
            confirmDelete: function (id, e) {
                let option = confirm(`¿Seguro que deseas eliminar el conteo #${id}?`);
                if (option) {
                    return true;
                }
                e.preventDefault();
                return false;
            },
            calculatePages: function () {
                let last = this.paginator.last_page;
                let delta = 5;
                let left = this.paginator.current_page - delta;
                let right = this.paginator.current_page + delta + 1;
                let range = [];
                let pages = [];
                let l;

                for (let i = 1; i <= last ; i++) {
                    if (i == 1 || i == last || (i >= left && i < right)) {
                        range.push(i);
                    }
                }

                range.forEach(function (i) {
                    if (l) {
                        if (i - l === 2) {
                            pages.push(l + 1);
                        } else if (i - l !== 1) {
                            pages.push('...');
                        }
                    }
                    pages.push(i);
                    l = i;
                });

                this.pages = pages;
            }
        },
        mounted: function () {
            this.getCountings(0);
        }
    });
</script>