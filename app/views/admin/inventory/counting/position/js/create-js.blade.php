<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.1.1/dist/vee-validate.js"></script>
<script src="{{ admin_asset_url() }}/js/vee-validate-messages.js"></script>
<script>
    const numericDash = {
        getMessage(field, args) {
            // will be added to default locale messages.
            // Returns a message.
            return `${field} debe ser dos números separados por un guión.`;
        },
        validate(value, args) {
            let result = value.match(/^([0-9])+(\-)([0-9])+/);
            return !(result === null);

        }
    };
    const numericDashIncluded = {
        getMessage(field, args) {
            return `${field} los valores deben estar entre ${args[0]} y ${args[1]}.`;
        },
        validate(value, args) {
            let result = value.match(/^([0-9])+(\-)([0-9])+/);
            let numbers;

            if (result !== null) {
                numbers = value.split('-');
                numbers[0] = parseInt(numbers[0]);
                numbers[1] = parseInt(numbers[1]);
                args[0] = parseInt(args[0]);
                args[1] = parseInt(args[1]);

                if (numbers[0] < args[0]
                    || numbers[0] > args[1]
                    || numbers[1] < args[0]
                    || numbers[1] > args[1]
                    || numbers[0] > numbers[1]
                ){
                    return false;
                }

                return !(numbers[0] < args[0]
                    || numbers[0] > args[1]
                    || numbers[1] < args[0]
                    || numbers[1] > args[1]
                    || numbers[0] > numbers[1]);
            }

            return !(result === null);

        }
    };

    let vue = new Vue({
        el: '#content',
        data: {
            cities: {{ $cities->toJson() }},
            cityId: {{ Session::get('admin_city_id') }},
            warehouses: {{ $warehouses->toJson() }},
            warehouseId: {{ Session::get('admin_warehouse_id') }},
            action: '',
            heightPositionIds: [],
            heightPositions: [],
            maxPosition: 0,
            minPosition: 0,
            positions: '',
            storage: '',
            isLoading: false
        },
        methods: {
            getWarehouses: function () {
                this.warehouseId = '';
                this.heightPositionIds = [];
                this.heightPositions = [];
                this.positions = '';
                this.storage = '';
                axios.get('{{ route('adminPositionCounting.getWarehousesByCityId') }}', {
                    params: {
                        city_id: this.cityId
                    }
                })
                    .then(response => {
                        this.warehouses = response.data;
                    })
                    .catch(error => {
                        alert(`Ocurrio el siguiente error ${error.message}`);
                        console.log(error);
                    })
                    .then(() => {
                        this.isLoading = false;
                    });
            },
            getType: function (e) {
                this.storage = '';
                this.heightPositionIds = [];
                this.heightPositions = [];
                this.positions = '';
            },
            getPositions: function (e) {
                let warehouse = this.warehouses.find(warehouse => warehouse.id === this.warehouseId);
                console.log(warehouse);
                if (this.action === 'Almacenamiento') {
                    if (this.storage === 'Seco') {
                        this.heightPositions = warehouse.dry_storage_position.height_positions;
                        this.maxPosition = warehouse.dry_storage_position.end_position;
                        this.minPosition = warehouse.dry_storage_position.start_position;
                    }else if (this.storage === 'Frío') {
                        this.heightPositions = warehouse.cold_storage_position.height_positions;
                        this.maxPosition = warehouse.cold_storage_position.end_position;
                        this.minPosition = warehouse.cold_storage_position.start_position;
                    } else {
                        alert('issue with selector');
                    }
                } else if (this.action === 'Alistamiento') {
                    if (this.storage === 'Seco') {
                        this.heightPositions = warehouse.dry_picking_position.height_positions;
                        this.maxPosition = warehouse.dry_picking_position.end_position;
                        this.minPosition = warehouse.dry_picking_position.start_position;
                    }else if (this.storage === 'Frío') {
                        this.heightPositions = warehouse.cold_picking_position.height_positions;
                        this.maxPosition = warehouse.cold_picking_position.end_position;
                        this.minPosition = warehouse.cold_picking_position.start_position;
                    } else {
                        alert('issue with selector');
                    }
                } else {
                    alert('issue with selector');
                }
            },
            createCounting: function (e) {
                this.isLoading = true;
                this.$validator.validate().then(result => {
                    if (!result) {
                        console.log(result);
                        alert('Primero debe solucionar los errores para poder continuar.');
                        e.preventDefault();
                        this.isLoading = false;
                        return false;
                    } else {
                        return true;
                    }
                });
            }
        }
    });

    vue.$validator.extend('numeric_dash', numericDash);
    vue.$validator.extend('numeric_dash_included', numericDashIncluded);
</script>