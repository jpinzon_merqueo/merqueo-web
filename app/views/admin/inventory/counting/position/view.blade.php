@extends('admin.layout')

@section('content')
    <section class="content-header">
        <div class="col-xs-6">
            <h1>
                {{ $title }}
                <small>Control panel</small>
            </h1>
        </div>
        <div class="col-xs-6">
                <span class="breadcrumb" style="float: right;margin-top: 20px;">
                    <a href="{{ route('adminPositionCounting.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver a la lista de conteos</a>
                </span>
        </div>
        @include('admin.inventory.counting.position.message')
    </section>
    <section class="content" id="content">
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <form id="search-form" class="admin-providers-table">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            {{ Form::label('city', 'Ciudad: ') }}
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            {{ Form::hidden('warehouse_id', $inventoryCountingPosition->warehouse->city->id) }}
                                            {{ Form::text('city', $inventoryCountingPosition->warehouse->city->city, array('class' => 'form-control', 'readonly' => 'true', 'id' => 'city')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            {{ Form::label('warehouse', 'Bodega: ') }}
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            {{ Form::hidden('warehouse_id', $inventoryCountingPosition->warehouse->id) }}
                                            {{ Form::text('warehouse', $inventoryCountingPosition->warehouse->warehouse, array('class' => 'form-control', 'readonly' => 'true', 'id' => 'warehouse')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            {{ Form::label('action', 'Acción: ') }}
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            {{ Form::select('action', array('Almacenamiento' => 'Almacenamiento', 'Alistamiento' => 'Alistamiento'), $inventoryCountingPosition->action, array('class' => 'form-control', 'id' => 'action', 'disabled' => 'true')) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            {{ Form::label('storage', 'Tipo: ') }}
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            {{ Form::select('storage', array('Seco' => 'Seco', 'Frío' => 'Frío'), explode(',', $inventoryCountingPosition->storage_type), array('class' => 'form-control', 'id' => 'storage', 'disabled' => 'true')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            {{ Form::label('height_positions', 'Alturas: ') }}
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12" id="alturas">
                                            {{ Form::select('height_positions[]', $storagePositions, explode(',', $inventoryCountingPosition->height_positions), array('multiple' => 'multiple', 'class' => 'form-control', 'disabled' => 'true')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            {{ Form::label('position', 'Posiciones: ') }}
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-12" id="alturas">
                                            {{ Form::text('positions', $inventoryCountingPosition->positions, array('class' => 'form-control', 'disabled' => 'true')) }}
                                        </div>
                                    </div>
                                </div>                
                            </div>
                        </form>
                    </div>
                    <div class="box-body">
                        <legend style="margin-bottom: 0;">Productos en el conteo</legend>
                        <h5><b>Nota:</b> Ingresa las unidades contadas del producto en cada campo de la tabla.</h5>
                        <div class="row">
                            <div class="col-xs-12" v-show="requestSuccess.length > 0">
                                <div class="col-xs-12">
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Información</b>
                                        <ul>
                                            <li v-for="message in requestSuccess">
                                                @{{ message }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12" v-show="requestErrors.length > 0">
                                <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <b>Alerta!</b>
                                    <ul>
                                        <li v-for="message in requestErrors">
                                            @{{ message }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="paging">
                            <div class="table-responsive">
                                <table id="table" class="admin-table table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            @if(Session::get('admin_designation') === 'Super Admin')
                                            <th>Imagen</th>
                                            @endif
                                            <th>Posición</th>
                                            <th>Referencia</th>
                                            <th>Producto</th>
                                            @if(Session::get('admin_designation') === 'Super Admin')
                                            <th>Unidad de medida</th>
                                            @endif
                                            @if(Session::get('admin_designation') === 'Super Admin')
                                            <th>Fecha de expiración</th>
                                            @endif
                                            @if(in_array(Session::get('admin_role_id'), InventoryCountingPosition::CLOSE_COUNTING_ROLE_IDS) || Session::get('admin_designation') === 'Super Admin')
                                            <th>Unidades iniciales en stock</th>
                                            @endif
                                            <th>Unidades contadas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-show="counting.inventory_counting_position_details.length > 0" v-for="detail in counting.inventory_counting_position_details">
                                            @if(Session::get('admin_designation') === 'Super Admin')
                                            <td>
                                                <img :src="detail.store_product.product.image_small_url" alt="">
                                            </td>
                                            @endif
                                            <td>
                                                @{{ detail.position_height }} @{{ detail.position }}
                                            </td>
                                            <td>
                                                @{{ detail.store_product.product.reference }}
                                            </td>
                                            <td>
                                                @{{ detail.store_product.product.name }}
                                            </td>
                                            @if(Session::get('admin_designation') === 'Super Admin')
                                            <td>
                                                @{{ detail.store_product.product.quantity }} @{{ detail.store_product.product.unit }}
                                            </td>
                                            @endif
                                            @if(Session::get('admin_designation') === 'Super Admin')
                                            <td>
                                                @{{ detail.expiration_date !== null ? moment(detail.expiration_date, 'YYYY-MM-DD').format('DD/MM/YYYY') : '' }}
                                            </td>
                                            @endif
                                            @if(in_array(Session::get('admin_role_id'), InventoryCountingPosition::CLOSE_COUNTING_ROLE_IDS) || Session::get('admin_designation') === 'Super Admin' )
                                            <td v-show="counting.action === 'Alistamiento'">
                                                @{{ detail.picking_original }}
                                            </td>
                                            <td v-show="counting.action === 'Almacenamiento'">
                                                @{{ detail.quantity_original }}
                                            </td>
                                            @endif
                                            <td v-if="counting.status === 'Cerrado'">
                                                <span v-if="counting.action === 'Alistamiento'">
                                                    @{{ detail.picking_counted }}
                                                </span>
                                                <span v-if="counting.action === 'Almacenamiento'">
                                                    @{{ detail.quantity_counted }}
                                                </span>
                                            </td>
                                            <td v-else>
                                                <label class="control-label">Debes oprimir la tecla enter para actualizar las cantidades</label>
                                                <span v-if="counting.action === 'Alistamiento'" :class="`${detail.picking_updated !== 1 ? 'has-error' : ''}`">
                                                    <input
                                                            type="text"
                                                            :name="`product-${detail.id}`"
                                                            v-model="detail.picking_counted"
                                                            v-validate="`required|integer|min_value:0`"
                                                            data-vv-as="cantidad"
                                                            class="form-control"
                                                            @keyup.enter="updateQuantity(detail, detail.picking_counted)"
                                                            :disabled="isLoading"
                                                    >
                                                </span>
                                                <span v-if="counting.action === 'Almacenamiento'" :class="`${detail.quantity_updated !== 1 ? 'has-error' : ''}`">
                                                    <input
                                                            type="text"
                                                            :name="`product-${detail.id}`"
                                                            v-model="detail.quantity_counted"
                                                            v-validate="`required|integer|min_value:0`"
                                                            data-vv-as="cantidad"
                                                            class="form-control"
                                                            @keyup.enter="updateQuantity(detail, detail.quantity_counted)"
                                                            :disabled="isLoading"
                                                    >
                                                </span>
                                                <span style="color: #d73925" v-show="errors.has(`product-${detail.id}`)">@{{ errors.first(`product-${detail.id}`) }}</span>
                                                <span v-show="isLoading">Guardando...</span>
                                            </td>
                                        </tr>
                                        <tr v-show="counting.inventory_counting_position_details < 1">
                                            <td colspan="8" align="center">No hay datos.</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <legend>Totales</legend>
                                    <div class="table-responsive">
                                        <table id="table-report" class="admin-table table table-bordered table-condensed">
                                            <tr>
                                                <th>Total referencias contadas</th>
                                                <td>{{ $inventoryCountingPosition->inventoryCountingPositionDetails->count() }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                @if($inventoryCountingPosition->status != 'Cerrado')
                                    <div class="col-xs-6 text-right">
                                        <a
                                                href="{{ route('adminPositionCounting.close_count', ['adminPositionCounting' => $inventoryCountingPosition->id]) }}"
                                                class="btn btn-primary"
                                                id="btn-cerrar-conteo"
                                                @click="validateCounting"
                                        >
                                            Cerrar Conteo
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.inventory.counting.position.js.view-js')
@endsection