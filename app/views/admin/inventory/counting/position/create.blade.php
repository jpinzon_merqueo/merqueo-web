@extends('admin.layout')

@section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    @include('admin.inventory.counting.position.message')
    <section class="content">
        <div class="row" id="content">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="{{ route('adminPositionCounting.save') }}" method="POST" @submit="createCounting">
                                    <div class="row">
                                        <div class="col-xs-4 form-group">
                                            <label for="city_id">Ciudad</label>
                                            <select
                                                    id="city_id"
                                                    data-vv-as="Ciudad"
                                                    v-validate="'required|numeric'"
                                                    v-model="cityId"
                                                    class="form-control"
                                                    @change="getWarehouses"
                                                    :disabled="isLoading"
                                                    {{ Session::get('admin_designation') !== 'Super Admin' ? 'disabled="disabled"' : '' }}
                                            >
                                                <option value="">-Selecciona-</option>
                                                <option v-for="city in cities" :value="city.id">@{{ city.city }}</option>
                                            </select>
                                            <input type="hidden" name="city_id" v-model="cityId">
                                            <span style="color: #d73925" v-show="errors.has('cityId')">@{{ errors.first('cityId') }}</span>
                                        </div>
                                        <div class="col-xs-4 form-group">
                                            <label for="warehouse_id">Bodega</label>
                                            <select
                                                    id="warehouse_id"
                                                    data-vv-as="Bodega"
                                                    v-validate="'required|numeric'"
                                                    v-model="warehouseId"
                                                    class="form-control"
                                                    :disabled="isLoading"
                                                    {{ Session::get('admin_designation') !== 'Super Admin' ? 'disabled="disabled"' : '' }}
                                            >
                                                <option value="">-Selecciona-</option>
                                                <option v-for="warehouse in warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                            </select>
                                            <input type="hidden" name="warehouse_id" v-model="warehouseId">
                                            <span style="color: #d73925" v-show="errors.has('warehouseId')">@{{ errors.first('warehouseId') }}</span>
                                        </div>
                                        <div class="col-xs-4 form-group">
                                            <label for="action">Acción</label>
                                            <select
                                                    name="action"
                                                    id="action"
                                                    data-vv-as="Acción"
                                                    v-validate="'required|included:Almacenamiento,Alistamiento'"
                                                    v-model="action"
                                                    class="form-control"
                                                    @change="getType"
                                                    :disabled="isLoading">
                                                <option value="">-Selecciona-</option>
                                                <option value="Almacenamiento">Almacenamiento</option>
                                                <option value="Alistamiento">Alistamiento</option>
                                            </select>
                                            <span style="color: #d73925" v-show="errors.has('action')">@{{ errors.first('action') }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 form-group">
                                            <label for="storage">Tipo</label>
                                            <select
                                                    name="storage"
                                                    id="storage"
                                                    data-vv-as="Tipo"
                                                    v-validate="'required|included:Seco,Frío'"
                                                    v-model="storage"
                                                    class="form-control"
                                                    :disabled="isLoading || action === ''"
                                                    @change="getPositions">
                                                <option value="">-Selecciona-</option>
                                                <option value="Seco">Seco</option>
                                                <option value="Frío">Frío</option>
                                            </select>
                                            <span style="color: #d73925" v-show="errors.has('storage')">@{{ errors.first('storage') }}</span>
                                        </div>
                                        <div class="col-xs-4 form-group">
                                            <label for="height_positions">Alturas</label>
                                            <select
                                                    name="height_positions[]"
                                                    id="height_position"
                                                    data-vv-as="Alturas"
                                                    v-validate="`required|included:${heightPositions.join()}`"
                                                    multiple="multiple"
                                                    v-model="heightPositionIds"
                                                    class="form-control"
                                                    :disabled="isLoading || storage === ''">
                                                <option v-for="heightPosition in heightPositions" :value="heightPosition">@{{ heightPosition }}</option>
                                            </select>
                                            <span style="color: #d73925" v-show="errors.has('height_positions[]')">@{{ errors.first('height_positions[]') }}</span>
                                        </div>
                                        <div class="col-xs-4 form-group">
                                            <label for="positions">Posiciones</label>
                                            <input
                                                    type="text"
                                                    name="positions"
                                                    id="positions"
                                                    data-vv-as="Posiciones"
                                                    v-validate="`required|numeric_dash|numeric_dash_included:${minPosition},${maxPosition}`"
                                                    v-model="positions"
                                                    class="form-control"
                                                    :disabled="isLoading || storage === ''">
                                            <span style="color: #d73925" v-show="errors.has('positions')">@{{ errors.first('positions') }}</span>
                                            <p class="small">Secuencia separada por guión para intervalos, por ejemplo: @{{ minPosition }}-@{{ maxPosition }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 form-group">
                                        <button type="submit" class="btn btn-primary" :disabled="isLoading || positions === '' || heightPositionIds === ''">Crear</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.inventory.counting.position.js.create-js')
@endsection