@if(Session::has('success'))
    <div class="col-xs-12">
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Información</b> {{ Session::get('success') }}
        </div>
    </div>
@endif

@if(Session::has('error'))
    <div class="col-xs-12">
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
    </div>
@endif

@if ($errors->any())
    <div class="col-xs-12">
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @foreach($errors->all() as $error)
            <p class=""><strong>{{ $error }}</strong></p>
            @endforeach
        </div>
    </div>
@endif