
@extends('admin.layout')

@section('content')
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script src="{{ asset_url() }}js/additional-methods.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('AdminInventoryCounting.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver a la lista de conteos</a>
        </span>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Conteos del día</h3>
                    </div>
                    <div class="box-body orders-missing-products">
                        <form role="form" id="counting-id-form">
                            <div class="row">
                                <div class="col-lg-3 col-xs-8">
                                    <div id="countingSelect">
                                        {{ Form::fillSelectWithoutKeys('id', $counting, '', array('class' => 'form-control required', 'id' => 'id'), TRUE ) }}
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-3">
                                    <button type="submit" class="btn btn-success" id="load-count">Mostrar</button>
                                    <!--button type="submit" class="btn btn-success" id="load-select">Cargar conteos</button-->
                                    <img src="{{ asset_url() }}/img/loading.gif" id="loader-count" class="unseen">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="box-item-count" style="display: none;">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div id="lists-items" class="order-item">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-body"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="detail-item" class="row" style="display: none;"></div>
    </section>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            localStorage.removeItem('detail_ids');
            var url_base = '{{ route('AdminInventoryCounting.count') }}',
                url_base_detail = '{{ route('AdminInventoryCounting.index') }}' + '/detail/';
            loadCountSelect();
            bindClickCount();
            bindUpdateDetail();
            $('#load-count').on('click', function(event) {
                event.preventDefault();
                localStorage.removeItem('detail_ids');
                var id = $('#id :selected').val();
                if (id != '' || id != 0) {
                    loadCount(id);
                } else {
                    swal(
                      'Oops...',
                      '¡Debes seleccionar un conteo!',
                      'error'
                    );
                }
            });

            /*$('#load-select').on('click', function(event) {
                event.preventDefault();
                $('#box-item-count').hide();
                $('#lists-items').empty();
                loadCountSelect();
            });*/

            function loadCountSelect() {
                $('#loader-count').removeClass('unseen');
                $('#load-count').attr('disabled', 'disabled');
                $('#box-item-count').hide();
                $('#lists-items').empty();
                $.ajax({
                    url: url_base,
                    type: 'GET',
                    dataType: 'html',
                })
                .done(function(data) {
                    $('#countingSelect').empty();
                    $('#countingSelect').html(data);
                })
                .fail(function() {
                    swal(
                      'Lo sentimos...',
                      'Al parecer tenemos problemas cargando los conteos del día.',
                      'error'
                    );
                })
                .always(function() {
                    $('#loader-count').addClass('unseen');
                    $('#load-count').removeAttr('disabled');
                });
            }

            function loadCount(id) {
                $('#loader-count').removeClass('unseen');
                $('#load-count').attr('disabled', 'disabled');
                $('#box-item-count').hide();
                $('#lists-items').empty();
                $('#detail-item').hide();
                $.ajax({
                    url: url_base + '/' + id,
                    type: 'GET',
                    dataType: 'html',
                })
                .done(function(data) {
                    $('#lists-items').empty()
                        .html(data);
                    $('#box-item-count').slideDown();
                })
                .fail(function() {
                    swal(
                      'Oops...',
                      '¡Al parecer tenemos problemas cargando los datos de este conteo, por favor recarga los conteos o vuelve a intentarlo más luego!',
                      'error'
                    );
                })
                .always(function() {
                    $('#loader-count').addClass('unseen');
                    $('#load-count').removeAttr('disabled');
                });
            }

            function bindClickCount() {
                $('#lists-items').on('click', '.count-item a', function(event) {
                    event.preventDefault();
                    let id = $(this).data('id'),
                        step = $(this).data('count'),
                        data = localStorage.getItem('detail_ids');
                    $('#loader-count').removeClass('unseen');
                    $('#load-count').attr('disabled', 'disabled');
                    $('#box-item-count').hide();
                    $.ajax({
                        url: url_base + '/' + id + '/detail/' + step,
                        data: {details: data},
                        type: 'GET',
                        dataType: 'json',
                    })
                    .done(function(data) {
                        if (data.status == 0) {
                            swal(
                              'Oops...',
                              data.message,
                              'error'
                            );
                        } else if (data.status == 2) {
                            swal(
                              '¡Bien hecho!',
                              data.message,
                              'success'
                            );
                            loadCountSelect();
                        } else {
                            loadDetail(data.product, step);
                        }
                    })
                    .fail(function() {
                        swal(
                          'Oops...',
                          '¡Al parecer tenemos problemas cargando las unidades!',
                          'error'
                        );
                    })
                    .always(function() {
                        //console.log("complete");
                    });
                });
                $('#detail-item').on('click', '.close-store-product-counting-detail-detail', function(event) {
                    event.preventDefault();
                    $('#detail-item').slideUp('fast', function() {
                        $('#box-item-count').slideDown('fast');
                    });
                });
            }

            function loadDetail(id, step) {
                $.ajax({
                    url: url_base_detail + id,
                    type: 'GET',
                    data: {'step': step},
                    dataType: 'html',
                })
                .done(function(data) {
                    $('#box-item-count').slideUp();
                    $('#detail-item').empty()
                        .html(data)
                        .slideDown();
                })
                .fail(function() {
                    swal(
                      'Oops...',
                      '¡Al parecer tenemos problemas cargando las unidades!',
                      'error'
                    );
                })
                .always(function() {
                    $('#loader-count').addClass('unseen');
                    $('#load-count').removeAttr('disabled');
                });
            }

            function bindUpdateDetail() {
                let inventory_count_detail_ids = [];
                $('#load-count').on('click', function() {
                    inventory_count_detail_ids = [];
                });
                $('#detail-item').on('click', 'button.store-product-counting-detail', function(event) {
                    event.preventDefault();
                    let $this = $(this),
                        id    = $('#store-product-counting-detail-id').val(),
                        $form = $('#store-product-counting-detail-update-form'),
                        data  = $form.serialize();
                    if ($form.valid()) {
                        inventory_count_detail_ids.push(id);
                        localStorage.setItem('detail_ids', JSON.stringify(inventory_count_detail_ids));
                        $this.attr('disabled', 'disabled');
                        $.ajax({
                            url: url_base_detail + id + '/update',
                            type: 'POST',
                            dataType: 'json',
                            data: data,
                        })
                        .done(function(data) {
                            $this.removeAttr('disabled');
                            if (data.status) {
                                swal(
                                  '¡Bien hecho!',
                                  'Se actualizó el conteo.',
                                  'success'
                                );
                                $('#detail-item .close-store-product-counting-detail-detail').trigger('click');
                                $('#lists-items .count-item a').trigger('click');
                            } else {
                                swal(
                                  'Oops...',
                                  data.message,
                                  'error'
                                );
                            }
                        })
                        .fail(function() {
                            $this.removeAttr('disabled');
                            swal(
                              'Oops...',
                              '¡Al parecer tenemos problemas actualizando el conteo!',
                              'error'
                            );
                        })
                        .always(function() {
                            //console.log("complete");
                        });
                    }
                });
            }

            $('body').on('click', '#save-inventory-counting-survey', function(event) {
                event.preventDefault();
                var $form = $('#inventory-counting-survey-form'),
                    action = $form.attr('action'),
                    data  = $form.serialize();
                if ($form.valid()) {
                    $('.fancybox').trigger('click');
                    $.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                    })
                    .done(function(data) {
                        if (data.status) {
                            swal(
                              '¡Bien hecho!',
                              'Se guardaron correctamente tus respuestas.',
                              'success'
                            );
                            $('#detail-item .close-store-product-counting-detail-detail').trigger('click');
                            $('#lists-items .count-item a').trigger('click');
                        }
                    })
                    .fail(function() {
                        $.fancybox.close();
                        swal(
                          'Oops...',
                          '¡Al parecer tenemos problemas tratango de guardar la encuesta del conteo!',
                          'error'
                        );
                    })
                    .always(function() {
                        $.fancybox.close();
                    });
                }
            });
        });
    </script>
@stop
