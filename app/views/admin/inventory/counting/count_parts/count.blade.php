@if ($count_details)
    <div class="count-item">
        <a href="javascript:;" id="count-item-{{ $count_details->id }}" data-id="{{ $count_details->id }}" data-count="@if($count_details->status == 'Pendiente' || $count_details->status == 'En validación primer conteo'){{'first'}}@else{{'second'}}@endif">
            <div class="row">
                <div class="col-xs-9">
                    <label>Conteo #</label> {{ $count_details->id }}<br>
                    <label>Ciudad:</label> {{ $count_details->warehouse->city->city }} <br>
                    <label>Bodega:</label> {{ $count_details->warehouse->warehouse }} <br>
                    <label>Creado por:</label> {{ $count_details->admin->fullname }} <br>
                    <label>Estado:</label> <span class="badge bg-@if($count_details->status == 'En validación primer conteo' || $count_details->status == 'En validación segundo conteo')blue @else()orange @endif">{{ $count_details->status }}</span><br>
                </div>
            </div>
        </a>
    </div>
@else
    <div class="count-item">
        <div class="row">
            <a href="javascript:;">
                <div class="col-xs-12 text-center">
                    <label>No se encontró el conteo seleccionado y/o está completado</label>
                </div>
            </a>
        </div>
    </div>
@endif
