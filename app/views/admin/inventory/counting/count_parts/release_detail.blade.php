
<form role="form" method="POST" id='main-form-relase-first' action="{{ route('AdminInventoryCounting.release_detail', ['id' => $inventory_counting_detail->id, 'step' => 'first']) }}">
    <div class="form-group">
        <label class="control-label">Nombre quien realizó el primer conteo:</label>
        <p class="form-control-static">{{ (is_numeric($inventory_counting_detail->first_count_admin_id)) ? $inventory_counting_detail->firstCountAdmin->fullname : 'Aún no se ha asignado' }}</p>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success center-block">Liberar para primer conteo</button>
    </div>
</form>
<form role="form" method="POST" id='main-form-relase-second' action="{{ route('AdminInventoryCounting.release_detail', ['id' => $inventory_counting_detail->id, 'step' => 'second']) }}">
   <div class="form-group">
        <label class="control-label">Nombre quien realizó el segundo conteo:</label>
        <p class="form-control-static">{{ (is_numeric($inventory_counting_detail->second_count_admin_id)) ? $inventory_counting_detail->secondCountAdmin->fullname : 'Aún no se ha asignado' }}</p>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success center-block">Liberar para segundo conteo</button>
    </div>
</form>
