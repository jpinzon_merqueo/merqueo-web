<div class="col-xs-12">
    <div class="box box-primary">
        <div class="box-body">
            <div class="header">
                <div class="row">
                    <div class="col-xs-12">
                        <h4>Detalles del conteo <button type="button" class="btn btn-primary pull-right close-store-product-counting-detail-detail">Cerrar</button></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php $status = ($inventory_counting_detail->inventoryCounting->status == 'Pendiente' && $inventory_counting_detail->inventoryCounting->first_count_date) ? 'En primer conteo' : $inventory_counting_detail->inventoryCounting->status; ?>
                        <?php $status = ($inventory_counting_detail->inventoryCounting->status == 'Primer conteo realizado' && $inventory_counting_detail->inventoryCounting->second_count_date) ? 'En segundo conteo' : $status; ?>
                        <strong>Conteo #</strong> {{ $inventory_counting_detail->inventoryCounting->id }}<br>
                        <strong>Ciudad:</strong> {{ $inventory_counting_detail->inventoryCounting->warehouse->city->city }}<br>
                        <strong>Bodega:</strong> {{ $inventory_counting_detail->inventoryCounting->warehouse->warehouse }}<br>
                        <strong>Estado :</strong> <span class="badge bg-orange">{{ $status }}</span><br>
                        <strong>Crado por :</strong> {{ $inventory_counting_detail->inventoryCounting->admin->fullname }}<br>
                    </div>
                </div>
            </div>
            <hr>
            <form action="{{ route('AdminInventoryCounting.save_survey', ['id' => $inventory_counting_detail->inventory_counting_id]) }}" id="inventory-counting-survey-form" method="POST" autocomplete="off">
                <div class="order-detail-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <h4 class="list-group-item-heading"><strong>Validaciones antes del conteo</strong></h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Verificar ordenes de compra en estado recibido (Sistema y físico)</label>
                                                    {{ Form::fillSelectWithoutKeys('answer_1', [0 => 'No', 1 => 'Si'], 0, array('class' => 'form-control', 'id' => 'answer-1') ) }}
                                                    {{ Form::hidden('question_1', 'Verificar ordenes de compra en estado recibido (Sistema y físico)') }}
                                                    <div class="purchase-order hidden" style="margin: 5px 0;">
                                                        {{ Form::number('purchase_order_answer_1', '', array('class' => 'form-control', 'id' => 'purchase_order_answer_1')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Verificar ordenes de compra en estado almacenado (Sistema y físico)</label>
                                                    {{ Form::fillSelectWithoutKeys('answer_2', [0 => 'No', 1 => 'Si'], 0, array('class' => 'form-control', 'id' => 'answer-2') ) }}
                                                    {{ Form::hidden('question_2', 'Verificar ordenes de compra en estado almacenado (Sistema y físico)') }}
                                                    <div class="purchase-order hidden" style="margin: 5px 0;">
                                                        {{ Form::number('purchase_order_answer_2', '', array('class' => 'form-control', 'id' => 'purchase_order_answer_2')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Merma actualizada</label>
                                                    {{ Form::fillSelectWithoutKeys('answer_3', [0 => 'No', 1 => 'Si'], 0, array('class' => 'form-control', 'id' => 'answer-3') ) }}
                                                    {{ Form::hidden('question_3', 'Merma actualizada') }}
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Verificar devoluciones en estado recibido (Sistema y físico)</label>
                                                    {{ Form::fillSelectWithoutKeys('answer_4', [0 => 'No', 1 => 'Si'], 0, array('class' => 'form-control', 'id' => 'answer-4') ) }}
                                                    {{ Form::hidden('question_4', 'Verificar devoluciones en estado recibido (Sistema y físico)') }}
                                                    <div class="purchase-order hidden" style="margin: 5px 0;">
                                                        {{ Form::number('purchase_order_answer_4', '', array('class' => 'form-control', 'id' => 'purchase_order_answer_4')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Verificar devoluciones en estado almacenado (Sistema y físico)</label>
                                                    {{ Form::fillSelectWithoutKeys('answer_5', [0 => 'No', 1 => 'Si'], 0, array('class' => 'form-control', 'id' => 'answer-5') ) }}
                                                    {{ Form::hidden('question_5', 'Verificar devoluciones en estado almacenado (Sistema y físico)') }}
                                                    <div class="purchase-order hidden" style="margin: 5px 0;">
                                                        {{ Form::number('purchase_order_answer_5', '', array('class' => 'form-control', 'id' => 'purchase_order_answer_5')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Instrucción de <a target="_blank" href="{{ route('adminConfig.generalConfigurations') }}">congelar el inventario mientras se realiza conteo</a> ( Devoluciones y recibos de bodega en esta devuelto y recibo)</label>
                                                    {{ Form::fillSelectWithoutKeys('answer_6', [0 => 'No', 1 => 'Si'], 0, array('class' => 'form-control', 'id' => 'answer-6') ) }}
                                                    {{ Form::hidden('question_6', 'Instrucción de congelar el inventario mientras se realiza conteo ( Devoluciones y recibos de bodega en esta devuelto y recibo)') }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button id="save-inventory-counting-survey" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
                <input id="survey-inventory-counting-id" type="hidden" name="id" value="{{ $inventory_counting_detail->inventory_counting_id }}">
                {{ Form::token() }}
                {{ Form::hidden('_method', 'POST') }}
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#answer-1, #answer-2, #answer-4, #answer-5').on('change', function(event) {
        var $this = $(this);
            value = $this.val(),
            $div = $this.parent().find('div.purchase-order');
        if (value == 1) {
            $div.removeClass('hidden');
        } else {
            $div.addClass('hidden');
        }
    });
    $("#inventory-counting-survey-form").validate({
      rules: {
        purchase_order_answer_1: {
            required: function(element) {
                return $("#answer-1").val() == 1;
            },
            number: true
        },
        purchase_order_answer_2: {
            required: function(element) {
                return $("#answer-2").val() == 1;
            },
            number: true
        },
        purchase_order_answer_4: {
            required:function(element) {
                return $("#answer-4").val() == 1;
            },
            number: true
        },
        purchase_order_answer_5: {
            required: function(element) {
                return $("#answer-5").val() == 1;
            },
            number: true
        }
      }
    });
</script>
