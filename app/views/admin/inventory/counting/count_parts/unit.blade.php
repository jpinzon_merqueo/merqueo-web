@if (!$has_survey)
<div class="col-xs-12">
    <div class="box box-primary">
        <div class="box-body">
            <div class="header">
                <div class="row">
                    <div class="col-xs-12">
                        <h4>Detalles del conteo <button type="button" class="btn btn-primary pull-right close-store-product-counting-detail-detail">Cerrar</button></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php $status = ($inventory_counting_detail->inventoryCounting->status == 'Pendiente' && $inventory_counting_detail->inventoryCounting->first_count_date) ? 'En primer conteo' : $inventory_counting_detail->inventoryCounting->status; ?>
                        <?php $status = ($inventory_counting_detail->inventoryCounting->status == 'Primer conteo realizado' && $inventory_counting_detail->inventoryCounting->second_count_date) ? 'En segundo conteo' : $status; ?>
                        <strong>Conteo #</strong> {{ $inventory_counting_detail->inventoryCounting->id }}<br>
                        <strong>Ciudad:</strong> {{ $inventory_counting_detail->inventoryCounting->warehouse->city->city }}<br>
                        <strong>Bodega:</strong> {{ $inventory_counting_detail->inventoryCounting->warehouse->warehouse }}<br>
                        <label>Estado :</label> <span class="badge bg-@if($status == 'En validación primer conteo' || $status == 'En validación segundo conteo')blue @else()orange @endif">{{ $status }}</span><br>
                        <strong>Crado por :</strong> {{ $inventory_counting_detail->inventoryCounting->admin->fullname }}<br>
                    </div>
                </div>
            </div>
            <hr>
            <form id="store-product-counting-detail-update-form" method="POST" autocomplete="off">
                <div class="order-detail-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <h4 class="list-group-item-heading">{{ $inventory_counting_detail->storeProduct->product->name }} {{ $inventory_counting_detail->storeProduct->product->quantity }} {{ $inventory_counting_detail->storeProduct->product->unit }} - Referencia: {{ $inventory_counting_detail->storeProduct->product->reference }}</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 product-image">
                                            <img class="img-responsive center-block" src="{{ $inventory_counting_detail->storeProduct->product->image_medium_url }}">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-12"><hr></div>
                                                <div class="col-xs-12">
                                                    {{ Form::label('picking_counted', 'Conteo en Picking - Posición: ' . (($store_product_warehouse) ? $store_product_warehouse->storage_position . '-' . $store_product_warehouse->storage_height_position : 'No encontrado en picking')) }}
                                                    @if($inventory_counting_detail->inventoryCounting->status == 'En validación primer conteo')
                                                    {{ Form::number('picking_counted', $inventory_counting_detail->first_picking_counted, ['class' => 'form-control input-lg', 'id' => 'picking_counted']) }}
                                                    @elseif($inventory_counting_detail->inventoryCounting->status == 'En validación segundo conteo')
                                                    {{ Form::number('picking_counted', $inventory_counting_detail->second_picking_counted, ['class' => 'form-control input-lg', 'id' => 'picking_counted']) }}
                                                    @else
                                                    {{ Form::number('picking_counted', '', ['class' => 'form-control input-lg', 'id' => 'picking_counted']) }}
                                                    @endif
                                                    @if ($show_stock)
                                                    <span class="help-block">Stock actual: {{ ($store_product_warehouse) ? $store_product_warehouse->picking_stock : '' }}</span>
                                                    @endif
                                                </div>
                                                @foreach($inventory_counting_detail_counts as $inventory_counting_detail_count)
                                                <div class="col-xs-12"><hr></div>
                                                <div class="col-xs-12">
                                                    <fieldset>
                                                        <legend>Posición: {{ $inventory_counting_detail_count->position }}-{{ $inventory_counting_detail_count->position_height }}</legend>
                                                        <div class="col-xs-12">
                                                            {{ Form::label('count_' . $inventory_counting_detail_count->id, 'Cantidad') }}
                                                            @if($inventory_counting_detail->inventoryCounting->status == 'En validación primer conteo')
                                                            {{ Form::number('count[' . $inventory_counting_detail_count->id . ']', $inventory_counting_detail_count->first_quantity, ['class' => 'form-control input-lg', 'id' => 'count_' . $inventory_counting_detail_count->id]) }}
                                                            @elseif($inventory_counting_detail->inventoryCounting->status == 'En validación segundo conteo')
                                                            {{ Form::number('count[' . $inventory_counting_detail_count->id . ']', $inventory_counting_detail_count->second_quantity, ['class' => 'form-control input-lg', 'id' => 'count_' . $inventory_counting_detail_count->id]) }}
                                                            @else
                                                            {{ Form::number('count[' . $inventory_counting_detail_count->id . ']', '', ['class' => 'form-control input-lg', 'id' => 'count_' . $inventory_counting_detail_count->id]) }}
                                                            @endif
                                                            @if ($show_stock)
                                                            <span class="help-block">Stock actual: {{ $inventory_counting_detail_count->quantity }}</span>
                                                            @endif
                                                            <span class="help-block">Fecha de vencimiento: {{ $inventory_counting_detail_count->expiration_date }}</span>
                                                        </div>
                                                        <br>
                                                        <div class="col-xs-12">
                                                            <label>
                                                                <input type="checkbox" value="{{ $inventory_counting_detail_count->id }}" name="is_bad_ubication[]" id="is_bad_ubication_{{ $inventory_counting_detail_count->id }}">
                                                                Ubicación incorrecta.
                                                            </label>
                                                        </div>
                                                        {{ Form::hidden('warehouse_storage_store_product[]', $inventory_counting_detail_count->id) }}
                                                        {{ Form::hidden('product_position[]', $inventory_counting_detail_count->position) }}
                                                        {{ Form::hidden('product_height[]', $inventory_counting_detail_count->position_height) }}
                                                    </fieldset>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @if('first' == $step)
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                              Novedades del producto
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <div class="col-xs-12">
                                                                <label>
                                                                    <input type="checkbox" name="is_bad_image">
                                                                    Imagen errónea.
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label>
                                                                    <input type="checkbox" name="is_bad_reference">
                                                                    Referencia incorrecta.
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-primary store-product-counting-detail">Guardar conteo</button>
                        </div>
                    </div>
                </div>
                <input id="store-product-counting-detail-id" type="hidden" name="id" value="{{ $inventory_counting_detail->id }}">
                <input type="hidden" name="step" value="{{ $step }}">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#store-product-counting-detail-update-form").validate({
      rules: {
        'picking_counted': {
            required: true,
            number: true,
            min: 0
        },
        @foreach($inventory_counting_detail_counts as $inventory_counting_detail_count)
        '{{ "count[" . $inventory_counting_detail_count->id }}]': {
            required: function(element) {
                return !$("#{{ 'is_bad_ubication_' . $inventory_counting_detail_count->id}}").is(':checked');
            },
            number: true,
            min: 0
        },
        @endforeach
      }
    });
</script>
@else
    @include('admin.inventory.counting.count_parts.survey', array('inventory_counting_detail' => $inventory_counting_detail))
@endif
