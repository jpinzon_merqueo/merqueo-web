@extends('admin.layout')

@section('content')

<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
</script>
<a href="#importing" class="fancybox unseen"></a>
<div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">

	@if(Session::has('message'))
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="post" id='form-storage' action="{{ route('adminInventoryStorage.importRemove') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label>Ciudad</label>
                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id || (Input::old('type') == $city->id)) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Bodega</label>
                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                    @foreach ($warehouses as $warehouse)
                                        <option value="{{ $warehouse->id }}" @if(Input::old('warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                    @endforeach
                                </select>
                            </div>
                         </div>
                         <div class="row">
                            <div class="form-group col-xs-6">
                                <label>¿Que deseas hacer?</label>
                                <select id="action" name="action" class="form-control">
                                    <option value="import" @if(Input::old('action') == 'import')) selected="selected" @endif>Importar almacenamiento masivo</option>
                                    <option value="remove" @if(Input::old('action') == 'remove')) selected="selected" @endif>Eliminar todo el almacenamiento de posición especifica</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Tipo almacenamiento</label>
                                <select name="storage_type" id="storage_type" class="form-control">
                                    <option value="Seco" @if(Input::old('type') == 'Seco')) selected="selected" @endif>Seco</option>
                                    <option value="Frío" @if(Input::old('type') == 'Frío')) selected="selected" @endif>Frío</option>
                                </select>
                            </div>
                        </div>
                        <div class="row position-to-block unseen">
                            <div class="form-group col-xs-4">
                                <label>Posición en bodega a eliminar</label>
                                <input type="text" name="position_from" id="position_from" class="form-control" placeholder="Posición" maxlength="3" value="{{Input::old('position_from')}}">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Posición en altura a eliminar</label>
                                <input type="text" name="position_height_from" id="position_height_from" class="form-control" placeholder="Posición en altura" maxlength="1" value="{{Input::old('position_height_from')}}">
                            </div>
                            <div class="form-group col-xs-4">
                                <br>
                                <button type="button" id="btn-remove-storage" class="btn btn-primary">Eliminar</button>
                            </div>
                        </div>
                        <div class="row import-block">
                            <div class="form-group col-xs-8">
                                <label>Archivo Excel</label>
                                <input type="file" class="form-control" id="file_storage" name="file_storage" placeholder="File">
                            </div>
                            <div class="form-group col-xs-4">
                                <br><button type="button" id="btn-import-storage" class="btn btn-primary" onclick="">Importar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">

$(document).ready(function(){

    $('#action').change(function(){
        $('#store_product_id').val('');
        $('#store_product_name').val('');
        if ($(this).val() == 'remove'){
            $('.position-to-block').show();
            $('.import-block').hide();
            $('#file_storage').val('');
        }else{
            $('.position-to-block').hide();
            $('.import-block').show();
            $('#position_from').val('');
            $('#position_height_from').val('');
        }
    });
    $('#action').trigger('change');

    $('#btn-import-storage').on('click', function(e){
        var validator = $('#form-storage').validate({
                            rules: {
                                city_id: 'required',
                                warehouse_id: 'required',
                                action: 'required',
                                storage_type: 'required',
                                file_storage: 'required'
                            }
                        });
        if(validator.form()){
            if (confirm('¿Estas seguro que deseas importar el almacenamiento?')){
                $('.fancybox').trigger('click');
                $('#form-storage').submit();
            }
        }
    });

    $('#btn-remove-storage').on('click', function(e){
        var validator = $('#form-storage').validate({
                            rules: {
                                city_id: 'required',
                                warehouse_id: 'required',
                                action: 'required',
                                storage_type: 'required',
                                position_from: {
                                    required: true,
                                    number: true,
                                },
                                position_height_from: {
                                    required: true,
                                },
                            }
                        });
        if(validator.form()){
            if (confirm('¿Estas seguro que deseas eliminar el almacenamiento?'))
                $('#form-storage').submit();
        }
    });
});

jQuery(document).ready(function($) {

});
</script>

@stop