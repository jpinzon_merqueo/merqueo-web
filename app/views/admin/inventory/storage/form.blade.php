@if (!Request::ajax())

@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
    @if(Session::has('message'))
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif
    <div class="row">
        <div class="col-xs-12">
            <form id="form-storage" action="{{ route('adminInventoryStorage.save') }}" method="POST">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="form-group col-xs-2">
                                <label>Ciudad</label>
                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id || (Input::old('type') == $city->id)) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-2">
                                <label>Bodega</label>
                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                    @foreach ($warehouses as $warehouse)
                                        <option value="{{ $warehouse->id }}" @if(Input::old('warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-3">
                                <label>Objeto a almacenar</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="Producto" @if(Input::old('type') == 'Producto')) selected="selected" @endif>Producto</option>
                                    <option value="Canastilla merqueo" @if(Input::old('type') == 'Canastilla merqueo')) selected="selected" @endif>Canastilla merqueo</option>
                                    <option value="Canastilla proveedor" @if(Input::old('type') == 'Canastilla proveedor')) selected="selected" @endif>Canastilla proveedor</option>
                                    <option value="Estiva" @if(Input::old('type') == 'Estiva')) selected="selected" @endif>Estiva</option>
                                    <option value="Sampling" @if(Input::old('type') == 'Sampling')) selected="selected" @endif>Sampling</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-3">
                                <label>¿Que deseas hacer?</label>
                                <select name="movement" id="movement" class="form-control">
                                    <option value="storage" @if(Input::old('movement') == 'storage')) selected="selected" @endif>Almacenar</option>
                                    <option value="move" @if(Input::old('movement') == 'move')) selected="selected" @endif>Mover de una posición a otra</option>
                                    @if ($admin_permissions['permission2'])
                                    <option value="remove" @if(Input::old('movement') == 'remove')) selected="selected" @endif>Eliminar de almacenamiento</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-xs-2 product-storage-type">
                                <label>Tipo almacenamiento</label>
                                <select name="storage_type" id="storage_type" class="form-control">
                                    <option value="Seco" @if(Input::old('type') == 'Seco')) selected="selected" @endif>Seco</option>
                                    <option value="Frío" @if(Input::old('type') == 'Frío')) selected="selected" @endif>Frío</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Origen</h4>
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Posición en bodega</label>
                                <input type="text" name="position_from" id="position_from" class="form-control" placeholder="Posición" maxlength="3" value="{{Input::old('position_from')}}">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Posición en altura</label>
                                <input type="text" name="position_height_from" id="position_height_from" class="form-control" placeholder="Posición en altura" maxlength="1" value="{{Input::old('position_height_from')}}">
                            </div>
                            <div class="form-group col-xs-4 quantity-from">
                                <label>Cantidad de unidades</label>
                                <input type="text" name="quantity_from" id="quantity_from" class="form-control" placeholder="Cantidad de unidades" value="{{Input::old('quantity_from')}}">
                            </div>
                        </div>
                        <div class="row product-block">
                            <div class="form-group col-xs-4">
                                <div class="col-xs-10">
                                    <label>Producto</label>
                                    <input type="text" class="form-control" name="store_product_name" id="store_product_name" placeholder="Producto"  onfocus="product_modal()" value="{{Input::old('store_product_name')}}">
                                </div>
                                <div class="col-xs-2">
                                     <label>&nbsp;</label>
                                    <button type="button" id="btn-search-product" class="btn btn-primary" onclick="product_modal()">Buscar</button>
                                </div>
                                <input type="hidden" name="store_product_id" id="store_product_id" value="{{Input::old('store_product_id')}}">
                                <input type="hidden" name="store_product_handle_expiration_date" id="store_product_handle_expiration_date" value="{{Input::old('store_product_handle_expiration_date')}}">
                            </div>
                            <div class="form-group col-xs-4 expiration-date">
                                <label>Fecha de vencimiento</label>
                                <input type="text" name="expiration_date" id="expiration_date" class="form-control" placeholder="Fecha de vencimiento" value="{{Input::old('expiration_date')}}">
                            </div>
                        </div>
                        <div class="row position-to-block unseen">
                            <div class="col-xs-12">
                                <h4>Destino</h4>
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Posición</label>
                                <input type="text" name="position_to" id="position_to" class="form-control" placeholder="Posición" maxlength="3" value="{{Input::old('position_to')}}">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Posición en altura</label>
                                <input type="text" name="position_height_to" id="position_height_to" class="form-control" placeholder="Posición en altura" maxlength="1" value="{{Input::old('position_height_to')}}">
                            </div>
                             <div class="form-group col-xs-4">
                                <label>Cantidad de unidades</label>
                                <input type="text" name="quantity_to" id="quantity_to" class="form-control" placeholder="Cantidad de unidades" value="{{Input::old('quantity_to')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <label>&nbsp;</label>
                                <button type="button" id="btn-save-storage" class="btn btn-primary">Guardar almacenamiento</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- Add products Modal -->
<div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar producto</h4>
            </div>
            <div class="modal-body">
                <div class="row search-product">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <table width="100%" class="modal-product-request-table">
                                    <tbody>
                                        <tr>
                                            <td align="right"><label>Buscar:</label>&nbsp;</td>
                                            <td>
                                                <input type="text" placeholder="Nombre, referencia" name="s" id="s" class="modal-search form-control">
                                            </td>
                                            <td colspan="2" align="left">
                                                <button type="button" id="btn-search-products" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 products-request pre-scrollable">
                                <table id="products-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Referencia</th>
                                            <th>Nombre</th>
                                            <th>Unidad de medida</th>
                                            <th>Seleccionar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="10" align="center">No hay productos.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div align="center" class="products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add products in position Modal -->
<div class="modal fade" id="modal-products-in-position" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar producto a mover de la posición <span class="position-label"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row search-product">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 products-position-request pre-scrollable">
                                <table id="product-position-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Imagen</th>
                                            <th>Referencia</th>
                                            <th>Nombre</th>
                                            <th>Unidad de medida</th>
                                            <th>Unidades almacenadas</th>
                                            <th>Fecha de vencimiento</th>
                                            <th>Fecha de almacenamiento</th>
                                            <th>Seleccionar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="14" align="center">No hay productos en la posición.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div align="center" class="products-position-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function product_modal()
{
    if ($('#movement').val() == 'storage')
        show_modal('products');
    else{
        if (!$('#position_from').val().length || !$('#position_from').val().length){
            alert('Por favor ingresa la posición origen.');
            return false;
        }

        show_modal('products-in-position');
        $('.products-position-request-loading').show();
        data = {
            warehouse_id: $('#warehouse_id').val(),
            position_from: $('#position_from').val(),
            position_height_from: $('#position_height_from').val(),
        };
        $.ajax({
            url: '{{ route('adminInventoryStorage.getProductsPositionAjax') }}',
            type: 'GET',
            dataType: 'html',
            data: data
        })
        .done(function(data) {
            $('.products-position-request tbody').html(data);
        })
        .fail(function(data) {
            $('.products-position-request tbody').html('Ocurrió un error al obtener los datos.');
        })
        .always(function(data) {
            $('.products-position-request-loading').hide();
            $('.position-label').html($('#position_from').val() + $('#position_height_from').val());
        });
    }
}

function show_modal(action)
{
    $('#modal-' + action).modal('show');
}

$(document).ready(function(){

    $('#expiration_date').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
    });

    $('#type').change(function(){
        if ($(this).val() == 'Producto'){
            $('.product-block').show();
            $('.product-storage-type').show();
        }else{
            $('.product-block').hide();
            $('.product-storage-type').hide();
            $('#store_product_id').val('');
            $('#store_product_name').val('');
            $('#store_product_handle_expiration_date').val('');
        }
    });
    $('#type').trigger('click');

    $('#movement').change(function(){
        $('#store_product_id').val('');
        $('#store_product_name').val('');
        $('#store_product_handle_expiration_date').val('');
        if ($(this).val() == 'move'){
            $('.position-to-block').show();
            $('.expiration-date').hide();
            $('#expiration_date').val('');
            $('.quantity-from').hide();
            $('#quantity_from').val('');
        }else{
            if ($(this).val() == 'remove'){
                $('.expiration-date').hide();
                $('#expiration_date').val('');
            }else $('.expiration-date').show();
            $('.position-to-block').hide();
            $('#quantity_to').val('');
            $('#position_to').val('');
            $('#position_height_to').val('');
            $('.quantity-from').show();
        }
    });
    $('#movement').trigger('change');

    $('#btn-search-products').click(function() {
        if ($('#s').val().length < 3){
            alert('Por favor escribe al menos 3 letras.');
            return false;
        }
        $('.products-request-loading').show();
        data = {
            s: $('#s').val(),
            warehouse_id: $('#warehouse_id').val(),
            storage_type: $('#storage_type').val()
        };
        $.ajax({
            url: '{{ route('adminInventoryStorage.searchProductsAjax') }}',
            type: 'GET',
            dataType: 'html',
            data: data
        })
        .done(function(data) {
            $('.products-request-loading').hide();
            $('.products-request tbody').html(data);
        })
        .fail(function(data) {
            $('.products-request-loading').hide();
            $('.products-request tbody').html('Ocurrió un error al obtener los datos.');
        })
        .always(function(data) {
            $('.products-request-loading').hide();
        });
    });

    $('body').on('click', '.product-selected', function(){
       $('#store_product_name').val($(this).data('name'));
       $('#store_product_id').val($(this).data('id'));
       $('#store_product_handle_expiration_date').val($(this).data('handle-expiration-date'));
       $('#modal-products').modal('toggle');
    });

    $('body').on('click', '.product-position-selected', function(){
       $('#store_product_name').val($(this).data('name'));
       $('#store_product_id').val($(this).data('id'));
       $('#store_product_handle_expiration_date').val($(this).data('handle-expiration-date'));
       $('#modal-products-in-position').modal('toggle');
    });

    $('#btn-save-storage').on('click', function(e){
        var validator = $('#form-storage').validate({
                            rules: {
                                city_id: 'required',
                                warehouse_id: 'required',
                                type: 'required',
                                movement: 'required',
                                store_product_name: {
                                    required: function (element) {
                                        if ($("#type").val() == 'Producto')
                                            return true;
                                        else
                                            return false;
                                    }
                                },
                                expiration_date: {
                                    required: function (element) {
                                        if ($("#type").val() == 'Producto' && $('#store_product_handle_expiration_date').val() == 1)
                                            return true;
                                        else
                                            return false;
                                    }
                                },
                                quantity_from: {
                                    required: true,
                                    number: true,
                                },
                                position_from: {
                                    required: true,
                                    number: true,
                                },
                                position_height_from: {
                                    required: true,
                                },
                                quantity_to: {
                                    required: function (element) {
                                        if ($("#movement").val() == 'move')
                                            return true;
                                        else
                                            return false;
                                    },
                                    number: true,
                                },
                                position_to: {
                                    required: function (element) {
                                        if ($("#movement").val() == 'move')
                                            return true;
                                        else
                                            return false;
                                    },
                                    number: true,
                                },
                                position_height_to: {
                                    required: function (element) {
                                        if ($("#movement").val() == 'move')
                                            return true;
                                        else
                                            return false;
                                    },
                                }
                            }
                        });
        if(validator.form()){
            if (confirm('¿Estas seguro que deseas guardar el almacenamiento?'))
                $('#form-storage').submit();
        }
    });

});

</script>

@stop

@else

    @section('product-table')
        @if (isset($products))
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td><img src="{{$product->image_small_url}}" height="50"></td>
                        <td>{{$product->reference}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->quantity}} {{$product->unit}}</td>
                        <td>
                            <div class="btn-group">
                               <a class="btn btn-xs btn-default product-selected" href="javascript:;" data-id="{{ $product->id }}" data-handle-expiration-date="{{ $product->handle_expiration_date }}" data-name="{{ $product->name.' '.$product->quantity.' '.$product->unit }}">
                                   <span class="glyphicon glyphicon-folder-open"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="7" align="center">Productos no encontrados.</td></tr>
            @endif
        @else
            <tr><td colspan="7" align="center">&nbsp;</td></tr>
        @endif
    @stop

    @section('product-position-table')
        @if (isset($products))
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td>{{$product->type}}</td>
                        <td><img src="{{$product->image_small_url}}" height="50"></td>
                        <td>{{$product->reference}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->product_quantity}} {{$product->unit}}</td>
                        <td align="center"><strong>{{$product->quantity}}</strong></td>
                        <td>@if ($product->type == 'Producto' && !empty($product->expiration_date)){{ date("d M Y", strtotime($product->expiration_date)) }}@endif</td>
                        <td>{{ date("d M Y g:i a", strtotime($product->created_at)) }}</td>
                        <td>
                            <div class="btn-group">
                               <a class="btn btn-xs btn-default product-position-selected" href="javascript:;" data-id="{{ $product->id }}" data-handle-expiration-date="{{ $product->handle_expiration_date }}" data-name="{{ $product->name.' '.$product->product_quantity.' '.$product->unit }}">
                                   <span class="glyphicon glyphicon-folder-open"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="14" align="center">No hay productos en la posición.</td></tr>
            @endif
        @else
            <tr><td colspan="14" align="center">&nbsp;</td></tr>
        @endif
    @stop

@endif