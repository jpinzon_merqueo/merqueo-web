@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        @if ($admin_permissions['insert'])
        <span class="breadcrumb" style="top:0px">
            <a href="{{ route('adminInventoryStorage.add') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Almacenamiento</button>
            </a>
            @if ($admin_permissions['permission1'])
            <a href="{{ route('adminInventoryStorage.importRemove') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-upload"></i> Importar Almacenamiento</button>
            </a>
            @endif
        </span>
        @endif
    </section>
    <section class="content">
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
            @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
            @endif
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="routes-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if( Session::get('admin_city_id') == $warehouse->id ) selected="selected" @endif >{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Buscar:</label>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="Posición, posición en altura, nombre de producto, referencia" id="input-search" class="form-control">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-search">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <br />
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        var web_url_ajax = "{{ route('adminInventoryStorage.index') }}";

        $(document).ready(function(){

            function search_storage(page, search)
            {
                if (search != '' && search.length <= 1){
                    alert('Digita mas de una letra en el buscador.');
                    return false;
                }

                $('.paging').html('');
                $('.paging-loading').show();
                data = {
                    warehouse_id: $('#warehouse_id').val(),
                    s: search,
                    p: 1
                };

                $.ajax({
                    url: web_url_ajax,
                    data: data,
                    type: 'GET',
                    dataType: 'html',
                    success: function(response) {
                        $('.paging-loading').hide();
                        $('.paging').html(response);
                    }, error: function() {
                        $('.paging-loading').hide();
                        $('.paging').html('Ocurrió un error al obtener los datos.');
                    }
                });
            }

            $('.btn-search').click(function(){
                search_storage(1, $('#input-search').val());
            });
            $('.btn-search').trigger('click');

        });
    </script>
    @endsection
@else
    @section('content')
    <table id="storages-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Ciudad</th>
                <th>Bodega</th>
                <th>Fecha de almacenamiento</th>
                <th>Tipo</th>
                <th>Producto</th>
                <th>Unidades almacenadas</th>
                <th>Fecha de vencimiento</th>
                <th>Posición</th>
                <th>Registrado por</th>
            </tr>
        </thead>
        <tbody>
            @if(count($storages))
            	@foreach($storages as $storage)
                <tr>
                    <td>{{ $storage->city }}</td>
                    <td>{{ $storage->warehouse }}</td>
                    <td>{{ date("d M Y g:i a", strtotime($storage->created_at)) }}</td>
                    <td>{{ $storage->type }}</td>
                    <td>@if ($storage->type == 'Producto'){{ $storage->name.' '.$storage->product_quantity.' '.$storage->unit }}@endif</td>
                    <td>{{ $storage->quantity }}</td>
                    <td>@if ($storage->type == 'Producto' && !empty($storage->expiration_date)){{ date("d M Y", strtotime($storage->expiration_date)) }}@endif</td>
                    <td>{{ $storage->position.$storage->position_height }}</td>
                    <td>{{ $storage->admin }}</td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="9" align="center">No hay datos.</td></tr>
            @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="table_info">Mostrando {{ count($storages) }} ítems</div>
        </div>
    </div>
    @endsection
@endif