@extends('admin.layout')
@section('content')
    <style>
        table td {
            vertical-align: middle !important;
        }
    </style>
<div id="content">
    <section class="content-header" style="position: relative;">
        <div class="col-xs-12">
            <h2>
                {{ $title }} {{ $transfer->id }}
                <small>Control panel</small>
            </h2>
        </div>
        <span class="breadcrumb" style="top: 0px; float: right;position: absolute;right: 10px;">
            <a href="{{ route('productTransfer.index') }}">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>Regresar a grilla</button>
            </a>
                @if(!empty($transfer) && in_array($transfer->status, ['Initiated']))
                    <form method="POST" action="{{ route('productTransfer.updateTransferStatus', ['id' => $transfer->id]) }}" class="form-inline" style="display: inline-block" @submit="confirm('¿Desea iniciar la transferencia?')">
                        <input type="hidden" name="status" value="In proccess">
                        <button class="btn btn-primary" type="submit">Iniciar</button>
                    </form>
                @endif
                @if(!empty($transfer) && in_array($transfer->status, ['In process']))
                    <form method="POST" action="{{ route('productTransfer.updateTransferStatus', ['id' => $transfer->id]) }}" class="form-inline" style="display: inline-block" @submit="confirm('¿Desea recibir la transferencia?')">
                        <input type="hidden" name="status" value="Received">
                        <button class="btn btn-primary" type="submit">Recibir</button>
                    </form>
                @endif
                @if(!empty($transfer) && !in_array($transfer->status, ['Storaged', 'Received', 'Cancelled_storaged']))
                    <form method="POST" action="{{ route('productTransfer.updateTransferStatus', ['id' => $transfer->id]) }}" class="form-inline" style="display: inline-block" @submit="confirm('¿Desea cancelar la transferencia?')">
                        <input type="hidden" name="status" value="Cancelled">
                        <button class="btn btn-danger" type="submit">Cancelar</button>
                    </form>
                @endif
        </span>
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable" style="overflow: hidden">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Éxito!</b> {{ Session::get('success') }}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable" style="overflow: hidden">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
    @endif
    @if($errors->all())
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> Ocurrieron los siquientes errores
            <ul>
                @foreach($errors->all()as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="callout callout-info">
                    <h4>Datos de Traslado</h4>
                    <p>
                        <label for="">Estado</label> - 
                        <span class="label label-{{ $transfer->status_class }}">
                            {{ $transfer->spanish_status }}
                        </span>
                    </p>
                    <p>
                        <label for="">Bodega de origen</label> - {{ $transfer->originWarehouse->warehouse }}
                    </p>
                    <p>
                        <label for="">Bodega de destino</label> - {{ $transfer->destinyWarehouse->warehouse }}
                    </p>
                    <p>
                        <label for="">Fecha de creación</label> - {{ $transfer->created_at_date_formatted }}
                    </p>
                    <p>
                        <label for="">Fecha de recepción</label> - {{ $transfer->received_date_formatted }}
                    </p>
                    <p>
                        <label for="">Fecha de almacenamiento</label> - {{ $transfer->storage_date_formatted }}
                    </p>
                    <p>
                        <label for="">Creado por</label> - {{ $transfer->admin->fullname }}
                    </p>
                </div>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="box-title">
                            Productos trasladados
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                <label for="search">Buscar</label>
                                <input type="text" ref="search" name="search" id="search" placeholder="ID, Referencia, PLU, Nombre" class="form-control" v-model="search" @keyup.enter="searchProducts" :disabled="isLoading">
                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Referencia</th>
                                    <th>PLU</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Cantidad recibida</th>
                                    <th v-show="transfer.status == 'Received' || transfer.status == 'Cancelled'">Cantidad almacenada</th>
                                    <th v-show="transfer.status == 'Received' || transfer.status == 'Cancelled'">Cantidad a almacenar</th>
                                    <th>Estado</th>
                                    <th v-show="transfer.status == 'Received' || transfer.status == 'Cancelled'">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(detail, index) in transfer.product_transfer_details" :key="detail.id">
                                    <td>@{{ index + 1 }}</td>
                                    <td>@{{ detail.store_product.product.id }}</td>
                                    <td>@{{ detail.store_product.product.reference }}</td>
                                    <td>@{{ detail.store_product.provider_plu }}</td>
                                    <td>
                                        @{{ detail.store_product.product.name }} @{{ detail.store_product.product.quantity }} @{{ detail.store_product.product.unit }}
                                        <br>
                                        <b>Posición origen (Posición/Altura/Tipo):</b> @{{ detail.position_from }}/@{{ detail.position_height_from }}/@{{ detail.storage_type }}
                                        <br>
                                        <b>Fecha de expiracion:</b> @{{ detail.expiration_date_formatted }}
                                    </td>
                                    <td>@{{ detail.transferred_quantity }}</td>
                                    <td v-show="!isSearched || transfer.status != 'In process'">@{{ detail.quantity_received }}</td>
                                    <td v-show="isSearched && transfer.status == 'In process'">
                                        <input type="text" ref="quantity_received" :name="`quantity_received-${detail.id}`" v-model="detail.quantity_received" data-vv-as="cantidad" v-validate="`required|min_value:${detail.transferred_quantity}|max_value:${detail.transferred_quantity}|integer`" class="form-control" @keyup.enter="updateQuantityReceived(`quantity_received-${detail.id}`, detail, true)" :disabled="isLoading">
                                        <span style="color: #d73925" v-show="errors.has(`quantity_received-${detail.id}`)">@{{ errors.first(`quantity_received-${detail.id}`) }}</span>
                                    </td>
                                    <td v-show="transfer.status == 'Received' || transfer.status == 'Cancelled'">
                                        @{{ detail.quantity_storaged }}
                                    </td>
                                    <td v-show="transfer.status == 'Received' || transfer.status == 'Cancelled'">
                                        <input ref="quantity_storage" :disabled="detail.status == 'In process' || detail.quantity_received == 0" type="text" v-model="detail.quantity_to_storage" class="form-control" ref="quantity_to_storage" :name="`quantity_to_storage-${detail.id}`" data-vv-as="cantidad" v-validate="`required|integer|min_value:1|max_value:${detail.quantity_received - detail.quantity_storaged}`" @keyup.enter="addProductStoraged(`quantity_to_storage-${detail.id}`, detail)">
                                        <span style="color: #d73925" v-show="errors.has(`quantity_to_storage-${detail.id}`)">@{{ errors.first(`quantity_to_storage-${detail.id}`) }}</span>
                                    </td>
                                    <td>
                                        <span :class="`label label-${detail.status_class}`">@{{ detail.spanish_status }}</span>
                                    </td>
                                    <td v-show="transfer.status == 'Received' || transfer.status == 'Cancelled'">
                                        <button class="btn btn-primary" @click="addProductStoraged(`quantity_to_storage-${detail.id}`, detail)" v-show="detail.status != 'No recibido' || detail.quantity_received > 0">
                                            Agregar
                                        </button>
                                    </td>
                                </tr>
                                <tr v-show="transfer.product_transfer_details.length == 0">
                                    <td colspan="8" class="text-center">No se encontraron productos</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-group" v-show="transfer.status == 'Received' || transfer.status == 'Cancelled' && products_storage.length > 0">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="box-title">
                            Estiba
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('productTransfer.storageProducts', ['id' => $transfer->id]) }}" method="POST" @submit="validateStorageProcess" data-vv-scope="custom_storage">
                            <div class="row">
                                <div class="col-xs-3 form-group">
                                    <label for="position">Posición (@{{ destiny_storage_min_position_value }} - @{{ destiny_storage_max_position_value }})</label>
                                    <input
                                            type="text"
                                            name="position"
                                            data-vv-scope="custom_storage"
                                            data-vv-as="posición"
                                            id="position"
                                            v-model="position"
                                            class="form-control"
                                            v-validate="`required|numeric|min_value:${destiny_storage_min_position_value}|max_value:${destiny_storage_max_position_value}`">
                                    <span style="color: #d73925" v-show="errors.has(`position`, `custom_storage`)">@{{ errors.first(`position`, `custom_storage`) }}</span>
                                </div>
                                <div class="col-xs-3 form-group">
                                    <label for="position_height">Posición en altura (@{{ destiny_storage_position_height }})</label>
                                    <input
                                            type="text"
                                            name="position_height"
                                            data-vv-scope="custom_storage"
                                            data-vv-as="posición en altura"
                                            id="position_height"
                                            v-model="position_height"
                                            class="form-control" v-validate="`required|alpha|included:${destiny_storage_position_height}`" @keyup="positionHeightUpperCase">
                                    <span style="color: #d73925" v-show="errors.has(`position_height`, `custom_storage`)">@{{ errors.first(`position_height`, `custom_storage`) }}</span>
                                </div>
                                <div class="col-xs-3 form-group">
                                    <label for="storage_type">Tipo de almacenamiento</label>
                                    <select
                                            name="storage_type"
                                            id="storage_type"
                                            data-vv-scope="storage_type"
                                            data-vv-as="tipo almacenamiento"
                                            v-model="storage_type"
                                            class="form-control"
                                            v-validate="`required|alpha`"
                                            @change="onStorageTypeChange">
                                        <option value="Seco">Seco</option>
                                        <option value="Frío">Frío</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has(`storage_type`, `custom_storage`)">@{{ errors.first(`storage_type`, `custom_storage`) }}</span>
                                </div>
                                <div class="col-xs-3 form-group">
                                    <label for="" style="display: block; color: white;">|</label>
                                    <button type="submit" class="btn btn-default">Almacenar</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>ID</th>
                                                <th>Referencia</th>
                                                <th>PLU</th>
                                                <th>Producto</th>
                                                <th>Cantidad recibida</th>
                                                <th>Cantidad ya almacenada</th>
                                                <th>Cantidad a almacenar</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(detail, index) in products_storage" :key="detail.id">
                                                <td>@{{ index + 1 }}</td>
                                                <td>@{{ detail.store_product.product.id }}</td>
                                                <td>@{{ detail.store_product.product.reference }}</td>
                                                <td>@{{ detail.store_product.provider_plu }}</td>
                                                <td>
                                                    @{{ detail.store_product.product.name }} @{{ detail.store_product.product.quantity }} @{{ detail.store_product.product.unit }}
                                                    <br>
                                                    <b>Posición origen (Posición/Altura/Tipo):</b> @{{ detail.position_from }}/@{{ detail.position_height_from }}/@{{ detail.storage_type }}
                                                    <br>
                                                    <b>Fecha de expiracion:</b> @{{ detail.expiration_date_formatted }}
                                                </td>
                                                <td>@{{ detail.quantity_received }}</td>
                                                <td>@{{ detail.quantity_storaged }}</td>
                                                <td>@{{ detail.quantity_to_storage }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" @click="removeProductStorage(detail)">
                                                        Eliminar
                                                    </button>
                                                    <input type="hidden" :name="`product[${detail.id}]`" :value="detail.quantity_to_storage">
                                                </td>
                                            </tr>
                                            <tr v-show="products_storage.length == 0">
                                                <td colspan="9" class="text-center">No se encontraron productos</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    @include('admin.inventory.product_transfer.js.show-js')
@endsection