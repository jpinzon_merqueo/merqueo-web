<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vee-validate@latest"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script>
    const messages = {
        _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
            if (max) {
                return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
            }

            return `El largo del campo ${field} debe ser ${length}.`;
        },
        max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field) => `El campo ${field} debe ser un valor válido.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });

    let vue = new Vue({
        el: '#content',
        data: {
            transfer: {{ $transfer->toJson() }},
            search: '',
            isLoading: false,
            statuses: ['Iniciado ', 'En proceso'],
            isSearched: false,
            products_storage: [],
            position: null,
            position_height: null,
            storage_type: 'Seco',
            storage_min_position_value: null,
            storage_max_position_value: null,
            storage_position_height: null
        },
        methods: {
            searchProducts: function () {
                let self = this;
                this.isLoading = true;
                axios.get('{{ route('productTransfer.searchTransferProductsAjax', ['id' => $transfer->id]) }}', {
                    params: {
                        search: this.search,
                    }
                })
                    .then(function (response) {
                        self.isLoading = false;
                        self.transfer.product_transfer_details = response.data;
                        if (self.search !== '') {
                            self.$nextTick(function () {
                                if (self.transfer.status == 'Cancelled') {
                                    self.$refs.quantity_storage[0].focus();
                                } else {
                                    self.$refs.quantity_received[0].focus();
                                }
                            });
                        } else {
                            self.$nextTick(function () {
                                document.getElementById('search').focus();
                            });
                        }
                        if (self.transfer.product_transfer_details.length > 0 && self.search !== '') {
                            self.isSearched = true;
                        } else {
                            self.isSearched = false;
                        }
                    })
                    .catch(error => {
                        self.isLoading = false;
                        alert(`${error.response.data}`);
                    });
            },
            confirm: function (message) {
                let desicion = confirm(message);
                if (desicion) {
                    return true;
                } else {
                    return false;
                }
            },
            updateQuantityReceived: function (inputName, detail, validateExpirationDate) {
                let self = this;
                let is_valid = this.$validator.validate(inputName, detail.quantity_received)
                    .then(result => {
                        if (!result) {
                            return;
                        }

                        axios
                            .post("{{ route('productTransfer.updateDetailReceivedQuantity', ['id' => $transfer->id]) }}", {
                                detail_id: detail.id,
                                quantity_received: detail.quantity_received,
                                expiration_date: detail.expiration_date
                            })
                            .then(function (response) {
                                self.search = '';
                                if (response) {
                                    self.searchProducts();
                                }
                            })
                            .catch(error => {
                                alert(`${error.response.data}`);
                                self.searchProducts();
                            });
                    });
            },
            storageProducts: function () {
                let desicion = confirm('¿Desea almacenar los productos?');
                if (!desicion){
                    return false;
                }
            },
            addProductStoraged: function (inputName, detail) {
                let self = this;
                let is_valid = this.$validator.validate(inputName, detail.quantity_to_storage)
                    .then(result => {
                        if (!result) {
                            return;
                        }
                        if (this.products_storage.length < 1) {
                            this.storage_type = detail.storage_type;
                        } else {
                            if (this.storage_type != detail.storage_type){
                                let option = confirm(`El tipo de almacenamiento elegido:${this.storage_type}, es diferente al tipo almacenamiento del producto agregado: ${detail.storage_type}. ¿Desea agregar el producto?`);
                                if (!option) {
                                    return false;
                                }
                            }
                        }
                        let product = this.products_storage.filter(product => product.store_product_id == detail.store_product_id);
                        if (product.length == 0) {
                            let clone_product = _.clone(detail, true);
                            this.products_storage.push(clone_product);
                        } else {
                            alert('Ya se había agregado este producto, debe eliminarlo primero para hacer una modificación.');
                            return;
                        }
                    })
                    .catch(error => {
                        alert(`${error.response.data}`);
                    });
            },
            removeProductStorage: function (detail) {
                let product_index = this.products_storage.findIndex(product => product.store_product_id == detail.store_product_id);
                this.products_storage.splice(product_index, 1);
            },
            positionHeightUpperCase: function (e) {
                this.position_height = this.position_height.charAt(0).toUpperCase();
            },
            validateStorageProcess: function (event) {
                let self = this;
                let is_valid = this.$validator.validateAll('custom_storage')
                    .then(result => {
                        if (!result) {
                            event.preventDefault();
                            return false;
                        }
                        return true;
                    })
                    .catch(error => {
                        event.preventDefault();
                        alert(`${error.response.data}`);
                        return false;
                    });
            },
            onStorageTypeChange: function (e) {
                if (this.transfer.status == 'Cancelled' && this.storage_type == 'Seco') {
                    this.storage_min_position_value = this.transfer.origin_warehouse.dry_storage_position.start_position;
                    this.storage_max_position_value = this.transfer.origin_warehouse.dry_storage_position.end_position;
                    this.storage_position_height = this.transfer.origin_warehouse.dry_storage_position.height_positions.join();

                    this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.dry_storage_position.start_position;
                    this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.dry_storage_position.end_position;
                    this.destiny_storage_position_height = this.transfer.destiny_warehouse.dry_storage_position.height_positions.join();
                }
                if (this.transfer.status == 'Cancelled' && this.storage_type == 'Frío') {
                    this.storage_min_position_value = this.transfer.origin_warehouse.cold_storage_position.start_position;
                    this.storage_max_position_value = this.transfer.origin_warehouse.cold_storage_position.end_position;
                    this.storage_position_height = this.transfer.origin_warehouse.cold_storage_position.height_positions.join();

                    this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.cold_storage_position.start_position;
                    this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.cold_storage_position.end_position;
                    this.destiny_storage_position_height = this.transfer.destiny_warehouse.cold_storage_position.height_positions.join();
                }
                if (this.transfer.status == 'Received' && this.storage_type == 'Seco') {
                    this.storage_min_position_value = this.transfer.origin_warehouse.dry_storage_position.start_position;
                    this.storage_max_position_value = this.transfer.origin_warehouse.dry_storage_position.end_position;
                    this.storage_position_height = this.transfer.origin_warehouse.dry_storage_position.height_positions.join();

                    this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.dry_storage_position.start_position;
                    this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.dry_storage_position.end_position;
                    this.destiny_storage_position_height = this.transfer.destiny_warehouse.dry_storage_position.height_positions.join();
                }
                if (this.transfer.status == 'Received' && this.storage_type == 'Frío') {
                    this.storage_min_position_value = this.transfer.origin_warehouse.cold_storage_position.start_position;
                    this.storage_max_position_value = this.transfer.origin_warehouse.cold_storage_position.end_position;
                    this.storage_position_height = this.transfer.origin_warehouse.cold_storage_position.height_positions.join();

                    this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.cold_storage_position.start_position;
                    this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.cold_storage_position.end_position;
                    this.destiny_storage_position_height = this.transfer.destiny_warehouse.cold_storage_position.height_positions.join();
                }

               if (this.storage_type == 'Seco') {
                   let product = this.products_storage.findIndex(product => product.storage_type == 'Frío');
                   if (product > -1){
                       let dry_option = confirm('Se encontraron productos de almacenaje en frío, ¿desea eliminarlos?');
                       if (dry_option) {
                           this.products_storage.splice(product, 1);
                       }
                   }
               }

               if (this.storage_type == 'Frío') {
                   let product = this.products_storage.findIndex(product => product.storage_type == 'Seco');
                   if (product > -1){
                       let dry_option = confirm('Se encontraron productos de almacenaje en seco, ¿desea eliminarlos?');
                       if (dry_option) {
                           this.products_storage.splice(product, 1);
                       }
                   }
               }
            }
        },
        mounted() {
            if (this.transfer.status == 'Cancelled' && this.storage_type == 'Seco') {
                this.storage_min_position_value = this.transfer.origin_warehouse.dry_storage_position.start_position;
                this.storage_max_position_value = this.transfer.origin_warehouse.dry_storage_position.end_position;
                this.storage_position_height = this.transfer.origin_warehouse.dry_storage_position.height_positions.join();

                this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.dry_storage_position.start_position;
                this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.dry_storage_position.end_position;
                this.destiny_storage_position_height = this.transfer.destiny_warehouse.dry_storage_position.height_positions.join();
            }
            if (this.transfer.status == 'Cancelled' && this.storage_type == 'Frío') {
                this.storage_min_position_value = this.transfer.origin_warehouse.cold_storage_position.start_position;
                this.storage_max_position_value = this.transfer.origin_warehouse.cold_storage_position.end_position;
                this.storage_position_height = this.transfer.origin_warehouse.cold_storage_position.height_positions.join();

                this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.cold_storage_position.start_position;
                this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.cold_storage_position.end_position;
                this.destiny_storage_position_height = this.transfer.destiny_warehouse.cold_storage_position.height_positions.join();
            }
            if (this.transfer.status == 'Received' && this.storage_type == 'Seco') {
                this.storage_min_position_value = this.transfer.origin_warehouse.dry_storage_position.start_position;
                this.storage_max_position_value = this.transfer.origin_warehouse.dry_storage_position.end_position;
                this.storage_position_height = this.transfer.origin_warehouse.dry_storage_position.height_positions.join();

                this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.dry_storage_position.start_position;
                this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.dry_storage_position.end_position;
                this.destiny_storage_position_height = this.transfer.destiny_warehouse.dry_storage_position.height_positions.join();
            }
            if (this.transfer.status == 'Received' && this.storage_type == 'Frío') {
                this.storage_min_position_value = this.transfer.origin_warehouse.cold_storage_position.start_position;
                this.storage_max_position_value = this.transfer.origin_warehouse.cold_storage_position.end_position;
                this.storage_position_height = this.transfer.origin_warehouse.cold_storage_position.height_positions.join();

                this.destiny_storage_min_position_value = this.transfer.destiny_warehouse.cold_storage_position.start_position;
                this.destiny_storage_max_position_value = this.transfer.destiny_warehouse.cold_storage_position.end_position;
                this.destiny_storage_position_height = this.transfer.destiny_warehouse.cold_storage_position.height_positions.join();
            }
        }
    });
</script>