<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script>
    let vue = new Vue({
        el: '#content',
        data: {
            formAttr: {
                cities: {{ $cities->toJson() }},
                warehouses: {{ $warehouses->toJson() }}
            },
            formData: {
                city_id: {{ Session::get('admin_city_id') }},
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
                status: 'Initiated',
                id: ''
            },
            paginator: {
                data: [],
                total: 0,
                per_page: 0,
                current_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
                code: '',
                campaign_validation: '',
            },
            pages: [],
        },
        methods: {
            searchTransfers: function (n) {
                let self = this;
                axios.get('{{ route('productTransfer.searchTransfersAjax') }}', {
                    params: {
                        city_id: this.formData.city_id,
                        warehouse_id: this.formData.warehouse_id,
                        status: this.formData.status,
                        id: this.formData.id,
                        page: n
                    }
                })
                    .then(function (response) {
                        response.data.data.map(function (i) {
                            let str = '{{ route('productTransfer.show') }}';
                            i.route = str.replace('%7Bid%7D', i.id);
                            return i;
                        });
                        self.paginator = response.data;
                        self.calculatePages();
                    })
                    .catch(error => {
                        alert(`${error.response.data}`);
                    });
            },
            calculatePages: function () {
                let last = this.paginator.last_page;
                let delta = 5;
                let left = this.paginator.current_page - delta;
                let right = this.paginator.current_page + delta + 1;
                let range = [];
                let pages = [];
                let l;

                for (let i = 1; i <= last ; i++) {
                    if (i == 1 || i == last || (i >= left && i < right)) {
                        range.push(i);
                    }
                }

                range.forEach(function (i) {
                    if (l) {
                        if (i - l === 2) {
                            pages.push(l + 1);
                        } else if (i - l !== 1) {
                            pages.push('...');
                        }
                    }
                    pages.push(i);
                    l = i;
                });

                this.pages = pages;
            },
            getWarehouses: function () {
                let self = this;
                axios.get('{{ route('productTransfer.getWarehousesAjax') }}', {
                    params: {
                        city_id: this.formData.city_id
                    }
                })
                    .then(function (response) {
                        self.formAttr.warehouses = response.data;
                        self.formData.warehouse_id = self.formAttr.warehouses[0].id;
                    })
                    .catch(error => {
                        alert(`${error.response.data}`);
                    });
            }
        },
        mounted: function () {
            this.searchTransfers(0);
        }
    });
</script>