<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vee-validate@latest"></script>
<script>
    const messages = {
        _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
            if (max) {
                return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
            }

            return `El largo del campo ${field} debe ser ${length}.`;
        },
        max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field) => `El campo ${field} debe ser un valor válido.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };
    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });
    let searchForm = new Vue({
        el: '#searchForm',
        data: {
            isSuperAdmin: {{ Session::get('admin_designation') == 'Super Admin' ? 'true' : 'false' }},
            searchForm: {
                cities: {{ $cities->toJson() }},
                city_id: {{ Session::get('admin_city_id') }},
                warehouses: {{ $warehouses->toJson() }},
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
                search: null
            },
            foundProducts: false,
            isLoading: false,
            products_added: []
        },
        methods:{
            getWarehouses: function (e) {
                let self = this;
                this.isLoading = true;
                this.searchForm.warehouse_id = null;
                grid.city_id = this.searchForm.city_id;
                axios
                    .get("{{ route('productTransfer.getWarehousesAjax') }}", {
                        params: {
                            city_id: this.searchForm.city_id
                        }
                    })
                    .then(function (response) {
                        self.searchForm.warehouses = response.data;
                        grid.warehouses = response.data;
                        self.isLoading = false;
                    })
                    .catch(error => {
                        alert(`${error.response.data}`);
                    });
            },
            searchProducts: function (e) {
                let self = this;

                this.$validator.validate().then(result => {
                    if (!result) {
                        return;
                    } else {
                        self.isLoading = true;
                        grid.destiny_city_id = grid.source_city_id = self.searchForm.city_id;
                        grid.origin_warehouse_id = self.searchForm.warehouse_id;
                        axios
                            .get("{{ route('productTransfer.searchProductsAjax') }}", {
                                params: {
                                    city_id: self.searchForm.city_id,
                                    warehouse_id: self.searchForm.warehouse_id,
                                    search: self.searchForm.search,
                                    products_added: self.products_added
                                }
                            })
                            .then(response => {
                                if (response.data.length) {
                                    this.foundProducts = true;
                                }
                                grid.products_found = response.data;
                                this.isLoading = false
                            })
                            .catch(error => {
                                alert(`${error.response.data}`);
                                this.isLoading = false
                            });
                    }
                });
            },
            reset: function (e) {
                this.foundProducts = false;
                this.isLoading = false;
                this.products_added = [];
                this.searchForm.search = '';
                grid.products_added = [];
                grid.products_found = [];
                grid.destiny_city_id = '';
                grid.source_city_id = '';
                grid.destiny_warehouse_id = '';
                grid.origin_warehouse_id = '';
                this.getWarehouses();
            }
        }
    });

    let grid = new Vue({
        el: '#grid',
        data: {
            products_found: [],
            products_added: [],
            cities: {{ $cities->toJson() }},
            destiny_city_id: '',
            source_city_id: '',
            warehouses: {{ $warehouses->toJson() }},
            destiny_warehouse_id: '',
            origin_warehouse_id: '',
            isLoading: false
        },
        methods: {
            addProduct: function (inputName, product, index) {
                let is_valid = this.$validator.validate(inputName, product.transfer_quantity)
                    .then(result => {
                        if (!result) {
                            return;
                        }
                        if  (product.expiration_date != null) {
                            let now = moment().format('YYYY-MM-DD');
                            let is_valid_date = moment(product.expiration_date, 'YYYY-MM-DD').isAfter(now);
                            if (!is_valid_date){
                                alert('El producto está vencido y no se puede hacer traslado,');
                                return;
                            }
                        }
                        this.products_added.unshift(product);
                        searchForm.products_added = this.products_added;
                        this.products_found.splice(index, 1);
                    });
            },
            removeProduct: function (index, product) {
                this.products_added.splice(index, 1);
                searchForm.products_added = this.products_added;
                this.products_found.unshift(product);
            },
            createTransfer: function (e) {
                this.origin_warehouse_id = searchForm.searchForm.warehouse_id;
                this.isLoading = true;
                let is_valid = this.$validator.validate('destiny_warehouse_id', this.destiny_warehouse_id)
                    .then(result => {
                        if (!result) {
                            e.preventDefault();
                            this.isLoading = false;
                            return false;
                        }
                    });
            },
            getWarehouses: function (e) {
                this.isLoading = true;
                this.destiny_warehouse_id = null;
                axios
                    .get("{{ route('productTransfer.getWarehousesAjax') }}", {
                        params: {
                            city_id: this.destiny_city_id
                        }
                    })
                    .then(response => {
                        this.warehouses = response.data;
                        this.isLoading = false;
                    })
                    .catch(error => {
                        alert(`${error.response.data}`);
                        this.isLoading = false;
                    });
            },
        },
        computed: {
            warehousesToShow: function () {
                return this.warehouses.filter((warehouse) => warehouse.id !== this.origin_warehouse_id);
            },
            productsAdded: function () {
                return JSON.stringify(this.products_added);
            }
        }
    });
</script>