@extends('admin.layout')
@section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb">
            <a href="{{ route('productTransfer.create') }}">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Transferencia</button>
            </a>
        </span>
    </section>
    <section id="content" class="content">
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form @submit.prevent="searchTransfers(1)">
                            <div class="row form-group">
                                <div class="col-lg-3 col-xs-12">
                                    <label for="city_id">Ciudad</label>
                                    <select name="city_id" id="city_id" class="form-control" v-model="formData.city_id" @change="getWarehouses">
                                        <option v-for="(city, index) in formAttr.cities" :value="city.id">@{{ city.city }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12">
                                    <label for="warehouse_id">Bodega</label>
                                    <select name="warehouse_id" id="warehouse_id" class="form-control" v-model="formData.warehouse_id">
                                        <option v-for="(warehouse, index) in formAttr.warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12">
                                    <label for="status">Estado</label>
                                    <select name="status" id="status" class="form-control" v-model="formData.status">
                                        <option value="Initiated">Iniciado</option>
                                        <option value="In process">En proceso</option>
                                        <option value="Received">Recibido</option>
                                        <option value="Storaged">Almacenado</option>
                                        <option value="Cancelled">Cancelado</option>
                                        <option value="Cancelled_storaged">Cancelado y almacenado</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12">
                                    <label for="id">ID:</label>
                                    <input type="number" min="0" id="id" name="id" v-model="formData.id" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th>Estado</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(transfer, index) in paginator.data">
                                        <td>@{{ index + 1 }}</td>
                                        <td>@{{ transfer.id }}</td>
                                        <td>@{{ transfer.origin_warehouse.warehouse }}</td>
                                        <td>@{{ transfer.destiny_warehouse.warehouse }}</td>
                                        <td>
                                            <span :class="`label label-${transfer.status_class}`">@{{ transfer.spanish_status }}</span>
                                        </td>
                                        <td class="text-center">
                                            <a :href="transfer.route">
                                                <button type="button" class="btn btn-danger">
                                                    Editar
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr v-show="paginator.data.length == 0">
                                        <td colspan="6" class="text-center">No se encontraron transferencias</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row" >
                                <div class="col-xs-12 text-center" >
                                    <ul class="pagination">
                                        <li :class="paginator.current_page == 1 ? 'disabled' : ''">
                                            <a href="javascript:void(0)" @click.prevent="paginator.current_page != 1 ? searchTransfers(paginator.current_page-1) : ''">&laquo;</a>
                                        </li>
                                        <li v-for="n in pages" :class="paginator.current_page == n ? 'active' : ''">
                                            <a href="javascriot:void(0)" @click.prevent="paginator.current_page != n && n != '...' ? searchTransfers(n) : ''">
                                                @{{ n }}
                                                <span class="sr-only" v-show="paginator.current_page == n">(current)</span>
                                            </a>
                                        </li>
                                        <li :class="paginator.current_page == paginator.last_page ? 'disabled' : ''">
                                            <a href="javascript:void(0)" @click.prevent="paginator.current_page != paginator.last_page ? searchTransfers(paginator.current_page+1) : ''">&raquo;</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.inventory.product_transfer.js.index-js')
@endsection