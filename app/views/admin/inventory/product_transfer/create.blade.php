@extends('admin.layout')
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <style>
        table td {
            vertical-align: middle !important;
        }
    </style>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top: 0px; float: right;position: absolute;right: 10px;">
            <a href="{{ route('productTransfer.index') }}">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>Regresar a grilla</button>
            </a>
        </span>
    </section>
    <section id="content" class="content">
        @if($errors->all())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> Ocurrieron los siquientes errores
                <ul>
                    @foreach($errors->all()as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="box-title">
                            Bodega de origen
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="" @submit.prevent="searchProducts" id="searchForm">
                            <div class="row">
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="city_id">Ciudad</label>
                                    <select v-validate="'required'" name="city_id" id="city_id" class="form-control" v-model="searchForm.city_id" :disabled="!isSuperAdmin || isLoading || foundProducts" @change="getWarehouses" required>
                                        <option v-for="(city, index) in searchForm.cities" :value="city.id">@{{ city.city }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group" :class="errors.has('warehouse_id') ? 'has-error' : ''">
                                    <label for="warehouse_id">Bodega</label>
                                    <select v-validate="'required|integer|min:0'" data-vv-as="bodega" name="warehouse_id" id="warehouse_id" class="form-control" v-model="searchForm.warehouse_id" :disabled="!isSuperAdmin || isLoading || foundProducts">
                                        <option v-for="(warehouse, index) in searchForm.warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('warehouse_id')">@{{ errors.first('warehouse_id') }}</span>
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group" :class="errors.has('search') ? 'has-error' : ''">
                                    <label for="search">Buscar</label>
                                    <input v-validate="'required|min:2|alpha_num'" data-vv-as="buscar" type="text" id="search" data-vv-as="buscar" name="search" class="form-control" v-model="searchForm.search" placeholder="ID, Referencia, Nombre, PLU" :disabled="isLoading">
                                    <span style="color: #d73925" v-show="errors.has('search')">@{{ errors.first('search') }}</span>
                                </div>
                                <div class="col-lg-1 col-xs-12 form-group">
                                    <label for="" style="color: #fff; display: block">|</label>
                                    <button type="submit" class="btn btn-primary" :disabled="isLoading">Buscar</button>
                                </div>
                                <div class="col-lg-1 col-xs-12 form-group">
                                    <label for="" style="color: #fff; display: block">|</label>
                                    <button type="reset" class="btn btn-primary" @click="reset">Reset</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <div align="center" class="paging-loading" v-show="isLoading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="grid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row"style="overflow: auto; max-height: 450px;">
                                <div class="col-xs-12">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Producto</th>
                                            <th>Almacenamiento</th>
                                            <th>Posición en altura</th>
                                            <th>Posición</th>
                                            <th>Cantidad en posición</th>
                                            <th>Cantidad a transferir</th>
                                            <th>Cantidad restante</th>
                                            <th>Acción</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(product, index) in products_found" :key="product.warehouse_storage_id">
                                                <td>
                                                    @{{ index + 1 }}
                                                </td>
                                                <td>
                                                    @{{ product.product_id }}
                                                </td>
                                                <td>
                                                    @{{ product.reference }}
                                                </td>
                                                <td>
                                                    @{{ product.name }} - @{{ product.quantity_unit }} @{{ product.unit }}
                                                    <br> @{{ product.formatted_expiration_date }}
                                                </td>
                                                <td>
                                                    @{{ product.storage_type }}
                                                </td>
                                                <td>
                                                    @{{ product.position_height_from }}
                                                </td>
                                                <td>
                                                    @{{ product.position_from }}
                                                </td>
                                                <td>
                                                    @{{ product.quantity }}
                                                </td>
                                                <td>
                                                    <div :class="errors.has('quantity-'+product.warehouse_storage_id) ? 'has-error' : ''">
                                                        <input type="number" v-model="product.transfer_quantity" data-vv-as="cantidad" :key="product.warehouse_storage_id" :name="'quantity-'+product.warehouse_storage_id" v-validate="{max_value: product.quantity, min_value: 1, integer: true, required: true}" class="form-control" @keyup.enter="addProduct('quantity-'+product.warehouse_storage_id, product, index)">
                                                        <span style="color: #d73925" v-show="errors.has('quantity-'+product.warehouse_storage_id)">@{{ errors.first('quantity-'+product.warehouse_storage_id) }}</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    @{{ product.quantity - product.transfer_quantity }}
                                                </td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-success" @click="addProduct('quantity-'+product.warehouse_storage_id, product, index)">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr v-show="products_found.length == 0">
                                                <td colspan="11" class="text-center">No se encontraro productos</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" v-show="products_added.length > 0">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="box-title">
                                Bodega de destino
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <form action="{{ route('productTransfer.store') }}" method="POST" @submit="createTransfer">
                                    <div class="col-xs-3 form-group">
                                        <label for="destiny_city_id">Ciudad</label>
                                        <select name="destiny_city_id" id="destiny_city_id" class="form-control" v-model="destiny_city_id" @change="getWarehouses" :disabled="isLoading">
                                            <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3 form-group">
                                        <label for="destiny_warehouse_id">Bodega</label>
                                        <select name="destiny_warehouse_id" data-vv-as="bodega" id="destiny_warehouse_id" class="form-control" v-model="destiny_warehouse_id" v-validate="{required: true}" :disabled="isLoading">
                                            <option v-for="(warehouse, index) in warehousesToShow" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                        </select>
                                        <span style="color: #d73925" v-show="errors.has('destiny_warehouse_id')">@{{ errors.first('destiny_warehouse_id') }}</span>
                                    </div>
                                    <div class="col-xs-3 form-group" v-show="products_added.length > 0">
                                        <label for="submit" style="color: #FFF; display: block;">|</label>
                                        <button type="submit" class="btn btn-primary" :disabled="isLoading">Crear</button>
                                    </div>
                                    <input type="hidden" name="products" :value="productsAdded" v-validate="{required: true}">
                                    <input type="hidden" name="origin_warehouse_id" id="origin_warehouse_id" v-model="origin_warehouse_id">
                                    <input type="hidden" name="destiny_warehouse_id" id="destiny_warehouse_id" v-model="destiny_warehouse_id">
                                </form>
                            </div>
                            <div class="row"style="overflow: auto; max-height: 450px;">
                                <div class="col-xs-12">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Producto</th>
                                            <th>Almacenamiento</th>
                                            <th>Posición en altura</th>
                                            <th>Posición</th>
                                            <th>Cantidad en posición</th>
                                            <th>Cantidad a transferir</th>
                                            <th>Cantidad restante</th>
                                            <th>Acción</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(product, index) in products_added" :key="product.warehouse_storage_id">
                                                <td>
                                                    @{{ index + 1 }}
                                                </td>
                                                <td>
                                                    @{{ product.product_id }}
                                                </td>
                                                <td>
                                                    @{{ product.reference }}
                                                </td>
                                                <td>
                                                    @{{ product.name }} - @{{ product.quantity_unit }} @{{ product.unit }}
                                                    <br>
                                                    @{{ product.formatted_expiration_date }}
                                                </td>
                                                <td>
                                                    @{{ product.storage_type }}
                                                </td>
                                                <td>
                                                    @{{ product.position_height_from }}
                                                </td>
                                                <td>
                                                    @{{ product.position_from }}
                                                </td>
                                                <td>
                                                    @{{ product.quantity }}
                                                </td>
                                                <td>
                                                    <input type="number" v-model="product.transfer_quantity" data-vv-as="Cantidad" :key="product.warehouse_storage_id" :name="'quantity-'+product.warehouse_storage_id" class="form-control" disabled>
                                                </td>
                                                <td>
                                                    @{{ product.quantity - product.transfer_quantity }}
                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger" @click="removeProduct(index, product)">
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr v-show="products_added.length == 0">
                                                <td colspan="11" class="text-center">No se encontraro productos</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.inventory.product_transfer.js.create-js')
@endsection