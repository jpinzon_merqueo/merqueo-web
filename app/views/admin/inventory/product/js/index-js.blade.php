<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.1.0-beta.11/dist/vee-validate.min.js"></script>

<script>
    const messages = {
            _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
        if (max) {
            return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
        }

        return `El largo del campo ${field} debe ser ${length}.`;
    },
    max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field, [excluded]) => `El campo ${field} debe ser diferente a ${excluded}.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });
    let searchForm = new Vue({
        el: '#searchForm',
        data: {
            from:{
                city_id: {{ Session::get('admin_city_id') }},
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
                product_info: '',
            },
            cities: {{ $cities->toJson() }},
            warehouses: {{ $warehouses->toJson() }},
            isLoading: false,
            product: null,
        },
        mounted(){
            this.$refs.reference.focus();
        },
        methods: {
            clearProduct: function () {
                this.product = null;
            },
            getWarehousesFrom: function (e) {
                this.isLoading = true;
                this.from.warehouse_id = '';
                this.warehouses = [];
                this.clearProduct();
                axios.get('{{ route('adminProductInfo.getWarehousesAjax') }}', {
                    params: {
                        cityId: this.from.city_id,
                    }
                })
                    .then(response => {
                    this.warehouses = response.data;
            })
            .catch(error => {
                    alert(`${error.response.data}`);
            })
            .then(() => {
                    this.isLoading = false;
            });
            },
            getProductAjax: function (e) {
                this.$validator.validate('from.*').then(result => {
                    if (!result) {
                    return false;
                }else{
                    this.isLoading = true;
                    this.product = null;
                    axios.get('{{ route('adminProductInfo.getProductAjax') }}', {
                        params: {
                            warehouseId: this.from.warehouse_id,
                            product: this.from.product_info
                        }
                    })
                        .then(response => {
                        this.product=response.data;
                    this.from.product_info = '';
                })
                .catch(error => {
                        this.from.product_info = '';
                    alert(`${error.response.data}`);
                })
                .then(() => {
                        this.isLoading = false;
                });
                }
            })
            },
            getInvestigation: function(id,store_id){
                this.isLoading = true;
                this.infoInvestigation = null;
                axios.get('{{ route('adminInvestigationStock.getInvestigationStock') }}', {
                    params: {
                        storeProductId: store_id,
                        startDate: this.from.dateStart,
                        warehouseId: this.from.warehouse_id,
                        endDate: this.from.dateEnd,
                    }
                })
                    .then(response => {
                    $('#modal-info').modal('show');
                this.infoInvestigation=response.data;
                this.infoInvestigation.investigationStock.map(data => {
                    data.status_new = '';
            });
            })
            .catch(error => {
                    alert(`${error.response.data}`);
            })
            .then(() => {
                    this.isLoading = false;
            });
            },

        }
    });
    searchForm.$validator.dictionary.locale = 'es';

</script>

<style>
    .border-none #date_start, #date_end{
        border:none!important;
        width: 100%!important;
        color: #555;
        size: 14px;
        font-family: inherit;
    }
</style>