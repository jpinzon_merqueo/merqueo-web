@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="searchForm">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="city_id">Ciudad</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Ciudad"
                                name="city_id" id="city_id" class="form-control" v-model="from.city_id"
                                @change="getWarehousesFrom" :disabled="isLoading">
                            <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.city_id')">@{{ errors.first('from.city_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Bodega</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Bodega"  @change="clearProduct()"
                                name="warehouse_id" id="warehouse_id" class="form-control" v-model="from.warehouse_id"
                                :disabled="isLoading">
                            <option value="">-Selecciona-</option>
                            <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{
                                warehouse.warehouse }}
                            </option>
                        </select>

                        <span style="color: #d73925" v-show="errors.has('from.warehouse_id')">@{{ errors.first('from.warehouse_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="product_info">Producto</label>
                        <input v-validate="'required'" data-vv-scope="from" data-vv-as="Producto"
                               name="product_info" id="product_info" @keyup.enter="getProductAjax()"
                               class="form-control" v-model="from.product_info" :disabled="isLoading"
                               placeholder="Referencia" ref="reference">
                        <span style="color: #d73925" v-show="errors.has('from.product_info')">@{{ errors.first('from.product_info') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <br/>
                        <button class="btn btn-primary" @click="getProductAjax()" :disabled="isLoading">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body" v-if="product!=null">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <h4>Producto: @{{ product.product.name }}</h4>
                                <h4>Referencia: @{{ product.product.reference }}</h4>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <img :src="product.product.image_medium_url" alt=""
                                     class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row" v-if="product!=null">
                            <div class="col-lg-6 col-md-12">
                                <h4>Alistamiento</h4>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th>Picking</th>
                                    <th>Posición</th>
                                    <th>Altura</th>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(data) in product.store_product_warehouses" class="text-center" v-show="product.store_product_warehouses.length>0">
                                        <td>@{{ data.picking_stock}}</td>
                                        <td>@{{ data.storage_position}}</td>
                                        <td>@{{ data.storage_height_position}}</td>
                                    </tr>

                                    <tr v-show="product.store_product_warehouses.length< 1">
                                        <td class="text-center" colspan="3">No se encontraron productos</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-12" v-if="">
                                <h4>Almacenamiento</h4>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th>Cantidad</th>
                                    <th>Posición</th>
                                    <th>Altura</th>
                                    <th>Fecha de vencimiento</th>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(data) in product.warehouse_storages" class="text-center" v-show="product.warehouse_storages.length>0">
                                        <td>@{{ data.quantity}}</td>
                                        <td>@{{ data.position}}</td>
                                        <td>@{{ data.position_height}}</td>
                                        <td>@{{ data.expiration_date}}</td>
                                    </tr>

                                    <tr v-show="product.warehouse_storages.length< 1">
                                        <td class="text-center" colspan="4">No se encontraron productos</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    @include('admin.inventory.product.js.index-js')
@endsection