@if (!Request::ajax())
    @extends('admin.layout')
    @section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <style type="text/css">
		.item-supply-task{
			border: 1px solid #ccc;
			border-radius: 5px;
			padding-bottom: 10px;
			padding-top: 10px;
		}

		.item-supply-task-content{
			font-size: 16px;
		}
		.selectLote{
			background-color: #d9f1e0;
		}
		#picking_supplier_list_tasks, #picking_supplier_list_task_manually {
		    padding: 10px;
		}
		.item-supply-task{
			padding-right: 0px;
			padding-left: 0px;
		}
		.item-supply-task-image{
			padding: 0px !important;
		}
		.item-supply-task-image img{
			width: 100%;
			height: auto;
		}
		.input_quantity_storaged{
			font-size: 23px;
			height: 41px;
			width: 80%;
		}
		.storaged_checkbox_label{
			font-weight: normal !important;
		}
		input[type="checkbox"] {
		    display:none;
		}
		input[type="checkbox"] + label span {
		    display:inline-block;
		    width:24px;
		    height:24px;
		    font-weight: normal;
		    background:url('{{ asset_url() }}/img/checkbox-empty.png') left top no-repeat;
		    cursor:pointer;
		}
		input[type="checkbox"]:checked + label span {
		    background:url('{{ asset_url() }}/img/checkbox_checked.png') left top no-repeat;
		}
		
		@media(min-width: 768px){
			#manually-suppling-search-product{
				margin-top: 25px;
			}
			.item-supply-task-image img{
				width: 50% !important;
			}
		}
    	@media(max-width: 768px){
    		.btn-item-task{
    			padding-top: 20px;
    		}
    		.item-supply-task{
    			margin-top: 10px;
    		}
    	}
    </style>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">
	    @if(Session::has('message') )
	        @if(Session::get('type') == 'success')
	            <div class="alert alert-success alert-dismissable">
	                <i class="fa fa-check"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <b>Información</b> {{ Session::get('message') }}
	            </div>
	        @else
	            <div class="alert alert-danger alert-dismissable">
	                <i class="fa fa-ban"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <b>Alerta!</b> {{ Session::get('message') }}
	            </div>
	        @endif
	    @endif
	    <div class="row">
	    	<div class="col-xs-12 col-md-12">
	    		<div class="box box-primary">
	    			<div class="box-body" id="form_search_start_supplier">
	    				<div class="row error-search">
				            <div class="col-xs-12">
				                <div class="alert alert-danger unseen"></div>
				                <div class="alert alert-success unseen"></div>
				            </div>
				        </div>
	    				<form id="picking-supplier-form" action="{{ route('adminInventoryPickingSupplier.getWarehousesAjax') }}" autocomplete="off" >
	    					<div class="row">
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label>Ciudad </label>
			    						<select name="city_id" id="city_id" class="form-control">
			    							<option value="">Selecciona</option>
			    							@foreach($cities AS $city)
			    							<option value="{{$city->id}}">{{$city->city}}</option>
			    							@endforeach
			    						</select>
			    					</div>
	    						</div>
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label>Bodega </label>
			    						<select name="warehouse_id" id="warehouse_id" class="form-control">
			    							<option value="">Selecciona</option>
			    						</select>
			    					</div>
	    						</div>
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label>Tipo de almacenamiento </label>
			    						<select name="type_storage" id="type_storage" class="form-control">
			    							<option value="">Selecciona</option>
			    							@foreach($types AS $type)
			    							<option value="{{$type}}">{{$type}}</option>
			    							@endforeach
			    						</select>
			    					</div>
	    						</div>
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label> </label>
			    						<button class="btn btn-primary btn-block" id="start-suppling-picking">Comenzar alistamiento</button>
			    					</div>
	    						</div>
	    					</div>
	    				</form>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-md-12 col-xs-12" id="table-supplier-manually">
	    		<div class="box box-primary">
					<div class="box-header ui-sortable-handle" style="cursor: move;">
						<!-- <i class="fa fa-tasks"></i> -->
						<h3 class="box-title">Surtidor manual</h3>
						<div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
							<div class="btn-group" data-toggle="btn-toggle">
								<button type="button" class="btn btn-primary btn-sm" id="manually-suppling">Surtir manualmente</i>
								</button>
							
							</div>
						</div>
					</div>
	    			<div class="box-body unseen" id="form-suppling-manually">
	    				<div class="row error-search">
				            <div class="col-xs-12">
				                <div class="alert alert-danger unseen"></div>
				                <div class="alert alert-success unseen"></div>
				            </div>
				        </div>
	    				<div class="row">
	    					<div class="col-md-5">
	    						<div class="form-group">
									<label for="">Producto</label>
									<input type="text" class="form-control" id="search_product" name="search_product" placeholder="Referencia, Nombre">
								</div>
	    					</div>
	    					<div class="col-md-1 col-xs-12">
	    						<div class="form-group">
									<button type="button" class="btn btn-primary btn-sm btn-block" id="manually-suppling-search-product" >Buscar</i>
								</button>
								</div>
	    					</div>
	    					<div class="col-md-6 col-xs-12">
	    					</div>
	    				</div>
    					<div class="row" id="picking_supplier_list_task_manually">
    						<div class="col-md-12 text-center emptydiv">No hay productos</div>
    					</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-md-12 col-xs-12" id="table-supplier">
	    		<div class="box box-primary">
	    			<div class="box-header ui-sortable-handle" style="cursor: move;">
						<!-- <i class="fa fa-tasks"></i> -->
						<h3 class="box-title">Productos a surtir</h3>
						
					</div>
	    			<div class="box-body" id="table.list-task">
	    				<div class="row error-search">
				            <div class="col-xs-12">
				                <div class="alert alert-danger unseen"></div>
				                <div class="alert alert-success unseen"></div>
				            </div>
				        </div>
	    				<div id="picking_supplier_list_tasks" class="row">
	    					<div class="col-md-12 text-center emptydiv">No hay tareas de surtido disponibles</div>
	    				</div>
	    					
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
		<!-- Modal -->
		<div class="modal fade" id="modal-storaged-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Unidades en bodega</h4>
					</div>
					<div class="modal-body" id="storaged-products-list">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<button type="button" class="btn btn-primary" id="storaged-products-btn">Surtir</button>
					</div>
				</div>
			</div>
		</div>


	</section>
	<script type="text/javascript">
		var add_overlay = function(id_element, colspan){
			var overlay ='<div class="col-xs-12 overlay text-center" style="width:100%;">';
            	overlay +='<img src="{{ asset_url() }}/img/loading.gif" />';
            overlay +='</div>';
            $(id_element).html(overlay);
		}
		var rmv_overlay = function(id_element){
            $(id_element).find('.overlay').remove();
		}
		var pickingSuplier = (function(){
        	'use strict';
            function pickingSuplier( args ){
                if( !( this instanceof pickingSuplier ) ){
                    return new pickingSuplier( args );
                }
                this.automatic_task = null;
                this.url_warehouses_ajax = '{{ route("adminInventoryPickingSupplier.getWarehousesAjax") }}';
                this.url_picking_task_ajax = '{{route("adminInventoryPickingSupplier.getSupplierPickingTasksAjax")}}';
                this.url_update_status = '{{route("adminInventoryPickingSupplier.updateTaskAjax")}}';
                this.url_storaged_products = '{{route("adminInventoryPickingSupplier.getStoragedProductsAjax")}}';
                this.url_save_supplied = '{{route("adminInventoryPickingSupplier.saveSupplied")}}';
                this.url_search_product = '{{route("adminInventoryPickingSupplier.searchProduct")}}';
                this.url_set_task = '{{route("adminInventoryPickingSupplier.setTask")}}';
                this.url_del_task = '{{route("adminInventoryPickingSupplier.delTask")}}';
                this.url_close_task = '{{route("adminInventoryPickingSupplier.closeTask")}}';
                this.url_update_all_task_ajax = '{{route("adminInventoryPickingSupplier.updateAllTasks")}}';
                this.bindActions();
            }

            pickingSuplier.prototype.bindActions = function(){
                var self = this;
                $.ajaxSetup({ cache: false });
				$('#city_id').on('change', function(event){
					if( $(this).val() != "" ){
						self.ajax_select_warehouse();
					}
				});

				$('#start-suppling-picking').on('click', function(event){
					event.preventDefault();
					if($("#picking-supplier-form").valid()){
						
						self.ajax_start_supply();
					}
				});

				$("#picking-supplier-form").validate({
					rules: {
						city_id: "required",
						warehouse_id: "required",
						type_storage: "required",
					},
					messages: {
						city_id: "Debe selecionar una Ciudad",
						warehouse_id: "Debe selecionar una bodega",
						type_storage: "Debe selecionar el tipo de almacenaje",
					}
				});

				$("#storaged-products-btn").on('click', function(){
					$("#storaged-products-btn").prop("disabled",true);
					self.save_supply();
				});
				
				$('#manually-suppling').on('click', function(){
					if($('#form-suppling-manually').is(":visible")){
						$('#form-suppling-manually').fadeOut();
					}else{
						$('#form-suppling-manually').fadeIn();
					}
				});

				$('#manually-suppling-search-product').on('click', function(event){
					event.preventDefault();
					self.ajax_search_products();
				});


            }

            pickingSuplier.prototype.validate_check_storage= function(){
            	var valid = true;
            	var count_checked = 0;
            	$.each($('.storaged_checkbox'), function(index, checkbox){
            		if($(checkbox).is(':checked')){
            			count_checked++;
            			var id = $(checkbox).val();
            			var quantity_to_suppling = parseInt($('#quantity_to_suppling_'+id).val());
            			var quantity_storaged = parseInt($('#quantity_storaged_'+id).val());
            			var quantity = parseInt($('#quantity_'+id).val());

            			if( quantity_to_suppling > quantity_storaged ){
            				$('#storaged-products-list').find('.error-search .alert-danger').html('La cantidad seleccionada es mayor a la cantidad disponible en el lote.').fadeIn().delay(5000).fadeOut();
            				valid = false;
            			}

            			/*if( quantity_to_suppling < quantity ){
            				$('#storaged-products-list').find('.error-search .alert-danger').html('La cantidad seleccionada es menor a la cantidad necesaria en picking.').fadeIn().delay(5000).fadeOut();
            				valid = false;
            			}*/
            		}
            	});

            	if(count_checked <= 0){
            		$('#storaged-products-list').find('.error-search .alert-danger').html('Debe seleccionar por lo menos una de los lotes.').fadeIn().delay(5000).fadeOut();
            		return false;
            	}
            	return valid;
            }

            pickingSuplier.prototype.save_supply = function(){
            	var self = this;

            	let quantity_to_supply = $('[name="quantity_to_suppling"]').val();
            	if (quantity_to_supply < 1) {
            	    alert('Por favor revise las cantidades a suplir ya que no pueden ser cero o negativas.');
            	    return;
				}

            	if(self.validate_check_storage()){
            		var data = $('#supplier_picking_form').serialize();
            		$.ajax({
            			type : 'POST',
            			url  : self.url_save_supplied,
            			data : $('#supplier_picking_form').serialize(),
            			error : function(xhr){
            				console.log(xhr)
            				alert('Ocurrio un error al actualizar los datos.')
            				$("#storaged-products-btn").prop("disabled", false);
            			},
            			success : function(xhr){
            				if(xhr.status){
            					$('#form_search_start_supplier').find('.error-search .alert-success').html(xhr.message).fadeIn().delay(5000).fadeOut();
            					alert(xhr.message);
            					$("#storaged-products-btn").prop("disabled", false);
            					if(xhr.result == 'finished'){
            						$('#modal-storaged-products').modal('hide');
	            					clearTimeout(self.automatic_task);
									self.ajax_start_supply();
            					}else{
            						self.supplier_picking(xhr.result, 'refill');
            						clearTimeout(self.automatic_task);
									self.ajax_start_supply();
            					}
            				}else{
            					alert(xhr.message)
            					$('#storaged-products-list').find('.error-search .alert-danger').html(xhr.message).fadeIn().delay(5000).fadeOut();
            					$("#storaged-products-btn").prop("disabled", false);
            				}
            			}
            		})
            	}else{
            		$("#storaged-products-btn").prop("disabled", false);
            	}
            }

            pickingSuplier.prototype.ajax_select_warehouse = function(){
            	var self = this;
            	$.ajax({
            		type : 'POST',
            		url  : self.url_warehouses_ajax,
            		data : { city_id : $('#city_id').val() },
            		dataType : 'json',
            		success : function( request ){
            			if(request.status == true){
            				$('#warehouse_id').html(request.result);
            			}
            		}
            	});
            }

            pickingSuplier.prototype.action_checkbox = function(){
            	var self = this;
            	$('.storaged_checkbox').on('change', function(){
            		
					var qty = $(this).data('qtytosupply');
					var id = $(this).data('storeproductid');
            		if($(this).is(':checked') ){
            			if( parseInt($(this).parent().find('.input_quantity_storaged').val()) > 0){
							$(this).parent().parent().addClass('selectLote');
            			}else{
            				$(this).prop('checked', false);
            				$(this).trigger('change');
            			}
					}else{
						$(this).parent().parent().removeClass('selectLote');
					}
					//self.update_field_qty_to_suplly(qty, id);
            	})
            	
            }
            pickingSuplier.prototype.update_field_qty_to_suplly = function(qty, id){
            	var qty_selected = 0;
            	var current_qty = 0;
            	$.each($('.storaged_checkbox'), function(index, check){
            		if($(check).is(':checked')){
            			qty_selected+= parseInt($(check).parent().find('.input_quantity_storaged').val())
            		}
            	});
            	$.each($('.storaged_checkbox'), function(index, check){
				    if(!$(check).is(':checked')){
				        qty = qty - qty_selected;
						current_qty = $(check).parent().find('.input_quantity_storaged').val();
				        if( qty > 0 && current_qty > qty ){
				            $(check).parent().find('.input_quantity_storaged').val(qty)
				        }else if(qty == 0){
				            $(check).parent().find('.input_quantity_storaged').val(0)
				        }else{
							$(check).parent().find('.input_quantity_storaged').val(current_qty)
				        }
				    }
				});
            }

            pickingSuplier.prototype.ajax_search_products = function(){
            	var self = this;
            	add_overlay('#picking_supplier_list_task_manually');
            	if($('#search_product').val()!="" && $('#warehouse_id').val()!="" && $('#type_storage').val() != ""){
	            	$.ajax({
	            		type : 'POST',
	            		url  : self.url_search_product,
	            		data : { search_product : $('#search_product').val(), warehouse_id : $('#warehouse_id').val(), type_storage : $('#type_storage').val() },
	            		dataType : 'json',
	            		success : function( request ){
	            			if(request.status == true){
	            				$('#picking_supplier_list_task_manually').html(request.result);
	            			}else{
	            				$('#form-suppling-manually').find('.error-search .alert-danger').html(request.message).fadeIn().delay(5000).fadeOut();
	            			}
	            			rmv_overlay('#picking_supplier_list_task_manually');
	            		}
	            	});
            	}else{
            		if($('#warehouse_id').val()==""){
            			$('#form-suppling-manually').find('.error-search .alert-danger').html("Debe seleccionar una bodega.").fadeIn().delay(5000).fadeOut();
            		}else if($('#type_storage').val()==""){
            			$('#form-suppling-manually').find('.error-search .alert-danger').html("Debe seleccionar un tipo de almacenamiento.").fadeIn().delay(5000).fadeOut();
            		}else if($('#search_product').val()==""){
            			$('#form-suppling-manually').find('.error-search .alert-danger').html("El campo de referencia o nombre de producto no puede estar vacio.").fadeIn().delay(5000).fadeOut();
            		}
            		rmv_overlay('#picking_supplier_list_task_manually');
            	}
            }

            pickingSuplier.prototype.ajax_start_supply = function(){
            	var self = this;
            	add_overlay('#picking_supplier_list_tasks', 9);
            	clearTimeout(self.automatic_task);
            	$('#start-suppling-picking').prop('disabled', true);

            	/*$.ajax({
            		type : 'POST',
            		url  : self.url_update_all_task_ajax,
            		data : $("#picking-supplier-form").serialize(),
            		success : function( request ){}
            	});*/

            	$.ajax({
            		type : 'POST',
            		url  : self.url_picking_task_ajax,
            		data : $("#picking-supplier-form").serialize(),
            		success : function( request ){
            			if(request.status == true){
            				$('#picking_supplier_list_tasks').html(request.result);
            				self.automatic_task = setTimeout(function(){
            					//console.log(self.url_picking_task_ajax)
            					self.ajax_start_supply();
            				}, 60000);
            				//console.log(request)
            			}else{
            				$('#form-suppling-manually').find('.error-search .alert-danger').html(request.message).fadeIn().delay(5000).fadeOut();
            				$('#picking_supplier_list_tasks').html('<div  class="text-center">No hay tareas de surtido disponibles</div>');
            				rmv_overlay('#picking_supplier_list_tasks');
            			}
            			$('#start-suppling-picking').prop('disabled', false);
            		}
            	}).fail(function($data){
            		$('#start-suppling-picking').prop('disabled', false);
            	});
            }

            pickingSuplier.prototype.update_status = function(id, status){
            	var self = this;
            	clearTimeout(self.automatic_task);
            	$.ajax({
            		type : 'POST',
            		url  : self.url_update_status,
            		data : { id : id, status : status },
            		success : function(){
            			self.ajax_start_supply();
            		}
            	})
            }

            pickingSuplier.prototype.supplier_picking = function(id, action){
            	var self = this;
            	if(action == 'refill'){
            		self.refil_storage_modal(id);
            	}else{
            		self.show_modal('storaged-products', id );
            	}
            }

            pickingSuplier.prototype.supplier_picking_delete = function(id){
            	var self = this;
            	if(confirm('¿Desea eliminar esta tarea de alistamiento?')){
	            	clearTimeout(self.automatic_task);
	            	$.ajax({
	            		type : 'POST',
	            		url  : self.url_del_task,
	            		data : { id : id },
	            		success : function(){
	            			self.ajax_start_supply();
	            		}
	            	})
            	}
            }

            pickingSuplier.prototype.supplier_picking_close = function(id){
            	var self = this;
            	if(confirm('¿Desea Cerrar esta tarea de alistamiento?')){
	            	clearTimeout(self.automatic_task);
	            	$.ajax({
	            		type : 'POST',
	            		url  : this.url_close_task,
	            		data : { id : id },
	            		success : function(){
	            			self.ajax_start_supply();
	            		}
	            	})
            	}
            }

            pickingSuplier.prototype.supplier_picking_manually = function(store_product_id, warehouse_id, admin_id){
            	var self = this;
            	clearTimeout(self.automatic_task);
            	$('#picking_supplier_list_task_manually').html('');
            	add_overlay('#picking_supplier_list_task_manually');
            	var get_task = false;
            	if( admin_id!=0){
            		if(confirm('Esta tarea esta asignada a otro usuario, desea tomarla de todas formas.')){
            			get_task = true;
            		}else{
            			rmv_overlay('#picking_supplier_list_task_manually');
            			return false;
            		}
            	}
            	$.ajax({
    				type : 'POST',
    				url  : self.url_set_task,
    				data : { store_product_id : store_product_id, warehouse_id : warehouse_id, get_task : get_task, type_storage : $('#type_storage').val() },
    				success : function( xhr ){
    					if( xhr.status == true){
    						$('#picking_supplier_list_task_manually').html('<div  class="text-center">No hay productos</div>');
    						$('#table.list-task').find('.error-search .alert-success').html(xhr.message).fadeIn().delay(5000).fadeOut();
    						$('#start-suppling-picking').trigger('click');
    						console.log(xhr.status)
    						$('#form-suppling-manually').delay(5000).fadeOut();
    					}else{
    						alert(xhr.message);
    						$('#table.list-task').find('.error-search .alert-danger').html(xhr.message).fadeIn().delay(5000).fadeOut();
    						//$('#form-suppling-manually').delay(5000).fadeOut();
    					}
    				}
    			});
    			self.ajax_start_supply();
    			rmv_overlay('#picking_supplier_list_task_manually');
            }
            pickingSuplier.prototype.refil_storage_modal = function(id_task){
            	var self = this;
            	console.log(id_task);
            	$.ajax({
    				type : 'POST',
    				url  : self.url_storaged_products,
    				data : { id : id_task },
    				beforeSend : function(data){
    					var overlay ='<div class="col-xs-12 overlay text-center" style="width:100%;">';
			            	overlay +='<img src="{{ asset_url() }}/img/loading.gif" />';
			            overlay +='</div>';
    					$('#storaged-products-list').html(overlay);
    				},
    				success : function( xhr ){
    					if( xhr.status == true){
    						$('#storaged-products-list').html(xhr.result);
    						self.action_checkbox();
    					}
    				}
    			});
            }

            pickingSuplier.prototype.show_modal = function(action, param){
            	var self = this;
            	if(action=='storaged-products'){
        			self.refil_storage_modal(param);
            	}
            	
            	$('#modal-'+action).modal('show');
            }

        
    		return pickingSuplier;
    	}());
    
    	var picking_supplier = new pickingSuplier;
		
	</script>
	@endsection
@else
	@section('tasks')
		@if(isset($tasks) && count($tasks))
			@foreach($tasks as $task)
				<div class="col-xs-12 item-supply-task">
					<div class="col-md-3 col-xs-3 text-center item-supply-task-image">
						<img src="{{$task->image}}" class="image-responsive" />
					</div>
					<div class="col-md-7 col-xs-9 item-supply-task-content">
						<label>Nombre:</label> {{$task->name}}<br>
						<label>Referencia:</label> {{$task->reference}}<br>
						<label>Posición:</label> {{$task->picking_position}} {{$task->picking_position_height}}<br>
						<label>Stock:</label> {{$task->minimum_picking_stock}} mínimo - {{$task->maximum_picking_stock}} máximo<br>
						<label>Unidades actuales:</label> {{$task->quantity_picking}} <br>
						<label>Unidades faltantes:</label> {{$task->quantity_to_suppling}} <br>
						<label>Estado:</label>
						@if($task->status == 'Pending')
							<span class="badge bg-yellow">Pendiente</span>
						@endif
						@if($task->status == 'In Process')
							<span class="badge bg-green">En proceso</span>
						@endif<br>
					</div>
					<div class="col-md-2 col-xs-12 btn-item-task">
						@if($task->status == 'In Process')
	                	<a href="#" class="btn-primary btn btn-block" onclick="picking_supplier.supplier_picking({{$task->id}}, 'Success')">Surtir</a>
	                	@endif
	                	@if($task->quantity_supplied == 0)
	                	<a href="#" class="btn-danger btn btn-block" onclick="picking_supplier.supplier_picking_delete({{$task->id}})">Eliminar</a>
	                	@endif
	                	@if($task->quantity_supplied > 0)
	                	<a href="#" class="btn-danger btn btn-block" onclick="picking_supplier.supplier_picking_close({{$task->id}})">Cerrar Tarea</a>
	                	@endif
					</div>
				</div>
			@endforeach
		@else
			<div class="col-xs-12 col-md-12 text-center emptydiv">No hay tareas disponibles</div>
		@endif
	
	@endsection

	@section('storaged-products')
	@if(isset($storaged_products))
		<div class="row error-search">
            <div class="col-xs-12">
                <div class="alert alert-danger unseen"></div>
                <div class="alert alert-success unseen"></div>
            </div>
        </div>
		<form id="supplier_picking_form">
			<input type="hidden" name="id_picking_supplier" value="{{$id_picking_supplier}}">
			<div class="row">
				@if(count($storaged_products) > 0)
					@foreach($storaged_products as $storaged_product)

						<div class="col-xs-12 item-supply-task">
							<div class="col-md-3 col-xs-3 text-center item-supply-task-image">
								<img src="{{$storaged_product->image}}" class="image-responsive" />
							</div>
							<div class="col-md-7 col-xs-9 item-supply-task-content">
								<label>Nombre:</label> {{$storaged_product->name}}<br>
								<label>Referencia:</label> {{$storaged_product->reference}}<br>
								<label>Posición:</label> {{$storaged_product->position}}{{$storaged_product->position_height}} <br>
								@if($storaged_product->expiration_date != '0000-00-00' && !is_null($storaged_product->expiration_date))
								<label>Fecha de vencimiento:</label> {{date('d M Y', strtotime($storaged_product->expiration_date))}}<br>
								@endif
								<label>Unidades almacenadas:</label> {{$storaged_product->quantity}}<br>
								<label>Unidades faltantes:</label> {{$storaged_product->quantity_to_suppling}}<br>
								<label>Unidades a surtir:</label> 
								<input type="hidden" name="quantity_storaged[]" id="quantity_storaged_{{$storaged_product->id}}" value="{{$storaged_product->quantity}}">
								<input type="hidden" name="quantity[]" id="quantity_{{$storaged_product->id}}" value="{{$storaged_product->quantity_to_suppling}}">
								<input type="number" class="input_quantity_storaged input_quantity_storaged_{{$storaged_product->store_product_id}}" name="quantity_to_suppling" id="quantity_to_suppling_{{$storaged_product->id}}" value="{{ $storaged_product->quantity_to_suppling > $storaged_product->quantity ? $storaged_product->quantity : $storaged_product->quantity_to_suppling}}">
								<br>
								<br>
								<input type="checkbox" id="chk-{{$storaged_product->id}}" class="storaged_checkbox" name="storage" data-qtytosupply="{{$storaged_product->quantity_to_suppling}}" data-storeproductid="{{$storaged_product->store_product_id}}" value="{{$storaged_product->id}}">
								<label class="storaged_checkbox_label" for="chk-{{$storaged_product->id}}"><span></span> Selecciona este lote</label>
							</div>
							<div class="col-md-2 col-xs-12 btn-item-task">
							</div>
						</div>
					@endforeach
				@else
					<div class="col-xs-12 text-center emptydiv">No hay unidades disponibles en bodega para este producto.</div>
				@endif
			</div>
		</form>
	@endif
	@endsection

	@section('search-products')
		@if(isset($searched_products) && count($searched_products))
			@foreach($searched_products as $searched_product)

			<div class="col-xs-12 item-supply-task">
				<div class="col-md-3 col-xs-3 text-center item-supply-task-image">
					<img src="{{$searched_product->image}}" class="image-responsive" />
				</div>
				<div class="col-md-7 col-xs-9 item-supply-task-content">
					<label>Nombre:</label> {{$searched_product->name}}<br>
					<label>Referencia:</label> {{$searched_product->reference}}<br>
					<label>Posición:</label> {{$searched_product->picking_position}} {{$searched_product->picking_position_height}}<br>
					<label>Stock:</label> {{$searched_product->minimum_picking_stock}} mínimo - {{$searched_product->maximum_picking_stock}} máximo<br>
					@if($searched_product->admin_fullname != '')
					<label>Usuario:</label> {{$searched_product->admin_fullname}}<br />
					@endif
					<label>Unidades actuales:</label> {{$searched_product->quantity_picking}} <br>
					<label>Unidades faltantes:</label> {{$searched_product->quantity_to_suppling}} <br>
				</div>
				<div class="col-md-2 col-xs-12 btn-item-task">
					<a href="#" class="btn-primary btn btn-block" onclick="picking_supplier.supplier_picking_manually({{$searched_product->id}}, {{$searched_product->warehouse_id}}, {{$searched_product->admin_id}})">Añadir tarea</a>
				</div>
				
			</div>
			

			@endforeach
		@else
			<div class="col-xs-12 col-md-12 text-center emptydiv">No hay productoss</div>
		@endif
	
	@endsection
@endif