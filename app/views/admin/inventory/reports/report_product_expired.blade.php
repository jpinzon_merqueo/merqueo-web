@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="searchForm">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                        <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                                <label for="city_id">Ciudad</label>
                                <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Ciudad"
                                        name="city_id" id="city_id" class="form-control" v-model="from.city_id"
                                        @change="getWarehousesFrom" :disabled="isLoading">
                                    <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                                </select>
                                <span style="color: #d73925" v-show="errors.has('from.city_id')">@{{ errors.first('from.city_id') }}</span>
                            </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Bodega</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Bodega"
                                @change="clearDataTable()"
                                name="warehouse_id" id="warehouse_id" class="form-control" v-model="from.warehouse_id"
                                :disabled="isLoading">
                            <option value="">-Selecciona-</option>
                            <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{
                                warehouse.warehouse }}
                            </option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.warehouse_id')">@{{ errors.first('from.warehouse_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 offset-lg-3 form-group pull-right">
                        <br/>
                        <button class="btn btn-primary" @click="getReport()" :disabled="isLoading">Buscar</button>
                    </div>  
                </div> 
            </div>
        </div>
        <div class="box-body">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="panel-body">
                            <table class="table table-bordered table-striped"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th
                                            class="text-center"
                                            v-for="(column) in columns"
                                            v-text="column"
                                    ></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(data) in datatable" class="text-center" v-if="datatable.length>0">
                                    <td>@{{ data.store_product.id}}</td>
                                    <td>
                                        <img :src="data.store_product.product.image_small_url" height="50px">
                                    </td>
                                    <td>@{{ data.store_product.product.name}}</td>
                                    <td>@{{ data.store_product.product.reference}}</td>
                                    <td>@{{data.expiration_date}}</td>
                                    <td>@{{data.position}} @{{data.position_height}}</td>
                                </tr>
                                <tr v-if="datatable.length==0" class="text-center">
                                    <td colspan="10">No hay datos</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.inventory.reports.js.report_product_expired-js')
@endsection