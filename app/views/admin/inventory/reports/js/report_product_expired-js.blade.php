<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.1.0-beta.11/dist/vee-validate.min.js"></script>
<script src="https://unpkg.com/vuejs-datepicker"></script>

<script>
    const messages = {
            _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
        if (max) {
            return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
        }

        return `El largo del campo ${field} debe ser ${length}.`;
    },
    max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field, [excluded]) => `El campo ${field} debe ser diferente a ${excluded}.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });

    let searchForm = new Vue({
        el: '#searchForm',
        components: {
        },
        data: {
            from:{
                city_id: {{ Session::get('admin_city_id') }},
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
            },
            cities: {{ $cities->toJson() }},
            warehouses: {{ $warehouses->toJson() }},
            isLoading: false,
            datatable:[],
            columns: [
                'Id','Imagen','Producto','Referencia','Fecha expiración','Posición almacenamiento'
            ],
            infoInvestigation: null
        },

        created() {
            let self = this
        },
        methods: {
            clearDataModal : function(data){
                data.typification_new = '';
                return;
            },
            clearDataTable : function () {
                this.datatable=[];
            },
            getWarehousesFrom: function (e) {
                this.isLoading = true;
                this.from.warehouse_id = '';
                this.warehouses = [];
                this.datatable=[];
                axios.get('{{ route('adminInvestigationStock.getWarehousesAjax') }}', {
                    params: {
                        cityId: this.from.city_id,
                    }
                })
                    .then(response => {
                    this.warehouses = response.data;
            })
            .catch(error => {
                    alert(`${error.response.data}`);
            })
            .then(() => {
                    this.isLoading = false;
            });
            },
            getReport: function (e) {
                this.$validator.validate('from.*').then(result => {
                    if (!result) {
                    return false;
                }else{
                    this.isLoading = true;
                    this.datatable =[]
                    axios.get('{{ route('adminStockReport.reportProductExpiredAjax') }}', {
                        params: {
                            warehouseId: this.from.warehouse_id
                        }
                    })
                        .then(response => {
                        this.datatable=response.data;
                })
                .catch(error => {
                        alert(`${error.response.data}`);
                })
                .then(() => {
                        this.isLoading = false;
                });
                }
            })
            }
        }
    });
    searchForm.$validator.dictionary.locale = 'es';

</script>