@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <fieldset>
                            <legend>Reportes de Inventario</legend>
                            <ul>
                                <li>
                                    <a href="{{ route('adminStockReport.report_product_expired') }}">
                                        Productos próximos a vencer
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('adminStockReport.report-product-disabled') }}">
                                        Productos apagados con stock
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('adminStockReport.report-product-enabled') }}">
                                        Productos activos con stock
                                    </a>
                                </li>
                            </ul>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection