@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script>
        var web_url_ajax = "{{ route('adminShrinkages.index') }}";
    </script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a href="javascript:;" class="shrinkage_origin" onclick="$('#modal-shrinkage-origin').modal('show');">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Reportar Origen de Merma</button>
            </a>
            <a href="javascript:;" onclick="$('#modal-shrinkage-type').modal('show');">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Merma</button>
            </a>
        </span>
    </section>
    <section class="content">
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

    	<div class="row">
    	    <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                    	<div class="row">
                    	    <div class="col-xs-12">
                    	        <form id="search-form">
                        	        <table width="100%" class="admin-shrinkages">
                                        <tr><td colspan="7"><div class="unseen alert alert-danger form-has-errors"></div></td></tr>
                                        <tr>
                                            <td align="right"><label>Ciudad:</label>&nbsp;</td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right"><label>Estado:</label>&nbsp;</td>
                                            <td>
                                                <select id="status" name="status" class="form-control">
                                                    <option value="">Selecciona</option>
                                                    <option value="Pendiente">Pendiente</option>
                                                    <option value="Cerrada">Cerrada</option>
                                                </select>
                                            </td>
                                            <td align="right"><label>Buscar:</label>&nbsp;</td>
                                            <td><input type="text" placeholder="Nro. Merma" class="search form-control"></td>
                                            <td><button type="button" id="btn-search-updates" class="btn btn-primary">Buscar</button></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
        				</div>
        				<br>
                    	<div class="paging">
                            <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Ciudad</th>
                                        <th>Estado</th>
                                        <th>Unidades</th>
                                        <th>Fecha de creación</th>
                                        <th>Fecha de cierre</th>
                                        <th>Usuario</th>
                                        <th>Ver</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="10" align="center">No hay mermas.</td></tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 actualizaciones.</div>
                                </div>
                            </div>
                        </div>
    					<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Planning Date -->
    <div class="modal fade" id="modal-shrinkage-origin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Ingresa el origen de la merma</h4>
                </div>
                <form method="get">
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label>Ciudad</label>
                                <select class="form-control" id="cities">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <label>Bodega</label>
                                <select class="form-control" id="origin_warehouse_id" name="origin_warehouse_id">
                                    <option value="">Selecciona</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                                <div class="col-xs-8">
                                    <label for="shrinkage-origin">Origen de Merma</label>
                                    <select class="form-control" name="origin" id="origin">
                                        <option value="Alistamiento">Alistamiento</option>
                                        <option value="Almacenamiento">Almacenamiento</option>
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    <label style="color: #FFF;">|</label>
                                    <button type="button" class="btn btn-primary form-control create-shrinkage_origin">Continuar</button>
                                </div>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Planning Date -->
    <div class="modal fade" id="modal-shrinkage-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Selecciona el tipo de creación de merma</h4>
                </div>
                <form method="get">
                    <div class="modal-body">
                        <div class="row form-group">
                                <div class="col-xs-8">
                                    <label for="shrinkage-origin">Origen de Merma</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="Automatica">Automática</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    <label style="color: #FFF;">|</label>
                                    <button type="button" class="btn btn-primary form-control create-shrinkage">Continuar</button>
                                </div>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function() {
        var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
        paging(1, $('.search').val());

        $('.create-shrinkage').on('click', function(){
            if($("#type").val() == 'Automatica')
                window.location.href = "{{route('adminShrinkages.addShrinkage', ['type'=> 'Automatica'])}}";
            else
                window.location.href = "{{route('adminShrinkages.addShrinkage', ['type'=> 'Manual'])}}";

        });

        $('.create-shrinkage_origin').on('click', function(){
            var url_origin = "{{ route('adminShrinkages.Origin', ['warehouse_id'=>'not-found' , 'origin' => 'Alistamiento']) }}";
                url_origin = url_origin.replace('not-found', $('#origin_warehouse_id').val());
            var url = url_origin.replace(/Alistamiento/g, $('#origin').val());
            window.location.href = url;
        });

        $('#btn-search-updates').click(function(){
            var error = false
            var form = this;
            $('.form-has-errors').html(error).fadeOut();

            /*if ($('.search').is(':visible') && ($('.search').val().length == 0)) {
                $('.search').parent().addClass('has-error');
                error = 'Completa el campo de búsqueda.';
            }else $('.search').parent().removeClass('has-error');

            if(error) {
                $('.form-has-errors').html(error).fadeIn();
                return false;
            }*/
            paging(1, $('.search').val());
        });

        $('#cities').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#cities').val() },
                success : function( request ){
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#origin_warehouse_id').html(options);
                }
            });
        });
        $('#cities').trigger('change');
    });
    </script>
    @endsection
@else
    @section('content')
    <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Ciudad</th>
                <th>Estado</th>
                <th>Unidades reportadas</th>
                <th>Fecha de creación</th>
                <th>Fecha de cierre</th>
                <th>Usuario</th>
                <th>Ver</th>
            </tr>
        </thead>
        <tbody>
        @if (count($shrinkages))
            @foreach($shrinkages as $shrinkage)
            <tr>
                <td>{{ $shrinkage->id }}</td>
                <td>{{ $shrinkage->city }}</td>
                <td>
                @if($shrinkage->status == 'Pendiente')
                 <span class="badge bg-blue">Pendiente</span>
                @elseif($shrinkage->status == 'Cerrada')
                 <span class="badge bg-green">Cerrada</span>
                @endif
            </td>
                <td>{{ $shrinkage->quantity_reported }}</td>
                <td>{{ $shrinkage->created_date }}</td>
                <td>{{ $shrinkage->close_date }}</td>
                <td>{{ $shrinkage->fullname }}</td>
                <td>
                <div class="btn-group">
                   <a class="btn btn-xs btn-default" href="{{ route('adminShrinkages.details', ['id' => $shrinkage->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                </div>
            </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="14" align="center">No hay mermas</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($shrinkages) }} mermas.</div>
        </div>
    </div>
    @endif
@stop
