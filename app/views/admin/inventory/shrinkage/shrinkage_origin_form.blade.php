@if (!Request::ajax())

@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

<section class="content-header">
    <h1>
        {{ $title }} en {{ Input::get('origin') }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
    @if(Session::has('message'))
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif
    <div class="row">
        <div class="col-xs-12">
            <form id="form-shrinkage-origin" action="{{ route('adminShrinkages.OriginSave') }}" method="POST">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="form-group col-xs-4 product-storage-type">
                                <label>Tipo almacenamiento</label>
                                <select name="storage_type" id="storage_type" class="form-control">
                                    <option value="Seco" @if(Input::old('type') == 'Seco')) selected="selected" @endif>Seco</option>
                                    <option value="Frío" @if(Input::old('type') == 'Frío')) selected="selected" @endif>Frío</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-4 position-group">
                                <label>Posición en bodega</label>
                                <input type="text" name="position" id="position" class="form-control" placeholder="Posición" maxlength="3" value="{{Input::old('position')}}">
                            </div>
                            <div class="form-group col-xs-4 position-group">
                                <label>Posición en altura</label>
                                <input type="text" name="position_height" id="position_height" class="form-control" placeholder="Posición en altura" maxlength="1" value="{{Input::old('position_height')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-3">
                                <label>Producto</label>
                                <input type="text" class="form-control" name="store_product_name" id="store_product_name" placeholder="Producto" value="{{Input::old('store_product_name')}}">
                            </div>
                            <div class="form-group col-xs-1">
                                <label>&nbsp;</label><br>
                                <button type="button" id="btn-search-product" class="btn btn-primary">Buscar</button>
                            </div>
                            <div class="form-group col-xs-3">
                                <label>Cantidad de unidades</label>
                                <input type="text" name="quantity" id="quantity" class="form-control" placeholder="Cantidad de unidades" value="{{Input::old('quantity')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-3">
                                    <label>&nbsp;</label>
                                    <button type="button" id="btn-save-origin-shrinkage" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="origin" id="origin" value="@if($origin){{$origin}}@else{{Input::old('origin')}}@endif">
                <input type="hidden" name="origin_warehouse_id" id="origin_warehouse_id" value="@if($origin_warehouse_id){{$origin_warehouse_id}}@else{{Input::old('origin_warehouse_id')}}@endif">
                <input type="hidden" name="store_product_id" id="store_product_id" value="{{Input::old('storage_product_id')}}">
                <input type="hidden" name="id" id="warehouse_storage_id" value="{{Input::old('warehouse_storage_id')}}">
            </form>
        </div>
    </div>
</section>

<!-- Add products in position Modal -->
<div class="modal fade" id="modal-products-in-position" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar producto a reportar en merma<span class="position-label"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row search-product">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 products-position-request pre-scrollable">
                                <table id="product-position-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Referencia</th>
                                            <th>Nombre</th>
                                            <th>Unidad de medida</th>
                                            <th>Stock {{ Input::get('origin') }}</th>
                                            <th>Fecha de almacenamiento</th>
                                            <th>Fecha de vencimiento</th>
                                            <th>Posición</th>
                                            <th>Seleccionar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="14" align="center">No hay productos en la posición.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div align="center" class="products-position-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add products Modal -->
<div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar producto</h4>
            </div>
            <div class="modal-body">
                <div class="row search-product">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <table width="100%" class="modal-product-request-table">
                                    <tbody>
                                        <tr>
                                            <td align="right"><label>Buscar:</label>&nbsp;</td>
                                            <td>
                                                <input type="text" placeholder="Nombre, referencia" name="s" id="s" class="modal-search form-control">
                                            </td>
                                            <td colspan="2" align="left">
                                                <button type="button" id="btn-search-products" class="btn btn-primary btn-search-products">Buscar</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 products-request pre-scrollable">
                                <table id="products-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Referencia</th>
                                            <th>Nombre</th>
                                            <th>Unidad de medida</th>
                                            <th>Seleccionar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="10" align="center">No hay productos.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div align="center" class="products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function show_modal(action)
{
    $('#modal-' + action).modal('show');
}


function product_modal()
{
    if (!$('#position').val().length && !$('#position_height').val().length && $('#store_product_name').val() == ""){
        alert('Por favor ingresa la posición y altura.');
        return false;
    }

    show_modal('products-in-position');
    $('.products-position-request-loading').show();

    data = {
        warehouse_id: $('#origin_warehouse_id').val(),
        position: $('#position').val(),
        position_height: $('#position_height').val(),
        storage_type: $('#storage_type').val(),
        origin: $('#origin').val(),
        s:$('#store_product_name').val()
    };
    $.ajax({
        url: '{{ route('adminShrinkages.getProductsPositionAjax') }}',
        type: 'GET',
        dataType: 'html',
        data: data
    })
    .done(function(data) {
        $('.products-position-request tbody').html(data);
    })
    .fail(function(data) {
        $('.products-position-request tbody').html('Ocurrió un error al obtener los datos.');
    })
    .always(function(data) {
        $('.products-position-request-loading').hide();
        $('.position-label').html($('#position').val() + $('#position_height').val());
    });
}

$(document).ready(function(){
    if($('#origin').val() == 'Alistamiento'){
        $('.position-group').addClass('hide');
    }else{
        $('.position-group').removeClass('hide');
    }

    $('#btn-search-product').click(function() {
        if($('#origin').val() == 'Almacenamiento'){
            if (!$('#position').val().length && !$('#position_height').val().length && $('#store_product_name').val() == ""){
                alert('Por favor ingresa la posición y altura.');
                return false;
            }

            if ($('#position').val().length && !$('#position_height').val().length && $('#store_product_name').val() == ""){
                alert('Por favor ingresa la altura.');
                return false;
            }

            if (!$('#position').val().length && $('#position_height').val().length && $('#store_product_name').val() == ""){
                alert('Por favor ingresa la posición.');
                return false;
            }
            product_modal();
        }else{
            show_modal('products');
        }
    });

    $('#store_product_name').on('focus', function(){
        if($('#origin').val() == 'Alistamiento'){
            show_modal('products');
        }
    });

    $('#btn-search-products').on('click', function(){
        if ($('#s').val() == ""){
            alert('Por favor ingresa el nombre del producto.');
            return false;
        }

       $('.products-request-loading').show();
        data = {
            s: $('#s').val(),
            warehouse_id: $('#origin_warehouse_id').val(),
            storage_type: $('#storage_type').val()
        };
        $.ajax({
            url: '{{ route('adminShrinkages.searchProductsAjax') }}',
            type: 'GET',
            dataType: 'html',
            data: data
        })
        .done(function(data) {
            $('.products-request-loading').hide();
            $('.products-request tbody').html(data);
        })
        .fail(function(data) {
            $('.products-request-loading').hide();
            $('.products-request tbody').html('Ocurrió un error al obtener los datos.');
        })
        .always(function(data) {
            $('.products-request-loading').hide();
        });
    });

    $('body').on('click', '.product-position-selected', function(){
       $('#store_product_name').val($(this).data('name'));
       $('#warehouse_storage_id').val($(this).data('id'));
       $('#modal-products-in-position').modal('toggle');
    });

    $('body').on('click', '.product-selected', function(){
       $('#store_product_name').val($(this).data('name'));
       $('#store_product_id').val($(this).data('id'));
       $('#modal-products').modal('toggle');
    });

    $('#btn-save-origin-shrinkage').on('click', function(e){
        var validator = $('#form-shrinkage-origin').validate({
                            rules: {
                                quantity: {
                                    required: true,
                                    number: true,
                                    min: 1
                                },
                                store_product_name: {
                                    required: true
                                }
                            }
                        });

        if(validator.form()){
            if ($('#store_product_name').val() == ''){
                alert('Por favor verificar el producto con el buscador.');
                return false;
            }

            if (confirm('¿Estas seguro que deseas registrar este producto?'))
                $('#form-shrinkage-origin').submit();
        }
    });

});

</script>

@stop

@else
    @section('product-position-table')
        @if (isset($products))
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td><img src="{{$product->image_small_url}}" height="50"></td>
                        <td>{{$product->reference}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->product_quantity}} {{$product->unit}}</td>
                        <td align="center"><strong>{{$product->quantity}}</strong></td>
                        <td>{{ date("d M Y g:i a", strtotime($product->created_at)) }}</td>
                        <td>@if (!empty($product->expiration_date)){{ date("d M Y", strtotime($product->expiration_date)) }}@endif</td>
                        <td>{{$product->position}}{{$product->position_height}} </td>
                        <td>
                            <div class="btn-group">
                               <a class="btn btn-xs btn-default product-position-selected" href="javascript:;" data-id="{{ $product->id }}" data-quantity="{{ $product->quantity }}" data-name="{{ $product->name.' '.$product->product_quantity.' '.$product->unit }}">
                                   <span class="glyphicon glyphicon-folder-open"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="14" align="center">No hay productos en la posición.</td></tr>
            @endif
        @else
            <tr><td colspan="14" align="center">&nbsp;</td></tr>
        @endif
    @stop

    @section('product-table')
        @if (isset($products))
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td><img src="{{$product->image_small_url}}" height="50"></td>
                        <td>{{$product->reference}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->quantity}} {{$product->unit}}</td>
                        <td>
                            <div class="btn-group">
                               <a class="btn btn-xs btn-default product-selected" href="javascript:;" data-id="{{ $product->id }}" data-handle-expiration-date="{{ $product->handle_expiration_date }}" data-name="{{ $product->name.' '.$product->quantity.' '.$product->unit }}">
                                   <span class="glyphicon glyphicon-folder-open"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="7" align="center">Productos no encontrados.</td></tr>
            @endif
        @else
            <tr><td colspan="7" align="center">&nbsp;</td></tr>
        @endif
    @stop
@endif
