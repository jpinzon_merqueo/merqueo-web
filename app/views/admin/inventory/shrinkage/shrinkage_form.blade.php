@if (!Request::ajax())

@extends('admin.layout')

@section('content')
<script>
    var url_ajax = "{{ route('adminShrinkages.addShrinkage') }}";
</script>
<style type="text/css">
    #purchase-order-table th, #product-purchase-order-table th{ text-align: center!important; }
    .modal-dialog { width: 70%!important; }
</style>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Hecho!</b> {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alerta!</b> {{ Session::get('error') }}
</div>
@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form">
                                <table width="100%" class="purchase-order-table">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td>
                                            <select id="city_id" name="city_id" class="form-control">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Tienda:</label>&nbsp;</td>
                                        <td>
                                            <select id="store_id" name="store_id" class="form-control">
                                                <option value="">Selecciona</option>
                                                @if (count($stores))
                                                @foreach ($stores as $store)
                                                    @if ( $store->city_id == Session::get('admin_city_id') )
                                                        <option value="{{ $store->id }}" selected="selected">{{ $store->name }}</option>
                                                    @endif
                                                @endforeach
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                                <label>Bodega:</label>
                                        </td>
                                        <td>
                                            <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                        <td><input type="text" placeholder="Referencia, PLU, nombre" name="s" id="s"  class="search form-control"></td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>
                                        <td colspan="6" align="center">
                                            <button type="button" id="btn-search-products" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;<button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <div class="col-xs-12">
                        <h3>Consulta de productos</h3>
                    </div>
                    <div class="col-xs-3">
                        <label>Estado de productos</label>
                        <select id="product_status" name="product_status" class="form-control">
                            <option value="">Selecciona</option>
                            @if($typifications)
                                @foreach($typifications AS $typification)
                                    <option value="{{ $typification->name }}">{{ $typification->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-xs-3 product_typifications_status" style="display: none;">
                        <label for="store_id">Motivo</label>
                        <div class="row">
                            <div class="col-xs-12">
                                <select name="product_typifications_status" id="product_typifications_status" class="form-control">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <div class="paging products-query pre-scrollable"></div>
                    <div align="center" class="paging-loading products-query-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
                <div class="box-footer">
                    <button type="button" id="btn-add-products" class="btn btn-primary">Agregar productos a Merma</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <form method="post" action="{{ route('adminShrinkages.save') }}" class="form-modal shrinkage" id="update-price-custom-product">
                    <div class="box-header">
                        <div class="col-xs-8">
                            <h3>Productos a registrar en Merma</h3><br>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="products-shrinkage pre-scrollable"></div>
                        <div align="center" class="products-shrinkage-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="city_id" class="city_id">
                        <input type="hidden" name="warehouse_id" class="warehouse_id">
                        <input type="hidden" name="type" id="type" value="{{$type}}">
                        <button type="submit" id="btn-generate-order-request" class="btn btn-primary" onclick="return confirm('¿Estas seguro que deseas generar la merma de los productos?')">Generar Merma</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<style>
    .error{
        color:#ff0000;
        font-size: 13px;
        display: none;
    }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('body').find('.shrinkage .city_id').val($('#city_id').val());
    $('body').find('.shrinkage .warehouse_id').val($('#warehouse_id').val());

    $('.products-shrinkage-loading').hide();
    search($('.search').val());
    get_products();

    $('body').on('click', '#btn-search-products', function(){
        search($('.search').val());
    });

    $('body').on('change', '#warehouse_id', function(){
        get_products();
    });

    $('body').on('change', '#city_id', function() {
        var city_id = $(this).val();
        $('#warehouse_id').html('').append($("<option value=''>Selecciona</option>"));
        $('#store_id').html('').append($("<option value=''>Selecciona</option>"));
        $.ajax({
            url: '{{ route('adminShrinkages.getStoresByCityAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: { city_id: city_id },
        })
        .done(function(data) {
            $('body').find('.shrinkage .city_id').val(city_id);
            $('body').find('.shrinkage .warehouse_id').val($('#warehouse_id').val());

            $.each(data.warehouses, function(index, val) {
                 $('#warehouse_id').append($('<option></option>').attr('value', val.id).text(val.warehouse));
            });

            $.each(data.stores, function(index, val) {
                 $('#store_id').append($('<option></option>').attr('value', val.id).text(val.name));
            });
        });
    });

    $('body').on('change', '#warehouse_id', function() {
        $('body').find('.shrinkage .warehouse_id').val($(this).val());
    });

    $('body').on('change', '.all-product', function() {
        if($('body .product').size() == $('body .product:checked').size() && $(this).is(':checked')){
            $('body .product').prop('checked',true);
        }else{
            if($(this).is(':checked')){
                $('body .product').prop('checked',true);
            }else{
                $('body .product').prop('checked',false);
            }
        }
    });

    $('body').on('click', '.product', function() {
        var selected = $('.product').length;
        var count = 0;

        $(".product").each(function(){
            if($(this).is(':checked'))
                count = count + 1;
        });

        if(selected == count){
            $('body .all-product').prop('checked',true);
        }else{
            $('body .all-product').prop('checked',false);
        }

    });

    $('#product_status').on('change', function(){
        if($(this).val() == ''){
            $('.product_typifications_status').hide();
        }else {
            $.ajax({
                url: '{{ route('adminShrinkages.gettypificationsAjax') }}',
                type: 'GET',
                dataType: 'json',
                data: {name: $(this).val()},
                context: this
            })
            .done(function (data) {
                $('#product_typifications_status').empty();
                if(data.success) {
                    $.each(data.typifications, function (key, value) {
                        $('#product_typifications_status').append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                }
                $('.product_typifications_status').show();

            })
            .fail(function () {
                console.log("Error no se pueden obtener los motivos de estado de productos");
            });
        }
    });

    //añadir productos a grilla de merma
    $('body').on('click', '#btn-add-products', function(){
        var products_selected = {}, status = true, values = true, reported = true;

        if($('#product_status').val() == ''){
            alert('Debe seleccionar un estado de producto.');
            return false;
        }

        $('.product:checked').each(function() {
            id = $(this).val();
            quantity_product_shrinkage_stock = $('#product_shrinkage_stock-' + id).val();
            quantity_product_shrinkage = $('#product_cant-' + id).val();


            if($('#type').val() == 'Automatica')
            {
                if (quantity_product_shrinkage != '' && parseInt(quantity_product_shrinkage) <= parseInt(quantity_product_shrinkage_stock)){
                    products_selected[id] = [quantity_product_shrinkage, $('#product_status').val(), $('#product_typifications_status').val()];
                    //products_selected['status'] = $('#product_status').val();
                }else{
                    reported = false;
                    $('#product_cant-' + id).css({ border:'solid 1px #ff0000'});
                }
            }else{
                if (quantity_product_shrinkage != ''){
                    products_selected[id] = [quantity_product_shrinkage, $('#product_status').val(), $('#product_typifications_status').val()];
                }else{
                    status = false;
                    $('#product_cant-' + id).css({ border:'solid 1px #ff0000'});
                }
            }

            if(quantity_product_shrinkage < parseInt(0) || quantity_product_shrinkage > parseInt(100)){
                values = false;
                $('#product_cant-' + id).css({ border:'solid 1px #ff0000'});
            }


        });

        if(!values){
            alert('Las unidades del producto a reportar debe ser mayor a cero y menor igual a 99.');
            return false;
        }

        if(!reported){
            alert('Las unidades del producto a reportar debe ser menor o igual a lo reportado en el stock de merma.');
            return false;
        }

        if (status){
            if (!$('.product:checked').length){
                alert('Debes seleccionar al menos un productos de la lista.');
                return false;
            }

            $('.products-shrinkage-loading').show();
            add_products_shrinkage($('#warehouse_id').val(), products_selected);
        }
    });

    $('body').on('keyup', '.product_cant', function(){
        $(this).css({ border:'solid 1px #cccccc'});
    });

    $('body').on('click', '#btn-remove-product', function(e){
        e.preventDefault();
        $('.products-shrinkage-loading').show();
        var id = $(this).data('id');
        $.ajax({
            url: '{{ route('adminShrinkages.deleteProductAjax') }}',
            type: 'POST',
            dataType: 'html',
            data: { id: id },
            success: function(data){
                $('.products-shrinkage').html(data);
                search($('.search').val());
            },
            error: function(data){
                $('.products-shrinkage-loading').hide();
                $('.products-shrinkage').html('Ocurrió un error al obtener los datos.');
            },
            beforeSend: function(){
                $('.products-shrinkage-loading').show();
            },
            complete: function(){
                $('.products-shrinkage-loading').hide();
            }
        });
    });

    /**
     * Buscar productos en primera grilla
     */
    function search(search)
    {
        data = {
            city_id: $('#city_id').val(),
            store_id: $('#store_id').val(),
            warehouse_id: $('#warehouse_id').val(),
            type: $('#type').val(),
            s: search,
        };
        $.ajax({
            url: url_ajax,
            data: data,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                console.log(data);
                $('.products-query').html(data.html);
            },
            error: function(data){
                $('.products-query-loading').hide();
                $('.products-query').html('Ocurrió un error al obtener los datos.');
            },
            beforeSend: function(){
                $('.products-query').hide();
                $('.products-query-loading').show();
            },
            complete: function(){
                $('.products-query').show();
                $('.products-query-loading').hide();
            }
        });
    }

    /**
     * Obtener productos de grilla de abajo
     */
    function get_products()
    {
        $.ajax({
            url: '{{ route("adminShrinkages.getProductsAjax") }}'+'?warehouse_id='+$('#warehouse_id').val(),
            type: 'GET',
            dataType: 'html',
            success: function(data){
                $('.products-shrinkage').html(data);
            },
            error: function(data){
                $('.products-shrinkage-loading').hide();
                $('.products-shrinkage').html('Ocurrió un error al obtener los datos.');
            },
            beforeSend: function(){
                $('.products-shrinkage').hide();
                $('.products-shrinkage-loading').show();
            },
            complete: function(){
                $('.products-shrinkage-loading').hide();
                $('.products-shrinkage').show();
            }
        });
    }

    /**
     * Agregar productos a grilla de abajo
     */
    function add_products_shrinkage(warehouse_id, products_selected, send_dates = true)
    {
        if (send_dates){
            data = {
                warehouse_id: warehouse_id,
                products: JSON.stringify(products_selected),
            };
        }else{
            data = {
                warehouse_id: warehouse_id,
                products: JSON.stringify(products_selected),
            };
        }

        $.ajax({
            url: url_ajax,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data){
                if(data.success){
                    $('.products-shrinkage').html(data.html);
                    search($('.search').val());
                    get_products();
                }else{
                    alert(data.message);
                }
            },
            error: function(data){
                $('.products-shrinkage-loading').hide();
                $('.products-shrinkage').html('Ocurrió un error al obtener los datos.');
            },
            beforeSend: function(){
                $('.products-shrinkage-loading').show();
            },
            complete: function(){
                $('.products-shrinkage-loading').hide();
            }
        });
    }
});
</script>
@stop
@endif

@if (Request::ajax() && isset($products))
    @section('content_products_query')
    <table id="purchase-order-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th><input type="checkbox" name="all-product" class="all-product"/></th>
                <th>ID</th>
                <th>Imagen</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Nombre</th>
                <th>Proveedor</th>
                <th>Unidad de medida</th>
                <th>Stock merma</th>
                <th>Unidades a reportar</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products))
            @foreach($products as $product)
            <tr>
                <td align="center"><input type="checkbox" name="product_id-{{ $product['id'] }}" class="product" value="{{ $product['id'] }}"/></td>
                <td>{{ $product['id'] }}</td>
                <td align="center"><img src="{{ $product['image_small_url'] }}" height ="50px"></td>
                <td>{{ $product['reference'] }}</td>
                <td>{{ $product['provider_plu'] }}</td>
                <td>{{ $product['name'] }}</td>
                <td>{{ $product['provider'] }}</td>
                <td>{{ $product['product_quantity']}} {{ $product['unit'] }}</td>
                <td align="center">{{ $product['shrinkage_stock']}}</td>
                <td align="center">
                   <input type="hidden" name="product_shrinkage_stock-{{ $product['id'] }}" id="product_shrinkage_stock-{{ $product['id'] }}" value="{{ $product['shrinkage_stock'] }}">
                   <input type="text" name="product_cant-{{ $product['id'] }}" id="product_cant-{{ $product['id'] }}" class="product_cant form-control" style="width:50px;text-align:center" value="1" />
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($products) }} productos</div>
        </div>
    </div><br>
    @stop
@endif

@if (Request::ajax() && isset($products_added))
    @section('content_products_shrinkage')
    <table id="product-purchase-order-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Imagen</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Nombre</th>
                <th>Proveedor</th>
                <th>Unidad de medida </th>
                <th>Unidades a reportar</th>
                <th>Estado</th>
                <th>Motivo</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products_added))
            @foreach($products_added as $product)
            <tr>
                <td>{{ $product['id'] }}</td>
                <td align="center"><img src="{{ $product['image_small_url']}}" height ="50px"></td>
                <td>{{ $product['reference'] }}</td>
                <td>{{ $product['provider_plu'] }}</td>
                <td>{{ $product['name'] }}</td>
                <td>{{ $product['provider'] }}</td>
                <td>{{ $product['product_quantity']}} {{ $product['unit'] }}</td>
                <td>{{ $product['quantity_unit_to_report'] }}</td>
                <td>{{ $product['product_status'] }}</td>
                <td>{{ $product['product_reason'] }}</td>
                <td>
                    <div class="btn-group">
                     <a class="btn btn-xs btn-default" id="btn-remove-product" data-id="{{ $product['id']}}">
                        <span class="glyphicon glyphicon-trash"></span>
                     </a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="15" align="center">Productos no encontrados.</td></tr>
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($products_added) }} productos</div>
        </div>
    </div><br>
    @stop
@endif
