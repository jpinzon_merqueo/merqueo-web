@extends('admin.layout')

@section('content')
<style type="text/css">
        th{ text-align: center!important; }
</style>
<section class="content-header">
    <h1>
        {{ $title }} # {{ $shrinkage->id }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top:0px">
    @if ($admin_permissions['update'])
        @if($shrinkage->status == 'Pendiente')
            <a href="{{ route('adminShrinkages.updateStatus', ['id' => $shrinkage->id, 'status' => 'Cerrada']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Cerrada?')">
                <button type="button" class="btn btn-success">Actualizar Stock y Cerrar Merma</button>
            </a>
            <a href="{{ route('adminShrinkages.delete', ['id' => $shrinkage->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar la merma?')">
                <button type="button" class="btn btn-danger">Eliminar</button>
            </a>
        @endif
        <a href="{{ route('adminShrinkages.printPdf', ['id' => $shrinkage->id]) }}" target="_blank">
            <button type="button" class="btn btn-primary">Imprimir</button>
        </a>
   @endif
   </span>
</section>

<section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    @if(Session::has('error') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif
    @if(Session::has('success') )
    <div class="alert alert-success">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="callout callout-info">
                <h4>Datos de Merma</h4>
                <p><label>Estado</label> -
                @if($shrinkage->status == 'Pendiente' || $shrinkage->status == 'Elimanada')
                    <span class="badge bg-orange">{{ $shrinkage->status }}</span>
                @elseif($shrinkage->status == 'Cerrada')
                    <span class="badge bg-green">{{ $shrinkage->status }}</span>
                @endif
                </p>
                <p><label>Tipo </label> - {{ $shrinkage->type }}</p>
                <p><label>Fecha de creación</label> - {{ date("d M Y g:i a", strtotime($shrinkage->created_at)) }}</p>
                <p><label>Ciudad</label> - {{ $shrinkage->city }}</p>
                <p><label>Creada por </label> - {{ $shrinkage->admin }}</p>
                <p><label>Unidades reportadas </label> - {{ $shrinkage->quantity_reported }}</p>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <legend>Productos solicitados</legend>
                    <table id="shelves-table" class="table table-bordered table-striped" border="1">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>ID</th>
                                <th>Referencia</th>
                                <th>PLU</th>
                                <th>Nombre</th>
                                <th>Proveedor</th>
                                <th>Unidad de medida </th>
                                <th>Unidades reportadas</th>
                                <th>Stock anterior</th>
                                <th>Stock nuevo</th>
                                <th>Estado</th>
                                <th>Motivo</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shrinkage_products as $shrinkage_product)
                            <tr>
                                <td align="center"><img src="{{ $shrinkage_product->image_url }}" height ="50px"></td>
                                <td>{{ $shrinkage_product->id }}</td>
                                <td>{{ $shrinkage_product->reference }}</td>
                                <td>{{ $shrinkage_product->plu }}</td>
                                <td>{{ $shrinkage_product->product_name }}</td>
                                <td>{{ $shrinkage_product->provider }}</td>
                                <td>{{ $shrinkage_product->product_quantity}} {{ $shrinkage_product->product_unit }}</td>
                                <td>{{ $shrinkage_product->quantity_reported }}</td>
                                <td>{{ $shrinkage_product->quantity_stock_before }}</td>
                                <td>{{ $shrinkage_product->quantity_stock_after }}</td>
                                <td id="status-{{ $shrinkage_product->id }}"><span class="badge bg-red">{{ $shrinkage_product->status }}</span></td>
                                <td id="status-{{ $shrinkage_product->id }}"><span class="badge bg-red">{{ $shrinkage_product->typifications }}</span></td>
                                <td>
                                    @if($shrinkage->status == 'Pendiente')
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a class="remove-product" href="{{ route('adminShrinkages.removeProduct', ['id' =>  $shrinkage->id, 'product_id' => $shrinkage_product->id]) }}">
                                                        Eliminar
                                                    </a>
                                                </li>
                                                <div class="divider">Editar cantidad</div>
                                                <li><a href="#" onclick="edit({{ $shrinkage_product->id }}, {{ $shrinkage_product->quantity_reported }});">Editar cantidad</a></li>
                                                <div class="divider"></div>
                                                <li> <a href="javascript:;" class="change-status" data-id="{{ $shrinkage_product->id }}" >Cambiar estado</a></li>
                                            </ul>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
<!-- Edit Modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar cantidad de Producto</h4>
            </div>
            <form method="post" id="update-quantity" action="{{ route('adminShrinkages.updateProduct') }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" id="shrinkage-details-id" name="id" value="">
                    <div class="form-group">
                        <label class="control-label">Cantidad</label>
                        <input type="text" class="form-control" name="quantity" id="product-quantity">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-success save-status accept-btn">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit status Modal -->
<div class="modal fade" id="edit-status-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar estado de producto</h4>
            </div>
            <form method="post" id="update-quantity" action="{{ route('adminShrinkages.updateProductAjax') }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" id="shrinkage-details-id" name="id" value="">
                    <div class="form-group">
                        <label class="control-label">Estado</label>
                        <select id="product_status" name="product_status" class="form-control">
                            <option value="">Selecciona</option>
                            @if(isset($typifications))
                                @foreach($typifications AS $typification)
                                    <option value="{{ $typification->name }}">{{ $typification->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group product_typifications_status" style="display: none;">
                        <label class="control-label">Motivo</label>
                        <select name="product_typifications_status" id="product_typifications_status" class="form-control"></select>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="shrinkage-product-id" name="id" value="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function edit(product_id, quantity){
        $('#shrinkage-details-id').val(product_id);
        $('#product-quantity').val(quantity);
        $('#edit-modal').modal('show');
    }

    $(function(){
        $('.save-status').on('click', function(){
            if($('#product-quantity').val() < 0){
                alert('Las unidades del producto a reportar debe ser mayo a cero.');
                return false;
            }else{
                $('#update-quantity').submit();
            }
        });

        $('.change-status').on('click', function () {
            $("#shrinkage-product-id").val($(this).data('id'));
            $('#edit-status-modal').modal('show');
        });

        $('#product_status').on('change', function(){
            if($(this).val() == ''){
                $('.product_typifications_status').hide();
            }else {
                $.ajax({
                    url: '{{ route('adminShrinkages.gettypificationsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {name: $(this).val()},
                    context: this
                })
                    .done(function (data) {
                        $('#product_typifications_status').empty();
                        if(data.success) {
                            $.each(data.typifications, function (key, value) {
                                $('#product_typifications_status').append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        }
                        $('.product_typifications_status').show();

                    })
                    .fail(function () {
                        console.log("Error no se pueden obtener los motivos de estado de productos");
                    });
            }
        });

        $('body').on('click', '.update-product', function(){
            var id = $(this).data('id');
            var status = $(this).data('status');

            $('.btn-group button').addClass('disabled');
            $.ajax({
                url: '{{ route('adminShrinkages.updateProductAjax') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    status: status
                },
            })
            .done(function(data) {
                if(data.status)
                    $('.badge').parents().find('#status-'+id+' .badge').html(data.product_status);
                else
                    alert(data.message);

            })
            .fail(function(data) {
                $('.btn-group button').removeClass('disabled');
                console.log("error");
            })
            .always(function(data) {
                $('.btn-group button').removeClass('disabled');
                //console.log("complete");
            });
        });
    });
</script>
@stop