<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.1.0-beta.11/dist/vee-validate.min.js"></script>

<script>
    const messages = {
            _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
        if (max) {
            return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
        }

        return `El largo del campo ${field} debe ser ${length}.`;
    },
    max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field, [excluded]) => `El campo ${field} debe ser diferente a ${excluded}.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });
    let searchForm = new Vue({
        el: '#searchForm',
        data: {
            from:{
                city_id: {{ Session::get('admin_city_id') }},
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
                status: ''
            },
            cities: {{ $cities->toJson() }},
            warehouses: {{ $warehouses->toJson() }},
            isLoading: false,
            orders: null,
            status :[
            '','Validation','Initiated','Enrutado','In Progress','Alistado','Dispatched','Delivered'
            ],
            paginator: {
                data: [],
                total: 0,
                per_page: 0,
                current_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
                code: '',
                campaign_validation: '',
            },
            pages: [],
            url:  '{{ route('adminOrderStorage.details',['id'=>'order_id']) }}'
        },
        mounted(){
            this.getOrdersAjax();
        },
        methods: {
            clearOrders: function () {
              this.orders = null;
            },
            getWarehousesFrom: function (e) {
                this.isLoading = true;
                this.from.warehouse_id = '';
                this.warehouses = [];
                this.orders= null;
                axios.get('{{ route('adminOrderBoard.getWarehousesAjax') }}', {
                    params: {
                        cityId: this.from.city_id,
                    }
                })
                    .then(response => {
                    this.warehouses = response.data;
            })
            .catch(error => {
                    alert(`${error.response.data}`);
            })
            .then(() => {
                    this.isLoading = false;
            });
            },
            calculatePages: function () {
                let last = this.paginator.last_page;
                let delta = 5;
                let left = this.paginator.current_page - delta;
                let right = this.paginator.current_page + delta + 1;
                let range = [];
                let pages = [];
                let l;

                for (let i = 1; i <= last ; i++) {
                    if (i == 1 || i == last || (i >= left && i < right)) {
                        range.push(i);
                    }
                }

                range.forEach(function (i) {
                    if (l) {
                        if (i - l === 2) {
                            pages.push(l + 1);
                        } else if (i - l !== 1) {
                            pages.push('...');
                        }
                    }
                    pages.push(i);
                    l = i;
                });

                this.pages = pages;
            },
            getOrdersAjax: function (n) {
                this.$validator.validate('from.*').then(result => {
                    if (!result) {
                    return false;
                }else{
                    this.isLoading = true;
                    this.product = null;
                    axios.get('{{ route('adminOrderBoard.showOrderBoard') }}', {
                        params: {
                            warehouseId: this.from.warehouse_id,
                            status: this.from.status,
                            page:n
                        }
                    })
                        .then(response => {

                    this.paginator = response.data;
                    this.calculatePages();
                    this.orders=response.data.data;
                    this.orders.map(data => {
                        data.orders.map(dataOrder => {
                        dataOrder.url =this.url.replace('order_id', dataOrder.id);
                        });
                    });
                })
                .catch(error => {
                        this.from.product_info = '';
                    alert(`${error.response.data}`);
                })
                .then(() => {
                        this.isLoading = false;
                });
                }
            })
            },
        }
    });
    searchForm.$validator.dictionary.locale = 'es';

</script>