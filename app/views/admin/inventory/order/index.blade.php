@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="searchForm">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="city_id">Ciudad</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Ciudad"
                                name="city_id" id="city_id" class="form-control" v-model="from.city_id"
                                @change="getWarehousesFrom" :disabled="isLoading">
                            <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.city_id')">@{{ errors.first('from.city_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Bodega</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Bodega"
                                @change="clearOrders()"
                                name="warehouse_id" id="warehouse_id" class="form-control" v-model="from.warehouse_id"
                                :disabled="isLoading">
                            <option value="">-Selecciona-</option>
                            <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{
                                warehouse.warehouse }}
                            </option>
                        </select>

                        <span style="color: #d73925" v-show="errors.has('from.warehouse_id')">@{{ errors.first('from.warehouse_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Estado</label>
                        <select data-vv-scope="from" data-vv-as="Estado"
                                @change="clearOrders()"
                                name="status" id="status" class="form-control" v-model="from.status"
                                :disabled="isLoading">
                            <option v-for="(stat, index) in status" :value="stat">@{{ stat }}
                            </option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <br/>
                        <button class="btn btn-primary" @click="getOrdersAjax(1)" :disabled="isLoading">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body" v-if="orders!=null">
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <th>Ruta</th>
                                <th>Pedido</th>
                                <th>Estado</th>
                                <th>Productos</th>
                                <th>Zona horaria</th>
                                <th>Franja</th>
                                <th>Secuencia</th>
                                <th>Alistador Seco</th>
                                <th>Alistador Frío</th>
                                </thead>
                                <tbody>
                                <template v-for="(data) in orders">
                                    <tr v-for="(dataOrder) in data.orders" v-if="data.orders.length>0">
                                        <td>@{{ data.route}}</td>
                                        <td><a :href="dataOrder.url" target="_blank">@{{ dataOrder.id }}</a>
                                        <td>@{{ dataOrder.status}}</td>
                                        <td>@{{ dataOrder.total_products}}</td>
                                        <td>@{{ data.shift}}</td>
                                        <td>@{{ dataOrder.delivery_time}}</td>
                                        <td>@{{ dataOrder.planning_sequence}}</td>
                                        <td>
                                            <template v-if="dataOrder.picker_dry!=null">
                                                @{{ dataOrder.picker_dry.last_name}}
                                                @{{ dataOrder.picker_dry.first_name}}
                                            </template>
                                        </td>
                                        <td>
                                            <template v-if="dataOrder.picker_cold!=null">
                                                @{{ dataOrder.picker_cold.last_name}}
                                                @{{ dataOrder.picker_cold.first_name }}
                                            </template>
                                        </td>
                                    </tr>
                                </template>
                                </tbody>
                            </table>

                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <ul class="pagination">
                                        <li :class="paginator.current_page == 1 ? 'disabled' : ''">
                                            <a href="javascript:void(0)"
                                               @click.prevent="paginator.current_page != 1 ? getOrdersAjax(paginator.current_page-1) : ''">&laquo;</a>
                                        </li>
                                        <li v-for="n in pages" :class="paginator.current_page == n ? 'active' : ''">
                                            <a href="javascriot:void(0)"
                                               @click.prevent="paginator.current_page != n && n != '...' ? getOrdersAjax(n) : ''">
                                                @{{ n }}
                                                <span class="sr-only"
                                                      v-show="paginator.current_page == n">(current)</span>
                                            </a>
                                        </li>
                                        <li :class="paginator.current_page == paginator.last_page ? 'disabled' : ''">
                                            <a href="javascript:void(0)"
                                               @click.prevent="paginator.current_page != paginator.last_page ? getOrdersAjax(paginator.current_page+1) : ''">&raquo;</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    @include('admin.inventory.order.js.index-js')
@endsection