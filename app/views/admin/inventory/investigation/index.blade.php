@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="searchForm">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="city_id">Ciudad</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Ciudad"
                                name="city_id" id="city_id" class="form-control" v-model="from.city_id"
                                @change="getWarehousesFrom" :disabled="isLoading">
                            <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.city_id')">@{{ errors.first('from.city_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Bodega</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Bodega"
                                @change="clearDataTable()"
                                name="warehouse_id" id="warehouse_id" class="form-control" v-model="from.warehouse_id"
                                :disabled="isLoading">
                            <option value="">-Selecciona-</option>
                            <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{
                                warehouse.warehouse }}
                            </option>
                        </select>

                        <span style="color: #d73925" v-show="errors.has('from.warehouse_id')">@{{ errors.first('from.warehouse_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Modulo</label>
                        <select data-vv-scope="from" data-vv-as="Modulo" name="module_id" id="module_id"
                                @change="clearDataTable()"
                                class="form-control" v-model="from.module_id" :disabled="isLoading">
                            <option value="">-Todos-</option>
                            <option v-for="(module, index) in modules" :value="module.id">@{{ module.module }}</option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.module_id')">@{{ errors.first('from.module_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="status">Estado</label>
                        <select data-vv-scope="from" data-vv-as="Estado" name="status" @change="clearDataTable()"
                                id="status" class="form-control" v-model="from.status" :disabled="isLoading">
                            <option value="">-Seleccione-</option>
                            <option v-for="(status, index) in listStatus" :value="status.id">@{{ status.status }}
                            </option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.status')">@{{ errors.first('from.status') }}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group border-none">
                        <label>Fechas Inicial:</label>
                        <vuejs-datepicker v-validate="'required'" @closed="parseDateStart()" data-vv-scope="from"
                                          data-vv-as="Fecha Inicial" name="date_start" id="date_start"
                                          class="form-control" v-model="from.date_start" placeholder="DD/MM/YYYY"
                                          :disabled="isLoading" :format="'dd/MM/yyyy'"></vuejs-datepicker>
                        <span style="color: #d73925" v-show="errors.has('from.date_start')">@{{ errors.first('from.date_start') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group border-none">
                        <label>Fecha Final</label>
                        <vuejs-datepicker v-validate="'required|earlier'" @closed="parseDateEnd()" data-vv-scope="from"
                                          data-vv-as="Fecha Final" name="date_end" id="date_end" class="form-control"
                                          v-model="from.date_end" :disabled="isLoading" placeholder="DD/MM/YYYY"
                                          :format="'dd/MM/yyyy'"></vuejs-datepicker>
                        <span style="color: #d73925" v-show="errors.has('from.date_end')">@{{ errors.first('from.date_end') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="product_info">Producto</label>
                        <input data-vv-scope="from" data-vv-as="Producto" name="product_info" id="product_info"
                               class="form-control" v-model="from.product_info" :disabled="isLoading"
                               placeholder="referencia,nombre">
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <br/>
                        <button class="btn btn-primary" @click="getCountings()" :disabled="isLoading">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="panel-body">
                            <table class="table table-bordered table-striped"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th
                                            v-for="(column) in columns"
                                            v-text="column"
                                    ></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(data) in datatable" v-if="datatable.length>0">
                                    <td>@{{ data.id}}</td>
                                    <td>
                                        <img :src="data.store_product.product.image_small_url" height="50px">
                                    </td>
                                    <td>@{{ data.store_product.product.name}}</td>
                                    <td>@{{ data.store_product.product.reference}}</td>
                                    <td>@{{ data.module}}</td>
                                    <td>@{{ data.quantity}}</td>
                                    <td>@{{ data.status}}</td>
                                    <td>@{{ data.typification}}</td>
                                    <td>@{{ data.created_at}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    @click="getInvestigation(data.id,data.store_product_id)">
                                                <span class="caret"></span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr v-if="datatable.length==0" class="text-center">
                                    <td colspan="10">No hay datos</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-info" class="modal fade modal-lg" tabindex="-1" aria-labelledby="Detalles de la investigacion">
            <div class="modal-dialog" role="document" style="width: 80%!important;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Detalles de la investigación</h4>
                    </div>
                    <div class="modal-body">
                        <div v-if="infoInvestigation!=null">
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="col-xs-6">
                                        <h3>@{{infoInvestigation.investigationStock[0].store_product.product.reference}}
                                            -
                                            @{{infoInvestigation.investigationStock[0].store_product.product.name}}</h3>
                                    </div>
                                    <div class="col-xs-6">
                                        <img :src="infoInvestigation.investigationStock[0].store_product.product.image_medium_url"
                                             height="120px">
                                    </div>
                                </div>
                                <div v-if="infoInvestigation.investigationStock.length>0" class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <div class="panel-body">
                                                <table class="table table-bordered table-striped"cellspacing="0"
                                                   width="100%">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Estado</th>
                                                    <th>Cantidad</th>
                                                    <th>Modulo</th>
                                                    <th>Fecha</th>
                                                    <th>Usuario</th>
                                                    <th>Modificado por</th>
                                                    <th>Cambiar Estado</th>
                                                    <th>Motivo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(data, index) in infoInvestigation.investigationStock"
                                                    class="text-center">
                                                    <td>@{{ data.id}}</td>
                                                    <td>@{{ data.status}}</td>
                                                    <td>@{{ data.quantity}}</td>
                                                    <td>@{{ data.module}}</td>
                                                    <td>@{{ data.created_at}}</td>
                                                    <td>@{{ (data.inventoryCountingPosition) ?
                                                        data.inventoryCountingPosition.admin.fullname : ''}}
                                                        @{{ (data.admin) ? data.admin.fullname : ''}}
                                                    </td>
                                                    <td>@{{ (data.close_admin) ? data.close_admin.fullname : ''}}</td>
                                                    <td>
                                                        <select v-validate="'required'" data-vv-scope="data"
                                                                data-vv-as="Motivo" name="modal_status"
                                                                id="modal_status" class="form-control"
                                                                @change="clearDataModal(data)"
                                                                v-model="data.status_new"
                                                                :disabled="isLoading || data.status=='Cerrado'">
                                                            <option value="">-Seleccione-</option>
                                                            <option v-for="(status, index) in listStatus"
                                                                    v-if="status.id!=data.status" :value="status.id">
                                                                @{{status.status }}
                                                            </option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div v-if="data.status!='Cerrado'">
                                                            <select v-validate="'required'" data-vv-scope="data"
                                                                    data-vv-as="Motivo" name="typification"
                                                                    id="typification" class="form-control"
                                                                    v-model="data.typification_new" :disabled="isLoading || data.status=='Cerrado' || data.status_new == ''
                                                                    || data.status_new == undefined">
                                                                <option value="">-Seleccione-</option>
                                                                <option v-for="(data) in infoInvestigation.tipifications"
                                                                        :value="data">@{{ data}}
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div v-if="data.status=='Cerrado'">
                                                            @{{data.typification}}
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" :disabled="isLoading" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="button" class="btn btn-primary" :disabled="isLoading"
                                @click="updateInvestigationStock()">Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </section>

    @include('admin.inventory.investigation.js.index-js')
@endsection