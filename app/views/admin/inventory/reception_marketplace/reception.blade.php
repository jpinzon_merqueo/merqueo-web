@extends('admin.layout')

@section('content')
	@include('admin.inventory.reception_marketplace.css.reception-css')
	<section class="content-header">
        <h1>
            {{ $title }}
			<a href="{{ route('adminReceptionMarketplace.index') }}" class="btn btn-primary pull-right">
				<< Regresar a ordenes
			</a>
        </h1>
    </section>
    <section class="content">
    	@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alerta!</b> {{ Session::get('error') }}
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="list-group">
							<div class="list-group-item">
								<div class="row">
									<div class="col-xs-4 text-center">
										<b>Ciudad:</b><br>
										{{ $reception->providerOrder->city->city }}
									</div>
									<div class="col-xs-4 text-center">
										<b>Proveedor:</b><br>
										{{ $reception->providerOrder->provider->name }}
									</div>
									<div class="col-xs-4 text-center">
										<b>Estado</b><br>
										@if($reception->status == 'En proceso')
											<span class="reception_status badge bg-orange">{{ $reception->status }}</span>
										@elseif($reception->status == 'Recibido' || $reception->status == 'Almacenado')
											<span class="reception_status badge bg-green">{{ $reception->status }}</span>
										@elseif($reception->status == 'Revisado')
											<span class="reception_status badge bg-yellow">{{ $reception->status }}</span>
										@elseif($reception->status == 'Contabilizado')
											<span class="reception_status badge bg-green">{{ $reception->status }}</span>
										@elseif($reception->status == 'Cancelado')
											<span class="reception_status badge bg-red">{{ $reception->status }}</span>
										@elseif($reception->status == 'Iniciado')
											<span class="reception_status badge bg-blue">{{ $reception->status }}</span>
										@elseif($reception->status == 'Recibido con factura')
											<span class="reception_status badge bg-grey">{{ $reception->status }}</span>
										@elseif($reception->status == 'Validación')
											<span class="reception_status badge bg-yellow">{{ $reception->status }}</span>
										@endif
									</div>
								</div>
							</div>
							@foreach ($reception_details as $detail)
								<div class="list-group-item @if ($detail->received_status == 'Recibido') background-green @elseif( $detail->received_status == 'No recibido' ) background-red @elseif( $detail->received_status == 'Parcialmente recibido' ) background-orange @endif" data-order="{{ $detail->quantity_order }}" data-received="{{ $detail->quantity_expected }}">
									<div class="row">
										<div class="col-xs-3">
											<img src="{{ $detail->storeProduct->product->image_app_url }}" class="img-responsive">
										</div>
										<div class="col-xs-6 name-container">
											{{ $detail->storeProduct->product->name }} {{ $detail->storeProduct->product->quantity }} {{ $detail->storeProduct->product->unit }}
											<div align="center" class="paging-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
										</div>
										<div class="col-xs-6 quantity-container" style="display: none;">
											<label for="quantity_expected">Cantidades recibidas:</label>
											<input type="number" name="quantity_expected" value="{{ $detail->quantity_expected }}" data-detail_id="{{ $detail->id }}" class="form-control">
										</div>
										<div class="col-xs-3 text-center">
											<strong>Solicitadas</strong>
											<span class="badge">{{ $detail->quantity_order }}</span>
											<br>
											<br>
											<strong>Recibidas</strong>
											<span class="badge quantity_expected">{{ $detail->quantity_expected }}</span>
										</div>
									</div>
								</div>
							@endforeach
							<div class="list-group-item">
								<div class="row">
									<div class="col-xs-12 text-center">
										<a href="{{ route('adminReceptionMarketplace.updateReceptionStatus', ['reception_id' => $reception->id, 'status' => 'Recibido con factura']) }}" class="btn btn-primary">
											Actualizar a recibido
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    @include('admin.inventory.reception_marketplace.js.reception-js')
@endsection