@extends('admin.layout')

@section('content')
	<section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
    	@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alerta!</b> {{ Session::get('error') }}
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<form id="search-form">
									<div class="row">
										<div class="col-lg-3 col-md-3 col-xs-12">
											<div class="row form-group">
												<div class="col-lg-3 col-md-3 col-xs-3 text-right">
													<label for="city_id">Ciudad:</label>
												</div>
												<div class="col-lg-9 col-md-9 col-xs-9">
													<select id="city_id" name="city_id" class="form-control get-warehouses">
														@foreach ($cities as $city)
															<option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="row form-group">
												<div class="col-lg-3 col-md-3 col-xs-3 text-right">
													<label for="warehouse_id">Bodega:</label>
												</div>
												<div class="col-lg-9 col-md-9 col-xs-9">
													<select id="warehouse_id" name="warehouse_id" class="form-control">
														@foreach ($warehouses as $warehouse)
															<option value="{{ $warehouse->id }}">{{ $warehouse->warehouse }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12">
											<div class="row form-group">
												<div class="col-lg-3 col-md-3 col-xs-3 text-right">
													Proveedor:
												</div>
												<div class="col-lg-9 col-md-9 col-xs-9">
													<select id="provider_id" name="provider_id" class="form-control">
														<option value="">Selecciona</option>
														@if (count($providers))
															@foreach ($providers as $provider)
																@if ($provider->city_id == Session::get('admin_city_id'))
																	<option value="{{ $provider->id }}">{{ $provider->name }}</option>
																@endif
															@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-xs-12">
											<div class="row form-group">
												<div class="col-lg-3 col-md-3 col-xs-3 text-right">
													Buscar:
												</div>
												<div class="col-lg-9 col-md-9 col-xs-9">
													<input name="search_term" type="text" placeholder="Nro. Orden" class="form-control">
												</div>
											</div>
											<div class="row form-group">
												<div class="col-lg-3 col-md-3 col-xs-3 text-right">
												</div>
												<div class="col-lg-9 col-md-9 col-xs-9">
													<button type="submit" id="btn-reception-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
													<button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<br>
						<div class="paging"></div>
						<div align="center" class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="list-group">
							<a href="javascript:;" class="list-group-item active">
								<div class="row">
									<div class="col-xs-3">
										<b>Orden de compra #</b>
									</div>
									<div class="col-xs-3">
										<b>Ciudad</b>
									</div>
									<div class="col-xs-3">
										<b>Proveedor</p>
									</div>
									<div class="col-xs-3">
										<b>Estado</b>
									</div>
								</div>
							</a>
							<div class="orders-container">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="dataTables_info" id="reception-table_info">
									Mostrando <span id="count-orders"></span> ítems de <span id="total-orders"></span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div align="center" class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
			<div class="col-xs-12 text-center" id="paginate-links">
			</div>
		</div>
    </section>

	<!--=====================================================
	=            Modal datos de recibo de bodega            =
	======================================================-->
	@if ( Session::has('show_modal') )
		<div class="modal fade" id="modal-reception-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form id="provider_order_data" action="{{ route('adminReceptionMarketplace.getReceptions', ['provider_order_id' => Session::get('provider_order_id')]) }}" method="POST">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Ingresar datos de recibo</h4>
							<br>
							@if ( Session::has('error') )
								<div id="reception_grouped_products_error" class="alert alert-danger alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<b>Alerta!</b> <div class="message">{{ Session::get('error') }}</div>
								</div>
							@endif
							<div id="reception_grouped_products_success" class="alert alert-success alert-dismissable" style="display: none;">
								<i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="message"></div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row form-group">
								<div class="col-xs-12">
									<label for="invoice_number">Número de factura</label>
									<input type="text" name="invoice_number" class="form-control required">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-xs-12">
									<label for="transporter">Transportador</label>
									<input type="text" name="transporter" class="form-control required">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-xs-12">
									<label for="plate">Número de placa</label>
									<input type="text" name="plate" class="form-control required">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-xs-12">
									<label for="driver_name">Nombre del conductor</label>
									<input type="text" name="driver_name" class="form-control required">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Guardar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	@endif

    @include('admin.inventory.reception_marketplace.js.index-js')
@endsection