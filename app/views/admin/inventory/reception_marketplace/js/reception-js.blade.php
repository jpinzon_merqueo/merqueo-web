<script type="text/javascript">
	var Product = (function() {
		'use strict';

		function Product() {
			// enforces new
			if (!(this instanceof Product)) {
				return new Product();
			}
			this.bindAction();
			// constructor body
		}

		Product.prototype.bindAction = function() {
			let self = this

			$('.name-container').click(function() {
				self.toggleName(this);
			});

			$('input[name="quantity_expected"]').keyup(function(event){
				var code = event.which;
				if (code == 32 || code == 13 || code == 188 || code == 186){
					let qty = $(this).val()
					if ( $(this).val() != '' ) {
						let answer = confirm(`¿Desea ingresar ${qty} unidades de este producto?`)
						if ( answer ) {
							self.updateQuantitiesAjax($(this).data('detail_id'), $(this).val(), this)
						}else{
							alert('Producto no actualizado')
						}
						self.toggleQuantity($(this).parents('.quantity-container')[0])
						return;
					}else{
						alert('Debes ingresar una cantidad antes de actualizar el producto.')
					}
				}
			});
		};

		Product.prototype.toggleName = function(obj) {
			$(obj).slideToggle('fast', function () {
				$(obj).next('.quantity-container').slideToggle('fast', function () {
					$(this).find('input[name="quantity_expected"]').focus().select();
				});
			});
		};

		Product.prototype.toggleQuantity = function(obj) {
			$(obj).slideToggle('fast', function () {
				$(obj).prev('.name-container').slideToggle('fast');
			});
		};

		Product.prototype.updateQuantitiesAjax = function(detail_id, quantity, obj) {
			$(obj).parents('.list-group-item').find('.paging-loading').show();
			$.ajax({
				url: '{{ route('adminReceptionMarketplace.updateProductQuantityAjax', ['reception_id' => $reception->id]) }}',
				type: 'POST',
				dataType: 'json',
				data: {
					detail_id: detail_id,
					quantity: quantity
				}
			})
			.done(function(data) {
				$(obj).parents('.list-group-item').removeClass(function (index, className) {
					return (className.match (/(^|\s)background-\S+/g) || []).join(' ');
				})
				if ( data.status == 'Recibido' ) {
					$(obj).parents('.list-group-item').addClass('background-green');
				} else if ( data.status == 'Parcialmente recibido'){
					$(obj).parents('.list-group-item').addClass('background-orange');
				} else if ( data.status == 'No recibido'){
					$(obj).parents('.list-group-item').addClass('background-red');
				}

				if (data.reception_status) {
					if (data.reception_status == 'En proceso') {
						$('.reception_status ').removeClass(function (index, className) {
							return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
						}).addClass('bg-orange').html('En proceso');
					}
				}
				$(obj).parents('.list-group-item').find('.quantity_expected').html(data.quantity);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				$(obj).parents('.list-group-item').find('.paging-loading').hide();
				console.log("complete");
			});
		};

		return Product;
	}());

	$(document).ready(function() {
		let product = new Product;
	});
</script>