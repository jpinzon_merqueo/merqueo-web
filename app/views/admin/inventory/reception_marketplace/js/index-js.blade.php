<script type="text/javascript">
	$(document).ready(function() {
		$('body').on('submit', '#search-form', function(event) {
			event.preventDefault();
			var data = $('#search-form').serialize();
			$('.paging-loading').show();
			$.ajax({
				url: '{{ route('adminReceptionMarketplace.getOrdersAjax') }}',
				type: 'GET',
				dataType: 'json',
				data: data,
			})
			.done(function(data) {
				$('.orders-container').html(data.providerOrders);
				$('#count-orders').html(data.providerOrders_count);
				$('#total-orders').html(data.providerOrders_total);
				$('#paginate-links').html(data.links);
			})
			.fail(function(data) {
				console.debug(data);
				console.log("error al buscar.");
			})
			.always(function() {
				$('.paging-loading').hide();
			});
		});

		$('body').on('click', '#paginate-links a', function(event) {
			event.preventDefault();
			var data = $('#search-form').serialize();
			$('.paging-loading').show();
			$.ajax({
				url: $(this).prop('href'),
				type: 'GET',
				dataType: 'json',
				data: data,
			})
			.done(function(data) {
				$('tbody.tbody').html(data.receptions);
				$('#count-receptions').html(data.receptions_count);
				$('#total-receptions').html(data.receptions_total);
				$('#paginate-links').html(data.links);
			})
			.fail(function(data) {
				console.debug(data);
				console.log("error al buscar.");
			})
			.always(function() {
				$('.paging-loading').hide();
			});
		});

		$('#search-form').trigger('submit');

		@if ( Session::has('show_modal') )
			$('#provider_order_data').validate();
			$('#modal-reception-data').modal('show');
		@endif
	});
</script>