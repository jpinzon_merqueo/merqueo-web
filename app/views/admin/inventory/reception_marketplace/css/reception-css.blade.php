<style type="text/css">
	.background-green{
		background-color: #dff0d8 !important;
		border-color: #d6e9c6;
		color: #3c763d;
	}
	.background-orange{
		background-color: #fcf8e3 !important;
		border-color: #faebcc;
		color: #8a6d3b;
	}
	.background-red{
		background-color: #f2dede !important;
		border-color: #ebccd1;
		color: #a94442;
	}
</style>