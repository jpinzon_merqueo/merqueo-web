@if(count($providerOrders))
	@foreach($providerOrders as $order)
		<a href="{{ route('adminReceptionMarketplace.getReceptions', ['provider_order_id' => $order->id]) }}" class="list-group-item">
			<div class="row">
				<div class="col-xs-3">
					{{ $order->id }}
				</div>
				<div class="col-xs-3">
					{{ $order->city->city }}
				</div>
				<div class="col-xs-3">
					{{ $order->provider->name }}
				</div>
				<div class="col-xs-3">
					@if($order->status == 'Iniciada')
					<span class="badge bg-orange">Iniciada</span>
					@elseif($order->status == 'Enviada')
					<span class="badge bg-yellow">Enviada</span>
					@elseif($order->status == 'Pendiente')
					<span class="badge bg-blue">Pendiente</span>
					@elseif($order->status == 'Cerrada')
					<span class="badge bg-green">Cerrada</span>
					@elseif($order->status == 'Cancelada')
					<span class="badge bg-red">Cancelada</span>
					@endif
				</div>
			</div>
		</a>
	@endforeach
@else
	<tr><td colspan="10" align="center">No hay datos.</td></tr>
@endif