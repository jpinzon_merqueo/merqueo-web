@if (!Request::ajax())
	@extends('admin.layout')
	@section('content')
	<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        
    </section>
    <section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Información</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
        <style type="text/css">
            #modal-products .modal-dialog, #modal-order-product-group .modal-dialog{
                width: 70%!important;
            }
            #modal-products .error{
                color:#ff0000;
                font-size: 13px;
                display: none;
            }

            #map-point {
                width: 100%;
                height: 400px;
                background-color: grey;
            }
        </style>
        <div class="row">
            <div class="col-xs-12">
                <form class="form-inline" id="provider_order_reception_form" action="{{ route('Supplier.getProviderOrderReceptionAjax') }}" id="search-form" method="POST" autocomplete="off">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row error-search">
                            <div class="col-xs-12">
                                <div class="alert alert-danger unseen"></div>
                                <div class="alert alert-success unseen"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="from-group">
                                    <label for="invoice_number" ># Recibo de bodega</label>
                                    <input type="text" id="invoice_number" name="invoice_number" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="from-group ">
                                    <label for="storage" >Tipo de almacenamiento</label><br>
                                    <select id="storage" name="storage" class="form-control" style="width: 100%;">
                                        <option value="Seco">Seco</option>
                                        <option value="Frío">Frío</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 text-center">
                                <div class="from-group text-center">
                                    <input type="button" class="btn btn-primary" name="search" id="search-provider-order" value="Buscar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="row" id="details">
            <div class="col-xs-12 unseen details">

                <div class="box box-primary">
                    <div class="box-header">
                        <h4 class="box-title">Detalles del recibo</h4>
                        
                    </div>
                    <div class="box-body">
                        <div class="row" id="order-reception-details">
                            
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 text-center">
                                <button type="button" class="btn btn-primary" id="create-stowage"><i class="fa fa-plus"></i> Agregar estiba</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 unseen details-products" >
                <div class="box box-primary">
                    <div class="box-header">
                        <h4 class="box-title">Productos en la estiba</h4>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-primary" id="btn-add-products">Agregar productos a la estiba <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="box-body product_list" >
                        <div class="row error-search">
                            <div class="col-xs-12">
                                <div class="alert alert-danger unseen"></div>
                                <div class="alert alert-success unseen"></div>
                            </div>
                        </div>
                        <div class="row unseen">

                        </div>
                        <div class="row ">
                            <form id="stowage-products-form"  method="POST" autocomplete="off">
                                <div class="col-md-12 col-xs-12 table-responsive">
                                    <table id="shelves-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Imagen</th>
                                                <th>Referencia</th>
                                                <th>Nombre</th>
                                                <th>Fecha de vencimiento</th>
                                                <th>Unidades recibidas</th>
                                                <th>Unidades ya almacenadas</th>
                                                <th>Unidades por almacenar</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="order-reception-products-details">
                                            <tr class="empty"><td colspan="9" align="center">No hay productos agreagdos a la estiba</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 text-center">
                                <div class="from-group text-center">
                                    <input type="button" class="btn btn-primary" name="search"  id="save-stowage" value="Almacenar productos en la estiba">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <div class="modal fade" id="modal-save-stowage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Posición destino</h4>
                </div>
                <form method="post" action="" class="form-modal" id="send-storage-data">
                    <div class="modal-body">
                        <div class="row error-search">
                            <div class="col-xs-12">
                                <div class="alert alert-danger unseen"></div>
                                <div class="alert alert-success unseen"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="sms">Posición</label>
                            <input type="number" class="form-control" id="position" name="position" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="sms">Posición en altura</label>
                            <input type="text" class="form-control" id="height_position" name="height_position" required=""></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="btn-save-stowage">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Add products Modal -->
    <div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar producto a la estiba</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 modal-search-products">
                            
                            <div class="row error-search">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger unseen"></div>
                                    <div class="alert alert-success unseen"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <form action="{{ route('Supplier.getProviderOrderReceptionProductDetailAjax') }}" id="search-form-product"  method="POST" autocomplete="off">
                                        <table width="100%" class="modal-product-request-table">
                                            <tbody>
                                                <tr>
                                                    <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                    <td>
                                                        <input type="text" name="reference" id="reference" class="form-control" placeholder="Referencia, Nombre">
                                                    </td>
                                                    <td colspan="2" align="left">
                                                        <input type="button" class="btn btn-primary" name="search" id="search-provider-order-product"  value="Buscar">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="2"><span class="error" style="display: none;">Campo requerido</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="modal-products-request pre-scrollable">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                    <th>Referencia</th>
                                                    <th>Nombre</th>
                                                    <th>Fecha de vencimiento</th>
                                                    <th>Unidades recibidas</th>
                                                    <th>Unidades ya almacenadas</th>
                                                    <th>Unidades por almacenar</th>
                                                    <th>Agregar a la estiba</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody" id="order-reception-products-details-search">
                                                <tr class="empty"><td colspan="9" align="center">No hay productos</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div align="center" class="modal-products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var delete_row = function(store_product_id){
            if(confirm('¿Esta seguro de quitar este producto de la estiba?')){
                $('#row_stowage_product_'+store_product_id).remove();
                if( $('#order-reception-products-details tr').length == 0 ){
                    $('#order-reception-products-details').html('<tr class="empty"><td colspan="9" align="center">No hay productos agregados a la estiba</td></tr>');
                }
            }
        }

        var providerOrderReception = (function(){
            'use strict';
            function providerOrderReception( args ){
                if( !( this instanceof providerOrderReception ) ){
                    return new providerOrderReception( args );
                }
                this.url_products_provider_order = '{{ route("Supplier.getProviderOrderReceptionProductDetailAjax") }}';
                this.url_process_storage = '{{ route("Supplier.storageProcessAjax") }}';
                this.url_search_products_provider_order = '{{ route("Supplier.getProviderOrderReceptionProductDetailAjax") }}';
                this.bindActions();
            }

            providerOrderReception.prototype.bindActions = function(){
                var self = this;
                $('#search-provider-order').on('click', function(event){
                    event.preventDefault();
                    if($('#invoice_number').val() != ""){
                        self.ajax()
                    }else{
                       $('.error-search .alert-danger').html('Debe ingresar un número de recibo para proceder con el almacenaje.').fadeIn().delay(5000).fadeOut();
                    }
                });

                $('#search-form-product').on('submit', function(event){
                    event.preventDefault();
                });
                $('#provider_order_reception_form').on('submit', function(event){
                    event.preventDefault();
                    self.ajax();
                })
                $('#storage').on('change', function(){
                    $('#order-reception-products-details').html('<tr class="empty"><td colspan="9" align="center">No hay productos agregados a la estiba</td></tr>');
                    $('#details').find('.details').fadeOut();
                    $('#details').find('.details-products').fadeOut();
                })

                $('#reference').keyup(function(event){
                    var code = event.which;
                    if (code == 32 || code == 13 || code == 188 || code == 186){
                        self.ajax_products();
                    }
                });

                $('#create-stowage').on('click', function(){
                    $('#details').find('.details-products').fadeIn();
                })

                $('#search-provider-order-product').on('click', function(){
                    event.preventDefault();
                    if($('#reference').val() != ""){
                        var trs = $('#order-reception-products-details-search').find('tr');
                        self.ajax_products();
                    }else{
                       $('.modal-search-products').find('.error-search .alert-danger').html('Debe ingresar la referencia del producto.').fadeIn().delay(5000).fadeOut();
                    }
                });

                $('#save-stowage').on('click', function(){
                    self.add_actions_to_products_form();
                });
                $('#modal-products').on('show.bs.modal', function(){
                    $.ajax({
                        url : self.url_products_provider_order,
                        type : 'POST',
                        data : { id_provider_order_reception : $('#id_provider_order_reception').val(), type: $('#type').val(), storage : $('#type_storage').val() },
                        dataType : 'json'
                    })
                    .done(function(data){
                        if(data.result){
                            var product_id = data.id;
                            $('#order-reception-products-details-search').html(data.msg);
                            $('#reference').val('');
                        }else{
                            $('.modal-search-products').find('.error-search .alert-danger').html(data.msg).fadeIn().delay(5000).fadeOut();
                        }
                    })
                    .fail(function(data){
                        console.log('Error al consultar los datos');
                    });
                });
                
                $('#btn-add-products').on('click', function(){
                    self.show_modal('products');
                })

                $('#btn-save-stowage').on('click', function(event) {
                    event.preventDefault();
                    if($('#position').val()!='' && $('#height_position').val()!=''){
                        $('#modal-save-stowage').modal('hide');
                        self.save_stowage();
                    }else{
                        $('#send-storage-data').find('.error-search .alert-danger').html('Los campos de posición y posición en altura son obligatorios.').fadeIn().delay(5000).fadeOut();
                    }
                });
            }

            providerOrderReception.prototype.ajax = function () {
                var self = this;
                $.ajax({
                    url : $('#provider_order_reception_form').attr('action'),
                    type : 'POST',
                    data : $('#provider_order_reception_form').serialize(),
                    dataType : 'json'
                })
                .done(function(data){
                    if(data.result){
                        $('#order-reception-details').html(data.msg);
                        $('#details').find('.details').fadeIn();
                        $('#details').find('.details-products').fadeOut();
                        $('#order-reception-products-details').html('<tr class="empty"><td colspan="9" align="center">No hay productos agregados a la estiba</td></tr>');
                    }else{
                        $('.error-search .alert-danger').html(data.msg).fadeIn().delay(5000).fadeOut();
                        $('#order-reception-details').html('')
                        $('#details').find('.details').hide();
                    }
                })
                .fail(function(data) {
                    console.log("error al cargar los datos.");
                });
            }

            providerOrderReception.prototype.ajax_products = function () {
                var self = this;
                $.ajax({
                    url : this.url_products_provider_order,
                    type : 'POST',
                    data : { id_provider_order_reception : $('#id_provider_order_reception').val(), search : $('#reference').val(), type: $('#type').val(), storage : $('#type_storage').val() },
                    dataType : 'json'
                })
                .done(function(data){
                    if(data.result){
                        var product_id = data.id;
                        $('#order-reception-products-details-search').html(data.msg);
                        $('#reference').val('');
                    }else{
                        $('.modal-search-products').find('.error-search .alert-danger').html(data.msg).fadeIn().delay(5000).fadeOut();
                    }
                })
                .fail(function(data){
                    console.log('Error al consultar los datos');
                });
            }

            providerOrderReception.prototype.add_actions_to_products_form = function(){
                var self = this;
                if($('#order-reception-products-details').find('tr.empty').length == 1 ){
                    $('.product_list').find('.error-search .alert-danger').html('No hay productos para agregar a la estiba').fadeIn().delay(5000).fadeOut();
                    return;
                }
                self.show_modal('save-stowage');
            }

            providerOrderReception.prototype.add_producto = function(id){
                var self = this;
                if(self.validate_product(id)){
                    if($('#row_stowage_product_'+id).length > 0){
                        $('.modal-search-products').find('.error-search .alert-danger').html('Este producto ya fue agregado').fadeIn().delay(5000).fadeOut();
                        return;
                    }else{
                        $('#order-reception-products-details').find('tr.empty').remove();
                        $('#order-reception-products-details').append(self.create_row(id));
                        $('.provider_order_reception_product_search_'+id).remove();
                        $('.modal-search-products').find('.error-search .alert-success').html('Este producto fue agregado a la estiba con éxito.').fadeIn().delay(5000).fadeOut();
                    }
                    
                }
            }

            providerOrderReception.prototype.create_row = function(id){
                var image = $('#provider_order_reception_product_'+id).find('.image').html();
                var reference = $('#provider_order_reception_product_'+id).find('.reference').html();
                var name = $('#provider_order_reception_product_'+id).find('.name').html();
                var unit = $('#provider_order_reception_product_'+id).find('.unit').html();
                var format_expiration_date = $('#provider_order_reception_product_'+id).find('.expiration_date').html();
                var quantity_received = $('#provider_order_reception_product_'+id).find('.quantity_received').html();
                var quantity_storaged = $('#provider_order_reception_product_'+id).find('.quantity_storaged').html();
                var quantity_to_storage = $('#quantity_to_storage_'+id).val();
                var expiration_date = $('#expiration_date_'+id).val();
                var user_quantity_to_storage = $('#user_quantity_to_storage_'+id).val();
                var parent_store_product_id = $('#parent_store_product_id_'+id).val();
                var type = $('#type_'+id).val();
                var row = '';
                row = '<tr id="row_stowage_product_'+id+'">';
                    row += '<td>'+image+'</td>';
                    row += '<td>'+reference+'</td>';
                    row += '<td>'+name+'</td>';
                    row += '<td>'+format_expiration_date+'</td>';
                    row += '<td>'+quantity_received+'</td>';
                    row += '<td>'+quantity_storaged+'</td>';
                    row += '<td class="form-group">';
                        row += user_quantity_to_storage;
                        row += '<input type="hidden" name="product_storage_id[]" value="'+id+'">';
                        row += '<input type="hidden" name="user_quantity_to_stowage_'+id+'" value="'+user_quantity_to_storage+'" >';
                        row += '<input type="hidden" name="quantity_to_stowage_'+id+'" value="'+quantity_to_storage+'" >';
                        row += '<input type="hidden" name="expiration_date_'+id+'" value="'+expiration_date+'" >';
                        row += '<input type="hidden" name="parent_store_product_id_'+id+'" value="'+parent_store_product_id+'" >';
                        row += '<input type="hidden" name="type_'+id+'" value="'+type+'" >';
                    row += '</td>';
                    row += '<td>';
                        row += '<span class="btn btn-danger" onclick="delete_row('+id+')"><i class="glyphicon glyphicon-trash"></i></span>';
                    row += '</td>';
                row += '</tr>';
                return row;
            }

            providerOrderReception.prototype.validate_product = function(product_storage_id){
                var self = this;
                var valid = true;
                var user_quantity_to_storage = parseInt($('#user_quantity_to_storage_'+product_storage_id).val());
                var quantity_to_storage = parseInt($('#quantity_to_storage_'+product_storage_id).val());
                if(user_quantity_to_storage < 0 ){
                    $('#quantity_to_storage_'+product_storage_id).parent().addClass('has-error');
                    $('#quantity_to_storage_'+product_storage_id).parent().find('.alert-field').html('La cantidad a almacenar no puede ser menor que 0').fadeIn().delay(5000).fadeOut();
                    valid = false;
                }
                if(user_quantity_to_storage > quantity_to_storage){
                    $('#quantity_to_storage_'+product_storage_id).parent().addClass('has-error');
                    $('#quantity_to_storage_'+product_storage_id).parent().find('.alert-field').html('La cantidad a almacenar no puede ser mayor que '+quantity_to_storage).fadeIn().delay(5000).fadeOut();
                    valid = false;
                }

                if(user_quantity_to_storage == 0){
                    $('#quantity_to_storage_'+product_storage_id).parent().addClass('has-error');
                    $('#quantity_to_storage_'+product_storage_id).parent().find('.alert-field').html('La cantidad a almacenar no puede ser cero.').fadeIn().delay(5000).fadeOut();
                    valid = false;
                }

                if(quantity_to_storage == 0){
                    $('#quantity_to_storage_'+product_storage_id).parent().addClass('has-error');
                    $('#quantity_to_storage_'+product_storage_id).parent().find('.alert-field').html('Este producto ya fue almacenado en su totalidad').fadeIn().delay(5000).fadeOut();
                    valid = false;
                }

                if(quantity_to_storage < 0){
                    $('#quantity_to_storage_'+product_storage_id).parent().addClass('has-error');
                    $('#quantity_to_storage_'+product_storage_id).parent().find('.alert-field').html('Este producto tiene una inconsistencia').fadeIn().delay(5000).fadeOut();
                    valid = false;
                }
                return valid;
            }

            providerOrderReception.prototype.save_stowage = function(){
                var self = this;
                
                var storage = $('#type_storage').val();
                var order_reception_id = $('#id_provider_order_reception').val();
                var data_form = $('#stowage-products-form').serialize()+'&order_reception_id='+order_reception_id+'&storage='+storage+'&'+$('#send-storage-data').serialize()+'&warehouse_id='+$('#warehouse_id').val();
                $('#btn-save-stowage').prop('disabled', true);
                $('#save-stowage').prop('disabled', true);
                $.ajax({
                    url : this.url_process_storage,
                    type : 'POST',
                    data : data_form,
                    dataType : 'json'

                })
                .done(function(data){
                    $('#send-storage-data')[0].reset();
                    if(data.result){
                        $('.product_list').find('.error-search .alert-success').html(data.msg).fadeIn().delay(5000).fadeOut();
                        $('#order-reception-products-details').html('<tr class="empty"><td colspan="9" align="center">No hay productos agregados a la estiba</td></tr>');
                        $('#details').find('.details-products').delay(5000).fadeOut();
                        $('#search-provider-order').trigger('click')
                        /*setTimeout(function(){
                            location.reload();
                        }, 3000);*/
                        $('#btn-save-stowage').prop('disabled', false);
                        $('#save-stowage').prop('disabled', false);
                    }else{
                        $('.product_list').find('.error-search .alert-danger').html(data.msg).fadeIn().delay(5000).fadeOut();

                        $('#order-reception-products-details').html('<tr class="empty"><td colspan="9" align="center">No hay productos agregados a la estiba</td></tr>');
                        $('#details').find('.details-products').delay(5000).fadeOut();
                        $('#search-provider-order').trigger('click')

                        $('#btn-save-stowage').prop('disabled', false);
                        $('#save-stowage').prop('disabled', false);

                    }

                })
                .fail(function(data){
                    $('#send-storage-data')[0].reset();
                    $('#btn-save-stowage').prop('disabled', false);
                    $('#save-stowage').prop('disabled', false);
                    console.log('Error al consultar los datos');
                });

            }

            providerOrderReception.prototype.show_modal = function(action, reference){
                var self = this;
                var reference = reference || null;
                if(action == 'supplied-picking'){
                    $('#send-supplied-data').on('submit', function(event){
                        event.preventDefault();
                        if( $('#quantity_supplied').val() != '' ){
                            $(reference).attr('data-quantity-supplied', $('#quantity_supplied').val() );
                            $('#quantity_supplied').val('');
                            $('#modal-' + action).modal('hide')
                            self.ajax_supply_process(action, reference);
                        }else{
                           $('.error-search .alert-danger').html('Debe agregar la cantidad surtida.').fadeIn().delay(5000).fadeOut();
                        }
                    })
                    /*$('#modal-supplied-picking').on('shown.bs.modal', function() {

                    });*/
                }

                if(action == 'save-stowage'){
                    
                }
                if(action == 'products'){
                    $('#modal-products').on('hidden.bs.modal', function(){
                        $('#order-reception-products-details-search').html('<tr class="empty"><td colspan="9" align="center">No hay productos</td></tr>');
                    });
                }

                $('#modal-' + action).modal('show');
            }
            return providerOrderReception;
        }());
        
        var provider_order_reception = new providerOrderReception;
    </script>
	@endsection
@else
    @section('order-receptions-details')
    @if(isset($id))
    <div class="col-md-12 col-xs-12 table-responsive">
        <input type="hidden" value="{{$id}}" id="id_provider_order_reception">
        <input type="hidden" id="type_storage" value="{{$storage}}">
        <input type="hidden" id="warehouse_id" value="{{$warehouse_id}}">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Bodega</th>
                    <th>Proveedor</th>
                    <th>Estado</th>
                    <th>Fecha recepción</th>
                    <th>Factura</th>
                    <th>Unidades de productos por almacenar</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$warehouse}}</td>
                    <td>{{$provider_name}}</td>
                    <td><span class="badge bg-green">{{$status}}</span></td>
                    <td>{{date('d M Y H:i:s', strtotime($received_date))}}</td>
                    <td>{{$invoice_number}}</td>
                    <td>{{$total_products}}</td>
                </tr>
            </tbody>
        </table>
        
    </div>
    @endif
    @endsection

    @section('order-receptions-product-details')
    @if(isset($product))
        @if($product)
            <tr id="provider_order_reception_product_{{$product['id']}}">
                <td><img src="{{$product['image_small_url']}}" class="image-responsive" /></td>
                <td>{{$product['reference']}}</td>
                <td>{{$product['name']}}</td>
                
                <td>{{$product['expiration_date']}}</td>
                <td>{{$product['quantity_received']}}</td>
                <td>{{$product['quantity_storaged']}}</td>
                <td>
                    <input type="hidden" name="product_storage_id[]" class="stowage_product_storage_id" value="{{$product['id']}}">
                    <input type="number" min="0" name="user_quantity_to_storage_{{$product['id']}}" id="user_quantity_to_storage_{{$product['id']}}" class="user_quantity_to_storage form-control" value="{{$product['quantity_to_storage']}}" >
                    <input type="hidden" name="quantity_to_storage_{{$product['id']}}" id="quantity_to_storage_{{$product['id']}}" class="quantity_to_storage" value="{{$product['quantity_to_storage']}}" >
                    <div class="unseen alert alert-danger form-has-errors alert-field"></div>
                </td>
                <td>
                    <span class="btn btn-danger" onclick="delete_row({{$product['id']}})"><i class="glyphicon glyphicon-trash"></i></span>
                </td>
            </tr>
        @endif
    @endif
    @endsection

    @section('order-receptions-product-details-search')
    @if(isset($products))
        @if(count($products))
            @foreach($products as $product)
            <tr id="provider_order_reception_product_{{$product['id']}}" class="provider_order_reception_product_search_{{$product['id']}}">
                <td class="image"><img src="{{$product['image_small_url']}}" class="image-responsive" /></td>
                <td class="reference">{{$product['reference']}}</td>
                <td class="name">{{$product['name']}}</td>
                
                <td class="expiration_date">
                    @if(!empty($product['expiration_date']) || !is_null($product['expiration_date']))
                    {{date('d M Y', strtotime($product['expiration_date']))}}
                    @endif
                </td>
                <td class="quantity_received">{{$product['quantity_received']}}</td>
                <td class="quantity_storaged">{{$product['quantity_storaged']}}</td>
                <td class="form-group">
                    <input type="hidden" class="stowage_product_storage_id" value="{{$product['id']}}">
                    <input type="number" id="user_quantity_to_storage_{{$product['id']}}" class="user_quantity_to_storage form-control" value="{{$product['quantity_to_storage']}}" >
                    <input type="hidden" id="quantity_to_storage_{{$product['id']}}" class="quantity_to_storage" value="{{$product['quantity_to_storage']}}" >
                    <input type="hidden" id="expiration_date_{{$product['id']}}" value="{{$product['expiration_date']}}" >
                    <input type="hidden" id="parent_store_product_id_{{$product['id']}}" value="{{$product['parent_store_product_id']}}" >
                    <input type="hidden" id="type_{{$product['id']}}" value="{{$product['type']}}" >
                    <div class="unseen alert alert-danger form-has-errors alert-field"></div>
                </td>
                <td>
                    <span class="btn btn-primary" onclick="provider_order_reception.add_producto({{$product['id']}})"><i class="glyphicon glyphicon-plus"></i></span>
                </td>
            </tr>
            @endforeach
        @endif
    @endif
    @endsection
@endif

