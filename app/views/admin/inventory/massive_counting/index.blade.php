@extends('admin.layout')

@section('content')
    <style>
        .list-group-item{
            background-color: #FFF;
        }
        .list-group-item-danger {
            color: #a94442;
            background-color: #f2dede;
        }
        .list-group-item-success {
            color: #3c763d;
            background-color: #dff0d8;
        }
    </style>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section id="content" class="content">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <form id="search-form" class="admin-providers-table" @submit.prevent="searchProduct">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <label for="city_id">Ciudad:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            <select id="city_id" name="city_id" class="form-control" v-model="city_id" :disabled="isDisabled === true ? 'disabled' : isDisabled" @change="onCityChanged(city_id)">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <label for="warehouse_id">Bodega:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            <select name="warehouse_id" id="warehouse_id" class="form-control" v-model="warehouse_id" :disabled="isDisabled">
                                                @foreach ($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}" {{ Session::get('admin_warehouse_id') == $warehouse->id ? 'selected="Selected"' : '' }}>{{ $warehouse->warehouse }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="storage">Tipo de Almacenamiento:</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-12">
                                            <select name="storage" id="storage" class="form-control" v-model="storage" :disabled="isDisabled">
                                                <option value="Seco">Seco</option>
                                                <option value="Refrigerado">Refrigerado</option>
                                                <option value="Congelado">Congelado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <label for="action">Acción:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            <select name="action" id="action" class="form-control" v-model="action" :disabled="isDisabled">
                                                <option value="Almacenamiento">Almacenamiento</option>
                                                <option value="Alistamiento">Alistamiento</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12" v-show="action == 'Alistamiento'">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <label for="position">Posición:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            <input type="text" name="position" id="position" class="form-control" v-model="position" @keypress="isNumber">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12" v-show="action == 'Alistamiento'">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="position_height">Posición en altura:</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-12">
                                            <input type="text" name="position_height" pattern="[A-Z]{1}" id="position_height" class="form-control" v-model="position_height">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12" v-show="action != 'Alistamiento'">
                                    <div class="row form-group">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <label for="search">Buscar:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-12">
                                            <input type="text" name="search" id="search" class="form-control" v-model="search">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12 col-lg-offset-2 col-md-offset-2">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-6 text-center">
                                            <button type="submit" class="btn btn-primary" :disabled="isSubmitDisable">Buscar</button>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-6 text-center">
                                            <button type="button" class="btn btn-primary" @click="resetForm">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div align="center" class="paging-loading" v-show="isLoading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>Productos buscados</strong>
                            </div>
                            <ul class="list-group products-fround" v-show="isProductsLoaded" style="max-height: 300px; overflow: auto;">
                                <li v-for="(product, index) in products" class="list-group-item" :key="product.product_id">
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-xs-9">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-xs-3">
                                                    <img :src="product.image_small_url" :alt="product.name+' '+product.quantity+' '+product.unit" class="img-responsive" height="50" width="50">
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-9">
                                                    @{{ product.name }} @{{ product.quantity }} @{{ product.unit }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2 text-center">
                                            <button :id="'product_'+product.product_id" :data-id="product.product_id" type="button" class="btn btn-primary btn-lg" @click="onClicked(product)">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                            {{-- <input :id="'product_'+product.id" :data-id="product.id" name="product" type="checkbox" :checked="product.added" @change="onChecked(product)" v-model="product.added" class="form-control"> --}}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="list-group products-not-fround" v-show="!isProductsLoaded">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                            No se encontraron productos
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>Productos contados</strong>
                            </div>
                            <form @submit.prevent="createCounting">
                                <div class="panel-body" v-show="isProductsAdded">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-xs-12 form-group">
                                            <label :for="storage_number">Posición</label>
                                            <input type="number" id="storage_number" name="storage_number" v-model.number="storage_number" class="form-control" @keypress="isNumber" :readonly="storage_number_disabled">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <label :for="storage_letter">Posición en altura</label>
                                            <input type="text" id="storage_letter" pattern="[A-Z]{1}"  name="storage_letter" v-model="storage_letter" class="form-control" :readonly="storage_letter_disabled">
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group products-added" v-show="isProductsAdded" style="max-height: 300px; overflow: auto;">
                                    <li v-for="(product, index) in products_added" class="list-group-item" :class="product.quantity_counted === 0 || (action == 'Almacenamiento' && product.handle_expiration_date == 1 && product.expiration_date == '')? 'list-group-item-danger' : (product.quantity_counted > 0 ? 'list-group-item-success' : '')">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-xs-2" v-show="action != 'Alistamiento'">
                                                <input type="checkbox" :checked="product.added" @change="onCheckedRemove(index, product)" v-model="product.added" class="form-control" v-show="action != 'Alistamiento'" height="50" width="50">
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-xs-2 text-center">
                                                <img :src="product.image_small_url" :alt="product.name+' '+product.quantity+' '+product.unit" height="50" width="50">
                                            </div>
                                            <div :class="'col-lg-'+(action == 'Alistamiento' ? 7 : 6)+' col-md-'+(action == 'Alistamiento' ? 7 : 6)+' col-xs-'+(action == 'Alistamiento' ? 7 : 6)">
                                                @{{ product.name }} @{{ product.quantity }} @{{ product.unit }}
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <input type="number" v-model="product.quantity_counted" class="form-control" @keypress="isNumber">
                                                <br>
                                                <button v-show="action == 'Almacenamiento' && product.handle_expiration_date" type="button" class="btn btn-primary btn-lg" @click="isUpdated(product, index)">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="panel-footer text-center" v-show="isProductsAdded">
                                    <button type="submit" class="btn btn-primary" :disabled="isSubmitDisable">Crear Conteo</button>
                                </div>
                            </form>
                            <ul class="list-group products-not-fround" v-show="!isProductsAdded">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                            No se encontraron productos
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div v-for="(product, index) in products_added" class="modal fade" :id="'modal_product_'+index" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form @submit.prevent="updateExpirationDate(product, index)">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Fecha de vencimiento del producto</h4>
                        </div>
                        <div class="modal-body">
                            <input type="date" v-model="product.expiration_date" class="form-control" required="required" min="{{ $date }}">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
    <!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
    <script src="https://unpkg.com/vue"></script>
    @include('admin.inventory.massive_counting.js.index-js')
@stop
