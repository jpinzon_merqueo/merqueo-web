@if ( count($products) )
    @foreach ($products as $product)
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-9">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <img src="{{ $product->image_app_url }}" alt="{{ $product->name }} {{ $product->quantity }} {{ $product->unit }}" class="img-responsive">
                        </div>
                        <div class="col-lg-9 col-md-9 col-xs-9">
                            {{ $product->name }} {{ $product->quantity }} {{ $product->unit }}
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-3"></div>
            </div>
        </li>
    @endforeach
@endif