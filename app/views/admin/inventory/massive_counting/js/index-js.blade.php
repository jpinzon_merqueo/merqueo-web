<script>
    new Vue({
        el: '#content',
        data: {
            city_id: {{ Session::get('admin_city_id') }},
            warehouse_id: {{ Session::get('admin_warehouse_id') }},
            storage: 'Seco',
            action: 'Almacenamiento',
            search: '',
            isDisabled: false,
            isSubmitDisable: false,
            isProductsLoaded: false,
            isProductsAdded: false,
            isLoading: false,
            storage_letter: '',
            storage_number: '',
            position: '',
            position_height: '',
            products: [],
            products_added: [],
            storage_number_disabled: false,
            storage_letter_disabled: false,
        },
        methods: {
            searchProduct: function (e) {
                if (this.action == 'Almacenamiento' &&  this.search.length < 3) {
                    alert('Debe ingresar al menos 3 letras en la búsqueda')
                    return
                }

                this.isDisabled = true
                this.isSubmitDisable = true
                this.isLoading = true
                $.ajax({
                    url: '{{ route('adminMassiveCounting.getProductsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: this.city_id,
                        warehouse_id: this.warehouse_id,
                        storage: this.storage,
                        action: this.action,
                        position: this.position,
                        position_height: this.position_height,
                        search: this.search
                    },
                    context: this
                })
                .done(function(data) {
                    if (data.length == 0) {
                        alert('No se encontraron productos para esta búsqueda.')
                        // this.resetForm()
                    }
                    if (this.action == 'Alistamiento') {
                        this.products_added = data
                        this.storage_number = this.position
                        this.storage_letter = this.position_height
                        this.storage_number_disabled = true
                        this.storage_letter_disabled = true
                    }else{
                        this.products = data
                    }
                })
                .fail(function() {
                })
                .always(function() {
                    this.isLoading = false
                    this.isSubmitDisable = false
                });
            },
            resetForm: function (e) {
                this.city_id = {{ $first_city->id }}
                this.storage = 'Seco'
                this.action = 'Almacenamiento'
                this.search = ''
                this.isProductsLoaded = false
                this.isProductsAdded = false
                this.position =  ''
                this.position_height =  ''
                this.storage_number_disabled = false
                this.storage_letter_disabled = false
                this.isDisabled = false
                this.products = []
                this.products_added = []
                this.storage_letter = ''
                this.storage_number = ''
                this.onCityChanged(this.city_id)
                $('.products-fround').html('')
            },
            createCounting: function (e) {
                if (this.storage_letter.length < 1) {
                    alert('El campo posición en altura no debe estar vacío.')
                    return
                }
                if (this.storage_number < 0) {
                    alert('El campo posición no debe estar vacío.')
                    return
                }

                let product_fail;
                product_fail = this.products_added.filter(function(product) {
                    return product.handle_expiration_date == 1 && product.expiration_date == '';
                });
                if (product_fail.length > 0 && this.action == 'Almacenamiento') {
                    alert('Debe ingresar las fechas de vencimientos de los productos.')
                    return
                }

                product_fail = this.products_added.filter(function(product) {
                    if (product.quantity_counted == '' || product.quantity_counted == 0) {
                        product.quantity_counted = 0;
                        return product.quantity_counted == '' || product.quantity_counted == 0;
                    }
                });
                if (product_fail.length > 0 && this.action == 'Almacenamiento') {
                    alert('Debe ingresar cantidades superiores a 0 en los productos contados.')
                    return
                }

                this.isLoading = true
                this.isSubmitDisable = true
                $.ajax({
                    url: '{{ route('adminMassiveCounting.createMassiveCountingAjax') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        city_id: this.city_id,
                        warehouse_id: this.warehouse_id,
                        action: this.action,
                        storage: this.storage,
                        storage_letter: this.storage_letter,
                        storage_number: this.storage_number,
                        products_added: this.products_added
                    },
                    context: this
                })
                .done(function(data) {
                    if (data.status) {
                        alert(data.message)
                    }else{
                        alert(data.message+' '+data.error)
                    }
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    this.resetForm()
                    this.isLoading = false
                    this.isSubmitDisable = false
                });
            },
            onClicked: function (product, e) {
                product.added = 1
                var copy = Object.assign({}, product);
                this.products_added.push(copy)
                /*if ( ! this.findProduct(product, this.products_added) ) {
                }else{
                    index = this.products_added.findIndex(x => x.id == product.id);
                    this.products_added.splice(index,1)
                    this.products_added.push(product)
                }*/
            },
            onCheckedRemove: function (index, product, e) {
                product.added = 0
                if (this.findProduct(product, this.products_added) ) {
                    this.products_added.splice(index, 1)
                }
            },
            findProduct: function (neddle, haystack) {
                var result = haystack.filter(function (element, index) {
                    if (element.id == neddle.id) {
                        return element;
                    }
                }, neddle)

                if ( result.length > 0 ) {
                    return result;
                }
                return false;
            },
            isNumber: function (evt) {
                evt = (evt) ? evt : window.event
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    evt.preventDefault()
                } else {
                    return true
                }
            },
            isUpdated: function (product, index, evt) {
                if (this.action == 'Almacenamiento') {
                    if (product.handle_expiration_date) {
                        $('#modal_product_'+index).modal({
                            backdrop: 'static',
                            keyboard: false,
                            show: true
                        })
                    }
                }
            },
            updateExpirationDate: function (product, index, evt) {
                if (product) {}
                $('#modal_product_'+index).modal('hide')
            },
            onCityChanged: function (city_id) {
                $('#warehouse_id').addClass('disabled');
                $.ajax({
                    url: "{{ route('admin.get_warehouses_ajax') }}",
                    data: { city_id: city_id },
                    type: 'GET',
                    dataType: 'json',
                    context: this,
                    success:
                        function(response) {
                            $('#warehouse_id').empty();
                            let index = null
                            $.each(response, function(key, value){
                                if (index == null) {
                                    index = key;
                                }
                                $('#warehouse_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                            this.warehouse_id = index
                            $('#warehouse_id').removeClass('disabled').trigger('change');
                        }
                });
            }
        },
        computed: {
            productsFound() {
                return this.products.filter(product => !product.added);
            },
            addedProducts(){
                return this.products_added.filter(product => product.added);
            }
        },
        watch: {
            storage_letter: function (val) {
                var letters = /^[A-Za-z]+$/;
                if ( val.match(letters) ) {
                    if ( val.length > 0 ) {
                        this.storage_letter = val[0].toUpperCase();
                    }
                }else{
                    this.storage_letter = ''
                }
            },
            position_height: function (val) {
                var letters = /^[A-Za-z]+$/;
                if ( val.match(letters) ) {
                    if ( val.length > 0 ) {
                        this.position_height = val[0].toUpperCase();
                    }
                }else{
                    this.position_height = ''
                }
            },
            products_added: function (val, oldVal) {
                if ( val.length == 0 ) {
                    this.isProductsAdded = false
                }else{
                    this.isProductsAdded = true
                }
            },
            products: function (val) {
                if ( val.length == 0 ) {
                    this.isProductsLoaded = false
                }else{
                    this.isProductsLoaded = true
                }
            }
        }
    });
</script>
