<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.1.0-beta.11/dist/vee-validate.min.js"></script>
<script>
    const messages = {
        _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
            if (max) {
                return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
            }

            return `El largo del campo ${field} debe ser ${length}.`;
        },
        max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field, [excluded]) => `El campo ${field} debe ser diferente a ${excluded}.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });
    let searchForm = new Vue({
        el: '#searchForm',
        data: {
            from: {
                city_id: {{ Session::get('admin_city_id') }},
                cities: {{ $cities->toJson() }},
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
                warehouses: {{ $warehouses->toJson() }},
                positionInfo: null,
                storage: '',
                action: '',
                position: '',
                heightPositionIds: [],
                position_height: ''
            },
            to: {
                warehouse_id: {{ Session::get('admin_warehouse_id') }},
                warehouses: {{ $warehouses->toJson() }},
                positionInfo: null,
                storage: '',
                position: '',
                action: '',
                position_height: '',
                heightPositionIds: []
            },
            productsFrom: [],
            productsTo: null,
            isLoading: false,
            transferProduct: false,
        },
        methods: {
            transferPositions: function () {
                let productsChecked = this.productsFrom.filter(product => {
                    return product.checked == true;
                });
                this.isLoading = true;
                axios.post('{{ route('adminMassiveTransferPicking.transferProducts') }}', {
                        warehouse_id : this.from.warehouse_id,
                        city_id: this.from.city_id,
                        storage_type: this.from.storage,
                        productsChecked: productsChecked,
                        from_height_Position: this.from.position_height,
                        to_height_Position: this.to.position_height,
                        from_position: this.from.position,
                        to_position: this.to.position,
                })
                    .then(response => {
                    alert('Se ha generado el traslado correctamente.');
                        this.validateFormFrom();
                        this.validateFormTo();
                this.isLoading = false;

            })
            .catch(error => {
                    alert(`${error.response.data}`);
            })
            .then(() => {
                    this.isLoading = false;
            });
            },

            getWarehousesFrom: function (e) {
                this.isLoading = true;
                this.from.warehouse_id = '';
                this.from.positionInfo = null;
                this.from.storage = '';
                this.productsFrom = [];
                this.productsTo = [];
                axios.get('{{ route('adminMassiveTransferPicking.getWarehousesAjax') }}', {
                    params: {
                        city_id: this.from.city_id,
                    }
                })
                    .then(response => {
                        this.from.warehouses = response.data;
                        this.to.warehouses = response.data;
                    })
                    .catch(error => {
                        alert(`${error.response.data}`);
                    })
                    .then(() => {
                        this.isLoading = false;
                    });
            },
            getStorageFrom: function (e){
                this.productsFrom = [];
                this.productsTo = [];
                this.to.warehouse_id = this.from.warehouse_id;
                if (this.from.warehouse_id === '') {
                    this.from.positionInfo = null;
                    this.from.storage = '';
                }
            },
            getType: function (e) {
                this.storage = '';
                this.heightPositionIds = [];
                this.heightPositions = [];
                this.positions = '';
            },
            getPositions: function (e) {
                this.to.storage = this.from.storage;
                this.productsFrom = [];
                this.productsTo = [];
                let warehouse = this.from.warehouses.filter(warehouse => {
                    return warehouse.id === this.from.warehouse_id;
            });
                this.from.position = '';
                this.from.position_height= '';
                if (warehouse.length > 0) {
                    if (this.from.storage === 'Seco'){
                        this.from.positionInfo = warehouse[0].dry_picking_position;
                        this.to.positionInfo = warehouse[0].dry_picking_position;
                    } else if (this.from.storage === 'Frío') {
                        this.from.positionInfo = warehouse[0].cold_picking_position;
                        this.to.positionInfo = warehouse[0].cold_picking_position;
                    } else {
                        this.from.positionInfo = null;
                        this.to.positionInfo = null;
                    }
                } else {
                    alert(`Ocurrió un error al cargar la información de la bodega`);
                }
            },
            validateFormFrom: function (e) {
                this.$validator.validate('from.*').then(result => {
                    if (!result) {
                        return false;
                    } else {
                        this.isLoading = true;
                        axios.post('{{ route('adminMassiveTransferPicking.getProductsAjax') }}', {
                                warehouse_id: this.from.warehouse_id,
                                storage: this.from.storage,
                                position: this.from.position,
                                position_height: this.from.position_height
                        })
                            .then(response => {
                                this.productsFrom = response.data;
                            })
                            .catch(error => {
                                alert(`${error.response.data}`);
                            })
                            .then(() => {
                                this.isLoading = false;
                            });
                    }
                });
            },
            validateFormTo: function (e) {
                this.$validator.validate('to.*').then(result => {
                    if (!result) {
                        return false;
                    } else {
                        this.transferProduct= false;
                        this.isLoading = true;
                        axios.post('{{ route('adminMassiveTransferPicking.getProductsAjax') }}', {
                            warehouse_id: this.from.warehouse_id,
                            storage: this.from.storage,
                            position: this.to.position,
                            position_height: this.to.position_height
                        })
                            .then(response => {
                                this.productsTo = response.data;
                                this.transferProduct= true;
                            })
                            .catch(error => {
                                alert(`${error.response.data}`);
                            })
                            .then(() => {
                                this.isLoading = false;
                            });
                    }
                });
            },
            clearProduct: function(type){
                if (type == 1) {
                    this.productsFrom = []
                }
                if (type == 2) {
                    this.transferProduct= false;
                    this.productsTo = []
                }
            }
        }
    });
    searchForm.$validator.dictionary.locale = 'es';
</script>