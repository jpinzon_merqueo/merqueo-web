@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="searchForm">
        @if($errors->all())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> Ocurrieron los siquientes errores
                <ul>
                    @foreach($errors->all()as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Éxito!</b> {{ Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <form @submit.prevent="validateFormFrom">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <label for="">Desde:</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group">
                                    <label for="city_id">Ciudad</label>
                                    <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Ciudad" name="city_id" id="city_id" class="form-control" v-model="from.city_id" @change="getWarehousesFrom" :disabled="isLoading">
                                        <option v-for="(city, index) in from.cities" :value="city.id">@{{ city.city }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('from.city_id')">@{{ errors.first('from.city_id') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group">
                                    <label for="warehouse_id">Bodega</label>
                                    <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Bodega" name="warehouse_id" id="warehouse_id" class="form-control" v-model="from.warehouse_id" @change="getStorageFrom" :disabled="isLoading">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="(warehouse, index) in from.warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('from.warehouse_id')">@{{ errors.first('from.warehouse_id') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group" v-if="from.warehouse_id !== ''">
                                    <label for="position">Almacenamiento:</label>
                                    <select v-validate="'required'" data-vv-scope="from" data-vv-as="Almacenamiento" name="storage" id="storage" class="form-control" v-model="from.storage" @change="getStoragesPositionFrom" :disabled="isLoading">
                                        <option value="">-Selecciona-</option>
                                        <option value="Seco" v-if="from.warehouse_id !== ''">Seco</option>
                                        <option value="Frío" v-if="from.warehouse_id !== ''">Frío</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('from.storage')">@{{ errors.first('from.storage') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group" v-if="from.storage !== '' && from.positionInfo !== null">
                                    <label for="position">Posición:</label>
                                    <input @change="clearProduct(1)" v-validate="`required|integer|between:${from.positionInfo.start_position},${from.positionInfo.end_position}`" data-vv-scope="from" data-vv-as="Posición" name="position" id="position" type="text" class="form-control" v-model="from.position" :disabled="isLoading">
                                    <span style="color: #d73925" v-show="errors.has('from.position')">@{{ errors.first('from.position') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group" v-if="from.storage !== '' && from.positionInfo !== null">
                                    <label for="position_height">Posición en altura:</label>
                                    <select @change="clearProduct(1)" v-validate="`required`" data-vv-scope="from" data-vv-as="Posición en altura" name="position_height" id="position_height" class="form-control" v-model="from.position_height" :disabled="isLoading">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="(position, index) in from.positionInfo.height_positions" :value="position">@{{ position }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('from.position_height')">@{{ errors.first('from.position_height') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <img class="u-loader" src="{{ asset('assets/img/loading.gif') }}" alt="" v-show="isLoading">
                                    <button type="submit" class="btn btn-primary" :disabled="isLoading">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-6" v-show="productsFrom.length > 0">
                <div class="box box-primary">
                    <div class="box-body">
                        <form @submit.prevent="validateFormTo">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <label for="">Hacia:</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group">
                                    <label for="city_id">Ciudad</label>
                                    <select v-validate="'required|integer|min:1'" data-vv-scope="to" data-vv-as="Ciudad" name="city_id" id="city_id" class="form-control" v-model="to.city_id" disabled="">
                                        <option v-for="(city, index) in to.cities" :value="city.id">@{{ city.city }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('to.city_id')">@{{ errors.first('to.city_id') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group">
                                    <label for="warehouse_id">Bodega</label>
                                    <select v-validate="'required|integer|min:1'" data-vv-scope="to" data-vv-as="Bodega" name="warehouse_id" id="warehouse_id" class="form-control" v-model="to.warehouse_id" disabled="">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="(warehouse, index) in to.warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('to.warehouse_id')">@{{ errors.first('to.warehouse_id') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group" v-if="to.warehouse_id !== ''">
                                    <label for="position">Almacenamiento:</label>
                                    <select v-validate="'required'" data-vv-scope="to" data-vv-as="Almacenamiento" name="storage" id="storage" class="form-control" v-model="to.storage" disabled="">
                                        <option value="">-Selecciona-</option>
                                        <option value="Seco" v-if="to.warehouse_id !== ''">Seco</option>
                                        <option value="Frío" v-if="to.warehouse_id !== ''">Frío</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('to.storage')">@{{ errors.first('to.storage') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group" v-if="to.storage !== '' && to.positionInfo !== null">
                                    <label for="position">Posición:</label>
                                    <input @change="clearProduct(2)" v-validate="`required|integer|between:${to.positionInfo.start_position},${to.positionInfo.end_position}${from.position == to.position && from.position_height == to.position_height ? '|excluded:'+from.position : ''}`" data-vv-scope="to" data-vv-as="Posición" name="position" id="position" type="text" class="form-control" v-model="to.position" :disabled="isLoading">
                                    <span style="color: #d73925" v-show="errors.has('to.position')">@{{ errors.first('to.position') }}</span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12 form-group" v-if="to.storage !== '' && to.positionInfo !== null">
                                    <label for="position_height">Posición en altura:</label>
                                    <select @change="clearProduct(2)" v-validate="`required${from.position == to.position && from.position_height == to.position_height ? '|excluded:'+from.position_height : ''}`" data-vv-scope="to" data-vv-as="Posición en altura" name="position_height" id="position_height" class="form-control" v-model="to.position_height" :disabled="isLoading">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="(position, index) in to.positionInfo.height_positions" :value="position">@{{ position }}</option>
                                    </select>
                                    <span style="color: #d73925" v-show="errors.has('to.position_height')">@{{ errors.first('to.position_height') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <img class="u-loader" src="{{ asset('assets/img/loading.gif') }}" alt="" v-show="isLoading">
                                    <button type="submit" class="btn btn-primary" :disabled="isLoading">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" v-show="productsTo !== null && to.position !== '' && to.position_height !== '' && !errors.has('to.*') && productsFrom.length > 0">
            <form>
                <input type="hidden" name="warehouse_id" v-model="from.warehouse_id">
                <input type="hidden" name="storage_type" v-model="from.storage">
                <input type="hidden" name="position_from" v-model="from.position">
                <input type="hidden" name="position_to" v-model="to.position">
                <input type="hidden" name="position_height_from" v-model="from.position_height">
                <input type="hidden" name="position_height_to" v-model="to.position_height">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center form-group">
                    <button type="submit" class="btn btn-danger" :disabled="isLoading" @click="transferPositions">Trasladar</button>
                </div>
            
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-xs-12" id="productsFrom">
                <div class="box box-primary">
                    <div class="box-body" style="max-height: 450px; overflow-y: auto;">
                        <label for="">Desde:</label>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Referencia</th>
                                    <th>Imagen</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <tr v-for="(product, index) in productsFrom">
                                    <td>@{{ index + 1 }}</td>
                                    <td><input type="checkbox" id="checkbox" v-model="product.checked"></td>
                                    <td>@{{ product.store_product.product.id }}</td>
                                    <td>@{{ product.store_product.product.reference }}</td>
                                    <td><img :src="product.store_product.product.image_small_url" alt="" class="img-responsive"></td>
                                    <td>@{{ `${product.store_product.product.name} ${product.store_product.product.quantity} ${product.store_product.product.unit}` }}</td>
                                    <td>@{{ product.quantity }}</td>
                                </tr>
                                <tr v-show="productsFrom.length< 1">
                                    <td class="text-center" colspan="7">No se encontraron productos</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-xs-12" id="productsTo" v-show="productsFrom.length > 0">
                <div class="box box-primary">
                    <div class="box-body" style="max-height: 450px; overflow-y: auto;">
                        <label for="">Hacia:</label>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Referencia</th>
                                    <th>Imagen</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(product, index) in productsTo">
                                    <td>@{{ index + 1 }}</td>
                                    <td>@{{ product.store_product.product.id }}</td>
                                    <td>@{{ product.store_product.product.reference }}</td>
                                    <td><img :src="product.store_product.product.image_small_url" alt="" class="img-responsive"></td>
                                    <td>@{{ `${product.store_product.product.name} ${product.store_product.product.quantity} ${product.store_product.product.unit}` }}</td>
                                    <td>@{{ product.quantity }}</td>
                                </tr>
                                <tr v-show="productsTo !== null && productsTo.length < 1">
                                    <td class="text-center" colspan="7">No se encontraron productos</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <form action=""></form>
            </div>
        </div>
    </section>

    @include('admin.inventory.massive_transfer.js.index-js')
@endsection