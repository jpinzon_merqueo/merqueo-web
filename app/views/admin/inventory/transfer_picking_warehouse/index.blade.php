@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content" id="searchForm">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="city_id">Ciudad origen</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="from" data-vv-as="Ciudad origen"
                                name="from_city_id" id="from_city_id" class="form-control" v-model="from.from_city_id"
                                @change="getWarehousesFrom" :disabled="isLoading">
                            <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.from_city_id')">@{{ errors.first('from.from_city_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Bodega origen</label>
                        <select v-validate="`required|integer`" data-vv-scope="from" data-vv-as="Bodega origen"  @change="clearProduct()"
                                name="from_warehouse_id" id="from_warehouse_id" class="form-control" v-model="from.from_warehouse_id"
                                :disabled="isLoading">
                            <option value="">-Selecciona-</option>
                            <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{
                                warehouse.warehouse }}
                            </option>
                        </select>

                        <span style="color: #d73925" v-show="errors.has('from.from_warehouse_id')">@{{ errors.first('from.from_warehouse_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="city_id">Ciudad destino</label>
                        <select v-validate="'required|integer|min:1'" data-vv-scope="to" data-vv-as="Ciudad destino"
                                name="to_city_id" id="to_city_id" class="form-control" v-model="from.to_city_id"
                                @change="getWarehousesTo" :disabled="isLoading">
                            <option v-for="(city, index) in cities" :value="city.id">@{{ city.city }}</option>
                        </select>
                        <span style="color: #d73925" v-show="errors.has('from.to_city_id')">@{{ errors.first('from.to_city_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="warehouse_id">Bodega destino</label>
                        <select v-validate="`required|integer|excluded:${from.from_warehouse_id}`" data-vv-scope="from" data-vv-as="Bodega destino"  @change="clearProduct()"
                                name="to_warehouse_id" id="to_warehouse_id" class="form-control" v-model="from.to_warehouse_id"
                                :disabled="isLoading">
                            <option value="">-Selecciona-</option>
                            <option v-for="(warehouse, index) in towarehouses" :value="warehouse.id">@{{
                                warehouse.warehouse }}
                            </option>
                        </select>

                        <span style="color: #d73925" v-show="errors.has('from.to_warehouse_id')">@{{ errors.first('from.to_warehouse_id') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <label for="product_info">Producto</label>
                        <input v-validate="'required'" data-vv-scope="from" data-vv-as="Producto"
                               name="product_info" id="product_info" @keyup.enter="getProductAjax()"
                               class="form-control" v-model="from.product_info" :disabled="isLoading"
                               placeholder="Referencia" ref="reference">
                        <span style="color: #d73925" v-show="errors.has('from.product_info')">@{{ errors.first('from.product_info') }}</span>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12 form-group">
                        <br/>
                        <button class="btn btn-primary" @click="getProductAjax()" :disabled="isLoading">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body" v-if="product!=null">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <h3>Origen: @{{ product.store_product_warehouses[0].warehouse.warehouse }}</h3>
                                <h4>Producto: @{{ product.product.name }}</h4>
                                <h4>Referencia: @{{ product.product.reference }}</h4>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <img :src="product.product.image_medium_url" alt=""
                                     class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row" v-if="product!=null">
                            <div class="col-lg-12 col-md-12">
                                <h4>Alistamiento</h4>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th>Cantidad de picking</th>
                                    <th>Cantidad a trasladar</th>
                                    <th>Posición</th>
                                    <th>Altura</th>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(data) in product.store_product_warehouses" class="text-center" v-show="product.store_product_warehouses.length>0">
                                        <td>@{{ data.picking_stock}}</td>
                                        <td>
                                            <template v-if="data.picking_stock>0">
                                            <input  v-validate = "`required|integer|min_value:1|max_value:${data.picking_stock}`" data-vv-scope="data" data-vv-as="Trasladar"
                                                   name="to_quantity" id="to_quantity"
                                                   class="form-control" v-model="data.to_quantity" :disabled="isLoading"
                                                   placeholder="Cantidad a trasladar" >
                                            <span style="color: #d73925" v-show="errors.has('data.to_quantity')">@{{ errors.first('data.to_quantity') }}</span>
                                            </template>
                                            <template v-if="data.picking_stock<=0">
                                                No tiene productos para trasladar
                                            </template>

                                        </td>
                                        <td>@{{ data.storage_position}}</td>
                                        <td>@{{ data.storage_height_position}}</td>
                                    </tr>

                                    <tr v-show="product.store_product_warehouses.length< 1">
                                        <td class="text-center" colspan="3">No se encontraron productos</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body" v-if="toproduct!=null">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <h3>Destino:@{{ toproduct.store_product_warehouses[0].warehouse.warehouse }}</h3>
                                <h4>Producto: @{{ toproduct.product.name }}</h4>
                                <h4>Referencia: @{{ toproduct.product.reference }}</h4>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <img :src="toproduct.product.image_medium_url" alt=""
                                     class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row" v-if="toproduct!=null">
                            <div class="col-lg-12 col-md-12">
                                <h4>Alistamiento</h4>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th>Cantidad de picking</th>
                                    <th>Posición</th>
                                    <th>Altura</th>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(data) in toproduct.store_product_warehouses" class="text-center" v-show="toproduct.store_product_warehouses.length>0">
                                        <td>@{{ data.picking_stock}}</td>
                                        <td>@{{ data.storage_position}}</td>
                                        <td>@{{ data.storage_height_position}}</td>
                                    </tr>

                                    <tr v-show="toproduct.store_product_warehouses.length< 1">
                                        <td class="text-center" colspan="3">No se encontraron productos</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" @click="transferProduct()" :disabled="isLoading">Trasladar</button>
                    </div>

                </div>
            </div>
        </div>

    </section>

    @include('admin.inventory.transfer_picking_warehouse.js.index-js')
@endsection