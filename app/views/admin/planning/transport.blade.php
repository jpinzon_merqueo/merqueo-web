@extends('admin.layout')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
    @if(Session::get('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::get('error'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif
    @if(Session::get('list_error'))
        <div class="alert alert-danger">
            <h4>Errores</h4>
            {{ Session::get('list_error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <fieldset>
                                <legend>Selecciona ciudad</legend>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-3">
                            <label>Ciudad</label>
                            <select id="city_id" name="city_id" class="form-control">
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id || Session::get('city_id') == $city->id) selected="selected" @endif >{{ $city->city }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-3">
                            <label>Bodega</label>
                            <select id="warehouse_id" name="warehouse_id" class="form-control">
                                @foreach ($warehouses as $warehouse)
                                    <option value="{{ $warehouse->id }}" @if(Session::get('warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                @endforeach
                            </select>
                        </div>
                        &nbsp;&nbsp;&nbsp;<img src="{{ asset_url() }}/img/loading.gif" class="unseen loading-city" />
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <form action="{{ route('adminPlanning.validateRoutes') }}" role="form" method="POST" id="validate-form" accept-charset="UTF-8">
                        <div class="row">
                            <div class="col-xs-12">
                                @if(Session::get('assign_exception'))
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ Session::get('assign_exception') }}
                                </div>
                                @endif
                                @if(Session::get('validate_success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <b>Hecho!</b> Hay {{ Session::get('validate_success') }} pedidos válidos.
                                </div>
                                @endif
                                @if(Session::get('validate_errors'))
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <b>Alerta!</b> Se encontraron errores en los siguientes pedidos.
                                    <hr>
                                    @foreach (Session::get('validate_errors') as $key => $error)
                                    <fieldset>
                                        <label>Pedido # <a target="_blank" href="@if ( $error['is_storage'] ){{ route('adminOrderStorage.details', ['id' => $key]) }}@else{{ route('adminOrders.details', ['id' => $key]) }}@endif">{{ $key }}</a></label>
                                        <ul>
                                            @if ( isset($error['driver_id']) )
                                                <li>
                                                    {{ $error['driver_id'] }}
                                                </li>
                                            @endif
                                            @if ( isset($error['vehicle_id']) )
                                                <li>
                                                    {{ $error['vehicle_id'] }}
                                                </li>
                                            @endif
                                            @if ( isset($error['products']) )
                                                <li>
                                                    {{ $error['products'] }}
                                                </li>
                                            @endif
                                            @if ( isset($error['payment_method']) )
                                                <li>
                                                    {{ $error['payment_method'] }}
                                                </li>
                                            @endif
                                            @if ( isset($error['status']) )
                                                <li>
                                                    {{ $error['status'] }}
                                                </li>
                                            @endif
                                        </ul>
                                    </fieldset>
                                    <hr>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Validar pedidos de ruta</legend>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-4">
                                <label>Ruta</label>
                                @if(!empty($routes))
                                    <input type="hidden" name="city_id" class="city_id">
                                    <input type="hidden" name="warehouse_id" class="warehouse_id">
                                    <select class="form-control required" name="route_id" id="validate_route">
                                        <option value="">-Selecciona-</option>
                                        @foreach ($routes as $route)
                                            <option value="{{ $route->id }}" @if(Session::get('route_id') == $route->id) selected="selected" @endif>{{ $route->route }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="col-xs-8 text-left">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <label style="display: block;">&nbsp</label>
                                        <button type="submit" id="btn-validate-route" class="btn btn-primary">Validar ruta</button>
                                    </div>
                                    @if($admin_permissions['permission2'])
                                    <div class="col-xs-2">
                                        <label style="display: block;">&nbsp</label>
                                        <button type="button" id="btn-route-reassign" class="btn btn-warning">Dividir ruta</button>
                                    </div>
                                    @endif
                                    <div class="col-xs-2">
                                        <label style="display: block;">&nbsp</label>
                                        <img src="{{ asset_url() }}/img/loading.gif" class="unseen loading-vehicle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <form action="{{ route('adminPlanning.assignTransporters') }}" role="form" method="post" id="assign-form" accept-charset="UTF-8">
                        <input type="hidden" name="city_id" class="city_id">
                        <input type="hidden" name="warehouse_id" class="warehouse_id">
                        <div class="row">
                            @if(Session::get('assign_success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b>Hecho!</b> Se han actualizado {{ Session::get('assign_success') }} pedidos.
                            </div>
                            @endif
                            @if(Session::get('assign_errors'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b>Alerta!</b> Se encontraron errores en los siguientes pedidos.
                                <hr>
                                @foreach (Session::get('assign_errors') as $key => $error)
                                <fieldset>
                                    <label>Pedido # <a target="_blank" href="{{ route('adminOrderStorage.details', ['id' => $key]) }}">{{ $key }}</a></label>
                                    <ul>
                                        @if ( isset($error['driver_id']) )
                                            <li>
                                                {{ $error['driver_id'] }}
                                            </li>
                                        @endif
                                        @if ( isset($error['vehicle_id']) )
                                            <li>
                                                {{ $error['vehicle_id'] }}
                                            </li>
                                        @endif
                                        @if ( isset($error['products']) )
                                            <li>
                                                {{ $error['products'] }}
                                            </li>
                                        @endif
                                        @if ( isset($error['payment_method']) )
                                            <li>
                                                {{ $error['payment_method'] }}
                                            </li>
                                        @endif
                                        @if ( isset($error['status']) )
                                            <li>
                                                {{ $error['status'] }}
                                            </li>
                                        @endif
                                    </ul>
                                </fieldset>
                                <hr>
                                @endforeach
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Asignar ruta completa a conductor</legend>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-3">
                                <label>Ruta</label>
                                @if (!empty($routes))
                                    <select class="route_id route form-control required" name="route_id">
                                        <option value="">-Selecciona-</option>
                                        @foreach ($routes as $route)
                                            <option value="{{ $route->id }}" @if(Session::get('route_id') == $route->id) selected="selected" @endif>{{ $route->route }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="form-group col-xs-3">
                                <label>Transportador</label>
                                <select class="form-control" name="transporter_id" id="transporter_id">
                                    <option value="">-Selecciona-</option>
                                    @if (!empty($transporters))
                                        @foreach ($transporters as $transporter)
                                            <option value="{{ $transporter->id }}">{{ $transporter->fullname }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-xs-3">
                                <label>Vehículo</label>
                                <select class="form-control required" name="vehicle_id" id="vehicle_id">
                                    <option value="">-Selecciona-</option>
                                    @if (!empty($vehicles))
                                        @foreach ($vehicles as $vehicle)
                                            <option value="{{ $vehicle->id }}">{{ $vehicle->plate }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-xs-3">
                                <label>Conductor</label>
                                <select class="form-control required" name="driver_id" id="driver_id">
                                    <option value="">-Selecciona-</option>
                                    @if (!empty($drivers))
                                        @foreach ($drivers as $driver)
                                            <option value="{{ $driver->id }}">{{ $driver->first_name.' '.$driver->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 text-right">
                                <button type="submit" id="btn-asignar" class="btn btn-primary">Asignar ruta</button>&nbsp;&nbsp;&nbsp;
                                @if($admin_permissions['permission1'])
                                <button type="submit" id="btn-reassign" class="btn btn-warning">Reasignar ruta</button>&nbsp;&nbsp;&nbsp;
                                @endif
                                <button type="submit" id="btn-order-departure" class="btn btn-primary" target="_blank">Generar planilla de salida</button>&nbsp;&nbsp;&nbsp;
                                <button type="button" id="btn-download-route-voucher" class="btn btn-primary" data-toggle="modal" data-target="#modal-download-route-voucher-order">Generar recibos</button>
                            </div>
                        </div>
                        <div class="row unseen loading-vehicle">
                            <div class="col-xs-12 text-center">
                                <br><img src="{{ asset_url() }}/img/loading.gif"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Order departure modal -->
<div class="modal fade" id="modal-order-departure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="order-departure-form" action="{{ route('adminPlanning.pdfOrderDeparture') }}" method="POST" accept-charset="UTF-8">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Generar Planilla de Salida de Cargue</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-xs-10">
                                        <table width="100%">
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <tr>
                                                        <td align="left"><label>No. de canastillas:</label>&nbsp;</td>
                                                        <td><input type="number" placeholder="Total de canastillas" id="layette_number" name="layette_number" class="form-control required"></td>
                                                    </tr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <tr>
                                                        <td align="left"><label>No. de bolsas:</label>&nbsp;</td>
                                                        <td><input type="number" placeholder="Total de bolsas" id="bag_number" name="bag_number" class="form-control required"></td>
                                                    </tr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <tr>
                                                        <td align="left"><label>Datafono:</label>&nbsp;</td>
                                                        <td>
                                                            <select class="form-control required" id="dataphone" name="dataphone">
                                                                <option value="" selected>-Selecciona-</option>
                                                                <option value="1">Sí</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <tr>
                                                        <td align="left"><label>Tarjetas de visita:</label>&nbsp;</td>
                                                        <td>
                                                            <select class="form-control required" id="visiting_card" name="visiting_card">
                                                                <option value="" selected>-Selecciona-</option>
                                                                <option value="1">Sí</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <tr>
                                                        <td align="left"><label>Dotación (Canguro, Chaqueta, Gorra):</label>&nbsp;</td>
                                                        <td>
                                                            <select class="form-control required" id="endowment" name="endowment">
                                                                <option value="" selected>-Selecciona-</option>
                                                                <option value="1">Sí</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <tr>
                                                        <td align="left"><label>Bolsas:</label>&nbsp;</td>
                                                        <td>
                                                            <select class="form-control required" id="bags" name="bags">
                                                                <option value="" selected>-Selecciona-</option>
                                                                <option value="1">Sí</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </div>
                                            </div>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="order_departure_city_id" id="order_departure_city_id" value="{{ Session::get('admin_city_id') }}">
                    <input type="hidden" name="order_departure_route_id" id="order_departure_route_id">
                    <input type="hidden" name="order_departure_driver_id" id="order_departure_driver_id">
                    <input type="hidden" name="order_departure_vehicle_id" id="order_departure_vehicle_id">
                    <button type="submit" class="btn btn-success" id="btn-generate-order-departure">&nbsp;Generar&nbsp;</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-reasign-route">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Formulario para la reasignación de rutas</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" id='form-route-reassign' autocomplete="off">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group" id="route-reassign-type">
                                {{ Form::label('reasign_route_type', 'Tipo', ['class' => 'control-label']) }}
                                {{ Form::fillSelectWithoutKeys('reasign_route_type', ['a' => 'Apoyo', 'c' => 'Canguro'], '', array('class' => 'form-control required', 'id' => 'reasign_route_type'), TRUE) }}
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group" id="route-reassign-driver">
                                {{ Form::label('reasign_route_driver_id', 'Conductor', ['class' => 'control-label']) }}
                                <select class="form-control required" name="reasign_route_driver_id" id="reasign_route_driver_id">
                                    <option value="">Seleccione</option>
                                    @if (!empty($drivers))
                                        @foreach ($drivers as $driver)
                                            <option value="{{ $driver->id }}">{{ $driver->first_name.' '.$driver->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group" id="route-reassign-vehicle">
                                {{ Form::label('reasign_route_vehicle_id', 'Vehículo', ['class' => 'control-label']) }}
                                {{ Form::fillSelectWithKeys('reasign_route_vehicle_id', !empty($vehicles) ? $vehicles : [], '', array('class' => 'form-control required', 'id' => 'reasign_route_vehicle_id'), 'id', 'plate', TRUE) }}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="table-orders" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Cliente</th>
                                            <th>Dirección</th>
                                            <th>Cantidad de productos</th>
                                            <th>Secuencia de planeación</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{ Form::token() }}
                    {{ Form::hidden('route_id', '', ['id' => 'reasign_route_id']) }}
                    {{ Form::hidden('_method', 'POST') }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="form-route-submit" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal recibo de bodega por pedido -->
<div  class="modal fade" id="modal-download-route-voucher-order" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Generar recibos</h4>
            </div>
            <form action="{{ route('adminPlanning.download_route_voucher') }}" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label>Ruta</label>
                            @if (!empty($routes))
                                <select class="route_id route form-control" name="route_id">
                                    <option value="">-Selecciona-</option>
                                    @foreach ($routes as $route)
                                        <option value="{{ $route->id }}" @if(Session::get('route_id') == $route->id) selected="selected" @endif>{{ $route->route }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="order_id">Pedido</label>
                            <input type="number" name="order_id" id="order_id" class="form-control" placeholder="# pedido (opcional)">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Generar recibos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<script type="text/javascript">
    var vehicle_id_route = 0;
    var transporter_id_route = 0;
    var driver_id_route = 0;

$(document).ready(function(){

    $('#assign-form').validate();
    $('#validate-form').validate();
    $('#order-departure-form').validate();
    $('#form-route-reassign').validate();

    function show_modal(action, reference) {
        var ref = reference || 0;
        $('#modal-' + action).modal('show');
    }

    var assignTransporter = (function() {
        'use strict';

        function assignTransporter() {
            // enforces new
            if (!(this instanceof assignTransporter)) {
                return new assignTransporter();
            }
            // constructor body
            this.url_get_vehicles = '{{ route('adminPlanning.getVehiclesAjax') }}';
            this.url_get_drivers = '{{ route('adminPlanning.getDriversAjax') }}';
            this.url_change_city = '{{ route('adminPlanning.getChangeCity') }}';
            this.transporter_id = null;
        }

        assignTransporter.prototype.bindActions = function() {
            var self = this;
            $('body').on('change', '#transporter_id', function(event) {
                event.preventDefault();
                self.transporter_id = $('#transporter_id').val();
                self.get_vehicles_ajax();
//                self.get_drivers_ajax();
            });
            $('body').on('change', '#vehicle_id', function(event) {
                self.get_drivers_ajax();
            });
        };

        assignTransporter.prototype.get_vehicles_ajax = function() {
            $.ajax({
                url: this.url_get_vehicles,
                type: 'GET',
                dataType: 'json',
                data: {
                    transporter_id: this.transporter_id
                }
            })
            .done(function(data) {
                $('#vehicle_id').empty();
                var html = '<option value="">-Selecciona-</option>';
                if ( data.length > 0 ) {
                    $.each(data, function(index, val) {
                        var selected = '';
                        if(vehicle_id_route == val.id)
                            selected = 'selected="selected"';
                        html += '<option value="'+ val.id +'" '+selected+'>'+ val.plate +'</option>';
                    });
                }
                $('#vehicle_id').html(html);
                $('#vehicle_id').change();
            })
            .fail(function(data) {
                console.error("error al obtener los vehiculos.");
            })
        };

        assignTransporter.prototype.get_drivers_ajax = function() {
            $.ajax({
                url: this.url_get_drivers,
                type: 'GET',
                dataType: 'json',
                data: {
                    vehicle_id: $("#vehicle_id").val()
                }
            })
            .done(function(data) {
                $('#driver_id').empty();
                var html = '<option value="">-Selecciona-</option>';
                if ( data.length > 0 ) {
                    $.each(data, function(index, val) {
                        var selected = '';
                        if(driver_id_route == val.id)
                            selected = 'selected="selected"';
                        html += '<option value="'+ val.id +'"' +selected+'>'+ val.first_name + ' ' + val.last_name + '</option>';
                    });
                }
                $('#driver_id').html(html);
            })
            .fail(function(data) {
                console.error("error al obtener los conductores.");
            })
        };

        assignTransporter.prototype.set_data = function() {
            this.transporter_id = $('#transporter_id').val();
        };

        return assignTransporter;
    }());

    $('body').on('click', '#assign-form button[type="submit"]', function(event) {
        event.preventDefault();
        var messageValidation = '';
        if (vehicle_id_route > 0 && vehicle_id_route != $('#vehicle_id').val()) {
            messageValidation+= '\n El vehículo sugerido es diferente al seleccionado.';
        }
        if ($('#assign-form').valid()){
            if ($(this).attr('id') == 'btn-asignar'){
                if (confirm('¿Esta seguro que desea realizar la asignación?'+messageValidation)) {
                    $('.city_id').val($('#city_id').val());
                    $('.warehouse_id').val($('#warehouse_id').val());
                    $(this).addClass('disabled');
                    $('#assign-form .loading-vehicle').removeClass('unseen');
                    $('#assign-form').attr('action', '{{ route('adminPlanning.assignTransporters') }}');
                    $('#assign-form').submit();
                }
            }
            if ($(this).attr('id') == 'btn-reassign'){
                if (confirm('¿Esta seguro que desea realizar la reasignación?'+messageValidation)) {
                    $('.city_id').val($('#city_id').val());
                    $('.warehouse_id').val($('#warehouse_id').val());
                    $(this).addClass('disabled');
                    $('#assign-form .loading-vehicle').removeClass('unseen');
                    $('#assign-form').attr('action', '{{ route('adminPlanning.reassignTransporters') }}');
                    $('#assign-form').submit();
                }
            }
            if ($(this).attr('id') == 'btn-order-departure') {
                show_modal('order-departure');
            }
        }
    });

    $('body').on('click', '#validate-form button[type="submit"]', function(event) {
        if ($('#validate-form').valid()){
            $('.city_id').val($('#city_id').val());
            $('.warehouse_id').val($('#warehouse_id').val());
            $(this).addClass('disabled');
            $('#validate-form .loading-vehicle').removeClass('unseen');
        }
    });

    $('body').on('click', '#btn-route-reassign', function(event) {
        event.preventDefault();
        if ($('#validate-form').valid()) {
            $('.city_id').val($('#city_id').val());
            $('.warehouse_id').val($('#warehouse_id').val());
            $('#validate-form .loading-vehicle').removeClass('unseen');
            var route_id = $('#validate_route').val(),
                $form = $('#form-route-reassign'),
                $this = $(this),
                url = "{{ route('adminPlanning.transport') }}/" + route_id + '/reasign-route';
            $this.addClass('disabled');
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
            })
            .done(function(data) {
                $('#validate-form .loading-vehicle').addClass('unseen');
                $this.removeClass('disabled');
                if (data.status) {
                    $form[0].reset();
                    $form.find('#reasign_route_id').val(route_id);
                    $form.find('tbody').html('');
                    $.each(data.orders, function(index, order) {
                         var item = '<tr class="' + order.status + '" id="order-' + order.id + '">';
                         item = item + '<td><input name="order[' + order.id + ']" value="' + order.id + '" type="checkbox"></td>';
                         item = item + '<td>' + order.order_group.user_firstname + ' ' + order.order_group.user_lastname + '</td>';
                         item = item + '<td>' + order.order_group.user_address + '</td>';
                         item = item + '<td>' + order.order_group.products_quantity + '</td>';
                         item = item + '<td>' + order.planning_sequence + '</td>';
                         $form.find('tbody').append(item);
                    });
                    show_modal('reasign-route');
                } else {
                    swal(
                      'Oops...',
                      data.message,
                      'error'
                    );
                }
            })
            .fail(function() {
                $('#validate-form .loading-vehicle').addClass('unseen');
                $this.removeClass('disabled');
                swal(
                  'Oops...',
                  '¡Al parecer tenemos problemas tratando de obtener la información!',
                  'error'
                );
            });
        }
    });

    $('#reasign_route_type').on('change', function() {
        var $this = $(this),
            $form = $('#form-route-reassign');
            $trs  = $form.find('tbody tr');
        switch($this.val()) {
            case 'a':
                $trs.show();
                break;
            case 'c':
                $trs.each(function(index, element) {
                    if ($(element).hasClass('Dispatched')) {
                        $(element).show();
                    } else {
                        $(element).hide()
                            .find('input[type=checkbox]')
                            .prop('checked', false);
                    }
                });
                break;
            default:
                $trs.show();
                break;
        }
    });

    $('#form-route-submit').on('click', function(event) {
        event.preventDefault();
        var $marks = $('#form-route-reassign input[type=checkbox]').filter(':checked');
        if ($marks.length) {
            if ($('#form-route-reassign').valid()) {
                var route_id = $('#reasign_route_id').val(),
                    $form = $('#form-route-reassign'),
                    $this = $(this),
                    data  = $form.serialize(),
                    url = "{{ route('adminPlanning.transport') }}/" + route_id + '/reasign-route';
                $this.addClass('disabled');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                })
                .done(function(data) {
                    $this.removeClass('disabled');
                    if (data.status) {
                        $('#modal-reasign-route').modal('hide');
                        swal(
                          '¡Bien hecho!',
                          data.message,
                          'success'
                        );
                    } else {
                        swal(
                          'Oops...',
                          data.message,
                          'error'
                        );
                    }
                })
                .fail(function() {
                    $this.removeClass('disabled');
                    swal(
                      'Oops...',
                      '¡Al parecer tenemos problemas tratando de obtener la información!',
                      'error'
                    );
                });
            } else {
                swal(
                  'Oops...',
                  'Debes diligenciar correctamente todos los datos necesarios.',
                  'error'
                );
            }
        } else {
            swal(
              'Oops...',
              'Debes seleccionar al menos una Orden para ser reasignada.',
              'error'
            );
        }
    });

    var assign_transporter = new assignTransporter;
    assign_transporter.bindActions();

    $('#city_id').change(function(e) {
        $('#order_departure_city_id').val($(this).val());
        $('#validate_route, .route, #vehicle_id, #driver_id, #transporter_id').addClass('disabled');
        $('#warehouse_id').html('').append($("<option value=''>Selecciona</option>"));
        $('.loading-city').removeClass('unseen');
        $.ajax({
            url: '{{ route('adminPlanning.getChangeCity') }}',
            data: { id: $(this).val() },
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            $('#validate_route, .route, #vehicle_id, #driver_id, #transporter_id').empty().append('<option value="">-Selecciona-</option>');
            $.each(data.routes, function(key, value) {
                $('#validate_route, .route').append('<option value="'+ value.id +'">'+ value.route +'</option>');
            });
            $.each(data.transporters, function(key, value) {
                $('#transporter_id').append('<option value="'+ value.id +'">'+ value.fullname +'</option>');
            });
            $.each(data.warehouses, function(index, val) {
                 $('#warehouse_id').append($('<option></option>').attr('value', val.id).text(val.warehouse));
            });
        })
        .complete(function(){
            $('.loading-city').addClass('unseen');
            $('#validate_route, .route, #vehicle_id, #driver_id, #transporter_id').removeClass('disabled');
        })
        .fail(function(data) {
            console.error("error al obtener los datos.");
            e.preventDefault();
        });
    });

    $('body').on('change', '.route', function(e) {

        var route_id = $(this).val();
        $.ajax({
            url: '{{ route('adminPlanning.getInfoRoute') }}',
            data: { id: route_id },
            type: 'GET',
            dataType: 'json'
        })
            .done(function(data) {
                vehicle_id_route =data.suggested_vehicle_id;
                transporter_id_route =data.transporter_id;
                $("#transporter_id").val(transporter_id_route);
                $("#transporter_id").change();
                driver_id_route =data.driver_id;
            })
            .fail(function(data) {
                console.error("error al obtener los datos.");
                vehicle_id_route =0;
                transporter_id_route =0;
                driver_id_route =0;
                e.preventDefault();
            });
    });

    $('body').on('change', '#warehouse_id', function(e) {
        var warehouse_id = $(this).val();
        var city_id = $('#city_id option:selected').val();

        $('#validate_route, .route, #vehicle_id, #driver_id, #transporter_id').addClass('disabled');
        $('#validate_route').html('').append($("<option value=''>Selecciona</option>"));
        $('.route').html('').append($("<option value=''>Selecciona</option>"));
        $('.loading-city').removeClass('unseen');
        $.ajax({
            url: '{{ route('adminPlanning.getChangeWarehouse') }}',
            data: { id: city_id, warehouse_id: warehouse_id },
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            $.each(data.routes, function(index, val) {
                 $('#validate_route').append($('<option></option>').attr('value', val.id).text(val.route));
                 $('.route').append($('<option></option>').attr('value', val.id).text(val.route));
            });
        })
        .complete(function(){
            $('.loading-city').addClass('unseen');
            $('#validate_route, .route, #vehicle_id, #driver_id, #transporter_id').removeClass('disabled');
        })
        .fail(function(data) {
            console.error("error al obtener los datos.");
            e.preventDefault();
        });
    });

    $('#warehouse_id').trigger('change');

    $('#driver_id').change(function(e) {
        $('#order_departure_driver_id').val($(this).val());
    });

    $('#vehicle_id').change(function(e) {
        $('#order_departure_vehicle_id').val($(this).val());
    });

    $('.route').change(function(e) {
        $('#order_departure_route_id').val($(this).val());
    });
    setTimeout(function(){
        $('#check-all').iCheck('destroy');
    }, 1000);
});

</script>

@stop
