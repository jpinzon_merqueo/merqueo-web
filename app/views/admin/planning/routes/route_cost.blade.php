<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>{{ $title }} - Merqueo</title>
    <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{ web_url() }}/admin_asset/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/base.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset_url() }}lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script src="{{ web_url() }}/assets/js/jquery.min.js"></script>
    <script src="{{ web_url() }}/admin_asset/js/base.js"></script>
    <script src="{{ web_url() }}/assets/js/jquery.validate.min.js"></script>
    <script src="{{ asset_url() }}lib/fancybox/jquery.fancybox.pack.js"></script>
    <style type="text/css">
        textarea.error{ border: 1px solid #FF0000 !important;}
    </style>
    <body>
        <aside>
            <a href="#importing" class="fancybox unseen"></a>
            <div id="importing" class="unseen">
                <p>Procesando...</p>
                <br>
                <img src="{{ web_url() }}/admin_asset/img/importing.gif">
            </div>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Detalles de la ruta</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        @if(Session::has('message') )
                                        @if(Session::get('type') == 'success')
                                        <div class="alert alert-success alert-dismissable">
                                            <i class="fa fa-check"></i>
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <b>Información</b> {{ Session::get('message') }}
                                        </div>
                                        @else
                                        <div class="alert alert-danger alert-dismissable">
                                            <i class="fa fa-ban"></i>
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <b>Alerta!</b> {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        @endif
                                        <div class="table-responsive">
                                            <table id="route_details" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Estado</th>
                                                        <th>Ruta</th>
                                                        <th>Placa</th>
                                                        <th># Pedidos despachados</th>
                                                        <th># Pedidos entregados</th>
                                                        <th># Pedidos cancelados</th>
                                                        @if($route_status)<th>Acciones</th> @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><span class="badge @if($route_status) {{ 'bg-green' }} @else {{ 'bg-orange' }} @endif">{{ $route->status_cost }}</span></td>
                                                        <td>{{ $route->route }}</td>
                                                        <td>{{ $vehicle->plate }}</td>
                                                        <td>{{ $total_orders_count }}</td>
                                                        <td>{{ $route->orders()->delivered()->count() }}</td>
                                                        <td>{{ $route->orders()->cancelled()->count() }}</td>
                                                        @if($route_status)<td><input id="edit_details" value="Modificar" type="button" class="btn btn-primary"></td> @endif
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($route_status) <fieldset disabled> @endif
                                    <form action="{{ route('adminRoutes.routeCostSave', ['route_id' => $route->id]) }}" class="form-horizontal" id="main-form" method="POST" autocomplete="off">
                                        <div class="col-xs-12">
                                            <legend style="padding-left: 10px;">Costos</legend>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label">Costo de la ruta</label>
                                                        <div class="col-xs-7 col-xs-offset-1">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">$</div>
                                                                <input data-show_hidden="1" id="base_cost" name="base_cost" type="number" class="form-control" placeholder="Monto" value="{{ $route_cost }}" >
                                                                <!--p class="form-control">{{ $route_cost }}</p-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="route_support" class="col-xs-3 control-label">¿Tuvo apoyo a la ruta?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="route_support" name="route_support" type="checkbox" @if(array_key_exists('route_support', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('route_support', $route_cost_details)) style="display: none;" @else <?php $costos += $route_cost_details['route_support']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="route_support_amount" class="control-label">Monto</label>
                                                                    <input id="route_support_amount" name="route_support_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('route_support', $route_cost_details)) value="{{ $route_cost_details['route_support']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="route_support_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="route_support_comments" name="route_support_comments" class="form-control" rows="3">@if(array_key_exists('route_support', $route_cost_details)){{$route_cost_details['route_support']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12"><hr></div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="recharge_extra_distance" class="col-xs-3 control-label">¿Tiene recargo de distancia extra?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="recharge_extra_distance" name="recharge_extra_distance" type="checkbox" @if(array_key_exists('recharge_extra_distance', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('recharge_extra_distance', $route_cost_details)) style="display: none;" @else <?php $costos += $route_cost_details['recharge_extra_distance']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="recharge_extra_distance_amount" class="control-label">Monto</label>
                                                                    <input id="recharge_extra_distance_amount" name="recharge_extra_distance_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('recharge_extra_distance', $route_cost_details)) value="{{ $route_cost_details['recharge_extra_distance']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="recharge_extra_distance_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="recharge_extra_distance_comments" name="recharge_extra_distance_comments" class="form-control" rows="3">@if(array_key_exists('recharge_extra_distance', $route_cost_details)){{$route_cost_details['recharge_extra_distance']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="recharge_extra" class="col-xs-3 control-label">¿Tiene recargo extra?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="recharge_extra" name="recharge_extra" type="checkbox" @if(array_key_exists('recharge_extra', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('recharge_extra', $route_cost_details)) style="display: none;" @else <?php $costos += $route_cost_details['recharge_extra']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="recharge_extra_amount" class="control-label">Monto</label>
                                                                    <input id="recharge_extra_amount" name="recharge_extra_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('recharge_extra', $route_cost_details)) value="{{ $route_cost_details['recharge_extra']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="recharge_extra_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="recharge_extra_comments" name="recharge_extra_comments" class="form-control" rows="3">@if(array_key_exists('recharge_extra', $route_cost_details)){{$route_cost_details['recharge_extra']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12"><hr></div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="toll" class="col-xs-3 control-label">Peaje</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="toll" name="toll" type="checkbox" @if(array_key_exists('toll', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('toll', $route_cost_details)) style="display: none;" @else <?php $costos += $route_cost_details['toll']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="toll_amount" class="control-label">Monto</label>
                                                                    <input id="toll_amount" name="toll_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('toll', $route_cost_details)) value="{{ $route_cost_details['toll']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="toll_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="toll_comments" name="toll_comments" class="form-control" rows="3">@if(array_key_exists('toll', $route_cost_details)){{$route_cost_details['toll']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($total_orders_count >= 36)
                                                <?php $recargo_max_pedidos = ($total_orders_count - 35) * 5000; $costos += $recargo_max_pedidos; ?>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="recargo_max_pedidos" class="col-xs-3 control-label">¿Tiene recargo > 35 pedidos?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="0" id="recargo_max_pedidos" name="recargo_max_pedidos" type="checkbox" checked disabled>
                                                        </div>
                                                        <div class="col-xs-7">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="control-label">Monto</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">$</div>
                                                                        <p class="form-control">{{ $recargo_max_pedidos }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-xs-12"><hr></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <legend style="padding-left: 10px;">Deducciones</legend>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="penalty" class="col-xs-3 control-label">¿Tuvo multas?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="penalty" name="penalty" type="checkbox" @if(array_key_exists('penalty', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('penalty', $route_cost_details)) style="display: none;" @else <?php $deducciones += $route_cost_details['penalty']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="penalty_amount" class="control-label">Monto</label>
                                                                    <input id="penalty_amount" name="penalty_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('penalty', $route_cost_details)) value="{{ $route_cost_details['penalty']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="penalty_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="penalty_comments" name="penalty_comments" class="form-control" rows="3">@if(array_key_exists('penalty', $route_cost_details)){{$route_cost_details['penalty']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="kangaroo" class="col-xs-3 control-label">¿Tuvo canguro?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="kangaroo" name="kangaroo" type="checkbox" @if(array_key_exists('kangaroo', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('kangaroo', $route_cost_details)) style="display: none;" @else <?php $deducciones += $route_cost_details['kangaroo']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="kangaroo_amount" class="control-label">Monto</label>
                                                                    <input id="kangaroo_amount" name="kangaroo_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('kangaroo', $route_cost_details)) value="{{ $route_cost_details['kangaroo']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="kangaroo_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="kangaroo_comments" name="kangaroo_comments" class="form-control" rows="3">@if(array_key_exists('kangaroo', $route_cost_details)){{$route_cost_details['kangaroo']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12"><hr></div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="baskets" class="col-xs-3 control-label">¿Tuvo productos y/o canastillas no devueltas?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="baskets" name="baskets" type="checkbox" @if(array_key_exists('baskets', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('baskets', $route_cost_details)) style="display: none;" @else <?php $deducciones += $route_cost_details['baskets']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="baskets_amount" class="control-label">Monto</label>
                                                                    <input id="baskets_amount" name="baskets_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('baskets', $route_cost_details)) value="{{ $route_cost_details['baskets']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="baskets_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="baskets_comments" name="baskets_comments" class="form-control" rows="3">@if(array_key_exists('baskets', $route_cost_details)){{$route_cost_details['baskets']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label for="other" class="col-xs-3 control-label">¿Otro?</label>
                                                        <div class="col-xs-1">
                                                            <input data-show_hidden="1" id="other" name="other" type="checkbox" @if(array_key_exists('other', $route_cost_details)) checked @endif>
                                                        </div>
                                                        <div class="col-xs-7" @if(!array_key_exists('other', $route_cost_details)) style="display: none;" @else <?php $deducciones += $route_cost_details['other']['value']; ?> @endif>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="other_amount" class="control-label">Monto</label>
                                                                    <input id="other_amount" name="other_amount" type="number" class="form-control" placeholder="Monto" @if(array_key_exists('other', $route_cost_details)) value="{{ $route_cost_details['other']['value'] }}" @endif>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label for="other_comments" class="control-label text-left">Comentarios</label>
                                                                    <textarea id="other_comments" name="other_comments" class="form-control" rows="3">@if(array_key_exists('other', $route_cost_details)){{$route_cost_details['other']['comments']}}@endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if($route_status)
                                        <div id="totals" class="col-xs-12">
                                            <legend style="padding-left: 10px;">Totales</legend>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label">Ruta</label>
                                                        <div class="col-xs-7">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">$</div>
                                                                        <?php $total_route = $costos - $deducciones; ?>
                                                                        <p id="total_route" class="form-control">{{ $total_route }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label">Flota</label>
                                                        <div class="col-xs-7">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">$</div>
                                                                        <?php $flota = ($vehicle->driver->transporter->type == 'Tercerizada') ? $total_route * 0.2 : 0; ?>
                                                                        <p id="total_shipping" class="form-control">{{ $flota }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label">Total</label>
                                                        <div class="col-xs-7">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">$</div>
                                                                        <p id="total" class="form-control">{{ $total_route + $flota }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12"><hr></div>
                                        </div>
                                        @endif
                                        <div id="action_buttons" class="col-xs-12" @if($route_status) style="display: none;" @endif>
                                            <hr>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">Guardar</button>
                                                @if($route_status)
                                                <button id="cancel" type="button" class="btn btn-primary">Cancelar</button>
                                                @endif
                                            </div>
                                        </div>
                                        {{ Form::token() }}
                                        {{ Form::hidden('route_type', $route_type->id) }}
                                        {{ Form::hidden('_method', 'POST') }}
                                    </form>
                                    @if($route_status) </fieldset> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </aside>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $("#main-form").validate({
                    rules: {
                        base_cost: {
                            required: true,
                            number: true
                        },
                        route_support_amount: {
                            required: function(element){
                                return $('#route_support').is(':checked');
                            },
                            number: true
                        },
                        route_support_comments: {
                            required: function(element){
                                return $('#route_support').is(':checked');
                            },
                            minlength: 3
                        },
                        recharge_extra_distance_amount: {
                            required: function(element){
                                return $('#recharge_extra_distance').is(':checked');
                            },
                            number: true
                        },
                        recharge_extra_distance_comments: {
                            required: function(element){
                                return $('#recharge_extra_distance').is(':checked');
                            },
                            minlength: 3
                        },
                        recharge_extra_amount: {
                            required: function(element){
                                return $('#recharge_extra').is(':checked');
                            },
                            number: true
                        },
                        recharge_extra_comments: {
                            required: function(element){
                                return $('#recharge_extra').is(':checked');
                            },
                            minlength: 3
                        },
                        toll_amount: {
                            required: function(element){
                                return $('#toll').is(':checked');
                            },
                            number: true
                        },
                        toll_comments: {
                            required: function(element){
                                return $('#toll').is(':checked');
                            },
                            minlength: 3
                        },
                        penalty_amount: {
                            required: function(element){
                                return $('#penalty').is(':checked');
                            },
                            number: true
                        },
                        penalty_comments: {
                            required: function(element){
                                return $('#penalty').is(':checked');
                            },
                            minlength: 3
                        },
                        kangaroo_amount: {
                            required: function(element){
                                return $('#kangaroo').is(':checked');
                            },
                            number: true
                        },
                        kangaroo_comments: {
                            required: function(element){
                                return $('#kangaroo').is(':checked');
                            },
                            minlength: 3
                        },
                        baskets_amount: {
                            required: function(element){
                                return $('#baskets').is(':checked');
                            },
                            number: true
                        },
                        baskets_comments: {
                            required: function(element){
                                return $('#baskets').is(':checked');
                            },
                            minlength: 3
                        },
                        other_amount: {
                            required: function(element){
                                return $('#other').is(':checked');
                            },
                            number: true
                        },
                        other_comments: {
                            required: function(element){
                                return $('#other').is(':checked');
                            },
                            minlength: 3
                        }
                    }
                });
                $('#route_support, #recharge_extra_distance, #recharge_extra, #toll, #penalty, #kangaroo, #baskets, #other').on('click', function(event) {
                    var $this = $(this);
                        $hidden_div = $this.parent().parent().find('div.col-xs-7');
                    if ($this.is(':checked')) {
                        $hidden_div.slideDown();
                    } else {
                        $hidden_div.slideUp();
                    }
                });
                $('#edit_details').on('click', function(event) {
                    event.preventDefault();
                    $('#route_details').find('th:last')
                        .hide()
                        .end()
                        .find('td:last')
                        .hide()
                        .end();
                    $('#main-form').parent().removeAttr('disabled');
                    $('#action_buttons').slideDown();
                    $('#totals').slideUp();
                });
                $('#cancel').on('click', function(event) {
                    event.preventDefault();
                    $('#route_details').find('th:last')
                        .show()
                        .end()
                        .find('td:last')
                        .show()
                        .end();
                    $('#main-form').parent().attr('disabled', 'disabled');
                    $('#totals').slideDown();
                    $('#action_buttons').slideUp();
                });
                $('.fancybox').fancybox({
                    autoSize    : true,
                    closeClick  : false,
                    closeBtn    : false ,
                    openEffect  : 'none',
                    closeEffect : 'none',
                    helpers   : {
                       overlay : {closeClick: false}
                    }
                });
            });
        </script>
  </body>
</html>
