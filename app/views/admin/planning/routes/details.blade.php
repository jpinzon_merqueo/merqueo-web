@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')

    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
            @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
            @endif
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">

                    <div class="row">
                        <div class="box-body">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Datos de Planeación</legend>
                                </fieldset>
                            </div>
                            <div class="form-group col-xs-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Ciudad</th>
                                            <th>Bodega</th>
                                            <th>Fecha de planeación</th>
                                            <th>Turno</th>
                                            <th>Estado</th>
                                            <th># Rutas</th>
                                            <th># Pedidos</th>
                                            <th>Limite de pedidos</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$routes->city}}</td>
                                            <td>{{$routes->warehouse}}</td>
                                            <td>{{date('d/m/Y', strtotime($routes->planning_date))}}</td>
                                            <td>{{$routes->shift}}</td>
                                            <td>@if($routes->status == 'En proceso')<span class="badge bg-orange">{{$routes->status}}</span>@else<span class="badge bg-green">{{ $routes->status }}</span>@endif</td>
                                            <td align="right">{{ $routes->routes }}</td>
                                            <td>{{$orders->orders}}</td>
                                            <td>{{$routes->orders_limit}}</td>
                                            <td>
                                                @if ($admin_permissions['update'])
                                                @if($routes->status == 'En proceso')
                                                <a href="{{ route('adminRoutes.updateStatus', ['city_id' => $routes->city_id, 'date' => Route::current()->parameter('date'), 'warehouse_id' => $warehouse_id, 'shift' => $routes->shift, 'status' => 'Terminada']) }}" class="btn btn-success" onclick="return confirm('¿Esta seguro que deseas actualizar la planeación a Terminada?')">Planeación terminada</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endif
                                                @endif
                                                @if ($admin_permissions['delete'])
                                                <a href="{{ route('adminRoutes.updateStatus', ['city_id' => $routes->city_id, 'date' => Route::current()->parameter('date'), 'warehouse_id' => $warehouse_id, 'shift' => $routes->shift, 'status' => 'Eliminar']) }}" class="btn btn-danger" onclick="return confirm('¿Esta seguro que deseas eliminar la planeación?')">Eliminar planeación</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endif
                                                <a href="{{ route('adminRoutes.download', ['city_id' => $routes->city_id, 'date' => Route::current()->parameter('date'), 'warehouse_id' => $warehouse_id, 'shift' => $routes->shift]) }}" class="btn btn-primary" onclick="$(this).addClass('disabled')">Descargar</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="{{ route('adminRoutes.routesMap', ['city_id' => $routes->city_id, 'date' => Route::current()->parameter('date'), 'warehouse_id' => $warehouse_id,  'shift' => $routes->shift]) }}" class="btn btn-primary" target="_blank">Ver mapa</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <table width="100%" class="planning-routes-table">
                                        <tr>
                                            <td align="right">
                                                <label>Nombre de ruta:</label>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control search" placeholder="Nombre de ruta">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>

                                </form>

                            </div>
                        </div>
                        <br />
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <script>
        var web_url_ajax = "{{ route('adminRoutes.details', ['city_id' => $routes->city_id, 'date' => Route::current()->parameter('date'), 'warehouse_id'=>$warehouse_id, 'shift' => $routes->shift ]) }}";

        var web_url_ajax_vehicles = "{{ route('adminTransporter.ajax-vehicles-transporter') }}";
        $(document).ready(function() {

            paging(1, '');

            @if (Session::has('file_url'))
            setTimeout(
                function() {
                    window.location = '{{ Session::get('file_url') }}';
                }, 2000);
            <?php Session::forget('file_url'); ?>
            @endif

            $('.route-map, .orders-details').fancybox({
                fitToView   : false,
                width       : '90%',
                height      : '95%',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
            });

            $('body').on('click', '#btn-picking-priority', function(){
               var picking_priority = {};
               error = false;
               $('.picking-priority').each(function(){
                    if (isNaN($(this).val())){
                        $(this).css('border', '1px solid red');
                        error = true;
                        return false;
                    }
                    picking_priority[$(this).attr('id')] = $(this).val();
               });

               if (error){
                    alert('Hay valores no numericos en la prioridad de ruta.');
                    return false;
               }

               data = {
                   picking_priority: JSON.stringify(picking_priority),
                   city_id: {{ $routes->city_id }},
                   warehouse_id : {{Route::current()->parameter('warehouse_id')}},
                   date: '{{ Route::current()->parameter('date') }}',
                   shift: '{{ $routes->shift }}',

               };

               $('#btn-picking-priority').addClass('disabled');
                $.ajax({
                    url: '{{ route('adminRoutes.updatePickingPriority') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                })
                .done(function(data) {
                    if(data.status){
                        alert('Prioridad de alistamiento actualizado con éxito.');
                        paging(1, '');
                    }else alert(data.message);
                })
                .always(function(data) {
                    $('#btn-picking-priority').removeClass('disabled');
                });
            });

            $('body').on('click', '#btn-dispatch-time', function(){
               var dispatch_time = {};
               error = false;
               $('.dispatch-time').each(function(){
                    if ($(this).val() == ""){
                        $(this).css('border', '1px solid red');
                        error = true;
                        return false;
                    }
                    dispatch_time[$(this).attr('id')] = $(this).val();
               });

               if (error){
                    alert('Hay valores no numericos en la prioridad de ruta.');
                    return false;
               }

               data = {
                   dispatch_time: JSON.stringify(dispatch_time),
                   city_id: {{ $routes->city_id }},
                   warehouse_id : {{Route::current()->parameter('warehouse_id')}},
                   date: '{{ Route::current()->parameter('date') }}',
                   shift: '{{ $routes->shift }}',

               };

               $('#btn-dispatch-time').addClass('disabled');
                $.ajax({
                    url: '{{ route('adminRoutes.updateDispatchTime') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                })
                .done(function(data) {
                    if(data.status){
                        alert('Tiempo de despacho actualizado con éxito.');
                        paging(1, '');
                    }else alert(data.message);
                })
                .always(function(data) {
                    $('#btn-dispatch-time').removeClass('disabled');
                });
            });
            $('body').on('click', '#asig_vehicle_route', function(){
                var data = $(this).data()
                $('#modal-asig-vehicle-route').modal('show');
                $('#transporter_id').val('')
                $('#vehicle_id').val('')
                $("#route_name").html(data.name);
                $("#route_id").val(data.id);
                $("#containers").val(data.containers);
                $("#orders_number").val(data.orders);
                $("#change-vehicle-route-form").validate().resetForm();
                $(".error").removeClass("error");
            });
            $('body').on('change', '#transporter_id,#status_containers', function(){
                var data = {'transporter_id': $('#transporter_id').val(),
                    'containers': $('#containers').val(),
                    'orders_number': $('#orders_number').val(),
                    'status_containers': $('#status_containers').is(':checked'),
                };
                $.ajax({
                    url: web_url_ajax_vehicles,
                    data: data,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        var vehicle_id = $('#vehicle_id');
                        vehicle_id.empty();
                        vehicle_id.append('<option value="">-Selecciona-</option>');
                        $.each(response.result.vehicles, function(key, value){
                            vehicle_id.append('<option value="' + value.id + '">'+(value.cost)+' / '
                                + value.plate + ' / '+ value.class_type + ' / N°'+ value.capacity_containers +'</option>');
                        });
                    }, error: function() {
                        alert('Ocurrió un error al obtener los datos.');
                    }
                });
            });
            $('.change-vehicle-route-form').validate({
                rules: {
                    route_id: "required",
                    vehicle_id: "required"
                }
            });
            $('body').on('click', '.btn-asing-vehicle-route[type="submit"]', function(e) {
                $('.loading div').removeClass('unseen');
                $('.btn-asing-vehicle-route').addClass('unseen');
                if ($('.change-vehicle-route-form').valid()) {
                    var data = {route_id: $('#route_id').val(), vehicle_id: $('#vehicle_id').val()};
                    $.post('{{ route('adminRoutes.updateVehicleOrder') }}', data)
                        .done(function (data) {
                            $('#modal-asig-vehicle-route').modal('hide');
                            $('.loading div').addClass('unseen');
                            $('.btn-asing-vehicle-route').removeClass('unseen');
                            window.location.reload(true);
                        })
                        .fail(function (data) {
                            $('.loading div').addClass('unseen');
                            $('.btn-asing-vehicle-route').removeClass('unseen');
                        });
                }
                else {
                        $('.loading div').addClass('unseen');
                        $('.btn-asing-vehicle-route').removeClass('unseen');
                    }

                e.preventDefault();
            });

        });
    </script>
    @endsection
@else
    @section('content')
    <table id="routes-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Prioridad en alistamiento @if($planning_route_status == 'En proceso')<br><button class="btn btn-success btn-sm" id="btn-picking-priority">Actualizar</button> @endif</th>
                <th>Hora de despacho @if($planning_route_status == 'En proceso')<br><button class="btn btn-success btn-sm" id="btn-dispatch-time">Actualizar</button> @endif</th>
                <th>Ruta</th>
                <th>Bodega</th>
                <th># Pedidos</th>
                <th># Productos</th>
                <th># Contenedores</th>
                @if($planning_route_status != 'En proceso')<th>Vehículo</th>@endif
                <th>Tiempo total</th>
                <th>Distancia a seq. #1</th>
                <th># Pedidos por horario de entrega</th>
                <th>Grupo de alistamiento</th>
                <th>Fecha de creación</th>
                <th>Ver mapa</th>
                @if ($admin_permissions['permission1'])
                <th>Costos</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @if(count($routes))
            	@foreach($routes as $route)
                <tr>
                    <td>@if($route->status == 'En proceso')<input type="number" class="picking-priority" id="route_{{ $route->id }}" value="{{ $route->picking_priority }}">@else {{ $route->picking_priority }} @endif</td>
                    <td>@if($route->status == 'En proceso')<input type="time" class="dispatch-time" id="time_{{ $route->id }}" value="{{ date('H:i', strtotime($route->dispatch_time)) }}">@else {{ date('H:i', strtotime($route->dispatch_time)) }} @endif</td>
                    <td>{{ $route->route }}</td>
                    <td>{{$route->warehouse}}</td>
                    <td align="right"><a href="{{ route('adminRoutes.getOrderDetails', ['id' => $route->id]) }}" data-fancybox-type="iframe" class="orders-details">{{ $route->orders }}</a></td>
                    <td align="right">{{ $route->products }}</td>
                    <td align="right">{{ $route->containers }} <br></td>
                    @if($planning_route_status != 'En proceso')
                    <td><label>{{$route->plate}}</label><br>
                        <a class="btn btn-xs btn-primary" id="asig_vehicle_route" data-id="{{$route->id}}"
                           data-name="{{$route->route}}" data-orders="{{$route->orders}}" data-containers="{{$route->containers}}">
                            @if($route->plate != '')Cambiar
                            @else Asignar
                            @endif</a>
                    </td>
                    @endif
                    <td>{{ get_time_text(round($route->planning_duration / 60)) }}</td>
                    <td>{{ round($route->planning_distance / 1000) }} km</td>
                    <td>
                        @if($route->delivery_times)
                        <table border="1" align="center" class="table table-bordered table-striped">
                            <tr>@foreach($route->delivery_times as $value)<th style="text-align: center !important;">{{ $value->delivery_time }}</th>@endforeach</tr>
                            <tr>@foreach($route->delivery_times as $value)<td align="center">{{ $value->orders_quantity }} @if($value->orders_quantity > 1) Pedidos @else Pedido @endif</td>@endforeach</tr>
                        </table>
                        @endif
                    </td>
                    <td><div class="col-md-6">{{ $route->picking_group }}</div><div class="col-md-6" style="background-color: {{ $route->color }}">&nbsp;</div></td>
                    <td>{{ date("d M Y g:i a", strtotime($route->created_at)) }}</td>
                    <td align="center">
                        <div class="btn-group">
                           <a class="btn btn-xs btn-default route-map" href="{{ route('adminRoutes.routeMap', ['id' => $route->id]) }}" data-fancybox-type="iframe"><span class="glyphicon glyphicon-map-marker"></span></a>
                        </div>
                    </td>
                    @if ($admin_permissions['permission1'])
                    <td align="center">
                        @if(($route->orders_delivered == $route->orders_count) && ($route->status == 'Terminada'))
                        <div class="btn-group">
                           <a class="btn btn-xs btn-default route-map @if($route->status_cost == 'Pendiente') {{ 'bg-orange' }} @else {{ 'bg-green' }} @endif" href="{{ route('adminRoutes.routesCost', ['id' => $route->id]) }}" data-fancybox-type="iframe"><span class="glyphicon glyphicon-usd"></span></a>
                        </div>
                        @endif
                    </td>
                    @endif
                </tr>
                @endforeach
                <tr>
                    <td colspan="4"><b>Total</b></td>
                    <td align="right"><b>{{ $total_orders }}</b></td>
                    <td align="right"><b>{{ $total_products }}</b></td>
                    <td align="right"><b>{{ $total_containers }}</b></td>
                    <td colspan="7">&nbsp;</td>
                </tr>
            @else
                <tr><td colspan="8" align="center">No hay datos.</td></tr>
            @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="table_info">Mostrando {{ count($routes) }} ítems</div>
        </div>
    </div>
    <div class="modal fade" id="modal-asig-vehicle-route" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="change-vehicle-route-form" accept-charset="UTF-8" class="change-vehicle-route-form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Asignar vehículo a <label id="route_name"></label></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box-body ">
                                    <div class="row" >
                                        <input id="route_id" name="route_id" hidden />
                                        <input id="containers" name="containers" hidden />
                                        <input id="orders_number" name="orders_number" hidden />
                                        <div class="row" >
                                        <div class="col-xs-6">
                                            <label>Transportador:</label>
                                            <select id="transporter_id" name="transporter_id"  required class="form-control">
                                                <option value="">-Selecciona-</option>
                                                @foreach($transporters as $transporter)
                                                    <option value="{{ $transporter->id }}">{{ $transporter->fullname }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Vehículo:</label>
                                            <select id="vehicle_id" name="vehicle_id" required class="form-control">
                                                <option value="">-Selecciona-</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <br>
                                            <input type="checkbox" id="status_containers" name="status_containers"> Tener en cuenta los contenedores
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-asing-vehicle-route">&nbsp;Guardar&nbsp;</button>
                    </div>

                    </form>
            </div>
        </div>
    </div>
    @endsection
@endif
