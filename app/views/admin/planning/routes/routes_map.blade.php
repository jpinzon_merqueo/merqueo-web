<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>{{ $title }} - Merqueo</title>

    <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{ web_url() }}/admin_asset/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/base.css" rel="stylesheet" type="text/css" />
    <script src="{{ web_url() }}/assets/js/jquery.min.js"></script>
    <script src="{{ web_url() }}/admin_asset/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/base.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ Config::get('app.google_api_key') }}"></script>

    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            position: absolute;
        }
    </style>

    <body class="skin-black routes-map">

        <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
        <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
        <script>
            $('.fancybox').fancybox({
                autoSize    : true,
                closeClick  : false,
                closeBtn    : false ,
                openEffect  : 'none',
                closeEffect : 'none',
                helpers   : {
                   overlay : {closeClick: false}
                }
            });
        </script>
        <a href="#processing" class="fancybox unseen"></a>
        <div id="processing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>

        <header class="header">
            <a href="{{ route('admin.dashboard') }}" class="logo">
                <img src="{{ web_url() }}/admin_asset/img/logo-home.png" width="130"/>
            </a>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    @if ($admin_permissions['update'])
                    <h4>Asignar pedidos a ruta</h4>
                    <div class="row">
                        <form id="route-map-form">
                            <input type="hidden" name="warehouse_id" id="warehouse_id" value="{{$warehouse_id}}">
                            <div class="col-xs-12">
                                <label class="label">Ruta origen</label>
                                <select id="origin_route_id" name="origin_route_id" class="form-control">
                                    <option value="">Selecciona</option>
                                    @foreach($routes as $route)
                                        <option value="{{ $route->id }}">{{ $route->route }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12">
                                <label class="label">Ruta destino</label>
                                <select id="destination_route_id" name="destination_route_id" class="form-control">
                                    <option value="">Selecciona</option>
                                    <option value="create_route">Crear nueva ruta</option>
                                    @foreach($routes as $route)
                                        <option value="{{ $route->id }}">{{ $route->route }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12">
                                <label class="label" style="display: inline-block">Asignar toda la ruta</label>
                                <input type="checkbox" class="assign-all-route" id="all_route">
                            </div>
                            <div class="col-xs-12">
                                <label class="label">Pedidos</label>
                                <input type="text" class="form-control" id="route_sequences" placeholder="Secuencias de ruta origen">
                            </div>
                            <div class="col-xs-12">
                                <p>Secuencia separada por comas o guión para intervalos, por ejemplo: 1, 5, 3-10</p>
                            </div>
                            <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-primary" id="btn-assign">Asignar</button>
                            </div>
                        </form>
                    </div>
                    @endif
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="label" style="display: inline-block">Ver pedidos de Marketplace</label>
                            <input type="checkbox" class="show-marketplace-route" id="show-marketplace-route">
                        </div>
                    </div>
                    <br>
                    <h4>Rutas (<span class="routes-qty">{{ count($routes) }}</span>)</h4>
                    <div id="routes"></div>
                </section>
            </aside>

            <aside class="right-side">
                <section class="content" style="padding-top: 0">
                    <div class="row">
                        <div id="map" class="col-xs-12"></div>
                    </div>
                </section>
            </aside>
        </div>

        <div class="modal fade" id="modal-delivery-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel">Editar Dirección de Entrega</h3>
                    </div>
                    <form class="form-modal form-address">
                        <div class="modal-body">
                            <div class="unseen alert alert-danger form-has-errors"></div>
                            <div class="form-group">
                                <input type="hidden" id="order_id" />
                                <input type="hidden" id="route_id" />
                                <label class="control-label">Ciudad</label>
                                <select name="city" id="city_id" class="form-control required">
                                    @foreach ($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Dirección de entrega</label>
                                <input type="text" class="form-control required" name="delivery_address" id="delivery_address">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Complemento de dirección</label>
                                <input type="text" class="form-control" name="delivery_address_further" id="delivery_address_further" placeholder="Ej: Apartamento 501, Torre 2">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Barrio</label>
                                <input type="text" class="form-control" name="delivery_address_neighborhood" id="delivery_address_neighborhood">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary btn-save-delivery-address">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <script>

        var json = {{ json_encode($planning_routes) }};

        $(document).ready(function() {

            var map;
            var current_infowindow_open;
            var infowindow = new google.maps.InfoWindow();
            var colors = [];
            var font_colors = [];
            var route_markers = [];
            var positions = [];

            //MOSTRAR SOLO PUNTOS EN MAPA

            var initialize = function() {
                var mapOptions = {
                    center: new google.maps.LatLng({{ $merqueo_location['lat'] }}, {{ $merqueo_location['lng'] }}),
                    zoom: 14,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    draggableCursor: "crosshair"
                };

                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                $.each(json, function(index, route) {
                    waypoints = [];
                    orders = [];
                    $.each(route, function(key, order) {
                        waypoints.push([order.latitude, order.longitude]);
                        orders.push(order);
                    });
                    colors[index] = getRandomColor(colors[index]);
                    font_colors[index] = getFontColor(colors[index]);
                    show_routes(waypoints, orders, index);
                });
                /*$.each(warehouse, function(i, warehouse){
                  createMarker(map, new google.maps.LatLng(warehouse.latitude, warehouse.longitude), '<b>Merqueo</b>', '', 'Merqueo');
                })*/
                createMarker(map, new google.maps.LatLng({{ $merqueo_location['lat'] }}, {{ $merqueo_location['lng'] }}), '<b>Merqueo</b>', '', 'Merqueo');
            };

            var show_routes = function(waypoints, orders, index) {
                total_distance = total_duration = 0;
                //console.log(orders)
                $.each(waypoints, function(key, position) {
                    var type = '';
                    if(orders[key].type == 'Marketplace'){
                          type = '<br><b>'+orders[key].type+'</b> - <b>'+orders[key].allied_store_name+'</b>';
                    }
                    html = '<br><b>Ruta:</b> ' + orders[key].route + '&nbsp;<a href="#route-' + orders[key].route_id + '" class="go-to-route">Ir a ruta</a>'
                      + '<br><b>Bodega:</b> ' + orders[key].warehouse
                      + type
                      + '<br><b>Pedido:</b> #' + orders[key].id
                      + '<br><b>Dirección: </b>' + orders[key].address;
                      @if ($admin_permissions['update'])
                      html += '&nbsp;<a href="javascript:;" class="edit-order-address" data-route="' + orders[key].route_id + '" data-order="' + orders[key].id + '">Editar</a>'
                      @endif
                      html += '<br><b>Barrio: </b>' + orders[key].neighborhood
                      + '<br><b>Horario: </b>' + orders[key].delivery_time
                      + '<br><b>Distancia: </b>' + orders[key].planning_distance +' km'
                      + '<br><b>Tiempo: </b>' + orders[key].planning_duration + ' min';
                      @if ($admin_permissions['update'])
                      html += '<br><b>Secuencia: </b><input type="text" class="order-sequence" size="4">&nbsp;'
                      + '<a href="javascript:;" class="btn btn-sm btn-primary btn-order-sequence" data-route="' + orders[key].route_id + '" data-order="' + orders[key].id + '">Actualizar</a>'
                      + '<br><br><a href="javascript:;" class="btn btn-sm btn-primary origin" data-route="' + orders[key].route_id + '" data-sequence="' + orders[key].planning_sequence + '">Marcar como ruta origen</a><br><br>'
                      + '<a href="javascript:;" class="btn btn-sm btn-primary destination" data-route="' + orders[key].route_id + '" data-sequence="' + orders[key].planning_sequence + '">Marcar como ruta destino</a>';
                      /*if(orders[key].delivery_time == '12:00 pm - 5:00 pm' && orders[key].shift == 'AM'){
                        html+= '<br><br><a href="javascript:;" class="btn btn-sm btn-primary delete-order-from-route" data-order="' + orders[key].id + '" data-route="' + orders[key].route_id + '" data-sequence="' + orders[key].planning_sequence + '" data-warehouseid="'+orders[key].warehouse_id+'">Eliminar pedido de esta ruta</a>';
                      }
                      @endif*/
                    LatLng = new google.maps.LatLng(position[0], position[1]);
                    createMarker(map, LatLng, html, orders[key].planning_sequence, index);
                    positions[LatLng] = positions[LatLng] == undefined ? 1 : positions[LatLng]+1;
                    total_distance += parseFloat(orders[key].planning_distance);
                    total_duration += parseInt(orders[key].planning_duration);
                });
                //console.log(positions);
                if (typeof orders[0] != 'undefined'){
                    route = ' <b>Ruta:</b> ' + orders[0].route + '</br>'
                            +' <b>Bodega:</b> ' + orders[0].warehouse + '</br>'
                            +' <b>Pedidos:</b> ' + orders.length + '</br>'
                            +' <b>Tiempo:</b> ' + total_duration + ' min</br>'
                            +' <b>Distancia:</b> ' + roundNumber(total_distance, 1) + ' km</br>'
                            +' <b>Ver ruta:</b> &nbsp;&nbsp;<input type="checkbox" class="show-all-route" data-index="' + index + '" checked="checked">'
                    if ($('#route-' + orders[0].route_id).length){
                        $('#route-' + orders[0].route_id).html(route);
                    }else{
                        route = '<div id="route-' + orders[0].route_id + '" class="route-block" style="color:' + font_colors[index] + '; background-color:' + colors[index] + '">' + route + '</div>';
                        $('#routes').append(route);
                    }
                }
            };

            var createMarker = function(map, latlng, html, marker_label, index) {

                  if (index == 'Merqueo')
                  {
                    url = 'https://merqueo.com/assets/img/markermerqueo.png';
                    index = 0;
                    marker_image = new google.maps.MarkerImage(url,
                          new google.maps.Size(25, 25),
                          new google.maps.Point(0, 0)
                      );
                      var marker = new google.maps.Marker({
                          position: latlng,
                          map: map,
                          icon: marker_image,
                          zIndex: parseInt(index)
                      });
                      marker.myname = 'Merqueo';
                  }else{

                      /*icon_size = positions[latlng] != undefined ? new google.maps.Size(31, 37) : new google.maps.Size(21, 36);
                      url = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker_label + '|' + colors[index] + '|' + font_colors[index];
                      marker_image = new google.maps.MarkerImage(url,
                          icon_size,
                          //new google.maps.Size(35, 64),
                          new google.maps.Point(0, -5)
                      );*/

                      var marker = new google.maps.Marker({
                          position: latlng,
                          map: map,
                          icon: {
                              path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                              fillColor: colors[index],
                              fillOpacity: 1,
                              strokeColor: 'black',
                              strokeWeight: 1,
                              scale: 7,
                              labelOrigin: new google.maps.Point(0,-2.4)
                          },
                          //icon: marker_image,
                          label: {
                              color: font_colors[index],
                              fontSize: '12px',
                              text: marker_label.toString()
                          },
                          zIndex: parseInt(index)
                      });
                      marker.myname = 'Pedido en ruta';
                  }

                  google.maps.event.addListener(marker, 'click', function() {
                      infowindow.setContent(html);
                      infowindow.open(map, marker);
                      current_infowindow_open = infowindow;
                  });

                  if (index != 'Merqueo'){
                      if (typeof route_markers[index] === 'undefined')
                         route_markers[index] = [];
                      route_markers[index].push(marker);
                  }

                  return marker;
            };

            function getRandomColor() {
              var letters = '0123456789ABCDEF';
              var color = '';
              for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
              }
              if ($.inArray(color, colors) == -1)
                return '#' + color;
              else return getRandomColor();
            }

            function getFontColor(color){
                var c = color.substring(1);  // strip #
                var rgb = parseInt(c, 16);   // convert rrggbb to decimal
                var r = (rgb >> 16) & 0xff;  // extract red
                var g = (rgb >>  8) & 0xff;  // extract green
                var b = (rgb >>  0) & 0xff;  // extract blue

                var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

                if (luma < 40) {
                    return '#FFFFFF'
                }
                return '#000000'
            }

            google.maps.event.addDomListener(window, 'load', initialize);

            //EVENTOS

            $('body').on('click', '.origin', function(){
                $('#origin_route_id').val($(this).data('route'));
                $('#route_sequences').val($(this).data('sequence'));
                current_infowindow_open.close();
            });

            $('body').on('click', '.destination', function(){
                $('#destination_route_id').val($(this).data('route'));
                current_infowindow_open.close();
            });

            $('body').on('click', '.delete-order-from-route', function(){
                var order_id = $(this).data('order');
                var route_id = $(this).data('route');
                var sequence = $(this).data('sequence');
                var warehouse_id = $(this).data('warehouseid');
                current_infowindow_open.close();
                $.ajax({
                  url : '{{ route("adminRoutes.updateRemoveOrder") }}',
                  type : 'POST',
                  data : { order_id : order_id, route_id : route_id, sequence : sequence, warehouse_id : warehouse_id },
                  success : function(response){
                      if (response.status){
                          $.each(response.result, function(index, route) {
                              //eliminar markers
                              $.each(route_markers[index], function(key, marker) {
                                  marker.setMap(null);
                              });
                              delete route_markers[index];
                              //agregar markers
                              waypoints = [];
                              orders = [];
                              $.each(route, function(key, order) {
                                  waypoints.push([order.latitude, order.longitude]);
                                  orders.push(order);
                              });
                              show_routes(waypoints, orders, index);

                              //eliminar ruta origen de selects y html
                              
                          });
                          $('.routes-qty').html($('.route-block').length);
                          obj.removeAttr('disabled');
                      }else{
                          alert(response.message);
                      }
                  }
                });
              
            });

            $('body').on('change', '.assign-all-route', function(){
                if(this.checked)
                    $('#route_sequences').attr('disabled', 'disabled').val('');
                else $('#route_sequences').removeAttr('disabled').val('');
            });

            $('body').on('change', '.show-all-route', function(){
                if (!this.checked)
                    show = false;
                else show = true;
                $.each(route_markers[$(this).data('index')], function(index, marker) {
                    marker.setVisible(show);
                });
            });

            $('body').on('change', '.show-marketplace-route', function(){
                if (!this.checked)
                    show = false;
                else show = true;
                $.each(route_markers[$(this).data('index')], function(index, marker) {
                    marker.setVisible(show);
                });
            });

            //reasignar pedidos a otra ruta
            $('body').on('click', '#btn-assign', function(){
                if ($('#origin_route_id').val() == '' || $('#destination_route_id').val() == ''
                || ($('#route_sequences').val() == '' && !$('#all_route').is(':checked'))){
                    alert('Ruta origen, ruta destino y pedidos es requerido.');
                    return false;
                }

                $('#btn-assign').addClass('disabled');
                $('.fancybox').trigger('click');

                data ={
                    origin_route_id: $('#origin_route_id').val(),
                    //origin_route_index: $('#route-' + $('#origin_route_id').val()).find('.show-all-route').data('index'),
                    destination_route_id: $('#destination_route_id').val(),
                    //destination_route_index: $('#route-' + $('#destination_route_id').val()).find('.show-all-route').data('index'),
                    route_sequences: $('#route_sequences').val(),
                    warehouse_id : $('#warehouse_id').val(),
                    all_route: $('#all_route').is(':checked') ? 1 : 0
                }

                $.ajax({
                    url: '{{ route('adminRoutes.joinOrdersToRoute') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function(response){
                        if (response.status){
                            $.each(response.result, function(index, route) {
                                //eliminar markers
                                if (typeof route_markers[index] != 'undefined'){
                                    $.each(route_markers[index], function(key, marker) {
                                        marker.setMap(null);
                                    });
                                    delete route_markers[index];
                                }else{
                                    if (typeof route[0] != 'undefined'){
                                        //nueva ruta
                                        colors[index] = getRandomColor();
                                        font_colors[index] = getFontColor(colors[index]);
                                        $('#origin_route_id').append('<option value="' + index + '">' + route[0]['route'] + '</option>');
                                        $('#destination_route_id').append('<option value="' + index + '">' + route[0]['route'] + '</option>');
                                    }
                                }

                                //eliminar ruta origen de selects y html
                                if (!route.length){
                                    if (data.origin_route_id == index){
                                        $('#route-' + index).remove();
                                        $("#origin_route_id option[value='" + index + "']").remove();
                                        $("#destination_route_id option[value='" + index + "']").remove();
                                    }
                                }

                                //agregar markers
                                waypoints = [];
                                orders = [];
                                $.each(route, function(key, order) {
                                    waypoints.push([order.latitude, order.longitude]);
                                    orders.push(order);
                                });
                                show_routes(waypoints, orders, index);

                            });
                            $('.routes-qty').html($('.route-block').length);
                            $('#route-map-form').get(0).reset();
                            $('#route_sequences').removeAttr('disabled');
                        }else{
                            alert(response.message);
                        }
                    },
                    complete: function(){
                        $.fancybox.close();
                        $('#btn-assign').removeClass('disabled');
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });

            //actualizar secuencia de pedido en ruta
            $('body').on('click', '.btn-order-sequence', function(){
                order_sequence = $(this).parent().find('.order-sequence').val();
                if (order_sequence == '' || isNaN(order_sequence) || order_sequence == 0){
                    alert('La secuencia es requerida, debe ser númerico y diferente de cero.');
                    return false;
                }
                obj = $(this);
                obj.addClass('disabled');
                $('.fancybox').trigger('click');

                data ={
                    order_sequence: order_sequence,
                    order_id: $(this).data('order')
                }

                $.ajax({
                    url: '{{ route('adminRoutes.updateOrderSequence') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function(response){
                        if (response.status){
                            $.each(response.result, function(index, route) {
                                //eliminar markers
                                $.each(route_markers[index], function(key, marker) {
                                    marker.setMap(null);
                                });
                                delete route_markers[index];
                                //agregar markers
                                waypoints = [];
                                orders = [];
                                $.each(route, function(key, order) {
                                    waypoints.push([order.latitude, order.longitude]);
                                    orders.push(order);
                                });
                                show_routes(waypoints, orders, index);

                                //eliminar ruta origen de selects y html
                                if (!route.length){
                                    if (obj.data('route') == index){
                                        $('#route-' + index).remove();
                                        $("#origin_route_id option[value='" + index + "']").remove();
                                        $("#destination_route_id option[value='" + index + "']").remove();
                                    }
                                }
                            });
                            $('.routes-qty').html($('.route-block').length);
                            obj.removeAttr('disabled');
                        }else{
                            alert(response.message);
                        }
                    },
                    complete: function(){
                        $.fancybox.close();
                        obj.removeClass('disabled');
                    },
                    error: function(error) {
                        $.fancybox.close();
                        console.log(error);
                    }
                });
            });

            //obtener direccion de pedido para editar
            $('body').on('click', '.edit-order-address', function(){
                order_id = $(this).data('order');
                route_id = $(this).data('route');
                $('.fancybox').trigger('click');
                $.ajax({
                    url: '{{ route('adminRoutes.getOrderDeliveryAddress') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: { order_id: order_id },
                    success: function(response){
                        if (response.status){
                            $('#order_id').val(order_id);
                            $('#route_id').val(route_id);
                            $('#city_id').val(response.result.user_city_id);
                            $('#delivery_address').val(response.result.user_address);
                            $('#delivery_address_further').val(response.result.user_address_further);
                            $('#delivery_address_neighborhood').val(response.result.user_address_neighborhood);
                            $('#modal-delivery-address').modal('show');
                        }else{
                            alert(response.message);
                        }
                    },
                    complete: function(){
                        $.fancybox.close();
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });

            //editar direccion de pedido y reasignar a ruta
            $('body').on('click', '.btn-save-delivery-address', function(){
                if ($('#delivery_address').val() == ''){
                    alert('La dirección es requerida');
                    return false;
                }

                data = {
                    order_id: $('#order_id').val(),
                    city: $('#city_id').val(),
                    delivery_address: $('#delivery_address').val(),
                    delivery_address_further: $('#delivery_address_further').val(),
                    delivery_address_neighborhood: $('#delivery_address_neighborhood').val()
                }

                $('.fancybox').trigger('click');
                $.ajax({
                    url: '{{ route('adminRoutes.updateOrderDeliveryAddress') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function(response){
                        if (response.status){
                            $.each(response.result, function(index, route) {
                                //eliminar markers
                                if (typeof route_markers[index] != 'undefined'){
                                    $.each(route_markers[index], function(key, marker) {
                                        marker.setMap(null);
                                    });
                                    route_markers[index] = [];
                                }else{
                                    if (typeof route[0] != 'undefined'){
                                        //nueva ruta
                                        colors[index] = getRandomColor();
                                        font_colors[index] = getFontColor(colors[index]);
                                        $('#origin_route_id').append('<option value="' + index + '">' + route[0]['route'] + '</option>');
                                        $('#destination_route_id').append('<option value="' + index + '">' + route[0]['route'] + '</option>');
                                    }
                                }
                                //agregar markers
                                waypoints = [];
                                orders = [];
                                $.each(route, function(key, order) {
                                    waypoints.push([order.latitude, order.longitude]);
                                    orders.push(order);
                                });
                                show_routes(waypoints, orders, index);

                                //eliminar ruta origen de selects y html
                                if (!route.length){
                                    if ($('#route_id').val() == index){
                                        $('#route-' + index).remove();
                                        $("#origin_route_id option[value='" + index + "']").remove();
                                        $("#destination_route_id option[value='" + index + "']").remove();
                                    }
                                }
                            });
                            $('.routes-qty').html($('.route-block').length);
                            $('.form-address').get(0).reset();
                            $('#modal-delivery-address').modal('hide');
                        }
                        alert(response.message);
                    },
                    complete: function(){
                        $.fancybox.close();
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });

     });
    </script>

</body>
</html>
