<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>{{ $title }} - Merqueo</title>

    <link href="{{ web_url() }}/admin_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{ web_url() }}/admin_asset/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="{{ web_url() }}/admin_asset/css/base.css" rel="stylesheet" type="text/css" />
    <script src="{{ web_url() }}/assets/js/jquery.min.js"></script>
    <script src="{{ web_url() }}/admin_asset/js/base.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ \Config::get('app.planning_google_api_key') }}"></script>

    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            position: absolute;
        }
    </style>

    <body>

        <aside>
            <section class="content" style="padding-top: 0">
                <div class="row">
                    <div id="map" class="col-xs-12"></div>
                </div>
            </section>
        </aside>

    <script>

        var json = {{ json_encode($routes) }};

        $(document).ready(function() {

            var map;
            var directions_display = [];
            var directions_service = new google.maps.DirectionsService();
            var infowindow = new google.maps.InfoWindow();
            var colors = [];
            var font_colors = [];
            var route_markers = [];
            var merqueo = [{{ $merqueo_location['lat'] }}, {{ $merqueo_location['lng'] }}];

            //console.log(json);

            var calculate = function(start, end, waypoints, index) {
                directions_display[index] = new google.maps.DirectionsRenderer();
                directions_display[index].setMap(map);
                directions_display[index].setOptions({
                    preserveViewport: true,
                    suppressInfoWindows: true,
                    suppressMarkers: true,
                    polylineOptions: {
                        strokeWeight: 4,
                        strokeOpacity: 0.8,
                        strokeColor: colors[index]
                    },
                    markerOptions: {
                         clickable: true,
                         zIndex: 3
                    }
                });

                var w = [];
                $.each(waypoints, function (o, i) {
                    w.push({location: {lat: parseFloat(i[0]), lng: parseFloat(i[1])}, stopover: true});
                });
                var departure_time = new Date();
                departure_time.setDate(departure_time.getDate() + 1);
                departure_time.setHours(7);
                var request = {
                    origin: {lat: start[0] , lng: start[1]},
                    destination: {lat: end[0] , lng: end[1]},
                    waypoints: w,
                    //optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING,
                    drivingOptions: {
                        departureTime: departure_time,
                    }
                };

                directions_service.route(request, function(response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directions_display[index].setDirections(response);
                    }else alert('Ocurrió un error de Google Maps: ' + status);
                });
            };

            var initialize = function() {
                var mapOptions = {
                    center: new google.maps.LatLng(merqueo[0], merqueo[1]),
                    zoom: 14,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    draggableCursor: "crosshair"
                };
                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                n = json.length - 1;
                $.each(json, function(index, route) {
                    waypoints = [];
                    colors[index] = getRandomColor();
                    font_colors[index] = getFontColor(colors[index]);
                    $.each(route, function(key, order) {
                        waypoints.push([parseFloat(order.latitude), parseFloat(order.longitude)]);
                        var type = '';
                        if( order.type == 'Marketplace'){
                          type = '<br><b>'+order.type+'</b> - <b>'+order.allied_store_name+'</b>';
                        }
                        html = '<br><b>Ruta:</b> ' + order.route
                              + type
                              + '<br><b>Pedido:</b> #' + order.id + '<br><b>Dirección: </b>' + order.address
                              + '<br><b>Barrio: </b>' + order.neighborhood + '<br><b>Horario: </b>' + order.delivery_time
                              + '<br><b>Distancia: </b>' + order.planning_distance + ' km'
                              + '<br><b>Tiempo: </b>' + order.planning_duration + ' min';
                        LatLng = new google.maps.LatLng(parseFloat(order.latitude), parseFloat(order.longitude));
                        createMarker(map, LatLng, html, order.planning_sequence, index);
                    });
                    if (index == 0)
                        start_location = [merqueo[0], merqueo[1]];
                    else start_location = [parseFloat(json[index - 1][json[index - 1].length - 1].latitude), parseFloat(json[index - 1][json[index - 1].length - 1].longitude)];
                    if (index == n)
                        end_location = [merqueo[0], merqueo[1]];
                    else{
                        last_element = waypoints[waypoints.length - 1];
                        end_location = [parseFloat(last_element[0]), parseFloat(last_element[1])];
                    }
                    calculate(start_location, end_location, waypoints, index);
                });
                createMarker(map, new google.maps.LatLng(merqueo[0], merqueo[1]), '<b>Merqueo</b>', '', 'Merqueo');
            };

            var createMarker = function(map, latlng, html, marker_label, index) {

                  if (index == 'Merqueo')
                  {
                    url = 'https://merqueo.com/assets/img/markermerqueo.png';
                    index = 0;
                    marker_image = new google.maps.MarkerImage(url,
                          new google.maps.Size(25, 25),
                          new google.maps.Point(0, 0)
                      );
                      var marker = new google.maps.Marker({
                          position: latlng,
                          map: map,
                          icon: marker_image,
                          zIndex: parseInt(index)
                      });
                      marker.myname = 'Merqueo';
                  }else{

                      /*url = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker_label + '|' + colors[index] + '|' + font_colors[index];
                      marker_image = new google.maps.MarkerImage(url,
                          new google.maps.Size(21, 34),
                          //new google.maps.Size(35, 64),
                          new google.maps.Point(0, -5)
                      );*/

                      var marker = new google.maps.Marker({
                          position: latlng,
                          map: map,
                          icon: {
                              path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                              fillColor: colors[index],
                              fillOpacity: 1,
                              strokeColor: 'black',
                              strokeWeight: 1,
                              scale: 7,
                              labelOrigin: new google.maps.Point(0,-2.4)
                          },
                          //icon: marker_image,
                          label: {
                              color: font_colors[index],
                              fontSize: '12px',
                              text: marker_label.toString()
                          },
                          zIndex: parseInt(index)
                      });
                      marker.myname = 'Pedido en ruta';
                  }

                  google.maps.event.addListener(marker, 'click', function() {
                      infowindow.setContent(html);
                      infowindow.open(map, marker);
                      current_infowindow_open = infowindow;
                  });

                  if (index != 'Merqueo'){
                      if (typeof route_markers[index] === 'undefined')
                         route_markers[index] = [];
                      route_markers[index].push(marker);
                  }

                  return marker;
            };

            function getRandomColor() {
              var letters = '0123456789ABCDEF';
              var color = '';
              for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
              }
              if ($.inArray(color, colors) == -1)
                return '#' + color;
              else return '#' + getRandomColor();
            }

            function getFontColor(color){
                var c = color.substring(1);  // strip #
                var rgb = parseInt(c, 16);   // convert rrggbb to decimal
                var r = (rgb >> 16) & 0xff;  // extract red
                var g = (rgb >>  8) & 0xff;  // extract green
                var b = (rgb >>  0) & 0xff;  // extract blue

                var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

                if (luma < 40) {
                    return '#FFFFFF'
                }
                return '#000000'
            }

            google.maps.event.addDomListener(window, 'load', initialize);

     });
    </script>

</body>
</html>
