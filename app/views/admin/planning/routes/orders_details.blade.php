@extends('admin.layout-modal')

@section('content')

<section class="content">
    <h3>
        {{ $title }}
    </h3><br>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <div class="row">
                    <div class="form-group col-xs-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th># Pedido</th>
                                    <th>Estado</th>
                                    <th>Dirección</th>
                                    <th>Ciudad</th>
                                    <th>Fecha de entrega</th>
                                    <th>Hora de entrega</th>
                                    <th>Secuencia</th>
                                    <th># Productos</th>
                                    <th># Contenedores</th>
                                    <!-- <th></th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @if (count($orders))
                                @foreach($orders as $order)
                                <tr>
                                    <td><a href="{{ route('adminOrderStorage.details', ['id' => $order->id]) }}" target="_blank">{{ $order->id }}</a></td>
                                    <td>
                                    @if($order->status == 'Validation')
                                    <span class="badge bg-maroon">Validation</span>
                                    @elseif($order->status == 'Delivered')
                                    <span class="badge bg-green">Delivered</span>
                                    @elseif($order->status == 'Cancelled')
                                    <span class="badge bg-red">Cancelled</span><br><strong>@if($order->temporarily_cancelled) (Temporalmente) @endif</strong>
                                    @elseif($order->status == 'Initiated')
                                    <span class="badge bg-yellow">Initiated</span>
                                    @elseif($order->status == 'Dispatched')
                                    <span class="badge bg-green">Dispatched</span>
                                    @elseif($order->status == 'In Progress')
                                    <span class="badge bg-orange">In Progress</span>
                                    @elseif($order->status == 'Alistado')
                                    <span class="badge bg-green">Alistado</span>
                                    @elseif($order->status == 'Enrutado')
                                    <span class="badge bg-orange">Enrutado</span>
                                    @endif
                                    </td>
                                    <td>{{ $order->address.' '.$order->neighborhood }}</td>
                                    <td>{{ $order->city }}</td>
                                    <td>{{ date('d/m/Y', strtotime($order->delivery_date)) }}</td>
                                    <td>{{ $order->delivery_time }}</td>
                                    <td align="right">{{ $order->planning_sequence }}</td>
                                    <td align="right">{{ $order->products_quantity }}</td>
                                    <td align="right">@if(!empty($order->expected_containers)){{ json_decode($order->expected_containers)->total }}@endif</td>
                                    <!-- <td>
                                        @if($order->route_status == 'En proceso' && $order->delivery_time == '12:00 pm - 5:00 pm')
                                        <button class="btn btn-primary" onclick="change_order_route({{ $order->id }})">Cambiar Ruta</button>
                                        @endif
                                    </td> -->
                                </tr>
                                @endforeach
                            @else
                                <tr><td colspan="8" align="center">No hay pedidos.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-3">
                        <div class="dataTables_info" id="table_info">Mostrando {{ count($orders) }} ítems</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="routes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form id="frm_change_order_route">
                        <div class="form-group">  
                            <label for="shift">Truno</label>
                            <select name="shift" id="shift" class="form-control" required>
                                <option value=""></option>
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="route_id">Ruta</label>
                            <select name="route_id" id="route_id" class="form-control" required>
                                <option value=""></option>
                            
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="secuence">Secuencia</label>
                            <input type="number" min="1" name="secuence" id="secuence" class="form-control" required>
                        </div>
                        <input type="hidden" name="order_id" id="order_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-change-route" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var url_list_routes = '{{ route("adminRoutes.listRoutesAjax")}}';
        var url_save_change = '{{ route("adminRoutes.changeRouteOrderAjax")}}';
        var change_order_route = function(order_id){
            $('#order_id').val(order_id);
            $('#routes_modal').modal('show');
        }
        $(document).ready(function(){
            $('#frm_change_order_route').validate({
                rules : {
                    shift : "required",
                    route_id : "required",
                    secuence : {
                        required : true,
                        min : 1
                    }
                },
                messages : {
                    shift : 'Este campo es obligatorio.',
                    route_id : 'Este campo es obligatorio.',
                    secuence: {
                        required : 'Este campo es obligatorio.',
                        min : 'Por favor ingrese un valor mayor o igual a 1.'
                    }
                }
            })
            var route_id = {{$route_id}};
            $('#shift').on('change', function(){
                var shift = $(this).val();
                if( shift != ''){
                    $.ajax({
                        url: url_list_routes,
                        type: 'POST',
                        data: { shift: shift, route_id: route_id },
                        success : function( response ){
                            if(response.status){
                                var options = '';
                                $.each(response.result, function(index, opt){
                                    options+='<option value="'+opt.id+'">'+opt.route+'</option>';
                                });
                                $('#route_id').html(options);
                            }else{
                                alert(response.message);
                            }
                        },
                    });
                }else{
                    $('#route_id').html('<option value=""></option>');
                }
                
            });
            $('#btn-change-route').on('click', function(){
                if($('#frm_change_order_route').valid()){
                    $.ajax({
                        url: url_save_change,
                        type: 'POST',
                        data: { route_id: $('#route_id').val(), order_sequence : $('#secuence').val(), order_id : $('#order_id').val() },
                        success : function( response ){
                            if(response.status){
                                alert(response.message);
                                $('#frm_change_order_route')[0].reset();
                                $('#routes_modal').modal('hide');
                                document.location.reload();
                            }else{
                                alert(response.message);
                            }
                        },
                    });
                }
            })

        })
    </script>
</section>
@endsection
