@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        @if ($admin_permissions['insert'])
        <span class="breadcrumb" style="top:0px">
            <a href="javascript:;" onclick="$('#modal-planning-date').modal('show');">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Planeación</button>
            </a>
            @if ($admin_permissions['update'])
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#modal-import-cost" data-toggle="modal" class="btn btn-primary"><i class="fa fa-upload"></i> Importar costos</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#modal-export-cost" data-toggle="modal" class="btn btn-primary"><i class="fa fa-download"></i> Exportar plantilla de costos</a>
            @endif
        </span>
        @endif
    </section>
    <section class="content">
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
            @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
            @endif
        @endif
        <!-- Modal importar costos de rutas -->
        <div class="modal fade in" id="modal-import-cost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Importar costos para rutas</h4>
                    </div>
                    <form role="form" method="POST" id="main-form-import" action="{{ route('adminRoutes.importCosts') }}" enctype="multipart/form-data" autocomplete="off">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="csv_file" class="control-label">Subir archivo:</label>
                                <input type="file" class="form-control" name="csv_file" id="csv_file">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="send-form-import-modal">Envíar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal Exportar plantilla costos de rutas -->
        <div class="modal fade in" id="modal-export-cost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Exportar plantilla de costos para rutas</h4>
                    </div>
                    <form role="form" method="GET" id="main-form-download" action="{{ route('adminRoutes.downloadTemplateCost') }}" autocomplete="off">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="city_id" class="control-label">Ciudad:</label>
                                    <select id="export_city_id" name="city_id" class="form-control">
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label for="start_date">Fecha inical:</label>
                                    <input id="start_date" name="start_date" class="form-control" placeholder="Fecha inicial para generar plantilla">
                                </div>
                                <div class="col-md-6">
                                    <label for="end_date">Fecha final:</label>
                                    <input id="end_date" name="end_date" class="form-control" placeholder="Fecha final para generar plantilla">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="send-form-download-modal">Envíar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="routes-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Fecha de planeación:</label>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control date" id="date" placeholder="Fecha de planeación">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>
                                                <button type="button" id="btn-search" class="btn btn-primary">Buscar</button>
                                            </td>
                                        </tr>
                                    </table>

                                </form>

                            </div>
                        </div>
                        <br />
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Planning Date -->
        <div class="modal fade" id="modal-planning-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Ingresa la fecha de planeación a crear</h4>
                    </div>
                    <form method="get" action="{{ route('adminRoutes.add') }}">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-xs-6">
                                    <label for="shopper-name">Ciudad</label>
                                    <select class="form-control" id="cities" name="city_id">
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-6">
                                    <label for="shopper-name">Bodega</label>
                                    <select class="form-control" id="warehouse_id" name="warehouse_id">
                                        <option value="">Selecciona</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-8">
                                    <label for="shopper-name">Fecha de planeación</label>
                                    <input class="form-control date" type="text" name="planning_date" placeholder="DD/MM/YYYY" required="" value="{{ date('d/m/Y', strtotime(date('Y-m-d').' + 1 days')) }}">
                                </div>
                                <div class="col-xs-3">
                                    <label style="color: #FFF;">|</label>
                                    <button type="submit" class="btn btn-primary form-control upload-file-panning">Continuar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
    <script>
        var web_url_ajax = "{{ route('adminRoutes.index') }}";
        var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
        $(document).ready(function() {

            $('.date').datetimepicker({
                format: 'DD/MM/YYYY',
            });
            $('#start_date, #end_date').datetimepicker({
                format: 'DD/MM/YYYY',
            });

            $.validator.addMethod("dateFormat", function(value,element) {
                    return value.match(/^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/);
                }, "Please enter a date using format dd/mm/yyyy"
            );

            $("#start_date").on("dp.change", function (e) {
                if ($('#end_date').data("DateTimePicker").date()) {
                    $('#start_date').data("DateTimePicker").maxDate($('#end_date').data("DateTimePicker").date());
                } else {
                    $('#start_date').data("DateTimePicker").maxDate(new Date());
                }
            });

            $("#end_date").on("dp.change", function (e) {
                if ($('#start_date').data("DateTimePicker").date()) {
                    $('#end_date').data("DateTimePicker").minDate($('#start_date').data("DateTimePicker").date());
                }
                $('#end_date').data("DateTimePicker").maxDate(new Date());
            });

            $('#cities').on('change', function(){
                $.ajax({
                    url : warehouse_url_ajax,
                    method : 'GET',
                    data : { city_id : $('#cities').val() },
                    success : function( request ){
                        console.log(request);
                        var options = '';
                        $.each(request, function( index, name){
                            options += '<option value="'+index+'">'+name+'</option>';
                        })
                        $('#warehouse_id').html(options);
                    }
                });
            });
            if($('#cities').length > 0){
                $('#cities').trigger('change');
            }
            $("#send-form-import-modal").click(function(event) {
                let validator = $("#main-form-import").validate({
                    rules: {
                        csv_file: {
                            required: true,
                        },
                    },
                });
                if(validator.form()) {
                    return confirm('¿Deseas imporar los datos de la hoja de calculo seleccionada? recuerda que si tienes costos guardados puede que sean reemplazados.');
                }
            });
            $("#send-form-download-modal").click(function () {
                let validator = $("#main-form-download").validate({
                    rules: {
                        export_city_id: {
                            required: true,
                        },
                        start_date: {
                            required: true,
                            dateFormat: true,
                        },
                        end_date: {
                            required: true,
                            dateFormat: true,
                        },
                    }
                });
            });
            //$('#modal-planning-date').
            paging(1, '');
            @if (Session::has('file_url'))
            setTimeout(
                function() {
                    window.location = '{{ Session::get('file_url') }}';
                }, 2000);
                <?php Session::forget('file_url'); ?>
            @endif
        });

    </script>
    @endsection
@else
    @section('content')
    <table id="routes-table" class="admin-routes-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>Ciudad</th>
                <th>Bodega</th>
                <th>Fecha de planeación</th>
                <th>Turno</th>
                <th>Estado</th>
                <th># Rutas</th>
                <th>Fecha de creación</th>
                <th>Creada por</th>
                <th>Detalle</th>
            </tr>
        </thead>
        <tbody>
            @if(count($planning_routes))
            	@foreach($planning_routes as $planning_route)
                <tr>
                    <td>{{ $planning_route->city }}</td>
                    <td>{{$planning_route->warehouse}}</td>
                    <td>{{ date("d M Y", strtotime($planning_route->planning_date)) }}</td>
                    <td>{{ $planning_route->shift }}</td>
                    <td>@if($planning_route->status == 'En proceso')<span class="badge bg-orange">{{ $planning_route->status }}</span>@else<span class="badge bg-green">{{ $planning_route->status }}</span>@endif</td>
                    <td>{{ $planning_route->routes }}</td>
                    <td>{{ date("d M Y g:i a", strtotime($planning_route->created_at)) }}</td>
                    <td>{{ $planning_route->admin }}</td>
                    <td align="center">
                        <div class="btn-group">
                           <a class="btn btn-xs btn-default" href="{{ route('adminRoutes.details', ['city_id' => $planning_route->city_id, 'date' => $planning_route->planning_date, 'warehouse_id' => $planning_route->warehouse_id, 'shift' => $planning_route->shift]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="9" align="center">No hay datos.</td></tr>
            @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="table_info">Mostrando {{ count($planning_routes) }} ítems</div>
        </div>
    </div>
    @endsection
@endif
