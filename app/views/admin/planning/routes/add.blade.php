@extends('admin.layout')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>

<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });

    $('.orders-details').fancybox({
        fitToView   : false,
        width       : '90%',
        height      : '95%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });
</script>
<a href="#processing" class="fancybox unseen"></a>
<div id="processing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>
<script>
    var web_url_ajax = "{{ route('adminRoutes.add') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
    @if(Session::has('message'))
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif
    <div class="row">
        <div class="col-xs-12">
            <form role="form" method="post" id="planning-routes-form" action="{{ route('adminRoutes.add') }}">
                
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="form-group col-xs-2">
                                <label>Ciudad</label>
                                <select id="city_id" name="city_id" class="form-control">
                                    <option value="{{ $city->id }}">{{ $city->city }}</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-2">
                                <label>Bodega</label>
                                <select class="form-control" name="warehouse_id" id="warehouse_id">
                                    <option value="{{$warehouse->id}}">{{$warehouse->warehouse}}</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-2">
                                <label>Fecha de planeación</label>
                                <input type="text" class="form-control" readonly="readonly" name="planning_date" value="@if(isset($post_data['planning_date'])){{ $post_data['planning_date'] }}@else{{ Input::get('planning_date') }}@endif">
                            </div>
                            <div class="form-group col-xs-2">
                                <label>Turno</label>
                                <select name="shift" class="form-control">
                                    @foreach ($shifts as $id => $shift)
                                        <option value="{{ $id }}" @if(isset($post_data['shift']) && $post_data['shift'] == $id)) selected="selected" @endif>{{ $shift }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-xs-2 text-left">
                                <label style="display: block;">&nbsp</label>
                                <button type="submit" id="btn-planning-route" class="btn btn-primary">Crear planeación de rutas</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="box-body">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Pedidos para entregar el {{ Input::get('planning_date') }} por horario</legend>
                                </fieldset>
                            </div>
                            <div class="form-group col-xs-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Ciudad</th>
                                            <th>Horario</th>
                                            <th># Pedidos</th>
                                            <th># Pedidos sin zona</th>
                                            <th># Pedidos con dirección errada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($orders))
                                        @foreach($orders as $order)
                                        <tr>
                                            <td>{{ $order->city }}</td>
                                            <td>{{ $order->delivery_time }}</td>
                                            <td align="right">{{ $order->orders }}</td>
                                            <td align="right">
                                                @if ($order->no_zone_orders)
                                                <a href="{{ route('adminRoutes.getOrderDetails', ['id' => 0, 'city_id' => $order->city_id, 'delivery_date' => $planning_date, 'delivery_time' => $order->delivery_time, 'type' => 'zone']) }}" data-fancybox-type="iframe" class="orders-details orders-details-link"><b>{{ $order->no_zone_orders }}</b></a>
                                                <font color="red">(Se debe asignar una zona)</font>
                                                @else
                                                {{ $order->no_zone_orders }}
                                                @endif
                                            </td>
                                            <td align="right">
                                                @if ($order->wrong_address_orders)
                                                <a href="{{ route('adminRoutes.getOrderDetails', ['id' => 0, 'city_id' => $order->city_id, 'delivery_date' => $planning_date, 'delivery_time' => $order->delivery_time, 'type' => 'address']) }}" data-fancybox-type="iframe" class="orders-details orders-details-link"><b>{{ $order->no_zone_orders }}</b></a>
                                                <font color="red">(Se debe asignar una dirección valida)</font>
                                                @else
                                                {{ $order->wrong_address_orders }}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr><td colspan="6" align="center">No hay pedidos.</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){

    $('#date').datetimepicker({
        format: 'DD/MM/YYYY',
    });

    $.validator.addMethod("dateFormat", function(value,element) {
            return value.match(/^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/);
        }, "Please enter a date using format dd/mm/yyyy"
    );

    $('body').on('click', '#planning-routes-form button[type="submit"]', function(event) {
        event.preventDefault();
        if (confirm('¿Esta seguro que deseas crear la planeación de rutas para este turno?')) {
            $(this).addClass('disabled');
            $('.fancybox').trigger('click');
            $('#planning-routes-form').submit();
        }
    });
});

</script>

@stop