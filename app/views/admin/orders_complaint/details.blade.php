@if (!Request::ajax())
@extends('admin.layout')

@section('content')

    <script>
        var delivery_windows;
    </script>
    <style type="text/css">
        #modal-products .modal-dialog{
            width: 70%!important;
        }
        #modal-products .error{
            color:#ff0000;
            font-size: 13px;
            display: none;
        }
    </style>
    <section class="content-header">
        <h1>
            {{ $title }} # {{ $order->id }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">

        </span>
    </section>

<section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
    @endif
    @if(Session::has('success') )
        <div class="alert alert-success">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
    @endif
    @if(Session::has('warning') )
        <div class="alert alert-warning">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('warning') }}
        </div>
    @endif
    <div class="row">
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Pedido</h4>
                <p><label>Estado</label> - <b>{{ $order->status }}</b></p>
                <p><label>Referencia</label> - {{ $order->reference }}</p>
                @if (!empty($order->invoice_number))
                <p><label>Número factura</label> - {{ $order->invoice_number }}
                </p>
                @endif
                <p><label>Origen</label> - {{ $order->source }} {{ $order->source_os }}</p>
                <p><label>Tienda</label> - {{ $order->store_name.' '.$order->store_city }}</p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Pago</h4>
                <p><label>Método de pago</label> - {{ $order->payment_method }}
                @if ($order->status == 'Delivered' && $order->payment_method == 'Efectivo y datáfono')
                    (Efectivo: ${{ number_format($order->user_cash_paid, 0, ',', '.') }} Datáfono: ${{ number_format($order->user_card_paid, 0, ',', '.') }})
                @endif
                @if($order->payment_method == 'Tarjeta de crédito')(Online) @endif
                </p>
                @if ($order->payment_method == 'Tarjeta de crédito' && $order->cc_token != '')
                    <p><label>Tarjeta de crédito</label> - **** {{ $order->cc_last_four.' '.$order->cc_type.' ('.$order->cc_country.')' }}</p>
                @if (!empty($order->cc_holder_name))
                    <p><label>Propietario</label> - {{ $order->cc_holder_name }}</p>
                @endif
                    <p><label>Token</label> - {{ $order->cc_token }}</p>
                @endif
                    <p><label>Estado de pago</label> - @if($order->payment_date == '') Pendiente @else Pagado @endif</p>
                @if($order->payment_date != '')
                    <p><label>Fecha de pago</label> - {{ date("d M Y h:i a", strtotime($order->payment_date)); }}</p>
                @endif
                <p><label>Subtotal</label> - <span id="total_amount">${{ number_format(0, 0, ',', '.') }}</span></p>
                <p><label>Domicilio</label> - <span id="delivery_amount">${{ number_format(0, 0, ',', '.') }}</span></p>
                <p><label>Descuento</label> - <span id="discount_amount">${{ number_format(0, 0, ',', '.') }}</span> @if($order->discount_percentage_amount) ({{ $order->discount_percentage_amount }}%) @endif
                    @if($admin_permissions['update'] && $order->discount_amount && $order->status != 'Delivered' && $order->status != 'Cancelled')
                        <a href="{{ route('adminOrderComplaint.deleteDiscount', ['id' => $order->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar el descuento del pedido?')">Eliminar</a>
                    @endif
                </p>
                @if (!empty($order->coupon_code))
                    <p><label>Cupón</label> - {{ $order->coupon_code }}</p>
                @endif
                <p><label>Total</label> - <b><span id="total">${{ number_format(0, 0, ',', '.') }}</span></b></p>
                @if($admin_permissions['update'] && ($order->payment_method == 'Datáfono' || $order->payment_method == 'Efectivo y datáfono') && $order->status != 'Validation' && $order->status != 'Initiated')
                    <p><a href="#" onclick="show_modal('voucher')">Ver/Subir voucher</a></p>
                @endif
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Cliente</h4>
                <p><label>Nombre</label> - {{ $order->user_firstname.' '.$order->user_lastname }}</p>
                <p><label>Celular</label> - {{ $order->user_phone }}</p>
                <p><label>Email</label> - {{ $order->user_email }}</p>
                <p><label>Dirección</label> - <b>{{ $order->user_address.' '.$order->user_address_further }}</b></p>
                @if (!empty($order->user_address_neighborhood))
                <p><label>Barrio</label> - {{ $order->user_address_neighborhood }}</p>
                @endif
                <p><label>Ciudad</label> - {{ $order->city }}</p>
                @if(!empty($order->zone))
                    <p><label>Zona</label> - {{ $order->zone }}</p>
                @endif
                @if (!empty($order->sift_payment_abuse))
                    <p><label>Calificacion cliente</label> - {{ $order->sift_payment_abuse }}</p>
                @endif
                @if($order->user_score)
                    <p><label>Calificación a conductor</label> - {{ $order->user_score }}</p>
                    @if(!empty($order->user_score_comments))
                        <p><label>Comentarios a conductor</label> - {{ $order->user_score_comments }}</p>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>
                                Datos del pedido
                            </h4>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <form action="{{ route('adminOrderComplaint.saveOrderComplaint', ['id' => $order->id]) }}" class="form" id="order-complaint-form" method="POST">
                            <div class="col-xs-2">
                                <label title="Fecha de entrega del pedido" for="delivery_day">Fecha de entrega</label>
                                <select name="delivery_day" id="delivery_day" class="form-control delivery_day required">
                                    <option>Cargando...</option>
                                </select>
                            </div>

                            <div class="col-xs-2">
                                <label title="Hora de entrega del pedido" for="delivery_window_id">Hora de entrega</label>
                                <select name="delivery_window_id" id="delivery_window_id" class="form-control delivery_window_id required">
                                    <option>Cargando...</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <label title="Motivo de reclamo" for="complaint_reason">Motivo de reclamo</label>
                                <select name="complaint_reason" id="complaint_reason" class="form-control complaint_reason required">
                                    <option value="">-Selecciona-</option>
                                    <option value="Producto Faltante cobrado">Producto Faltante cobrado</option>
                                    <option value="Producto diferente al solicitado">Producto diferente al solicitado</option>
                                    <option value="Producto vencido">Producto vencido</option>
                                    <option value="Producto próximo a vencer">Producto próximo a vencer</option>
                                    <option value="Producto en mal estado">Producto en mal estado</option>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <label title="Comentarios sobre la orden" for="comments">Comentarios</label>
                                <textarea class="form-control" placeholder="Escribe observaciones especificas del pedido" name="comments"></textarea>
                            </div>
                            <div class="col-xs-2">
                                @if ($admin_permissions['update'])
                                    @if ($order->status === 'Delivered')
                                        <label style="color: #FFF; display: block;">|</label>
                                        <button type="submit" id="save" class="btn btn-success">Guardar</button>
                                    @endif
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-9">
                            <h4>
                                Productos para pedido de reclamo
                            </h4>
                        </div>
                        <div class="col-xs-3 text-right">
                            <button class="btn btn-primary" onclick="show_modal('products')">Agregar Productos</button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="session-products-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Referencia</th>
                                        <th>Nombre</th>
                                        <th>Unidad de medida</th>
                                        <th>Precio</th>
                                        <th>Precio en reclamo</th>
                                        <th>Cantidad</th>
                                        <th>Estado</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($session_products))
                                        @foreach($session_products as $product)
                                            <tr>
                                                <td>
                                                    <img src="{{ $product['product_image_url'] }}" height ="50px">
                                                </td>
                                                <td>
                                                    {{ $product['reference'] }}
                                                </td>
                                                <td>
                                                    {{ $product['name'] }}
                                                </td>
                                                <td>
                                                    {{ $product['product_quantity'] }} {{ $product['product_unit'] }}
                                                </td>
                                                <td>
                                                    @if ($product['original_price']) <p style="text-decoration: line-through;">${{number_format($product['original_price'])}}</p> <p>${{number_format($product['price'])}}</p> @else ${{number_format($product['price'])}} @endif
                                                </td>
                                                <td>
                                                    ${{number_format($product['complaint_price'])}}
                                                </td>
                                                <td>
                                                    <input type="number" name="product_quantity" class="product_cant form-control" value="{{ !empty($product['session_quantity']) ? $product['session_quantity'] : 0  }}" readonly="">
                                                </td>
                                                <td>
                                                    @if($product['complaint_status'] == 'Fullfilled')
                                                        <span class="badge bg-green">Disponible</span>
                                                    @elseif($product['complaint_status'] == 'Not Available')
                                                        <span class="badge bg-red">No disponible</span>
                                                    @elseif($product['complaint_status'] == 'Pending')
                                                        <span class="badge bg-orange">Pendiente</span>
                                                    @elseif($product['complaint_status'] == 'Returned')
                                                        <span class="badge bg-red">Devolución</span>
                                                    @else
                                                        <span class="badge bg-blue">Reemplazado</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a class="update-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-status="Returned" data-type="Product" data-order_id="{{ $order->id }}" data-store_id="{{$order->store_id}}">
                                                                    Devolución
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="update-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-status="Pending" data-type="Product" data-order_id="{{ $order->id }}" data-store_id="{{$order->store_id}}">
                                                                    Pendiente
                                                                </a>
                                                            </li>
                                                            <div class="divider"></div>
                                                            <li>
                                                                <a class="btn-remove-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-order_id="{{$order->id}}" data-store_id="{{$order->store_id}}" data-product_table="{{ $product['product_table'] }} " data-is_picking_up="{{$product['is_picking_up']}}">
                                                                    Eliminar
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="14" align="center">No hay productos asignados.</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-9">
                            <h4>
                                Productos para recoger
                            </h4>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="session-products-pickup-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Imagen</th>
                                    <th>Referencia</th>
                                    <th>Nombre</th>
                                    <th>Unidad de medida</th>
                                    <th>Precio</th>
                                    <th>Precio en reclamo</th>
                                    <th>Cantidad</th>
                                    <th>Estado</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($session_products_pickup))
                                    @foreach($session_products_pickup as $product)
                                        <tr>
                                            <td>
                                                <img src="{{ $product['product_image_url'] }}" height ="50px">
                                            </td>
                                            <td>
                                                {{ $product['reference'] }}
                                            </td>
                                            <td>
                                                {{ $product['name'] }}
                                            </td>
                                            <td>
                                                {{ $product['product_quantity'] }} {{ $product['product_unit'] }}
                                            </td>
                                            <td>
                                                @if ($product['original_price']) <p style="text-decoration: line-through;">${{number_format($product['original_price'])}}</p> <p>${{number_format($product['price'])}}</p> @else ${{number_format($product['price'])}} @endif
                                            </td>
                                            <td>
                                                ${{number_format($product['complaint_price'])}}
                                            </td>
                                            <td>
                                                <input type="number" name="product_quantity" class="product_cant form-control" value="{{ !empty($product['session_quantity']) ? $product['session_quantity'] : 0  }}" readonly="">
                                            </td>
                                            <td>
                                                @if($product['complaint_status'] == 'Fullfilled')
                                                    <span class="badge bg-green">Disponible</span>
                                                @elseif($product['complaint_status'] == 'Not Available')
                                                    <span class="badge bg-red">No disponible</span>
                                                @elseif($product['complaint_status'] == 'Pending')
                                                    <span class="badge bg-orange">Pendiente</span>
                                                @elseif($product['complaint_status'] == 'Returned')
                                                    <span class="badge bg-red">Devolución</span>
                                                @else
                                                    <span class="badge bg-blue">Reemplazado</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">

                                                        <div class="divider"></div>
                                                        <li>
                                                            <a class="btn-remove-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-order_id="{{$order->id}}" data-store_id="{{$order->store_id}}" data-product_table="{{ $product['product_table'] }}" data-is_picking_up="{{$product['is_picking_up']}}">
                                                                Eliminar
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="14" align="center">No hay productos asignados.</td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<!-- Add products Modal -->
<div class="modal fade" id="modal-products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Productos a Pedido</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <form class="modal-search-form">
                            <div class="row error-search">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger unseen"></div>
                                    <div class="alert alert-success unseen"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table width="100%" class="modal-product-request-table">
                                        <tbody>
                                            <tr>
                                                <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                <td>
                                                    <input type="text" placeholder="Nombre" name="s" id="s" class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
                                                </td>
                                                <td colspan="2" align="left">
                                                    <button type="button" id="btn-modal-search" class="btn btn-primary" data-order_id="{{ $order->id }}"  data-store_id="{{ $order->store_id }}" data-type="0">
                                                        Buscar
                                                    </button>&nbsp;&nbsp;
                                                    <button type="button" id="btn-modal-search-complaint" class="btn btn-primary" data-order_id="{{ $order->id }}"  data-store_id="{{ $order->store_id }}" data-type="1">
                                                        Cargar productos del pedido
                                                    </button>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="3"><span class="error" style="display: none;">Campo requerido</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="modal-products-request pre-scrollable">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Imagen</th>
                                                <th>Referencia</th>
                                                <th>Nombre</th>
                                                <th>Unidad de medida</th>
                                                <th>Precio</th>
                                                <th>Precio en reclamo</th>
                                                <th>Recoger producto</th>
                                                <th>Cantidad</th>
                                                <th>Agregar</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            @if ( !empty($order_products) )
                                                @foreach ($order_products as $product)
                                                    <tr>
                                                        <td>
                                                            <img src="{{ $product->product_image_url }}" height ="50px">
                                                        </td>
                                                        <td>
                                                            {{ $product->reference }}
                                                        </td>
                                                        <td>
                                                            {{ $product->name }}
                                                        </td>
                                                        <td>
                                                            {{ $product->product_quantity }} {{ $product->product_unit }}
                                                        </td>
                                                        <td>
                                                            @if ($product->original_price && $product->original_price != $product->price)
                                                                <p style="text-decoration: line-through;">
                                                                    ${{number_format($product->original_price)}}
                                                                </p>
                                                                <p>${{number_format($product->price)}}</p>
                                                            @else
                                                                ${{number_format($product->price)}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <label style="display: block">
                                                                {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::ZERO_PRICE) }} $0
                                                            </label>
                                                            <label style="display: block">
                                                                {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::SELL_PRICE) }} {{ currency_format($product->price) }}
                                                            </label>
                                                            @if (($product->special_price ?: $product->current_price) != $product->price)
                                                                <label style="display: block">
                                                                    {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::CURRENT_PRICE) }} {{ currency_format($product->special_price ?: $product->current_price) }}
                                                                </label>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>Recoger producto</label>
                                                                <input type="checkbox" name="is_picking_up" class="product_picking_up form-control" value="1">
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <input type="number" name="product_quantity" class="product_cant form-control" value="0" min="0" max="{{$product->quantity}}">
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-xs btn-success btn-add-product" href="javascript:;" data-order_product_id="{{$product->id}}" data-store_product_id="{{$product->store_product_id}}" data-order_id="{{$order->id}}" data-store_id="{{$order->store_id}}" data-original_price="{{ $product->original_price }}" data-price="{{ $product->price }}" data-product_table="order_products">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr><td colspan="14" align="center">&nbsp;</td></tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div align="center" class="modal-products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&key={{ Config::get('app.google_api_key') }}"></script>
<script type="text/javascript">
    var store_id = @if($order->store_id) {{$order->store_id}} @else 0 @endif;
    var products_flag = true;

    function show_modal(action) {
        $('#modal-' + action).modal('show');
    }

    $(function() {
        var search = (function() {
            'use strict';

            function search(args) {
                // enforces new
                if (!(this instanceof search)) {
                    return new search(args);
                }
                this.url_search = '{{ route('adminOrderComplaint.searchComplaintProductAjax') }}';
                this.search = null;
                this.store_id = null;
                this.order_id = null;
                this.type = null;
                // constructor body
            }

            search.prototype.bindActions = function() {
                var self = this;
                $('body').on('click', '#btn-modal-search', function(event){
                    event.preventDefault();
                    if ( $('.modal-search').val() != '' ){
                        $('.modal-products-request-loading').show();
                        self.search = $('.modal-search').val();
                        self.store_id = $(this).data('store_id');
                        self.order_id = $(this).data('order_id');
                        self.type = $(this).data('type');
                        self.ajax();
                    }else{
                        $('.modal-search').css({ border:'solid 1px #ff0000'});
                        $('.error').show();
                    }
                });

                $('body').on('click', '#btn-modal-search-complaint', function(event) {
                    event.preventDefault();

                    $('.modal-products-request-loading').show();
                    self.store_id = $(this).data('store_id');
                    self.order_id = $(this).data('order_id');
                    self.type = $(this).data('type');
                    self.ajax();
                });
            };

            search.prototype.ajax = function() {
                $('.modal-products-request-loading').show();
                $.ajax({
                    url: this.url_search,
                    type: 'GET',
                    dataType: 'html',
                    data: {
                        s: this.search,
                        store_id: this.store_id,
                        order_id: this.order_id,
                        type: this.type
                    }
                })
                .done(function(data) {
                    $('.modal-products-request-loading').hide();
                    $('.modal-products-request .tbody').html(data);
                })
                .fail(function(data) {
                    $('.modal-products-request-loading').hide();
                    $('.modal-products-request .tbody').html('Ocurrió un error al obtener los datos.');
                })
                .always(function(data) {
                    $('.modal-products-request-loading').hide();
                });
            };
            return search;
        }());

        var manageProduct = (function() {
            'use strict';

            function manageProduct(args) {
                // enforces new
                if (!(this instanceof manageProduct)) {
                    return new manageProduct(args);
                }
                this.url_add_product = '{{ route('adminOrderComplaint.addComplaintProductAjax') }}';
                this.url_remove_product = '{{ route('adminOrderComplaint.removeComplaintProductAjax') }}';
                this.url_update_status = '{{ route('adminOrderComplaint.updateProductStatusAjax') }}';
                this.complaint_price = null;
                this.complaint_status = 'Pending';
                this.order_id = null;
                this.original_price = null;
                this.price = null;
                this.store_product_id = null;
                this.product_table = null;
                this.quantity = null;
                this.store_id = null;
                this.is_picking_up = null;
                this.order_product_id = null;
                this.is_picking_up = null;
            }

            manageProduct.prototype.bindActions = function(args) {
                var self = this;
                $('body').on('click', '.btn-add-product', function(event) {
                    event.preventDefault();
                    self.complaint_price = $(this).parents('tr').find('input[name="complaint_price"]:checked').val();
                    var is_picking_up_field = $(this).parents('tr').find('input[name="is_picking_up"]')
                    self.is_picking_up = (is_picking_up_field.is(':checked'))?is_picking_up_field.val():0;
                    self.order_id = $(this).data('order_id');
                    self.original_price = $(this).data('original_price');
                    self.price = $(this).data('price');
                    self.store_product_id = $(this).data('store_product_id');
                    self.product_table = $(this).data('product_table');
                    self.order_product_id = $(this).data('order_product_id')
                    self.quantity = $(this).parents('tr').find('.product_cant').val();
                    self.quantity_max = $(this).parents('tr').find('.product_cant').attr('max');
                    self.store_id = $(this).data('store_id');
                    $(this).parents('tr').find('.product_cant').val(0);
                    $(this).parents('tr').find('.complaint_price').val(0);
                    if (self.quantity > 0) {
                        if (parseInt(self.quantity) <= parseInt(self.quantity_max)) {
                            self.ajax_add();
                        }else{
                            $('.alert-success').fadeOut();
                            $('.alert-danger').html('La cantidad de producto no puede ser mayor a la cantidad original.').fadeIn('fast').delay(5000).fadeOut('fast');
                        }
                    }else{
                        $('.alert-success').fadeOut();
                        $('.alert-danger').html('La cantidad de producto debe ser mayor a cero.').fadeIn('fast').delay(5000).fadeOut('fast');
                    }
                });
                $('body').on('click', '.btn-remove-product', function(event) {
                    event.preventDefault();
                    self.order_id = $(this).data('order_id');
                    self.store_product_id = $(this).data('store_product_id');
                    self.is_picking_up = $(this).data('is_picking_up');
                    self.ajax_remove($(this));
                });
                $('body').on('click', '.update-product', function(event) {
                    event.preventDefault();
                    self.store_product_id = $(this).data('store_product_id');
                    self.order_id = $(this).data('order_id');
                    self.store_id = $(this).data('store_id');
                    self.complaint_status = $(this).data('status');
                    self.ajax_update_status();
                });
            };

            manageProduct.prototype.ajax_add = function() {
                $.ajax({
                    url: this.url_add_product,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        complaint_price: this.complaint_price,
                        complaint_status: this.complaint_status,
                        order_id: this.order_id,
                        original_price: this.original_price,
                        price: this.price,
                        store_product_id: this.store_product_id,
                        product_table: this.product_table,
                        quantity: this.quantity,
                        store_id: this.store_id,
                        is_picking_up : this.is_picking_up,
                        order_product_id : this.order_product_id
                    },
                })
                .done(function(data) {
                    if (data.success) {
                        $('.alert-success').html(data.success).fadeIn('fast').delay(5000).fadeOut('fast', function() {
                            $('#modal-products').modal('hide');
                        });
                        $('#session-products-table tbody').empty().html(data.html);
                        $('#session-products-pickup-table tbody').empty().html(data.html_pickup);
                        products_flag = false;
                    }else{
                        $('.alert-danger').html(data.error).fadeIn('fast').delay(5000).fadeOut('fast');
                        products_flag = true;
                    }
                })
                .fail(function(data) {
                    console.error("error");
                });
            };

            manageProduct.prototype.ajax_remove = function(ob) {
                $.ajax({
                    url: this.url_remove_product,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        order_id: this.order_id,
                        store_product_id: this.store_product_id,
                        is_picking_up : this.is_picking_up
                    },
                })
                .done(function(data) {
                    if ( data.success ) {
                        ob.parents('tr').remove();
                    }else{
                        $('.alert-danger').html(data.error).fadeIn('fast').delay(5000).fadeOut('fast');
                    }
                })
                .fail(function(data) {
                    console.error("error al eliminar productos.");
                });

            };

            manageProduct.prototype.ajax_update_status = function() {
                $.ajax({
                    url: this.url_update_status,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        order_id: this.order_id,
                        store_product_id: this.store_product_id,
                        status: this.complaint_status,
                        store_id: this.store_id
                    }
                })
                .done(function(data) {
                    if ( data.success ) {
                        $('.alert-success').html(data.success).fadeIn('fast').delay(5000).fadeOut('fast');
                        $('#session-products-table tbody').empty().html(data.html);
                    }else{
                        $('.alert-danger').html(data.error).fadeIn('fast').delay(5000).fadeOut('fast');
                    }
                })
                .fail(function(data) {
                    console.error("error al cambiar el estado del producto");
                });

            };

            return manageProduct;
        }());

        var search = new search;
        search.bindActions();

        var add_product = new manageProduct;
        add_product.bindActions();

        var delivery_windows;

        $.ajax({
         url: "{{ route('adminOrderStorage.getStoreDeliverySlotAjax') }}",
         data: { store_id: {{ $order->store_id }}, validate_slots: 0, show_today: 1 },
         type: 'get',
         dataType: 'json',
         success:
            function(response) {
                $('#delivery_day').empty().append('<option value="">Selecciona el día</option>');
                $('#delivery_window_id').empty().append('<option value="">Selecciona la hora</option>');
                $.each(response[store_id].days, function(key, value){
                    $('#delivery_day').append('<option value="' + key + '">' + value + '</option>');
                });
                delivery_windows = response[store_id].time;
                $('#delivery_day')[0].selectedIndex = 1;
                $('*[name="delivery_day"]').trigger('change');
            }
        });

        $('.delivery_day').on('change', function() {
            $('#delivery_window_id option').remove();
            $('#delivery_window_id').append($('<option value="">Selecciona la hora</option>'));
            if ($(this).val() != '') {
                delivery_window_ids = delivery_windows[$(this).val()];
                $.each(delivery_window_ids, function(value, data) {
                    $('#delivery_window_id').append($("<option></option>").attr('value', value).text(data.text));
                });
                $('.delivery_window_id').show();
            }else{
                $('.delivery_window_id').hide();
            }
            $('#delivery_window_id')[0].selectedIndex = 1;
            $('#delivery_window_id').trigger('change');
        });

        $('#order-complaint-form').validate({
            rules: {
                complaint_reason:  "required"
            }
        });

        $('#order-complaint-form').submit(function(e) {
            if( !$('.update-product').length && $('#complaint_reason').val() != '-Selecciona-' ) {
                alert('Debe seleccionar minimo un producto para generar el reclamo');
                e.preventDefault();
            } else if ($('#complaint_reason').val()) {
                if(!confirm('¿Estas seguro que deseas generar el pedido de Bodega?')) e.preventDefault();
            }
        });
    });
</script>
@stop

@else
    @section('product-table')
        @if (isset($products))
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td><img src="{{$product['product_image_url']}}" height="50"></td>
                        <td>{{$product['reference']}}</td>
                        <td>{{$product['name']}}</td>
                        <td>{{$product['product_quantity']}} {{$product['product_unit']}}</td>
                        <td>
                            @if ($product['original_price'])
                                <p style="text-decoration: line-through;">
                                    ${{number_format($product['original_price'])}}
                                </p>
                                <p>
                                    ${{number_format($product['price'])}}
                                </p>
                            @else
                                ${{number_format($product['price'])}}
                            @endif
                        </td>
                        <td>
                            @if ( !empty($readonly) )
                                ${{number_format($product['complaint_price'])}}
                            @else
                                <label style="display: block">
                                    {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::ZERO_PRICE) }} $0
                                </label>
                                <label style="display: block">
                                    {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::CURRENT_PRICE) }} {{ currency_format($product['price']) }}
                                </label>
                                <?php $price = $product['special_price'] ?: $product['current_price'] ?>
                                @if (!empty($price) && $price != $product['price'])
                                    <label style="display: block">
                                        {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::CURRENT_PRICE) }} {{ currency_format($price) }}
                                    </label>
                                @endif
                            @endif
                        </td>
                        @if ( empty($readonly) )
                        <td>
                            <div class="">
                                {{--<label>Recoger producto</label>--}}
                                <input type="checkbox" name="is_picking_up" class="product_picking_up " value="1">
                            </div>
                        </td>
                        @endif
                        <td>
                            <input type="number" name="product_quantity" class="product_cant form-control" value="{{ !empty($product['session_quantity']) ? $product['session_quantity'] : 0 }}" @if ( !empty($readonly) ) readonly="" @endif min="0" max="{{$product['quantity']}}">
                        </td>
                        @if ( !empty($readonly) )
                            <td>
                                @if($product['complaint_status'] == 'Fullfilled')
                                    <span class="badge bg-green">Disponible</span>
                                @elseif($product['complaint_status'] == 'Not Available')
                                    <span class="badge bg-red">No disponible</span>
                                @elseif($product['complaint_status'] == 'Pending')
                                    <span class="badge bg-orange">Pendiente</span>
                                @elseif($product['complaint_status'] == 'Returned')
                                    <span class="badge bg-red">Devolución</span>
                                @else
                                    <span class="badge bg-blue">Reemplazado</span>
                                @endif
                            </td>
                        @endif
                        <td>
                            @if ( !empty($readonly) )
                                <div class="btn-group text-left">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a class="update-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-status="Returned" data-type="Product" data-order_id={{ $order->id }} data-store_id="{{$order->store_id}}">
                                                Devolución
                                            </a>
                                        </li>
                                        <li>
                                            <a class="update-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-status="Pending" data-type="Product" data-order_id="{{ $order->id }}" data-store_id="{{$order->store_id}}">
                                                Pendiente
                                            </a>
                                        </li>
                                        <div class="divider"></div>
                                        <li>
                                            <a class="btn-remove-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-order_id="{{$order->id}}" data-store_id="{{$order->store_id}}" data-product_table="{{ $product['product_table'] }}" data-is_picking_up="{{$product['is_picking_up']}}">
                                                Eliminar
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @else
                                <div class="btn-group text-center">
                                    <a class="btn btn-xs btn-success @if ( !empty($readonly) ) btn-remove-product @else btn-add-product @endif" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-order_id="{{$order_id}}" data-store_id="{{$store_id}}" data-original_price="{{ $product['original_price'] }}" data-price="{{ $product['price'] }}" data-product_table="{{ $product['product_table'] }}" data-is_picking_up="{{$product['is_picking_up']}}">
                                        <span class="glyphicon @if ( !empty($readonly) ) glyphicon-minus @else glyphicon-plus @endif"></span>
                                    </a>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
            @endif
        @else
            <tr><td colspan="14" align="center">&nbsp;</td></tr>
        @endif
    @stop
    @section('product-pickup-table')
        @if (isset($products_pickup))
            @if (count($products_pickup))
                @foreach ($products_pickup as $product)
                    <tr>
                        <td><img src="{{$product['product_image_url']}}" height="50"></td>
                        <td>{{$product['reference']}}</td>
                        <td>{{$product['name']}}</td>
                        <td>{{$product['product_quantity']}} {{$product['product_unit']}}</td>
                        <td>
                            @if ($product['original_price'])
                                <p style="text-decoration: line-through;">
                                    ${{number_format($product['original_price'])}}
                                </p>
                                <p>
                                    ${{number_format($product['price'])}}
                                </p>
                            @else
                                ${{number_format($product['price'])}}
                            @endif
                        </td>
                        <td>
                            @if ( !empty($readonly) )
                                ${{number_format($product['complaint_price'])}}
                            @else
                                <label style="display: block">
                                    {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::ZERO_PRICE) }} $0
                                </label>
                                <label style="display: block">
                                    {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::CURRENT_PRICE) }} {{ currency_format($product['price']) }}
                                </label>
                                <?php $price = $product['special_price'] ?: $product['current_price'] ?>
                                @if (!empty($price) && $price != $product['price'])
                                    <label style="display: block">
                                        {{ Form::radio('complaint_price', \admin\AdminOrderComplaintController::CURRENT_PRICE) }} {{ currency_format($price) }}
                                    </label>
                                @endif
                            @endif
                        </td>
                        <td>
                            <input type="number" name="product_quantity" class="product_cant form-control" value="{{ !empty($product['session_quantity']) ? $product['session_quantity'] : 0 }}" @if ( !empty($readonly) ) readonly="" @endif>
                        </td>
                        @if ( !empty($readonly) )
                            <td>
                                @if($product['complaint_status'] == 'Fullfilled')
                                    <span class="badge bg-green">Disponible</span>
                                @elseif($product['complaint_status'] == 'Not Available')
                                    <span class="badge bg-red">No disponible</span>
                                @elseif($product['complaint_status'] == 'Pending')
                                    <span class="badge bg-orange">Pendiente</span>
                                @elseif($product['complaint_status'] == 'Returned')
                                    <span class="badge bg-red">Devolución</span>
                                @else
                                    <span class="badge bg-blue">Reemplazado</span>
                                @endif
                            </td>
                        @endif
                        <td>
                            @if ( !empty($readonly) )
                                <div class="btn-group text-left">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a class="update-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-status="Returned" data-type="Product" data-order_id={{ $order->id }} data-store_id="{{$order->store_id}}">
                                                Devolución
                                            </a>
                                        </li>
                                        <li>
                                            <a class="update-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-status="Pending" data-type="Product" data-order_id="{{ $order->id }}" data-store_id="{{$order->store_id}}">
                                                Pendiente
                                            </a>
                                        </li>
                                        <div class="divider"></div>
                                        <li>
                                            <a class="btn-remove-product" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-order_id="{{$order->id}}" data-store_id="{{$order->store_id}}" data-product_table="{{ $product['product_table'] }}" data-is_picking_up="{{$product['is_picking_up']}}">
                                                Eliminar
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @else
                                <div class="btn-group text-center">
                                    <a class="btn btn-xs btn-success @if ( !empty($readonly) ) btn-remove-product @else btn-add-product @endif" href="javascript:;" data-store_product_id="{{$product['store_product_id']}}" data-order_id="{{$order_id}}" data-store_id="{{$store_id}}" data-original_price="{{ $product['original_price'] }}" data-price="{{ $product['price'] }}" data-product_table="{{ $product['product_table'] }}" data-is_picking_up="{{$product['is_picking_up']}}">
                                        <span class="glyphicon @if ( !empty($readonly) ) glyphicon-minus @else glyphicon-plus @endif"></span>
                                    </a>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
            @endif
        @else
            <tr><td colspan="14" align="center">&nbsp;</td></tr>
        @endif
    @stop
@endif
