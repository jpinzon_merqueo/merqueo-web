@extends('admin.layout')

@section('content')

<script>
    var web_url_ajax = "{{ route('adminCustomers.index') }}";
    var delivery_times;
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
     @if(Session::has('message') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('message') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <form role="form" method="post" id='main-form' action="{{ route('adminOrders.save') }}" enctype="multipart/form-data">
            @if($user)
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Información de Cliente Existente</h3>
                </div><!-- /.box-header -->
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="box-body">
                    <div class="form-group">
                        <label>Nombres</label>
                        <input disabled type="text" class="form-control" id="" name="first_name" placeholder="Nombres" value="{{ $post['first_name'] or $user->first_name }}">
                    </div>
                    <div class="form-group">
                        <label>Apellidos</label>
                        <input disabled type="text" class="form-control" id="" name="last_name" placeholder="Apellidos" value="{{ $post['last_name'] or $user->last_name }}">
                    </div>
                    <div class="form-group">
                        <label>Teléfono celular</label>
                        <input @if ($user->phone) disabled @else required @endif type="text" class="form-control" id="" name="phone" placeholder="Teléfono celular" value="{{ $post['phone'] or $user->phone }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input disabled type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ $post['email'] or $user->email }}">
                    </div>
                </div><!-- /.box-body -->
            </div>
            @else
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Buscar Cliente</h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label>Buscar</label>
                        <input type="text" class="form-control search" id="" name="s" placeholder="Teléfono, Nombre, Email">
                    </div>
                    <div class="container-overflow">
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
                <script>
                $(document).ready(function() {
                    action = 'customers';
                    paging(1, '');
                });
                </script>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Información del Cliente</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="form-group">
                        <label>Nombres</label>
                        <input required type="text" class="form-control" id="" name="first_name" placeholder="Nombres" value="{{ $post['first_name'] or '' }}">
                    </div>
                    <div class="form-group">
                        <label>Apellidos</label>
                        <input required type="text" class="form-control" id="" name="last_name" placeholder="Apellidos" value="{{ $post['last_name'] or '' }}">
                    </div>
                    <div class="form-group">
                        <label>Teléfono celular</label>
                        <input required type="text" class="form-control" id="" name="phone" placeholder="Teléfono celular" value="{{ $post['phone'] or '' }}">
                    </div>
                    <div class="form-group">
                        <label>¿El cliente tiene email?</label>
                        <select type="text" class="form-control required customer_has_email" name="customer_has_email">
                            <option value="0" @if (isset($post['customer_has_email']) && $post['customer_has_email'] == '0') selected="selected" @endif>No</option>
                            <option value="1" @if (isset($post['customer_has_email']) && $post['customer_has_email'] == '1') selected="selected" @endif>Sí</option>
                        </select>
                    </div>
                    <div class="customer_email unseen">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" id="email" class="form-control" name="email" placeholder="Email" value="{{ $post['email'] or '' }}">
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
            @endif

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Detalles del Pedido</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="form-group">
                        <label>Ciudad</label>
                        <select class="form-control" id="city_id" name="city_id">
                            @foreach($cities as $city)
                            <option value="{{ $city->id }}" @if ($post && $post['city_id'] == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tienda</label>
                        <select type="text" class="form-control required" id="store_id" name="store_id">
                            @foreach($stores as $st)
                                <option value="{{$st->id}}" @if (isset($post['store_id']) && $post['store_id'] == $st->id) selected="selected" @endif data-delivery-amount="{{ $st->delivery_order_amount }}">{{$st->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Escribe observaciones especificas del pedido" name="comments">{{ $post['comments'] or '' }}</textarea>
                    </div>

                    @if($user && $address)
                        <div class="radio">
                          <label>
                            <input type="radio" name="addrRadio" value="1" @if (!count($address)) checked="checked" @endif>
                            Crear una nueva dirección
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="addrRadio" value="2" @if (count($address)) checked="checked" @endif>
                            Usar una dirección existente
                          </label>
                        </div>
                        <div class="user-address @if (!count($address)) unseen @endif">
                            <input type="hidden" name="address_id" id="address_id" value="">
                            @if (count($address))
                                @foreach($address as $addr)
                                <div class="bg-info">
                                    <a class="address btn btn-default btn-sm pull-right" data-id="{{ $addr['id'] }}">+ Enviar a esta dirección</a>
                                    <b>{{ $addr['label'] }}</b><br>
                                    {{ $addr['address'] }}
                                    @if (!empty($addr['address_further']))<br>{{ $addr['address_further'] }} @endif<br>
                                </div><hr>
                                @endforeach
                            @else
                            <p>No hay direcciones</p>
                            @endif
                        </div>
                    @endif
                    <div class="new-address @if ($address && count($address)) unseen @endif">
                        <div class="form-group">
                            <label>Dirección de entrega</label>
                            <div class="row col-xs-10 col-xs-offset-1" style="float: none !important">
                                <div class="form-group col-xs-4">
                                    <select id="dir1" name="dir[]" class="form-control">
                                        <option value="Calle" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Calle' || Session::get('dir_parts')[0] == 'Calle') selected="selected" @endif>Calle</option>
                                        <option value="Carrera" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Carrera' || Session::get('dir_parts')[0] == 'Carrera') selected="selected" @endif>Carrera</option>
                                        <option value="Avenida" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Avenida' || Session::get('dir_parts')[0] == 'Avenida') selected="selected" @endif>Avenida</option>
                                        <option value="Avenida Carrera" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Avenida Carrera' || Session::get('dir_parts')[0] == 'Avenida Carrera') selected="selected" @endif>Avenida Carrera</option>
                                        <option value="Avenida Calle" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Avenida Calle' || Session::get('dir_parts')[0] == 'Avenida Calle') selected="selected" @endif>Avenida Calle</option>
                                        <option value="Circular" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Circular' || Session::get('dir_parts')[0] == 'Circular') selected="selected" @endif>Circular</option>
                                        <option value="Circunvalar" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Circunvalar' || Session::get('dir_parts')[0] == 'Circunvalar') selected="selected" @endif>Circunvalar</option>
                                        <option value="Diagonal" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Diagonal' || Session::get('dir_parts')[0] == 'Diagonal') selected="selected" @endif>Diagonal</option>
                                        <option value="Manzana" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Manzana' || Session::get('dir_parts')[0] == 'Manzana') selected="selected" @endif>Manzana</option>
                                        <option value="Transversal" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Transversal' || Session::get('dir_parts')[0] == 'Transversal') selected="selected" @endif>Transversal</option>
                                        <option value="Vía" @if (isset($post['dir'][0]) && $post['dir'][0] == 'Vía' || Session::get('dir_parts')[0] == 'Vía') selected="selected" @endif>Vía</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="text" id="dir2" name="dir[]" value="{{ $post['dir'][2] or Session::get('dir_parts')[2]}}" class="form-control">
                                </div>
                                <label for="dir3" class="col-xs-1 text-center">#</label>
                                <div class="form-group col-xs-2">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="hidden" value="#" name="dir[]">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="text" id="dir3" name="dir[]" value="{{ $post['dir'][6] or Session::get('dir_parts')[6]}}" class="form-control">
                                </div>
                                <label for="dir4" class="col-xs-1 text-center">-</label>
                                <div class="form-group col-xs-2">
                                    <input type="hidden" value="-" name="dir[]">
                                    <input type="text" id="dir4" name="dir[]" value="{{ $post['dir'][8] or Session::get('dir_parts')[8]}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Indicaciones</label>
                            <input type="text" class="form-control" id="" name="address_further" placeholder="Indicaciones" value="{{ $post['address_further'] or '' }}">
                        </div>

                        <div class="form-group">
                            <label>Tipo dirección</label>
                            <select type="text" class="form-control required" name="label">
                                <option value="Casa" @if (isset($post['address_name']) && $post['address_name'] == 'Casa') selected="selected" @endif>Casa</option>
                                <option value="Apartamento" @if (isset($post['address_name']) && $post['address_name'] == 'Apartamento') selected="selected" @endif>Apartamento</option>
                                <option value="Oficina" @if (isset($post['address_name']) && $post['address_name'] == 'Oficina') selected="selected" @endif>Oficina</option>
                            </select>
                        </div>
                    </div><br><!-- new-address -->

                   <div class="form-group">
                        <label>Fecha de entrega</label>
                        <select type="text" class="form-control required" id="delivery_day" name="delivery_day">
                            <option value="">Cargando...</option>
                        </select>
                    </div>
                    <div class="form-group delivery_time unseen">
                        <select type="text" class="form-control required" id="delivery_time" name="delivery_time">
                            <option value="">Cargando...</option>
                        </select>
                    </div><br>

                    <div class="form-group">
                        <label>Método de pago</label>
                        <select type="text" class="form-control required" name="payment_method">
                            <option value="Efectivo" @if (isset($post['payment_method']) && $post['payment_method'] == 'Efectivo') selected="selected" @endif>Efectivo</option>
                            <option value="Datáfono" @if (isset($post['payment_method']) && $post['payment_method'] == 'Datáfono') selected="selected" @endif>Datáfono</option>
                        </select>
                    </div>

                </div><!-- /.box-body -->
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Productos del Pedido</h3>
                </div><!-- /.box-header -->

                <div class="box-body order-products">
                    <div class="form-group @if (Session::get('admin_designation') == 'Cliente' && Session::get('admin_designation_store_id') == 26) unseen @endif">
                        <label>Tipo de Pedido</label>
                        <select id="order_type" name="order_type" class="form-control required">
                            <option value="callcenter" @if (isset($post['order_type']) && $post['order_type'] == 'callcenter') selected="selected" @endif>Callcenter Surtifruver</option>
                            <option value="normal" @if (isset($post['order_type']) && $post['order_type'] == 'normal') selected="selected" @endif>Merqueo.com</option>
                        </select>
                    </div>

                @if (isset($post['product']))
                    @foreach ($post['product'] as $key => $name)
                    <div class="form-group product-item-block">
                        @if ($key == 0)
                        <label>Producto</label>
                        @else
                        <label class="col-xs-12" style="padding-left: 0">Producto<a href="javascript:;" style="float: right" class="product-delete text-right">X</a></label>
                        @endif
                        <input type="text" autocomplete="off" class="form-control product-item product-name" id="" name="product[]" placeholder="Nombre" value="{{ $name }}">
                        <div class="autocomplete clearfix"></div>
                        <input type="hidden" class="product-id" name="product_id[]" value="{{ $post['product_id'][$key] }}">
                        <input type="hidden" class="product-image" name="product_image[]" value="{{ $post['product_image'][$key] }}">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control quantity" id="" name="product_quantity[]" placeholder="Cantidad" value="{{ $post['product_quantity'][$key] }}">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control product-price" id="" name="product_price[]" placeholder="Precio" value="{{ $post['product_price'][$key] }}">
                    </div>
                    @endforeach
                @else
                    <div class="form-group product-item-block">
                        <label>Producto</label>
                        <input type="text" autocomplete="off" class="form-control product-item product-name" id="" name="product[]" placeholder="Nombre" value="">
                        <div class="autocomplete clearfix"></div>
                        <input type="hidden" class="product-id" name="product_id[]" value="">
                        <input type="hidden" class="product-image" name="product_image[]" value="">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control quantity" id="" name="product_quantity[]" placeholder="Cantidad" value="">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control product-price" id="" name="product_price[]" placeholder="Precio" value="">
                    </div>
                @endif
                    <div class="form-group">
                        <div class="btn btn-success add-product">Agregar Producto</div>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="box-footer">
                <div class="col-xs-4">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td align="right"><b>Subtotal</b></td>
                            <td align="right" class="total_amount">$0</td>
                        </tr>
                        <tr>
                            <td align="right"><b>Domicilio</b></td>
                            <td align="right" class="delivery_amount">${{ number_format($store->delivery_order_amount, 0, ',', '.'); }}</td>
                        </tr>
                        <tr>
                            <td align="right"><b>Total</b></td>
                            <td align="right" class="grand_total">$0</td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-8"></div>

                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary">Crear Pedido</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>

<script>
var delivery_amount = 0;
var today = '{{ date('Y-m-d'); }}';
$(document).ready(function() {

    //cargar tiendas por ciudad
    $('#city_id').change(function() {
        var city_id = $(this).val();
        $.ajax({
             url: "{{ route('adminOrders.getStoresAjax') }}",
             data: { city_id: city_id },
             type: 'get',
             success:
                function(response) {
                    $('#delivery_day option').remove();
                    $('#delivery_day').append($('<option value="">Selecciona el día</option>'));
                    $('#delivery_time option').remove();
                    $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
                    $('#store_id option').remove();
                    $('#store_id').append('<option value="" selected="selected"></option>');
                    $.each(response, function(key, value){
                        $('#store_id').append('<option value="' + value.id + '" data-delivery-amount="' + value.delivery_order_amount + '">' + value.name + '</option>');
                    });
                }
        });
    });

    //cargar horarios por tienda
    $('#store_id').change(function() {
        var selected = $(this).find('option:selected');
        delivery_amount = selected.data('delivery-amount');
        $('.delivery_amount').html(numberWithCommas(delivery_amount));
        var store_id = $(this).val();
        var validate_slots = 1;
        if (store_id != 26){
            $('#order_type').val('normal');
            $('#order_type').trigger('change');
        }
        if (store_id == 29 || store_id == 26 || store_id == 34){
            validate_slots = 0;
        }
        $.ajax({
             url: "{{ route('adminOrders.getStoreDeliverySlotAjax') }}",
             data: { store_id: store_id, validate_slots: validate_slots },
             type: 'get',
             success:
                function(response) {
                    $('#delivery_day option').remove();
                    $('#delivery_day').append($('<option value="">Selecciona el día</option>'));
                    $('#delivery_time option').remove();
                    $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
                    $.each(response[store_id].days, function(key, value){
                        $('#delivery_day').append('<option value="' + key + '">' + value + '</option>');
                    });
                    delivery_times = response[store_id].time;
                    $('*[name="delivery_day"]').trigger('change');
                }
        });
    });
    $('#store_id').trigger('change');

    //cargar productos por tienda
    $('body').on('keyup change', '.product-item', function(){
       if ($('#order_type').val() == 'normal' && $(this).val().length > 2)
       {
           var obj = $(this);
            $.ajax({
                 url: "{{ route('adminOrders.getProductsAjax') }}",
                 data: { store_id: $('#store_id').val(), s: obj.val() },
                 type: 'get',
                 dataType: 'html',
                 success: function(response) {
                        if(response.length) {
                            obj.next().html(response);
                            obj.next().removeClass('unseen');
                        }
                    }
            });
        }else{
            if ($(this).val().length < 3){
                $(this).next().addClass('unseen').html('');
            }
        }
    }).on('focusout', '.product-item-block', function(){
        var obj =$(this);
        setTimeout(function() {obj.find('.autocomplete').addClass('unseen'); }, 200);
    }).on('focus', '.product-item', function(){
        if ($(this).val().length > 3 && $(this).next().html() != '') {
            $(this).next().removeClass('unseen');
        }
    });

    $('body').on('click', '.get-product-item', function(){
        $(this).closest('.product-item-block').find('.product-id').val($(this).attr('data-id'));
        $(this).closest('.product-item-block').find('.product-image').val($(this).attr('data-image'));
        $(this).closest('.product-item-block').find('.product-name').val($(this).attr('data-name'));
        $(this).closest('.product-item-block').next().next().find('.product-price').val($(this).attr('data-price'));
    });

    //actualizar totales
    $('body').on('focusout', '.product-price', function(){
        update_totals();
    });
    $('body').on('focusout', '.quantity', function(){
        update_totals();
    });
    update_totals();

   $('.add-product').click(function() {
        html = $('.product_template:last').clone(true, true);
        $(html).insertBefore(this);
        $('.product_template').eq(-2).removeClass('unseen');
        if ($('#order_type').val() == 'normal')
            $('.product-price').eq(-2).removeClass('unseen');
        update_totals();
    });

    //dia de entrega
    $('#order_type').on('change', function() {
        if ($(this).val() == 'normal')
            $('.order-products .product-price').removeClass('unseen');
        else{
            $('.order-products .product-price').each(function(){
                $(this).addClass('unseen');
                $(this).removeClass('error');
            });
        }
    });
    $('#order_type').trigger('change');

    //dia de entrega
    $('*[name="delivery_day"]').on('change', function() {
        $('#delivery_time option').remove();
        $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
        if ($(this).val() != '') {
            times = delivery_times[$(this).val()];
            $.each(times, function(value, text) {
                $('#delivery_time').append($("<option></option>").attr('value', value).text(text));
            });
            $('.delivery_time').show();
        }else{
            $('.delivery_time').hide();
        }
    });

    $('input[name="addrRadio"]').on('ifChanged', function (e) {
        $('input[name="address_id"]').val('');

        if(parseInt($(this).val()) == 2) {
            $('.user-address').slideDown();
            $('.new-address').slideUp();
        } else {
            $('.user-address').slideUp();
            $('.new-address').slideDown();
        }
    })

    $('.address').click(function (e) {
        $('.address').addClass('btn-default').removeClass('btn-primary');

        $(this).addClass('btn-primary');
        $('input[name="address_id"]').val($(this).attr('data-id'));
    })

    $('*[name="delivery_day"]').trigger('change');

    $('body').on('click', '.product-delete', function() {
        $(this).parent().parent().parent().remove();
        update_totals();
    });

    $('.customer_has_email').change(function(){
        if ($(this).val() == 1){
            $('#email').val('');
            $('.customer_email').show();
        }else{
            $('.customer_email').hide();
            $('#email').val('domicilios@surtifruver.com');
        }
    });
    $('.customer_has_email').trigger('change');

    $("#main-form").submit(function(e) {
        //e.preventDefault();

        if ($('#address_id').length){
            if ($('input[name=addrRadio]:checked', '#main-form').val() == 2 && $('#address_id').val() == ''){
                alert('Debes seleccionar una dirección de entrega.');
                return false;
            }else{
                if ($('input[name=addrRadio]:checked', '#main-form').val() == 1
                && ($('#dir2').val() == '' || $('#dir3').val() == '' || $('#dir4').val() == '')){
                    alert('Debes ingresar la dirección de entrega completa.');
                    return false;
                }
            }
        }else{
            if ($('#dir2').val() == '' || $('#dir3').val() == '' || $('#dir4').val() == ''){
                alert('Debes ingresar la dirección de entrega completa.');
                return false;
            }
        }

        if ($('#delivery_day').val() == '' || $('#delivery_time').val() == ''){
            alert('Debes ingresar la fecha y hora de entrega.');
            return false;
        }

        $('.order-products .product-name').each(function(){
            if (!$(this).val().length){
               $(this).addClass('error');
            }else $(this).removeClass('error');
        });

        $('.order-products .quantity').each(function(){
            if (!$(this).val().length){
               $(this).addClass('error');
            }else{
                if ($('#order_type').val() != 'callcenter'){
                    if ($(this).val() == 0)
                        $(this).addClass('error');
                    else $(this).removeClass('error');
                }else $(this).removeClass('error');
            }
        });

        if ($('#order_type').val() == 'normal'){
            $('.order-products .product-price').each(function(){
                if (!$(this).val().length){
                   $(this).addClass('error');
                }else{
                    if ($('#order_type').val() != 'callcenter'){
                        /*if ($(this).val() == 0)
                            $(this).addClass('error');
                        else $(this).removeClass('error');*/
                    }else $(this).removeClass('error');
                }
            });
        }

        if ($('.order-products .error').length){
            alert('Debes ingresar todos los campos requeridos en los productos.');
           return false;
        }

        $(this).submit();
        return false;
    });

    function update_totals()
    {
        total_amount = 0;
        $('.product-price').each(function(){
            qty = $(this).parent().prev().find('.quantity').val();
            if (qty != '' && $(this).val() != ''){
                total_amount = total_amount + (parseInt($(this).val()) * parseInt(qty))
            }
        });
        $('.total_amount').html(numberWithCommas(parseInt(total_amount)));
        grand_total = total_amount + delivery_amount;
        $('.grand_total').html(numberWithCommas(parseInt(grand_total)));
    }

});
</script>
<div class="product_template unseen">
    <div class="form-group product-item-block">
        <label class="col-xs-12" style="padding-left: 0">Producto<a href="javascript:;" style="float: right" class="product-delete text-right">X</a></label>
        <input type="text" autocomplete="off" class="form-control product-item product-name" id="" name="product[]" placeholder="Nombre" value="">
        <div class="autocomplete clearfix"></div>
        <input type="hidden" class="product-id" name="product_id[]" value="">
        <input type="hidden" class="product-image" name="product_image[]" value="">
    </div>
    <div class="form-group">
        <input type="number" class="form-control quantity" id="" name="product_quantity[]" placeholder="Cantidad" value="">
    </div>
    <div class="form-group">
        <input type="number" class="form-control product-price unseen" id="" name="product_price[]" placeholder="Precio" value="">
    </div>
</div>

@stop