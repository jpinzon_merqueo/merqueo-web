@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<script>
    var web_url_ajax = "{{ route('adminOrders.index') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    @if ($admin_permissions['insert'])
    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminOrders.add') }}">
        <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Pedido</button>
        </a>
    </span>
    @endif
</section>
<section class="content">
@if(Session::has('success') )
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Hecho!</b> {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error') )
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alerta!</b> {{ Session::get('error') }}
</div>
@endif

	<div class="row">
	    <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                	<div class="row">
                	    <div class="col-xs-12">
                	        <form id="search-form">
                    	        <table width="100%" class="orders-table">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td>
                                            <select id="city_id" name="city_id" class="form-control">
                                                <option value="">Selecciona</option>
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if($city->id == 2) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Shopper:</label>&nbsp;</td>
                                        <td><select id="shopper_id" name="shopper_id" class="form-control">
                                            <option value="">Selecciona</option>
                                            @if (count($shoppers))
                                                @foreach ($shoppers as $shopper)
                                                @if ( $shopper->city_id == Session::get('admin_city_id') )
                                                    <option value="{{ $shopper->id }}">{{ $shopper->first_name.' '.$shopper->last_name }}</option>
                                                @endif
                                                @endforeach
                                            @endif
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Tienda:</label>&nbsp;</td>
                                        <td>
                                            <select id="store_id" name="store_id" class="form-control">
                                                <option value="">Selecciona</option>
                                                @if (count($stores))
                                                @foreach ($stores as $store)
                                                    @if ( $store->city_id == Session::get('admin_city_id') )
                                                        <option value="{{ $store->id }}">{{ $store->name }}</option>
                                                    @endif
                                                @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td align="right"><label>Zona:</label>&nbsp;</td>
                                        <td><select id="zone_id" name="zone_id" class="form-control">
                                            <option value="">Selecciona</option>
                                            @if (count($zones))
                                                @foreach ($zones as $zone)
                                                @if ($zone->city_id == Session::get('admin_city_id'))
                                                    <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                                                @endif
                                                @endforeach
                                            @endif
                                        </select></td>
                                        <td align="right"><label>Revisado:</label>&nbsp;</td>
                                        <td><select id="revision" name="revision" class="form-control">
                                            <option value="">Selecciona</option>
                                            <option value="1">Sí</option>
                                            <option value="0">No</option>
                                        </select></td>
                                     </tr>
                                     <tr>
                                        <td align="right"><label>Origen:</label>&nbsp;</td>
                                        <td><select id="source" name="source" class="form-control">
                                            <option value="">Selecciona</option>
                                            <option value="Web">Web</option>
                                            <option value="Device">Device</option>
                                            <option value="Web Service">Web Service</option>
                                            <option value="Callcenter">Callcenter</option>
                                        </select>
                                        <select id="source_os" name="source_os" class="unseen form-control">
                                            <option value="">Selecciona</option>
                                            <option value="iOS">iOS</option>
                                            <option value="Android">Android</option>
                                        </select></td>
                                        <td align="right"><label>Método de pago:</label>&nbsp;</td>
                                        <td>
                                            <select id="payment_method" name="payment_method" class="form-control">
                                                <option value="">Selecciona</option>
                                                @if (count($payment_methods))
                                                    @foreach ($payment_methods as $index => $value)
                                                    <option value="{{ $index }}">{{ $value }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td align="right"><label>Ordenar por:</label>&nbsp;</td>
                                        <td><select id="order_by" name="order_by" class="form-control">
                                            <option value="unfulfilled_orders" selected="selected">Pedidos pendientes</option>
                                            <option value="customer_delivery_date">Fecha de entrega</option>
                                            <option value="orders.id">ID de pedido</option>
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                        <td><input type="text" placeholder="ID, dirección, nombre, celular, email, referencia" class="search form-control"></td>
                                        <td align="right"><label>Mostrar pedidos:</label>&nbsp;</td>
                                        <td><select id="show_orders" name="show_orders" class="form-control">
                                            <option value="">Selecciona</option>
                                            <option value="today" selected="selected">Solo de hoy</option>
                                        </select></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Estado:</label>&nbsp;</td>
                                        <td><select id="status" name="status" multiple="multiple" class="form-control">
                                            <option value="Validation">Validation</option>
                                            <option value="Initiated">Initiated</option>
                                            <option value="In Progress">In Progress</option>
                                            <option value="Dispatched">Dispatched</option>
                                            <option value="Delivered">Delivered</option>
                                            <option value="Cancelled">Cancelled</option>
                                            <!--<option value="In Enlistment">In Enlistment</option>
                                            <option value="Enlisted">Enlisted</option>-->
                                        </select></td>
                                        <td>
                                            <div class="col-xs-offset-2 col-xs-2 orders-scheduled_delivery" style="float: right">&nbsp;</div><br>
                                            <div class="col-xs-offset-2 col-xs-2 orders-posible_fraud" style="float: right">&nbsp;</div><br>
                                            <div class="col-xs-offset-2 col-xs-2 orders-suspect" style="float: right">&nbsp;</div>
                                        </td>
                                        <td>&nbsp;Pedidos reprogramados<br>&nbsp;Pedidos en lista negra<br>&nbsp;Pedidos con posible fraude</td>
                                        <td><button type="button" id="btn-search" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                            <button type="reset" id="btn-reset" class="btn btn-primary">Reset</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
    				</div>
    				<br>
                	<div class="paging"></div>
					<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Allocate Shopper Modal -->
<div class="modal fade" id="modal-allocate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Asignar Shopper</h4>
            </div>
            <form method="post" action="" class="form-modal" id="allocate_form">
                <div class="modal-body">
                    <input type="hidden" class="order-id" name="id" value="">
                    <div class="form-group">
                        <div class="row form-group">
                            <div class="col-xs-3">
                                <label for="shopper-name">Shopper a asignar</label>
                                <input class="form-control" type="text" name="shopper-name" id="allocate-shopper-name" readonly="" required="">
                                <input class="form-control" type="hidden" name="shopper-id" id="allocate-shopper-id">
                            </div>
                            <div class="col-xs-3">
                                <label for="store-branch-name">Tienda a asignar</label>
                                <input class="form-control" type="text" name="store-branch-name" id="allocate-store-branch-name" readonly="" required="">
                                <input class="form-control" type="hidden" name="store-branch-id" id="allocate-store-branch-id">
                            </div>
                            <div class="col-xs-2">
                                <label for="store-branch-name">Zona</label>
                                <select class="form-control" name="zone_id" id="zone-id" data-store="">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <label for="supermarket-id">Supermercado</label>
                                <select name="supermarket-id" id="supermarket-id" class="form-control" data-store="">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <label style="color: #FFF;">|</label>
                                <button type="submit" class="btn btn-primary form-control">Asignar pedido</button>
                            </div>
                        </div>
                        <div id="map-allocate" style="height: 350px"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&key={{ Config::get('app.google_api_key') }}&sensor=true"></script>
<script>
    var map_allocate = {map: {}, shoppers: [], stores: [], customer: [], infowindow: []};
    var shopper_obj;
    var active_shoppers_obj;
    var store_branches;
    var city_id;
    var store_id;
    var bogota_lat;
    var bogota_lng;
    var medellin_lat;
    var medellin_lng;
    var order_address;
    var draw_zones;
    var form_route = '{{ route('adminOrders.updateShopper', ['id' => 'order_id']) }}';
$(document).ready(function() {
	paging(1, $('.search').val());
    $('body').on('change', '#zone-id', function(event) {
        event.preventDefault();
        var zone_id = $(this).val();
        var store_id = $(this).data('store');
        $.ajax({
            url: '{{ route('adminOrders.getShoppersByZonesAjax') }}',
            type: 'POST',
            dataType: 'json',
            data: {
                    zone_id: zone_id,
                    store_id: store_id
                }
        })
        .done(function(data) {
            $.each(map_allocate.shoppers, function(index, val){ val.setMap(null) });
            $.each(data, function(index, val) {
                if ( val.location ) {
                    var lat_lng = val.location.split(' ');
                    var new_marker = setMarker(map_allocate, lat_lng[0], lat_lng[1], val.profile, val.first_name + ' ' + val.last_name, val.num_orders);
                    map_allocate.shoppers.push(new_marker);
                    setInfoWindow_shoppers(map_allocate, new_marker, val);
                }
            });
        })
        .fail(function() {
            console.log("error al obtener los shoppers.");
        });
    });
    $('body').on('change', '#supermarket-id', function(event) {
        event.preventDefault();
        var supermarket_id = $(this).val();
        var store_id = $(this).data('store');
        $.ajax({
            url: '{{ route('adminOrders.getStoresBySupermarketAjax') }}',
            type: 'POST',
            dataType: 'json',
            data: {
                supermarket_id: supermarket_id,
                store_id: store_id
            },
        })
        .done(function(data) {
            $.each(map_allocate.stores, function(index, val){ val.setMap(null) });
            $.each(data.store_branches, function(index, val) {
                var new_marker_store = setMarker(map_allocate, val.lat, val.lng, 'stores', val.name);
                map_allocate.stores.push(new_marker_store);
                setInfoWindow_stores(map_allocate, new_marker_store, val);
            });
            console.log(data);
            console.log("success");
        })
        .fail(function(data) {
            console.log("error");
        })
        .always(function(data) {
            console.log("complete");
        });
    });
    $('#city_id').change(function () {
        var city_id = $(this).val();
        $.ajax({
            url: '{{ route('adminOrders.getStoresShoppersZonesByCityAjax') }}',
            type: 'get',
            dataType: 'json',
            data: { city_id: city_id },
        })
        .done(function(data) {
            var html = html2 = html3 = '';
            $('#store_id').empty();
            html += '<option value=""></option>';
            $.each(data.stores, function(index, val) {
                html += '<option value="'+ val.id +'">'+ val.name +'</option>';
            });
            $('#store_id').append(html);

            $('#shopper_id').empty();
            html2 += '<option value=""></option>';
            $.each(data.shoppers, function(index, val) {
                html2 += '<option value="'+ val.id +'">'+ val.first_name +' '+ val.last_name +'</option>';
            });
            $('#shopper_id').append(html2);

            $('#zone_id').empty();
            html3 += '<option value=""></option>';
            $.each(data.zones, function(index, val) {
                html3 += '<option value="'+ val.id +'">'+ val.name +'</option>';
            });
            $('#zone_id').append(html3);
        })
        .fail(function() {
            console.log("error en la consulta para obtener shoppers y tiendas por ciudad");
        });
    });

    function initAjaxMap(data) {
        $('#allocate_form').attr('action', form_route.replace('order_id', data.order.id));
        $('#allocate_form .order-id').val(data.order.id);

        if (data.zones) {
            $('#zone-id').empty();
            var select = '<option value=""></option>';
            $.each(data.zones, function(index, val) {
                select += '<option value="' + val.id + '" ' + (val.id == data.order.zone_id ? 'selected="selected"' : '') + '>' + val.name + '</option>';
            });
            $('#zone-id').html(select);
            $('#zone-id').data('store', data.order.store_id)
        }
        if ( data.supermarkets ) {
            $('#supermarket-id').empty();
            var select_html = '<option value=""></option>';
            $.each(data.supermarkets, function(index, val) {
                select_html += '<option value="' + val.id + '" ' + (val.id == data.order.zone_id ? 'selected="selected"' : '') + '>' + val.name + '</option>';
            });
            $('#supermarket-id').html(select_html);
            $('#supermarket-id').data('store', data.order.store_id);
        }

        shopper_obj = data.current_shopper;
        active_shoppers_obj = data.shoppers_active;
        store_branches = data.store_branches;
        city_id = data.order.city_id;
        store_id = data.order.store_id;
        bogota_lat = 4.686806;
        bogota_lng = -74.074663;
        medellin_lat = 6.251366;
        medellin_lng = -75.567139;
        order_address = data.order.user_address_latitude + " " + data.order.user_address_longitude;
        draw_zones = data.draw_zones;
    }

    function mapInit(lat, lng, element_container, clickHandler = false, map_obj) {
        var myOptions = { center: new google.maps.LatLng(lat, lng),zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };
        map_obj.map = new google.maps.Map(document.getElementById(element_container), myOptions);
        google.maps.Circle.prototype.contains = function(latLng) {
            return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
        }
    }

    function drawZones(map_obj) {
        var colors = ['#d28580', '#d0ab3c', '#c27bce', '#838eca', '#84ded6', '#79c17c', '#80b0c7', '#b39084', '#1e6951', '#59691e', '#69261e', '#681e69', '#00ccc4', '#0bcc00'];
        if ( draw_zones ) {
            for (var i = 0; i < draw_zones.length; i++) {
                var zone = draw_zones[i];
                var temp = new Array();
                for (var j = 0; j < zone.length; j++) {
                    var coords = zone[j].split(' ');
                    temp.push( {lat: parseFloat(coords[0]), lng: parseFloat(coords[1])} );
                }

                var bermudaTriangle = new google.maps.Polygon({
                    paths: temp,
                    strokeColor: colors[i],
                    strokeOpacity: 0.5,
                    strokeWeight: 2,
                    fillColor: colors[i],
                    fillOpacity: 0.25
                });
                bermudaTriangle.setMap(map_obj.map);
            }
        }
    }

    function setMarker(map_obj, lat, lng, type, title, num_orders = null) {
        if( type == 'Shopper Moto' ){
            if ( num_orders > 10 ) {
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper/marker_shopper_+10.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }else{
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper/marker_shopper_'+ num_orders +'.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: image,
                map: map_obj.map,
                title: title
            });
        }else if( type == 'Shopper Carro' ){
            if ( num_orders > 10 ) {
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper_notdisponible/marker_shopper_+10.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }else{
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper_notdisponible/marker_shopper_'+ num_orders +'.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: image,
                map: map_obj.map,
                title: title
            });
        }else if( type == 'stores' ){
            var image = {
                url: '{{ admin_asset_url() }}img/store/store.png',
                size: new google.maps.Size(35, 47),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 47),
                type: type
            };
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: image,
                map: map_obj.map,
                title: title
            });
        }else{
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map_obj.map,
                title: title
            });
        }
        // map_obj.markers.push(marker);
        return marker;
    }

    function setInfoWindow_shoppers(map_obj, marker, shopper_obj) {
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>' +
            '<h4 id="firstHeading" class="firstHeading">'+ shopper_obj.first_name + ' ' + shopper_obj.last_name +'</h4>'+
            '<div id="bodyContent">' +
            '<strong>Teléfono:</strong> ' + shopper_obj.phone +
            '<br>' +
            '<strong>Tipo:</strong> ' + shopper_obj.profile +
            '<br>' +
            '<strong>Pedidos pendientes:</strong> ' + shopper_obj.num_orders +
            '<br>' +
            '<strong>Ultima conexión:</strong> ' + shopper_obj.updated_at_formatted +
            '<hr style="margin: 0.5rem">' +
            '<div class="text-center">'+
            '<button type="button" class="btn btn-success assign_shopper" data-id="' + shopper_obj.id + '" data-fullname="' + shopper_obj.first_name + ' ' + shopper_obj.last_name + '" style="padding: 0.2rem 1rem;">Asignar</button>'+
            '</div>'+
            '</div>'+
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            map_obj.infowindow.push(infowindow);
            google.maps.event.addListener(infowindow, 'domready', function() {
                $('.assign_shopper').click(function(event) {
                    $('#allocate-shopper-name').removeAttr('readonly');
                    $('#allocate-shopper-name').val($(this).data('fullname'));
                    $('#allocate-shopper-name').attr('readonly', true);
                    $('#allocate-shopper-id').val($(this).data('id'));
                    infowindow.close();
                    restartIconShoppersMarkers();
                    var icon_obj = marker.getIcon();
                    if ( icon_obj.type == 'Shopper Moto' ) {
                        icon_obj.url = icon_obj.url.replace('marker_shopper', 'marker_shopper_open');
                    }else if ( icon_obj.type == 'Shopper Carro' ) {
                        icon_obj.url = icon_obj.url.replace('marker_shopper_notdisponible', 'marker_shopper_open');
                    }
                    marker.setIcon(icon_obj);
                });
            });
            marker.addListener('click', function() {
                $.each(map_allocate.infowindow, function(index, val) {
                    val.close();
                });
                infowindow.open(map_obj.map, marker);
            });
    }

    function restartIconShoppersMarkers() {
        for (var i = map_allocate.shoppers.length - 1; i >= 0; i--) {
            var icon_url = map_allocate.shoppers[i].getIcon();
            if ( icon_url.type == 'Shopper Moto' ) {
                icon_url.url = icon_url.url.replace('marker_shopper_open', 'marker_shopper');
            }else if ( icon_url.type == 'Shopper Carro' ) {
                icon_url.url = icon_url.url.replace('marker_shopper_open', 'marker_shopper_notdisponible');
            }
            map_allocate.shoppers[i].setIcon(icon_url);
        }
        return;
    }

    function restartIconStoresMarkers() {
        for (var i = map_allocate.stores.length - 1; i >= 0; i--) {
            var icon_url = map_allocate.stores[i].getIcon();
            icon_url.url = icon_url.url.replace('store_open', 'store');
            map_allocate.stores[i].setIcon(icon_url);
        }
        return;
    }

    function setInfoWindow_stores(map_obj, marker, store_obj) {
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h4 id="firstHeading" class="firstHeading">'+ store_obj.store_name + ' - ' + store_obj.name +'</h4>'+
            '<div id="bodyContent" class="text-center">'+
            '   <button type="button" class="btn btn-success assign_store" data-id="'+ store_obj.id +'" data-name="'+ store_obj.name +'">Asignar</button>'+
            '</div>';
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            map_obj.infowindow.push(infowindow);
            google.maps.event.addListener(infowindow, 'domready', function () {
                $('.assign_store').click(function(event) {
                    $('#allocate-store-branch-name').removeAttr('readonly');
                    $('#allocate-store-branch-name').val($(this).data('name'));
                    $('#allocate-store-branch-name').attr('readonly', true);
                    $('#allocate-store-branch-id').val($(this).data('id'));
                    infowindow.close();
                    restartIconStoresMarkers();
                    var icon_obj = marker.getIcon();
                    icon_obj.url = icon_obj.url.replace('store', 'store_open');
                    marker.setIcon(icon_obj);
                });
            });
            marker.addListener('click', function() {
                $.each(map_allocate.infowindow, function(index, val) {
                    val.close();
                });
                infowindow.open(map_obj.map, marker);
            });
    }

    $('body').on('click', '.open-shopper-allocate', function(event) {
        event.preventDefault();
        $('.open-shopper-allocate').attr('disabled', true);
        var order_id = $(this).data('id');
        $.ajax({
            url: '{{ route('adminOrders.getAllocateShopperInfoAjax') }}',
            type: 'POST',
            dataType: 'json',
            data: {
                    order_id: order_id
                },
        })
        .done(function(data) {
            initAjaxMap(data);
            $('#modal-allocate').modal('show');
        })
        .fail(function(data) {
            console.log("error al consultar la información de la orden");
        })
        .always(function(data) {
            $('.open-shopper-allocate').removeAttr('disabled');
        });

        $('#modal-allocate').on('shown.bs.modal', function () {
            var window_height = $(window).height();
            $('#map-allocate').height(window_height - 250);
            var lat_lng = order_address.split(' ');
            mapInit(lat_lng[0], lat_lng[1], 'map-allocate', false, map_allocate);
            drawZones(map_allocate);
            var customer_marker = setMarker(map_allocate, lat_lng[0], lat_lng[1], 'default', 'Lugar de entrega');
            map_allocate.customer.push(customer_marker);
            if ( store_branches ) {
                $.each(store_branches, function(index, val) {
                    var new_marker_store = setMarker(map_allocate, val.lat, val.lng, 'stores', val.name);
                    map_allocate.stores.push(new_marker_store);
                    setInfoWindow_stores(map_allocate, new_marker_store, val);
                });
            }
            if ( active_shoppers_obj ) {
                $.each(active_shoppers_obj, function(index, val) {
                    if ( val.location ) {
                        var lat_lng = val.location.split(' ');
                        var new_marker = setMarker(map_allocate, lat_lng[0], lat_lng[1], val.profile, val.first_name + ' ' + val.last_name, val.num_orders);
                        map_allocate.shoppers.push(new_marker);
                        setInfoWindow_shoppers(map_allocate, new_marker, val);
                    }
                });
            }
        });
        $('#modal-allocate').on('hidden.bs.modal', function () {
            $('#allocate-shopper-name').val('');
            $('#allocate-shopper-id').val('');
            $('#allocate-store-branch-name').val('');
            $('#allocate-store-branch-id').val('');
        });
        $('body').on('click', '#allocate_form .btn-primary', function(event) {
            event.preventDefault();
            $('#allocate-shopper-name').removeAttr('readonly');
            $('#allocate-store-branch-name').removeAttr('readonly');
            if( $('#allocate_form').valid() ){
                var inputs = $('#allocate_form :input').serializeArray();
                var action = $('#allocate_form').attr('action');
                $.ajax({
                    url: action,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        "shopper-id": inputs[2].value,
                        "store-branch-id": inputs[4].value,
                        "ajax": true
                    },
                    context: this
                })
                .done(function() {
                    $('.order-status-ajax_'+inputs[0].value).empty().html(inputs[1].value);
                })
                .fail(function() {
                    console.log("error al guardar el shopper");
                })
                .always(function() {
                    $('#modal-allocate').modal('hide');
                });
            }
            $('#allocate-shopper-name').attr('readonly', true);
            $('#allocate-store-branch-name').attr('readonly', true);
        });
    });
});
</script>

@else

@section('content')

<table id="orders-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre / Dirección</th>
            <th>Cant. Pedidos</th>
            <th>Tienda</th>
            <th>Ciudad / Zona</th>
            <th>Celular</th>
            <th>Items</th>
            <th>Total</th>
            <th>Método de pago</th>
            <th>Estado</th>
            <th>Shopper</th>
            @if (Session::get('admin_designation') != 'Cliente')
            <th>Fecha entrega</th>
            @endif
            <th>Revisado</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
    @if (count($orders))
        @foreach($orders as $order)
        <tr @if ($order->posible_fraud) class="orders-posible_fraud" @elseif ($order->suspect) class="orders-suspect" @elseif ($order->scheduled_delivery) class="orders-scheduled_delivery" "@endif>
            <td>{{ $order->id }}</td>
            <td>
                {{ $order->user_firstname.' '.$order->user_lastname }}
                {{ $order->user_address.' '.$order->user_address_further }}
                @if (!empty($order->user_address_neighborhood))
                <br>
                <b>Barrio: </b>{{ $order->user_address_neighborhood }}
                @endif
            </td>
            <td>{{ $order->orders_qty }}</td>
            <td>{{ $order->store }}</td>
            <td>{{ $order->city }}<br>{{ $order->zone }}</td>
            <td>{{ $order->user_phone }}</td>
            <td>{{ $order->total_products }}</td>
            <td>
                ${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}
                @if (!empty($order->coupon_code)) <br><b>Cupón:</b> {{ $order->coupon_code }} @endif
            </td>
            <td>{{ $order->payment_method }}</td>
            <td>
            @if($order->status == 'Validation')
            <span class="badge bg-maroon">Validation</span>
            @elseif($order->status == 'Delivered')
            <span class="badge bg-green">Delivered</span>
            @elseif($order->status == 'Cancelled')
            <span class="badge bg-red">Cancelled</span>
            @elseif($order->status == 'Initiated')
            <span class="badge bg-yellow">Initiated</span>
            @elseif($order->status == 'Dispatched')
            <span class="badge bg-green">Dispatched</span>
            @elseif($order->status == 'In Progress')
            <span class="badge bg-green">In Progress</span>
            @elseif($order->status == 'In Enlistment')
            <span class="badge bg-green">In Enlistment</span>
            @elseif($order->status == 'Enlisted')
            <span class="badge bg-green">Enlisted</span>
            @endif
            <p><b>Origen:</b> {{ $order->source }} {{ $order->source_os }}
            @if($order->user_score)
            <br><b>Customer Score:</b> @if($order->user_score < 4) <span class="badge bg-red">{{ $order->user_score }}</span> @else <span class="badge bg-green">{{ $order->user_score }}</span> @endif
            @endif
            </p>
            </td>
            <td class="order-status-ajax_{{$order->id}}">
        	@if($order->shopper_id)
        	{{ $order->first_name.' '.$order->last_name }}
        	@else
        	<span class="badge bg-red">Pendiente</span>
        	@endif
        	</td>
        	@if (Session::get('admin_designation') != 'Cliente')
            <td>
            	@if ($order->status != 'Delivered' && $order->status != 'Cancelled')
            	{{ get_time('left', $order->customer_delivery_date, $order->store_delivery_time_minutes); }}
            	@else
            	{{ date("d M Y",strtotime($order->customer_delivery_date)); }} @endif
        	</td>
        	@endif
        	<td align="center">@if ($order->is_checked) <img src="{{ asset_url() }}img/available.png" width="10"> @else <img src="{{ asset_url() }}img/no_available.png" width="10"> @endif</td>
            <td>
                <div class="btn-group">
                   <a class="btn btn-xs btn-default" href="{{ route('adminOrders.details', ['id' => $order->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                   <br>
                   <br>
                   <button class="btn btn-xs btn-default open-shopper-allocate" data-id="{{ $order->id }}"><i class="fa fa-motorcycle"></i></button>
                   <!-- <a class="btn btn-xs btn-default open-shopper-allocate"><span class="fa fa-motorcycle"></span></a> -->
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="14" align="center">Pedidos no encontrados.</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($orders) }} pedidos</div>
    </div>
</div>

@endif

@stop