@extends('admin.layout')

@section('content')

<script>
	var delivery_times;
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <form id="search-affected" method="POST">
                                <table width="100%" class="orders-table">
                                    <tr>
                                        <td align="right">
                                            <label>Ciudad:</label>
                                        </td>
                                        <td>
                                            <select id="city_id" name="city_id">
                                                <option></option>
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td align="right"><label>Ordenar por:</label>&nbsp;</td>
                                        <td><select id="order_by" name="order_by">
                                            <option value="unfulfilled_orders">Pedidos pendientes</option>
                                            <option value="customer_delivery_date">Fecha de entrega</option>
                                            <option value="orders.id"  selected="selected">ID de pedido</option>
                                        </select></td>
                                    </tr>
                                     <tr>
                                        <td align="right"><label>Mostrar pedidos:</label>&nbsp;</td>
                                        <td><select id="show_orders" name="show_orders">
                                            <option value=""  selected="selected">Anteriores</option>
                                            <option value="today">Solo de hoy</option>
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Buscar:</label>&nbsp;</td>
                                        <td><input type="text" placeholder="ID, dirección, nombre, celular, email, referencia" class="search" style="width: 250px"></td>
                                        <td>&nbsp;</td>
                                        <td><button type="Submit">Buscar</button>&nbsp;&nbsp;<button type="reset" id="btn-reset">Reset</button></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <br>
                        <div class="paging">
                            <table id="afected_table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre / Dirección</th>
                                        <th>Tienda</th>
                                        <th>Ciudad / Zona</th>
                                        <th>Celular</th>
                                        <th>Items</th>
                                        <th>Total</th>
                                        <th>Método de pago</th>
                                        <th>Estado</th>
                                        <th>Shopper</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if (count($orders))
                                    @foreach($orders as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>
                                            {{ $order->user_firstname.' '.$order->user_lastname }}
                                            <br>
                                            {{ $order->user_address }}
                                            @if (!empty($order->sift_payment_abuse))
                                            <br>
                                            <b>Calificacion: </b>{{ $order->sift_payment_abuse }}
                                            @endif
                                        </td>
                                        <td>{{ $order->store }}</td>
                                        <td>{{ $order->city }}<br>{{ $order->zone }}</td>
                                        <td>{{ $order->user_phone }}</td>
                                        <td>{{ $order->total_products }}</td>
                                        <td>
                                            ${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}
                                            @if (!empty($order->coupon_code)) <br><b>Cupón:</b> {{ $order->coupon_code }} @endif
                                        </td>
                                        <td>{{ $order->payment_method }}</td>
                                        <td>
                                        @if($order->status == 'Validation')
                                        <span class="badge bg-maroon">Validation</span>
                                        @elseif($order->status == 'Delivered')
                                        <span class="badge bg-green">Delivered</span>
                                        @elseif($order->status == 'Cancelled')
                                        <span class="badge bg-red">Cancelled</span>
                                        @elseif($order->status == 'Initiated')
                                        <span class="badge bg-yellow">Initiated</span>
                                        @elseif($order->status == 'Dispatched')
                                        <span class="badge bg-green">Dispatched</span>
                                        @elseif($order->status == 'In Progress')
                                        <span class="badge bg-green">In Progress</span>
                                        @elseif($order->status == 'In Enlistment')
                                        <span class="badge bg-green">In Enlistment</span>
                                        @elseif($order->status == 'Enlisted')
                                        <span class="badge bg-green">Enlisted</span>
                                        @endif
                                        <p><b>Origen:</b> {{ $order->source }} {{ $order->source_os }}
                                        @if($order->user_score)
                                        <br><b>Customer Score:</b> @if($order->user_score < 4) <span class="badge bg-red">{{ $order->user_score }}</span> @else <span class="badge bg-green">{{ $order->user_score }}</span> @endif
                                        @endif
                                        </p>
                                        </td>
                                        <td class="order-status-ajax_{{$order->id}}">
                                        @if($order->shopper_id)
                                        {{ $order->first_name.' '.$order->last_name }}
                                        @else
                                        <span class="badge bg-red">Pendiente</span>
                                        @endif
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                               <a class="btn btn-xs btn-default" href="{{ route('adminOrders.details', ['id' => $order->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                                               <br>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="14" align="center">Pedidos no encontrados.</td></tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="paginacion" align="right">
                                {{ $orders->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>


@stop