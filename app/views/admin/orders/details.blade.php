@extends('admin.layout')

@section('content')

<script>
	var delivery_times;
</script>

<section class="content-header">
    <h1>
        {{ $title }} # {{ $order->id }}
        <small>Control panel</small>
    </h1>
    <span class="breadcrumb" style="top:0px">
    @if ($admin_permissions['update'])
    @if($order->status == 'Validation')
        <a href="{{ route('adminOrders.updateStatus', ['id' => $order->id, 'status' => 'Initiated']) }}" onclick="return confirm('¿Estas seguro que deseas cambiar el estado a Initiated?')">
            <button type="button" class="btn btn-success">Initiated</button>
        </a>
    @else
        @if($order->status == 'Initiated' && ($order->shopper_id || $order->source == 'Callcenter'))
        <a href="#">
            <button type="button" class="btn btn-success" onclick="show_modal('accept-order')">Aceptar Pedido</button>
        </a>
        @else
            @if($order->status == 'In Progress')
                @if((!$order_has_pending_total_real_amount || count($order_stores) == 1) && $order->payment_method == 'Tarjeta de crédito' && empty($order->cc_charge_id))
                <a href="{{ route('adminOrders.saveCharge', ['id' => $order->id]) }}" onclick="return confirm('¿Estas seguro que deseas cobrar el total a la tarjeta de crédito?')">
                    <button type="button" class="btn btn-success">Realizar Cobro a Tarjeta</button>
                </a>
                @else
                    <a @if ($order_has_pending_total_real_amount) href="javascript:;" onclick="show_modal('dispatched')" @else href="{{ route('adminOrders.updateStatus', ['id' => $order->id, 'status' => 'Dispatched']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Dispatched?')" @endif>
                        <button type="button" class="btn btn-success">Dispatched</button>
                    </a>
                @endif
            @else
                @if($order->status == 'Dispatched')
                @if($order->payment_method == 'Tarjeta de crédito')
                <a href="{{ route('adminOrders.updateStatus', ['id' => $order->id, 'status' => 'Delivered']) }}" onclick="return confirm('¿Estas seguro que deseas actualizar el estado a Delivered?')">
                    <button type="button" class="btn btn-success">Delivered</button>
                </a>
                @else
                <button type="button" class="btn btn-success" onclick="show_modal('delivered')">Delivered</button>
                @endif
                @endif
            @endif
        @endif
    @endif
    @if(($order->status != 'Delivered' && $order->status != 'Cancelled') || ($order->status == 'Delivered' && $admin_permissions['permission2']))
    <a href="#" data-id="{{ $order->id }}">
        <button type="button" class="btn btn-danger" onclick="show_modal('reject-order')">Cancelar Pedido</button>
    </a>
    @endif
    @if($order->status == 'In Progress' || $order->status == 'Initiated' || $order->status == 'Validation')
    <a href="#" onclick="show_modal('add-product')" class="add-product">
        <button type="button" class="btn btn-primary">Agregar Producto</button>
    </a>
    @endif
    <a href="#" onclick="show_modal('note')">
        <button type="button" class="btn btn-primary">Agregar Nota</button>
    </a>
    @if($order->status == 'Initiated' || $order->status == 'In Progress' || ($order->status == 'Dispatched' && $order->payment_method != 'Tarjeta de crédito') || ($order->status == 'Dispatched' && $order->payment_method == 'Tarjeta de crédito' && empty($order->cc_charge_id)))
        <div class="btn btn-primary" onclick="show_modal('add-coupon')">Agregar Cupón</div>
    @endif
    @if($order->status != 'Initiated' || $order->status != 'In Progress')
    	@if ($order->revision_orders_required && $order->status == 'Delivered' && !$order->is_checked)
            <a href="{{ route('adminOrders.updateRevisionChecked', ['id' => $order->id]) }}" onclick="return confirm('¿Estas seguro que deseas marcar como revisado el pedido?')">
            	<button type="button" class="btn btn-success">Pedido Revisado</button>
        	</a>
		@endif
	@if (empty($order->invoice_number) || $admin_permissions['permission2'])
    <div class="btn btn-primary" onclick="show_modal('change-status')">Cambiar Estado</div>
    @endif
    @endif
    @if(($order->status == 'Dispatched' || $order->status == 'Delivered') && $order->payment_method == 'Tarjeta de crédito' && empty($order->cc_charge_id))
    <a href="{{ route('adminOrders.saveCharge', ['id' => $order->id]) }}" class="charge-amount" onclick="return confirm('¿Estas seguro que deseas cobrar el total a la tarjeta de crédito?')">
        <button type="button" class="btn btn-warning">Realizar Cobro a Tarjeta</button>
    </a>
    @endif
    @if ($admin_permissions['permission1'] && $order->payment_method == 'Tarjeta de crédito' && !empty($order->cc_charge_id) && empty($order->cc_refund_date))
    <a href="{{ route('adminOrders.saveRefund', ['id' => $order->id]) }}" class="charge-amount" onclick="return confirm('¿Estas seguro que deseas reembolsar el cobro a la tarjeta de crédito?')">
        <button type="button" class="btn btn-warning">Reembolsar Cobro a Tarjeta</button>
    </a>
    @endif
    @endif

    @if(Session::get('admin_designation') == 'Cliente' && Session::get('admin_designation_store_id') == 26 && $order->status != 'Cancelled' && $order->status != 'Delivered')
        <a href="#" onclick="show_modal('add-product')" class="add-product">
            <button type="button" class="btn btn-primary">Agregar Producto</button>
        </a>
    @endif
    @if ( $order->status == 'Delivered' )
    <a href="{{ route('adminOrderComplaint.orderComplaint', ['id' => $order->id]) }}">
        <button type="button" class="btn btn-success">Generar pedido de reclamo</button>
    </a>
    @endif
    </span>
</section>

<section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
    @if(Session::has('error') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('error') }}
    </div>
    @endif
    @if(Session::has('success') )
    <div class="alert alert-success">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Pedido</h4>
                <p><label>Estado</label> - <b>{{ $order->status }}</b></p>
                <p><label>Referencia</label> - {{ $order->reference }}</p>
                @if (!empty($order->invoice_number))
                <p><label>Número factura</label> - {{ $order->invoice_number }}
                    <a href="javascript:;" data-reference="{{ $order->id }}" data-href="{{ route('adminOrders.detailsViewInvoice', ['id' => $order->id]) }}" class="view-invoice">Ver factura</a>
                    - <a href="javascript:;" class="modal-invoice">Actualizar factura</a>
                </p>
                @endif
                <p><label>Origen</label> - {{ $order->source }} {{ $order->source_os }}</p>
                <p><label>Tienda</label> - {{ $order->store_name.' '.$order->store_city }}</p>
                <p><label>Fecha de creación</label> - {{ date("d M Y g:i a",strtotime($order->date)) }}</p>
                @if ($order->posible_fraud)
                    <p class="orders-posible_fraud"><label>Lista negra</label> - {{ $order->posible_fraud }}</p>
                @elseif ($order->suspect)
                    <p class="orders-suspect"><label>Posible fraude</label> - {{ $order->suspect }}</p>
                @endif
                @if ($order->delivery_time == 'Immediately')
                    <p>
                        <label>Fecha de entrega</label> - {{ date("d M Y g:i a", strtotime($order->customer_delivery_date)) }} - {{ $order->delivery_time }}
                        @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <a href="#" onclick="show_modal('delivery-date')">Editar</a>
                        @endif
                    </p>
                @else
                    <p>
                        <label>Fecha de entrega</label> - @if(Session::get('admin_role_id') == 3) {{ date("d M Y g:i a", strtotime($order->customer_delivery_date)) }} @else {{ date("d M Y g:i a", strtotime($order->customer_delivery_date)) }} ({{ $order->delivery_time }}) @endif
                        @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                            <a href="#" onclick="show_modal('delivery-date')">Editar</a>
                        @endif
                    </p>
                @endif
                @if($order->management_date != '')
                    <p><label>Fecha gestión</label> - {{ date("d M Y g:i a",strtotime($order->management_date)); }}</p>
                @endif
                <p><label>Comentarios</label> - {{ $order->user_comments }}</p>
                @if($order->status == 'Cancelled')
                <p><label>Motivo de cancelación</label> - {{ $order->reject_reason }}
                @foreach($order_stores as $order_store)
                - En tienda {{ $order_store->name }} @if($order_store->product_return) con devolucíon @else sin devolucíon @endif
                @endforeach
                </p>
                @if(!empty($order->reject_comments))
                <p><label>Comentarios de cancelación</label> - {{ $order->reject_comments }}</p>
                @endif
                @endif
                <p><label>Revisado</label> - @if($order->is_checked) Sí @else No @endif</p>
                @if(!empty($order->campaign))
                    <p><label>Campaña</label> - {{ ucfirst($order->campaign) }}</p>
                @endif
                <p><a href="#" title="Log de pedido" onclick="show_modal('orders_log')">Log de pedido</a></p>
                <p><a href="#" title="Notas de pedido" onclick="show_modal('notes')">Notas de pedido</a></p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Pago</h4>
                @if(!empty($order->total_real_amount) || !empty($order->management_date))
                    <p><label>Total factura</label>- ${{ number_format($order->total_real_amount, 0, ',', '.') }} @if(!empty($order->total_real_amount) && $order_has_pending_total_real_amount) (Parcial) @endif</p>
                @endif
                <p><label>Método de pago</label> - {{ $order->payment_method }}
                @if ($order->status == 'Delivered' && $order->payment_method == 'Efectivo y datáfono') (Efectivo: ${{ number_format($order['user_cash_paid'], 0, ',', '.') }} Datáfono: ${{ number_format($order['user_card_paid'], 0, ',', '.') }}) @endif
                @if($order->payment_method == 'Tarjeta de crédito')(Online) @endif
                @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled' || ($admin_permissions['permission2'] && $order->status == 'Delivered'))
                <a href="#" onclick="show_modal('payment-method')">Editar</a>
                @endif
                </p>
                @if ($order->payment_method == 'Tarjeta de crédito' && $order->cc_token != '')
                    <p><label>Tarjeta de crédito</label> - **** {{ $order->cc_last_four.' '.$order->cc_type.' ('.$order->cc_country.')' }}</p>
                @if (!empty($order->cc_holder_name))
                    <p><label>Propietario</label> - {{ $order->cc_holder_name }}</p>
                @endif
                    <p><label>Token</label> - {{ $order->cc_token }}</p>
                @endif
                    <p><label>Estado de pago</label> - @if($order->payment_date == '') Pendiente @else Pagado @endif</p>
                @if($order->payment_date != '')
                    <p><label>Fecha de pago</label> - {{ date("d M Y h:i a", strtotime($order->payment_date)); }}</p>
                @endif
                <p><label>Subtotal</label> - <span id="total_amount">${{ number_format($order->total_amount, 0, ',', '.') }}</span></p>
                <p><label>Domicilio</label> - <span id="delivery_amount">${{ number_format($order->delivery_amount, 0, ',', '.') }}</span></p>
                <p><label>Descuento</label> - <span id="discount_amount">${{ number_format($order->discount_amount, 0, ',', '.') }}</span> @if($order->discount_percentage_amount) ({{ $order->discount_percentage_amount }}%) @endif
                    @if($admin_permissions['update'] && $order->discount_amount && $order->status != 'Delivered' && $order->status != 'Cancelled')
                        <a href="{{ route('adminOrders.deleteDiscount', ['id' => $order->id]) }}" onclick="return confirm('¿Estas seguro que deseas eliminar el descuento del pedido?')">Eliminar</a>
                    @endif
                </p>
                @if (!empty($order->coupon_code))
                    <p><label>Cupón</label> - {{ $order->coupon_code }}</p>
                @endif
                <p><label>Total</label> - <b><span id="total">${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}</span></b></p>
                @if($admin_permissions['update'] && $order->payment_method == 'Datáfono' && $order->status != 'Validation' && $order->status != 'Initiated')
                    <p><a href="#" onclick="show_modal('voucher')">Ver/Subir voucher</a></p>
                @endif
            </div>
        </div>
        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Cliente</h4>
                <p><label>Nombre</label> - {{ $order->user_firstname.' '.$order->user_lastname }}</p>
                <p><label>Celular</label> - {{ $order->user_phone }}</p>
                <p><label>Email</label> - {{ $order->user_email }}</p>
                <p><label>Dirección</label> - <b>{{ $order->user_address.' '.$order->user_address_further }}</b>
                    @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                    <a href="#" onclick="show_modal('delivery-address')">Editar</a>
                    @endif
                    <a href="#" onclick="show_modal('map-coords-user')">Ver Mapa</a>
                </p>
                @if (!empty($order->user_address_neighborhood))
                <p><label>Barrio</label> - {{ $order->user_address_neighborhood }}</p>
                @endif
                <p><label>Ciudad</label> - {{ $order->city }}</p>
                @if(!empty($order->zone))
                <p><label>Zona</label> - {{ $order->zone }}</p>
                @endif
                @if (!empty($order->sift_payment_abuse))
                <p><label>Calificacion cliente</label> - {{ $order->sift_payment_abuse }}</p>
                @endif
                @if($order->user_score)
                <p><label>Calificación a Shopper</label> - {{ $order->user_score }}</p>
                @if(!empty($order->user_score_comments))
                <p><label>Comentarios a Shopper</label> - {{ $order->user_score_comments }}</p>
                @endif
                @endif
                @if ($admin_permissions['update'])
                <p><a href="#" onclick="show_modal('credits')">Log de movimientos de crédito</a></p>
                <p><a href="#" onclick="show_modal('free-delivery-log')">Log de domicilio gratis</a></p>
                <p><a href="#" onclick="show_modal('orders')">Historial de pedidos</a></p>
                @if($order->status == 'Dispatched' || $order->status == 'In Progress' && $admin_permissions['permission3'])
                <p>
                    <button type="button" class="btn btn-success" id="send-sms-action">Enviar mensaje de texto</button>
                </p>
                @endif
                    @if($order->status == 'Delivered')
                <p>
                    <form method="POST" action="{{ route('adminOrders.reSendDeliveryEmail', ['id' => $order->id]) }}" class="form-horizontal" id="re-send-email">
                        <button type="submit" class="btn btn-success">Reenviar correo de pedido entregado</button>
                    </form>
                </p>
                    @endif
                @endif
            </div>
        </div>

        <div class="col-xs-3">
            <div class="callout callout-info">
                <h4>Datos de Shopper</h4>
                @if($current_shopper)
                <p><label>Nombre</label> - {{ $current_shopper['first_name'] }} {{ $current_shopper['last_name'] }}</p>
                <p><label>Celular</label> - {{ $current_shopper['phone'] }}</p>
                <p><label>Email</label> - {{ $current_shopper['email'] }}</p>
                <p><label>Fecha de asignación</label> - {{ date("d M Y h:i a", strtotime($order->allocated_date)); }}</p>
                @if($order->received_date != '')
                    <p><label>Fecha de aceptación</label> - {{ date("d M Y h:i a", strtotime($order->received_date)); }}</p>
                @endif
                @if($order->dispatched_date != '')
                    <p><label>Fecha de despacho</label> - {{ date("d M Y h:i a", strtotime($order->dispatched_date)); }}</p>
                @endif
                @foreach($order_stores as $order_store)
                @if (!empty($order_store->total_real_amount) || !empty($order_store->invoice_image_url))
                <div class="block-order-store">
                    <h4>Pedido de {{ $order_store->name }}</h4>
                    @if ($admin_permissions['update'])
                        <p><label>Factura</label> - <a href="#" onclick="show_modal('invoice-{{ $order_store->store_id }}')">
                            @if(!empty($order_store->invoice_image_url)) Ver foto @else Subir foto @endif
                        </a></p>
                    @endif
                    @if (!empty($order_store->shopper_payment_method))
                    <p><label>Total factura</label> - ${{ number_format($order_store->total_real_amount, 0, ',', '.') }}</p>
                    @endif
                    @if (!empty($order_store->shopper_payment_method))
                    <p>
                        <label>Pagó con</label> - {{ $order_store->shopper_payment_method }}
                        @if ($order_store->shopper_payment_method == 'Efectivo') - ${{ number_format($order_store->shopper_cash_paid, 0, ',', '.') }} @endif
                        @if ($order_store->shopper_payment_method == 'Tarjeta merqueo') - ${{ number_format($order_store->shopper_card_paid, 0, ',', '.') }} @endif
                        @if ($order_store->shopper_payment_method == 'Efectivo y tarjeta merqueo') (Efectivo: ${{ number_format($order_store->shopper_cash_paid, 0, ',', '.') }} Tarjeta merqueo: ${{ number_format($order_store->shopper_card_paid, 0, ',', '.') }}) @endif
                        @if($admin_permissions['update'])
                        <a href="#" onclick="show_modal('shopper-payment-method')">Editar</a>
                        @endif
                    </p>
                    @endif
                    @if (!empty($order_store->supermarket))
                    <p>
                        <label>Compró en</label> - {{ $order_store->supermarket }}
                        @if($admin_permissions['update'])
                        <a href="#" onclick="show_modal('shopper-supermarket')">Editar</a>
                        @endif
                    </p>
                    @endif
                    @if (!empty($order_store->supermarket_authorizing_admin))
                    <p>
                        <label>Autorizado por</label> - {{ $order_store->supermarket_authorizing_admin }}
                        @if($admin_permissions['update'])
                        @if(!empty($order_store->supermarket_authorizing_admin))
                        <a href="#" onclick="show_modal('authorize-other-supermarket')">Editar</a>
                        @else
                        <a href="#" onclick="show_modal('authorize-other-supermarket')">Agregar</a>
                        @endif
                        @endif
                    </p>
                    @endif
                </div>
                @endif
                @endforeach
                    @if($admin_permissions['update'] && $order->status != 'Delivered' && $order->status != 'Cancelled')
                <div class="btn btn-success" onclick="show_modal('allocate2')">Reasignar</div>
                <div class="btn btn-success" onclick="show_modal('allocate')">Reasignar por mapa</div>
                <div class="btn btn-primary" onclick="show_modal('track')"><span class="glyphicon glyphicon-map-marker"></span> Track</div>
                    @endif
                @else
                    @if ($admin_permissions['update'])
                <div class="btn btn-success" onclick="show_modal('allocate2')">Asignar Shopper</div>
                <div class="btn btn-success" onclick="show_modal('allocate')">Reasignar por mapa</div>
                    @endif
                @endif
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="shelves-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Referencia</th>
                                <th>Nombre</th>
                                <th>Unidad de medida</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Precio original</th>
                                <th>Comentarios</th>
                                <th>Tienda</th>
                                <th>Estado</th>
                                @if($admin_permissions['update'] && $order->status != 'Cancelled' && $order->status != 'Delivered')
                                <th>Acción</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order_products as $product)
                            @if($product->order_id == $order->id)
                            <tr>
                                <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                <td>{{ $product->reference }}</td>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                                <td align="right">{{ $product->quantity }}</td>
                                <td align="right">${{ number_format($product->price, 0, ',', '.') }}</td>
                                <td align="right">@if ($product->price != $product->original_price)<strike>${{ number_format($product->original_price, 0, ',', '.') }}</strike>@else ${{ number_format($product->original_price, 0, ',', '.') }} @endif</td>
                                <td>{{ $product->product_comment }}</td>
                                <td>{{ $product->store_name }}</td>
                                <td align="center" id="status-{{ $product->id }}">
                                    @if($product->fulfilment_status == 'Fullfilled')
                                    <span class="badge bg-green">Disponible</span>
                                    @elseif($product->fulfilment_status == 'Not Available')
                                    <span class="badge bg-red">No disponible</span>
                                    @elseif($product->fulfilment_status == 'Pending')
                                    <span class="badge bg-orange">Pendiente</span>
                                    @elseif($product->fulfilment_status == 'Returned')
                                    <span class="badge bg-red">Devolución</span>
                                    @else
                                    <span class="badge bg-blue">Reemplazado</span>
                                    @endif
                                </td>
                                @if($admin_permissions['update'] && $order->status != 'Cancelled' && $order->status != 'Delivered')
                                <td>
                                    <div class="btn-group">
                                        @if(true || $product->fulfilment_status == 'Pending')
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a class="update-product" href="javascript:;" data-product="{{ $product->id }}" data-status="{{ 'Fullfilled' }}" data-type="{{ $product->type }}">Disponible</a></li>
                                            <li><a class="update-product" href="javascript:;" data-product="{{ $product->id }}" data-status="{{ 'Not Available' }}">No disponible</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onclick="edit({{ $product->id }},{{ $product->quantity }},{{ $product->price }});">Cambiar cantidad y precio</a></li>
                                            <li><a href="#" onclick="replace({{ $product->id }});">Reemplazar producto</a></li>
                                        </ul>
                                        @endif
                                    </div>
                                </td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<!-- Customer Credits Modal -->
<div class="modal fade" id="modal-credits" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Creditos de Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive container-overflow">
                            <table id="shelves-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($customer_credits))
                                        @foreach($customer_credits as $credit)
                                        <tr>
                                            <td>{{ date("d M Y g:i a",strtotime($credit->created_at)); }}</td>
                                            <td>{{ $credit->description }}</td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="2" align="center">No hay datos.</td></tr>
                                    @endif
                                </tbody>
                            </table><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Customer Free Delivery Modal -->
<div class="modal fade" id="modal-free-delivery-log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Domicilio Gratis Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive container-overflow">
                            <table id="shelves-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if (count($customer_free_deliveries))
                                    @foreach($customer_free_deliveries as $free_delivery)
                                    <tr>
                                        <td>{{ date("d M Y g:i a",strtotime($free_delivery->created_at)); }}</td>
                                        <td>{{ $free_delivery->description }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="2" align="center">No hay datos.</td></tr>
                                @endif
                                </tbody>
                            </table><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Customer Orders Modal -->
<div class="modal fade" id="modal-orders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:900px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Historial de Pedidos Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive container-overflow">
                            <table id="shelves-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Fecha</th>
                                        <th>Nombre / Dirección</th>
                                        <th>Tienda</th>
                                        <th>Items</th>
                                        <th>Total</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if (count($customer_orders))
                                    @foreach($customer_orders as $customer_order)
                                    <tr>
                                        <td>{{ $customer_order->id }}</td>
                                        <td>{{ date("d M Y g:i a",strtotime($customer_order->date)); }}</td>
                                        <td>{{ $customer_order->user_firstname.' '.$customer_order->user_lastname }}<br>{{ $customer_order->user_address.' '.$customer_order->user_address_further }}</td>
                                        <td>{{ $customer_order->store }}</td>
                                        <td>{{ $customer_order->total_products }}</td>
                                        <td>${{ number_format($customer_order->total_amount + $customer_order->delivery_amount - $customer_order->discount_amount, 0, ',', '.') }}</td>
                                        <td>
                                        @if($customer_order->status == 'Delivered')
                                            <span class="badge bg-green">Delivered</span>
                                        @elseif($customer_order->status == 'Cancelled')
                                            <span class="badge bg-red">Cancelled</span>
                                        @elseif($customer_order->status == 'Initiated')
                                            <span class="badge bg-yellow">Initiated</span>
                                        @elseif($customer_order->status == 'Dispatched')
                                            <span class="badge bg-green">Dispatched</span>
                                        @else
                                            <span class="badge bg-green">In Progress</span>
                                        @endif
                                            <p><b>Source:</b> {{ $customer_order->source }} {{ $customer_order->source_os }}
                                            @if($customer_order->user_score)
                                                <br><b>Calificación a Shopper:</b> @if($customer_order->user_score < 4) <span class="badge bg-red">{{ $customer_order->user_score }}</span> @else <span class="badge bg-green">{{ $customer_order->user_score }}</span> @endif
                                            @endif
                                            </p>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="7" align="center">No hay datos.</td></tr>
                                @endif
                                </tbody>
                            </table><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Order Notes Modal -->
<div class="modal fade" id="modal-notes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notas de Pedido</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive container-overflow">
                            <table id="shelves-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Descripción</th>
                                        <th>Usuario admin</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if (count($order_notes))
                                    @foreach($order_notes as $note)
                                    <tr>
                                        <td>{{ date("d M Y g:i a",strtotime($note->created_at)); }}</td>
                                        <td>{{ $note->description }}</td>
                                        <td>{{ $note->fullname }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="3" align="center">No hay datos.</td></tr>
                                @endif
                                </tbody>
                            </table><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Customer Note Modal -->
<div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Nota a Pedido</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.saveNote', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                    <div class="form-group reject-comments">
                        <label class="reject-label">Nota</label>
                        <textarea class="form-control required" required="required" name="note"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Dispatched Order Modal -->
<div class="modal fade" id="modal-dispatched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@if ($order_has_pending_total_real_amount) Subir total de factura @else Actualizar estado a En camino @endif</h4>
            </div>
            <form method="get" action="{{ route('adminOrders.updateStatus', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                    <input type="hidden" name="status" value="Dispatched">
                    <div class="form-group">
                        <label class="control-label">Tienda</label>
                        <select name="store_id" class="form-control required">
                            <option value="">-Selecciona-</option>
                            @foreach($order_stores as $order_store)
                            <option value="{{ $order_store->store_id }}">{{ $order_store->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Total de factura</label>
                        <input type="text" class="form-control" name="total_real_amount" id="order_total">
                    </div>
                    @if ($current_shopper && $current_shopper['profile'] != 'Shopper Cliente')
                    <div class="form-group">
                        <label class="control-label">Método de pago del Shopper</label>
                        <select name="shopper_payment_method" class="form-control required shopper-payment-method">
                            <option value="">-Selecciona-</option>
                            <option value="Efectivo">Efectivo</option>
                            <option value="Tarjeta merqueo">Tarjeta merqueo</option>
                            <option value="Efectivo y tarjeta merqueo">Efectivo y tarjeta merqueo</option>
                            <option value="No aplica">No aplica</option>
                        </select>
                    </div>
                    <div class="shopper-block-cash-card unseen">
                        <div class="form-group">
                            <label class="control-label">Total pagado en efectivo</label>
                            <input type="text" class="form-control required shopper-cash-paid" name="shopper_cash_paid" id="shopper_cash_paid">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Total pagado con tarjeta</label>
                            <input type="text" class="form-control required shopper-card-paid" name="shopper_card_paid" id="shopper_card_paid">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Supermercado donde se compro el pedido</label>
                        <select name="shopper_supermarket" class="form-control required shopper-supermarket">
                            <option value="">-Selecciona-</option>
                            @foreach($supermarkets['store'] as $supermarket)
                            <option value="{{ $supermarket->name }}">{{ $supermarket->name }}</option>
                            @endforeach
                            <option value="Otro">Otro</option>
                        </select>
                    </div>
                    <div class="shopper-other-supermarket-block unseen">
                        <div class="form-group">
                            <label class="control-label">¿Cual otro supermercado?</label>
                            <select name="shopper_supermarket_other" class="form-control required shopper-supermarket-other">
                                <option value="">-Selecciona-</option>
                                @foreach($supermarkets['others'] as $supermarket)
                                <option value="{{ $supermarket->name }}">{{ $supermarket->name }}</option>
                                @endforeach
                                <option value="Otro">Otro</option>
                            </select>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Authorize Other Supermarket Modal -->
<div class="modal fade" id="modal-authorize-other-supermarket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Autorización para comprar en otro supermercado</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateAuthorizeShopperSupermarket', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                    <div class="form-group">
                        <label class="control-label">Tienda</label>
                        <select name="store_id" class="form-control required">
                            <option value="">-Selecciona-</option>
                            @foreach($order_stores as $order_store)
                            <option value="{{ $order_store->store_id }}">{{ $order_store->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">¿Quien autoriza comprar en otro supermercado?</label>
                        <select name="shopper_supermarket_other_admin_id" class="form-control required shopper-supermarket-other-admin-id">
                            <option value="">-Selecciona-</option>
                            @foreach($supermarkets['admin_users'] as $admin_user)
                            <option value="{{ $admin_user->id }}">{{ $admin_user->fullname }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Status Order Modal -->
<div class="modal fade" id="modal-change-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cambiar Estado de Pedido</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateStatusAdmin', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                    <div class="form-group">
                        <label class="control-label">Nuevo estado de pedido</label>
                        <select class="form-control required" name="status" id="modal-new-status">
                            <option value="">-Selecciona-</option>
                            <option value="Validation">Validation</option>
                            <option value="Initiated">Initiated</option>
                            <option value="In Progress">In Progress</option>
                            <option value="Cancelled">Cancelled</option>
                        </select>
                    </div>
                    <div class="reject-block" style="display: none">
                        <div class="form-group">
                            <label class="reject-label">Razón por la cual se cancela el pedido</label>
                            <select class="form-control required" name="reject_reason">
                                <option value="">-Selecciona-</option>
                                @foreach($reasons as $reason)
                                <option value="{{ $reason->reason }}">{{ $reason->reason }}</option>
                                @endforeach
                            </select>
                        </div>
                        @foreach($order_stores as $order_store)
                        <div class="block-order-store">
                            <div class="form-group">
                                <label class="control-label">Tienda</label>
                                <select name="store_id" class="form-control required">
                                    <option value="{{ $order_store->store_id }}">{{ $order_store->name }}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="reject-label ">Estado de devolución de los productos</label>
                                <select class="form-control required" name="product_return_{{ $order_store->store_id }}">
                                    <option value="">-Selecciona-</option>
                                    <option value="1">Con devolución</option>
                                    <option value="0">Sin devolución</option>
                                </select>
                            </div>
                        </div>
                        @endforeach
                        <div class="form-group">
                            <label class="reject-label">Comentarios de cancelación</label>
                            <textarea class="form-control" name="reject_comments"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Coupon Order Modal -->
<div class="modal fade" id="modal-add-coupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Cupón</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.saveCoupon', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                    <div class="form-group">
                        <label class="control-label">Código de cupón</label>
                        <input type="text" class="form-control" name="coupon_code" id="coupon_code">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Shopper Payment Method -->
<div class="modal fade" id="modal-shopper-payment-method" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Método de Pago Shopper</h4>
      </div>
      <form method="post" action="{{ route('adminOrders.updateShopperPaymentMethod', ['id' => $order->id]) }}" class="form-modal">
          <div class="modal-body">
              <div class="unseen alert alert-danger form-has-errors"></div>
              <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
              <div class="form-group">
                <label class="control-label">Tienda</label>
                <select name="store_id" class="form-control required">
                    <option value="">-Selecciona-</option>
                    @foreach($order_stores as $order_store)
                    <option value="{{ $order_store->store_id }}">{{ $order_store->name }}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">Método de pago de shopper</label>
                <select class="form-control shopper-payment-method" name="shopper_payment_method">
                    <option value="" selected="selected">-Seleccione-</option>
                    <option value="Efectivo">Efectivo</option>
                    <option value="Tarjeta merqueo">Tarjeta merqueo</option>
                    <option value="Efectivo y tarjeta merqueo">Efectivo y tarjeta merqueo</option>
                    <option value="No aplica">No aplica</option>
                </select>
              </div>
              <div class="shopper-block-total-real-amount">
                  <div class="form-group">
                       <label class="control-label">Total factura</label>
                       <input type="text" class="form-control required" name="total_real_amount" id="total_real_amount">
                  </div>
              </div>
              <div class="shopper-block-cash-card unseen">
                <div class="form-group">
                    <label class="control-label">Total pagado en efectivo</label>
                    <input type="text" class="form-control required shopper-cash-paid" name="shopper_cash_paid" id="shopper_cash_paid">
                </div>
                <div class="form-group">
                    <label class="control-label">Total pagado con tarjeta</label>
                    <input type="text" class="form-control required shopper-card-paid" name="shopper_card_paid" id="shopper_card_paid">
                </div>
             </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
      </form>
    </div>
  </div>
</div>
<!-- Shopper Payment Method -->

<!-- Shopper Supermarket -->
<div class="modal fade" id="modal-shopper-supermarket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Supermercado Shopper</h4>
      </div>
      <form method="post" action="{{ route('adminOrders.updateShopperSupermarket', ['id' => $order->id]) }}" class="form-modal">
          <div class="modal-body">
              <div class="unseen alert alert-danger form-has-errors"></div>
              <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
              <div class="form-group">
                <label class="control-label">Tienda</label>
                <select name="store_id" class="form-control required">
                    <option value="">-Selecciona-</option>
                    @foreach($order_stores as $order_store)
                    <option value="{{ $order_store->store_id }}">{{ $order_store->name }}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">Supermercado</label>
                <select class="form-control shopper-supermarket" name="shopper_supermarket">
                    <option value="">-Selecciona-</option>
                    @foreach ($supermarkets['store'] as $supermarket)
                        <option value="{{ $supermarket->name }}">{{ $supermarket->name }}</option>
                    @endforeach
                    <option value="Otro">Otro</option>
                </select>
              </div>
              <div class="shopper-other-supermarket-block unseen">
                <div class="form-group">
                    <label class="control-label">¿Cual otro supermercado?</label>
                    <select name="shopper_supermarket_other" class="form-control required shopper-supermarket-other">
                        <option value="">-Selecciona-</option>
                        @foreach($supermarkets['others'] as $supermarket)
                        <option value="{{ $supermarket->name }}">{{ $supermarket->name }}</option>
                        @endforeach
                    </select>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
      </form>
    </div>
  </div>
</div>
<!-- Shopper Supermarket -->

<!-- Allocate Shopper Modal -->
<div class="modal fade" id="modal-allocate2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Asignar Shopper</h4>
      </div>
        <form method="post" action="{{ route('adminOrders.updateShopper', ['id' => $order->id]) }}">
      <div class="modal-body">
          <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
          <div class="form-group">
            <label class="control-label">Shopper</label>
            <select class="form-control" name="shopper-id" id="modal-shoppers">
                @foreach($shoppers_active as $shopper)
                    <option value="{{$shopper['id']}}">{{$shopper['first_name']}} {{$shopper['last_name']}}</option>
                @endforeach
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Allocate Shopper Modal -->
<div class="modal fade" id="modal-allocate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Asignar Shopper</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateShopper', ['id' => $order->id]) }}" id="allocate_form">
                <div class="modal-body">
                    <input type="hidden" class="order-id" name="id" value="{{ $order->id }}">
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="shopper-name">Shopper a asignar</label>
                            <input class="form-control" type="text" name="shopper-name" id="allocate-shopper-name" readonly="" required="">
                            <input class="form-control" type="hidden" name="shopper-id" id="allocate-shopper-id">
                        </div>
                        <div class="col-xs-3">
                            <label for="store-branch-name">Tienda a asignar</label>
                            <input class="form-control" type="text" name="store-branch-name" id="allocate-store-branch-name" readonly="" required="">
                            <input class="form-control" type="hidden" name="store-branch-id" id="allocate-store-branch-id">
                        </div>
                        <div class="col-xs-2">
                            <label for="store-branch-name">Zonas</label>
                            <select class="form-control" name="zone_id" id="zone-id" data-store="{{ $order->store_id }}">
                                <option value=""></option>
                                @foreach($zones as $zone)
                                <option value="{{$zone['id']}}" @if($zone['id'] == $order->zone_id) selected="" @endif>{{$zone['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-2">
                            <label for="supermarket-id">Supermercado</label>
                            <select name="supermarket-id" id="supermarket-id" class="form-control" data-store="{{ $order->store_id }}">
                                <option value=""></option>
                                @foreach ($supermarkets['store'] as $supermarket)
                                    <option value="{{ $supermarket->id }}">{{ $supermarket->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-2">
                            <label style="color: #FFF;">|</label>
                            <button type="submit" class="btn btn-primary form-control">Asignar pedido</button>
                        </div>
                    </div>
                    <div id="map-allocate" style="height: 350px"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modificar Pedido</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateProduct') }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" id="order-product-id" name="id" value="">
                    <div class="form-group">
                        <label class="control-label">Precio</label>
                        <input type="text" class="form-control" name="price" id="order-product-price">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Cantidad</label>
                        <input type="text" class="form-control" name="quantity" id="order-product-quantity">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Replace Modal -->
<div class="modal fade" id="replace-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Reemplazar Producto</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateProduct') }}" enctype="multipart/form-data" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" id="order-product-parent-id" name="id" value="">
                    <div class="form-group">
                        <label class="control-label">Nombre</label>
                        <input type="text" class="form-control" name="product_name" >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Precio</label>
                        <input type="text" class="form-control" name="price" >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Cantidad</label>
                        <input type="text" class="form-control" name="quantity" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Product Modal -->
<div class="modal fade" id="modal-add-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Producto</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateProduct') }}" enctype="multipart/form-data" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <input type="hidden" id="order_id" name="order_id" value="{{ $order->id }}">
                    <div class="form-group">
                        <label class="control-label">Nombre</label>
                        <input type="text" class="form-control" name="product_name" >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Precio</label>
                        <input type="text" class="form-control" name="price" >
                    </div>
                    <div class="form-group">
                        <label class="control-label">Cantidad</label>
                        <input type="text" class="form-control" name="quantity" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reject Order Modal -->
<div class="modal fade" id="modal-reject-order">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancelar Pedido</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateStatus', ['id' => $order->id, 'status' => 'Cancelled']) }}" enctype="multipart/form-data" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                        <label class="reject-label">Selecciona la razón por la cual cancelas este pedido</label>
                        <select class="form-control required" name="reject_reason">
                            <option value="">-Selecciona-</option>
                            @foreach($reasons as $reason)
                            <option value="{{$reason->reason}}">{{$reason->reason}}</option>
                            @endforeach
                        </select>
                    </div>
                    @foreach($order_stores as $order_store)
                    <div class="block-order-store">
                        <div class="form-group">
                            <label class="control-label">Tienda</label>
                            <select name="store_id" class="form-control required">
                                <option value="{{ $order_store->store_id }}">{{ $order_store->name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="reject-label ">Estado de devolución de los productos</label>
                            <select class="form-control required" name="product_return_{{ $order_store->store_id }}">
                                <option value="">-Selecciona-</option>
                                <option value="1">Con devolución</option>
                                <option value="0">Sin devolución</option>
                            </select>
                        </div>
                    </div>
                    @endforeach
                    <div class="form-group">
                        <label for="notification" class="reject-label">¿Deseas enviar notificación al cliente?</label>
                        <div class="checkbox">
                            <label for="notification">
                                <input type="checkbox" name="reject_notification" id="notification" class="form-control reject-notification" value="true"> Enviar notificación de cancelación.
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="reject-label">Comentarios de cancelación</label>
                        <textarea class="form-control" id="reject_comments" name="reject_comments"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-danger save-status reject-btn">Cancelar Pedido</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Accept Order Modal -->
<div class="modal fade" id="modal-accept-order">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title accept-title">Aceptar Pedido</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateStatus', ['id' => $order->id, 'status' => 'In Progress']) }}" enctype="multipart/form-data" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group accept-options">
                        <label class="accept-label">Selecciona el tiempo de entrega para éste pedido</label>
                        <select name="shopper_delivery_time_minutes" class="form-control required">
                            <option value="15">15 minutos</option>
                            <option value="30">30 minutos</option>
                            <option value="45">45 minutos</option>
                            <option value="60">1 hora</option>
                            <option value="75">1 hora 15 minutos</option>
                            <option value="90">1 hora 30 minutos</option>
                            <option value="105">1 hora 45 minutos</option>
                            <option value="150">2 horas</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success save-status accept-btn">Aceptar Pedido</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Track Shopper Modal -->
<div class="modal fade" id="modal-track">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Track Shopper</h4>
            </div>
            <div class="modal-body">
                <div id="map" class="col-xs-12" style="height: 300px;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- orders_log Image Order Modal -->
<div class="modal fade" id="modal-orders_log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 900px; width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Log de Pedido</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body table-responsive container-overflow">
                                <table id="shelves-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Cliente</th>
                                            <th>Shopper</th>
                                            <th>Usuario</th>
                                            <th>Log</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($order_log))
                                        @foreach($order_log as $log)
                                        <tr>
                                            <td>{{ $log->user_name }}</td>
                                            <td>{{ $log->shopper_name }}</td>
                                            <td>{{ $log->admin_name }}</td>
                                            <td>{{ $log->type }}</td>
                                            <td>{{ date_format($log->created_at, 'd/m/Y h:i:s A') }}</td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr><td colspan="5" align="center">No hay datos.</td></tr>
                                        @endif
                                    </tbody>
                                </table><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@foreach($order_stores as $order_store)
<!-- Invoice Image Order Modal -->
<div class="modal fade" id="modal-invoice-{{ $order_store->store_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Imagen de Factura {{ $order_store->name }}</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.saveUploadImage', ['id' => $order->id]) }}" enctype="multipart/form-data" class="form-modal">
                <input type="hidden" name="type" value="invoice">
                <input type="hidden" name="store_id" value="{{ $order_store->store_id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Imagen</label>
                        <input type="file" class="form-control" name="image" required="required">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Subir</button>
                    </div>
                    @if ($order_store->invoice_image_url)
                    <div class="form-group">
                        <label class="control-label">Imagen actual</label>
                        <img src="{{ $order_store->invoice_image_url }}" width="100%">
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

<!-- Voucher Image Order Modal -->
<div class="modal fade" id="modal-voucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Imagen de Voucher</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.saveUploadImage', ['id' => $order->id]) }}" enctype="multipart/form-data" class="form-modal">
                <input type="hidden" name="type" value="voucher">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Imagen</label>
                        <input type="file" class="form-control" name="image" required="required" >
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Subir</button>
                    </div>
                    @if ($order->voucher_image_url)
                    <div class="form-group">
                        <label class="control-label">Imagen actual</label>
                        <img src="{{ $order->voucher_image_url }}" width="100%">
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Customer Delivery Date Modal -->
<div class="modal fade" id="modal-delivery-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Fecha de Entrega</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateDeliveryDate', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                        <label class="control-label">Nueva fecha de entrega</label>
                        <select class="form-control required delivery_day" name="delivery_day" id="delivery_day">
                            <option value="">Cargando...</option>
                        </select>
                        <div id="loading unseen"></div>
                        <div class="delivery_time unseen"><br/>
                            <select class="form-control required" name="delivery_time" id="delivery_time">
                                <option value="">Cargando...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                <input type="hidden" id="store_id" value="{{ $order->store_id }}" />
            </form>
        </div>
    </div>
</div>

<!-- Customer Delivery Address Modal -->
<div class="modal fade" id="modal-delivery-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Dirección de Entrega</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateDeliveryAddress', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                        <label class="control-label">Ciudad</label>
                        <select name="city" id="city" class="form-control required">
                            @foreach ($cities as $city)
                            <option value="{{ $city->id }}" @if ( $order->city_id == $city->id )selected="selected"@endif>{{ $city->city }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Dirección de entrega</label>
                        <input type="text" class="form-control required" name="delivery_address" id="delivery_address" value="{{ $order->user_address }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Complemento de dirección</label>
                        <input type="text" class="form-control" name="delivery_address_further" id="delivery_address_further" placeholder="Ej: Apartamento 501, Torre 2" value="{{ $order->user_address_further }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Barrio</label>
                        <input type="text" class="form-control" name="delivery_address_neighborhood" id="delivery_address_neighborhood" value="{{ $order->user_address_neighborhood }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Payment Method Modal -->
<div class="modal fade" id="modal-payment-method" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Método de Pago</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updatePaymentMethod', ['id' => $order->id]) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                        <label class="control-label">Método de pago</label>
                        <select class="form-control required payment_method" name="payment_method">
                            <option value="Efectivo" @if ($order['payment_method'] == 'Efectivo') selected="selected" @endif>Efectivo</option>
                            <option value="Tarjeta de crédito" @if ($order['payment_method'] == 'Tarjeta de crédito') selected="selected" @endif>Tarjeta de crédito</option>
                            <option value="Datáfono" @if ($order['payment_method'] == 'Datáfono') selected="selected" @endif>Datáfono</option>
                            <option value="Efectivo y datáfono" @if ($order['payment_method'] == 'Efectivo y datáfono') selected="selected" @endif>Efectivo y datáfono</option>
                        </select>
                    </div>
                    <div class="credit_cards unseen"><br/>
                        <div class="form-group">
                            <label class="control-label">Tarjeta de crédito</label>
                            <select class="form-control required" name="credit_card_id">
                                @foreach($customer_credit_cards as $credit_card)
                                <option value="{{ $credit_card->id }}" @if (isset($post['credit_card_id']) && $post['credit_card_id'] == $credit_card->id) selected="selected" @endif>**** {{ $credit_card->last_four }} {{ $credit_card->type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Payment Method Modal -->
<div class="modal fade" id="modal-delivered" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Actualizar estado a Entregado</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateStatus', ['id' => $order->id, 'status' => 'Delivered']) }}" class="form-modal">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                        <label class="control-label">Método de pago del cliente</label>
                        <select class="form-control required payment_method_delivered" name="customer_payment_method">

                            <option value="Efectivo" @if (isset($post['payment_method']) && $post['payment_method'] == 'Efectivo') selected="selected" @endif>Efectivo</option>
                            <option value="Datáfono" @if (isset($post['payment_method']) && $post['payment_method'] == 'Datáfono') selected="selected" @endif>Datáfono</option>
                            <option value="Efectivo y datáfono" @if (isset($post['payment_method']) && $post['payment_method'] == 'Efectivo y datáfono') selected="selected" @endif>Efectivo y datáfono</option>
                        </select>
                    </div>
                    <div class="customer-block-cash-card unseen">
                        <div class="form-group">
                          <label class="control-label">Total pagado con efectivo</label>
                          <input type="text" class="form-control required customer-cash-paid" name="customer_cash_paid" id="customer_cash_paid" value="">
                        </div>
                        <div class="form-group">
                          <label class="control-label">Total pagado con datáfono</label>
                          <input type="text" class="form-control required customer-card-paid" name="customer_card_paid" id="customer_card_paid" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update user data to show invoice  <--start-->
<div class="modal fade" id="modal-update-user-invoice-data">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3>Actualizar Datos de Facturación</h3>
      </div>
      <div class="modal-body">
          <form id="invoice-data-form" method="post" action="{{ route('adminOrders.updateInvoice', ['id' => $order->id]) }}">
            <div class="form-group">
                <label for="user_identity_type">Tipo de Documento</label>
                <select id="user_identity_type" name="user_identity_type" class="form-control">
                      <option value="Cédula">Cédula</option>
                      <option value="NIT">NIT</option>
                </select>
            </div>
            <div class="form-group">
              <label for="user_identity_number">Número de Identificación</label>
              <input type="text" id="user_identity_number"  class="form-control" placeholder="Número de identificación" name="user_identity_number">
            </div>
            <div class="form-group">
                <label for="identity_number">Razón Social o Nombre Completo</label>
                <input type="text" id="user_business_name" class="form-control" placeholder="Razón Social o Nombre Completo" name="user_business_name">
            </div>
            <button type="submit" class="btn btn-success">Guardar</button>
            <br><br>
        </form>
        <br>
      </div>
    </div>
  </div>
</div>

<!-- Customer Delivery Address Modal -->
<div class="modal fade" id="modal-send-sms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Envío de mensaje de texto</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.sendSms', ['id' => $order->id]) }}" class="form-modal" id="send-sms">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                        <label class="control-label" for="sms">Mensaje</label>
                        <textarea class="form-control" name="sms" required=""></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" id="send-sms-btn">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Price custom product Modal -->
<div class="modal fade" id="modal-price-custom-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Precio para producto personalizado</h4>
            </div>
            <form method="post" action="{{ route('adminOrders.updateProductCustom') }}" class="form-modal" id="update-price-custom-product">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group">
                      <label for="user_identity_number">Precio</label>
                      <input type="text" id="price" class="form-control" placeholder="Precio" name="price">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" id="save-product-price-btn">Guardar</button>
                </div>
                <input type="hidden" id="product_id" class="form-control" name="product_id">
                <input type="hidden" id="status" class="form-control" name="status" value="Fullfilled">
                <input type="hidden" id="order_id" name="order_id" value="{{ $order->id }}" />
            </form>
        </div>
    </div>
</div>

<!-- Map with coords user Modal -->
<div class="modal fade" id="modal-map-coords-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Mapa del punto de entrega</h3>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&key={{ Config::get('app.google_api_key') }}"></script>
<script type="text/javascript">
    var shopper_obj = JSON.parse('@if( isset($current_shopper) ){{ json_encode($current_shopper) }}@endif');
    var active_shoppers_obj = JSON.parse('@if( isset($shoppers_map) ){{ json_encode($shoppers_map) }}@endif');
    var store_branches = JSON.parse('@if( isset($store_branches) ){{ json_encode($store_branches) }}@endif');
    var city_id = {{ $order->city_id }};
    var store_id = @if($order->store_id) {{$order->store_id}} @else 0 @endif;
    var bogota_lat = 4.686806;
    var bogota_lng = -74.074663;
    var medellin_lat = 6.251366;
    var medellin_lng = -75.567139;
    var order_address = '{{ $order->user_address_latitude }} {{ $order->user_address_longitude }}';
    var markersArray = [];
    var map_track = {map: {}, markers: []};
    var map_allocate = {map: {}, shoppers: [], stores: [], customer: [], infowindow: []};
    var draw_zones = JSON.parse('@if( isset($draw_zones) ){{ json_encode($draw_zones) }}@endif');

    function mapInit(lat, lng, element_container, clickHandler = false, map_obj) {
        var myOptions = { center: new google.maps.LatLng(lat, lng),zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };
        map_obj.map = new google.maps.Map(document.getElementById(element_container), myOptions);
        google.maps.Circle.prototype.contains = function(latLng) {
            return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
        }
    }

    function drawZones(map_obj) {
        var colors = ['#d28580', '#d0ab3c', '#c27bce', '#838eca', '#84ded6', '#79c17c', '#80b0c7', '#b39084', '#1e6951', '#59691e', '#69261e', '#681e69', '#00ccc4', '#0bcc00'];
        if ( draw_zones ) {
            for (var i = 0; i < draw_zones.length; i++) {
                var zone = draw_zones[i];
                var temp = new Array();
                for (var j = 0; j < zone.length; j++) {
                    var coords = zone[j].split(' ');
                    temp.push( {lat: parseFloat(coords[0]), lng: parseFloat(coords[1])} );
                }

                var bermudaTriangle = new google.maps.Polygon({
                    paths: temp,
                    strokeColor: colors[i],
                    strokeOpacity: 0.5,
                    strokeWeight: 2,
                    fillColor: colors[i],
                    fillOpacity: 0.25
                });
                bermudaTriangle.setMap(map_obj.map);
            }
        }
    }

    function setMarker(map_obj, lat, lng, type, title, num_orders = null) {
        if( type == 'Shopper Moto' ){
            if ( num_orders > 10 ) {
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper/marker_shopper_+10.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }else{
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper/marker_shopper_'+ num_orders +'.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: image,
                map: map_obj.map,
                title: title
            });
        }else if( type == 'Shopper Carro' ){
            if ( num_orders > 10 ) {
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper_notdisponible/marker_shopper_+10.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }else{
                var image = {
                    url: '{{ admin_asset_url() }}img/marker_shopper_notdisponible/marker_shopper_'+ num_orders +'.png',
                    size: new google.maps.Size(35, 47),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 47),
                    type: type
                };
            }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: image,
                map: map_obj.map,
                title: title
            });
        }else if( type == 'stores' ){
            var image = {
                url: '{{ admin_asset_url() }}img/store/store.png',
                size: new google.maps.Size(35, 47),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 47),
                type: type
            };
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: image,
                map: map_obj.map,
                title: title
            });
        }else{
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map_obj.map,
                title: title
            });
        }
        // map_obj.markers.push(marker);
        return marker;
    }

    function setInfoWindow_shoppers(map_obj, marker, shopper_obj)
    {
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>' +
            '<h4 id="firstHeading" class="firstHeading">'+ shopper_obj.first_name + ' ' + shopper_obj.last_name +'</h4>'+
            '<div id="bodyContent">' +
            '<strong>Teléfono:</strong> ' + shopper_obj.phone +
            '<br>' +
            '<strong>Tipo:</strong> ' + shopper_obj.profile +
            '<br>' +
            '<strong>Pedidos pendientes:</strong> ' + shopper_obj.num_orders +
            '<br>' +
            '<strong>Ultima conexión:</strong> ' + shopper_obj.updated_at_formatted +
            '<hr style="margin: 0.5rem">' +
            '<div class="text-center">'+
            '<button type="button" class="btn btn-success assign_shopper" data-id="' + shopper_obj.id + '" data-fullname="' + shopper_obj.first_name + ' ' + shopper_obj.last_name + '" style="padding: 0.2rem 1rem;">Asignar</button>'+
            '</div>'+
            '</div>'+
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            map_obj.infowindow.push(infowindow);
            google.maps.event.addListener(infowindow, 'domready', function() {
                $('.assign_shopper').click(function(event) {
                    $('#allocate-shopper-name').removeAttr('readonly');
                    $('#allocate-shopper-name').val($(this).data('fullname'));
                    $('#allocate-shopper-name').attr('readonly', true);
                    $('#allocate-shopper-id').val($(this).data('id'));
                    infowindow.close();
                    restartIconShoppersMarkers();
                    var icon_obj = marker.getIcon();
                    if ( icon_obj.type == 'Shopper Moto' ) {
                        icon_obj.url = icon_obj.url.replace('marker_shopper', 'marker_shopper_open');
                    }else if ( icon_obj.type == 'Shopper Carro' ) {
                        icon_obj.url = icon_obj.url.replace('marker_shopper_notdisponible', 'marker_shopper_open');
                    }
                    marker.setIcon(icon_obj);
                });
            });
            marker.addListener('click', function() {
                $.each(map_allocate.infowindow, function(index, val) {
                    val.close();
                });
                infowindow.open(map_obj.map, marker);
            });
    }

    function restartIconShoppersMarkers() {
        for (var i = map_allocate.shoppers.length - 1; i >= 0; i--) {
            var icon_url = map_allocate.shoppers[i].getIcon();
            if ( icon_url.type == 'Shopper Moto' ) {
                icon_url.url = icon_url.url.replace('marker_shopper_open', 'marker_shopper');
            }else if ( icon_url.type == 'Shopper Carro' ) {
                icon_url.url = icon_url.url.replace('marker_shopper_open', 'marker_shopper_notdisponible');
            }
            map_allocate.shoppers[i].setIcon(icon_url);
        }
        return;
    }

    function restartIconStoresMarkers() {
        for (var i = map_allocate.stores.length - 1; i >= 0; i--) {
            var icon_url = map_allocate.stores[i].getIcon();
            icon_url.url = icon_url.url.replace('store_open', 'store');
            map_allocate.stores[i].setIcon(icon_url);
        }
        return;
    }

    function setInfoWindow_stores(map_obj, marker, store_obj)
    {
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h4 id="firstHeading" class="firstHeading">'+ store_obj.store_name + ' - ' + store_obj.name +'</h4>'+
            '<div id="bodyContent" class="text-center">'+
            '   <button type="button" class="btn btn-success assign_store" data-id="'+ store_obj.id +'" data-name="'+ store_obj.name +'">Asignar</button>'+
            '</div>';
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            map_obj.infowindow.push(infowindow);
            google.maps.event.addListener(infowindow, 'domready', function () {
                $('.assign_store').click(function(event) {
                    $('#allocate-store-branch-name').removeAttr('readonly');
                    $('#allocate-store-branch-name').val($(this).data('name'));
                    $('#allocate-store-branch-name').attr('readonly', true);
                    $('#allocate-store-branch-id').val($(this).data('id'));
                    infowindow.close();
                    restartIconStoresMarkers();
                    var icon_obj = marker.getIcon();
                    icon_obj.url = icon_obj.url.replace('store', 'store_open');
                    marker.setIcon(icon_obj);
                });
            });
            marker.addListener('click', function() {
                $.each(map_allocate.infowindow, function(index, val) {
                    val.close();
                });
                infowindow.open(map_obj.map, marker);
            });
    }

    function show_modal(action)
    {
        console.log('1864: ', action);
        if (action == 'delivery-date' && $('#delivery_day option').length == 1)
        {
            var store_id = $('#store_id').val();
            $.ajax({
             url: "{{ route('adminOrders.getStoreDeliverySlotAjax') }}",
             data: { store_id: store_id, validate_slots: 1 },
             type: 'get',
             success:
                function(response) {
                    $('#delivery_day option').remove();
                    $('#delivery_day').append($('<option value="">Selecciona el día</option>'));
                    $('#delivery_time option').remove();
                    $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
                    $.each(response[store_id].days, function(key, value){
                        $('#delivery_day').append('<option value="' + key + '">' + value + '</option>');
                    });
                    delivery_times = response[store_id].time;
                    $('*[name="delivery_day"]').trigger('change');
                }
            });
        }

        $('#modal-' + action).modal('show');
    }

    $(function()
    {
        $('#modal-track').on('shown.bs.modal', function() {
            if (shopper_obj && shopper_obj.location) {
                var lat_lng = shopper_obj.location.split(' ');
                var lat_lng2 = order_address.split(' ');
                mapInit(lat_lng[0], lat_lng[1], 'map', false, map_track);
                setMarker(map_track, lat_lng[0], lat_lng[1], 'shopper', shopper_obj.first_name+' '+shopper_obj.last_name);
                setMarker(map_track, lat_lng2[0], lat_lng2[1], 'default', 'Sitio de entrega');
            }else{
                if ( city_id == 1 ) {
                    mapInit(bogota_lat, bogota_lng, 'map', false, map_track);
                }else{
                    mapInit(medellin_lat, medellin_lng, 'map', false, map_track);
                }
            }
        });

        $('#modal-allocate').on('shown.bs.modal', function () {
            var window_height = $(window).height();
            $('#map-allocate').height(window_height - 250);
            var lat_lng = order_address.split(' ');
            mapInit(lat_lng[0], lat_lng[1], 'map-allocate', false, map_allocate);
            drawZones(map_allocate);
            var customer_marker = setMarker(map_allocate, lat_lng[0], lat_lng[1], 'default', 'Lugar de entrega');
            map_allocate.customer.push(customer_marker);
            if ( store_branches ) {
                $.each(store_branches, function(index, val) {
                    var new_marker_store = setMarker(map_allocate, val.lat, val.lng, 'stores', val.name);
                    map_allocate.stores.push(new_marker_store);
                    setInfoWindow_stores(map_allocate, new_marker_store, val);
                });
            }
            if ( active_shoppers_obj ) {
                $.each(active_shoppers_obj, function(index, val) {
                    if ( val.location ) {
                        var lat_lng = val.location.split(' ');
                        var new_marker = setMarker(map_allocate, lat_lng[0], lat_lng[1], val.profile, val.first_name + ' ' + val.last_name, val.num_orders);
                        map_allocate.shoppers.push(new_marker);
                        setInfoWindow_shoppers(map_allocate, new_marker, val);
                    }
                });
            }
        });

        $('#modal-allocate').on('hidden.bs.modal', function () {
            $('#allocate-shopper-name').val('');
            $('#allocate-shopper-id').val('');
            $('#allocate-store-branch-name').val('');
            $('#allocate-store-branch-id').val('');
        });

        $('.modal-invoice').click(function(){
            show_modal('update-user-invoice-data');
        });

        $('#invoice-data-form').validate({
            rules: {
                user_identity_number: "required",
                user_business_name: "required"
            }
        });

        $('.view-invoice').on('click',function(){
            var url = $(this).data('href');
            window.open(url,'_blank');
        });
    });

    function edit(product_id,quantity,price){
        $('#order-product-id').val(product_id);
        $('#order-product-price').val(price);
        $('#order-product-quantity').val(quantity);
        $('#edit-modal').modal('show');
    }

    function replace(product_id){
        $('#order-product-parent-id').val(product_id);
        $('#replace-modal').modal('show');
    }

    $(function() {

    	$('#modal-new-status').change(function(){
    		if ($(this).val() == 'Cancelled'){
    			$('.reject-block').show();
    		}else{
    		    $('.reject-block').hide();
    		}
    	});
    	$('#modal-new.status').trigger('change');

    	$('.delivery_day').on('change', function() {
    	    $('#delivery_time option').remove();
            $('#delivery_time').append($('<option value="">Selecciona la hora</option>'));
            if ($(this).val() != '') {
                times = delivery_times[$(this).val()];
                $.each(times, function(value, data) {
                    $('#delivery_time').append($("<option></option>").attr('value', value).text(data.text));
                });
                $('.delivery_time').show();
            }else{
                $('.delivery_time').hide();
            }
		});
		$('.delivery_day').trigger('change');

		$('.payment_method').on('change', function() {
            if ($(this).val() != 'Tarjeta de crédito') {
                $('.credit_cards').hide();
            } else{
                $('.credit_cards').show();
            }
        });
        $('.payment_method').trigger('change');

        $('.payment_method_delivered').on('change', function() {
            if ($(this).val() != 'Efectivo y datáfono') {
                $('.customer-block-cash-card').hide();
                $('.customer-cash-paid').val('');
                $('.customer-card-paid').val('');
            } else{
                $('.customer-block-cash-card').show();
            }
        });
        $('.payment_method_delivered').trigger('change');

    	$('.form-modal').on('submit', function(e) {
    		var error = false
    		var form = this;

    		$('input[type!="hidden"], select', this).each(function() {
    			if ($(this).is(':visible') && ($(this).val().length == 0)) {
					$(this).parent().addClass('has-error');
					error = 'Completa los campos requeridos.';
				}else $(this).parent().removeClass('has-error');
			});

			if(error) {
				$('.form-has-errors', form).html(error).fadeIn();
				return false;
			}
			return true;
		});

        $('body').on('change', '.shopper-payment-method', function() {
            if ($(this).val() != 'No aplica'){
                if ($(this).val() == 'Efectivo y tarjeta merqueo'){
                    $(this).parent().parent().find('.shopper-block-total-real-amount').hide();
                    $(this).parent().parent().find('.shopper-block-total-real-amount').val('');
                    $(this).parent().parent().find('.shopper-block-cash-card').show();
                }else{
                    $(this).parent().parent().find('.shopper-block-cash-card').hide();
                    $(this).parent().parent().find('.shopper-cash-paid').val('');
                    $(this).parent().parent().find('.shopper-card-paid').val('');
                    $(this).parent().parent().find('.shopper-block-total-real-amount').show();
                }
            }else $(this).parent().parent().find('.shopper-block-cash-card').hide();
        });
        $('.shopper-payment-method').trigger('change');

		$('body').on('change', '.shopper-supermarket', function() {
            if ($(this).val() == 'Otro'){
                $(this).parent().parent().find('.shopper-other-supermarket-block').show();
            }else{
                $(this).parent().parent().find('.shopper-other-supermarket-block').hide();
                $(this).parent().parent().find('.shopper-supermarket-other').val('');
                $(this).parent().parent().find('.shopper-supermarket-other-admin-id').val('');
            }
        });
        $('.shopper-supermarket').trigger('change');

        $('body').on('change', '#zone-id', function(event) {
            event.preventDefault();
            var zone_id = $(this).val();
            var store_id = $(this).data('store');
            $.ajax({
                url: '{{ route('adminOrders.getShoppersByZonesAjax') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                        zone_id: zone_id,
                        store_id: store_id
                    }
            })
            .done(function(data) {
                $.each(map_allocate.shoppers, function(index, val){ val.setMap(null) });
                $.each(data, function(index, val) {
                    if ( val.location ) {
                        var lat_lng = val.location.split(' ');
                        var new_marker = setMarker(map_allocate, lat_lng[0], lat_lng[1], val.profile, val.first_name + ' ' + val.last_name, val.num_orders);
                        map_allocate.shoppers.push(new_marker);
                        setInfoWindow_shoppers(map_allocate, new_marker, val);
                    }
                });
            })
            .fail(function() {
                console.log("error al obtener los shoppers.");
            });
        });

        $('body').on('change', '#supermarket-id', function(event) {
            event.preventDefault();
            var supermarket_id = $(this).val();
            var store_id = $(this).data('store');
            $.ajax({
                url: '{{ route('adminOrders.getStoresBySupermarketAjax') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    supermarket_id: supermarket_id,
                    store_id: store_id
                },
            })
            .done(function(data) {
                $.each(map_allocate.stores, function(index, val){ val.setMap(null) });
                $.each(data.store_branches, function(index, val) {
                    var new_marker_store = setMarker(map_allocate, val.lat, val.lng, 'stores', val.name);
                    map_allocate.stores.push(new_marker_store);
                    setInfoWindow_stores(map_allocate, new_marker_store, val);
                });
            })
            .fail(function(data) {
                console.log("error");
            })
        });

        $('body').on('click', '#allocate_form .btn-primary', function(event) {
            event.preventDefault();
            $('#allocate-shopper-name').removeAttr('readonly');
            $('#allocate-store-branch-name').removeAttr('readonly');
            if($('#allocate_form').valid()){
                $('#allocate_form').submit();
            }
            $('#allocate-shopper-name').attr('readonly', true);
            $('#allocate-store-branch-name').attr('readonly', true);
        });

        $('#re-send-email').submit(function (argument) {
            if (confirm('¿Estas seguro que deseas enviar el correo nuevamente al cliente?')) {
                return true;
            }
            return false;
        });

        $('body').on('click', '#send-sms-action', function(event) {
            $('#modal-send-sms').modal('show');
        });

        $('#send-sms').validate();
        $('#send-sms').submit(function(event) {
            if ( confirm('¿Desea enviar el mensaje de texto al cliente?') ) {
                return true;
            }
            return false;
        });

        $('#update-price-custom-product').validate();
        $('body').on('click', '.update-product', function(event){
            event.preventDefault();
            var product_id = $(this).data('product');
            var status = $(this).data('status');
            if(status == 'Fullfilled' && $(this).data('type') == 'Custom'){
                $('#modal-price-custom-product').modal('show');
                $('#modal-price-custom-product #product_id').val(product_id);
            }else{
                $('.btn-group button').addClass('disabled');
                $.ajax({
                    url: '{{ route('adminOrders.updateProductAjax') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        id: product_id,
                        status: status
                    },
                })
                .done(function(data) {
                    if(data.success == 'true'){
                        if(data.status  == 'Fullfilled'){
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-orange');
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-red');
                            $('.badge').parents().find('#status-'+product_id+' .badge').addClass('bg-green');
                            $('.badge').parents().find('#status-'+product_id+' .badge').html('Disponible');
                        }else{
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-orange');
                            $('.badge').parents().find('#status-'+product_id+' .badge').removeClass('bg-green');
                            $('.badge').parents().find('#status-'+product_id+' .badge').addClass('bg-red');
                            $('.badge').parents().find('#status-'+product_id+' .badge').html('No disponible');
                        }

                        $('#total_amount').html(data.payment_data.total_amount);
                        $('#delivery_amount').html(data.payment_data.delivery_amount);
                        $('#discount_amount').html(data.payment_data.discount_amount);
                        $('#total').html(data.payment_data.total);
                    }

                   $('.btn-group button').removeClass('disabled');

                })
                .fail(function(data) {
                    $('.btn-group button').removeClass('disabled');
                    console.log("error");
                })
                .always(function(data) {
                    $('.btn-group button').removeClass('disabled');
                    console.log("complete");
                });
            }

        });
    });
</script>

<script>
var config = {
    apiKey: '{{ Config::get('app.firebase.key') }}',
    authDomain: '{{ Config::get('app.firebase.domain') }}',
    databaseURL: '{{ Config::get('app.firebase.url') }}',
    storageBucket: '{{ Config::get('app.firebase.storage_bucket') }}',
};
firebase.initializeApp(config);

/*var shopper_location = firebase.database().ref('/shoppers');
shopper_location.on('child_changed', function(data) {
  updateLocation(data.key, data.val().location);
});*/

function updateLocation(shopper_id, location)
{
    $.ajax({
        url: '{{ route('adminShoppers.updateLocation') }}',
        type: 'POST',
        data: { shopper_id: shopper_id, location: location },
    })
    .done(function(response) {
        if (response.status == 'ERROR')
            console.log("No fue posible actualizar localización del shopper.");
    });
}
</script>


@stop
