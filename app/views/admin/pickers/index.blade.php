@extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

        <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminPickers.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Alistador</button>
        </a>
    </span>

    </section>
    <section class="content">
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alert!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form id="search-form" class="pickers-table" action="{{route('adminPickers.index')}}">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <label>Ciudad:</label>
                                                </td>
                                                <td width="33%">
                                                    <select id="city_id" name="city_id" class="form-control">
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>&nbsp;&nbsp;</td>
                                                <td align="right">
                                                    <label>Buscar:</label>
                                                </td>
                                                <td>
                                                    <input type="text" name="s" placeholder="Nombre, cédula, celular" id="input-search" class="form-control">
                                                </td>
                                                <td>&nbsp;&nbsp;</td>
                                                <td>
                                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <div class="paging">
                                            <table id="pickers-table" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Ciudad</th>
                                                    <th>Nombre</th>
                                                    <th>Apellido</th>
                                                    <th>Celular</th>
                                                    <th>Cédula</th>
                                                    <th>Tipo de Alistador</th>
                                                    <th>Estado</th>
                                                    <th>Último acceso</th>
                                                    <th>Sessión</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if (count($pickers))
                                                    @foreach($pickers as $picker)
                                                        <tr>
                                                            <td>{{ $picker->city }}</td>
                                                            <td>{{ $picker->first_name }}</td>
                                                            <td>{{ $picker->last_name }}</td>
                                                            <td>{{ $picker->phone }}</td>
                                                            <td>{{ $picker->identity_number }}</td>
                                                            <td>{{ $picker->profile }}</td>
                                                            <td>
                                                                @if ($picker->status == 1)
                                                                    <span class="badge bg-green">Activo</span>
                                                                @else
                                                                    <span class="badge bg-red">Inactivo</span>
                                                                @endif
                                                            </td>
                                                            <td>{{ $picker->last_logged_in }}</td>
                                                            <td>
                                                                @if ($picker->session)
                                                                    <span class="badge bg-green">Activa</span>
                                                                @else
                                                                    <span class="badge bg-red">Inactiva</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                                        <span class="caret"></span>
                                                                        <span class="sr-only">Toggle Dropdown</span>
                                                                    </button>
                                                                    <ul class="dropdown-menu" role="menu">
                                                                        <li><a href="{{ route('adminPickers.edit', ['id' => $picker->id]) }}">Editar</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="{{ route('adminPickers.delete', ['id' => $picker->id]) }}"  onclick="return confirm('¿Estas seguro que desea eliminar al picker?')">Eliminar</a></li>
                                                                        @if ($picker->session == 1)
                                                                            <li class="divider"></li>
                                                                            <li><a href="{{ route('adminPickers.logout', ['id' => $picker->id]) }}">Cerrar Sessión Dispositivo</a></li>
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr><td colspan="10" align="center">Alistadores no encontrados</td></tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            {{ $pickers->links() }}
                                        </div>
                                        <div align="center" class="paging-loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>
@endsection