@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title }}</h3>
                    </div>
                    <form role="form" method="post" id='main-form' action="{{ route('adminPickers.save') }}" enctype="multipart/form-data">
                        <div class="box-body">
                            <input type="hidden" name="id" id="id" value="{{ $picker ? $picker->id : '' }}">
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Cédula</label>
                                    <input type="number" class="form-control" name="identity_number" placeholder="Ingresa la Cédula" value="{{ $picker ? $picker->identity_number : '' }}">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="Ingresa el Nombre" value="{{ $picker ? $picker->first_name : '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Apellido</label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Ingresa el Apellido" value="{{ $picker ? $picker->last_name : '' }}">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Celular</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Ingresa el Celular" value="{{ $picker ? $picker->phone : '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Ingresa Email" value="{{ $picker ? $picker->email : '' }}">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Ciudad</label>
                                    <select class="form-control" id="city_id" name="city_id">
                                        <option value="">-Selecciona-</option>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" @if ($picker->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Bodega</label>
                                    <select id="warehouse_id" name="warehouse_id" class="form-control">
                                        @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                        @foreach ($warehouses as $warehouse)
                                            <option value="{{ $warehouse->id }}" @if($picker->warehouse_id == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label for="profile">Tipo alistador</label>
                                    <select name="profile" id="profile" class="form-control required">
                                        <option value="">-Selecciona-</option>
                                        <option value="Alistador Seco" @if($picker->profile == 'Alistador Seco') selected="selected" @endif>Alistador Seco</option>
                                        <option value="Alistador Frío" @if($picker->profile == 'Alistador Frío') selected="selected" @endif>Alistador Frío</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label for="profile">Tipo rol</label>
                                    <select name="rol" id="rol" class="form-control required">
                                        <option value="">-Selecciona-</option>
                                        <option value="Extra" @if($picker->rol == 'Extra') selected="selected" @endif>Extra</option>
                                        <option value="De planta" @if($picker->rol == 'De planta') selected="selected" @endif>De planta</option>
                                        <option value="Lider" @if($picker->rol == 'Lider') selected="selected" @endif>Lider</option>
                                        <option value="Validador" @if($picker->rol == 'Validador') selected="selected" @endif>Validador</option>
                                        <option value="Administrativo" @if($picker->rol == 'Administrativo') selected="selected" @endif>Administrativo</option>
                                        <option value="Empacador" @if($picker->rol == 'Empacador') selected="selected" @endif>Empacador</option>
                                        <option value="Empacador extra" @if($picker->rol == 'Empacador extra') selected="selected" @endif>Empacador extra</option>
                                        <option value="Prueba" @if($picker->rol == 'Prueba') selected="selected" @endif>Prueba</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label for="type">Tipo contrato</label>
                                    <select name="type" id="type" class="form-control required">
                                        <option value="">-Selecciona-</option>
                                        <option value="Fijo" @if (isset($picker) && $picker->type === 'Fijo') selected="selected" @endif>Fijo</option>
                                        <option value="Temporal" @if (isset($picker) && $picker->type === 'Temporal') selected="selected" @endif>Temporal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Clave</label>
                                    <input type="password" class="form-control" name="password" placeholder="Ingresa la Clave" value="">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label for="status">Estado</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">-Selecciona-</option>
                                        <option value="1" @if (isset($picker) && $picker->status === 1) selected="selected" @endif>Activo</option>
                                        <option value="0" @if (isset($picker) && $picker->status === 0) selected="selected" @endif>Inactivo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#main-form').validate({
                rules: {
                    type: "required",
                    identity_number: {
                        required: true,
                        number: true,
                        minlength: 7
                    },
                    first_name: 'required',
                    last_name: 'required',
                    phone: 'required',
                    password: {
                        required: function (element) {
                            if ($('#id').val() !== ''){
                                return false;
                            }
                            else{
                                return true;
                            }
                        }
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    city_id: {
                        required: true
                    }
                }
            });
        });
    </script>

@stop