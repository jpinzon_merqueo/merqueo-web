@extends('admin.layout')

@section('content')

<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
</script>
<a href="#importing" class="fancybox unseen"></a>
<div id="importing" class="unseen"><p>Enviando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alert!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id='main-form' action="{{ route('adminConfig.sendPush') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                        	<div class="form-group col-xs-12">
                                <label>Sistema operativo</label>
                                <select class="form-control" name="os" id="os">
                                    <option value=""></option>
                                    <option value="iOS">iOS</option>
                                    <option value="Android">Android</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <label>Mensaje</label>
                                <input type="text" class="form-control" id="message" name="message" placeholder="Ingresa el mensaje a enviar en el Push Notification">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 unseen begin_number">
                                <label>Número inicial</label>
                                <input type="text" class="form-control" id="begin_number" name="begin_number" placeholder="Ingresa el número inicial para iOS si ocurrió un error anteriormente">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 deeplink">
                                <label for="deeplink">¿Deseas enviarlo con deeplink?</label>
                                <select class="form-control" name="deeplink" id="deeplink">
                                    <option value="1">Sí</option>
                                    <option value="0" selected="selected">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row users_without_orders">
                            <div class="form-group col-xs-12">
                                <label for="users_without_orders">Enviar mensaje a:</label>
                                <select class="form-control" name="users_without_orders" id="users_without_orders">
                                    <option value="0">Todos los usuarios</option>
                                    <option value="1">Usuarios que no han pedido</option>
                                </select>
                            </div>
                        </div>
                        <div class="is_deeplink unseen">
                            <div class="row">
                                <div class="form-group col-xs-4 deeplink_type">
                                    <label for="deeplink_type">El deeplink va dirigido a:</label>
                                    <select class="form-control" name="deeplink_type" id="deeplink_type">
                                        <option></option>
                                        <option value="tienda">Una tienda</option>
                                        <option value="categoria">Una categoria</option>
                                        <option value="subcategoria">Una subcategoria</option>
                                        <option value="producto">Un producto</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_cities unseen">
                                    <label for="deeplink_cities">Ciudad</label>
                                    <select class="form-control" name="deeplink_cities" id="deeplink_cities">
                                        <option></option>
                                        @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_store unseen">
                                    <label for="deeplink_store">Tienda</label>
                                    <select class="form-control" name="deeplink_store" id="deeplink_store">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-4 deeplink_departments unseen">
                                    <label for="deeplink_departments">Categoría</label>
                                    <select class="form-control" name="deeplink_departments" id="deeplink_departments">
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_shelves unseen">
                                    <label for="deeplink_shelves">Subcategoria</label>
                                    <select class="form-control" name="deeplink_shelves" id="deeplink_shelves">
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_products unseen">
                                    <label for="deeplink_products">Producto</label>
                                    <select class="form-control" name="deeplink_products" id="deeplink_products">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary" onclick="return confirm('¿Estas seguro que deseas enviar el mensaje?')">Enviar Mensaje Push Notification</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){

    $("#main-form").validate({
      rules: {
      	os: 'required',
        message: 'required',
        deeplink: 'required',
        deeplink_type: 'required',
        deeplink_cities: 'required',
        deeplink_store: 'required',
        deeplink_departments: 'required',
        deeplink_shelves: 'required',
      	deeplink_products: 'required'
      }
    });

    $('#main-form').submit(function () {
        if($(this).valid()) {
           $('.fancybox').trigger('click');
        }
    });

    $('#os').change(function(){
        if ($(this).val() == 'iOS'){
            if (!$('.begin_number').is(':visible'))
                $('.begin_number').show();
        }else{
            $('.begin_number').hide();
            $('#begin_number').val('');
        }
    });

    $('#deeplink').change(function () {
        if ($(this).val() == 1) {
            $('.users_without_orders').slideUp('fast').prop('selectedIndex', 0);
            $('.is_deeplink').slideDown('fast');
            $('.deeplink_type').slideDown('fast');
        }else{
            $('.is_deeplink').slideUp('fast');
            $('.deeplink_type, .deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_store, .deeplink_shelves, .deeplink_products').slideUp('fast');
            $('#deeplink_type, #deeplink_cities, #deeplink_store, #deeplink_departments, #deeplink_store, #deeplink_shelves, #deeplink_products').prop('selectedIndex', 0);
            $('.users_without_orders').slideDown('fast').prop('selectedIndex', 0);
        }
    });

    $('#deeplink_type').change(function () {
        var type = $(this).val();

        $('.deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
        $('#deeplink_cities, #deeplink_store, #deeplink_departments, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);

        if ( type != '' ) {
            $('.deeplink_cities').slideDown('fast');
        }else{
            $('.deeplink_cities').slideUp('fast');
        }
    });

    $('#deeplink_cities').change(function () {
        var city_id = $('#deeplink_cities').val();
        var type = $('#deeplink_type').val();

        $('.deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
        $('#deeplink_store, #deeplink_departments, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);

        // if (type == 'tienda') {
            $.ajax({ url: "{{ route('adminOrders.getStoresAjax') }}",
                data: {city_id: city_id},
                type: 'get',
                async: false,
                success:
                function(msg) {
                    $('#deeplink_store').empty();
                    $('#deeplink_store').append('<option value=""></option>');
                    $.each(msg,function(key,value){
                        $('#deeplink_store').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    $('.deeplink_store').slideDown('fast');
                }
            });
        // }
    });

    $('#deeplink_store').change(function () {
        var type = $('#deeplink_type').val();
        var store_id = $(this).val();

        $('.deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
        $('#deeplink_departments, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);

        if (type == 'categoria' || type == 'subcategoria' || type == 'producto') {
            $.ajax({ url: "{{ route('adminProducts.getDepartmentsAjax') }}",
                data: {store_id: store_id},
                type: 'get',
                async: false,
                success:
                function(msg) {
                    $('#deeplink_departments').empty();
                    $('#deeplink_departments').append('<option value=""></option>');
                    $.each(msg,function(key,value){
                        if (value.status == 1) {
                            $('#deeplink_departments').append('<option value="'+value.id+'">'+value.name+'</option>');
                        }
                    });
                    $('.deeplink_departments').slideDown('fast');
                }
            });
        }
    });

    $('#deeplink_departments').change(function () {
        var type = $('#deeplink_type').val();
        var department_id = $(this).val();

        $('.deeplink_products').slideUp('fast');
        $('#deeplink_products').prop('selectedIndex',0);

        if (type == 'subcategoria' || type == 'producto') {
            $.ajax({ url: "{{ route('adminProducts.getShelvesAjax') }}",
                data: {department_id: department_id},
                type: 'get',
                async: false,
                success:
                function(msg) {
                    $('#deeplink_shelves').empty();
                    $('#deeplink_shelves').append('<option value=""></option>');
                    $.each(msg,function(key,value){
                        if (value.status == 1) {
                            $('#deeplink_shelves').append('<option value="'+value.id+'">'+value.name+'</option>');
                        }
                    });
                    $('.deeplink_shelves').slideDown('fast');
                }
            });
        }
    });

    $('#deeplink_shelves').change(function () {
        var type = $('#deeplink_type').val();
        var shelf_id = $(this).val();
        $('.deeplink_products').slideUp('fast');
        $('#deeplink_products').prop('selectedIndex',0);

        if ( type == 'producto' ) {
            $.ajax({
                url: "{{ route('adminProducts.getProductsAjax') }}",
                type: 'GET',
                dataType: 'json',
                data: {shelf_id: shelf_id},
            })
            .done(function(data) {
                $('#deeplink_products').empty();
                $('#deeplink_products').append('<option value=""></option>');
                $.each(data,function(key,value){
                    if (value.status == 1) {
                        $('#deeplink_products').append('<option value="'+value.id+'">'+value.name+'</option>');
                    }
                });
                $('.deeplink_products').slideDown('fast');
            })
            .fail(function() {
                console.log("error");
            })

        }
    });
});
</script>

@stop