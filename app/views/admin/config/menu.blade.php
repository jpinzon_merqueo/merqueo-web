@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
     @if(Session::has('error') )
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif
     @if(Session::has('success') )
    <div class="alert alert-success">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Hecho!</b> {{ Session::get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-6">
        	<div class="box box-primary">
        		<div class="box-header">
	        		<h3 class="box-title">
	        			Available Menu Items
	        		</h3>
        		</div>
        		<div class="box-body">
        			@foreach ($menuItems as $item)
        			<div class="row form-group">
        				<div class="col-xs-12">
	        				<div class="btn-group col-xs-12">
	        					<button type="button" class="btn btn-default dropdown-toggle col-xs-12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-align: left !important;">
	        						<i class="fa {{ $item->icon_class }}"></i> {{ $item->title }} <span class="caret pull-right"></span>
	        					</button>
	        					<ul class="dropdown-menu" style="left: 90% !important;">
	        						<li class="text-center form-group">
	        							{{ Form::open(['route' => 'adminConfig.addMenuItem']) }}
	        							<input type="hidden" name="id" value="{{ $item->id }}">
	        							<input type="hidden" name="title" value="{{ $item->title }}">
                                        <input type="hidden" name="iconclass" value="{{ $item->icon_class }}">
	        							<input type="hidden" name="action" value="{{ $item->controller_action }}">
	        							<input class="btn btn-default" type="submit" value="Add to menu">
	        							{{ Form::close() }}
        							</li>
	        						<li class="text-center">
	        							{{ Form::open(['route' => 'adminConfig.deleteMenuItem']) }}
	        							<input type="hidden" name="id" value="{{ $item->id }}">
	        							<input type="hidden" name="title" value="{{ $item->title }}">
	        							<input class="btn btn-danger" type="submit" value="Delete">
	        							{{ Form::close() }}
        							</li>
	        					</ul>
	        				</div>
        				</div>
        			</div>
        			@endforeach
        		</div>
        	</div>
        	<div class="box-body"></div>
        </div>
        <div class="col-xs-6">
        	<div class="box box-primary">
        		<div class="box-header">
        			<h3 class="box-title">
        				Admin Menu
        			</h3>
        		</div>
        		<div class="box-body">
        			<div class="row form-group">
        				<div class="col-xs-12">
							<div class="dd" id="nestable">
								<ol class="dd-list">
								@if ( !empty($menu) )
									@foreach ($menu as $link)
									<li class="dd-item" data-id="{{ $link['id'] }}" data-title="{{ $link['title'] }}" data-iconclass="{{ $link['iconclass'] }}" data-action="{{ $link['action'] }}">
										<div class="dd-handle">
											<i class="fa @if ( isset($link['iconclass']) ){{ $link['iconclass'] }}@endif"></i> {{ $link['title'] }}
										</div>
										<div class="dd-remove">
											<i class="fa fa-trash-o"></i>
										</div>
										
										@if (!empty($link['children']))
											<ol class="dd-list">
											@foreach ($link['children'] as $sublink)
												<li class="dd-item" data-id="{{ $sublink['id'] }}" data-title="{{ $sublink['title'] }}" data-iconclass="{{ $sublink['iconclass'] }}" data-action="{{ $sublink['action'] }}">
													<div class="dd-handle">
														<i class="fa @if ( isset($sublink['iconclass']) ){{ $sublink['iconclass'] }}@endif"></i> {{ $sublink['title'] }}
													</div>
													<div class="dd-remove">
														<i class="fa fa-trash-o"></i>
													</div>
												</li>
											@endforeach
											</ol>
										@endif
									</li>
									@endforeach
								@endif
								</ol>
							</div>
        				</div>
        			</div>
        			<div class="row form-group">
        				<div class="col-xs-12 text-center">
        					{{ Form::open(['route' => 'adminConfig.saveMenu']) }}
        						<input type="hidden" id="nestable-output" name="nestable-output">
        						<input type="submit" class="btn btn-primary" value="Save">
        					{{ Form::close() }}
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</section>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var updateOutput = function(e)
		{
			var list   = e.length ? e : $(e.target),
			output = list.data('output');
			if (window.JSON) {
	            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
	        } else {
	        	output.val('JSON browser support required for this demo.');
	        }
    	};
	    // activate Nestable for list 1
	    $('#nestable').nestable({
	    	group: 1,
	    	maxDepth: 2
	    })
	    .on('change', updateOutput);

	    updateOutput($('#nestable').data('output', $('#nestable-output')));

	    $('body').on('click', '.dd-remove', function(event) {
	    	event.preventDefault();
	    	$(this).parent().remove();
	    	updateOutput($('#nestable').data('output', $('#nestable-output')));
	    });
	});
</script>
@endsection