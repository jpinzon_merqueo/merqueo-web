@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Hecho!</b> {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alert!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id='main-form' action="{{ route('adminConfig.adjust') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body">
                        @if ( !empty($url) )
                        <div class="row">
                            <div class="col-xs-12">
                                <h3>Url Generada</h3>
                                <div class="form-group">
                                    {{-- <h4>{{ $url }}</h4> --}}
                                    <div class="row form-group">
                                        <div class="col-xs-4">
                                            <input type="text" name="generated_url" value="{{ $url }}" class="form-control" id="generated_url" class="form-control" readonly="">
                                        </div>
                                        <div class="col-xs-3">
                                            <button type="button" class="btn btn-primary copy-action" style="display: inline-block"><i class="fa fa-clipboard"></i> Copiar url</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input type="text" name="generated_url_encoded" value="{{ $url_encoded }}" class="form-control" id="generated_url_encoded" class="form-control" readonly="">
                                        </div>
                                        <div class="col-xs-3">
                                            <button type="button" class="btn btn-primary copy-action" style="display: inline-block"><i class="fa fa-clipboard"></i> Copiar url</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endif
                        <div class="row">
                            <div class="form-group col-xs-4">
                                <label for="link_tracker">Url tracker de Adjust</label>
                                <input type="text" name="link_tracker" class="form-control">
                            </div>
                        </div>
                        <div class="is_deeplink">
                            <div class="row">
                                <div class="form-group col-xs-4 deeplink_type">
                                    <label for="deeplink_type">Tipo de deeplink</label>
                                    <select class="form-control" name="deeplink_type" id="deeplink_type">
                                        <option>Selecciona</option>
                                        <option value="store">Tienda</option>
                                        <option value="department">Departamento</option>
                                        <option value="shelf">Pasillos</option>
                                        <option value="product">Producto</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_cities unseen">
                                    <label for="deeplink_cities">Ciudad</label>
                                    <select class="form-control" name="city_id" id="deeplink_cities">
                                        <option>Selecciona</option>
                                        @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_store unseen">
                                    <label for="deeplink_store">Tienda</label>
                                    <select class="form-control" name="store_id" id="deeplink_store">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-4 deeplink_departments unseen">
                                    <label for="deeplink_departments">Departamento</label>
                                    <select class="form-control" name="department_id" id="deeplink_departments">
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_shelves unseen">
                                    <label for="deeplink_shelves">Pasillo</label>
                                    <select class="form-control" name="shelf_id" id="deeplink_shelves">
                                    </select>
                                </div>
                                <div class="form-group col-xs-4 deeplink_products unseen">
                                    <label for="deeplink_products">Producto</label>
                                    <select class="form-control" name="product_id" id="deeplink_products">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary">Generar url</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
function copy_to_clipboard() {
    console.log($(this));
    var $temp = $("<input>");
    var element = '#generated_url';
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
    alert('Url copiada!');
}
$(document).ready(function(){


    $("#main-form").validate({
        rules: {
            os: 'required',
            message: 'required',
            deeplink: 'required',
            deeplink_type: 'required',
            city_id: 'required',
            store_id: 'required',
            department_id: 'required',
            shelf_id: 'required',
            product_id: 'required',
            link_tracker: 'required'
        }
    });

    $('#os').change(function(){
        if ($(this).val() == 'iOS'){
            if (!$('.begin_number').is(':visible'))
                $('.begin_number').show();
        }else{
            $('.begin_number').hide();
            $('#begin_number').val('');
        }
    });

    $('#deeplink').change(function () {
        var is_deeplink = $(this).val();
        if ( is_deeplink == 1 ) {
            $('.is_deeplink').slideDown('fast');
            $('.deeplink_type').slideDown('fast');
        }else{
            $('.is_deeplink').slideUp('fast');
            $('.deeplink_type, .deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_store, .deeplink_shelves, .deeplink_products').slideUp('fast');
            $('#deeplink_type, #deeplink_cities, #deeplink_store, #deeplink_departments, #deeplink_store, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);
        }
    });

    $('#deeplink_type').change(function () {
        var type = $(this).val();

        $('.deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
        $('#deeplink_cities, #deeplink_store, #deeplink_departments, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);

        if ( type != '' ) {
            $('.deeplink_cities').slideDown('fast');
        }else{
            $('.deeplink_cities').slideUp('fast');
        }
    });

    $('#deeplink_cities').change(function () {
        var city_id = $('#deeplink_cities').val();
        var type = $('#deeplink_type').val();

        $('.deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
        $('#deeplink_store, #deeplink_departments, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);

        // if (type == 'tienda') {
            $.ajax({ url: "{{ route('adminConfig.getStoresAjax') }}",
                data: {city_id: city_id},
                type: 'get',
                async: false,
                success:
                function(msg) {
                    $('#deeplink_store').empty();
                    $('#deeplink_store').append('<option value="">Selecciona</option>');
                    $.each(msg,function(key,value){
                        $('#deeplink_store').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    $('.deeplink_store').slideDown('fast');
                }
            });
        // }
    });

    $('#deeplink_store').change(function () {
        var type = $('#deeplink_type').val();
        var store_id = $(this).val();

        $('.deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
        $('#deeplink_departments, #deeplink_shelves, #deeplink_products').prop('selectedIndex',0);

        if (type == 'department' || type == 'shelf' || type == 'product') {
            $.ajax({ url: "{{ route('adminProducts.getDepartmentsAjax') }}",
                data: {store_id: store_id},
                type: 'get',
                async: false,
                success:
                function(msg) {
                    $('#deeplink_departments').empty();
                    $('#deeplink_departments').append('<option value="">Selecciona</option>');
                    $.each(msg,function(key,value){
                        if (value.status == 1) {
                            $('#deeplink_departments').append('<option value="'+value.id+'">'+value.name+'</option>');
                        }
                    });
                    $('.deeplink_departments').slideDown('fast');
                }
            });
        }
    });

    $('#deeplink_departments').change(function () {
        console.debug('var1');
        var type = $('#deeplink_type').val();
        var department_id = $(this).val();

        $('.deeplink_products').slideUp('fast');
        $('#deeplink_products').prop('selectedIndex',0);

        if (type == 'shelf' || type == 'product') {
            $.ajax({ url: "{{ route('adminProducts.getShelvesAjax') }}",
                data: {department_id: department_id},
                type: 'get',
                async: false,
                success:
                function(msg) {
                    $('#deeplink_shelves').empty();
                    $('#deeplink_shelves').append('<option value="">Selecciona</option>');
                    $.each(msg,function(key,value){
                        if (value.status == 1) {
                            $('#deeplink_shelves').append('<option value="'+value.id+'">'+value.name+'</option>');
                        }
                    });
                    $('.deeplink_shelves').slideDown('fast');
                }
            });
        }
    });

    $('#deeplink_shelves').change(function () {
        console.debug('var1');
        var type = $('#deeplink_type').val();
        var shelf_id = $(this).val();
        $('.deeplink_products').slideUp('fast');
        $('#deeplink_products').prop('selectedIndex',0);

        if ( type == 'product' ) {
            $.ajax({
                url: "{{ route('adminProducts.getProductsAjax') }}",
                type: 'GET',
                dataType: 'json',
                data: {shelf_id: shelf_id},
            })
            .done(function(data) {
                $('#deeplink_products').empty();
                $('#deeplink_products').append('<option value="">Selecciona</option>');
                $.each(data,function(key,value){
                    if (value.status == 1) {
                        $('#deeplink_products').append('<option value="'+value.id+'">'+value.name+'</option>');
                    }
                });
                $('.deeplink_products').slideDown('fast');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

        }
    });

    $('body').on('click', '.copy-action', function(event) {
        event.preventDefault();
        var $temp = $("<input>");
        var url = $(this).parents('.row').eq(0).find('.form-control:eq(0)').val();
        $("body").append($temp);
        $temp.val(url).select();
        document.execCommand("copy");
        $temp.remove();
        alert('Url copiada!');
    });
});
</script>

@stop