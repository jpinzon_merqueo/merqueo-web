@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

	@if(Session::has('success'))
	<div class="alert alert-success alert-dismissable">
	    <i class="fa fa-check"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Hecho!</b> {{ Session::get('success') }}
	</div>
	@endif
	@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
	    <i class="fa fa-ban"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <b>Alert!</b> {{ Session::get('error') }}
	</div>
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id='main-form' action="{{ route('adminConfig.sendSms') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group col-xs-12">
                            <label>Message</label>
                            <input type="text" class="form-control" id="message" name="message" placeholder="Enter Message">
                        </div>
                        <div class="form-group col-xs-12 unseen begin_number">
                            <label>Test Number</label>
                            <input type="text" class="form-control" id="test_number" name="test_number" placeholder="Enter Test Number">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to send this message?')">Send SMS Notification Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){

    $("#main-form").validate({
      rules: {
      	os: 'required',
      	message: 'required'
      }
    });

    $('#os').change(function(){
        if ($(this).val() == 'iOS'){
            if (!$('.begin_number').is(':visible'))
                $('.begin_number').show();
        }else{
            $('.begin_number').hide();
            $('#begin_number').val('');
        }
    });

});
</script>

@stop