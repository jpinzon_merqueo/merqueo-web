@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Eliminar archivos temporales de descarga</legend>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <button class="btn btn-primary" type="button" id="delete_tmp_files">Eliminar archivos</button>&nbsp;&nbsp;&nbsp;<img src="{{ asset_url() }}/img/loading.gif" class="unseen loading" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Opciones de configuración</legend>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            @if(count($options))
                                <div class="col-xs-4">
                                    <span>{{$options[0]['label']}}</span>
                                </div>
                                <div class="col-xs-3">
                                    <div class="box box-primary">
                                        <div class="box-body table-responsive">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <table width="100%">
                                                        <tr>
                                                            <td><label>Ciudad</label></td>
                                                            <td><label>Bodega</label></td>
                                                            <td><label>Valor</label></td>
                                                        </tr>
                                                        @foreach($options as $option)
                                                            <tr>
                                                                <td>{{$option->city}}</td>
                                                                <td>{{$option->warehouse}}</td>
                                                                <td>
                                                                    <input type="hidden" value="{{$option->warehouse_id}}" id="warehouse_id_{{$option->warehouse_id}}">
                                                                    <select class="form-control config-type" id="{{$option->warehouse_id}}" name="{{$option->type}}">
                                                                        <option value="0" {{ (0 === $option->value) ? "selected" : ''}}>No</option>
                                                                        <option value="1" {{ (1 === $option->value) ? "selected" : ''}}>Si</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <img src="{{ asset_url() }}/img/loading.gif" class="unseen loading-{{$option->type}}">
                                </div>
                            @else
                                <div class="col-xs-12">
                                    <h4>No se encuentran opciones de configuración</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <fieldset>
                                    <legend>Compensación automática por llegada tarde</legend>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <form class="">
                                    <div class="form-group">
                                        <label for="distribute_credit_late">Compensación automática</label>
                                        <select name="distribute_credit_late"  id="distribute_credit_late" class="form-control">
                                            <option value="1" {{ (1 == get_config('distribute_credit_late')) ? "selected" : '' }}>Activada</option>
                                            <option value="0" {{ (0 == get_config('distribute_credit_late')) ? "selected" : '' }}>Desactivada</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="max_time_credit_late">Tiempo de asignación del crédito después de terminada la franja(Minutos)</label>
                                        <input type="number" class="form-control" id="max_time_credit_late" value="{{  get_config('max_time_credit_late') }}" name="max_time_credit_late">
                                    </div>
                                    <div class="form-group">
                                        <label for="amount_credit_late">Valor del crédito</label>
                                        <input type="number" class="form-control" id="amount_credit_late" value="{{  get_config('amount_credit_late') }}" name="amount_credit_late">
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" id="btnSaveCompensation" type="button">Guardar</button>
                                        <img src="{{ asset_url() }}/img/loading.gif" class="unseen loading-comp">

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btnSaveCompensation").on('click', function (event) {
                event.preventDefault();
                if(confirm("¿Esta seguro que desea actulizar los datos de la compesacion automatica?") === true){
                    $('.loading-comp').removeClass('unseen');
                    $("#btnSaveCompensation").addClass('disabled');
                    $.ajax({
                        data: {distribute_credit_late: $("#distribute_credit_late option:selected").val(), max_time_credit_late: $("#max_time_credit_late").val(), amount_credit_late: $("#amount_credit_late").val()},
                        url: "{{ route('adminConfig.changeCompensation') }}",
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(response) {
                            if (response.status) {
                                $('#btnSaveCompensation').removeClass('disabled');
                                $('.loading-comp').addClass('unseen');
                                alert('¡Informacion actualizada!');
                            }
                            else {
                                alert('Error al actualizar la informacion.');
                                $('#btnSaveCompensation').removeClass('disabled');
                                $('.loading-comp').removeClass('unseen');
                            }
                        }, error: function() {
                            $('.loading-comp').removeClass('unseen');
                            $('#btnSaveCompensation').removeClass('disabled');
                        }
                    });
                }
            });


            $('#delete_tmp_files').on('click', function(event) {
                if (confirm("¿Está seguro que desea eliminar los archivos temporales del servidor?") === true) {
                    $('.loading').removeClass('unseen');
                    $('#delete_tmp_files').addClass('disabled');
                    $.ajax({
                        url: "{{ route('adminConfig.deleteTmpFilesAjax') }}",
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(response) {
                            if (response.status) {
                                $('#delete_tmp_files').removeClass('disabled');
                                $('.loading').addClass('unseen');
                                alert('¡Archivos borrados exitosamente!');
                            }
                            else {
                                alert('No se encontraron archivos.');
                                $('#delete_tmp_files').removeClass('disabled');
                                $('.loading').removeClass('unseen');
                            }
                        }, error: function() {
                            $('.loading').removeClass('unseen');
                            $('#delete_tmp_files').removeClass('disabled');
                        }
                    });
                }
                event.preventDefault();
            });

            $('.config-type').on('change', function(event) {
                var id = $(this).attr('id');
                $('.loading-{{$option->type}}').removeClass('unseen');
                var data = {type: $('#'+id).attr('name'), value: $('#'+id+' option:selected').val(), warehouse_id: $('#warehouse_id_'+id).val()};
                $.ajax({
                    url: "{{ route('adminConfig.changeStatusGeneralConfigAjax') }}",
                    type: 'POST',
                    data: data,
                    dataType: 'JSON',
                    success: function(response) {
                        if (response.status) {
                            alert('Cambio de estado exitoso.');
                            $('.loading-{{$option->type}}').addClass('unseen');
                        }
                        else {
                            alert('Ocurrio un problema al cambiar el estado.');
                            $('.loading-{{$option->type}}').addClass('unseen');
                        }
                    }, error: function() {
                        alert('Ocurrio un problema al cambiar el estado.');
                        $('.loading-{{$option->type}}').addClass('unseen');
                    }
                });
                event.preventDefault();
            });
        });
    </script>
@endsection