@extends('admin.layout')
	@section('content')
	<link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/js/plugins/codemirror/lib/codemirror.css">
	<link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/js/plugins/codemirror/theme/dracula.css">
	<script src="{{ web_url() }}/admin_asset/js/plugins/codemirror/lib/codemirror.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/codemirror/addon/mode/simple.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/codemirror/addon/edit/matchbrackets.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/codemirror/addon/comment/continuecomment.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/codemirror/addon/comment/comment.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/codemirror/mode/javascript/javascript.js" type="text/javascript"></script>
	<section class="content-header">
	    <h1>
	        {{ $title }}
	        <small>Control panel</small>
	    </h1>
	    <span class="breadcrumb" style="top:0px">
	        <a type="button" class="btn btn-primary btn-sync-elastic" style="color:#fff;" href="{{ route('adminConfig.elasticSearchConfigSync') }}"><i class="fa fa-sync"></i>Actualizar todos los productos al indice de Elasticsearch</a>
		</span>
	</section>
	<section class="content">

		@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
		    <i class="fa fa-check"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <b>Hecho!</b> {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
		    <i class="fa fa-ban"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <b>Alert!</b> {{ Session::get('error') }}
		</div>
		@endif

	    <div class="row">
	        <div class="col-xs-12 col-md-12">
	        	
	        	<div class="box box-primary">
	    			<div class="box-body" id="form_search_start_supplier">
	    				<div class="row error-search">
				            <div class="col-xs-12">
				                <div class="alert alert-danger unseen"></div>
				                <div class="alert alert-success unseen"></div>
				            </div>
				        </div>
				        <form id="elasticseach-config-form" method="post" action="{{ route('adminConfig.elasticSearchConfigSave') }}" autocomplete="off" >
	    					<div class="row">
	    						<div class="col-md-6">
			    					<div class="form-group">
			    						<label>JSON Crear Indice</label>
	        							<textarea id="create_index" name="create_index">{{$create_index}}</textarea>
			    					</div>
	    						</div>
	    						<div class="col-md-6">
			    					<div class="form-group">
			    						<label>JSON Mapear Indice</label>
	        							<textarea id="mapping_index" name="mapping_index">{{$mapping_index}}</textarea>
			    					</div>
	    						</div>
	    						<!-- <div class="col-md-6">
			    					<div class="form-group">
			    						<label></label>
	        							<textarea id="mapping_fields" name="mapping_fields"></textarea>
			    					</div>
	    						</div> -->
	    					</div>
	    				</form>
				    </div>
		    		<div class="box-footer clearfix">
				    	<button class="btn btn-primary pull-right" id="save-config">Guardar configuración</button>
		    		</div>
	    		</div>
	        </div>
	    </div>
	</section>
	<script type="text/javascript">
		CodeMirror.commands.save = function(cm, ob){ 
			console.log(cm)
			console.log(ob)
		};


		var create_index = CodeMirror.fromTextArea(document.getElementById("create_index"),{
			lineNumbers: true,
			styleActiveLine: true,
			matchBrackets: true,
	        autoCloseBrackets: true,
	        mode: "application/ld+json",
	        lineWrapping: true,
	        'theme' : 'dracula',
	        cantEdit : true,
	        beforeChange : function( cm, ob){
	        	console.log(cm);
	        	console.log(ob)
	        },
	        change : function( cm, ob ){
	        	console.log(cm.getValue());
	        	console.log(ob)
	        }
		});
		var mapping_index = CodeMirror.fromTextArea(document.getElementById("mapping_index"),{
			lineNumbers: true,
			styleActiveLine: true,
			matchBrackets: true,
	        autoCloseBrackets: true,
	        mode: "application/ld+json",
	        lineWrapping: true,
	        'theme' : 'dracula',
	        cantEdit : true
		});
		$(document).ready(function(){
			$('body').on('click', '#save-config', function(){
				//console.log(create_index.getValue());
				//console.log(mapping_index.getValue());
				$('#create_index').val(create_index.getValue());
				$('#mapping_index').val(mapping_index.getValue());
				//console.log($('#create_index').val());
				//console.log($('#mapping_index').val());
				$('#elasticseach-config-form').submit();
			});
			$('body').on('click', '.btn-sync-elastic', function(){
				$('.btn-sync-elastic').addClass('disabled');
				$('.btn-sync-elastic').append('  <i class="fa fa-refresh fa-spin"></i>');
			});
		});

	</script>
	@endsection