<form role="form" method="POST" id='main-form' action="{{ route('adminProducts.save') }}" enctype="multipart/form-data">
	<input type="hidden" name="id" value="{{ $product ? $product->id : '' }}">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="box-title">
						Información básica
					</h4>
				</div>
				<div class="box-body">
					<div class="form-group row">
						<div class="col-xs-6">
							<label title="Nombre del producto que le será mostrado al cliente." for="name">Nombre</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="Ingresa el nombre" value="{{ $product ? $product->name : Input::old('name') }}">
						</div>
						<div class="col-xs-6">
							<label title="Formato URL del producto" for="slug">Slug</label>
							<input type="text" class="form-control" name="slug" placeholder="Ingresa el slug" value="{{ $product ? $product->slug : Input::old('slug') }}">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Referencia del producto" for="reference">Referencia (EAN)</label>
							<input type="text" class="form-control" name="reference" id="reference" placeholder="Ingresa la referencia" value="{{ $product ? $product->reference : Input::old('reference') }}">
						</div>
						<div class="col-xs-3">
							<label title="Categoría del producto" for="category">
								Categoria <a href="javascript:;" class="label label-success category">Agregar</a> <a href="javascript:;" class="label label-danger category unseen">Cancelar</a>
							</label>
							<div id="category-container">
								<div class="option-1">
									<select class="form-control" id="category" name="category_id" required="">
										<option value=""></option>
										@foreach ( $categories as $category)
											<option value='{{ $category->id }}' {{ $product && ($category->id == $product->category_id) || $category->id == Input::old('category_id') ? 'selected="selected"' : ''}}>{{ $category->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="option-2 row unseen">
									<div class="col-xs-8">
										<input type="text" name="new-category" id="new-category" class="form-control">
									</div>
									<div class="col-xs-4 text-right">
										<button type="button" class="btn btn-default">Guardar</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-3">
							<label title="Subcategoría del producto" for="subcategory">
								Subcategoría <a href="javascript:;" class="label label-success subcategory">Agregar</a> <a href="javascript:;" class="label label-danger subcategory unseen">Cancelar</a>
							</label>
							<div id="subcategory-container">
								<div class="option-1">
									<select class="form-control" id="subcategory" name="subcategory_id">
										<option value=""></option>
										@if ( $product && !is_null($product->subcategory_id) )
											@foreach ($subcategories as $subcategory)
												<option value="{{ $subcategory->id }}" {{ $subcategory->id == $product->subcategory_id || $subcategory->id == Input::old('subcategory_id') ? 'selected="selected"' : '' }} >{{ $subcategory->name }}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="option-2 row unseen">
									<div class="col-xs-8">
										<input type="text" name="new-subcategory" id="new-subcategory" class="form-control">
									</div>
									<div class="col-xs-4 text-right">
										<button type="button" class="btn btn-default">Guardar</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-3">
							<label title="Marca del producto" for="brand">
								Marca <a href="javascript:;" class="label label-success brand">Agregar</a> <a href="javascript:;" class="label label-danger brand unseen">Cancelar</a>
							</label>
							<div id="brand-container">
								<div class="option-1">
									<select class="form-control" id="brand" name="brand_id" required="">
										<option value=""></option>
										@foreach ( $brands as $brand)
											<option value='{{ $brand->id }}' {{ $product && ($brand->id == $product->brand_id) || $brand->id == Input::old('brand_id') ? 'selected="selected"' : ''}}>{{ $brand->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="option-2 row unseen">
									<div class="col-xs-8">
										<input type="text" name="new-brand" id="new-brand" class="form-control">
									</div>
									<div class="col-xs-4 text-right">
										<button type="button" class="btn btn-default">Guardar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Submarca del producto" for="subbrand">
								Submarca <a href="javascript:;" class="label label-success subbrand">Agregar</a> <a href="javascript:;" class="label label-danger subbrand unseen">Cancelar</a>
							</label>
							<div id="subbrand-container">
								<div class="option-1">
									<select class="form-control" id="subbrand" name="subbrand_id">
										<option value=""></option>
										@if ( $product && !is_null($product->subbrand_id) )
											@foreach ($subbrands as $subbrand)
												<option value="{{ $subbrand->id }}" {{ $subbrand->id == $product->subbrand_id || $subbrand->id == Input::old('subbrand_id') ? 'selected="selected"' : '' }} >{{ $subbrand->name }}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="option-2 row unseen">
									<div class="col-xs-8">
										<input type="text" name="new-subbrand" id="new-subbrand" class="form-control">
									</div>
									<div class="col-xs-4 text-right">
										<button type="button" class="btn btn-default">Guardar</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-3">
							<label title="Fabricante del producto" for="maker_id">
								Fabricante <a href="javascript:;" class="label label-success maker">Agregar</a> <a href="javascript:;" class="label label-danger maker unseen">Cancelar</a>
							</label>
							<div id="maker-container">
								<div class="option-1">
									<select class="form-control" id="maker" name="maker_id">
										<option value=""></option>
										@foreach ($makers as $maker)
											<option value="{{ $maker->id }}" {{ $product && $maker->id == $product->maker_id || $maker->id == Input::old('maker_id') ? 'selected="selected"' : '' }} >{{ $maker->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="option-2 row unseen">
									<div class="col-xs-8">
										<input type="text" name="new-maker" id="new-maker" class="form-control">
									</div>
									<div class="col-xs-4 text-right">
										<button type="button" class="btn btn-default">Guardar</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-3">
							<label title="Puede tener código de barras." for="">
								¿Tiene código de barras?
							</label>
							<select class="form-control" name="has_barcode" id="has_barcode">
								<option value="1" @if( $product && $product->has_barcode == 1 ) selected="selected" @endif>Sí</option>
								<option value="0" @if( $product && $product->has_barcode == 0 ) selected="selected" @endif>No</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label title="Es perecedero." for="">
								¿Es perecedero?
							</label>
							<select class="form-control" name="is_perishable" id="is_perishable">
								<option value="1" @if( $product && $product->is_perishable == 1 ) selected="selected" @endif>Sí</option>
								<option value="0" @if( $product && $product->is_perishable == 0 ) selected="selected" @endif>No</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label title="Se puede mexclar con:" for="">
								Puede mezclarse con:
							</label>
							<select class="form-control" name="can_mix" id="can_mix">
								<option value="Food" @if( $product && $product->can_mix == 'Food' ) selected="selected" @endif>Con alimentos</option>
								<option value="Pets" @if( $product && $product->can_mix == 'Pets' ) selected="selected" @endif>Solo mascotas</option>
								<option value="Toilet" @if( $product && $product->can_mix == 'Toilet' ) selected="selected" @endif>Solo productos de aseo</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Imagenes" for="" class="control-form">
								Imagenes
							</label>
							<a href="#" class="btn btn-primary" id="btn-images" style="max-width: 100%; display: block">
								Seleccionar / Editar Imagenes
							</a>
						</div>
						@if(!empty($product->image_medium_url))
							<div class="col-xs-1">
								<a href="#" class="thumbnail">
									<img src="{{$product->image_medium_url}}"  width="60px" height="60px">
								</a>
							</div>
						@endif
						<div class="col-xs-8">
							<div class="row">
								@if(!empty($product->images))
									@foreach($product->images as $image)
										<div class="col-xs-2">
											<a href="#" class="thumbnail">
												<img src="{{$image->image_medium_url}}"  width="60px" height="60px">
											</a>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-6">
							<label title="Descripción del producto" for="description">Descripción</label>
							<textarea class="ckeditor" id="description" name="description" placeholder="Ingresa descripción" rows="6">{{ $product ? $product->description : Input::old('description') }}</textarea>
						</div>
						<div class="col-xs-6">
							<label title="Información nutricional del producto" for="nutrition_facts">Información nutricional</label>
							<textarea class="ckeditor" id="nutrition_facts" name="nutrition_facts" placeholder="Ingresa información nutricional" rows="6">{{ $product ? $product->nutrition_facts : Input::old('nutrition_facts') }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--==================================
	=            Presentacion            =
	===================================-->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="box-title">
						Presentación
					</h4>
				</div>
				<div class="box-body">
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Presentación del producto" for="presentation">Presentación</label>
							<input type="text" class="form-control" id="presentation" name="presentation" placeholder="Presentación del producto" value="{{ $product ? $product->presentation : Input::old('presentation') }}">
						</div>
						<div class="col-xs-3">
							<label title="SubPresentación del producto" for="subpresentation">Sub presentación</label>
							<input type="text" class="form-control" id="subpresentation" name="subpresentation" placeholder="Presentación del producto" value="{{ $product ? $product->subpresentation : Input::old('subpresentation') }}">
						</div>
						<div class="col-xs-3">
							<label title="Unidades del producto dentro de la presentación." for="quantity">Cantidad</label>
							<input type="text" class="form-control" id="quantity" name="quantity" placeholder="Ingresa cantidad"  value="{{ $product ? $product->quantity : Input::old('quantity') }}" required="required">
						</div>
						<div class="col-xs-3 ">
							<label title="Unidad de medida de la presentación del producto" for="unit">Unidad de medida</label>
							<input type="text" class="form-control" id="unit" name="unit" placeholder="Ingresa unidad"  value="{{ $product ? $product->unit : Input::old('unit') }}" required="required">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--====  End of presentacion  ====-->

	<!--==================================
	=   Precio por Unidad de Medida PUM  =
	===================================-->
	@if( ($product && $product->type == 'Simple') || !$product)
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="box-title">
						Precio por Unidad de Medida (PUM)
					</h4>
				</div>
				<div class="box-body">
					<div class="form-group row">
						<div class="col-xs-4">
							<label title="¿El producto tiene PUM?" for="has_pum">¿Tiene PUM?</label>
							<select class="form-control" name="has_pum" id="has_pum" >
								<option value="1">Sí</option>
								<option value="0" {{ $product && !$product->unit_pum && !$product->net_quantity_pum ? "selected" : ''}} >No</option>
							</select>
						</div>
						<div class="col-xs-4">
							<label title="Peso neto" for="net_quantity_pum">Cantidad neta</label>
							<input type="number" class="form-control" id="net_quantity_pum" name="net_quantity_pum" placeholder="Peso neto" {{ $product && !$product->net_quantity_pum ? 'min="0" readonly' : 'min="0.01" required'}} value="{{ $product ? $product->net_quantity_pum : Input::old('net_quantity_pum') }}">
						</div>
						<div class="col-xs-4">
							<label title="Unidad de medida PUM" for="unit_pum">Unidad de medida PUM</label>
							<select class="form-control" name="unit_pum" id="unit_pum"  {{ $product && !$product->unit_pum ? 'readonly' : 'required'}}>
								<option value=""></option>
								<option value="Gramo" {{ $product && ($product->unit_pum == 'Gramo') ? "selected" : ''}} >Gramo</option>
								<option value="Metro" {{ $product && ($product->unit_pum == 'Metro') ? "selected" : ''}} >Metro</option>
								<option value="Mililitro" {{ $product && ($product->unit_pum == 'Mililitro') ? "selected" : ''}} >Mililitro</option>
								<option value="Unidad" {{ $product && ($product->unit_pum == 'Unidad') ? "selected" : ''}} >Unidad</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
		<!--====  End of PUM  ====-->

	<!--===============================
	=            Dimensión            =
	================================-->

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="box-title">
						Dimensiones
					</h4>
				</div>
				<div class="box-body">
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Presentación del producto" for="presentation">Alto</label>
							<input type="number" class="form-control" id="height" name="height" placeholder="Alto del producto" min="0.1" value="{{ $product ? $product->height : Input::old('height') }}">
						</div>
						<div class="col-xs-3">
							<label title="SubPresentación del producto" for="subpresentation">Ancho</label>
							<input type="number" class="form-control" id="width" name="width" placeholder="Ancho del producto" min="0.1" value="{{ $product ? $product->width : Input::old('width') }}">
						</div>
						<div class="col-xs-3">
							<label title="Unidades del producto dentro de la presentación." for="quantity">Largo</label>
							<input type="number" class="form-control" id="length" name="length" placeholder="Largo del producto"  min="0.1" value="{{ $product ? $product->length : Input::old('length') }}">
						</div>
						<div class="col-xs-3">
							<label title="Peso en gramos del producto." for="weight">Peso(Gr)</label>
							<input type="number" class="form-control" id="weight" name="weight" placeholder="Peso en gramos(gr) del producto"  value="{{ $product ? $product->weight : Input::old('weight') }}" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Volumen en mililitros del producto." for="volume">Volumen(Cm<sup>3</sup>)</label>
							<input type="number" class="form-control" id="volume" name="volume" placeholder="Volumen del producto"  value="{{ $product ? $product->volume : Input::old('volume') }}" required readonly>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--====  End of Dimensión  ====-->

	<!--==================================
	=            Contabilidad            =
	===================================-->
	<!--<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="box-title">
						Contabilidad
					</h4>
				</div>
				<div class="box-body">
					<div class="row form-group">
						<div class="col-xs-6">
							<label for="accounting_account">Cuenta contable</label>
							<input type="text" class="form-control" id="accounting_account" name="accounting_account" placeholder="Ingresa la cuenta contable" value="{{ $product ? $product->accounting_account : "" }}" @if (!$admin_permissions['permission1'] && $product) readonly="readonly" @endif>
						</div>
						<div class="col-xs-6">
							<label for="accounting_account_type">Naturaleza de cuenta contable</label>
							<select class="form-control" id="accounting_account_type" name="accounting_account_type">
								<option value="Credito" {{ $product && ('Credito' == $product->accounting_account_type) ? 'selected="selected"' : ""}}>Crédito</option>
								<option value="Debito" {{ $product && ('Debito' == $product->accounting_account_type) ? 'selected="selected"' : ""}}>Débito</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-4">
							<label for="accounting_line">Linea producto contable</label>
							<input type="text" id="accounting_line" name="accounting_line" class="form-control" value="{{ $product && isset($product->accounting_line) ? $product->accounting_line : Input::old('accounting_line') }}" @if (!$admin_permissions['permission1'] && $product) readonly="readonly" @endif>
						</div>
						<div class="col-xs-4">
							<label for="accounting_group">Grupo producto contable</label>
							<input type="text" id="accounting_group" name="accounting_group" class="form-control" value="{{ $product && isset($product->accounting_group) ? $product->accounting_group : Input::old('accounting_group') }}" @if (!$admin_permissions['permission1'] && $product) readonly="readonly" @endif>
						</div>
						<div class="col-xs-4">
							<label for="accounting_code">Código contable</label>
							<input type="text" id="accounting_code" name="accounting_code" class="form-control" value="{{ $product && isset($product->accounting_code) ? $product->accounting_code : Input::old('accounting_code') }}" @if (!$admin_permissions['permission1'] && $product) readonly="readonly" @endif>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
	<!--====  End of Contabilidad  ====-->
	<div class="row">
		<div class="col-xs-3 col-xs-offset-9 text-right">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</div>
	</div>

	<!--====  Start Modal images  ====-->
	<div class="modal fade" id="modal-images" tabindex="-1" role="dialog" aria-labelledby="modalImages" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modalImages">Imagenes del producto</h4>
				</div>
				<div class="modal-body">
					@if(!empty($product->image_medium_url))
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-12">
										<label title="Imágen del producto" for="image">Imágen principal del producto</label>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-2">
										<img src="{{ $product->image_medium_url }}" width="60px" height="60px" name="{{ $product->image_medium_url }}" />
									</div>
									<div class="col-xs-10">
										<input type="file" class="form-control" name="image">
									</div>
								</div>
							</div>
						</div>
						<br />
					@else
						<div class="row">
							<div class="col-xs-12">
								<label title="Imágen del producto" for="image">Imágen principal del producto</label>
								<input type="file" class="form-control" name="image">
							</div>
						</div>
					@endif
					@if(!empty($product->images))
						<div class="row">
							<div class="col-xs-offset-8 col-xs-4" style="text-align: right">
								<a href="#" class="btn btn-primary" id="btn-add-image">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
									Agregar imagen
								</a>
							</div>
						</div>
						<input type="hidden" id="products_index" value="{{count($product->images)}}">
						<div class="row" id="items" style="margin-top: 30px;">
							@foreach($product->images as $key => $image)
								<div class="col-xs-10" id="image-item-{{$key}}">
									<div class="row">
										<div class="col-xs-2">
											<img src="{{ $image->image_small_url }}" width="60px" height="60px" name="images[{{$key}}]" data-id="{{$image->id}}"  />
										</div>
										<div class="col-xs-10">
											<input type="file" class="form-control" name="images[{{$key}}]">
										</div>
									</div>
								</div>
								<div class="col-xs-2 remove-image-item-{{$key}}" style="margin-bottom: 10px;">
									<a href="#" class="btn btn-danger remove-image-item" id="remove-image-item-{{$key}}">
										<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
									</a>
								</div>
							@endforeach
						</div>
					@else
						<div class="row">
							<div class="col-xs-offset-8 col-xs-4" style="text-align: right">
								<a href="#" class="btn btn-primary" id="btn-add-image">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
									Agregar imagen
								</a>
							</div>
						</div>
						<div class="row" id="items" style="margin-top: 30px;"></div>
					@endif
				</div>
				<input type="hidden" id="products_index" value="0">
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<!--====  End Modal images ====-->
</form>

@if (!empty($product->id))
	<hr>
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					@include('admin.products.specifications', compact('product'))
				</div>
			</div>
		</div>
	</div>
@endif

<!--=========================================
=            Producto en tiendas            =
==========================================-->
@if ( $product )
	<hr>
	<div class="row" id="store_product">
		<div class="col-xs-12">
			<h3>Producto en tiendas</h3>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<div class="row form-group">
						<div class="col-xs-4">
							<label for="">Tienda</label>
							<select name="store_id" id="store_id" class="form-control">
								<option value="">Selecciona</option>
								@foreach ($stores as $store)
									<option value="{{ $store->id }}">{{ $store->name }} - {{ $store->city_name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-3">
							<label for="" style="color:#FFF;">loader</label>
							<div style="display: block;">
								<img src="{{ asset_url() }}/img/loading.gif" class="unseen loading">
							</div>
						</div>
					</div>
					<div id="store_product_section">
					</div>
				</div>
			</div>
		</div>
	</div>
@endif
<!--====  End of Producto en tiendas  ====-->
<script>
    $(document).ready(function() {
        <!--====  Calcular el volumen del producto  ====-->
        $('#height, #width, #length').on('input', function (e) {
			if($('#height').val() !== '' && $('#width').val() !== '' && $('#length').val() !== '' ){
				var v = parseFloat($('#height').val() * $('#width').val() * $('#length').val()).toFixed(1);
				var volumen = Math.round(v);
                $('#volume').val(volumen);
			}else $('#volume').val('');
        });
        <!--====  End volumen del producto  ====-->
        <!--====  Activar campos de producto con PUM  ====-->
        $('#has_pum').on('change', function (e) {
            if( $(this).val() == 1 ){
				$('#net_quantity_pum').attr('min','0.01');
                $('#unit_pum, #net_quantity_pum').prop('required',true).attr('readonly',false);
			}else{
                $('#net_quantity_pum').attr('min','0').val('');
                $('#unit_pum option[value=""]').attr('selected','selected');
                $('#unit_pum, #net_quantity_pum').attr('readonly', 'readonly').prop('required',false);
                if($('#unit_pum, #net_quantity_pum').hasClass('error'))
                    $('#unit_pum, #net_quantity_pum').removeClass('error');
			}
        });
        <!--====  End campos PUM  ====-->
    });
</script>