    {{ Form::open(['route' => 'saveStoreProductWarehouse', 'id' => 'store-product-warehouse-form']) }}

    @if (empty($store_product_warehouse->id))
        <div class="row">
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Alerta!</strong> Este producto aún no ha sido agregado a esta bodega.
            </div>
        </div>
    @else
        {{ Form::hidden('id', $store_product_warehouse->id) }}
    @endif
    {{ Form::hidden('warehouse_id', $warehouse->id) }}
    {{ Form::hidden('store_product_id', $store_product->id) }}

    <div class="row form-group">
        <div class="col-md-3">
            {{ Form::label('is_visible', '¿Producto visible? (abastecimiento)', ['class' => 'control-label']) }}
            {{ Form::select('is_visible', $boolean_field_values, $store_product_warehouse->is_visible, ['class' => 'form-control', 'required']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('status', 'Estado', ['class' => 'control-label']) }}
            {{ Form::select('status', $states, $store_product_warehouse->exists ? $store_product_warehouse->status : 1, ['class' => 'form-control']) }}
        </div>
        <div class="col-md-3">
            <label for="discontinued">¿Producto descodificado?</label>
            <select name="discontinued" id="discontinued" class="form-control">
                <option value="1" {{ !empty($store_product_warehouse) && $store_product_warehouse->discontinued == 1 ? 'Selected' : '' }}>Si</option>
                <option value="0" {{ !empty($store_product_warehouse) && $store_product_warehouse->discontinued == 0 ? 'Selected' : '' }}>No</option>
            </select>
        </div>
        <div class="col-md-3">
            {{ Form::label('first_reception_date', 'Primera fecha de recepción', ['class' => 'control-label']) }}
            {{ Form::text('first_reception_date', isset($store_product_warehouse->first_reception_date) ? date('d/m/Y', strtotime($store_product_warehouse->first_reception_date)) : '', ['class' => 'form-control', 'readonly', 'placeholder' => 'Primera fecha de recepción']) }}
        </div>
        <div class="col-md-3 form-group">
            {{ Form::label('manage_stock', '¿Maneja stock?', ['class' => 'control-label']) }}
            {{ Form::select('manage_stock', $boolean_field_values, $store_product_warehouse->manage_stock, ['class' => 'form-control', 'required']) }}
        </div>
    </div>

    <div class="manage_stock_container" style="display: {{ empty($store_product_warehouse->manage_stock) ? 'none' : 'block' }};">
        <div class="row form-group">
            <div class="col-md-3">
                {{ Form::label('is_visible_stock', '¿Visible sin stock?', ['class' => 'control-label']) }}
                {{ Form::select('is_visible_stock', $boolean_field_values, $store_product_warehouse->is_visible_stock, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('minimum_stock', 'Stock mínimo', ['class' => 'control-label']) }}
                {{ Form::number('minimum_stock', $store_product_warehouse->minimum_stock, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ideal_stock', 'Stock ideal', ['class' => 'control-label']) }}
                {{ Form::number('ideal_stock', $store_product_warehouse->ideal_stock, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-3">
                {{ Form::label('minimum_picking_stock', 'Stock mínimo en alistamiento', ['class' => 'control-label']) }}
                {{ Form::number('minimum_picking_stock', $store_product_warehouse->minimum_picking_stock, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('maximum_picking_stock', 'Stock máximo en alistamiento', ['class' => 'control-label']) }}
                {{ Form::number('maximum_picking_stock', $store_product_warehouse->maximum_picking_stock, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('storage_position', 'Posición en bodega', ['class' => 'control-label']) }}
                {{ Form::number('storage_position', $store_product_warehouse->storage_position, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('storage_height_position', 'Posición en altura', ['class' => 'control-label']) }}
                {{ Form::text('storage_height_position', $store_product_warehouse->storage_height_position, ['class' => 'form-control', 'max-length' => 4]) }}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-3">
                {{ Form::label('reception_stock', 'Stock en recibo', ['class' => 'control-label']) }}
                {{ Form::number('reception_stock', $store_product_warehouse->reception_stock, ['class' => 'form-control', 'disabled']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('picking_stock', 'Stock en alistamiento', ['class' => 'control-label']) }}
                {{ Form::number('picking_stock', $store_product_warehouse->picking_stock, ['class' => 'form-control', 'disabled']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('picked_stock', 'Stock alistado', ['class' => 'control-label']) }}
                {{ Form::number('picked_stock', $store_product_warehouse->picked_stock, ['class' => 'form-control', 'disabled']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('storaged_stock', 'Stock almacenado', ['class' => 'control-label']) }}
                {{ Form::number('storaged_stock',  $store_product->getStorageStock($warehouse->id), ['class' => 'form-control', 'disabled']) }}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-3">
                {{ Form::label('return_stock', 'Stock en devolución', ['class' => 'control-label']) }}
                {{ Form::number('return_stock', $store_product_warehouse->return_stock, ['class' => 'form-control', 'disabled']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('current_stock', 'Stock actual total', ['class' => 'control-label']) }}
                {{ Form::number('current_stock', $store_product->getCurrentStock($warehouse->id), ['class' => 'form-control', 'disabled']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('commited_stock', 'Stock comprometido', ['class' => 'control-label']) }}
                {{ Form::number('commited_stock', $store_product->getCommittedStock($warehouse->id), ['class' => 'form-control', 'disabled']) }}
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-3 col-xs-offset-9 text-right">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary', 'id' => 'btn-save-product-warehouse']) }}
        </div>
    </div>

    {{ Form::close() }}

<script>

    $(document).ready(function() {
        var identifier = '#store-product-warehouse-form';
        var form = $(identifier);
        var requiredIfStockEnabledCallback = function (element) {
            var stockEnabled = $('#manage_stock').find(':selected').val();
            var elementValue = $(element).val();

            return !(stockEnabled && elementValue);
        };

        form.validate({
            rules: {
                minimum_stock: {
                    required: requiredIfStockEnabledCallback
                },
                ideal_stock: {
                    required: requiredIfStockEnabledCallback,
                    min: {
                        param: function () {
                            return parseInt($('#minimum_stock').val(), 10);
                        }
                    }
                },
                minimum_picking_stock: {
                    required: requiredIfStockEnabledCallback
                },
                maximum_picking_stock: {
                    required: requiredIfStockEnabledCallback,
                    min: {
                        param: function () {
                            return parseInt($('#minimum_picking_stock').val(), 10);
                        }
                    }
                },
                storage_position: {
                    required: requiredIfStockEnabledCallback
                },
                storage_height_position: {
                    required: requiredIfStockEnabledCallback
                }
            }
        });

        form.on('submit', function (event) {
            $('#btn-save-product-warehouse').attr('disabled', true);
            var form = $(this);
            var serializedForm = form.serialize();
            var url = form.prop('action');
            var messageContainer = $('#store-product-warehouse-message');
            event.preventDefault();

            if (!form.valid()) {
                return true;
            }

            $.ajax({
                url: url,
                type: 'POST',
                data: serializedForm
            }).done(function(response) {
                messageContainer.html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+ response.message +'</div>').delay(3000).find('.alert-success.close').click();
                $('#warehouse_id').val('').trigger('change');
                $('#btn-save-product-warehouse').attr('disabled', false);
            }).fail(function(response) {
                var response = response.responseJSON;
                var message = '';
                if (typeof response === 'object') {
                    for (var index in response) {
                        if (!response.hasOwnProperty(index)) {
                            continue;
                        }
                        message += '<p><b>' + index + '</b> ' + response[index] + '</p>';
                    }
                }
                messageContainer.html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+ message +'</div>').delay(3000).find('.alert-danger.close').click();
                $('#btn-save-product-warehouse').attr('disabled', false);
            }).always(function() {
                messageContainer.show();
                setTimeout(function() {
                    messageContainer.hide(function() {
                        messageContainer.find('.alert').remove();
                    });
                }, 5000);
            });
        })
    });
</script>