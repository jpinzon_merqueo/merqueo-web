<form method="post"
      action="{{ route('adminProducts.saveProductDetailAjax', $product->id) }}">

    @if (!empty($detail->id))
        {{ Form::hidden('id', $detail->id) }}
    @endif

    <div class="row">
        <div class="form-group col-md-6">
            {{ Form::label('type', 'Tipo', ['class' => 'control-label']) }}
            {{ Form::fillSelectWithoutKeys('type', $detail_types, $detail->type, ['class' => 'form-control', 'required'], true) }}
        </div>

        <div class="form-group col-md-6">
            {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
            {{ Form::text('name', $detail->name, ['class' => 'form-control', 'required']) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Descripción', ['class' => 'control-label']) }}
        {{ Form::textarea('description', $detail->description, ['class' => 'form-control', 'required', 'rows' => 3]) }}
    </div>

    <div class="form-group">
        {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
    </div>
</form>
