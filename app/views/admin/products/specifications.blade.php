<div class="row">
    <div class="col-md-12">
        <h3>Agregar detalle</h3>
        <div class="detail-form new-form">
            @include('admin.products.specifications_form', ['detail' => new ProductDetail(), 'product' => $product])
        </div>
    </div>
</div>

<div class="row items">
    <div class="col-md-12">
        <h3>Detalles del producto</h3>
        <table id="products-table" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Tipo</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Acción</th>
            </tr>
            </thead>
            <tbody>
                @if (count($product->details))
                    @foreach($product->details as $detail)
                        <tr>
                            <td>{{ $detail->type }}</td>
                            <td>{{ $detail->name }}</td>
                            <td>{{ str_limit($detail->description, 50) }}</td>

                            <td>
                                <div style="display:none" class="form-detail">
                                    @include('admin.products.specifications_form', compact('detail', 'product'))
                                </div>

                                <div class="text-center">
                                    <a href="{{ route('adminProducts.saveProductDetailAjax', $product->id) }}"
                                       class="update-detail">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-pencil" aria-hidden="true" title="Editar"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('adminProducts.deleteProductDetailAjax', $detail->id) }}"
                                       class="delete-detail">
                                        <button class="btn btn-danger">
                                            <i class="fa fa-times" aria-hidden="true" title="Eliminar"></i>
                                        </button>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" class="text-center">No hay detalles relacionados.</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {

        var originalForm = $('.new-form');
        var detailForm = $('.detail-form form');
        var originalFormHtml = originalForm.html();

        $('.update-detail').on('click', function (event) {
            event.preventDefault();
            $(this).parent().parent().find('.form-detail').show();
        });

        $('.delete-detail').on('click', function (event) {
            if (!confirm('¿Realmente deseas eliminar este detalle?')) {
                event.preventDefault();
            }
        });

        detailForm.validate();
    });
</script>
