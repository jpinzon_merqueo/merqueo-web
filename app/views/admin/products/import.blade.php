@extends('admin.layout')

@section('content')

<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<script>
    $('.fancybox').fancybox({
        autoSize    : true,
        closeClick  : false,
        closeBtn    : false ,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers   : {
           overlay : {closeClick: false}
        }
    });
</script>
<a href="#importing" class="fancybox unseen"></a>
<div id="importing" class="unseen"><p>Procesando...</p><br><img src="{{ web_url() }}/admin_asset/img/importing.gif"></div>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">

	@if(Session::has('respond') && $respond = Session::get('respond'))
        @if ( !empty($respond['success']) )
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @if ($respond['success'])
        	       <b>Exitoso!</b> {{ $respond['success'] }}
                @endif
        	</div>
        @elseif ( !empty($respond['error']) )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @if ($respond['error'])
                   <b>Alerta!</b> {{ $respond['error'] }}
                @endif
            </div>
        @endif
	@endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id='main-form' action="{{ route('adminProducts.import') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="box-body row">
                        <div class="form-group col-xs-12">
                            <label for="city_id">Ciudad</label>
                            <select class="form-control" id="city_id" name="city_id">
                                <option value="" selected="selected"></option>
                                @foreach($cities as $city)
                                <option value="{{$city->id}}" {{ $city->id == Session::get('admin_city_id') ? 'selected="selected"' : '' }}>{{$city->city}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="store_id">Tienda</label>
                            <select class="form-control" id="store_id" name="store_id">
                                @foreach($stores as $store)
                                <option value="{{$store->id}}">{{$store->name}} - {{$store->city}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="warehouse_id">Bodega</label>
                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                @foreach($warehouses as $warehouse)
                                <option value="{{$warehouse->id}}">{{$warehouse->warehouse}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Proceso a ejecutar</label>
                            <select type="text" class="form-control" name="process" id="process">
                                <option value="" selected="selected"></option>
                                <option value="count_images">Contar imágenes de producto</option>
                                <option value="import_products">Importar productos</option>
                                <option value="update_products">Actualizar productos</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Ruta a carpeta de las imágenes</label>
                            <input type="text" class="form-control" name="path_images" id="path_images" placeholder="Ingresar ruta a carpeta de las imágenes">
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Archivo</label>
                            <input type="file" class="form-control" name="file" placeholder="File">
                        </div>
                        <div class="form-group col-xs-12">
                            <!-- <a href="{{ $url_sample_file }}" target="_blank">Descargar archivo de ejemplo vacío</a> -->
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Está seguro que quiere importar este archivo?')">Importar productos</button>
                        <button type="button" class="btn btn-success export-products">Exportar archivo de productos</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Update stock Modal -->
<div class="modal fade" id="modal-export-products">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title accept-title">Exportar productos</h4>
            </div>
            <form id="update-to-enrutado" method="POST" action="{{ route('adminProducts.exportProducts') }}" class="form-modal" accept-charset="UTF-8">
                <div class="modal-body">
                    <div class="unseen alert alert-danger form-has-errors"></div>
                    <div class="form-group accept-options row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <label for="export_city_id">Ciudad</label>
                            <select class="form-control" id="export_city_id" name="export_city_id">
                                <option value="" selected="selected"></option>
                                @foreach($cities as $city)
                                <option value="{{$city->id}}" {{ $city->id == Session::get('admin_city_id') ? 'selected="selected"' : '' }}>{{$city->city}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group accept-options row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                           <label for="export_store_id">Tienda</label>
                           <select class="form-control" id="export_store_id" name="export_store_id">
                               @foreach($stores as $store)
                               <option value="{{$store->id}}">{{$store->name}} - {{$store->city}}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="form-group accept-options row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <label for="export_warehouse_id">Bodega</label>
                            <select class="form-control" id="export_warehouse_id" name="export_warehouse_id">
                                @foreach($warehouses as $warehouse)
                                <option value="{{$warehouse->id}}">{{$warehouse->warehouse}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success export-products-file accept-btn">Exportar archivo</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$('#main-form').validate({
  rules: {
    store_id: 'required',
    process: 'required',
    path_images: {
        required : function() {
            if ($('#process').val() != 'update_products') {
                return true;
            } else {
                if ($('#path_images').val() != '') {
                    return true;
                } else
                    return false;
            }
        },
    },
    file: 'required',
  }
});

$('#main-form').submit(function () {
    if($(this).valid()) {
       $('.fancybox').trigger('click');
    }
});

jQuery(document).ready(function($) {
    $('body').on('click', '.export-products', function(event) {
        event.preventDefault();
        $('#modal-export-products').modal('show');
    });
    $('body').on('click', '.export-products-file', function(event) {
        $('#modal-export-products').modal('hide');
    });
    $('body').on('change', '#city_id', function(event) {
        event.preventDefault();
        let city_id = $(this).val();
        $('#city_id, #store_id, #warehouse_id').prop('disabled', true);
        $.ajax({
            url: '{{ route('adminProducts.getStoresByCityAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: {city_id: city_id},
        })
        .done(function(data) {
            $('#store_id').empty();
            let html = '';
            if ( data.length ) {
                $.each(data, function(index, val) {
                    html += `<option value="${val.id}">${val.name} - ${val.city_name}</option>`;
                });
            }
            $('#store_id').html(html);
        })
        .fail(function(data) {
            console.log("Ocurrió un error al obtener las tiendas.");
        })
        .always(function() {
            $('#city_id, #store_id').prop('disabled', false);
        });

        $.ajax({
            url: '{{ route('admin.get_warehouses_ajax') }}',
            type: 'GET',
            dataType: 'json',
            data: {city_id: city_id},
        })
        .done(function(data) {
            $('#warehouse_id').empty();
            let html = '';
            $.each(data, function(index, val) {
                html += `<option value="${index}">${val}</option>`;
            });
            $('#warehouse_id').html(html);
        })
        .fail(function() {
            console.log("Ocurrió un error al obtener las bodegas.");
        })
        .always(function() {
            $('#city_id, #warehouse_id').prop('disabled', false);
        });
    });

    $('#export_city_id').change(function(event) {
        event.preventDefault();
        let city_id = $(this).val();
        $('#export_city_id, #export_store_id, #export_warehouse_id').prop('disabled', true);
        $.ajax({
            url: '{{ route('adminProducts.getStoresByCityAjax') }}',
            type: 'GET',
            dataType: 'json',
            data: {city_id: city_id},
        })
        .done(function(data) {
            $('#export_store_id').empty();
            let html = '';
            if ( data.length ) {
                $.each(data, function(index, val) {
                    html += `<option value="${val.id}">${val.name} - ${val.city_name}</option>`;
                });
            }
            $('#export_store_id').html(html);
        })
        .fail(function(data) {
            console.log("Ocurrió un error al obtener las tiendas.");
        })
        .always(function() {
            $('#export_city_id, #export_store_id').prop('disabled', false);
        });

        $.ajax({
            url: '{{ route('admin.get_warehouses_ajax') }}',
            type: 'GET',
            dataType: 'json',
            data: {city_id: city_id},
        })
        .done(function(data) {
            $('#export_warehouse_id').empty();
            let html = '';
            $.each(data, function(index, val) {
                html += `<option value="${index}">${val}</option>`;
            });
            $('#export_warehouse_id').html(html);
        })
        .fail(function() {
            console.log("Ocurrió un error al obtener las bodegas.");
        })
        .always(function() {
            $('#export_city_id, #export_warehouse_id').prop('disabled', false);
        });
    });
});
</script>

@stop