@if (!Request::ajax())
    @extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel
            </small>
        </h1>

        <span class="breadcrumb" style="top:0px">
                <a href="{{ route('adminProducts.add') }}">
                    <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Producto</button>
                </a>
            </span>
        @if ($admin_permissions['permission1'])
            <span class="breadcrumb fcbk_feed_store col-md-6"
                  style="top: 0px; right: 100px">
                    <div class="col-md-6">
                        <div class="col-md-7">
                            <select class="form-control">
                                <option value="">Selecciona alguna tienda</option>
                                @foreach ($stores as $store)
                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <a href="{{ route('adminProducts.downloadMakeFacebookFeed', 0) }}"
                           class="col-md-5">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-download"></i> Feed Facebook
                            </button>
                        </a>
                    </div>

                    <span class="fcbk_feed_store col-md-6">
                        <a href="{{ route('adminProducts.downloadElasticSearchJsonProducts') }}"
                           data-pass-validation="true"
                           class="col-md-12">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-download"></i> Json Elastic Search
                            </button>
                        </a>
                    </span>

                    <a id="mock-download" download></a>
                </span>
        @endif
    </section>

    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <table width="100%" class="orders-table">
                                        @if ( !empty($cities) )
                                            <tr>
                                                <td align="right">
                                                    <label>Ciudad:</label>
                                                </td>
                                                <td>
                                                    <select id="city_id" name="city_id" class="form-control">
                                                        <option value="">Selecciona</option>
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}">{{ $city->city }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td align="right"><label>Buscar:</label>&nbsp;</td>
                                                <td><input name="s" type="text" placeholder="ID, referencia, nombre, precio, PLU" class="search form-control"></td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td align="right"><label>Ordenar por:</label>&nbsp;</td>
                                            <td>
                                                <select id="order_by" name="order_by" class="form-control">
                                                    <option value="products.created_at">Fecha de creación</option>
                                                    <option value="products.updated_at">Fecha de actualización</option>
                                                    <option value="products.id">ID producto</option>
                                                </select>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <button type="submit" id="search-btn" class="btn btn-primary">Buscar</button>&nbsp;&nbsp;
                                                <button type="reset" id="btn-reset" class="btn btn-primary">Resetear</button>
                                            </td>
                                        </tr>
                                        <tr>
                                        <tr>
                                        </tr>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="paging"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                <div class="paginate">
                                </div>
                            </div>
                        </div>
                        <div align="center" class="paging-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        var searchForm = (function() {
            'use strict';

            function searchForm() {
                // enforces new
                if (!(this instanceof searchForm)) {
                    return new searchForm();
                }
                this.url_product_ajax = "{{ route('adminProducts.index') }}";
                this.bindAction();
                this.searchProducts();
            }

            searchForm.prototype.bindAction = function() {
                let self = this;
                $('body').on('submit', '#search-form', function(event) {
                    event.preventDefault();
                    self.searchProducts();
                });

                $('body').on('click', '.paginate a', function(event) {
                    event.preventDefault();
                    var form_data = $('#search-form').serialize();
                    var paginate_url = $(this).prop('href');
                    self.getPaginateInfo(form_data, paginate_url);
                });
            };

            searchForm.prototype.searchProducts = function() {
                $('.paging-loading').show();
                $.ajax({
                    url: this.url_product_ajax,
                    type: 'GET',
                    dataType: 'json',
                    data: $('#search-form').serialize(),
                })
                    .done(function(data) {
                        if ( data ) {
                            $('.paging').empty();
                            $('.paging').html(data.html);
                            $('.paginate').empty().html(data.links_html);
                        }
                    })
                    .fail(function(data) {
                    })
                    .always(function(data) {
                        $('.paging-loading').hide();
                    });
            };

            searchForm.prototype.getPaginateInfo = function(form_data, paginate_url) {
                $('.paging-loading').show();
                $.ajax({
                    url: paginate_url,
                    type: 'GET',
                    dataType: 'json',
                    data: form_data,
                })
                    .done(function(data) {
                        if ( data ) {
                            $('.paging').empty();
                            $('.paging').html(data.html);
                            $('.paginate').empty().html(data.links_html);
                        }
                    })
                    .fail(function(data) {
                        console.debug(data);
                        console.log("error al buscar.");
                    })
                    .always(function() {
                        $('.paging-loading').hide();
                    });
            };

            return searchForm;
        }());
        $(document).ready(function() {
            let search_form = new searchForm;
            var form = $('.fcbk_feed_store')
            var defaultUrl = form.find('a').prop('href')
            var defaultAnchorContent = form.find('a button').html()
            var storeId = 0
            var descargando = false
            // paging(1, '');
            $('#city_id').change(function () {
                var city_id = $(this).val();
                $.ajax({
                    url: '{{ route('adminProducts.getStoresByCityAjax') }}',
                    type: 'get',
                    dataType: 'json',
                    data: {city_id: city_id},
                })
                    .done(function(data) {
                        var html = '';
                        html += '<option value=""></option>';
                        $.each(data, function(index, val) {
                            html += '<option value="'+ val.id +'">'+ val.name +'</option>';
                        });

                        $('#store_id').empty().append(html);
                    })
                    .fail(function() {
                        console.log("error al filtrar por ciudad.");
                    });
            });

            form.find('select').on('change', function() {
                storeId = $(this).find(':selected').val()
            });

            form.find('a:not(#mock-download)').on('click', function (event) {
                var anchor = $(this)
                event.preventDefault()
                if (descargando || !storeId && !anchor.data('pass-validation')) {
                    return true
                }
                var downloadFunction = download($(this).find('button').html())
                var button  = anchor.find('button')
                button.html('Descargando...')
                button.addClass('disabled')
                descargando = true

                $.get(replaceIdFromUrl($(this).prop('href'), storeId))
                    .done(downloadFunction.bind(this))
                    .fail(errorAction)
            });

            /**
             * Descarga el archivo que se retorne en la url.
             * @param  {Object} data
             * @return {Function}
             */
            function download(anchorContent) {
                return function (data) {
                    var anchor = form.find('#mock-download')
                    var button = $(this).find('button')
                    anchor.prop('href', data.url)
                    anchor[0].click()
                    button.html(anchorContent)
                    button.removeClass('disabled')
                    descargando = false
                }
            }

            /**
             * Acción en caso de que se presente un error.
             * @param  {Object} response
             */
            function errorAction(response) {
                alert(response.responseJSON.message)
            }

            /**
             * Modifica el id de la la tienda para descargar el
             * el feed de facebook.
             *
             * @param {string} url
             * @param {int} newId
             * @return {string}
             */
            function replaceIdFromUrl(url, newId) {
                return url.replace(/[0-9]+$/, newId)
            }
        });
    </script>
@endsection
@else
@section('content')
    <table id="products-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Referencia</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Unidad</th>
            <th>Editar</th>
        </tr>
        </thead>
        <tbody>
        @if (count($products))
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->reference }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->unit }}</td>
                    <td class="text-center">
                        <a href="{{ route('adminProducts.edit', ['id' => $product->id]) }}">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-folder-open"></span>
                            </button>
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr><td colspan="10" align="center">No se encontraron productos.</td></tr>
        @endif
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando <span></span> productos</div>
        </div>
    </div>
@endsection
@endif