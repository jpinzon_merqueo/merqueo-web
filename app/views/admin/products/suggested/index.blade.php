@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminProducts.editSuggestedProduct') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Producto Sugerido</button>
        </a>
    </span>
</section>

<section class="content">
    @if(Session::has('message'))
        @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('message') }}
            </div>
        @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('message') }}
            </div>
        @endif
    @endif
<div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                   <div class="paging">
                        <table id="products-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Promoción</th>
                                    <th>Pasillos</th>
                                    <th>Prioridad</th>
                                    <th>Fecha Inical</th>
                                    <th>Fecha Final</th>
                                    <th>Estado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($suggested_products))
                                @foreach($suggested_products as $suggested_product)
                                <tr>
                                    <td>{{ $suggested_product->name }} {{ $suggested_product->quantity }} {{ $suggested_product->unit }}</td>
                                    <td>{{ $suggested_product->promo }}</td>
                                    <td>{{ $suggested_product->shelf }}</td>
                                    <td>{{ $suggested_product->priority }}</td>
                                    <td>@if($suggested_product->start_date != '0000-00-00 00:00:00'){{ date('d/m/Y', strtotime($suggested_product->start_date)) }}@endif</td>
                                    <td>@if($suggested_product->end_date != '0000-00-00 00:00:00'){{ date('d/m/Y', strtotime($suggested_product->end_date)) }}@endif</td>
                                    <td>
                                        @if($suggested_product->status == 1)
                                            <span class="badge bg-green">Activo</span>
                                        @else
                                            <span class="badge bg-red">Inactivo</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ route('adminProducts.toggle', ['id' => $suggested_product->store_product_id]) }}">Activar</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li><a href="{{ route('adminProducts.toggle', ['id' => $suggested_product->store_product_id]) }}">Desactivar</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{ route('adminProducts.editSuggestedProduct', ['id' => $suggested_product->store_product_id, 'promo' => $suggested_product->promo]) }}">Editar</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{ route('adminProducts.deleteSuggestedProducts', ['id'
                                                => $suggested_product->store_product_id]) }}" onclick="return confirm
                                                ('¿Estas seguro que deseas eliminar el producto sugerido?')">Eliminar</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr><td colspan="10" align="center">No se encontraron productos.</td></tr>
                            @endif
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-xs-3">
                                <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($suggested_products) }} productos sugeridos</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    localStorage.removeItem('shelves_suggested_products');
</script>
@stop