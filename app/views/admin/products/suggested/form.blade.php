@extends('admin.layout')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminProducts.getSuggestedProducts') }}">
            <button type="button" class="btn btn-primary">Salir a Productos Sugeridos</button>
        </a>
    </span>
    </section>
    <section class="content">
        @if(Session::has('message'))
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Hecho!</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="add-product-suggested-form" action="{{ route('adminProducts.saveSuggestedProduct') }}" method="POST">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="city_id">Ciudad</label>
                                                <select name="city_id" id="city_id" class="form-control" @if(isset($suggested_products)) disabled="disabled" @endif>
                                                    <option value="">-Selecciona-</option>
                                                    @foreach($cities as $city)
                                                        <option value="{{ $city->id }}" @if(isset($suggested_product->shelf->store->city)) @if($city->id == $suggested_product->shelf->store->city->id) selected="selected" @endif @endif>{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group store_container">
                                                <label for="store_id">Tienda</label>
                                                <select name="store_id" id="store_id" class="form-control" @if(isset($suggested_products)) disabled="disabled" @endif>
                                                    <option value="">-Selecciona-</option>
                                                    @if(isset($stores))
                                                        @foreach ($stores as $store)
                                                            <option value="{{ $store->id }}" @if(isset($suggested_product->shelf->store)) @if($store->id == $suggested_product->shelf->store->id) selected="selected" @endif @endif>{{ $store->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group store_product_id">
                                                <label for="store_product_id">Producto relacionado</label>
                                                <select name="store_product_id" id="store_product_id" class="form-control" @if(isset($suggested_products)) disabled="disabled" @endif>
                                                    <option value="">-Selecciona-</option>
                                                    @if ( isset($products) )
                                                        @foreach ($products as $product)
                                                            <option value="{{ $product->id }}" @if(isset($suggested_product->store_product_id)) @if($product->id == $suggested_product->store_product_id) selected="selected" @endif @endif>{{ $product->name }} {{ $product->quantity }} {{ $product->unit }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="promo">Promoción</label>
                                                <input type="text" name="promo" id="promo" placeholder="Nombre de la promoción" class="form-control" @if(isset($suggested_product->promo)) value="{{ $suggested_product->promo }}" disabled="disabled" @endif>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="priority">Prioridad</label>
                                                <input type="number" name="priority" id="priority" placeholder="Prioridad" class="form-control" @if(isset($suggested_product->priority)) value="{{ $suggested_product->priority }}" @endif>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="status">Estado</label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="">-Selecciona-</option>
                                                    <option value="1" @if(isset($suggested_product->status)) @if($suggested_product->status) selected="selected" @endif @endif>Activo</option>
                                                    <option value="0" @if(isset($suggested_product->status)) @if(!$suggested_product->status) selected="selected" @endif @endif>Inactivo</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="start_date">Fecha Inicial</label>
                                                <input type="text" name="start_date" id="start_date" placeholder="yyyy/mm/dd" class="form-control" @if(isset($suggested_product->start_date)) value="{{ $suggested_product->start_date }}" @endif>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="end_date">Fecha Final</label>
                                                <input type="text" name="end_date" id="end_date" placeholder="yyyy/mm/dd" class="form-control" @if(isset($suggested_product->end_date)) value="{{ $suggested_product->end_date }}" @endif>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <label for="department_id">Departamento</label>
                                            <select class="form-control" name="department_id" id="department_id" required="required">
                                                @if(!empty($departments))
                                                    <option value="" selected="selected">-Selecciona-</option>
                                                    @foreach($departments as $department)
                                                        <option value="{{ $department->id }}" @if(isset($suggested_product->shelf->department)) @if($department->id == $suggested_product->shelf->department->id) selected="selected" @endif @endif>{{ $department->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <fieldset>
                                        <div class="row">
                                            <div class="form-group col-xs-4 shelves_id unseen">
                                                <label for="shelves_id">Pasillo</label>
                                                <select class="form-control" name="shelves_id" id="shelves_id" required="required" multiple>
                                                    @if(isset($suggested_products))
                                                        @foreach($shelves as $shelf)
                                                            <option value="{{ $shelf->id }}">{{ $shelf->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-4 shelves_id unseen">
                                                <button style="margin-top: 18%" class="btn btn-primary" type="button" id="add_shelves">Agregar pasillos al producto</button>
                                                <div style="margin-top: 2%" class="paging-loading unseen"><img src="{{asset_url() }}/img/loading.gif" /></div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                @if(isset($suggested_products))
                                                    <input type="hidden" name="update" value="true">
                                                    <input type="hidden" name="city_id" value="{{ $suggested_product->shelf->store->city->id }}">
                                                    <input type="hidden" name="store_id" value="{{ $suggested_product->shelf->store->id }}">
                                                    <input type="hidden" name="store_product_id" value="{{ $suggested_product->store_product_id }}">
                                                    <input type="hidden" name="promo" value="{{ $suggested_product->promo }}">
                                                @endif
                                                <button class="btn btn-primary" type="submit">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        Pasillos para aplicar producto
                    </h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead align="center">
                                <tr>
                                    <th width="50%">Departamento</th>
                                    <th width="50%">Pasillo</th>
                                </tr>
                                </thead>
                                <tbody class="tbody">
                                    <tr><td colspan="2" align="center">&nbsp;</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (isset($suggested_products))
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        Pasillos en los que se aplicó el producto
                    </h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead align="center">
                                <tr>
                                    <th width="45%">Departamento</th>
                                    <th width="45%">Pasillo</th>
                                    <th width="10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($suggested_products as $sp)
                                        <tr>
                                            <td>{{ $sp->shelf->department->name }}</td>
                                            <td>{{ $sp->shelf->name }}</td>
                                            <td>
                                                @if($suggested_products->count() > 1)<a href="{{ route('adminProducts.deleteSuggestedProductApplyedToShelf', ['id' => $sp->id]) }}" onclick="return confirm('¿Seguro que deseas eliminar este pasillo para el producto?');" class="btn btn-danger"><i class="fa fa-trash-o"></i> Quitar</a>@endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
</section>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script type="text/javascript">
        var BasicSuggestedProduct = (function() {
            'use strict';

            function BasicSuggestedProduct(args) {
                // enforces new
                if (!(this instanceof BasicSuggestedProduct)) {
                    return new BasicSuggestedProduct(args);
                }
                // constructor body
                localStorage.removeItem('shelves_suggested_products');
                this.city_id = null;
                this.store_id = null;
                this.department_id = null;
                this.shelves_suggested_products = [];
                this.get_stores_url = "{{ route('adminProducts.getStoresByCityAjax') }}";
                this.get_products_url = "{{ route('adminProducts.getProductsByStoreAjax') }}";
                this.get_departments_url = "{{ route('adminProducts.getDepartmentsByStoreAjax') }}";
                this.get_shelves_url = "{{ route('adminProducts.getShelvesByDepartmentAjax') }}";
                this.add_shelves_suggestion_products_url = "{{ route('adminProducts.addShelvesSuggestedProductsAjax') }}";
                this.bindActions();
            }

            BasicSuggestedProduct.prototype.bindActions = function() {
                var self = this;
                $('body').on('change', '#city_id', function(event) {
                    $('#store_id').prop('selectedIndex', 0).trigger('change');
                    self.city_id = $(this).val();
                    if ( self.city_id ) {
                        self.getStores();
                    }
                });
                $('body').on('change', '#store_id', function(event) {
                    $('#store_product_id').prop('selectedIndex', 0).trigger('change');
                    self.store_id = $(this).val();
                    if ( self.store_id ) {
                        self.getProducts();
                        self.getDepartmets();
                    }
                });
                $('body').on('change', '#department_id', function(event) {
                    self.shelves_id = $('#shelves_id');
                    self.department_id = $(this).val();
                    if ( self.department_id ) {
                        self.getShelves(function () {
                            $('#shelves_id').trigger('change');
                        });
                    } else {
                        $('#shelves_id').empty();
                        $('.shelves_id').addClass('unseen');
                    }
                });

                $('body').on('click', '#add_shelves', function(event) {
                    event.preventDefault();
                    self.shelves_id = $('#shelves_id').val();
                    self.department_id = $('#department_id').val();
                    var value = self.shelves_id || [];
                    if(value.length) {
                        if(self.shelves_suggested_products.length) {
                            if (_.every(self.shelves_suggested_products, ['department_id', self.department_id])) {
                                var index = _.findIndex(self.shelves_suggested_products, function(shelve) {return shelve.department_id === self.department_id;});
                                self.shelves_suggested_products[index]['shelves'] = self.shelves_id;
                            } else {
                                var index = _.findIndex(self.shelves_suggested_products, function(shelve) {return shelve.department_id === self.department_id;});
                                if (index === -1) {
                                    self.shelves_suggested_products.push({department_id: self.department_id, shelves: self.shelves_id});
                                } else {
                                    self.shelves_suggested_products[index].shelves = self.shelves_id;
                                }
                            }
                        } else {
                            self.shelves_suggested_products = [];
                            self.shelves_suggested_products.push({department_id: self.department_id, shelves: self.shelves_id});
                        }
                    }
                    localStorage.setItem('shelves_suggested_products', JSON.stringify(self.shelves_suggested_products));
                    self.addShelves();
                });
            };

            BasicSuggestedProduct.prototype.getStores = function() {
                var store_id = $('#store_id');
                store_id.attr('disabled', 'disabled');
                $.ajax({
                    url: this.get_stores_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data: {city_id: this.city_id}
                })
                .done(function(data) {
                    store_id.empty();
                    store_id.append('<option value="" selected="selected">-Selecciona-</option>');
                    $.each(data, function(key, value) {
                        store_id.append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    store_id.removeAttr('disabled');
                })
                .fail(function(err) {
                    alert('Ocurrio un problema al cargar las tiendas por la ciudad seleccionada');
                    store_id.removeAttr('disabled');
                });
            };

            BasicSuggestedProduct.prototype.getProducts = function() {
                var store_products = $('#store_product_id');
                store_products.attr('disabled', 'disabled');
                $.ajax({
                    url: this.get_products_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data: {store_id: this.store_id}
                })
                .done(function(data) {
                    store_products.empty();
                    store_products.append('<option value="" selected="selected">-Selecciona-</option>');
                    $.each(data.result.products, function(key, value) {
                        store_products.append('<option value="'+value.id+'">' + value.name + ' ' + value.quantity + ' ' + value.unit + '</option>');
                    });
                    store_products.removeAttr('disabled');
                })
                .fail(function(err) {
                    alert('Ocurrio un problema al cargar los productos por la tienda seleccionada');
                    store_products.removeAttr('disabled');
                });
            };

            BasicSuggestedProduct.prototype.getDepartmets = function() {
                var departments= $('#department_id');
                departments.attr('disabled', 'disabled');
                $.ajax({
                    url: this.get_departments_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data: {store_id: this.store_id}
                })
                .done(function(data) {
                    departments.empty();
                    departments.append('<option value="" selected="selected">-Selecciona-</option>');
                    $.each(data.result.departments, function(key, value) {
                        departments.append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    departments.removeAttr('disabled');
                })
                .fail(function(err) {
                    alert('Ocurrio un problema al cargar los productos por la tienda seleccionada');
                    departments.removeAttr('disabled');
                });
            };

            BasicSuggestedProduct.prototype.getShelves = function (callback) {
                var shelves = $('#shelves_id');
                shelves.attr('disabled', 'disabled');
                shelves.empty();
                $('.shelves_id').removeClass('unseen');
                shelves.append('<option value="">Cargando opciones de pasillos...</option>');
                $.ajax({
                    url: this.get_shelves_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data: {department_id: this.department_id}
                })
                .done(function(data) {
                    shelves.empty();
                    $.each(data.result.shelves, function(key, value) {
                        shelves.append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    shelves.removeAttr('disabled');
                    if (callback instanceof Function) {
                        callback();
                    }
                })
                .fail(function(err) {
                    shelves.removeAttr('disabled');
                    alert('Ocurrio un problema al cargar los pasillos para el departamento seleccionado');
                    shelves.empty();
                    shelves.append('<option value="">hubo probleas cargando las opciones de Pasillos</option>');
                });
            };

            BasicSuggestedProduct.prototype.addShelves = function() {
                var shelves = JSON.parse(localStorage.getItem('shelves_suggested_products'));
                $('.paging-loading').removeClass('unseen');
                $('#add_shelves').addClass('disabled');
                var table_shelves = $('.table .tbody');
                table_shelves.empty();
                table_shelves.append('<tr><td colspan="2">Cargando pasillos...</td></tr>');
                $.ajax({
                    url: this.add_shelves_suggestion_products_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data: {shelves_suggested_products: shelves}
                })
                .done(function(data) {
                    table_shelves.empty();
                    $.each(data.result.shelves, function(key, value) {
                        $.each(value.items, function(k, v) {
                            table_shelves.append('<tr><td>'+value.department+'</td><td>'+v+'</td></tr>');
                        });
                    });
                    $('#add_shelves').removeClass('disabled');
                    $('.paging-loading').addClass('unseen');
                })
                .fail(function(err) {
                    alert('Ocurrio un problema al cargar los pasillos para el departamento seleccionado');
                    table_shelves.empty();
                    table_shelves.append('<tr><td colspan="2">Hubo problemas al cargar los pasillos...</td></tr>');
                });
            };
            return BasicSuggestedProduct;
        }());

        $(document).ready(function() {
            var basic_suggested_product = new BasicSuggestedProduct;

            $('#start_date, #end_date').datetimepicker({
                format: 'YYYY/MM/DD'
            });

            $('#add-product-suggested-form').validate({
                rules: {
                    city_id: 'required',
                    promo: 'required',
                    store_product_id: 'required',
                    priority: 'required',
                    status: 'required',
                    store_id: 'required',
                    department_id: 'required',
                    shelves_id: 'required',
                    start_date: {
                        required: true,
                        date: true
                    },
                    end_date: {
                        required: true,
                        date: true
                    }
                }
            });
        });
    </script>
@stop
@endsection