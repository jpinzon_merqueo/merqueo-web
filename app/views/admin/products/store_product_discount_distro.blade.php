<div class="product-distribution" style="margin-bottom: 20px">
    @if (!empty($parent))
        <div class="row form-group">
            <div class="col-md-12" style="text-align: center">
                Precio del descuento en este producto (max: {{ $store_product_group->storeProductGroup->price * $store_product_group->quantity }}).
            </div>
        </div>
    @endif
    <div class="row form-group">
        @if (!empty($parent))
            <div class="col-xs-2">
                <img src="{{ $product->image_small_url ?: $product->image_medium_url }}" alt="">
            </div>
            <div class="col-xs-{{ !empty($parent) ? 6 : 10 }}">
                {{ $product->fullName }}
            </div>
        @endif

        @if (!empty($parent))
            <div class="col-xs-4">
                <input type="text"
                       quantity="{{ $store_product_group->quantity }}"
                       individual-price="{{ $store_product_group->storeProductGroup->price }}"
                       name="discount_amount[{{ empty($index) ? 0 : $index }}]"
                       class="form-control product_discount"
                       value="{{ $store_product->discount_amount ?: 0 }}"
                       placeholder="Ingresa el descuento en este producto">
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
            <b>Distribución</b>
        </div>
        <div class="col-xs-4">
            <b>Descuento</b>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-xs-4"><b>Mercadeo</b></div>
        <div class="col-xs-4 percentage-input">
            <input type="number"
                   class="form-control merqueo_discount"
                   name="merqueo_discount[{{ empty($index) ? 0 : $index }}]"
                   value="{{ empty($store_product->merqueo_discount) ? 0 : $store_product->merqueo_discount }}"
                    @if (!$permission) disabled @endif >
        </div>
        <div class="col-xs-4">
            <input type="text"
                   class="form-control merqueo_total"
                   name="merqueo_total[]"
                   disabled>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-xs-4"><b>Proveedor</b></div>
        <div class="col-xs-4 percentage-input">
            <input type="number"
                   class="form-control provider_discount"
                   name="provider_discount[{{ empty($index) ? 0 : $index }}]"
                   value="{{ empty($store_product->provider_discount) ? 0 : $store_product->provider_discount }}">
        </div>
        <div class="col-xs-4">
            <input type="text"
                   class="form-control provider_total"
                   name="provider_total[]"
                   disabled>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-xs-4"><b>Comercial</b></div>
        <div class="col-xs-4 percentage-input">
            <input type="number"
                   class="form-control seller_discount"
                   name="seller_discount[{{ empty($index) ? 0 : $index }}]"
                   value="{{ empty($store_product->seller_discount) ? 0 : $store_product->seller_discount }}">
        </div>

        <div class="col-xs-4">
            <input type="number"
                   class="form-control seller_total"
                   name="seller_total[]"
                   disabled>
        </div>
    </div>
    <div class="row form-group distribution-alert" style="display: none; color: red">
        <div class="col-xs-4"><b>Diferencia</b></div>
        <div class="col-xs-4 alert-value text-center">0</div>
    </div>
</div>