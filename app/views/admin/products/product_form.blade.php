@if ( !Request::ajax() )
	@extends('admin.layout')

@section('content')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
	<style type="text/css">
		.search-result{
			max-height: 300px;
			overflow: auto;
		}
	</style>
	<section class="content-header">
		<h1>
			{{ $title }} - {{ $sub_title }}
			<small>Control panel</small>
		</h1>
		<span class="breadcrumb" style="top:0">
		    <a href="{{ route('adminProducts.index') }}">
		    	<button type="button" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Regresar a lista</button>
		    </a>
		</span>
	</section>

	<section class="content">

		@if(Session::has('error'))
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Alerta!</b> {{ Session::get('error') }}
			</div>
		@endif

		@if(Session::has('success'))
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Hecho!</b> {{ Session::get('success') }}
			</div>
		@endif

		<input type="hidden" name="id" value="{{ $product ? $product->id : '' }}">
		@if ( $product )
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#home" data-toggle="tab">Inicio</a>
				</li>
				<li>
					<a href="#group" data-toggle="tab">Tipo de producto</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="home">

					@include('admin.products.product_form_only', ['product' => $product])

				</div>
				<div class="tab-pane" id="group">
					<div class="row">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-header">
									<h4 class="box-title">
										Información de producto
									</h4>
								</div>
								<div class="box-body">
									<form role="form" method="POST" id='main-form' action="{{ route('adminProducts.save') }}" enctype="multipart/form-data">
										<div class="row form-group">
											<div class="col-xs-3">
												<label title="Tipo de producto" for="type">Tipo de producto</label>
												<select class="form-control" id="type" name="type">
													<option value="Simple" {{ $product && ($product->type == 'Simple') ? "selected" : ''}}>Simple</option>
													<option value="Agrupado" {{ $product && ($product->type == 'Agrupado') ? "selected" : ''}}>Agrupado</option>
													<option value="Proveedor" {{ $product && ($product->type == 'Proveedor') ? "selected" : ''}}>Proveedor</option>
												</select>
											</div>
										</div>
										<div class="row provider_type_product_container" @if(!empty($product->type) && $product->type != 'Proveedor') style="display: none;" @endif>
											<div class="col-xs-3">
												<label title="Que tipo de inventario se maneja" for="provider_type_product">Tipo de inventario</label>
												<select name="provider_type_product" id="provider_type_product" class="form-control">
													<option value="Normal" {{ !empty($product->provider_type_product) && $product->provider_type_product == 'Normal' ? 'selected' : '' }}>Normal</option>
													<option value="Asociado" {{ !empty($product->provider_type_product) && $product->provider_type_product == 'Asociado' ? 'selected' : '' }}>Asociado</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-1">
												<input type="hidden" name="id" value="{{ $product ? $product->id : '' }}">
												<label title="Tipo de producto" for="type" style="color: #FFF; display: block;">|</label>
												<button type="submit" class="btn btn-success">Guardar</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row product-group @if ( $product->type == 'Simple' ) unseen @endif">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-header">
									<h4 class="box-title">
										Buscar productos para el grupo
									</h4>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-xs-12">
											<div class="row error-search">
												<div class="col-xs-12">
													<div class="alert alert-danger unseen"></div>
													<div class="alert alert-success unseen"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12">
													<table width="100%" class="modal-product-request-table">
														<tbody>
														<tr>
															<td align="right"><label>Buscar:</label>&nbsp;</td>
															<td>
																<input type="text" placeholder="Nombre, referencia, PLU" name="s" id="s" class="modal-search form-control" style="border: 1px solid rgb(204, 204, 204);">
															</td>
															<td colspan="2" align="left">
																<button type="button" id="btn-search-form" class="btn btn-primary" data-store_id="{{ $product->store_id }}">Buscar</button>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td colspan="2"><span class="error" style="display: none;">Campo requerido</span></td>
														</tr>
														</tbody>
													</table>
													<div align="center" class="modal-products-request-loading" style="display: none;"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 search-result unseen">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row product-group @if ( $product->type == 'Simple' ) unseen @endif">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-header">
									<h4 class="box-title">
										Productos
									</h4>
									<div class="row error-table">
										<div class="col-xs-12">
											<div class="alert alert-danger unseen"></div>
											<div class="alert alert-success unseen"></div>
										</div>
									</div>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-xs-12 related-products">
											<table class="table table-bordered table-striped">
												<thead>
												<tr>
													<th>Imagen</th>
													<th>Referencia</th>
													<th>Nombre</th>
													<th>Unidad de medida</th>
													<th>Precio</th>
													<th>Stock actual</th>
													<th>Cantidad</th>
													<th>Accion</th>
												</tr>
												</thead>
												<tbody class="tbody">
												@if (isset($products))
													@if (count($products))
														@foreach ($products as $product)
															<tr>
																<td><img src="{{$product->image_small_url}}" class="img-responsive"></td>
																<td>{{$product->reference}}</td>
																<td>{{$product->name}}</td>
																<td>{{$product->quantity}} {{$product->unit}}</td>
																<td>
																	@if ($product->special_price) <p style="text-decoration: line-through;">${{number_format($product->price)}}</p> <p>${{number_format($product->special_price)}}</p> @else ${{number_format($product->price)}} @endif
																</td>
																<td>{{$product->current_stock}}</td>
																<td>
																	<input type="number" name="product_quantity" class="product_cant form-control" value="0">
																</td>
																<td>
																	<div class="btn-group">
																		<a class="btn btn-xs btn-danger btn-add-product" href="javascript:;" data-current_stock="{{$product->current_stock}}" data-product_id="{{$product->id}}" data-order_id="{{$order_id}}" data-store_id="{{$store_id}}">
																			<span class="glyphicon glyphicon-plus"></span>
																		</a>
																	</div>
																</td>
															</tr>
														@endforeach
													@else
														<tr><td colspan="14" align="center">No hay productos asignados.</td></tr>
													@endif
												@else
													<tr><td colspan="14" align="center">No hay productos asignados.</td></tr>
												@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@else
			@include('admin.products.product_form_only', ['product' => $product])
			@if ( $product )
				<div class="row">
					<div class="col-xs-3 col-xs-offset-9 text-right">
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			@endif
		@endif
	</section>

	<script type="text/javascript">
        var Images = (function() {
            'use strict';
            function Images(args) {
                // enforces new
                if (!(this instanceof Images)) return new Images(args);
                this.index_item = 0;
                this.url = "{{ route('adminProducts.deleteProductImageAjax') }}";
                this.bindActions();
            }

            Images.prototype.bindActions = function(args) {
                var self = this;
                $('body').on('click', '#btn-images', function(event) {
                    self.openModal();
                    event.preventDefault();
                });

                $('body').on('click', '#btn-add-image', function(event) {
                    self.addImageItem();
                    event.preventDefault();
                });
                $('body').on('click', '.remove-image-item', function(event) {
                    var index = (event.currentTarget.id).slice(-1);
                    var product_index_node = $('#products_index');
                    var item = $('#image-item-'+index).find('img[name="images['+index+']"]');
                    if (typeof item[0] === 'undefined') {
                        $('#image-item-'+index + ', #remove-image-item-'+index + ', .remove-image-item-'+index).remove();
                        var product_index = product_index_node.val() === 0 ? 0 : parseInt(product_index_node.val()) - 1;
                        if(product_index !== 0) {
                            product_index--;
                            product_index_node.val(product_index + 1);
                        } else {
                            product_index_node.val(0);
                        }
                    } else {
                        var confirm = window.confirm('¿Estas seguro que deseas eliminar la imagen del producto?');
                        if (confirm) self.removeImageItem(event.currentTarget.id);
                    }
                    event.preventDefault();
                });
            };

            Images.prototype.openModal = function(args) {
                $('#modal-images').modal('show');
            };

            Images.prototype.addImageItem = function(args) {
                var product_index = $('#products_index').val() === 0 ? 0 : parseInt($('#products_index').val()) - 1;
                product_index++;
                $('#items').append(
                    '<div class="col-xs-10" id="image-item-'+product_index+'" style="margin-bottom: 10px; margin-top:' +
                    ' 10px;">' +
                    '	<input type="file" class="form-control" name="images[]">' +
                    '</div>' +
                    '<div class="col-xs-2 remove-image-item-'+product_index +'" style="margin-bottom: 10px; ' +
                    'margin-top: 10px;">'+
                    '	<a href="#" class="btn btn-danger remove-image-item" ' +
                    'id="remove-image-item-'+product_index+'">' +
                    '		<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>' +
                    '	</a>'+
                    '</div>'
                );
                $('#products_index').val(product_index + 1);
            };

            Images.prototype.removeImageItem = function(args) {
                var index = args.slice(-1);
                var remove_item = $('#remove-image-item-'+index);
                var item = $('#image-item-'+index+' img[name="images['+index+']"]');
                var id = item.attr('data-id');
                var input = $('#image-item-'+index+' input[name="images['+index+']"]');
                remove_item.attr('disabled', 'disabled');
                item.addClass('unseen');
                input.parent().addClass('unseen');
                item.after('<div align="center" class="paging-loading"><img src="{{ asset_url() }}/img/loading.gif" /></div>');
                input.parent().after('<div align="center"><b>Eliminando Imagen...</b></div>');
                $.ajax({
                    url: this.url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {product_image_id: id}
                })
                    .done(function(data) {
                        $('#' + args + ', .remove-image-item-' + index + ', #image-item-'+ index).remove();
                        $('.paging-loading').addClass('unseen');
                        alert('Imagen de producto eliminada con éxito');
                    })
                    .fail(function(data) {
                        alert("Error al eliminar la imagen del producto.");
                        $('.paging-loading').addClass('unseen');
                        item.removeClass('unseen');
                        input.removeClass('unseen');
                    });
            };
            return Images;
        }());

        var subcategory = (function() {
            'use strict';

            function subcategory() {
                // enforces new
                if (!(this instanceof subcategory)) {
                    return new subcategory();
                }
                this.url = '{{ route('adminProducts.addSubcategoryAjax') }}';
                this.category_id = null;
                this.new_subcategory = null;
                this.bindInteracts();
            }
            subcategory.prototype.bindInteracts = function() {
                var self = this;
                $('body').on('click', '.subcategory.label-success', function(event) {
                    event.preventDefault();
                    if ( $('#category').val() != '' ) {
                        var label_danger = $(this).next('.label-danger');
                        $(this).fadeOut('fast', function () {
                            label_danger.fadeIn('fast');
                        });
                        var option_2 = $(this).parent().parent().find('#subcategory-container .option-2');
                        $(this).parent().parent().find('#subcategory-container .option-1').slideUp('fast', function (){
                            option_2.slideDown('fast');
                        });
                    }else{
                        alert('Primero debes seleccionar una categoría.');
                    }
                });
                $('body').on('click', '.subcategory.label-danger', function(event) {
                    event.preventDefault();
                    var label_success = $(this).prev('.label-success');
                    $(this).fadeOut('fast', function () {
                        label_success.fadeIn('fast');
                    });
                    var option_1 = $(this).parent().parent().find('#subcategory-container .option-1');
                    $(this).parent().parent().find('#subcategory-container .option-2').slideUp('fast', function () {
                        option_1.slideDown('fast');
                    });
                    $('#new-subcategory').val('');
                });
                $('body').on('click', '#subcategory-container .btn-default', function(event) {
                    event.preventDefault();
                    if ( $('#category').val() != '' && $('#new-subcategory').val() != '' ) {
                        self.category_id = $('#category').val();
                        self.new_subcategory = $('#new-subcategory').val();
                        self.ajax();
                    }else{
                        alert('La categoría y el nombre de la nueva subcategoría no pueden ser vacíos.');
                    }
                });
            };
            subcategory.prototype.ajax = function() {
                return $.ajax({
                    url: this.url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        category_id: this.category_id,
                        new_subcategory: this.new_subcategory,
                    },
                    context: this
                })
                    .done(function(data) {
                        this.ajax_response(data);
                    })
                    .fail(function(data) {
                        console.log("error al guardar la nueva subcategoría.");
                    });
            };
            subcategory.prototype.ajax_response = function(data) {
                if ( data.status ) {
                    $('#subcategory').html(data.html);
                    alert('Se ha agregado la subcategoría.');
                    $('.subcategory.label-danger').trigger('click');
                }else{
                    alert(data.message);
                    $('.subcategory.label-danger').trigger('click');
                }
            };
            return subcategory;
        }());

        var subbrand = (function() {
            'use strict';

            function subbrand() {
                // enforces new
                if (!(this instanceof subbrand)) {
                    return new subbrand();
                }
                this.url = '{{ route('adminProducts.addSubbrandAjax') }}';
                this.brand_id = null;
                this.new_subcategory = null;
                this.bindInteracts();
            }
            subbrand.prototype.bindInteracts = function() {
                var self = this;
                $('body').on('click', '.subbrand.label-success', function(event) {
                    event.preventDefault();
                    if ( $('#brand').val() != '' ) {
                        var label_danger = $(this).next('.label-danger');
                        $(this).fadeOut('fast', function () {
                            label_danger.fadeIn('fast');
                        });
                        var option_2 = $(this).parent().parent().find('#subbrand-container .option-2');
                        $(this).parent().parent().find('#subbrand-container .option-1').slideUp('fast', function (){
                            option_2.slideDown('fast');
                        });
                    }else{
                        alert('Primero debes seleccionar una marca.');
                    }
                });
                $('body').on('click', '.subbrand.label-danger', function(event) {
                    event.preventDefault();
                    var label_success = $(this).prev('.label-success');
                    $(this).fadeOut('fast', function () {
                        label_success.fadeIn('fast');
                    });
                    var option_1 = $(this).parent().parent().find('#subbrand-container .option-1');
                    $(this).parent().parent().find('#subbrand-container .option-2').slideUp('fast', function () {
                        option_1.slideDown('fast');
                    });
                    $('#new-subbrand').val('');
                });
                $('body').on('click', '#subbrand-container .btn-default', function(event) {
                    event.preventDefault();
                    if ( $('#brand').val() != '' && $('#new-subbrand').val() != '' ) {
                        self.brand_id = $('#brand').val();
                        self.new_subbrand = $('#new-subbrand').val();
                        self.ajax();
                    }else{
                        alert('La marca y el nombre de la nueva submarca no pueden ser vacíos.');
                    }
                });
            };
            subbrand.prototype.ajax = function() {
                return $.ajax({
                    url: this.url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        brand_id: this.brand_id,
                        new_subbrand: this.new_subbrand,
                    },
                    context: this
                })
                    .done(function(data) {
                        this.ajax_response(data);
                    })
                    .fail(function(data) {
                        console.log("error al guardar la nueva submarca.");
                    });
            };
            subbrand.prototype.ajax_response = function(data) {
                if ( data.status ) {
                    $('#subbrand').html(data.html);
                    alert('Se ha agregado la submarca.');
                    $('.subbrand.label-danger').trigger('click');
                }else{
                    alert(data.message);
                    $('.subbrand.label-danger').trigger('click');
                }
            };
            return subbrand;
        }());

        var searcher = (function() {
            'use strict';

            function searcher(args) {
                // enforces new
                if (!(this instanceof searcher)) {
                    return new searcher(args);
                }
                this.url = '{{ route('adminProducts.searchProductsAjax') }}';
                this.product_id = {{ ( $product ? $product->id : 'null' ) }};
                this.term = null;
                this.bindActions();
            }

            searcher.prototype.bindActions = function() {
                var self = this;
                $('body').on('click', '#btn-search-form', function(event) {
                    event.preventDefault();
                    var term = $('#s').val();
                    self.term = term;
                    self.ajax();
                });
                $('#btn-search-form').trigger('click');
            };

            searcher.prototype.ajax = function () {
                $('.modal-products-request-loading').show();
                $.ajax({
                    url: this.url,
                    type: 'GET',
                    dataType: 'html',
                    data: {
                        term: this.term,
                        product_id: this.product_id
                    },
                })
                    .done(function(data) {
                        $('.search-result').html(data).removeClass('unseen');
                    })
                    .fail(function(data) {
                        console.log("error al cargar los productos.");
                    })
                    .always(function(data) {
                        $('.modal-products-request-loading').hide();
                    });

            }
            return searcher;
        }());

        var productGroup = (function() {
            'use strict';

            function productGroup() {
                // enforces new
                if (!(this instanceof productGroup)) {
                    return new productGroup();
                }
                this.url = '{{ route('adminProducts.addProductGroupAjax') }}';
                this.url_products = '{{ route('adminProducts.getProductGroupAjax') }}';
                this.url_products_remove = '{{ route('adminProducts.removeProductGroupAjax') }}';
                this.product_id = {{ ( $product ? $product->id : 'null' ) }};
                this.product_group_id = null;
                this.quantity = null;
                this.price = null;
                this.type = null;
                this.bindInteracts();
                this.ajax_get_products();
                // constructor body
            }

            productGroup.prototype.bindInteracts = function() {
                var self = this;
                $('body').on('click', '.btn-add-product', function(event) {
                    event.preventDefault();
                    var quantity = $(this).parents('tr').find('.product_quantity').val();
                    var price = $(this).parents('tr').find('.product_price_group').val();
                    var product_group_id = $(this).data('product_id');
                    var type = $('#main-form #type').val();

                    if ( quantity == '' || quantity == 0 ) {
                        $('.error-search .alert-danger').html('Debes ingresar un valor en el campo "cantidad".').fadeIn().delay(2500).fadeOut();
                        return;
                    }
                    if ( price == '' ) {
                        $('.error-search .alert-danger').html('Debes ingresar un valor en el campo "precio".').fadeIn().delay(2500).fadeOut();
                        return;
                    }
                    if ( confirm('¿Seguro que quieres agregar este producto?') ) {
                        self.product_group_id = product_group_id;
                        self.quantity = quantity;
                        self.price = price;
                        self.type = type;
                        self.ajax( $(this).parents('tr') );
                    }
                });

                $('body').on('click', '.btn-remove-product', function(event) {
                    event.preventDefault();
                    var product_group_id = $(this).data('product_id');
                    var type = $('#main-form #type').val();
                    if ( confirm('¿Seguro que quieres borrar este producto del grupo?') ) {
                        self.product_group_id = product_group_id;
                        self.type = type;
                        self.ajax_remove_products();
                    }
                });
            };

            productGroup.prototype.ajax = function(element){
                $('.modal-products-request-loading').show();
                $.ajax({
                    url: this.url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        product_id: this.product_id,
                        product_group_id: this.product_group_id,
                        quantity: this.quantity,
                        price: this.price,
                        type: this.type
                    },
                    context: this
                })
                    .done(function(data) {
                        if ( data.status ) {
                            this.ajax_get_products();
                            element.remove();
                            $('.error-search .alert-success').html(data.message).fadeIn().delay(2500).fadeOut();
                            $('#store_id').trigger('change');
                            return true;
                        }
                        $('.error-search .alert-error').html(data.message).fadeIn().delay(2500).fadeOut();
                        return true;
                    })
                    .fail(function() {
                        console.log("error al agregar un producto.");
                    })
                    .always(function() {
                        $('.modal-products-request-loading').hide();
                    });
            };

            productGroup.prototype.ajax_get_products = function() {
                $('.modal-products-request-loading').show();
                $.ajax({
                    url: this.url_products,
                    type: 'GET',
                    dataType: 'html',
                    data: {
                        product_id: this.product_id
                    },
                })
                    .done(function(data) {
                        $('.related-products').html(data);
                    })
                    .fail(function(data) {
                        console.log("error al cargar los productos relacionados.");
                    })
                    .always(function(data) {
                        $('.modal-products-request-loading').hide();
                    });
            };

            productGroup.prototype.ajax_remove_products = function() {
                $('.modal-products-request-loading').show();
                $.ajax({
                    url: this.url_products_remove,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        product_id: this.product_id,
                        product_group_id: this.product_group_id,
                        type: this.type
                    },
                    context: this
                })
                    .done(function(data) {
                        if ( data.status ) {
                            $('.error-table .alert-success').html(data.message).fadeIn().delay(2500).fadeOut();
                            this.ajax_get_products();
                            $("#store_id option:first").attr('selected','selected');
                            $("#store_id").trigger('change');
                            return;
                        }
                        $('.error-table .alert-danger').html(data.message).fadeIn().delay(5000).fadeOut();
                    })
                    .fail(function() {
                        console.log("error al borrar un producto.");
                    })
                    .always(function() {
                        $('.modal-products-request-loading').hide();
                    });
            };
            return productGroup;
        }());

        var category = (function() {
            'use strict';

            function category() {
                // enforces new
                if (!(this instanceof category)) {
                    return new category();
                }
                this.url_get_subcategory = '{{ route('adminProducts.getSubcategoriesAjax') }}';
                this.url_save_new_category = '{{ route('adminProducts.addCategoryAjax') }}';
                this.new_category = null;
                this.category_id = null;
                this.bindActions();
            }

            category.prototype.bindActions = function() {
                let self = this;
                $('#category').change(function(event) {
                    self.category_id = $(this).val();
                    self.ajax_get_subcategory();
                });
                $('body').on('click', '.category.label-success', function(event) {
                    event.preventDefault();
                    var label_danger = $(this).next('.label-danger');
                    $(this).fadeOut('fast', function () {
                        label_danger.fadeIn('fast');
                    });
                    var option_2 = $(this).parent().parent().find('#category-container .option-2');
                    $(this).parent().parent().find('#category-container .option-1').slideUp('fast', function (){
                        option_2.slideDown('fast');
                    });
                });
                $('body').on('click', '.category.label-danger', function(event) {
                    event.preventDefault();
                    var label_success = $(this).prev('.label-success');
                    $(this).fadeOut('fast', function () {
                        label_success.fadeIn('fast');
                    });
                    var option_1 = $(this).parent().parent().find('#category-container .option-1');
                    $(this).parent().parent().find('#category-container .option-2').slideUp('fast', function () {
                        option_1.slideDown('fast');
                    });
                    $('#new-category').val('');
                });
                $('body').on('click', '#category-container .btn-default', function(event) {
                    event.preventDefault();
                    if ( $('#new-category').val() != '' ) {
                        self.new_category = $('#new-category').val();
                        self.ajax_add_new_category();
                    }else{
                        alert('El nombre de la nueva categoría no puede ser vacíos.');
                    }
                });
            };

            category.prototype.ajax_get_subcategory = function() {
                $.ajax({
                    url: this.url_get_subcategory,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        category_id: this.category_id
                    },
                })
                    .done(function(data) {
                        var $html = '<option value=""></option>';
                        $('#subcategory').empty();
                        for (var i = data.length - 1; i >= 0; i--) {
                            data[i];
                            $html += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
                        }
                        $('#subcategory').append($html);
                    })
                    .fail(function() {
                        console.log("Ocurrio un error al cargar subcategorías.");
                    });
            };

            category.prototype.ajax_add_new_category = function() {
                return $.ajax({
                    url: this.url_save_new_category,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        new_category: this.new_category
                    },
                    context: this
                })
                    .done(function(data) {
                        this.ajax_response(data);
                    })
                    .fail(function(data) {
                        console.log("error al guardar la nueva categoría.");
                    });
            };

            category.prototype.ajax_response = function(data) {
                if ( data.status ) {
                    $('#category').html(data.html);
                    alert('Se ha agregado la categoría.');
                    $('.category.label-danger').trigger('click');
                }else{
                    alert(data.message);
                    $('.category.label-danger').trigger('click');
                }
            };

            return category;
        }());

        var brand = (function() {
            'use strict';

            function brand() {
                // enforces new
                if (!(this instanceof brand)) {
                    return new brand();
                }
                this.url_get_subbrand = '{{ route('adminProducts.getSubbrandsAjax') }}';
                this.url_save_new_brand = '{{ route('adminProducts.addBrandAjax') }}';
                this.new_brand = null;
                this.brand_id = null;
                this.bindActions();
                // constructor body
            }

            brand.prototype.bindActions = function() {
                let self = this;
                $('#brand').change(function(event) {
                    self.brand_id = $(this).val();
                    self.ajax_get_subbrands();
                });
                $('body').on('click', '.brand.label-success', function(event) {
                    event.preventDefault();
                    var label_danger = $(this).next('.label-danger');
                    $(this).fadeOut('fast', function () {
                        label_danger.fadeIn('fast');
                    });
                    var option_2 = $(this).parent().parent().find('#brand-container .option-2');
                    $(this).parent().parent().find('#brand-container .option-1').slideUp('fast', function (){
                        option_2.slideDown('fast');
                    });
                });
                $('body').on('click', '.brand.label-danger', function(event) {
                    event.preventDefault();
                    var label_success = $(this).prev('.label-success');
                    $(this).fadeOut('fast', function () {
                        label_success.fadeIn('fast');
                    });
                    var option_1 = $(this).parent().parent().find('#brand-container .option-1');
                    $(this).parent().parent().find('#brand-container .option-2').slideUp('fast', function () {
                        option_1.slideDown('fast');
                    });
                    $('#new-brand').val('');
                });
                $('body').on('click', '#brand-container .btn-default', function(event) {
                    event.preventDefault();
                    if ( $('#new-brand').val() != '' ) {
                        self.new_brand = $('#new-brand').val();
                        self.ajax_add_new_brand();
                    }else{
                        alert('El nombre de la nueva marca no puede ser vacíos.');
                    }
                });
            };

            brand.prototype.ajax_get_subbrands = function(first_argument) {
                $.ajax({
                    url: this.url_get_subbrand,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        brand_id: this.brand_id
                    },
                })
                    .done(function(data) {
                        var $html = '<option value=""></option>';
                        $('#subbrand').empty();
                        for (var i = data.length - 1; i >= 0; i--) {
                            data[i];
                            $html += '<option value="'+ data[i]['id'] +'">'+ data[i]['name'] +'</option>';
                        }
                        $('#subbrand').append($html);
                    })
                    .fail(function() {
                        console.log("Ocurrió un error al cargar las submarcas.");
                    });
            };

            brand.prototype.ajax_add_new_brand = function() {
                return $.ajax({
                    url: this.url_save_new_brand,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        new_brand: this.new_brand
                    },
                    context: this
                })
                    .done(function(data) {
                        this.ajax_response(data);
                    })
                    .fail(function(data) {
                        console.log("error al guardar la nueva marca.");
                    });
            };

            brand.prototype.ajax_response = function(data) {
                if ( data.status ) {
                    $('#brand').html(data.html);
                    alert('Se ha agregado la marca.');
                    $('.brand.label-danger').trigger('click');
                }else{
                    alert(data.message);
                    $('.brand.label-danger').trigger('click');
                }
            };

            return brand;
        }());

        var productStore = (function() {
            'use strict';

            function productStore() {
                // enforces new
                if (!(this instanceof productStore)) {
                    return new productStore();
                }
                this.url_get_store_product = '{{ route('adminProducts.getStoreProductAjax') }}';
                this.store_id = null;
                this.product_id = {{ $product ? $product->id : 'null' }};
                this.bindActions();
                // constructor body
            }

			productStore.prototype.bindActions = function() {
				let self = this;
				$('#store_id').change(function(event) {
					$('.loading').show();
					$('#store_product_section').slideUp('fast', function () {
						$(this).empty();
					});
					self.store_id = $(this).val();
					if ( self.store_id != '' ) {
						self.ajax_get_store_product();
						$('#store_product_section').slideDown('fast', function () {
							$('html, body').animate({
							        scrollTop: $("#store_product_section").offset().top
							    }, 1000);
						});
					}else{
						$('.loading').hide();
					}
				});
				$('body').on('click', '.copy-basic-info', function (event) {
				    event.preventDefault();
				    let current_store_id = $('input[name="store_id"]').val();
				    let current_product_id = $('input[name="product_id"]').val();
                    $.ajax({
                        url: '{{ route('adminProducts.getStoreProductInfoCopyAjax') }}',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            store_id: current_store_id,
							product_id: current_product_id
						},
                    })
                        .done(function(data) {
                            if (typeof data.error != 'undefined') {
                                alert(data.error);
                                return
							}
							$('#department_id').val(data.departments.id);
                            $('#department_id').trigger('change');
                            setTimeout(function () {
                                $('#shelf_id').val(data.shelves.id);
                            }, 3000);
                            $('#sort_order').val(data.store_product.sort_order);
                            $('#handle_expiration_date').val(data.store_product.handle_expiration_date);
                            $('#is_best_price').val(data.store_product.is_best_price)
                        })
                        .fail(function() {
                            console.log("error");
                        })
                        .always(function() {
                        });

                });
				$('body').on('change', '#manage_stock', function(event) {
					event.preventDefault();
					if ( $(this).val() == 1 ) {
						$('.manage_stock_container').slideToggle();
					}else{
						$('.manage_stock_container').slideToggle();
					}
				});
			};

            productStore.prototype.ajax_get_store_product = function() {
                $.ajax({
                    url: this.url_get_store_product,
                    type: 'GET',
                    dataType: 'html',
                    data: {
                        store_id: this.store_id,
                        product_id: this.product_id
                    },
                })
                    .done(function(data) {
                        $('#store_product_section').html(data);
                        $('#delivery_discount_start_date').datetimepicker({
                            format: 'DD/MM/YYYY HH:mm:ss'
                        });
                        $('#special_price_starting_date').datetimepicker({
                            format: 'DD/MM/YYYY HH:mm:ss'
                        });
                        $('#special_price_expiration_date').datetimepicker({
                            format: 'DD/MM/YYYY HH:mm:ss'
                        });
                        $("#delivery_discount_start_date").on("dp.change", function (e) {
                            $('#delivery_discount_start_date').data("DateTimePicker").minDate(e.date);
                        });
                        $("#special_price_starting_date").on("dp.change", function (e) {
                            $('#special_price_expiration_date').data("DateTimePicker").minDate(e.date);
                        });
                        $("#special_price_expiration_date").on("dp.change", function (e) {
                            $('#special_price_starting_date').data("DateTimePicker").maxDate(e.date);
                        });
                    })
                    .fail(function(data) {
                        console.log("error");
                    })
                    .always(function() {
                        $('.loading').hide();
                    });
            };

            return productStore;
        }());

        var maker = (function() {
            'use strict';

            function maker(args) {
                // enforces new
                if (!(this instanceof maker)) {
                    return new maker(args);
                }
                this.url_save_new_maker = '{{ route('adminProducts.addMakerAjax') }}';
                this.new_maker = null;
                this.maker_id = null;
                this.bindActions();
                // constructor body
            }

            maker.prototype.bindActions = function(args) {
                let self = this;
                $('body').on('click', '.maker.label-success', function(event) {
                    event.preventDefault();
                    var label_danger = $(this).next('.label-danger');
                    $(this).fadeOut('fast', function () {
                        label_danger.fadeIn('fast');
                    });
                    var option_2 = $(this).parent().parent().find('#maker-container .option-2');
                    $(this).parent().parent().find('#maker-container .option-1').slideUp('fast', function (){
                        option_2.slideDown('fast');
                    });
                });
                $('body').on('click', '.maker.label-danger', function(event) {
                    event.preventDefault();
                    var label_success = $(this).prev('.label-success');
                    $(this).fadeOut('fast', function () {
                        label_success.fadeIn('fast');
                    });
                    var option_1 = $(this).parent().parent().find('#maker-container .option-1');
                    $(this).parent().parent().find('#maker-container .option-2').slideUp('fast', function () {
                        option_1.slideDown('fast');
                    });
                    $('#new-maker').val('');
                });
                $('body').on('click', '#maker-container .btn-default', function(event) {
                    event.preventDefault();
                    if ( $('#new-maker').val() != '' ) {
                        self.new_maker = $('#new-maker').val();
                        self.ajax_add_new_maker();
                    }else{
                        alert('El nombre de la nueva marca no puede ser vacíos.');
                    }
                });
            };

            maker.prototype.ajax_add_new_maker = function() {
                return $.ajax({
                    url: this.url_save_new_maker,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        new_maker: this.new_maker
                    },
                    context: this
                })
                    .done(function(data) {
                        this.ajax_response(data);
                    })
                    .fail(function(data) {
                        console.log("error al guardar la nueva marca.");
                    });
            };

            maker.prototype.ajax_response = function(data) {
                if ( data.status ) {
                    $('#maker').html(data.html);
                    alert('Se ha agregado la marca.');
                    $('.maker.label-danger').trigger('click');
                }else{
                    alert(data.message);
                    $('.maker.label-danger').trigger('click');
                }
            };

            return maker;
        }());

        $(window).load(function() {
            $('#store_id').trigger('change');
        });

        $(document).ready(function() {

            function currentSellOffDistribution(parentContainer) {
                var marketingValue = parseInt($(parentContainer).find('.merqueo_discount').val()) || 0;
                var providerValue = parseInt($(parentContainer).find('.provider_discount').val()) || 0;
                var sellerValue = parseInt($(parentContainer).find('.seller_discount').val()) || 0;

                return marketingValue + providerValue + sellerValue;
            }

            function isSellOffDistributionValid(parentContainer) {
                var hasSpecialPrice = parseInt($('#has_special_price').find(':selected').val());
                if (!hasSpecialPrice) {
                    return true;
                }

                return currentSellOffDistribution(parentContainer) === 100;
            }

            function isTotalDiscountValid() {
                var total = 0;
                var wrongDiscount = false;
                var hasSpecialPrice = parseInt($('#has_special_price').find(':selected').val());
                var discount = parseInt($('#price').val()) - parseInt($('#special_price').val());
                if (!hasSpecialPrice) {
                    return true;
                }
                $('.product_discount').each(function(index, element) {
                    var individualPrice = this.getAttribute("individual-price");
                    var quantity = this.getAttribute("quantity");
                    var discountValue = parseInt(element.value) || 0;
                    if(discountValue > (quantity*individualPrice)){
                        wrongDiscount = true;
                        return;
                    }
                    total += discountValue || 0;
                });

                return total === discount && !wrongDiscount;
            }

            $.validator.addMethod('validDiscountDistribution', function (value, element) {
                var currentDiscount = $(element).closest('.product-distribution').find('.product_discount').val();
                if (parseInt(currentDiscount) === 0) {
                    return true;
                }
                return isSellOffDistributionValid($(element).closest('.product-distribution'));
            }, 'La distribución de los descuentos no es valida.');

            $.validator.addMethod('validDiscountByProduct', function () {
                return isTotalDiscountValid();
            }, 'La distribución del descuento en los productos no es correcta.');

            $.validator.addMethod('validSpecialPrice', function (value) {
                return parseInt(value) < parseInt($('#price').val());
            }, 'El precio especial debe ser menor a precio de venta del producto.');

            $('#store_id').trigger('change');
            $('body').on('click', '#store_product_form .btn-primary', function(event) {
                $("#store_product_form").validate();
            });

            $('body').on('change', '#department_id', function(event) {
                var department = $( "#department_id" ).val();

                $.ajax({ url: "{{ route('adminProducts.getShelvesAjax') }}",
                    data: {department_id: department},
                    type: 'get',
                    async: false,
                    success:
                        function(msg) {
                            $('#shelf_id').find('option').remove().end();
                            $.each(msg,function(key,value){
                                $('#shelf_id').append('<option value="'+value.id+'">'
                                    +value.name+'</option>');
                            });
                        }
                });
            });

            $('body').on('change', '#has_quantity_special_price_stock', function() {
                var value = $(this).val();
                if (parseInt(value)) {
                    $('.has_quantity_special_depend').show();
                } else {
                    $('.has_quantity_special_depend').hide();
                    $('#quantity_special_price_stock').val(0);
                }
            });

            $('body').on('change', '#provider_type', function() {
                var groupAlliedStore = $('.tienda_aliada');
                var groupProvider = $('.proveedor');
                switch ($(this).find(':selected').val()){
                    case 'Marketplace':
                        groupAlliedStore.removeClass('hide');
                        blank(groupProvider);
                        break;
                    case 'Merqueo':
                        blank(groupAlliedStore);
                        groupProvider.removeClass('hide');
                        break;
                    default:
                        blank(groupAlliedStore);
                        blank(groupProvider);
                }

                function blank(element) {
                    element.find('select').val('');
                    element.addClass('hide');
                }
            });

            $('#main-form #type').change(function (event) {
                if ( $(this).val() != 'Simple' ) {
                    $('.product-group').removeClass('unseen');
                }else{
                    $('.product-group').addClass('unseen');
                }
            });
            $('body').on('click', '#is_sell_off', function(event) {
                event.preventDefault();
                if ( $(this).val() == 1 ) {
                    $('.sell_off_container').removeClass('unseen');
                }else{
                    $('.sell_off_container').addClass('unseen');
                }
            });

            var images = new Images;

            var sub_category = new subcategory;

            var sub_brand = new subbrand;

            var search_btn = new searcher;

            var product_group = new productGroup;

            var category_obj = new category;

            var brand_obj = new brand;

            var maker_obj = new maker;

            var product_store = new productStore;
        });

        $(document).ready(function (event) {
            $('body').on('change', '#type', function (event) {
                var val = $(this).val()
                if (val == 'Proveedor') {
                    $('.provider_type_product_container').slideDown('fast');
                } else {
                    $('.provider_type_product_container').slideUp('fast');
                    $('#provider_type_product').val('Normal');
                }
            });
        });
	</script>

	<script type="text/javascript">
        var mainFormValidator = $("#main-form").validate({
            rules: {
                reference: 'required',
                name: 'required',
                price: {
                    required: true,
                    number: true
                },
                quantity_special_price:{
                    required: function(){
                        if ($('#special_price').val() != '' && $('#special_price').val() != 0){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                quantity_special_price_stock: {
                    required: function() {
                        return parseInt($('#quantity_special_price_stock').val());
                    },
                    number: true
                },
                type_use_special_price:{
                    required: function(){
                        if ($('#special_price').val() != '' && $('#special_price').val() != 0){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                first_order_special_price:{
                    required: function(){
                        if ($('#special_price').val() != '' && $('#special_price').val() != 0){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                quantity: {
                    number: true
                },
                store_id: 'required',
                department_id: 'required',
                shelf_id: 'required',
                brand_id: 'required',
                sort_order:{
                    required: true,
                    number: true
                },
                cost:{
                    number: true
                },
                iva:{
                    number: true
                },
                base_price:{
                    number: true
                },
                negociated_price:{
                    number: true
                },
                consumption_tax:{
                    number: true
                },
                accounting_account:{
                    required: function(){
                        if ($('#store').val() != 0 && $('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                accounting_line:{
                    required: function(){
                        if ($('#store').val() != 0 && $('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                accounting_group:{
                    required: function(){
                        if ($('#store').val() != 0 && $('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                accounting_code:{
                    required: function(){
                        if ($('#store').val() != 0 && $('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                minimum_stock:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                ideal_stock:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    },
                    number: true
                },
                type_stock:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                base_cost:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                cost:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                negociated_cost:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                storage:{
                    required: function(){
                        return true;
                        /*if ($('#store').val() != '' && $('#store').val() == 63){
						}else{
							return false;
						}*/
                    }
                },
                storage_position:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                storage_shelf:{
                    required: function(){
                        if ($('#store').val() != '' && $('#store').val() == 63){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            }
        });
	</script>

	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>

@stop
@else
	@if ( isset($subcategories) )
@section('subcategories')
	<option value=""></option>
	@foreach ($subcategories as $subcategory)
		<option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
	@endforeach
@stop
@endif
@if ( isset($categories) )
@section('categories')
	<option value=""></option>
	@foreach ($categories as $category)
		<option value="{{ $category->id }}">{{ $category->name }}</option>
	@endforeach
@stop
@endif
@if ( isset($brands) )
@section('brands')
	<option value=""></option>
	@foreach ($brands as $brand)
		<option value="{{ $brand->id }}">{{ $brand->name }}</option>
	@endforeach
@stop
@endif
@if ( isset($makers) )
@section('makers')
	<option value=""></option>
	@foreach ($makers as $maker)
		<option value="{{ $maker->id }}">{{ $maker->name }}</option>
	@endforeach
@stop
@endif
@if ( isset($products) )
@section('products')
	<table class="table table-bordered table-striped">
		<thead>
		<tr>
			<th>Imagen</th>
			<th>Referencia</th>
			<th>Nombre</th>
			<th>Unidad de medida</th>
			<th>Precio</th>
			<th>Cantidad</th>
			<th>Agregar</th>
		</tr>
		</thead>
		<tbody class="tbody">
		@if (count($products))
			@foreach ($products as $product)
				<tr>
					<td><img src="{{$product->image_small_url}}" class="img-responsive" width="60"></td>
					<td>{{$product->reference}}</td>
					<td>{{$product->name}}</td>
					<td>{{$product->quantity}} {{$product->unit}}</td>
					<td>
						@foreach ($product->prices as $price)
							@if ( $price['store_id'] == 63 )
								<div class="row">
									<div class="col-xs-6">
										<strong>Bogotá</strong>
									</div>
									<div class="col-xs-6">
										@if ($price['special_price']) <p style="text-decoration: line-through;">${{number_format($price['price'])}}</p> <p>${{number_format($price['special_price'])}}</p> @else ${{number_format($price['price'])}} @endif
									</div>
								</div>
							@endif
							@if ( $price['store_id'] == 64 )
								<hr>
								<div class="row">
									<div class="col-xs-6">
										<strong>Medellín</strong>
									</div>
									<div class="col-xs-6">
										@if ($price['special_price']) <p style="text-decoration: line-through;">${{number_format($price['price'])}}</p> <p>${{number_format($price['special_price'])}}</p> @else ${{number_format($price['price'])}} @endif
									</div>
								</div>
							@endif
						@endforeach
					</td>
					<td>
						<input type="number" name="product_quantity" class="product_quantity form-control" value="1">
					</td>
					<td>
						<div class="btn-group">
							<a class="btn btn-xs btn-default btn-add-product" href="javascript:;" data-current_stock="{{$product->current_stock}}" data-product_id="{{$product->id}}">
								<span class="glyphicon glyphicon-plus"></span>
							</a>
						</div>
					</td>
				</tr>
			@endforeach
		@else
			<tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
		@endif
		</tbody>
	</table>
@stop
@endif
@if (isset($product_group))
@section('product_group')
	<table class="table table-bordered table-striped">
		<thead>
		<tr>
			<th>Imagen</th>
			<th>Referencia</th>
			<th>Nombre</th>
			<th>Unidad de medida</th>
			<th>Cantidad</th>
			<th>Acción</th>
		</tr>
		</thead>
		<tbody class="tbody">
		@if (count($product_group))
			@foreach ($product_group as $product)
				<tr>
					<td><img src="{{$product->image_small_url}}" class="img-responsive" width="60"></td>
					<td>{{$product->reference}}</td>
					<td>{{$product->name}}</td>
					<td>{{$product->quantity}} {{$product->unit}}</td>
					<td>
						<input type="number" name="product_quantity" class="product_quantity form-control" value="{{$product->product_group_quantity}}" readonly="">
					</td>
					<td>
						<div class="btn-group">
							<a class="btn btn-xs btn-danger btn-remove-product" href="javascript:;" data-current_stock="{{$product->current_stock}}" data-product_id="{{$product->id}}">
								<span class="glyphicon glyphicon-minus"></span>
							</a>
						</div>
					</td>
				</tr>
			@endforeach
		@else
			<tr><td colspan="14" align="center">Productos no encontrados.</td></tr>
		@endif
		</tbody>
	</table>
@stop
@endif
@if ( isset($store_products) )
@section('store_products')
	<hr>
	<form id="store_product_form" role="form" method="post" action="{{ route('adminProducts.saveStoreProduct') }}" enctype="multipart/form-data">

		@if ( isset($product) && ($product->type == 'Agrupado' || $product->type == 'Proveedor') )
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#basic_info" data-toggle="tab">Información básica</a>
				</li>
				<li>
					<a href="#store_group" data-toggle="tab">Grupo de productos</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="basic_info">
					<br>
					<hr>
					@endif

					@if ( $store_product )
						<input type="hidden" name="id" value="{{ $store_product->id }}">
					@else
						<div class="alert alert-danger alert-dismissable">
							<b>Alerta!</b> Este producto aún no ha sido agregado a esta tienda
							<button type="button" class="btn btn-primary copy-basic-info">Copiar información básica</button>
						</div>
						<input type="hidden" name="store_id" value="{{ $store_id }}">
						<input type="hidden" name="product_id" value="{{ $product_id }}">
					@endif
					<div class="row">
						<div class="col-xs-12">
							<h3>
								Información básica
							</h3>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-3">
							<label for="department_id">Departamento</label>
							<select name="department_id" id="department_id" class="form-control" required="">
								<option value="">Selecciona</option>
								@foreach ($departments as $department)
									<option value="{{ $department->id }}" @if ( $department->selected ) selected="selected" @endif >{{ $department->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-3">
							<label for="shelf_id">Pasillo</label>
							<select name="shelf_id" id="shelf_id" class="form-control" required="">
								<option value="">Selecciona</option>
								@if ( isset($shelves) )
									@foreach ($shelves as $shelf)
										<option value="{{ $shelf->id }}" @if ( $shelf->selected ) selected="selected" @endif>{{ $shelf->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
						<div class="col-xs-3">
							<label for="sort_order">Posición en página web</label>
							<input type="text" id="sort_order" name="sort_order" class="form-control" value="@if ( $store_product ){{ $store_product->sort_order }}@endif" placeholder="Ingresa orden" required="">
						</div>
						<div class="col-xs-3">
							<label for="">¿Maneja fecha de vencimiento?</label>
							<select class="form-control" id="handle_expiration_date" name="handle_expiration_date">
								<option value="1" @if ( $store_product && $store_product->handle_expiration_date == 1 ) selected="" @endif>Si</option>
								<option value="0" @if ( $store_product && $store_product->handle_expiration_date == 0 ) selected="" @endif>No</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-3">
							<label for="is_best_price">¿Es mejor precio garantizado?</label>
							<select name="is_best_price" id="is_best_price" class="form-control">
								<option value="0" @if ( $store_product && $store_product->is_best_price == 0 ) selected="" @endif>No</option>
								<option value="1" @if ( $store_product && $store_product->is_best_price == 1 ) selected="" @endif>Sí</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label for="is_best_price">Deeplink</label>
							<input type="text" id="deeplink" name="deeplink" class="form-control" value="@if ( $store_product ){{ $store_product->deeplink }}@endif" placeholder="Url dinámica" readonly="readonly">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<h3>
								Proveedor
							</h3>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<label title="Tipo de proveedor" for="provider_type">Tipo de proveedor</label>
							<select name="provider_type" id="provider_type" class="form-control required" required>
								<option value="">Selecciona</option>
								<option {{ !empty($store_product->provider_id) ? 'selected' : '' }}>Merqueo</option>
								<option {{ !empty($store_product->allied_store_id) ? 'selected' : '' }}>Marketplace</option>
							</select>
						</div>
						<div class="col-xs-3 proveedor {{ empty($store_product->provider_id) ? 'hide' : '' }}">
							<label title="Proveedor del producto" for="provider">Proveedor</label>
							<select name="provider_id" id="provider_id" class="form-control required" required>
								<option value=""></option>
								@foreach ($providers as $provider)
									<option value="{{ $provider->id }}" @if ( $provider->selected ) selected="" @endif >{{ $provider->id }} - {{ $provider->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-3 tienda_aliada {{ empty($store_product->allied_store_id) ? 'hide' : '' }}">
							<label title="Tienda aliada donde venden el producto en cada ciudad" for="allied_store_id">Tienda aliada</label>
							<select name="allied_store_id" id="allied_store_id" class="form-control">
								<option value=""></option>
								@if ( count($allied_stores) )
									@foreach ($allied_stores as $allied_store)
										<option value="{{ $allied_store->id }}" @if ($store_product && $store_product->allied_store_id == $allied_store->id) selected="selected" @endif >{{ $allied_store->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Código PLU" for="provider_plu">PLU</label>
							<input type="text" name="provider_plu" id="provider_plu" class="form-control" value="@if ( $store_product ){{ $store_product->provider_plu }}@endif">
						</div>
						<div class="col-xs-3">
							<label title="Tipo de embalaje que ofrece el proveedor" for="">Tipo de embalaje</label>
							<select name="provider_pack_type" id="provider_pack_type" class="form-control" required="required">
								<option value=""></option>
								<option value="Caja" @if ( $store_product && $store_product->provider_pack_type == 'Caja' ) selected="" @endif>Caja</option>
								<option value="Estiba" @if ( $store_product && $store_product->provider_pack_type == 'Estiba' ) selected="" @endif>Estiba</option>
								<option value="Unidad" @if ( $store_product && $store_product->provider_pack_type == 'Unidad' ) selected="" @endif>Unidad</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label title="Cantidad de unidades del producto entregados por el proveedor" for="provider_pack_quantity">Embalaje</label>
							<input type="text" name="provider_pack_quantity" id="provider_pack_quantity" class="form-control" value="@if ( $store_product ){{ $store_product->provider_pack_quantity }}@endif">
						</div>
						<div class="col-xs-3">
							<label title="Costo del producto" for="cost">Costo con IVA</label>
							<input type="number"
								   name="cost"
								   id="cost"
								   class="form-control"
								   {{ $product->type === 'Simple' ? '' : 'disabled' }}
								   value="{{ empty($store_product->cost) ? 0 : $store_product->cost }}">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-3">
							<label title="Costo base de almacenamiento del producto" for="base_cost">Costo base (sin impuestos)</label>
							<input type="text" name="base_cost" id="base_cost" class="form-control" disabled>
						</div>
						<div class="col-xs-3">
							<label title="Costo promedio del producto" for="cost">Costo promedio</label>
							<input type="text" name="cost_average" id="cost_average" class="form-control" disabled="disabled" value="@if ( $store_product ){{ $store_product->cost_average }}@endif">
						</div>
						<div class="@if ($store_product && $store_product->guarantee_policies) col-xs-4 @else col-xs-6 @endif">
							<label>Políticas de garantía</label>
							<input type="file" class="form-control" name="guarantee_policies">
						</div>
						@if ($store_product && $store_product->guarantee_policies)
							<div class="col-xs-2">
								<br><br>
								<a href="{{ $store_product->guarantee_policies }}" target="_blank">Ver archivo</a>
							</div>
						@endif
					</div>

					<hr>
					<div class="row">
						<div class="col-xs-12">
							<h3>
								Precio
							</h3>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-3">
							<label for="price">Precio con IVA</label>
							<input type="text"
								   id="price"
								   name="price"
								   class="form-control"
								   value="{{ empty($store_product->price) ? 0 : $store_product->price }}"
								   placeholder="Ingresa el precio"
								   {{ $product->type === 'Simple' ? '' : 'disabled' }}
								   required>
						</div>
						<div class="col-xs-3">
							<label for="iva">I.V.A. (%)</label>
							<input type="text" id="iva" name="iva" class="form-control" value="@if ( $store_product ){{ $store_product->iva }}@endif" placeholder="Ingresa el IVA">
						</div>
						<div class="col-xs-3">
							<label for="consumption_tax">Impuesto al consumo (%)</label>
							<input type="text" id="consumption_tax" name="consumption_tax" class="form-control" value="@if ( $store_product ){{ $store_product->consumption_tax }}@endif" placeholder="Ingresa el impuesto al consumo">
						</div>
						<div class="col-xs-3">
							<label for="base_price">Precio base (Sin impuestos)</label>
							<input type="text" id="base_price" class="form-control" disabled>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-3">
							<label for="margin_percentage">% Margen</label>
							<input type="text" id="margin_percentage" class="form-control" disabled>
						</div>
					</div>
					<hr>

					@if (!empty($product) && (!empty($store_product) || $product->type === 'Simple'))
						<div class="row">
							<div class="col-xs-12">
								<h3>
									Promoción
								</h3>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-3">
								{{ Form::label('has_special_price', 'El producto cuenta con promoción', ['class' => 'control-form']) }}
								{{
									Form::fillSelectWithoutKeys(
										'has_special_price',
										['No', 'Sí'],
										empty($store_product->special_price) ? 0 : 1,
										['class' => 'form-control'],
										true
									)
								}}
							</div>
							<div class="col-xs-3 form-group">
								<label for="delivery_discount_amount">Descuento en domicilio</label>
								<input type="number" class="form-control" name="delivery_discount_amount" id="delivery_discount_amount" placeholder="Ingresa valor de descuento en domicilio" value="@if ( $store_product ){{ $store_product->delivery_discount_amount }}@endif">
							</div>
							<div class="col-xs-3 form-group">
								<label for="delivery_maximum_amount">Descuento total máximo en domicilio</label>
								<input type="number" class="form-control" name="delivery_maximum_amount" id="delivery_maximum_amount" placeholder="Ingresa valor total máximo en domicilio" value="@if ( $store_product ){{ $store_product->delivery_maximum_amount }}@endif">
							</div>
							<div class="col-xs-3 form-group">
								<label for="delivery_discount_start_date">Fecha inicio de descuento en domicilio</label>
								<input type="text" class="form-control" name="delivery_discount_start_date" id="delivery_discount_start_date" placeholder="20/02/2019 00:00:00" value="{{ empty($store_product->delivery_discount_start_date) ? '' : $store_product->delivery_discount_start_date }}">
							</div>
						</div>
						<div class="has_special_price_depend" style="display: {{ empty($store_product->special_price) ? 'none' : 'block' }}">
							<div class="row form-group has_special_price_depend">
								<div class="col-xs-3">
									<label for="special_price">Precio especial</label>
									<input type="text" id="special_price" name="special_price" class="form-control" value="@if ( $store_product ){{ $store_product->special_price }}@endif" placeholder="Ingresa precio especial">
								</div>
								<div class="col-xs-3">
									<label for="special_price">Descuento</label>
									<input type="text" id="discount" class="form-control" disabled>
								</div>
								@if ($admin_permissions['permission1'])
									<div class="col-xs-3">
										<label for="price">Precio a mostrar en página</label>
										<input type="number" id="public_price" name="public_price" class="form-control"
											   value="{{ empty($store_product->public_price) ? '' : $store_product->public_price}}"
											   placeholder="Ingresa el precio">
									</div>
								@endif
								<div class="col-xs-3">
									<label for="">Fecha de inicio</label>
									<input type="text"
										   id="special_price_starting_date"
										   name="special_price_starting_date"
										   class="form-control"
										   placeholder="20/02/2019 00:00:00"
										   value="{{ empty($store_product->special_price_starting_date) ? '' : $store_product->special_price_starting_date }}">
								</div>
							</div>
							<div class="row">
								<div class="col-xs-3 form-group">
									<label for="">Fecha de expiración</label>
									<input type="text"
										   id="special_price_expiration_date"
										   name="special_price_expiration_date"
										   class="form-control"
										   placeholder="20/02/2019 00:00:00"
										   value="{{ empty($store_product->special_price_expiration_date) ? '' : $store_product->special_price_expiration_date }}">
								</div>
								<div class="col-xs-3 form-group">
									<label for="quantity_special_price">Cantidad por pedido</label>
									<input type="text" id="quantity_special_price" name="quantity_special_price" class="form-control" value="@if ( $store_product ){{ $store_product->quantity_special_price }}@endif" placeholder="Ingresa cantidad por pedido con precio especial">
								</div>
								<div class="col-xs-3 form-group">
									<label for="">Tipo de uso para cliente</label>
									<select name="first_order_special_price" id="first_order_special_price" class="form-control">
										<option value="">Selecciona</option>
										<option value="1" @if ( $store_product && $store_product->first_order_special_price == 1 ) selected="" @endif>Solo primera compra</option>
										<option value="0" @if ( $store_product && $store_product->first_order_special_price == 0 ) selected="" @endif>Normal</option>
									</select>
								</div>
								<div class="col-xs-3 form-group">
									{{ Form::label('has_quantity_special_price_stock', 'Cuenta con un limite de unidades en promoción', ['class' => 'control-form']) }}
									{{
										Form::fillSelectWithoutKeys(
											'has_quantity_special_price_stock',
											['No', 'Sí'],
											!empty($store_product->has_quantity_special_price_stock),
											['class' => 'form-control']
										)
									}}
								</div>
							</div>
							<div class="row form-group">
								<div class="col-xs-3 has_quantity_special_depend" style="{{ !empty($store_product->has_quantity_special_price_stock) ? '' : 'display:none' }}">
									<label for="quantity_special_price_stock">
										Cantidad de unidades en descuento restantes en stock
									</label>
									<input type="number"
										   id="quantity_special_price_stock"
										   name="quantity_special_price_stock"
										   class="form-control"
										   value="{{ empty($store_product->quantity_special_price_stock) ? 0 : $store_product->quantity_special_price_stock }}"
										   placeholder="Ingresa la cantidad de unidades en descuento">
								</div>
							</div>

							{{--Inicio distribución de promoción--}}
							<div class="row">
								<div class="col-xs-12">
									<h4>
										Distribución de promoción
										<small>
											Diferencia: $ <span id="total-discount-amount" style="color: red"></span>
										</small>
									</h4>
								</div>
							</div>

							@if (!empty($store_product))
                                <?php $permission = $admin_permissions['permission1'] ?>
								@if ($product->type === 'Simple')
                                    <?php $index = 0; ?>
									@include('admin.products.store_product_discount_distro', compact('store_product', 'product', 'index', 'permission'))
								@else
                                    <?php $store_product->load('storeProductGroup') ?>
									@foreach ($store_product->storeProductGroup as $index => $store_product_group)
										<div class="col-md-6">
                                            <?php
                                            $parent = $product;
                                            $store_product_group->load('storeProductGroup.product');
                                            $product_group = $store_product_group->storeProductGroup->product;
                                            ?>
											@include('admin.products.store_product_discount_distro', ['store_product' => $store_product_group, 'product' => $product_group, 'parent' => $parent, 'index' => $index, 'permission' => $permission])
										</div>
									@endforeach
								@endif
							@endif
							{{--Fin distribución de promoción--}}

						</div>

						<script>
                            var storeProductForm = null;
                            $(document).ready(function() {
                                $.validator.addMethod('regx', function (value, element, regexpr) {
                                    if(regexpr.test(value)){
                                        return true;
                                    }
                                    return false;
                                }, 'No puedes escribir números negativos.');
                                $.validator.addMethod('vdate', function (value, element) {
                                    var inputVal = element.value.split(' ');
                                    var date = inputVal[0].split('/');
                                    var hour = inputVal[1].split(':');
                                    var dateNow = new Date();
                                    var dateInput = new Date(date[2], (parseInt(date[1]) - 1).toString(), date[0], hour[0], hour[1], hour[2]);
                                    if (dateNow.getTime() > dateInput.getTime()) {
                                        return false;
                                    }
                                    return true;
                                }, 'La fecha ingresada debe ser mayor a la fecha actual.');
                                $.validator.addMethod('higherToDicountAmount',function (value, element, discount_amount) {
                                    if (parseInt(value) < parseInt(discount_amount)) {
                                        return false;
                                    }
                                    return true;
                                }, 'El valor debe ser mayor o igual al de descuento en domicilio.');
                                storeProductForm = $("#store_product_form").validate({
                                    rules: {
                                        special_price: {
                                            required: function() {
                                                return parseInt($('#has_special_price').find(':selected').val(), 10);
                                            },
                                            number: true,
                                            validSpecialPrice: true
                                        },
                                        delivery_discount_amount:{
                                            regx: function(){
                                                if ($('#delivery_discount_amount').val() != 0) {
                                                    return /^\s*[a-zA-Z0-9,\s]+\s*$/;
                                                }
                                            }
                                        },
                                        delivery_maximum_amount:{
                                            required: function () {
                                                if ($('#delivery_discount_amount').val() != 0) {
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            },
                                            regx: function(){
                                                if ($('#delivery_discount_amount').val() != 0) {
                                                    return /^\s*[a-zA-Z0-9,\s]+\s*$/;
                                                }
                                            },
                                            higherToDicountAmount: function () {
                                                if ($('#delivery_discount_amount').val() != 0) {
                                                    return $('#delivery_discount_amount').val();
                                                }
                                            }
                                        },
                                        delivery_discount_start_date:{
                                            required: function () {
                                                if ($('#delivery_discount_amount').val() != 0) {
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            },
                                            vdate: true
                                        },
                                        <?php $total = $product->type === 'Simple' ? 1 : count($store_product->storeProductGroup); ?>
												@for($i = 0; $i < $total; $i++)
                                        'merqueo_discount[{{ $i }}]': {
                                            number: true,
                                            validDiscountDistribution: true
                                        },
                                        'provider_discount[{{ $i }}]': {
                                            number: true,
                                            validDiscountDistribution: true
                                        },
                                        'seller_discount[{{ $i }}]': {
                                            number: true,
                                            validDiscountDistribution: true
                                        },
                                        'discount_amount[{{ $i }}]': {
                                            number: true,
                                            validDiscountByProduct: true
                                        },
										@endfor
                                    }
                                });
                            });
						</script>

					@else
						<div class="row">
							<div class="alert alert-danger alert-dismissable">
								<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
								Almacene el producto en la tienda para configurar los datos de promoción.
							</div>
						</div>
					@endif

					<div class="row">
						<div class="col-xs-12">
							<h3>
								Almacenamiento <small>(Si aplica)</small>
							</h3>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-3">
							<label title="Almacenamiento del producto" for="storage">Almacenamiento</label>
							<select name="storage" id="storage" class="form-control required">
								<option value="">Selecciona</option>
								<option value="Seco" @if ( $store_product && $store_product->storage == 'Seco' ) selected="" @endif>Seco</option>
								<option value="Refrigerado" @if ( $store_product && $store_product->storage == 'Refrigerado' ) selected="" @endif>Refrigerado</option>
								<option value="Congelado" @if ( $store_product && $store_product->storage == 'Congelado' ) selected="" @endif>Congelado</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label title="Dias de vida util" for="useful_life_days">Días de vida util</label>
							<input type="text" name="useful_life_days" id="useful_life_days" class="form-control" value="@if ( $store_product ){{ $store_product->useful_life_days }}@endif" placeholder="Ingresa los días de vida util" maxlength="4">
						</div>
					</div>

					@if ( isset($product) && ($product->type == 'Agrupado' || $product->type == 'Proveedor') )
				</div>
				<div class="tab-pane" id="store_group">
					<br>
					<hr>
					@if ( isset($store_product_groups) || isset($store_product_group_errors) )
						<table class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Imagen</th>
								<th>Referencia</th>
								<th>Nombre</th>
								<th>Unidad de medida</th>
								<th>Cantidad</th>
								<th>Precio unidad</th>
								<th>Total</th>
							</tr>
							</thead>
							<tbody class="tbody">
							@if ( isset($store_product_groups) && count($store_product_groups))
								@foreach ($store_product_groups as $product)
									<tr>
										<td><img src="{{$product->image_small_url}}" class="img-responsive" width="60"></td>
										<td>{{$product->reference}}</td>
										<td>{{$product->name}}</td>
										<td>{{$product->quantity}} {{$product->unit}}</td>
										<td>
											@if ( empty( $product->store_product_group_quantity ) )
												{{ $product->group_quantity }}
												<input type="hidden" name="store_product_group_quantity[]" value="{{ $product->group_quantity }}">
											@else
												{{ $product->store_product_group_quantity }}
											@endif
										</td>
										<td>
											{{ currency_format($product->store_product_group_price) }}
										</td>
										<td>
											{{
                                                currency_format(
                                                    $product->store_product_group_price *
                                                    (empty($product->store_product_group_quantity)
                                                        ? $product->group_quantity
                                                        : $product->store_product_group_quantity
                                                    )
                                                )
                                            }}
											<input type="hidden" name="store_product_group_id[]" value="{{ $product->store_product_group_id }}">
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="14" align="center">
										<div class="alert alert-danger alert-dismissable">
											<b>Alert!</b> Los siguientes productos no están creados en esta tienda. No se pueden ejecutar cambios sin resolver este inconveniente.
										</div>
									</td>
								</tr>
								@foreach ($store_product_group_errors as $product)
									<tr>
										<td><img src="{{$product->image_small_url}}" class="img-responsive" width="60"></td>
										<td>{{$product->reference}}</td>
										<td>{{$product->name}}</td>
										<td>{{$product->quantity}} {{$product->unit}}</td>
										<td>
											@if ($product->special_price) <p style="text-decoration: line-through;">${{number_format($product->price)}}</p> <p>${{number_format($product->special_price)}}</p> @else ${{number_format($product->price)}} @endif
										</td>
										<td>
										</td>
									</tr>
								@endforeach
							@endif
							</tbody>
						</table>
					@endif
				</div>
			</div>
		@endif
		<hr>
		<div class="row">
			<div class="col-xs-3 col-xs-offset-9 text-right">
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<h3>
					Inventario
				</h3>
			</div>
		</div>

		<div class="row" id="store-product-warehouse-message"></div>

		@if (empty($store_product))

			<div class="row">
				<div class="alert alert-danger alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
					Almacene el producto en la tienda para configurar los datos del inventario.
				</div>
			</div>

		@else
			<div class="row form-group">
				<div class="col-xs-3">
					{{ Form::label('warehouse_id', 'Bodega', ['class' => 'control-form']) }}
					{{ Form::fillSelectWithKeys('warehouse_id', $warehouses, null, ['class' => 'form-control'], 'id', 'warehouse', true) }}
				</div>
			</div>

			<div class="stock-section-container"></div>
			<script>
                $(document).ready(function() {
                    $('#warehouse_id').on('change', function () {
                        var value = $(this).find(':selected').val();
                        var url = '{{ route('getStoreProductWarehouseForm', [$store_product->id, 'REPLACE']) }}';

                        if (!value) {
                            $('.stock-section-container').hide();
                            return;
                        }

                        $.ajax({url: url.replace('REPLACE', value)})
                            .done(function(response) {
                                $('.stock-section-container').show().html(response);
                            });
                    });
                });
			</script>
		@endif
		<script>
            $(document).ready(function() {
                var watchedPriceInputs = [
                    'input[name="price"]',
                    'input[name="iva"]',
                    '#cost',
                    '#special_price'
                ];
                var watchedDiscountInputs = [
                    '.merqueo_discount',
                    '.provider_discount',
                    '.seller_discount',
                    '.product_discount'
                ];

                watchedPriceInputs.forEach(function(identifier) {
                    $(identifier).on('change', function() {
                        calcPrice();
                        calcDiscountDifference();
                        calcBaseCostPrice();
                    });
                });

                watchedDiscountInputs.forEach(function(identifier) {
                    $(identifier).on('change', calcDiscountDifference);
                });

                $('#has_special_price').on('change', function() {
                    var value = parseInt($(this).find(':selected').val(), 10);
                    $('.has_special_price_depend')[value ? 'show' : 'hide']();
                });

                $('input[name="price"]').trigger('change');

                function calcPrice() {
                    var fullPrice = parseInt($('#price').val(), 10) || 0;
                    var taxes = (parseInt($('#iva').val(), 10) || 0) / 100;
                    var price = Math.round(fullPrice  / (1 + taxes));
                    var cost = parseInt($('#cost').val(), 10) || 0;
                    var specialPrice = parseInt($('#special_price').val(), 10) || fullPrice;
                    var margin = fullPrice ? (fullPrice - cost) / fullPrice : 0;
                    var discount = fullPrice - specialPrice;

                    $('#base_price').val(price);
                    $('#margin_percentage').val((margin * 100).toFixed(2));
                    $('#discount').val(discount);
                }

                function calcDiscountDifference() {
                    var has_errors = false;
                    var total_value = 0;
                    var cluster = $('.product-distribution');

                    cluster.find(".form-control:first").each(function(index, elem){
                        if(elem.value.trim() == ""){
                            elem.value = "0";
                        }
                    });

                    cluster.each(function(index, element) {
                        var $element = $(element);
                        var discountValue = parseInt($element.find('.product_discount').val() || $('#discount').val()) || 0;
                        var alert = $element.find('.distribution-alert');
                        var marketingValue = parseInt($element.find('.merqueo_discount').val()) || 0;
                        var providerValue = parseInt($element.find('.provider_discount').val()) || 0;
                        var sellerValue = parseInt($element.find('.seller_discount').val()) || 0;
                        var total = marketingValue + providerValue + sellerValue;
                        total_value += discountValue;

                        $element.find('.merqueo_total').val(marketingValue / 100 * discountValue);
                        $element.find('.provider_total').val(providerValue / 100 * discountValue);
                        $element.find('.seller_total').val(sellerValue / 100 * discountValue);

                        if (total !== 100 && discountValue) {
                            alert.show();
                            alert.find('.alert-value').text(100 - total);
                            has_errors = true;
                        } else {
                            alert.hide();
                        }
                    });

                    if (!has_errors && storeProductForm && storeProductForm.resetForm) {
                        storeProductForm.resetForm();
                    }

                    if(cluster.length > 1) {
                        var discount = $('#discount').val();
                        var difference = total_value - discount;
                        if (difference !== 0) {
                            $('#total-discount-amount').show();
                            $('#total-discount-amount').text(difference);
                        } else {
                            $('#total-discount-amount').hide();
                        }
                    }

                }

                function calcBaseCostPrice() {
                    var cost = parseInt($('#cost').val()) || 0;
                    var taxes = (parseInt($('#iva').val(), 10) || 0) / 100;

                    $('#base_cost').val(cost / (1 + taxes));
                }
            });
		</script>
	</form>
@endsection
@endif
@endif
