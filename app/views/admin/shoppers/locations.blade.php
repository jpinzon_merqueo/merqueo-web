@extends('admin.layout')

@section('content')

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminShoppers.index') }}">
            <button type="button" class="btn btn-primary"></i> Volver a Shoppers</button>
        </a>
    </span>
</section>

<section class="content">
    <div class="row">
        <div id="map" class="col-xs-12" style="height: 450px;"></div>
    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key={{ Config::get('app.google_api_key') }}&sensor=true"></script>
<script type="text/javascript">
    var map = null;
    var shoppers = {{ $shoppers_locations }};
    var markers = [];

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: { lat: {{ $city->latitude }}, lng: {{ $city->longitude }} }
    });
    loadShoppers();

    function loadShoppers()
    {
        //eliminar markers
        markers.forEach(function(marker) {
            marker.setMap(null);
        });

        markers = [];

        //agregar marker
        shoppers.forEach(function(shopper) {
            addMarker(shopper);
        });

        setTimeout(function() {
            $.getJSON('{{ route('adminShoppers.getLocations') }}', function(s) {
                s = (typeof s == 'string') ? JSON.parse(s) : s;
                shoppers = s;
                loadShoppers();
            })
        }, 30000)
    }

    function addMarker(shopper)
    {
        if (shopper.location.length != 2) return false;

        var marker = new google.maps.Marker({
            position: { lat: parseFloat(shopper.location[0]), lng: parseFloat(shopper.location[1]) },
            map: map,
            title: shopper.name,
        });

        var content = '<h4>Shopper</h4>\
            <p><b>Nombre:</b> ' + shopper.name + '</p>\
            <p><b>Última conexión:</b> ' + shopper.date + '</p>\
        ';

        var info = new google.maps.InfoWindow({
            content: content
        });

        marker.addListener('click', function() {
            info.open(map, marker);
        });

        markers.push(marker);
    }
</script>

@stop