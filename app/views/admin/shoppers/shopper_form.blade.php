@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div>
                <form role="form" method="post" id='main-form' action="{{ route('adminShoppers.save') }}" enctype="multipart/form-data">
                    <div class="box-body">
                        <input type="hidden" name="id" id="id" value="{{ $shopper ? $shopper->id : '' }}">
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Cédula</label>
                                <input type="number" class="form-control" name="identity_number" placeholder="Ingresa la Cédula" value="{{ $shopper ? $shopper->identity_number : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="first_name" placeholder="Ingresa el Nombre" value="{{ $shopper ? $shopper->first_name : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Apellido</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Ingresa el Apellido" value="{{ $shopper ? $shopper->last_name : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Celular</label>
                                <input type="text" class="form-control" name="phone" placeholder="Ingresa el Celular" value="{{ $shopper ? $shopper->phone : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Ingresa Email" value="{{ $shopper ? $shopper->email : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Ciudad</label>
                                <select class="form-control" id="city_id" name="city_id">
                                    <option></option>
                                	@foreach($cities as $city)
    				       			<option value="{{ $city->id }}" @if ($shopper && $shopper->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
    				       			@endforeach
                                </select>
                             </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Zonas</label>
                                <select class="form-control @if ( !$shopper->zonerel ) unseen @endif " id="zone_id" name="zone_id[]" multiple>
                                    @if ($shopper->zonerel)
                                        @foreach($zones as $zone)
                                            <option value="{{ $zone->id }}"
                                                @if($shopper->zonerel)
                                                    @foreach($shopper->zonerel as $zonerel)
                                                        @if ($zonerel['id'] == $zone->id)
                                                            selected="selected"
                                                        @endif
                                                    @endforeach
                                                @endif
                                            >
                                                {{$zone->city['city']}} - {{ $zone->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Puede recibir pedidos de:</label>
                                <select class="form-control" id="store_id" name="store_id[]" multiple>
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}" @if ( in_array($store->id, $stores_selected) ) selected="selected" @endif>{{ $store->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="photo">Foto</label>
                                <input type="file" class="form-control" name="photo">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                @if ( !empty($shopper->photo_url) )
                                    <img src="{{ $shopper->photo_url }}" alt="{{ $shopper->id }}" class="img-responsive">
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="profile">Perfil</label>
                                <select name="profile" id="profile" class="form-control">
                                    <option value="Shopper Moto" @if($shopper->profile == 'Shopper Moto') selected="selected" @endif>Shopper Moto</option>
                                    <option value="Shopper Carro" @if($shopper->profile == 'Shopper Carro') selected="selected" @endif>Shopper Carro</option>
                                    <option value="Shopper Cliente" @if($shopper->profile == 'Shopper Cliente') selected="selected" @endif>Shopper Cliente</option>
                                    <option value="Alistador Seco" @if($shopper->profile == 'Alistador Seco') selected="selected" @endif>Alistador Seco</option>
                                    <option value="Alistador Frío" @if($shopper->profile == 'Alistador Frío') selected="selected" @endif>Alistador Frío</option>
                                </select>
                             </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Clave</label>
                                <input type="password" class="form-control" name="password" placeholder="Ingresa la Clave" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="status">Estado</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1" @if ( isset($shopper) && $shopper->status === 1 ) selected=""@endif>Activo</option>
                                    <option value="0" @if ( isset($shopper) && $shopper->status === 0 ) selected=""@endif>Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$("#main-form").validate({
    rules: {
        identity_number: {
            required: true,
            number: true,
            minlength: 7
        },
        first_name: "required",
        last_name: "required",
        phone: "required",
        password: {
            required: function (element) {
                if ($("#id").val() != ''){
                    return false;
                }
                else{
                    return true;
                }
            }
        },
        email: {
            required: true,
            email: true
        },
        city_id: {
            required: true
        }
    }
});

$(document).ready(function() {
    $('#city_id').change(function () {
        $('#zone_id').empty().slideUp('fast');
        $('#store_id').empty().slideUp('fast');
        $.ajax({
            url: '{{ route('adminShoppers.getZonesByCityAjax') }}',
            type: 'get',
            dataType: 'json',
            data: {
                city_id: $(this).val()
            }
        })
        .done(function(data) {
            if (data.zones.length) {
                var html = '';
                $.each(data.zones, function(index, val) {
                    html += '<option value="'+ val.id +'">'+ val.name +'</option>';
                });
                $('#zone_id').append(html).slideDown('fast');
            }
            if (data.stores.length) {
                var html = '';
                $.each(data.stores, function(index, val) {
                    html += '<option value="'+ val.id +'">'+ val.name +'</option>';
                });
                $('#store_id').append(html).slideDown('fast');
            }
        })
        .fail(function() {
            console.log("falló la consulta para obtener.");
        });

    });
});
</script>

@stop