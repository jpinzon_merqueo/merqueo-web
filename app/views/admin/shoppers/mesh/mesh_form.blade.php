	@extends('admin.layout')
	@section('content')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
	<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
	<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>

	<section class="content-header">
		<h1>
			{{ $title }}
			<small>Control panel</small>
		</h1>
	</section>
	<section class="content">
	@if(Session::has('message') )
		@if(Session::get('type') == 'success')
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('message') }}
		</div>
		@else
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('message') }}
		</div>
		@endif
	@endif
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body table-responsive">
						<div class="row">
							<div class="col-xs-12">
								<form id="mesh_form" action="{{ route('adminShopperMesh.meshLoader') }}" method="POST" enctype="multipart/form-data">
									<div class="row form-group">
										<div class="col-xs-3">
											<label for="title">Título</label>
											<input type="text" name="title" class="form-control" required="">
										</div>
										<div class="col-xs-3">
											<label for="start_date">Fecha de inicio</label>
											<input type="text" name="start_date" id="start_date" class="form-control" required="">
										</div>
										<div class="col-xs-3">
											<label for="start_date">Fecha de finalización</label>
											<input type="text" name="end_date" id="end_date" class="form-control" required="">
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<label for="mesh_file">Importar archivo de mallas</label>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-3">
											<input type="file" name="mesh_file" class="form-control" required="">
										</div>
										<div class="col-xs-3">
											<button type="submit" class="btn btn-success">Importar</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="importing" class="unseen"><p>Procesando...</p><br><img src="http://devmerqueo.com/admin_asset/img/importing.gif"></div>
	</section>
	<script type="text/javascript">
		$('#start_date').datetimepicker({
			format: 'DD/MM/YYYY',
		});

		$('#end_date').datetimepicker({
			format: 'DD/MM/YYYY',
		});

		$('#mesh_form').validate();

		$('body').on('submit', '#mesh_form', function(event) {
			var confirm = window.confirm('¿Esta seguro de que desea importar este archivo?');
			if (confirm) {
	    		$.fancybox.open(['#importing'],{
			        autoSize    : true,
			        closeClick  : false,
			        closeBtn    : false ,
			        openEffect  : 'none',
			        closeEffect : 'none',
			        helpers   : {
			           overlay : {closeClick: false}
			        }
			    });
			    return true;
			}
		    return false;
		});

	</script>
	@endsection