@if (!Request::ajax())
	@extends('admin.layout')
	@section('content')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>


	<section class="content-header">
		<h1>
			{{ $title }} ({{ format_date('normal_without_time',$shopper_mesh->start_date) }} - {{ format_date('normal_without_time',$shopper_mesh->end_date) }})
			<small>Control panel</small>
		</h1>
	</section>
	<section class="content">
		@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('error') }}
		</div>
		@endif
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<div class="row">
								<div class="col-xs-12">
									<form id="mesh_form" action="{{ route('adminShopperMesh.meshShopperDetailUpdate', ['id' => $id, 'id_detail' => $id_detail]) }}" method="POST" enctype="multipart/form-data">
										<div class="row form-group">
											<div class="col-xs-3">
												<label for="title">Shopper</label>
												<input type="text" name="shopper" value="{{ $shopper->first_name }} {{ $shopper->last_name }}" class="form-control" readonly="">
											</div>
											<div class="col-xs-3">
												<label for="date">Fecha</label>
												<input type="text" name="date" id="date" class="form-control" required="">
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-3">
												<label for="entrance_time">Hora de entrada</label>
												<input type="text" name="entrance_time" id="entrance_time" class="form-control" required="">
											</div>
											<div class="col-xs-3">
												<label for="departure_time">Hora de salida</label>
												<input type="text" name="departure_time" id="departure_time" class="form-control" required="">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3">
												<button type="submit" class="btn btn-success">Guardar</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<div class="row">
								<div class="col-xs-12">
									<h3>Novedades</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6" id="comments-table">
								</div>
								<div class="col-xs-3">
									<form id="create-comment">
										<div class="row form-group">
											<div class="col-xs-12">
												<label>
													Tipificación
												</label>
												<select name="comment_type" id="comment_type" class="form-control" required="">
													<option value=""></option>
													@if ( count($types) )
														@foreach ($types as $type)
															<option value="{{ $type->id }}">{{ ucfirst( mb_strtolower($type->type) ) }}</option>
														@endforeach
													@endif
												</select>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-12">
												<label>
													Comentario
												</label>
												<textarea name="comment" id="comment" class="form-control"></textarea>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-12">
												<button type="button" class="btn btn-success send-comment">Guardar</button>
											</div>
										</div>
										<div class="row form-group">
											<div class="alert alert-success alert-dismissable" style="display: none;">
												<i class="fa fa-check"></i>
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<b>Alert!</b> <span class="texto"></span>
											</div>
											<div class="alert alert-danger alert-dismissable" style="display: none;">
												<i class="fa fa-ban"></i>
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<b>Alert!</b> <span class="texto"></span>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
	<script type="text/javascript">
		var date = new Date('{{ $shopper->date }} 00:00:00');
		$('#date').datetimepicker({
			format: 'DD/MM/YYYY',
			defaultDate: date
		});

		var entrance_time = new Date('{{ $shopper->date }} {{ $shopper->entrance_time }}');
		var hours_entrance = entrance_time.getHours();
		var minutes_entrance = entrance_time.getMinutes();
		var seconds_entrance = entrance_time.getSeconds();

		if ( hours_entrance.toString().length == 1 ) {
			var hours_entrance = '0'+hours_entrance;
		}
		if ( minutes_entrance.toString().length == 1 ) {
			var minutes_entrance = '0'+minutes_entrance;
		}
		if ( seconds_entrance.toString().length == 1 ) {
			var seconds_entrance = '0'+seconds_entrance;
		}
		$('#entrance_time').datetimepicker({
			format: 'HH:mm:ss',
			defaultDate: new Date(2016, 10, 10, hours_entrance, minutes_entrance, seconds_entrance)
		});

		var departure_time = new Date('{{ $shopper->date }} {{ $shopper->departure_time }}');
		var hours_departure = departure_time.getHours();
		var minutes_departure = departure_time.getMinutes();
		var seconds_departure = departure_time.getSeconds();

		if ( hours_departure.toString().length == 1 ) {
			var hours_departure = '0'+hours_departure;
		}
		if ( minutes_departure.toString().length == 1 ) {
			var minutes_departure = '0'+minutes_departure;
		}
		if ( seconds_departure.toString().length == 1 ) {
			var seconds_departure = '0'+seconds_departure;
		}
		$('#departure_time').datetimepicker({
			format: 'HH:mm:ss',
			defaultDate: new Date(2016, 10, 10, hours_departure, minutes_departure, seconds_departure)
		});

		$('#mesh_form').validate();

		$('body').on('click', '.send-comment', function(event) {
			if ( $('#create-comment').valid() ) {
				var type = $('#create-comment #comment_type').val();
				var comment = $('#create-comment #comment').val();
				$.ajax({
					url: '{{ route('adminShopperMesh.saveComment', ['id' => $id, 'id_detail' => $id_detail]) }}',
					type: 'POST',
					dataType: 'json',
					data: {
							type: type,
							comment: comment
					},
				})
				.done(function(data) {
					if (data.success) {
						$('#comments-table').empty().append(data.html);
						var alert = $('.alert-success');
						alert.fadeIn('fast');
						alert.find('.texto').html(data.success);
						alert.delay(1500).fadeOut('fast');
					}else{
						var alert = $('.alert-danger');
						alert.fadeIn('fast');
						alert.find('.texto').html(data.success);
						alert.delay(1500).fadeOut('fast');
					}
					$('#create-comment')[0].reset();
				})
				.fail(function() {
					console.log("error al guardar comentarios.");
				});

			}

		});

		jQuery(document).ready(function($) {
			$.ajax({
				url: '{{ route('adminShopperMesh.comments', ['id' => $id, 'id_detail' => $id_detail]) }}',
				type: 'GET',
				dataType: 'html'
			})
			.done(function(data) {
				$('#comments-table').empty().append(data);
				$('.paging-loading').hide('fast');
				console.log("success");
			})
			.fail(function() {
				console.log("error al obtener los comentarios.");
			});
		});
	</script>
	@endsection
@else
	@section('comments')
		<table class="table table-stripped">
			<thead>
				<tr>
					<th>
						No.
					</th>
					<th>
						Usuario
					</th>
					<th>
						Tipo
					</th>
					<th>
						Comentario
					</th>
				</tr>
			</thead>
			<tbody>
				@if (count($comments))
				@foreach ($comments as $key => $comment)
				<tr>
					<td>
						{{ $key + 1 }}
					</td>
					<td>
						{{ $comment->fullname }}
					</td>
					<td>
						{{ ucfirst( mb_strtolower($comment->type) ) }}
					</td>
					<td>
						{{ $comment->comment }}
					</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	@endsection
@endif