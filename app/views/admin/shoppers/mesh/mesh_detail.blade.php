@if (!Request::ajax())

	@extends('admin.layout')
	@section('content')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<section class="content-header">
		<h1>
			{{ $title }} ({{ format_date('normal_without_time',$shopper_mesh->start_date) }} - {{ format_date('normal_without_time',$shopper_mesh->end_date) }})
			<small>Control panel</small>
		</h1>
	</section>
	<section class="content">
	@if(Session::has('message') )
		@if(Session::get('type') == 'success')
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="<t></t>rue">×</button>
			<b>Alert!</b> {{ Session::get('message') }}
		</div>
		@else
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Alert!</b> {{ Session::get('message') }}
		</div>
		@endif
	@endif
		<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body table-responsive">
							<!-- <div class="row">
								<div class="col-xs-12">
									<div class="dataTables_filter" id="shoppers-table_filter">
										<label>Buscar: <input type="text" class="search"></label>
									</div>
								</div>
							</div> -->
							<div class="paging"></div>
							<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
						</div>
					</div>
				</div>
			</div>
	</section>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$.ajax({
				url: '{{ route('adminShopperMesh.meshDetail', ['id' => $mesh_id]) }}',
				type: 'GET',
				dataType: 'html'
			})
			.done(function(data) {
				$('.paging').empty().append(data);
				$('.paging-loading').hide('fast');
				console.log("success");
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});

		});
	</script>
	@endsection
@else
	@section('mesh')
	<table id="schedule-table" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>No.</th>
				<th>Llave</th>
				<th>Shopper</th>
				<th>Fecha</th>
				<th>Hora entrada</th>
				<th>Hora salida</th>
				<th>Acción</th>
			</tr>
		</thead>
		<tbody>
		@if (count($shopper_details))
			@foreach($shopper_details as $key => $detail)
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $detail->key }}</td>
				<td>{{ $detail->first_name }} {{ $detail->last_name }}</td>
				<td>{{ format_date('normal_without_time', $detail->date) }}</td>
				<td>{{ $detail->entrance_time }}</td>
				<td>{{ $detail->departure_time }}</td>
				<td>
					<a href="{{ route('adminShopperMesh.meshShopperDetail', ['id' => $mesh_id, 'id_detail' => $detail->id]) }}" class="btn btn-success">Ver detalles</a>
				</td>
			</tr>
			@endforeach
		@else
			<tr><td colspan="10" align="center">Mallas no encontradas</td></tr>
		@endif
		</tbody>
	</table>

	<div class="row">
		<div class="col-xs-3">
			<div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($shopper_details) }} registros</div>
		</div>
	</div>
	@endsection
@endif