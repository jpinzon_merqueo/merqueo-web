@if (!Request::ajax())

@extends('admin.layout')

@section('content')
<script>
    var web_url_ajax = "{{ route('adminShoppersBalance.index') }}";
    $('body').on('click','.export-balance', function(e){
        e.preventDefault();
        $(this).addClass('disabled');
        $.ajax({ url: "{{ route('adminShoppers.balanceExport') }}",
            //data: { id: id} ,
            type: 'post',
            dataType: 'json',
            success:
                function(response) {
                    $('.loading').hide();
                    $('.export-balance').removeClass('disabled');

                    if(response.status){
                        window.open(response.file_url,'_blank');
                    }else{
                        console.log("error al consultar los movimientos del shopper.");
                    }

                }
        });
    });
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-primary export-balance pull-right">Exportar Saldos</button>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="dataTables_filter" id="shoppers-table_filter">
                                <label>Buscar: <input type="text" class="search"></label>
                            </div>
                        </div>
                    </div>
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
    paging(1, '');
});
</script>

@else

@section('content')
<table id="shoppers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Celular</th>
            <th>Ciudad</th>
            <th>Estado</th>
            <th>Ver saldos</th>
        </tr>
    </thead>
    <tbody>
    @if (count($shoppers))
        @foreach($shoppers as $shopper)
        <tr>
            <td>{{ $shopper->first_name }}</td>
            <td>{{ $shopper->last_name }}</td>
            <td>{{ $shopper->phone }}</td>
            <td>{{ $shopper->city }}</td>
            <td>
                @if($shopper->is_active == 1)
                    <span class="badge bg-green">Trabajando</span>
                @else
                    <span class="badge bg-red">No trabajando</span>
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <a class="btn btn-xs btn-default" href="{{ route('adminShoppersBalance.edit', ['id' => $shopper->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="6" align="center">Shopper no encontrado</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($shoppers) }} registros</div>
    </div>
</div>

@endif
@stop