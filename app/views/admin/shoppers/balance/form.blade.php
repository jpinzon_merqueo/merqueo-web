@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Información del Shopper</h3>
                </div>
                <div class="box-body">
                    <input type="hidden" name="id" id="id" value="{{ $shopper->id }}">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            @if (!empty($shopper->photo_url))
                                <img src="{{ $shopper->photo_url }}" alt="{{ $shopper->id }}" class="img-responsive">
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Nombre completo</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $shopper->first_name.' '.$shopper->last_name }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Cédula</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $shopper->identity_number }}">
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Teléfono celular</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $shopper->phone }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                @if ($admin_permissions['insert'])
                <div class="box-header">
                    <h3 class="box-title">Registrar Movimiento</h3>
                </div>
                <div class="alert alert-success alert-dismissable unseen"></div>
                <div class="alert alert-danger alert-dismissable unseen"></div>
                <div class="box-body">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-3">
                            <label>Saldo actual shopper</label>
                            <br><span class="badge @if ($balance['shopper'] >= 0) bg-green @else bg-red @endif balance-shopper" style="font-size: 20px;">${{ number_format($balance['shopper'], 0, ',', '.') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <label>Saldo en tarjeta</label>
                            <br><span class="badge @if ($balance['card'] >= 0) bg-green @else bg-red @endif balance-card" style="font-size: 20px;">${{ number_format($balance['card'], 0, ',', '.') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <button type="button" class="btn btn-primary export-movement">Exportar movimientos</button>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label>Tipo movimiento</label>
                            <select class="form-control" name="movement" id="movement">
                                <option value="Recarga en tarjeta">Recarga en tarjeta</option>
                                <option value="Abono en efectivo">Abono en efectivo</option>
                                <option value="Deducción">Deducción</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <label>Valor</label>
                            <input type="text" class="form-control" name="amount" id="amount">
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <label>Observación</label>
                            <input type="text" class="form-control" name="description" id="description">
                        </div>
                        <div class="col-xs-12 col-sm-2"><br>
                            <button type="button" id="btn-save-recharge" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
                @endif
                <div class="box-header">
                    <h3 class="box-title">Movimientos</h3>
                </div>
                <div class="box-body">
                    <div class="container-overflow" style="min-height: 400px">
                        <div class="movements">{{ $movements_html }}</div>
                        <div align="center" class="loading unseen"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){

    $('#btn-save-recharge').click(function () {
        var shopper_id = $('#id').val();
        var amount = $('#amount').val();
        var movement = $('#movement').val();
        var description = $('#description').val();
        if (amount == ''){
            alert('El valor es requerido.');
            return false;
        }
        if (isNaN(amount)){
            alert('El valor debe ser númerico, diferente de cero y sin puntos');
            return false;
        }

        $('.movements').hide();
        $('.loading').show();
        $(this).addClass('disabled');
        $.ajax({ url: "{{ route('adminShoppersBalance.saveMovement') }}",
            data: { amount: amount, shopper_id: shopper_id, movement: movement, description: description },
            type: 'post',
            dataType: 'json',
            success:
                function(response) {
                    $('.loading').hide();
                    $('#btn-save-recharge').removeClass('disabled');
                    $('#amount').val('');
                    $('#description').val('');
                    if (response.balance.shopper >= 0)
                        $('.balance-shopper').removeClass('bg-red').addClass('bg-green').html(numberWithCommas(response.balance.shopper));
                    else $('.balance-shopper').removeClass('bg-green').addClass('bg-red').html(numberWithCommas(response.balance.shopper));
                    if (response.balance.card >= 0)
                        $('.balance-card').removeClass('bg-red').addClass('bg-green').html(numberWithCommas(response.balance.card));
                    else $('.balance-card').removeClass('bg-green').addClass('bg-red').html(numberWithCommas(response.balance.card));
                    $('.movements').html(response.html).show();
                    if (!response.status)
                        $('.alert-danger').html(response.message).show();
                    else $('.alert-success').html(response.message).show();
                }
        });
    });

    $('body').on('click', '.movement-remove-item', function() {
        if (confirm('¿Estas seguro que deseas eliminar este movimiento?'))
        {
            var id = $(this).attr('data-id');
            var shopper_id = $('#id').val();
            $('.movements').hide();
            $('.loading').show();
            $(this).addClass('disabled');
            $.ajax({ url: "{{ route('adminShoppersBalance.deleteMovement') }}",
                data: { id: id, shopper_id: shopper_id },
                type: 'post',
                dataType: 'json',
                success:
                    function(response) {
                        $('.loading').hide();
                        if (response.balance.shopper >= 0)
                            $('.balance-shopper').removeClass('bg-red').addClass('bg-green').html(numberWithCommas(response.balance.shopper));
                        else $('.balance-shopper').removeClass('bg-green').addClass('bg-red').html(numberWithCommas(response.balance.shopper));
                        if (response.balance.card >= 0)
                            $('.balance-card').removeClass('bg-red').addClass('bg-green').html(numberWithCommas(response.balance.card));
                        else $('.balance-card').removeClass('bg-green').addClass('bg-red').html(numberWithCommas(response.balance.card));
                        $('.movements').html(response.html).show();
                        if (!response.status)
                            $('.alert-danger').html(response.message).show();
                        else $('.alert-success').html(response.message).show();
                    }
            });
        }
    });

    $('body').on('click','.export-movement', function(){
        var shopper_id = $('#id').val();
        $(this).addClass('disabled');
        $.ajax({ url: "{{ route('adminShoppersBalance.exportMovement') }}",
            data: { shopper_id: shopper_id},
            type: 'post',
            dataType: 'json',
            success:
                function(response) {
                    $('.loading').hide();
                    $('.export-movement').removeClass('disabled');

                    if(response.status){
                        window.open(response.file_url,'_blank');
                    }else{
                        console.log("error al consultar los movimientos del shopper.");
                    }

                }
        });
    });

});
</script>

@stop