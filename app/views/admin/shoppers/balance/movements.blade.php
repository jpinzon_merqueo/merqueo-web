<table id="shoppers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Tipo movimiento</th>
            <th>Pedido</th>
            <th>Método de pago Cliente</th>
            <th>Método de pago Shopper</th>
            <th>Total pedido</th>
            <th>Total factura</th>
            <th>Saldo shopper</th>
            <th>Saldo tarjeta</th>
            <th>Observación</th>
            <th>Fecha</th>
            <th>Hecho por</th>
            <th>Estado</th>
            @if ($admin_permissions['delete'])
            <th>Eliminar</th>
            @endif
        </tr>
    </thead>
    <tbody>
    @if (count($movements))
        @foreach($movements as $movement)
        <tr>
            <td>{{ $movement->movement }}</td>
            <td>
                @if (!empty($movement->order_id))
                <a href="{{ route('adminOrders.details', ['id' => $movement->order_id]) }}" target="_blank">{{ $movement->order_id }}</a>
                <br>Comprado en {{ $movement->supermarket }}
                @endif
            </td>
            <td>{{ $movement->user_payment_method }}</td>
            <td>{{ $movement->shopper_payment_method }}</td>
            <td align="right">${{ number_format($movement->order_total_amount, 0, ',', '.') }}</td>
            <td align="right">${{ number_format($movement->total_real_amount, 0, ',', '.') }}</td>
            <td align="right" @if ($movement->shopper_balance > 0) style="color: green" @else @if ($movement->shopper_balance < 0) style="color: red" @endif @endif><b>${{ number_format($movement->shopper_balance, 0, ',', '.') }}</b></td>
            <td align="right" @if ($movement->card_balance > 0) style="color: green" @else @if ($movement->card_balance < 0) style="color: red" @endif @endif><b>${{ number_format($movement->card_balance, 0, ',', '.') }}</b></td>
            <td>{{ $movement->description }}</td>
            <td>{{ $movement->date }}</td>
            <td>{{ $movement->user }}</td>
            <td>@if ($movement->status)
                <span class="badge bg-green">Activo</span>
                @else
                <span class="badge bg-red">Inactivo</span>
                @endif
            </td>
            @if ($admin_permissions['delete'])
            <td>
                @if ($movement->status && $movement->movement != 'Gestión de pedido')
                <div class="btn-group">
                   <a class="btn btn-xs btn-default movement-remove-item" data-id="{{ $movement->id }}" href="javascript:;"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                @endif
            </td>
            @endif
        </tr>
        @endforeach
    @else
        <tr><td colspan="15" align="center">No hay movimientos.</td></tr>
    @endif
    </tbody>
</table>