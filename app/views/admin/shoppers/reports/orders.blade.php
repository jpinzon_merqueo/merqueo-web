@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
@if ( Session::has('success') || Session::has('error') )
<section class="content">
    @if(Session::has('success') )
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('success') }}
    </div>
    @endif

    @if(Session::has('error') )
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif
</section>
@endif
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="orders-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name / Address</th>
                                <th>Store</th>
                                <th>City</th>
                                <th>Phone</th>
                                <th>Items</th>
                                <th>Amount</th>
                                <th>Payment Method</th>
                                <th>Order Status</th>
                                <th>Shopper Status</th>
                                @if (Session::get('admin_designation') != 'Cliente' && Session::get('admin_designation_store_id') != 26)
                                <th>Delivery Date</th>
                                @endif
                                <th>Revision</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (count($orders))
                            @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->order_id }}</td>
                                <td>
                                    {{ $order->ordergroups_user_firstname.' '.$order->ordergroups_user_lastname }}
                                    <br>
                                    {{ $order->ordergroups_user_address }}
                                </td>
                                <td>{{ $order->stores_name }}</td>
                                <td>{{ $order->cities_city }}</td>
                                <td>{{ $order->ordergroups_user_phone }}</td>
                                <td>{{ $order->order_total_products }}</td>
                                <td>${{ number_format($order->order_total_amount + $order->order_delivery_amount - $order->order_discount_amount, 0, ',', '.') }}</td>
                                <td>{{ $order->ordergroups_payment_method }}</td>
                                <td>
                                @if($order->order_status == 'Delivered')
                                <span class="badge bg-green">Delivered</span>
                                @elseif($order->order_status == 'Cancelled')
                                <span class="badge bg-red">Cancelled</span>
                                @elseif($order->order_status == 'Initiated')
                                <span class="badge bg-yellow">Initiated</span>
                                @elseif($order->order_status == 'Dispatched')
                                <span class="badge bg-green">Dispatched</span>
                                @else
                                <span class="badge bg-green">In Progress</span>
                                @endif
                                <p><b>Source:</b> {{ $order->ordergroups_source }} {{ $order->ordergroups_source_os }}
                                @if($order->order_user_score)
                                <br><b>Customer Score:</b> @if($order->order_user_score < 4) <span class="badge bg-red">{{ $order->order_user_score }}</span> @else <span class="badge bg-green">{{ $order->order_user_score }}</span> @endif
                                @endif
                                </p>
                                </td>
                                <td>
                                @if($order->shoppers_id)
                                <span class="badge bg-green">{{ $order->shoppers_first_name.' '.$order->shoppers_last_name }}</span>
                                @else
                                <span class="badge bg-red">Pending</span>
                                @endif
                                </td>
                                @if (Session::get('admin_designation') != 'Cliente' && Session::get('admin_designation_store_id' != 26))
                                <td>
                                    @if ($order->order_status != 'Delivered' && $order->order_status != 'Cancelled')
                                    {{ get_time('left', $order->ordergroups_delivery_date); }}
                                    @else
                                    {{ date("d M Y",strtotime($order->ordergroups_delivery_date)); }} @endif
                                </td>
                                @endif
                                <td align="center">@if ($order->order_is_checked) <img src="{{ asset_url() }}img/available.png" width="10"> @else <img src="{{ asset_url() }}img/no_available.png" width="10"> @endif</td>
                                <td>
                                    <div class="btn-group">
                                       <a class="btn btn-xs btn-default" href="{{ route('adminOrders.details', ['id' => $order->order_id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr><td colspan="13" align="center">Orders not found</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Showing {{ count($orders) }} entries</div>
        </div>
    </div>
</section>

@endsection