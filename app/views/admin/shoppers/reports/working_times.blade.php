@extends('admin.layout')

@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <!-- <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/bootstrap-datetimepicker.js" type="text/javascript"></script>       -->

    <script>
        var web_url_ajax = "{{ route('adminShoppers.tracing') }}";
    </script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>   
    </section>
    <section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alert!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alert!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-1">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <strong>Nombre:</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <strong>Phone:</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <strong>Email:</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <strong>City:</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="col-xs-12">
                                        {{ $shopper->first_name }} {{ $shopper->last_name }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        {{ $shopper->phone }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        {{ $shopper->email }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        {{ $shopper->city }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="row">
                                    <div class="col-xs-12">
                                        {{ Form::open(['method' => 'POST', 'url' => route('adminShoppers.workingTimes', ['id' => $shopper->id]), 'class' => 'form-horizontal']) }}
                                        <div class="form-group">
                                            <label for="date">Select a day</label>
                                            <input type="text" class="form-control" name="date" id="date" placeholder="Date">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="Filter">
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">               
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                            </div>                          
                        </div>                                        
                        <br>
                        <div class="paging">
                            <table id="shoppers-tracing-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Activated at</th>
                                        <th>Deactivated at</th>
                                        <th>Business hours</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($working_times['data'] as $time)
                                        <tr>
                                            <td>
                                                {{ $time['id'] }}
                                            </td>
                                            <td>
                                                {{ $time['activated_at'] }}
                                            </td>
                                            <td>
                                                {{ $time['deactivated_at'] }}
                                            </td>
                                            <td>
                                                {{ $time['diff_hours'] }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>   
                            </table>
                        </div>                                  
                        {{--<div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>--}}
                    </div>      
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                @if (Input::has('date'))
                    {{ $working_times2->appends(['date' => Input::get('date')])->links() }}
                @else
                    {{ $working_times2->links() }}
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="dataTables_info" id="shelves-table_info">Showing {{ $working_times2->getTotal() }} entries</div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#date').datetimepicker({
                format: 'DD/MM/YYYY',
            });
        });
    </script>

@stop