<?php
    $shoppers_locations = array();
    foreach($shoppers as $shopper) {
        $shoppers_locations[] = array(
            'id' => $shopper->id,
            'first_name' => $shopper->first_name,
            'name' => $shopper->first_name . ' ' . $shopper->last_name,
            'location' => explode(' ', $shopper->location),
            'date' => str_replace('mess', 'meses', trim(_ago(strtotime($shopper->updated_at)))),
            'status' => $shopper->is_available
        );
    }

    $shoppers_locations = json_encode($shoppers_locations);


    function _ago($tm,$rcs = 0) {
       $cur_tm = time(); $dif = $cur_tm-$tm;
       $pds = array('segundo','minuto','hora','dia','semana','mes','año','década');
       $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
       for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);

       $no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
       if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
       return $x;
    }
?>
@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
    
    <span class="breadcrumb" style="top:0px">
        <a href="{{ admin_url(true) }}/{{ $store_admin ? 'storeadmin' : 'shopper' }}/add">
        <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Add {{$store_admin?"Store Admin":"Shopper"}}</button>
        </a>
    </span>
    
</section>
<section class="content">
@if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif

<style>
    .angular-google-map-container { height: 400px; }
</style>

    <h3>
        Live Shoppers
    </h3>
    <div ng-app="merqueo_map">
        <div ng-controller="shopers_maps">
            
            <div class="row" >
                <ui-gmap-google-map center='map.center' zoom='map.zoom' options="map.options">
                    <ui-gmap-marker ng-repeat="m in markers" coords='m.coords' icon='m.icon' options="m.options" ng-click='onMarkerClicked(m)' fit="fit" idkey='m.key'>
                        <ui-gmap-window show="m.showWindow" closeClick="'closeClick'" ng-cloak>
                            <div>
                                <h4>Shopper:</h4>
                                <p>Nombre: @{{m.name}}</p>
                                <p>Última conexión: @{{m.date}} </p>
                                <div ng-if="m.isavailable">
                                     <p>@{{m.isavailable}}</p>
                                </div>
                                <div ng-if="m.disponibilidad != ''">
                                    <p>Disponibilidad: @{{m.disponibilidad}} </p>
                                </div>
                                <div ng-if="m.disponibilidad != '' &&  m.orden != 0">
                                    <p>Numero de Orden Activa: @{{m.orden}} </p>
                                </div>
                            </div>
                        </ui-gmap-window>
                    </ui-gmap-marker>
                    <ui-gmap-polygon
                            ng-repeat="poly in polygons_draw"
                            path='poly.path'
                            fill='poly.fill'
                            stroke='poly.stroke'
                            editable='poly.editable'
                            visible='poly.visible'>
                    </ui-gmap-polygon>
                    <ui-gmap-polygon
                            ng-repeat="zone in zones_draw"
                            path='zone.path'
                            fill='zone.fill'
                            stroke='zone.stroke'
                            editable='zone.editable'
                            visible='zone.visible'>
                    </ui-gmap-polygon>
                </ui-gmap-google-map>
                <div id="control_click">
                    <div ng-show="controlunico" style="text-align: right; margin: 15px;">
                        <button class="btn btn-sm btn-primary" ng-show="controlunico" ng-click="controlClick()">Ver todos los shoppers</button>
                    </div>
                    <div ng-show="controlunicofiltros" style="text-align: right; margin: 15px;">
                        <button class="btn btn-sm btn-primary" ng-show="controlunicofiltros" ng-click="controlfiltersClick()">Ver sin filtros</button>
                    </div>
                </div>
                <div id="markets_tiendas">
                    <ul ng-show="!controlunico" style='margin:5px; display: inline-block; list-style: none; width: 100%;'>
                        <h2>Coverage Area</h2>
                        <li ng-repeat="store in store_draw" ng-click="filterstore(store)" class="@{{key}}" style='width:13%; display: inline-block; height:auto; text-align:left; margin:15px'>
                            <a href='#' class='itemfilterzone' idzone="@{{key}}" colorzone="@{{store.color}}" title="@{{store.name}} @{{store.store}}">
                                <div style='background-color:@{{store.color}}; width: 15px; height:15px; display:inline-block !important; vertical-align: top; margin: 4%; border:2px solid black;'></div>
                                <div style='display:inline-block !important; vertical-align: top; margin-top:2%; width: 70%;'>
                                    @{{store.name}} @{{store.store}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="markets_tiendas_locations">
                    <ul ng-show="!controlunico" style='margin:5px; display: inline-block; list-style: none; width: 100%;'>
                        <h2>Zones Available</h2>
                        <li ng-repeat="zoneact in store_draw_locations" class="@{{key}}" style='width:13%; display: inline-block; height:auto; text-align:left; margin:15px'>
                            <a href='#' class='itemfilterzonelocations' idzone='"@{{key}}"' ng-click="filterzone(zoneact)" colorzone="@{{zoneact.color}}" title='@{{zoneact.name}} @{{ zoneact.zone}}'>
                                <div style='background-color:@{{zoneact.color}}; width: 15px; height:15px; display:inline-block !important; vertical-align: top; margin: 4%; border:2px solid black;'>
                                </div>
                                <div style='display:inline-block !important; vertical-align: top; margin-top:2%; width: 70%;'>
                                    @{{zoneact.name}} @{{ zoneact.zone}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-xs-12">

                    <div class="box box-primary">
                        <div class="box-body table-responsive">
                            <table id="customers-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Phone</th>
                                        <th>Identity Number</th>
                                        <th>City</th>
                                        <th>Zone</th>
                                        <th>Track</th>
                                        <th>State App</th>
                                        <th>State</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($shoppers as $shopper)
                                    <tr>
                                        <td>{{ $shopper->first_name }}</td>
                                        <td>{{ $shopper->last_name }}</td>
                                        <td>{{ $shopper->phone }}</td>
                                        <td>{{ $shopper->identity_number }}</td>
                                        <td>{{ $shopper->city }}</td>
                                        <td>
                                            @foreach($shopper->zonerel as $zonerel)
                                                @foreach($zones as $zoneactiv)
                                                    @if ($zonerel['name'] == $zoneactiv['name'])
                                                        <p>{{$zonerel->city['city']}} - {{$zonerel['name']}}</p>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </td>
                                        <td>
                                            <div ng-open="sho_avaible({{$shopper->id}})" class="shopper_{{$shopper->id}}">
                                                <div class="shopperdate_{{$shopper->id}}" style="text-align: center; margin:15px;"></div>
                                                <button ng-click="show_shopper({{$shopper->id}})" type="button" class="localizate" style="background-color:#00a65a !important; border:none; width:100%; color:white; border-radius:5px; margin-top:10px;">Track</button>
                                            </div>                                            
                                        </td>
                                        <td>
                                            @if($shopper->is_available == 1)
                                                <span class="badge bg-green">Available</span>
                                            @else
                                                <span class="badge bg-red">No available</span>
                                            @endif
                                        </td> 
                                        <td>
                                            @if($shopper->is_active == 1)
                                                <span class="badge bg-green">Active</span>
                                            @else
                                                <span class="badge bg-red">Inactive</span>
                                            @endif
                                        </td> 
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{ admin_url(true) }}/{{$store_admin?"storeadmin":"shopper"}}/edit/{{ $shopper->id }}">Edit</a></li>
                                                    @if($shopper->is_active == 1)
                                                    <li><a href="{{ admin_url(true) }}/{{$store_admin?"storeadmin":"shopper"}}/toggle/{{ $shopper->id }}">Desactivate Shopper</a></li>
                                                    @else
                                                    <li><a href="{{ admin_url(true) }}/{{$store_admin?"storeadmin":"shopper"}}/toggle/{{ $shopper->id }}">Activate Shopper</a></li>
                                                    @endif
                                                    <li class="divider"></li>
                                                    <li><a href="{{ admin_url(true) }}/{{$store_admin?"storeadmin":"shopper"}}/delete/{{ $shopper->id }}"  onclick="return confirm('Are you sure you want to delete it?')">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                               
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="modal-track">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Track Shopper</h4>
          </div>
          <div class="modal-body">
            <div id="map_shopper" class="col-xs-12" style="height: 300px;"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</section>
<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key={{ Config::get('app.google_api_key') }}&sensor=false"></script>
<script src="https://cdn.firebase.com/js/client/2.4.1/firebase.js"></script>
<script type="text/javascript">
    var zones    = <?php echo json_encode($locations); ?>;
    var zoneslocations    = <?php echo json_encode($zonelocations); ?>;
    var map = null;
    var map_shoppers = null;
    var shoppers = {{ $shoppers_locations }};
    var markers = [];
    var markers_shopper = [];
    var marker;
    var marker_shopper;
    var hexVal = "0123456789ABCDEF".split("");
    var defaultColor = '#ff0000';
    var poly;
    var store_draw = [];
    var store_draw_locations = [];
    var store_html = '';
    var store_html_locations = '';
    var zonesdraw;
    var zonesdraw_locations;
    var firebaseshoopers = [];


    angular.module('merqueo_map', ['uiGmapgoogle-maps', 'firebase'])

    .controller("shopers_maps", ["$scope", "uiGmapLogger","$firebaseObject","$timeout",   function ($scope, logger, $firebaseObject, $timeout ) {

            logger.doLog = true;
            logger.currentLevel = logger.LEVELS.debug;
            var lastId = 1;
            var clusterThresh = 6;
            $scope.polygons =[];
            $scope.zones = [];
            $scope.store_draw = [];
            $scope.controlunico = false;
            $scope.controlText = 'Ver Shoppers';
            $scope.zonessave = [];
            $scope.poligonsafe = [];
            $scope.controlunicofiltros = false;


            var objfirebase = new Firebase("https://merqueoshoppers.firebaseio.com/shoppers");

            var datafirebase = $firebaseObject(objfirebase);

            google.maps.visualRefresh = true;

            datafirebase.$loaded().then(function() {
                $scope.objmarkers(datafirebase, $scope, 'initial');
            });

            // To make the data available in the DOM, assign it to $scope
             $scope.data = datafirebase;

             // For three-way data bindings, bind it to the scope instead
             datafirebase.$watch(function() {
                $scope.objmarkers(datafirebase, $scope, 'reload');
            });

             $scope.markerssafe = [];

            $scope.makeColor = function(){
                return '#' + hexVal.sort(function(){
                    return (Math.round(Math.random())-0.5);
                }).slice(0,6).join('');
            }


            $scope.objmarkers = function(datafirebase, $scope, status){
                
                $scope.fit = false;
                if(status == "initial"){
                    $scope.markers = [];
                    angular.forEach($scope.shoppersobj, function(value, key) {
                        angular.forEach(datafirebase, function(valuefire, keyfire) {
                            if (value.id == parseInt(valuefire.idshopper)){
                                $scope.disponibilidad = '';
                                $scope.isavailable = '';
                                $scope.ordenact = '';
                                $scope.optionsico = '';
                                $scope.optionsitem = {};

                                if (typeof valuefire.status != 'undefined'){

                                    if (value.isavailable == 1){
                                         $scope.isavailable = 'Disponible';
                                         $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_open.svg';
                                    }
                                    if (value.isavailable == 0){
                                         $scope.isavailable = 'No Disponible';
                                         $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_notdisponible.svg';
                                    }


                                    if (valuefire.status == "Disponible" && typeof($scope.ordenact) == 'undefined'){
                                        $scope.disponibilidad = valuefire.status;
                                        $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_open.svg';
                                    }

                                    if (valuefire.status == "No Disponible"){
                                        $scope.disponibilidad = valuefire.status;
                                        $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_notdisponible.svg';
                                    }

                                    if (valuefire.status != "Disponible" && valuefire.status != "No Disponible"){
                                        $scope.disponibilidad = valuefire.status;
                                        $scope.optionsitem.icon = '/admin_asset/img/marker_shopper.svg';
                                    }    

                                    if (valuefire.status == "Disponible" && typeof($scope.ordenact) != 'undefined'){
                                        $scope.disponibilidad = valuefire.status;
                                        $scope.optionsitem.icon = '/admin_asset/img/marker_shopper.svg';
                                    } 
                                }

                                if (valuefire.data_orden){
                                    if (valuefire.data_orden !=''){
                                        $scope.ordenact = valuefire.data_orden.order_id;
                                    }
                                }

                                if ($scope.ordenact == ''){
                                    $scope.ordenact = 0;
                                }

                                $scope.arrayinclude = true;

                                
                                $scope.optionsitem.labelContent = value.name;
                                $scope.optionsitem.labelAnchor = "22 0";
                                $scope.optionsitem.labelClass = 'marker-labels' +' shopper-' + valuefire.idshopper;

                                $scope.arrayact = {
                                  'coords': {
                                    'latitude': valuefire.latkey,
                                    'longitude': valuefire.longkey
                                  },
                                  'key': 'shopper-' + valuefire.idshopper,
                                  'orden': $scope.ordenact,
                                  'isavailable': $scope.isavailable,
                                  'disponibilidad': $scope.disponibilidad,
                                  'options': $scope.optionsitem,
                                  'name': value.name,
                                  'date' : valuefire.fecha.fecha + " " + valuefire.fecha.hora,
                                  'id': valuefire.idshopper
                                }

                                angular.forEach($scope.markers, function(value, key) {
                                    if( value.id == $scope.arrayact.id){

                                        $scope.arrayinclude = false;
                                    }
                                });

                                if ($scope.arrayinclude){
                                    this.push($scope.arrayact);
                                }
                                
                            }
                        }, $scope.markers);

                        $scope.disponibilidad = '';
                        $scope.isavailable = '';
                        $scope.ordenact = '';

                        $scope.optionsico = '';
                        $scope.optionsitem = {};

                        if (typeof value.status != 'undefined'){
                            if (value.isavailable == 1 || value.status == parseInt(1)){
                                 $scope.isavailable = 'Disponible';
                                 $scope.disponibilidad = 'Disponible';
                                 $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_open.svg';
                            }
                            if (value.isavailable == 0 || value.status == parseInt(0)){
                                 $scope.isavailable = 'No Disponible';
                                 $scope.disponibilidad = 'No Disponible';
                                 $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_notdisponible.svg';
                            }

                            if (value.status != ''){
                                
                                if (value.status == parseInt(1)){
                                     $scope.disponibilidad = 'Disponible';
                                     $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_open.svg';
                                }
                                if (value.status == parseInt(0)){
                                     $scope.disponibilidad = 'No Disponible';
                                     $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_notdisponible.svg';
                                }
                            }      
                        }


                        $scope.arrayinclude = 1;

                        if ($scope.ordenact == ''){
                            $scope.ordenact = 0;
                        }
                        
                        $scope.optionsitem.labelContent = value.name;
                        $scope.optionsitem.labelAnchor = "22 0";
                        $scope.optionsitem.labelClass = 'marker-labels' +' shopper-' + value.id;
                        if (value.location[0] != '' && value.location[1] != ''){
                            $scope.arrayact = {
                              'coords': {
                                'latitude': value.location[0],
                                'longitude': value.location[1]
                              },
                              'key': 'shopper-' + value.id,
                              'orden': $scope.ordenact,
                              'isavailable': $scope.isavailable,
                              'disponibilidad': $scope.disponibilidad,
                              'options': $scope.optionsitem,
                              'name': value.name,
                              'date' : value.date,
                              'id':  value.id
                            }
                        }else{
                            $scope.arrayact = {
                              'coords': {
                                'latitude': "4.6786999",
                                'longitude': "-74.0469998"
                              },
                              'key': 'shopper-' + value.id,
                              'orden': $scope.ordenact,
                              'isavailable': $scope.isavailable,
                              'disponibilidad': $scope.disponibilidad,
                              'options': $scope.optionsitem,
                              'name': value.name,
                              'date' : value.updated_at,
                              'id':  value.id
                            }
                        }

                        angular.forEach($scope.markers, function(value, key) {
                            if( value.id == $scope.arrayact.id){
                                $scope.arrayinclude = 0;
                            }
                        });

                        if ($scope.arrayinclude == 1){
                            this.push($scope.arrayact);
                        }

                    }, $scope.markers);
                    $scope.markerssafe = $scope.markers;
                }

                if(status == "reload"){
                    angular.forEach($scope.markers, function(value, key) {
                        angular.forEach(datafirebase, function(valuefire, keyfire) {
                                if (value.id == parseInt(valuefire.idshopper)){
                                    $scope.disponibilidad = '';
                                    $scope.isavailable = '';
                                    $scope.ordenact = '';
                                    $scope.optionsico = '';
                                    $scope.optionsitem = {};

                                    if (typeof valuefire.status != 'undefined'){
                                        if (value.isavailable == 1){
                                             $scope.isavailable = 'Disponible';
                                             $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_open.svg';
                                        }
                                        if (value.isavailable == 0){
                                             $scope.isavailable = 'No Disponible';
                                             $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_notdisponible.svg';
                                        }

                                        if (valuefire.status == "Disponible" && typeof(valuefire.data_orden) == 'undefined'){
                                            $scope.disponibilidad = valuefire.status;
                                            $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_open.svg';
                                        }

                                        if (valuefire.status == "No Disponible"){
                                            $scope.disponibilidad = valuefire.status;
                                            $scope.optionsitem.icon = '/admin_asset/img/marker_shopper_notdisponible.svg';
                                        }

                                        if (valuefire.status != "Disponible" && valuefire.status != "No Disponible"){
                                            $scope.disponibilidad = valuefire.status;
                                            $scope.optionsitem.icon = '/admin_asset/img/marker_shopper.svg';
                                        }    

                                        if (valuefire.status == "Disponible" && typeof(valuefire.data_orden) != 'undefined'){
                                            $scope.disponibilidad = valuefire.status;
                                            $scope.optionsitem.icon = '/admin_asset/img/marker_shopper.svg';
                                        } 
                                    }

                                    if (valuefire.data_orden){
                                        if (valuefire.data_orden !=''){
                                            $scope.ordenact = valuefire.data_orden.order_id;
                                        }
                                    }

                                    if ($scope.ordenact == ''){
                                        $scope.ordenact = 0;
                                    }

                                    $scope.arrayinclude = true;

                                    
                                    $scope.optionsitem.labelContent = value.name;
                                    $scope.optionsitem.labelAnchor = "22 0";
                                    $scope.optionsitem.labelClass = 'marker-labels' +' shopper-' + valuefire.idshopper;
                                    
                                    $scope.arrayact = {
                                      'coords': {
                                        'latitude': valuefire.latkey,
                                        'longitude': valuefire.longkey
                                      },
                                      'key': 'shopper-' + valuefire.idshopper,
                                      'orden': $scope.ordenact,
                                      'isavailable': $scope.isavailable,
                                      'disponibilidad': $scope.disponibilidad,
                                      'options': $scope.optionsitem,
                                      'name': value.name,
                                      'date' : valuefire.fecha.fecha + " " + valuefire.fecha.hora,
                                      'id': valuefire.idshopper
                                    }

                                    $scope.markers[key] = $scope.arrayact;
                                    
                                }
                            });
                    });
                }

                if(status == "filterdata"){
                    angular.forEach($scope.markerssafe, function(valuefire, keyfire) {
                        if (parseInt(datafirebase) == parseInt(valuefire.id)){
                            console.log(valuefire);
                            $scope.arrayact = {
                              'coords': {
                                'latitude': valuefire.coords.latitude,
                                'longitude': valuefire.coords.longitude
                              },
                              'key': 'shopper-' + valuefire.idshopper,
                              'orden': valuefire.orden,
                              'isavailable': valuefire.isavailable,
                              'disponibilidad': valuefire.disponibilidad,
                              'options': valuefire.options,
                              'name': valuefire.name,
                              'date' : valuefire.date,
                              'id': valuefire.id
                            }

                            this.push($scope.arrayact);
                            
                        }
                    }, $scope.markers);

                    $scope.fit = true;
                    $scope.controlunico = true;
                }
            }



            $scope.shoppersobj = shoppers;

            $scope.map = {
              actualZoom: null,
              showMarkers: true,
              doCluster: true,
              options: {
                streetViewControl: false,
                panControl: false,
              },
              markerControl: {},
              center: {
                latitude: {{$lat}},
                longitude: {{$lng}}
              },
              clusterOptions: {},
              zoom: 12
            };

            $scope.searchResults = {
              results: []
            };


            $scope.reset = function () {
              lastId = 1;
              $scope.searchResults.results.length = 0;
            //      $scope.searchResults = {
            //        results: []
            //      };
            };

            $scope.onMarkerClicked = function (marker) {
            //    if (markerToClose) {
            //      markerToClose.showWindow = false;
            //    }
                markerToClose = marker; // for next go around
                marker.showWindow = true;
                $scope.$apply();
                //window.alert("Marker: lat: " + marker.latitude + ", lon: " + marker.longitude + " clicked!!")
              };

            $scope.sho_avaible = function (idshopper){
                angular.forEach($scope.markerssafe, function(value, key) {

                    if (value.id == parseInt(idshopper)){
                        angular.element('.shopperdate_'+idshopper).html(value.date);
                        angular.element('.shopper_'+idshopper).css('display', 'block');

                    }
                });
            }

            $scope.show_shopper = function(idshopper){
                $scope.markers = [];
                $scope.zones = [];
                $scope.polygons = [];
                $scope.objmarkers(idshopper, $scope, 'filterdata');
                $( 'html, body').animate({
                    scrollTop: 0
                  }, 500);
            }

            $scope.controlClick = function () {
                $scope.fit = false;
                $scope.controlunico = false;
                $scope.zones_draw = [];
                $scope.polygons_draw = [];
                $scope.controlunicofiltros = false;
                angular.forEach($scope.markerssafe, function(valuefire, keyfire) {
                    $scope.arrayact = {
                          'coords': {
                            'latitude': valuefire.coords.latitude,
                            'longitude': valuefire.coords.longitude
                          },
                          'key': 'shopper-' + valuefire.idshopper,
                          'orden': valuefire.orden,
                          'isavailable': valuefire.isavailable,
                          'disponibilidad': valuefire.disponibilidad,
                          'options': valuefire.options,
                          'name': valuefire.name,
                          'date' : valuefire.date,
                          'id': valuefire.id
                        }

                        this.push($scope.arrayact);
                }, $scope.markers);

                console.log($scope.zonessave);

                angular.forEach($scope.zonessave, function(value, key) {
                    $scope.zones.push(value);
                });

                angular.forEach($scope.poligonsafe, function(value, key) {
                    $scope.polygons.push(value);
                });
            };


            $scope.change_zones = function (){
                $scope.store_draw = [];
                $scope.store_draw_locations = [];
                angular.forEach(zones, function(zone, key) {
                    $scope.zones_array = [];
                    var zone_store = [];
                    zone_store.id = zone.id;
                    zone_store.name = zone.store_name;
                    zone_store.store = (zone.name != null)?zone.name:'';
                    zone_store.delivery_zone = zone.delivery_zone
                    if(zone.delivery_zone != null && zone.delivery_zone.length) {
                        var cobertura = zone.delivery_zone.split(',');
                        for(var i = 0; i < cobertura.length; i++) {
                            var c = cobertura[i].split(' ');
                            $scope.zones_array.push({'latitude':c[0], 'longitude':c[1]});
                        }
                    }

                    var coloract = $scope.makeColor();
                    zone_store.color = coloract;
                    var validate = $.inArray($scope.store_draw, zone_store);
                    if (validate){
                        $scope.store_draw.push(zone_store);
                    }
                    
                    
                    $scope.zonesdraw = {
                        'path': $scope.zones_array,
                        'stroke': {
                            'color': coloract,
                            'weight': 3,
                            'opacity': 1.0,
                        },
                        'editable': false,
                        'draggable': false,
                        'geodesic': true,
                        'visible': true,
                        'fill': {
                            color: coloract,
                            opacity: 0.35
                        },
                        'id':zone.id
                    };
                    $scope.polygons.push($scope.zonesdraw);
                });

                $scope.poligonsafe = $scope.polygons;

                angular.forEach(zoneslocations, function(zone, key) {
                    $scope.zones_array_locations = [];
                    var zone_store_locations = [];
                    zone_store_locations.id = zone.id;
                    zone_store_locations.name = zone.namecity;
                    zone_store_locations.zone = (zone.name != null)?zone.name:'';
                    zone_store_locations.delivery_zone = zone.delivery_zone
                    if(zone.delivery_zone != null && zone.delivery_zone.length) {
                        var cobertura = zone.delivery_zone.split(',');

                        for(var i = 0; i < cobertura.length; i++) {
                            var c = cobertura[i].split(' ');
                            $scope.zones_array_locations.push({'latitude':c[0], 'longitude':c[1]});
                        }
                    }

                    var coloract = $scope.makeColor();
                    zone_store_locations.color = coloract;
                    var validate = $.inArray($scope.zone_store_locations, zone_store_locations);
                    if (validate){
                        $scope.store_draw_locations.push(zone_store_locations);
                    }

                    zonesdraw_locations= {
                        'path': $scope.zones_array_locations,
                        'stroke': {
                            'color': coloract,
                            'weight': 3,
                            'opacity': 1.0,
                        },
                        'editable': false,
                        'draggable': false,
                        'geodesic': true,
                        'visible': true,
                        'fill': {
                            color: coloract,
                            opacity: 0.35
                        },
                        'id':zone.id
                    };
                    $scope.zones.push(zonesdraw_locations);
                });

                $scope.zonessave = $scope.zones;
            }

            $scope.filterzone = function(zonesact){
                $scope.polygons = [];
                $scope.controlunicofiltros = true;
                angular.forEach($scope.zonessave, function(value, key) {
                    if (value.id == zonesact.id){
                        $scope.map.center = value.path[0];
                        $scope.zones_draw = [];
                        $scope.zones_draw.push(value);
                    }
                });
                
            }

            $scope.filterstore = function(storeact){
                $scope.zones = [];
                $scope.controlunicofiltros = true;
                angular.forEach($scope.poligonsafe, function(value, key) {

                    if (value.id == storeact.id){
                        $scope.map.center = value.path[0];
                        $scope.polygons_draw = [];
                        $scope.polygons_draw.push(value);
                    }
                });
                
            }

            $scope.controlfiltersClick = function(){
                $scope.zones_draw = [];
                $scope.polygons_draw = [];
                $scope.controlunicofiltros = false;
            }

            $scope.change_zones();

        }
    ]);

    
</script>
@stop