@if (!Request::ajax())

@extends('admin.layout')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
<!-- <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/bootstrap-datetimepicker.js" type="text/javascript"></script>       -->

<script>
    var web_url_ajax = "{{ route('adminShoppers.tracing') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
@if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                {{ Form::open(['method' => 'POST', 'route' => 'adminShoppers.tracingExport3', 'id' => 'filter-form']) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>
                                Exportar informe de tracing de shoppers agrupados
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label>Fecha inicial</label>
                            <input type="text" class="form-control date-field" id="start_date" name="start_date" placeholder="Ingrese una fecha inicial" required="required">
                        </div>
                        <div class="form-group col-xs-6">
                            <label>Fecha final</label>
                            <input type="text" class="form-control date-field" id="end_date" name="end_date" placeholder="Ingrese una fecha final" required="required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label>Ciudad</label>
                            <select name="report_city_id" id="report_city_id" class="form-control" required="required">
                                <option></option>
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->city }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="submit" class="btn btn-primary" value="Exportar">
                            <input type="reset" class="btn btn-primary" value="Limpiar">
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <!-- <div class="col-xs-6">
            <div class="box box-primary">
                {{ Form::open(['method' => 'POST', 'route' => 'adminShoppers.tracingExport2', 'id' => 'filter-form']) }}
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>
                                Exportar informe de tracing de shoppers por detalle
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label>Fecha inicial</label>
                            <input type="text" class="form-control date-field" id="start_date2" name="start_date" placeholder="Ingrese una fecha">
                        </div>
                        <div class="form-group col-xs-6">
                            <label>Fecha final</label>
                            <input type="text" class="form-control date-field" id="end_date2" name="end_date" placeholder="Ingrese una fecha">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="submit" class="btn btn-primary" value="Exportar">
                            <input type="reset" class="btn btn-primary" value="Limpiar">
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div> -->
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="search-form">
                                <table width="100%" class="shoppers-tracing-table">
                                    <tr>
                                        <td align="right"><label>Shopper:</label>&nbsp;</td>
                                        <td><select id="shopper_id" name="shopper_id">
                                            <option value=""></option>
                                            @if (count($shoppers))
                                                @foreach ($shoppers as $shopper)
                                                <option value="{{ $shopper->id }}">{{ $shopper->first_name.' '.$shopper->last_name }}</option>
                                                @endforeach
                                            @endif
                                        </select></td>
                                        <td align="right"><label>Source:</label>&nbsp;</td>
                                        <td><select id="source" name="source">
                                            <option value=""></option>
                                            <option value="Web">Web</option>
                                            <option value="Device">Device</option>
                                            <option value="Web Service">Web Service</option>
                                            <option value="Callcenter">Callcenter</option>
                                        </select>
                                        <select id="source_os" name="source_os" class="unseen">
                                            <option value=""></option>
                                            <option value="iOS">iOS</option>
                                            <option value="Android">Android</option>
                                        </select></td>
                                        <td align="right"><label>Order By:</label>&nbsp;</td>
                                        <td><select id="order_by" name="order_by">
                                            <option value="" selected="selected"></option>
                                            <option value="shoppers.first_name">Fullname</option>
                                            <option value="business_hours">Business hours</option>
                                            <option value="orders_qty">Orders of the day</option>
                                            <option value="productivity">Productivity</option>
                                        </select>
                                        <select id="sort_order_by" name="sort_order_by">
                                            <option value="asc">Ascending</option>
                                            <option value="desc" selected="selected">Descending</option>
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>City:</label>&nbsp;</td>
                                        <td><select id="city_id" name="city_id">
                                            <option value=""></option>
                                            @if (count($cities))
                                                @foreach ($cities as $city)
                                                <option value="{{ $city->id }}">{{ $city->city }}</option>
                                                @endforeach
                                            @endif
                                        </select></td>
                                        <td align="right"><label>State Work:</label>&nbsp;</td>
                                        <td><select id="is_active" name="is_active">
                                            <option value=""></option>
                                            <option value="1" selected="selected">Working</option>
                                            <option value="0">Not working</option>
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Search:</label>&nbsp;</td>
                                        <td><input id="search" type="text" placeholder="ID, name, phone, address" class="search" style="width: 250px"></td>
                                        <td align="right"><label>Status:</label>&nbsp;</td>
                                        <td><select id="status" name="status" multiple="multiple" style="height: 100px">
                                            <option value="Initiated">Initiated</option>
                                            <option value="In Progress">In Progress</option>
                                            <option value="Dispatched">Dispatched</option>
                                            <option value="Delivered">Delivered</option>
                                            <option value="Cancelled">Cancelled</option>
                                        </select></td>
                                        <td>&nbsp;</td>
                                        <td><button type="button" id="btn-search">Search</button>&nbsp;&nbsp;<button type="reset" id="btn-reset">Reset</button></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Working Time Modal -->
<div class="modal fade" id="modal-working-time" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Set Shopper Working Time</h4>
      </div>
        <form method="post" action="{{ route('adminShoppers.toggle') }}" class="form-modal">
          <div class="modal-body">
              <div class="form-group">
                <label class="control-label">Datetime</label>
                <input type="text" class="form-control datetimepicker" name="working_time" id="working_time">
                <input type="hidden" name="shopper_id" id="shopper_tracing_id">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" onclick="return working_time_form()">Save</button>
          </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
    paging(1, '');

    Number.prototype.padLeft = function(base,chr){
        var  len = (String(base || 10).length - String(this).length)+1;
        return len > 0? new Array(len).join(chr || '0')+this : this;
    }

    var d = new Date,
    dformat = [(d.getMonth()+1).padLeft(),
               d.getDate().padLeft(),
               d.getFullYear()].join('/') +' ' +
              [d.getHours().padLeft(),
               d.getMinutes().padLeft(),
               d.getSeconds().padLeft()].join(':');
    var f = new Date(dformat);
    $('.datetimepicker').datetimepicker({
         format: 'MM/DD/YYYY hh:mm A',
         defaultDate: f.toISOString(),
         sideBySide: true,
         locale: 'es'
    });
    $('#start_date').datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $("#start_date").on("dp.change", function (e) {
        $('#start_date').data("DateTimePicker").maxDate(e.date);
    });
    /*$('#start_date2').datetimepicker({
        format: 'DD/MM/YYYY',
    });*/
    $('#end_date').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
    });
    $("#start_date").on("dp.change", function (e) {
        $('#end_date').data("DateTimePicker").minDate(e.date);
    });
    $("#end_date").on("dp.change", function (e) {
    });

    //Inicializar campos en el form para generar excel.
    $('#search-form select').each(function(index, el) {
        $('#filter-form input[name="'+$(this).attr('id')+'"]').val($(this).val());
    });
    //Detectar cambios en el form de filtro y enviarlos al form para generar excel.
    $('#search-form select').change(function(event) {
        $('#filter-form input[name="'+$(this).attr('id')+'"]').val($(this).val());
        if ( $(this).attr('id') == 'source' && $(this).val() != 'Device' ) {
            $('#filter-form input[name="source_os"]').val('');
        }
    });
    $('#search-form #search').keyup(function(event) {
        $('#filter-form input[name="search"]').val($(this).val());
    });
});

function working_time(shopper_id){
     $('#shopper_tracing_id').val(shopper_id);
     $('#modal-working-time').modal('show');
}
function working_time_form(){
    if ($('#working_time').val() != ''){
        return true;
    }else{
        $('#working_time').addClass('error');
        return false;
    }
}

</script>

@else

@section('content')
<table id="shoppers-tracing-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Fullname</th>
            <th>City</th>
            <th>Zones</th>
            <th>State Work</th>
            <th>Business Hours</th>
            <th>Orders Of The Day</th>
            <th>Productivity</th>
            <th>Current Order</th>
            <th>Order On Time</th>
            <th>Order Allocated Time</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @if (count($shoppers) && !empty($shoppers) )
        @foreach($shoppers as $shopper)
        <tr>
            <td>{{ $shopper->first_name.' '.$shopper->last_name }}</td>
            <td>{{ $shopper->city }}</td>
            <td>
                @foreach($shopper->zones as $zone)
                    <p>{{ $zone->name }}</p>
                @endforeach
            </td>
            <td>
                @if($shopper->is_active)
                    <span class="badge bg-green">Working</span>
                @else
                    <span class="badge bg-red">Not working</span>
                @endif
            </td>
            <td>{{ $shopper->business_hours }}</td>
            <td>{{ $shopper->orders_qty }}</td>
            <td>{{ $shopper->productivity }}</td>
            <td>
            @if ($shopper->order_id)
                Order # <a href="{{ route('adminOrders.details', ['id' => $shopper->order_id]) }}" target="_blank">{{ $shopper->order_id }}</a> - {{ $shopper->order_pending->store_name }}
                @if($shopper->order_pending->status == 'Initiated')
                <span class="badge bg-yellow">Initiated</span>
                @elseif($shopper->order_pending->status == 'Dispatched')
                <span class="badge bg-green">Dispatched</span>
                @else
                <span class="badge bg-green">In Progress</span>
                @endif
                <p style="margin: 0">{{ $shopper->order_pending->user_address }}</p>
                <p><b>Source:</b> {{ $shopper->order_pending->source }} {{ $shopper->order_pending->source_os }}</p>
            @endif
            </td>
            <td>
            @if ($shopper->order_id)
                {{ get_time('left', $shopper->order_pending->delivery_date) }}
            @endif
            </td>
            <td>
            @if ($shopper->order_id)
                {{ get_time('passed', $shopper->order_pending->allocated_date) }}
            @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        @if($shopper->is_active == 1)
                        <li><a href="javascript:;" onclick="working_time({{ $shopper->id }})">Set Shopper As Not Working</a></li>
                        @else
                        <li><a href="javascript:;" onclick="working_time({{ $shopper->id }})">Set Shopper As Working</a></li>
                        @endif
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="12" align="center">Shoppers not found</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Showing {{ count($shoppers) }} entries</div>
    </div>
</div>

@endif

@stop