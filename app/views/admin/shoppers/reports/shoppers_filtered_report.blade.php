@extends('
admin.layout')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alert!</b> {{ Session::get('message') }}
            </div>
            @else
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alert!</b> {{ Session::get('message') }}
            </div>
            @endif
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Hitorical: Cumplimiento</h3>
                    </div>
                    {{ Form::open(['method' => 'POST', 'route' => 'adminShoppers.filter']) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label>Start Date</label>
                                <input type="text" class="form-control date-field" id="start_date" name="start_date" placeholder="Enter Start Date">
                            </div>
                            <div class="form-group col-xs-6">
                                <label>End Date</label>
                                <input type="text" class="form-control date-field" id="end_date" name="end_date" placeholder="Enter End Date">
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="submit" class="btn btn-primary" value="Filter">
                                <input type="reset" class="btn btn-primary" value="Reset">
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class="box-header">
                    <h3 class="box-title">Cumplimiento</h3>
                </div>
                <div class="box box-primary">
                    <div id="ct-chart-4" style="display: block; position: relative;"></div>
                    <div class="row text-center">
                        <div class="col-xs-8 col-xs-offset-2">
                            <a href="{{ route('adminShoppers.reportDetails', ['section' => 'fulfillment', 'id' => 'in-time']) }}" style="color: #00A65A;">
                                <div class="row">
                                    <strong>A tiempo:</strong>
                                    {{ number_format((float)$fulfillment['orders_delivered_in_time_percentage'], 2) }}% ({{count($fulfillment['orders_delivered_in_time'])}})
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-xs-8 col-xs-offset-2">
                            <a href="{{ route('adminShoppers.reportDetails', ['section' => 'fulfillment', 'id' => 'out-of-time']) }}" style="color: #F56954">
                                <div class="row">
                                    <strong>Fuera de tiempo: </strong>
                                    {{ number_format((float)$fulfillment['orders_delivered_not_in_time_percentage'], 2) }}% ({{count($fulfillment['orders_delivered_not_in_time'])}})
                                </div>
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="col-xs-2" style="height: 20px; background: #00A65A;">
                        </div>
                        <div class="col-xs-10">
                          <p>
                            Pedidos que cumplieron con el tiempo de entrega.
                          </p>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="col-xs-2" style="height: 20px; background: #F56954;">
                        </div>
                        <div class="col-xs-10">
                          <p>
                            Pedidos que no cumplieron con el tiempo de entrega.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-9">
                <div class="box-header">
                    <h3 class="box-title">Cancelados</h3>
                </div>
                <div class="box box-primary">
                @if ( !empty( $cancelled['cancelled_orders_per_reason'] ) )
                <div class="row">
                    <div class="col-xs-7">
                        <div id="ct-chart-5" style="display: block; position: relative;"></div>
                    </div>
                    <div class="col-xs-5">
                        <div class="col-xs-12">
                            <div id="donut-info">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <th>
                                        Color
                                    </th>
                                    <th>
                                        Razón
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Porcentaje
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach ($cancelled['cancelled_orders_per_reason'] as $index => $info)
                                        <tr>
                                            <td>
                                                <div style="height: 20px; width: 20px; background-color: {{ $cancelled['colors'][$index]  }}; ">       
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('adminShoppers.reportDetails' , ['section' => 'cancelled-orders', 'id' => $info['label']]) }}" style="color:#000; font-weight: bold;">
                                                    {{ $info['label'] }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('adminShoppers.reportDetails' , ['section' => 'cancelled-orders', 'id' => $info['label']]) }}" style="color:#000; font-weight: bold;">
                                                    {{ $info['cancelled_count'] }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('adminShoppers.reportDetails' , ['section' => 'cancelled-orders', 'id' => $info['label']]) }}" style="color:#000; font-weight: bold;">
                                                    {{ $info['percentage'] }}%
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                  <tr class="info">
                                    <td colspan="2" class="text-center">
                                      <strong>
                                        Total Pedidos cancelados <br>
                                        <hr>
                                        Total pedido del día
                                      </strong>
                                    </td>
                                    <td>
                                      <strong>
                                        {{ $cancelled['cancelled_orders_total'] }} <br>
                                        <hr>
                                         {{ count($cancelled['orders_day']) }}
                                      </strong>
                                    </td>
                                    <td style="vertical-align: middle;">
                                      <strong>
                                        {{ number_format($cancelled['cancelled_orders_total'] * 100 / count($cancelled['orders_day']) ,2)}}%
                                      </strong>
                                    </td>
                                  </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                @else
                    <div class="col-xs*12">
                        <br><p class="text-center">No se encontraron pedidos cancelados.</p><br>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">

        // Cumplimiento
        Morris.Bar({
          element: 'ct-chart-4',
          stacked: true,
          data: [
            { y: 'Pedidos', a: {{(float)$fulfillment['orders_delivered_not_in_time_percentage']}}, b: {{ (float)$fulfillment['orders_delivered_in_time_percentage'] }} },
          ],
          hideHover: true,
          xkey: 'y',
          ykeys: ['a', 'b'],
          labels: ['Cumplidos', 'Incumplidos'],
          hoverCallback: function (index, options, content, row) {
              var html = '';
              html = '';
              html += '<a href="{{ route('adminShoppers.reportDetails', ['section' => 'fulfillment', 'id' => 'in-time']) }}" style="color: #00A65A;"><strong>A tiempo:</strong> <br />';
              html += ''+row.b.toFixed(2)+'% ({{count($fulfillment['orders_delivered_in_time'])}})</a><br /><br />';
              html += '<a href="{{ route('adminShoppers.reportDetails', ['section' => 'fulfillment', 'id' => 'out-of-time']) }}" style="color: #F56954;"><strong>Fuera de tiempo:</strong> <br />';
              html += ''+row.a.toFixed(2)+'% ({{count($fulfillment['orders_delivered_not_in_time'])}})</a><br /><br />';

              return html;
          },
          barColors: [ '#F56954', '#00A65A' ]
        });

        @if ( !empty($cancelled['cancelled_orders_per_reason']) )
        // Cancelados
        Morris.Donut({
            element: 'ct-chart-5',
            data: jQuery.parseJSON('{{ json_encode($cancelled['cancelled_orders_per_reason']) }}'),
            formatter: function (y ,data) {
                var html = '';
                html += '<h2 class="text-center"><strong>'+data.label+'</strong></h2>';
                html += '<div class="text-center" style="font-size: 3rem;">'+y+'% ('+data.cancelled_count+')</div>';
                $('#donut-info').html(html);
                return y+'%';
            },
            colors: jQuery.parseJSON('{{ json_encode($cancelled['colors']) }}')
        });
        @endif

        $(document).ready(function(){
            $.validator.addMethod("dateFormat", function(value,element) {
                return value.match(/^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/);
            }, "Please enter a date using format dd/mm/yyyy"
            );

            $("#seachForm").validate({
                rules: {
                    start_date: {
                        required: true,
                        // dateFormat: true
                    },
                    end_date: {
                        required: true,
                        // dateFormat: true
                    },
                },
                submitHandler: function(form){
                    $('.loading').show();
                    $(form).submit();
                }
            });
            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
            $('#end_date').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'YYYY-MM-DD HH:mm:ss'
            });
            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date);
            });
            $("#end_date").on("dp.change", function (e) {
                // $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
            // $('#start_date').datetimepicker();
        });
    </script>
@endsection