@if (!Request::ajax())

@extends('admin.layout')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script>
    var web_url_ajax = "{{ route('adminShoppers.index') }}";
</script>

<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminShoppers.locations') }}">
            <button type="button" class="btn btn-primary"></i> Ver Mapa con Ubicación de Shoppers</button>
        </a>
        <a href="{{ route('adminShoppers.add') }}">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Shopper</button>
        </a>
    </span>

</section>
<section class="content">
@if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif

<div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="dataTables_filter" id="shoppers-table_filter">
                                <label>Buscar: <input type="text" class="search"></label>
                            </div>
                        </div>
                    </div>
                    <div class="paging"></div>
                    <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Working Time Modal -->
<div class="modal fade" id="modal-working-time" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Registrar Tiempo de Trabajo</h4>
      </div>
        <form method="post" action="{{ route('adminShoppers.toggle') }}" class="form-modal">
          <div class="modal-body">
              <div class="form-group">
                <label class="control-label">Fecha</label>
                <input type="text" class="form-control datetimepicker" name="working_time" id="working_time">
                <input type="hidden" name="shopper_id" id="shopper_tracing_id">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary" onclick="return working_time_form()">Guardar</button>
          </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
    paging(1, '');

    Number.prototype.padLeft = function(base,chr){
        var  len = (String(base || 10).length - String(this).length)+1;
        return len > 0? new Array(len).join(chr || '0')+this : this;
    }

    var d = new Date,
        dformat = [(d.getMonth()+1).padLeft(),
                   d.getDate().padLeft(),
                   d.getFullYear()].join('/') +' ' +
                  [d.getHours().padLeft(),
                   d.getMinutes().padLeft(),
                   d.getSeconds().padLeft()].join(':');

        var f = new Date(dformat);
        var date_widget = $('.datetimepicker').datetimepicker({
             format: 'MM/DD/YYYY hh:mm A',
             defaultDate: f.toISOString(),
             sideBySide: true,
             locale: 'es'
        });

    $('#modal-working-time').on('shown.bs.modal', function(event) {
        var d = new Date,
        dformat = [(d.getMonth()+1).padLeft(),
                   d.getDate().padLeft(),
                   d.getFullYear()].join('/') +' ' +
                  [d.getHours().padLeft(),
                   d.getMinutes().padLeft(),
                   d.getSeconds().padLeft()].join(':');

        var f = new Date(dformat);
        date_widget.data("DateTimePicker").date(f);
    });

});

function working_time(shopper_id){
     $('#shopper_tracing_id').val(shopper_id);
     $('#modal-working-time').modal('show');
}
function working_time_form(){
    if ($('#working_time').val() != ''){
        return true;
    }else{
        $('#working_time').addClass('error');
        return false;
    }
}
</script>

@else

@section('content')
<table id="shoppers-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Celular</th>
            <th>Cédula</th>
            <th>Ciudad</th>
            <th>Zonas</th>
            <th>Trabajando</th>
            <th>Estado</th>
            <th>Tiempo de trabajo actual</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @if (count($shoppers))
        @foreach($shoppers as $shopper)
        <tr>
            <td>{{ $shopper->first_name }}</td>
            <td>{{ $shopper->last_name }}</td>
            <td>{{ $shopper->phone }}</td>
            <td>{{ $shopper->identity_number }}</td>
            <td>{{ $shopper->city }}</td>
            <td>
                @foreach($shopper->zonerel as $zonerel)
                    @foreach($zones as $zoneactiv)
                        @if ($zonerel['name'] == $zoneactiv['name'])
                            <p>{{$zonerel->city['city']}} - {{$zonerel['name']}}</p>
                        @endif
                    @endforeach
                @endforeach
            </td>
            <td>
                @if($shopper->is_active == 1)
                    <span class="badge bg-green">Trabajando</span>
                @else
                    <span class="badge bg-red">No trabajando</span>
                @endif
            </td>
            <td>
                @if ($shopper->status == 1)
                    <span class="badge bg-green">Activo</span>
                @else
                    <span class="badge bg-red">Inactivo</span>
                @endif
            </td>
            <td>
                <div class="fluid-container">
                    <div class="row">
                        <div class="col-xs-12">
                            <strong>Inició: </strong>{{ $shopper->schedule_activated_at }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <strong>Terminó: </strong>{{ $shopper->schedule_deactivated_at }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="{{ route('adminShoppers.workingTimes', ['id' => $shopper->id]) }}">Ver historial</a>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ route('adminShoppers.edit', ['id' => $shopper->id]) }}">Editar</a></li>
                        @if($shopper->is_active)
                        <li><a href="javascript:;" onclick="working_time({{ $shopper->id }})">Colocar como No trabajando</a></li>
                        @else
                        <li><a href="javascript:;" onclick="working_time({{ $shopper->id }})">Colocar como Trabajando</a></li>
                        @endif
                        <li class="divider"></li>
                        <li><a href="{{ route('adminShoppers.delete', ['id' => $shopper->id]) }}"  onclick="return confirm('¿Estas seguro que deseas eliminarlo?')">Eliminar</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <tr><td colspan="10" align="center">Shopper no encontrado</td></tr>
    @endif
    </tbody>
</table>

<div class="row">
    <div class="col-xs-3">
        <div class="dataTables_info" id="shelves-table_info">Mostrando {{ count($shoppers) }} registros</div>
    </div>
</div>

@endif

@stop