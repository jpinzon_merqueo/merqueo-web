@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <form @submit.prevent="searchOrders" autocomplete="off">
                            <div class="row">
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="validation_type">Tipo de validación</label>
                                    <select name="validation_type" id="validation_type" class="form-control" v-model="validation_type">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="(reason, index) in validation_reasons" :value="reason.id">@{{ reason.reason }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="start_date">Fecha inicial entrega</label>
                                    <input type="text" name="start_date" id="start_date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="end_date">Fecha final entrega</label>
                                    <input type="text" name="end_date" id="end_date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="delivery_time">Hora de entrega</label>
                                    <select name="delivery_time" id="delivery_time" class="form-control" v-model="delivery_time">
                                        <option value="">-Selecciona-</option>
                                        <optgroup label="Franjas normales">
                                            <option v-for="(delivery_window, index) in delivery_windows" :value="delivery_window.delivery_window">@{{ delivery_window.delivery_window }}</option>
                                        </optgroup>
                                        <optgroup label="Mismo día">
                                            <option v-for="(delivery_window_same_day, index) in delivery_windows_same_day" :value="delivery_window_same_day.delivery_window">@{{ delivery_window_same_day.delivery_window }}</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="store">Tienda</label>
                                    <select name="store" id="store" class="form-control" v-model="store" @change="getWarehousesByCity">
                                        <option value="">-Selecciona-</option>
                                        <option v-for="(store, index) in stores" :value="store.city_id">@{{ store.name }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="warehouse">Bodega</label>
                                    <select name="warehouse" id="warehouse" class="form-control" v-model="warehouse">
                                        <option value selected>-Selecciona-</option>
                                        <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <label for="search">Búsqueda</label>
                                    <input type="text" name="search" id="search" class="form-control" placeholder="Celular, ID pedido, email, nombre, referencia" v-model="search">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-xs-12 form-group">
                                    <button class="btn btn-primary" type="submit">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="paging">
                            <table id="orders-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Razón de validación</th>
                                    <th>Nombre/Dirección</th>
                                    <th>Cant. Pedidos</th>
                                    <th>Ciudad/Zona/Bodega</th>
                                    <th>Celular</th>
                                    <th>Alistamiento</th>
                                    <th>Total</th>
                                    <th>Método de pago</th>
                                    <th>Estado</th>
                                    <th>Alistador seco</th>
                                    <th>Ruta</th>
                                    <th>Secuencia</th>
                                    <th>Fecha entrega</th>
                                    <th>Ver</th>
                                </tr>
                                </thead>
                                <tbody v-html="orders.html">
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center" v-html="orders.links_html">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                Mostrando <span v-html="orders.orders_count"></span> de <span v-html="orders.orders_total"></span>
                            </div>
                        </div>
                        <div align="center" class="paging-loading" v-show="is_loading"><br><img
                                    src="{{ asset_url() }}/img/loading.gif"/></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.call_center.order_validation.js.index-js')
@endsection