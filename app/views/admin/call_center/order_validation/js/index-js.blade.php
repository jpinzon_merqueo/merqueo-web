<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.min.js"></script>-->
<script src="https://unpkg.com/vee-validate@latest"></script>
<script>
    const messages = {
        _default: (field) => `El campo ${field} no es válido.`,
        after: (field, [target, inclusion]) => `El campo ${field} debe ser posterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        alpha_dash: (field) => `El campo ${field} solo debe contener letras, números y guiones.`,
        alpha_num: (field) => `El campo ${field} solo debe contener letras y números.`,
        alpha_spaces: (field) => `El campo ${field} solo debe contener letras y espacios.`,
        alpha: (field) => `El campo ${field} solo debe contener letras.`,
        before: (field, [target, inclusion]) => `El campo ${field} debe ser anterior ${inclusion ? 'o igual ' : ''}a ${target}.`,
        between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        confirmed: (field) => `El campo ${field} no coincide.`,
        credit_card: (field) => `El campo ${field} es inválido.`,
        date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
        date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
        decimal: (field, [decimals = '*'] = []) => `El campo ${field} debe ser numérico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
        digits: (field, [length]) => `El campo ${field} debe ser numérico y contener exactamente ${length} dígitos.`,
        dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} píxeles por ${height} píxeles.`,
        email: (field) => `El campo ${field} debe ser un correo electrónico válido.`,
        ext: (field) => `El campo ${field} debe ser un archivo válido.`,
        image: (field) => `El campo ${field} debe ser una imagen.`,
        included: (field) => `El campo ${field} debe ser un valor válido.`,
        integer: (field) => `El campo ${field} debe ser un entero.`,
        ip: (field) => `El campo ${field} debe ser una dirección ip válida.`,
        length: (field, [length, max]) => {
            if (max) {
                return `El largo del campo ${field} debe estar entre ${length} y ${max}.`;
            }

            return `El largo del campo ${field} debe ser ${length}.`;
        },
        max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
        max_value: (field, [max]) => `El campo ${field} debe de ser ${max} o menor.`,
        mimes: (field) => `El campo ${field} debe ser un tipo de archivo válido.`,
        min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
        min_value: (field, [min]) => `El campo ${field} debe ser ${min} o superior.`,
        excluded: (field) => `El campo ${field} debe ser un valor válido.`,
        numeric: (field) => `El campo ${field} debe contener solo caracteres numéricos.`,
        regex: (field) => `El formato del campo ${field} no es válido.`,
        required: (field) => `El campo ${field} es obligatorio.`,
        size: (field, [size]) => `El campo ${field} debe ser menor a ${formatFileSize(size)}.`,
        url: (field) => `El campo ${field} no es una URL válida.`
    };

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                messages: messages
            }
        }
    });
    let vue_obj = new Vue({
        el: '.content',
        data: {
            validation_reasons: {{ $validation_reasons }},
            delivery_windows: {{ $delivery_windows }},
            delivery_windows_same_day: {{ $delivery_windows_same_day }},
            stores: {{ $stores }},
            warehouses: [],
            validation_type: '',
            delivery_time: '',
            store: '',
            warehouse: '',
            search: '',
            orders: [],
            paginate_links: '',
            is_loading: false
        },
        methods: {
            searchOrders: function (page) {
                this.$validator.validate().then(result => {
                    if (!result) {
                        return;
                    }

                    if( $('#start_date').val() != '' && $('#end_date').val() == '') {
                        alert('Debe seleccionar la fecha de final de entrega en el rango de fechas');
                        return;
                    } else if ($('#start_date').val() == '' && $('#end_date').val() != '') {
                        alert('Debe seleccionar la fecha de inicio de entrega en el rango de fechas');
                        return;
                    }

                    let selectedStore = this.stores.find((store) => store.city_id === this.store);

                    let params = {
                        validation_type: this.validation_type,
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        search: this.search,
                        delivery_time: this.delivery_time,
                        warehouse: this.warehouse,
                        page: this.page,
                        store_id: selectedStore ? selectedStore.id : null
                    };

                    this.orders = [];
                    this.paginate_links = '';
                    this.is_loading = true;

                    if (page) {
                        params.page = page;
                    }

                    axios.post('{{ route('AdminOrderValidation.search') }}', params)
                        .then(response => {
                            this.orders = response.data;
                            this.paginate_links = response.data.paginate_links;
                            this.is_loading = false;
                        })
                        .catch(error => {
                            alert(error.data);
                            this.is_loading = false;
                        });
                });
            },
            getWarehousesByCity: function(){
                if(this.store){
                    let params = {
                        city_id: this.store
                    };
                    axios.get('{{ route('adminUsers.getWarehousesAjax') }}', {params: params})
                        .then(response => {
                            if(response.data.status == 200){
                                this.warehouses = response.data.result.warehouses;
                            }
                        })
                        .catch(error => {
                            console.error(error.data);
                        });
                }else {
                    this.warehouses = null;
                }
                this.warehouse = '';
            }
        },
        mounted(){
            this.searchOrders();
            let selft = this;
            $('body').on('click', '.pagination a', function (e) {
                e.preventDefault();
                let url_string = $(this).attr('href');
                var url = new URL(url_string);
                var page = url.searchParams.get("page");
                selft.searchOrders(page);
            });
            $('#start_date, #end_date').datetimepicker({
                format: 'DD/MM/YYYY',
            });
            $('#start_date').datetimepicker().on('dp.change', function (e) {
                if(e.date){
                    var incrementDay = moment(new Date(e.date));
                    $('#end_date').data('DateTimePicker').minDate(incrementDay);
                }
            });
            $('#end_date').datetimepicker().on('dp.change', function (e) {
                if(e.date){
                    var decrementDay = moment(new Date(e.date));
                    $('#start_date').data('DateTimePicker').maxDate(decrementDay);
                }
            });
        }
    });

</script>