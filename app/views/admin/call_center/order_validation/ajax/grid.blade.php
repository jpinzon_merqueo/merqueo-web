@if (count($orders))
    @foreach($orders as $order)
        <tr @if ($order->posible_fraud) class="orders-posible_fraud" @elseif ($order->suspect) class="orders-suspect" @elseif ($order->scheduled_delivery) class="orders-scheduled_delivery"@endif>
            <td>{{ $order->id }}
                @if($order->child_order_id)<p>(hijo - <a href="{{ route('adminOrderStorage.details', ['id' => $order->child_order_id]) }}">{{ $order->child_order_id }}</a>)</p>@endif
                @if($order->parent_order_id)<p>(padre - <a href="{{ route('adminOrderStorage.details', ['id' => $order->parent_order_id]) }}">{{ $order->parent_order_id }}</a>)</p>@endif
                <p>{{ $order->type }}</p>
            </td>
            <td>{{ $order->orderValidationReason ? $order->orderValidationReason->reason : null }}</td>
            <td>
                {{ $order->orderGroup->user_firstname.' '.$order->orderGroup->user_lastname }}
                <br>
                {{ $order->orderGroup->user_address.' '.$order->orderGroup->user_address_further }}
                @if (!empty($order->orderGroup->user_address_neighborhood))
                    <br>
                    <b>Barrio: </b>{{ $order->orderGroup->user_address_neighborhood }}
                @endif
            </td>
            <td>{{ $order->orders_qty }}</td>
            <td>{{ $order->orderGroup->city->city }} / <br> {{ $order->orderGroup->zone->name }} / <br> {{ $order->orderGroup->warehouse->warehouse }}</td>
            <td>{{ $order->orderGroup->user_phone }}</td>
            <td>
                <div style="text-align:center;">
                    @if($order->products_storage_dry > 0)<img src="{{ asset_url() }}img/dry.png" width="20" title="Seco">@endif
                    @if($order->products_storage_cold > 0)<img src="{{ asset_url() }}img/cold.png" width="20" title="Frío">@endif
                </div>
                <div style="text-align:center;">
                    ({{ $order->total_products }} @if($order->total_products > 1) items @else item @endif)
                </div>
            </td>
            <td>
                ${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}
                @if (!empty($order->coupon_code)) <br><b>Cupón:</b> {{ $order->coupon_code }} @endif
            </td>
            <td>{{ $order->payment_method }}</td>
            <td>
                @if($order->status == 'Validation')
                    <span class="badge bg-maroon">Validation</span>
                @elseif($order->status == 'Delivered')
                    <span class="badge bg-green">Delivered</span>
                @elseif($order->status == 'Cancelled')
                    <span class="badge bg-red">Cancelled</span><br><strong>@if($order->temporarily_cancelled) (Temporalmente) @endif</strong>
                @elseif($order->status == 'Initiated')
                    <span class="badge bg-yellow">Initiated</span>
                @elseif($order->status == 'Dispatched')
                    <span class="badge bg-green">Dispatched</span>
                @elseif($order->status == 'In Progress')
                    <span class="badge bg-orange">In Progress</span>
                @elseif($order->status == 'Alistado')
                    <span class="badge bg-green">Alistado</span>
                @elseif($order->status == 'Enrutado')
                    <span class="badge bg-orange">Enrutado</span>
                @endif
                <p><b>Origen:</b> {{ $order->orderGroup->source }} {{ $order->orderGroup->source_os }}
                    @if(!empty($order->user_score_date))
                        <br><b>Calificación:</b> @if(!$order->user_score) <span class="badge bg-red">Negativa</span> @else <span class="badge bg-green">Positiva</span> @endif
                    @endif
                </p>
            </td>
            <td>{{ $order->shopper_first_name.' '.$order->shopper_last_name }}</td>
            <td>@if (!empty($order->route->route)) {{ $order->route->route }} @else {{ $order->planning_route }} @endif</td>
            <td>{{ $order->planning_sequence }}</td>
            <td>
                @if ($order->status != 'Delivered' && $order->status != 'Cancelled')
                    {{ get_time('left', $order->delivery_date); }}
                @else
                    {{ date("d M Y",strtotime($order->delivery_date)); }} @endif
            </td>
            <td>
                <div class="btn-group">
                    <a class="btn btn-xs btn-default" href="{{ route('adminOrderStorage.details', ['id' => $order->id]) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr><td colspan="19" align="center">Pedidos no encontrados.</td></tr>
@endif