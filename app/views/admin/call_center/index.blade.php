@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
		<strong style="font-size: 2rem;">Estado: </strong>
            @if ( $admin->is_paused )
                <span class="label label-danger" style="font-size: 15px; margin-right: 3rem;">En Pausa</span>
                <form method="POST" action="{{ route('AdminCallCenter.statusUpdate', ['admin_id' => $admin->id]) }}"
                      accept-charset="UTF-8" style="display: inline;">
				<input type="hidden" name="status" value="{{ ($admin->is_paused ? 0 : 1) }}">
				<button type="submit" class="btn btn-success">Recibir pedidos</button>
		</form>
            @else
                <span class="label label-success" style="font-size: 15px; margin-right: 3rem;">Recibiendo pedidos</span>
                <form method="POST" action="{{ route('AdminCallCenter.statusUpdate', ['admin_id' => $admin->id]) }}"
                      accept-charset="UTF-8" style="display: inline;">
			<input type="hidden" name="status" value="{{ ($admin->is_paused ? 0 : 1) }}">
			<button type="submit" class="btn btn-danger">Dejar de recibir pedidos</button>
		</form>
            @endif

            {{--TODO Validar permisos--}}
            @if (true)
                <a class="btn btn-info open-modal"
                   href="{{ action('admin\callcenter\CustomerServiceController@users_table_ajax') }}">
                Gestionar permisos
            </a>
            @endif
	</span>
    </section>
    <section class="content">
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="paging">
                            <table id="orders-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID Grupo</th>
                                    <th>Tipo servicio</th>
                                    <th>Nombre completo</th>
                                    <th>Fecha creación</th>
                                    <th>Ciudad</th>
                                    <th class="text-center">Pedidos</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($order_groups))
                                    @foreach($order_groups as $order_group)
                                        <tr>
                                            <td style="vertical-align: middle">
                                                {{ $order_group->order_id }}
                                            </td>
                                            <td style="vertical-align: middle">
											<span class="label label-default"
                                                  style="background-color: {{ $order_group->customer_services['color'] }};">
												{{ $order_group->customer_services['description'] }}
											</span>
                                            </td>
                                            <td style="vertical-align: middle" width="150">
                                                {{ $order_group->first_name.' '.$order_group->last_name }}
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{ $order_group->created_at }}
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{ $order_group->city }}
                                            </td>
                                            <td>
                                                <table class="table table-bordered table-striped dataTable">
                                                    <thead>
                                                    <tr>
                                                        <th>
                                                            ID pedido
                                                        </th>
                                                        <th>
                                                            Tienda
                                                        </th>
                                                        <th>
                                                            Fecha de entrega
                                                        </th>
                                                        <th>
                                                            Cant.
                                                        </th>
                                                        <th>
                                                            Total
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($order_group->orders as $order)
                                                        <tr>
                                                            <td>
                                                                {{ $order['id'] }}
                                                            </td>
                                                            <td>
                                                                {{ $order['store_name'] }}
                                                            </td>
                                                            <td>
                                                                {{ $order['delivery_date'] }}
                                                            </td>
                                                            <td>
                                                                {{ $order['total_products'] }}
                                                            </td>
                                                            <td>
                                                                ${{ number_format($order['total_amount'] + $order['delivery_amount'] - $order['discount_amount'], 0, ',', '.') }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class="text-center" style="vertical-align: middle">
                                                <div class="btn-group">
                                                    <a class="btn btn-xs btn-default"
                                                       href="{{ route('AdminCallCenter.details', ['id' => $order_group->order_id, 'customer_services' => $order_group->customer_services['id']]) }}"><span
                                                                class="glyphicon glyphicon-folder-open"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="14" align="center">Orders not found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div align="center" class="paging-loading unseen"><br><img
                                    src="{{ asset_url() }}/img/loading.gif"/></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $('body').on('click', '.open-modal', function (event) {
                event.preventDefault();
                var url = $(this).prop('href');
                var modalDiv = $('#modal-note');
                modalDiv.modal();
                $.ajax(url)
                    .done(function (response) {
                        modalDiv.find('.modal-body').html(response);
                    })
            });
        });
    </script>

    <!-- Add Permissions modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Nota a Pedido</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

@endsection