@if ( !Request::ajax() )
    @extends('admin.layout')

@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary content-header">
                <div class="box-body">
                    <b>{{ $title }}</b>
                </div>
            </div>
        </div>
    </section>
    <script>
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    </script>
    <style>
        .Rojo {
            color: #f81616;
        }

        .Azul {
            color: #0061ec;
        }

        .icon {
            width: 30px;
            height: 30px;
        }

        .padding-left {
            padding-left: 10px;
        }

        .padding-right {
            padding-right: 10px;
        }

        .padding-top {
            padding-top: 10px;
        }

        .bottom-table {
            background-color: #f6f6f6;
            border-bottom-left-radius: 10%;
            border-bottom-right-radius: 10%;
        }

        .border {
            border: #f2eeee 1px solid !important;
        }

        .duo {
            display: flex;
            justify-content: space-between;
            flex-basis: 80%;
            flex-direction: row;
            padding-bottom: 10px;
        }

        .duo2 {
            display: flex;
            justify-content: space-between;
            flex-basis: 100%;
            flex-direction: column;
            text-align: center;
        }

        .box-route {
            background-color: #ffffff;
            border-radius: 5%;
            margin: 10px;
            overflow: hidden;
        }
    </style>

    <section>
        <div class="row">
            <div>
                <div class="col-md-12 col-xs-12 padding">
                    <div>
                        <?php
                        $dry = '<div>';
                        ?>
                        @foreach ($data as  $id => $order)
                            <?php
                            $routeProducts = route('adminMissingProducts.getProductsFromOrder', [$id]);
                            $dry .= '<div class="box-route">
                                              <div class="padding-top duo"><div class="padding-left"><b>Pedido #' . $id . '</b></div><div class="padding-right text-right"><a href="' . $routeProducts . '"><span class="Rojo">Pendiente </span><span class="glyphicon glyphicon-chevron-right"></span></a></div></div>
                                              <div class="duo border bottom-table">
                                                <div class="padding-left">Horario de entrega:</div>
                                                <div class="padding-right">' . $order['delivery_time'] . '</div>
                                              </div>
                                    </div>';
                            ?>
                        @endforeach
                        <?php
                        if ($dry == '<div>') {
                            $dry .= 'No se encontraron productos faltantes';
                        }

                        $dry .= '</div>';
                        ?>
                        <div id="dry">
                            <?php echo $dry; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="text-center">
        <?php
        $routeBack = route('adminMissingProduct.index');
        echo "<a class='btn btn-primary' href='$routeBack'>Volver</a>";
        ?>
    </div>
@stop
@endif
