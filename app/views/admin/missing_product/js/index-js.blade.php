<script type="text/javascript">
	$(function() {
		var orderHandler = (function() {
			'use strict';

			function orderHandler(args) {
				// enforces new
				if (!(this instanceof orderHandler)) {
					return new orderHandler(args);
				}
				// constructor body
				this.url_get_orders = '{{ route('adminMissingProduct.index') }}';
				this.url_get_stores = '{{ route('adminMissingProduct.getStoresAjax') }}';
				this.data = {
					city_id: null,
					store_id: null,
					status: null
				};
			}

			orderHandler.prototype.bindActions = function() {
				var self = this;
				$('body').on('click', '#btn-search-orders', function(event) {
					event.preventDefault();
					self.set_data();
					self.get_orders_ajax();
				});
				$('body').on('submit', '#search-form', function(event) {
					event.preventDefault();
					self.set_data();
					self.get_orders_ajax();
				});
				$('body').on('change', '#city_id', function(event) {
					event.preventDefault();
					self.set_data();
					self.get_stores_ajax();
				});
			};

			orderHandler.prototype.set_data = function(){
				this.data = $('#search-form').serialize();
			};

			orderHandler.prototype.get_orders_ajax = function() {
				$('.loader').removeClass('unseen');
				$.ajax({
					url: this.url_get_orders,
					type: 'GET',
					dataType: 'html',
					data: this.data,
				})
				.done(function(data) {
					$('.orders-table-missings').empty().html(data);
				})
				.fail(function(data) {
					console.error("error al cargar los pedidos.");
				})
				.always(function(data) {
					$('.loader').addClass('unseen');
				});
			};

			orderHandler.prototype.get_stores_ajax = function() {
				$('.loader').removeClass('unseen');
				$.ajax({
					url: this.url_get_stores,
					type: 'GET',
					dataType: 'json',
					data: this.data,
				})
				.done(function(data) {
					var html = '';
					$('#store_id').empty();
					$.each(data.stores, function(index, val) {
						 html += '<option value="'+val.id+'">'+val.name+'</option>';
					});
					$('#store_id').html(html);
				})
				.fail(function(data) {
					console.error("error al cargar las tiendas.");
				})
				.always(function(data) {
					$('.loader').addClass('unseen');
				});
			};

			return orderHandler;
		}());
		var order_handler = new orderHandler;
		order_handler.bindActions();
		order_handler.set_data();
		order_handler.get_orders_ajax();
		order_handler.get_stores_ajax();
	});
</script>