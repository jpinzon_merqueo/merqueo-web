@if ( !Request::ajax() )
    @extends('admin.layout')

@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary content-header">
                <div class="box-body">
                    <b>{{ $title }}</b>
                </div>
            </div>
        </div>
    </section>
    <style>
        .Rojo {
            color: #f81616;
        }

        .Azul {
            color: #0061ec;
        }

        .blue {
            color: #28456f;
            font-size: 9px;
        }

        .warning-title {
            background-color: red;
            color: white;
            text-align: center;
            grid-row: 1 / span 3;
            width: 100%;
        }

        .icon {
            width: 30px;
            height: 30px;
        }

        .padding-left {
            padding-left: 10px;
        }

        .padding-right {
            padding-right: 10px;
        }

        .padding-top {
            padding-top: 10px;
        }

        .padding {
            padding: 20px;
        }

        .padding-bottom {
            padding-bottom: 20px;
        }

        .bottom-table {
            background-color: #f6f6f6;
            border-bottom-left-radius: 10%;
            border-bottom-right-radius: 10%;
        }

        .border {
            border: #f2eeee 1px solid !important;
        }

        .duo {
            display: flex;
            justify-content: space-between;
            flex-basis: 80%;
            flex-direction: row;
            padding-bottom: 10px;
        }

        .duo2 {
            display: flex;
            justify-content: space-between;
            flex-basis: 100%;
            flex-direction: column;
            text-align: center;
        }

        .width-image {
            width: 40%;
            align-items: center;
            display: flex;
        }

        .width-image-2 {
            width: 16%;
            align-items: center;
            display: flex;
        }

        .width-data {
            width: 60%;
        }

        .content-text {
            font-size: 1em;
            display: grid;
            width: 100%;
            grid-template-columns: auto auto;
        }

        .box-route {
            background-color: #ffffff;
            border-radius: 5%;
            margin: 10px;
            overflow: hidden;
        }

        .text-bold {
            font-size: 26px;
            font-weight: bold;
        }

        .text-bold-2 {
            font-size: 20px;
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <section>
        <form id="form" action="{{route('adminMissingProducts.completeOrder')}}" method="post">
            <div class="row">
                <div>
                    <div class="col-md-12 col-xs-12 padding">
                        <div>
                            <input type="hidden" name="order_id" value="{{$order_id}}">
                            @if ( count($products) )
                                @foreach ($products as $product)
                                    <?php
                                    $code = $product->storeProduct->product->reference;
                                    $last = substr($code, -4);
                                    $codeLenght = strlen($code);
                                    $lastLenght = strlen($last);
                                    $first = substr($code, 0, $codeLenght - $lastLenght);
                                    ?>
                                    <div class="box-route">
                                        <div>
                                            <div class="warning-title">UNIDADES QUE FALTAN: {{ $product->quantity_original == $product->quantity ? $product->quantity : $product->quantity_original== null ? $product->quantity : $product->quantity_original - $product->quantity }}</div>
                                            <div class="warning-title">Cliente agregó {{$product->quantity_original == null ? $product->quantity : $product->quantity_original}} unidades
                                                (<span id="request_{{ $product->storeProduct->product->reference }}">{{ $product->quantity_original == $product->quantity ? 0 : $product->quantity_original== null ? 0 : $product->quantity_original - $product->quantity }}</span> de {{$product->quantity_original != null ? $product->quantity_original : $product->quantity}})
                                            </div>
                                            <div class="warning-title text-bold-2">Almacenamiento: {{$product->storage}}</div>
                                        </div>
                                        <div class="duo text-center padding-top">
                                            <div class="width-image">
                                                <img src="{{ $product->storeProduct->product->image_medium_url }}"
                                                     alt="{{ $product->storeProduct->product->name }}"
                                                     class="img-responsive">
                                            </div>
                                            <div class="padding-right text-right width-data">
                                                <div>POSICIÓN EN BODEGA:</div>
                                                <div class="text-bold">{{$product->warehouse_position}}</div>
                                                <div>POSICIÓN EN PICKING:</div>
                                                <div class="text-bold">{{$product->picking_position}}</div>
                                            </div>
                                        </div>
                                        <div class="text-bold-2 padding-left">STOCK EN BODEGA: {{$product->warehouse_storage}}</div>
                                        <div class="text-bold-2 padding-left">STOCK EN PICKING: {{$product->picking_stock}}</div>
                                        <div class="padding-left">
                                            <b>{{ $product->storeProduct->product->name }} {{ $product->storeProduct->product->quantity }} {{ $product->storeProduct->product->unit }}</b>
                                        </div>
                                        <div class="duo bottom-table padding-left">
                                            <div>
                                                <div class="blue padding-top"><b>CÓDIGO DE REFERENCIA DE ESTE PRODUCTO</b>
                                                </div>
                                                <div><?php echo $first; ?><b class="Rojo"><?php echo $last; ?></b></div>
                                            </div>
                                            <div class="width-image-2 padding-right padding-top">
                                                <img class="img-responsive"
                                                     src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAoKADAAQAAAABAAAAgAAAAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgAgACgAwERAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/dAAQAFP/aAAwDAQACEQMRAD8A/v4oA+AP+CqH7RvxK/ZF/wCCen7V37Snwel0KD4m/B74W3ni7wbL4m0k67oCavBq2k2aHVNIF1Ym/tvIvJgYBdwfOVbf8oFAHyz+zn+3T8efiZ8BP+CunxC8UXPhCTxJ+xf8dv2i/h78FZLHw39jsovD3wz/AGXfhb8XPC6+KrQahINfvY/Fvi/Vjf3Svp/23Tfs1l5URhM7AH8+X7O3/Bwf/wAFDvif+x54L+N/iXUPgt/wm2ufs7/8FX/iffmw+GDW2kN4l/Y68C/AbXvg4I7A+I5WjsI9S+IniJ/E9qtyTrkDWUIlsvsu9wD0z9oD/gvJ+318Nv8AgiZ/wTm/b08N6h8HR8e/2n/2jPFfwy+J81/8NXu/Bz+F9M1z4v2GnpoHhseIoG0nUIrfwXoYlvDqN157rdsYk+0jygD6i/4KIf8ABYv9sr9mb/grj4K/Yu+GN38L4/gvr/8AwT6+M37Sd8niDwG+r+Kf+Fk+Bf2cP2qvidoTw66uu2Sx6EPFXwg8HSXmlGyY3GnLqdkLqL7es0AB+Jvx0/4Ogv8AgqH8Pf2G/wBgr9oHw/ffs/8A/CwP2itf/a5074iNe/CKW50Zrb4MePfh94d8F/2Jpo8WRHTJE07xJqY1RzcXH2+YwS7YfJ2uAfbHgD/g4D/4KFeJ/wBkrwB8aNQ1D4Kr4y8R/wDBPHxd+0vqRt/hfJHpbfE3R/8Agqi/7IlhJDYt4nkMWgp8HVEEukCYmXxOTr32tFxYMAfrt/wW+/4KZ/tPfsDfGP8A4JY+CPgBd+AoNC/a8/aJuvhf8X18ZeED4mvJfDaeKfgjpMTeGbkatpo0O/Sz8d+IQ1w0N6HlkspPLT7GUnAPzu/4JO/8Fzf26v2xf+CzXxy/YV+Mt78I5/gb8PL39qe30BfDHw6bQfF5T4ReNJtC8IG+8Qf8JBeR3DJYov8AaRXT4RfS5kxDkCgDmtL/AOC7n7fV38F/+E6m1P4O/wBvJ+yBovxpLJ8M2Wzfxvf/APBX7xb+xVPKbb/hImI0r/hSOiWcKacso2+LDL4hMzI405AD9Fv2Mv8AgqH+1T8cf29/BH7Onjy8+Hsvw5139p7/AIK9/CnUE0jwYdM1xvCH7FniD9n7TfgnHFqn9sXIi1OC2+JfiP8A4Sq/FsV8QOLFhbWP2Y+aAfoz4m/ao+LGlf8ABXb4VfsYWk3hwfBPxf8AsFfEv9ojWYJdEMnitviP4W+N/hLwDpMlr4h+2qtvoY8P6zeLc6V/Z8hnvfKuRdIIzGwB+fPxd/4Kc/tReDPgP/wU0+IOiXngBfEf7Kf/AAVR+DP7Inwne78HG4sYvg/468T/ALJ+l65D4itP7WiOteJfsXxk8YrZ66JbUW8p0uU2Mv2ExzgH6N/An9pz4pfEH/gpJ+33+y94im8Pv8Kv2dPhf+x74t+G0FloxtPEMWr/ABs8O/E3UvHDa7rX2yUatbPdeE9JOkwCztP7Oj+1IZLnzwYgD+OX9m7/AIOWP+CmnxS8a/8ABRjw/wCKNT+Apsf2Xv2afjX8Ufhm2n/COW0mPi3wJ8c/hX8PdAm19/8AhLJhqmnJ4f8AFus/bLBRbfab17W5+0ILZopwD7s/4LV/8F1v27v2D/8Agpf+zR+yn8Brz4QxfCr4r/CH9nfxp4pXxh8On8R+Iv7b+JnxS8b+EPEzadrCeINPW0tG0nQLD7Dbm0n+yXPnTeZIJQiAHr3/AAWK/wCCz37av7EPi7/govpXwQvvhZFp/wCzFqX/AATztPhhD4s8AN4glSL9pbwx8WNV+Jn/AAkE412wbV3kvPB2inQSosxpMS3UZF0bkOgB6P8Asw/8Fd/2wfi5/wAE5/8AglD+094svfhmvxP/AGxP2y/iZ8D/AIySaZ4Ee08PyeA/CulftSXukR+F9HbXJ20LWEl+EnhD7TqRu74XCpqY+yp9uX7OAfWf/BFL/gor+0b+3hZzTfHq48ETyJ+xH+xv8fFPg/wqfDY/4T744fE/9sfwn44cg6pqIOkPpHwP8DjStOAX7Bcpq0/nS/2gEgAP/9D+/igD8iP+C9v/ACh2/wCCgP8A2QbU/wD1IPD9AHwX+xn/AMmmf8HE3/Z1/wC2V/6wd8BKAP49/wBjH/lG78M/+zMv+Dgj/wBVb+yXQB7l+1//AMqvf/BF3/s83xz/AOpT+0VQB92f8Fkv+ViX4X/9ofP2l/8A1i79vugD+Xj9q3/lFZ/wST/7G7/goj/6tv4P0AfqR8Hf+UeHwi/7Q4/EL/1/fLQB/RP/AMHSf/JyP/BBD/s8+7/9Tz9mCgD8av8Ag32/5WZP2rf+wp+3t/6s64oA5DQf+TZpP+0dHhj/ANiMviJQB+y//BNP/lLd8L/+z5v+Di//ANTD9jygD9nfHH/KxL8A/wDtEt8bf/Wo/h7QB+P37RP/ACaj/wAFxv8AtPh+zP8A+pv/AME+qAP2g/ZR/wCU1n/BXH/shP8AwTi/9Q7450Af5tf7E/8AyVL/AILSf9mS/tO/+tWfAWgD9WP+Dnn/AJTh/sOf9m6/sb/+r7+KVAH0h/wckf8AI+/8Fnf+w5/wRz/9Qf8AaBoA9o/YS/5Qv/8ABv3/ANpKPjf/AOo9+3lQB+gv/BsL/wAg25/7Rcf8E1v/AFev/BSigD//0f7+KAPyI/4L2/8AKHb/AIKA/wDZBtT/APUg8P0AfBf7Gf8AyaZ/wcTf9nX/ALZX/rB3wEoA/j3/AGMf+Ubvwz/7My/4OCP/AFVv7JdAHuX7X/8Ayq9/8EXf+zzfHP8A6lP7RVAH3Z/wWS/5WJfhf/2h8/aX/wDWLv2+6AP5eP2rf+UVn/BJP/sbv+CiP/q2/g/QB+pHwd/5R4fCL/tDj8Qv/X98tAH9E/8AwdJ/8nI/8EEP+zz7v/1PP2YKAPxq/wCDfb/lZk/at/7Cn7e3/qzrigDkNB/5Nmk/7R0eGP8A2Iy+IlAH7L/8E0/+Ut3wv/7Pm/4OL/8A1MP2PKAP2d8cf8rEvwD/AO0S3xt/9aj+HtAH4/ftE/8AJqP/AAXG/wC0+H7M/wD6m/8AwT6oA/aD9lH/AJTWf8Fcf+yE/wDBOL/1DvjnQB/m1/sT/wDJUv8AgtJ/2ZL+07/61Z8BaAP1Y/4Oef8AlOH+w5/2br+xv/6vv4pUAfSH/ByR/wAj7/wWd/7Dn/BHP/1B/wBoGgD2j9hL/lC//wAG/f8A2ko+N/8A6j37eVAH6C/8Gwv/ACDbn/tFx/wTW/8AV6/8FKKAP//S/v4oA/Ij/gvb/wAodv8AgoD/ANkG1P8A9SDw/QB8F/sZ/wDJpn/BxN/2df8Atlf+sHfASgD+Pf8AYx/5Ru/DP/szL/g4I/8AVW/sl0Ae5ftf/wDKr3/wRd/7PN8c/wDqU/tFUAfdn/BZL/lYl+F//aHz9pf/ANYu/b7oA/l4/at/5RWf8Ek/+xu/4KI/+rb+D9AH6kfB3/lHh8Iv+0OPxC/9f3y0Af0T/wDB0n/ycj/wQQ/7PPu//U8/ZgoA/Gr/AIN9v+VmT9q3/sKft7f+rOuKAOQ0H/k2aT/tHR4Y/wDYjL4iUAfsv/wTT/5S3fC//s+b/g4v/wDUw/Y8oA/Z3xx/ysS/AP8A7RLfG3/1qP4e0Afj9+0T/wAmo/8ABcb/ALT4fsz/APqb/wDBPqgD9oP2Uf8AlNZ/wVx/7IT/AME4v/UO+OdAH+bX+xP/AMlS/wCC0n/Zkv7Tv/rVnwFoA/Vj/g55/wCU4f7Dn/Zuv7G//q+/ilQB9If8HJH/ACPv/BZ3/sOf8Ec//UH/AGgaAPaP2Ev+UL//AAb9/wDaSj43/wDqPft5UAfoL/wbC/8AINuf+0XH/BNb/wBXr/wUooA//9P+/igD8iP+C9v/ACh2/wCCgP8A2QbU/wD1IPD9AHwX+xn/AMmmf8HE3/Z1/wC2V/6wd8BKAP49/wBjH/lG78M/+zMv+Dgj/wBVb+yXQB7l+1//AMqvf/BF3/s83xz/AOpT+0VQB92f8Fkv+ViX4X/9ofP2l/8A1i79vugD+Xj9q3/lFZ/wST/7G7/goj/6tv4P0AfqR8Hf+UeHwi/7Q4/EL/1/fLQB/RP/AMHSf/JyP/BBD/s8+7/9Tz9mCgD8av8Ag32/5WZP2rf+wp+3t/6s64oA5DQf+TZpP+0dHhj/ANiMviJQB+y//BNP/lLd8L/+z5v+Di//ANTD9jygD9nfHH/KxL8A/wDtEt8bf/Wo/h7QB+P37RP/ACaj/wAFxv8AtPh+zP8A+pv/AME+qAP2g/ZR/wCU1n/BXH/shP8AwTi/9Q7450Af5tf7E/8AyVL/AILSf9mS/tO/+tWfAWgD9WP+Dnn/AJTh/sOf9m6/sb/+r7+KVAH0h/wckf8AI+/8Fnf+w5/wRz/9Qf8AaBoA9l/YX/5Qt/8ABv8A/wDaSX45f+o5+3lQB+hH/BsL/wAg25/7Rcf8E1v/AFev/BSigD//1P7+KAPyI/4L2/8AKHb/AIKA/wDZBtT/APUg8P0AfBf7Gf8AyaZ/wcTf9nX/ALZX/rB3wEoA/j3/AGMf+Ubvwz/7My/4OCP/AFVv7JdAHuX7X/8Ayq9/8EXf+zzfHP8A6lP7RVAH3Z/wWS/5WJfhf/2h8/aX/wDWLv2+6AP5eP2rf+UVn/BJP/sbv+CiP/q2/g/QB+pHwd/5R4fCL/tDj8Qv/X98tAH9E/8AwdJ/8nI/8EEP+zz7v/1PP2YKAPxq/wCDfb/lZk/at/7Cn7e3/qzrigDkNB/5Nmk/7R0eGP8A2Iy+IlAH7L/8E0/+Ut3wv/7Pm/4OL/8A1MP2PKAP2d8cf8rEvwD/AO0S3xt/9aj+HtAH4/ftE/8AJqP/AAXG/wC0+H7M/wD6m/8AwT6oA/aD9lH/AJTWf8Fcf+yE/wDBOL/1DvjnQB/m1/sT/wDJUv8AgtJ/2ZL+07/61Z8BaAP1Y/4Oef8AlOH+w5/2br+xv/6vv4pUAfSH/ByR/wAj7/wWd/7Dn/BHP/1B/wBoGgD2X9hf/lC3/wAG/wD/ANpJfjl/6jn7eVAH6Ef8Gwv/ACDbn/tFx/wTW/8AV6/8FKKAP//V/v4oA/Ij/gvb/wAodv8AgoD/ANkG1P8A9SDw/QB8F/sZ/wDJpn/BxN/2df8Atlf+sHfASgD+Pf8AYx/5Ru/DP/szL/g4I/8AVW/sl0Ae5ftf/wDKr3/wRd/7PN8c/wDqU/tFUAfdn/BZL/lYl+F//aHz9pf/ANYu/b7oA/l4/at/5RWf8Ek/+xu/4KI/+rb+D9AH6kfB3/lHh8Iv+0OPxC/9f3y0Af0T/wDB0n/ycj/wQQ/7PPu//U8/ZgoA/Gr/AIN9v+VmT9q3/sKft7f+rOuKAOQ0H/k2aT/tHR4Y/wDYjL4iUAfsv/wTT/5S3fC//s+b/g4v/wDUw/Y8oA/Z3xx/ysS/AP8A7RLfG3/1qP4e0Afj9+0T/wAmo/8ABcb/ALT4fsz/APqb/wDBPqgD9oP2Uf8AlNZ/wVx/7IT/AME4v/UO+OdAH+bX+xP/AMlS/wCC0n/Zkv7Tv/rVnwFoA/Vj/g55/wCU4f7Dn/Zuv7G//q+/ilQB9If8HJH/ACPv/BZ3/sOf8Ec//UH/AGgaAPZf2F+P+CLX/Bv+fT/gpL8cv/Uc/by+v8vzoA/Qj/g2F/5Btz/2i4/4Jrf+r1/4KUUAf//W/v4oA/Ij/gvb/wAodv8AgoD/ANkG1P8A9SDw/QB8F/sZ/wDJpn/BxN/2df8Atlf+sHfASgD+Pf8AYx/5Ru/DP/szL/g4I/8AVW/sl0Ae5ftf/wDKr3/wRd/7PN8c/wDqU/tFUAfdn/BZL/lYl+F//aHz9pf/ANYu/b7oA/l4/at/5RWf8Ek/+xu/4KI/+rb+D9AH6kfB3/lHh8Iv+0OPxC/9f3y0Af0T/wDB0n/ycj/wQQ/7PPu//U8/ZgoA/Gr/AIN9v+VmT9q3/sKft7f+rOuKAOQ0H/k2aT/tHR4Y/wDYjL4iUAfsv/wTT/5S3fC//s+b/g4v/wDUw/Y8oA/Z3xx/ysS/AP8A7RLfG3/1qP4e0Afj9+0T/wAmo/8ABcb/ALT4fsz/APqb/wDBPqgD9oP2Uf8AlNZ/wVx/7IT/AME4v/UO+OdAH+bX+xP/AMlS/wCC0n/Zkv7Tv/rVnwFoA/Vj/g55/wCU4f7Dn/Zuv7G//q+/ilQB9If8HJH/ACPv/BZ3/sOf8Ec//UH/AGgaAPZP2GSB/wAEWf8AggCTwB/wUj+OZJ9APDf7eRJP0AoA/Qn/AINhv+Qbc/8AaLj/AIJrf+r0/wCCk9AH/9f+/igD8iP+C9v/ACh2/wCCgP8A2QbU/wD1IPD9AHwX+xn/AMmmf8HE3/Z1/wC2V/6wd8BKAP49/wBjH/lG78M/+zMv+Dgj/wBVb+yXQB7l+1//AMqvf/BF3/s83xz/AOpT+0VQB92f8Fkv+ViX4X/9ofP2l/8A1i79vugD+Xj9q3/lFZ/wST/7G7/goj/6tv4P0AfqR8Hf+UeHwi/7Q4/EL/1/fLQB/RP/AMHSf/JyP/BBD/s8+7/9Tz9mCgD8av8Ag32/5WZP2rf+wp+3t/6s64oA5DQf+TZpP+0dHhj/ANiMviJQB+y//BNP/lLd8L/+z5v+Di//ANTD9jygD9nfHH/KxL8A/wDtEt8bf/Wo/h7QB+P37RP/ACaj/wAFxv8AtPh+zP8A+pv/AME+qAP2g/ZR/wCU1n/BXH/shP8AwTi/9Q7450Af5tf7E/8AyVL/AILSf9mS/tO/+tWfAWgD9WP+Dnn/AJTh/sOf9m6/sb/+r7+KVAH0h/wckf8AI+/8Fnf+w5/wRz/9Qf8AaBoA9g/YdOP+CKn/AAQGPp/wUe+PB/8ALY/b1+v8vzoA/Q7/AINh/wDkHXH/AGi3/wCCav8A6vP/AIKTUAf/0P7+KAPyI/4L2/8AKHb/AIKA/wDZBtT/APUg8P0AfBf7Gf8AyaZ/wcTf9nX/ALZX/rB3wEoA/j3/AGMf+Ubvwz/7My/4OCP/AFVv7JdAHuX7X/8Ayq9/8EXf+zzfHP8A6lP7RVAH3Z/wWS/5WJfhf/2h8/aX/wDWLv2+6AP5eP2rf+UVn/BJP/sbv+CiP/q2/g/QB+pHwd/5R4fCL/tDj8Qv/X98tAH9E/8AwdJ/8nI/8EEP+zz7v/1PP2YKAPxq/wCDfb/lZk/at/7Cn7e3/qzrigDkNB/5Nmk/7R0eGP8A2Iy+IlAH7L/8E0/+Ut3wv/7Pm/4OL/8A1MP2PKAP2d8cf8rEvwD/AO0S3xt/9aj+HtAH4/ftE/8AJqP/AAXG/wC0+H7M/wD6m/8AwT6oA/aD9lH/AJTWf8Fcf+yE/wDBOL/1DvjnQB/m1/sT/wDJUv8AgtJ/2ZL+07/61Z8BaAP1Y/4Oef8AlOH+w5/2br+xv/6vv4pUAfSH/ByR/wAj7/wWd/7Dn/BHP/1B/wBoGgD1z9iM4/4In/8ABAs+n/BRr49n8vCv7ex/z6UAfol/wbD/APIOuP8AtFv/AME1f/V5/wDBSagD/9H+/igD8iP+C9v/ACh2/wCCgP8A2QbU/wD1IPD9AHwX+xn/AMmmf8HE3/Z1/wC2V/6wd8BKAP49/wBjH/lG78M/+zMv+Dgj/wBVb+yXQB7l+1//AMqvf/BF3/s83xz/AOpT+0VQB92f8Fkv+ViX4X/9ofP2l/8A1i79vugD+Xj9q3/lFZ/wST/7G7/goj/6tv4P0AfqR8Hf+UeHwi/7Q4/EL/1/fLQB/RP/AMHSf/JyP/BBD/s8+7/9Tz9mCgD8av8Ag32/5WZP2rf+wp+3t/6s64oA5DQf+TZpP+0dHhj/ANiMviJQB+y//BNP/lLd8L/+z5v+Di//ANTD9jygD9nfHH/KxL8A/wDtEt8bf/Wo/h7QB+P37RP/ACaj/wAFxv8AtPh+zP8A+pv/AME+qAP2g/ZR/wCU1n/BXH/shP8AwTi/9Q7450Af5tf7E/8AyVL/AILSf9mS/tO/+tWfAWgD9WP+Dnn/AJTh/sOf9m6/sb/+r7+KVAH0h/wckf8AI+/8Fnf+w5/wRz/9Qf8AaBoA9X/Yqbb/AMESP+CCTHkL/wAFE/2gWIHfHhH9vg+38/yoA/Rj/g2I/wCQdcf9otv+Can/AKvL/gpNQB//0v7+KAPyI/4L2/8AKHb/AIKA/wDZBtT/APUg8P0AfBf7Gf8AyaZ/wcTf9nX/ALZX/rB3wEoA/j3/AGMf+Ubvwz/7My/4OCP/AFVv7JdAHuX7X/8Ayq9/8EXf+zzfHP8A6lP7RVAH3Z/wWS/5WJfhf/2h8/aX/wDWLv2+6AP5eP2rf+UVn/BJP/sbv+CiP/q2/g/QB+pHwd/5R4fCL/tDj8Qv/X98tAH9E/8AwdJ/8nI/8EEP+zz7v/1PP2YKAPxq/wCDfb/lZk/at/7Cn7e3/qzrigDkNB/5Nmk/7R0eGP8A2Iy+IlAH7L/8E0/+Ut3wv/7Pm/4OL/8A1MP2PKAP2d8cf8rEvwD/AO0S3xt/9aj+HtAH4/ftE/8AJqP/AAXG/wC0+H7M/wD6m/8AwT6oA/aD9lH/AJTWf8Fcf+yE/wDBOL/1DvjnQB/m1/sT/wDJUv8AgtJ/2ZL+07/61Z8BaAP1Y/4Oef8AlOH+w5/2br+xv/6vv4pUAfSH/ByR/wAj7/wWd/7Dn/BHP/1B/wBoGgD1X9i3/lCJ/wAEFf8AtIj+0H/6iH7fNAH6Nf8ABsR/yDrj/tFt/wAE1P8A1eX/AAUmoA//0/7+KAPyI/4L2/8AKHb/AIKA/wDZBtT/APUg8P0AfBf7Gf8AyaZ/wcTf9nX/ALZX/rB3wEoA/j3/AGMf+Ubvwz/7My/4OCP/AFVv7JdAHuX7X/8Ayq9/8EXf+zzfHP8A6lP7RVAH3Z/wWS/5WJfhf/2h8/aX/wDWLv2+6AP5eP2rf+UVn/BJP/sbv+CiP/q2/g/QB+pHwd/5R4fCL/tDj8Qv/X98tAH9E/8AwdJ/8nI/8EEP+zz7v/1PP2YKAPxq/wCDfb/lZk/at/7Cn7e3/qzrigDkNB/5Nmk/7R0eGP8A2Iy+IlAH7L/8E0/+Ut3wv/7Pm/4OL/8A1MP2PKAP2d8cf8rEvwD/AO0S3xt/9aj+HtAH4/ftE/8AJqP/AAXG/wC0+H7M/wD6m/8AwT6oA/aD9lH/AJTWf8Fcf+yE/wDBOL/1DvjnQB/m1/sT/wDJUv8AgtJ/2ZL+07/61Z8BaAP1Y/4Oef8AlOH+w5/2br+xv/6vv4pUAfSH/ByR/wAj7/wWd/7Dn/BHP/1B/wBoGgD1X9i3/lCJ/wAEFf8AtIj+0H/6iH7fNAH6Nf8ABsR/yDrj/tFt/wAE1P8A1eX/AAUmoA//1P785p4LdBJcTRQIWCh5pEiQsQSFDOVG4gEgZyQCRjDbgD8iv+C9MsU3/BHP9v8AlhkSWNvgNqm2SJ1dGx4h0AHa6kqcEEHB4IIODQB8q/8ABND4cXvxj+FP/BdL4Rabqlromo/FT9vb9pP4cWGtX1vLd2WkXvjj9jT9nDwza6peWkEkU91a2E+qJd3FvDLHLNFE8cciOysoB8M/A7/g2c+M/wAJ/wBl7wp8Abr9qb4XazqXh/4If8FIPhNL4jtPAXiqDT7m6/bj8H/Bzw14c1eK0k1h5xb+A7r4YXl3rcDSrLqsGq20Ni0MltLI4B3fxq/4Nz/jD8VP+CTn7DH/AAThsv2mvhjpHjL9kj44+Jvi5rvxKuvA3ii58PeMNM1rWfidqFlpWk+H4tYj1TT7q2Tx7Zw3V3eXctvNLp1yYkiE8QiAPob9uT/ghz8Sv2rP+Cl/hH9vDRf2gPAHgvw1oH7FnxS/ZRPgDW/CWv6jrl54g+IvwJ/aM+Edj4ph1a11W20/+zdP1L426drU+mGA3tzY6Df2kMguLu3mgAPyZ+Mn/Bo/8dPiL+yF+xv+zfH+2X8HtF1L9l/Wv2ltT1jxVffDnxg+l+KV+PHjLwV4n0yHTrRdcjuLF9Ah8Kz2t611K4vJLuJ7cIsTlgD6t8Df8G1/xl8O/s0eDfgZF+1Z8J9RvvC37Efif9k6fxFb+APFT6fceINc/wCCizfttweLY7ZfEBmXSrbwrIvgOfS3lW9k17GtpONOIsqAP05/4K3f8EnPH/8AwUh+Kv8AwTs8e+DfjD4M+GVl+xL8bLr4ueJtM8S+HNa1298c20/iP4SazbaTocul31pDpUnlfDfU7eS51BJ0ebUbRowqW1wsoB8Nf8E4v+DfX4nfsJf8FVPi5/wUJ8X/ALTPwy8beGfibdftEXdp8NdH8IeIND8Q6cnxt8VzeINNEusanq8+nzf2GjC2vXitWF3IrNAsSkBAD8dP2z/2RvEf7CL/ABE/Zi8W+M9E+IWvfD//AIJm/A7Wr3xX4d0q/wBE0jUU+JX/AAXf1H4p2dva6bqd1eXkD6VY+M7bSLmSW6dbm7sZ7qJYYZkiQA/pY/ZV/wCCQ3xE/Z7/AGyvCX7UGtfGfwX4l0bw5+0H/wAFOPjNN4U0vwtrlhqdzpX7eutfBXVfCmhRahd6jParqHw9T4U3UXiG+eH7NrTatbnTbe0FvJvAPuTxD+yB4g1b/gpj8Ov2908daBbeDvBH7Gfj39mO78Ay6ZenXr7WvGPxb8NfEi28XQa8LsaVDo9lZaHJpk2nS2bXctzcx3Md0sSsjAHxF8R/+CUHjT4i/B/9v74XaX8b/Alrqf7Y/wDwUT+Fn7bnh7U38P6te23gnwv8O/EX7N2sXfgbW7O31NbjVNb1GD4HahbQazYy22nwTeILGWS2ZLSeOUA+7vg9+ybr3wy/b1/bO/a3vvG2haroP7Ufw5/Zg8H6D4GtdMvbbX/Cc3wA0Xx/o+salq2pzXktlqdp4im8aWsmnpZWdq9gLKWO6knedCgB/LR8F/8Ag1X+NfwO8Rfty+Ldf/bK+Cs9p+1v8Bfip8G9HMngDxZp0Hg3WfiT8Xvh18SdP1fU5r3xBHHqNlYQeCrrSZrG1lhurq41C3mglCQSI4B9of8ABV//AIN3vi3/AMFFv29/gH+2N4R/aS+HPw10L4OfDD4IeA73wX4j8E+Jtc1bWrz4T/ELxb42vdRtdS0zVLWzgtdXh8SQ2NpBLD5tvLaySTOyOlAHpn/BUL/gg/8AFP8A4KBeJv25de8KftC/D34e2X7W1/8AsR3mgW+veDfEWtT+Dl/ZQ8P/ABK0XXo9WuNN1W3i1M+L5/HNpPpJs4YP7Mj065ivftDzxvEAWfAX/BGr4i/s3f8ABNn9hz9m3V/jj4H8S6x/wT6+Ofxo/ah8XeJtP8Ka5Y6d8RfD+ueEv2lvJ8KeHrC51OS60PVbcfGLThLqupy3ljIdCvilrH9shW3APAv+DYj/AJB1x/2i2/4Jqf8Aq8v+Ck1AH//V/cf/AIL2fsjftMftSeL/ANgnU/hR+ydq/wC2p8Cvg14++Nni79ov4AaD+0PpX7OGp+M4dZ8A6PoPw7t4/Gt/r2hXafY9Wl1rVbdtMN5It5p8Gn3aW1nq01xQBB+yl8D/ANlj/gon/wAEXP2h/wBk39jTwP4w/Yn0jx94m+LfwG+LPw4+K9z4r+KvjD9n/wDaI8M+IPDq/Ezw94n/AOEi8a3Go+IpbD7Do01nFY+JNHtJdN1Gxnk0/RtRbUtNiAPmn/gpD+yZ4t/ZX/Z91H4AwfGvxDc6F/wVs/4Lhfs62/jvVvAMOsfDjXfCXwj+KvhT4feGvGXw1fX9O8RXuoXj6lb/AAJaWfX7GbR0u7fXY9Kl0wRQXEt6Ae0fsifs9+B/+Cdn/BdLxZ+x3+zBe+OPDn7On7Qf/BMlf2jtX+GPiz4i+PfiRoHhn4v/AA3/AGhYvhnYeKNC/wCE48Ra5qVl/a3h3WLyPVYxqIE88rJFJHG0aQAHmX7Ff7O3x1/Zn/4OCNe0z9oT9rLx7+1Z8Xfjl/wTh+Ivxo+JHiXVtPbwZ8N/Dd3cftM+GvDHg7wL8J/hnb6trFp4O8E+EPC2jW1nY2cupX801/calfR/ZTeXCSgHT/8ABeP4F/8ABMnQNS1P4zftZ6/+1R8W/wBrf41+B5/ht+xx+zD8F/jf8Wj4t1H4g6Ho407QL74G/B7wJqMGk+HM+JbjTNW8ZeLvEmmat4ak1S4mkuLDVdVv49D1QA+mT+xZ4y+N/wDwRm/Z88Lf8FGfgB4z/bF/aw+An7PiePz8H9O+K/irwF498Z/GfSvCGrW3hrwbrHjfwl418ORah4yn0S50nwn4r1rV9X1S3uNeTWNYhS9v5UM4B+fH/BsjY/Bz9l/4T/t7ad8arn4sfAv9sTwb4qj+KP7W37O3xrtfGPhvQv2evgxoVr41174Vz/CvTvGfiTxTqXiX4bQ+FdQ1++1H4iXmr3uv6lK2mWOtY0628Manr4B7j/wRe/bE8G/GH9pj4ufHn46eLvEun/tLf8FXrrxl8cP2YPhPeHWb7w98Pv2Dv2WNcv8A4U/B7TLqWS7bw/4X8V+K7y/8YeLbjTLS2iv/ABPAyeJLnab23iYA+Mv+CrH7LPizwP8A8FEf2xv27v2x/wDgm98Vf22v+Cftv8LP2fzovj74V/tUn4ba78A/DPw48ELb/GLxrZ/CPQfHXh7xL43tRevPc61pk0ejWtjDo51yLU0sNQ1W+twD9Qv2kv8Agk/4R/4KD22lfHr4EfHnTfh38G/i7/wT4/ZQ/Z8+Eui6h4C13xbPpXgX4Y/tMeEf2sPCPi651TUvGul6vfprfhDT9N8E/wBj6mi6xYXkza7qWs6hMkumuAeb/wDBwl+29+0H4NsvgZ/wTf8A2LPCnxo8XftH/tlRa3r3xJu/2edGi1j4x/D/APZR8JXkVj8S9W+H7Xd3p+maH4w8ao2p+HtD8UatqNhpmhWGk+Ime+sNUvNHvYAD6o/Ygf8AYN/4KV/8E4Zf2WLT4P8Axl8LfBb4Aavpv7Mnxd/Z0/aB1fxf4F+Ovwu8efAx/D2rQaF8Stf8NeK7fX7jXzJDofii612w8Ttba1JeXUOox288Wp6NagH5Tf8ABGD9g34NfEj/AIKNfF//AIKH/soeB/EfwK/YL/Z7uPiV+zX+zLpcnxU+Jnju/wD2rPiHZz6p4M+Kvx41r/hPfFXiUWPww0cy6joPgDS9PWzh1S9jsdTuP+JjoGrJOAfhL+0B4/8AFkX7Zv7RHxNe++Net+LvDf8AwVr8O+FtD/4LdaN8YP2grf8AZh/Z5+F8/jfQrh/gVrXw90nQLj4eynwDpb3Pw71OC2nfwvKHks7W4vdCTR7zUgD95v8AgsdqH7KY/wCCo3w51D/gr7d/F+P/AIJkWf7Fd3d/s73nhkfGSX4Eah+1Te+PdQHjV/HNx8EBcas3jw+BF0ZvBh1J4bNW/sO4LeSsErAHxb4O+LPxW+I//BBH9nr4WaZ8SPjVpvwH/ae/4LDeC/2Qfgn418TeKNY0z4w6v+wH4o+Pl9a+GND1nxek0Gs210Lfw9qvhOWazu/JbR9N/sKFjozmzoA/TX4Q/BLwH/wSm/4LH+Mfgp+y1deO9C/Zz+Mv/BKP4l/tE+Jvg54n+Ivjr4k6Dp/xk+BHxZg0vSvG+lSeOdf8Qatp02peFry50eeBNQWC6udR1eUfNNBHbgH4r+N/2OdJ8Lf8EJv2d/8AgrZ4W+KvxqsP+CifxR+MHw7+IvjT9oC9+MHxJ1pPFMf7QP7RV/8ADXWfB3iDwFceKh4JvvCUHhTxVY2d9o1vo1k+qPp+oQ3lz9k1zVbKcA/rH/4Jaf8ABLnV/wDgnLbzW+qfGbTvix5n7Kf7MP7NitYeB7nwdtuf2evHf7TXjO68XEXHijxEDB4qX9oO20+30YbZNIPhaa5l1HUP7WSGwAP/1v6+v24Ph1/wUb+JD/DrRv2D/wBo39n39nDRJYfFdp8YPFnxc+Dmt/F/xvCL1dFTwjqvwv0yLXtK8JLe6OqeIX1Kx8V28lre3FzpEkdwIba6t5wDS/4J2/sJeEf+Cev7Pc/wX0Hx/wCLvi/4u8YfEnx78bvjR8ZfHcdnbeLPi58afihqMGpeOfHmrafp+6y0s6i9pp9jY6ZDPePaabptml3qWqX5u9SuwDlP+Cn/AOw34g/b3/Zw0f4b/D74oQfBb40fCz4y/Cv9on4D/FG+0AeJ9J8JfFv4Qa8da8N3Gu6Es9rPf6JqFvcalpOoLa3Cz263sd6INQjtpNMvQD56/Yb/AGAP2u/BP7YfxI/b6/4KB/tDfB/44ftC+IfgFoP7MXw50D4EfDXW/AHw4+G/wp07xrL8QNda3k8S6te6zquu+LPE62V7qHmWlvDam3lRbm7heyt9NAPpCX9i3xHN/wAFTrT/AIKByeM9D/4Q+y/Yduf2VrfwALG/HiYeKbr41R/EybxS+o86W+gHR4101bYFNRXUsuUe1begB+en7U3/AATZ/wCCi+qf8FNfGf8AwUP/AGOPjn+xppGreIvgP4C+B3hLTP2p/hL8QPiJ4i+Emh+G5NQvfFMHw1vfDF9bWegxeNtZ1G71PWtTt5YdSuob670iRFsXuTfgH3T8W/hl/wAFWNf/AGZvgvpnwg/ak/ZW8BftdeGZ5Ln43+Jtd+AXijxJ8CvijHNDfRx6Z4X0a48YSeMfAMOnM+nXB1FG1m41eeC6H2XRradLaAA8L/Y+/wCCVfi7w54p/a6+PH/BRL4xeGf2w/2jP23/AIbaZ8DPjDD4Z8ES/Db4K+Fv2fNK0nUdIj+C3gPwvbai2sz6JqcGqXb694j1O6s9Z1RktJPJttRGpanq4B86+Ff+DcD9iH4U/wDBRL9nL9rv4JfC7wN8PPg58DvAHiSa7+DMOr/FHUdS1H4/WvirS9c+GPxU07V9U8ZaharZeFLIajBP4fu2TSmvLPSp20rUvPmfTwD3j9uf9hL/AIKN/t1eMvit8DtZ/bf+FXwE/wCCdXxOs9H0HxD4D+EXwRu9S/ag8XeArjRNMtvH3w9174neMNfu/C+gWXi7VU1mNvEfhvR2nTw9fwaReaFe266lBqQB+vfwx+HXhT4P/Db4f/CbwJYPpfgn4Y+CvC3w+8IabJPJdS2Hhjwbodj4e0K0mupiZrqa30zTrWKW5mLTXEitNKzO7NQB+Xn7fX/BPv8AaI+L/wC0f8D/ANun9hr9oTwX+z9+138Ffhp43+Bt2Pi54BufiR8Hfir8F/Heq23iK68JeLdJ028tNd0HUPDvim3PiPQ9c0L7RLPcym0v7dooLWa3AOz/AGBv+CfHjj9k34F/tG6V8VfjtP8AG79qH9sL4j/ED42/tA/Ge28OQ+EvDMvxO8c+FrHwnZ2ngPwdZS/8SPwb4N0fSdLstGtXnS8uvs89yY9NhltdM08A9d/4Jmfsf3v7A/7CH7Nv7Imra/onivW/gt4Fn0LxF4m8OWl9ZaHr3iTWPEWt+KvEOqaZb6ljUEtLrWdfvXhN4kU7ph2gg3CFAD8RNf8A+CDv7bR+FPxW/wCCfHgv9vb4aaD/AMEtPjJ8W/EPxL8SeDdR+AcWq/tS6B4Y8X/EuL4qeJPhNoHj2XVT4XvdPuvEUCC38f6sp8Qwb/Oj0ZLNG0WcA/Xf/got+w58Tf20v2b/AAH+yF8NPjcPgP8ABDX/ABb4T0H9pfU9P0281P4l+Nf2dPDulXEer/C/4ea6s8dp4c1rxreWujaPr+v6nDcRHw1Jq9vJBewXV1pGqgHK/ty/8EvPB37Rf7CXw7/Y8/Z18SaZ+zHe/s4eLPgt8Sf2VvEWmeHYtf8ADPw28efADUob/wAA/wBt+HZZY5te0W5gF9Za3JJdPqVxeag2v3T6pexT2uoAHj37Hv8AwTh/a0tP2vfiN+3R/wAFHv2ifhD8evjN4g/Zg/4ZA8AeCPgP8Ntc+Hvwv8EfCPUvGZ8d+Lr+4bxJqt5rGr+KfFOtIgnl+x20VhbXGowJd3VpLp1npYB8H+Cv+CE37c1r8Nvgv+wR8Rv24vhF4o/4JdfAX9oPQPjH4Y8C6f8ABjWLX9pbxl4K8G/FLU/iz4U+DXjLxnca23hS18Pwa/qItrrW9MW4voYLOzaytEsba10e3AP6q6AP/9k=">
                                            </div>
                                        </div>
                                        <div class="padding text-center">¿Cuántas unidades agregaste?</div>
                                        <input type="hidden" name="product[reference][]" value="{{$product->storeProduct->product->reference}}">
                                        <input type="hidden" name="product[picking_stock][]" value="{{$product->picking_stock}}">
                                        <input type="hidden" name="product[warehouse_storage][]" value="{{$product->warehouse_storage}}">
                                        <div class="padding-bottom">
                                            <select class="form-control" id="select_{{$product->storeProduct->product->reference}}" name="product[quantities][]" onchange="changePickingUnits({{$product->storeProduct->product->reference}})">
                                                <option value="">Seleccione una opción</option>
                                                <?php
                                                for ($i = 0 ; $i <= ($product->quantity_original == $product->quantity ? $product->quantity : $product->quantity_original == null ?  $product->quantity : $product->quantity_original - $product->quantity ); $i++) {
                                                ?>
                                                <option value="{{$i}}">{{$i}} unidad(es)</option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
{{--                                <div class="text-center padding">--}}
{{--                                    <label type="submit" onclick="submit_form()" class="btn btn-primary" id="send-form">--}}
{{--                                        ALISTAR FALTANTES--}}
{{--                                    </label>--}}
{{--                                </div>--}}
                            @else
                                <div class="text-center padding">
                                    No se encontraron productos
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <script>
        $(function () {
            var message =  '{{$message}}'
            if(message != ''){
                swal(
                    'Oops...',
                    message,
                    'error'
                );
            }
        });
        function changePickingUnits(reference) {
            var cantidad = $("#select_" + reference).val();
            $("#request_" + reference).html(cantidad);
        }

        function submit_form() {
            var error = false;
            $('select').each(function (index, element) {
                if ($(element).val() == "") {
                    error = true;
                }
            });
            if (error) {
                swal(
                    'Oops...',
                    'Debes seleccionar una cantidad para cada producto.',
                    'error'
                );
            } else {
                $('#send-form').removeAttr('onclick');
                $('#send-form').prop('disabled', true)
                $('#form').submit();
            }
        }
    </script>
@stop
@endif
