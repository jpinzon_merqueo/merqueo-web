@if ( !Request::ajax() )
    @extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <style>
        .Rojo {
            color: #f81616;
        }

        .Azul {
            color: #0061ec;
        }

        .warning-title {
            background-color: red;
            color: white;
            text-align: center;
            grid-row: 1 / span 3;
            width: 100%;
        }

        .padding {
            padding: 10px;
        }

        .border {
            border: #f2eeee 1px solid !important;
        }

        .duo {
            display: flex;
            justify-content: space-between;
            flex-basis: 80%;
            flex-direction: row;
            padding-bottom: 10px;
        }

        .duo2 {
            display: flex;
            justify-content: space-between;
            flex-basis: 100%;
            flex-direction: column;
            text-align: center;
        }

        .midle {
            width: 50%;
        }

        .content-text {
            font-size: 1em;
            display: grid;
            width: 100%;
            grid-template-columns: auto auto;
        }

        .box-route {
            background-color: #ffffff;
            border-radius: 5%;
            margin: 10px;
            overflow: hidden;
        }
    </style>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.min.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
    <script>
        $(function () {
            var message = '{{$message}}'
            var error = '{{$error}}'
            if (message != '') {
                if (error == 'si') {
                    swal(
                        'Ops...',
                        message,
                        'error'
                    );
                } else {
                    swal(
                        'Éxito',
                        message,
                        'success'
                    );
                }
            }
        });
    </script>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form id="search-form" class="orders-table">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-xs-6 col-lg-4 col-md-4">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <label for="city_id">Ciudad:</label>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-xs-12">
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    <option value="">Selecciona</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}"
                                                                @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-lg-4 col-md-4">
                                            <div class="row form-group">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <label for="warehouse_id">Bodega:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-12">
                                                    <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                        @foreach ($warehouses as $warehouse)
                                                            <option value="{{ $warehouse->id }}"
                                                                    @if( Session::get('admin_warehouse_id') == $warehouse->id ) selected="selected" @endif >{{ $warehouse->warehouse }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                                            <br>
                                            <button type="button" id="btn-search-orders" class="btn btn-primary">
                                                Buscar
                                            </button>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img class="loader unseen"
                                                                            src="{{ asset_url() }}img/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div>
                <div class="col-md-12 col-xs-12 padding">
                    <div class="orders-table-missings"></div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.missing_product.js.index-js')
@stop
@else
@section('orders')
    @if ( count($orders) || count($ordersWitoutPlannign) )
        <ul class="nav nav-tabs text-center box box-primary">
            <li class="active"><a data-toggle="tab" href="#am"><p>Franja:</p><p>AM</p></a></li>
            <li><a data-toggle="tab" href="#md"><p>Franja:</p><p>MD</p></a></li>
            <li><a data-toggle="tab" href="#pm"><p>Franja:</p><p>PM</p></a></li>
            <li><a data-toggle="tab" href="#sp"><p>Sin</p><p>planeación</p></a></li>
        </ul>
        <?php $am = '<div>';
        $md = '<div>';
        $pm = '<div>';
        $sp = '<div>'; ?>
        @foreach ($orders as  $shift => $order)
            @if($shift == "AM")
                @foreach ($order as  $route => $data)
                    @foreach ($data as $group => $info)
                        <?php
                        $routeOrders = route('adminMissingProducts.getOrdersFromRoute', $info['route_id']);
                        $routeProducts = route('adminMissingProducts.getProductsFromRoute', $info['route_id']);
                        $am .= '<div class="box-route"> <div class="duo2"> <p>Ruta</p><p><b>' . $route . '</b></p></div>';
                        $am .= '<div class="duo" >
                                    <div class="content-text">
                                        <div class="warning-title">
                                            <h1>' . $info['orders'] . '</h1>
                                            <p>Pedidos con productos faltantes</p>
                                        </div>
                                        <div class="border">Total productos faltantes: <span class="Rojo">' . $info['missing_products'] . '<span></div>
                                        <div class="border">Hora de despacho: ' . $info['delivery_time'] . '</div>
                                        <div class="border">Grupo de alistamiento: <span class="' . $group . '">' . $group . '</span></div>
                                    </div>
                                </div>
                                <div class="duo text-center">
                                    <div class="midle text-left"><a href="' . $routeOrders . '">Ver pedidos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                    <div class="midle text-left"><a href="' . $routeProducts . '">ver productos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                </div></div>';
                        ?>
                    @endforeach
                @endforeach
            @endif
            @if($shift == "MD")
                @foreach ($order as  $route => $data)
                    @foreach ($data as $group => $info)
                        <?php
                        $routeOrders = route('adminMissingProducts.getOrdersFromRoute', $info['route_id']);
                        $routeProducts = route('adminMissingProducts.getProductsFromRoute', $info['route_id']);
                        $md .= '<div class="box-route"> <div class="duo2"> <p>Ruta</p><p><b>' . $route . '</b></p></div>';
                        $md .= '<div class="duo" >
                                    <div class="content-text">
                                        <div class="warning-title">
                                            <h1>' . $info['orders'] . '</h1>
                                            <p>Pedidos con productos faltantes</p>
                                        </div>
                                        <div class="border">Total productos faltantes: <span class="Rojo">' . $info['missing_products'] . '<span></div>
                                        <div class="border">Hora de despacho: ' . $info['delivery_time'] . '</div>
                                        <div class="border">Grupo de alistamiento: <span class="' . $group . '">' . $group . '</span></div>
                                    </div>
                                </div>
                                <div class="duo text-center">
                                    <div class="midle text-left"><a href="' . $routeOrders . '">Ver pedidos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                    <div class="midle text-left"><a href="' . $routeProducts . '">ver productos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                </div></div>';
                        ?>
                    @endforeach
                @endforeach
            @endif
            @if($shift == "PM")
                @foreach ($order as  $route => $data)
                    @foreach ($data as $group => $info)
                        <?php
                        $routeOrders = route('adminMissingProducts.getOrdersFromRoute', $info['route_id']);
                        $routeProducts = route('adminMissingProducts.getProductsFromRoute', $info['route_id']);
                        $pm .= '<div class="box-route"> <div class="duo2"> <p>Ruta</p><p><b>' . $route . '</b></p></div>';
                        $pm .= '<div class="duo" >
                                    <div class="content-text">
                                        <div class="warning-title">
                                            <h1>' . $info['orders'] . '</h1>
                                            <p>Pedidos con productos faltantes</p>
                                        </div>
                                        <div class="border">Total productos faltantes: <span class="Rojo">' . $info['missing_products'] . '<span></div>
                                        <div class="border">Hora de despacho: ' . $info['delivery_time'] . '</div>
                                        <div class="border">Grupo de alistamiento: <span class="' . $group . '">' . $group . '</span></div>
                                    </div>
                                </div>
                                <div class="duo text-center">
                                    <div class="midle text-left"><a href="' . $routeOrders . '">Ver pedidos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                    <div class="midle text-left"><a href="' . $routeProducts . '">ver productos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                </div></div>';
                        ?>
                    @endforeach
                @endforeach
            @endif
        @endforeach

        @foreach( $ordersWitoutPlannign as $shift => $orders)
            @if($shift == "SP")
                @foreach ($orders as  $zone => $data)
                    @foreach ($data as $group => $info)
                        <?php
                        $routeOrders = route('adminMissingProducts.getOrdersFromZone', $info['zone_id']);
                        $routeProducts = route('adminMissingProducts.getProductsFromZone', $info['zone_id']);
                        $sp .= '<div class="box-route"> <div class="duo2"> <p>Zona</p><p><b>' . $zone . '</b></p></div>';
                        $sp .= '<div class="duo" >
                                    <div class="content-text">
                                        <div class="warning-title">
                                            <h1>' . $info['orders'] . '</h1>
                                            <p>Pedidos con productos faltantes</p>
                                        </div>
                                        <div class="border">Total productos faltantes: <span class="Rojo">' . $info['missing_products'] . '<span></div>
                                        <div class="border">Hora de despacho: ' . $info['delivery_time'] . '</div>
                                    </div>
                                </div>
                                <div class="duo text-center">
                                    <div class="midle text-left"><a href="' . $routeOrders . '">Ver pedidos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                    <div class="midle text-left"><a href="' . $routeProducts . '">ver productos <span class="glyphicon glyphicon-chevron-right"></span></a></div>
                                </div></div>';
                        ?>
                    @endforeach
                @endforeach
            @endif
        @endforeach
        <?php $am .= '</div>';
        $md .= '</div>';
        $pm .= '</div>';
        $sp .= '</div>';
        if ($am == '<div></div>') {
            $am = '<div class="text-center padding">No se encontraron faltantes para la franja horaria seleccionada.</div>';
        }
        if ($md == '<div></div>') {
            $md = '<div class="text-center padding">No se encontraron faltantes para la franja horaria seleccionada.</div>';
        }
        if ($pm == '<div></div>') {
            $pm = '<div class="text-center padding">No se encontraron faltantes para la franja horaria seleccionada.</div>';
        }
        if($sp == '<div></div>'){
            $sp = '<div class="text-center padding">No se encontraron faltantes sin planeación.</div>';
        }
        ?>
        <div class="tab-content">
            <div id="am" class="tab-pane fade in active">
                <?php echo $am; ?>
            </div>
            <div id="md" class="tab-pane fade">
                <?php echo $md; ?>
            </div>
            <div id="pm" class="tab-pane fade">
                <?php echo $pm;?>
            </div>
            <div id="sp" class="tab-pane fade">
                <?php echo $sp;?>
            </div>
        </div>
    @else
        <div class="text-center">No se encontraron pedidos con faltantes</div>
    @endif
@stop
@endif
