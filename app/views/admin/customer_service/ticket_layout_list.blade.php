@extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

    <section class="content-header">
        <h1>
            {{ empty($title) ? 'Plantillas de tickets' : $title }}
            <small>Panel de control</small>
        </h1>
    </section>
    <section class="content" style="position: relative;">
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('success') }}
            </div>
        @endif

        <div style="position: absolute; right: 15px">
            <a href="{{ route('customerService.TicketLayout.saveForm', 0) }}" class="btn btn-primary">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Agregar
            </a>
        </div>

        {{ Form::open(['route' => 'customerService.TicketLayout', 'method' => 'GET']) }}
        <div class="form-group col-md-4">
            {{ Form::label('ticket_solution_id', 'Tipo de solución', ['class' => 'control-label']) }}
            {{
                Form::fillSelectWithoutKeys(
                    'ticket_solution_id',
                    $solutions,
                    Input::get('ticket_solution_id'),
                    ['class' => 'form-control'],
                    true
                )
            }}
        </div>
        <div class="form-group col-lg-3">
            {{ Form::label('', 's', ['class' => 'control-label', 'style' => 'opacity: 0']) }}
            {{ Form::submit('Buscar', ['class' => 'btn btn-primary form-control']) }}
        </div>
        {{ Form::close() }}

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="paging">
                            <table id="orders-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tipo solución</th>
                                    <th>Nombre</th>
                                    <th>Asunto</th>
                                    <th style="width: 70px">Fecha creación</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody id="table-tickets-content">
                                @if ($layouts->isEmpty())
                                    <tr>
                                        <td colspan="6" align="center">No hay layouts disponibles.</td>
                                    </tr>
                                @else
                                    @foreach($layouts as $layout)
                                        <tr>
                                            <td>
                                                {{ $layout->id }}
                                            </td>
                                            <td>
                                                <span class="label label-default">
                                                    {{
                                                        empty($layout->solution)
                                                            ? '-'
                                                            : $layout->solution->name
                                                    }}
                                                </span>
                                            </td>
                                            <td>{{ $layout->name }}</td>
                                            <td>{{ $layout->email_subject }}</td>
                                            <td>
                                                {{ format_date('normal_with_time', $layout->created_at) }}
                                            </td>
                                            <td class="text-center" style="vertical-align: middle">

                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle"
                                                            type="button"
                                                            data-toggle="dropdown"
                                                            id="dropdownMenu1"
                                                            aria-haspopup="true"
                                                            aria-expanded="true">
                                                        Acciones
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu text-left" aria-labelledby="dropdownMenu1">
                                                        <li>
                                                            <a href="{{ action('admin\callcenter\TicketLayoutController@save_layout_form', $layout->id) }}">
                                                                Editar
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="delete-config"
                                                               href="{{ action('admin\callcenter\TicketLayoutController@delete_layout', $layout->id) }}">
                                                                Eliminar
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="open-modal"
                                                               data-title="Previsualización del template"
                                                               href="{{ action('admin\callcenter\TicketLayoutController@render_example_ajax', $layout->id) }}">
                                                                Previsualizar
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $layouts->links() }}
                        </div>
                        <div align="center" class="paging-loading unseen"><br><img
                                    src="{{ asset_url() }}/img/loading.gif"/></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Add Permissions modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var body = $('body');

            body.on('click', '.delete-config', function (event) {
                if (!confirm('¿Realmente desea eliminar este layout?')) {
                    event.preventDefault();
                }
            });

            body.on('click', '.open-modal', function (event) {
                event.preventDefault();
                var anchor = $(this);
                var url = anchor.prop('href');
                var title = anchor.data('title');
                var modalDiv = $('#modal-note');

                modalDiv.modal('show');
                modalDiv.find('h4').text(title);

                $.ajax(url)
                    .done(function (response) {
                        modalDiv.find('.modal-body').html(response);
                    })
            });
        });
    </script>
@endsection