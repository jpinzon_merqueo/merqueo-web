<div>
    <form id="assign-ticket-form"
          method="POST"
          data-is-reassign="{{ empty($ticket->activeCall) ? '0' : '1' }}"
          action="{{ action('admin\callcenter\TicketController@assign_ticket', $ticket->id) }}">
        <div class="form-group">
            {{ Form::label('admin_id', 'Seleccione el usuario que se encargará del ticket', ['class' => 'control-label']) }}
            {{ Form::fillSelectWithKeys('admin_id', $admins, null, ['class' => 'form-control'], 'id', 'fullname', true) }}
        </div>
        {{ Form::submit('Asignar', ['class' => 'btn btn-primary']) }}
    </form>

    {{--
        Para evitar que se agregue un nuevo listener al cargar el formulario
        la función que realiza el manejo del evento "submit" se realiza
        en el archivo "./admin-index.blade.php"
    --}}
</div>