<div class="form-group public-message col-md-6 solution-id-depend">
    {{ Form::label('ticket_layout_id', 'Plantilla del mail a enviar') }}
    {{ Form::fillSelectWithKeys('ticket_layout_id', $layouts, null, ['class' => 'form-control'], 'id', 'name', true) }}
</div>