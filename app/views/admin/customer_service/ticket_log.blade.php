<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID asignación</th>
        <th>Usuario</th>
        <th>Fecha asignación</th>
        <th>Fecha de cierre</th>
        <th>Estado</th>
        <th>Calificación</th>
        <th>Comentario</th>
    </tr>
    </thead>

    <tbody>
    @if(count($ticket->calls))
        @foreach($ticket->calls as $call)
            <tr>
                <th>{{ $call->id }}</th>
                <th>{{ $call->admin->fullname }}</th>
                <th>{{ format_date('normal_long_with_time', $call->created_at) }}</th>
                <th>
                    {{
                        $call->management_date
                            ? format_date('normal_long_with_time', $call->management_date) : '-'
                    }}
                </th>
                <th>{{ $call->status }}</th>
                <th>
                    @if ($call->user_score === null)
                        -
                    @elseif(empty($call->user_score))
                        Negativa
                    @else
                        Positiva
                    @endif
                </th>
                <th>{{ $call->comments ?: '-' }}</th>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7" style="text-align: center">
                No hay información asociada.
            </td>
        </tr>
    @endif
    </tbody>
</table>