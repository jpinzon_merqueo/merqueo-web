@extends('admin.layout')

@section('content')
<link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
<section class="content-header">
	<h1>
		Pedido agrupado #{{ $order_group->id }}
		<small>Control panel</small>
	</h1>
</section>
<section class="content">
	@if(Session::has('error') )
	<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Alerta!</b> {{ Session::get('error') }}
	</div>
	@endif
	@if(Session::has('success') )
	<div class="alert alert-success">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Alerta!</b> {{ Session::get('success') }}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-8">
			<div class="row">
				<div class="col-xs-6">
					<div class="callout callout-info">
						<h4>Datos de cliente</h4>
						<div class="row">
							<div class="col-xs-12">
								<table class="table table-hover">
									<tr>
										<td>
											<strong>Nombre:</strong>
										</td>
										<td>
											{{ $order_group->user_firstname }} {{ $order_group->user_lastname }}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Email:</strong>
										</td>
										<td>
											{{ $order_group->user_email }}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Dirección:</strong>
										</td>
										<td>
											{{ $order_group->user_address }}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Ciudad:</strong>
										</td>
										<td>
											{{ $city->city }}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Celular:</strong>
										</td>
										<td>
											{{ $order_group->user_phone }}
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="callout callout-info">
						<h4>Datos de pedido agrupado</h4>
						<div class="row">
							<div class="col-xs-12">
								<table class="table table-hover">
									<tr>
										<td>
											<strong>Fecha:</strong>
										</td>
										<td>
											{{ date("d M Y g:i a",strtotime($order_group->created_at)) }}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Origen:</strong>
										</td>
										<td>
											{{ $order_group->source }} @if ( !empty($order_group->source_os) ) - {{ $order_group->source_os }}  @endif
										</td>
									</tr>
									<tr>
										<td>
											<strong>Subtotal:</strong>
										</td>
										<td>
											{{ $total_amount }}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Domicilio:</strong>
										</td>
										<td>
											{{ $delivery_amount}}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Descuento:</strong>
										</td>
										<td>
											{{ $discount_amount}}
										</td>
									</tr>
									<tr>
										<td>
											<strong>Total:</strong>
										</td>
										<td>
											{{ $grand_total}}
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h4>
						Pedidos por tienda
					</h4>
				</div>
			</div>
			@foreach ($orders as $order)
			<div class="row">
				<div class="col-xs-12">
					<div class="callout callout-info">
						<div class="row form-group">
							<div class="col-xs-12">
								<h3>
									Pedido #{{ $order->id }}
								</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="row">
									<div class="col-xs-12">
										<h4>Detalles de pedido</h4>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<table class="table table-hover">
											<tr>
												<td>
													<strong>Tienda:</strong>
												</td>
												<td>
													{{ $order->name }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Estado:</strong>
												</td>
												<td>
													{{ $order->status }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Referencia:</strong>
												</td>
												<td>
													{{ $order->reference }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Fecha de entrega:</strong>
												</td>
												<td>
													{{ date("d M Y g:i a",strtotime($order->delivery_date)) }}
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="row">
									<div class="col-xs-12">
										<h4>Detalles de pago</h4>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<table class="table table-hover">
											<tr>
												<td>
													<strong>Método de pago:</strong>
												</td>
												<td>
													{{ $order->payment_method }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Subtotal:</strong>
												</td>
												<td>
													${{ number_format($order->total_amount, 0, ',', '.') }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Domicilio:</strong>
												</td>
												<td>
													${{ number_format($order->delivery_amount, 0, ',', '.') }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Descuento:</strong>
												</td>
												<td>
													${{ number_format($order->discount_amount, 0, ',', '.') }}
												</td>
											</tr>
											<tr>
												<td>
													<strong>Total:</strong>
												</td>
												<td>
													${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="row">
									<div class="col-xs-12">
										<h4>Productos de pedido</h4>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-3 text-right">
										<button type="button" class="btn btn-primary products-view" data-orderid="{{ $order->id }}">Ver Productos</button>
									</div>
									<div class="col-xs-9">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="col-xs-4">
			<fieldset>
				<legend>{{ $customer_service->description }}</legend>
				<form id="call_form" action="{{ route('AdminCallCenter.saveAttemps', ['group_id' => $order_group->id, 'service_id' => $service_id]) }}" method="POST">
					<div class="row">
						<div class="col-xs-12">
							<strong>Cliente contesta </strong>
							<label>
								<input id="costumer-answer" name="costumer-answer" type="checkbox" value="1">
							</label>
						</div>
					</div>
					<hr>
					<div class="row comments-container unseen">
						<div class="col-xs-6" style="float: none; display: inline-block;">
							<div class="form-group">
								<label for="comments">Comentarios</label>
								<textarea style="display: inline-block" id="comments" name="comments" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-xs-6" style="float: none; display: inline;">
							<input style="display: inline-block" id="submit" type="submit" value="Guardar" class="btn btn-primary">
						</div>
					</div>
					@if ( $service_id == 2 )
					<div id="user_score_container" class="row form-group unseen">
						<div class="col-xs-6">
							<strong>Calificación del cliente</strong>
							<select id="user_score" name="user_score" class="form-control" required="">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
					</div>
					@endif
					<div class="row form-group">
						<div class="col-xs-12">
							<strong>Cliente no contesta</strong>
							<img class="u-loader" src="{{ asset('assets/img/loading.gif') }}" alt="" style="display: none;">
							<div class="form-inline">
								@if ( !empty($order_call_center) )
									@for ($i = 1; $i <= $allowed_attemps; $i++)
									<div id="attemp-container-{{ $i }}" class="form-group" style="margin-right: 1rem;">
										<label style="font-weight: normal;">
											<input id="attemp-{{ $i }}" name="costumer-not-answer" type="checkbox" value="{{ $i }}" data-attemp="No contesta" @if( !empty($order_call_center) && $order_call_center->attemps >= $i) checked="" readonly="true" disabled="" @endif> Intento #{{ $i }}
										</label>
									</div>
									@endfor
								@else
									@for ($i = 1; $i <= $allowed_attemps; $i++)
									<div id="attemp-container-{{ $i }}" class="form-group @if( $i != 1 ) hidden @endif" style="margin-right: 1rem;">
										<label style="font-weight: normal;">
											<input id="attemp-{{ $i }}" name="costumer-not-answer" type="checkbox" value="{{ $i }}" data-attemp="No contesta"> Intento #{{ $i }}
										</label>
									</div>
									@endfor
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 error">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<input type="hidden" name="status" id="status" value="" required="">
						</div>
					</div>
				</form>
			</fieldset>
		</div>
	</div>
</section>
<div class="hidden">
	<div id="products-table">
			<div class="col-lg-12">
				<table class="table table-bordered table-striped dataTable">
					<thead>
						<tr>
							<th>
								No.
							</th>
							<th>
								Imagen
							</th>
							<th>
								Nombre
							</th>
							<th>
								Cantidad
							</th>
							<th>
								Unidades
							</th>
							<th>
								Precio original
							</th>
							<th>
								Precio
							</th>
						</tr>
					</thead>
					<tbody class="products-content">
					</tbody>
					<tfoot class="products-totals">
					</tfoot>
				</table>
			</div>
	</div>
</div>

<script type="text/javascript">
	$('body').on('click', '.products-view', function(event) {
		$('.products-view').prop('disabled', true);
		event.preventDefault();
		var order_id = $(this).data('orderid');
		$.ajax({
			url: '{{ route('AdminCallCenter.getOrderProductsAjax') }}',
			type: 'GET',
			dataType: 'json',
			data: {
				order_id: order_id
			},
		})
		.done(function(data) {
			var modal = $('#products-table');
			var html = '';
			$('.products-content').empty();
			$('.products-totals').empty();
			$.each(data.products, function(index, val) {
				html += '<tr>';
				html += '	<td align="left">';
				html += '		'+(index+1);
				html += '	</td>';
				html += '	<td>';
				html += '		<img src="'+ val.product_image_url +'" alt="'+ val.product_name +'" width="100" />';
				html += '	</td>';
				html += '	<td>';
				html += '		'+val.product_name;
				html += '	</td>';
				html += '	<td>';
				html += '		'+val.product_quantity;
				html += '	</td>';
				html += '	<td>';
				html += '		'+val.product_unit;
				html += '	</td>';
				html += '	<td>';
				html += '		'+val.original_price;
				html += '	</td>';
				html += '	<td>';
				html += '		'+val.price;
				html += '	</td>';
				html += '</tr>';
			});
			$('.products-content').append(html);
			html = '';
			html += '<tr>';
			html += '	<td colspan="5" style="font-weight: bold;">';
			html += '		Total';
			html += '	</td>';
			html += '	<td colspan="2">';
			html += '		'+data.total_amount;
			html += '	</td>';
			html += '</tr>';
			html += '<tr>';
			html += '	<td colspan="5" style="font-weight: bold;">';
			html += '		Descuento / Crédito';
			html += '	</td>';
			html += '	<td colspan="2">';
			html += '		'+data.discount_amount;
			html += '	</td>';
			html += '</tr>';
			html += '<tr>';
			html += '	<td colspan="5" style="font-weight: bold;">';
			html += '		Costo de envío';
			html += '	</td>';
			html += '	<td colspan="2">';
			html += '		'+data.delivery_amount;
			html += '	</td>';
			html += '</tr>';
			html += '<tr>';
			html += '	<td colspan="5" style="font-weight: bold;">';
			html += '		Total a pagar';
			html += '	</td>';
			html += '	<td colspan="2">';
			html += '		'+data.total;
			html += '	</td>';
			html += '</tr>';
			$('.products-totals').append(html);
			$.fancybox(modal);
			$('.products-view').prop('disabled', false);
		})
		.fail(function() {
			$('.products-view').prop('disabled', false);
			console.log("error al consultar los productos de la orden.");
		});
	});

	$('input[name="costumer-not-answer"]:checkbox:enabled').on('ifChecked', function () {
		$('.u-loader').fadeIn();
		var attemps = $(this).val();
		$.ajax({
			url: '{{ route('AdminCallCenter.saveAttemps', ['group_id' => $order_group->id, 'service_id' => $service_id]) }}',
			type: 'POST',
			dataType: 'json',
			data: {
				'costumer-not-answer': attemps,
				status: 'No contesta'
			},
		})
		.done(function(data) {
			if (data) {
				if (data.redirect_url) {
					window.location.replace(data.redirect_url);
					return;
				}
				$('input[name="costumer-not-answer"]:checkbox:enabled:checked').iCheck('disable').parent().parent().parent().next('div').removeClass('hidden');
			}
		})
		.fail(function() {
			console.log("error al guardar intentos.");
		});
		$('.u-loader').fadeOut();

		$('#status').val('No contesta');
		$('input#costumer-answer:checkbox').iCheck('uncheck');
	});
	$('input#costumer-answer:checkbox').on('ifChecked', function () {
		$('.comments-container').slideDown();
		$('input[name="costumer-not-answer"]:checkbox:enabled').iCheck('uncheck');
		$('#user_score_container').slideDown();
		$('#status').val('Contesta');
	});
	$('input#costumer-answer:checkbox').on('ifUnchecked', function () {
		$('.comments-container').slideUp();
		$('#user_score_container').slideUp();
		$('#status').val('No contesta');
	});
	$('#submit').click(function(event) {
		$('.error').empty();
		if ( $('input:checkbox:enabled:checked').length == 0 ) {
			$('.error').text('Debe seleccionarl al menos una de las opciones');
			return false;
		}else{
			return true;
		}
	});
</script>
@endsection
