<!DOCTYPE html>
<html lang="es" ng-app="merqueoRate">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Merqueo - Calificación de pedido</title>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <link href="{{ asset_url() }}css/user_score.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body style="text-align: center">
<div layout="row" layout-align="center center" class="navbar navbar-default">
    <a href="/">
        <img src="{{ asset_url() }}img/order_rate/Logo.png" alt="">
    </a>
</div>
<div layout="row" flex="100">
    <div ui-view="" flex="80" flex-offset="10">
        <div layout="column" layout-align="center center">
            <img src="{{ asset_url() }}img/order_rate/ilustracion-thankyou.png" alt="" class="than-you-page-image">
            <h1 class="rate-title">¡Gracias!</h1>
            <p class="rate-message">
                {{ $message }}
            </p>

            <a href="/" style="text-underline: none">
                <button class="md-button md-ink-ripple md-primary md-raised rate-button"
                        style="width: 350px">
                    Ir al home
                </button>
            </a>
        </div>
    </div>
</div>
<script src="{{ asset_url() }}js/angular.min.js"></script>
<!-- AngularJS Material Dependencies -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.7/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.7/angular-aria.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.7/angular-messages.min.js"></script>

<!-- AngularJS Material Javascript now available via Google CDN; version 1.1.4 used here -->
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.6/angular-material.min.js"></script>
<script>
    angular.module('merqueoRate', ['ngMaterial'])
</script>
</body>
</html>
