@extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

    <section class="content-header">
        <h1>
            {{ empty($title) ? 'Customer Service' : $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <strong style="font-size: 2rem;">Estado: </strong>

            {{-- Botónes de sesión --}}
            <span id="stop-session" style="display: none">
                <span class="label label-success" style="font-size: 15px; margin-right: 3rem;">Recibiendo tickets</span>
                <button type="submit" class="btn btn-danger action-buttons" onclick="stopReceiving()">Dejar de recibir tickets</button>
            </span>
            <span id="start-session" style="display: none">
                <span class="label label-danger"
                      style="font-size: 15px; margin-right: 3rem;">En Pausa</span>
                <button type="submit" class="btn btn-success action-buttons" onclick="startTimer(true)">
                    Recibir tickets
                </button>
            </span>

	    </span>
    </section>
    <section class="content">
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <form class="filter-form form-horizontal" action="{{ action('admin\callcenter\TicketController@index') }}">
                    <div class="form-group col-md-3">
                        {{ Form::label('wildcard', 'Busqueda: ', ['class' => 'col-md-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::text('wildcard', Input::get('wildcard'), ['class' => 'form-control col-md-10', 'placeholder' => '# Ticket, referencia ,pedido, usuario, email']) }}
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        {{ Form::label('status', 'Estado: ', ['class' => 'col-lg-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::select('status', ['' => 'Seleccione'] + $status, Input::get('status'), ['class' => 'form-control'], true) }}
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        {{ Form::label('crated_at', 'Fecha: ', ['class' => 'col-lg-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::text('created_at', empty($date_filter) ? '' : $date_filter->format('d/m/Y'), ['class' => 'form-control', 'placeholder' => 'Creación del ticket'] ) }}
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        {{ Form::submit('Buscar', ['class' => 'btn btn-primary']) }}
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="paging">
                            <table id="orders-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID Ticket</th>
                                    <th>Tipo servicio</th>
                                    <th>Ciudad</th>
                                    <th class="text-center">ID Orden</th>
                                    <th>Nombre completo</th>
                                    <th style="width: 70px">Fecha creación</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody id="table-tickets-content">
                                @if ($calls->isEmpty())
                                    <tr>
                                        <td colspan="14" align="center">No hay tickets disponibles.</td>
                                    </tr>
                                @else
                                    @foreach($calls as $index => $call)
                                        <tr class="{{ $index === 0 ? '' : 'disabled' }}">
                                            <td style="vertical-align: middle">
                                                {{ $call->ticket->id }}
                                            </td>
                                            <td style="vertical-align: middle">
											<span class="label label-default"
                                                  style="background-color: {{ $call->ticket->customerService->color }}">
												{{ $call->ticket->customerService->description }}
											</span>
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{
                                                    !empty($call->ticket->order->orderGroup->city->city)
                                                        ? $call->ticket->order->orderGroup->city->city : 'No disponible'
                                                }}
                                            </td>
                                            <td style="vertical-align: middle">
                                                @if(!empty($call->ticket->order->id))
                                                    <a href="{{ action('admin\AdminOrderStorageController@details', $call->ticket->order->id) }}"
                                                       target="_blank">
                                                        # {{ $call->ticket->order->id }}
                                                    </a>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle" width="150">
                                                @if (empty($call->ticket->order->user))
                                                    <span style="color: #ff5136;">Eliminado</span>
                                                @else
                                                    {{ $call->ticket->order->user->first_name}} {{ $call->ticket->order->user->last_name }}
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{ format_date('normal_with_time', $call->ticket->created_at) }}
                                            </td>
                                            <td class="text-center" style="vertical-align: middle">
                                                <div class="btn-group">
                                                    @if($index === 0)
                                                        <a class="btn btn-xs btn-default open-modal"
                                                           data-title="Gestionar ticket"
                                                           href="{{ action('admin\callcenter\TicketController@update_status_form', $call->id) }}">
                                                            <span class="glyphicon glyphicon-folder-open"></span>
                                                        </a>
                                                    @else
                                                        <a class="btn btn-xs btn-default" href="#">
                                                            <span class="glyphicon glyphicon-folder-open"></span>
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div align="center" class="paging-loading unseen"><br><img
                                    src="{{ asset_url() }}/img/loading.gif"/></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Add Permissions modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Nota a Pedido</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        /**
         * Segundos necesarios para que se soliciten nuevos tickets;
         * @type {number}
         */
        var TIMER_SECONDS_FRECUENCY = 30;
        var interval = null;
        var isSessionActive = {{ Session::get(\admin\callcenter\TicketController::RECEIVE_TICKETS) ? 'true' : 'false' }};
        var isMakingARequets = false;

        // Este archivo se divide en dos secciones debido a que en necesario
        // registrar unicamente una vez los listener asociados al formulario
        // de cambio de estado del "ticket_call".

        // Eventos y funciones asociados a la ventana actual.
        $(document).ready(function () {
            var body = $('body');

            if (isSessionActive) {
                startTimer();
            }

            toggleButtons(isSessionActive);

            body.on('click', '.open-modal', function (event) {
                event.preventDefault();
                var anchor = $(this);
                var url = anchor.prop('href');
                var title = anchor.data('title');
                var modalDiv = $('#modal-note');

                modalDiv.modal('show');
                modalDiv.find('h4').text(title);

                $.ajax(url)
                    .done(function (response) {
                        modalDiv.find('.modal-body').html(response);
                    })
            });
        });

        /**
         * Inicializa el timer y de ser necesario realiza
         * una petición inical que es util en caso de que el
         * usuario cambie de "En pausa" a "Recibiendo tickets".
         *
         * @param {boolean=} forceBeginWithRequest
         */
        function startTimer(forceBeginWithRequest) {
            if (forceBeginWithRequest) {
                receiveTickets();
            }

            interval = setInterval(receiveTickets, 1000 * TIMER_SECONDS_FRECUENCY);
        }

        /**
         * Carga tickets al usuario y el contenido de la tabla con el
         * listado de los tickets.
         *
         * @param fulfill
         * @param reject
         */
        function receiveTickets(fulfill, reject) {
            isSessionActive = true;
            if (isMakingARequets) {
                return true;
            }
            makingARequest(true);
            $.ajax({
                url: '{{ action('admin\callcenter\TicketController@user_receive_tickets') }}',
                type: 'POST'
            }).done(function (response) {
                var html = $(response).find('#table-tickets-content').html();
                $('#table-tickets-content').html(html);
                toggleButtons(true);
                if (fulfill instanceof Function) fulfill();
            }).fail(function (error) {
                console.log(error);
                toggleButtons(true);
                if (reject instanceof Function) reject();
            }).always(function () {
                makingARequest(false);
            });
        }

        function makingARequest(value) {
            isMakingARequets = value;
            if (value) {
                $('.action-buttons').addClass('disabled');
            } else {
                $('.action-buttons').removeClass('disabled');
            }
        }

        /**
         * Deja de recibir tickets
         */
        function stopReceiving() {
            clearInterval(interval);

            $.ajax({
                url: '{{ action('admin\callcenter\TicketController@stop_receiving_tickets') }}'
            }).done(function () {
                toggleButtons(false);
                isSessionActive = false;
            }).fail(function (response) {
                if (response.responseJSON && response.responseJSON.error) {
                    alert(response.responseJSON.error.message);
                }
            })
        }

        /**
         * Refresca el contenido sin necesidad de agregar tickets al usuario.
         */
        function refreshContent() {
            $.ajax({
                url: window.location.href
            }).done(function (response) {
                var html = $(response).find('#table-tickets-content').html();
                $('#table-tickets-content').html(html);
            }).fail(function (response) {
                if (response.responseJSON && response.responseJSON.error) {
                    alert(response.responseJSON.error.message);
                }
            })
        }

        /**
         * Cambia el estado de los botones.
         *
         * @param {boolean} isSessionActive
         */
        function toggleButtons(isSessionActive) {
            var stopSessionDiv = $('#stop-session');
            var startSessionDiv = $('#start-session');

            if (isSessionActive) {
                stopSessionDiv.show();
                startSessionDiv.hide();
            } else {
                stopSessionDiv.hide();
                startSessionDiv.show();
            }
        }

        // Eventos y funciones asociados al formulario de cambio de estado de "ticket_call".
        $(document).ready(function () {
            var body = $('body');

            $('.filter-form input[name="created_at"]').datetimepicker({
                format: 'DD/MM/YYYY',
                maxDate: new Date()
            });

            body.on('click', '#show-details', function (event) {
                event.preventDefault();
                var anchor = $(this);
                var defaultText = 'Ver calificación del pedido';
                anchor.text(anchor.text() === defaultText ? 'Ocultar' : defaultText);
                $('#order-details').toggle();
            });

            body.on('change', '#called', function () {
                var selectedValue = parseInt($(this).find(':selected').val(), 10);
                var dependentDivs = $('.called-depend');

                if (selectedValue) {
                    dependentDivs.show();
                } else {
                    dependentDivs.hide();
                }
            });

            body.on('change', '#status_ticket', function () {
                var selectedValue = $(this).find(':selected').val();
                var dependedDivs = $('.status-ticket-depend');

                if (selectedValue === '{{ \tickets\Ticket::SOLVED }}' || selectedValue === '{{ \tickets\Ticket::RESOLVED }}') {
                    dependedDivs.show();
                } else {
                    dependedDivs.hide();
                }
            });

            body.on('click', '.template-action', function () {
                var content = $(this).data('content');
                $('#comments').html(content);
            });

            body.on('click', '.order', function(){
                $('#solution_identifier').val($(this).val());
                $('.reference').val($(this).data('reference'));
            });

            body.on('click', '#order-complaint', function(){
                if ($('#solution_identifier').val() == "") {
                    alert("Debe seleccionar el pedido al cual se va a generar reclamo.");
                    return false;
                } else {
                    var url = "{{ route('adminOrderComplaint.orderComplaint', 'not-found') }}";
                    var url_order = url.replace('not-found', $('#solution_identifier').val());
                    window.open(url_order, '_blank');
                }
            });

            body.on('change', '#solution_id', function () {
                var isComplaint = false;
                var selectedValue = $(this).find(':selected').val();
                var solutionIdGroup = $('.solution_identifier_group');
                var amountGroup = $('.amount_group');
                var expirationDateGroup = $('.expiration_date_group');
                var solutionDependentDivs = $('.solution-id-depend');

                solutionDependentDivs[selectedValue ? 'show' : 'hide']();

                switch (selectedValue) {
                    case '{{ \tickets\TicketSolution::COMPLAINT_ORDER }}':
                        isComplaint = true;
                    case '{{ \tickets\TicketSolution::VOUCHER }}':
                        var action = isComplaint ? ['show', 'hide'] : ['hide', 'show'];
                        solutionIdGroup.find('label').text(isComplaint ? '# Pedido' : 'Código del cupón');
                        solutionIdGroup.show();
                        amountGroup.hide();
                        expirationDateGroup.hide();
                        $('#complaint-order-link')[action[0]]();
                        $('#coupon-link')[action[1]]();
                        break;

                    case '{{ \tickets\TicketSolution::CREDIT }}':
                        solutionIdGroup.hide();
                        amountGroup.show();
                        expirationDateGroup.show();
                        expirationDateGroup.find('label').text('Fecha de expiración del crédito');
                        break;

                    case '{{ \tickets\TicketSolution::FREE_DELIVERY }}':
                        solutionIdGroup.hide();
                        amountGroup.hide();
                        expirationDateGroup.show();
                        expirationDateGroup.find('label').text('Fecha de expiración de proximo pedido con domicilio gratis');
                        break;

                    case '{{ \tickets\TicketSolution::OTHER }}':
                    default:
                        solutionIdGroup.hide();
                        amountGroup.hide();
                        expirationDateGroup.hide();
                }

                if (selectedValue) {
                    var url = '{{ route('customerService.Ticket.selectSolutionLayout', 'REPLACE') }}';
                    $.ajax({url: url.replace('REPLACE', selectedValue)})
                        .done(function (response) {
                            $('#email-content-templates').html(response);
                        })
                }
            });

            body.on('click', '#preview-anchor', function (event) {
                event.preventDefault();
                if (!$('#complaint-order-form').valid()) {
                    return true;
                }
                var button = $(this);
                var href = button.data('href');
                var serializedForm = $('#complaint-order-form').serialize();

                $.ajax({
                    url: href,
                    data: serializedForm
                })
                    .done(function (response) {
                        var newWindow = window.open();

                        if (!newWindow) {
                            alert('Debes habilitar la ventanas emergentes en la parte superior derecha del navegador.');
                            return true;
                        }

                        var body = newWindow.document.body;
                        body.style.width = '60%';
                        body.style.margin = '5% auto';
                        body.style.fontFamily = 'arial,sans-serif';
                        body.innerHTML = response;
                    })
                    .fail(function (response) {
                        var message = getMessage(response);
                        if (message) {
                            alert(message);
                        }
                    });
            });

            body.on('submit', '#complaint-order-form', function (event) {
                event.preventDefault();

                if (!confirm('¿Estás seguro de gestionar este ticket?')) {
                    return true;
                }

                var form = $(this);
                var url = form.prop('action');
                var serializedForm = form.serialize();

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: serializedForm
                }).done(function (response) {
                    var modalDiv = $('#modal-note');
                    modalDiv.modal('hide');
                    isSessionActive
                        ? receiveTickets()
                        : refreshContent();
                }).fail(function (response) {
                    var message = getMessage(response);
                    if (message) {
                        alert(message);
                    }
                })
            });
        });

        function getMessage(response) {
            if (response.responseJSON && response.responseJSON.error) {
                return response.responseJSON.error.message;
            }
        }
    </script>

    <style>
        tr.disabled {
            opacity: 0.6;
        }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
@endsection
