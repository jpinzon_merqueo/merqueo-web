@if (isset($render_permissions))
    <table class="table">
        <thead>
        <tr>
            <th>Nombre Completo</th>
            <th>Permisos</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->fullname }}</td>
                <td>
                    <form action="{{ action('admin\callcenter\CustomerServiceController@assign_services_ajax', $user->id) }}"
                          class="col-md-10 user-permissions">
                        @foreach($services as $service)
                            <div class="col-md-2">
                                <label>
                                    <input type="checkbox"
                                           value="{{ $service->id }}"
                                           {{ $user->hasService($service) ? 'checked' : '' }}
                                           name="services[]"> {{ $service->description }}
                                </label>
                            </div>
                        @endforeach
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        $(document).ready(function () {
            $('body').on('submit', '.user-permissions', function (event) {
                event.preventDefault();
                var form = $(this);
                var formSerialized = form.serialize();
                var action = form.prop('action');
                var button = form.find('button');
                if (button.hasClass('disabled')) {
                    return true;
                }

                button.addClass('disabled');
                button.text('Guardando...');

                $.ajax({
                    url: action,
                    type: 'POST',
                    data: formSerialized,
                    dataType: 'json'
                }).done(function (response) {
                    button.removeClass('disabled');
                    button.text('Guardar');

                    button.append(' <i class="fa fa-check saved" aria-hidden="true" style="color: white"></i>');
                    setTimeout(function () {
                        var check = $('.saved');
                        check.hide(function () {
                            check.remove();
                        });
                    }, 3000);
                })
            });
        });
    </script>
@endif