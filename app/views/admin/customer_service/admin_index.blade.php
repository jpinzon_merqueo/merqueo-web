@extends('admin.layout')

@section('content')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
            <a class="btn btn-primary open-modal"
               data-modal-title="Asignación de permisos"
               href="{{ action('admin\callcenter\CustomerServiceController@users_table_ajax') }}">
                Gestionar permisos
            </a>
	    </span>
    </section>
    <section class="content">
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if(Session::has('success') )
            <div class="alert alert-success">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <form class="filter-form form-horizontal" action="{{ action('admin\callcenter\CustomerServiceController@index') }}">
                    <div class="form-group col-md-4">
                        {{ Form::label('wildcard', 'Busqueda: ', ['class' => 'col-md-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::text('wildcard', Input::get('wildcard'), ['class' => 'form-control col-md-10', 'placeholder' => '# Pedido, usuario, email']) }}
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::label('status', 'Estado: ', ['class' => 'col-lg-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::select('status', ['' => 'Seleccione'] + $status, Input::get('status'), ['class' => 'form-control'], true) }}
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::label('admin_id', 'Usuario', ['class' => 'col-lg-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::select('admin_id', ['' => 'Seleccione'] + $admins, Input::get('admin_id'), ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::label('crated_at', 'Fecha: ', ['class' => 'col-lg-3 control-label']) }}
                        <div class="col-md-9">
                            {{ Form::text('created_at', empty($date_filter) ? '' : $date_filter->format('d/m/Y'), ['class' => 'form-control', 'placeholder' => 'Creación del ticket'] ) }}
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::submit('Buscar', ['class' => 'btn btn-primary']) }}
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="paging">
                            <table id="orders-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID Ticket</th>
                                    <th>Tipo servicio</th>
                                    <th>Ciudad</th>
                                    <th class="text-center"># Pedido</th>
                                    <th>Nombre completo</th>
                                    <th>
                                        <a href="{{
                                            action(
                                                'admin\callcenter\CustomerServiceController@index',
                                                [
                                                    'order_by' => 'tickets.created_at',
                                                    'order' => (\Input::get('order') === 'asc' ? 'desc' : 'asc')
                                               ] + \Input::all()
                                            ) }}">
                                            <i class="fa fa-arrow-{{ \Input::get('order') === 'asc' ? 'down' : 'up' }}" aria-hidden="true"></i>
                                            Fecha creación
                                        </a>
                                    </th>
                                    <th>Usuario asignado</th>
                                    <th>Estado</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($tickets))
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td style="vertical-align: middle">
                                                {{ $ticket->id }}
                                            </td>
                                            <td style="vertical-align: middle">
											<span class="label label-default"
                                                  style="background-color: {{ $ticket->customerService->color }}">
												{{ $ticket->customerService->description }}
											</span>
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{
                                                    !empty($ticket->order->orderGroup->city->city)
                                                        ? $ticket->order->orderGroup->city->city : 'No disponible'
                                                }}
                                            </td>
                                            <td style="vertical-align: middle">
                                                @if(!empty($ticket->order->id))
                                                    <a href="{{ action('admin\AdminOrderStorageController@details', $ticket->order->id) }}"
                                                       target="_blank">
                                                        # {{ $ticket->order->id }}
                                                    </a>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle" width="150">
                                                @if (empty($ticket->order->user))
                                                    <span style="color: #ff5136;">Eliminado</span>
                                                @else
                                                    {{ $ticket->order->user->first_name}} {{ $ticket->order->user->last_name }}
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{ format_date('normal_with_time', $ticket->created_at) }}
                                            </td>
                                            <td style="vertical-align: middle">
                                                @if ($ticket->isSolved())
                                                    @if (!empty($ticket->lastCall->admin))
                                                        {{ $ticket->lastCall->admin->fullname }}
                                                    @else
                                                        <span style="color: #ff5136;">
                                                            No hay un registro activo
                                                        </span>
                                                    @endif
                                                @else
                                                    @if (!empty($ticket->activeCall->admin))
                                                        {{ $ticket->activeCall->admin->fullname }}
                                                    @else
                                                        Sin asignar
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle">
                                                <span class="label label-<?php
                                                switch ($ticket->status) {
                                                    case \tickets\Ticket::PENDING:
                                                        echo 'warning';
                                                        break;

                                                    case \tickets\Ticket::ASSIGNED:
                                                        echo 'primary';
                                                        break;

                                                    case \tickets\Ticket::SOLVED:
                                                        echo 'success';
                                                        break;
                                                    case \tickets\Ticket::RESOLVED:
                                                        echo 'success';
                                                        break;
                                                    case \tickets\Ticket::CREATED:
                                                    default:
                                                        echo 'default';;
                                                }
                                                ?>">
                                                    {{ $ticket->status }}
                                                </span>
                                            </td>
                                            <td class="text-center" style="vertical-align: middle; text-align: left">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle"
                                                            type="button"
                                                            data-toggle="dropdown"
                                                            id="dropdownMenu1"
                                                            aria-haspopup="true"
                                                            aria-expanded="true">
                                                        Acciones
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                        <li>
                                                            <a class="open-modal"
                                                               data-modal-title="Asignación del ticket"
                                                               href="{{ action('admin\callcenter\TicketController@assign_ticket_form', $ticket->id) }}">
                                                                Asignar
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="open-modal"
                                                               data-modal-title="Historial de ticket"
                                                               href="{{ action('admin\callcenter\TicketController@ticket_logs', $ticket->id) }}">
                                                                Ver historico
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="14" align="center">Orders not found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {{ $tickets->links() }}
                        </div>
                        <div align="center" class="paging-loading unseen">
                            <br><img src="{{ asset_url() }}/img/loading.gif">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        /**
         * Segundos necesarios para que se soliciten nuevos tickets;
         * @type {number}
         */
        var TIMER_SECONDS_FRECUENCY = 60;
        var interval = null;

        $(document).ready(function () {
            var body = $('body');

            interval = setInterval(receiveTicketsFromEmail, 1000 * TIMER_SECONDS_FRECUENCY);

            $('.filter-form input[name="created_at"]').datetimepicker({
                format: 'DD/MM/YYYY',
                maxDate: new Date()
            });

            body.on('click', '.open-modal', function (event) {
                event.preventDefault();
                var url = $(this).prop('href');
                var modalDiv = $('#modal-note');
                modalDiv.modal();
                modalDiv.find('.modal-header h4').html($(this).data('modal-title'));
                $.ajax(url)
                    .done(function (response) {
                        modalDiv.find('.modal-body').html(response);
                    })
            });

            body.on('change', '#assign-ticket-form select[name="admin_id"]', function() {
                setSelectStatus($(this));
            });

            body.on('submit', '#assign-ticket-form', function (event) {
                event.preventDefault();

                var form = $(this);
                var isReassign = parseInt(form.data('is-reassign')) === 1;
                var url = form.prop('action');
                var serializedForm = form.serialize();
                var select = $(this).find('select');

                if (!setSelectStatus(select)) {
                    event.preventDefault();
                    return true;
                }

                if (isReassign && !confirm('¿Desea reasignar el ticket?')) {
                    return;
                }

                $.ajax({
                    url: url,
                    type: 'POST',
                    method: 'POST',
                    crossDomain: true,
                    data: serializedForm
                }).done(function () {
                    // TODO Refrescar vista
                    // TODO Si ya se asigno, notificar al administrador y realizar la reasignación.
                    location.reload();
                }).fail(function (response) {
                    if (response.responseJSON && response.responseJSON.error) {
                        alert(response.responseJSON.error.message);
                    }
                })
            });

            /**
             * @param {jQuery} select
             */
            function setSelectStatus(select) {
                var currentValue = select.find(':selected').val();
                select[currentValue ? 'removeClass' : 'addClass']('error');
                return currentValue;
            }

            /**
             * Carga de nuevo el formulario con el filtro asociado.
             */
            function receiveTicketsFromEmail() {
                $('.filter-form').submit();
            }
        });
    </script>

    <!-- Add Permissions modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Nota a Pedido</h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
