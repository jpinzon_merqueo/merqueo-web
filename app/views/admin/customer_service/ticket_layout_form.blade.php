@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $layout->exists ? 'Editar plantilla de ticket' : 'Nueva plantilla de ticket' }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Info!</b> {{ Session::get('success') }}
            </div>
        @endif
        @if(Session::has('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alert!</b> {{ Session::get('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- form start -->
                    {{ Form::open(['action' => 'customerService.TicketLayout.save', 'id' => 'save-form']) }}
                    <div class="box-body">
                        <input type="hidden" name="id" id="id" value="{{ $layout->id }}">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3 form-group">
                                {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
                                {{ Form::text('name', $layout->name, ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="col-xs-12 col-sm-3 form-group">
                                {{ Form::label('ticket_solution_id', 'Solución', ['class' => 'control-label']) }}
                                {{ Form::fillSelectWithoutKeys('ticket_solution_id', $solutions, $layout->ticket_solution_id, ['class' => 'form-control', 'required'], true) }}
                            </div>
                            <div class="col-xs-12 col-sm-6 form-group">
                                {{ Form::label('email_subject', 'Asunto del mail', ['class' => 'control-label']) }}
                                {{ Form::text('email_subject', $layout->email_subject, ['class' => 'form-control', 'required']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('body', 'Cuerpo del mail', ['class' => 'control-label']) }}
                                {{ Form::hidden('body', $layout->body, ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="col-md-12 form-group">
                                {{ Form::label('available_variables', 'Variables disponibles', ['class' => 'control-label']) }}
                                {{ Form::fillSelectWithoutKeys('available_variables', $variables, null, ['class' => 'form-control'], true) }}
                                {{ Form::textarea('body-container', $layout->body, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="box-footer">
                            <a href="{{ route('customerService.TicketLayout.renderAjax', 0) }}"
                               class="btn btn-primary modal-open"
                               data-title="Previsualización"
                               id="preview-template">
                                Previsualizar
                            </a>
                            {{ Form::submit('Guardar', ['class' => 'btn btn-success']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Add Permissions modal -->
    <div class="modal fade" id="modal-note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <div align="center"><br><img src="{{ asset_url() }}/img/loading.gif"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var bodyContainer = CKEDITOR.replace('body-container');

            bodyContainer.on('change', function () {
                $('#body').val(bodyContainer.getData());
            });

            $('.modal-open').on('click', function (event) {
                event.preventDefault();

                var anchor = $(this);
                var url = anchor.prop('href');
                var title = anchor.data('title');
                var modalDiv = $('#modal-note');

                modalDiv.modal('show');
                modalDiv.find('h4').text(title);

                $.ajax({
                    url: url,
                    data: {content: bodyContainer.getData()}
                })
                    .done(function (response) {
                        modalDiv.find('.modal-body').html(response);
                    })
            });

            $('#available_variables').on('change', function () {
                var select = $(this);
                var value = select.find(':selected').val();
                var text = select.find(':selected').text();
                if (value) {
                    bodyContainer.insertText('@{{' + text + '}}');
                    select.val();
                }
            });

            $("#save-form").validate();
        });
    </script>

@stop