{{ Form::open(['action' => ['admin\callcenter\TicketController@update_status', $call->id], 'id' => 'complaint-order-form', 'class' => 'row']) }}

@if(!empty($call->ticket->order->id))
<div class="col-md-12">
    <div id="order-details" style="display: none"></div>
    <a href="#" id="show-details" class="btn btn-primary">Ver calificación del pedido</a>
    <br><br>
</div>
@endif

@if(!empty($orders))
<div class="form-group col-md-12">
    <label for="comments" class="control-label">Ultimos pedidos del usuario</label><br>
    @foreach($orders as $order)
        <input type="radio" name="order_id" class="order" value="{{$order->id}}" data-reference="{{$order->reference}}"> <b>Pedido:</b> {{$order->id}} - <b>Referencia:</b> {{$order->reference}} - <b>Fecha de creación:</b> {{format_date('normal_long_with_time', $order->date)}}
        <input type="hidden" name="reference" class="reference">
        <br>
    @endforeach
</div>
@endif


@if(!empty($call->ticket->message))
<div class="form-group col-md-12">
    <label for="comments" class="control-label">Mensaje del usuario</label>
    <textarea class="form-control" rows="20" name="comments" cols="50" id="comments" readonly>{{ str_replace('<br>', "\n", $call->ticket->message) }}</textarea>
</div>
@endif

@if(!empty($call->ticket->message))
    <div class="form-group col-md-4">
        {{ Form::label('reason_id', 'Razón del ticket', ['class' => 'control-label']) }}
        {{ Form::fillSelectWithoutKeys('reason_id', $reasons, null, ['class' => 'form-control', 'required'], true) }}
    </div>
@endif

<div class="form-group @if(!empty($call->ticket->message)) col-md-4 @else col-med-6 @endif">
    {{ Form::label('status_ticket', 'Estado del ticket', ['class' => 'control-label']) }}
    {{ Form::fillSelectWithoutKeys('status_ticket', $status_ticket, null, ['class' => 'form-control', 'required'], true) }}
</div>

<div class="form-group @if(!empty($call->ticket->message)) col-md-4 @else col-med-6 @endif">
    {{ Form::label('called', '¿Realizo una llamada?', ['class' => 'control-label']) }}
    {{
        Form::fillSelectWithoutKeys(
            'called',
            ['No', 'Sí'],
            null,
            ['class' => 'form-control', 'required'],
            true
        )
    }}
</div>

<div class="form-group col-md-12 called-depend" style="display: none">
    {{ Form::label('status_call', 'Estado de la llamada', ['class' => 'control-label']) }}
    {{ Form::fillSelectWithoutKeys('status_call', $status_call, null, ['class' => 'form-control'], true) }}
</div>

<div class="form-group col-md-12">
    {{ Form::label('comments', 'Comentario acerca del ticket', ['class' => 'control-label']) }}
    {{ Form::textarea('comments', null, ['class' => 'form-control', 'rows' => '2']) }}
</div>

<div class="status-ticket-depend" style="display: none">
    <div class="form-group col-md-6">
        {{ Form::label('solution_id', 'Solución') }}
        {{ Form::fillSelectWithoutKeys('solution_id', $solutions, null, ['class' => 'form-control'], true) }}
    </div>

    <div class="form-group solution_identifier_group col-md-6" style="display: none">
        {{ Form::label('solution_identifier', 'Identificador (ID)') }}
        {{ Form::text('solution_identifier', '', ['class' => 'form-control']) }}
        <div id="coupon-link" style="display: none">
            <a href="{{ route('adminCoupons.add') }}" target="_blank">Crear Cupon</a>
        </div>
        @if(!empty($call->ticket->order->id))
            <div id="complaint-order-link" style="display: none">
                <a href="{{ route('adminOrderComplaint.orderComplaint', $call->ticket->order->id) }}" target="_blank">Crear
                    Pedido reclamo</a>
            </div>
        @else
            @if(!empty($orders))
            <div id="complaint-order-link" style="display: none">
                <a href="javascript:;" id="order-complaint" target="_blank">Crear
                    Pedido reclamo</a>
            </div>
            @endif
        @endif
    </div>

    <div class="form-group amount_group col-md-6" style="display: none">
        {{ Form::label('content', 'Contenido adicional', ['class' => 'control-label']) }}
        {{ Form::textarea('content', null, ['class' => 'form-control', 'rows' => '2']) }}
    </div>

    <div class="form-group amount_group col-md-6" style="display: none">
        {{ Form::label('amount', 'Valor del crédito a asignar') }}
        {{ Form::number('amount', '', ['class' => 'form-control']) }}
    </div>

    <div class="form-group expiration_date_group col-md-6" style="display: none; position: relative">
        {{ Form::label('expiration_date', 'Fecha expiración') }}
        {{ Form::text('expiration_date', '', ['class' => 'form-control']) }}
    </div>

    <div id="email-content-templates"></div>

    <div class="form-group col-md-12 solution-id-depend" style="display: none">
        {{ Form::label('content', 'Contenido adicional', ['class' => 'control-label']) }}
        {{ Form::textarea('content', null, ['class' => 'form-control', 'rows' => '2']) }}
    </div>
</div>

<div class="col-md-12 text-center">
    {{
        Form::button(
            'Vista previa',
            [
                'class' => 'btn btn-primary solution-id-depend',
                'id' => 'preview-anchor',
                'data-href' => route('customerService.Ticket.solutionPreview', $call->id),
                'style' => 'display: none'
            ]
        )
    }}
    {{ Form::submit('Enviar', ['class' => 'btn btn-success']) }}
</div>

{{ Form::close() }}

<style>
    .comments.public textarea {
        background: #ffe80759;
    }
</style>

<script>
    $(document).ready(function () {
        // Los eventos asociados a este formulario se encuentran
        // en el template "admin/customer_service/user-index.blade.php"
        // debido a que en necesario que solo es necesario que se registren
        // los listener una vez.

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        $('#expiration_date').datetimepicker({
            format: 'DD/MM/YYYY',
            minDate: tomorrow,
            locale: 'es'
        });

        @if(!empty($call->ticket->order->id))
            $.ajax({
                url: '{{ route('adminOrderStorage.getOrderProductsAjax', $call->ticket->order->id) }}'
            }).done(function (response) {
                $('#order-details').html(response);
            });
        @endif

        $('#complaint-order-form').validate({
            showErrors: function (errorMap, errorList) {
                errorList = errorList.map(function (error) {
                    if (error.method === 'required') {
                        error.message = 'Este campo es obligatorio';
                    }
                });
                this.defaultShowErrors();
            },
            rules: {
                status_call: {
                    required: {
                        depends: function (element) {
                            var isRequired = $('#called').find(':selected').val() === '1';
                            if (!isRequired) {
                                $(element).val('');
                                return false;
                            }

                            return !$(element).find(':selected').val();
                        }
                    }
                },
                solution_identifier: {
                    required: {
                        depends: function (element) {
                            var solution = $('#solution_id').find(':selected').val();
                            var complaintOrder = '{{ \tickets\TicketSolution::COMPLAINT_ORDER }}';
                            var voucher = '{{ \tickets\TicketSolution::VOUCHER }}';
                            if (!solution || [complaintOrder, voucher].indexOf(solution) < 0) {
                                return false;
                            }

                            return !$(element).val();
                        }
                    }
                },
                amount: {
                    required: {
                        depends: function (element) {
                            var solution = $('#solution_id').find(':selected').val();
                            var credit = '{{ \tickets\TicketSolution::CREDIT }}';
                            if (!solution || solution !== credit) {
                                return false;
                            }

                            return !$(element).val();
                        }
                    }
                },
                expiration_date: {
                    required: {
                        depends: function (element) {
                            var solution = $('#solution_id').find(':selected').val();
                            var freeDelivery = '{{ \tickets\TicketSolution::FREE_DELIVERY }}';
                            var credit = '{{ \tickets\TicketSolution::CREDIT }}';
                            if (!solution || [freeDelivery, credit].indexOf(solution) < 0) {
                                return false;
                            }

                            return !$(element).val();
                        }
                    }
                },
                ticket_layout_id: {
                    required: {
                        depends: function (element) {
                            var solution = $('#solution_id').find(':selected').val();
                            if (!solution) {
                                return false;
                            }

                            return !$(element).val();
                        }
                    }
                },
            }
        });
    });
</script>
