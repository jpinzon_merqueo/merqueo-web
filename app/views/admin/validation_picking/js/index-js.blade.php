<script type="text/javascript">
	jQuery(document).ready(function($) {

		var routeSelect = (function() {
			'use strict';

			function routeSelect() {
				// enforces new
				if (!(this instanceof routeSelect)) {
					return new routeSelect();
				}
				this.url_get_routes = '{{ route('apiValidation.getRoutesAjax') }}';
				this.bindActions();
			}

			routeSelect.prototype.bindActions = function() {
				let self = this;
				$('#warehouse_id').change(function(event) {
					self.get_routes_ajax();
				});
			};

			routeSelect.prototype.set_data = function() {
				this.route_id = $('#route_id').val();
			};
			routeSelect.prototype.get_routes_ajax = function() {
				$('#loader-routes').removeClass('unseen');
				$('#load-orders').attr('disabled', 'disabled');
				$.ajax({
					url: this.url_get_routes,
					type: 'GET',
					dataType: 'html',
					data: $('#validate-picking-form').serialize(),
				})
				.done(function(data) {
					$('#route_id').empty();
					$('#route_id').html(data);
				})
				.fail(function() {
					console.error("error al cargar datos de rutas.");
				})
				.always(function() {
					$('#loader-routes').addClass('unseen');
					$('#load-orders').removeAttr('disabled');
				});
			};
			return routeSelect;
		}());
		var route_select = new routeSelect;
		route_select.get_routes_ajax();

		var orderLoader = (function() {
			'use strict';

			function orderLoader() {
				// enforces new
				if (!(this instanceof orderLoader)) {
					return new orderLoader();
				}
				// constructor body
				this.url_get_orders = '{{ route('apiValidation.getOrdersAjax') }}';
				this.url_update_orders = '{{ route('apiValidation.updateOrderRevisionDateAjax') }}'
				this.route_id = null;
				this.bindActions();
			}

			orderLoader.prototype.set_data = function() {
				this.route_id = $('#route_id').val();
			};

			orderLoader.prototype.get_orders_ajax = function() {
				$('#loader-routes').removeClass('unseen');
				$('#load-orders').attr('disabled', 'disabled');
				$('#lists-items').slideUp('fast');
				$('#detail-items').slideUp('fast').empty();
				this.set_data();
				$.ajax({
					url: this.url_get_orders,
					type: 'GET',
					dataType: 'json',
					data: {
							route_id: this.route_id
						},
				})
				.done(function(data) {
					$('#pending_orders').empty().html(data['orders_w_missings']);
					$('#cold_orders').empty().html(data['orders_w_colds']);
					$('#orders_to_check').empty().html(data['orders_to_check']);
					$('#lists-items').slideDown('fast');
				})
				.fail(function() {
					console.error("error al cargar las ordenes.");
				})
				.always(function() {
					$('#loader-routes').addClass('unseen');
					$('#load-orders').removeAttr('disabled');
				});
			};

			orderLoader.prototype.update_ready_orders = function(order_id) {
				$('#loader-routes').removeClass('unseen');
				$.ajax({
					url: this.url_update_orders,
					type: 'POST',
					dataType: 'json',
					data: {
							order_id: order_id
						},
				})
				.done(function(data) {
					if ( data.status) {
					}
				})
				.fail(function() {
					console.error("error al actualizar las ordenes.");
				})
				.always(function() {
					$('#loader-routes').addClass('unseen');
				});
			};

			orderLoader.prototype.bindActions = function(){
				var self = this;
				$('#validate-picking-form').validate();
				$('body').on('submit', '#validate-picking-form', function(event) {
					event.preventDefault();
					self.get_orders_ajax();
				});
				$('body').on('click', '.checks .fullfilled', function(event) {
					if ( !$(this).parents('.order-item').hasClass('alert-success') ) {
						event.preventDefault();
						var order_id = $(this).data('id');
						self.update_ready_orders(order_id);
						$(this).parents('.order-item').addClass('alert-success');
					}
				});
			};
			return orderLoader;
		}());
		var order_loader = new orderLoader;

		var orderDetails = (function() {
			'use strict';

			function orderDetails() {
				// enforces new
				if (!(this instanceof orderDetails)) {
					return new orderDetails();
				}
				// constructor body
				this.url_order_details = '{{ route('apiValidation.getOrderDetailsAjax') }}';
				this.order_id = null;
				this.route_id = null;
				this.sequence = null;
				this.type = null;
				this.products = [];
				this.bindActions();
			}

			orderDetails.prototype.bindActions = function() {
				var self = this;
				$('body').on('click', '.order-detail-link', function(event) {
					event.preventDefault();
					self.order_id = $(this).data('order_id');
					self.route_id = $(this).data('route');
					self.sequence = $(this).data('sequence');
					self.type = $(this).data('type');
					self.get_details_ajax();
				});
				$('body').on('click', '.close-order-detail', function(event) {
					event.preventDefault();
					$('#detail-items').slideUp('fast', function () {
						$(this).empty();
						$('#lists-items').slideDown('fast');
					});
				});
				$('body').on('click', '.list-group-item .fullfilled', function(event) {
					var product_id = $(this).data('id');
					event.preventDefault();
					$(this).parents('.list-group-item').removeClass('alert-danger').addClass('alert-success');
					self.update_product(product_id, 'Fullfilled');
				});
				$('body').on('click', '.list-group-item .not-available', function(event) {
					var product_id = $(this).data('id');
					event.preventDefault();
					$(this).parents('.list-group-item').removeClass('alert-success').addClass('alert-danger');
					self.update_product(product_id, 'Not Available');
				});
				$('body').on('click', '.update-order-status', function(event) {
					event.preventDefault();
					self.update_order_status();
				});
				$('body').on('click', '.img-responsive', function(event) {
					event.preventDefault();
					var src = null;
					src = $(this).attr('src');
					src = src.replace('medium', 'large');
					console.log(src);
					$('.large-image-product').attr('src', src);
					$('#modal-large-image').modal('show');
				});
			};

			orderDetails.prototype.get_details_ajax = function() {
				var self = this;
				var data = {
					order_id: this.order_id,
					route_id: this.route_id,
					sequence: this.sequence,
					type: this.type
				}
				if ( localStorage.getItem(this.order_id) !== null && localStorage.getItem(this.order_id) !== undefined ) {
					data.products = JSON.parse( localStorage.getItem( this.order_id ) );
				}
				$('#loader-routes').removeClass('unseen');
				$('#lists-items').slideUp('fast');
				$.ajax({
					url: this.url_order_details,
					type: 'POST',
					dataType: 'json',
					data: data,
					context: this
				})
				.done(function(data) {
					if (data.status) {
						$('#detail-items').slideDown('fast').empty().html(data.html);
						self.products = data.products;
						console.log(self.products)
					}
				})
				.fail(function() {
					console.log("error al obtener los detalles.");
				})
				.always(function() {
					$('#loader-routes').addClass('unseen');
				});
			};

			orderDetails.prototype.update_product = function(product_id, status) {
				var self = this;
				var arr = jQuery.grep(self.products, function( obj, index ) {
					if ( obj.id == product_id ) {
						self.products[index].fulfilment_status = status;
					}
				});
				localStorage.setItem(this.order_id, JSON.stringify(this.products));
			};

			orderDetails.prototype.validate_products_status = function() {
				var products_count = $('.list-group li').length;
				var products_clicked = $('.list-group li.alert-danger, .list-group li.alert-success').length;
				if ( products_clicked != products_count ) {
					return false;
				}
				return true;
			};

			orderDetails.prototype.update_order_status = function() {
				var is_valid = this.validate_products_status();
				$('#loader-routes').removeClass('unseen');
				$('body').find('.update-order-status').attr('disabled', 'disabled');
				if ( !is_valid ) {
					alert('Es necesario que indiques los estados de los productos.');
					return;
				}

				$.ajax({
					url: '{{ route('apiValidation.updateOrderStatusAjax') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						order_id: this.order_id,
						products: this.products
					},
					context: this
				})
				.done(function(data) {
					if ( data.status ) {
						$('.close-order-detail').trigger('click');
						$('#'+this.order_id).slideUp('fast').remove();
						localStorage.removeItem(this.order_id);
						alert(data.message);
						order_loader.get_orders_ajax();
					}else{
						alert(data.message);
						order_loader.get_orders_ajax();
					}
				})
				.fail(function() {
					console.log("error al guardar el estado de las ordenes.");
					$('body').find('.update-order-status').removeAttr('disabled');
				})
				.always(function() {
					$('#loader-routes').addClass('unseen');
					$('body').find('.update-order-status').removeAttr('disabled');
				});

			};

			return orderDetails;
		}());
		var order_details = new orderDetails;

	});
</script>