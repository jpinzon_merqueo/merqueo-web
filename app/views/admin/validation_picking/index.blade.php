@if ( !Request::ajax() )
	@extends('admin.layout')

	@section('content')

		<section class="content-header">
			<h1>
				{{ $title }}
				<small>Control panel</small>
			</h1>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Pedidos con faltantes</h3>
						</div>
						<div class="box-body orders-missing-products">
							<form role="form" id="validate-picking-form">
								<input type="hidden" name="html" value="true">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-xs-4">
										<div class="row form-group">
											<div class="col-lg-3 col-md-3 col-xs-3 text-right">
												<label for="city_id">Ciudad:</label>
											</div>
											<div class="col-lg-9 col-md-9 col-xs-12">
												<select name="city_id" id="city_id" class="form-control get-warehouses">
													@foreach ($cities as $city)
														<option value="{{ $city->id }}">{{ $city->city }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-3 col-md-3 col-xs-3 text-right">
												<label for="warehouse_id">Bodega:</label>
											</div>
											<div class="col-lg-9 col-md-9 col-xs-12">
												<select name="warehouse_id" id="warehouse_id" class="form-control">
													@foreach ($warehouses as $warehouse)
														<option value="{{ $warehouse->id }}">{{ $warehouse->warehouse }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-4">
										<div class="row form-group">
											<div class="col-lg-3 col-md-3 col-xs-3 text-right">
												<label title="Rutas" for="routes">Rutas</label>
											</div>
											<div class="col-lg-9 col-md-9 col-xs-12">
												<select class="form-control required" id="route_id" name="route_id">
													<option value="">-Seleccionar-</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-9 col-md-9 col-xs-12 col-lg-offset-3 col-md-offset-3 text-center">
												<button type="submit" class="btn btn-success" id="load-orders">Cargar</button>
												<img src="{{ asset_url() }}/img/loading.gif" id="loader-routes" class="unseen">
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div id="lists-items" class="row" style="display: none;">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#pending_orders" data-toggle="tab">Seco</a>
								</li>
								<li>
									<a href="#cold_orders" data-toggle="tab">Frío</a>
								</li>
								<li>
									<a href="#orders_to_check" data-toggle="tab">Revisión</a>
								</li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="pending_orders">
								</div>
								<div class="tab-pane" id="cold_orders">
								</div>
								<div class="tab-pane" id="orders_to_check">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="detail-items" class="row">
			</div>

			<div class="modal fade" id="modal-large-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Imagen de producto</h4>
							<div class="row">
								<div class="col-xs-12">
									<img src="" class="img-responsive large-image-product">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		@include('admin.validation_picking.js.index-js')
	@stop
@else
	@section('routes')
		<option value="">-Seleccionar-</option>
		@if ( !empty($routes) )
			@foreach ($routes as $route)
				<option value="{{ $route->id }}">{{ $route->route }}</option>
			@endforeach
		@endif
	@stop
@endif