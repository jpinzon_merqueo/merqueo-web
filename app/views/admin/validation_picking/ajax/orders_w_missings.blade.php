@if ( count($orders_w_missings) )
	<br>
	@foreach ($orders_w_missings as $order)
		<div class="order-item" id="{{ $order->id }}">
			<div class="row">
				<a href="javascript:;" data-type="seco" data-order_id="{{ $order->id }}" data-route="{{ $order->route_id }}" data-sequence="{{ $order->planning_sequence }}" class="order-detail-link">
					<div class="col-xs-12">
						<label>Pedido #</label> {{ $order->id }}<br>
						<label>Ruta:</label> {{ $order->route}} <br>
						<label>Secuencia:</label> {{ $order->planning_sequence }} <br>
					</div>
				</a>
			</div>
		</div>
	@endforeach
@else
	<br>
	<div class="order-item">
		<div class="row">
			<a href="javascript:;">
				<div class="col-xs-12 text-center">
					<label>No se encontraron pedidos</label>
				</div>
			</a>
		</div>
	</div>
@endif