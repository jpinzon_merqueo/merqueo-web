@if ( isset($order) && count($products) )
	<div class="col-xs-12 order-detail">
		<div class="box box-primary">
			<div class="box-body">
				<div class="order-detail-header">
					<div class="row">
						<div class="col-xs-12">
							<h4>Detalle de pedido <button type="button" class="btn btn-danger pull-right close-order-detail">X</button></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<strong>Pedido #</strong> {{ $order['order_id'] }}<br>
							<strong>Ruta:</strong> {{ $order['route'] }} <br>
							<strong>Secuencia:</strong> {{ $order['sequence'] }} <br>
						</div>
					</div>
				</div>
				<hr>
				<div class="order-detail-content">
					<div class="row">
						<div class="col-xs-12">
							<h4>Productos faltantes</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<ul class="list-group">
								@foreach ($products as $product)
									<li class="list-group-item @if ( $product['fulfilment_status'] == 'Fullfilled' ) alert-success @endif @if ( $product['fulfilment_status'] == 'Not Available' ) alert-danger @endif">
										<h4 class="list-group-item-heading">{{ $product['product_name'] }}</h4>
										<br>
										<div class="row">
											<div class="col-xs-3 product-image" style="padding-right: 0;">
												<img class="img-responsive" src="{{ $product['product_image_url'] }}">
											</div>
											<div class="col-xs-7" style="padding-right: 0;">
												@if ( $product['quantity'] > 1 )
													<label>Cantidad:</label> <span style="color: #a94442; font-size: 1.8rem;"><strong>{{ $product['quantity'] }}</strong></span>
													<br>
												@else
													<label>Cantidad:</label> {{ $product['quantity'] }}<br>
												@endif
												<label>Precio:</label> ${{ number_format($product['price'], 0, ',', '.') }} <br>
												<label>Almacenamiento:</label> {{ $product['storage'] }} <br>
												<label>Posición en bodega:</label> {{ $product['storage_position'] }} <br>
												<label>Posición en altura:</label> {{ $product['storage_height_position'] }} <br>
												<label>Stock actual:</label> {{ $product['current_stock'] }}
											</div>
											<div class="col-xs-2" style="padding: 0;">
												<button class="btn btn-default fullfilled" data-status="Fullfilled" data-id="{{ $product['id'] }}">
													<span class="glyphicon glyphicon-ok"></span>
												</button>
												<br>
												<br>
												<button class="btn btn-default not-available" data-status="Not Available" data-id="{{ $product['id'] }}">
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</div>
										</div>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<button class="btn btn-success update-order-status">Pedido alistado</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@else
@endif