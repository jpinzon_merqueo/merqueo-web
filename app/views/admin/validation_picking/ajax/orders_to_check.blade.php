@if ( count($orders_to_check) )
	<br>
	<div class="row form-group">
		<div class="col-xs-12 text-center">
			<h3>{{ count($orders_to_check) }} pedidos</h3>
		</div>
	</div>
	@foreach ($orders_to_check as $order)
		<div class="order-item @if ( !empty($order->picking_revision_date) ) alert-success @endif" id="{{ $order->id }}">
			<div class="row">
				<div class="col-xs-9">
					<label>Pedido #</label> {{ $order->id }}<br>
					<label>Ruta:</label> {{ $order->route }} <br>
					<label>Secuencia:</label> {{ $order->planning_sequence }} <br>
				</div>
				<div class="col-xs-3 checks">
					<br>
					<br>
					<button class="btn btn-default fullfilled" data-id="{{ $order->id }}">
						<span class="glyphicon glyphicon-ok"></span>
					</button>
				</div>
			</div>
			@if ( $order->dry_products )
			<div class="row">
				<div class="col-xs-12">
						<label>Productos en seco:</label> {{ $order->dry_products }} <br>
				</div>
			</div>
			@endif
			@if ( $order->cold_products )
			<div class="row">
				<div class="col-xs-12">
					<div class="bag">
						<label>Productos en frio:</label> <strong>{{ $order->cold_products }}</strong> <br>
					</div>
				</div>
			</div>
			@endif
		</div>
	@endforeach
@else
	<br>
	<div class="order-item">
		<div class="row">
			<a href="javascript:;">
				<div class="col-xs-12 text-center">
					<label>No se encontraron pedidos</label>
				</div>
			</a>
		</div>
	</div>
@endif