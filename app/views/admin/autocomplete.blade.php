<ul class="autocomplete-admin">
@foreach($products as $product)
  <li class="clearfix">
    <a href="javascript:;" class="get-product-item" data-id="{{ $product->id }}" data-image="{{ $product->image_medium_url }}" data-name="{{ $product->name }}" data-price="{{ $product->price }}">
        <div class="col-xs-2" align="center">
            <img src="{{ $product->image_small_url }}" width="30">
        </div>
        <div class="col-xs-10">
            <div class="name">{{ $product->name.' '.$product->quantity.' '.$product->unit }}</div>
            <div class="price">$ {{ number_format(!$product->special_price ? $product->price : $product->special_price) }}</div>
        </div>
    </a>
  </li>
@endforeach
</ul>