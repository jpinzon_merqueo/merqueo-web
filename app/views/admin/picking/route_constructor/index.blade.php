@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
        <style>
            .img-loader{
                width: 50px;
                height: 50px;
            }

            .bg-warning{
                background: #fcf8e3;
                border: 1px solid #f5fcad;
                margin-bottom: 5px;
            }
            #modal_resolv_new .modal-body{
                overflow: hidden;
            }
            .overlay-div {
                position: absolute;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                z-index: 17;
                width: 100%;
                /* background-color: green; */
                height: 100%;
                /* opacity: 0.1; */
                /* float: left; */
            }
            .order-constructor-body{
                padding: 10px;
            }
            .order-constructor-footer{
                padding: 10px;
            }

        </style>
        <section class="content-header">
            <h1>
                {{ $title }}
                <small>Control panel</small>
            </h1>
            <span class="breadcrumb" style="top: 0px;">
                <div class="btn-group">
                </div>
            </span>
        </section>
        <section class="content">
            <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
            <link href="{{admin_asset_url()}}/css/timepicker/bootstrap-timepicker.css" rel="stylesheet">
            @if(Session::has('message') )
                @if(Session::get('type') == 'success')
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Información</b> {{ Session::get('message') }}
                    </div>
                @else
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alerta!</b> {{ Session::get('message') }}
                    </div>
                @endif
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-default btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <form class="form" id="search-form" method="GET" autocomplete="off">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group col-md-4">
                                            {{ Form::label('city_id', 'Ciudad:') }}
                                            {{ Form::fillSelectWithKeys('city_id', $cities, $city, array('class' => 'form-control get-warehouses', 'id' => 'city_id'), 'id', 'city' ) }}
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{ Form::label('warehose_id', 'Bodega:') }}
                                            {{ Form::fillSelectWithKeys('warehouse_id', $warehouses, $warehouse_id, ['id' => 'warehouse_id', 'class' => 'form-control'],'id', 'warehouse') }}
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{ Form::label('shift', 'Jornada:') }}
                                            {{ Form::select('shift', $shifts, $shift, ['id' => 'shift', 'class' => 'form-control']) }}
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{ Form::label('route_id', 'Rutas:') }}
                                            <select class="form-control" id="route_id" name="route_id">
                                                <option value="">Seleccione</option>
                                            </select>
                                        </div>
                                        {{ Form::token() }}
                                        {{ Form::hidden('_method', 'GET') }}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pedido</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-default btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive" id="list_orders">
                                        <div align="center" class="paging-loading img-loader center-block" id="img-loader"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- /.  modals messages-->
        <div class="modal modal-info fade" id="modal-info">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-outline pull-center" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <script src="{{admin_asset_url()}}/js/plugins/datetimepicker/moment.js"></script>
        <script src="{{admin_asset_url()}}/js/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js"></script>
        <script src="https://cdn.rawgit.com/serratus/quaggaJS/0420d5e0/dist/quagga.min.js"></script>
        <script>
            var anim ;
            var _scannerIsRunning = false;
            var loadAnimation = function (element_id) {
                anim = bodymovin.loadAnimation({
                    wrapper: document.getElementById(element_id),
                    animType: 'svg',
                    loop: true,
                    prerender: true,
                    autoplay: true,
                    path: "{{ web_url() }}/admin_asset/img/material_loader.json"
                });
            };
            var startScanner = function(){
                Quagga.init({
                    inputStream: {
                        name: "Live",
                        type: "LiveStream",
                        target: document.querySelector('#scanner-container'),
                        constraints: {
                            width: 480,
                            height: 320,
                            facingMode: "environment"
                        },
                    },
                    decoder: {
                        readers: [
                            "code_128_reader",
                            "ean_reader",
                            "ean_8_reader",
                            "code_39_reader",
                            "code_39_vin_reader",
                            "codabar_reader",
                            "upc_reader",
                            "upc_e_reader",
                            "i2of5_reader"
                        ],
                        debug: {
                            showCanvas: true,
                            showPatches: true,
                            showFoundPatches: true,
                            showSkeleton: true,
                            showLabels: true,
                            showPatchLabels: true,
                            showRemainingPatchLabels: true,
                            boxFromPatches: {
                                showTransformed: true,
                                showTransformedBox: true,
                                showBB: true
                            }
                        }
                    },
                }, function (err) {
                    if (err) {
                        console.log(err);
                        return
                    }
                    console.log("Initialization finished. Ready to start");
                    Quagga.start();
                    // Set flag to is running
                    _scannerIsRunning = true;
                });

                Quagga.onProcessed(function (result) {
                    var drawingCtx = Quagga.canvas.ctx.overlay,
                        drawingCanvas = Quagga.canvas.dom.overlay;

                    if (result) {
                        if (result.boxes) {
                            drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                            result.boxes.filter(function (box) {
                                return box !== result.box;
                            }).forEach(function (box) {
                                Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, { color: "green", lineWidth: 2 });
                            });
                        }

                        if (result.box) {
                            Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, { color: "#00F", lineWidth: 2 });
                        }

                        if (result.codeResult && result.codeResult.code) {
                            Quagga.ImageDebug.drawPath(result.line, { x: 'x', y: 'y' }, drawingCtx, { color: 'red', lineWidth: 3 });
                        }
                    }
                });

                Quagga.onDetected(function (result) {
                    $('#codebar').val(result.codeResult.code);
                    routeConstructor.validateBarcode();
                });
            };

            var routeConstructor = {
                urls : {
                    'getRoutesUrl' : '{{route('adminPlanningRouteConstructor.getRoutesAjax')}}',
                    'getOrdersUrl' : '{{route('adminPlanningRouteConstructor.getOrdersAjax')}}',
                    'saveUrl' : '{{route('adminPlanningRouteConstructor.saveRoutes')}}'
                },
                time_out : null,
                loadRoutes : function(){
                    var self = this;
                    $.ajax({
                        url : self.urls.getRoutesUrl,
                        type: 'GET',
                        data : $('#search-form').serialize(),
                        dataType : 'json',
                        cache : false,
                        beforeSend : function () {
                            //self.addLoadAnimation('img-loader');
                        },
                        success: function(response){
                            if(response.status){
                                var options = '<option value="">Seleccione</option>';
                                $.each(response.result, function (index, route) {
                                    options += '<option value="'+route.id+'">'+route.route+'</option>';
                                });
                                $('#route_id').html(options);
                            }else{
                                self.showModalMessages('Ocurrio un error al cargar las rutas.', 'error');
                            }
                        },
                        error : function(xhr){
                            self.showModalMessages('Ocurrio un error al cargar las ruta.', 'error');
                        }
                    })
                },
                loadOrders : function(){
                    var self = this;
                    $.ajax({
                        url : self.urls.getOrdersUrl,
                        type: 'GET',
                        data : $('#search-form').serialize(),
                        dataType : 'json',
                        cache : false,
                        beforeSend : function () {
                            self.addLoadAnimation('#list_orders', 'img-loader');
                            if(_scannerIsRunning){
                                Quagga.stop();
                                _scannerIsRunning = false;
                            }
                        },
                        success: function(response){
                            if(response.status){
                                $('#list_orders').html(response.result);
                            }else{
                                self.showModalMessages('Ocurrio un error al cargar los pedidos.', 'error');
                            }
                        },
                        error : function(xhr){
                            self.showModalMessages('Ocurrio un error al cargar los pedidos.', 'error');
                        }
                    })
                },
                saveOrder : function(){
                    var self = this;
                    $.ajax({
                        url : self.urls.saveUrl,
                        type: 'GET',
                        data : $('#save-codebar').serialize(),
                        dataType : 'json',
                        cache : false,
                        beforeSend : function () {

                        },
                        success: function(response){
                            if(response.status){
                                self.showModalMessages(response.message, 'success');
                            }else{
                                self.showModalMessages(response.message, 'error');
                            }
                            self.loadOrders();
                        },
                        error : function(xhr){
                            self.showModalMessages('Ocurrio un error al cargar los pedidos.', 'error');
                            self.loadOrders();
                        }
                    })
                },
                validateBarcode : function(){
                    var self = this;
                    var barcode =  $('#codebar').val().split('-');
                    var order_id = $('#order_id').val();
                    if(barcode[0] != '') {
                        if (barcode[0] == order_id) {
                            $('.btn-start-scanner').hide();
                            $('.btn-move-to-route').removeClass('hidden')
                            $('.btn-move-to-route').show();
                            $('#scanner-container').hide();
                            var ruta = $('#route_id').find('option:selected').html()
                            $('.order-constructor-message').html('<h2 class="text-center text-success">Por favor mueve el pedido a la ruta: <b>'+ruta+'</b><h2>')
                            _scannerIsRunning = false;
                            Quagga.stop();
                        } else {
                            $('#codebar').val('');
                            self.showModalMessages('El código de barras no pertenece al pedido.', 'error');
                        }
                    }
                },
                addLoadAnimation : function (content, id) {
                    $(content).html('<div align="center" class="paging-loading img-loader center-block" id="img-loader"></div>');
                    loadAnimation(id);
                },
                showModalMessages : function (content, type) {
                    $('#modal-info').removeClass('modal-danger');
                    $('#modal-info').removeClass('modal-info');
                    $('#modal-info').removeClass('modal-success');
                    $('#modal-info').removeClass('modal-warning');
                    switch (type){
                        case 'info':
                            $('#modal-info').addClass('modal-info');
                            $('#modal-info').find('.modal-body').html(content);
                            break;
                        case 'error':
                            $('#modal-info').addClass('modal-danger');
                            break;
                        case 'success':
                            $('#modal-info').addClass('modal-success');
                            break;
                        case 'warning':
                            $('#modal-info').addClass('modal-warning');
                            break;
                    }
                    $('#modal-info').find('.modal-body').html(content);
                    $('#modal-info').modal('show');
                }
            };
            $(document).ready(function () {
                $('body').on('change', '#warehouse_id', function(e){
                    e.preventDefault();
                    $('#shift').val('');
                });
                $('body').on('change', '#shift', function (e) {
                    e.preventDefault();
                    routeConstructor.loadRoutes();
                });
                $('#shift').trigger('change');
                $('body').on('change', '#route_id', function (e) {
                    e.preventDefault();
                    routeConstructor.loadOrders();
                });
                $('body').on('click', '.btn-start-scanner', function (e) {
                    e.preventDefault();
                    if (_scannerIsRunning) {
                        Quagga.stop();
                    } else {
                        startScanner();
                        $('.btn-start-scanner').hide();
                    }
                });
                $('body').on('click', '.btn-move-to-route', function (e) {
                    e.preventDefault();
                    routeConstructor.saveOrder();
                });
            });
        </script>
    @endsection
@endif