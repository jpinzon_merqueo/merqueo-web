@if (Request::ajax())
    @section('content')
        @if($order)
            <form id="save-codebar">
                <div class="col-md-6 order-constructor">
                    <div class="col-md-12 col-sm-12 col-xs-12 order-constructor-body">
                        <div class="overlay-div"></div>
                        <div class="col-md-12 col-sm-12 order-constructor-title-message text-center"><h4>Busca el siguiente pedido</h4></div>
                        <div class="col-md-8 col-sm-12 col-xs-12 order-constructor-title text-center">
                            <h1 class="text-center">{{$order->id}}-{{$order->picking_sequence_letter}}</h1>
                        </div>
                        <div class="col-md-12 col-sm-12 order-constructor-locality text-center">
                            <h3 class="text-center">{{isset($order->orderGroup->locality->name)?$order->orderGroup->locality->name:$order->orderGroup->zone->name}}</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 order-constructor-locality text-center">
                            <h3 style="color: #D0006F;">Debes mover <b>{{$order->picking_baskets}}</b> canastillas </h3>
                        </div>
                        <div class="col-md-12 order-constructor-scanner ">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-barcode"></i>
                                </span>
                                <input type="hidden" id="order_id" name="order_id" value="{{$order->id}}">
                                <input type="text" id="codebar" name="codebar" class="form-control">
                            </div>
                            <div class="col-md-12 order-constructor-message"></div>
                        </div>
                    </div>
                    <div class="col-md-12" id="scanner-container"></div>
                    <div class="col-md-12 col-sm-12 col-xs-12 order-constructor-footer">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button class="btn-flat btn btn-primary btn-block btn-start-scanner">Escanear código</button>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button class="btn-flat btn btn-primary btn-block btn-move-to-route hidden">HE MOVIDO EL PEDIDO A LA RUTA</button>
                        </div>
                    </div>
                </div>
            </form>
        @else
            <div class="col-md-12 col-sm-12 text-center">
                <h3>No hay pedidos disponibles para esta ruta.</h3>
            </div>
        @endif
    @endsection
@endif