@if (!Request::ajax())
    @extends('admin.layout')
@section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">

            </span>
    </section>
    <style type="text/css">
        .dataTables_empty{
            text-align: center;
        }
    </style>
    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Información</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <div class="box-body">
                        <div class="row error-search">
                            <div class="col-xs-12">
                                <div class="alert alert-danger unseen"></div>
                                <div class="alert alert-success unseen"></div>
                            </div>
                        </div>
                        <form id="third-party-form" action="{{ route('adminThirdPartyProducts.updateThirdPartyCost') }}"  enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Archivo csv</label>
                                        <input type="file" name="file" id="file">
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary  pull-right" id="save-btn">Actualizar Precios</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header"></div>
                    <div class="box-body">
                        <div class="table-responsive" id="product-list">

                        </div>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('#save-btn').on('click', function(){
                $('#third-party-form').submit();
            });
        })

    </script>
@endsection
@else
@section('table')
    @if (isset($products))
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Referencia</th>
                <th>Producto</th>
                <th>Aciones</th>
            </tr>
            </thead>
            <tbody>
            @if(count($products))
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->storeProduct[0]->id}}</td>
                        <td>{{$product->reference}}</td>
                        <td>{{$product->name}} {{$product->quantity}}{{$product->unit}}</td>
                        <td>{{$product->id}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4" class="text-center">No hay productos disponibles</td>
                </tr>
            @endif
            </tbody>
        </table>
    @endif
@endsection
@endif