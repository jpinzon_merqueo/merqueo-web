@extends('admin.layout')

@if (!Request::ajax())
    @section('content')
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="{{ web_url() }}/admin_asset/js/get-url-deeplink.js" type="text/javascript"></script>
        <section class="content-header">
            <h1>
                {{ $title }}
                <small>Control panel</small>
            </h1>
        </section>
        <section class="content">
            <div class="row">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alerta!</b> {{ Session::get('success') }}
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('error') }}
                </div>
                @endif
            </div>
            <div class="row">
                <div class="form-group col-xs-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <form id="generate-deeplink" 
                                        data-store-url="{{ route('adminGenerateDeeplink.getStoresAjax') }}"
                                        data-department-url="{{ route('adminGenerateDeeplink.getDepartmentsAjax') }}"
                                        data-shelves-url="{{ route('adminGenerateDeeplink.getShelvesAjax') }}"
                                        data-product-url="{{ route('adminGenerateDeeplink.getProductsAjax') }}"
                                        action="{{ route('adminGenerateDeeplink.index') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                        <div class="row">
                                            <div class="form-group col-xs-4 deeplink">
                                                <label for="deeplink">Deeplink</label>
                                                <select class="form-control" name="deeplink" id="deeplink">
                                                    <option value="">-Selecciona-</option>
                                                    <option value="1">Push o InApp</option>
                                                    <option value="0">URL externa</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="is_deeplink @if(empty($banner['deeplink_type'])) unseen @endif">
                                            <div class="row">
                                                <div class="form-group col-xs-4 deeplink_type">
                                                    <label for="deeplink_type">Tipo de deeplink</label>
                                                    <select class="form-control" name="deeplink_type" id="deeplink_type">
                                                        <option value="">-Selecciona-</option>
                                                        <option value="store">Tienda</option>
                                                        <option value="department">Departamento</option>
                                                        <option value="shelf">Pasillo</option>
                                                        <option value="product">Producto</option>
                                                        <option value="cart">Carrito</option>
                                                        <option value="list_banners">Listado de Banners</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-xs-4 deeplink_cities">
                                                    <label for="deeplink_cities">Ciudad</label>
                                                    <select class="form-control" name="deeplink_city_id" id="deeplink_city_id">
                                                            <option value="">-Selecciona-</option>
                                                            @foreach($cities as $city)
                                                                <option value="{{ $city->id }}">
                                                                    {{ $city->city }}
                                                                </option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-xs-4 deeplink_store">
                                                    <label for="deeplink_store">Tienda</label>
                                                    <select class="form-control" name="deeplink_store_id" id="deeplink_store_id">
                                                        <option value="">-Selecciona-</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-xs-4 deeplink_departments">
                                                    <label for="deeplink_departments">Departamento</label>
                                                    <select class="form-control" name="deeplink_department_id" id="deeplink_department_id">
                                                        <option value="">-Selecciona-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-xs-4 deeplink_shelves">
                                                    <label for="deeplink_shelves">Pasillo</label>
                                                    <select class="form-control" name="deeplink_shelf_id" id="deeplink_shelf_id">
                                                        <option value="">-Selecciona-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-xs-4 deeplink_products">
                                                    <label for="deeplink_products">Producto</label>
                                                    <select class="form-control" name="deeplink_store_product_id" id="deeplink_store_product_id">
                                                        <option value="">-Selecciona-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-12">
                                                    <label for="deeplink_generated">Deeplink</label>
                                                    <input type="text" class="form-control" id="deeplink_generated" name="deeplink_generated" readonly="true" value="">
                                            </div>
                                            <div class="form-group col-xs-12">
                                                <button type="button" onclick="javascript:copyToClipboard();" class="btn btn-primary">Copiar al portapapeles</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endsection
@endif