@if (!Request::ajax())
    @extends('admin.layout')
@section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="{{ web_url() }}/admin_asset/css/select2/select2.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/jquery.dataTables.js"
            type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/dataTables.bootstrap.js"
            type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/select2/select2.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
	        <a type="button" class="btn btn-primary" style="color:#fff;" href="{{ route('samplingProducts.index') }}"><i
                        class="fa fa-arrow-circle-left"></i> Volver</a>
            </a>

    </section>
    <section class="content">
        @if(Session::has('message') )
            @if(Session::get('type') == 'success')
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Información</b> {{ Session::get('message') }}
                </div>
            @else
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Alerta!</b> {{ Session::get('message') }}
                </div>
            @endif
        @endif
        <div class="row">
            @if($action == 'add')
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-body" id="form_search_start_supplier">
                        <div class="row error-search">
                            <div class="col-xs-12">
                                <div class="alert alert-danger unseen"></div>
                                <div class="alert alert-success unseen"></div>
                            </div>
                        </div>
                        <form id="search-product-form" action="{{ route('samplingProducts.index') }}"
                              autocomplete="off">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Ciudad</label>
                                        <select name="city_id" id="city_id" class="form-control">
                                            @foreach($cities AS $city)
                                                <option value="{{$city->id}}">{{$city->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Bodega</label>
                                        <select name="warehouse_id" id="warehouse_id" class="form-control" required>
                                            <option value="">Selecciona</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Producto</label>
                                        <input type="text" name="search-product" id="search-product"
                                               class="form-control" placeholder="ID, Referencia, Nombre" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">

                                        <button style="margin-top: 25px;" class="btn btn-primary" id="search-products">
                                            Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-md-12" id="product-content">
                <center><img class="u-loader" src="{{ asset('assets/img/loading.gif') }}" alt=""></center>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        var productSamples = (function () {
            'use strict';

            function productSamples(args) {
                if (!(this instanceof productSamples)) {
                    return new productSamples(args);
                }
                this.url_warehouses_ajax = '{{ route("admin.get_warehouses_ajax") }}';
                this.url_search_products_ajax = '{{route("samplingProducts.search_products_ajax")}}';
                this.url_get_data_ajax = '{{route("samplingProducts.get_data_ajax")}}';
                this.store_product_id = "{{ (isset($product))?$product->store_product_id:null }}";
                this.warehouse_id = "{{ (isset($product))?$product->warehouse_id:null }}";
                this.sampling_product_id = "{{ (isset($product))?$product->id:null }}";
                this.type = "{{ (isset($product))?$product->type:null }}";
                this.store_product_ids = [@if (isset($types))@if(count($types))@foreach($types as $type)@if('Productos' == $type->type ){{ $type->related_ids }} @endif @endforeach @endif @endif];
                this.store_product_bougth_ids = [@if (isset($types))@if(count($types))@foreach($types as $type)@if('Producto Comprado' == $type->type){{ $type->related_ids }} @endif @endforeach @endif @endif];
                this.department_ids = [ @if (isset($types)) @if(count($types)) @foreach($types as $type) @if('Departamentos' == $type->type) {{ $type->related_ids }} @endif @endforeach @endif @endif];
                this.shelve_ids = [@if (isset($types)) @if(count($types)) @foreach($types as $type) @if('Pasillos' == $type->type) {{ $type->related_ids }} @endif @endforeach @endif @endif];
                this.quantity = [{{ (isset($product))?$product->quantity:null }}];
                this.brand = "{{ (isset($product))?$product->brand:null }}";
                this.zone_ids = [{{ (isset($product))?$product->zone_ids:null }}];

                this.bindActions();
            }

            productSamples.prototype.bindActions = function () {
                var self = this;
                $('#city_id').on('change', function (event) {
                    $('#product-content').hide();
                    if ($(this).val() != "") {
                        self.ajax_select_warehouse();
                    }
                });

                $('#search-product-form').validate();

                $('#search-products').on('click', function (event) {
                    event.preventDefault();
                    if ($('#search-product-form').valid()) {
                        self.search_products();
                    }
                });

                $('body').on('click', '.select-product', function () {
                    var id = $(this).data('id');
                    var warehouse_id = $(this).data('warehouse');
                    $('#product-content').show();
                    self.get_data({type: 'product', store_product_id: id, warehouse_id: warehouse_id});
                    $.fancybox.close();
                });

                $('body').on('click', '.sampling-product-form-save', function () {
                    if ($('#sampling-product-form').valid()) {

                        if($("input.type_association:checked").length == 0){
                            alert('Por favor selecionar al menos un tipo de sampling.');
                            return false;
                        }

                        $('#sampling-product-form').submit();
                        //console.log($('#sampling-product-form').serialize())
                    }
                });

                $('#city_id').trigger('change');
                $('body').on('click', '.type_association', function () {
                    var warehouse_id = $('#product_warehouse_id').val();

                    if ($(this).hasClass('type_association_products') && $(this).is(':checked')) {
                        $('.asociation-products').removeClass('hide');
                        self.get_data({type: $('.type_association_products').val(), warehouse_id: warehouse_id});

                    } else if ($(this).hasClass('type_association_products') && $(this).not(':checked')) {
                        $('.asociation-products').addClass('hide');
                        $('#asociation_products').html('');
                        $('#asociation_products').select2('destroy');
                        $('#asociation_products').select2();

                    }

                    if ($(this).hasClass('type_association_shelves') && $(this).is(':checked')) {
                        $('.asociation-shelves').removeClass('hide');
                        self.get_data({type: $('.type_association_shelves').val(), warehouse_id: warehouse_id});

                    } else if ($(this).hasClass('type_association_shelves') && $(this).not(':checked')) {
                        $('.asociation-shelves').addClass('hide');
                        $('#asociation_shelves').html('');
                        $('#asociation_shelves').select2('destroy');
                        $('#asociation_shelves').select2();
                    }

                    if ($(this).hasClass('type_association_departments') && $(this).is(':checked')) {
                        $('.asociation-departments').removeClass('hide');
                        self.get_data({type: $('.type_association_departments').val(), warehouse_id: warehouse_id});

                    } else if ($(this).hasClass('type_association_departments') && $(this).not(':checked')) {
                        $('.asociation-departments').addClass('hide');
                        $('#asociation_departments').html('');
                        $('#asociation_departments').select2('destroy');
                        $('#asociation_departments').select2();
                    }

                    if ($(this).hasClass('type_association_products_bougth') && $(this).is(':checked')) {
                        $('.asociation-products-bougth').removeClass('hide');
                        self.get_data({type: $('.type_association_products_bougth').val(), warehouse_id: warehouse_id});

                    } else if ($(this).hasClass('type_association_products_bougth') && $(this).not(':checked')) {
                        $('.asociation-products-bougth').addClass('hide');
                        $('#asociation_products_bougth').html('');
                        $('#asociation_products_bougth').select2('destroy');
                        $('#asociation_products_bougth').select2();
                    }

                });

                $('#sample-products-list').DataTable({
                    "oLanguage": {
                        "sUrl": '{{ web_url() }}/admin_asset/js/plugins/datatables/language/Spanish.json'
                    }
                });

                if (this.store_product_id != null) {
                    self.get_data({
                        type: 'product',
                        store_product_id: this.store_product_id,
                        warehouse_id: this.warehouse_id
                    });
                }
            }

            productSamples.prototype.ajax_select_warehouse = function () {
                var self = this;
                $.ajax({
                    type: 'GET',
                    url: self.url_warehouses_ajax,
                    data: {city_id: $('#city_id').val()},
                    dataType: 'json',
                    success: function (request) {
                        var options = '';
                        $.each(request, function (index, name) {
                            options += '<option value="' + index + '">' + name + '</option>';
                        })
                        $('#warehouse_id').html(options);
                        //$('#search-products').trigger('click');
                    }
                });
            }

            productSamples.prototype.get_data = function (data_to_send) {
                var self = this;

                $.ajax({
                    type: 'POST',
                    url: self.url_get_data_ajax,
                    data: data_to_send,
                    dataType: 'json',
                    success: function (request) {
                        if (request.status) {
                            switch (data_to_send.type) {
                                case 'product':
                                    self.show_product(request.result);
                                    break;
                                case 'Productos':
                                    self.show_assocc_product(request.result);
                                    break;
                                case 'Producto Comprado':
                                    self.show_assocc_product_bougth(request.result);
                                    break;
                                case 'Pasillos':
                                    self.show_assocc_shelve(request.result);
                                    break;
                                case 'Departamentos':
                                    self.show_assocc_departamentos(request.result);
                                    break;
                            }
                        }
                    }
                })
            }

            productSamples.prototype.show_product = function (product) {
                $("#product-content").html(product);

                /*if (this.type != null) {
                    $("#type_association").val(this.type);
                    $("#type_association").trigger('change');
                }*/

                @if(isset($types))
                @if(count($types))
                @foreach($types as $type)
                $('.type_association').each(function () {
                    if ($(this).val() === '{{ $type->type }}') {
                        $(this).prop('checked');
                        $(this).trigger('click');
                    }
                });
                @endforeach
                @endif
                @endif

                if (this.sampling_product_id != null) {
                    $('#sampling_product_id').val(this.sampling_product_id);
                    $('#quantity').val(this.quantity);
                    $('#brand').val(this.brand);
                    this.show_assocc_zones();
                }
                $('#sampling-product-form').validate();
            }

            productSamples.prototype.show_assocc_product = function (data) {
                var self = this
                var options = '';
                $('.asociation-products label').html('Productos')
                $.each(data, function (index, prod) {
                    //console.log(self.store_product_ids);
                    var prop = '';
                    if (self.store_product_ids != null && self.store_product_ids.indexOf(prod.id) >= 0) {
                        prop = 'selected="selected"';
                    }
                    options += '<option value="' + prod.id + '" ' + prop + '>' + prod.reference + ' - ' + prod.name + '</option>';
                })
                $('#asociation_products').html(options);
                $('#asociation_products').select2('destroy');
                $('#asociation_products').select2();
            }

            productSamples.prototype.show_assocc_product_bougth = function (data) {
                var self = this
                var options = '';
                $('.asociation-products-bougth label').html('Productos Comprados')
                $.each(data, function (index, prod) {
                    var prop = '';
                    if (self.store_product_bougth_ids != null && self.store_product_bougth_ids.indexOf(prod.id) >= 0) {
                        prop = 'selected="selected"';
                    }
                    options += '<option value="' + prod.id + '" ' + prop + '>' + prod.reference + ' - ' + prod.name + '</option>';
                })
                $('#asociation_products_bougth').html(options);
                $('#asociation_products_bougth').select2('destroy');
                $('#asociation_products_bougth').select2();
            }

            productSamples.prototype.show_assocc_shelve = function (data) {
                var self = this
                var options = '';
                $('.asociation-shelves label').html('Pasillos')
                $.each(data, function (index, shelve) {
                    var prop = '';
                    if (self.shelve_ids != null && self.shelve_ids.indexOf(shelve.id) >= 0) {
                        prop = 'selected="selected"';
                    }
                    options += '<option value="' + shelve.id + '" ' + prop + '>' + shelve.name + '</option>';
                })
                $('#asociation_shelves').html(options);
                $('#asociation_shelves').select2('destroy');
                $('#asociation_shelves').select2();
            }

            productSamples.prototype.show_assocc_departamentos = function (data) {
                var self = this
                var options = '';
                $('.asociation-departments label').html('Departamentos')
                $.each(data, function (index, departament) {
                    var prop = '';
                    if (self.department_ids != null && self.department_ids.indexOf(departament.id) >= 0) {
                        prop = 'selected="selected"';
                    }
                    options += '<option value="' + departament.id + '" ' + prop + '>' + departament.name + '</option>';
                })
                $('#asociation_departments').html(options);
                $('#asociation_departments').select2('destroy');
                $('#asociation_departments').select2();
            }

            productSamples.prototype.show_assocc_zones = function () {
                var self = this
                if (self.zone_ids != null) {
                    $.each($('#zone_ids').find('option'), function (index, option) {
                        var val = parseInt($(option).val());
                        if (self.zone_ids.indexOf(val) >= 0) {
                            $(option).attr('selected', 'selected');
                        }
                    });
                    $('#zone_ids').select2('destroy');
                    $('#zone_ids').select2();
                }
            }

            productSamples.prototype.search_products = function () {
                var self = this;
                //$.fancybox.showActivity();
                $.ajax({
                    type: 'GET',
                    url: self.url_search_products_ajax,
                    data: {warehouse_id: $('#warehouse_id').val(), term: $('#search-product').val()},
                    dataType: 'html',
                    success: function (request) {
                        $.fancybox(
                            request,
                            {
                                'autoDimensions': false,
                                'width': 500,
                                'height': 'auto',
                                'transitionIn': 'none',
                                'transitionOut': 'none'
                            }
                        );

                    }
                });
            }

            productSamples.prototype.show_modal = function (action, param) {
                var self = this;
                if (action == 'storaged-products') {
                    self.refil_storage_modal(param);
                }

                $('#modal-' + action).modal('show');
            }


            return productSamples;
        }());

        var picking_supplier = new productSamples;
    </script>
@endsection
@else
@section('search-products')
    @if(isset($products))
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Selecciona el producto de muestra</h3>

                    <div class="box-tools pull-right">
                        <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->
                        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" onclick="$.fancybox.close();"><i class="fa fa-times"></i></button> -->
                    </div>
                </div>
                <div class="box-body" id="form_search_start_supplier">
                    <table class="table table-bordered table-hover dataTable" id="search-products-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Imagen</th>
                            <th>Referencia</th>
                            <th>Nombre</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($products))
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td><img src="{{$product->image_medium_url}}" class="img-responsive" width="80"
                                             height="80"></td>
                                    <td>{{$product->reference}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>
                                        <button data-id="{{$product->id}}" data-warehouse="{{$warehouse_id}}"
                                                class="btn btn-block btn-success select-product">Selecciona
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            $('#search-products-table').DataTable({
                "oLanguage": {
                    "sUrl": '{{ web_url() }}/admin_asset/js/plugins/datatables/language/Spanish.json'
                }
            });
        </script>
    @endif
@endsection
@section('selected-product')
    @if(isset($product))

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Producto de muestra</h3><br>

                <div class="box-tools pull-right">
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" onclick="$.fancybox.close();"><i class="fa fa-times"></i></button> -->
                </div>
            </div>
            <div class="box-body" id="form_search_start_supplier">
                <form id="sampling-product-form" method="post" action="{{ route('samplingProducts.save') }}"
                      autocomplete="off">
                    <input type="hidden" name="sampling_product_id" id="sampling_product_id" value="">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Imagen</label>
                                <img src="{{$product->image_medium_url}}" class="img-responsive" width="200">
                                <input type="hidden" name="store_product_id" id="store_product_id"
                                       value="{{$product->id}}">
                                <input type="hidden" name="warehouse_id" id="product_warehouse_id"
                                       value="{{$warehouse_id}}">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Producto</label>
                                        <input type="text" name="product_name" value="{{$product->name}}" class="form-control"
                                               disabled>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Referencia</label>
                                        <input type="text" name="product_reference" value="{{$product->reference}}"
                                               class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            @if($action == 'edit')
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Ciudad </label>
                                    <input type="text" class="form-control" value="{{ $warehouse->city->city }}" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label>Bodega </label>
                                    <input type="text" class="form-control" value="{{ $warehouse->warehouse }}"disabled>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <label>Unid. Entregadas</label>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="number" min="0" id="unit_delivered" name="unit_delivered"
                                               value="{{ $product->delivery }}" class="form-control" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><br></div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Marca</label>
                                        <input type="text" id="brand" name="brand" value="{{$product->brand}}"
                                               class="form-control" required/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <label>Cant. disponible</label>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="number" min="0" id="quantity" name="quantity"
                                               value="{{$product->quantity}}" class="form-control" required/>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div>
                                <label>Tipo</label>
                            </div>
                            <label class="checkbox-inline"><input name="type_association_products" type="checkbox"
                                                                  class="type_association type_association_products"
                                                                  value="Productos"/>Producto</label>
                            <label class="checkbox-inline"><input name="type_association_shelves" type="checkbox"
                                                                  class="type_association type_association_shelves"
                                                                  value="Pasillos"/>Pasillo</label>
                            <label class="checkbox-inline"><input name="type_association_departments" type="checkbox"
                                                                  class="type_association type_association_departments"
                                                                  value="Departamentos"/>Departamento</label>
                            <label class="checkbox-inline"><input name="type_association_products_bougth"
                                                                  type="checkbox"
                                                                  class="type_association type_association_products_bougth"
                                                                  value="Producto Comprado"/>Producto Comprado</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="zones">
                                <label>Zonas</label>
                                <select id="zone_ids" name="zone_ids[]" class="form-control" multiple="multiple">
                                    <option value="">Seleccione</option>

                                    @if(isset($zones) && count($zones))
                                        @foreach($zones AS $zone)
                                            <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3 asociation-products hide">
                            <div class="form-group">
                                <label> </label>
                                <select id="asociation_products" name="asociation_products[]" class="form-control "
                                        multiple="multiple" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 asociation-shelves hide">
                            <div class="form-group">
                                <label> </label>
                                <select id="asociation_shelves" name="asociation_shelves[]" class="form-control "
                                        multiple="multiple" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 asociation-departments hide">
                            <div class="form-group">
                                <label> </label>
                                <select id="asociation_departments" name="asociation_departments[]"
                                        class="form-control "
                                        multiple="multiple" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 asociation-products-bougth hide">
                            <div class="form-group">
                                <label> </label>
                                <select id="asociation_products_bougth" name="asociation_products_bougth[]"
                                        class="form-control " multiple="multiple" required>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-primary pull-right sampling-product-form-save">Guardar</a>
            </div>
        </div>

        <script type="text/javascript">
            $('#asociation_products').select2();
            $('#asociation_shelves').select2();
            $('#asociation_departments').select2();
            $('#asociation_products_bougth').select2();
            $('#zone_ids').select2();
        </script>
    @endif
@endsection
@endif