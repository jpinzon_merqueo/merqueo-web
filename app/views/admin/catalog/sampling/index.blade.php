@if (!Request::ajax())
    @extends('admin.layout')
    @section('content')
    <link href="{{ asset_url() }}/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
	        <a type="button" class="btn btn-primary" style="color:#fff;" href="{{ route('samplingProducts.add') }}"><i class="fa fa-plus"></i> Nuevo producto de muestra</a>
		</span>
    </section>
    <style type="text/css">
    	.dataTables_empty{
    		text-align: center;
    	}
    </style>
    <section class="content">
	    @if(Session::has('message') )
	        @if(Session::get('type') == 'success')
	            <div class="alert alert-success alert-dismissable">
	                <i class="fa fa-check"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <b>Información</b> {{ Session::get('message') }}
	            </div>
	        @else
	            <div class="alert alert-danger alert-dismissable">
	                <i class="fa fa-ban"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <b>Alerta!</b> {{ Session::get('message') }}
	            </div>
	        @endif
	    @endif
	    <div class="row">
	    	<div class="col-xs-12 col-md-12">
	    		<div class="box box-primary">
	    			<div class="box-header with-border">
						<!-- <h3 class="box-title">Removable</h3> -->

						<!-- <div class="box-tools pull-right">
							
						</div> -->
						<!-- /.box-tools -->
					</div>
	    			<div class="box-body" id="form_search_start_supplier">
	    				<div class="row error-search">
				            <div class="col-xs-12">
				                <div class="alert alert-danger unseen"></div>
				                <div class="alert alert-success unseen"></div>
				            </div>
				        </div>
				        <form id="picking-supplier-form" action="{{ route('samplingProducts.index') }}" autocomplete="off" >
	    					<div class="row">
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label>Ciudad </label>
			    						<select name="city_id" id="city_id" class="form-control">
			    							@foreach($cities AS $city)
			    							<option value="{{$city->id}}">{{$city->city}}</option>
			    							@endforeach
			    						</select>
			    					</div>
	    						</div>
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label>Bodega </label>
			    						<select name="warehouse_id" id="warehouse_id" class="form-control">
			    							<option value="">Selecciona</option>
			    						</select>
			    					</div>
	    						</div>
	    						<div class="col-md-3">
			    					<div class="form-group">
			    						<label> </label>
			    						<button style="margin-top:25px; " class="btn btn-primary" id="search-products">Buscar</button>
			    					</div>
	    						</div>
	    					</div>
	    				</form>
				    </div>
				</div>
			</div>
			<div class="col-xs-12 col-md-12">
	    		<div class="box box-primary">
	    			<div class="box-body" id="form-sample-products-list">
				        <table id="sample-products-list" class="table table-bordered table-hover dataTable">
				        	<thead>
				        		<tr>
				        			<th>ID</th>
				        			<th>Nombre</th>
				        			<th>Relación</th>
				        			<th>Acciones</th>
				        		</tr>
				        	</thead>
				        	<tbody id="sample-products-list-tbody">
				        		
				        	</tbody>
				        	
				        </table>
				    </div>
				</div>
			</div>
		</div>
	
	</section>


	<script type="text/javascript">
		var productSamples = (function(){
        	'use strict';
            function productSamples( args ){
                if( !( this instanceof productSamples ) ){
                    return new productSamples( args );
                }
                this.url_warehouses_ajax = '{{ route("admin.get_warehouses_ajax") }}';
                this.url_get_sample_products_ajax = '{{route("samplingProducts.index")}}';
                
                this.bindActions();
            }

            productSamples.prototype.bindActions = function(){
                var self = this;
				$('#city_id').on('change', function(event){
					if( $(this).val() != "" ){
						self.ajax_select_warehouse();
					}
				});
				$('#search-products').on('click', function(event){
					event.preventDefault();
					if( $('#warehouse_id').val() != "" ){
						self.ajax_show_sample_products();
					}
				});
				$('#city_id').trigger('change');

				
				$('#sample-products-list').DataTable({
					"oLanguage": {
						"sUrl": '{{ web_url() }}/admin_asset/js/plugins/datatables/language/Spanish.json'
					}
				});

				$('body').on('click', '.delete-sample', function(event){
					event.preventDefault();
					if(confirm('¿Esta seguro de eliminar este producto de muestra?')){
						location.href = $(this).attr('href');
					}
				});
            }

            productSamples.prototype.ajax_select_warehouse = function(){
            	var self = this;
            	$.ajax({
            		type : 'GET',
            		url  : self.url_warehouses_ajax,
            		data : { city_id : $('#city_id').val() },
            		dataType : 'json',
            		success : function( request ){
            			var options = '';
	                    $.each(request, function( index, name){
	                        options += '<option value="'+index+'">'+name+'</option>';
	                    })
	                    $('#warehouse_id').html(options);
	                    $('#search-products').trigger('click');
            		}
            	});
            }

            productSamples.prototype.ajax_show_sample_products = function(){
            	var self = this;
            	let img_load = '{{ asset('assets/img/loading.gif') }}';
                $('#sample-products-list-tbody').html('<tr><td colspan="8" align="center"><img class="u-loader" src="' + img_load + '" alt=""></td></tr>');
            	$.ajax({
            		type : 'GET',
            		url  : self.url_get_sample_products_ajax,
            		data : { warehouse_id : $('#warehouse_id').val() },
            		dataType : 'html',
            		success : function( request ){
            			$('#form-sample-products-list').html(request);
		            	$('#sample-products-list').DataTable({
							"oLanguage": {
								"sUrl": '{{ web_url() }}/admin_asset/js/plugins/datatables/language/Spanish.json'
							}
						});
            		}
            	});
            }

            productSamples.prototype.show_modal = function(action, param){
            	var self = this;
            	if(action=='storaged-products'){
        			self.refil_storage_modal(param);
            	}
            	
            	$('#modal-'+action).modal('show');
            }

        
    		return productSamples;
    	}());
    
    	var picking_supplier = new productSamples;
	</script>

    @endsection
@else
	@section('table')
		<table id="sample-products-list" class="table table-bordered table-hover table-striped dataTable">
        	<thead>
        		<tr>
        			<th>ID</th>
        			<th>Imagen</th>
        			<th>Nombre</th>
        			<th>Tipo</th>
        			<th>Marca</th>
					<th>Unidades entregadas</th>
        			<th>Estado</th>
        			<th>Acciones</th>
        		</tr>
        	</thead>
        	<tbody id="sample-products-list-tbody">
			@if(count($samples_products) > 0)
				@foreach($samples_products as $sample_product)
                    <tr>
						<td>{{$sample_product->id}}</td>
						<td><img src="{{$sample_product->image_small_url}}" class="img-responsive" ></td>
						<td>{{$sample_product->product_name}}</td>
						<td>@if(count($sample_product->type) > 0)
							<?php $count = count($sample_product->type); $i = 1;?>
							@foreach($sample_product->type AS $type)
									{{ $type->type }}
									@if($i < $count)
										,
									@endif
									<?php $i++; ?>
								@endforeach
							@endif
						</td>
						<td>{{$sample_product->brand}}</td>
						<td>{{ $sample_product->delivered }}</td>
						<td>
							@if($sample_product->status == 1)
                    		<span class="badge bg-green">Activo</span>
                    		@else
                    		<span class="badge bg-red">Inactivo</span>
                    		@endif
						</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
		                        	<span class="caret"></span>
		                        </button>
		                        <ul class="dropdown-menu">
		                        	<li>
		                        		<a href="{{route('samplingProducts.edit', ['id' => $sample_product->id])}}">Editar</a>
		                        	</li>
		                        	<li>
		                        		@if($sample_product->status == 1)
		                        		<a class="delete-sample" href="{{route('samplingProducts.delete', ['id' => $sample_product->id])}}">Desactivar</a>
		                        		@else
		                        		<a class="delete-sample" href="{{route('samplingProducts.delete', ['id' => $sample_product->id])}}">Activar</a>
		                        		@endif
		                        	</li>
		                        </ul>
							</div>
						</td>
					</tr>
				@endforeach
			@endif
        	</tbody>
        </table>
	@endsection
@endif

<!-- [id] => 1
                                    [store_product_id] => 996
                                    [warehouse_id] => 1
                                    [type] => Departamentos
                                    [parent_store_product_ids] => 
                                    [department_ids] => 
                                    [shelve_ids] => 24,19,67
                                    [status] => 1
                                    [created_at] => 2018-03-16 19:07:42
                                    [updated_at] => 2018-03-16 19:07:42
                                    [product_name] => Enlatado atún lomitos Isabel Agua 175 Gr -->