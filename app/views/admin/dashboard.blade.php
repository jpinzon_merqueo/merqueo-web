@extends('admin.layout')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="jumbotron" style="background-color: #dd4b39 !important; color: #fff;">
                <h1>Esta versión del dashboard está obsoleta</h1>
                <h2><a href="https://n-dashboard.merqueo.com/admin-m3rqu30/" style="color:white; text-decoration: underline">Use la nueva versión</a></h2>
                <p>Todas las rutas, excepto las de planeación, fueron eliminadas</p>
            </div>
        </div>
    </div>
</section>

@stop