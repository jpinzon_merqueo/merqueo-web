@if(!Request::ajax())
@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

    @if(Session::has('type') && Session::has('type') == 'failed')
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alerta!</b> {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div>
                <div class="box-body">
                    <form role="form" method="POST" id="main-form" action="{{ route('adminZones.save') }}" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="{{ $zone->id }}">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Nombre</label>
                                <input name="name_zones" class="form-control new-name" value="{{ $zone->name }}" type="text" placeholder="Nombre">
                            </div>
                            <div class="col-sm-6">
                                <label>Ciudad</label>
                                <select class="form-control get-warehouses" name="city_id" id="city_id">
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" @if ($zone && $zone->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label title="Bodega" for="warehouse_id">Bodega</label>
                                <select class="form-control" id="warehouse_id" name="warehouse_id">
                                    <option value="" selected="selected">-Selecciona-</option>
                                    @foreach($warehouses as $warehouse)
                                        <option value="{{$warehouse->id}}" @if ($zone && $zone->warehouse_id ==
                                        $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label title="Estado" for="status">Estado</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{ $zone && (1 === $zone->status) || Input::old('warehouse_id')
                                     === 1 ?"selected" : ''}}>Activo</option>
                                    <option value="0" {{ $zone && (0 === $zone->status) || Input::old('warehouse_id')
                                    === 0 ? "selected" : ''}}>Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Número maxímo de pedidos por ruta</label>
                                <input name="orders_per_route" class="form-control new-name" value="{{ $zone->orders_per_route }}" type="text" placeholder="Número maxímo de pedidos por ruta">
                            </div>
                            <div class="col-sm-6">
                                <label title="Estado" for="status">¿Permite entrega para mismo día?</label>
                                <select class="form-control" id="delivery_on_same_day" name="delivery_on_same_day">
                                    <option value="0" {{ $zone && (0 === $zone->delivery_on_same_day) ? "selected" : ''}}>No</option>
                                    <option value="1" {{ $zone && (1 === $zone->delivery_on_same_day)  ?"selected" : ''}}>Sí</option>
                                </select>
                            </div>
                        </div>
                        @if(isset($zone->id) && $zone->id > 0)
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#frm-modal">Limite de pedidos por franja horaria</button>
                            </div>
                        </div>
                        @endif

                        <br>

                        <div class="form-group row">
                            <div class="col-sm-3">
                                <div class="box-header">
                                    <h3 class="box-title">Cobertura para mañana</h3>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="btn btn-primary show-map">Ocultar mapa</div>
                            </div>
                            <div id="block-normal-day">
                                <div class="col-sm-12">
                                    <div id="map_canvas"></div>
                                </div>
                                <div class="col-md-12">
                                    <br><div class="btn btn-primary undo-poly pull-right">Deshacer</div>
                                    <input type="hidden" id="data_zones" name="delivery_zone" value="{{ $zone->delivery_zone }}"><br>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <br>

                        @if ($edit)
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <fieldset>
                                        <legend>Cobertura con restricción horaria</legend>
                                        <button id="add-new" type="button" class="btn btn-success add-new">Nueva restricción horaria</button>
                                        <div id="zone_location_slots_table_container" style="max-height: 400px; overflow-y: auto;">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Fecha de creación </th>
                                                    <th>Estado </th>
                                                    <th>Acción</th>
                                                </tr>
                                                </thead>
                                                <tbody class="zones">
                                                    <tr><td colspan="5" align="center">No hay restricciones.</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        @endif
                        <div class="box-footer row">
                            <div class="col-md-11"></div>
                            <div class="col-md-1" style="text-align: right;">
                                <button class="btn btn-primary save-zone">Guardar </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="frm-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Limite de pedidos por franjas horarias</h4>
            </div>
            <div class="modal-body">
                <form id="frm-add-delivery-window">
                    <input type="hidden" id="id-window" name="id-window">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($delivery_windows as $delivery_window)
                                <div class="form-group">
                                    <label for="">Número maxímo de pedidos por ruta de {{$delivery_window->delivery_window}}</label>
                                    <input name="delivery_window[{{$delivery_window->id}}]" data-id="{{$delivery_window->id}}" id="delivery_window-{{$delivery_window->id}}" class="form-control delivery_windows_fields" value="{{$zone->getOrdersPerRoute($delivery_window->id)}}" type="number" placeholder="Número maxímo de pedidos por horario">
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-12">
                            <h4>Franjas para el mismo día</h4>
                            @foreach($delivery_windows_same_day as $delivery_window)
                                <div class="form-group">
                                    <label for="">Número maxímo de pedidos por ruta de {{$delivery_window->delivery_window}}</label>
                                    <input name="delivery_window[{{$delivery_window->id}}]" data-id="{{$delivery_window->id}}" id="delivery_window-{{$delivery_window->id}}" class="form-control delivery_windows_fields" value="{{$zone->getOrdersPerRoute($delivery_window->id)}}" type="number" placeholder="Número maxímo de pedidos por horario">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary btn-save-delivery-window" data-dismiss="modal">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-zone">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title edit-title">Editar cobertura con restricción horaria</h4>
                <h4 class="modal-title add-title">Nueva cobertura con restricción horaria</h4>
            </div>
            <form id="form-zone">
                <div class="modal-body">
                    <input class="form-control" name="zone-id" id="zone-id" type="hidden">
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label>Nombre de restricción</label>
                            <input class="form-control new-name-modal" name="new-name" type="text" placeholder="Nombre restricción"><br>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="alert alert-danger" style="display:none;" role="alert">
                                <p>Área vacía en zona de delimitación</p>
                            </div>
                            <div id="map_canvas_modal"></div><br>
                            <div class="btn btn-primary undo-poly-modal">Deshacer</div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary save-zone-modal" id="save-zone-modal">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade lg" id="modal-location-slots">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title add-title">Horarios de restricción</h4>
            </div>
            <form id="form-location-slot">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <label for="location-slot-day">Día</label>
                            <select name="location-slot-day" id="location-slot-day" class="form-control">
                                <option value="0">Domingo</option>
                                <option value="1">Lunes</option>
                                <option value="2">Martes</option>
                                <option value="3">Miércoles</option>
                                <option value="4">Jueves</option>
                                <option value="5">Viernes</option>
                                <option value="6">Sábado</option>
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <label for="shift_delivery">Horario de entrega</label>
                            <select name="shift_delivery" id="shift_delivery" class="form-control">
                                <optgroup label="Franjas normales">
                                    @foreach($delivery_windows as $delivery_window)
                                        <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="Mismo día">
                                    @foreach($delivery_windows_same_day as $delivery_window)
                                        <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <button type="button" class="btn btn-primary" id="save-location-slot" data-location="" style="margin-top:25px;">Guardar</button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        Domingo
                                    </th>
                                    <th>
                                        Lunes
                                    </th>
                                    <th>
                                        Martes
                                    </th>
                                    <th>
                                        Miércoles
                                    </th>
                                    <th>
                                        Jueves
                                    </th>
                                    <th>
                                        Viernes
                                    </th>
                                    <th>
                                        Sábado
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="zone_location_slots">
                                @yield('zone_location_slots')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="location-id" value="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key={{ Config::get('app.google_api_key') }}"></script>
<script src="{{ web_url() }}/admin_asset/js/coverage-map.js" type="text/javascript"></script>

<style>
#map_canvas, #map_canvas_modal{
    width: 100%;
    height: 400px;
}
.new-name {
    margin-bottom: 10px;
}
</style>

<script type="text/javascript">
var zones = {{ json_encode($zones_map) }};
var lat;
var lng;
@if ($edit)
    var restricted_zones = {{ json_encode($locations) }};
    var zone_id = {{ $zone->id }};
@endif
$(document).on('ready',function() {
    $('.alert ').delay(5000).fadeOut();
    @if(isset($zone->id) && $zone->id)
    $('.btn-save-delivery-window').on('click', function () {
        var zone_id = '{{$zone->id}}';
        var nro_orders = get_orders_per_delivery_window();
        $.ajax({
            url : '{{ route("adminZones.saveOrdersPerDeliveryWindow") }}',
            type : 'POST',
            data : { zone_id : zone_id, nro_orders : nro_orders },
            success : function ( request ) {
                if(request.result){
                    alert(request.message)
                }else{
                    alert(request.message);
                }
            },
            error : function (request) {

            }
        })
    })
    @endif
    var map = new MapPosition(4.687376662169584, -74.0848549141556, $('#map_canvas')[0]);

    map.init();
    map.canvasMap.setZoom(13);

    if (zones.delivery_zone != null && zones.delivery_zone.length) {
        var coverage = zones.delivery_zone.split(',');
        for (var i = 0; i < coverage.length; i++) {
            var c = coverage[i].split(' ');
            if (i == 0) {
                lat = parseFloat(c[0]);
                lng = parseFloat(c[1]);
            }

            map.addLatLng(new google.maps.LatLng(c[0], c[1]));
        }
        map.center(new google.maps.LatLng(lat, lng));
        map.centerByPolygon(coverage);
    }

    @if ($edit)
        $.ajax({
            url: '{{ route('adminZones.getLocation', ['id' => $zone->id]) }}',
            type: 'GET',
            dataType: 'json'
        }).done(function(data) {
            $('#zone_location_slots_table_container').empty();
            $('#zone_location_slots_table_container').html(data.html);
        })
        .fail(function() {
            console.error("error al cargar los puntos de servicio");
        });

        var mapModal = new MapPosition(4.687376662169584, -74.0848549141556, $('#map_canvas_modal')[0], function (event) {
            let cc = zones.delivery_zone.split(',')
                .map(function (element) {
                    return element.split(' ').map(function (number) {
                        return parseFloat(number);
                    });
                });

            return MapPosition.pointInPolygon(cc, event.latLng.lat(), event.latLng.lng());
        });

        var editing = -1;

        $('.add-new').click(function() {
            editing = -1;
            $('.edit-title').hide();
            $('.add-title, .new-name-modal').show();
            $('.new-name-modal').val('');
            $('#zone-id').val('');
            $('#modal-zone').modal('show');
            mapModal.init();
            // mapModal.addLatLng(new google.maps.LatLng(c[0], c[1]));
            mapModal.canvasMap.setZoom(13);
            if (zones.delivery_zone != null && zones.delivery_zone.length) {
                var coverage = zones.delivery_zone.split(',');
                for (var i = 0; i < coverage.length; i++) {
                    var c = coverage[i].split(' ');
                    if (i == 0) {
                        lat = parseFloat(c[0]);
                        lng = parseFloat(c[1]);

                    }
                }
                mapModal.center(new google.maps.LatLng(lat, lng));
                mapModal.centerByPolygon(coverage);
            }

            var primaryPolygon = new google.maps.Polygon({
                strokeColor: "#288626",
                strokeOpacity: 0.1,
                strokeWeight: 1,
                fillColor: "#55FF55",
                fillOpacity: 0.3,
                editable: false,
                paths: map.poly.getPath().getArray(),
            });

            google.maps.event.addListener(primaryPolygon, 'click', function (event) {
                mapModal.addLatLng(event.latLng);
            });

            mapModal.addPolygon(primaryPolygon);
        });

        $('#modal-zone').on('shown.bs.modal', function() {
            google.maps.event.trigger(mapModal.canvasMap, 'resize');
        });

        $('.undo-poly-modal').click(function() {
            mapModal.undo();
        });

        $('body').on('click', '.change-status', function () {
            $('.change-status').attr('disabled', true);
            var id = $(this).attr('data-id');
            $.ajax({
                url: '{{ admin_url() }}/zones/delivery-zone/change-status/' + id,
                type: 'POST',
                success: function(status) {
                    $.ajax({
                        url: '{{ route('adminZones.getLocation', ['id' => $zone->id]) }}',
                        type: 'GET',
                        dataType: 'json'
                    }).done(function(data) {
                        $('#zone_location_slots_table_container').empty();
                        $('#zone_location_slots_table_container').html(data.html);
                        $('.change-status').attr('disabled', false);
                    })
                        .fail(function() {
                            console.error("error al cargar los puntos de servicio");
                        });
                }
            });
        });

        $('body').on('click', '.save-zone-modal', function() {
            var validator = $('#form-zone').validate({
                rules: {
                    'new-name': {
                        required: true
                    }
                }
            });

            if (!mapModal.getCoordinates()){
                $('#map_canvas_modal').css('border', '1px solid red');
                $('.alert').css('display', 'block');
                return false;
            }

            if ( validator.form() ) {
                var data = {
                    zone_id: zone_id,
                    delivery_zone: mapModal.getCoordinates(),
                    name: $('.new-name-modal').val(),
                };

                if($('.new-name-modal').val() == ''){
                    alert('Ingresa el nombre de la restricción.');
                    return false;
                }

                if($('#zone-id').val() != '') {
                    editing = $('#zone-id').val();
                }

                $(this).attr('disabled', 'disabled');

                //adminZones.UpdateDeliveryZone
                $.ajax({
                    url: '{{ admin_url() }}/zones/delivery-zone/update/'+editing,
                    type: 'POST',
                    data: data,
                    success: function(status) {

                        $('#modal-zone').modal('hide');

                        // Si es una zona que tenemos, actualizo la variable:
                        if(editing != -1) {
                            for(var i in restricted_zones) {
                                var zone = restricted_zones[i];
                                if(zone.id == editing) {
                                    restricted_zones[i].delivery_zone = data.delivery_zone;
                                }
                            }
                        } else {
                            // Si es nueva, la agrego a la vista
                            data.id = status.id;
                            data.created_at = status.created_at;
                            restricted_zones.push(data);

                            $('.zones').append('<tr><td>' + data.name + '</td>\
                                                   <td>' + data.created_at + '</td>\
                                                   <td>\
                                                       <button type="button" class="btn btn-primary edit-zone" data-name="'+data.name+'" data-id="' + data.id + '">Ver cobertura</button>\
                                                       <button type="button" class="btn btn-primary location-slots" data-id="' + data.id + '">Horarios</button>\
                                                       <button type="button" class="btn btn-danger delete-zone" data-id="' + data.id + '">Eliminar</button>\
                                                   </td>\
                                               </tr>');
                            $('.new-name-modal').val('');
                            $('#map_canvas_modal').css('border', 'none');
                            $('.alert').css('display', 'none');
                        }
                        $.ajax({
                            url: '{{ route('adminZones.getLocation', ['id' => $zone->id]) }}',
                            type: 'GET',
                            dataType: 'json'
                        })
                        .always(function(){
                            $('.save-zone-modal').removeAttr('disabled');
                        })
                        .done(function(data) {
                            $('#zone_location_slots_table_container').empty();
                            $('#zone_location_slots_table_container').html(data.html);
                        })
                        .fail(function() {
                            console.error("error al cargar los puntos de servicio");
                        });
                    }
                });
            }
        });

        $('body').on('click', '.edit-zone', function() {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            editing = id;
            if(name != ''){
                $('.edit-title').show();
                $('.new-name-modal').val(name);
                $('#zone-id').val(id);
                $('.add-title').hide();
            }else{
                $('.edit-title').hide();
                $('.add-title').show();
            }
            for(var i in restricted_zones) {
                var zone = restricted_zones[i];
                if(zone.id == id) {
                    // Edito esta zona:
                    // Si tengo cobertura, la muestro:
                    if(zone.delivery_zone != null && zone.delivery_zone.length) {
                        mapModal.init();
                        mapModal.canvasMap.setZoom(13);
                        var primaryPolygon = new google.maps.Polygon({
                            strokeColor: "#288626",
                            strokeOpacity: 0.1,
                            strokeWeight: 1,
                            fillColor: "#55FF55",
                            fillOpacity: 0.3,
                            editable: false,
                            paths: map.poly.getPath().getArray(),
                        });

                        google.maps.event.addListener(primaryPolygon, 'click', function (event) {
                            mapModal.addLatLng(event.latLng);
                        });

                        mapModal.addPolygon(primaryPolygon);

                        var cobertura = zone.delivery_zone.split(',');

                        for(var i = 0; i < cobertura.length; i++) {
                            var c = cobertura[i].split(' ');
                            if (i == 0) {
                                lat = parseFloat(c[0]);
                                lng = parseFloat(c[1]);
                            }
                            mapModal.addLatLng(new google.maps.LatLng(c[0], c[1]));
                        }
                        mapModal.center(new google.maps.LatLng(lat, lng));
                        mapModal.centerByPolygon(cobertura);
                        $('#modal-zone').modal('show');
                    } else alert('Invalid delivery zone. (empty)');
                }
            }
        });

        $('body').on('click', '.delete-zone', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var btn = $(this);
            var $tr = $(this).closest('tr');
            if(confirm('¿Deseas eliminar la zona de resticción?')) {
                $.ajax({
                    url: '{{ admin_url() }}/zones/delivery-zone/delete/' + id,
                    type: 'POST',
                    success: function(status) {
                        $tr.find('td').fadeOut(1000, function(){
                            $tr.remove();
                        });
                    }
                });
            }
        });

        $('body').on('click', '.location-slots', function(event) {
            event.preventDefault();
            var location_slot_id = $(this).data('id');

            $.ajax({
                url: '{{ route('adminZones.getLocationSlot', ['id' => $zone->id]) }}',
                type: 'GET',
                dataType: 'json',
                data: {
                    location_slot_id: location_slot_id
                }
            })
                .done(function(data) {
                    if (data.success) {
                        $('#zone_location_slots').html(data.html);
                    }
                })
                .fail(function() {
                    console.error("error");
                });

            $('#save-location-slot').data('location', location_slot_id);
            $('#modal-location-slots').modal('show');
        });

        $('#save-location-slot').click(function (e) {
            e.preventDefault();
            var validator = $('#form-location-slot').validate({
                rules: {
                    'location-slot-day': {
                        required: true
                    }
                }
            });
            if ( validator.form() ) {
                var data = {
                    day: $('#location-slot-day').val(),
                    shift_delivery: $('#shift_delivery').val(),
                    location_id: $(this).data('location')
                };
                $(this).addClass('disabled');
                $.ajax({
                    url: '{{ route('adminZones.saveLocationSlot', ['id' => $zone->id]) }}',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    context: this
                })
                    .done(function(data) {
                        if (data.success) {
                            $('#zone_location_slots').html(data.html);
                        }
                    })
                    .fail(function(data) {
                        console.log("Ocurrio un error al guardar la información de las zonas.");
                    })
                    .always(function () {
                        $(this).removeClass('disabled');
                    });
            }
            $('#latitude, #longitude').attr('readonly', true);
        });

        $('body').on('click', '.remove-location-slot', function(event) {
            event.preventDefault();
            var location_slot_id = $(this).data('id');
            $('.remove-location-slot').attr('disabled', true);
            $.ajax({
                url: '{{ route('adminZones.deleteLocationSlotAjax', ['id' => $zone->id]) }}',
                type: 'POST',
                dataType: 'json',
                context: this,
                data: {
                    'location_slot_id' : location_slot_id
                }
            })
                .done(function(data) {
                    if (data.success) {
                        $(this).parent().remove();
                    }
                })
                .fail(function() {
                    console.error("error al borrar el horario");
                })
                .always(function() {
                    $('.remove-location-slot').removeAttr('disabled');
                });
        });

    @endif

    $('.save-zone').click(function(e) {
        e.preventDefault();
        var validator = $('#main-form').validate({
            rules: {
                name_zones: {
                    required: true
                },
                city_id: {
                  required: true
                },
                delivery_zone: {
                  required: true
                },
                zone_id: {
                    required:true
                },
                warehouse_id: {
                    required: true
                }
              }
        });

        if (validator.form()) {

            var dataact = $('#data_zones').val();
            if (!dataact && !map.getCoordinates()){
                $('#map_canvas').css('border', '1px solid red');
                $('.alert').css('display', 'block');
            }else{
                if (map.getCoordinates()){
                    $('#data_zones').val(map.getCoordinates());
                }
                $('#map_canvas').css('border', 'none');
                $('.alert').css('display', 'none');
                $('#main-form').submit();
            }
        }
    });

    $('.undo-poly').click(function() {
        map.undo();
    });
    $('.show-map').click(function() {
        if ($('#block-normal-day').is(':visible'))
            $('.show-map').html('Ver mapa');
        else $('.show-map').html('Ocultar mapa');
        $('#block-normal-day').toggle('slow');
    });
});
function get_orders_per_delivery_window() {
    var deliver_window_fields = [];
    $.each($('.delivery_windows_fields'), function (index, element) {
        var field = {delivery_window_id: $(element).data('id'), value: $(element).val()}
        deliver_window_fields.push(field);
    })
    return deliver_window_fields;
}
</script>
@stop
@endif
@if ( Request::ajax() && $section == 'zone_location_slots_table_container' )
@section('zone_location_slots_table_container')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Fecha de creación </th>
            <th>Estado </th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody class="zones">
        @if (count($locations))
            @foreach ($locations as $location)
                <tr>
                    <td>{{ $location->name }}</td>
                    <td>{{ $location->date }}</td>
                    <td>
                        @if($location->status == 1)
                            <span class="badge bg-green">Activo</span>
                        @else
                            <span class="badge bg-red">Inactivo</span>
                        @endif
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary edit-zone" data-name="{{ $location->name }}" data-id="{{ $location->id }}">Ver cobertura</button>
                        <button type="button" class="btn btn-primary location-slots" data-id="{{ $location->id }}">Horarios</button>
                        <button type="button" class="btn @if($location->status == 1) btn-danger @else btn-success @endif change-status" data-id="{{ $location->id }}">@if($location->status == 1) Desactivar @else Activar @endif</button>
                        <button type="button" class="btn btn-danger delete-zone" data-id="{{ $location->id }}">Eliminar</button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5" align="center">
                    No hay restricciones.
                </td>
            </tr>
        @endif
        </tbody>
    </table>
@stop
@endif

@if ( Request::ajax() && $section == 'zone_location_slots' )
@section('zone_location_slots')
    @if (count($slot_obs))
        <tr>
            @if ( !empty( $slot_obs[0] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[0] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[1] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[1] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[2] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[2] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[3] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[3] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[4] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[4] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[5] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[5] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[6] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[6] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
            @if ( !empty( $slot_obs[7] ) )
                <td>
                    <table class="table table-striped table-bordered">
                        @foreach ($slot_obs[7] as $slot_item)
                            <tr>
                                <td class="text-center">
                                    {{ $slot_item['delivery_window'] }} <button class="btn btn-danger btn-xs pull-right remove-location-slot" type="button" data-id="{{ $slot_item['id'] }}">X</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            @else
                <td></td>
            @endif
        </tr>
    @else
        <tr>
            <td colspan="8" align="center">No hay horarios</td>
        </tr>
    @endif
@stop
@endif