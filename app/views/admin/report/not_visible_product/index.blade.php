@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <style>
        .div-info-not-visible {
            border: 1px solid;
            margin-bottom: 5px;
        }
        /*.div-info-not-visible div, .div-info-not-visible div h4{
            text-align: center;
        }*/
        </style>
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

    	<div class="row">
    	    <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                    	<div class="row">
                    	    <div class="col-xs-12">
                    	        <form id="search-form">
                        	        <table width="100%" class="admin-shrinkages">
                                        <tr><td colspan="4"><div class="unseen alert alert-danger form-has-errors"></div></td></tr>
                                        <tr>
                                            <td align="right"><label>Ciudad:</label>&nbsp;</td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Tipo de producto:</label>
                                            </td>
                                            <td>
                                                <select class="form-control" id="type" name="type">
                                                    <option value="Simple" selected="">Simple</option>
                                                    <option value="Agrupado">Agrupado</option>
                                                    <option value="Proveedor">Proveedor</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
        				</div>
        				<br>
                    	<div class="paging">
                            <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Foto</th>
                                        <th>Id</th>
                                        <th>Referencia</th>
                                        <th>Nombre</th>
                                        <th>Stock actual</th>
                                        <th>Stock comprometido</th>
                                        <th>Tiempo inactivo</th>
                                        <th>Fecha inactivo</th>
                                        <th>Responsable</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="9" align="center">No hay datos.</td></tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 actualizaciones.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    var url_get_products_ajax = "{{ route('notVisibleProduct.getProductsListAjax') }}";
    var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";

    function get_products(){
        data = {
            warehouse_id: $('#warehouse_id').val(),
            type: $('#type').val()
        };
        $('#warehouse_id').attr('disabled', true);
        $.ajax({
            url: url_get_products_ajax,
            data: data,
            type: 'POST',
            dataType: 'html',
            success: function(response) {
                $('#warehouse_id').attr('disabled', false);
                $('.paging').html(response);
            }, error: function() {
                $('#warehouse_id').attr('disabled', false);
            }
        });
    }
    $(document).ready(function() {

        $('#warehouse_id').on('change', function(){
            get_products();
        });

        $('#type').on('change', function(){
            get_products();
        });

        $('#cities').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#cities').val() },
                success : function( request ){
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse_id').html(options);
                    get_products();
                }
            });
        });
        $('#cities').trigger('change');
        get_products();
        // setInterval(function(){ get_products(); }, 60000);
    });
    </script>
    @endsection
@else
    @section('content')

    <div class="row">

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">

                    <h3>{{ $qty_not_visibles }} / {{ $qty_all_products }}</h3>
                    <p>Productos No Visibles</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">
                    <br>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    @if($qty_all_products > 0)
                    <h3>{{ round(($qty_not_visibles / $qty_all_products) * 100, 1) }} %</h3>
                    @else
                    <h3>0%</h3>
                    @endif
                    <p>Porcentaje de Productos No Visibles</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">
                  <br>
                </a>
            </div>
        </div>

      </div>

    <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
        <thead>
            <tr>
                <th>Foto</th>
                <th>Id</th>
                <th>Referencia</th>
                <th>Nombre</th>
                <th>Stock actual</th>
                <th>Stock comprometido</th>
                <th>Tiempo inactivo</th>
                <th>Fecha inactivo</th>
                <th>Responsable</th>
            </tr>
        </thead>
        <tbody>
        @if (count($products))
            @foreach($products as $product)
            <tr>
                <td><img src="{{$product->image}}" class="img-responsive" width="60"></td>
                <td>{{$product->store_product_id}}</td>
                <td>{{$product->reference}}</td>
                <td>{{$product->product_name}}</td>
                <td>{{$product->current_stock}}</td>
                <td>{{$product->commited_stock}}</td>
                <td>{{$product->quantity_days}}</td>
                <td>{{$product->deactivate_date}}</td>
                <td>{{$product->admin_fullname}}</td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="19" align="center">No hay datos</td></tr>
        @endif
        </tbody>
    </table>
    @endif
@stop