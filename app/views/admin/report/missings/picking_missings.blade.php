@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script>
        var web_url_ajax = "{{ route('adminReport.pickingMissing') }}";
    </script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

    	<div class="row">
    	    <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                    	<div class="row">
                    	    <div class="col-xs-12">
                    	        <form id="search-form">
                        	        <table width="100%" class="admin-shrinkages">
                                        <tr><td colspan="4"><div class="unseen alert alert-danger form-has-errors"></div></td></tr>
                                        <tr>
                                            <td align="right"><label>Ciudad:</label>&nbsp;</td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
        				</div>
        				<br>
                    	<div class="paging">
                            <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Alistador</th>
                                        <th># Faltantes día</th>
                                        <th>Total alistado día</th>
                                        <th>% Faltantes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="10" align="center">No hay datos.</td></tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 actualizaciones.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    function targe_missings(){
        data = {
            city_id: $('#city_id').val(),
            warehouse_id: $('#warehouse_id').val()
        };
        $.ajax({
            url: web_url_ajax,
            data: data,
            type: 'GET',
            dataType: 'html',
            success: function(response) {
                $('.paging').html('');
                $('.paging-loading').hide();
                $('.paging').html(response);
            }, error: function() {
                $('.paging-loading').hide();
                $('.paging').html('Ocurrió un error al obtener los datos.');
            }
        });
    }
    $(document).ready(function() {
        var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
        targe_missings();
        setInterval(function(){ targe_missings(); }, 60000);

        $('#warehouse_id').on('change', function(){
            targe_missings();
        });

        $('#cities').on('change', function(){
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#cities').val() },
                success : function( request ){
                    var options = '';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#origin_warehouse_id').html(options);
                }
            });
        });
        $('#cities').trigger('change');
    });
    </script>
    @endsection
@else
    @section('content')
    <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
        <thead>
            <tr>
                <th>Alistador</th>
                <th># Faltantes día</th>
                <th>Total alistado día</th>
                <th>% Faltantes</th>
            </tr>
        </thead>
        <tbody>
        @if (count($missings))
            @foreach($missings as $missing)
            <tr>
                <td>{{ $missing->picker }}</td>
                <td align="center">{{ $missing->quantity_not_available }}</td>
                <td align="center">{{ $missing->quantity_fullfilled }}</td>
                <td align="center">@if($missing->quantity_fullfilled > 0){{ number_format(($missing->quantity_not_available / $missing->quantity_fullfilled) * 100, 1, ',', '.') }} % @endif</td>
            </tr>
            @endforeach
        @else
            <tr><td colspan="14" align="center">No hay datos</td></tr>
        @endif
        </tbody>
    </table>
    @endif
@stop