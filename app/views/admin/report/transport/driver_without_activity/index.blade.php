@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script src="{{ web_url() }}/admin_asset/js/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <style>
        .div-info-not-visible {
            border: 1px solid;
            margin-bottom: 5px;
        }
        .pie-charts{
            margin-bottom: 5px;
        }
        .form-group{
            min-height: 86px;
        }
        #returns_chart, #negative_rating_chart, #late_delivery_chart{
            height: 150px;
            padding-bottom: 25px;
            padding-top: 25px;
        }
        .btn-show-report{
            margin-top: 25px;
        }
        </style>
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <div class="form-group col-md-4">
                                        <label>Ciudad:</label>
                                        <select id="city_id" name="city_id" class="form-control get-warehouses" required>
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Bodega:</label>
                                        <select id="warehouse_id" name="warehouse_id" class="form-control" required>
                                            <option value="">Selecciona</option>
                                            @foreach ($warehouses as $warehouse)
                                                <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Franja horaria:</label>
                                        <select name="shift_delivery" id="shift_delivery" class="form-control">
                                            <option value="">Selecciona</option>
                                            <optgroup label="Franjas normales">
                                                @foreach($delivery_windows as $delivery_window)
                                                    <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                                @endforeach
                                            </optgroup>
                                            <optgroup label="Mismo día">
                                                @foreach($delivery_windows_same_day as $delivery_window)
                                                    <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Zona:</label>
                                        <select class="form-control" id="zone_id" name="zone_id" required>
                                            <option value="">Selecciona</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-md-4">
                                        <button type="button" class="btn btn-primary btn-show-report">Generar reporte</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row paging">
                            <div class="col-md-12 table-responsive">
                                <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Vehiculo</th>
                                            <th>Ruta</th>
                                            <th>Nombre transportador</th>
                                            <th>Conductor</th>
                                            <th>Hora última entrega</th>
                                            <th>Tiempo de inactividad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="6" align="center">No hay datos.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 registros.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    var url_get_vehicles_ajax = "{{ route('driverswithoutactivity.getVehicles') }}";
    var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
    var zones_url_ajax = "{{ route('driverswithoutactivity.getZones') }}";
    
    function get_vehicles(){
        if($('#search-form').valid()){
            data = $('#search-form').serialize();
            change_status_fields(true);
            $.ajax({
                url: url_get_vehicles_ajax,
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    change_status_fields(false);
                    $('.paging').html(response.result.table);
                
                    setTimeout(function(){ get_vehicles(); }, 60000);
                }, error: function() {
                    change_status_fields(false);
                }
            });
        }
    }

    function change_status_fields(status){
        $('#warehouse_id').attr('disabled', status);
        $('#delivery_time').attr('disabled', status);
        $('#zone_id').attr('disabled', status);
    }

    $(document).ready(function() {

        $('#warehouse_id').on('change', function(){
            change_status_fields(true);
            $.ajax({
                url : zones_url_ajax,
                data : { warehouse_id: $('#warehouse_id').val() },
                type : 'POST',
                dataType : 'json',
                success : function( request ){
                    var zones = request.result.zones
                    var options = '<option value="">Selecciona</option>';
                    options += '<option value="all">Todas las zonas</option>';
                    $.each(zones, function( index, zone){
                        options += '<option value="'+zone.id+'">'+zone.name+'</option>';
                    })
                    $('#zone_id').html(options);
                    change_status_fields(false);
                }, error: function(){
                    change_status_fields(false);
                }
            });
        });

        
        $('.btn-show-report').on('click', function(){
            get_vehicles();
        })

        $('#cities').on('change', function(){
            change_status_fields(true);
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#cities').val() },
                success : function( request ){
                    var options = '<option value="">Selecciona</option>';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse_id').html(options);
                    $('#warehouse_id').trigger('change');
                    change_status_fields(false);
                },error: function(){
                    change_status_fields(false);
                }
            });
        });
        $('#cities').trigger('change');
        $('#warehouse_id').trigger('change');

    });

    </script>
    @endsection
@else
    @section('content')
    <div class="col-md-12 table-responsive">
        <table id="provider-orders-table" class="admin-provider-orders table table-bordered ">
            <thead>
                <tr>
                    <th>Vehiculo</th>
                    <th>Ruta</th>
                    <th>Nombre transportador</th>
                    <th>Conductor</th>
                    <th>Hora última entrega</th>
                    <th>Tiempo de inactividad</th>
                </tr>
            </thead>
            <tbody>
            @if (count($orders))
                @foreach($orders as $order)
                <tr class="@if($order['diff']>15) {{'danger'}} @else {{'success'}} @endif">
                    <td>{{$order['plate']}}</td>
                    <td>{{$order['route']}}</td>
                    <td>{{$order['fullname']}}</td>
                    <td>{{$order['driver_name']}}</td>
                    <td>{{$order['management_date']}}</td>
                    <td>{{$order['formated_inactivity']}}</td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="19" align="center">No hay datos</td></tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{count($orders)}} registros.</div>
        </div>
    </div>
    @stop
@endif