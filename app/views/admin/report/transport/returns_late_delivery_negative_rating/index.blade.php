@if (!Request::ajax())

    @extends('admin.layout')

    @section('content')
    <script src="{{ web_url() }}/admin_asset/js/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="{{ web_url() }}/admin_asset/js/plugins/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <style>
        .div-info-not-visible {
            border: 1px solid;
            margin-bottom: 5px;
        }
        .pie-charts{
            margin-bottom: 5px;
        }
        .form-group{
            min-height: 86px;
        }
        #returns_chart, #negative_rating_chart, #late_delivery_chart{
            height: 150px;
            padding-bottom: 25px;
            padding-top: 25px;
        }
        .btn-show-report{
            margin-top: 25px;
        }
        </style>
        @if(Session::has('success') )
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error') )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form">
                                    <div class="form-group col-md-3">
                                        <label>Ciudad:</label>
                                        <select id="city_id" name="city_id" class="form-control get-warehouses get-delivery-windows" required>
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Bodega:</label>
                                        <select id="warehouse_id" name="warehouse_id" class="form-control" required>
                                            <option value="">Selecciona</option>
                                            @foreach ($warehouses as $warehouse)
                                                <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Franjas horarias:</label>
                                        <select class="form-control" id="delivery_time" name="delivery_time" required>
                                            <option value="">Selecciona</option>
                                            <optgroup label="Franjas normales">
                                                @foreach($delivery_windows as $delivery_window)
                                                    <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                                @endforeach
                                            </optgroup>
                                            <optgroup label="Mismo día">
                                                @foreach($delivery_windows_same_day as $delivery_window)
                                                    <option value="{{ $delivery_window->id }}">{{ $delivery_window->delivery_window }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Zona:</label>
                                        <select class="form-control" id="zone_id" name="zone_id" required>
                                            <option value="">Selecciona</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Tipo de reporte:</label>
                                        <select class="form-control" id="report_type" name="report_type" required>
                                            <option value="">Selecciona</option>
                                            <option value="returns">Devoluciones</option>
                                            <option value="late_delivery">Entregas tarde</option>
                                            <option value="negative_rating">Calificación negativa</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="button" class="btn btn-primary btn-show-report">Generar reporte</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row pie-charts">
                            <div class="col-md-4 returns_chart" id="returns_chart"></div>
                            <div class="col-md-4 late_delivery_chart" id="late_delivery_chart"></div>
                            <div class="col-md-4 negative_rating_chart" id="negative_rating_chart"></div>
                        </div>
                        <div class="row paging">
                            <div class="col-md-12 table-responsive">
                                <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#Id Pedido</th>
                                            <th>Ruta</th>
                                            <th>Nombre Transportador</th>
                                            <th>Franja Horaria</th>
                                            <th>Calificación</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="5" align="center">No hay datos.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3">
                                    <div class="dataTables_info" id="shelves-table_info">Mostrando 0 registros.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    var url_get_products_ajax = "{{ route('returnslatedeliverynegativerating.getOrders') }}";
    var warehouse_url_ajax = "{{ route('admin.get_warehouses_ajax') }}";
    var zones_url_ajax = "{{ route('returnslatedeliverynegativerating.getZones') }}";
    var delivery_windows_url_ajax = "{{ route('returnslatedeliverynegativerating.getDeliveryWindows') }}";
    var data_series_retuns = [
        { label: 'Con devoluciones', data: 0, color: '#0073b7'},
        { label: 'Sin devoluciones', data: 1, color: '#E91A58' },
    ];
    var data_series_delay = [
        { label: 'Con retraso en la entrega', data: 0, color: '#0073b7' },
        { label: 'Sin retraso en la entrega', data: 1, color: '#E91A58' },
    ];
    var data_series_rating = [
        { label: 'Con calificación negativa', data: 0, color: '#0073b7'},
        { label: 'Sin calificación negativa', data: 1, color: '#E91A58' },
    ];
    function get_orders(){
        if($('#search-form').valid()){
            data = $('#search-form').serialize();
            change_status_fields(true);
            $.ajax({
                url: url_get_products_ajax,
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    change_status_fields(false);
                    $('.paging').html(response.result.table);
                    $('#returns_chart').unbind();
                    $('#negative_rating_chart').unbind();
                    $('#late_delivery_chart').unbind();
                
                    draw_chart('#negative_rating_chart',  response.result.charts.negative_rating );
                    draw_chart('#late_delivery_chart',  response.result.charts.late_delivery );
                    draw_chart('#returns_chart',  response.result.charts.returns );
                    setTimeout(function(){ get_orders(); }, 60000);
                }, error: function() {
                    change_status_fields(false);
                }
            });
        }

    }

    /*
     * DONUT CHART
     * -----------
     */
    function draw_chart(id_element, data){
        var options_series = {
            series: {
                pie: {
                    show       : true,
                    radius     : 1,
                    innerRadius: 0.5,
                    label      : {
                        show     : true,
                        radius   : 1,
                        formatter: function(label, series) {
                            return '<div style="font-size:14px; text-align:center; padding:2px; color: '+series.color+'; font-weight: 600;">'
                              + label
                              + '<br>'
                              + Math.round(series.percent) + '%</div>'
                        },
                        background: {
                            opacity: 0.7,
                            color: '#fff'
                        }
                        // threshold: 0.1
                    }

                }
            },
            legend: {
                show: false
            }
        };
        $.plot(id_element, data, options_series);
    }
    /*
     * END DONUT CHART
     */
    function change_status_fields(status){
        $('#warehouse_id').attr('disabled', status);
        $('#delivery_time').attr('disabled', status);
        $('#zone_id').attr('disabled', status);
        $('#report_type').attr('disabled', status);
    }

    $(document).ready(function() {

        $('#warehouse_id').on('change', function(){
            change_status_fields(true);
            $.ajax({
                url : zones_url_ajax,
                data : { warehouse_id: $('#warehouse_id').val() },
                type : 'POST',
                dataType : 'json',
                success : function( request ){
                    var zones = request.result.zones
                    var options = '<option value="">Selecciona</option>';
                    options += '<option value="all">Todas las zonas</option>';
                    $.each(zones, function( index, zone){
                        options += '<option value="'+zone.id+'">'+zone.name+'</option>';
                    })
                    $('#zone_id').html(options);
                    change_status_fields(false);
                }, error: function(){
                    change_status_fields(false);
                }
            });
        });

        
        $('.btn-show-report').on('click', function(){
            get_orders();
        });
        $('.get-delivery-windows').on('change', function(){
            var element = this;
            $.ajax({
                url : delivery_windows_url_ajax,
                method : "POST",
                data : { city_id : $(element).val() },
                success : function(request){
                    change_status_fields();
                    var options = '<option value="">Selecciona</option>';
                    options += '<optgroup label="Franjas normales">';
                    $.each(request.result.deliveryWindows, function( index, deliveryWindow){
                        options += '<option value="'+deliveryWindow.id+'">'+deliveryWindow.delivery_window+'</option>';
                    });
                    options += '</optgroup>';
                    options += '<optgroup label="Mismo día">';
                    $.each(request.result.deliveryWindowsSameDay, function( index, deliveryWindow){
                        options += '<option value="'+deliveryWindow.id+'">'+deliveryWindow.delivery_window+'</option>';
                    });
                    options += '</optgroup>';
                    $('#delivery_time').html(options);
                    change_status_fields(false);
                },
                error : function(){
                    change_status_fields(false);
                }
            });
        });

        $('#cities').on('change', function(){
            change_status_fields(true);
            $.ajax({
                url : warehouse_url_ajax,
                method : 'GET',
                data : { city_id : $('#cities').val() },
                success : function( request ){
                    var options = '<option value="">Selecciona</option>';
                    $.each(request, function( index, name){
                        options += '<option value="'+index+'">'+name+'</option>';
                    })
                    $('#warehouse_id').html(options);
                    $('#warehouse_id').trigger('change');
                    change_status_fields(false);
                },error: function(){
                    change_status_fields(false);
                }
            });

        });
        $('#cities').trigger('change');
        $('#warehouse_id').trigger('change');

        $('#returns_chart').unbind();
        $('#negative_rating_chart').unbind();
        $('#late_delivery_chart').unbind();
        draw_chart('#returns_chart',  data_series_retuns );
        draw_chart('#negative_rating_chart',  data_series_rating );
        draw_chart('#late_delivery_chart',  data_series_delay );
    });

    </script>
    @endsection
@else
    @section('content')
    <div class="col-md-12 table-responsive">
        <table id="provider-orders-table" class="admin-provider-orders table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#Id pedido</th>
                    <th>Ruta</th>
                    <th>Nombre transportador</th>
                    <th>Franja horaria</th>
                    @if($type=='negative_rating')
                    <th>Calificación</th>
                    <th>Tipo</th>
                    <th>Comentarios</th>
                    @endif
                    @if($type == 'late_delivery')
                    <th>Retraso</th>
                    @endif
                </tr>
            </thead>
            <tbody>
            @if (count($orders))
                @foreach($orders as $order)
                <tr>
                    <td>
                        <a href="{{route('adminOrderStorage.details', ['id'=>$order->order_id ])}}" target="_blank">{{$order->order_id}}</a>
                    </td>
                    <td>{{$order->route}}</td>
                    <td>{{$order->driver_name}}</td>
                    <td>{{$order->delivery_time}}</td>
                    @if($type=='negative_rating')
                    <td align="center">
                        @if($order->user_score == 0)
                            <span class="badge bg-red"><i class="fa fa-thumbs-o-down"></i></span>
                        @else
                            <span class="badge bg-green"><i class="fa fa-thumbs-o-up"></i></span>
                        @endif
                    </td>
                    <td>
                        @if(is_null($order->user_score_typification))
                            Con los productos
                        @else
                            {{$order->user_score_typification}}
                        @endif
                    </td>
                    <td>{{$order->user_score_comments}}</td>
                    @endif
                    @if($type == 'late_delivery')
                    <td>{{$order->delay_time}} minutos</td>
                    @endif
                </tr>
                @endforeach
            @else
                <tr><td colspan="19" align="center">No hay datos</td></tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <div class="col-xs-3">
            <div class="dataTables_info" id="shelves-table_info">Mostrando {{count($orders)}} registros.</div>
        </div>
    </div>
    @stop
@endif