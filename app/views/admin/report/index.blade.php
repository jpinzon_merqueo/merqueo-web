@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <fieldset>
                            <legend>Reportes de Inventario</legend>
                            <ul>
                                <li>
                                    <a href="{{ route('reportMissingPickingProduct.index') }}">
                                        Faltantes en alistamiento
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('notVisibleProduct.index') }}">
                                        Productos no visibles
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('reportMissingPickingProduct.index') }}">
                                        Faltantes en alistamiento
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('adminReport.pickingMissing') }}">
                                        Faltantes por alistador
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('commitedProducts.index') }}">
                                        Productos comprometidos
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('ceroStock.index') }}">
                                        Stocks en cero
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('commitedProducts.assortment') }}">
                                        Surtido
                                    </a>
                                </li>
                            </ul>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <fieldset>
                            <legend>Reportes de transportadores</legend>
                            <ul>
                                <li>
                                    <a href="{{ route('reportMissingPickingProduct.index') }}">
                                        Faltantes en alistamiento
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('returnslatedeliverynegativerating.index') }}">
                                        Devoluciones, entregas tarde y calificación negativa
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('driverswithoutactivity.index') }}">
                                        Vehículos sin entregar pedido
                                    </a>
                                </li>
                            </ul>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

        @if(Session::get('admin_role_id') == 1 || Session::get('admin_role_id') == 6)
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <fieldset>
                            <legend>Reportes de contabilidad</legend>
                            <ul>
                                <li>
                                    <a href="{{ route('adminReceptionReports.index') }}">
                                        Reporte de recibos de bodega
                                    </a>
                                </li>
                            </ul>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </section>
@endsection