@if ( count($today_drivers) )
    @foreach ($today_drivers as $key => $driver)
        <tr>
            <td>
                {{ $key + 1 }}
            </td>
            <td class="text-center">
                {{ $driver->first_name }} {{ $driver->last_name }}
            </td>
            <td>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-xs-9">
                        <strong>Ruta: </strong>{{ $driver->route }}
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-3">
                        <strong>Ordenes: </strong>{{ $driver->route_orders }}
                    </div>
                </div>
            </td>
            <td>
                @if ( $driver->num_orders_route )
                    {{ $driver->num_orders_route }}
                @else
                    0
                @endif
            </td>
            <td>
                @if ($driver->modified_orders)
                    {{ $driver->modified_orders }}
                @else
                    0
                @endif
            </td>
            <td>
                @if ( $driver->modified_orders_percentage > 0 )
                    {{ number_format((float)$driver->modified_orders_percentage, 2, '.', '') }}%
                @else
                    0.00%
                @endif
            </td>
            <td>
                @if ( $driver->yesterday_modified_orders_percentage > 0 )
                    {{ number_format((float)$driver->yesterday_modified_orders_percentage, 2, '.', '') }}%
                @else
                    0.00%
                @endif
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="6" class="text-center">
            No se encontró información
        </td>
    </tr>
@endif