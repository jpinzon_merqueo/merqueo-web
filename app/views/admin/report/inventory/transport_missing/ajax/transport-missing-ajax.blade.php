<tr>
    <td class="text-center">
        {{ $today_orders->num_routes }}
    </td>
    <td class="text-center">
        {{ $today_orders->dispatched_orders }}
    </td>
    <td class="text-center">
        {{ $today_orders->modified_orders }}
    </td>
    <td class="text-center">
        {{ $today_orders->modified_orders_percentage }}%
    </td>
    <td class="text-center">
        {{ $yesterday_orders->modified_orders_percentage }}%
    </td>
</tr>