<script type="text/javascript">
    $(document).ready(function() {
        var Transport = (function() {
            'use strict';

            function Transport(args) {
                // enforces new
                if (!(this instanceof Transport)) {
                    return new Transport(args);
                }
                this.bindAction();
                this.getTransportMissingAjax();
                this.getTransporterMissingAjax();
                var self = this;
                setInterval(function () {
                    self.getTransportMissingAjax();
                    self.getTransporterMissingAjax();
                }, 60000);
            }

            Transport.prototype.bindAction = function(args) {
                var self = this;
                $('#warehouse_id').change(function(event) {
                    self.getTransportMissingAjax();
                    self.getTransporterMissingAjax();
                });
            };

            Transport.prototype.getTransportMissingAjax = function(first_argument) {
                $.ajax({
                    url: '{{ route('reportTransportMissing.getTransportMissingAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: $('#search-form').serialize(),
                })
                .done(function(data) {
                    $('.tbody').html('');
                    $('.tbody').html(data.html);
                })
                .fail(function() {
                })
                .always(function() {
                });
            };

            Transport.prototype.getTransporterMissingAjax = function(first_argument) {
                $.ajax({
                    url: '{{ route('reportTransportMissing.getTransporterMissingAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: $('#search-form').serialize(),
                })
                .done(function(data) {
                    $('.tbody-2').html('');
                    $('.tbody-2').html(data.html);
                })
                .fail(function() {
                })
                .always(function() {
                });
            };

            return Transport;
        }());

        var trans = new Transport;
    });
</script>