@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>

    <section class="content">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form id="search-form" class="admin-providers-table">
                            <div class="row form-group">
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                            <label for="city_id">Ciudad:</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-9">
                                            <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                            <label for="warehouse_id">Bodega:</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-9">
                                            <select name="warehouse_id" id="warehouse_id" class="form-control">
                                                @foreach ($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}" {{ Session::get('admin_warehouse_id') == $warehouse->id ? 'selected="Selected"' : '' }}>{{ $warehouse->warehouse }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-lg-3 col-md-3 col-xs-3 text-right">
                                            <label for="stocks">Stocks:</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-9">
                                            <select name="stocks" id="stocks" class="form-control">
                                                <option value="picked">Alistado</option>
                                                <option value="picking">Alistamiento</option>
                                                <option value="recibo">Recibos</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-3 col-md-3 col-xs-12 col-lg-offset-5 col-md-offset-5">
                                    <button class="btn btn-primary" type="submit">Buscar</button>&nbsp;
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                                <h3>
                                    Alistado <span id="picked-stock" class="label label-primary">0</span>
                                </h3>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                                <h3>
                                    Alistamiento <span id="picking-stock" class="label label-success">0</span>
                                </h3>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                                <h3>
                                    Recibos <span id="received-stock" class="label label-info">0</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.report.inventory.cero_stock.js.index-js')
@endsection
