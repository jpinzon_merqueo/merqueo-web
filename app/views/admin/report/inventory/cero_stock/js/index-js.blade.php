<script type="text/javascript">
    $(document).ready(function() {
        var CeroStock = (function() {
            'use strict';

            function CeroStock(args) {
                // enforces new
                if (!(this instanceof CeroStock)) {
                    return new CeroStock(args);
                }
                this.bindAction();
                this.getCeroStockAjax();
                var self = this;
                setInterval(function () {
                    self.getCeroStockAjax();
                }, 60000);
            }

            CeroStock.prototype.bindAction = function(args) {
                var self = this;
                $('#warehouse_id').change(function(event) {
                    self.getCeroStockAjax();
                });
                $('body').on('submit', '#search-form', function(event) {
                    event.preventDefault();
                    self.getCeroStockAjax();
                });

                $('body').on('click', '.pagination a', function(event) {
                    event.preventDefault();
                    var form_data = $('#search-form').serialize();
                    var paginate_url = $(this).prop('href');
                    self.getPaginateInfo(form_data, paginate_url);
                });
            };

            CeroStock.prototype.getCeroStockAjax = function(first_argument) {
                $.ajax({
                    url: '{{ route('ceroStock.getCeroStockAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: $('#search-form').serialize(),
                })
                .done(function(data) {
                    $('#picked-stock').html(data.picked)
                    $('#picking-stock').html(data.picking)
                    $('#received-stock').html(data.reception)
                    $('.table-responsive').html(data.html)
                })
                .fail(function() {
                    alert('ocurrió un error al obtener los datos.')
                })
                .always(function() {
                });
            };

            CeroStock.prototype.getPaginateInfo = function(form_data, paginate_url) {
                $.ajax({
                    url: paginate_url,
                    type: 'GET',
                    dataType: 'json',
                    data: form_data,
                })
                .done(function(data) {
                    $('#picked-stock').html(data.picked)
                    $('#picking-stock').html(data.picking)
                    $('#received-stock').html(data.reception)
                    $('.table-responsive').html(data.html)
                })
                .fail(function() {
                    alert('ocurrió un error al obtener los datos.')
                })
                .always(function() {
                });
            };

            return CeroStock;
        }());

        var trans = new CeroStock;
    });
</script>
