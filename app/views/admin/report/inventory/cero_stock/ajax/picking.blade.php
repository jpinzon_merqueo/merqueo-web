    <table id="missings-table" class="admin-missings-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Referencia</th>
                <th>PLU</th>
                <th>Imagen</th>
                <th>Producto</th>
                <th>Posición</th>
                <th>Inventario Actual</th>
                <th>Inventario en Alistamiento</th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if (count($products))
                @foreach ($products as $key => $product)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->reference }}</td>
                    <td>{{ $product->storeProduct->first()->provider_plu }}</td>
                    <td><img src="{{ $product->image_small_url }}" alt="{{ $product->name }}" width="50" height="50"></td>
                    <td>{{ $product->name }} {{ $product->unit }} {{ $product->quantity }}</td>
                    <td>{{ $product->storeProduct->first()->storeProductWarehouses->first()->storage_position }} - {{ $product->storeProduct->first()->storeProductWarehouses->first()->storage_height_position }}</td>
                    <td>{{ $product->storeProduct->first()->getCurrentStock($warehouse_id) }}</td>
                    <td>{{ $product->storeProduct->first()->storeProductWarehouses->first()->picking_stock }}</td>
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="9" class="text-center">
                    No se encontraron productos
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    @if (count($products))
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            {{ $products->links()->render() }}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            Mostrando {{ $products->count() }} de {{ $products->getTotal() }}
        </div>
    </div>
    @endif