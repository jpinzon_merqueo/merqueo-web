    <table id="missings-table" class="admin-missings-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Recibo</th>
                <th>Fecha Recepción</th>
                <th>Hora Actual</th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if (count($receptions))
                @foreach ($receptions as $key => $reception)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $reception->id }}</td>
                    <td>{{ $reception->received_date }}</td>
                    <td>{{ $reception->current_date }}</td>
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="4" class="text-center">
                    No se encontraron recibos
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    @if (count($receptions))
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            {{ $receptions->links()->render() }}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            Mostrando {{ $receptions->count() }} de {{ $receptions->getTotal() }}
        </div>
    </div>
    @endif