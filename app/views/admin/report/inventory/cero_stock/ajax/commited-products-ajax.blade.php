@if ( count($commited_products) )
    @foreach ($commited_products as $key => $commited_product)
        <tr>
            <td>
                {{ $key + 1 }}
            </td>
            <td>
                {{ $commited_product->id }}
            </td>
            <td class="text-center">
                {{ $commited_product->reference }}
            </td>
            <td class="text-center">
                {{ $commited_product->provider_plu }}
            </td>
            <td>
                <img src="{{ $commited_product->image_small_url }}" alt="{{ $commited_product->product_name }}" class="img-responsive" height="50" width="50">
            </td>
            <td>
                {{ $commited_product->product_name }}
            </td>
            <td>
                {{ empty($commited_product->fecha_1) ? 0 : $commited_product->fecha_1 }}
            </td>
            <td>
                {{ empty($commited_product->fecha_2) ? 0 : $commited_product->fecha_2 }}
            </td>
            <td>
                {{ empty($commited_product->fecha_3) ? 0 : $commited_product->fecha_3 }}
            </td>
            <td>
                {{ empty($commited_product->fecha_4) ? 0 : $commited_product->fecha_4 }}
            </td>
            <td>
                {{ empty($commited_product->fecha_5) ? 0 : $commited_product->fecha_5 }}
            </td>
            <td>
                {{ empty($commited_product->total) ? 0 : $commited_product->total }}
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="12" class="text-center">
            No se encontró información
        </td>
    </tr>
@endif
