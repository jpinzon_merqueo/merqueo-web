<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th># Pedido</th>
            <th>Ruta</th>
            <th>Sequencia</th>
            <th>Unidades Faltantes</th>
        </tr>
    </thead>
    <tbody>
        @if ( count($orders) )
            @foreach ($orders as $order)
                <tr>
                    <td>
                        <a href="{{ route('adminOrderStorage.details', ['id' => $order->id]) }}" target="_blank">
                            {{ $order->id }}
                        </a>
                    </td>
                    <td>{{ $order->route }}</td>
                    <td>{{ $order->planning_sequence }}</td>
                    <td class="text-center">{{ $order->quantity }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4" class="text-center">No se encontraron pedidos</td>
            </tr>
        @endif
    </tbody>
</table>