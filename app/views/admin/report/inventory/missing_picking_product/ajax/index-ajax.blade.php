@if ( count($products) )
    @foreach ($products as $product)
        <tr>
            <td class="text-center">
                <img src="{{ $product->storeProduct->product->image_small_url }}" alt="{{ $product->storeProduct->product->name }}" class="img-responsive" width="50">
            </td>
            <td>
                {{ $product->storeProduct->product->id }}
            </td>
            <td>
                {{ $product->storeProduct->product->name }} {{ $product->storeProduct->product->quantity }} {{ $product->storeProduct->product->unit }}
            </td>
            <td>
                {{ $product->storeProduct->product->reference }}
            </td>
            <td class="text-center">
                <a href="javascript:;" class="get-orders" data-city_id="{{ $city_id }}" data-warehouse_id="{{ $warehouse_id }}" data-store_product_id="{{ $product->storeProduct->id }}">
                   {{ $product->quantity }}
                </a>
            </td>
            <td class="text-center">
                {{ $product->current_stock }}
            </td>
            <td class="text-center">
                {{ empty($product->quantity_received) ? 0 : $product->quantity_received }}
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="7" class="text-center">
            No se encontraron productos
        </td>
    </tr>
@endif