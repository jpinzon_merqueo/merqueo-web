<script>
    $(document).ready(function() {
        var missings = (function() {
            'use strict';

            function missings(args) {
                // enforces new
                if (!(this instanceof missings)) {
                    return new missings(args);
                }
                // constructor body
                this.bindActions();
                this.getMissingProductsAjax();
                var self = this;
                setInterval(function () {
                    self.getMissingProductsAjax();
                }, 60000);
            }

            missings.prototype.bindActions = function(args) {
                var self = this;
                $('body').on('submit', '#search-form', function(event) {
                    event.preventDefault();
                    self.getMissingProductsAjax();
                });
                $('body').on('click', '.get-orders', function (event) {
                    event.preventDefault();
                    self.getOrdersByMissingProductAjax(this);
                });
                $('body').on('change', '#warehouse_id', function(event) {
                    self.getMissingProductsAjax();
                });
            };

            missings.prototype.getMissingProductsAjax = function() {
                $('.paging-loading').show();
                $.ajax({
                    url: '{{ route('reportMissingPickingProduct.getMissingProductsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: $('#search-form').serialize(),
                })
                .done(function(data) {
                    $('.tbody').html('');
                    $('.tbody').html(data.html);
                })
                .fail(function() {
                })
                .always(function() {
                    $('.paging-loading').hide();
                });
            };

            missings.prototype.getOrdersByMissingProductAjax = function(obj) {
                $.ajax({
                    url: '{{ route('reportMissingPickingProduct.getOrdersByMissingProductAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        city_id: $(obj).data('city_id'),
                        warehouse_id: $(obj).data('warehouse_id'),
                        store_product_id: $(obj).data('store_product_id')
                    },
                })
                .done(function(data) {
                    $('.modal-body').html('');
                    $('.modal-body').html(data.html);
                    $('#orders-modal').modal();
                })
                .fail(function() {
                })
                .always(function() {
                });
            };

            return missings;
        }());
        var Missing = new missings();
    });

</script>