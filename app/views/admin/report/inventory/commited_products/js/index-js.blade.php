<script type="text/javascript">
    $(document).ready(function() {
        var CommittedProducts = (function() {
            'use strict';

            function CommittedProducts(args) {
                // enforces new
                if (!(this instanceof CommittedProducts)) {
                    return new CommittedProducts(args);
                }
                this.bindAction();
                this.getCommittedProductsAjax();
            }

            CommittedProducts.prototype.bindAction = function(args) {
                var self = this;
                $('#warehouse_id').change(function(event) {
                    self.getCommittedProductsAjax();
                });
                $('body').on('submit', '#search-form', function(event) {
                    event.preventDefault();
                    self.getCommittedProductsAjax();
                });
            };

            CommittedProducts.prototype.getCommittedProductsAjax = function(first_argument) {
                $.ajax({
                    url: '{{ route('commitedProducts.getCommitedProductsAjax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: $('#search-form').serialize(),
                })
                .done(function(data) {
                    $('#table_commited_products').html('');
                    $('#table_commited_products').html(data.html);
                })
                .fail(function() {
                })
                .always(function() {
                });
            };

            return CommittedProducts;
        }());

        var trans = new CommittedProducts;

        $('body').on('click', 'ul.pagination li a', function (e) {
            e.preventDefault();
            let pageParam = e.currentTarget.href.substr(e.currentTarget.href.indexOf('page='));
            let img_load = '{{ asset('assets/img/loading.gif') }}';
            $('.tbody').html('<tr><td colspan="12" align="center"><img class="u-loader" src="' + img_load + '" alt=""></td></tr>');
            $.ajax({
                url: '{{ route('commitedProducts.getCommitedProductsAjax') }}',
                type: 'GET',
                dataType: 'json',
                data: $('#search-form').serialize() + '&'+ pageParam,
            })
            .done(function(data) {
                $('#table_commited_products').html('');
                $('#table_commited_products').html(data.html);
            })
            .fail(function() {
            })
            .always(function() {
            });
        });
    });
</script>
