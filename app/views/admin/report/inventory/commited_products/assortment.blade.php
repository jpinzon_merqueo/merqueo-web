@if ( !Request::ajax() )
    @extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form id="search-form" class="orders-table">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="row form-group">
                                        <div class="col-xs-6 col-lg-4 col-md-4">
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <label for="city_id">Ciudad:</label>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-xs-12">
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    <option value="">Selecciona</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}"
                                                                @if( Session::get('admin_city_id') == $city->id ) selected="selected" @endif >{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-lg-4 col-md-4">
                                            <div class="row form-group">
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <label for="warehouse_id">Bodega:</label>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-xs-12">
                                                    <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                        @foreach ($warehouses as $warehouse)
                                                            <option value="{{ $warehouse->id }}"
                                                                    @if( Session::get('admin_warehouse_id') == $warehouse->id ) selected="selected" @endif >{{ $warehouse->warehouse }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <label for="city_id">&nbsp;</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <button type="button" id="btn-search-orders" class="btn btn-primary">
                                                    Buscar
                                                </button>&nbsp;&nbsp;
                                            </div>
                                        </div>
                                    </div>
                                    <div class="paging"></div>

                                </div>
                            </div>
                        </form>
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img class="loader unseen"
                                                                            src="{{ asset_url() }}img/loading.gif"/>
                        </div>

                        <br>
                        <div class="row">
                            <div>
                                <div class="col-md-12 col-xs-12 padding">
                                    <div class="orders-table-missings"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function () {
            setTimeout(function () {
                location.reload();
            }, 120000)
        })
    </script>

    @include('admin.report.inventory.commited_products.js.assortment-js')
@stop
@else
@section('products')
    @if ( count($products) )
        <div class="paging">
            <div class="table-responsive">
                <table id="products" class="table table-bordered table-striped">
                    <thead>
                    <tr class="text-center">
                        <th>Imagen</th>
                        <th>Producto</th>
                        <th># Referencia</th>
                        <th>Posición picking</th>
                        <th>Cantidad actual picking</th>
                        <th>Cantidad comprometida</th>
                        <th>Cantidad a surtir</th>
                        <th>Posición de almacenamiento</th>
                        <th>Fecha de vencimiento</th>
                        <th>Cantidad en posición de almacenamiento</th>
                    </tr>
                    </thead>
                    @foreach($products as $name => $product)
                        @if ($product['amount_committed'] > $product['picking_stock'])
                            <tr class="text-center">
                                <td><img src="{{$product['image_small_url']}}"/></td>
                                <td>{{$name}}</td>
                                <td>{{$product['reference']}}</td>
                                <td>{{$product['picking_position']}}</td>
                                <td>{{$product['picking_stock']}}</td>
                                <td>{{$product['amount_committed']}}</td>
                                <td>{{$product['maximum_picking_stock'] >  $product['amount_committed'] ? $product['maximum_picking_stock'] : $product['picking_stock'] > 0 ? $product['amount_committed'] - $product['picking_stock'] : $product['amount_committed']}}</td>
                                <td>{{$product['warehouse_position']}}</td>
                                <td>{{$product['expiration_date']}}</td>
                                <td>{{$product['warehouse_storage']}}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    @else
        <div class="text-center">No se encontraron productos faltantes</div>
        @endif
        @stop
        @endif
        </div>
        </div>
        </div>
        </div>
        </section>
