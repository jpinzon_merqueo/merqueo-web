@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Hecho!</b> {{ Session::get('success') }}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b> {{ Session::get('error') }}
            </div>
        @endif
        @if( $errors->has() )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alerta!</b>
                <ul>
                    @if($errors->has())
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    @endif
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form action="{{ route('adminReceptionReports.exportReceptionReport') }}" method="POST" id="receptionForm">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-xs-12 form-group">
                                    <label for="warehouse_id">Bodega:</label>
                                    <select name="warehouse_id" id="warehouse_id" class="form-control">
                                        <option value="">-Seleccione la bodega-</option>
                                        <option v-for="(warehouse, index) in warehouses" :value="warehouse.id">@{{ warehouse.warehouse }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12 form-group">
                                    <label for="month">Seleccione un mes:</label>
                                    <input type="month" name="month" id="month" class="form-control" placeholder="Seleccione la fecha">
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12 form-group">
                                    <label for="" style="color: #FFF; display: block">|</label>
                                    <button class="btn btn-primary">Generar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.report.accounting.receptions.js.index-vue')
@endsection