<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vee-validate@latest"></script>
<script>
    let form = new Vue({
        el: '#receptionForm',
        data: {
            warehouses: {{ $warehouses->toJson() }}
        }
    })
</script>