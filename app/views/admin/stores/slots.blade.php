@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
    @if(Session::has('message') )
        @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('message') }}
        </div>
        @else
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alert!</b> {{ Session::get('message') }}
        </div>
        @endif
    @endif
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header">
                    @if(!$slot_id)
                    <h3 class="box-title">Crear horario</h3>
                    @else
                    <h3 class="box-title">Actualizar horario</h3>
                    @endif
                </div>
                <div class="box-body">
                	<p>
                       <a href="{{ route('adminStores.formUpdateSlots', ['id' => $store_id]) }}">
                            <input type="button" value="Actualizar horarios masivamente" class="btn btn-primary">
                       </a>
                    </p>
                    @if($slot_id)
                    <p>{{ format_date('normal_with_time', $start_time).' - '.format_date('normal_with_time', $end_time) }}</p>
                    @endif
                    <label>Selecciona @if($slot_id) nuevo @endif horario:</label>
                    <form method="post" action="{{ route('adminStores.saveSlot', ['id' => $store_id]) }}">
                    <input type='hidden' name='slot_id' value='{{ $slot_id }}'>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" name="time" class="form-control pull-right" id="reservationtime" />
                    </div>
                    <br>
                    @if (!$slot_id)
                    <input type="submit" value="Guardar" class="btn btn-primary">
                    @else
                    <input type="submit" value="Actualizar Horario" class="btn btn-primary">
                    <a href="{{ route('adminStores.deleteSlot', ['store_id' => $store_id, 'slot_id' => $slot_id]) }}"><input type="button" value="Eliminar Horario" class="btn btn-danger"  onclick="return confirm('Are you sure you want to delete it?')"></a>
                    @endif
                    </form>
                </div>
            </div>
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div>
</section>

<script type="text/javascript">
$(document).ready(function() {

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        eventClick: function(data, event, view) {

            var content = '<h3>Owner</h3>';

            tooltip.set({
                'content.text': content
            })
            .reposition(event).show(event);
        },
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        selectable: true,
        events: [
        @foreach ($slots as $slot)
        {
            id: '{{ $slot->id }}',
            title: '{{ date("d/m, h:i A", strtotime($slot->start_time)) }}  to  {{ date("d/m, h:i A", strtotime($slot->end_time)) }}',
            start: '{{ $slot->start_time }}',
            end: '{{ $slot->end_time }}',
            backgroundColor: 'green',
            borderColor: 'green',
            allDay : false,
            url : "{{ route('adminStores.slots', ['id' => $store_id]) }}?id={{ $slot->id }}",
        },
        @endforeach
        ],
        eventClick: function(event) {
            if (event.url) {
                document.location=event.url;
                return false;
            }
        }

    });

});
</script>

<script type="text/javascript">
    $(function() {
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'YYYY-MM-DD h:mm:ss A',
            startDate: '{{ $start_time }}',
            endDate: '{{ $end_time }}'
        });
    });
</script>
@stop