@extends('admin.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ web_url() }}/admin_asset/js/plugins/datetimepicker/moment.js" type="text/javascript"></script>
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Actualizar Horarios</h3>
                </div>
                <div class="box-body">
                    <form id="main-form" method="post" action="{{ route('adminStores.UpdateSlots', ['id' => $store_id]) }}">
                        <input type='hidden' name='slot_id' value='{{ $slot_id }}'>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Fecha final de registro de horarios</label>
                                <input id="end_date" name="end_date" type="text" class="form-control" placeholder="Ingresa la fecha final" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Lunes</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="monday_start" name="monday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="monday_end" name="monday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Martes</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="tuesday_start" name="tuesday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="tuesday_end" name="tuesday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Miércoles</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="wednesday_start" name="wednesday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="wednesday_end" name="wednesday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Jueves</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="thursday_start" name="thursday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="thursday_end" name="thursday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Viernes</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="friday_start" name="friday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="friday_end" name="friday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input id="friday_end_next_day" name="friday_end_next_day" type="checkbox" value="1" class="form-control time"/>
                                    <label>Cierre próximo día</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Sábado</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="saturday_start" name="saturday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="saturday_end" name="saturday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input id="saturday_end_next_day" name="saturday_end_next_day" type="checkbox" value="1" class="form-control time"/>
                                    <label>Cierre próximo día</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Domingo</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="sunday_start" name="sunday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="sunday_end" name="sunday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12">
                                <label>Festivo</label><br>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de apertura</label>
                                    <input id="holiday_start" name="holiday_start" type="text" class="form-control time" placeholder="Ingresa la hora de apertura" />
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>Hora de cierre</label>
                                    <input id="holiday_end" name="holiday_end" type="text" class="form-control time" placeholder="Ingresa la hora de cierre" />
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="Actualizar" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function() {
        $.validator.addMethod("dateFormat", function(value,element) {
            return value.match(/^(19|20)\d\d[- --.](0[1-9]|1[012])[- --.](0[1-9]|[12][0-9]|3[01])$/);
            }, "Please enter a date using format yyyy-mm-dd"
        );

        $("#main-form").validate({
          rules: {
            end_date: {
              required: true,
              dateFormat: true
            },
          },
          submitHandler: function(form){
            $(form).submit();
          }
        });

        $('#end_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $(".time").each(function(index) {
            var time = $(this).attr('id');
            $('#'+time).datetimepicker({
                format: 'HH:mm'
            });
        });

    });
</script>
@stop