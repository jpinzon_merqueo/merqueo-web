@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

    <span class="breadcrumb" style="top:0px">
        <a href="{{ route('adminStores.addShelf', ['store_id' => $store_id, 'department_id' => $department_id]) }}">
        <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo Pasillo</button>
        </a>
    </span>

</section>

<section class="content">
    @if(Session::has('message') )
@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Hecho!</b> {{ Session::get('message') }}
</div>
@else
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Alert!</b> {{ Session::get('message') }}
</div>
@endif
@endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="shelves-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Posición</th>
                                <th>Estado</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shelves as $shelf)
                            <tr>
                                <td>{{ $shelf->name }}</td>
                                <td>{{ $shelf->sort_order }}</td>
                                <td>
                                    @if($shelf->status == 1)
                                        <span class="badge bg-green">Activo</span>
                                    @else
                                        <span class="badge bg-red">Inactivo</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger">Editar</button>
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ route('adminStores.editShelf', ['store_id' => $store_id, 'department_id' => $department_id, 'shelf_id' => $shelf->id]) }}">Editar</a></li>
                                            <li class="divider"></li>
                                            <li><a href="{{ route('adminStores.deleteShelf', ['store_id' => $store_id, 'department_id' => $department_id, 'shelf_id' => $shelf->id]) }}"  onclick="return confirm('¿Estas seguro que deseas eliminar el pasillo?')">Eliminar</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function() {
        // DataTable
        var table = $('#shelves-table').DataTable();
    });
</script>


@stop