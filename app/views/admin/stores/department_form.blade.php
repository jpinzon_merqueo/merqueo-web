@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">

        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alert!</b> {{ Session::get('error') }}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title }}</h3>
                    </div>
                    <form role="form" method="POST" id="main-form" action="{{ route('adminStores.saveDepartment', ['id' => $store_id]) }}"  enctype="multipart/form-data">
                        <div class="box-body">
                            <input type="hidden" name="store_id" value="{{ $store_id }}">
                            <input type="hidden" name="id" value="{{ $department ? $department->id : "" }}">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" id="" name="name" placeholder="Ingresa el nombre" value="{{ $department ? $department->name : "" }}">
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <input type="text" class="form-control" id="" name="description" placeholder="Ingresa la descripción" value="{{ $department ? $department->description : "" }}">
                            </div>
                            <div class="form-group">
                                <label>Posición</label>
                                <input type="text" class="form-control" id="" name="sort_order" placeholder="Ingresa la posición" value="{{ $department ? $department->sort_order : "99999" }}">
                            </div>
                            <div class="form-group">
                                <label>Codigo en SAP (ItemsGroupCode)</label>
                                <input type="text" class="form-control" id="" name="sap_item_group_code" placeholder="Ingresa el codigo" value="{{ $department ? $department->sap_item_group_code : "" }}">
                            </div>
                            <div class="form-group">
                                <label>Deeplink</label>
                                <input type="text" class="form-control" id="" name="deeplink" placeholder="Url dinámica" value="{{ $department ? $department->deeplink : "" }}" readonly="readonly">
                            </div>
                            <div class="form-group">
                                <label>Estado</label>
                                <select class="form-control" id="" name="status">
                                    <option value="1" {{ $department && (1 == $department->status) ? "selected" : ""}}>Activo</option>
                                    <option value="0" {{ $department && (0 == $department->status) ? "selected" : ""}}>Inactivo</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="image_list_app_url">Imagen app lista</label>
                                        <input type="file" name="image_list_app_url" id="image_list_app_url" class="form-control" @if(!isset($department) && empty($department->image_list_app_url)) required="required"  @elseif(empty($department->image_list_app_url)) required="required" @endif>
                                        @if(isset($department) && !empty($department->image_list_app_url))
                                            <br/><img src="{{ $department->image_list_app_url }}" style="width: 170px;" class="img-responsive">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="image_header_app_url">Imagen app header</label>
                                        <input type="file" name="image_header_app_url" id="image_header_app_url" class="form-control" @if(!isset($department) && empty($department->image_header_app_url)) required="required" @elseif(empty($department->image_header_app_url)) required="required" @endif>
                                        @if(isset($department) && !empty( $department->image_header_app_url))
                                            <br/><img src="{{ $department->image_header_app_url }}" style="width: 170px;" class="img-responsive">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="image_icon_url">Imagen icono</label>
                                        <input type="file" name="image_icon_url" id="image_icon_url" class="form-control imagen_icon_url" @if(!isset($department) && empty($department->image_icon_url)) required="required" @elseif(empty($department->image_icon_url)) required="required" @endif>
                                        @if(isset($department) && !empty( $department->image_icon_url))
                                            <br/><img src="{{ $department->image_icon_url }}" style="max-width: 170px;" class="img-responsive">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $("#main-form").validate({
            rules: {
                name: "required",
                sap_item_group_code: "required",
                sort_order: "required"
            }
        });
    </script>

@stop
