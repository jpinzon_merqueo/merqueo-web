@extends('admin.layout')

@section('content')
<section class="content-header">
    <h1>
        {{ $title }}
        <small>Control panel</small>
    </h1>

</section>
<section class="content">

    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b> {{ Session::get('error') }}
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                </div>
                <form role="form" method="post" id='main-form' action="{{ route('adminStores.saveShelf', ['store_id' => $store_id, 'department_id' => $department_id]) }}" enctype="multipart/form-data">
                    <div class="box-body">
                        <input type="hidden" name="store_id" value="{{ $store_id }}">
                        <input type="hidden" name="department_id" value="{{ $department_id }}">
                        <input type="hidden" name="id" value="{{ $shelf ? $shelf->id : "" }}">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="name" placeholder="Ingresa el nombre" value="{{ $shelf ? $shelf->name : "" }}">
                        </div>

                        <div class="form-group">
                            <label>Categoría</label>
                            <select class="form-control" name="category" id="category">
                                @foreach($shelf_category as $cat)
                                    <option value="{{ $cat }}" @if($category == $cat) selected="true" @endif>{{ $cat }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Descripción</label>
                            <input type="text" class="form-control" name="description" placeholder="Ingresa la descripción" value="{{ $shelf ? $shelf->description : "" }}">
                        </div>
                        <div class="form-group">
                            <label>Posición</label>
                            <input type="text" class="form-control" name="sort_order" placeholder="Ingresa la posición" value="{{ $shelf ? $shelf->sort_order : "99999" }}">
                        </div>
                        <div class="form-group">
                            <label>Deeplink</label>
                            <input type="text" class="form-control" name="deeplink" placeholder="Url dinámica" value="{{ $shelf ? $shelf->deeplink : "" }}" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="image_menu_app_url">Imagen menu app </label>
                            <input type="file" name="image_menu_app_url" id="image_menu_app_url" class="form-control" @if(!isset($shelf) && empty($shelf->image_menu_app_url)) required="required" @elseif(empty($shelf->image_menu_app_url)) required="required" @endif>
                            @if(isset($shelf) && !empty( $shelf->image_menu_app_url))
                                <br/><img src="{{ $shelf->image_menu_app_url }}" style="width: 170px;" class="img-responsive">
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <select class="form-control" name="status">
                                <option value="1" {{ $shelf && (1 == $shelf->status) ? "selected" : ""}}>Activo</option>
                                <option value="0" {{ $shelf && (0 == $shelf->status) ? "selected" : ""}}>Inactivo</option>
                            </select>
                        </div>
                    </div>

                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title_position }}</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label>Pasillo</label>
                            <input type="text" class="form-control" name="storage_shelf" placeholder="Ingresa el pasillo" value="{{ $shelf ? $shelf->storage_shelf : "" }}">
                        </div>
                        <div class="form-group">
                            <label>Rack</label>
                            <input type="text" class="form-control" name="storage_rack" placeholder="Ingresa el rack" value="{{ $shelf ? $shelf->storage_rack : "" }}">
                        </div>
                        <div class="form-group">
                            <label>Posición</label>
                            <input type="text" class="form-control" name="storage_position" placeholder="Ingresa la posición Position" value="{{ $shelf ? $shelf->storage_position : "" }}">
                        </div>
                        <div class="form-group">
                            <label>Altura</label>
                            <input type="text" class="form-control" name="storage_height" placeholder="Ingresa la altura" value="{{ $shelf ? $shelf->storage_height : "" }}">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$("#main-form").validate({
  rules: {
    name: "required",
    sort_order: "required"
  }
});
</script>

@stop

