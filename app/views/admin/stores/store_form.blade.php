@if(!Request::ajax())
    @extends('admin.layout')
    @section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>

    </section>
    <section class="content">

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Información</b> {{ Session::get('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ $sub_title }}</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" id='main-form' action="{{ route('adminStores.save') }}" enctype="multipart/form-data">
                        <div class="box-body">
                            <input type="hidden" name="id" value="{{ $store ? $store->id : '' }}">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="name" placeholder="Ingresa nombre" value="{{ $store ? $store->name : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>NIT</label>
                                    <input type="text" class="form-control" name="nit" placeholder="Ingresa NIT"  value="{{ $store ? $store->nit : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Ingresa teléfono"  value="{{ $store ? $store->phone : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Ciudad</label>
                                    <select class="form-control" id="city_id" name="city_id">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" @if ($store && $store->city_id == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Texto de horario</label>
                                    <input type="text" class="form-control" name="schedule" placeholder="Ingresa texto de horario"  value="{{ $store ? $store->schedule : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Valor mínimo de pedido</label>
                                    <input type="text" class="form-control" name="minimum_order_amount" placeholder="Ingresa valor mínimo de pedido"  value="{{ $store ? $store->minimum_order_amount : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Texto de tiempo de entrega</label>
                                    <input type="text" class="form-control" name="delivery_time" placeholder="Ingresa texto tiempo de entrega"  value="{{ $store ? $store->delivery_time : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Tiempo de entrega en minutos</label>
                                    <input type="text" class="form-control" name="delivery_time_minutes" placeholder="Ingresa tiempo de entrega en minutos"  value="{{ $store ? $store->delivery_time_minutes : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Tiempo de entrega en minutos en el mismo día</label>
                                    <input type="number" class="form-control" name="delivery_time_minutes_today" placeholder="Ingresa tiempo de entrega en minutos"  value="{{ $store ? $store->delivery_time_minutes_today : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>¿Requiere revisión de pedidos y factura?</label>
                                    <select class="form-control" name="revision_orders_required">
                                        <option value="1" {{ $store && $store->revision_orders_required ? "selected" : '' }}>Sí</option>
                                        <option value="0" {{ $store && !$store->revision_orders_required ? "selected" : '' }}>No</option>
                                    </select>
                                </div>
                            </div>
                            @if($store && $store->deeplink_store && $store->deeplink_cart)
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label class="control-label" for="deeplink_store">Deeplink Tienda</label>
                                    <input type="text" class="form-control" name="deeplink_store" id="deeplink_store" value="{{ $store && $store->deeplink_store ? $store->deeplink_store : '' }}" disabled="disabled">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label" for="deeplink_cart">Deeplink Carrito</label>
                                    <input type="text" class="form-control" name="deeplink_cart" id="deeplink_cart" value="{{ $store && $store->deeplink_cart ? $store->deeplink_cart : '' }}" disabled="disabled">
                                </div>
                            </div>
                            @endif
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Color de fondo para app</label>
                                    <input type="text" class="form-control" name="app_background_color" placeholder="Ingresa color de fondo para app"  value="{{ $store ? $store->app_background_color : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Posición</label>
                                    <input type="text" class="form-control" name="sort_order" placeholder="Ingresa orden"  value="{{ $store ? $store->sort_order : '' }}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label class="control-label">Ubicación de la bodega</label>
                                    <input type="text" class="form-control" name="latlng" id="latlng" placeholder="Ej: 6.122646,-75.634480" value="{{ $store && $store->latitude && $store->longitude ? $store->latitude.','.$store->longitude : '' }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Estado</label>
                                    <select class="form-control" name="status">
                                        <option value="1" {{ $store && $store->status ? "selected" : ''}}>Activo</option>
                                        <option value="0" {{ $store && !$store->status ? "selected" : ''}}>Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <label>Descripción para SEO</label>
                                    <textarea class="ckeditor" name="description" placeholder="Ingresa descripción para SEO" rows="6">{{ $store ? $store->description : '' }}</textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <label>Descripción en App</label>
                                    <textarea class="form-control" name="description_app" placeholder="Ingresa descripción para app" rows="6">{{ $store ? $store->description_app : '' }}</textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <label>Descripción en Web</label>
                                    <textarea class="ckeditor" name="description_web" placeholder="Ingresa descripción para web" rows="6">{{ $store ? $store->description_web : '' }}</textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="@if ($store && $store->logo_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Logo</label>
                                    <input type="file" class="form-control" name="logo" >
                                </div>
                                @if ($store && $store->logo_url)
                                <div class="col-xs-2">
                                    <img src="{{ $store->logo_url }}" class="img-responsive" />
                                </div>
                                @endif
                                <div class="@if ($store && $store->logo_small_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Logo pequeño</label>
                                    <input type="file" class="form-control" name="logo_small" >
                                </div>
                                 @if ($store && $store->logo_small_url)
                                <div class="col-xs-2">
                                    <img src="{{ $store->logo_small_url }}" class="img-responsive" />
                                </div>
                                @endif
                            </div>
                            <div class="row form-group">
                                <div class="@if ($store && $store->app_logo_large_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Logo grande para app</label>
                                    <input type="file" class="form-control" name="app_logo_large" >
                                </div>
                                 @if ($store && $store->app_logo_large_url)
                                <div class="col-xs-2">
                                    <img src="{{ $store->app_logo_large_url }}" class="img-responsive" />
                                </div>
                                @endif
                                <div class="@if ($store && $store->app_logo_small_url) col-xs-4 @else col-xs-6 @endif">
                                    <label>Logo pequeño para app</label>
                                    <input type="file" class="form-control" name="app_logo_small" >
                                </div>
                                 @if ($store && $store->app_logo_small_url)
                                <div class="col-xs-2">
                                    <img src="{{ $store->app_logo_small_url }}" class="img-responsive" />
                                </div>
                                @endif
                            </div>
                            <br />
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    $("#main-form").validate({
      rules: {
        name: {
            required: true
        },
        nit: {
          required: true
        },
        phone: {
          required: true
        },
        city_id: {
          required: true
        },
        schedule: {
          required: true
        },
        description: {
          required: true
        },
        delivery_time: {
          required: true
        },
        delivery_time_minutes: {
          required: true,
          number: true
        },
        minimum_order_amount: {
          required: true,
          number: true
        },
        sort_order: {
          required: true,
          number: true
        }
      }
    });
    </script>

    <script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>

    @stop
@endif