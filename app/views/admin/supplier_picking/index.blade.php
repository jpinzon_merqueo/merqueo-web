@if ( !Request::ajax() )
	@extends('admin.layout')

	@section('content')

		<section class="content-header">
			<h1>
				{{ $title }}
				<small>Control panel</small>
			</h1>
		</section>
		<section class="content">
			<div id="lists-items" class="row">
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Pedidos con faltantes</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#pending_orders" data-toggle="tab">Seco</a>
								</li>
								<li>
									<a href="#cold_orders" data-toggle="tab">Frío</a>
								</li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="pending_orders">
								</div>
								<div class="tab-pane" id="cold_orders">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="detail-items" class="row">
			</div>

			<div class="modal fade" id="modal-large-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Imagen de producto</h4>
							<div class="row">
								<div class="col-xs-12">
									<img src="" class="img-responsive large-image-product">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<style type="text/css">
			.quantity{
				font-size: 60px;
			    color: red;
			    font-weight: bold;
			    display: inline-block;
  				vertical-align: middle;
			}
		</style>
		<script type="text/javascript">
			var orderLoader = (function() {
				'use strict';

				function orderLoader() {
					// enforces new
					if (!(this instanceof orderLoader)) {
						return new orderLoader();
					}
					// constructor body
					this.url_get_orders = '{{ route('apiSupplier.getOrdersAjax') }}';
				}

				orderLoader.prototype.get_orders_ajax = function() {
					$('#loader-routes').removeClass('unseen');
					$('#load-orders').attr('disabled', 'disabled');
					$('#lists-items').slideUp('fast');
					$('#detail-items').slideUp('fast').empty();
					$.ajax({
						url: this.url_get_orders,
						type: 'GET',
						dataType: 'json',
					})
					.done(function(data) {
						$('#pending_orders').empty().html(data['orders_w_missings']);
						$('#cold_orders').empty().html(data['orders_w_colds']);
						$('#lists-items').slideDown('fast');
					})
					.fail(function() {
						console.error("error al cargar las ordenes.");
					})
					.always(function() {
						$('#loader-routes').addClass('unseen');
					});
				};

				orderLoader.prototype.update_ready_orders = function(order_id) {
					$('#loader-routes').removeClass('unseen');
					$.ajax({
						url: this.url_update_orders,
						type: 'POST',
						dataType: 'json',
						data: {
								order_id: order_id
							},
					})
					.done(function(data) {
						if ( data.status) {
						}
					})
					.fail(function() {
						console.error("error al actualizar las ordenes.");
					})
					.always(function() {
						$('#loader-routes').addClass('unseen');
					});
				};

				return orderLoader;
			}());

			jQuery(document).ready(function($) {

				var order_loader = new orderLoader;
				order_loader.get_orders_ajax();

				var orderDetails = (function() {
					'use strict';

					function orderDetails() {
						// enforces new
						if (!(this instanceof orderDetails)) {
							return new orderDetails();
						}
						// constructor body
						this.url_order_details = '{{ route('apiSupplier.getOrderDetailsAjax') }}';
						this.url_product_confirm_quantity = '{{ route('apiSupplier.confirmProductQuantityAjax') }}';
						this.order_product_id = null;
						this.quantity = null;
						this.type = null;
						this.products = null;
						this.bindActions();
					}

					orderDetails.prototype.bindActions = function() {
						var self = this;
						$('body').on('click', '.order-detail-link', function(event) {
							event.preventDefault();
							self.order_id = $(this).data('order_id');
							self.type = $(this).data('type');
							self.get_details_ajax();
						});
						$('body').on('click', '.close-order-detail', function(event) {
							event.preventDefault();
							$('#detail-items').slideUp('fast', function () {
								$(this).empty();
								$('#lists-items').slideDown('fast');
							});
						});
						$('body').on('click', '.list-group-item .fullfilled', function(event) {
							event.preventDefault();
							var order_product_id = $(this).data('id');
							if($(this).data('quantity') > 1){
								$('.order_product_id').val(order_product_id);
								$('.product_type').val($(this).data('type'));
								$('.product-modal').modal('show');
							}else{
								$(this).parents('.list-group-item').removeClass('alert-danger').addClass('alert-success');
								self.update_product(order_product_id, 'Fullfilled');
							}
						});
						$('body').on('click', '.list-group-item .not-available', function(event) {
							var product_id = $(this).data('id');
							event.preventDefault();
							$(this).parents('.list-group-item').removeClass('alert-success').addClass('alert-danger');
							self.update_product(product_id, 'Not Available');
						});
						$('body').on('click', '.update-order-status', function(event) {
							event.preventDefault();
							self.update_order_status();
						});
						$('body').on('click', '.img-responsive', function(event) {
							event.preventDefault();
							var src = null;
							src = $(this).attr('src');
							src = src.replace('medium', 'large');
							console.log(src);
							$('.large-image-product').attr('src', src);
							$('#modal-large-image').modal('show');
						});
						$('body').on('click', '.update-product', function(){
							event.preventDefault();
							self.order_product_id = $('.order_product_id').val();
							self.quantity = $('.confirm-quantity').val();
							self.type = $('.product_type').val();
							self.product_confirm_quantity();
						});
					};

					orderDetails.prototype.get_details_ajax = function() {
						var data = {
							order_id: this.order_id,
							type: this.type
						}
						if ( localStorage.getItem(this.order_id) !== null && localStorage.getItem(this.order_id) !== undefined ) {
							data.products = JSON.parse( localStorage.getItem( this.order_id ) );
						}
						$('#loader-routes').removeClass('unseen');
						$('#lists-items').slideUp('fast');
						$.ajax({
							url: this.url_order_details,
							type: 'POST',
							dataType: 'json',
							data: data,
							context: this
						})
						.done(function(data) {
							if (data.status) {
								$('#detail-items').slideDown('fast').empty().html(data.html);
								this.products = data.products;
							}
						})
						.fail(function() {
							console.log("error al obtener los detalles.");
						})
						.always(function() {
							$('#loader-routes').addClass('unseen');
						});
					};

					orderDetails.prototype.update_product = function(product_id, status) {
						var self = this;
						var arr = jQuery.grep(this.products, function( obj, index ) {
							if ( obj.id == product_id ) {
								self.products[index].fulfilment_status = status;
							}
						});
						localStorage.setItem(this.order_id, JSON.stringify(this.products));
					};

					orderDetails.prototype.product_confirm_quantity = function(){
						if(this.quantity == ''){
							alert('Es necesario que indiques la cantidad del producto.');
							return;
						}

						var data = {
							order_product_id: this.order_product_id,
							quantity: this.quantity,
							type: this.type
						}

						$.ajax({
							url: this.url_product_confirm_quantity,
							type: 'POST',
							dataType: 'json',
							data: data,
							context: this
						})
						.done(function(data) {
							if (data.status) {
								event.preventDefault();
								$('.product-modal').modal('hide');
								$('.product-'+this.order_product_id).parents('.list-group-item').removeClass('alert-danger').addClass('alert-success');
								$('.product-'+this.order_product_id).parents('.list-group-item').find('.quantity').html(data.quantity);
								$('.confirm-quantity').val('');
								this.update_product(this.order_product_id, 'Fullfilled');

							}else{
								alert(data.message);
								event.preventDefault();
								$('.product-modal').modal('hide');
							}
						})
						.fail(function() {
							console.log("error al confirmar la cantidad del producto.");
						})
						.always(function() {

						});
					};

					orderDetails.prototype.validate_products_status = function() {
						var products_count = $('.list-group li').length;
						var products_clicked = $('.list-group li.alert-danger, .list-group li.alert-success').length;
						if ( products_clicked != products_count ) {
							return false;
						}
						return true;
					};

					orderDetails.prototype.update_order_status = function() {
						var is_valid = this.validate_products_status();
						$('#loader-routes').removeClass('unseen');
						$('body').find('.update-order-status').attr('disabled', 'disabled');
						if ( !is_valid ) {
							alert('Es necesario que indiques los estados de los productos.');
							return;
						}

						$.ajax({
							url: '{{ route('apiSupplier.updateOrderStatusAjax') }}',
							type: 'POST',
							dataType: 'json',
							data: {
								order_id: this.order_id,
								products: this.products
							},
							context: this
						})
						.done(function(data) {
							if ( data.status ) {
								$('.close-order-detail').trigger('click');
								$('#'+this.order_id).slideUp('fast').remove();
								localStorage.removeItem(this.order_id);
								alert(data.message);
								order_loader.get_orders_ajax();
							}else{
								alert(data.message);
								order_loader.get_orders_ajax();
							}
						})
						.fail(function() {
							console.log("error al guardar el estado de las ordenes.");
							$('body').find('.update-order-status').removeAttr('disabled');
						})
						.always(function() {
							$('#loader-routes').addClass('unseen');
							$('body').find('.update-order-status').removeAttr('disabled');
						});

					};

					return orderDetails;
				}());
				var order_details = new orderDetails;

			});
		</script>
	@stop
@else
	@section('orders_w_missings')
		@if ( isset($orders_w_missings) && count($orders_w_missings) )
			<br>
			@foreach ($orders_w_missings as $order)
				<div class="order-item" id="{{ $order->id }}">
					<div class="row">
						<a href="javascript:;" data-type="seco" data-order_id="{{ $order->id }}" data-route="{{ $order->route_id }}" data-sequence="{{ $order->planning_sequence }}" class="order-detail-link">
							<div class="col-xs-12">
								<label>Pedido #</label> {{ $order->id }}<br>
								<label>Ruta:</label> {{ $order->route }} <br>
								<label>Secuencia:</label> {{ $order->planning_sequence }} <br>
							</div>
						</a>
					</div>
				</div>
			@endforeach
		@else
			<br>
			<div class="order-item">
				<div class="row">
					<a href="javascript:;">
						<div class="col-xs-12 text-center">
							<label>No se encontraron pedidos</label>
						</div>
					</a>
				</div>
			</div>
		@endif
	@stop
	@section('orders_w_colds')
		@if ( isset($orders_w_colds) && count($orders_w_colds) )
			<br>
			@foreach ($orders_w_colds as $order)
				<div class="order-item" id="{{ $order->id }}">
					<div class="row">
						<a href="javascript:;" data-type="congelado" data-order_id="{{ $order->id }}" data-route="{{ $order->route_id }}" data-sequence="{{ $order->planning_sequence }}" class="order-detail-link">
							<div class="col-xs-12">
								<label>Pedido #</label> {{ $order->id }}<br>
								<label>Ruta:</label> {{ $order->route }} <br>
								<label>Secuencia:</label> {{ $order->planning_sequence }} <br>
							</div>
						</a>
					</div>
				</div>
			@endforeach
		@else
			<br>
			<div class="order-item">
				<div class="row">
					<a href="javascript:;">
						<div class="col-xs-12 text-center">
							<label>No se encontraron pedidos</label>
						</div>
					</a>
				</div>
			</div>
		@endif
	@stop
	@section('order_detail')
		@if ( isset($products) && isset($order) && count($products) )
			<div class="col-xs-12 order-detail">
				<div class="box box-primary">
					<div class="box-body">
						<div class="order-detail-header">
							<div class="row">
								<div class="col-xs-12">
									<h4>Detalle de pedido <button type="button" class="btn btn-danger pull-right close-order-detail">X</button></h4>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<strong>Pedido #</strong> {{ $order['order_id'] }}<br>
								</div>
							</div>
						</div>
						<hr>
						<div class="order-detail-content">
							<div class="row">
								<div class="col-xs-12">
									<h4>Productos faltantes</h4>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<ul class="list-group">
										@foreach ($products as $product)
											<li class="list-group-item @if ( $product['fulfilment_status'] == 'Fullfilled' ) alert-success @endif @if ( $product['fulfilment_status'] == 'Not Available' ) alert-danger @endif">
												<h4 class="list-group-item-heading">{{ $product['product_name'] }}</h4>
												<br>
												<div class="row">
													<div class="col-xs-3 product-image" style="padding-right: 0;">
														<img class="img-responsive" src="{{ $product['product_image_url'] }}">
													</div>
													<div class="col-xs-7" style="padding-right: 0;">
														<label>Cantidad:</label><span class="quantity">{{ $product['quantity_missing'] }}</span><br>
														<label>Almacenamiento:</label> {{ $product['storage'] }} <br>
														<label>Posición:</label>{{ $product['position'] }} - {{ $product['position_height'] }}<br>
													</div>
													<div class="col-xs-2" style="padding: 0;">
														<button class="btn btn-default fullfilled product-{{ $product['id'] }}" data-status="Fullfilled" data-id="{{ $product['id'] }}" data-quantity="{{ $product['quantity_missing'] }}" data-type="@if(isset($product['type'])){{ $product['type'] }}@else{{ 'Agrupado' }}@endif">
															<span class="glyphicon glyphicon-ok"></span>
														</button>
														<br>
														<br>
														<button class="btn btn-default not-available" data-status="Not Available" data-id="{{ $product['id'] }}" data-type="@if(isset($product['type'])){{ $product['type'] }}@else{{ 'Agrupado' }}@endif">
															<span class="glyphicon glyphicon-remove"></span>
														</button>
													</div>
												</div>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 text-center">
									<button class="btn btn-success update-order-status">Pedido alistado</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade product-modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Confirmar cantidad</h4>
						</div>
						<div class="modal-body">
							<p><input class="form-control text confirm-quantity" type="number" class="number" placeholder="Digita cantidad" required="required"></p>
							<input type="hidden" name="order_product_id" class="order_product_id" />
							<input type="hidden" name="order_id" class="order_id" />
							<input type="hidden" name="product_type" class="product_type" />
							<div class="btn btn-success btn-block update-product">Enviar</div>
							<div class="btn btn-default btn-block" data-dismiss="modal">Cerrar</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@else
		@endif
	@stop
@endif