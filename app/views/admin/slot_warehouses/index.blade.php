@if(!Request::ajax())
@extends('admin.layout')
@section('content')
<script>
	var web_url_ajax = "{{ route('adminSlotWarehouse.index') }}";
</script>

<section class="content-header">
	<h1>
		{{ $title }}
		<small>Control panel</small>
	</h1>
</section>
<section class="content">
@if(Session::has('message'))
	@if(Session::get('type') == 'success')
	<div class="alert alert-success alert-dismissable">
		<i class="fa fa-check"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Hecho!</b> {{ Session::get('message') }}
	</div>
	@else
	<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Alerta!</b> {{ Session::get('message') }}
	</div>
	@endif
@endif
	<div class="row">
        <div class="col-xs-12">
			<div class="box box-primary">
                <div class="box-body">
                    <form role="form" method="POST" id='slot-cities-form' action="{{ route('adminSlotWarehouse.save') }}" class = "form-group">
                        <div class="row">
                            <div class="col-xs-2 form-group">
                                <label>Seleccione la ciudad</label>
                                <select name="city_id" id="city_id" class="form-control">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" @if($city->id == $cityId) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-2 form-group">
                                <label>Seleccione la bodega</label>
                                <select name="warehouse_id" id="warehouse_id" class="form-control" required>
                                    @foreach($warehouses as $warehouse)
                                        <option value="{{ $warehouse->id }}" >{{ $warehouse->warehouse }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-1 form-group">
                                <label>&nbsp;</label>
                                <input type="submit" id="btn-save" name="submit" value="Guardar" class="btn btn-primary btn-block">
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                        <div class="row">
                            <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                            <div class="col-md-12 slot-warehouses-container">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var BasicSlotCities = (function() {
        'use strict';

        function BasicSlotCities(args) {
            // enforces new
            if (!(this instanceof BasicSlotCities)) {
                return new BasicSlotCities(args);
            }
            // constructor body
            this.city_id = null;
            this.warehouse_id = null;
            this.get_warehouses_by_city_url = "{{ route('adminSlotWarehouse.getWarehousesByCityAjax') }}";
            this.get_index = "{{ route('adminSlotWarehouse.index') }}";
            this.bindActions();
        }

        BasicSlotCities.prototype.bindActions = function() {
            var self = this;
            $('body').on('change', '#city_id', function(event) {
                self.city_id = $(this).val();
                if (self.city_id) self.getWarehouses();
            });
            $('#warehouse_id').on('click', function (event) {
                $('#warehouse_id').prop('disabled', true);
                event.preventDefault();
                self.warehouse_id = $('#warehouse_id').val();
                $('#warehouse_id').prop('disabled', false);
            })
            $('.alert ').delay(5000).fadeOut();
            $('body').on('change', '#warehouse_id', function(event) {
                self.warehouse_id = $(this).val();
                if (self.warehouse_id) self.getSlots();
            });
            self.getSlots();
        };

        BasicSlotCities.prototype.getWarehouses = function() {
            var self = this;
            var warehouse_id = $('#warehouse_id');
            warehouse_id.attr('disabled', 'disabled');
            $.ajax({
                url: this.get_warehouses_by_city_url,
                type: 'GET',
                dataType: 'JSON',
                data: {city_id: $('#city_id').val()}
            })
            .done(function(data) {
                warehouse_id.empty();
                $.each(data.result.warehouses, function(key, value) {
                    if (key === 0) {
                        warehouse_id.append('<option value="'+value.id+'" selected="">'+value.warehouse+'</option>');
                    } else {
                        warehouse_id.append('<option value="'+value.id+'">'+value.warehouse+'</option>');
                    }
                });
                warehouse_id.prop('disabled', false);
                warehouse_id.removeAttr('disabled');
                self.getSlots();
            })
            .fail(function(err) {
                alert('Ocurrio un error al cargar las bodegas de la ciudad seleccionada.');
                warehouse_id.removeAttr('disabled');
                warehouse_id.prop('disabled', false);
            });
        };

        BasicSlotCities.prototype.getSlots = function() {
            var warehouse_id = $('#warehouse_id');
            var city_id = $('#city_id');

            $('.slot-zones-container').html('<div class="row"><div class="col-xs-12 text-center">Cargando slots...</div></div>');
            warehouse_id.attr('disabled', 'disabled');
            $('.slot-warehouses-container').html('');
            $('.paging-loading').show();
            city_id.attr('disabled', 'disabled');
            $.ajax({
                url: this.get_index,
                type: 'GET',
                dataType: 'JSON',
                data: {warehouse_id: $('#warehouse_id').val(), city_id : $('#city_id').val()}
            })
            .done(function(data) {
                if(data.status == true){
                    $('.slot-warehouses-container').html(data.result);
                }
                warehouse_id.removeAttr('disabled');
                city_id.removeAttr('disabled');
                $('.paging-loading').hide();
            })
            .fail(function(err) {
                alert('Ocurrio un problema al cargar los slots por bodega.');
                warehouse_id.removeAttr('disabled');
                city_id.removeAttr('disabled');
            });
        };
        return BasicSlotCities;
    }());

    $(document).ready(function() {
        var basic_slot_cities = new BasicSlotCities;
        //basic_slot_cities.getSlots();
    });
</script>
@endsection
@else
@section('list_slot')
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <h3>Bodega {{$slotWarehouse['warehouse']->warehouse}}</h3>
            <hr>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Domingo
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Lunes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Martes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Miércoles
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Jueves
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Viernes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                    <th class="text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                Sábado
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($slotWarehouse['dates'] as $key => $date)
                    <tr>
                        <td>
                            <table class="table table-bordered text-center">
                                <tr>
                                    <td nowrap="nowrap">
                                        {{ $date['start'] }}
                                    </td>
                                    <td nowrap="nowrap">
                                        {{ $date['end'] }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                        @foreach ($date['slots'] as $slot)
                            <td style="vertical-align: middle">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <input type="text" name="time[{{$key}}][{{$slot['day']}}][numberProducts]" class="form-control" value="{{ $slot['numberProducts'] }}" style="text-align: center;">
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <input type="text" disabled="disabled" style="text-align: center;" name="time[{{$key}}][{{$slot['day']}}][currentProducts]" class="form-control" value="{{ $slot['currentProducts'] }}">
                                    </div>
                                </div>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td class="text-center" style="font-weight: bold">
                        <div class="row">
                            <div class="col-xs-12">
                                Hoy
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Limite
                            </div>
                            <div class="col-xs-6">
                                Actuales
                            </div>
                        </div>
                    </td>
                </tr>
                @foreach ($slotWarehouse['today'] as $key => $date)
                    <tr>
                        <td>
                            <table class="table table-bordered text-center">
                                <tr style="text-transform: uppercase">
                                    <td nowrap="nowrap">
                                        {{ $date['start'] }}
                                    </td>
                                    <td nowrap="nowrap">
                                        {{ $date['end'] }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                        @foreach ($date['slots'] as $slot)

                            <td style="vertical-align: middle">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <input type="text" name="time[{{$key}}][{{$slot['day']}}][numberProducts]" class="form-control" value="{{ $slot['numberProducts'] }}" style="text-align: center;">
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <input type="text" disabled="disabled" style="text-align: center;" name="time[{{$key}}][{{$slot['day']}}][currentProducts]" class="form-control" value="{{ $slot['currentProducts'] }}">
                                    </div>
                                </div>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <hr>
    </div>
@endsection
@endif