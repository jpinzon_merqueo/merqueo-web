@if (!Request::ajax())
    @extends('admin.layout')
    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }}  # {{ $order->id }}
            <small>Control panel</small>
        </h1>
        <span class="breadcrumb" style="top:0px">
        @if ($order && $admin_permissions['update'] && ($order->status_validation_dry == 'Pendiente' && $validation == 'seco') || ($order->status_validation_cold == 'Pendiente' && $validation == 'frio'))
            <a href="{{ route('AdminPickingQuality.updateValidation', ['id' => $order->id, 'validation'=> $validation]) }}" onclick="return confirm('¿Estas seguro que deseas cerrar la validación del pedido?')">
                <button type="button" class="btn btn-success">Pedido Validado</button>
            </a>
        @endif
        </span>
    </section>
    <style type="text/css">
        .button-container{ position: relative; }
        #quality-question .error{
                position: absolute;
                top: 18px;
                width: 210px;
        }
    </style>
    <script type="text/javascript">
    function search_products(validation, product_id = ''){
        $('#product-reference').prop('readonly', true);
        $('.load').removeClass('unseen');
        $.ajax({
            url: '{{ route('AdminPickingQuality.ValidateProductAjax', ['id' => $order->id]) }}',
            data: { s: $('#product-reference').val(),
                    validation_type: $('#validation-type').val(),
                    validation_product: $('#validation-product').val(),
                    product_id: product_id,
                    validation: validation,
                    check_pack_dry: $('.check_pack_dry:checked').val(),
                    check_gift_dry: $('.check_gift_dry:checked').val(),
                    check_pack_cold: $('.check_pack_cold:checked').val(),
                    check_gift_cold: $('.check_gift_cold:checked').val()
                },
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            $('#products_table_container').empty();
            $('#products_table_container').html(data.html);

            if(!product_id && $('#validation-type').val() == 'manually_validate'){
                $('.delete-product').addClass('unseen');
                $('.manually_validate').removeClass('unseen');
            }

            if(product_id){
                $('.delete-product').removeClass('unseen');
                $('.manually_validate').addClass('unseen');
            }

            if ( data.success )  {
                $('#products_error .message').empty().html(data.message);
                $('#products_error').slideDown('fast').delay(10000).slideUp('fast');
            }
        })
        .fail(function() {
            console.error("error al cargar los productos");
        })
        .always(function() {
            $('#product-reference').val('');
            $('.load').addClass('unseen');
        });
        $('#product-reference').prop('readonly', false);
    }

    $(document).ready(function() {
        @if(($order->status_validation_dry == 'Pendiente' && $validation == 'seco') || ($order->status_validation_cold == 'Pendiente' && $validation == 'frio'))
            search_products($('#validation').val());
        @endif

        $('body').on('change', '#validation', function(){
            var opc = $(this).val();
            if(opc === 'seco' )
                window.location.href = "{{ route('AdminPickingQuality.edit', ['id' => $order->id, 'validation'=> 'seco']) }}";
            else
                window.location.href = "{{ route('AdminPickingQuality.edit', ['id' => $order->id, 'validation'=> 'frio']) }}";

        });

        $('#product-reference').keyup(function(event){
            code = event.which;
            if (code == 13 || code == 188 || code == 186){
                search_products($('#validation').val());
            }
        });

        $('body').on('click', '.btn-remove-product', function(){
            $('.btn-remove-product').addClass('disable');
            $('.load').removeClass('unseen');
            $.ajax({
                url: '{{ route('AdminPickingQuality.delete', ['id' => $order->id]) }}',
                data: { product_id: $(this).data('id'), validation: $('#validation').val()},
                type: 'GET',
                dataType: 'json'
            })
            .done(function(data) {
                $('#products_table_container').empty();
                $('#products_table_container').html(data.html);
                $('.delete-product').removeClass('unseen');
                $('.manually_validate').addClass('unseen');

                if ( data.success ) {
                    $('#products_error .message').empty().html(data.message);
                    $('#products_error').slideDown('fast').delay(10000).slideUp('fast');
                }
            })
            .fail(function() {
                console.error("error al cargar los productos");
            })
            .always(function() {
                $('#product-reference').val('');
                $('.load').addClass('unseen');
            });
            $('.btn-remove-product').removeClass('disable');
        });

        $('body').on('click', '.validate-product', function(){
            search_products($('#validation').val(), $(this).data('product'));
        });
    });
    </script>

    <section class="content">

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    <form role="form" method="post" id='main-form' action="{{ route('AdminPickingQuality.save', ['id' => $order->id]) }}">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="callout callout-info">
                                    <h4>Datos de Validación</h4>
                                    <p><label>Estado de validación en seco</label> - <b>{{ $order->status_validation_dry }} </b></p>
                                    <p><label>Estado de validación en frio</label> - <b>{{ $order->status_validation_cold }} </b></p>
                                    @if($order->picking_revision_date)
                                    <p><label>Fecha de validación</label> - {{ date("d M Y g:i a", strtotime($order->picking_revision_date)) }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="callout callout-info">
                                    <h4>Datos de Pedido</h4>
                                    <p><label>Estado</label> - <b>{{ $order->status }} </b></p>
                                    <p><label>Ciudad</label> - {{ $order->store_city }}</p>
                                    <p><label>Cantidad de productos</label> - {{ $order->quantity + $order->quantity_group }}</p>
                                    <p><label>Fecha de entrega</label> - {{ date("d M Y g:i a", strtotime($order->customer_delivery_date)) }} ({{ $order->delivery_time }})</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="callout callout-info">
                                    <h4>Datos de Transportador</h4>
                                    @if (!empty($order->route) || !empty($order->planning_route))
                                    <p><label>Ruta - </label>@if (!empty($order->route)) {{ $order->route }} @else {{ $order->planning_route }} @endif</p>
                                    <p><label>Secuencia - </label> {{ $order->planning_sequence }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <legend class="title-validation">Alistamiento</legend>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <select name="validation" id="validation" class="form-control validation">
                                                <option value="seco" @if($validation && $validation == 'seco') selected="selected" @endif>Seco</option>
                                                <option value="frio" @if($validation && $validation == 'frio') selected="selected" @endif>Frío</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(($order->status_validation_cold == 'Pendiente' && $validation == 'frio') || ($order->status_validation_dry == 'Pendiente' && $validation == 'seco'))
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <legend class="title-validation">Validación de productos alistados</legend>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="products_error" class="alert alert-danger alert-dismissable" style="display: none;">
                                                <i class="fa fa-ban"></i>
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <b>Alerta!</b> <div class="message"></div>
                                            </div>
                                            <div id="products_success" class="alert alert-success alert-dismissable" style="display: none;">
                                                <i class="fa fa-ban"></i>
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <b>Alerta!</b> <div class="message"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space-in-question"><p>&nbsp;</p></div>
                                    @if(is_null($order->check_pack_dry) && $validation == 'seco')
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <h4>¿El embalaje fue el correcto seco?</h4>
                                            <label for="check_pack_dry" class="radio-inline control-label">
                                                <input type="radio" name="check_pack_dry" class="form-control check_pack_dry" value="1"/> Sí
                                            </label>
                                            <label for="check_pack_dry" class="radio-inline control-label">
                                                <input type="radio" name="check_pack_dry" class="form-control check_pack_dry" value="0"/> No
                                            </label>
                                        </div>
                                        <div class="col-xs-3">
                                            <h4>¿El pedido contiene obsequio en seco?</h4>
                                            <label for="check_gift_dry" class="radio-inline control-label">
                                                <input type="radio" name="check_gift_dry" class="form-control check_gift_dry" value="1"/> Sí
                                            </label>
                                            <label for="check_gift_dry" class="radio-inline control-label">
                                                <input type="radio" name="check_gift_dry" class="form-control check_gift_dry" value="0"/> No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row space-in-question"><p>&nbsp;</p></div>
                                    @endif

                                    @if(is_null($order->check_pack_cold) && $validation == 'frio')
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <h4>¿El embalaje fue el correcto en frio?</h4>
                                            <label for="check_pack_cold" class="radio-inline control-label">
                                                <input type="radio" name="check_pack_cold" class="form-control check_pack_cold" value="1"/> Sí
                                            </label>
                                            <label for="check_pack_cold" class="radio-inline control-label">
                                                <input type="radio" name="check_pack_cold" class="form-control check_pack_cold" value="0"/> No
                                            </label>
                                        </div>
                                        <div class="col-xs-3">
                                            <h4>¿El pedido contiene obsequio en frio?</h4>
                                            <label for="check_gift_dry" class="radio-inline control-label">
                                                <input type="radio" name="check_gift_cold" class="form-control check_gift_cold" value="1"/> Sí
                                            </label>
                                            <label for="check_gift_dry" class="radio-inline control-label">
                                                <input type="radio" name="check_gift_cold" class="form-control check_gift_cold" value="0"/> No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row space-in-question"><p>&nbsp;</p></div>
                                    @endif

                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label for="validation-type">Tipo de validación</label>
                                            <select name="validation-product" id="validation-product" class="form-control">
                                                <option value="Validado" selected="selected">Validar producto alistado</option>
                                                <option value="Mal estado">Productos en mal estado</option>
                                                <option value="Vencido">Productos vencidos</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="validation-type">Buscar</label>
                                            <select name="validation-type" id="validation-type" class="form-control">
                                                <option value="reference_validate" selected="selected">Por referencia</option>
                                                <option value="manually_validate">Manualmente</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="product-reference">Gestionar producto</label>
                                            <input type="text" placeholder="referencia, nombre producto" id="product-reference" name="product-reference" class="form-control"/><br>
                                        </div>
                                        <div class="col-xs-3">
                                            <button style="margin-top: 24px;" onclick="search_products('{{ $validation }}');" class="btn btn-primary">Buscar</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="products_table_container">
                                                <div class="col-xs-12 text-center unseen load"><img src="{{ asset_url() }}/img/loading.gif" align="center"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                    @endif

                    @if(($order->status_validation_dry == 'Validado' && $validation == 'seco') || ($order->status_validation_cold == 'Validado' && $validation == 'frio'))
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <legend>Resumen de la validación</legend>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="products_table_container">
                                                @if(!$admin_permissions['permission1'])
                                                    <legend>Productos a sacar del pedido</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_expired))
                                                              @foreach($products_expired as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_shrinkage }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif

                                                            @if (count($products_aditional))
                                                              @foreach($products_aditional as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif

                                                            @if (count($products_changed))
                                                              @foreach($products_changed as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif

                                                            @if (count($products_poor_condition))
                                                              @foreach($products_poor_condition as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_shrinkage }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif
                                                            @if (!$products_expired && !$products_aditional && !$products_changed && !$products_poor_condition)
                                                            <tr>
                                                                <td colspan="5" align="center">
                                                                    No hay productos para sacar.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                <br/>
                                                @elseif($products_expired && $admin_permissions['permission1'])
                                                    <legend>Productos Vencidos</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Cantidad solicitados</th>
                                                                <th>Cantidad validados</th>
                                                                <th>Cantidad adicional en unidades</th>
                                                                <th>Cantidad de trocados</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_expired))
                                                              @foreach($products_expired as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                                                                    <td align="center">{{ $product->quantity + $product->quantity_group }}</td>
                                                                    <td align="center">{{ $product->quantity_validated_expired }}</td>
                                                                    <td align="center">{{ $product->quantity_additional_expired }}</td>
                                                                    <td align="center">{{ $product->quantity_changed_expired }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    No hay productos validados.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                <br>
                                                @endif

                                                @if($products_poor_condition && $admin_permissions['permission1'])
                                                    <legend>Productos en mal estado</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Cantidad solicitados</th>
                                                                <th>Cantidad validados</th>
                                                                <th>Cantidad adicional en unidades</th>
                                                                <th>Cantidad de trocados</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_poor_condition))
                                                              @foreach($products_poor_condition as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">{{ $product->quantity + $product->quantity_group }}</td>
                                                                    <td align="center">{{ $product->quantity_validated_poor_condition }}</td>
                                                                    <td align="center">{{ $product->quantity_additional_poor_condition }}</td>
                                                                    <td align="center">{{ $product->quantity_changed_poor_condition }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    No hay productos validados.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                @endif
                                                <br/>
                                                @if($products_pending && $admin_permissions['permission1'])
                                                    <legend>Productos faltantes</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Cantidad solicitados</th>
                                                                <th>Cantidad validados</th>
                                                                <th>Cantidad adicional en unidades</th>
                                                                <th>Cantidad faltantes</th>
                                                                <th>Cantidad de trocados</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_pending))
                                                              @foreach($products_pending as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity }} {{ $product->product_unit }}</td>
                                                                    <td align="center">{{ $product->quantity + $product->quantity_group }}</td>
                                                                    <td align="center">{{ $product->quantity_validated }}</td>
                                                                    <td align="center">{{ $product->quantity_additional }}</td>
                                                                    <td align="center">{{ $product->quantity_pending }}</td>
                                                                    <td align="center">{{ $product->quantity_changed }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    No hay productos validados.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                <br/>
                                                @endif
                                                @if($products_aditional && $admin_permissions['permission1'])
                                                    <legend>Productos con unidades adicionales</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Cantidad solicitados</th>
                                                                <th>Cantidad validados</th>
                                                                <th>Cantidad adicional en unidades</th>
                                                                <th>Cantidad faltantes</th>
                                                                <th>Cantidad de trocados</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_aditional))
                                                              @foreach($products_aditional as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">{{ $product->quantity + $product->quantity_group }}</td>
                                                                    <td align="center">{{ $product->quantity_validated }}</td>
                                                                    <td align="center">{{ $product->quantity_additional }}</td>
                                                                    <td align="center">{{ $product->quantity_pending }}</td>
                                                                    <td align="center">{{ $product->quantity_changed }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    No hay productos validados.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                    <br/>
                                                @endif

                                                @if($products_changed && $admin_permissions['permission1'])
                                                    <legend>Productos trocados</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Cantidad solicitados</th>
                                                                <th>Cantidad validados</th>
                                                                <th>Cantidad adicional en unidades</th>
                                                                <th>Cantidad faltantes</th>
                                                                <th>Cantidad de trocados</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_changed))
                                                              @foreach($products_changed as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">{{ $product->quantity + $product->quantity_group }}</td>
                                                                    <td align="center">{{ $product->quantity_validated }}</td>
                                                                    <td align="center">{{ $product->quantity_additional }}</td>
                                                                    <td align="center">{{ $product->quantity_pending }}</td>
                                                                    <td align="center">{{ $product->quantity_changed }}</td>
                                                                    <td align="center">
                                                                        <span class="badge bg-red">{{ $product->status_product_validation }}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    No hay productos validados.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                <br/>
                                                @endif

                                                @if($products_validated && $admin_permissions['permission1'])
                                                    <legend>Productos alistados completos</legend>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Referencia</th>
                                                                <th>Producto</th>
                                                                <th>Unidad de medida</th>
                                                                <th>Cantidad solicitados</th>
                                                                <th>Cantidad validados</th>
                                                                <th>Cantidad adicional en unidades</th>
                                                                <th>Cantidad faltantes</th>
                                                                <th>Cantidad de vencidos</th>
                                                                <th>Cantidad de trocados</th>
                                                                <th>Cantidad en mal estado</th>
                                                                <th>Estado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($products_validated))
                                                              @foreach($products_validated as $product)
                                                                <tr>
                                                                    <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                                                                    <td>{{ $product->reference }}</td>
                                                                    <td>{{ $product->product_name }}</td>
                                                                    <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                                                                    <td align="center">{{ $product->quantity + $product->quantity_group }}</td>
                                                                    <td align="center">{{ $product->quantity_validated  }}</td>
                                                                    <td align="center">{{ $product->quantity_additional }}</td>
                                                                    <td align="center">{{ $product->quantity_pending }}</td>
                                                                    <td align="center">{{ $product->quantity_expired }}</td>
                                                                    <td align="center">{{ $product->quantity_changed }}</td>
                                                                    <td align="center">{{ $product->quantity_poor_condition }}</td>
                                                                    <td align="center">
                                                                        @if($product->status_product_validation == 'Validado' && $product->quantity_expired == 0)
                                                                            <span class="badge bg-green">Validado</span>
                                                                        @else
                                                                            <span class="badge bg-red">Validado</span>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    No hay productos validados.
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@else
@section('content')
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Referencia</th>
            <th>Producto</th>
            <th>Unidad de medida</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        @if (count($products))
          @foreach($products as $product)
            <tr>
                <td align="center"><img src="{{ $product->product_image_url }}" height ="50px"></td>
                <td>{{ $product->reference }}</td>
                <td>{{ $product->product_name }}</td>
                <td align="center">{{ $product->product_quantity.' '.$product->product_unit }}</td>
                <td align="center">
                    <div class="btn-group manually_validate unseen">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="validate-product" href="javascript:;" data-product="{{ $product->store_product_id }}">Validado</a></li>
                        </ul>
                    </div>
                    <div class="btn-group delete-product">
                        <a class="btn btn-xs btn-danger btn-remove-product" href="javascript:;" data-id="{{ $product->product_validation_id }}">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
        @else
        <tr>
            <td colspan="5" align="center">
                No hay productos validados.
            </td>
        </tr>
        @endif
    </tbody>
</table>
@endif
@stop
