@if (!Request::ajax())
    @extends('admin.layout')

    @section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>Control panel</small>
        </h1>
        @if ($admin_permissions['insert'])
        <span class="breadcrumb" style="top:0px">
            <a href="javascript:;" onclick="$('#modal-picking-Validation').modal('show');">
            <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Validación de Pedido</button>
            </a>
        </span>
        @endif
    </section>
    <section class="content">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Hecho!</b> {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Alerta!</b> {{ Session::get('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="search-form" class="qualities-table">
                                    <table width="100%">
                                        <tr>
                                            <td align="right">
                                                <label>Ciudad:</label>
                                            </td>
                                            <td>
                                                <select id="city_id" name="city_id" class="form-control get-warehouses">
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}" @if(Session::get('admin_city_id') == $city->id) selected="selected" @endif>{{ $city->city }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right">
                                                <label>Bodega:</label>
                                            </td>
                                            <td>
                                                <select id="warehouse_id" name="warehouse_id" class="form-control">
                                                    @if( Session::get('admin_designation') == 'Super Admin' )<option value="">Selecciona</option>@endif
                                                    @foreach ($warehouses as $warehouse)
                                                        <option value="{{ $warehouse->id }}" @if(Session::get('admin_warehouse_id') == $warehouse->id) selected="selected" @endif>{{ $warehouse->warehouse }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td align="right"><label># Pedido</label></td>
                                            <td>
                                                <input type="text" placeholder="ID pedido" id="input-search" class="form-control">
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td><button type="button" id="btn-search" class="btn btn-primary">Buscar</button></td>
                                        </tr>
                                   </table>
                                </form>
                            </div>
                        </div>
                        <br />
                        <div class="paging"></div>
                        <div align="center" class="paging-loading"><br><img src="{{ asset_url() }}/img/loading.gif" /></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Planning Date -->
        <div class="modal fade" id="modal-picking-Validation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Ingresa # Pedido</h4>
                    </div>
                    <form id="add-picking-validation-form" method="POST" action="{{ route('AdminPickingQuality.add') }}">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-xs-8">
                                    <label for="shopper-name"># Pedido</label>
                                    <input class="form-control" type="text" name="id" required="required">
                                </div>
                                <div class="col-xs-3">
                                    <label style="color: #FFF;">|</label>
                                    <button type="submit" class="btn btn-primary form-control save_order">Continuar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
    <script>
        var web_url_ajax = "{{ route('AdminPickingQuality.index') }}";
        $(document).ready(function() {
            paging(1, '');
            $('#btn-search').on('click', function(e) {
                paging(1, '');
                e.preventDefault();
            });

            $('body').on('change', '#city_id', function() {
                var city_id = $(this).val();
                $('#warehouse_id').html('').append($("<option value=''>Selecciona</option>"));
                $.ajax({
                    url: '{{ route('admin.get_warehouses_ajax') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: { city_id: city_id },
                })
                .done(function(data) {
                    $.each(data.warehouses, function(index, val) {
                         $('#warehouse_id').append($('<option></option>').attr('value', val.id).text(val.warehouse));
                    });
                });
            });
        });
    </script>
    @endsection
@else
    @section('content')
    <table id="qualities-table" class="qualities-table table table-bordered table-striped">
        <thead>
            <tr>
                <th>Pedido</th>
                <th>Validación creada por</th>
                <th>Alistador seco</th>
                <th>Alistador frio</th>
                <th>Cantidad de productos</th>
                <th>Fecha de inicio validación seco</th>
                <th>Fecha de final validación seco</th>
                <th>Fecha de inicio validación frio</th>
                <th>Fecha de final validación frio</th>
                <th>Estado del pedido</th>
                <th>Estado de validación seco</th>
                <th>Estado de validación frio</th>
                <th>Detalle</th>
            </tr>
        </thead>
        <tbody>
            @if(count($orders))
                @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->admin_validator }}</td>
                    <td>{{ $order->picker_dry_name }}</td>
                    <td>{{ $order->picker_cold_name }}</td>
                    <td>{{ $order->quantity + $order->quantity_group}}</td>
                    <td>@if($order->validation_date_start_dry){{ date("d M Y", strtotime($order->validation_date_start_dry)) }}@endif</td>
                    <td>@if($order->validation_date_end_dry){{ date("d M Y", strtotime($order->validation_date_end_dry)) }}@endif</td>
                    <td>@if($order->validation_date_start_cold){{ date("d M Y", strtotime($order->validation_date_start_cold)) }}@endif</td>
                    <td>@if($order->validation_date_end_cold){{ date("d M Y", strtotime($order->validation_date_end_cold)) }}@endif</td>
                    <td>
                        @if($order->status == 'Validation')
                        <span class="badge bg-maroon">Validation</span>
                        @elseif($order->status == 'Delivered')
                        <span class="badge bg-green">Delivered</span>
                        @elseif($order->status == 'Cancelled')
                        <span class="badge bg-red">Cancelled</span><br><strong>@if($order->temporarily_cancelled) (Temporalmente) @endif</strong>
                        @elseif($order->status == 'Initiated')
                        <span class="badge bg-yellow">Initiated</span>
                        @elseif($order->status == 'Dispatched')
                        <span class="badge bg-green">Dispatched</span>
                        @elseif($order->status == 'In Progress')
                        <span class="badge bg-orange">In Progress</span>
                        @elseif($order->status == 'Alistado')
                        <span class="badge bg-green">Alistado</span>
                        @elseif($order->status == 'Enrutado')
                        <span class="badge bg-orange">Enrutado</span>
                        @endif
                    </td>
                    <td>@if($order->status_validation_dry == 'Pendiente')<span class="badge bg-orange">{{ $order->status_validation_dry }}</span>@else<span class="badge bg-green">{{ $order->status_validation_dry }}</span>@endif</td>
                    <td>@if($order->status_validation_cold == 'Pendiente')<span class="badge bg-orange">{{ $order->status_validation_cold }}</span>@else<span class="badge bg-green">{{ $order->status_validation_cold }}</span>@endif</td>
                    <td align="center">
                        <div class="btn-group">
                           <a class="btn btn-xs btn-default" href="{{ route('AdminPickingQuality.edit', ['id' => $order->id, 'validation'=> 'seco']) }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="13" align="center">No hay datos.</td></tr>
            @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-3">
            <div class="dataTables_info" id="table_info">Mostrando {{ count($orders) }} ítems</div>
        </div>
    </div>
    @endsection
@endif
