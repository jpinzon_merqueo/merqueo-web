@extends('layout')

@section('title')Mis productos - Merqueo.com @stop
@section('description')El supermercado del ahorro. Entrega a domicilio en {{ $store->city_name }}, puedes pagar con tarjeta o en efectivo contra entrega. Todo sin moverte de casa. @stop

@section('content')

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "{{ $store->name }}",
  "logo": "{{ $store->logo_url }}",
  "url": "{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}",
  "contactPoint" : [{
        "@type" : "ContactPoint",
        "telephone" : "{{ $store->phone }}",
        "contactType" : "Teléfono Domicilios"
      }]
  }
}
</script>

<div class="col-md-12 content_list_data" style="margin-top: 115px">
	<div class="row col-xs-12 clearfix pro-row">
	    <div class="col-md-12">
            <div class="col-md-12">
                <div class="text_hora">
                    <h1>Mis productos a domicilio</h1>
                </div>
            </div>
        </div>

		<ol class="breadcrumb" data-catrel="{{ $store->id }}"  itemscope itemtype="http://schema.org BreadcrumbList">
		  {{ Breadcrumb::render('>', $store->name, 2, 'Mis productos') }}
		</ol>

		<div class="col-md-12 product-page-title">
			<h2><a class="head" href="#">Productos disponibles</a> <span></span></h2>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			{{ View::make('elements.products-list', compact('products', 'store')) }}
		</div>
	</div>
</div>
@stop
