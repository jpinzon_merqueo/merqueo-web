@extends('layout_header')

@section('content')
<div class="row">
    <div class="pro-bg-search col-md-12 col-xs-12">
        <div class="page-content col-md-12 col-xs-12 error-pages-container">
        	<img src="{{ asset_url() }}/img/404.png">
        </div>
        <div style="margin-bottom: 200px"></div>
    </div>
</div>
@include('layout_footer')
@stop