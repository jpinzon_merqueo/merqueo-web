@extends('layout_header')

@section('content')

    <div class="container-fluid pro-bg col-md-12 col-xs-12 error-pages-container">
        <div>
            <img src="{{ asset_url() }}/img/500.jpg"/>
            <br><br>
            <button type="button"
                    class="btn btn-success btn-block"
                    style="width: 30%; margin: 0 auto;"
                    onclick="window.location = '{{ url() }}'">Ir al home
            </button>
        </div>
    </div>

@stop