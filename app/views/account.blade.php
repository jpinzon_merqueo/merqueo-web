@extends('layout_home')

@section('content')

<div class="clearfix container-fluid user_account">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="account_user col-md-10 col-sm-10 col-xs-10">
		<div class="col-md-12 account-page-title">
			<div class="mdl-grid logtexttienda">
			</div>
			<h2>Mi Cuenta</h2>
			<h2 class="muted"></h2>
			@if(Session::has('message') )
			@if(Session::get('type') == 'success')
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				{{ Session::get('message') }}
			</div>
			@else
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<b>Error!</b> {{ Session::get('message') }}
			</div>
			@endif
			@endif
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">

			<div role="tabpanel">
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs acc-des" role="tablist">
				<li role="presentation" class="active"><a href="#acc" aria-controls="acc" role="tab" data-toggle="tab">Mi Cuenta</a></li>
				<li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Direcciones</a></li>
				<li role="presentation"><a href="#pay" aria-controls="pay" role="tab" data-toggle="tab">Tarjetas de Crédito</a></li>
				<li role="presentation"><a href="#ref" aria-controls="ref" role="tab" data-toggle="tab">Créditos</a></li>
				<li role="presentation"><a href="#ord" aria-controls="ord" role="tab" data-toggle="tab">Pedidos</a></li>
			  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">

			<div role="tabpanel" class="tab-pane fade active in" id="acc">
				<br>
				<div class="col-md-6 col-sm-12 col-xs-12 left">
					<h3 class="muted">INFORMACIÓN PERSONAL</h3>
					<h3 class="muted"><hr></h3>
					 <form class="form-horizontal" id="user-data-form" role="form" method="post" action="{{ web_url() }}/user/update">
					    <div class="unseen alert alert-danger form-has-errors"></div>
						<div class="form-group">
							<input type="text" class="form-control" name="first_name" placeholder="Nombres" value="{{ $data['user']['first_name'] }}">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="last_name" placeholder="Apellidos" value="{{ $data['user']['last_name'] }}">
						</div>
						<div class="form-group" style="position:relative;">
							<input type="text" class="form-control" name="phone" placeholder="Teléfono celular" maxlength="10" value="{{ $data['user']['phone'] }}" @if (!empty($data['user']['phone'])) disabled="disabled" @endif>
							<img class="ok-cell @if(!$data['user']['phone_validated_date']) hide @endif" src="{{ asset_url() }}/img/OkCell.png" align="right" style="position:absolute;top: 20px; right:0;">

						</div>
						@if (empty($data['user']['email']))
						<div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Contraseña" name="password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Confirmar contraseña" name="password_confirmation">
                                        </div>
                                    @endif
                                    @if(!$data['user']['phone_validated_date'])
                                        <br>
                                        <div class="alert alert-success alert-dismissable unseen" id="phone_validation_success"></div>
                                        <div class="alert alert-danger alert-dismissable unseen" id="phone_validation_alert"></div>
                                        <div class="phone_validation">
                                            <div class="col-md-2 form-group">
                                                <img src="{{ asset_url() }}/img/iconCel.png" width="100"/>
                                            </div>
                                            <div class="col-md-10 form-group">
                                                <input type="hidden" value="{{ $data['user']['phone'] }}" placeholder="Número celular" id="phone_number" class="form-control"/>
                                                <span class="text-right">Valida tu número de celular.<a href="javascript:;" onclick="smsLogin();" style="color:#1385b2;font-weight: bold;"> Validar número</a></span>
                                            </div>
                                        </div>
                                    @endif
                                    <br>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-info btnacc">Guardar</button>
                                    </div>
                                </form>
                                <br>

                                @if (!empty($data['user']['email']))
                                    <h3 class="muted">INFORMACIÓN DE LA CUENTA</h3>
                                    <h3 class="muted">
                                        <hr>
                                    </h3>
                                    <div class="clearfix acc-info">
                                        <span class="title">Email</span>
                                        <span class="value">{{ $data['user']['email'] }}</span>
                                    </div>
                                    <br>
                                    <div class="clearfix acc-info">
                                        <span class="title">Clave</span>
                                        <span class="value">*****</span>
                                        <a class="btn-edit-password btnacc bt-info" href="#" data-toggle="modal" data-target="#edit-pwd-modal">Cambiar</a>
                                    </div>
                                @endif
                                <br><br><br><br>
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12 right">
                                <h3 class="muted">VINCULACIÓN FACEBOOK</h3>
                                <h3 class="muted">
                                    <hr>
                                </h3>
                                <br>
                                @if( !$data['user']['fb_id'] )
                                    <p>Vincula tu cuenta de Facebook con tu cuenta de merqueo.com para facilitar el ingreso.</p>
                                    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>
                                    <div id="status"></div>
                                @else
                                    <div align="center"><img src="{{ asset_url() }}img/success.png" width="40"><br><br>Ya estas conectado con Facebook.</div>
                                @endif

                                <br><br><br>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="add">
                            <br>
                            <a class="btn btn-large btn-default add" href="#" data-toggle="modal"
                               data-target="#add-address-modal"><i class="icon-plus"></i>Añadir dirección</a>
                            <div id="no-more-tables">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="numeric">Nombre</th>
                                        <th class="numeric">Dirección</th>
                                        <th class="numeric">Complemento</th>
                                        <th class="numeric">Ciudad</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($data['address']))
                                        @foreach($data['address'] as $address )
                                            <tr>
                                                <td data-title="Label">{{ $address['label'] }}</td>
                                                <td data-title="Address">{{ $address['address'] }}</td>
                                                <td data-title="Address Further">{{ $address['address_further'] }}</td>
                                                <td data-title="City">{{ $address['city'] }}</td>
                                                <td><a href="#" class="btn btn-info" data-toggle="modal" data-target="#edit-address-modal"
                                                       onclick="edit_address({{ $address['id'] }},'{{ $address['label'] }}','{{ $address['address_1'] }}|{{ $address['address_2'] }}|{{ $address['address_3'] }}|{{ $address['address_4'] }}','{{ $address['address_further'] }}','{{ $address['neighborhood'] }}','{{ $address['is_south_location'] }}','{{ $address['city_id'] }}')">
                                                        <i class="glyphicon glyphicon-pencil"></i>&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <a class="btn btn-info" onclick="return confirm('¿Estas seguro que deseas eliminar esta dirección?')" href="{{ web_url() }}/user/address/delete/{{ $address['id'] }}">
                                                        <i class="glyphicon glyphicon-remove"></i>&nbsp;Eliminar</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" align="center">No hay direcciones.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="pay">
                            <br>
                            <a class="btn btn-large btn-default add" href="#" data-toggle="modal"
                               data-target="#add-card-modal"><i class="icon-plus"></i>Añadir Tarjeta de Crédito</a>
                            <br>

                            <div id="no-more-tables">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="numeric">Método</th>
                                        <th class="numeric">Tipo</th>
                                        <th class="numeric">Número</th>
                                        <th class="numeric">Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($data['credit_cards']))
                                        @foreach($data['credit_cards'] as $credit_card )
                                            <tr>
                                                <td data-title="Method"><img src="{{ web_url() }}/assets/img/credit_card.png">&nbsp;&nbsp;&nbsp;Tarjeta de Crédito</td>
                                                <td data-title="Type">{{ $credit_card->type }}</td>
                                                <td data-title="Number">**** {{ $credit_card->last_four }}</td>
                                                <td data-title="Action"><a class="btn btn-info" onclick="return confirm('¿Estas seguro que deseas eliminar esta tarjeta de crédito?')" href="{{ web_url() }}/user/card/delete/{{ $credit_card->id }}">Eliminar</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" align="center">No hay métodos de pago.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="ord">
                            <br>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="numeric" style="text-align: center">Nro. Pedido</th>
                                    <th class="numeric" style="text-align: center">Fecha</th>
                                    <th class="numeric" style="text-align: center">Estado</th>
                                    <th class="numeric" style="text-align: center">Tienda</th>
                                    <th class="numeric" style="text-align: center">Cantidad Productos</th>
                                    <th class="numeric" style="text-align: center">Total (*)</th>
                                    <th></th>
                                    <!--<th></th>-->
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($data['orders']))
                                    @foreach($data['orders'] as $order )
                                        <tr>
                                            <td data-title="Label" align="center">{{ $order['reference'] }}</td>
                                            <td data-title="Label" align="center">{{ date('d/m/Y', strtotime($order['date'])) }}</td>
                                            <td data-title="Label" align="center">{{ $status[$order['status']] }}</td>
                                            <td data-title="Label" align="center">{{ $order['store_name'] }}</td>
                                            <td data-title="Label" align="center">{{ $order['total_products'] }}</td>
                                            <td data-title="Label" align="right">$ {{ $order['total_amount'] }}</td>
                                            <td align="center">
                                                <a href="{{ web_url() }}/user/order/{{ $order['reference'] }}" target="_blank" class="order-details" data-fancybox-type="iframe">
                                                <i class="glyphicon glyphicon-check"></i> Ver Detalles</a>
                                            </td>
                                            <td align="center">
                                                @if($order['status'] == 'Delivered' && $order['invoice_number'] != "")
                                                    <a href="{{ route('frontUser.UserInvoice', ['reference' => $order['reference']]) }}" target="_blank" class="invoice">
                                                    <i class="glyphicon glyphicon-check"></i> Ver Factura</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" align="center">No hay pedidos.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <br>
                            (*) El precio final esta sujeto a cambios
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="ref">
                            @include('account.referred')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('layout_footer')

    @if (!empty($data['user']['email']))
        <!-- edit password start -->
            <div class="modal fade account_user_modal" id="edit-pwd-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Editar Contraseña</h3>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal modal-form password" id="edit-account-form" method="post"
                                  action="{{ web_url() }}/user/update">
                                <div class="unseen alert alert-danger form-has-errors-password"></div>
                                <div class="form-group">
                                    <label class="col-sm-3">Nueva contraseña</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="Nueva Contraseña" name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3">Confirmar nueva contraseña</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="Confirmar nueva contraseña" name="password_confirmation">
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <input class="btn btn-cancel-modal btn-subtle" data-dismiss="modal" type="reset" value="Cancelar">
                                    <input class="btn btn-info" type="submit" value="Guardar Información de Cuenta">
                                </div>
                            </form>
                            <br><br><br>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- edit password end -->
    @endif

    <!-- add address start -->
        <div class="modal fade account_user_modal" id="add-address-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3>Añadir Dirección</h3>
                    </div>
                    <div class="modal-body">
                        <form class="address-add" method="post" action="{{ web_url() }}/user/address">
                            <div class="unseen alert alert-danger form-has-errors-address-add"></div>
                            <div class="form-group">
                                <label for="lab">Nombre:</label>
                                <input type="text" class="form-control" placeholder="Nombre" name="label">
                            </div>
                            <div class="form-group clearfix">
                                <label for="add" style="display: block">Dirección:</label>
                                <div class="col-xs-3" style="padding-left: 0">
                                    <select name="dir[]" class="form-control">
                                        <option value="Calle">Calle</option>
                                        <option value="Carrera">Carrera</option>
                                        <option value="Avenida">Avenida</option>
                                        <option value="Avenida Carrera">Avenida Carrera</option>
                                        <option value="Avenida Calle">Avenida Calle</option>
                                        <option value="Circular">Circular</option>
                                        <option value="Circunvalar">Circunvalar</option>
                                        <option value="Diagonal">Diagonal</option>
                                        <option value="Manzana">Manzana</option>
                                        <option value="Transversal">Transversal</option>
                                        <option value="Vía">Vía</option>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="text" name="dir[]" class="form-control" placeholder="127">
                                </div>
                                <label for="dir3" class="col-xs-1">#</label>
                                <div class="col-xs-2">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="hidden" value="#" name="dir[]">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="text" name="dir[]" class="form-control" placeholder="7">
                                </div>
                                <label for="dir4" class="col-xs-1">-</label>
                                <div class="col-xs-2" style="width: 20%;">
                                    <input type="hidden" value="-" name="dir[]">
                                    <input type="text" name="dir[]" class="form-control" placeholder="12">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-6 no-padding">
                                    <input type="text" class="form-control" name="address_further" placeholder="Ej: Apartamento 501, Torre 2" maxlength="100">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="address_neighborhood" placeholder="Barrio" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="lab">Ciudad</label>
                                <select type="text" class="form-control required" name="city_id">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group clearfix unseen">
                                <label for="lab">¿Tu dirección se encuentra en el sur de la ciudad?</label>
                                <select type="text" class="form-control required" name="address_is_south_location">
                                    <option value="1">Sí</option>
                                    <option value="0" selected="selected">No</option>
                                </select>
                            </div>
                            <input type="hidden" name="address">
                            <button type="submit" class="btn btn-info">Guardar Dirección</button>
                            <br><br>
                        </form>
                        <br>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--add address end -->

        <!-- edit address start -->
        <div class="modal fade account_user_modal" id="edit-address-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Actualizar Dirección</h3>
                    </div>
                    <div class="modal-body">
                        <form class="address-edit" method="post" action="{{ web_url() }}/user/address/update">
                            <div class="unseen alert alert-danger form-has-errors-address-edit"></div>
                            <div class="form-group">
                                <label for="lab">Nombre:</label>
                                <input type="text" id='edit-modal-label' class="form-control" placeholder="Nombre"
                                       name="label">
                            </div>
                            <div class="form-group clearfix">
                                <label for="add" style="display: block">Dirección:</label>
                                <div class="col-xs-3" style="padding-left: 0">
                                    <select id="edit-modal-address-1" name="dir[]" class="form-control">
                                        <option value="Calle">Calle</option>
                                        <option value="Carrera">Carrera</option>
                                        <option value="Avenida">Avenida</option>
                                        <option value="Avenida Carrera">Avenida Carrera</option>
                                        <option value="Avenida Calle">Avenida Calle</option>
                                        <option value="Circular">Circular</option>
                                        <option value="Circunvalar">Circunvalar</option>
                                        <option value="Diagonal">Diagonal</option>
                                        <option value="Manzana">Manzana</option>
                                        <option value="Transversal">Transversal</option>
                                        <option value="Vía">Vía</option>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="text" id="edit-modal-address-2" name="dir[]" class="form-control" placeholder="127">
                                </div>
                                <label for="dir3" class="col-xs-1">#</label>
                                <div class="col-xs-2">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="hidden" value="#" name="dir[]">
                                    <input type="hidden" value=" " name="dir[]">
                                    <input type="text" id="edit-modal-address-3" name="dir[]" class="form-control" placeholder="7">
                                </div>
                                <label for="dir4" class="col-xs-1">-</label>
                                <div class="col-xs-2">
                                    <input type="hidden" value="-" name="dir[]">
                                    <input type="text" id="edit-modal-address-4" name="dir[]" class="form-control" placeholder="12">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-6 no-padding">
                                    <input type="text" id="edit-modal-address-further" class="form-control" name="address_further" placeholder="Ej: Apartamento 501, Torre 2" maxlength="100">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="edit-modal-neighborhood" class="form-control" name="address_neighborhood" placeholder="Barrio" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="lab">Ciudad</label>
                                <select type="text" id="edit-modal-city" class="form-control required" name="city_id">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group clearfix unseen">
                                <label for="lab">¿Tu dirección se encuentra en el sur de la ciudad?</label>
                                <select type="text" id="edit-modal-is-south-location" class="form-control required" name="address_is_south_location">
                                    <option value="1">Sí</option>
                                    <option value="0" selected="selected">No</option>
                                </select>
                            </div>
                            <input type="hidden" name="address">
                            <input type="hidden" name="address_id" id="edit-modal-address-id">
                            <button type="submit" class="btn btn-info">Guardar Dirección</button>
                            <br><br>
                        </form>
                        <br>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--edit address end -->

        <!-- add card start -->
        <div class="modal fade account_user_modal" id="add-card-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3>Añadir Tarjeta de Crédito</h3>
                    </div>
                    <div class="modal-body">
                        <div class="unseen alert alert-danger form-has-errors-card-add"></div>
                        <div class="col-lg-12" id="add-card">
                            <form action="{{ web_url() }}/user/card" method="POST" class="card-add form-horizontal">
                                <div class="form-group">
                                    Solo aceptamos tarjetas de crédito tipo {{ $config['credit_cards_message'] }}.
                                    <br><br><img src="{{ asset_url() }}img/payment_methods.png" width="150"></p>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <label>Nombre (tal como aparece en tu tarjeta)</label>
                                        <input type="text" class="form-control onlyLetters cc required" placeholder="Nombre" name="name_cc" maxlength="24">
                                    </div>
                                    <label class="label-complement">&nbsp;</label>
                                    <div class="form-group col-xs-6">
                                        <label>Número de tarjeta</label>
                                        <input type="text" class="form-control onlyNumbers cc required" placeholder="Número de tarjeta" name="number_cc" maxlength="16">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <label>Código de seguridad</label>
                                        <input type="text" class="form-control onlyNumbers cc required" placeholder="Código de seguridad" name="code_cc" maxlength="3">
                                    </div>
                                    <label class="label-complement">&nbsp;</label>
                                    <div class="form-group col-xs-6">
                                        <label>Ciudad</label>
                                        <input type="text" autocomplete="off" class="form-control onlyLetters cc required" placeholder="Ciudad" name="city_cc" maxlength="24">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <label>Fecha de expiración</label>
                                        <select type="text" class="form-control cc required" id="expiration_year_cc" name="expiration_year_cc">
                                            <option value="" selected="selected">Año</option>
                                            @foreach($dates_cc['years'] as $year)
                                                <option value="{{ $year }}">{{ $year }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="label-complement">&nbsp;</label>
                                    <div class="form-group col-xs-6">
                                        <label>&nbsp;</label>
                                        <select type="text" class="form-control cc required" id="expiration_month_cc" name="expiration_month_cc">
                                            <option value="" selected="selected">Mes</option>
                                            @foreach($dates_cc['months'] as $month)
                                                <option value="{{ $month }}">{{ $month }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <label>Tipo de documento</label>
                                        <select type="text" class="form-control cc required" id="document_type_cc" name="document_type_cc">
                                            <option value="" selected="selected">Tipo de documento</option>
                                            <option value="CC">Cédula de ciudadania</option>
                                            <option value="CE">Cédula de extranjeria</option>
                                            <option value="NIT">Número de identificación tributario</option>
                                            <option value="PP">Pasaporte</option>
                                        </select>
                                    </div>
                                    <label class="label-complement">&nbsp;</label>
                                    <div class="form-group col-xs-6">
                                        <label>Número de documento</label>
                                        <input type="text" autocomplete="off" class="form-control onlyNumbers cc required" placeholder="Número de documento" id="document_number_cc" name="document_number_cc" maxlength="16">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <label>Teléfono fijo o celular</label>
                                        <input type="text" autocomplete="off" class="form-control onlyNumbers cc required" placeholder="Teléfono fijo o celular" id="phone_cc" name="phone_cc" maxlength="10">
                                    </div>
                                    <label class="label-complement">&nbsp;</label>
                                    <div class="form-group col-xs-6">
                                        <label>Correo electrónico</label>
                                        <input type="text" autocomplete="off" class="form-control cc required" placeholder="Correo electrónico" id="email_cc" name="email_cc" maxlength="30" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <label>Dirección de facturación</label>
                                        <input type="text" autocomplete="off" class="form-control cc required" placeholder="Dirección" id="address_cc" name="address_cc" maxlength="50">
                                    </div>
                                    <label class="label-complement">&nbsp;</label>
                                    <div class="form-group col-xs-6">
                                        <label>&nbsp;</label>
                                        <input type="text" autocomplete="off" class="form-control cc" placeholder="Complemento dirección" id="address_further_cc" name="address_further_cc" maxlength="30">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info btn-lg btn-add-card">Guardar Tarjeta</button> <span class="loading unseen">&nbsp;&nbsp;&nbsp;<imgsrc="{{ asset_url() }}/img/loading.gif"/></span>
                                </div>
                            </form>
                        </div>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <!-- update invoice data start -->
        <div class="modal fade account_user_modal" id="update_invoice_data">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h3>Actualizar Datos de Facturación</h3>
                    </div>
                    <div class="modal-body">
                        <form id="invoice-data-form" method="post" action="{{ route('frontUser.updateInvoice') }}">
                            <div class="form-group">
                                <label for="identity_type">Tipo de Documento</label>
                                <select id="identity_type" name="identity_type" class="form-control">
                                    <option value="Cédula">Cédula</option>
                                    <option value="NIT">NIT</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="identity_number">Número de Identificación</label>
                                <input type="text" id="identity_number" class="form-control" placeholder="Número de identificación" name="identity_number">
                            </div>
                            <div class="form-group">
                                <label for="identity_number">Razón Social o Nombre Completo</label>
                                <input type="text" id="business_name" class="form-control" placeholder="Razón Social o Nombre Completo" name="business_name">
                            </div>
                            <button type="submit" class="btn btn-info">Guardar</button>
                            <br><br>
                        </form>
                        <br>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--update invoice data end -->

        <!--add card end -->
        <script type="text/javascript">
            function edit_address(id, label, address, address_further, neighborhood, is_south_location, city_id) {
                var address = address.split('|');
                $("#edit-modal-address-id").val(id);
                $("#edit-modal-address-1").val(address[0]);
                $("#edit-modal-address-2").val(address[1]);
                $("#edit-modal-address-3").val(address[2]);
                $("#edit-modal-address-4").val(address[3]);
                $("#edit-modal-address-further").val(address_further);
                $("#edit-modal-neighborhood").val(neighborhood);
                $("#edit-modal-is-south-location").val(is_south_location);
                $("#edit-modal-label").val(label);
                $("#edit-modal-city").val(city_id);
            }
        </script>

        <script type="text/javascript">

            $(document).ready(function () {
                $('#link').on('click', function () {
                    if ($('.share_link').hasClass('unseen'))
                        $('.share_link').show();
                    else $('.share_link').hide();
                });

                $('a[href="#ord"]').on('click', function (event) {
                    trackEvent('last_products_bought_viewed');
                });

                $('a[href="#ref"]').on('click', function () {
                    $(window).trigger('scroll');
                });

                $('.btn-shared-merqueo').on('click', function () {
                    trackEvent('referral_code_shared', {channel: $(this).prop('id'), screen: pageName()});
                });

                $(document).on('click', '.order-details', function () {
                    trackEvent('order_status_viewed', {screen: pageName()});
                });

                $("#invoice-data-form").validate({
                    rules: {
                        identity_number: "required",
                        business_name: "required"
                    }
                });

                $("#user-data-form").on('submit', function (e) {
                    e.preventDefault();
                    var error = false;
                    var form = this;

                    $('input[type!="hidden"]', this).each(function () {
                        if ($(this).val().length == 0 && $(this).is(':visible')) {
                            $(this).parent().addClass('has-error');
                            error = "Debes ingresar todos los campos.";
                        } else $(this).parent().removeClass('has-error');
                    });

                    if (!error && $('input[name="phone"]', this).length
                        && $('input[name="phone"]', this).val() != '' && $('input[name="phone"]', this).val().length < 10) {
                        error = "El número celular debe ser mínimo de 10 digitos.";
                        $('input[name="phone"]').parent().addClass('has-error');
                    }

                    if (!error && $('input[name="email"]', this).length && $('input[name="email"]').val() != '' && !validateEmail($('input[name="email"]').val())) {
                        error = "El formato del email ingresado no es valido.";
                        $('input[name="email"]').parent().addClass('has-error');
                    }

                    if (!error & $('input[name="password"]', this).length
                        && $('input[name="password"]').val() != $('input[name="password_confirmation"]').val()) {
                        error = "Las contraseñas no coinciden.";
                        $('input[name="password_confirmation"]').parent().addClass('has-error');
                    }

                    if (error) {
                        $('.form-has-errors').html(error).fadeIn();
                        return false;
                    } else {
                        form.submit();
                        return true;
                    }
                });

                $('form.email').on('submit', function (e) {
                    e.preventDefault();
                    var error = false;
                    var form = this;

                    $('input[type!="hidden"]', this).each(function () {
                        if ($(this).val().length == 0 && $(this).is(':visible')) {
                            $(this).parent().addClass('has-error');
                            error = "Debes ingresar todos los campos";
                        } else $(this).parent().removeClass('has-error');
                    });

                    if ($('input[name="email"]', this).length
                        && $('input[name="email"]').val() != $('input[name="email_confirmation"]').val()) {
                        error = "El email ingresado no coincide con la confirmación";
                        $('input[name="email_confirmation"]').parent().addClass('has-error');
                    }

                    if (error) {
                        $('.form-has-errors-email').html(error).fadeIn();
                        return false;
                    } else {
                        form.submit();
                        return true;
                    }
                });

                $('form.password').on('submit', function (e) {
                    e.preventDefault();
                    var error = false;
                    var form = this;

                    $('input[type!="hidden"]', this).each(function () {
                        if ($(this).val().length == 0 && $(this).is(':visible')) {
                            $(this).parent().addClass('has-error');
                            error = "Debes ingresar todos los campos";
                        } else $(this).parent().removeClass('has-error');
                    });

                    if ($('input[name="password"]', this).length
                        && $('input[name="password"]').val() != $('input[name="password_confirmation"]').val()) {
                        error = "Las contraseñas no coinciden";
                        $('input[name="password_confirmation"]').parent().addClass('has-error');
                    }

                    if (error) {
                        $('.form-has-errors-password').html(error).fadeIn();
                        return false;
                    } else {
                        form.submit();
                        return true;
                    }
                });

                $('form.address-add').on('submit', function (e) {
                    e.preventDefault();
                    var error = false;
                    var form = this;

                    $('input[type!="hidden"]', this).each(function () {
                        if ($(this).val().length == 0 && $(this).is(':visible') && $(this).attr('name') != 'address_further') {
                            $(this).parent().addClass('has-error');
                            error = "Debes ingresar todos los campos";
                        } else $(this).parent().removeClass('has-error');
                    });

                    if (error) {
                        $('.form-has-errors-address-add').html(error).fadeIn();
                        return false;
                    } else {
                        var address = '';
                        $('*[name="dir[]"]', this).each(function () {
                            address += $(this).val();
                        });
                        $('input[name="address"]', this).val(address);
                        form.submit();
                        return true;
                    }
                });

                $('form.address-edit').on('submit', function (e) {
                    e.preventDefault();
                    var error = false;
                    var form = this;

                    $('input[type!="hidden"]', this).each(function () {
                        if ($(this).val().length == 0 && $(this).is(':visible') && $(this).attr('name') != 'address_further') {
                            $(this).parent().addClass('has-error');
                            error = "Debes ingresar todos los campos.";
                        } else $(this).parent().removeClass('has-error');
                    });

                    if (error) {
                        $('.form-has-errors-address-edit').html(error).fadeIn();
                        return false;
                    } else {
                        var address = '';
                        $('*[name="dir[]"]', this).each(function () {
                            address += $(this).val();
                        });
                        $('input[name="address"]', this).val(address);
                        form.submit();
                        return true;
                    }
                });

                $('form.card-add').on('submit', function (e) {
                    $('.btn-add-card').attr("disabled", true);
                    e.preventDefault();
                    var error = false;
                    var form = this;

                    $('input[type!="hidden"], select', this).each(function () {
                        if (!$(this).val().length && $(this).is(':visible') && $(this).hasClass('required')) {
                            $(this).parent().addClass('has-error');
                            error = "Debes ingresar todos los campos";
                        } else $(this).parent().removeClass('has-error');
                    });

                    if (!error) {
                        if ($('input[name="email_cc"]').val() != '' && !validateEmail($('input[name="email_cc"]').val())) {
                            error = "El formato del email ingresado no es valido.";
                            $('input[name="email_cc"]').parent().addClass('has-error');
                        } else $('input[name="email_cc"]').parent().removeClass('has-error');

                        if (!validate_credit_card($('input[name="number_cc"]').val())) {
                            error = "El número de la tarjeta de crédito no es valido.";
                            $('input[name="number_cc"]').parent().addClass('has-error');
                        } else $('input[name="number_cc"]').parent().removeClass('has-error');
                    }

                    if (error) {
                        $('.form-has-errors-card-add').html(error).fadeIn();
                        $('.btn-add-card').removeAttr("disabled");
                        return false;
                    } else {
                        $('.loading').show();
                        form.submit();
                        return true;
                    }
                });

                $('.order-details').fancybox({
                    maxWidth: 1200,
                    maxHeight: 800,
                    fitToView: false,
                    width: '80%',
                    height: '80%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    beforeClose: function () {
                        var $iframe = $('.fancybox-iframe');
                        if ($('.added-to-cart', $iframe.contents()).length)
                            window.location.reload();
                    },
                });

                var checked = false;
                $(window).on('scroll', function () {
                    var element = $('.promo-code-referral');
                    if (isElementIntoView(element) && !checked) {
                        checked = true;
                        trackEvent('referral_code_viewed');
                    }
                });
            });

            // initialize Account Kit with CSRF protection
            AccountKit_OnInteractive = function () {
                AccountKit.init(
                    {
                        appId: "{{ Config::get("app.facebook_app_id") }}",
                        state: "{{ Cache::get('js_version_number') }}",
                        version: "v1.0",
                        fbAppEventsEnabled: true
                    }
                );
            };

            // login callback
            function loginCallback(response) {
                if (response.status === "PARTIALLY_AUTHENTICATED") {
                    var code = response.code;
                    var csrf = response.state;
                    // Send code to server to exchange for access token
                    $.ajax({
                        url: '{{ route('frontUser.cellphoneValidation') }}',
                        type: 'POST',
                        data: {code: code},
                        dataType: 'json'
                    })
                        .done(function (response) {
                            if (response.status) {
                                $('.phone_validation').hide();
                                $('.ok-cell').removeClass('hide');
                                $('#phone_validation_success').html(response.message).show();
                            } else {
                                $('#phone_validation_alert').html(response.message).show();
                            }
                        });
                }
                else if (response.status === "NOT_AUTHENTICATED") {
                    //console.log(response);
                }
                else if (response.status === "BAD_PARAMS") {
                    //console.log(response);
                }
            }

            // phone form submission handler
            function smsLogin() {
                var phoneNumber = document.getElementById("phone_number").value;
                AccountKit.login(
                    'PHONE',
                    {countryCode: '+57', phoneNumber: phoneNumber},
                    loginCallback
                );
            }

        </script>

@stop
