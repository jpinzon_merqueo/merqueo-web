<!doctype html>
<html lang="es">
<head>
    <base href="/">
    <meta name="robots" content="noindex,nofollow">
    <meta charset="utf-8">
    <title>@yield('title', 'El supermercado del ahorro. Entrega a domicilio - Merqueo.com')</title>
    <meta name="description" content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')">
    <meta name="keywords" content="@yield('keywords', 'supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express')">
    <meta name="author" content="Merqueo">
    <meta name="viewport" content="width=824, initial-scale=0"/>
    <meta name="apple-itunes-app" content="app-id=1080127941">
    <meta name="google-play-app" content="app-id=com.merqueo">

    <meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Merqueo.com" />
    <meta property="og:url" content="{{ Request::url() }}" />
    @if (Request::segment(1) == 'registro')
    <meta property="og:title" content="@if($metadata['code_referred_title']) {{ $metadata['code_referred_title'] }} @else {{ Config::get('app.referred.share_title') }} @endif" />
    <meta property="og:description" content="@if($metadata['code_referred_description']) {{ $metadata['code_referred_description'] }} @else {{ Config::get('app.referred.share_description') }} @endif" />
    <meta property="og:image" content="{{ Config::get('app.referred.share_image_url') }}" />
    @else
    <meta property="og:title" content="@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')" />
    <meta property="og:description" content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')" />
    <meta property="og:image" content="{{ asset_url() }}img/fb_logo.jpg" />
    @endif

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css?v={{Cache::get('js_version_number')}}">
    <link rel="stylesheet" href="{{asset_url()}}/lib/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="{{asset_url()}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset_url()}}/css/style.css">
    <link rel="stylesheet" href="{{asset_url()}}/css/base.css?v={{Cache::get('js_version_number')}}">
    <link rel="stylesheet" href="{{asset_url()}}/css/cities.css">
    <link rel="stylesheet" href="{{asset_url()}}css/smart-app-banner.css?v={{Cache::get('js_version_number')}}">

    @include('common/meta_icons')

    <link rel="canonical" href="{{ Request::url() }}">

    <script>
        var fb_app_id = '{{ Config::get("app.facebook_app_id") }}';
        var web_url = '{{ web_url() }}';
        var cart_id = '{{ Session::get("cart_id") }}';
        var store_id = '';
        var google_api_key = '{{ Config::get("app.google_api_key") }}';
    </script>

    <script type="text/javascript" src="{{ asset_url() }}js/Leanplum_JavaScript-1.2.4/leanplum.min.js"></script>
    @include('common/analytics')

    <script type="text/javascript" src="{{asset_url()}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/material.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.lazyload.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.elevatezoom.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/geocode.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{asset_url()}}js/general.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/checkout.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{asset_url()}}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/smart-app-banner.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/bar-new-web.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/ui.js?v={{ Cache::get('js_version_number') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://sdk.accountkit.com/es_ES/sdk.js"></script>
</head>

<body class="home_init">
    <script>
        banner = undefined;

        function run(force) {
            var n = document.querySelector('.smartbanner');
            if (n) {
                n.parentNode.removeChild(n);
            }
            new SmartBanner({
                daysHidden: 15,
                daysReminder: 90,
                appStoreLanguage: 'us',
                title: 'Merqueo',
                author: 'Merqueo SAS',
                button: 'Descargar App',
                store: {
                    ios: 'tu experiencia',
                    android: 'tu experiencia',
                    windows: 'tu experiencia'
                },
                price: {
                    ios: 'Mejora',
                    android: 'Mejora',
                    windows: 'Mejora'
                },
                force: force
            });
        };
        var device = navigator.userAgent;
        if (device.match(/Android/i)) {
            run('android');
        }
    </script>
    <div class="col-md-12 col-xs-12 contini_home">
        <header class="clearfix">
            <div class="col-md-12 col-xs-12 header_logo clearfix">
                <nav class="col-md-12 col-xs-12 clearfix">
                    <div class="col-md-5 col-xs-2 col-sm-2">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset_url() }}img/pin_gre.png" alt="">
                            @if (!Session::get('city_id'))
                              Cambiar Ciudad
                             @else
                              @foreach($footer['cities'] as $city)
                                @if (Session::get('city_id') == $city->id)
                                  {{ $city->city }}
                                  <b class="caret"></b>
                                @endif
                              @endforeach
                             @endif
                        </a>
                        <ul class="dropdown-menu">
                            <form class="form-change-city">
                                <div class="ciudades">
                                  @foreach($footer['cities'] as $city)
                                        <li class="textCiudades" style="min-width:180px;">
                                      @if (Session::get('is_new') == 1)
                                          <a href="{{ route('frontStore.all_departments', ['city_slug' => $city->slug, 'store_slug' => 'super-merqueo']) }}">
                                      @else
                                        <a href="{{ route('frontStoreCity.index', ['city_slug' => $city->slug]) }}">
                                      @endif
                                         <input class="change-city" type="checkbox" id="{{ $city->id }}" name="city_id" value="{{ $city->id }}" @if (Session::get('city_id') == $city->id) checked="checked" @endif />
                                        <label for="{{ $city->slug }}">{{ $city->city }}</label>
                                      </a>
                                    </li>
                                  @endforeach
                                </div>
                            </form>
                        </ul>
                    </div>
                    <div class="col-md-5 col-xs-5 col-sm-5 log_gen">
                        <a class="logo-center" itemprop="brand" itemscope itemtype="http://schema.org/Brand" href="{{web_url()}}" id="logo_mer"><img src="{{ asset_url() }}img/Logo.svg" alt="logo" title="Merqueo.com"></a>
                    </div>
                    <div class="col-md-1 col-xs-1 col-sm-1 pull-right contentmenu_home">
                        @if (Auth::check())
                            <a href="#" class="mdl-navigation__link dropdown-toggle user-navbar" data-toggle="dropdown" role="button" aria-expanded="false">
                                <strong>{{Auth::user()->first_name}}</strong>
                                <b class="caret"></b>
                            </a>
                        @else
                            <a class="mdl-navigation__link ingress" data-toggle="modal" data-target="#login">Ingresar</a>

                        @endif @if (Auth::check())
                            <ul class="dropdown-menu dropdown-menuhome" role="menu">
                                <li><a href="{{web_url()}}/mi-cuenta"><i class="fa fa-user"></i>Mi Cuenta</a></li>
                                <li class="divider"></li>
                                <li><a href="{{web_url()}}/logout" class="logout"><i class="fa fa-power-off"></i>Cerrar sesión</a></li>
                            </ul>
                        @endif
                    </div>
                </nav>
            </div>
        </header>

        @yield('content')
    </div>

    @include('login_modal')

    <div id="coverage" class="unseen"><img src="{{ asset_url() }}img/coverage_map_{{$metadata['city_slug']}}.jpg" alt="merqueo-cobertura" width="740" /></div>
    <script>
        $(document).ready(function() {
            $('.form-change-city').change(function(e) {
                e.preventDefault();
            });

            $('.fancybox').fancybox({
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: true,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });

            $(window).resize(function() {
                var ancho = $( window ).width();
                var anchocont = $('.home_init .contentmenu_home').width();
                var anchodrop = $('.home_init .contentmenu_home .dropdown-menuhome').width();
                var valuecal = (anchocont - anchodrop) + 12;
                $('.home_init .contentmenu_home .dropdown-menuhome').css('margin-left', valuecal);
            });
        });
    </script>
    <div class="clearfix"></div>

</body>

</html>
