@if (strpos( Route::getCurrentRoute()->getPath(), 'checkout') === false)
    <div class="bar_store_advertising">
        <div class="items_bar_store">
            @if (Auth::check() && ($user_discounts['free_delivery_days'] || $user_discounts['free_delivery_next_order']))
                <div class="free-delivery-days">
                    @if ($user_discounts['free_delivery_days'] > 1)
                        Tienes <b>{{ $user_discounts['free_delivery_days'] }}</b> días de domicilio gratis
                    @else
                        @if ($user_discounts['free_delivery_days'] == 1)
                            Tienes domicilio gratis hasta el día de hoy
                        @else
                            @if ($user_discounts['free_delivery_next_order'])
                                Tienes domicilio gratis en tu próximo pedido.
                            @endif
                        @endif
                    @endif
                </div>
            @endif
            <div class="col-md-2 elementoslist shop_ico">
                <button type="button" class="mq-best-price-trigger" data-toggle="modal" data-target="#best-price-modal">
                    <img src="{{ asset_url() }}img/pesos.png"
                         alt="Mejor precio garantizado">Mejor precio
                    garantizado
                </button>
            </div>
            <div class="col-md-2 elementoslist shop_ico">
                <div class="imagenico col-sm-1">
                    <img src="{{ asset_url() }}img/ubicacion-act.svg" alt="Merqueo ubicación">
                </div>
                <div class="textico col-sm-10">
                    <h6 id="address-update-button">
						<span id="address-description">
							<b>
								@if (!empty(Session::get('address')->address_text))
                                    {{ Session::get('address')->address_text }}
                                @else
                                    Selecciona la dirección
                                @endif
							</b>
						</span>
                        <br>
                        <span>
							Cambiar dirección
							<i class="fa fa-angle-down"></i>
						</span>
                    </h6>
                </div>
            </div>
            <div class="col-md-2 elementoslist shop_ico">
                <div class="imagenico col-md-4"><img src="{{ asset_url() }}img/seguro.png" alt="seguro"></div>
                <div class="textico col-md-8">
                    <h6>Compra<br>segura</h6>
                </div>
            </div>
            <div class="col-md-2 elementoslist shop_ico">
                <div class="imagenico col-md-4"><img src="{{ asset_url() }}img/90minutos.png" alt="90minutos"></div>
                <div class="textico col-md-8">
                    <h6>Entrega <br><b>mañana</b></h6>
                </div>
            </div>
            <div class="col-md-2 elementoslist shop_ico">
                <div class="imagenico col-md-4"><img src="{{ asset_url() }}img/sinSalir.png" alt="casa"></div>
                <div class="textico col-md-8">
                    <h6>Sin salir <br>de casa</h6>
                </div>
            </div>

        </div>
    </div>
@endif