@extends('layout_page')

@section('extra_assets')
    <link rel="stylesheet" href="{{asset_url()}}css/register.css">
@stop

@section('content')

<section id="register_referred">

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="credits-gift-text" >
                    ¡{{ $firstName }} te regaló <strong>{{ currency_format(Config::get('app.referred.amount')) }}</strong> en cŕeditos para tu primer mercado en Merqueo!
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h2 class="redeem-credits-text" >
                    <strong class="emphasis-question">¿Cómo haces para redimirlos?</strong>
                </h2>
            </div>
        </div>

        <div class="row">
            <div  class="col-sm-12">
                <h2 class="redeem-credits-text" >
                    Descarga nuestra app en tu celular e ingresa el código <strong>{{ $referralCode }}</strong>
                    en el <span class="emphasis-text" >último paso del proceso de compra</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="img-container">
                    <img src="{{ asset_url() }}img/app_referred_register.png"></img>
                    <strong>{{ $referralCode }}</strong>
                </div>
            </div>
        </div>
        <div class="row apps">
            <div class="col-sm-3 col-sm-offset-3">
                <a href="https://itunes.apple.com/us/app/id1080127941" target="_blank">
                    <img src="{{ asset_url() }}img/appStore.png" alt="apple">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="https://play.google.com/store/apps/details?id=com.merqueo&amp;hl=es" target="_blank">
                    <img src="/assets/img/androidStore.png" alt="android">
                </a>
            </div>
        </div>
        <div class="row legal">
            <div class="col-sm-12">
                <a href="{{ web_url().'/'}}terminos" target="_blank">Términos y Condiciones</a>
            </div>
        </div>


        </div>
</section>

</body>

</html>

<script>
$(document).ready(function() {
    document.body.style.backgroundColor = "#f6f6f6";
});
</script>


@include('common.account_kit_validation')
@stop