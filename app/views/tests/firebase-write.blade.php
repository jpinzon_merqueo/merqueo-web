<html>
    <body>
    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js"></script>
    <script>
        window.onload = function(){
            const config = {
                apiKey: 'AIzaSyACjspv7SPkKjhE0ywkxBd1HOpl82UX1T8',
                databaseURL: 'https://merqueo-dev.firebaseio.com',
            }

            let token = ''
            firebase.initializeApp(config);
            const database = firebase.database()
            const appAuth = firebase.auth()
            const ref = database.ref()
            const orders = ref.child('orders')

            auth()
                .then(() => write(`Other items: ${Math.random() * 10}`))
                .catch(console.error)

            ref.on('value', snap => console.log(snap.val()))

            function read() {
                return database
                    .ref('/orders')
                    .once('value')
                    .then(console.log);
            }

            function auth(callback){
                return makeRequestP('/api/transporter/1.0/token')
                    .then(result => {
                        token = result.message.token
                        return appAuth.signInWithCustomToken(token)
                    })
                    .then(callback)
                    .catch(console.error)
            }

            function write(myValue) {
                let value = {}
                const newKey = orders.push().key
                value[`/orders/${newKey}`] = {state: myValue}
                return database.ref().update(value)
            }

            function makeRequestP(url) {
                return new Promise((fulfill, reject) => makeRequest(url, fulfill, reject))
            }

            function makeRequest(url, success, error) {
                const httpRequest = new XMLHttpRequest();
                httpRequest.onreadystatechange = () => {
                    if (httpRequest.readyState === XMLHttpRequest.DONE) {
                        const result = JSON.parse(httpRequest.responseText)
                        httpRequest.status === 200
                            ? success(result)
                            : error(JSON.parse(httpRequest.responseText))
                    }
                }
                httpRequest.open('GET', url);
                httpRequest.send();
            }
        }
    </script>
    </body>
</html>
