<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <title>Pedido SOAT</title>
  <link href="{{ asset_url() }}/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css">
  <script type="text/javascript" src="{{ asset_url() }}/js/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/Leanplum_JavaScript-1.2.4/leanplum.min.js"></script>
  @include('common/analytics')
</head>

<body>
<div align="center" style="margin: 20px">
	<h3>Pedido {{ $data->reference }} - {{ $data->type }} - <b>{{ $data->cc_payment_status }}</b></h3>

	@if(Session::has('message'))
  	<div class="row">
		@if(Session::get('type') == 'success')
		<div class="alert alert-success alert-dismissable added-to-cart">
		    {{ Session::get('message') }}
		</div>
		@else
		<div class="alert alert-danger alert-dismissable">
		    {{ Session::get('message') }}
		</div>
		@endif
	</div>
	@endif

	<hr>

	<div>
		@if($data->cc_payment_status=='Aprobada')
			@if($data->type=='SOAT')
			<span>SOAT vigente desde:</span> 
			<h3><b>{{ date('d/m/Y',strtotime($data['date_start']) ) }} hasta {{ date('d/m/Y',strtotime($data['date_end']) ) }}</b></h3>
			@endif
		@endif
		<span>Metodo de Pago:</span>
		<h3><b> {{$data['cc_type']}}  **** {{$data['last_four']}}</b></h3>
		<span>Valor Pagado:</span>
		<h3><b>{{ currency_format($data['total']) }}</b></h3>
	</div>

	<!-- Si es SOAT -->
	@if($data->type=='SOAT')
	<table class="table table-striped table-condensed">
		<thead>
			<tr> 
				<th>Datos del vehiculo asegurado</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><b>Placa:</b>  {{$data['params']['plate']}}   </td>
				<td><b>Marca:</b>  {{$data['params']['runt']['vehicleBrand']}}   </td>
			</tr>
			<tr>
				<td><b>Clase Vehículo:</b> {{$data['params']['runt']['vehicleType']}}   </td>
				<td><b>Linea:</b> {{$data['params']['runt']['vehicleLine']}}   </td>
			</tr>
			<tr>
				<td><b>Modelo:</b> {{$data['params']['runt']['vehicleYear']}}   </td>
				<td> </td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th>Datos del tomador</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> <b>Nombres y Apellidos:</b> {{$data['params']['taker']['firstName']}} {{$data['params']['taker']['lastName']}} </td>
				<td><b>Telefono:</b> {{$data['params']['taker']['phone']}} </td>
			</tr>
			<tr>
				<td><b>Tipo de documento:</b> {{$data['params']['documentType']}}</td>
				<td><b>N° Documento:</b> {{$data['params']['document']}} </td>
			</tr>
			<tr>
				<!--<td><b>Ciudad de residencia:</b>  {{$data['params']['taker']['municipality']}}  </td>-->
				<td><b>Dirección:</b> {{$data['params']['taker']['address']}}</td>
			</tr>
			<tr>
				<td><b>Correo:</b> {{$data['params']['email']}} </td>
			</tr>
		</tbody>
	
	</table>
	@endif

	<!-- Si es recarga  -->
	@if($data->type=='Recarga')
	<table class="table table-striped table-condensed">
		<thead>
			<tr> 
				<th>Datos de Recarga</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><b>Operador:</b>  {{$data['params']['recharge']['operatorName'] }}   </td>
				<td><b>Numero :</b>  {{$data['params']['recharge']['destinationNumber']}}   </td>
			</tr>
		</tbody>
	</table>
	@endif

	
 </div>

</body>

</html>