<div class="modal fade " id="age-validation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center warning-title">!Sólo para adultos¡</h2>
                <br>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                      <img src="{{ asset_url() }}/img/icon-cigarrillos.png" />
                    </div>
                    <br/><br/>
                    <div class="col-xs-12 text-center warning-subtitle">
                      Debes tener +<strong>18 años</strong> para adquirir este producto
                      <br/><br/>
                    </div>
                    <div class="col-xs-12 text-center">
                      <button type="button" type="button" class="btn btn-warning btn-age-validation" data-dismiss="modal" data-val="1">Sí soy mayor de edad</button>
                      <button type="button" class="btn btn-warning btn-age-validation" data-dismiss="modal" data-val="0">No soy mayor de edad</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(function(){
    $('body').on('click','.btn-warning', function(e){
       e.preventDefault();
       $.warning = $(this).data('val');
       if($.warning == 1){
         localStorage.warning = 1;
         addItem(null, pageName());
       }
    });
  });
</script>