<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.js"></script>

<div class="modal fade gift-modal-style"
		id="gift-modal-container"
		tabindex="-1"
		role="dialog"
		aria-labelledby="myModalLabel"
		aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div id="bell"></div>
				<div class="close" data-dismiss="modal">
					<img src="/assets/img/gift/cerrar.png">	
				</div>
				<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
			</div>
			<div class="modal-body">
                <img src="{{ $campaign['image'] }}" alt="">	
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		$('#gift-modal-container').modal('show');
	});
    bodymovin.loadAnimation({
        container: document.getElementById('bell'), // Required
        path: '/assets/img/gift/bell.json', // Required
        renderer: 'svg', // Required
        loop: false, // Optional
        autoplay: true, // Optional
        name: 'Bell', // Name for future reference. Optional.
    });
</script>

<audio autoplay>
  <source src="/assets/img/gift/sound-bells.mp3" type="audio/mpeg">
</audio>
