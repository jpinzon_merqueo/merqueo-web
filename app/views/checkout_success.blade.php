@extends('layout_page')

@section('extra_assets')
	<title>Tu pedido</title>
	<meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
	<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,800" rel="stylesheet">
	<link href="{{ asset_url() }}css/order-success.css" rel="stylesheet">
@endsection

@section('content')
	<section>
		<div class="container">
			<div class="row">

				<div class="col-sm-7">
					<div class="miPedido_table">
						<p class="miPedidoTitle">Mi Pedido <br> <span>#{{ $order_data['reference'] }}</span></p>
						@if(Session::has('message') && !empty(Session::get('message')))
						<div class="alert alert-info alert-dismissable">
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                        @endif
						<div class="table-responsive">
							<table class="table">
								<tbody>
								<tr>
									<td scope="row" class="table-left">Estado</td>
									<td class="table-right recibido">Recibido</td>
								</tr>
								<tr>
									<td scope="row" class="table-left">Fecha de entrega</td>
									<td class="table-right">{{ $order_data['delivery_date'] }}</td>
								<tr>
									<td scope="row" class="table-left">Productos pedidos</td>
									<td class="table-right">{{ $order_data['products'] }}</td>
								</tr>
								<tr>
									<td scope="row" class="table-left">Dirección</td>
									<td class="table-right">{{ $order_data['address'] }}</td>
								</tr>
								<tr>
									<td scope="row" class="table-left">Método de pago</td>
									<td class="table-right">{{ $order_data['payment_method'] }}</td>
								</tr>
								<tr>
									<td scope="row" class="table-left totalText">Total</td>
									<td class="table-right totalText">$ {{ number_format($order_data['total'], 2, ',', '.') }}</td>
								</tr>
								</tbody>
							</table>
						</div>

						<div class="referidosBox">
							<p class="domicilioTitle">¿Quieres ganar créditos?</p>
							<p class="domicilioText">Invita a tus amigos a Merqueo y obtén {{ currency_format(Config::get('app.referred.amount')) }} en créditos por cada amigo que pida.</p>
							<input type="hidden" value="{{ web_url() }}/registro/{{ $user->referral_code }}" id="referral_code">
							<div class="row socialMedia">
								<div class="col-md-6 refSocial"><a class="socialLinks" id="facebook" href="https://www.facebook.com/dialog/share?app_id={{ Config::get('app.facebook_app_id') }}&display=popup&href={{ web_url() }}/registro/{{ $user->referral_code }}"><img src="{{ asset_url() }}img/order_success/facebook.png"/> <p>Facebook</p></a></div>
								<div class="col-md-6 refSocial"><a class="socialLinks" data-copy-to-clipboard="true" id="link"><img src="{{ asset_url() }}img/order_success/link.png"/> <p>Link</p></a></div>
							</div>
						</div>
						<p class="text-center"><br><button onclick="window.location = '{{ web_url() }}'" class="btn-tags-change">Ir al inicio</button></p>
					</div>
				</div>

				<div class="col-sm-5">
					<div class="imgPedidoRecibido">
						<img src="{{ asset_url() }}img/order_success/pedidoRecibido.svg" alt="">
						<p class="pedidoRecibidoText">Pedido Recibido</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
        function copyToClipboard(element) {
            var $temp = $('<input/>');
            $('body').append($temp);
            $temp.val($(element).val()).select();
            document.execCommand('copy');
            $('#link').html('<img src="{{ asset_url() }}img/order_success/link.png"/> <p>Link copiado compartelo</p>');
            $temp.remove();
        }

        $(document).ready(function () {
            $('body').on('click', '.socialLinks', function(event) {
				var anchor = $(this);
                event.preventDefault();
                trackEvent('referral_code_shared', {channel: anchor.prop('id'), screen: pageName()});
				if (anchor.data('copy-to-clipboard')) {
                    copyToClipboard('#referral_code');
                    return true;
				}

				window.open(anchor.prop('href'), '', 'width=700,height=500');
			});
		});
	</script>

	@if ($survey)
		@include('survey_modal')
        <script>
			$(document).ready(function() {
				$('#answers-user').modal('show');
			});
        </script>
    @endif

	@if($gift['win_gift'])
		@include('modal_campaign_gift', ['campaign' => $gift['campaign'] ])
	@endif

@stop
