<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('title', "El supermercado del ahorro. Entrega a domicilio en {$store->city_name} - Merqueo.com")</title>
    <meta name="robots" content="noindex,nofollow">
    <meta charset="utf-8">
    <meta name="description"
          content="@yield('description', 'El supermercado del ahorro. Entrega a domicilio - Merqueo.com')">
    <meta name="keywords"
          content="@yield('keywords', 'supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express')">
    <meta name="author" content="Merqueo">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=1224, initial-scale=0">
    <meta name="apple-itunes-app" content="app-id=1080127941">
    <meta name="google-play-app" content="app-id=com.merqueo">

    <meta property="fb:app_id" content="{{ Config::get("app.facebook_app_id") }}"/>
    <meta property="og:locale" content="es_ES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title', 'Merqueo.com - El supermercado del ahorro. Entrega a domicilio')"/>
    <meta property="og:description" content="@yield('description', 'El supermercado del ahorro. Pide ahora todo lo que necesites y recíbelo en la puerta de tu casa. Pide ahora! Puedes pagar con tarjeta o efectivo contra entrega.')"/>
    <meta property="og:url" content="{{ web_url() }}"/>
    <meta property="og:site_name" content="Merqueo.com"/>
    <meta property="og:image" content="{{ asset_url() }}img/fb_logo.jpg"/>

    {{--iOS meta tags--}}
    <meta property="al:ios:url" content="mrq://"/>
    <meta property="al:ios:1080127941" content="12345" />
    <meta property="al:ios:Merqueo - Mercado a Domicilio" content="Merqueo" />
    {{--iOS meta tags--}}

    <link href="//fonts.googleapis.com/css?family=Crete+Round:400,500,400italic|Open+Sans:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,500,700,400italic" rel="stylesheet">

    <link href="{{ asset_url() }}css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset_url() }}css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset_url() }}lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="{{ asset_url() }}css/nivo-slider.css" rel="stylesheet">
    <link href="{{ asset_url() }}css/default/default.css" rel="stylesheet"/>
    <link href="{{ asset_url() }}css/cities.css" rel="stylesheet">
    <link href="{{ asset_url() }}css/smart-app-banner.css" rel="stylesheet">
    <link href="{{ asset_url() }}css/base.css?v={{ Cache::get('js_version_number') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css?v={{Cache::get('js_version_number')}}">
    <link href="{{ asset_url() }}css/layout-footer.css" rel="stylesheet">

    @include('common/meta_icons')

    <link rel="canonical" href="{{ Request::url() }}">
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.min.js"></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
    <script>
        var content_ids;
        var fb_app_id = '{{ Config::get("app.facebook_app_id") }}';
        var web_url = '{{ web_url() }}';
        var cart_id = @if (Session::has('cart_id')) {{ Session::get('cart_id') }};
        @else 0;
                @endif
        var store_id = {{ $store->id }};
        var store_name = '{{ $store->name }}';
        var department_name = '{{ isset($currentDepartment) ? $currentDepartment->name : '' }}' || null;
        var shelf_name = '{{ isset($currentShelf) ? $currentShelf->name : '' }}' || null;
        var google_api_key = '{{ Config::get("app.google_api_key") }}';
        var dataLayer = [];
    </script>
    <script type="text/javascript" src="{{ asset_url() }}js/smart-app-banner.js"></script>

    <script>
        banner = undefined;

        function run(force) {
            var n = document.querySelector('.smartbanner');
            if (n) {
                n.parentNode.removeChild(n);
            }
            new SmartBanner({
                daysHidden: 15, // days to hide banner after close button is clicked (defaults to 15)
                daysReminder: 90, // days to hide banner after "VIEW" button is clicked (defaults to 90)
                appStoreLanguage: 'us', // language code for the App Store (defaults to user's browser language)
                title: 'Merqueo',
                author: 'Merqueo SAS',
                button: 'Descargar App',
                store: {
                    ios: 'tu experiencia',
                    android: 'tu experiencia',
                    windows: 'tu experiencia'
                },
                price: {
                    ios: 'Mejora',
                    android: 'Mejora',
                    windows: 'Mejora'
                },
                force: force
            });
        }

        var device = navigator.userAgent;
        //console.log(device);
        if (device.match(/Android/i)) {
            run('android');
        }

        $('.form-change-city').click(function (e) {
            e.preventDefault();
            banner = undefined;

            function run(force) {
                var n = document.querySelector('.smartbanner');
                if (n) {
                    n.parentNode.removeChild(n);
                }
                new SmartBanner({
                    daysHidden: 15, // days to hide banner after close button is clicked (defaults to 15)
                    daysReminder: 90, // days to hide banner after "VIEW" button is clicked (defaults to 90)
                    appStoreLanguage: 'us', // language code for the App Store (defaults to user's browser language)
                    title: 'Merqueo',
                    author: 'Merqueo SAS',
                    button: 'Descargar App',
                    store: {
                        ios: 'tu experiencia',
                        android: 'tu experiencia',
                        windows: 'tu experiencia'
                    },
                    price: {
                        ios: 'Mejora',
                        android: 'Mejora',
                        windows: 'Mejora'
                    },
                    force: force
                });
            }

            var device = navigator.userAgent;
            //console.log(device);
            if (device.match(/Android/i)) {
                run('android');
            }
        });

    </script>

    <script type="text/javascript" src="{{ asset_url() }}js/Leanplum_JavaScript-1.2.4/leanplum.min.js"></script>
    @include('common/analytics')

    <script type="text/javascript" src="{{ asset_url() }}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.lazyload.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.elevatezoom.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/bootstrap.min.js"></script>

    <script type="text/javascript" src="{{ asset_url() }}js/geocode.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/headroom.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/bar-new-web.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/ui.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/general.js"></script>
    <script type="text/javascript"
            src="{{ asset_url() }}js/checkout.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{ asset_url() }}/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}js/jquery.nivo.slider.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvibhMsWZ-mUXjLzzqktEbxTTVOs65FxI" async
            defer></script>

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id={{ Config::get('app.google_tag_manager.container_id') }}"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- REACT COMPONENTS -->
    <!-- Services APP Component -->
    <link rel="stylesheet" href="{{ asset_url() }}components/services.bundle.css">
    <!--<script type="text/javascript" src="{{ asset_url() }}components/vendors.js"></script>
    <script type="text/javascript" src="{{ asset_url() }}components/services.bundle.js"></script>-->
    <!-- END REACT COMPONENTS -->
</head>

<body class="layout_content">

<nav class="navbar navbar-default navbar-fixed-top mq-navbar" id="navbar-bctn">
    <div class="col-md-12 col-xs-12">
        <nav class="col-md-8 col-xs-12 cont_nav">
            <div class="col-md-2 col-xs-12">
                <a itemprop="brand" itemscope itemtype="http://schema.org/Brand"
                   href="{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store['slug'] }}" id="logo_mer"><img
                            src="{{ asset_url() }}img/Logo.svg" alt="logo" title="Merqueo.com"></a>
            </div>
            <div class="col-md-1">
                <a href="#" class="dropdown-toggle mq-city-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">
                    <img src="{{ asset_url() }}img/pin-new.png" alt="">
                    @foreach($footer['cities'] as $city)
                        @if (Session::get('city_id') == $city->id)
                            {{ $city->city }}
                            <b class="caret"></b>
                        @endif
                    @endforeach
                </a>
                <ul class="dropdown-menu">
                    <form class="form-change-city">
                        <div class="ciudades">
                            @foreach($footer['cities'] as $city)
                                <a class="link-change-city"
                                   @if (Session::get('city_id') != $city->id) href="{{ route('frontStoreCity.index', ['city_slug' => $city->slug])}}"
                                   @else href="javascript:void(0)" @endif>
                                    <li class="textCiudades" style="min-width:180px;">
                                        <input class="change-city" type="checkbox" id="{{ $city->id }}" name="city_id"
                                               value="{{ $city->id }}"
                                               @if (Session::get('city_id') == $city->id) checked="checked" @endif />
                                        <label for="{{ $city->slug }}">{{ $city->city }}</label>
                                    </li>
                                </a>
                            @endforeach
                        </div>
                    </form>
                </ul>
            </div>
            <div>
                <!--<a class="mdl-navigation__link fancybox" href="#coverage">Ver Cobertura</a>-->
            </div>
            <div></div>
            <div class="col-md-8 col-sm-8 col-xs-12 search_form">
                <form id="header-search-form" class="navbar-form navbar-left mq-form-search" role="search" autocomplete="off"
                      method="get"
                      action="{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store['slug'] }}/buscar">
                    <div class="input-group">
                        <input type="text" class="form-control" autocomplete="off"
                               placeholder="Buscar en {{ $store->name }}" name="q">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                        <div class="autocomplete"></div>
                    </div>
                </form>
            </div>

        </nav>
        <nav class="col-md-4 col-xs-12">
            <div class="col-sm-4 pl-0">
                <!-- SERVICES INTEGRATION REACT COMPONENT -->
                <div id="services-app"></div>
                <!-- END SERVICES INTEGRATION REACT COMPONENT -->
            </div>
            @if (strpos(Route::getCurrentRoute()->getPath(), 'checkout') === false)
                <div class="col-md-5 col-sm-5 col-xs-5 right cart-close mq-cart-close" id="cart-btn">
                    <div class="btn-group col-md-12">
                        <button type="button" class="col-md-12 btn lef btn-default">
                          <span class="count">
                            <img src="{{ asset_url() }}img/cart-new.png" alt="">
                            <span class="badge cart-total-quantity"></span>
                          </span>
                            <div id="checkout-label_cart" class="cart_summary hides">
                                <div id="title_summary">
                                    <p>Mi Pedido</p>
                                </div>
                                <div id="summary_value">
                                    $<span class="cart-total-amount"></span>
                                </div>
                            </div>
                        </button>
                        @if (isset($last_order) && $last_order)
                            <a href="{{ web_url() }}/user/order/{{ $last_order['reference'] }}" target="_blank"
                               class="reloadcart" data-fancybox-type="iframe"> + Ultimo pedido</a>
                        @endif
                    </div>
                </div>
            @endif
            <div class="contentmenu_home mq-content-menu-home">
                @if (Auth::check())
                    <a href="#" class="mdl-navigation__link dropdown-toggle user-navbar" data-toggle="dropdown"
                       role="button" aria-expanded="false">
                        <strong>{{Auth::user()->first_name}}</strong>
                        <b class="caret"></b>
                    </a>
                @else
                    <a class="mdl-navigation__link ingress" data-toggle="modal" data-target="#login">Ingresar</a>

                @endif @if (Auth::check())
                    <ul class="dropdown-menu dropdown-menuhome" role="menu">
                        <li><a href="{{web_url()}}/mi-cuenta"><i class="fa fa-user"></i>Mi Cuenta</a></li>
                        <li class="divider"></li>
                        <li><a href="{{web_url()}}/logout" class="logout"><i class="fa fa-power-off"></i>Cerrar
                                sesión</a></li>
                    </ul>
                @endif
            </div>
        </nav>
    </div>
</nav>

<div class="container-fluid" id="main-content-bctn">
    <div class="sub-menu">

    </div>
    <div class="page-content col-md-12 col-lg-12">
        <div class="row">
            @if (strpos(Route::getCurrentRoute()->getPath(), 'checkout') === false)
                <div class="col-md-2 col-lg-2 col-xs-12">
                    <div class="categories web container-fluid pinned">
                        <ul class="nav nav-pills nav-stacked col-xs-2" id="aside-departament">
                            @foreach ($departments as $department )
                                @if ($department->qty_products)
                                    <li>
                                        <a class="top-category link"
                                           href="{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}/{{ $department->slug }}"><span
                                                    class="click-link">{{ $department->name }}</span> <span
                                                    class="caret"></span></a>
                                        <ul class="sub-category">
                                            @foreach($shelves as $shelf)
                                                @if ($shelf->department_id == $department->id)
                                                    <a href="{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}/{{ $department->slug }}/{{ $shelf->slug }}">{{ $shelf->name }}</a>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            @if (strpos( Route::getCurrentRoute()->getPath(), 'checkout') === false)
                <div class="container-fluid pagecontentscroll pro-bg col-lg-10 col-md-{{ strpos( Route::getCurrentRoute()->getPath(), 'checkout') === false ? 10 : 12 }} col-xs-12 {{ explode('@', Route::currentRouteAction())[1] }}">
                    @else
                        <div class="container-fluid pagecontentscroll checkout pro-bg col-lg-12 col-md-{{ strpos(Route::getCurrentRoute()->getPath(), 'checkout') === false ? 10 : 12 }} col-xs-12 {{ explode('@', Route::currentRouteAction())[1] }}">
                            @if (strpos( Route::getCurrentRoute()->getPath(), 'orden_confirmada') === false)
                                <div class="mdl-cell mdl-cell--12-col" style="margin-bottom: 4%!important"></div>
                            @endif
                            @endif
                            <div class="contnt_block_store col-md-10 col-lg-10">
                                <div class="contnstore_list_change">
                                    @if($stores)
                                        @foreach ($stores as $store_data)
                                            <a href="{{ web_url() }}/{{ $store_data->city_slug }}/domicilios-{{ $store_data->slug }}">
                                                <div class="card_supporting-text col-md-6 col-xs-6">
                                                    <div class="cont_store_tex">
                                                        <div class="titleint">
                                                            <h2>{{ $store_data->name }}</h2>
                                                        </div>
                                                        <div class="content_store">
                                                            {{ $store_data->description_web }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            @include('layout_header_top')

                            @yield('content')

                            <div class="container-fluid col-md-6 col-sm-6 col-xs-12 pinned" id="cart-desc">
                                <div class="cart-data"></div>
                            </div>

                        </div>
                </div>
        </div>

        <!-- City change modal -->
        <div class="modal fade" id="modal-city-change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Carrito de Compras</h4>
                    </div>
                    <div class="modal-body">
                        <span>Si cambias de ciudad perderás los productos que están en tu carrito de compras. ¿Estas seguro que deseas continuar?</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success save-status accept-btn">&nbsp;Sí&nbsp;</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Removed products on change modal -->
        <div class="modal fade current-model next-modal-style" id="modal-product-change" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Carrito de Compras</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="accept-changes">Continuar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="dissmis-changes">
                            Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Best price modal -->
        <div class="modal fade mq-best-price-modal" id="best-price-modal" tabindex="-1" role="dialog">
            <script type="text/javascript">
                $(document).ready(function () {
                    var $modal_best = $('#best-price-modal');
                    var $terms_trigger = $('.mq-modal-terms-header');
                    var $terms_footer = $('.mq-best-price-modal .modal-footer');
                    var $terms_link = $('.mq-modal-terms-header a');
                    var $items_container = $('#mq-best-price-p');
                    var loading = false;
                    var moreloading = true;
                    var page = 1;
                    var store_id = '{{$store->id}}';
                    var warehouse = '{{ isset($warehouse) ? $warehouse->id : null }}';

                    $modal_best.on('show.bs.modal', function () {
                        loading = false;
                        moreloading = true;
                        trackEvent('best_price_guaranted_clicked');
                        getItems(page);
                    });

                    $('.mq-best-price-modal .modal-body').scroll(function () {
                        if ($(this).scrollTop() + 200 > $items_container[0].scrollHeight) {
                            getItems(page);
                        }
                    });

                    $terms_trigger.on('click', function (e) {
                        e.preventDefault();
                        toogleTerms();
                    });

                    function getItems(goPage) {
                        if (!loading && moreloading) {
                            loading = true;
                            $('.mq-loading-best-price').show();
                            $.ajax({
                                method: 'get',
                                url: '/{{$store['city_slug']}}/{{$store['slug']}}/'+'best-price?page='+goPage+'&warehouse_id='+warehouse+'&store_id='+store_id,
                            }).then((res) => {
                                get_cart();
                                if (res !== 'false') {
                                    $('.mq-loading-best-price').hide();
                                    $('#mq-best-price-p').append(res);
                                    $(".mq-best-price-container .product-list-img img").lazyload({
                                        container: $('.mq-best-price-modal .modal-body')
                                    });
                                    page = page + 1;
                                } else {
                                    moreloading = false;
                                    $('.mq-loading-best-price').hide();
                                }
                                loading = false;
                            });
                        }
                    }

                    function toogleTerms() {
                        if ($terms_footer.hasClass('terms-showed')) {
                            $terms_footer.removeClass('terms-showed');
                            $terms_link.html('Ver más');
                        } else {
                            $terms_footer.addClass('terms-showed');
                            $terms_link.html('Ver menos');
                        }
                    }
                });
            </script>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="{{ asset_url() }}img/mejor-precio-garantizado.png" alt="Mejor precio garantizado">
                        <hr class="mq-best-price-modal-hr">
                        <button type="button" class="close mq-best-price-close-modal" data-dismiss="modal"
                                aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="mq-card-bp">
                            <div class="mq-card-bp-img">
                                <img src="{{ asset_url() }}img/mejor-precio.jpg" alt="Mejor precio garantizado">
                            </div>
                            <div class="mq-card-bp-footer">
                                Conoce más de nuestros precios garantizados
                            </div>
                        </div>
                        <div class="mq-best-price-products">
                            <h2>Destacado <img src="{{ asset_url() }}img/blue-arrow-down.png" alt="flecha abajo"></h2>
                            <div class="mq-best-price-container" id="mq-best-price-p"></div>
                            <div class="mq-loading-best-price">
                                <div class="mq-spinner"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="mq-modal-terms-header">
                            <p>Términos y condiciones <a href="javascript:void(0)" target="_blank">Ver más</a></p>
                        </div>
                        <div class="mq-modal-terms-content">
                        <ul data-hash="#mejor-precio">
                            <li>MEJOR PRECIO GARANTIZADO</li>
                        </ul>
                        <p align="justify">
                            El Mejor Precio Garantizado aplica a los productos marcados con esta campaña en donde MERQUEO 
                            garantiza que el precio del producto es el mejor precio del mercado. En caso de que algún usuario 
                            de MERQUEO posterior a haber realizado su compra en la Plataforma encuentre el producto adquirido 
                            (que debe ser de las mismas características y marca al ofertado) a menor precio podrá presentar 
                            una solicitud vía e-mail a <a href="mailto:{{ Config::get('app.admin_email') }}" style="color: dodgerblue">{{ Config::get('app.admin_email') }}</a>
                            adjuntando comprobantes necesarios 
                            (factura o imagen que contenga el menor precio del producto en comparación al ofertado por MERQUEO) 
                            para que sea reintegrado en crédito (saldo a favor) al Usuario el valor del producto comprado. 
                            Adicionalmente, se hará este crédito por el valor de solo un ítem de las características
                            mencionadas en este literal. MERQUEO tendrá quince (15) días hábiles para la revisión y 
                            resolución de estas solicitudes.
                            <br><br>
                            Para que un producto cumpla la condición de Mejor Precio Garantizado se deberá cumplir las siguientes características: 
                        </p>
                        <ul style="margin-left: 20px; list-style-type: circle">
                            <li>Si el producto fue comprado en Bogotá deberá compararse con productos vendidos 
                                en la ciudad de Bogotá, de igual forma aplica para los productos comprados en Medellín.
                            </li>
                            <li>
                                Productos comparables: En productos Marcas Propias de Justo y Bueno, serán 
                                comparables con marcas blancas, es decir, de consumo común y que sean marca 
                                propia de otra cadena de distribución o supermercado. En caso de no ser productos 
                                Marcas propias de Justo y Bueno, la comparación debe realizarse con la misma marca 
                                y las mismas características del producto de otros supermercados en la misma fecha de compra. 
                            </li>
                            <li>
                                Las cualidades del producto como tamaño, dimensiones y usos deberán ser idénticos. 
                            </li>
                            <li>
                                La diferencia de precio sólo será válida si el precio de venta del otro supermercado 
                                es menor al precio de venta de MERQUEO y este precio no es afectado por 
                                algún descuento en particular.
                            </li>
                            <li>
                                El cliente debe haber comprado el producto en MERQUEO.
                            </li>
                        </ul>
                        
                        <ul>
                            <li>NORMAS ESPECÍFICAS SOBRE ORDENES DE COMPRA EN LAS QUE SE INCLUYA BEBIDAS ALCOHÓLICAS Y 
                                CIGARRILLOS
                            </li>
                        </ul>
                        <p align="justify">
                            Prohíbase el expendio de bebidas embriagantes a menores de edad. Ley 124 de 1994. El exceso de 
                            alcohol es perjudicial para la salud. Ley 30 de 1986 <br><br>
                            Toda Orden de Compra que incluya bebidas alcohólicas o cigarrillos sólo podrá ser requerida 
                            por Usuarios mayores de 18 años de edad. El Repartidor que efectúe la entrega de Órdenes de 
                            compra que incluyan dichos productos exigirá, para su entrega, la identificación del Usuario 
                            que efectuó la Orden de compra, exigiendo la presentación de una cédula de identidad o 
                            pasaporte vigente, liberando de toda responsabilidad, tanto a MERQUEO, sus directores,
                            empleados, subsidiarias, afiliados, agentes y representantes, como también al Repartidor, 
                            en caso de no producirse la entrega por la falta de acreditación de identidad del Usuario.
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @if (\Session::has('product_not_available_on_change') || \Session::has(CheckoutController::MUST_TO_SET_ADDRESS_SESSION_KEY))
        <!-- Removed products on checkout missing products modal -->
            <div class="modal fade current-model-missing-product" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="max-width: 520px;text-align: center; margin: 0 auto">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Carrito de compras</h4>
                        </div>
                        <div class="modal-body">
                            @if (\Session::pull('product_not_available_on_change'))
                                <span>El producto no esta disponible cerca de la dirección.</span>
                            @elseif (\Session::pull(CheckoutController::MUST_TO_SET_ADDRESS_SESSION_KEY))
                                <span>Debes seleccionar una dirección.</span>
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Continuar
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function () {
                    setTimeout(function () {
                        $('.current-model-missing-product').modal('show');
                    }, 100)
                });
            </script>
        @endif

        <div class="main-loader" style="display: none;">
            <img src="/assets/img/rolling.svg" alt="">
        </div>

        @include('layout_footer')
    </div>

    <script>
        $(document).ready(function () {
            $('input[name=q]').keyup(function (event, element) {

            });
            var changedCityReference = null;
            var safeheightsearch = 0;

            $(".fancybox").fancybox({
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: true,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });

            var target = $(".android-footer").offset().top;
            var alturafooter = $(".android-footer").height();
            var value = target - (alturafooter + 40);
            if (value < 0) {
                $('.android-footer').css('display', 'none');
            }

            $(window).scroll(function () {
                var numberwindow = $(window).scrollTop();

                if (value > 0) {
                    if (numberwindow >= value) {
                        var valormenuheight = parseInt(2) + parseInt(numberwindow);
                        $('.nav-pills').css({
                            "-webkit-transform": "translateY(-" + valormenuheight + "px)",
                            "-ms-transform": "translateY(-" + valormenuheight + "px)",
                            "transform": "translateY(-" + valormenuheight + "px)"
                        });
                    } else {
                        $('.nav-pills').css({
                            "-webkit-transform": "translateY(0px)",
                            "-ms-transform": "translateY(0px)",
                            "transform": "translateY(0px)"
                        });

                    }
                } else {
                    $('.page-content').css('margin-bottom', '480px');
                    $('.android-footer').css('display', 'block');
                    var valormenuheight = parseInt(2) + parseInt(numberwindow);
                    $('.nav-pills').css('height', '80%');
                    $('.nav-pills').css({
                        "-webkit-transform": "translateY(-" + valormenuheight + "px)",
                        "-ms-transform": "translateY(-" + valormenuheight + "px)",
                        "transform": "translateY(-" + valormenuheight + "px)"
                    });
                }
            });

            $('.link-change-city').click(function (e) {
                var numItems = parseInt($('span.cart-total-amount').text());
                changedCityReference = $(this).prop('href');
                if (numItems || checkoutWindowActive()) {
                    show_modal('city-change');
                    e.preventDefault();
                } else {
                    window.location.href = changedCityReference;
                }
            });

            $('.accept-btn').click(function () {
                window.location.href = changedCityReference;
            });

            /**
             * @returns {boolean}
             */
            function checkoutWindowActive() {
                return !!(window.location.pathname || '').match(/checkout/ig);
            }
        });
    </script>

    <style>
        .modal-body {
            padding: 10px;
            position: relative;
        }
    </style>

    @include('login_modal')

    @include('stop_and_compare')

    @if (strpos(Route::getCurrentRoute()->getPath(), 'checkout') === false)
        @include('address_modal')
    @endif

    @include('warning_modal')

    <div id="coverage" class="unseen"><img src="{{ asset_url() }}img/coverage_map_{{ $store->city_slug }}.jpg"
                                           alt="merqueo-cobertura" width="740"/></div>

    @if (Session::get('show_discount'))
        {{ Session::forget('show_discount') }}
        <a class="fancybox linkdiscount" href="#discount" style="display:none!important;"></a>
        <div id="discount" class="unseen"><img src="{{ asset_url() }}img/promo-entrega-manana.png"
                                               alt="merqueo-cobertura" width="390"/></div>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $('.linkdiscount').trigger('click');
                }, 1000);
            });
        </script>
@endif
@include('common.account_kit_validation')

</body>
</html>
