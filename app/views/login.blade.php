@extends('layout_home')

@section('content')

<div class="container home header-top bg_login">
    <div class="col-md-11 home-inner-container">
		<div class="row clearfix">
			<div class="col-md-8 col-sm-8 col-xs-12 column log-inner text-center ">
				<h2 class="text-center">Bienvenido a Merqueo</h2><br>
				<div class="login-fb text-center">
					<div class="unseen alert alert-danger fb-login-error"></div>
   		  			<div align="center" class="fb-login-loading unseen"><img src="{{ asset_url() }}/img/loading.gif" /></div>
	              	<button type="button" class="btn btn-primary btn-block fb-login"><i class="icon-facebook"></i>&nbsp;&nbsp;&nbsp;Ingresa con tu cuenta de Facebook</button>
	            </div>
	            <hr>
	            <h4 class="text-center">Ingresa con tu cuenta de Merqueo</h4><br>
	            <div class="@if (!Session::has('message')) unseen @endif alert alert-danger form-has-errors">@if (Session::has('message')) {{ Session::get('message') }} @endif</div>
				<form class="login" method="post" action="{{ route('frontUser.verify', ['redirect' => $redirect]) }}">
				  <div class="form-group">
				    <input type="email" class="form-control" placeholder="Correo eléctronico" name="email">
				  </div>
				  <div class="form-group">
				    <input type="password" class="form-control" placeholder="Contraseña" name="password">
				  </div>
				  <button type="submit" class="btn btn-info col-md-3 col-sm-3 col-xs-3">INGRESAR</button>
				</form>
				<br><br><br>
				<h5 class="text-center">
					<a href="{{ web_url() }}/recuperar-clave">¿Olvidaste tu contraseña?</a>
				</h5>
				<br>
			</div>
		</div>
	</div>
</div>

@include('layout_footer')
@include('common.account_kit_validation')
@stop