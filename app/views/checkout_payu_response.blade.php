@extends('layout_page')

@section('extra_assets')
	<title>Tu pedido</title>
	<meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
	<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,800" rel="stylesheet">
	<link href="{{ asset_url() }}css/order-success.css" rel="stylesheet">
	<link href="{{ asset_url() }}css/payu-response.css" rel="stylesheet">
@endsection

@section('content')
	<section>
		<div class="container">
			<div class="row">

				<div class="col-sm-12">
					<div class="miPedido_table payuResponse_table" id="printer">
						@if(Session::has('message') && !empty(Session::get('message')))
						<div class="alert alert-info alert-dismissable">
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                        @endif
						<div class="col-sm-12 miPedidoTitle" >
							<p>Resultado de la operación</p>
						</div>
						
						<div class="table-responsive">
							<table class="table">
								<tr>
									<td class="col-md-6 label"><label>Nombre de la empresa:</label> Merqueo S.A.S</td>
									<td class="col-md-6 label"><label>NIT de la empresa:</label> 900.871.444-8</td>
								</tr>
								<tr>
			                        <td class="col-md-6 label">
			                        	<label>Fecha de transacción:</label> {{$date_payment}}
			                        </td>
			                        <td class="col-md-6 label">
			                        	<label>Estado:</label>
										<span class="status-transaction {{ $status_transaction_result == 'APPROVED' ? 'approved' : ''}}"> {{$status_transaction}} </span>
			                        </td>
								</tr>
								<tr>
			                        <td class="col-md-6 label">
			                        	<label>Referencia del pedido:</label> {{$reference_code}}
			                        </td>
			                        <td class="col-md-6 label">
			                        	<label>Referencia transacción:</label> {{$transaction_id}}
			                        </td>
								</tr>
								<tr>
			                        <td class="col-md-6 label">
			                        	<label>Número transacción/CUS:</label> {{$cus}}
			                        </td>
			                        <td class="col-md-6 label">
			                        	<label>Banco:</label> {{$pse_bank}}
			                        </td>
								</tr>
								<tr>
			                        <td class="col-md-6 label">
			                        	<label>Valor:</label> ${{$tx_value}}
			                        </td>
			                        <td class="col-md-6 label">
			                        	<label>Moneda:</label> {{$currency}}
			                        </td>
								</tr>
								<tr>
			                        <td class="col-md-6 label">
			                        	<label>Descripcion:</label> {{$description}}
			                        </td>
			                        <td class="col-md-6 label">
			                        	<label>IP Origen:</label> {{$ip_origen}}
			                        </td>
								</tr>
							</table>
						</div>
                        
                        <div class="clearfix">
                        	&nbsp;
                        </div>
						<div class="col-sm-12">
							<div class="btns-payu">
								@if ($status_transaction_result != 'APPROVED' && $status_transaction_result != 'PENDING')
								<button onclick="window.location = '{{ $url_retry }}'" class="btn-payu btn-payu-go-transaction">Reintentar transacción</button>
								<button class="btn-payu btn-payu-cancel" data-toggle="modal" data-target="#cancel-transaction-modal">Cancelar transacción</button>
								@else
								<button onclick="window.location = '{{ $url_end_transaction }}'" class="btn-payu btn-payu-go-home approved">Finalizar transacción</button>
								@endif
							</div>
							<div class="clearfix">
	                        	&nbsp;
	                        </div>
						</div>
						<div class="clearfix">
                        	&nbsp;
                        </div>
                        <div class="col-sm-12">
							<button onclick="window.print()" class="btn-payu btn-payu-downoload center-block">Imprimir comprobante
								<img src="{{ asset_url() }}img/icono-descargar.svg" class="icono_descargar">
							</button>
							
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	@include('checkout_cancel_modal')

	<!-- jQuery -->
	<script src="https://sdk.accountkit.com/es_ES/sdk.js"></script>
	
	<script>
        $('#phone_validation_alert').hide();
        $('#phone_validation_success').hide();
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function(){
            AccountKit.init(
                {
                    appId: "{{ Config::get("app.facebook_app_id") }}",
                    state: "{{ Cache::get('js_version_number') }}",
                    version: "v1.0",
                    fbAppEventsEnabled: true
                }
            );
        };

        // login callback
        function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {
                var code = response.code;
                var csrf = response.state;
                // Send code to server to exchange for access token
                $.ajax({
                    url: '{{ route('frontUser.cellphoneValidation') }}',
                    type: 'POST',
                    data: { code: code },
                    dataType: 'json'
                })
				.done(function(response) {
					if (response.status){
						$('.phone_validation').hide();
						$('#phone_validation_success').html(response.message).show();
					}else{
						$('#phone_validation_alert').html(response.message).show();
					}
				});
            }
            else if (response.status === "NOT_AUTHENTICATED") {
                //console.log(response);
            }
            else if (response.status === "BAD_PARAMS") {
                //console.log(response);
            }
        }


        /*$(document).ready(function () {
            $('body').on('click', '.socialLinks', function(event) {
				var anchor = $(this);
                event.preventDefault();
                trackEvent('referral_code_shared', {channel: anchor.prop('id'), screen: pageName()});
				if (anchor.data('copy-to-clipboard')) {
                    copyToClipboard('#referral_code');
                    return true;
				}

				window.open(anchor.prop('href'), '', 'width=700,height=500');
			});
		});*/
	</script>


	
    @if(isset($user))
    <script>
        function smsLogin() {
            var phoneNumber = '{{ $user->phone }}';
            AccountKit.login(
                'PHONE',
                {countryCode: '+57', phoneNumber: phoneNumber},
                loginCallback
            );
        }
	</script>
	@endif

@stop
