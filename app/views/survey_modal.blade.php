<div class="modal fade" id="answers-user" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body text-center">
        <div class="row">
          <br /><br />
          <div class="col-md-12">
            <div class="col-md-12">
                <h4 class="title-answers col-md-12">{{ $survey['question'] }}</h4>
              </div>
            <br />
            <div class="unseen alert alert-danger form-has-errors answer-error"></div>
            <form role="form" class="form_items_answers" method="post" action="#">
              <div class="form_items">
                <div class="bg_items col-md-12">
                  <div class="col-md-12">
                    <ul>
                    @foreach($survey['answers'] as $key => $answers)
                      <li class="item_answers col-md-5">
                        <input class="answer-item col-md-1" type="radio" name="answers_item" id="{{ $answers }}" value="{{ $answers }}"/>
                        <label class="col-md-11" for="{{ $answers }}">{{ $answers }}</label>
                      </li>
                    @endforeach
                    </ul>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <div class="col-md-12 cont_loading">
                  <div class="col-md-5"></div>
                  <div class="loading col-md-2">
                    <img src="{{ asset_url() }}img/hourglass.gif">
                  </div>
                  <div class="col-md-5"></div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-5"></div>
                  <button type="submit" class="col-md-2 btn_send_answers" data-id="{{ $survey['id'] }}" data-order="{{ $order_data['order_id'] }}">Enviar</button>
                  <div class="col-md-5"></div>
                </div>
              </div>
              <div class="sucess_data">
                <div class="bg_items col-md-12">
                </div>
              </div>
            </form>
          </div>
        </div>
        <br />
      </div>
    </div>
  </div>
</div>

<script>
  $(document).on('ready', function(){
    var data_answer= '';
    var control = 0;
    $('#answers-user input').on('change', function(){
        var data_select = $(this).val();
        data_answer = data_select;
    });

    $('.item_answers label').on('click', function(){
        labelID = $(this).attr('for');
        $('#'+labelID).trigger('click');
    });

    $('#answers-user').on('click', '.btn_send_answers', function(event){
      event.preventDefault();
      $(this).addClass('disabled');
      if(control == 0){
        control = 1;
        $('.cont_loading').css('display', 'inline-block');
        $('.answer-error').hide();
        var idsurvey = $(this).data("id");
        var idorder = $(this).data("order");
        if (data_answer != ""){
          $.ajax({
            url: web_url + '/user/survey',
            type: 'post',
            data: {survey_id: idsurvey, answer: data_answer, order_id:idorder, os: 'web'},
            dataType: 'json',
            success: function(response) {
              $('.cont_loading').css('display', 'none');
              if (response.status) {
                $('.btn_send_answers').addClass('disabled');
                $('.form_items').hide();
                $('.sucess_data .bg_items').html(response.message).show();
                $('.sucess_data').css('display', 'block');
                setTimeout(function(){
                    $('#answers-user').modal('hide');
                }, 2000);
              }else {
                $('.answer-error').html(response.message);
                $('.answer-error').css('display', 'inline-block');
                control = 0;
                $(this).removeClass('disabled');
              }
            }, error: function() {
                $('.cont_loading').css('display', 'none');
                $('.answer-error').html('Error en el formulario');
                $('.answer-error').css('display', 'inline-block');
                control = 0;
                $(this).removeClass('disabled');
            }
          });
        }else{
          $('.cont_loading').css('display', 'none');
          $('.answer-error').html('Selecciona una opción para responder la encuesta.');
          $('.answer-error').css('display', 'inline-block');
          control = 0;
          $(this).removeClass('disabled');
        }
      }
    });
  });
</script>