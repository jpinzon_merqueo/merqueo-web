<ul>
@if(count($products))
    @foreach($products as $product)
  <li class="col-xs-12">
    <div class="col-xs-2" align="center">
      <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $store['city_slug'], 'store_slug' => $store['slug'], 'department_slug' => $product['department_slug'], 'shelf_slug' => $product['shelf_slug'], 'product_slug' => $product['slug']]) }}" class="name-link">
        <img class="search-product-image" src="{{ $product['image_small_url'] }}" data-zoom-image="{{ $product['image_large_url'] }}">
      </a>
    </div>
    <div class="col-xs-6">
      <div class="name">
        <a href="{{ route('frontStoreProducts.single_product', ['city_slug' => $store['city_slug'], 'store_slug' => $store['slug'], 'department_slug' => $product['department_slug'], 'shelf_slug' => $product['shelf_slug'], 'product_slug' => $product['slug']]) }}" class="name-link">
          {{ $product['name'] }}
        </a>
      </div>
        @if ($product['special_price'] > -1)
        <div class="price"><strike>$ {{number_format($product['price'], 2)}}</strike> <span class="price product-special-price">$ {{ number_format($product['special_price'], 2) }}</span></div>
        @else
        <div class="price">$ {{number_format($product['price'], 2)}}</div>
        @endif
        @if(isset($product['pum']))
          <div class="product-pum">
            @if($product['type'] == 'Simple')
              <small>{{ $product['pum'] }}</small>
            @elseif($product['type'] == 'Agrupado')
              @foreach(explode('|', $product['pum'], 1) as $pum)
                @if($pum)
                  <small>{{ mb_substr($pum, 0, 22)  }} ...</small>
                @endif
              @endforeach
            @endif
          </div>
        @endif
    </div>
    <div class="col-xs-4">
      <div class="btn btn-xs btn-success btn-block cont_search_item" data-id="{{ $product['id'] }}" onclick="modify_cart({{ $product['id'] }}, {{ $product['has_warning'] }}, 1, 0, 'search', event)">AGREGAR +</div>
      <div class="cont_search_cart">
        <div class="btn btn-xs btn-success btndown" data-id="{{ $product['id'] }}" onclick="modify_cart({{ $product['id'] }}, 0, -1, 0, 'search', event)"> - </div>
        <div class="dataactitem" id="item-act-{{ $product['id'] }}">0</div>
        <div class="btn btn-xs btn-success btnup" data-id="{{ $product['id'] }}" onclick="modify_cart({{ $product['id'] }}, 0, 1, 0, 'search', event)"> + </div>
      </div>
  </li>
    @endforeach
@else
  <li class="col-xs-12">
    <div align="center">
      No se encontraron resultados en {{ $store->name }}
    </div>
  </li>
@endif
  <li class="col-xs-12">
    <div align="center">
      <a href="{{ route('frontStore.search_products', ['city_slug'=>$store['city_slug'], 'store_slug'=>$store['slug']]) }}?city_name={{$store['name']}}&q={{ $query }}">Buscar '{{ $search }}' en toda la tienda</a>
    </div>
  </li>
</ul>
