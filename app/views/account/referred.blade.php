<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

<div class="row referral">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="referral-title">¡Comparte tu código!</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="referral-text">Regala {{ currency_format(Config::get('app.referred.amount')) }} a tus
                    vecinos y amigos en créditos de Merqueo y cuando hagan su primer pedido tu cuenta será
                    cargada con {{ currency_format(Config::get('app.referred.referred_by_amount')) }}.
                    <img src="/assets/img/info-ref.svg" alt="info-ref">
                    <div class="referral-tooltip">
                        <p>Términos y condiciones</p>
                        Aplica para pedidos mínimo de {{ currency_format(Config::get('app.minimum_discount_amount')) }}.
                        Válido para los {{ Config::get('app.referred.amount_expiration_days') }} días siguientes a la fecha de
                        redención. No acumulable con otras promociones o descuentos.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-12 text-center">
                <div class="share-code">
                    <p class="promo-code-referral share-code-code">
                        <span id="ReferralCode">{{ $data['user']['referral_code'] }}</span>
                        <button class="share-code-btn"
                                data-clipboard-target="#ReferralCode">
                            Copiar
                        </button>
                    </p>
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 10px">
            <div class="col-xs-12 text-center">
                <button id="facebook" type="button"
                        onclick="window_open('https://www.facebook.com/dialog/share?app_id={{ Config::get('app.facebook_app_id') }}&display=popup&href={{ web_url() }}/registro/{{ $data['user']['referral_code'] }}')"
                        class="btn-shared-merqueo" target="_blank">
                    <img src="{{ asset_url() }}/img/referidosweb/facebook.svg" width="45" height="45">
                </button>
                <button id="link" type="button" class="btn-shared-merqueo"
                        onclick="window_open('https://web.whatsapp.com/send?text=¿Ya probaste Merqueo? {{ Auth::user()->first_name }} te regaló {{ currency_format(Config::get('app.referred.amount')) }} para tu primer pedido. Descarga la app e ingresa el código {{ $data['user']['referral_code'] }} ¡Vamos! {{ web_url() }}/registro/{{ $data['user']['referral_code'] }}')"
                >
                    <img src="{{ asset_url() }}/img/referidosweb/whatsapp.svg" width="45" height="45">
                </button>
                <button id="twitter" type="button"
                        onclick="window_open('https://twitter.com/intent/tweet?text=¿Ya probaste Merqueo? {{ Auth::user()->first_name }} te regaló {{ currency_format(Config::get('app.referred.amount')) }} para tu primer pedido. Descarga la app e ingresa el código {{ $data['user']['referral_code'] }} ¡Vamos! {{ web_url() }}/registro/{{ $data['user']['referral_code'] }}')"
                        class="btn-shared-merqueo" target="_blank">
                    <img src="{{ asset_url() }}/img/referidosweb/twitter.svg" width="45" height="45">
                </button>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-1">
        <div class="referral-header">
            <div class="referral-img">
                <div id="gift-animation"></div>
            </div>
            <div class="referral-info">
                <h2>Tienes</h2>
                <h2 class="pinkLabel">{{ $data['credit_available'] }}</h2>
                <h2>Para usar en Merqueo</h2>
                @if ($data['user_discounts']['free_delivery_days'])
                <p class="referral-pill"><span class="merqueo-color">{{ $data['user_discounts']['free_delivery_days'] }}</span> días de domicilio gratis</p>
                @else
                @if ($data['user_discounts']['free_delivery_next_order'])
                <p class="referral-pill">Tu próximo domicilio es gratis</p>
                @endif
                @endif
            </div>
        </div>
        @if($data['credit_available'])
            <div class="row referral-label">
                <div class="col-xs-12">
                    <h2 style="font-size: 22px"><b>Historial</b></h2>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                @if(count($data['credit_movements']))
                    <table class="table referral-table">
                        <tbody>
                        @foreach($data['credit_movements'] as $credit_movement)
                            <tr>
                                <td width="70%">
                                    <p><b>{{ $credit_movement['description'] }}</b></p>
                                    <p class="muted">{{ $credit_movement['expiration_date'] }}</p>
                                </td>
                                <td width="25%">
                                    <p><b>Disponible: </b></p>
                                    <p class="muted">Crédito inicial: </p>
                                </td>
                                <td width="10%" style="text-align: right">
                                @if ($credit_movement['expired'] && $credit_movement['current_amount_value'])
                                    <p class="ref-invalid">{{ $credit_movement['current_amount'] }}</p>
                                    <p class="ref-invalid">Vencido</p>
                                @else
                                    <p @if ($credit_movement['current_amount_value'])class="ref-valid"@endif>{{ $credit_movement['current_amount'] }}</p>
                                    <p class="muted">{{ $credit_movement['initial_amount'] }}</p>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="empty-history text-center">
                        <img src="/assets/img/empty-entry.svg" alt="empty">
                        <p>
                            <b>¿Aun no has compartido tu código?</b><br>
                            Comparte y disfruta de tu crédito
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    bodymovin.loadAnimation({
        container: document.getElementById('gift-animation'), // Required
        path: '/assets/img/animations/git-animation.json', // Required
        renderer: 'svg', // Required
        loop: true, // Optional
        autoplay: true, // Optional
        name: 'Gift animation', // Name for future reference. Optional.
    });

    var clipboard = new ClipboardJS('.share-code-btn');
    clipboard.on('success', function (e) {
        $('.share-code-btn').text('¡Copiado!')
    });
</script>