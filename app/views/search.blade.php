@extends('layout')

@section('title')Busqueda de {{ $q }} en {{ $store->store_name }} {{ $store->city_name }} - Merqueo.com @stop

@section('content')

    <script>
        var content_ids = [{{ $content_ids }}];
        var search_string = '{{ $q }}';
    </script>

	<div class="mdl-grid logtexttienda">
		</div>
	<div class="col-md-12 pro-search-title" style="margin-top: 140px;">
		@if (count($products))
		<h3 style="margin:10px;">Resultados de la búsqueda "{{ $q }}"</h3>
		@else
		<h3>No se encontraron resultados para "{{ $q }}"</h3><br>
		@endif
	</div>

	<div class="col-xs-12 clearfix pro-row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			{{ View::make('elements.search-products-list', ['products' => $products]) }}
		</div>
	</div>
@stop