@extends('layout_device')

@section('content')
<h2>Para que tengas una mejor experiencia, descarga la app y empieza a ahorrar.</h2>

<a href="{{ $controlmobile == '0' ? $footer['links']['android_url'] : $footer['links']['ios_url'] }}" class="main-cta app-download">Descargar aplicación</a>

<a href="{{$url_send}}/not-mobile" class="secondary-cta continue">Seguir al sitio web</a>
@stop
