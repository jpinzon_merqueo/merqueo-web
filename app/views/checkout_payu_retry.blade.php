@extends('layout_page')

@section('extra_assets')
	<title>Tu pedido</title>
	<meta property="fb:app_id" content="{{ Config::get('app.facebook_app_id') }}"/>
	<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,800" rel="stylesheet">
	<link href="{{ asset_url() }}css/order-success.css" rel="stylesheet">
	<link href="{{ asset_url() }}css/payu-response.css" rel="stylesheet">
@endsection

@section('content')
	
	<script>
		
	</script>
	<section>
		<div class="container">
			<div class="row">

				<div class="col-sm-12">
					<div class="miPedido_table retryTransaction_table" id="printer">
						@if(isset($error) && !empty($error))
						<div class="alert alert-info alert-dismissable">
                            <strong>{{ $error }}</strong>
                        </div>
                        @endif
                        
						@if(!isset($success))
						<div class="col-sm-12 miPedidoTitle" >
							<p>Reintentar o cambiar método de pago</p>
						</div>
						<br/>
						<div class="col-xs-12">
							<div class="row">
								<h3>Dirección de entrega</h3>
							</div>
							<div class="row">
								<b style="font-size:20px;">{{$order->orderGroup->user_address}}</b>
							</div>
							<hr style="min-width:100%;"/>
							<br/>
							<div class="row">
								<h3>Fecha de entrega</h3>
							</div>
							<div class="row">
								<b style="font-size:20px;">{{ format_date('day_name', $order->delivery_date ) }}, de {{ $order->delivery_time  }}</b> 
							</div>
							<br/>

							<div class="selectministore_down">
								<div class="len_store"><p>Valor sujeto a disponibilidad de productos</p></div>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6 text-left">
										<p>Subtotal</p>
										<p>Costo domicilio</p>
										<p>Descuento</p>
										<!--<p class="item_cart_col">Ahorro</p>-->
										<p>Total este pedido</p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 text-right">
										<div class="storeprod_10">
											<p class="subtotal" >${{ number_format($order->total_amount, 0, ',', '.') }}</p>
											<p class="delivery-amount" data-amount="{{ $order->delivery_amount }}">
												${{ number_format($order->delivery_amount, 0, ',', '.') }}
											</p>
											<p class="discount-amount">@if ($order->discount_amount)-${{ number_format($order->discount_amount, 0, ',', '.') }}@else $0 @endif</p>
											<!--<p class="discount-amount item_cart_col">$ {{ number_format($order->discount_amount, 0, ',', '.') }}</p>-->
											<p class="total_store total_item_store_10 sd-store-price">$<span>{{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}</span></p>
										</div>
									</div>
								</div>
								<div class="len_store"></div>
							</div>
						</div>
						<form class="form-horizontal col-xs-12" id="retry_payment_form" role="form"  method="post" action="{{route('PayU.retryTransactionSave')}}">
							<input type="hidden" name="order_id" value="{{$order->id}}">
							<div class="clearfix">
	                        	&nbsp;
	                        </div>
							<div class="row payment"><h3>Método de Pago</h3></div>
							<hr style="min-width:100%;"/>
							<br/>
							@if(!empty($discount_percentage))
							<div class="form-group">
								<div class="blo_promo">
									<div class="contblo">
										<div class="img_pro">
											<img src="{{ asset_url() }}/img/visa.png" alt="grocery-bags">
										</div>
										<div class="text-title">
											<p>Hoy <span>{{ $discount_percentage }}%</span> de descuento pagando online con tarjeta débito VISA habilitada para pago en línea.</p>
										</div>
									</div>
								</div>
							</div>
							@endif


							<div class="form-group">
						       	<select type="text" class="form-control required" name="retry_payment_method" id="payment_method">
						       	    <option value="">Selecciona el método de pago</option>
						       	    @foreach($payment_methods as $value => $name)
							       		<option value="{{ $value }}" @if (isset($post['payment_method']) && $post['payment_method'] == $value) selected="selected" @endif>{{ $name }}</option>
						       		@endforeach
						       	</select>
						    </div>
						    <div class="retry-form_pse unseen">
						    	<div class="row"><br><h4>Débito - PSE</h4><br></div>
						    	<div class="row">
						    		<div class="form-group col-xs-6">
						    			<label>Banco</label>
						    			<select class="form-control pse_field" id="bank_pse" name="bank_pse">
					                        <option value="" selected="selected">Seleccione</option>
					                        @foreach($banks as $bank)
					                        	<option value="{{($bank->pseCode==0)?'':$bank->pseCode}}" @if (isset($post['bank_pse']) && $post['bank_pse'] == $bank->pseCode) selected="selected" @endif>{{$bank->description}}</option>
					                        @endforeach
						    			</select>
						    		</div>
						    		<label class="label-complement">&nbsp;</label>
						    		<div class="form-group col-xs-6">
										<label>Nombre</label>
									    <input type="text" autocomplete="off" class="form-control onlyLetters pse_field" placeholder="Nombre" name="name_pse" maxlength="24" value="{{ $post['name_pse'] or '' }}">
									</div>
						    	</div>
						    	<div class="row">
						    		<div class="form-group col-xs-6">
						    			<label>Tipo de persona</label>
						    			<select class="form-control pse_field" id="type_person_pse" name="type_person_pse">
					                        <option value="" selected="selected">Seleccione</option>
					                        <option value="N">Persona Natural</option>
					                        <option value="J">Persona Jurídica</option>
						    			</select>
						    		</div>
						    		<label class="label-complement">&nbsp;</label>
								    <div class="form-group col-xs-6">
					                    <label>Tipo de documento</label>
					                    <select type="text" class="form-control pse_field" id="document_type_pse" name="document_type_pse">
					                        <option value="" selected="selected">Tipo de documento</option>
					                        <option value="CC">Cédula de ciudadania</option>
					                        <option value="CE">Cédula de extranjeria</option>
					                        <option value="NIT">Número de identificación tributario</option>
					                        <option value="PP">Pasaporte</option>
					                    </select>
					                </div>
						    	</div>
						    	<div class="row">
								    <div class="form-group col-xs-6">
								        <label>Documento de identificación</label>
					                    <input type="text" autocomplete="off" class="form-control onlyNumbers pse_field" placeholder="Número de documento" id="document_number_pse" name="document_number_pse" maxlength="16" value="{{ $post['document_number_pse'] or '' }}">
					                </div>
					                <label class="label-complement">&nbsp;</label>
					                <div class="form-group col-xs-6">
					                    <label>Télefono</label>
					                    <input type="text" autocomplete="off" class="form-control onlyNumbers pse_field" placeholder="Teléfono del titular" id="phone_pse" name="phone_pse" maxlength="10" value="{{ $post['phone_pse'] or '' }}">
					                </div>
					            </div>
					            <script type="text/javascript" src="https://maf.pagosonline.net/ws/fp/tags.js?id={{$device_session_id}}80200"></script>
								<noscript>
									<iframe style="width: 100px; height: 100px; border: 0; position: absolute; top: -5000px;" src="https://maf.pagosonline.net/ws/fp/tags.js?id={{$device_session_id}}80200"></iframe>
								</noscript>

						    	
						    </div>
						    
							<div class="retry-form_cc unseen">
						    	<div class="row"><br><h4>Tarjeta de Crédito</h4><br></div>
						    	<div class="user-credit-card @if(!$credit_cards) unseen @endif">
					    	    @if($credit_cards)
						    		<h4><div class="credit-card-toggle pull-right btn btn-info">Nueva tarjeta</div></h4><br><br><hr>
									<input type="hidden" name="credit_card_id" value="">
									@foreach($credit_cards as $cc)
									<div class="bg-info">
										<a class="credit-card btn btn-default btn-sm pull-right" data-id="{{ $cc['id'] }}" href="javascript:;">+ Utilizar esta tarjeta</a>
										<img src="{{ web_url() }}/assets/img/credit_card.png">&nbsp;&nbsp;&nbsp;Tarjeta de crédito<br>
										<b>**** {{ $cc['last_four'] }}</b><br>
										<i>{{ $cc['type'] }}</i><br>
										@if(empty($cc['holder_document_number']))
										<div class="add-document-number-cc unseen">
										    <br>
									        <div class="row">
					                            <div class="form-group col-xs-6">
					                                <label>Documento de identificación</label>
					                                <select type="text" class="form-control cc" name="add_document_type_cc_{{ $cc['id'] }}">
					                                    <option value="" selected="selected">Tipo de documento</option>
					                                    <option value="CC" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'CC') selected="selected" @endif>Cédula de ciudadania</option>
					                                    <option value="CE" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'CE') selected="selected" @endif>Cédula de extranjeria</option>
					                                    <option value="NIT" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'NIT') selected="selected" @endif>Número de identificación tributario</option>
					                                    <option value="PP" @if (isset($post['add_document_type_cc_'.$cc['id']]) && $post['add_document_type_cc_'.$cc['id']] == 'PP') selected="selected" @endif>Pasaporte</option>
					                                </select>
					                           </div>
					                           <label class="label-complement">&nbsp;</label>
					                           <div class="form-group col-xs-6">
					                                <label>&nbsp;</label>
					                                <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Número de documento" name="add_document_number_cc_{{ $cc['id'] }}" maxlength="16" value="{{ $post['add_document_number_cc_'.$cc['id']] or '' }}">
					                           </div>
					                        </div>
					                    </div>
					                    @endif
									</div><hr>
									@endforeach
								@endif
								</div>
						    	<div class="new-credit-card @if($credit_cards) unseen @endif">
						    		@if($credit_cards)
										<div class="credit-card-toggle pull-right btn btn-info">Usar una de mis tarjetas guardadas</div>
										<br><br><br>
									@endif
									<div class="form-group">
									    <p>Si es tu primera compra con Tarjeta de Crédito y no eres el dueño, te llamáremos y modificaremos el método de pago a Datáfono. Lo hacemos por tu seguridad. (Solo se hará la primera vez).
										<!--<p>Se realizará un cargo de $1.000 COP en tu tarjeta de crédito para su validación, este monto no se cobrará ni aparecerá en tu extracto bancario. Solo aceptamos tarjetas de crédito tipo VISA, MASTERCARD y AMERICAN EXPRESS.-->
										<br><br><img src="{{ asset_url() }}/img/payment_methods.png" width="100">
										<img src="{{ asset_url() }}/img/credibanco.gif" width="100">
										</p>
									</div>
						        	<div class="form-group">
										<label>Nombre (tal como aparece en tu tarjeta)</label>
									    <input type="text" autocomplete="off" class="form-control onlyLetters cc" placeholder="Nombre" name="name_cc" maxlength="24" value="{{ $post['name_cc'] or '' }}">
									</div>
									<div class="form-group">
										<label>Número de tarjeta</label>
									    <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Número de tarjeta" id="number_cc" name="number_cc" maxlength="16" value="{{ $post['number_cc'] or '' }}">
									</div>
									<div class="form-group">
										<label>Código de seguridad</label>
									    <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Código de seguridad" name="code_cc" maxlength="4" value="{{ $post['code_cc'] or '' }}">
									</div>
									<div class="row">
										<div class="form-group col-xs-6">
											<label>Fecha de expiración</label>
										    <select type="text" class="form-control cc" id="expiration_year_cc" name="expiration_year_cc">
										    	<option value="" selected="selected">Año</option>
								           		@foreach($dates_cc['years'] as $year)
								           		<option value="{{ $year }}" @if (isset($post['expiration_year_cc']) && $post['expiration_year_cc'] == $year) selected="selected" @endif>{{ $year }}</option>
								           		@endforeach
								           	</select>
							           </div>
							           <label class="label-complement">&nbsp;</label>
							           <div class="form-group col-xs-6">
							           		<label>&nbsp;</label>
								           	<select type="text" class="form-control cc" id="expiration_month_cc" name="expiration_month_cc">
										    	<option value="" selected="selected">Mes</option>
								           		@foreach($dates_cc['months'] as $month)
								           		<option value="{{ $month }}" @if (isset($post['expiration_month_cc']) && $post['expiration_month_cc'] == $month) selected="selected" @endif>{{ $month }}</option>
								           		@endforeach
								           	</select>
							           </div>
									</div>
									<div class="row">
									    <div class="form-group col-xs-6">
					                        <label>Documento de identificación del titular</label>
					                        <select type="text" class="form-control cc" id="document_type_cc" name="document_type_cc">
					                            <option value="" selected="selected">Tipo de documento</option>
					                            <option value="CC" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'CC') selected="selected" @endif>Cédula de ciudadania</option>
					                            <option value="CE" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'CE') selected="selected" @endif>Cédula de extranjeria</option>
					                            <option value="NIT" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'NIT') selected="selected" @endif>Número de identificación tributario</option>
					                            <option value="PP" @if (isset($post['document_type_cc']) && $post['document_type_cc'] == 'PP') selected="selected" @endif>Pasaporte</option>
					                        </select>
					                   </div>
					                   <label class="label-complement">&nbsp;</label>
					    			   <div class="form-group col-xs-6">
					    			        <label>&nbsp;</label>
					                        <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Número de documento" id="document_number_cc" name="document_number_cc" maxlength="16" value="{{ $post['document_number_cc'] or '' }}">
					                   </div>
					                </div>
					                <div class="row">
					                    <div class="form-group col-xs-6">
					                        <label>Teléfono del titular</label>
					                        <input type="text" autocomplete="off" class="form-control onlyNumbers cc" placeholder="Teléfono del titular" id="phone_cc" name="phone_cc" maxlength="10" value="{{ $post['phone_cc'] or '' }}">
					                   </div>
					                   <label class="label-complement">&nbsp;</label>
					                   <div class="form-group col-xs-6">
					                        <label>Dirección del titular</label>
					                        <input type="text" autocomplete="off" class="form-control cc" placeholder="Dirección del titular" id="address_cc" name="address_cc" maxlength="50" value="{{ $post['address_cc'] or '' }}">
					                   </div>
					                </div>
						        </div>
						        <div class="form-group">
									<label>Número de cuotas</label>
								    <select type="text" class="form-control cc" id="installments_cc" name="installments_cc">
								    	<option value="" selected="selected"></option>
						           		@for ($i = 1; $i <= 36; $i++)
						           			@if ($i == 1)
						           				<option value="1" selected="selected">1</option>
						           				@else
						           				<option value="{{ $i }}" @if (isset($post['installments_cc']) && $post['installments_cc'] == $i) @endif>{{ $i }}</option>
						           			@endif
						           		@endfor
						           	</select>
								</div>
						    </div>
	                        <div class="clearfix">
	                        	&nbsp;
	                        </div>
	                        
							<div class="col-sm-12">
								<div class="btns-payu">
									<input type="submit" name="submit" id="retry-payment-button" class="btn-payu btn-payu-go-transaction" value="Pagar">
									<input type="button" value="Cancelar" class="btn-payu btn-payu-cancel" data-toggle="modal" data-target="#cancel-transaction-modal">
								</div>
								<div class="clearfix">
		                        	&nbsp;
		                        </div>
							</div>
							<div class="clearfix">
	                        	&nbsp;
	                        </div>
						</form>
						@else
							<!-- <div class="col-sm-12 miPedidoTitle" >
								<p>Mi Pedido: <span>#{{ $order->reference }}</span></p>
							</div> -->
							<style type="text/css">
								.retry-pse{
									margin: 0 auto;
									width: 100%;
									height: 250px;
									position: relative;
									color: #000;
    								padding: 10px;
    								
    								/*background-color: #ccc;*/
								}
							</style>
							<div class="retry-pse" id="lottie"></div>
							
							<div class="row payment" style="text-align: center"><h3>{{ $message }}</h3><hr></div>
							<div class="referidosBox">

								<p class="domicilioTitle">¿Quieres ganar domicilio gratis?</p>
								<p class="domicilioText">Invita a tus amigos a Merqueo y obtén {{ Config::get('app.referred.free_delivery_days') }} días de domicilios gratis por cada amigo que pida.</p>

								<input type="hidden" value="{{ web_url() }}/registro/{{ $user->referral_code }}" id="referral_code">
								<div class="row socialMedia">
									<div class="col-md-6 refSocial"><a class="socialLinks" id="facebook" href="https://www.facebook.com/dialog/share?app_id={{ Config::get('app.facebook_app_id') }}&display=popup&href={{ web_url() }}/registro/{{ $user->referral_code }}"><img src="{{ asset_url() }}img/order_success/facebook.png"/> <p>Facebook</p></a></div>
									<div class="col-md-6 refSocial"><a class="socialLinks" data-copy-to-clipboard="true" id="link"><img src="{{ asset_url() }}img/order_success/link.png"/> <p>Link</p></a></div>
								</div>
							</div>
							<div class="clearfix">
	                        	&nbsp;
	                        </div>
	                        <div class="clearfix">
	                        	&nbsp;
	                        </div>
							<div class="col-sm-12">
								<div class="col-sm-offset-4 col-sm-4 btns-payu">
									<button onclick="window.location = '{{ web_url() }}'" class="btn-payu btn-payu-go-home">Ir al inicio</button>
								</div>
								<div class="clearfix">
		                        	&nbsp;
		                        </div>
							</div>
						@endif
						<!-- <form class="form-horizontal" id="retry_payment_form" method="post" rol="form" action="{{route('PayU.retryTransactionSave')}}">
							<input type="submit" name="" value="submit">
						</form> -->
					</div>
				</div>

			</div>
		</div>
	</section>
	@include('checkout_cancel_modal')

	<!-- jQuery -->
	<script src="https://sdk.accountkit.com/es_ES/sdk.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js" type="text/javascript"></script>
	<script>
		var animation = bodymovin.loadAnimation({
			//container: document.getElementById('lottie'), // Required
			wrapper : document.getElementById('lottie'),
			animType : 'svg',
			path: '{{ asset_url() }}img/retry_payment/checked_done_.json', // Required
			//renderer: 'svg/canvas/html', // Required
			prerender : true,
			loop: false, // Optional
			autoplay: true, // Optional
			name: "loader", // Name for future reference. Optional.
		})
		$(document).ready(function(){
			$('#retry_payment_form').validate();

			$('#payment_method').on('change', function (){
				if($(this).val() == 'Tarjeta de crédito'){
					$('.retry-form_cc').show();
					$('.cc').addClass('required').val('');
					$('#address_further_cc').removeClass('required');
				}else{
					$('.retry-form_cc').hide();
					$('.cc').removeClass('required').val('');
					$('#expiration_cc').val('');
					$('#installments_cc').val('');
				}
				if($(this).val() == 'Débito - PSE'){
					$('.retry-form_pse').show()
					$('.pse_field').addClass('required').val('');
				}else{
					$('.pse_field').removeClass('required').val('');
					$('.retry-form_pse').hide()
				}
			});
			/*$('#retry-payment-button').on('click', function(event){
				event.preventDefault();
				if($('#retry_payment_form').valid()){
					alert('submit')
					event.default();
					$('#retry_payment_form').submit();
					//document.getElementById('retry_payment_form').submit();
				}
			})*/
		});
        $('#phone_validation_alert').hide();
        $('#phone_validation_success').hide();
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function(){
            AccountKit.init(
                {
                    appId: "{{ Config::get("app.facebook_app_id") }}",
                    state: "{{ Cache::get('js_version_number') }}",
                    version: "v1.0",
                    fbAppEventsEnabled: true
                }
            );
        };

        // login callback
        function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {
                var code = response.code;
                var csrf = response.state;
                // Send code to server to exchange for access token
                $.ajax({
                    url: '{{ route('frontUser.cellphoneValidation') }}',
                    type: 'POST',
                    data: { code: code },
                    dataType: 'json'
                })
				.done(function(response) {
					if (response.status){
						$('.phone_validation').hide();
						$('#phone_validation_success').html(response.message).show();
					}else{
						$('#phone_validation_alert').html(response.message).show();
					}
				});
            }
            else if (response.status === "NOT_AUTHENTICATED") {
                //console.log(response);
            }
            else if (response.status === "BAD_PARAMS") {
                //console.log(response);
            }
        }

        function copyToClipboard(element) {
            var $temp = $('<input/>');
            $('body').append($temp);
            $temp.val($(element).val()).select();
            document.execCommand('copy');
            $('#link').html('<img src="{{ asset_url() }}img/order_success/link.png"/> <p>Link copiado compartelo</p>');
            $temp.remove();
        }
        $(document).ready(function(){
        	/*$('#btn-payu-go-transaction').on('click', function(){
        		$('#user-data-form')[0].submit();
        	})*/
        })


		/*$(document).ready(function () {
            $('body').on('click', '.socialLinks', function(event) {
				var anchor = $(this);
                event.preventDefault();
                trackEvent('referral_code_shared', {channel: anchor.prop('id'), screen: pageName()});
				if (anchor.data('copy-to-clipboard')) {
                    copyToClipboard('#referral_code');
                    return true;
				}

				window.open(anchor.prop('href'), '', 'width=700,height=500');
			});
		});*/

	</script>


	
    @if(isset($user))
    <script>
        function smsLogin() {
            var phoneNumber = '{{ $user->phone }}';
            AccountKit.login(
                'PHONE',
                {countryCode: '+57', phoneNumber: phoneNumber},
                loginCallback
            );
        }
	</script>
	@endif
	

@stop
