<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Merqueo</title>
  <meta name="format-detection" content="telephone=no" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, target-densitydpi=medium-dpi, user-scalable=0" />
  <meta name="description" content="">
  <meta name="author" content="">

    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="js/less-1.3.3.min.js"></script-->
    <!--append ‘#!watch’ to the browser URL, then refresh the page. -->
    <link href='//fonts.googleapis.com/css?family=Crete+Round:400,500,400italic|Open+Sans:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Noto+Sans:400,500,700,400italic' rel='stylesheet' type='text/css'>
    <link href="{{ asset_url() }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset_url() }}/css/style.css" rel="stylesheet">
    <link href="{{ asset_url() }}/css/base.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->

  <link rel="apple-touch-icon" sizes="57x57" href="{{ asset_url() }}img/icon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="{{ asset_url() }}img/icon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="{{ asset_url() }}img/icon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset_url() }}img/icon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="{{ asset_url() }}img/icon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="{{ asset_url() }}img/icon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="{{ asset_url() }}img/icon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="{{ asset_url() }}img/icon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset_url() }}img/icon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset_url() }}img/icon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset_url() }}img/icon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ asset_url() }}img/icon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset_url() }}img/icon/favicon-16x16.png">
  <link rel="manifest" href="{{ asset_url() }}img/icon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{ asset_url() }}img/icon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <script>
    // Javascript Vars
    var fb_app_id = '{{ Config::get("app.facebook_app_id") }}';
    var web_url = '{{ web_url() }}';
    var google_api_key = '{{ Config::get("app.google_api_key") }}';
  </script>

  @include('common/analytics')

  <script type="text/javascript" src="{{ asset_url() }}js/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/jquery.lazyload.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/headroom.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/geocode.js"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="{{ asset_url() }}js/general.js"></script>
</head>

<body>

  <div class="top-head-fix">
    <nav class="pro navbar navbar-default">
      <div class="navbar-header">
        <a class="navbar-brand border-right" href="{{ web_url() }}"><img src="{{ asset_url() }}img/logo-header-x.png"></a>
      </div>
    </nav>
  </div>

<div align="center" class="container-fluid pro-bg-no col-md-12 col-xs-12">
    <div class="modal-header green-title">
        <h3 class="modal-title">Descargar App</h3>
    </div>
    <br>
    <a href="{{ $android_url }}"><img src="{{ asset_url() }}img/googleplay.png" width="250"></a><br><br>
</div>

</body>
</html>
