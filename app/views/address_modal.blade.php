@if (!isset($no_address_modal))
<script>
    var city_lat = {{ $store->city_lat }};
    var city_lng = {{ $store->city_lng }};
</script>
<style type="text/css">
    #map-point {
        width: 100%;
        height: 400px;
        background-color: grey;
    }

</style>
<div class="modal fade" id="address-city" data-coverage="{{ Session::get('coverage') }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="mdl-dialog">
            <div class="bluebg">
                <h4 class="mdl-dialog__title">Tu Dirección</h4>
            </div>
            <div class="mdl-dialog__content">
              <div class="select_address">
                @if ($user_addresses)
                <div class="user_addresses">
                    <h4>Selecciona una dirección: <div class="addressnew pull-right btn btn-info">Nueva dirección</div></h4>
                    <input type="hidden" name="address_id" value="">
                    <div class="addresslist">
                    @foreach($user_addresses as $addr)
                    <div class="bg-info">
                      <a class="address btn btn-default btn-sm pull-right addressitemsave" data-id="{{ $addr['id'] }}" href="javascript:;">+ Enviar a esta dirección</a>
                      <b>{{ $addr['label'] }}</b><br>
                      {{ $addr['address'] }}
                      @if (!empty($addr['address_further']))<br>{{ $addr['address_further'] }} @endif<br>
                      <font color="grey">{{ $addr['city'] }}</font><br>
                    </div><hr>
                    @endforeach
                    </div>
                    <div class="loading_user">
                      <br/><img src="{{ asset_url() }}img/hourglass.gif">
                    </div>
                </div>
                @endif

                <div class="unseen alert alert-danger form-has-errors"></div>

                <div class="directaddress">
                  @if ($user_addresses)
                  <div class="btn btn-info addressnewform">Usar una de mis direcciones guardadas</div><br><br>
                  @endif
                  <p>Digita la dirección para validar cobertura</p>
                  <div class="estructura-direccion-wrapper" align="center">
                    <div class="form-group">
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label style="float:left">Ciudad</label>
                                <select name="city_id" class="form-control" id="city_id">
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}" @if($city->id == Session::get('city_id')) selected="selected" @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @if (count($surrounded_cities['surrounded_cities']))
                            <div class="col-xs-6">
                                <label style="float:left">Zona</label>
                                <select name="surrounded_city_id" class="form-control" id="surrounded_city_id">
                                    <option value="{{ $surrounded_cities['city']->id }}" selected="selected">{{ $surrounded_cities['city']->city }}</option>
                                    @foreach($surrounded_cities['surrounded_cities'] as $surrounded_city)
                                    <option value="{{ $surrounded_city->id }}">{{ $surrounded_city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-3" style="width: 30%">
                                <select name="dir1" class="form-control" id="dir1">
                                    <option value="Calle">Calle</option>
                                    <option value="Carrera">Carrera</option>
                                    <option value="Avenida">Avenida</option>
                                    <option value="Avenida Carrera">Avenida Carrera</option>
                                    <option value="Avenida Calle">Avenida Calle</option>
                                    <option value="Circular">Circular</option>
                                    <option value="Circunvalar">Circunvalar</option>
                                    <option value="Diagonal">Diagonal</option>
                                    <option value="Manzana">Manzana</option>
                                    <option value="Transversal">Transversal</option>
                                    <option value="Vía">Vía</option>
                                </select>
                            </div>
                            <div class="col-xs-3" style="width: 20%">
                                <input name="dir2" class="form-control" id="dir2" autocomplete="off" type="text">
                            </div>
                            <div class="col-xs-1" style="width: 4%">
                                <label class="form-label">#</label>
                            </div>
                            <div class="col-xs-3" style="width: 20%">
                                <input name="dir3" class="form-control" id="dir3"  autocomplete="off" type="text">
                            </div>
                            <div class="col-xs-1" style="width: 4%">
                                <label class="form-label">-</label>
                            </div>
                            <div class="col-xs-3" style="width: 20%">
                                <input name="dir4" class="form-control" id="dir4"  autocomplete="off" type="text">
                            </div>
                        </div>
                        <input type="hidden" name="lat" div="form-group div-lat width: 8%;" class="form-control" id="lat">
                        <input type="hidden" name="lng" div="form-group div-lng width: 8%;" class="form-control" id="lng">
                        <input type="hidden" name="readable" div="form-group div-readable width: 8%;" class="form-control" id="readable">
                     </div>
                  </div>
                  <div class="loading">
                    <img src="{{ asset_url() }}img/hourglass.gif">
                  </div>
                  <button class="button btn-success btn small btn-validate-address">Validar</button>
                </div>

                <div class="errorcover">
                  <div class="h3 red"></div>
                  <a href="javascript:;" class="button close-modal btn btn-success" data-dismiss="modal">Aceptar</a>
                </div>
              </div>
              <div class="addresses hide">
                <div>
                    <br><h4 style="text-align: center !important;">Encontramos tu dirección en 2 zonas diferentes selecciona la correcta.</h4>
                    <div class="addresslist"></div>
                </div>
              </div>
              <div class="mapsaddress">
                <p>¡Opps! No encontramos tu dirección</p>
                <div id="map"></div>
                <p>Arrastra el ícono en el mapa hasta tu ubicación para poder validar la cobertura</p>
              </div>
              <div class="confirm-mapsaddress hide" style="text-align: center;">
                <div id="map-point"></div>
                <br/>
                <span class="glyphicon glyphicon-map-marker"></span>&nbsp;<span class="confirm-address-text"></span><br>
                <p class="confirm-text" style="font-size: 18px;">¿Tu dirección coincide con la ubicación del Pin?</p>
                <p class="text-not-match hide" style="font-size: 18px;">Mueve el mapa y ubica el pin en la dirección correcta</p>
                <div class="row">
                    <button class="btn btn-default btn-sm" id="go-back">Volver</button>&nbsp;&nbsp;
                    <button class="btn btn-default btn-sm" id="not-match-address">No coincide</button>&nbsp;&nbsp;
                    <button class="btn btn-default btn-sm match-address" id="match-address" style="background-color: #e91a58;color: #fff;">Sí coincide</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="mdl-dialog__actions">
              <button type="button" class="mdl-button close" data-dismiss="modal"><i class="material-icons">clear</i></button>
            </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endif