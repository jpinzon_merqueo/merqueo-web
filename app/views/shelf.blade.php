@extends('layout')

@section('title'){{ $data['shelf']['name'] }} en {{ $store->name }} - Merqueo.com @stop
@section('description'){{ $data['shelf']['name'] }} en {{ $store->name }}. Puedes pagar con tarjeta o en efectivo contra entrega. @stop

@section('content')

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "{{ $store->name }}",
  "logo": "{{ $store->logo_url }}",
  "url": "{{ web_url() }}/{{ $store->city_slug }}/domicilios-{{ $store->slug }}",
  "contactPoint" : [{
        "@type" : "ContactPoint",
        "telephone" : "{{ $store->phone }}",
        "contactType" : "Teléfono Domicilios"
      }]
  }
}
</script>

<div class="col-md-12 content_list_data" style="margin-top: 115px">
	<div class="row col-xs-12 clearfix pro-row">

	    <div class="col-md-12">
            <div class="col-md-12">
                <div class="text_hora">
                    <h1>{{ $data['shelf']['name'] }}</h1>
                </div>
            </div>
        </div>

		<ol class="breadcrumb" data-catrel="{{$data['store']['id']}}"  itemscope itemtype="http://schema.org BreadcrumbList">
		  {{ Breadcrumb::render('>', $data['store']['name'], 2, $store->city_name) }}
		</ol>

		<div class="col-md-12 product-page-title">
			<h2><a class="head" href="#">{{ $data['shelf']['name'] }}</a> <span></span></h2>
		</div>
		@if ($data['shelf']['description'] != '')
	    <div class="category-msg alerts">
	      <p>{{ $data['shelf']['description'] }}</p>
	    </div>
	  	@endif
		<div class="col-md-12 col-sm-12 col-xs-12">
		{{ View::make('elements.products-list', ['products' => $data['shelf']['products'], 'store' => $data['store'], 'variation_key' => $variation_key]) }}
		</div>
	</div>
</div>
@stop