@extends('emails/email_layout')

@section('content')

<tr>
  <td>
    <table  width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td  style="text-align: center;">
            <img src="{{ asset_url() }}img/mail/header.png" width="100%" alt="">
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>
<tr>
  <td style="border-bottom: 1px solid #f2eeed;padding: 30px 30px 30px 30px;">
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
                  <![endif]-->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td style="text-align: center" class="bodycopy" style="font-size: 16px; line-height: 22px;">
                          <h3 style="color: #153643; font-family: sans-serif;font-size: 17px; line-height: 22px;text-align: center">Hola {{ $user['name'] }}</h3>
                          <p style="color: #153643; font-family: sans-serif;font-weight: lighter;font-size: 14px; line-height: 22px;">
                            Pide hoy sin costo de domicilio.
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: center;">
                          <img src="{{ asset_url() }}img/mail/check.png" alt="">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" margin-top: 30px">
                    <tbody>
                      <tr>
                        {{-- <td colspan="2" class="bodycopy" style="font-size: 16px; line-height: 22px;text-align: center">
                         <p style="background-color: #2c2f3a; color:#FFF; padding: 5px; font-family: sans-serif;font-weight: lighter;font-size: 14px; line-height: 22px; font-weight: bold;margin: 0">Usuario referido</p>
                       </td> --}}
                     </tr>
                     <tr>
                      <td style="padding: 30px 10px 0 0;">
                        <p style="color: #153643; font-family: sans-serif;font-size: 12px; line-height: 22px">
                          Queremos informarte que puedes realizar tu pedido sin costo de domicilio.
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-top: 30px;">
                        <p style="color: #153643; font-family: sans-serif;font-size: 12px; line-height: 22px">Si tienes alguna duda cont&aacute;ctanos en <strong><a style="text-decoration: none; color: #2c2f3a" href="mailto:{{ $contact_email }}">{{ $contact_email }}</a></strong> o ll&aacute;manos al <strong>{{ $contact_phone }}</strong> de lunes a domingo, de <strong>8 am a 9 pm.</strong></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" margin-top: 30px">
                  <tbody>
                    <tr>
                      <td></td>
                      <td>
                       <hr>
                     </td>
                     <td></td>
                   </tr>
                   <tr>
                    <td style="padding: 15px 5px; width: 33%">
                      <p style="text-align: center;"><img style="width: 80%" src="{{ asset_url() }}img/mail/dondeEstes.png" alt=""></p>
                    </td>
                    <td style="padding: 15px 5px; width: 33%">
                      <p style="text-align: center;"><img style="width: 80%" src="{{ asset_url() }}img/mail/coche.png" alt=""></p>
                    </td>
                    <td style="padding: 15px 5px; width: 33%">
                      <p style="text-align: center;"><img style="width: 80%" src="{{ asset_url() }}img/mail/bolsa.png" alt=""></p>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 5px; width: 33%">
                      <h3 style="color: #153643; font-family: sans-serif;font-size: 17px; line-height: 22px; text-align: center;">D&oacute;nde est&eacute;s</h3>
                      <p style="color: #153643; font-family: sans-serif;font-weight: lighter;font-size: 12px; line-height: 22px; text-align: center;">Pide desde tu celular a los principales supermercados de Colombia</p>
                    </td>
                    <td style="padding: 0 5px; width: 33%">
                      <h3 style="color: #153643; font-family: sans-serif;font-size: 17px; line-height: 22px; text-align: center">Efic&aacute;z</h3>
                      <p style="color: #153643; font-family: sans-serif;font-weight: lighter;font-size: 12px; line-height: 22px; text-align: center;">Uno de nuestros compradores lo seleccionar&aacute; cuidadosamente por ti. </p>
                    </td>
                    <td style="padding: 0 5px; width: 33%">
                     <h3 style="color: #153643; font-family: sans-serif;font-size: 17px; line-height: 22px; text-align: center;">A tu puerta</h3>
                     <p style="color: #153643; font-family: sans-serif;font-weight: lighter;font-size: 12px; line-height: 22px; text-align: center;">Paga con tarjeta de cr&eacute;dito directo en la aplicaci&oacute;n, con dat&aacute;fono o efectivo.</p>
                   </td>
                 </tr>
               </tbody>
             </table>
           </td>
         </tr>

         @stop