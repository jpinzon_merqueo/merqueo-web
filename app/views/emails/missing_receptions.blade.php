@extends('emails/email_layout')

@section('content')

<tr>
    <td>
        <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Ordenes de compra no recibidas ({{ $now }})</h1><br>
        <table style="font-family: sans-serif; width: 100%; border: 1px solid grey">
        	<thead>
	        	<tr>
	                <th align="center" style="border-right: 1px solid grey;">
	                    Orden de compra #
	                </th>
	                <th align="left" style="border-right: 1px solid grey;">
	                	Proveedor
	                </th>
	                <th align="left" style="border-right: 1px solid grey;">
	                	Fecha de creación
	                </th>
	                <th align="left" style="border-right: 1px solid grey;">
	                	Fecha de entrega
	                </th>
	                <th align="center">
	                	Unidades solicitadas
	                </th>
	            </tr>
        	</thead>
        	<tbody>
				@foreach ($provider_orders as $provider_order)
					<tr>
					    <td align="center" style="border-right: 1px solid grey; border-top: 1px solid grey">
					    	{{ $provider_order->id }}
					    </td>
					    <td align="left" style="border-right: 1px solid grey; border-top: 1px solid grey">
					    	{{ $provider_order->provider_name }}
					    </td>
					    <td align="left" style="border-right: 1px solid grey; border-top: 1px solid grey">
					    	{{ date("d M Y g:i a", strtotime($provider_order->date)) }}
					    </td>
					    <td align="left" style="border-right: 1px solid grey; border-top: 1px solid grey">
					    	{{ date("d M Y g:i a", strtotime($provider_order->delivery_date)) }}
					    </td>
					    <td align="center" style="border-top: 1px solid grey">
							{{ $provider_order->quantity_order }}
					    </td>
					</tr>
				@endforeach
        	</tbody>
        </table>
        <br>
        <br>
    </td>
</tr>

@stop