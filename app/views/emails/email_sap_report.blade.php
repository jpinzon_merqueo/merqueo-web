@extends('emails/email_layout')
@section('content')
    <p>Descarga la plantilla en: </p>
    <a href="{{ $url }}" title="Link de descarga">{{ $url }}</a>.
@stop