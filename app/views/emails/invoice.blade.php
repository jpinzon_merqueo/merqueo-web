@extends('emails/email_layout_noheader')

@section('content')
    <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; font-family: Helvetica, Arial, sans-serif;"
           class="scaleForMobile">
        <tr>
            <td width="100%">

                <!-- Space -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                    <tr>
                        <td width="100%">
                        </td>
                    </tr>
                </table>

                <!-- Header -->
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <img width="550" style="margin-bottom: 10px" src="{{ asset_url() }}img/email_design/20180710/FACTURA.png" alt="card img">
                        </td>
                    </tr>
                </table><!-- End Header -->
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="background-color: #f7f7f7; color: #494d54; padding: 20px 20px 40px">
                    <tr>
                        <td align="center">
                            <a href="{{ $url_download }}" style="border: none; background-color: #d0006f; color: white; padding: 15px 30px; text-decoration: none; border-radius: 20px">DESCARGAR AHORA</a>
                        </td>
                    </tr>
                </table>
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="background-color: #f7f7f7; color: #494d54;">
                    <tr>
                        <td align="center">
                            <p style="font-size: 14px; margin: 0">Si necesitas ayuda, comunícate con nosotros al correo
                                electrónico</p>
                            <p style="font-size: 14px; margin: 0"><b>{{ Config::get('app.admin_email') }}</b></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@stop