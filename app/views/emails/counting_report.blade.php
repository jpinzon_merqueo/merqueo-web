@extends('emails/email_layout')

@section('content')

<tr>
    <td>
        <table style="font-family: sans-serif"> 
            <tr>
                <td valign="top"><br>
                    <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Conteo # {{ $id }}</h1>
                    <p><b>Bodega:</b> {{ $warehouse }}</p>
                    <p><b>Tipo:</b> {{ $countingType }}</p>
                    <p><b>Tipo almacenamiento:</b> {{ $storageType }}</p>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>   

@stop