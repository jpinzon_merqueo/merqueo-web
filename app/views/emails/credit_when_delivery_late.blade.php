@extends('emails/email_layout')

@section('content')

<tr>
  <td>
    <table  width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td  style="text-align: center;">
            <!--<img src="{{ asset_url() }}img/mail/header.png" width="100%" alt="">-->
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>
<tr>
  <td style="border-bottom: 1px solid #f2eeed;padding: 30px 30px 30px 30px;">
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
                  <![endif]-->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td style="text-align: center" class="bodycopy" style="font-size: 16px; line-height: 22px;">
                          <h3 style="color: #153643; font-family: sans-serif;font-size: 17px; line-height: 22px;text-align: center">Hola {{ $name }}</h3>
                          <p style="color: #153643; font-family: sans-serif;font-weight: lighter;font-size: 14px; line-height: 22px;">
                              Discúlpanos por ir tarde.
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" margin-top: 10px">
                    <tbody>

                     <tr>
                      <td style="padding: 10px 10px 0 0;">
                        <p style="color: #153643; font-family: sans-serif;font-size: 12px; line-height: 22px">
                            Agregamos {{ $amount }} por la demora en la entrega, para que los uses en tu próximo pedido. Revisa el módulo de créditos.
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-top: 30px;">
                        <p style="color: #153643; font-family: sans-serif;font-size: 12px; line-height: 22px">Si tienes alguna duda cont&aacute;ctanos en <strong><a style="text-decoration: none; color: #2c2f3a" href="mailto:{{ $contact_email }}">{{ $contact_email }}</a></strong></strong></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
           </td>
         </tr>

         @stop