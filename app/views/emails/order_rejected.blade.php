@extends('emails/email_layout')
@section('title')Merqueo | Cancelado @stop
@section('content')

  <!-- HEADER -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
    <tr>
      <td width="100%">
        <!-- Header -->
        <table style="padding: 30px 0;" width="700px"  bgcolor="#e53046" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td style="font-size: 14px; color: #FFFFFF; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" width="100%">
              <center><img src="{{ asset_url() }}img/email_design/20180418/icono-pedido-cancelado.png" alt=""></center>
              <h1 style="font-weight: normal; font-size: 22pt;">TU PEDIDO HA SIDO<br><span style="font-weight: bold">CANCELADO</span></h1>
              <p style="font-size: 14pt;">Número: {{ $order->reference }}</p>
              <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20180418/location.png" alt="">Entrega en {{ $order_group->user_address.' '.$order_group->user_address_further }}</p>
              <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20170727/calendar.png" alt="">Horario entre {{ $order->delivery_time }}</p>
              @if($order->payment_method == 'Tarjeta de crédito' || $order->payment_method == 'Tarjeta de crédito' || $order->payment_method == 'Datáfono')
                <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20170727/card.png" alt="">Método de pago {{ $order->payment_method }}</p>
              @elseif($order->payment_method == 'Efectivo' || $order->payment_method == 'Efectivo y datáfono')
                <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20180418/cash.png" alt="">Método de pago {{ $order->payment_method }}</p>
              @endif
            </td>
          </tr>
        </table><!-- End Header -->

      </td>
    </tr>
  </table><!-- End Main -->

  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
    <tr>
      <td width="100%">
        <!-- Space -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
          <tr>
            <td width="100%" height="30">
            </td>
          </tr>
        </table>

        <table  width="700px" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tbody>
          <tr><td>
              <h1 style="font-size: 20px; color: #3f3f3f; font-family: Helvetica, Arial, sans-serif">Resumen del pedido</h1>
              <!-- 1st 3 Images -->
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #545454; font-family: Helvetica, Arial, sans-serif; font-size: 14px;">
                <tr style="text-align: center; font-weight: bold">
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 20%;">Imagen</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 40%;">Producto</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 10%;">Cantidad</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 15%; text-align: right">Precio</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 15%; text-align: right">Total</td>
                </tr>
                @if (!empty($products_not_available['products']) )
                  @foreach($products_not_available['products'] as $product)
                    <tr height="70">
                      <td style="text-align: center; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">
                        <img style="max-width: 100px" src="{{ $product['img'] }}" width="auto" height="100px" alt="">
                      </td>
                      <td style="@if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}</td>
                      <td style="text-align: center; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">{{ $product['qty_original'] }}</td>
                      <td style="text-align: right; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">${{ number_format($product['price'], 0, ',', '.') }}</td>
                      <td style="text-align: right; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">${{ number_format($product['total'], 0, ',', '.') }}</td>
                    </tr>
                  @endforeach
                @endif
                @if ( !empty($products_available['products']) )
                  @foreach($products_available['products'] as $product)
                    <tr height="70">
                      <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;">
                        <img style="max-width: 100px" src="{{ $product['img'] }}" width="auto" height="100px" alt="">
                      </td>
                      <td style="border-bottom: 1px solid #ddd; padding: 5px; width: 40%;">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}
                        @if($product['qty_original'] > $product['qty'])
                          <br><p style="color:#ff0000;">
                            <img src="{{ asset_url() }}img/email_design/20180418/no-disponible.png">
                            {{ $product['qty_original'] - $product['qty'] }}
                            @if(($product['qty_original'] - $product['qty']) == 1) unidad no disponible @else unidades no disponibles @endif
                          </p>
                        @endif
                      </td>
                      <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px; width: 10%;">{{ $product['qty_original'] }}</td>
                      <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; width: 15%;">${{ number_format($product['price'], 0, ',', '.') }}</td>
                      <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; width: 15%;">${{ number_format($product['total'], 0, ',', '.') }}</td>
                    </tr>
                  @endforeach
                @endif
                <tr height="30">
                  <td colspan="4" style="text-align: left; padding: 5px; font-weight: bold">Subtotal</td>
                  <td style="text-align: right; padding: 5px">${{ number_format($order->total_amount, 0, ',', '.') }}</td>
                </tr>
                <tr height="30">
                  <td colspan="4" style="text-align: left; padding: 5px; font-weight: bold">Costo domicilio</td>
                  <td style="text-align: right; padding: 5px">${{ number_format($order->delivery_amount, 0, ',', '.') }}</td>
                </tr>
                <tr height="30">
                  <td colspan="4" style="text-align: left; padding: 5px; font-weight: bold">Descuento</td>
                  <td style="text-align: right; padding: 5px">@if ( !empty( $order->discount_amount ) )-@endif${{ number_format($order->discount_amount, 0, ',', '.') }}</td>
                </tr>
                <tr height="30">
                  <td colspan="4" style="border-top: 1px solid #ddd; text-align: left; font-size: 18px; padding: 5px; color: #3f3f3f;"><p><b>Total</b></p></td>
                  <td style="border-top: 1px solid #ddd; text-align: right; font-size: 18px; font-weight: bold; padding: 15px 5px; color: #073349; color: #3f3f3f;">${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}</td>
                </tr>
              </table>

            </td></tr>
          </tbody>
        </table>
      </td>
    </tr>
  </table><!-- End 3 Columns -->

  <!-- Products CTA -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
    <tr>
      <td width="100%">

        <!-- Space -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="MobileCenter">
          <tr>
            <td width="100%" height="25">
            </td>
          </tr>
        </table>

        <!-- Icons -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td width="100%">
              <!-- Icons -->
              <table style="padding: 2% 0;" width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                <tr>
                  <td width="100%" style="text-align: center;">
                    <p style="text-align: center; color: #3f3f3f; font-size: 18px; font-weight: 100; font-family: Helvetica, Arial, sans-serif; margin: 0;">Sí necesitas ayuda, comun&iacute;cate con nosotros al correo electr&oacute;nico<br><b>{{ Config::get('app.admin_email') }}<b></p><br>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table><!-- Products CTA -->

      </td>
    </tr>
  </table><!-- Products CTA -->
@stop
