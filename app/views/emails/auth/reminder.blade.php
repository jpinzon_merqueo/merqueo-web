@extends('emails/email_layout2_i18n')
@section('content')
    <table width="590" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; color: #545454"
           class="scaleForMobile">
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <h2 style="margin-bottom: 0"><b>{{ $username }}</b></h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="font-size: 18px">Si olvidaste tu contraseña !no te preocupes!</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <img width="300" src="{{ asset_url() }}img/email_design/contrasena.gif"
                                 alt="Remember password">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="font-size: 18px">Haz click en el siguiente enlace y restablece tu contraseña</p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="{{ $country_data['web_view'] }}/restablecer-clave/{{ $token }}"
                               style="text-decoration: none; color: white; background: #d0006f; padding: 15px; font-size: 16px; border-radius: 40px"
                               target="_blank">RESTABLECER CONTRASEÑA</a>
                        </td>
                    </tr>
                    <tr>
                        <td height="40"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="font-size: 13px">

                                @if($country_data['country_code'] === 'co')
                                    Este enlace expirará en 10 días. Si el enlace no funciona,
                                @else
                                    Este enlace tiene vigencia por 10 días. Si no funciona,
                                @endif  

                                por favor copia y pega la URL en tu navegador. Si no has realizado esta petición puedes
                                ignorar este mensaje y la contraseña seguirá siendo la misma. Si tienes alguna duda
                                contáctanos en <b>{{ $contact_email }}</b>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@stop