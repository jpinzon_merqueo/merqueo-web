@extends('emails/email_layout_noheader')

@section('content')
    <table width="590" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; color: #545454"
           class="scaleForMobile">
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <img style="max-width: 100%" src="{{ asset_url() }}img/email_design/referidos_20mil.png"
                                 alt="Header-bg">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <p style="font-size: 18px; margin: 0"> ¡Hola!, Te regalo $20.000 para tu próximo 
                    <b style="text-transform: uppercase; color: black"></b> 
                </p>
                <p style="margin: 0; font-size: 18px">
                    pedido en Merqueo.com.  
                    <b style="color: black"> Es muy fácil,</b> 
                    descarga la app,
                </p>
                <p style="margin: 0; font-size: 18px">
                    ingresa tus productos al carrito y escribe el siguiente
                </p>
                <p style="margin: 0; font-size: 18px">
                    código al finalizar tu pedido:
                </p>
            </td>
        </tr>
        <tr>
            <td height="30"></td>
        </tr>

        <tr>
            <td align="center">
                <p style="border: 5px solid;border-radius: 50px;width: 200px;height: 50px;font-size: 27px"> 
                    <b style="color: black; vertical-align:sub;"> {{$referal_code}} </b>
                </p> 
            </td>
        </tr>
       
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <a href="{{ $referral_url }}">
                                <img style="max-width: 100%" src="{{ asset_url() }}img/email_design/redime_20mil.png"
                                 alt="btn-redime">
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td height="40"></td>
        </tr>

        <tr>
            <td align="center">
                <p style="font-size: 18px; margin: 0"> 
                    Consulta los términos y condiciones en www.merqueo.com/terminos , 
                </p>
                <p style="font-size: 18px; margin: 0"> 
                    ante cualquier inquietud puedes escribir a nuestro correo electrónico 
                </p>
                <p style="font-size: 18px; margin: 0"> 
                    servicioalcliente@merqueo.com.
                </p>
            </td>
        </tr>



    </table>
@stop