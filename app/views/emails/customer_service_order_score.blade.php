@extends('emails/email_layout')

@section('content')
    <tr>
        <td>
            <table style="font-family: sans-serif">
                <tr>
                    <td valign="top">
                        <br>
                        <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Calificacion de pedido # {{ $order->id }}</h1>
                        <br><br>
                        <b>ID del pedido: </b> {{ $order->id }}<br>
                        <b>Email del usuario: </b> {{ $user->email }}<br>
                        <b>Nombre del usuario: </b> {{ $user->first_name }} {{ $user->last_name }}<br>
                        <b>Valor del pedido: </b>${{ number_format(($order->total_amount + $order->delivery_amount - $order->discount_amount), 0, ',', '.') }}<br>
                        <b>Fecha de ingreso del pedido: </b> {{ $order->created_at }}<br>
                        <b>Fecha de entrega del pedido: </b> {{ $order->management_date }}<br>
                        <b>Calificacion: </b> {{ 'Negativa' }}<br>
                        <b>Comentarios del pedido: </b> {{ $order->user_score_comments }}<br>
                        <b>Dispositivo: </b>{{ $type }}<br>
                        @if (!empty($order->user_score_typification))
                            <b>Tipificación: </b>
                            <ul>
                                @foreach(explode(';', $order->user_score_typification) as $type)
                                    <li>{{ $type }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <br/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@stop