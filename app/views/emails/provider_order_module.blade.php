@extends('emails/email_layout')

@section('content')
    @if (!empty($content))
        <p>{{ $content }}</p>
    @endif
@endsection