@extends('emails/email_layout')

@section('content')
    <p>Buen día</p>
    <p>Adjunto envió orden de compra, por favor confirmar recibido y fecha de entrega.</p>
    @if (!empty($content))
        <p>{{ $content }}</p>
    @endif
    <p>Gracias.</p>
@endsection