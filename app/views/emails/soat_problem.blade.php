@extends('emails/email_layout')

@section('content')
    <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; font-family: Helvetica, Arial, sans-serif;"
           class="scaleForMobile">
        <tr>
            <td width="100%">

                <!-- Space -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                    <tr>
                        <td width="100%">
                        </td>
                    </tr>
                </table>

                <!-- Header -->
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="padding: 0 40px; color: #fff; background: #38466e">
                    <tr>
                        <td width="100%" align="center">
                            <p style="font-size: 40px; margin: 30px auto 20px; line-height: 1">
                                ¡Tuvimos problemas<br>
                                <b>con tu compra!</b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <img width="120" style="padding: 5px; margin-bottom: 10px" src="{{ asset_url() }}img/email_design/20180613/error-soat.png" alt="card img">
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="center">
                            <p style="margin: 0 auto 30px; font-size: 16px;">
                                Tu transacción no se ha podido efectuar correctamente <br>
                                debido a inconvenientes con tu método de pago seleccionado. <br>
                                Verifica con tu entidad bancaria y vuelve a intentarlo.
                            </p>
                        </td>
                    </tr>
                </table><!-- End Header -->
                <table width="550" height="3" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="background-color: #d0006f"></table>
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="background-color: #f7f7f7; color: #494d54; padding: 40px 21px">
                    <tr>
                        <td align="center">
                            <p style="font-size: 16px; color: #3f3f3f">Si necesitas ayuda, comunícate con nosotros al <br>
                                correo electrónico <b>servicioalcliente@merqueo.com</b></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@stop