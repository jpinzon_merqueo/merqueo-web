<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <title>Merqueo</title>
    <!-- Hotmail ignores some valid styling, so we have to add this -->
    <style type="text/css">
        /* Star hover using lang hack in its own style wrapper, otherwise Gmail will strip it out */
        * [lang~="x-star-wrapper"]:hover *[lang~="x-star-number"] {
            color: #119da2 !important;
            border-color: #119da2 !important;
        }

        * [lang~="x-star-wrapper"]:hover *[lang~="x-full-star"],
        * [lang~="x-star-wrapper"]:hover ~ *[lang~="x-star-wrapper"] *[lang~="x-full-star"] {
            display: block !important;
            width: auto !important;
            overflow: visible !important;
            float: none !important;
            margin-top: -1px !important;
        }

        * [lang~="x-star-wrapper"]:hover *[lang~="x-empty-star"],
        * [lang~="x-star-wrapper"]:hover ~ *[lang~="x-star-wrapper"] *[lang~="x-empty-star"] {
            display: block !important;
            width: 0 !important;
            overflow: hidden !important;
            float: left !important;
            height: 0 !important;
        }
    </style>

    <style type="text/css">
        /* iPad Text Smoother */
        div, p, a, li, td {
            -webkit-text-size-adjust: none;
        }

        /* This is the color of the very top and the footer: #00bd9c; */
        .ReadMsgBody {
            width: 100%;
            background-color: #ffffff;
        }

        .ExternalClass {
            width: 100%;
            background-color: #ffffff;
        }

        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
        }

        html {
            width: 100%;
            font-family: Helvetica, Arial, Sans-Serif;
        }

        table[class=fullWidth] {
            width: 100% !important;
        }

        img {
            max-width: 100%;
        }

        @media only screen and (max-width: 640px) {
            body {
                width: auto !important;
            }

            table[class=scaleForMobile] {
                width: 100% !important;
                padding-left: 30px !important;
                padding-right: 30px !important;
            }

            table[class=fullWidth] {
                width: 100% !important;
            }

            table[class=mobileCenter] {
                width: 100% !important;
                text-align: center !important;
            }

            td[class=mobileCenter] {
                width: 440px !important;
                text-align: center !important;
            }

            td[class=responsiveFooter] {
                width: 100% !important;
                text-align: center !important;
            }

            span[class=eraseForMobile] {
                width: 0;
                display: none !important;
            }

            td[class=eraseForMobile] {
                width: 0;
                display: none !important;
            }

            td[class=footerText] {
                text-align: center !important;
                padding: 6% 0;
            }

            table[class=eraseForMobile] {
                width: 0;
                display: none !important;
            }

            table[class=headerImageTable] {
                width: 100%;
                text-align: left !important;
            }

            img[class=middleImage] {
                width: 184px !important;
                height: auto !important;
            }

            td[class=pad1] {
                width: 100%;
                height: 50px !important;
            }

            td[class=pad2] {
                width: 100%;
                height: 20px !important;
            }

            td[class=pad3] {
                width: 100%;
                height: 30px !important;
            }

            td[class=pad4] {
                width: 100%;
                height: 40px !important;
            }

            td[class=iconScale] {
                text-align: right !important;
            }

            table[class=round] {
                width: 146px !important;
                border-radius: 100% !important;
                text-align: center !important;
            }

            td[class=navTd] {
                width: 33% !important;
                text-align: center !important;
            }
        }

        @media only screen and (max-width: 479px) {
            body {
                width: auto !important;
            }

            table[class=scaleForMobile] {
                width: 100% !important;
                padding-left: 30px !important;
                padding-right: 30px !important;
            }

            table[class=fullWidth] {
                width: 100% !important;
            }

            table[class=mobileCenter] {
                width: 100% !important;
                text-align: center !important;
            }

            td[class=mobileCenter] {
                width: 280px !important;
                text-align: center !important;
            }

            span[class=eraseForMobile] {
                width: 0;
                display: none !important;
            }

            td[class=eraseForMobile] {
                width: 0;
                display: none !important;
            }

            td[class=responsiveFooter] {
                width: 100% !important;
                text-align: center !important;
            }

            td[class=footerText] {
                text-align: center !important;
                padding: 6% 0;
            }

            table[class=eraseForMobile] {
                width: 0;
                display: none !important;
            }

            td[class=pad1] {
                width: 100%;
                height: 50px !important;
            }

            img[class=imageScale] {
                width: 100% !important;
            }

            img[class=middleImage] {
                width: 100% !important;
                height: auto !important;
            }

            td[class=navTd] {
                width: 33% !important;
                text-align: center !important;
            }

            tr[class=navSmall] {
                display: none !important;
            }
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td width="100%" valign="top" bgcolor="#ffffff">
            <!-- Header Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="d0006f" style="background-color: #d0006f">
                <tr>
                    <td>
                        
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                               class="mobileCenter">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </table>

                        <!-- Logo -->
                        <table width="590" border="0" cellpadding="0" cellspacing="0" align="center"
                               class="mobileCenter">
                            <tr>
                                <td width="100%" valign="middle">

                                    <!-- Logo -->
                                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="left"
                                           class="mobileCenter">
                                        <tr>
                                            <td valign="middle" align="center" width="100%" class="mobileCenter">
                                                <a href="#"><img
                                                            src="{{ asset_url() }}img/email_design/logo-barra-superior.png"
                                                            alt="" border="0"></a>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table><!-- End Nav -->

                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                               class="mobileCenter">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table><!-- End Nav Mobile Wrapper -->

            @yield('content')

        </td>
    </tr>
</table>

<!-- Wrapper 2 -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
       style="background-color: #ffffff;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;"
       class="scaleForMobile">
    <tbody>
    <tr>
        <td width="100%">
            <!-- 1st 3 Images -->
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile"
                   bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td width="100%" style="padding: 30px 0 0">
                        <!-- Middle Image 1 -->
                        <table width="170" border="0" cellpadding="0" cellspacing="0" align="center"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: left;"
                               class="mobileCenter">
                            <tbody>
                            <tr>
                                <td width="33%" style="line-height: 1px; text-align: center;"><a
                                            href="{{ Config::get('app.facebook_url') }}" target="_blank"><img
                                                src="{{asset_url()}}img/email_design/20170727/face.png"
                                                style="width: 30px; height: auto;" alt="" border="0"></a></td>
                                <td width="33%" style="line-height: 1px; text-align: center;"><a
                                            href="{{ Config::get('app.twitter_url') }}" target="_blank"><img
                                                src="{{asset_url()}}img/email_design/20170727/twitter.png"
                                                style="width: 30px; height: auto;" alt="" border="0"></a></td>
                                <td width="33%" style="line-height: 1px; text-align: center;"><a
                                            href="{{ Config::get('app.instagram_url') }}" target="_blank"><img
                                                src="{{asset_url()}}img/email_design/20170727/insta.png"
                                                style="width: 30px; height: auto;" alt="" border="0"></a></td>
                            </tr>
                            </tbody>
                        </table><!-- End Image 1 -->
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="padding: 30px 0 0">

                        <!-- Middle Image 2 -->
                        <table width="530" border="0" cellpadding="0" cellspacing="0" align="center"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;"
                               class="scaleForMobile">
                            <tbody>
                            <tr>
                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center"
                                       style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;"
                                       class="mobileCenter">
                                    <tbody>
                                    <tr>
                                        <td class="footerText" style="text-align: center;"><p
                                                    style="font-size: 14px;color: #545454;font-family: Helvetica, Arial, sans-serif;line-height: 4px;margin-right: 3%;">
                                                Descarga la aplicación</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="padding: 30px 0 0">
                        <!-- Middle Image 2 -->
                        <table width="530" border="0" cellpadding="0" cellspacing="0" align="center"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;"
                               class="scaleForMobile">
                            <tbody>
                            <tr>
                                <td width="100%" class="responsiveFooter">
                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center"
                                           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;"
                                           class="mobileCenter">
                                        <tbody>
                                        <tr>
                                            <td width="25%" style="line-height: 1px; text-align: center; padding: 0 2%">
                                                <a href="{{ Config::get('app.ios_url') }}" target="_blank"><img
                                                            src="{{asset_url()}}img/email_design/20180418/appStore.png"
                                                            style="height: auto; width: 100%; margin-right: 3%; text-align: right;"
                                                            alt="" border="0"></a></td>
                                            <td width="25%" style="line-height: 1px; text-align: center; padding: 0 2%">
                                                <a href="{{ Config::get('app.android_url') }}" target="_blank"><img
                                                            src="{{asset_url()}}img/email_design/20180418/androidStore.png"
                                                            style="height: auto; width: 100%; text-align: right;" alt=""
                                                            border="0"></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table><!-- End Image 2 -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- Logo -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter"
                   style="padding: 30px 0 0">
                <tr>
                    <td width="80%" valign="middle">

                        <!-- Logo -->
                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center"
                               class="mobileCenter">
                            <tr>
                                <td valign="middle" align="center" width="100%" class="mobileCenter">
                                    <a href="#"><img src="{{ asset_url() }}img/email_design/20180418/logo.png" alt=""
                                                     border="0"></a>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table><!-- End Nav -->
            <!-- End 1st 2 Images -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
                <tbody>
                <tr>
                    <td width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; color: #153643; font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 30px;"
                               class="mobileCenter">
                            <tbody>
                            <tr>
                                <td class="footerText" width="50%" style="line-height: 1px; text-align: center;"><p>
                                        <strong><a style="text-decoration: none; color: #153643;"
                                                   href="{{ Config::get('app.url') }}"
                                                   target="_blank">www.merqueo.com</a></strong></p><br>
                                    <p>Bogotá, Colombia</p></td>
                            </tr>
                            </tbody>
                        </table><!-- End Image 2 -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- Space -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                <tbody>
                <tr>
                    <td width="100%" height="20"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- End Wrapper -->
</body>
</html>