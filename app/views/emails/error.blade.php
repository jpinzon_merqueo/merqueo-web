@extends('emails/email_layout')

@section('content')

<tr>
    <td>
        <table style="font-family: sans-serif"> 
            <tr>
                <td valign="top"><br>
                    <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Error de API en producción</h1><br>
                    <br/>
                    <p><b>Fecha:</b>{{ $date }}</p>
                    <p><b>IP:</b>{{ $ip }}</p>
                    <br>
                    <p>{{ $message }}</p>
                    <br/>
                </td>
            </tr>
        </table>
    </td>
</tr>   

@stop