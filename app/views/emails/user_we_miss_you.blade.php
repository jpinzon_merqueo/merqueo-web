@extends('emails/email_layout')

@section('content')


<!-- HEADER -->
<table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
  <tr>
    <td width="100%">

      <!-- Space -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
        <tr>
          <td width="100%">
          </td>
        </tr>
      </table>

      <!-- Header -->
      <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
        <tr>
          <td width="100%">
            <a href="https://goo.gl/IbF365"><img style="display: block; margin: 0 auto; width: 100%; max-width: 700px;" src="{{ asset_url() }}img/email_design/header-bg-te-extranamos.jpg" alt=""></a>
          </td>
        </tr>
      </table><!-- End Header -->

    </td>
  </tr>
</table><!-- End Main -->

<!-- 3 Columns Middle Images -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
  <tr>
    <td width="100%" style="text-align: center;">

      <!-- Space -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
        <tr>
          <td width="100%" height="20">
          </td>
        </tr>
      </table>

      <!-- Message -->
      <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
        <tr>
        <td width="100%" style="font-size: 18px; color: #545454; text-align: center; font-family: Helvetica, Arial, sans-serif; line-height: 24px;">

            <p>Encuentra hasta <strong style="color: #e30044;">40 productos</strong> en promoción en nuestra tienda <strong>Euro</strong>.</p>

            <p>Productos que te pueden interesar:</p>
          </td>
        </tr>
      </table><!-- End Header -->

      <!-- Space -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
        <tr>
          <td width="100%" height="50">
          </td>
        </tr>
      </table>

      <!-- 1st 3 Images -->
      <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
        <tr>
          <td width="100%">
            <div style="display: inline-block;">
              <!-- Middle Image 1 -->
              <table width="200" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: left;" class="mobileCenter">
                <tr>
                  <td width="100%" style="line-height: 1px; text-align: center;">
                    <a href="#"><img src="{{ asset_url() }}img/email_design/1.png" width="48" height="auto" style="width: 48px; height: auto;" alt="" border="0"></a>
                  </td>
                </tr>
                <tr>
                  <td width="100%" height="20"></td>
                </tr>
                <tr>
                    <td width="100%" style="font-size: 18px; color: #757575; text-align: center; font-weight: 100; font-family: Helvetica, Arial, sans-serif; line-height: 30px;">Descarga la app
                    </td>
                  </tr>
                  <tr>
                    <td width="100%" style="font-size: 16px; color: #545454; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 24px;">o ingresa a <br> <strong><a style="text-decoration: none; color: #545454" href="https://merqueo.com/"> www.merqueo.com</a></strong>
                    </td>
                  </tr>
              </table><!-- End Image 1 -->
            </div>
              <!-- Space -->
              
            <div style="display: inline-block;">
              <!-- Middle Image 2 -->
              <table width="200" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: left;" class="mobileCenter">
                <tr>
                  <td width="100%" style="line-height: 1px; text-align: center;">
                    <a href="#"><img src="{{ asset_url() }}img/email_design/1.png" width="48" height="auto" style="width: 48px; height: auto;" alt="" border="0"></a>
                  </td>
                </tr>
                <tr>
                  <td width="100%" height="20"></td>
                </tr>
                <tr>
                    <td width="100%" style="font-size: 18px; color: #757575; text-align: center; font-weight: 100; font-family: Helvetica, Arial, sans-serif; line-height: 30px;">Agrega al carrito
                    </td>
                  </tr>
                  <tr>
                    <td width="100%" style="font-size: 16px; color: #545454; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 24px;">Selecciona tus productos favoritos
                    </td>
                  </tr>
              </table><!-- End Image 2 -->
            </div>
              <!-- Space -->
              
            <div style="display: inline-block;">
              <!-- Middle Image 3 -->
              <table width="200" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: left;" class="mobileCenter">
                <tr>
                  <td width="100%" style="line-height: 1px; text-align: center;">
                    <a href="#"><img src="{{ asset_url() }}img/email_design/1.png" width="48" height="auto" style="width: 48px; height: auto;" alt="" border="0"></a>
                  </td>
                </tr>
                <tr>
                  <td width="100%" height="20"></td>
                </tr>
                <tr>
                    <td width="100%" style="font-size: 18px; color: #757575; text-align: center; font-weight: 100; font-family: Helvetica, Arial, sans-serif; line-height: 30px;">Finaliza tu pedido
                    </td>
                  </tr>
                  <tr>
                    <td width="100%" style="font-size: 16px; color: #545454; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 24px;">Realiza tu pago y obtén domicilio gratis!
                    </td>
                  </tr>
              </table><!-- End Image 3 -->
            </div>
          </td>
        </tr>
      </table><!-- End 1st 2 Images -->

      <!-- Space -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
        <tr>
          <td width="100%" height="40">
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table><!-- End 3 Columns -->

<!-- Products CTA -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
  <tr>
    <td width="100%">

      <!-- Space -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="MobileCenter">
        <tr>
          <td width="100%" height="25">
          </td>
        </tr>
      </table>

      <!-- Icons -->
      <table width="360" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
        <tr>
          <td width="100%" style="text-align: center;">
            <a href="https://goo.gl/IbF365" style="display: inline-block; color: #30db79; font-weight: bold; border: 2px solid #30db79; padding: 15px 30px; font-size: 18px; text-decoration: none; font-family: Helvetica, Arial, sans-serif; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px;">Ver todos los productos</a>
          </td>
        </tr>
      </table>

      <!-- Space -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
        <tr>
          <td width="100%" height="55">
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table><!-- Products CTA -->

@stop