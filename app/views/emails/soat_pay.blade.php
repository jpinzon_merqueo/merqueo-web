@extends('emails/email_layout')

@section('content')
    <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; font-family: Helvetica, Arial, sans-serif;"
           class="scaleForMobile">
        <tr>
            <td width="100%">

                <!-- Space -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                    <tr>
                        <td width="100%">
                        </td>
                    </tr>
                </table>

                <!-- Header -->
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center" style="padding: 0 40px; color: #fff; background: #38466e">
                    <tr>
                        <td width="100%" align="center">
                            <p style="font-size: 30px; margin: 30px auto">
                                Transacción realizada <br>
                                <b>satisfactoriamente</b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="center">
                            <p style="margin: 0 auto 30px">
                                En unos minutos recibirás un correo desde <br>
                                Seguros Mundial con tu SOAT digital.
                            </p>
                        </td>
                    </tr>
                </table><!-- End Header -->
                <table width="550" height="3" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="background-color: #d0006f"></table>
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="background-color: #f7f7f7; color: #494d54; padding: 40px 21px">
                    <tr>
                        <td width="47%">
                            <img style="max-width: 100%; height: auto; margin-bottom: 20px" src="{{ asset_url() }}img/email_design/social/user.png" alt="user">
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            <img style="max-width: 100%; height: auto; margin-bottom: 20px" src="{{ asset_url() }}img/email_design/social/vehicle.png" alt="user">
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            Nombre
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            Marca
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{$taker["firstName"]}} {{$taker["lastName"]}}</b>
                            </p>
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{$runt["vehicleBrand"]}}</b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            Documento
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            Tipo
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{$document}}</b>
                            </p>
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{$runt["vehicleType"]}}</b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            Dirección
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            Modelo
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{$taker["address"]}}</b>
                            </p>
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{$runt["vehicleYear"]}}</b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            Teléfono
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            Placa
                        </td>
                    </tr>
                    <tr>
                        <td width="47%">
                            <p class="jh-button">
                                <b>{{ $taker['phone'] }}</b>
                            </p>
                        </td>
                        <td width="6%"></td>
                        <td width="47%">
                            <p class="jh-button">
                                <b> {{ $plate }}</b>
                            </p>
                        </td>
                    </tr>
                </table>
                <table width="550" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <p style="color: #d0006f; margin: 20px auto; font-size: 25px">
                                <b>¡Gracias por tu compra!</b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="color: #32363d">
                                Si tienes algún inconveniente con la generación del SOAT <br>
                                o deseas cancelar la compra, por favor comunícate con <br>
                                <b><a href="" target="_blank">info@mimotor.co</a></b> o al <b>3143936790</b> en Bogotá. <br>
                                <b><br><br>
                                    Recuerda que cualquier cambio deberá ser <br>
                                    realizado antes de las <span style="color: #d0006f;">5PM</span> del día de hoy
                                </b>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@stop