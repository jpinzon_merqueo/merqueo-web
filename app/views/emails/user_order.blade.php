@extends('emails/email_layout')

@section('content')
    <p>Buen día</p>
    <p>Adjunto envió factura de compra.</p>
    @if (!empty($content))
        <p>{{ $content }}</p>
    @endif
    <p>Gracias.</p>
@endsection