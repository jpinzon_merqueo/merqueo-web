@extends('emails/email_layout')

@section('content')

<tr>
    <td>
        <table style="font-family: sans-serif">
            <tr>
                <td valign="top"><br>
                    <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Reporte diario de pedidos</h1><br>
                    <h2 style="font-size:18px; font-weight:normal; margin:0;">Pedidos realizados ({{ $cont['date'] }})</h2><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <tr>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">&nbsp;</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Hoy</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Semana pasada</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Este mes</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Mes pasado</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cont['created'] as $city => $data)
                            @if ($city != 'totals')
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">{{ $city }}</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;">
                                {{ number_format($data['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_month'] }}
                            </td>
                        </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Totales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                {{ number_format($cont['created']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['created']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['created']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['created']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['created']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['created']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <h2 style="font-size:18px; font-weight:normal; margin:0;">Ventas pedidos realizados ({{ $cont['date'] }})</h2><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <tr>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">&nbsp;</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Hoy</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Semana pasada</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Este mes</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Mes pasado</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cont['created_total_amount'] as $city => $data)
                            @if ($city != 'totals')
                                <tr>
                                    <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">{{ $city }}</td>
                                    <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;">
                                        ${{ number_format($data['orders_today'], 0, ',', '.') }}
                                    </td>
                                    <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                        ${{ number_format($data['orders_week_ago'], 0, ',', '.') }}
                                    </td>
                                    <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                        {{ $data['percentage_day'] }}
                                    </td>
                                    <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                        ${{ number_format($data['orders_this_month'], 0, ',', '.') }}
                                    </td>
                                    <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                        ${{ number_format($data['orders_last_month'], 0, ',', '.') }}
                                    </td>
                                    <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                        {{ $data['percentage_month'] }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Subtotales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                ${{ number_format($cont['created_total_amount']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['created_total_amount']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['created_total_amount']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['created_total_amount']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['created_total_amount']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['created_total_amount']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Domicilios</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                ${{ number_format($cont['created_delivery_amount']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['created_delivery_amount']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['created_delivery_amount']['totals']['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['created_delivery_amount']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['created_delivery_amount']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['created_delivery_amount']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['created_delivery_amount']['totals']['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['created_delivery_amount']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Totales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                ${{ number_format(($cont['created_total']['totals']['orders_today']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format(($cont['created_total']['totals']['orders_week_ago']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['created_total']['totals']['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ ($cont['created_total']['totals']['percentage_day']) }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format(($cont['created_total']['totals']['orders_this_month']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format(($cont['created_total']['totals']['orders_last_month']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['created_total']['totals']['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ ($cont['created_total']['totals']['percentage_month']) }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <h2 style="font-size:18px; font-weight:normal; margin:0;">Pedidos entregados ({{ $cont['date'] }})</h2><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <tr>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">&nbsp;</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Hoy</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Semana pasada</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Este mes</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Mes pasado</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cont['delivered'] as $city => $data)
                            @if ($city != 'totals')
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">{{ $city }}</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;">
                                {{ number_format($data['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_month'] }}
                            </td>
                        </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Totales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                {{ number_format($cont['delivered']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['delivered']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['delivered']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['delivered']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['delivered']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['delivered']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <h2 style="font-size:18px; font-weight:normal; margin:0;">Ventas pedidos entregados ({{ $cont['date'] }})</h2><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <tr>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">&nbsp;</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Hoy</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Semana pasada</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Este mes</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Mes pasado</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cont['total_amount'] as $city => $data)
                            @if ($city != 'totals')
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">{{ $city }}</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;">
                                ${{ number_format($data['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                ${{ number_format($data['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                ${{ number_format($data['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                ${{ number_format($data['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_month'] }}
                            </td>
                        </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Subtotales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                ${{ number_format($cont['total_amount']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['total_amount']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['total_amount']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['total_amount']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['total_amount']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['total_amount']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Domicilios</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                ${{ number_format($cont['delivery_amount']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['delivery_amount']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['delivery_amount']['totals']['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['delivery_amount']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['delivery_amount']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format($cont['delivery_amount']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['delivery_amount']['totals']['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['delivery_amount']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Totales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                ${{ number_format(($cont['total']['totals']['orders_today']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format(($cont['total']['totals']['orders_week_ago']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['total']['totals']['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ ($cont['total']['totals']['percentage_day']) }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format(($cont['total']['totals']['orders_this_month']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                ${{ number_format(($cont['total']['totals']['orders_last_month']), 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($cont['total']['totals']['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ ($cont['total']['totals']['percentage_month']) }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <h2 style="font-size:18px; font-weight:normal; margin:0;">Adquisiciones ({{ $cont['date'] }})</h2><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <tr>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">&nbsp;</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Hoy</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Semana pasada</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Este mes</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Mes pasado</th>
                            <th align="center" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">Cambio</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cont['acquisitions'] as $city => $data)
                            @if ($city != 'totals')
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">{{ $city }}</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: normal;">
                                {{ number_format($data['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ number_format($data['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: normal;">
                                {{ $data['percentage_month'] }}
                            </td>
                        </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em; font-weight: bold;">Totales</td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #f3f1f1; border-bottom:1px solid #f3f1f1; border-right:1px solid #f3f1f1;font-weight: bold;">
                                {{ number_format($cont['acquisitions']['totals']['orders_today'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top"style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['acquisitions']['totals']['orders_week_ago'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_day'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['acquisitions']['totals']['percentage_day'] }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['acquisitions']['totals']['orders_this_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ number_format($cont['acquisitions']['totals']['orders_last_month'], 0, ',', '.') }}
                            </td>
                            <td align="center" valign="top" style="@if ($data['percentage_month'] < 0 ) color:red; @else color: green; @endif font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;font-weight: bold;">
                                {{ $cont['acquisitions']['totals']['percentage_month'] }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br><br>
                </td>
            </tr>
        </table>
    </td>
</tr>

@stop
