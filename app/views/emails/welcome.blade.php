@extends('emails/email_layout2_i18n')

@section('content')
    <table width="590" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; color: #545454"
           class="scaleForMobile">
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <img style="max-width: 100%" src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/header-bg2.png"
                                 alt="Header-bg">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <h2 style="margin: 0">
                            @if(!isset($name))
                                <b>!Hola,</b>
                                @else
                                <b>!Hola <span style="text-transform: uppercase">{{ $name }},</span>    </b>
                                @endif
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="font-size: 18px">Te estabamos esperando. Ahora puedes hacer tu mercado desde tu
                                casa, sin filas ni trancones, y lo mejor... con los precios más bajos.</p>
                        </td>
                    </tr>
                    @if ($referred['success'])
                    <tr>
                        <td align="center">
                            <p style="font-size: 20px"><b>Fuiste referido por <span style="text-transform: uppercase; color: black">{{ $referred['referrer_name'] }}</span>
                                    y acabas de recibir <br><span style="color: #d0006f; font-size: 28px">{{ currency_format($referred['amount']) }}</span> para tu primera compra</b></p>
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <h2 style="margin: 0; color: black"><b>¿Cómo usar la App?</b></h2>
                        </td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                <tr>
                                    <td style="width: 25%">
                                        <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/circulo1.png" alt="feature 1">
                                    </td>
                                    <td style="width: 25%">
                                        <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/circulo2.png" alt="feature 2">
                                    </td>
                                    <td style="width: 25%">
                                        <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/circulo3.png" alt="feature 3">
                                    </td>
                                    <td style="width: 25%">
                                        <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/circulo4.png" alt="feature 4">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <p style="margin-top: 0; padding: 0 5px;">Ingresa a Merqueo y registra tu
                                            dirección</p>
                                    </td>
                                    <td align="center">
                                        <p style="margin-top: 0; padding: 0 5px;">Agrega tus productos al carrito</p>
                                    </td>
                                    <td align="center">
                                        <p style="margin-top: 0; padding: 0 5px;">Selecciona tu método de pago</p>
                                    </td>
                                    <td align="center">
                                        <p style="margin-top: 0; padding: 0 5px;">Programa tu pedido y recíbelo en
                                            casa</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #d0006f; border-radius: 10px">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                                   style="color: #fff; padding: 10px">
                                <tr>
                                    <td style="border-style: dashed; border-color: white; border-width: 1px;  border-radius: 10px; padding: 10px;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="color: white">
                                            <tr>
                                                <td style="width: 50%" align="center">
                                                    <h2 style="margin: 0; font-family:Courier New, Courier, monospace; color: white">
                                                        RECIBE {{ $country_data['referred_credit'] }}</h2>
                                                    <p style="font-size: 18px; background-color: white; color: #656565; text-align: center; margin: 10px 30px; font-weight: bold; padding: 10px; border-radius: 10px">
                                                        {{ $referral_code }}
                                                    </p>
                                                </td>
                                                <td style="width: 50%">
                                                    <p style="font-size: 17px; color: white">
                                                        Invita a tus amigos a ser parte de Merqueo y regala
                                                        <b>{{ $country_data['referred_credit'] }}</b> para su primera compra. Una vez rediman tu código
                                                        recibirás <b>{{ $country_data['referred_by_credit'] }}</b>*
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <p style="font-size: 20px; margin:10px 0; color: white"><b>¡Comparte tu
                                                            código!</b>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                                           align="center">
                                                        <tr>
                                                            <td align="center">
                                                                <a id="link" type="button" target="_blank"
                                                                   href="https://api.whatsapp.com/send?text=¿Ya probaste Merqueo? {{ $name }} te regaló {{ $country_data['referred_credit'] }} para tu primer pedido. Descarga la app e ingresa el código {{ $referral_code }} ¡Vamos! {{ $country_data['web_view'] }}/registro/{{ $referral_code }}">
                                                                    <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/iconbox-1.png">
                                                                </a>
                                                            </td>
                                                            <td align="center">
                                                                <a id="facebook" type="button"
                                                                   href="https://www.facebook.com/dialog/share?app_id={{ Config::get('app.facebook_app_id') }}&display=popup&href={{ $country_data['web_view'] }}/registro/{{ $referral_code }}"
                                                                   target="_blank">
                                                                    <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/iconbox-2.png">
                                                                </a>
                                                            </td>
                                                            @if($country_data['country_code'] === 'co')
                                                            <td align="center">
                                                                <a id="twitter" type="button" target="_blank"
                                                                   href="https://twitter.com/intent/tweet?text=¿Ya probaste Merqueo? {{ $name }} te regaló {{ $country_data['referred_credit'] }} para tu primer pedido. Descarga la app e ingresa el código {{ $referral_code }} ¡Vamos! {{ $country_data['web_view'] }}/registro/{{ $referral_code }}"
                                                                >
                                                                    <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/iconbox-3.png">
                                                                </a>
                                                            </td>
                                                            @endif
                                                            <td align="center">
                                                                <a id="messenger" type="button"
                                                                   href="fb-messenger://share/?link={{ $country_data['web_view'] }}/registro/{{ $referral_code }}&app_id={{ Config::get('app.facebook_app_id') }}">
                                                                    <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/iconbox-5.png">
                                                                </a>
                                                            </td>
                                                            <td align="center">
                                                                <a id="email" type="button"
                                                                   href="mailto:?subject=¿Ya%20probaste%20Merqueo?%20{{ $name }}%20te%20regaló%20{{ $country_data['referred_credit'] }}&body=¿Ya%20probaste%20Merqueo?%20{{ $name }}%20te%20regaló%20{{ $country_data['referred_credit'] }}%20tu%20primer%20pedido.%20Descarga%20app%20e%20ingresa%20el%20código%20{{ $referral_code }}%20¡Vamos!%20{{ $country_data['web_view'] }}/registro/{{ $referral_code }}">
                                                                    <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/iconbox-6.png">
                                                                </a>
                                                            </td>
                                                            <td align="center">
                                                                <a target="_blank" href="sms:?body=¿%20probaste%20Merqueo?%20{{ $name }}%20te%20regaló%20{{ $country_data['referred_credit'] }}%20para%20tu%20primer%20pedido.%20Descarga%20la%20app%20e%20ingresa%20el%20código%20{{ $referral_code }}%20¡Vamos!%20{{ $country_data['web_view'] }}/registro/{{ $referral_code }}">
                                                                    <img src="https://merqueo.s3-us-west-1.amazonaws.com/images/email/iconbox-7.png">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <p style="font-size: 16px; margin: 10px 0;">*Vigencia del crédito {{ $country_data['referred_credit_expiration_days'] }}
                                                        días, válido para pedidos superiores a {{ $country_data['minimum_order_total_use_credit'] }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="60"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="{{ $country_data['web_view'] }}" style="text-decoration: none; color: white; background: #d0006f; padding: 15px; font-size: 16px; border-radius: 40px" target="_blank">COMIENZA AHORA</a>
                        </td>
                    </tr>
                    <tr>
                        <td height="60"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="font-size: 18px; margin: 0">Si necesitas ayuda, comunícate con nosotros al correo electrónico</p>
                            <p style="font-size: 18px; margin: 0"><b>{{ $country_data['admin_email'] }}</b></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@stop
