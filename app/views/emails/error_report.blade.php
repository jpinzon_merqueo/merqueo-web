@extends('emails/email_layout')

@section('content')

<tr>
    <td>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;margin:0px;padding:0px;font-family:Helvetica;font-size:12px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;width:100%!important;line-height:100%!important">
            <tbody>
                <tr>
                    <td width="10" valign="top" style="border-collapse:collapse"><br></td>
                    <td valign="top" align="center" style="border-collapse:collapse">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;width:600px;max-width:600px;background-color:rgb(255,255,255)">
                            <tbody>
                                <tr>
                                    <td width="600" valign="top" align="center" style="border-collapse:collapse">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                            <tbody>
                                                <tr>
                                                    <td align="left" valign="top" style="border-collapse:collapse;padding-bottom:28px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="border-collapse:collapse;padding-bottom:28px">
                                                        <span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;display:block;margin:0px;font-size:16px">Ha recibido este mensaje porque se reporta <span>&nbsp;</span><strong><u>Hoy</u></strong><span>&nbsp;</span>un error recurrente en el sistema de <span>&nbsp;</span><em>'{{ getenv('APP_URL') }}'</em><span>&nbsp;</span>.</span>
                                                    </td>
                                                </tr>
                                                @foreach ($errors as $error)
                                                    <tr>
                                                        <td align="center" valign="top" style="border-collapse:collapse;padding-bottom:28px;padding-top:28px;background-color:rgb(249,250,251)">
                                                            <span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;display:block;margin:0px;font-size:14px;font-weight:900;color:rgb(167,173,181);text-transform:uppercase;line-height:21px">Datos de error reportado</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="border-collapse:collapse;padding-bottom:42px;background-color:rgb(249,250,251)">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" width="20" style="border-collapse:collapse"></td>
                                                                        <td align="center" valign="top" style="border-collapse:collapse">
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" colspan="4" valign="middle" style="border-collapse:collapse;background-color:rgb(224,228,232);padding:6px 20px;border:1px solid rgb(204,204,204)"><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;display:block;margin:0px;font-size:16px">Resumen General</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center" height="84" valign="middle" width="20%" style="border-collapse:collapse;border:1px solid rgb(204,204,204)"><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:16px;font-weight:900; padding: 10px;">{{ $error['cant'] }}<span>&nbsp;</span></span><br><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:12px;text-transform:uppercase;color:rgb(153,153,153)">Errores</span></td>
                                                                                        <td align="center" height="84" valign="middle" width="20%" style="border-collapse:collapse;border-width:1px 0px;border-top-style:solid;border-bottom-style:solid;border-top-color:rgb(204,204,204);border-bottom-color:rgb(204,204,204)"><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:14px; text-align: center; font-weight:900; padding: 10px;">{{ $error['updated_at']}}</span><br><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:12px;text-transform:uppercase;color:rgb(153,153,153); ">Fecha</span></td>
                                                                                        <td align="center" height="84" valign="middle" width="20%" style="border-collapse:collapse;border:1px solid rgb(204,204,204);"><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:14px; text-align: center; font-weight:900; padding: 10px;">{{ $error['url']}}</span><br><span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:12px;text-transform:uppercase;color:rgb(153,153,153); ">URL</span></td>
                                                                                        <td align="center" height="84" valign="middle" width="40%" style="border-collapse:collapse;border:1px solid rgb(204,204,204); max-width:350px; padding: 10px;">
                                                                                            <span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:16px;font-weight:900; max-width:450px;word-wrap:break-word;">{{ $error['description'] }}<span>&nbsp;</span>
                                                                                            </span>
                                                                                            <br>
                                                                                            <span style="font-family:lato,'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:28px;margin:0px;font-size:12px;text-transform:uppercase;color:rgb(153,153,153)">Descripción</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td align="center" valign="top" width="20" style="border-collapse:collapse"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top" style="border-collapse:collapse">&nbsp;</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="10" valign="top" style="border-collapse:collapse">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>

@stop