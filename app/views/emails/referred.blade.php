@extends('emails/email_layout2')

@section('content')
    <table width="590" border="0" cellpadding="0" cellspacing="0" align="center"
           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; color: #545454"
           class="scaleForMobile">
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="center">
                            <img style="max-width: 100%" src="{{ asset_url() }}img/email_design/ganaste3.png" alt="Header-bg">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <p style="font-size: 18px; margin: 0">Gracias a ti, <b
                            style="text-transform: uppercase; color: black">{{ $username_referrer }}</b> se unió a
                    Merqueo.</p>
                <p style="margin: 0; font-size: 18px">Ahora tienes <b style="color: black">{{ currency_format($referred_by_amount) }}</b> en tu cuenta,
                    para que continúes ahorrando.</p>
            </td>
        </tr>
        <tr>
            <td height="40"></td>
        </tr>
        <tr>
            <td style="background-color: #d0006f; border-radius: 10px">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                       style="color: #fff; padding: 10px">
                    <tr>
                        <td style="border-style: dashed; border-color: white; border-width: 1px;  border-radius: 10px; padding: 10px;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                                   style="color: white">
                                <tr>
                                    <td align="center">
                                        <p style="font-weight: bold; color: white; font-size: 18px; margin: 0; text-transform: uppercase; font-family:Courier New, Courier, monospace">
                                            Sigue compartiendo <br>
                                            Tu código y obtendrás más crédito*
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <p style="width: 40%; font-size: 18px; background-color: white; color: black; text-align: center; margin: 10px 30px; font-weight: bold; padding: 10px; border-radius: 10px">
                                            {{$referal_code}}
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <img style="height: auto;width: 80px;margin-left: -10px;margin-bottom: -25px;"
                                                         src="{{ asset_url() }}img/email_design/fest-left.png">
                                                </td>
                                                <td align="center">
                                                    <p style="margin: 0; font-size: 18px; color: white">Crédito para tus
                                                        siguientes compras:</p>
                                                    <p style="color: white; font-size: 50px; margin: 0; font-weight: bold">
                                                        {{ currency_format($credit_available) }}</p>
                                                </td>
                                                <td align="right">
                                                    <img style="height: auto;width: 80px;margin-right: -10px;margin-bottom: -25px;"
                                                         src="{{ asset_url() }}img/email_design/fest-right.png">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="40"></td>
        </tr>
        <tr>
            <td align="center">
                <h2 style="margin: 0; color: black"><b>¿Compartir Código?</b></h2>
            </td>
        </tr>
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0"
                       align="center">
                    <tr>
                        <td align="center">
                            <a id="link" type="button" target="_blank"
                               href="https://api.whatsapp.com/send?text=¿Ya probaste Merqueo? {{ $username }} te regaló {{ currency_format($referred['amount']) }} para tu primer pedido. Descarga la app e ingresa el código {{$referal_code}} ¡Vamos! {{ web_url() }}/registro/{{$referal_code}}">
                                <img src="{{ asset_url() }}img/email_design/social/icono1.png">
                            </a>
                        </td>
                        <td align="center">
                            <a id="facebook" type="button"
                               href="https://www.facebook.com/dialog/share?app_id={{ Config::get('app.facebook_app_id') }}&display=popup&href={{ web_url() }}/registro/{{$referal_code}}"
                               target="_blank">
                                <img src="{{ asset_url() }}img/email_design/social/icono2.png">
                            </a>
                        </td>
                        <td align="center">
                            <a id="twitter" type="button" target="_blank"
                               href="https://twitter.com/intent/tweet?text=¿Ya probaste Merqueo? {{ $username }} te regaló {{ currency_format($referred['amount']) }} para tu primer pedido. Descarga la app e ingresa el código {{$referal_code}} ¡Vamos! {{ web_url() }}/registro/{{$referal_code}}"
                            >
                                <img src="{{ asset_url() }}img/email_design/social/icono3.png">
                            </a>
                        </td>
                        <td align="center">
                            <a id="messenger" type="button"
                               href="fb-messenger://share/ web_url() }}/registro/{{$referal_code}}&app_id={{ Config::get('app.facebook_app_id') }}">
                                <img src="{{ asset_url() }}img/email_design/social/icono5.png">
                            </a>
                        </td>
                        <td align="center">
                            <a id="email" type="button"
                               href="mailto:?subject=¿Ya%20probaste%20Merqueo?%20{{ $username }} %20te%20regaló%20{{ currency_format($referred['amount']) }}&body=¿Ya%20probaste%20Merqueo?%20{{ $username }}%20te%20regaló%20{{ currency_format($referred['amount']) }}%20tu%20primer%20pedido.%20Descarga%20app%20e%20ingresa%20el%20código%20{{$referal_code}}%20¡Vamos!%20{{ web_url() }}/registro/{{$referal_code}}">
                                <img src="{{ asset_url() }}img/email_design/social/icono6.png">
                            </a>
                        </td>
                        <td align="center">
                            <a target="_blank"
                               href="sms:?body=¿%20probaste%20Merqueo?%20{{ $username }}%20te%20regaló%20{{ currency_format($referred['amount']) }}%20para%20tu%20primer%20pedido.%20Descarga%20la%20app%20e%20ingresa%20el%20código%20{{$referal_code}}%20¡Vamos!%20{{ web_url() }}/registro/{{$referal_code}}">
                                <img src="{{ asset_url() }}img/email_design/social/icono7.png">
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center">
                <p style="font-size: 16px; margin: 10px 0;">*Vigencia del crédito {{$expiration_days}}
                    días, válido para pedidos superiores a {{ currency_format($minimum_discount_amount)   }}</p>
            </td>
        </tr>
        <tr>
            <td height="40"></td>
        </tr>
        <tr>
            <td align="center">
                <p style="font-size: 18px; margin: 0">Si necesitas ayuda, comunícate con nosotros al correo
                    electrónico</p>
                <p style="font-size: 18px; margin: 0"><b>{{ Config::get('app.admin_email') }}</b></p>
            </td>
        </tr>
        <tr>
            <td height="40"></td>
        </tr>
    </table>
@stop