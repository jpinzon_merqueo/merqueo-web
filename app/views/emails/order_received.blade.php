@extends('emails/email_layout_i18n')
@section('title')Merqueo | Pedido Recibido! @stop
@section('content')

  <!-- HEADER -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
    <tr>
      <td width="100%">
        <!-- Header -->
        <table style="padding: 30px 0;" width="700"  bgcolor="#23bcbb" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td style="font-size: 16px; color: #FFFFFF; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" width="100%">
              <center><img src="{{ asset_url() }}img/email_design/20180418/icono-pedido-recibido.png" alt=""></center>
              <h1 style="font-weight: normal;">HEMOS RECIBIDO <br><span style="font-weight: bold">TU MERQUEO</span></h1>
              <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20180418/location.png" alt="">Entrega en {{ $order_group->user_address.' '.$order_group->user_address_further }}</p>
              @if($orders[0]->payment_method == 'Tarjeta de crédito' || $orders[0]->payment_method == 'Tarjeta de crédito' || $orders[0]->payment_method == 'Datáfono' || $orders[0]->payment_method == 'Terminal')
                <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20170727/card.png" alt="">Método de pago {{ $orders[0]->payment_method }}</p>
              @elseif($orders[0]->payment_method == 'Efectivo' || $orders[0]->payment_method == 'Efectivo y datáfono')
                <p><img style="position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20180418/cash.png" alt="">Método de pago {{ $orders[0]->payment_method }}</p>
              @endif
            </td>
          </tr>
        </table><!-- End Header -->

      </td>
    </tr>
  </table><!-- End Main -->

  @if(isset($applyPromotionaFirstTHousandOrdersMexico))
    @if($applyPromotionaFirstTHousandOrdersMexico)
      <!-- apply promotion mexico 100 primeros pedidos -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
        <tr>
          <td width="100%">
            <!-- Header -->
            <table style="padding: 0 0;" width="700"  bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
                <tr>
                    <td>
                    <img src="https://d50xhnwqnrbqk.cloudfront.net/images/email/envio_gratis.jpg" alt="¡GANASTE! TUS ENVÍOS SERÁN GRATIS POR 1 AÑO.">
                    </td>
                </tr>
            </table>
          </td>
        </tr>
      </table>
      <!-- End Header apply promotion mexico 100 primeros pedidos -->
    @endif
  @endif

  <!-- 3 Columns Middle Images -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
    <tr>
      <td width="100%">
        <!-- Space -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
          <tr>
            <td width="100%" height="30">
            </td>
          </tr>
        </table>
      @foreach($orders as $order)
        <table width="700px" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
            <tbody>
            <tr><td>
                <!-- 1st 3 Images -->
                <h1 style="font-size: 20px; color: #3f3f3f; font-family: Helvetica, Arial, sans-serif">Resumen del pedido</h1>
                <p style="width: 48%;float: left;color: #3f3f3f; text-align: left; font-family: Helvetica, Arial, sans-serif">Número: {{ $order->reference }}</p>
                <p style="width: 48%;float: right;color: #3f3f3f; padding: 5px; text-align: center; font-family: Helvetica, Arial, sans-serif; border-radius: 10px; background-color: #c5e6fd; font-weight: bold">
                  @if(isset($delivery_express) && $delivery_express)
                    {{ $order->delivery_time }}
                  @else
                    Entrega {{ format_date('day_name', $order->delivery_date) }} entre {{ $order->delivery_time }}
                  @endif
                </p>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #545454; font-family: Helvetica, Arial, sans-serif; font-size: 14px;">
                  <tr style="text-align: center; font-weight: bold">
                    <td style="border-bottom: 1px solid #ddd; padding: 5px; color: #3f3f3f;">Imagen</td>
                    <td style="border-bottom: 1px solid #ddd; padding: 5px; color: #3f3f3f;">Producto</td>
                    <td style="border-bottom: 1px solid #ddd; padding: 5px; color: #3f3f3f;">Cantidad</td>
                    <td style="border-bottom: 1px solid #ddd; padding: 5px; color: #3f3f3f; text-align: right;">Precio</td>
                    <td style="border-bottom: 1px solid #ddd; padding: 5px; color: #3f3f3f; text-align: right;">Total</td>
                  </tr>
                  @if ( !empty($products) )
                    <?php $total_amount = 0; ?>
                    @foreach($products as $product)
                      @if ($product['order_type'] == $order->type)
                        <tr height="70">
                          <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px; width: 20%">
                            <img style="max-width: 100px" src="{{ $product['img'] }}" width="auto" height="100px" alt=""></td>
                          <td style="border-bottom: 1px solid #ddd; padding: 5px; width: 43%">
                            {{ $product['name'] }} 
                            {{ $product['quantity'] }} 
                            {{ $product['unit'] }}
                          </td>
                          <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px; width: 12%">
                            {{ $product['qty'] }}
                          </td>
                          <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; width: 15%">
                            {{ CountryCurrencyFormat($country_data['format_value'], $product['price']) }}
                          </td>
                          <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; width: 15%">
                            {{ CountryCurrencyFormat($country_data['format_value'], $product['total']) }}
                          </td>
                        </tr>
                        <?php $total_amount += $product['total']; ?>
                      @endif
                    @endforeach
                  @endif

                  <tr height="30">
                    <td colspan="4" style="text-align: left; padding: 5px; font-weight: bold">Subtotal</td>
                    <td style="text-align: right; padding: 5px">{{ CountryCurrencyFormat($country_data['format_value'], $total_amount) }}</td>
                  </tr>
                  <tr height="30">
                    <td colspan="4" style="text-align: left; padding: 5px; font-weight: bold">
                      @if($country_data['country_code'] === 'mx')
                        Costo de envío
                      @else
                        Costo domicilio
                      @endif
                    </td>
                    <td style="text-align: right; padding: 5px">{{ CountryCurrencyFormat($country_data['format_value'], $order->delivery_amount) }}</td>
                  </tr>
                  <tr height="30">
                    <td colspan="4" style="text-align: left; padding: 5px; font-weight: bold">Descuento</td>
                    <td style="text-align: right; padding: 5px">@if (!empty($order->discount_amount))-@endif{{ CountryCurrencyFormat($country_data['format_value'], $order->discount_amount) }}</td>
                  </tr>
                  <tr height="50">
                    <td colspan="4" style="border-top: 1px solid #ddd; text-align: left; font-size: 18px; font-weight: bold; padding: 5px; color: #3f3f3f;">Total a pagar</td>
                    <td style="border-top: 1px solid #ddd; text-align: right; font-size: 15px; font-weight: bold; padding: 15px 5px; color: #073349; color: #3f3f3f;">{{ CountryCurrencyFormat($country_data['format_value'], $total_amount + $order->delivery_amount - $order->discount_amount) }}</td>
                  </tr>
                </table><!-- End Message 2 -->
              </td></tr>
            </tbody>
          </table>
        @endforeach
      </td>
    </tr>
  </table><!-- End 3 Columns -->

  <!-- Products CTA -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
    <tr>
      <td width="100%">

        <!-- Space -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="MobileCenter">
          <tr>
            <td width="100%" height="25">
            </td>
          </tr>
        </table>

        <!-- Icons -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td width="100%">
              <!-- Icons -->
              <table style="padding: 2% 0;" width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                <tr>
                  <td width="100%" style="text-align: center;">
                    <p style="text-align: center; color: #3f3f3f; font-size: 18px; font-weight: 100; font-family: Helvetica, Arial, sans-serif; margin: 0;">Sí necesitas ayuda, comun&iacute;cate con nosotros al correo electr&oacute;nico<br><b>{{ $country_data['admin_email'] }}<b></p><br>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table><!-- Products CTA -->

      </td>
    </tr>
  </table><!-- Products CTA -->
@stop
