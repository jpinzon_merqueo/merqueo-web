@extends('emails/email_layout')

@section('content')
    <div style="position: relative; display: block; margin-top: 15px; margin-bottom: 15px">
        {{--Es necesario que se imprima el contenido HTML correctamente--}}
        <?= $content ?>
        {{--Es necesario que se imprima el contenido HTML correctamente--}}
    </div>

    <hr style="border-color: #e8e8e852;">

    @if (!isset($is_mock))
        <div style="text-align: center; display: block">
            ¿Ha sido resuelta tu solicitud?
        </div>
        <div style="text-align: center; display: block; margin-top: 15px">
            <?php $button_style = 'color: white; padding: 4px 15px; margin: 5px 5px; text-align: center; text-decoration: none; border-radius: 0.1em;'; ?>
            <a style="background-color: #10CA93; {{ $button_style }}"
               href="{{ route('customerService.Ticket.solutionRate', [$call->token, 1]) }}">
                Sí
            </a>
            <a style="background-color: #FF4545; {{ $button_style }}"
               href="{{ route('customerService.Ticket.solutionRate', [$call->token, 0]) }}">
                No
            </a>
        </div>
    @endif

    <p style="margin-top: 30px;font-size: 10px;color: #989898;">
        MERQUEO S.A.S. expresa que esta dirección de correo y cualquier documento adjunto es confidencial y es
        únicamente para la entidad o persona para la que va dirigido. Este correo puede contener datos personales los
        cuales están sujetos a la Política de Privacidad de la compañía (https://merqueo.com/politicas-de-privacidad)
        conforme a la cual el tratamiento de esos datos se circunscribe exclusivamente a la autorización dada por el
        titular al responsable y/o encargado la cual impide su divulgación o uso por parte de terceros no autorizados.
        En consecuencia, en caso de recibir ese mensaje por error por favor informe a quien lo remitió y elimínelo de su
        sistema. Si usted no es la persona a quien se dirige el mensaje, está siendo notificado por este medio que
        cualquier divulgación, copia, distribución o uso de la información aquí contenida es estrictamente prohibido e
        ilegal. El mensaje expresa la opinión del autor pero no necesariamente la opinión oficial de MERQUEO S.A.S. Si
        tiene alguna duda sobre el contenido o alcance de esta declaración, comuníquela a {{ Config::get('app.admin_email') }}.
    </p>

    <p style="font-size: 10px;color: #989898;">
        This e-mail (and any attachments) is from MERQUEO S.A.S. and is intended solely for the use of the individual(s)
        to whom it is addressed. This e-mail can include personal data that will be treated in accordance with the
        Company’s Privacy Policy (https://merqueo.com/politicas-de-privacidad). Company will treat your personal
        information as confidential and your details will not be given or sold to non-authorized third parties. If you
        believe you received this e-mail in error, please notify the sender immediately, delete the e-mail from your
        computer and do not copy or disclose it to anyone else. If you are not the intended recipient, any retention,
        dissemination, distribution or copying of this message is strictly prohibited and penalized by law. The
        information and views set out in this e-mail are those of the author(s) and do not necessarily reflect the
        official opinion of MERQUEO S.A.S. If you have any questions or comments concerning this Disclaimer, please
        contact us at {{ Config::get('app.admin_email') }}.
    </p>

@endsection